<?php
require_once 'class.phpmailer.php';

if(file_exists(dirname(dirname(dirname(dirname(__FILE__)))).'/application/models/core/Db.php'))
{
	require_once dirname(dirname(dirname(dirname(__FILE__)))).'/application/models/core/Db.php';
}

function sMail($to, $toname, $sub, $msg, $ccadd, $attach_file_path="") {
//	$db = new Db();
//	$db->ConnectionOpen();
//	$result_smtp = $db->Fetch('smtp_setting', '*', 'smtp_id = 1');
//	$row_smtp = mysql_fetch_object($result_smtp);
        date_default_timezone_set('Asia/Kolkata');
	try {
		$mail = new PHPMailer(true); //New instance, with exceptions enabled

		$mail->IsSMTP();                           // tell the class to use SMTP
//		$mail->SMTPAuth   = true;                  // enable SMTP authentication
//               // $mail->SMTPDebug  = 2; // enables SMTP debug information (for testing)
//                $mail->SMTPSecure = 'tls';
//		$mail->Port       = stripslashes($row_smtp->port);                    // set the SMTP server port
//		$mail->Host       = stripslashes($row_smtp->host); // SMTP server
//		$mail->Username   = stripslashes($row_smtp->username);     // SMTP server username
//		$mail->Password   = stripslashes($row_smtp->password);
//		$mail->From       = $mail->Username;            // SMTP server password
//		$mail->FromName   = stripslashes($row_smtp->from_name);
//		$mail->AddAddress($to,$toname);



    $mail->SMTPAuth   = true;
		$mail->SMTPDebug  = 1;
  	$mail->SMTPSecure = 'tls';
		$mail->Port       = 587;                    // set the SMTP server port
		$mail->Host       = "smtp.gmail.com"; // SMTP server
		$mail->Username   = "support@hellouni.org"; // SMTP server username
		$mail->Password   = "2023@#support";
		$mail->From       = "support@hellouni.org";            // SMTP server password
		$mail->FromName   = "HelloUni Organization";
    $mail->AddAddress($to,$toname);

//		if(!empty($attach_file_path)) {
//			if(file_exists($attach_file_path)) {
//				$mail->AddAttachment($attach_file_path);
//			} else {
//				echo "File or path not found";
//				exit;
//			}
//		}
		$attach_file_path = explode(",",$attach_file_path);
		$basePath = $_SERVER['DOCUMENT_ROOT'];

		if (is_array($attach_file_path)){
			if(count($attach_file_path) > 0){
				for($path = 0; $path < count($attach_file_path); $path++){
					if(!empty($attach_file_path[$path])) {
						if(file_exists($basePath.'/'.$attach_file_path[$path])) {
							$mail->AddAttachment($basePath.'/'.$attach_file_path[$path]);
						}
					}
				}
			}
		}else{
			if(!empty($attach_file_path)) {
				if(file_exists($attach_file_path)) {
					$mail->AddAttachment($attach_file_path);
				} else {
					echo "File or path not found2";
					exit;
				}
			}
		}

		if(!empty($ccadd)){
			$ccaddArray = explode(",",$ccadd);
			foreach($ccaddArray as $key) {
	    			$mail->AddCC($key);
			}
		}


		$mail->Subject  = $sub;
		$mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
		$mail->WordWrap = 80; // set word wrap
		$mail->MsgHTML(nl2br($msg));
		$mail->IsHTML(true); // send as HTML
		$mail->Send();
		return true;
	}  catch (phpmailerException $e) {
           // echo $e->errorMessage(); //Pretty error messages from PHPMailer
          } catch (Exception $e) {
           // echo $e->getMessage(); //Boring error messages from anything else!
          }
}



function sMailDist($to, $toname, $sub, $msg, $attach_file_path="") {
	$db = new Db();
	$db->ConnectionOpen();
	$result_smtp = $db->Fetch('smtp_setting', '*', 'smtp_id = 2');
	$row_smtp = mysql_fetch_object($result_smtp);
	try {
		$mail = new PHPMailer(true); //New instance, with exceptions enabled

		$mail->IsSMTP();                           // tell the class to use SMTP
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->Port       = stripslashes($row_smtp->port);                    // set the SMTP server port
		$mail->Host       = stripslashes($row_smtp->host); // SMTP server
		$mail->Username   = stripslashes($row_smtp->username);     // SMTP server username
		$mail->Password   = stripslashes($row_smtp->password);
		$mail->From       = $mail->Username;          // SMTP server password
		$mail->FromName   = stripslashes($row_smtp->from_name);
		$mail->AddAddress($to,$toname);

		if(!empty($attach_file_path)) {
			if(file_exists($attach_file_path)) {
				$mail->AddAttachment($attach_file_path);
			} else {
				echo "File or path not found";
				exit;
			}
		}
		$mail->Subject  = $sub;
		$mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
		$mail->WordWrap = 80; // set word wrap
		$mail->MsgHTML(nl2br($msg));
		$mail->IsHTML(true); // send as HTML
		$mail->Send();
		return true;
	} catch (phpmailerException $e) {
		return false;
	}
}



/**
   * Mail to many users
   * Returns false if mail not send
   * @param array $to_names
   * @param array $to_emails email ids
   * @param string $sub email subject.
   * @param string $msg email message.
   * @param string $from_name name of sender
   * @param string $attach_file_path absolute path of the file
   * @return bool
   */
function MailTo($to_names, $to_emails, $sub, $msg, $from_name='', $attach_file_path='', $cc_names=null, $cc_email=null, $bcc_names=null, $bcc_email=null) {
	if(!is_array($to_names) || !is_array($to_emails)) {
		echo "Invalid arguments passed.";
	}
	if(count($to_names) != count($to_emails)) {
		echo "Names and email count not matched.";
	}

	$db = new Db();
	$db->ConnectionOpen();
	$result_smtp = $db->Fetch('smtp_setting', '*', 'smtp_id = 1');
	$row_smtp = mysql_fetch_object($result_smtp);
	try {
		$mail = new PHPMailer(true); //New instance, with exceptions enabled

		$mail->IsSMTP();                           // tell the class to use SMTP
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->Port       = stripslashes($row_smtp->port);                    // set the SMTP server port
		$mail->Host       = stripslashes($row_smtp->host); // SMTP server
		$mail->Username   = stripslashes($row_smtp->username);     // SMTP server username
		$mail->Password   = stripslashes($row_smtp->password);            // SMTP server password


		//$mail->AddReplyTo("ambuja.grihashilpi@gmail.com","Ambuja Grihshipli");

		//$mail->From       = $from;
		$mail->FromName = (isset($from_name) && !empty($from_name)) ? $from_name : stripslashes($row_smtp->from_name);

		foreach($to_names as $key => $value) {
			$email_name = trim($to_emails[$key]);
			$email_add = trim($value);
			if(!empty($email_name) && !empty($email_add)) {
				$mail->AddAddress($email_name, $email_add);
			}
		}

		if(!empty($cc_names)){
			foreach($cc_names as $key => $value) {
				$cc_name = trim($cc_email[$key]);
				$ccemail_add = trim($value);
				if(!empty($cc_name) && !empty($ccemail_add)){
					$mail->AddCC($cc_name, $ccemail_add);
				}
			}
		}

		if(!empty($bcc_names)){
			foreach($bcc_names as $key => $value) {
			$bcc_name = trim($bcc_email[$key]);
			$bccemail_add = trim($value);
				if(!empty($bcc_name) && !empty($bccemail_add)){
					$mail->AddBCC($bcc_name, $bccemail_add);
				}
			}
		}


		if(!empty($attach_file_path)) {
			if(file_exists($attach_file_path)) {
				$mail->AddAttachment($attach_file_path);
			} else {
				echo "File or path not found";
				exit;
			}
		}

		$mail->Subject  = $sub;

		$mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
		$mail->WordWrap = 80; // set word wrap
		$mail->MsgHTML(nl2br($msg));
		$mail->IsHTML(true); // send as HTML

//		print_r($mail);

		$mail->Send();
		return true;
	} catch (phpmailerException $e) {
		return false;
	}
}
?>
