<?php
require 'vendor/autoload.php';
ini_set('memory_limit', '-1');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Shared\Date;

$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
$reader->setReadDataOnly(TRUE);
$spreadsheet = $reader->load("eduloans_1.xlsx");

//$worksheet = $spreadsheet->getActiveSheet();
$worksheet = $spreadsheet->getSheet(0);

$host   =       'localhost';
$user   =       'root';
$pass   =       'root123';
$db     =       'eduloans';

$conn = mysqli_connect($host, $user, $pass, $db);
if(!$conn){
        die(mysqli_connect_error());
}

//echo '<table>' . PHP_EOL;
foreach ($worksheet->getRowIterator() as $rowIndex => $row) {
	if($rowIndex == 1){
		continue;
	}
	$query = "INSERT INTO student_dump values(";
	//echo '<tr>' . PHP_EOL;
    	$cellIterator = $row->getCellIterator();
    	$cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                                                       //    even if a cell value is not set.
                                                       // By default, only cells that have a value
                                                       //    set will be iterated.
    	foreach ($cellIterator as $index => $cell) {
		if($index == 'T'){
			break;
		}
		$value = $cell->getValue();
		//if(PhpOffice\PhpSpreadsheet\Shared\Date::isDateTime($cell)){
		if($index == 'B' || $index == 'O'){
			$date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($cell->getValue());
                        $dateTimeArray = (array)$date;
			$explodeDateTime = explode(" ", $dateTimeArray["date"]);
          //              echo '<td>' . $explodeDateTime[0] . '</td>' . PHP_EOL;
			$value = $explodeDateTime[0];
		}
		else{
	    //    	echo '<td>' . $cell->getValue() . '</td>' . PHP_EOL;
		}

		$query .= "'" . mysqli_escape_string($conn, $value) . "',";
    	}

	$query = substr($query, 0, -1) . ")";
    	//echo '</tr>' . PHP_EOL;

	if(!mysqli_query($conn, $query)){
		echo $query;
		die(mysqli_error($conn));
	}
}
echo "Done";
//echo '</table>' . PHP_EOL;

/*
$host   =       'localhost';
$user   =       'root';
$pass   =       'root123';
$db     =       'casinocoin';

$conn = mysqli_connect($host, $user, $pass, $db);
if(!$conn){
	die(mysqli_connect_error());
}
*/
?>
