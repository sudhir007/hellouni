<?php
class College_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getAllColleges()
    {
        $this->db->select('colleges.*, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('colleges');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->order_by('colleges.id', 'desc');
        $result=$this->db->get()->result();
		return $result;
    }

    function getAllCollegesByUniversity($universityId)
    {
        $this->db->select('colleges.*, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('colleges');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->where('universities.id', $universityId);
        $result=$this->db->get()->result();
		return $result;
    }

    function getCollegeById($collegeId)
    {
        $this->db->from('colleges');
        $this->db->where('id', $collegeId);
        $result=$this->db->get()->result();
		return $result[0];
    }

    function getCollegeByIdNew($collegeId)
    {
      $this->db->select('colleges.*, campuses.id AS campus_id, universities.id AS university_id');
      $this->db->from('colleges');
      $this->db->join('campuses', 'campuses.id = colleges.campus_id');
      $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->where('colleges.id', $collegeId);
        $result=$this->db->get()->result();
		return $result[0];
    }

    function addCollege($collegeData)
    {
   	    $this->db->insert('colleges', $collegeData);
        $collegeId = $this->db->insert_id();
        return $collegeId;
    }

    function editCollege($collegeId, $collegeData)
    {
        $this->db->where('id', $collegeId);
   	    $this->db->update('colleges', $collegeData);
    }

    function getAllCategories()
    {
        $this->db->from('colleges_categories');
        $result = $this->db->get()->result();
        return $result;
    }

    function getCollegeByName($collegeName, $campusId)
    {
        $this->db->from('colleges');
        $this->db->where('campus_id', $campusId);
        $this->db->where('name', $collegeName);
        $query=$this->db->get();
        //echo $this->db->last_query();
		return $query->result_array();
    }


     /* server side datatable code for college start */

    var $column_order = array('colleges.id','colleges.name','colleges.campus_name','universities.name',null); //set column field database for datatable orderable
    var $column_search = array('colleges.id','colleges.name','colleges.campus_name','universities.name'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('colleges.id' => 'desc'); // default order

    private function _get_datatables_query_getAllCollegesByUniversity($universityId)
    {
        $this->db->select('colleges.*, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('colleges');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->where('universities.id', $universityId);
        //$this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_getAllCollegesByUniversity($universityId)
    {
        $this->_get_datatables_query_getAllCollegesByUniversity($universityId);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_getAllCollegesByUniversity($universityId)
    {
        $this->_get_datatables_query_getAllCollegesByUniversity($universityId);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_getAllCollegesByUniversity($universityId)
    {
        //$this->db->from($this->table);
        $this->db->select('colleges.*, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('colleges');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->where('universities.id', $universityId);
        
        return $this->db->count_all_results();
    }

    private function _get_datatables_query_getAllColleges()
    {
        $this->db->select('colleges.*, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('colleges');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->order_by('colleges.id', 'desc');
        //$this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_getAllColleges()
    {
        $this->_get_datatables_query_getAllColleges();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_getAllColleges()
    {
        $this->_get_datatables_query_getAllColleges();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_getAllColleges()
    {
        //$this->db->from($this->table);
        $this->db->select('colleges.*, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('colleges');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->order_by('colleges.id', 'desc');
        
        return $this->db->count_all_results();
    }

     /* server side datatable code for college start */
}
?>
