<?php
class Course_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getAllCourses()
    {
        $this->db->select('courses.*, departments.name AS department_name, degree_master.name AS degree_name, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('courses');
        $this->db->join('departments', 'departments.id = courses.department_id');
        $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->order_by('courses.id', 'desc');
        $result=$this->db->get()->result();
		return $result;
    }

    function getAllCoursesByUniversity($universityId)
    {
        $this->db->select('courses.*, departments.name AS department_name, degree_master.name AS degree_name, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('courses');
        $this->db->join('departments', 'departments.id = courses.department_id');
        $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->where('universities.id', $universityId);
        $result=$this->db->get()->result();
		return $result;
    }

    function getAllDegrees()
    {
        $this->db->from('degree_master');
        $result=$this->db->get()->result();
		return $result;
    }

    function getCourseById($courseId)
    {
        $this->db->from('courses');
        $this->db->where('id', $courseId);
        $result=$this->db->get()->result();
		return $result[0];
    }

    function getCourseByIdNew($courseId) {
      $this->db->select('courses.*, departments.id AS department_id, colleges.id AS college_id, campuses.id AS campus_id, universities.id AS university_id');
      $this->db->from('courses');
      $this->db->join('departments', 'departments.id = courses.department_id');
      $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
      $this->db->join('colleges', 'colleges.id = departments.college_id');
      $this->db->join('campuses', 'campuses.id = colleges.campus_id');
      $this->db->join('universities', 'universities.id = campuses.university_id');
      $this->db->where('courses.id', $courseId);
      $result=$this->db->get()->result();
		  return $result[0];
    }

    function getAllCoursesByDepartmentId($departmentId) {
      $this->db->select('courses.*, departments.name AS department_name, degree_master.name AS degree_name, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
      $this->db->from('courses');
      $this->db->join('departments', 'departments.id = courses.department_id');
      $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
      $this->db->join('colleges', 'colleges.id = departments.college_id');
      $this->db->join('campuses', 'campuses.id = colleges.campus_id');
      $this->db->join('universities', 'universities.id = campuses.university_id');
      $this->db->where('courses.department_id', $departmentId);
      $this->db->where('courses.status', '1');
      $result=$this->db->get()->result();
		  return $result;
    }

    function getAllDepartmentByDepartmentId($departmentId){

      $query1 = "SELECT * FROM departments where college_id IN (
                SELECT college_id FROM departments where id = $departmentId);";

      $query = $this->db->query($query1);
      $result_data = $query->result_array();

      return $result_data;
    }

    function addCourse($courseData)
    {
        try
   	    {
            $this->db->insert('courses', $courseData);
            $courseId = $this->db->insert_id();
            return $courseId;
        }
        catch(Exception $e)
        {
            return NULL;
        }
    }

    function editCourse($courseId, $courseData)
    {
        $this->db->where('id', $courseId);
   	    $this->db->update('courses', $courseData);
    }

    function getAllCategories()
    {
        $this->db->from('courses_categories');
        $result = $this->db->get()->result();
        return $result;
    }

    function getCourseByNameDegree($courseName, $degreeId, $departmentId)
    {
        $this->db->from('courses');
        $this->db->where('department_id', $departmentId);
        $this->db->where('name', $courseName);
        $this->db->where('degree_id', $degreeId);
        $query=$this->db->get();
		return $query->result_array();
    }

    function getCategoriesByMainstream($collegeCategoryId)
    {
        $this->db->from('courses_categories');
        $this->db->where('college_category_id', $collegeCategoryId);
        $result = $this->db->get()->result();
        return $result;
    }

    function getAllCoursesByDepartment($departmentId)
    {
        $this->db->select('courses.*, departments.name AS department_name, degree_master.name AS degree_name');
        $this->db->from('courses');
        $this->db->join('departments', 'departments.id = courses.department_id');
        $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
        $this->db->where('departments.id', $departmentId);
        $result=$this->db->get()->result();
		return $result;
    }

    function getAllCourseCategories()
 	 {
 		$this->db->from('courses_categories');
 		$this->db->select('*');
 		$result=$this->db->get()->result();
 		return $result;

 	 }

 	 function getAllCollegeCategories()
 	 {
 		$this->db->from('colleges_categories');
 		$this->db->select('*');
 		$result=$this->db->get()->result();
 		return $result;

 	 }

   function getCourseCategoriesByCollegeCategory($collegeCategoryId)
 	{
 	   $this->db->from('courses_categories');
 	   $this->db->where_in('college_category_id', $collegeCategoryId);
 	   $this->db->select('*');
 	   $result=$this->db->get()->result();
 	   return $result;

 	}

  function getDegree($condition1)
	 {

		$this->db->where($condition1);
		$this->db->from('degree_master');
		$this->db->join('status_master', 'status_master.id = degree_master.status','left');

		$this->db->select('degree_master.id,degree_master.name,degree_master.status,status_master.status as status_name');

		$result=$this->db->get()->result();

		return $result;

	 }

   /* server side datatable code for course start */

    var $column_order_course = array('courses.id','courses.name','colleges.name','degree_master.name','universities.name','courses.duration','courses.deadline_link','courses.total_fees',null,null); //set column field database for datatable orderable
  var $column_search_course = array('courses.id','courses.name','colleges.name','degree_master.name','universities.name','courses.duration','courses.deadline_link','courses.total_fees'); //set column field database for datatable searchable just firstname , lastname , address are searchable
  var $order_course = array('courses.id' => 'desc'); // default order

    private function _get_datatables_query_course()
  {
        $this->db->select('courses.*, departments.name AS department_name, degree_master.name AS degree_name, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('courses');
        $this->db->join('departments', 'departments.id = courses.department_id');
        $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->order_by('courses.id', 'desc');

    $i = 0;
  
    foreach ($this->column_search_course as $item) // loop column 
    {
      if($_POST['search']['value']) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        //if(count($this->column_search) - 1 == $i) //last loop
          //$this->db->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
      $this->db->order_by($this->column_order_course[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->order_course))
    {
      $order = $this->order_course;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables_course()
  {
    $this->_get_datatables_query_course();
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    //print_r($this->db->last_query()); exit;
    return $query->result();
  }

  function count_filtered_course()
  {
    $this->_get_datatables_query_course();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all_course()
  {
        $this->db->select('courses.*, departments.name AS department_name, degree_master.name AS degree_name, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('courses');
        $this->db->join('departments', 'departments.id = courses.department_id');
        $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->order_by('courses.id', 'desc');
    
    return $this->db->count_all_results();
  }

  private function _get_datatables_query($universityId)
  {
        $this->db->select('courses.*, departments.name AS department_name, degree_master.name AS degree_name, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('courses');
        $this->db->join('departments', 'departments.id = courses.department_id');
        $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->where('universities.id', $universityId);

    $i = 0;
  
    foreach ($this->column_search_course as $item) // loop column 
    {
      if($_POST['search']['value']) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        //if(count($this->column_search) - 1 == $i) //last loop
          //$this->db->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
      $this->db->order_by($this->column_order_course[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->order_course))
    {
      $order = $this->order_course;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables($universityId)
  {
    $this->_get_datatables_query($universityId);
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered($universityId)
  {
    $this->_get_datatables_query($universityId);
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all($universityId)
  {
        $this->db->select('courses.*, departments.name AS department_name, degree_master.name AS degree_name, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('courses');
        $this->db->join('departments', 'departments.id = courses.department_id');
        $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->where('universities.id', $universityId);
    
    return $this->db->count_all_results();
  }

  /* server side datatable code for counselor end */
}
?>
