<?php
class University_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }


     /* server side datatable code for university logo start */

    var $column_order_application = array('users_applied_universities.university_id','universities.name','um.name','um.email','um.phone',null); //set column field database for datatable orderable
    var $column_search_application = array('users_applied_universities.university_id','universities.name','um.name','um.email','um.phone'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order_application = array('users_applied_universities.university_id' => 'desc'); // default order

    private function _get_datatables_query_application()
    {
        $this->db->select('users_applied_universities.*, universities.name, universities.slug, universities.website, user_master.email, universities.banner, um.name AS student_name, um.email AS student_email, um.phone AS student_phone');
        $this->db->from('users_applied_universities');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('user_master', 'user_master.id = universities.user_id');
        $this->db->join('user_master um', 'um.id = users_applied_universities.user_id');
        $this->db->group_by('users_applied_universities.university_id, users_applied_universities.user_id');
        $this->db->order_by('users_applied_universities.id DESC');

        $i = 0;
    
        foreach ($this->column_search_application as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //if(count($this->column_search) - 1 == $i) //last loop
                   // $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_application[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order_application))
        {
            $order = $this->order_application;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_application()
    {
        $this->_get_datatables_query_application();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_application()
    {
        $this->_get_datatables_query_application();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_application()
    {
        $this->db->select('users_applied_universities.*, universities.name, universities.slug, universities.website, user_master.email, universities.banner, um.name AS student_name, um.email AS student_email, um.phone AS student_phone');
        $this->db->from('users_applied_universities');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('user_master', 'user_master.id = universities.user_id');
        $this->db->join('user_master um', 'um.id = users_applied_universities.user_id');
        $this->db->group_by('users_applied_universities.university_id, users_applied_universities.user_id');
        $this->db->order_by('users_applied_universities.id DESC');

        return $this->db->count_all_results();
    }

    /* server side datatable code for university logo end */

     /* server side datatable code for university logo start */

    var $column_order_slider = array('universities.id','universities.name',null,null,null,null); //set column field database for datatable orderable
    var $column_search_slider = array('universities.id','universities.name'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order_slider = array('universities.id' => 'desc'); // default order

    private function _get_datatables_query_slider()
    {
        $this->db->from('universities');
        $this->db->where('active', 1);
        $this->db->where('logo IS NOT NULL');
        $this->db->where('slider_approved', 0);

        $i = 0;
    
        foreach ($this->column_search_slider as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //if(count($this->column_search) - 1 == $i) //last loop
                   // $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_slider[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order_slider))
        {
            $order = $this->order_slider;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_slider()
    {
        $this->_get_datatables_query_slider();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_slider()
    {
        $this->_get_datatables_query_slider();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_slider()
    {
        $this->db->from('universities');
        $this->db->where('active', 1);
        $this->db->where('logo IS NOT NULL');
        $this->db->where('slider_approved', 0);
        //echo "<pre>"; print_r($this->db->last_query()); exit;
        return $this->db->count_all_results();
    }

    /* server side datatable code for university logo end */


    /* server side datatable code for university logo start */

    var $column_order_logo = array('universities.id','universities.name',null,null,null,null); //set column field database for datatable orderable
    var $column_search_logo = array('universities.id','universities.name'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order_logo = array('universities.id' => 'desc'); // default order

    private function _get_datatables_query_logo()
    {
        $this->db->from('universities');
        $this->db->where('active', 1);
        $this->db->where('logo IS NOT NULL');
        $this->db->where('logo_approved', 0);

        $i = 0;
    
        foreach ($this->column_search_logo as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //if(count($this->column_search) - 1 == $i) //last loop
                   // $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_logo[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order_logo))
        {
            $order = $this->order_logo;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_logo()
    {
        $this->_get_datatables_query_logo();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_logo()
    {
        $this->_get_datatables_query_logo();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_logo()
    {
        $$this->db->from('universities');
        $this->db->where('active', 1);
        $this->db->where('logo IS NOT NULL');
        $this->db->where('logo_approved', 0);
        //echo "<pre>"; print_r($this->db->last_query()); exit;
        return $this->db->count_all_results();
    }

    /* server side datatable code for university logo end */


    /* server side datatable code for researches start */

    var $column_order_researches = array('researches.id','researches.topic','researches.research_url','researches.funding','universities.name','colleges.name','departments.name',null); //set column field database for datatable orderable
    var $column_search_researches = array('researches.id','researches.topic','researches.research_url','researches.funding','universities.name','colleges.name','departments.name'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order_researches = array('researches.id' => 'desc'); // default order

    private function _get_datatables_query_getAllResearchesByUniversity($universityId)
    {
        $this->db->from('researches');
        $this->db->join('departments', 'departments.id = researches.department_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->select('researches.*, departments.name AS department_name, colleges.name AS college_name, universities.name AS university_name');
        $this->db->where('universities.id', $universityId);

        $i = 0;
    
        foreach ($this->column_search_researches as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //if(count($this->column_search) - 1 == $i) //last loop
                   // $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_researches[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order_researches))
        {
            $order = $this->order_researches;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_getAllResearchesByUniversity($universityId)
    {
        $this->_get_datatables_query_getAllResearchesByUniversity($university_id);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_getAllResearchesByUniversity($universityId)
    {
        $this->_get_datatables_query_getAllResearchesByUniversity($university_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_getAllResearchesByUniversity($universityId)
    {
        $this->db->from('researches');
        $this->db->join('departments', 'departments.id = researches.department_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->select('researches.*, departments.name AS department_name, colleges.name AS college_name, universities.name AS university_name');
        $this->db->where('universities.id', $universityId);
        
        return $this->db->count_all_results();
    }

    private function _get_datatables_query_getAllResearches()
    {
        $this->db->from('researches');
        $this->db->join('departments', 'departments.id = researches.department_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->select('researches.*, departments.name AS department_name, colleges.name AS college_name, universities.name AS university_name');

        $i = 0;
    
        foreach ($this->column_search_researches as $item) // loop column 
        {
            
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
            }
            $i++;
        }
        //echo "<pre>"; print_r($this->db->last_query()); exit;
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_researches[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order_researches))
        {
            $order = $this->order_researches;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_getAllResearches()
    {
        $this->_get_datatables_query_getAllResearches();

        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
//echo "<pre>"; print_r($this->db->last_query()); exit;
        return $query->result();
    }

    function count_filtered_getAllResearches()
    {
        $this->_get_datatables_query_getAllResearches();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_getAllResearches()
    {
        $this->db->from('researches');
        $this->db->join('departments', 'departments.id = researches.department_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->select('researches.*, departments.name AS department_name, colleges.name AS college_name, universities.name AS university_name');
        
        return $this->db->count_all_results();
    }

    /* server side datatable code for researches end */


    /* server side datatable code for representative start */

    var $column_order = array('user_master.name','user_master.email','universities.name','user_master.createdate','status_master.status',null); //set column field database for datatable orderable
    var $column_search = array('user_master.name','user_master.email','universities.name','user_master.createdate','status_master.status'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('user_master.id' => 'desc'); // default order

    private function _get_datatables_query($universityId)
    {
        $this->db->from('alumnus');
        $this->db->join('universities', 'universities.id = alumnus.university_id');
        $this->db->join('user_master', 'user_master.id = alumnus.user_id');
        $this->db->join('type_master', 'type_master.type_id = user_master.type');
        $this->db->join('status_master', 'status_master.id = user_master.status');
        $this->db->select('alumnus.*, user_master.name AS name, user_master.email AS email, user_master.createdate, status_master.status AS status_name, universities.name AS university_name');
        $this->db->where('universities.id', $universityId);
        $this->db->where('user_master.status != 5');

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //if(count($this->column_search) - 1 == $i) //last loop
                   // $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_AlumnusByUniversity($universityId)
    {
        $this->_get_datatables_query($university_id);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered($universityId)
    {
        $this->_get_datatables_query($university_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($universityId)
    {
        $this->db->from('alumnus');
        $this->db->join('universities', 'universities.id = alumnus.university_id');
        $this->db->join('user_master', 'user_master.id = alumnus.user_id');
        $this->db->join('type_master', 'type_master.type_id = user_master.type');
        $this->db->join('status_master', 'status_master.id = user_master.status');
        $this->db->select('alumnus.*, user_master.name AS name, user_master.email AS email, user_master.createdate, status_master.status AS status_name, universities.name AS university_name');
        $this->db->where('universities.id', $universityId);
        $this->db->where('user_master.status != 5');
        
        return $this->db->count_all_results();
    }

    private function _get_datatables_query_AllAlumnus()
    {
        $this->db->from('alumnus');
        $this->db->join('universities', 'universities.id = alumnus.university_id');
        $this->db->join('user_master', 'user_master.id = alumnus.user_id');
        $this->db->join('type_master', 'type_master.type_id = user_master.type');
        $this->db->join('status_master', 'status_master.id = user_master.status');
        $this->db->select('alumnus.*, user_master.name AS name, user_master.email AS email, user_master.createdate, status_master.status AS status_name, universities.name AS university_name');
        $this->db->where('user_master.status != 5');

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
            }
            $i++;
        }
        //echo "<pre>"; print_r($this->db->last_query()); exit;
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_AllAlumnus()
    {
        $this->_get_datatables_query_AllAlumnus();

        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
//echo "<pre>"; print_r($this->db->last_query()); exit;
        return $query->result();
    }

    function count_filtered_AllAlumnus()
    {
        $this->_get_datatables_query_AllAlumnus();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_AllAlumnus()
    {
        $this->db->from('alumnus');
        $this->db->join('universities', 'universities.id = alumnus.university_id');
        $this->db->join('user_master', 'user_master.id = alumnus.user_id');
        $this->db->join('type_master', 'type_master.type_id = user_master.type');
        $this->db->join('status_master', 'status_master.id = user_master.status');
        $this->db->select('alumnus.*, user_master.name AS name, user_master.email AS email, user_master.createdate, status_master.status AS status_name, universities.name AS university_name');
        $this->db->where('user_master.status != 5');
        
        return $this->db->count_all_results();
    }

    /* server side datatable code for representative end */

    function getAllUniversities()
    {
        $this->db->from('universities');
        $result=$this->db->get()->result();
		return $result;
    }

    function getAllUniversitiesActive()
    {
        $this->db->select('id,name');
        $this->db->from('universities');
        $this->db->where('active','1');
        $result=$this->db->get()->result_array();
		    return $result;
    }

    function getAllCountries()
    {
        $this->db->from('country_master');
        $result=$this->db->get()->result();
		return $result;
    }

    function getAllAffiliation()
    {
        $this->db->from('affiliation_type_master');
        $result=$this->db->get()->result();
        return $result;
    }

    function getAlumnuById($alumnuId)
    {
        $this->db->from('alumnus');
        $this->db->join('user_master', 'user_master.id = alumnus.user_id');
        $this->db->select('alumnus.*, user_master.name AS name, user_master.email AS email, user_master.status, user_master.username AS username');
        $this->db->where('alumnus.id', $alumnuId);
        $result=$this->db->get()->row();
		return $result;
    }

    function getAlumnuByUserId($userId)
    {
        $this->db->from('alumnus');
        $this->db->join('user_master', 'user_master.id = alumnus.user_id');
        $this->db->select('alumnus.*, user_master.name AS name, user_master.email AS email, user_master.status');
        $this->db->where('alumnus.user_id', $userId);
        $result=$this->db->get()->row();
		return $result;
    }

    function getAllAlumnus()
    {
        $this->db->from('alumnus');
        $this->db->join('universities', 'universities.id = alumnus.university_id');
        $this->db->join('user_master', 'user_master.id = alumnus.user_id');
        $this->db->join('type_master', 'type_master.type_id = user_master.type');
		$this->db->join('status_master', 'status_master.id = user_master.status');
        $this->db->select('alumnus.*, user_master.name AS name, user_master.email AS email, user_master.createdate, status_master.status AS status_name, universities.name AS university_name');
        $this->db->where('user_master.status != 5');
        $result=$this->db->get()->result();
		return $result;
    }

    function getAlumnusByUniversity($universityId)
    {
        $this->db->from('alumnus');
        $this->db->join('universities', 'universities.id = alumnus.university_id');
        $this->db->join('user_master', 'user_master.id = alumnus.user_id');
        $this->db->join('type_master', 'type_master.type_id = user_master.type');
		$this->db->join('status_master', 'status_master.id = user_master.status');
        $this->db->select('alumnus.*, user_master.name AS name, user_master.email AS email, user_master.createdate, status_master.status AS status_name, universities.name AS university_name');
        $this->db->where('universities.id', $universityId);
        $this->db->where('user_master.status != 5');
        $result=$this->db->get()->result();
		return $result;
    }

    function addAlumnu($alumnuData)
    {
        $user_master_data = array(
            'name' 		=> $alumnuData['name'],
            'email' 	=> $alumnuData['email'],
            'username'  => $alumnuData['username'],
   	        'password' 	=> $alumnuData['password'],
   	        'type' 		=> $alumnuData['type'] ,
   	        'createdate' => date('Y-m-d H:i:s'),
            'status' 	=> $alumnuData['status']
        );

        $this->db->insert('user_master', $user_master_data);
   	    $user_id = $this->db->insert_id();

        $data = array(
            'user_id' => $user_id,
            'university_id' => $alumnuData['university_id'],
            'created_at' => date('Y-m-d H:i:s')
        );

        $this->db->insert('alumnus', $data);
    }

    function editAlumnu($alumnuData)
    {
        $user_master_data = array(
            'name' 		=> $alumnuData['name'],
            'email' 	=> $alumnuData['email'],
   	        'status' 	=> $alumnuData['status'],
   	        'flag'		=> '1'
        );

        if(isset($alumnuData['password']))
        {
            $user_master_data['password'] = $alumnuData['password'];
        }

        $this->db->start_cache();
        $this->db->where('id', $alumnuData['user_id']);
        $this->db->update('user_master', $user_master_data);
        $this->db->stop_cache();
   	    $this->db->flush_cache();

        $data = array(
            'university_id' => $alumnuData['university_id']
        );

        $this->db->start_cache();
        $this->db->where('id', $alumnuData['alumnu_id']);
        $this->db->update('alumnus', $data);
        $this->db->stop_cache();
   	    $this->db->flush_cache();
    }

    function updateAlumnu($alumnuId, $alumnuData)
    {
        $this->db->where('id', $alumnuId);
        $this->db->update('alumnus', $alumnuData);
    }

    function getAllResearches()
    {
        $this->db->from('researches');
        $this->db->join('departments', 'departments.id = researches.department_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
		$this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->select('researches.*, departments.name AS department_name, colleges.name AS college_name, universities.name AS university_name');
        $result=$this->db->get()->result();
		return $result;
    }

    function getAllResearchesByUniversity($universityId)
    {
        $this->db->from('researches');
        $this->db->join('departments', 'departments.id = researches.department_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
		$this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->select('researches.*, departments.name AS department_name, colleges.name AS college_name, universities.name AS university_name');
        $this->db->where('universities.id', $universityId);
        $result=$this->db->get()->result();
		return $result;
    }

    function getResearchById($researchId)
    {
        $this->db->from('researches');
        $this->db->join('departments', 'departments.id = researches.department_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
		$this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->select('researches.*, departments.name AS department_name, colleges.name AS college_name, universities.name AS university_name, universities.id AS university_id, colleges.id AS college_id');
        $this->db->where('researches.id', $researchId);
        $result=$this->db->get()->row();
		return $result;
    }

    function addResearch($researchData)
    {
        $this->db->insert('researches', $researchData);
        $research_id = $this->db->insert_id();
        return $research_id;
    }

    function editResearch($researchId, $researchData)
    {
        $this->db->where('id', $researchId);
        $this->db->update('researches', $researchData);
    }

    function getUniversityByName($universityName, $countryId)
    {
        $this->db->from('universities');
        $this->db->where('country_id', $countryId);
        $this->db->where('name', $universityName);
        $query=$this->db->get();
		return $query->result_array();
    }

    function getresearchByURL($researchURL)
    {
        $this->db->from('researches');
        $this->db->where('research_url', $researchURL);
        $query=$this->db->get();
		return $query->result_array();
    }

    function getUniversityById($universityId)
    {
        $this->db->from('universities');
        $this->db->join('campuses', 'campuses.university_id = universities.id');
        $this->db->where('universities.id', $universityId);
        $query=$this->db->get();
		return $query->result_array();
    }

    function getUnapprovedLogos()
    {
        $this->db->from('universities');
        $this->db->where('active', 1);
        $this->db->where('logo IS NOT NULL');
        $this->db->where('logo_approved', 0);
        $result=$this->db->get()->result();
		return $result;
    }

    function getUnapprovedSliders()
    {
        $this->db->from('universities');
        $this->db->where('active', 1);
        $this->db->where('logo IS NOT NULL');
        $this->db->where('slider_approved', 0);
        $result=$this->db->get()->result();
		return $result;
    }

    function updateUniversity($universityId, $universityData)
    {
        $this->db->where('id', $universityId);
        $this->db->update('universities', $universityData);
    }

    function getUniversitiesByCounselor($counselorId)
    {
        $this->db->from('university_to_counselor');
        $this->db->where('university_to_counselor.counselor_id', $counselorId);
        $result=$this->db->get()->result_array();
		return $result;
    }

    function getActiveUniversities($sortBy)
 	{
 		$this->db->select('universities.*, country_master.name AS country_name, user_master.email AS email, user_master.image AS image, user_master.id AS login_id');
         $this->db->from('universities', 'universities.id = campuses.university_id');
 		$this->db->join('country_master', 'universities.country_id = country_master.id');
		$this->db->join('user_master', 'universities.user_id = user_master.id');
		$this->db->where('universities.active', 1);
        $this->db->where('universities.logo_approved', 1);
		$this->db->order_by($sortBy);
         $result=$this->db->get()->result();
 		return $result;
 	}

    function getUniversitiesByCountry($countryId)
    {
        $this->db->from('universities');
        $this->db->where('country_id', $countryId);
        $result=$this->db->get()->result();
		return $result;
    }

    function addInternship($internshipData)
    {
        $this->db->insert('universities_internships', $internshipData);
        $internship_id = $this->db->insert_id();
        return $internship_id;
    }

    function getAllInternships()
    {
        $this->db->select('universities_internships.*, universities.name AS university_name, colleges.name AS college_name, departments.name AS department_name');
        $this->db->from('universities_internships');
        $this->db->join('universities', 'universities.id = universities_internships.university_id');
        $this->db->join('colleges', 'colleges.id = universities_internships.college_id');
        $this->db->join('departments', 'departments.id = universities_internships.department_id');
        $result=$this->db->get()->result();
		return $result;
    }

    function getAllInternshipsByUniversity($universityId)
    {
        $this->db->select('universities_internships.*, universities.name AS university_name, colleges.name AS college_name, departments.name AS department_name');
        $this->db->from('universities_internships');
        $this->db->join('universities', 'universities.id = universities_internships.university_id');
        $this->db->join('colleges', 'colleges.id = universities_internships.college_id');
        $this->db->join('departments', 'departments.id = universities_internships.department_id');
        $this->db->where('universities.id', $universityId);
        $result=$this->db->get()->result();
		return $result;
    }

    function getInternshipById($internshipId)
    {
        $this->db->select('universities_internships.*, universities.name AS university_name, colleges.name AS college_name, departments.name AS department_name');
        $this->db->from('universities_internships');
        $this->db->join('universities', 'universities.id = universities_internships.university_id');
        $this->db->join('colleges', 'colleges.id = universities_internships.college_id');
        $this->db->join('departments', 'departments.id = universities_internships.department_id');
        $this->db->where('universities_internships.id', $internshipId);
        $result=$this->db->get()->row();
		return $result;
    }

    function updateInternship($internshipId, $internshipData)
    {
        $this->db->where('id', $internshipId);
        $this->db->update('universities_internships', $internshipData);
    }

    function getInternshipRequestById($internshipId)
    {
        $this->db->select('universities_internships.*, universities.name AS university_name, colleges.name AS college_name, departments.name AS department_name, student_details.other_details AS student_other_details, user_master.name AS student_name, user_master.email AS student_email, user_master.phone AS student_phone, internships_students.other_details AS internship_other_details');
        $this->db->from('internships_students');
        $this->db->join('universities_internships', 'internships_students.internship_id = universities_internships.id');
        $this->db->join('universities', 'universities.id = universities_internships.university_id');
        $this->db->join('colleges', 'colleges.id = universities_internships.college_id');
        $this->db->join('departments', 'departments.id = universities_internships.department_id');
        $this->db->join('student_details', 'student_details.id = internships_students.student_id');
        $this->db->join('user_master', 'user_master.id = student_details.user_id');
        $this->db->where('universities_internships.id', $internshipId);
        $result=$this->db->get()->row();
		return $result;
    }

    function getAllInternshipRequests()
    {
        $this->db->select('universities_internships.*, universities.name AS university_name, colleges.name AS college_name, departments.name AS department_name, student_details.other_details AS student_other_details, user_master.name AS student_name, user_master.email AS student_email, user_master.phone AS student_phone, internships_students.other_details AS internship_other_details');
        $this->db->from('internships_students');
        $this->db->join('universities_internships', 'internships_students.internship_id = universities_internships.id');
        $this->db->join('universities', 'universities.id = universities_internships.university_id');
        $this->db->join('colleges', 'colleges.id = universities_internships.college_id');
        $this->db->join('departments', 'departments.id = universities_internships.department_id');
        $this->db->join('student_details', 'student_details.id = internships_students.student_id');
        $this->db->join('user_master', 'user_master.id = student_details.user_id');
        $result=$this->db->get()->result();
		return $result;
    }

    function getApplicationsList()
    {
        $this->db->select('users_applied_universities.*, universities.name, universities.slug, universities.website, user_master.email, universities.banner, um.name AS student_name, um.email AS student_email, um.phone AS student_phone');
		$this->db->from('users_applied_universities');
		$this->db->join('universities', 'universities.id = users_applied_universities.university_id');
		$this->db->join('user_master', 'user_master.id = universities.user_id');
		$this->db->join('user_master um', 'um.id = users_applied_universities.user_id');
		$this->db->group_by('users_applied_universities.university_id, users_applied_universities.user_id');
		$this->db->order_by('users_applied_universities.id DESC');
		$result = $this->db->get();
		return $result->result_array();
    }

    function getApplicationsByUniversity($universityId)
    {
        $this->db->select('users_applied_universities.*, universities.name, universities.slug, universities.website, user_master.email, universities.banner, colleges.name AS college_name, um.name AS student_name, um.email AS student_email, um.phone AS student_phone');
		$this->db->from('users_applied_universities');
		$this->db->join('universities', 'universities.id = users_applied_universities.university_id');
		$this->db->join('user_master', 'user_master.id = universities.user_id');
        $this->db->join('user_master um', 'um.id = users_applied_universities.user_id');
		$this->db->join('colleges', 'colleges.id = users_applied_universities.college_id', 'left');
		$this->db->where('users_applied_universities.university_id', $universityId);
		$result = $this->db->get();
		return $result->result_array();
    }

    function getApplicationById($applicationId)
	{
		$this->db->select('users_applied_universities.*, universities.name, universities.slug, universities.website, user_master.email, universities.banner, colleges.name AS college_name, um.name AS student_name, um.email AS student_email, um.phone AS student_phone');
		$this->db->from('users_applied_universities');
		$this->db->join('universities', 'universities.id = users_applied_universities.university_id');
		$this->db->join('user_master', 'user_master.id = universities.user_id');
        $this->db->join('user_master um', 'um.id = users_applied_universities.user_id');
		$this->db->join('colleges', 'colleges.id = users_applied_universities.college_id', 'left');
		$this->db->where('users_applied_universities.id', $applicationId);
		$result = $this->db->get();
		return $result->result_array();
	}

    function checkVisaByApplication($applicationId)
	{
		$this->db->from('users_visa_applicatios');
		$this->db->where('application_id', $applicationId);
		return $this->db->get()->result_array();
	}

    function getVisaApplicationsByUniversity($universityId)
    {
        $this->db->select('users_visa_applicatios.*, universities.id AS university_id, universities.name, universities.slug, universities.website, user_master.email, universities.banner, colleges.name AS college_name, um.name AS student_name, um.email AS student_email, um.phone AS student_phone');
        $this->db->from('users_visa_applicatios');
		$this->db->join('users_applied_universities', 'users_applied_universities.id = users_visa_applicatios.application_id');
		$this->db->join('universities', 'universities.id = users_applied_universities.university_id');
		$this->db->join('user_master', 'user_master.id = universities.user_id');
        $this->db->join('user_master um', 'um.id = users_applied_universities.user_id');
		$this->db->join('colleges', 'colleges.id = users_applied_universities.college_id', 'left');
		$this->db->where('users_applied_universities.university_id', $universityId);
		$result = $this->db->get();
		return $result->result_array();
    }

    function getVisaById($visaId)
	{
		$this->db->select('users_visa_applicatios.*, universities.id AS university_id, universities.name, universities.slug, universities.website, user_master.email, universities.banner, colleges.name AS college_name, um.name AS student_name, um.email AS student_email, um.phone AS student_phone');
        $this->db->from('users_visa_applicatios');
		$this->db->join('users_applied_universities', 'users_applied_universities.id = users_visa_applicatios.application_id');
		$this->db->join('universities', 'universities.id = users_applied_universities.university_id');
		$this->db->join('user_master', 'user_master.id = universities.user_id');
        $this->db->join('user_master um', 'um.id = users_applied_universities.user_id');
		$this->db->join('colleges', 'colleges.id = users_applied_universities.college_id', 'left');
		$this->db->where('users_visa_applicatios.id', $visaId);
		$result = $this->db->get();
		return $result->result_array();
	}

    function getBrochuresByUniversity($universityId)
    {
        $this->db->from('universities_brochures');
        $this->db->where('university_id', $universityId);
        $result = $this->db->get();
        return $result->result();
    }

    function getBrochuresById($id)
    {
        $this->db->from('universities_brochures');
        $this->db->where('id', $id);
        $result = $this->db->get();
        return $result->row();
    }

    function addBrochure($data)
    {
        $this->db->insert('universities_brochures', $data);
        return true;
    }
}
?>
