<?php
class Campus_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getAllCampuses()
    {
        $this->db->select('campuses.*, universities.name AS university_name');
        $this->db->from('campuses');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->order_by('campuses.id', 'desc');
        $result=$this->db->get()->result();
		return $result;
    }

    function getCampusesByUniversity($universityId)
    {
        $this->db->select('campuses.*, universities.name AS university_name');
        $this->db->from('campuses');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->where('campuses.university_id', $universityId);
        $result=$this->db->get()->result();
		return $result;
    }

    function getCampusById($campusId)
    {
        $this->db->from('campuses');
        $this->db->where('id', $campusId);
        $result=$this->db->get()->result();
		return $result[0];
    }

    function getCampusByIdNew($campusId)
    {
      $this->db->select('campuses.*');
      $this->db->from('campuses');
      $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->where('campuses.id', $campusId);
        $result=$this->db->get()->result();
		return $result[0];
    }

    function addCampus($campusData)
    {
   	    $this->db->insert('campuses', $campusData);
        //echo $this->db->_error_message();
        //exit;
        $campusId = $this->db->insert_id();
        return $campusId;
    }

    function editCampus($campusId, $campusData)
    {
        $this->db->where('id', $campusId);
   	    $this->db->update('campuses', $campusData);
    }

    function getCampusByName($campusName, $universityId)
    {
        $this->db->from('campuses');
        $this->db->where('university_id', $universityId);
        $this->db->where('name', $campusName);
        $query=$this->db->get();
		return $query->result_array();
    }



     /* server side datatable code for campus start */

    var $column_order = array('campuses.id','campuses.name','universities.name',null); //set column field database for datatable orderable
    var $column_search = array('campuses.id','campuses.name','universities.name'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('campuses.id' => 'desc'); // default order

    private function _get_datatables_query_getCampusesByUniversity($universityId)
    {
        $this->db->select('campuses.*, universities.name AS university_name');
        $this->db->from('campuses');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->where('campuses.university_id', $universityId);
        //$this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_getCampusesByUniversity($universityId)
    {
        $this->_get_datatables_query_getCampusesByUniversity($universityId);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_getCampusesByUniversity($universityId)
    {
        $this->_get_datatables_query_getCampusesByUniversity($universityId);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_getCampusesByUniversity($universityId)
    {
        //$this->db->from($this->table);
        $this->db->select('campuses.*, universities.name AS university_name');
        $this->db->from('campuses');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->where('campuses.university_id', $universityId);
        
        return $this->db->count_all_results();
    }

    private function _get_datatables_query_getAllCampuses()
    {
        $this->db->select('campuses.*, universities.name AS university_name');
        $this->db->from('campuses');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->order_by('campuses.id', 'desc');
        //$this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_getAllCampuses()
    {
        $this->_get_datatables_query_getAllCampuses();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_getAllCampuses()
    {
        $this->_get_datatables_query_getAllCampuses();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_getAllCampuses()
    {
        //$this->db->from($this->table);
        $this->db->select('campuses.*, universities.name AS university_name');
        $this->db->from('campuses');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->order_by('campuses.id', 'desc');
        
        return $this->db->count_all_results();
    }

     /* server side datatable code for campus start */
}
?>
