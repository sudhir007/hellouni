<?php
class Country_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function getCountriesByNames($countryNames)
    {
        $this->db->from('country_master');
        $this->db->where_in('name', $countryNames);
        $query = $this->db->get();
        if($query->num_rows() >= 1)
        {
            return $query->result_array();
        }
        return array();
    }
}
?>
