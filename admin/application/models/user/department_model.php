<?php
class Department_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getAllDepartments()
    {
        $this->db->select('departments.*, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('departments');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->order_by('departments.id', 'desc');
        $result=$this->db->get()->result();
		return $result;
    }

    function getAllDepartmentsByUniversity($universityId)
    {
        $this->db->select('departments.*, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('departments');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->where('universities.id', $universityId);
        $result=$this->db->get()->result();
		return $result;
    }

    function getAllDepartmentsByCollege($collegeId)
    {
        $this->db->from('departments');
        $this->db->where('college_id', $collegeId);
        $result=$this->db->get()->result();
		return $result;
    }

    function getDepartmentById($departmentId)
    {
        $this->db->from('departments');
        $this->db->where('id', $departmentId);
        $result=$this->db->get()->result();
		return $result[0];
    }

    function getDepartmentByIdNew($departmentId) {
      $this->db->select('departments.*, colleges.id AS college_id, campuses.id AS campus_id, universities.id AS university_id');
      $this->db->from('departments');
      $this->db->join('colleges', 'colleges.id = departments.college_id');
      $this->db->join('campuses', 'campuses.id = colleges.campus_id');
      $this->db->join('universities', 'universities.id = campuses.university_id');
      $this->db->where('departments.id', $departmentId);

      $result=$this->db->get()->result();
		  return $result[0];
    }

    function addDepartment($departmentData)
    {
   	    $this->db->insert('departments', $departmentData);
        //echo $this->db->_error_message();
        $departmentId = $this->db->insert_id();
        return $departmentId;
    }

    function editDepartment($departmentId, $departmentData)
    {
        $this->db->where('id', $departmentId);
   	    $this->db->update('departments', $departmentData);
        //echo $this->db->_error_message();
    }

    function getDepartmentByName($departmentName, $collegeId)
    {
        $this->db->from('departments');
        $this->db->where('college_id', $collegeId);
        $this->db->where('name', $departmentName);
        $query=$this->db->get();
		return $query->result_array();
    }


     /* server side datatable code for department start */

    var $column_order = array('departments.id','departments.name','colleges.name','departments.contact_title','departments.contact_name','departments.contact_email','campuses.name','universities.name',null); //set column field database for datatable orderable
    var $column_search = array('departments.id','departments.name','colleges.name','departments.contact_title','departments.contact_name','departments.contact_email','universities.name'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('departments.id' => 'desc'); // default order

    private function _get_datatables_query_getAllDepartmentsByUniversity($universityId)
    {
        $this->db->select('departments.*, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('departments');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->where('universities.id', $universityId);
        //$this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_getAllDepartmentsByUniversity($universityId)
    {
        $this->_get_datatables_query_getAllDepartmentsByUniversity($universityId);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_getAllDepartmentsByUniversity($universityId)
    {
        $this->_get_datatables_query_getAllDepartmentsByUniversity($universityId);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_getAllDepartmentsByUniversity($universityId)
    {
        //$this->db->from($this->table);
        $this->db->select('departments.*, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('departments');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->where('universities.id', $universityId);
        
        return $this->db->count_all_results();
    }

    private function _get_datatables_query_getAllDepartments()
    {
        $this->db->select('departments.*, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('departments');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->order_by('departments.id', 'desc');
        //$this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_getAllDepartments()
    {
        $this->_get_datatables_query_getAllDepartments();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
       // echo "<pre>";print_r($this->db->last_query()); exit;
        return $query->result();
    }

    function count_filtered_getAllDepartments()
    {
        $this->_get_datatables_query_getAllDepartments();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_getAllDepartments()
    {
        //$this->db->from($this->table);
        $this->db->select('departments.*, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('departments');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->order_by('departments.id', 'desc');
        
        return $this->db->count_all_results();
    }

     /* server side datatable code for department start */
}
?>
