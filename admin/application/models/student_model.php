<?php
class Student_model extends CI_Model
{
    function getAll($universityId = NULL)
    {
        $this->db->select('student_details.id AS student_id, student_details.user_id AS student_login_id, student_details.facilitator_id AS facilitator_id, user_master.name AS student_name, user_master.email AS student_email, user_master.phone AS student_phone, student_details.desired_destination AS desired_destination, um.name AS facilitator_name, oum.name AS originator_name, user_master.createdate');
        $this->db->from('student_details');
        $this->db->join('user_master', 'user_master.id = student_details.user_id');
        $this->db->join('user_master AS um', 'um.id = student_details.facilitator_id');
        $this->db->join('user_master AS oum', 'oum.id = student_details.originator_id');
        $this->db->where('user_master.type = 5');
        $this->db->where('user_master.status = 1');
        if($universityId)
        {
            $this->db->join('users_applied_universities', 'users_applied_universities.user_id = user_master.id');
            $this->db->where('users_applied_universities.university_id = ' . $universityId);
        }
        $this->db->order_by('user_master.createdate', 'desc');
        return $this->db->get()->result();
    }

    function getAllForCounselors($counselorId)
    {
        $this->db->select('student_details.id AS student_id, student_details.user_id AS student_login_id, student_details.facilitator_id AS facilitator_id, user_master.name AS student_name, user_master.email AS student_email, user_master.phone AS student_phone, student_details.desired_destination AS desired_destination, um.name AS facilitator_name, oum.name AS originator_name');
        $this->db->from('student_details');
        $this->db->join('user_master', 'user_master.id = student_details.user_id');
        $this->db->join('user_master AS um', 'um.id = student_details.facilitator_id');
        $this->db->join('user_master AS oum', 'oum.id = student_details.originator_id');
        $this->db->where('um.id', $counselorId);
        return $this->db->get()->result();
    }

    function getAllCounselors($universityId = NULL)
    {
        $this->db->from('user_master');
        if($universityId)
        {
            $this->db->join('university_to_counselor', 'university_to_counselor.counselor_id = user_master.id');
            $this->db->where('university_to_counselor.university_id', $universityId);
        }
        $this->db->where('user_master.type', 7);
        return $this->db->get()->result();
    }

    function updateStudentDetail($studentId, $updatedData)
    {
        $this->db->where('id', $studentId);
        $this->db->update('student_details', $updatedData);
    }

    function updateUser($userId, $updatedData)
    {
        $this->db->where('id', $userId);
        $this->db->update('user_master', $updatedData);
        echo $this->db->last_query();
    }

    function getStudentDetails($conditions=array())
    {
        $this->db->where($conditions);
        $this->db->from('user_master');
        $this->db->join('type_master', 'type_master.type_id = user_master.type','left');
        $this->db->join('status_master', 'status_master.id = user_master.status','left');
        $this->db->join('student_details', 'student_details.user_id = user_master.id','left');
        $this->db->select('user_master.id,user_master.name,user_master.email,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,user_master.fid,user_master.registration_type,status_master.status as status_name,student_details.address,student_details.country,student_details.city,
        student_details.zip,student_details.dob,student_details.phone,student_details.parent_name,student_details.parent_mob,student_details.parent_occupation,student_details.id AS student_id,
        student_details.hear_about_us,student_details.passport,student_details.desired_destination,student_details.passport_number, student_details.gender, student_details.passport_issue_date, student_details.passport_expiry_date, student_details.marital_status, student_details.place_of_birth, student_details.correspondence_address, student_details.state,
    		student_details.company_name, student_details.designation, student_details.work_start_date, student_details.work_end_date, student_details.e_name, student_details.e_mobile_number, student_details.e_email, student_details.e_relation');
        $result=$this->db->get()->row();
        return $result;
    }

    function getParticularCity($conditions)
    {
        $this->db->where($conditions);
        $this->db->from('city_master');
        $this->db->join('state_master', 'state_master.zone_id = city_master.state_id','left');
        $this->db->join('location_country_master', 'location_country_master.id = state_master.country_id','left');
        $this->db->join('status_master', 'status_master.id = city_master.status','left');
        $this->db->select('city_master.id,city_master.city_name,city_master.state_id,city_master.status,state_master.zone_id as stateid,state_master.name as statename,location_country_master.id as country_id,location_country_master.name as countryname');
        $result = $this->db->get()->row();
        return $result;
    }

    function getLocationStates($conditions)
    {
        $this->db->where($conditions);
        $this->db->from('state_master');
        $this->db->join('status_master', 'status_master.id = state_master.status','left');
        $this->db->select('state_master.zone_id,state_master.country_id,state_master.name,state_master.code,state_master.status,status_master.status as status_name');
        $result = $this->db->get()->result();
        return $result;
    }

    function getCities($conditions)
    {
        $this->db->where($conditions);
        $this->db->from('city_master');
        $this->db->join('status_master', 'status_master.id = city_master.status','left');
        $this->db->select('city_master.id,city_master.city_name,city_master.state_id,city_master.status');
        $result = $this->db->get()->result();
        return $result;
    }

    function checkDesiredCourse($conditions=array())
    {
        $this->db->where($conditions);
        $this->db->from('desired_courses');
        $this->db->join('student_details', 'student_details.id = desired_courses.student_id','left');
        $this->db->join('user_master', 'user_master.id = student_details.user_id','left');
        $this->db->select('*');
        $result=$this->db->get()->row();
        return $result;
    }

    function checkSecondaryQualification($conditions=array())
    {
        $this->db->where($conditions);
        $this->db->from('secondary_qualification');
        $this->db->join('student_details', 'student_details.id = secondary_qualification.student_id','left');
        $this->db->join('user_master', 'user_master.id = student_details.user_id','left');
        $this->db->select('*');
        $result=$this->db->get()->row();
        return $result;
    }

    function checkDiplomaQualification($conditions=array())
    {
        $this->db->where($conditions);
        $this->db->from('diploma_qualification');
        $this->db->join('student_details', 'student_details.id = diploma_qualification.student_id','left');
        $this->db->join('user_master', 'user_master.id = student_details.user_id','left');
        $this->db->select('*');
        $result=$this->db->get()->row();
        return $result;
    }

    function checkHigherSecondaryQualification($conditions=array())
    {
        $this->db->where($conditions);
        $this->db->from('higher_secondary_qualification');
        $this->db->join('student_details', 'student_details.id = higher_secondary_qualification.student_id','left');
        $this->db->join('user_master', 'user_master.id = student_details.user_id','left');
        $this->db->select('*');
        $result=$this->db->get()->row();
        return $result;
    }

    function checkBachelorQualification($conditions=array())
    {
        $this->db->where($conditions);
        $this->db->from('bachelor_qualification');
        $this->db->join('student_details', 'student_details.id = bachelor_qualification.student_id','left');
        $this->db->join('user_master', 'user_master.id = student_details.user_id','left');
        $this->db->select('*');
        $result=$this->db->get()->row();
        return $result;
    }

    function checkMasterQualification($conditions=array())
    {
        $this->db->where($conditions);
        $this->db->from('master_qualification');
        $this->db->join('student_details', 'student_details.id = master_qualification.student_id','left');
        $this->db->join('user_master', 'user_master.id = student_details.user_id','left');
        $this->db->select('*');
        $result=$this->db->get()->row();
        //return $this->db->last_query();
        return $result;
    }

    function checkQualifiedExam($conditions=array())
    {
        $this->db->where($conditions);
        $this->db->from('qualified_exams');
        $this->db->join('student_details', 'student_details.id = qualified_exams.student_id','left');
        $this->db->select('*');
        $result=$this->db->get()->row();
        return $result;
	}

    function getCountries($conditions)
    {
        $this->db->where($conditions);
        $this->db->from('country_master');
        $this->db->join('status_master', 'status_master.id = country_master.status','left');
        $this->db->order_by('country_master.order', 'ASC');
        $this->db->select('country_master.id,country_master.name,country_master.code,country_master.status,status_master.status as status_name');
        $result = $this->db->get()->result();
        return $result;
    }

    function getLocationCountries($conditions)
    {
        $this->db->where($conditions);
        $this->db->from('location_country_master');
        $this->db->join('status_master', 'status_master.id = location_country_master.status','left');
        $this->db->select('location_country_master.id,location_country_master.name,location_country_master.code,location_country_master.status,status_master.status as status_name');
        $result = $this->db->get()->result();
        return $result;
    }

    function getIntakes($condition)
    {
        $this->db->where($condition);
        $this->db->from('intake_master');
        $this->db->join('status_master', 'status_master.id = intake_master.status','left');
        $this->db->select('intake_master.id,intake_master.name,intake_master.status,status_master.status as status_name');
        $result=$this->db->get()->result();
        return $result;
    }

    function getAppliedUniversities($studentId)
    {
        $this->db->select('users_applied_universities.*, universities.name, universities.slug, universities.website, user_master.email, universities.banner, colleges.name AS college_name, courses.name AS course_name, user_master.name AS student_name, user_master.phone AS student_phone');
        $this->db->from('users_applied_universities');
        $this->db->join('user_master', 'user_master.id = users_applied_universities.user_id');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('colleges', 'colleges.id = users_applied_universities.college_id', 'left');
        $this->db->join('courses', 'courses.id = users_applied_universities.course_id', 'left');
        $this->db->where('user_master.id', $studentId);
        $result=$this->db->get()->result();
        return $result;
    }

    function updateStudentDetails($insertData=array(), $id)
    {
        $this->db->where('id', $id);
        $this->db->update('student_details',$insertData);
        return true;
    }

    function updateSecondaryQualification($insertData=array(), $id)
    {
        $this->db->where('student_id', $id);
        $this->db->update('secondary_qualification',$insertData);
        return true;
    }

    function insertSecondaryQualification($data=array())
    {
        $this->db->insert('secondary_qualification', $data);
        return $secondary_qualification_id = $this->db->insert_id();
    }

    function updateDiplomaQualification($insertData=array(), $id)
    {
        $this->db->where('student_id', $id);
        $this->db->update('diploma_qualification',$insertData);
        return true;
    }

    function insertDiplomaQualification($data=array())
    {
        $this->db->insert('diploma_qualification', $data);
        return $diploma_qualification_id = $this->db->insert_id();
    }

    function updateHigherSecondaryQualification($insertData=array(), $id)
    {
        $this->db->where('student_id', $id);
        $this->db->update('higher_secondary_qualification',$insertData);
        return true;
    }

    function insertHigherSecondaryQualification($data=array())
    {
        $this->db->insert('higher_secondary_qualification', $data);
        return $higher_secondary_qualification_id = $this->db->insert_id();
    }

    function updateBachelorQualification($insertData=array(), $id)
    {
        $this->db->where('student_id',$id);
        $this->db->update('bachelor_qualification',$insertData);
        return true;
    }

    function insertBachelorQualification($data=array())
    {
        $this->db->insert('bachelor_qualification', $data);
        return $secondary_qualification_id = $this->db->insert_id();
    }

    function updateMasterQualification($insertData=array(), $id)
    {
        $this->db->where('student_id',$id);
        $this->db->update('master_qualification',$insertData);
        return true;
    }

    function insertMasterQualification($data=array())
    {
        $this->db->insert('master_qualification', $data);
        return $secondary_qualification_id = $this->db->insert_id();
    }

    function updateQualifiedExam($insertData=array(), $id)
    {
        $this->db->where('student_id',$id);
        $this->db->update('qualified_exams',$insertData);
        return true;
    }

    function insertQualifiedExam($data=array())
    {
        $this->db->insert('qualified_exams', $data);
        return $this->db->insert_id();
    }

    function updateDesiredCourse($insertData=array(), $id)
    {
        $this->db->where('student_id',$id);
        $this->db->update('desired_courses',$insertData);
        return true;
    }

    function insertDesiredCourse($data=array())
    {
        $this->db->insert('desired_courses', $data);
        return $desired_courses_id = $this->db->insert_id();
    }

    function getMySessions($userId)
	{
		$this->db->from('slots');
		$this->db->join('universities', 'universities.id = slots.university_id');
		$this->db->where('slots.user_id', $userId);
		$this->db->where('slots.confirmed_slot IS NOT NULL');
		$result = $this->db->get();
		return $result->result_array();
	}

    function insertLead($insertData=array())
    {
        $this->db->insert('lead_master', $insertData);
        $lead_id = $this->db->insert_id();
        return $lead_id;
    }

    function insertUserMaster($insertData=array())
    {
        $this->db->insert('user_master', $insertData);
        $user_id = $this->db->insert_id();
        return $user_id;
    }

    function insertStudentDetails($insertData=array())
    {
        $this->db->insert('student_details', $insertData);
        return $this->db->insert_id();
    }

    function insertDesireDetails ($insertData=array())
    {
        $this->db->insert('desired_courses', $insertData);
        return $this->db->insert_id();
    }

    function getUserMaster($unameOrEmail)
  	{
  		$this->db->from('user_master');
  		$this->db->where('username', $unameOrEmail);
  		$this->db->or_where('email', $unameOrEmail);
  		$this->db->or_where('phone', $unameOrEmail);
  		$result = $this->db->get();
  		return $result->result_array();
  	}

    function getSlotByUserAndUniversity($userId, $universityId)
	{
		$this->db->from('slots');
		$this->db->where('user_id', $userId);
		$this->db->where('university_id', $universityId);
        $this->db->where('(confirmed_slot > now() OR confirmed_slot IS NULL)');
		$result=$this->db->get()->row();
	   	return $result;
	}

    function addSlot($slotData)
	{
		$this->db->insert('slots', $slotData);
		return $this->db->insert_id();
	}

    function getDocumentsList()
	{
		$this->db->from('document_meta');
    $this->db->where('entity_type', 'STUDENT');
		$result = $this->db->get();
		return $result->result_array();
	}

	function getDocumentByStudentType($userId, $documentType)
	{
		$this->db->from('document');
		$this->db->where('user_id', $userId);
		$this->db->where('document_meta_id', $documentType);
		$result = $this->db->get()->row();
		return $result;
	}

	function updateDocumentByStudentType($userId, $documentType, $updatedData)
	{
		$this->db->where('user_id', $userId);
		$this->db->where('document_meta_id', $documentType);
		$this->db->update('document', $updatedData);
	}

	function addDocument($data)
	{
		$this->db->insert('document', $data);
 	   	return $id = $this->db->insert_id();
	}

	function getDocumentsByStudent($userId)
	{
		$this->db->select('document.*, document_meta.name AS document_type');
		$this->db->from('document');
		$this->db->join('document_meta', 'document_meta.id = document.document_meta_id');
		$this->db->where('document.user_id', $userId);
		$result = $this->db->get();
		return $result->result_array();
	}

    function getCommentsByStudent($studentId)
	{
		$this->db->select('comments.*, user_master.name AS student_name, universities.name AS university_name, um.name AS commented_by, user_master.email AS student_email, user_master.phone AS student_phone');
		$this->db->from('comments');
    $this->db->join('user_master', 'user_master.id = comments.student_login_id');
    $this->db->join('user_master AS um', 'um.id = comments.added_by');
    $this->db->join('universities', 'universities.id = comments.entity_id AND comments.entity_type = "UNIVERSITY" ', 'left');
		$this->db->where('comments.student_login_id', $studentId);
    $this->db->order_by('comments.added_time', 'DESC');
		$result = $this->db->get();
		return $result->result();
	}

    function addComment($data)
	{
		  $this->db->insert('comments', $data);
 	   	return $id = $this->db->insert_id();
	}

    function getVisaByApplicationId($applicationId)
	{
		$this->db->select('users_visa_applicatios.*, universities.id AS university_id, universities.name, universities.slug, universities.website, user_master.email, universities.banner, colleges.name AS college_name, um.name AS student_name, um.email AS student_email, um.phone AS student_phone');
        $this->db->from('users_visa_applicatios');
		$this->db->join('users_applied_universities', 'users_applied_universities.id = users_visa_applicatios.application_id');
		$this->db->join('universities', 'universities.id = users_applied_universities.university_id');
		$this->db->join('user_master', 'user_master.id = universities.user_id');
        $this->db->join('user_master um', 'um.id = users_applied_universities.user_id');
		$this->db->join('colleges', 'colleges.id = users_applied_universities.college_id', 'left');
		$this->db->where('users_visa_applicatios.application_id', $applicationId);
		$result = $this->db->get();
		return $result->result_array();
	}

  function webinarListById($userId){

    $query1 = "SELECT w.*,wm.webinar_attented FROM webinar_master w
                JOIN webinar_log_master wm ON wm.webinar_id = w.id
                WHERE  wm.user_id = $userId
                ;";

            $query = $this->db->query($query1);

            $result_data = $query->result_array();

            return $result_data;
  }

  function likesListById($userId){

    $query1 = "SELECT u.id, u.name AS name, ld.entity_type, ld.date_created FROM likes_detail ld
                  JOIN universities u ON u.id =  ld.entity_id
                  WHERE ld.user_id = $userId AND ld.entity_type = 'universities'
                  UNION
                  SELECT u.id, u.name AS name, ld.entity_type, ld.date_created FROM likes_detail ld
                  JOIN courses u ON u.id =  ld.entity_id
                  WHERE ld.user_id = $userId AND ld.entity_type = 'courses'
                  UNION
                  SELECT u.id, u.topic AS name, ld.entity_type, ld.date_created FROM likes_detail ld
                  JOIN researches u ON u.id =  ld.entity_id
                  WHERE ld.user_id = $userId AND ld.entity_type = 'researches'
                ;";

            $query = $this->db->query($query1);

            $result_data = $query->result_array();

            return $result_data;
  }

  function viewsListById($userId){

    $query1 = "SELECT u.id, u.name AS name, ld.entity_type, ld.date_created FROM views_detail ld
                JOIN universities u ON u.id =  ld.entity_id
                WHERE ld.user_id = $userId AND ld.entity_type = 'universities'
                UNION
                SELECT u.id, u.name AS name, ld.entity_type, ld.date_created FROM views_detail ld
                JOIN courses u ON u.id =  ld.entity_id
                WHERE ld.user_id = $userId AND ld.entity_type = 'courses'
                UNION
                SELECT u.id, u.topic AS name, ld.entity_type, ld.date_created FROM views_detail ld
                JOIN researches u ON u.id =  ld.entity_id
                WHERE ld.user_id = $userId AND ld.entity_type = 'researches'
                ;";

            $query = $this->db->query($query1);

            $result_data = $query->result_array();

            return $result_data;
  }

  function favouritesListById($userId){

    $query1 = "SELECT u.id, u.name AS name,'universities' AS entity_type, ld.createdate AS date_created FROM favourites ld
                JOIN universities u ON u.id =  ld.university_id
                WHERE ld.user_id = $userId
                UNION
                SELECT u.id, u.name AS name, ld.entity_type, ld.date_created FROM favourites_detail ld
                JOIN courses u ON u.id =  ld.entity_id
                WHERE ld.user_id = $userId AND ld.entity_type = 'courses'
                UNION
                SELECT u.id, u.topic AS name, ld.entity_type, ld.date_created FROM favourites_detail ld
                JOIN researches u ON u.id =  ld.entity_id
                WHERE ld.user_id = $userId AND ld.entity_type = 'researches'
                ;";

            $query = $this->db->query($query1);

            $result_data = $query->result_array();

            return $result_data;
  }

  function getUserSessions($userId, $slotType = 'UNIVERSITY')
	{
		$this->db->select('slots.*, universities.name, universities.slug, universities.website, user_master.email, universities.banner');
		$this->db->from('slots');
		$this->db->join('universities', 'universities.id = slots.university_id');
		$this->db->join('user_master', 'user_master.id = universities.user_id');
		$this->db->where('slots.user_id', $userId);
		$this->db->where('slots.slot_type', $slotType);
		//$this->db->group_by('universities.id');
		$this->db->order_by('slots.id DESC');
		$result = $this->db->get();
		return $result->result_array();
	}

  function notesList($userId){

    $query1 = "SELECT c.*,um.name as created_by_name FROM comments c
							JOIN user_master um ON um.id = c.added_by
							WHERE c.student_login_id = $userId ORDER BY c.id DESC ;";

		$query_1 = $this->db->query($query1);
		$result = $query_1->result_array();
	 		return $result;
 	}

	function appDocList($userId) {

		$query1 = " SELECT dm.*,d.url as document_url FROM document_meta dm
								LEFT JOIN  document d ON d.document_meta_id = dm.id AND d.user_id = $userId
								WHERE dm.entity_type = 'APPLICATION' " ;

		$query_1 = $this->db->query($query1);
		$result = $query_1->result_array();
		return $result;
	}

  function studentapplicationlist($universityId){

    $universityCondition = $universityId === 0 ? "" : " AND universities.id = $universityId";

    $query1 = "SELECT DISTINCT(universities.id) as university_id, user_master.email AS university_email, universities.name, universities.website, universities.establishment_year, universities.type, universities.carnegie_accreditation, universities.total_students, universities.total_students_international, universities.placement_percentage, universities.logo, universities.slug, universities.ranking_world, country_master.name AS country_name
              FROM universities
              JOIN country_master ON  universities.country_id = country_master.id
              JOIN user_master ON universities.user_id = user_master.id
              JOIN campuses ON campuses.university_id = universities.id
              JOIN colleges ON colleges.campus_id = campuses.id
              JOIN departments ON departments.college_id = colleges.id
              JOIN courses ON courses.department_id = departments.id
              JOIN degree_master ON courses.degree_id = degree_master.id
              WHERE universities.active = 1
              $universityCondition
              ORDER BY  universities.ranking_world DESC;
                ;";

            $query = $this->db->query($query1);

            $result_data = $query->result_array();

            return $result_data;
  }

  function applyUniversity($data=array())
	{
		$this->db->insert('users_applied_universities', $data);
		return $this->db->insert_id();
	}

	function checkUniversityApplied($userId, $universityId, $collegeId = NULL)
	{
		$this->db->from('users_applied_universities');
		$this->db->where('user_id', $userId);
		$this->db->where('university_id', $universityId);
		if($collegeId)
		{
			$this->db->where('college_id', $collegeId);
		}
		$result = $this->db->get();
		return $result->result_array();
	}

  function getUniversityDataByid($universityId, $courseCategoryId)
 	{
 		$this->db->select('universities.*, country_master.name AS country_name, user_master.email AS email, user_master.image AS image, user_master.id AS login_id, degree_master.name AS degree, colleges.name AS college, courses.id AS course_id,courses.name AS course, campuses.name AS campus, courses.eligibility_gpa AS eligibility_gpa, courses.eligibility_gre AS eligibility_gre, courses.eligibility_toefl AS eligibility_toefl, courses.gpa_scale AS gpa_scale, courses.eligibility_ielts AS eligibility_ielts, courses.course_view_count, courses.course_likes_count, courses.course_fav_count, courses.start_months AS start_months, courses.application_fee_amount AS application_fee_course, courses.no_of_month_work_permit AS stay_back_option, courses.expected_salary AS eligibility_salary, courses.duration AS course_duration, courses.deadline_link AS deadline_link, courses.total_fees AS total_fees, colleges.id AS college_id, colleges.total_enrollment AS college_total_enrollment, colleges.application_deadline_usa AS college_application_deadline_usa, colleges.application_deadline_international AS college_application_deadline_international, colleges.application_fees_usa AS college_application_fees_usa, colleges.application_fees_international AS college_application_fees_international, colleges.average_gre AS college_average_gre, colleges.admission_url AS college_admission_url, departments.id AS department_id, departments.total_enrollment AS department_total_enrollment, departments.application_deadline_usa AS department_application_deadline_usa, departments.application_deadline_international AS department_application_deadline_international, departments.application_fees_usa AS department_application_fees_usa, departments.application_fees_international AS department_application_fees_international, departments.admissions_department_url AS department_admissions_department_url, departments.average_gre AS department_average_gre, departments.name AS department_name, colleges.establishment_date AS college_establishment_date, colleges.ranking AS college_ranking, colleges.college_research_expenditure AS college_research_expenditure, colleges.research_expenditure_per_faculty AS college_research_expenditure_per_faculty, colleges.acceptance_rate AS college_acceptance_rate, country_master.currency AS currency,universities.whatsapp_link AS group_url');
    $this->db->from('universities', 'universities.id = campuses.university_id');
 		$this->db->join('country_master', 'universities.country_id = country_master.id');
		$this->db->join('user_master', 'universities.user_id = user_master.id');
		$this->db->join('campuses', 'campuses.university_id = universities.id');
		$this->db->join('colleges', 'colleges.campus_id = campuses.id');
		$this->db->join('departments', 'departments.college_id = colleges.id');
		$this->db->join('courses', 'courses.department_id = departments.id');
		$this->db->join('degree_master', 'courses.degree_id = degree_master.id');
		//$this->db->join('universities_groups', 'universities_groups.university_id = universities.id', 'left');
 		$this->db->where('universities.id', $universityId);
		if($courseCategoryId){
			$this->db->where('courses.category_id', $courseCategoryId);
		}
         $result=$this->db->get()->result_array();
 		return $result;
 	}

  public function getNewDocumentList($productId=10000) {

		$query1 = " SELECT dm.id AS document_master_id , pdm.id AS product_document_id, pdm.display_name as product_document_name, pdm.description, dm.display_name,
								dm.document_type, dgm.group_name, dgm.tab_name
								FROM product_document_master pdm
								JOIN document_master dm ON JSON_CONTAINS(pdm.document_id, CAST(dm.id as JSON), '$')
								JOIN document_group_master dgm ON dgm.id = dm.group_id
								WHERE pdm.product_id = $productId  order by dgm.sort_order; ";

								$query = $this->db->query($query1);

								$result_data = $query->result_array();

								return [
										"data" 	=> $result_data,
										"query" => $query1
										 ];
	}

	public function getNewDocumentUserDocList($entityId, $productId=10000) {

		$query1 = " SELECT dm.id AS document_master_id , pdm.id AS product_document_id, d.name AS document_display_name, d.id AS document_id, pdm.description, dm.display_name, d.url AS document_url, dm.document_type
								FROM product_document_master pdm
								JOIN document_master dm ON JSON_CONTAINS(pdm.document_id, CAST(dm.id as JSON), '$')
							  JOIN document d ON d.document_meta_id = dm.id AND d.user_id = $entityId AND d.user_type = 'STUDENT'
								WHERE pdm.product_id = $productId; ";

								$query = $this->db->query($query1);

								$result_data = $query->result_array();

								return [
										"data" 	=> $result_data,
										"query" => $query1
										 ];
	}

  public function getAllStudentList() {

    $query1 = "SELECT sd.id, sd.user_id, um.name, um.email, um.phone, um.status, um.createdate, oum.name AS originator_name, oum.email as originator_email,
                  fum.name AS facilitator_name, fum.email as facilitator_email
                  FROM student_details sd
                  JOIN user_master um ON um.id = sd.user_id
                  JOIN user_master oum ON oum.id = sd.originator_id
                  JOIN user_master fum ON fum.id = sd.facilitator_id
                  WHERE um.type = 5
                  ORDER BY um.createdate  DESC;";

								$query = $this->db->query($query1);

								$result_data = $query->result();

                return $result_data;

	}

  public function getAllStudentListMy($userId) {

    $query1 = "SELECT sd.id, sd.user_id, um.name, um.email, um.phone, um.status, um.createdate, oum.name AS originator_name, oum.email as originator_email,
                  fum.name AS facilitator_name, fum.email as facilitator_email
                  FROM student_details sd
                  JOIN user_master um ON um.id = sd.user_id
                  JOIN user_master oum ON oum.id = sd.originator_id
                  JOIN user_master fum ON fum.id = sd.facilitator_id
                  WHERE um.type = 5 AND sd.facilitator_id = $userId
                  ORDER BY um.createdate  DESC;";

								$query = $this->db->query($query1);

								$result_data = $query->result();

                return $result_data;

	}

  public function getAllStudentListUnassigned() {

    $query1 = "SELECT sd.id, sd.user_id, um.name, um.email, um.phone, um.status, um.createdate, oum.name AS originator_name, oum.email as originator_email
                  FROM student_details sd
                  JOIN user_master um ON um.id = sd.user_id
                  JOIN user_master oum ON oum.id = sd.originator_id
                  WHERE um.type = 5 AND ( sd.facilitator_id IS NULL OR sd.facilitator_id = '' )
                  ORDER BY um.createdate  DESC;";

								$query = $this->db->query($query1);

								$result_data = $query->result();

                return $result_data;
	}

}
?>
