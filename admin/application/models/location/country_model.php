<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model 
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 
  <Reverse bidding system> 
    Copyright (C) <2009>  <FreeLance PHP Script>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
    If you want more information, please email me at support@freelancephpscript.com or 
    contact us from http://www.freelancephpscript.com/contact 
 
 
 */
	 class Country_model extends CI_Model {
	 
 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
	

	
	function getCountries($conditions1)
     { 
	   
	   $this->db->where($conditions1);
	   $this->db->from('country_master');
	   $this->db->join('status_master', 'status_master.id = country_master.status','left');
	   $this->db->select('country_master.id,country_master.name,country_master.code,country_master.status,status_master.status as status_name');
       $result = $this->db->get()->result();
	   
       return $result; 
    }
	
	function getActiveCountries()
     { $conditions = array('status'=> '1');
	   $this->db->where($conditions);
	   $this->db->select('*');
	   $this->db->from('infra_country_master');
       $result = $this->db->get()->result();
       return $result; 
    }
	
	 
	  function getCountry($id)
	 {
	 
	 	$conditions = array('id'=> $id);
	 	$this->db->where($conditions);
	 	$this->db->select('*');
		$query = $this->db->get('infra_country_master');		
		$row   = $query->row();
		return $row;
	 }
	 
	 
	 function getCountryByid($conditions)
	 {
	 
	 	
	 	$this->db->where($conditions);
	 	$this->db->from('country_master');
	   $this->db->join('status_master', 'status_master.id = country_master.status','left');
	   $this->db->select('country_master.id,country_master.name,country_master.code,country_master.status,status_master.status as status_name');
	   
	   $result = $this->db->get()->row();
       return $result; 
	 }
	 
	 function getuniversityCountry($conditions)
	 {
	 
	 	
	 	$this->db->where($conditions);
	 	$this->db->from('country_to_university');
		$this->db->join('country_master', 'country_master.id = country_to_university.country_id','left');
	    $this->db->join('status_master', 'status_master.id = country_master.status','left');
	    $this->db->select('country_master.id,country_master.name,country_master.code,country_master.status,status_master.status as status_name');
	   
	   $result = $this->db->get()->result();
       return $result; 
	 }
	 
	 function insertCountry($insertData=array())
	 {
	 $this->db->insert('country_master', $insertData);
	 return 'success';
	 }
	 
	 function insertUniversityCountry($insertData=array())
	 {
	 $this->db->insert('country_to_university', $insertData);
	 return 'success';
	 }
	 
	 function editCountry($insertData=array())
	 {
	 $this->db->where('id',$insertData['id']);
	 $this->db->update('country_master',$insertData);
	 
	 
	 
	 return 'success';
	 }
	 
	 function deleteCountry($id)
	 {
	 $this->db->delete('country_master', array('id' => $id)); 
	 
	 	  
	 return 'success';
	 }
	 
	 function deleteUniversityCountry($data=array())
	 {
	 $this->db->delete('country_to_university',$data); 
	 
	 	  
	 return 'success';
	 }
	 
	 
	/* server side datatable code for country start */

    var $column_order = array('country_master.id','country_master.name','country_master.code','status_master.status',null); //set column field database for datatable orderable
	var $column_search = array('country_master.id','country_master.name','country_master.code','status_master.status'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	var $order = array('country_master.id' => 'desc'); // default order

    private function _get_datatables_query($condition1)
	{
		$this->db->where($condition1);
	   $this->db->from('country_master');
	   $this->db->join('status_master', 'status_master.id = country_master.status','left');
	   $this->db->select('country_master.id,country_master.name,country_master.code,country_master.status,status_master.status as status_name');
		//$this->db->from($this->table);

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					//$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				//if(count($this->column_search) - 1 == $i) //last loop
					//$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($condition1)
	{
		$this->_get_datatables_query($condition1);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($condition1)
	{
		$this->_get_datatables_query($condition1);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($condition1)
	{
		//$this->db->from($this->table);
		$this->db->where($condition1);
	   $this->db->from('country_master');
	   $this->db->join('status_master', 'status_master.id = country_master.status','left');
	   $this->db->select('country_master.id,country_master.name,country_master.code,country_master.status,status_master.status as status_name');
		
		return $this->db->count_all_results();
	} 

	/* server side datatable code for country end */
	 
}
	
	
	
	
	
	
	
	
	
	
	
	
	
?>