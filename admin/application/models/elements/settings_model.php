<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model 
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 
  <Reverse bidding system> 
    Copyright (C) <2009>  <FreeLance PHP Script>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
    If you want more information, please email me at support@freelancephpscript.com or 
    contact us from http://www.freelancephpscript.com/contact 
 
 
 */
	 class Settings_model extends CI_Model {
	 
 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
	
	function getSettings()
     { $this->db->from('site_settings'); //$this->table is a field with the table of categoris
       $result = $this->db->get()->result(); //fetching the result
       
	   
	   
	  // print_r($result); die('++++++++++');
	   return $result; 
    }
	
	
	
	
	function updateSettings($data)
	 {
	     
		  foreach(array_keys($data) as $dt){
		  
		  $updateData = array("value"=>$data["".$dt.""]);
		  //print_r($updateData);
		   $this->db->where('key',$dt);
		   $this->db->update('site_settings',$updateData);
		 }
		
		//$this->db->update('site_settings',$updateData);
		
		
		
	 }
	 
	 
	 
	 function updateEmail($email,$id)
	 {
		   $updateData = array("email"=>$email);
		   $this->db->where('id',$id);
		   $this->db->update('infra_email_master',$updateData);
	 }
	 
	  function updateContact($value,$id)
	 {
		   $updateData = array("value"=>$value);
		   $this->db->where('id',$id);
		   $this->db->update('infra_contact_master',$updateData);
	 }
	 
	 
	  function getEmail($id)
	 {
	 
	 	$conditions = array('id'=> $id);
	 	$this->db->where($conditions);
	 	$this->db->select('email');
		$query = $this->db->get('infra_email_master');		
		$row   = $query->row();
		return $row->email;
	 }
	 
	  function getContact($id)
	 {
	 
	 	$conditions = array('id'=> $id);
	 	$this->db->where($conditions);
	 	$this->db->select('value');
		$query = $this->db->get('infra_contact_master');		
		$row   = $query->row();
		return $row->value;
	 }
	 
	 
	 
}
	
	
	
	
	
	
	
	
	
	
	
	
	
?>