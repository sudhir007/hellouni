<?php
class Tokbox_model extends CI_Model
{
    function addToken($tokenData)
    {
        $this->db->insert('tokbox_token', $tokenData);
        return $this->db->insert_id();
    }

    function editToken($tokenData, $tokenId)
    {
        $this->db->where('id', $tokenId);
        $this->db->update('tokbox_token', $tokenData);
    }

    function getTokenById($id)
    {
        $this->db->from('tokbox_token');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    function getTokenByParticipant($participants)
    {
        $query_string = "SELECT * FROM tokbox_token WHERE JSON_CONTAINS(participants, '" . $participants . "') ORDER BY id DESC LIMIT 1";
        //echo $query_string;
        $query = $this->db->query($query_string);
        return $query->row();
    }
}
?>
