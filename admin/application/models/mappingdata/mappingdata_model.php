<?php

class Mappingdata_model extends CI_Model
{

    //select single row from tablename
    public function GetData($table,$condition='',$order='',$group='',$limit='')
    {
        if($condition != '')
        $this->db->where($condition);
        if($order != '')
        $this->db->order_by($order);
        if($limit != '')
        $this->db->limit($limit);
        if($group != '')
        $this->db->group_by($group);
        return $this->db->get($table)->row();
    }
    //select data from tablename with dynamic table
    function getAllData($tablename,$condition='',$order='')
    {
        if($condition !='')
        $this->db->where($condition);
        if($order !='')     
        $this->db->order_by($order);        
        return $this->db->get($tablename)->result();
    }
    // Insert all data in tablename
    function insert($tablename, $data)
    {
        $this->db->insert($tablename, $data);
    }
    //Update data in tablename with condition 
    function updateData($tableName, $data, $condition  )
    {
       // $this->db->where($condition);
        $this->db->where('id',$condition);
        $this->db->update($tableName, $data);
    }   
    // Delete data
    function delete($tablename, $condition)
    {
        $this->db->where($condition);
        $this->db->delete($tablename);
    }

    function getAllSuggestedUniversity(){

        $this->db->select('dsn.*, um.name AS created_by_name');
        $this->db->from('suggested_university dsn');
        $this->db->join('user_master AS um', 'dsn.user_id = um.id');

        $query = $this->db->get();
        return $query->result();

        
    }

    function getAllCourseData(){

        $this->db->select('dsn.*, um.name AS created_by_name');
        $this->db->from('suggested_course dsn');
        $this->db->join('user_master AS um', 'dsn.user_id = um.id');

        $query = $this->db->get();
        return $query->result();

        
    }

    function getAllCourseDataById($getid){

        $this->db->select('dsn.*, um.name AS created_by_name');
        $this->db->from('suggested_course dsn');
        $this->db->join('user_master AS um', 'dsn.user_id = um.id');
        $this->db->where('dsn.id',$getid);
        $query = $this->db->get();
        return $query->row();

    }

    function updateDataCourse($tableName, $data, $condition  )
    {
       // $this->db->where($condition);
        $this->db->where('id',$condition);
        $this->db->update($tableName, $data);
    } 

    function callingDataList($facilitatorList){

      $query1 = "SELECT cd.*, dsn.display_name AS source_name, um.name AS assign_to_name
                  FROM calling_data cd
                  JOIN data_source_name dsn ON cd.source_id = dsn.id
                  JOIN user_master um ON cd.assigned_to = um.id
                  WHERE cd.assigned_to IN ($facilitatorList)
                   ;";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;

    }

    function commentsList($callingId){

      $query1 = "SELECT cdm.*, um.name AS comment_by
                  FROM calling_data_comments cdm
                  JOIN user_master um ON cdm.added_by = um.id
                  WHERE cdm.entity_id = $callingId
                   ;";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;

    }

    function sourceList($userId){

      $query1 = "SELECT dsn.*, um.name AS created_by_name
                  FROM data_source_name dsn
                  JOIN user_master um ON dsn.created_by = um.id
                  WHERE dsn.created_by = $userId
                   ;";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;

    }


    /* Server side datatables for calling list start */

    var $column_order = array('cd.name','cd.email','cd.mobile','cd.mobile1','cd.lead_state','um.name','dsn.display_name','cd.date_created','cd.next_followup_date',null); //set column field database for datatable orderable
    var $column_search = array('cd.name','cd.email','cd.mobile','cd.mobile1','cd.lead_state','um.name','dsn.display_name','cd.date_created','cd.next_followup_date'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('cd.id' => 'desc'); // default order

    private function _get_datatables_query_callingList($facilitatorList)
    {
        $this->db->select('cd.*, dsn.display_name AS source_name, um.name AS assign_to_name');
        $this->db->from('calling_data cd');
        $this->db->join('data_source_name AS dsn', 'cd.source_id = dsn.id');
        $this->db->join('user_master AS um', 'cd.assigned_to = um.id');
        $this->db->where("cd.assigned_to IN ($facilitatorList) ");
        
        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //if(count($this->column_search) - 1 == $i) //last loop
                   // $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_callingList($facilitatorList)
    {
        $this->_get_datatables_query_callingList($facilitatorList);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $query->result();
    }

    function count_filtered_callingList($facilitatorList)
    {
        $this->_get_datatables_query_callingList($facilitatorList);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_callingList($facilitatorList)
    {
        $this->db->select('cd.*, dsn.display_name AS source_name, um.name AS assign_to_name');
        $this->db->from('calling_data cd');
        $this->db->join('data_source_name AS dsn', 'cd.source_id = dsn.id');
        $this->db->join('user_master AS um', 'cd.assigned_to = um.id');
        $this->db->where("cd.assigned_to IN ($facilitatorList) ");
        
        return $this->db->count_all_results();
    }

    /* Server side datatables for calling list end */


    /* Server side datatables for sourceList  start */

    var $column_orders = array('dsn.name','dsn.display_name','dsn.status','dsn.date_created'); //set column field database for datatable orderable
    var $column_searchs = array('dsn.name','dsn.display_name','dsn.status','dsn.date_created'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $orders = array('dsn.id' => 'desc'); // default order

    private function _get_datatables_query_sourceList($userId)
    {
        $this->db->select('dsn.*, um.name AS created_by_name');
        $this->db->from('data_source_name dsn');
        $this->db->join('user_master AS um', 'dsn.created_by = um.id');
        $this->db->where("dsn.created_by = $userId ");

         
        $i = 0;
    
        foreach ($this->column_searchs as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //if(count($this->column_search) - 1 == $i) //last loop
                   // $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_orders[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->orders))
        {
            $order = $this->orders;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_sourceList($userId)
    {
        $this->_get_datatables_query_sourceList($userId);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $query->result();
    }

    function count_filtered_sourceList($userId)
    {
        $this->_get_datatables_query_sourceList($userId);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_sourceList($userId)
    {
        $this->db->select('dsn.*, um.name AS created_by_name');
        $this->db->from('data_source_name dsn');
        $this->db->join('user_master AS um', 'dsn.created_by = um.id');
        $this->db->where("dsn.created_by = $userId ");
        
        return $this->db->count_all_results();
    }

    /* Server side datatables for sourceList end */

  }

 ?>
