/* university tables */

CREATE TABLE `university_overview_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `university_id` bigint(20) unsigned NOT NULL,
  `total_students` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `international_students` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_degree_programs` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `establishment_year` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `acceptance_rate` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `internships` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `application_fee` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `average_tuition_fee` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `average_cost_living` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `average_eligibility` json DEFAULT NULL,
  `overview_paragraph_one` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `overview_paragraph_two` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `overview_paragraph_three` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `read_more_international_students` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `read_more_student_life` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `read_more_employment_figures` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `read_more_programs` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `read_more_research` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `read_more_alumni` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_by` bigint(20) unsigned DEFAULT NULL,
  `updated_by` bigint(20) unsigned DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `university_scholarships_financial_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `university_id` bigint(20) unsigned NOT NULL,
  `financial_assistance` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `financial_aid` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accommodations_offered` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `on_campus` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `off_campus` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_by` bigint(20) unsigned DEFAULT NULL,
  `updated_by` bigint(20) unsigned DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `university_faq_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `university_id` bigint(20) unsigned NOT NULL,
  `q_one` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `a_one` text DEFAULT NULL,
  `q_two` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `a_two` text DEFAULT NULL,
  `q_three` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `a_three` text DEFAULT NULL,
  `q_four` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `a_four` text DEFAULT NULL,
  `q_five` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `a_five` text DEFAULT NULL,
  `q_six` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `a_six` text DEFAULT NULL,
  `q_seven` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `a_seven` text DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_by` bigint(20) unsigned DEFAULT NULL,
  `updated_by` bigint(20) unsigned DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `university_admission_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `university_id` bigint(20) unsigned NOT NULL,
  `a_one` text DEFAULT NULL,
  `a_two` text DEFAULT NULL,
  `a_three` text DEFAULT NULL,
  `a_four` text DEFAULT NULL,
  `a_five` text DEFAULT NULL,
  `a_five_a` text DEFAULT NULL,
  `a_five_b` text DEFAULT NULL,
  `a_five_c` text DEFAULT NULL,
  `a_five_d` text DEFAULT NULL,
  `a_six` text DEFAULT NULL,
  `a_six_a` text DEFAULT NULL,
  `a_seven` text DEFAULT NULL,
  `a_eight` text DEFAULT NULL,
  `a_nine` text DEFAULT NULL,
  `a_ten` text DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_by` bigint(20) unsigned DEFAULT NULL,
  `updated_by` bigint(20) unsigned DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `suggested_university` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `university_name` bigint(20) unsigned DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `mapped_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `updated_by` bigint(20) unsigned DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `suggested_course` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `course_name` bigint(20) unsigned DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `mapped_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `updated_by` bigint(20) unsigned DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
