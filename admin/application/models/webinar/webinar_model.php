<?php

class Webinar_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function webinar_list(){

      $query1 = "SELECT *
                FROM (
                SELECT w.*,count(wm.webinar_id) as acount FROM webinar_master w
                LEFT JOIN webinar_log_master wm ON wm.webinar_id = w.id
                JOIN user_master um ON wm.user_id = um.id AND um.type = 5
                GROUP BY w.id
                UNION
                SELECT *, '0' as acount FROM webinar_master
                WHERE id NOT IN (
                SELECT w.id FROM webinar_master w
                LEFT JOIN webinar_log_master wm ON wm.webinar_id = w.id
                JOIN user_master um ON wm.user_id = um.id AND um.type = 5
                GROUP BY w.id)
                	) AS i
                ORDER BY webinar_date,STR_TO_DATE(webinar_time, '%h.%i%p');";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;
    }

    function webinar_student_list(){

      $this->db->select('student_details.id AS student_id, student_details.user_id AS student_login_id, student_details.facilitator_id AS facilitator_id, user_master.name AS student_name, user_master.email AS student_email, user_master.phone AS student_phone, student_details.desired_destination AS desired_destination, um.name AS facilitator_name, oum.name AS originator_name');
      $this->db->from('student_details');
      $this->db->join('user_master', 'user_master.id = student_details.user_id');
      $this->db->join('user_master AS um', 'um.id = student_details.facilitator_id');
      $this->db->join('user_master AS oum', 'oum.id = student_details.originator_id');
      $this->db->where('user_master.type', 5);
      $this->db->where('user_master.registration_type','WEBINAR');

      $result = $this->db->get();
      $result_data = $result->result_array();

      return $result_data;

    }

    function webinar_student_list_by_id($webinarId){

      $this->db->select('student_details.id AS student_id, student_details.user_id AS student_login_id,
      student_details.facilitator_id AS facilitator_id, user_master.name AS student_name, user_master.email AS student_email,
      user_master.phone AS student_phone, student_details.desired_destination AS desired_destination,
      um.name AS facilitator_name, oum.name AS originator_name, wm.webinar_name, wlm.webinar_attented, wm.tokbox_token_id');
      $this->db->from('student_details');
      $this->db->join('user_master', 'user_master.id = student_details.user_id');
      $this->db->join('user_master AS um', 'um.id = student_details.facilitator_id');
      $this->db->join('user_master AS oum', 'oum.id = student_details.originator_id');
      $this->db->join('webinar_log_master AS wlm', 'wlm.user_id = user_master.id');
      $this->db->join('webinar_master AS wm', 'wm.id = wlm.webinar_id');
      $this->db->where('user_master.type', 5);
      $this->db->where('wlm.webinar_id',$webinarId);

      $result = $this->db->get();
      $result_data = $result->result_array();

      return $result_data;

    }

    function university_student_list_by_id($webinarId){

      $this->db->select('user_master.email AS student_email, student_details.id AS student_id, student_details.user_id AS student_login_id,
      user_master.name AS student_name, user_master.phone AS student_phone, student_details.desired_destination AS desired_destination,
      wm.webinar_name, wlm.webinar_attented, wm.tokbox_token_id');
      $this->db->from('student_details');
      $this->db->join('user_master', 'user_master.id = student_details.user_id');
      $this->db->join('webinar_log_master AS wlm', 'wlm.user_id = user_master.id');
      $this->db->join('webinar_master AS wm', 'wm.id = wlm.webinar_id');
      $this->db->where('user_master.type', 5);
      $this->db->where('wlm.webinar_id',$webinarId);
      $this->db->group_by('user_master.email');

      $result = $this->db->get();
      $result_data = $result->result_array();

      return $result_data;

    }

    function getWebinarById($wid){

      $this->db->select('*');
      $this->db->from('webinar_master');
      $this->db->where('id',$wid);

      $result=$this->db->get()->result();
      return $result[0];

    }

    function post($data, $tableName) {

   $insert_query = $this->db->insert($tableName, $data);

   return "SUCCESS";
   //$error = $this->db->error();
   $lastInsertedId = 0;
   if ($insert_query)
   {
       $lastInsertedId = $this->db->insert_id();
   }

   $where = ['id' => $lastInsertedId];
   $data = $this->get($where, $tableName);



   return [
       "data" => $data,
       "last_inserted_id" => $lastInsertedId
   ];
}

  function get($where = array(), $tableName) {

    $this->db->select('*');
    $this->db->from($tableName);
    $this->db->where($where);

    $query = $this->db->get();
    $data = $query->result_array();

    return $response = [
        'data' => $data
    ];
  }


  function put ($where = array(), $data = array(), $tableName) {

    $this->db->where($where);
    $this->db->update($tableName, $data);

    $query1 = $this->db->last_query();
    return $query1;
    return 'success';


   }

   function putWithGet ($where = array(), $data = array(), $tableName) {

     $this->db->where($where);
     $this->db->update($tableName, $data);

     $query1 = $this->db->last_query();

     $this->db->select('*');
     $this->db->from($tableName);
     $this->db->where($where);
     $query = $this->db->get();
     //$error_result = $this->db->error();
     $otpData = $query->result_array();

     return [
       'data' => $otpData,
       'lastquery' => $query1
     ];

    }

   public function upsert ($wheredata = array(), $insertData = array(), $tableName) {

     $this->db->select('*');
     $this->db->from($tableName);
     $this->db->where($wheredata);
     $query = $this->db->get();
     //$error_result = $this->db->error();
     $otpData = $query->result_array();
     //return $query->num_rows();

     if($query->num_rows() === 0) {

         $insert_query = $this->db->insert($tableName, $insertData);
         //return $insert_query;
         $data = $this->db->insert_id();

     } else {

     $this->db->where($wheredata);
     $this->db->update($tableName, $insertData);

     $data = "1";

     }

       return $response = [
           'data' => $data,
           "last_inserted_id" => "1"
       //    'error' => $error_result
       ];
   }

   function getAllPolls()
   {
       $this->db->from('polls');
       $this->db->join('poll_questions', 'poll_questions.poll_id = polls.id');
       $this->db->join('webinar_polls', 'webinar_polls.poll_id = polls.id', 'left');
       $this->db->join('webinar_master', 'webinar_polls.webinar_id = webinar_master.id', 'left');

       $query = $this->db->get();
       return $query->result_array();
   }

   function studentListByUniversityFair($userId){

    $query1 = "SELECT um.*,sd.*,sd.id AS student_id FROM user_master um
              JOIN virtual_fair_slots vfs ON json_contains(vfs.student_booked, CAST(um.id as JSON), '$') OR json_contains(vfs.student_attending, CAST(um.id as JSON), '$')
              JOIN virtual_fair_participants vfp ON vfp.id = vfs.participant_id
              JOIN student_details sd ON sd.user_id = um.id
              WHERE vfp.university_id = $userId AND um.type = 5;";

    $query = $this->db->query($query1);

    $result_data = $query->result_array();

    return $result_data;

   }

   function getSeminarStudents()
   {
       $this->db->select('user_master.id, user_master.name, user_master.email, seminar_students.student_id, document.id AS document_id, document.document_meta_id');
       $this->db->from('seminar_students');
       $this->db->join('user_master', 'user_master.id = seminar_students.user_id');
       $this->db->join('document', 'document.user_id = user_master.id', 'left');
       $this->db->order_by('user_master.id', 'asc');
       //$this->db->group_by('user_master.id');
       $query = $this->db->get();
       $result = $query->result_array();
       return $result;
   }

   function getDocument($userId)
   {
       $this->db->select('document.*, document_meta.name AS document_name, document_meta.required');
       $this->db->from('document');
       $this->db->join('document_meta', 'document.document_meta_id = document_meta.id');
       $this->db->where('document.user_id', $userId);
       $query = $this->db->get();
       $result = $query->result_array();
       return $result;
   }

   function getDocumentsByUniversity($universityId)
   {
       $this->db->from('document_meta');
       $this->db->where('university_id', $universityId);
       $query = $this->db->get();
       $result = $query->result_array();
       return $result;
   }

   // Partner Registration Process

   function checkUserLogin($userEmail,$userMobile) {

      $where_in = array('5','8','9');

      $this->db->select('*');
      $this->db->from('user_master');
      $this->db->where_in('type', $where_in);
      $this->db->where("(email = '$userEmail' OR phone = '$userMobile')");
      $query = $this->db->get();
      $result = $query->result_array();

      return $result;
    }

    function checkMasterOtp($mobileNumber){

        $this->db->select('*');
        $this->db->from('otp_master');
        $this->db->where('mobile_number', $mobileNumber);
        $this->db->where('status', 1);
        $query = $this->db->get();
        $result = $query->result_array();

        return $result;
    }

    function checkUserIDLogin($userId) {
      $this->db->select('*');
      $this->db->from('user_master');
      $this->db->where('username', $userId);
      $query = $this->db->get();
      $result = $query->result_array();

      return $result;
    }


 }

?>
