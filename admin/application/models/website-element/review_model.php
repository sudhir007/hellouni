<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model 
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 
  <Reverse bidding system> 
    Copyright (C) <2009>  <FreeLance PHP Script>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
    If you want more information, please email me at support@freelancephpscript.com or 
    contact us from http://www.freelancephpscript.com/contact 
 
 
 */
	 class Review_model extends CI_Model {
	 
 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
	function insertReview($insertData=array())
	 {
	  $this->db->insert('review_master', $insertData);
	 //die('+++++++');
	 return 'success';
	 }
	 
	 
	 
	 
	 function getReviews($condition1,$condition2)
     { 
	 
	 if(count($condition1)>0 || count($condition2)>0  || isset($condition1) || isset($condition2)){
	  $this->db->where($condition1);
	  $this->db->or_where($condition2);
	  }
	  
	 $this->db->from('review_master');
	 $this->db->join('room_type_master', 'room_type_master.id = review_master.room_id','left');
	 $this->db->join('infra_description_master', 'infra_description_master.id = room_type_master.description_id','left');
	  $this->db->join('infra_status_master', 'infra_status_master.id = review_master.status','left');
	  
	 $this->db->select('review_master.review_id as id,review_master.room_id,review_master.author,review_master.text,review_master.rating,review_master.status,review_master.date_added,review_master.date_modified,infra_status_master.name as status_name,infra_description_master.name as room_name');   
	 
	 $result=$this->db->get()->result();
	 
	
	 return $result;
	 
    }
	
	function getReviewByid($conditions)
     { 
	 $this->db->where($conditions);
	 $this->db->from('review_master');
	  $this->db->join('infra_status_master', 'infra_status_master.id = review_master.status','left');
	 $this->db->select('review_master.review_id as id,review_master.room_id,review_master.author,review_master.text,review_master.rating,review_master.status,review_master.date_added,review_master.date_modified,infra_status_master.name as status_name');  
	 
	 $result=$this->db->get()->row();

	 return $result;
	 
    }
	
	
	
	
	 
	 
	 function editReview($id,$insertData=array())
	 {
	 		
			
		 $this->db->where('review_id',$id);
		 $this->db->update('review_master',$insertData);

	  return 'success';
	 }
	 
	 
	 
	 function deleteReview($id)
	 {
	  $data = array('status' => '4');  
	   
	 $this->db->where('review_id',$id);
	 $this->db->update('review_master',$data);
	 return 'success';
	 }
	
	 
}
	
	
	
	
	
	
	
	
	
	
	
	
	
?>