<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model 
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 
  <Reverse bidding system> 
    Copyright (C) <2009>  <FreeLance PHP Script>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
    If you want more information, please email me at support@freelancephpscript.com or 
    contact us from http://www.freelancephpscript.com/contact 
 
 
 */
	 class Room_model extends CI_Model {
	 
 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
	function insertRoom($insertData=array())
	 {
	  		
	 $description_tbl_data = array(
     'name' => $insertData['name'],
     'description' => $insertData['description']
     );
	 
	 $this->db->insert('infra_description_master', $description_tbl_data);
	 $description_id = $this->db->insert_id();
	 
	 
	 $seo_tbl_data = array(
     'meta_tag' => $insertData['room_meta_tag'],
     'meta_description' => $insertData['room_meta_description']
     );
	 
	 $this->db->insert('seo_master', $seo_tbl_data);
	 $seo_id = $this->db->insert_id();
	 
	 $room_tbl_data = array(
     'description_id' =>  $seo_id,
	 'seo_id' =>  $description_id,
     'sort_order' => $insertData['sort_order'],
	 'rate' => $insertData['room_rate'],
	 'enquery' => $insertData['enquery'],
	 'booking' => $insertData['booking'],
	 'virtual_tour' => $insertData['virtual_tour'],
	 'status'       => '1'
	 );
	 
	 //print_r($room_tbl_data); exit();
	 $this->db->insert('room_type_master', $room_tbl_data);
	 $room_id = $this->db->insert_id();
	 
	 if($insertData['properties']){
	 foreach($insertData['properties'] as $key):
	  $roomToproperty_tbl_data = array(
      'property_id' =>  $key,
      'room_type_id' => $room_id
	  );
	  $this->db->insert('room_to_property', $roomToproperty_tbl_data);
	  endforeach;
	 }
	 //print_r($insertData['amenities']);
//	 
//	 die('+++');
     if($insertData['amenities']){
	 foreach($insertData['amenities'] as $key):
	  $roomToamenity_tbl_data = array(
      'amenities_id' =>  $key,
      'room_type_id' => $room_id
	  );
	 
	  $this->db->insert('amenities_to_rooms', $roomToamenity_tbl_data);
	  endforeach;
	  }
	  // insert into gallery_to_room	(photo)
	 if($insertData['photo']){ 
	 foreach ($insertData['photo'] as $key1){
	 $photodata = array(
	 'gallery_id' => $key1,
     'room_type_id' => $room_id
      );
	 	  
	 $this->db->insert('gallery_to_room', $photodata);
	 }
	 
	 }
	 // insert into gallery_to_property	(video)
	 if($insertData['video']){ 
	 foreach ($insertData['video'] as $key2){
	 $videodata = array(
	 'gallery_id' => $key2,
     'room_type_id' => $room_id
      );
	 $this->db->insert('gallery_to_room', $videodata);
	 }
	 }
	 
	 
	
	 return 'success';
	 }
	 
	 
	 
	 
	 function getRooms($condition1,$condition2)
     { 
	 
	 if(count($condition1)>0 || count($condition2)>0  || isset($condition1) || isset($condition2)){
	  $this->db->where($condition1);
	  $this->db->or_where($condition2);
	  }
	  
	 $this->db->from('room_type_master');
	 $this->db->join('infra_description_master', 'room_type_master.description_id = infra_description_master.id','left');
	 $this->db->join('seo_master', 'room_type_master.seo_id = seo_master.id','left');
	 
	 
	 
	 $this->db->select('room_type_master.id,room_type_master.description_id,room_type_master.sort_order,infra_description_master.name,infra_description_master.description,seo_master.meta_tag,seo_master.meta_description, infra_description_master.id as desc_id');
	 
	 $result=$this->db->get()->result();
	 
	
	 return $result;
	 
    }
	
	function getRoomByid($conditions)
     { 
	 $this->db->where($conditions);
	 $this->db->from('room_type_master');
	 $this->db->join('infra_description_master', 'room_type_master.description_id = infra_description_master.id','left');
	 $this->db->join('seo_master', 'room_type_master.seo_id = seo_master.id','left');
	 
	  $this->db->select('room_type_master.id,room_type_master.description_id,room_type_master.sort_order,room_type_master.rate,room_type_master.enquery,room_type_master.booking,room_type_master.virtual_tour,infra_description_master.name,infra_description_master.description,seo_master.meta_tag,seo_master.meta_description, infra_description_master.id as desc_id');
	 
	 $result=$this->db->get()->result();

	 return $result;
	 
    }
	
	function getPropertiesBy_Room($conditions)
     { 
	 $this->db->where($conditions);
	 $this->db->from('room_to_property');
	
	 $this->db->select('room_to_property.id,room_to_property.property_id');
	 
	 $result=$this->db->get()->result();

	 return $result;
	 
    }
	
	function getAmenitiesBy_Room($conditions)
     { 
	 $this->db->where($conditions);
	 $this->db->from('amenities_to_rooms');
	
	 $this->db->select('amenities_to_rooms.id,amenities_to_rooms.room_type_id,amenities_to_rooms.amenities_id');
	 
	 $result=$this->db->get()->result();

	 return $result;
	 
    }
	
	
	 
	 function getContactmaster_info($conditions)
     { 
	    $query = $this->db->get_where('infra_contact_master', $conditions)->result();
		return $query; 
		 
	 }
	 
	 
	 
	 
	 function editRoom($insertData=array())
	 {
	   $description_tbl_data = array(
       'name' => $insertData['name'],
       'description' => $insertData['description'],
	   
       );
	 
	 $room_details = $this->db->get_where('room_type_master', array('room_type_master.id' => $insertData['room_id']))->row();
	
	 $description_id = $room_details->description_id;
	 $seo_id = $room_details->seo_id;
	 
	 
	 $seo_tbl_data = array(
     'meta_tag' => $insertData['room_meta_tag'],
     'meta_description' => $insertData['room_meta_description']
     );
	  $this->db->where('id',$seo_id);
	 $this->db->update('seo_master',$seo_tbl_data);

	 
	 // update infra_description_master
	 $this->db->where('id',$description_id);
	 $this->db->update('infra_description_master',$description_tbl_data);
	 
	 // update room_type_master
	 $room_master_tbl_data = array(
	 'sort_order' => $insertData['sort_order'],
	 'rate' => $insertData['room_rate'],
	 'enquery' => $insertData['enquery'],
	 'booking' => $insertData['booking'],
	 'virtual_tour' => $insertData['virtual_tour']
	 );
	 
	 // update amenities_master
	 $this->db->where('id',$insertData['room_id']);
	 $this->db->update('room_type_master',$room_master_tbl_data);
	 
	 
	 $this->db->delete('room_to_property', array('room_type_id' => $insertData['room_id'])); 
	
	 foreach($insertData['properties'] as $key):
	  $roomToproperty_tbl_data = array(
      'property_id' =>  $key,
      'room_type_id' => $insertData['room_id']
	  );
	 
	  $this->db->insert('room_to_property', $roomToproperty_tbl_data);
	  endforeach;
	  
	  $this->db->delete('amenities_to_rooms', array('room_type_id' => $insertData['room_id'])); 
	  
	  foreach($insertData['amenities'] as $key):
	  $roomToamenity_tbl_data = array(
      'amenities_id' =>  $key,
      'room_type_id' => $insertData['room_id']
	  );
	 
	  $this->db->insert('amenities_to_rooms', $roomToamenity_tbl_data);
	 
	 endforeach;
	 
	 
	  $this->db->delete('gallery_to_room', array('room_type_id' => $insertData['room_id'])); 
	 
	  // insert into gallery_to_room	(photo)
	 foreach ($insertData['photo'] as $key1){
	 $photodata = array(
	 'gallery_id' => $key1,
     'room_type_id' => $insertData['room_id']
      );
	 $this->db->insert('gallery_to_room', $photodata);
	 }
	
	 // insert into gallery_to_property	(video)
	 foreach ($insertData['video'] as $key2){
	 $videodata = array(
	 'gallery_id' => $key2,
     'room_type_id' => $insertData['room_id']
      );
	 $this->db->insert('gallery_to_room', $videodata);
	 }
	 
	  return 'success';
	 }
	 
	 
	 
	 function deleteRoom($id)
	 {
	  $data = array('status' => '4');  
	   
	 $this->db->where('id',$id);
	 $this->db->update('room_type_master',$data);
	 return 'success';
	 }
	
	 
}
	
	
	
	
	
	
	
	
	
	
	
	
	
?>