<?php
class Universitydata_model extends CI_Model
{
  function __construct()
  {
      parent::__construct();
  }


   function post($data, $tableName) {

  $insert_query = $this->db->insert($tableName, $data);

  return "1";
  //$error = $this->db->error();
  $lastInsertedId = 0;
  if ($insert_query)
  {
      $lastInsertedId = $this->db->insert_id();
  }

  $where = ['id' => $lastInsertedId];
  $data = $this->get($where, $tableName);



  return [
      "data" => $data,
      "last_inserted_id" => $lastInsertedId
  ];
}

 function get($where = array(), $tableName) {

      $this->db->select('*');
      $this->db->from($tableName);
      $this->db->where($where);

      $query = $this->db->get();
      $data = $query->result_array();

      return $response = [
          'data' => $data
      ];
  }


 function put ($where = array(), $data = array(), $tableName) {

      $this->db->where($where);
      $this->db->update($tableName, $data);
      //echo $this->db->last_query();

      $this->db->select('*');
      $this->db->from($tableName);
      $this->db->where($where);
      $query = $this->db->get();
      $data = $query->result_array();

      return $data ;
  }

  public function upsert ($wheredata = array(), $insertData = array(), $tableName) {

    $this->db->select('*');
    $this->db->from($tableName);
    $this->db->where($wheredata);
    $query = $this->db->get();
    //$error_result = $this->db->error();
    $otpData = $query->result_array();
    //return $query->num_rows();

    if($query->num_rows() === 0) {

        $insert_query = $this->db->insert($tableName, $insertData);
        //return $insert_query;
        $data = $this->db->insert_id();

    } else {

    $this->db->where($wheredata);
    $this->db->update($tableName, $insertData);

    $data = "1";

    }

      return $response = [
          'data' => $data,
          "last_inserted_id" => "1"
      //    'error' => $error_result
      ];
  }

  public function getImageData($where = array(), $columnName, $tableName){

    $this->db->select($columnName);
    $this->db->from($tableName);
    $this->db->where($where);

    $query = $this->db->get();
    $data = $query->result_array();

    return $response = [
        'data' => $data
    ];

  }

  public function getUniversityAddData($universityId){


    $query1 = "SELECT u.id AS u_id, u.name AS university_name, u.banner AS university_banner, u.logo AS university_logo, u.map_preview AS university_map_preview,  uat.*
                FROM universities u
                LEFT JOIN university_aditional_data uat ON u.id = uat.university_id
                WHERE u.id = $universityId";

    $query = $this->db->query($query1);

    $result_data = $query->result_array();

    return $response = [
        'data' => $result_data
    ];

  }

  public function getUniversityAddDataList(){

      $query1 = "SELECT u.id AS university_id, u.name AS university_name
                  FROM universities u
                  JOIN university_aditional_data uat ON u.id = uat.university_id
                  WHERE u.active = 1";

      $query = $this->db->query($query1);

      $result_data = $query->result_array();

      return $response = [
          'data' => $result_data
      ];

    }

    public function universityProcessInfoList(){

        $query1 = "SELECT u.id AS university_id,u.user_id, u.name AS university_name, u.active
                    FROM universities u
                    JOIN university_application_process_info upi ON u.id = upi.entity_id
                    WHERE upi.status = 1";

        $query = $this->db->query($query1);

        $result_data = $query->result_array();

        return $response = [
            'data' => $result_data
        ];

      }

}
