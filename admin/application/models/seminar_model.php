<?php
class Seminar_model extends CI_Model
{
    function updateRoom($studentId, $updatedData)
    {
        $this->db->where('user_id', $studentId);
        $this->db->update('seminar_students', $updatedData);
    }
}
