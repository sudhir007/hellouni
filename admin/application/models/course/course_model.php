<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 */
	 class Course_model extends CI_Model {

 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


	function getCourse($condition1)
	 {

		$this->db->where($condition1);
		$this->db->from('course_master');
		$this->db->join('course_type', 'course_type.id = course_master.type','left');
		$this->db->join('status_master', 'status_master.id = course_master.status','left');



		$this->db->select('course_master.id,course_master.name,course_master.details,course_master.type,course_type.type as typename,course_master.createdate,course_master.modifydate,course_master.status,status_master.status as status_name');

		//,course_master.type,course_type.type as typename,course_master.createdate,course_master.modifydate,course_master.status,status_master.status as status_name

		$result=$this->db->get()->result();

		return $result;

	 }

	 function getAllCourses($condition)
	 {
		 $this->db->where($condition);
 		$this->db->from('courses');
		$result=$this->db->get()->result();

		return $result;
	 }

	 	function getCourseDetailsById($condition1)
	 {

		$this->db->where($condition1);
		$this->db->from('course_master');
		$this->db->join('course_type', 'course_type.id = course_master.type','left');
		$this->db->join('status_master', 'status_master.id = course_master.status','left');



		$this->db->select('course_master.id,course_master.name,course_master.details,course_master.type,course_type.type as typename,course_master.createdate,course_master.modifydate,course_master.status,status_master.status as status_name');

		//,course_master.type,course_type.type as typename,course_master.createdate,course_master.modifydate,course_master.status,status_master.status as status_name

		$result=$this->db->get()->row();

		return $result;

	 }


	 function getSubCourses($condition1)
	 {

		$this->db->where($condition1);
		$this->db->from('course_to_subCourse');
		$this->db->join('course_master', 'course_master.id = course_to_subCourse.subcourse_id','left');
		$this->db->join('status_master', 'status_master.id = course_master.status','left');


		//$this->db->select('*');
		$this->db->select('course_master.id as idd,course_master.name');

		//,course_master.type,course_type.type as typename,course_master.createdate,course_master.modifydate,course_master.status,status_master.status as status_name
		$result=$this->db->get()->result();
		//print_r($result); exit;
		return $result;

	 }


	 function getuniversity_to_degree_to_course ($conditions){

	 $this->db->where($conditions);

	 $this->db->from('university_to_degree_to_course');
	 $this->db->join('user_master', 'user_master.id = university_to_degree_to_course.university_id','left');

	 $this->db->select('university_to_degree_to_course.id,university_to_degree_to_course.degree_id,university_to_degree_to_course.course_id,university_to_degree_to_course.university_id,university_to_degree_to_course.language,university_to_degree_to_course.admissionsemester,university_to_degree_to_course.beginning,university_to_degree_to_course.duration,university_to_degree_to_course.application_deadline,university_to_degree_to_course.details,university_to_degree_to_course.tutionfee,university_to_degree_to_course.enrolmentfee,university_to_degree_to_course.livingcost,university_to_degree_to_course.jobopportunities,university_to_degree_to_course.required_language,university_to_degree_to_course.academicrequirement,university_to_degree_to_course.academicrequirement,user_master.name');

	 $result=$this->db->get()->result();
	 //echo $this->db->last_query();
	 return $result;
	}



	function getCoursesRelatedToUniversityDegree ($conditions){

	 $this->db->where($conditions);

	 $this->db->from('university_to_degree_to_course');
	 $this->db->join('course_master', 'course_master.id = university_to_degree_to_course.course_id','left');
	 $this->db->join('status_master', 'status_master.id = course_master.status','left');
	 $this->db->select('course_master.id,course_master.name,course_master.status,status_master.status as status_name');

	 $result=$this->db->get()->result();

	 return $result;
	}



	 function getAllCourseTypes()
	 {
		$this->db->from('course_type');
		$this->db->select('*');
		$result=$this->db->get()->result();
		return $result;

	 }

	 function getAllCourseCategories()
	 {
		$this->db->from('courses_categories');
		$this->db->select('*');
		$result=$this->db->get()->result();
		return $result;

	 }

	 function getAllCollegeCategories()
	 {
		$this->db->from('colleges_categories');
		$this->db->select('*');
		$result=$this->db->get()->result();
		return $result;

	 }

	function checkUser($conditions=array())
	 {

		//echo 'aaaaaa'; die('----');
		$this->db->where($conditions);
		$this->db->from('user_master');
		$this->db->join('type_master', 'type_master.type_id = user_master.type','left');
		$this->db->join('status_master', 'status_master.id = user_master.status','left');



		$this->db->select('user_master.id,user_master.name,user_master.email,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,status_master.status as status_name');



		$result=$this->db->get()->row();
		return $result;

	 }


	 function checkUserBy_Email($conditions=array())
	 {


		$this->db->where($conditions);
		$this->db->from('infra_email_master');
		$this->db->join('infra_user_master', 'infra_email_master.id = infra_user_master.email','left');
		$this->db->join('infra_role_master', 'infra_role_master.id = infra_user_master.role_id','left');
		$this->db->join('infra_status_master', 'infra_status_master.id = infra_user_master.status_id','left');

		$this->db->select('infra_user_master.id,infra_user_master.name,infra_user_master.user_name,infra_role_master.name as role,infra_email_master.email,infra_user_master.status_id,infra_status_master.name as statusname');



		$result=$this->db->get()->row();
		return $result;

	 }


	function addRemerberme($insertData=array(),$expire)
	{

		 $this->auth_model->setUserCookie('uname',$insertData['username'], $expire);
		 $this->auth_model->setUserCookie('pwd',$insertData['password'], $expire);

		// echo $val=get_cookie('uname',TRUE); exit;
		 echo $this->input->cookie('uname', TRUE); exit;
		 //die();

	}

	function removeRemeberme()
	{

	  $this->auth_model->clearUserCookie(array('uname','pwd'));

	}


	function insertCourse($insertData=array())
	 {

	 $course_master_data = array(
     'name' 		=> $insertData['name'],
     'details' 		=> $insertData['details'],
	 'type' 		=> $insertData['type'],

	 'createdate' 	=> $insertData['createdate'],
	 'createdby' 	=> $insertData['createdby'],
	 'status' 		=> $insertData['status']
     );
	$this->db->insert('course_master', $course_master_data);
	$course_id = $this->db->insert_id();

	if($insertData['parent']){

	$course_to_subcourse_data = array(
        	'course_id' 	=>  $insertData['parent'],
        	'subcourse_id' 	=>  $course_id
        	);
	$this->db->insert('course_to_subCourse', $course_to_subcourse_data);
	}

	 return 'success';

	 }


	function getCourseByid ($conditions){

	 $this->db->where($conditions);


	 $this->db->from('course_master');
	 $this->db->join('course_type', 'course_type.id = course_master.type','left');
	 $this->db->join('status_master', 'status_master.id = course_master.status','left');
	 $this->db->join('course_to_subCourse', 'course_to_subCourse.subcourse_id = course_master.id','left');

	 $this->db->select('course_master.id,course_master.name, course_master.details, course_master.type, course_type.type as typename, course_master.status, status_master.status as statusname,course_to_subCourse.course_id');

	 $result=$this->db->get()->row();

	 return $result;
	}



	function editCourse($insertData=array())
	 {
	// print_r($insertData); exit;
	 // Update User table
		$course_master_data = array(
		 'name' 		=> $insertData['name'],
		 'details' 		=> $insertData['details'],
		 'type' 		=> $insertData['type'],


		 'modifiedby' 	=> $insertData['modifiedby'],
		 'status' 		=> $insertData['status']
		 );

	 $this->db->where('id',$insertData['id']);
	 $this->db->update('course_master',$course_master_data);

	 $course_id = $this->db->insert_id();

	 if($insertData['parent']){
	 	/*$this->db->delete('course_to_subCourse', array('subcourse_id' => $insertData['id']));

		 $course_to_subcourse_data = array(
				'course_id' 	=>  $insertData['parent'],
				'subcourse_id' 	=>  $insertData['id']
				);
		 $this->db->insert('course_to_subCourse', $course_to_subcourse_data);*/

		 $query = $this->db->get_where('course_to_subCourse', array('subcourse_id' => $insertData['id']))->row();

				 if($query){

				 $this->db->where('subcourse_id',$insertData['id']);
				 $this->db->update('course_to_subCourse',array(	'course_id' =>  $insertData['parent']));

				 }else{

				 $course_to_subcourse_data = array(
						'course_id' 	=>  $insertData['parent'],
						'subcourse_id' 	=>  $insertData['id']
						);
				 $this->db->insert('course_to_subCourse', $course_to_subcourse_data);

				 }
		 }else{

		  $this->db->delete('course_to_subCourse', array('subcourse_id' => $insertData['id']));

		  }
	  return 'success';
	 }

	 function deleteCourse($id)
	 {
	  $data = array('status' => '5');

	 $this->db->where('id',$id);
	 $this->db->update('course_master',$data);
	 return 'success';
	 }


	function Update_User_activation($insertData=array(),$id)
	 {
	   $this->db->where('id',$id);
	   $this->db->update('infra_user_master',$insertData);

	}


	 function getSendToEmail($type)
	{

		$query="SELECT emailid_master.`Email` FROM `mailtemplate`,`emailid_master` WHERE
emailid_master.`ID`=mailtemplate.`To` and mailtemplate.`MailType`='".$type."'";

		$result=$this->db->query($query);
		return($result->row());
	}


	function TempTop() {

 $top='<table width="963" border="2" cellspacing="0" cellpadding="0" align="center"       style="border-collapse:collapse; border-color:#5cd5f0">
		<tr>
		<td align="left" valign="top" style="padding:10px;">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="left" valign="top" style="background:#333;"><img src="'.base_url().'images/logo-small1.png" width="300" height="72" alt="" /></td></tr>
	</table>';

 return $top;

	             }

	function TempBody($username,$link,$linktext,$text) {


 $top='<table>
		<tr>
		<td align="left" valign="top" style="padding:10px;">

		<p>Hi '.$username.',</p>

		<p>'.$text.'</p>

		<p style="color:#0000CC"><a href="'.$link.'">"'.$linktext.'"</a></p>
		</td>
		</tr>
	</table>';

 return $top;

	             }


function TempBottom() {

 $bottom='<p>Thanks and Regards,</p>
              <p>Super Admin</p>
						<strong><p><a href="'.base_url().'">TripAdapt</a></p></strong></td>
						</tr>
						</table>
						</td>
						</tr>
						</table>';

                return $bottom;

	                  }








function SendCustomEmail($P_Type,$P_To,$id)
{


    if($P_Type=="CustomerMessageToAdmin")
	{

   $result2=$this->BrandInfo($id);
$result1=$this->EmailInfo($P_Type);




$from=$result1->Email;
$to=$P_To;
$subject=$result1->Subject;
$content=$result1->Message;
$body='<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						<td align="left" valign="top" style="font:normal 14px/20px Verdana, Geneva, sans-serif; color:#333; padding:20px 0 20px 0px;">
<p>Dear Administrator,</p>
<p>You have a new message from <font style="color:#0000ff">'.$result2->username.'</font></p>
						'.$content.'

';


	}





	 if($P_Type=="ChangePasswordToCustomer")
	{

   $result2=$this->BrandInfo($id);
$result1=$this->EmailInfo($P_Type);




$from=$result1->Email;
$to="sas.somnath@gmail.com";
$subject=$result1->Subject;
$content=$result1->Message;

$body='<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						<td align="left" valign="top" style="font:normal 14px/20px Verdana, Geneva, sans-serif; color:#333; padding:20px 0 20px 0px;">

						'.$content.'
						<p> Login Id:<font color="#0000ff">'.$result2->email.'</font> <br ></p>


				       <p> Password:<font color="#0000ff">'.$result2->passwrd.'</font></p>
					<strong>  </strong>

';


	}




if($P_Type=="NewQueryToAdmin"){

	$result5=$this->MsgInfoTo($id);
 $result4=$this->MsgInfo($id);

$result1=$this->EmailInfo($P_Type);

$from=$result1->Email;
$to=$P_To;
$subject=$result1->Subject;
$content=$result1->Message;
$body='<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						<td align="left" valign="top" style="font:normal 14px/20px Verdana,
<p>There is a new query from <font style="color:#0000ff">'.$result4['BrandName'].'</font> to <font style="color:#0000ff">'.$result5['BrandName'].'</font></p>
						'.$content.'';


	                                             }


	if($P_Type=="NewAdvertToAdmin"){


$result1=$this->EmailInfo($P_Type);

$from=$result1->Email;
$to=$P_To;
$subject=$result1->Subject;
$content=$result1->Message;
$body='<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						<td align="left" valign="top" style="font:normal 14px/20px Verdana, Geneva, sans-serif; color:#333; padding:20px 0 20px 0px;">
<p>Dear Administrator,</p>
<p>There is a new Advert in RangyBrands.</p>
						'.$content.'

';


	                                             }



$toid=$to;
$sub=$subject;
$fromid=$from;
$message=$this->TempTop().$body.$this->TempBottom();

				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= 'From: '.$fromid . "\r\n";

				$mail=mail($toid, $sub, $message, $headers);

return $mail;






}



	function SendMail($to,$from,$subject,$body) {

                //$to = $query->email;
				//$subject = "Plese Register First";
				//******body
				$message = "<html><head></head><body>";
				$message .= $this->TempTop();
				$message .= $body;
				$message .= $this->TempBottom();

				//$from = $this->config->item('site_admin_mail');

				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
				$headers .= "From:" . $from;
				mail($to,$subject,$message,$headers);

           }


    function CheckReset($conditions){

	$this->db->where($conditions);
	$this->db->from('infra_user_master');
	$this->db->select('*');

	 $result=$this->db->get()->row();

	 return $result;

	}

 function getSecurity_Credential($conditions){

	$this->db->where($conditions);
	$this->db->from('user_security_question');
	$this->db->select('*');

	 $result=$this->db->get()->row();

	 return $result;

	}

	function getCourseByIdAndDegree($courseId, $degreeId)
	{
		$this->db->select('courses.*, departments.name AS department_name, degree_master.name AS degree_name, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name, country_master.name AS country_name, universities.id AS university_id');
        $this->db->from('courses');
        $this->db->join('departments', 'departments.id = courses.department_id');
        $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
		$this->db->join('country_master', 'universities.country_id = country_master.id');
		$this->db->where('courses.id', $courseId);
		$this->db->where('courses.degree_id', $degreeId);
        $result=$this->db->get()->result();
		return $result;
	}

	function getCoursesByUniversityAndDegree($universityId, $degreeId)
	{
		$this->db->select('courses.*, departments.name AS department_name, degree_master.name AS degree_name, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name, country_master.name AS country_name, universities.id AS university_id');
        $this->db->from('courses');
        $this->db->join('departments', 'departments.id = courses.department_id');
        $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
		$this->db->join('country_master', 'universities.country_id = country_master.id');
		$this->db->where('universities.id', $universityId);
		$this->db->where('courses.degree_id', $degreeId);
        $result=$this->db->get()->result();
		return $result;
	}

    function getAllNewCourses()
    {
        $this->db->from('courses');
        $result=$this->db->get()->result();
		return $result;
    }

	function getCoursesByCategory($categoryId)
    {
		$this->db->select('courses.*, departments.name AS department_name, degree_master.name AS degree_name, colleges.name AS college_name');
        $this->db->from('courses');
        $this->db->join('departments', 'departments.id = courses.department_id');
        $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
		$this->db->join('colleges', 'colleges.id = departments.college_id');
		$this->db->where('courses.category_id', $categoryId);
        $result=$this->db->get()->result();
		return $result;
    }

	function getCoursesByCategoryAndDegree($categoryId, $degreeId, $collegeCategoryId, $selectedCountry, $sortBy)
    {

				$this->db->select('universities.*, departments.name AS department_name, degree_master.name AS degree_name, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name, country_master.name AS country_name, country_master.id AS country_id, universities.id AS university_id, colleges_categories.display_name AS mainstream, courses_categories.display_name AS substream, user_master.email AS email, user_master.image AS image, user_master.id AS login_id');
        $this->db->from('courses');
        $this->db->join('departments', 'departments.id = courses.department_id');
        $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
				$this->db->join('country_master', 'universities.country_id = country_master.id');
				$this->db->join('colleges_categories', 'colleges_categories.id = colleges.category_id');
				$this->db->join('courses_categories', 'courses_categories.id = courses.category_id');
				$this->db->join('user_master', 'universities.user_id = user_master.id');
				$this->db->where('courses.category_id', $categoryId);
				$this->db->where('courses.degree_id', $degreeId);
				$this->db->where('colleges_categories.id', $collegeCategoryId);
				if($selectedCountry){
						$this->db->where('country_master.id', $selectedCountry);
				}
				$this->db->order_by($sortBy);
				$this->db->limit('10');
        $result=$this->db->get()->result();


				if(!$result){

					$selectedCountryId = empty($selectedCountry) ? '5' : $selectedCountry;

					$this->db->select('universities.*, departments.name AS department_name, degree_master.name AS degree_name, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name, country_master.name AS country_name, country_master.id AS country_id, universities.id AS university_id, colleges_categories.display_name AS mainstream, courses_categories.display_name AS substream, user_master.email AS email, user_master.image AS image, user_master.id AS login_id');
	        $this->db->from('courses');
	        $this->db->join('departments', 'departments.id = courses.department_id');
	        $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
	        $this->db->join('colleges', 'colleges.id = departments.college_id');
	        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
	        $this->db->join('universities', 'universities.id = campuses.university_id');
					$this->db->join('country_master', 'universities.country_id = country_master.id');
					$this->db->join('colleges_categories', 'colleges_categories.id = colleges.category_id');
					$this->db->join('courses_categories', 'courses_categories.id = courses.category_id');
					$this->db->join('user_master', 'universities.user_id = user_master.id');
					$this->db->where('country_master.id', '5');
					$this->db->order_by($sortBy);
	        $result=$this->db->get()->result();


				}

		return $result;

    }

	function getAllResearches()
    {
        $this->db->from('researches');
        $this->db->join('departments', 'departments.id = researches.department_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
				$this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->select('researches.*, departments.name AS department_name, colleges.name AS college_name, universities.name AS university_name, universities.banner AS banner');
        $result=$this->db->get()->result_array();
		return $result;
    }

	function getResearchesBySearch($str)
    {
        $query_string = "SELECT MATCH(researches.topic, researches.description) AGAINST('" . $str . "*' IN BOOLEAN MODE) AS relevance, `researches`.*, `departments`.`name` AS department_name, `colleges`.`name` AS college_name, `universities`.`name` AS university_name FROM (`researches`) JOIN `departments` ON `departments`.`id` = `researches`.`department_id` JOIN `colleges` ON `colleges`.`id` = `departments`.`college_id` JOIN `campuses` ON `campuses`.`id` = `colleges`.`campus_id` JOIN `universities` ON `universities`.`id` = `campuses`.`university_id` WHERE MATCH (researches.topic, researches.description) AGAINST ('" . $str . "*' IN BOOLEAN MODE) ORDER BY relevance DESC";

		$query = $this->db->query($query_string);
		$result = $query->result();
		return $result;
    }

	function getCourseCategoriesByCollegeCategory($collegeCategoryId)
	{
	   $this->db->from('courses_categories');
	   $this->db->where_in('college_category_id', $collegeCategoryId);
	   $this->db->select('*');
	   $result=$this->db->get()->result();
	   return $result;

	}

	function getSimilarUniversitiesByCourse($courseName)
	{
		$this->db->select('courses.name AS course_name, universities.id AS university_id, universities.name AS university_name');
        $this->db->from('courses');
        $this->db->join('departments', 'departments.id = courses.department_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
		$this->db->where('courses.name', $courseName);
		$this->db->group_by('universities.id');
        $result=$this->db->get()->result_array();
		return $result;
	}

	function getComparedUniversitiesByCourse($courseName, $universityIds)
	{
		$this->db->select('courses.*, universities.id AS university_id, universities.name AS university_name, degree_master.name AS degree_name, colleges.name AS college_name, colleges.id AS college_id, universities.user_id AS university_login_id');
        $this->db->from('courses');
		$this->db->join('degree_master', 'degree_master.id = courses.degree_id');
        $this->db->join('departments', 'departments.id = courses.department_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
		$this->db->where_in('universities.id', $universityIds);
		$this->db->where('courses.name', $courseName);
		$this->db->order_by('universities.id', 'ASC');
        $result=$this->db->get()->result_array();
		return $result;
	}

	function getComparedUniversitiesCountByCourse($courseName, $universityIds)
	{
		$this->db->select('count(universities.id) AS count, universities.id AS university_id');
        $this->db->from('courses');
        $this->db->join('departments', 'departments.id = courses.department_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
		$this->db->where_in('universities.id', $universityIds);
		$this->db->where('courses.name', $courseName);
		$this->db->group_by('universities.id');
        $result=$this->db->get()->result_array();
		return $result;
	}

	function getComparedCollegesCountByCourse($courseName, $universityIds)
	{
		$this->db->select('count(colleges.id) AS count, colleges.id AS college_id');
        $this->db->from('courses');
        $this->db->join('departments', 'departments.id = courses.department_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
		$this->db->where_in('universities.id', $universityIds);
		$this->db->where('courses.name', $courseName);
		$this->db->group_by('colleges.id');
        $result=$this->db->get()->result_array();
		return $result;
	}

	function getCourseDetailById($courseId)
    {
		$this->db->select('courses.*, departments.name AS department_name, degree_master.name AS degree_name');
        $this->db->from('courses');
        $this->db->join('departments', 'departments.id = courses.department_id');
        $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
		$this->db->where('courses.id', $courseId);
        $result=$this->db->get()->result_array();
		return $result;
    }

	function getAllDegrees()
	{
	   $this->db->from('degree_master');
	   $this->db->select('*');
	   $result=$this->db->get()->result();
	   return $result;

	}

	function courseDetail($courseId) {
		$this->db->select('courses.*, departments.name AS department_name, degree_master.name AS degree_name, universities.id AS uni_id, universities.about AS uni_about, universities.name AS uni_name, universities.mandatory_fields AS uni_mandatory_fields, universities.consent_data AS uni_consent_data');
        $this->db->from('courses');
        $this->db->join('departments', 'departments.id = courses.department_id');
				$this->db->join('colleges', 'departments.college_id = colleges.id');
				$this->db->join('campuses', 'colleges.campus_id = campuses.id');
				$this->db->join('universities', 'campuses.university_id = universities.id');
        $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
		$this->db->where('courses.id', $courseId);
        $result=$this->db->get()->result_array();
		return $result;
    }

		function relatedCourseDetail($courseId,$departmentsId) {

			$query1 = "SELECT courses.*, universities.name as university_name, universities.banner as university_banner, departments.name AS department_name, degree_master.name AS degree_name,
									country_master.name AS country_name, user_master.email AS email, user_master.image AS image, user_master.id AS login_id
									FROM universities
									JOIN country_master ON  universities.country_id = country_master.id
									JOIN user_master ON universities.user_id = user_master.id
									JOIN campuses ON campuses.university_id = universities.id
									JOIN colleges ON colleges.campus_id = campuses.id
									JOIN departments ON departments.college_id = colleges.id
									JOIN courses ON courses.department_id = departments.id
									JOIN degree_master ON courses.degree_id = degree_master.id
									WHERE courses.department_id = $departmentsId
									AND courses.id != $courseId
									AND courses.status = 1
									limit 4;";

			$query = $this->db->query($query1);

			$result_data = $query->result_array();

			return $result_data;
	    }

		function getCourseByIdNew($courseId) {
			$this->db->from('courses');
			$this->db->where('id', $courseId);
			$result = $this->db->get();
			return $result->result_array();
		}

		function getResearchByIdNew($researchId) {
			$this->db->from('researches');
			$this->db->where('id', $researchId);
			$result = $this->db->get();
			return $result->result_array();
		}

		function getDetailResearches($researchId) {

	        $this->db->from('researches');
	        $this->db->join('departments', 'departments.id = researches.department_id');
	        $this->db->join('colleges', 'colleges.id = departments.college_id');
	        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
					$this->db->join('universities', 'universities.id = campuses.university_id');
					$this->db->where('researches.id', $researchId);
	        $this->db->select('researches.*, departments.name AS department_name, colleges.name AS college_name, universities.name AS university_name, universities.banner AS banner, universities.consent_data AS uni_consent_data');

					$result=$this->db->get();

			return $result->result_array();
	    }

}













?>
