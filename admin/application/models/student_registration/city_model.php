<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com

  <Reverse bidding system>
    Copyright (C) <2009>  <FreeLance PHP Script>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
    If you want more information, please email me at support@freelancephpscript.com or
    contact us from http://www.freelancephpscript.com/contact


 */
	 class City_model extends CI_Model {

 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }





	function getCities($conditions1,$conditions2)
     {
	  if(count($conditions1)>0 || count($conditions2)>0  || isset($conditions1) || isset($conditions2)){
	  $this->db->where($conditions1);
	  $this->db->or_where($conditions2);
	  }

	   $this->db->from('infra_city_master');
	   $this->db->join('infra_state_master', 'infra_city_master.state_id = infra_state_master.zone_id','left');
	   $this->db->join('infra_status_master', 'infra_status_master.id = infra_city_master.status','left');
       $this->db->select('infra_city_master.id,infra_city_master.city_name,infra_city_master.status,infra_state_master.zone_id as state_id,infra_state_master.name,infra_state_master.code,infra_state_master.status as state_status,infra_status_master.name as statusname');

	   $result = $this->db->get()->result();
       return $result;
    }


	function getCitiesByState($state_id)
     {
	  $this->db->from('infra_city_master');
	  $this->db->where('state_id', $state_id);
      $result = $this->db->get()->result();
	  return $result;
    }

	  function getCity($id)
	 {

	 	$conditions = array('id'=> $id);
	 	$this->db->where($conditions);
	 	$this->db->select('*');
		$query = $this->db->get('infra_city_master');
		$row   = $query->row();
		return $row;
	 }


	 function getCityDetailsBy_Id($conditions)
	 {


	 	$this->db->where($conditions);
	 	$this->db->from('infra_city_master');
	   $this->db->join('infra_state_master', 'infra_city_master.state_id = infra_state_master.zone_id','left');
	   $this->db->join('infra_status_master', 'infra_status_master.id = infra_city_master.status','left');
       $this->db->select('infra_city_master.id,infra_city_master.city_name,infra_city_master.status,infra_state_master.zone_id as state_id,infra_state_master.name,infra_state_master.code,infra_state_master.status as state_status,infra_status_master.name as statusname');


	   $result = $this->db->get()->result();
       return $result;
	 }




	 function insertCity($insertData=array())
	 {
	 $this->db->insert('infra_city_master', $insertData);
	 return 'success';
	 }


	 function editCity($insertData=array())
	 {
	 $this->db->where('id',$insertData['id']);
	 $this->db->update('infra_city_master',$insertData);



	 return 'success';
	 }


	 function deleteCity($id)
	 {
	 $this->db->delete('infra_city_master', array('id' => $id));

	 return 'success';
	 }

	 function getAllCities()
	 {
		 $conditions = array('status'=> '1');
		 $this->db->where($conditions);
		 $this->db->select('*');
		 $this->db->from('city_master');
		 $result = $this->db->get()->result();
		 return $result;
	 }



}













?>
