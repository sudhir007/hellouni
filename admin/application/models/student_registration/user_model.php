<?php
/**
 * Reverse bidding system User_model Class *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com

 */

class User_model extends CI_Model
{
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function getUser($condition1, $condition2)
	{
		if (count($condition1) > 0 || count($condition2) > 0  || isset($condition1) || isset($condition2)) {
			$this->db->where($condition1);
			$this->db->where($condition2);
		}

		$this->db->from('user_master');
		$this->db->join('type_master', 'type_master.type_id = user_master.type', 'left');
		$this->db->join('status_master', 'status_master.id = user_master.status', 'left');

		$this->db->select('user_master.id,user_master.name,user_master.email,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,status_master.status as status_name');

		$result = $this->db->get()->result();
		return $result;
	}

	function checkUser($username, $password, $userStatus, $userType = [])
	{
		$this->db->select('user_master.id,user_master.name,user_master.email,user_master.image,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,status_master.status as status_name,user_master.username,user_master.createdate');
		$this->db->from('user_master');
		$this->db->join('type_master', 'type_master.type_id = user_master.type', 'left');
		$this->db->join('status_master', 'status_master.id = user_master.status', 'left');
		$this->db->where('user_master.password', $password);
		$this->db->where('user_master.status', $userStatus);
		if ($userType) {
			$this->db->where_in('user_master.type', $userType);
		}
		$this->db->where("(user_master.email = '$username' OR user_master.phone = '$username' OR user_master.username = '$username')");


		$result = $this->db->get()->row();
		return $result;
	}

	function checkUserEmail($conditions = array())
	{
		//echo 'aaaaaa'; die('----');
		$this->db->select('user_master.id,user_master.name,user_master.email,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,status_master.status as status_name');
		$this->db->from('user_master');
		$this->db->join('type_master', 'type_master.type_id = user_master.type', 'left');
		$this->db->join('status_master', 'status_master.id = user_master.status', 'left');
		$this->db->where($conditions);

		$result = $this->db->get()->row();
		return $result;
	}

	function getStudentDetails($conditions = array())
	{
		//echo 'aaaaaa'; die('----');
		$this->db->where($conditions);
		$this->db->from('user_master');
		$this->db->join('type_master', 'type_master.type_id = user_master.type', 'left');
		$this->db->join('status_master', 'status_master.id = user_master.status', 'left');
		$this->db->join('student_details', 'student_details.user_id = user_master.id', 'left');

		$this->db->select('user_master.id,user_master.name,user_master.last_name,user_master.email,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,user_master.fid,status_master.status as status_name,student_details.address,student_details.country,student_details.city,student_details.zip,student_details.dob,student_details.phone,student_details.parent_name,student_details.parent_mob,student_details.parent_email,student_details.parent_occupation,student_details.parent_relation,student_details.hear_about_us,student_details.passport,student_details.desired_destination,student_details.id as student_id,student_details.other_details as student_other_details, student_details.facilitator_id AS counselor_id, student_details.intake_month, student_details.intake_year, student_details.passport_number, student_details.gender, student_details.passport_issue_date, student_details.passport_expiry_date, student_details.marital_status, student_details.place_of_birth, student_details.correspondence_address, student_details.state,
		student_details.company_name, student_details.designation, student_details.work_start_date, student_details.work_end_date, student_details.e_name, student_details.e_mobile_number, student_details.e_email, student_details.e_relation');

		$result = $this->db->get()->row();
		return $result;
	}

	function getLead($conditions = array())
	{
		$this->db->where($conditions);

		$this->db->from('lead_master');
		$this->db->select('lead_master.id,lead_master.name,lead_master.email,lead_master.phone,lead_master.mainstream,lead_master.courses,lead_master.degree,lead_master.countries');
		$this->db->order_by("id", "desc");
		$result = $this->db->get()->row();
		return $result;
	}

	function checkUserBy_Email($conditions = array())
	{
		$this->db->where($conditions);
		$this->db->from('infra_email_master');
		$this->db->join('infra_user_master', 'infra_email_master.id = infra_user_master.email', 'left');
		$this->db->join('infra_role_master', 'infra_role_master.id = infra_user_master.role_id', 'left');
		$this->db->join('infra_status_master', 'infra_status_master.id = infra_user_master.status_id', 'left');

		$this->db->select('infra_user_master.id,infra_user_master.name,infra_user_master.user_name,infra_role_master.name as role,infra_email_master.email,infra_user_master.status_id,infra_status_master.name as statusname');

		$result = $this->db->get()->row();
		return $result;
	}

	function getUniversityDetailsByid($conditions)
	{
		$this->db->where($conditions);
		$this->db->from('university_details');

		/*$this->db->join('user_master', 'user_master.id = university_details.university_id','left');
	 $this->db->join('status_master', 'status_master.id = user_master.status','left');*/

		$this->db->select('university_details.university_id,university_details.about,university_details.website,university_details.banner,university_details.student,university_details.internationalstudent,university_details.facilities,university_details.address,university_details.country,university_details.state,university_details.city');

		$result = $this->db->get()->row();
		return $result;
	}

	function getuniversity_to_degree_to_course($conditions)
	{
		$this->db->where($conditions);
		$this->db->from('university_to_degree_to_course');

		$this->db->select('university_to_degree_to_course.id,university_to_degree_to_course.degree_id,university_to_degree_to_course.course_id,university_to_degree_to_course.university_id,university_to_degree_to_course.language,university_to_degree_to_course.admissionsemester,university_to_degree_to_course.beginning,university_to_degree_to_course.duration,university_to_degree_to_course.application_deadline,university_to_degree_to_course.details,university_to_degree_to_course.tutionfee,university_to_degree_to_course.enrolmentfee,university_to_degree_to_course.livingcost,university_to_degree_to_course.jobopportunities,university_to_degree_to_course.required_language,university_to_degree_to_course.academicrequirement,university_to_degree_to_course.academicrequirement');

		$result = $this->db->get()->row();
		return $result;
	}

	function checkFavourites($conditions = array())
	{
		//echo 'aaaaaa'; die('----');
		$this->db->where($conditions);
		$this->db->from('favourites');

		/*$this->db->join('user_master', 'user_master.id = favourites.user_id','left');
		$this->db->join('status_master', 'status_master.id = user_master.status','left');*/

		$this->db->select('favourites.id,favourites.user_id,favourites.university_id,favourites.course');
		$result = $this->db->get()->row();
		return $result;
	}

	function commonCheck($conditions = array(), $tableName)
	{
		$this->db->where($conditions);
		$this->db->from($tableName);
		$this->db->select('id');
		$result = $this->db->get()->row();
		return $result;
	}

	function getFavourites($conditions = array())
	{
		//echo 'aaaaaa'; die('----');
		$this->db->where($conditions);
		$this->db->from('favourites');
		$this->db->join('universities', 'universities.id = favourites.university_id');
		$this->db->join('user_master', 'user_master.id = universities.user_id');
		$this->db->join('country_master', 'country_master.id = universities.country_id');
		$this->db->join('courses_categories', 'courses_categories.id = favourites.course');
		$this->db->join('degree_master', 'degree_master.id = favourites.degree');

		$this->db->select('favourites.id,favourites.user_id,favourites.university_id as uni_id,favourites.course,user_master.name,user_master.image,country_master.name as countryname,courses_categories.name as coursename,degree_master.name as degreename,universities.banner,degree_master.id as degree_id, user_master.id as university_login_id,universities.slug as slug, universities.website as website,user_master.email as email,favourites.createdate as confirmed_slot,universities.university_view_count,universities.university_likes_count,universities.university_fav_count');

		$result = $this->db->get()->result_array();
		return $result;
	}

	function insertFavourites($data = array())
	{
		$this->db->insert('favourites', $data);
		return $favouritesid = $this->db->insert_id();
	}

	function deleteFavourites($condition)
	{

		$this->db->where($condition);
		if ($this->db->delete('favourites'))
			return true;
	}

	function insertCommon($data = array(), $tableName)
	{
		$this->db->insert($tableName, $data);
		return $favouritesid = $this->db->insert_id();
	}

	function deleteCommon($condition, $tableName)
	{
		$this->db->where($condition);
		if ($this->db->delete($tableName))
			return true;
	}

	function addRemerberme($insertData = array(), $expire)
	{
		$this->auth_model->setUserCookie('uname', $insertData['username'], $expire);
		$this->auth_model->setUserCookie('pwd', $insertData['password'], $expire);

		// echo $val=get_cookie('uname',TRUE); exit;
		echo $this->input->cookie('uname', TRUE);
		exit;
		//die();

	}

	function removeRemeberme()
	{
		$this->auth_model->clearUserCookie(array('uname', 'pwd'));
	}

	function insertUser($insertData = array())
	{
		$user_master_data = array(
			'name' 		=> $insertData['name'],
			'email' 		=> $insertData['email'],
			'password' 	=> $insertData['password'],
			'type' 		=> $insertData['type'],
			'createdate' 	=> $insertData['createdate'],
			'status' 		=> $insertData['status'],
			'notification' => $insertData['notification']

		);

		$this->db->insert('user_master', $user_master_data);
		$user_id = $this->db->insert_id();
		$user_permission_course_data = array(
			'user_id' 	=> $user_id,
			'view' 	=> $insertData['view'],
			'create' 	=> $insertData['create'],
			'edit' 	=> $insertData['edit'],
			'del' 		=> $insertData['del']

		);
		$this->db->insert('user_permission_course', $user_permission_course_data);
		return 'success';
	}

	function insertStudent($insertData = array())
	{
		$user_master_data = array(
			'name' 		=> $insertData['name'],
			'email' 		=> $insertData['email'],
			'password' 	=> $insertData['password'],
			'image' 		=> $insertData['image'],
			'type' 		=> $insertData['type'],
			'createdate' 	=> $insertData['createdate'],
			'status' 		=> $insertData['status'],
			'notification' => '0',
			'flag' 		=> '0',
			'activation' 	=> $insertData['activation'],
			'fid' 	=> $insertData['activation']

		);

		$this->db->insert('user_master', $user_master_data);
		$user_id = $this->db->insert_id();

		$studentDetails_data = array(
			'user_id' 	=> $user_id,
			'address' 	=> $insertData['address'],
			'country' 	=> $insertData['country'],
			'city' 	=> $insertData['city'],
			'zip' 		=> $insertData['zip'],
			'dob' 		=> $insertData['dob'],
			'phone' 	=> $insertData['phone']
		);
		$this->db->insert('student_details', $studentDetails_data);
		return $user_id;
	}

	function insertUserMaster($insertData = array())
	{
		$this->db->insert('user_master', $insertData);
		//echo $this->db->last_query();
		$user_id = $this->db->insert_id();
		return $user_id;
	}

	function insertStudentDetails($insertData = array())
	{
		$this->db->insert('student_details', $insertData);
		return $this->db->insert_id();
	}

	function insertDesireDetails($insertData = array())
	{
		$this->db->insert('desired_courses', $insertData);
		return $this->db->insert_id();
	}

	function insertLead($insertData = array())
	{
		$this->db->insert('lead_master', $insertData);
		$lead_id = $this->db->insert_id();
		return $lead_id;
	}

	function updateUserInfo($insertData = array(), $id)
	{
		$this->db->where('id', $id);
		$this->db->update('user_master', $insertData);
		return true;
	}

	function updateStudentDetails($insertData = array(), $id)
	{
		$this->db->where('user_id', $id);
		$this->db->update('student_details', $insertData);
		return true;
	}

	//select single row from tablename
	function getSingleData($tablename, $condition)
	{
		$this->db->where($condition);
		return $this->db->get($tablename)->row();
	}

	// Get multiple data
	function get_data($table, $con = '', $order = '', $limit = '', $group = '')
	{
		if ($con != '')
			$this->db->where($con);
		if ($order != '')
			$this->db->order_by($order);
		if ($limit != '')
			$this->db->limit($limit);
		if ($group != '')
			$this->db->group_by($group);
		return $this->db->get($table)->result();
	}

	// Delete data
	function delete($tablename, $condition)
	{
		$this->db->where($condition);
		$this->db->delete($tablename);
	}

	// insert and update
	function save($table, $data, $con = '')
	{
		if ($con != '') {
			$this->db->where($con);
			$this->db->update($table, $data);
		} else {
			$this->db->insert($table, $data);
			return $this->db->insert_id();
		}
	}

	function insertStudentWorkDetails($insertData = array())
	{
		$this->db->insert('student_work_exp', $insertData);
		/*$this->db->where('user_id',$id);
			 $this->db->update('student_work_exp',$insertData);*/
		return true;
	}

	function updateLeadInfo($insertData = array(), $id)
	{
		$this->db->where('id', $id);
		$this->db->update('lead_master', $insertData);
		return true;
	}

	function getUserByid($conditions)
	{
		$this->db->where($conditions);
		$this->db->from('user_master');
		$this->db->join('type_master', 'type_master.type_id = user_master.type', 'left');
		$this->db->join('status_master', 'status_master.id = user_master.status', 'left');
		$this->db->join('user_permission_course', 'user_permission_course.user_id = user_master.id', 'left');

		$this->db->select('user_master.id,user_master.name,user_master.email,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,status_master.status as status_name,user_master.notification,user_permission_course.view,user_permission_course.create,user_permission_course.edit,user_permission_course.del,user_master.username');

		$result = $this->db->get()->row();
		return $result;
	}

	function getUniversityByid($conditions)
	{
		$this->db->where($conditions);
		$this->db->from('user_master');
		$this->db->join('university_details', 'university_details.university_id = user_master.id', 'left');
		$this->db->join('country_master', 'country_master.id = university_details.country', 'left');
		$this->db->join('status_master', 'status_master.id = user_master.status', 'left');
		$this->db->select('user_master.id,user_master.name,user_master.email,user_master.type,university_details.about,university_details.country,country_master.code,country_master.name as country_name');
		$result = $this->db->get()->row();
		//echo $this->db->last_query();
		return $result;
	}

	function editUser($insertData = array())
	{
		// Update User table
		$user_master_data = array(
			'name' 		=> $insertData['name'],
			'email' 		=> $insertData['email'],
			'password' 	=> $insertData['password'],
			'type' 		=> $insertData['type'],
			'status' 		=> $insertData['status'],
			'notification' => $insertData['notification']
		);

		$this->db->where('id', $insertData['id']);
		$this->db->update('user_master', $user_master_data);
		$this->db->from('user_permission_course');
		$this->db->where('user_id', $insertData['id']);
		$this->db->select('*');
		$is_user_exists = $this->db->get()->row();

		//print_r($is_user_exists); exit;
		// $is_user_exists = $this->db->get_where('user_permission_course', array('user_permission_course.user_id' => $insertData['id']))->row();

		if ($is_user_exists) {
			$user_permission_course_data = array(
				'view' 	=> $insertData['view'],
				'create' 	=> $insertData['create'],
				'edit' 	=> $insertData['edit'],
				'del' 		=> $insertData['del']
			);
			$this->db->where('user_id', $insertData['id']);
			$this->db->update('user_permission_course', $user_permission_course_data);
		} else {
			$user_permission_course_data = array(
				'user_id'	=> $insertData['id'],
				'view' 	=> $insertData['view'],
				'create' 	=> $insertData['create'],
				'edit' 	=> $insertData['edit'],
				'del' 		=> $insertData['del']
			);
			$this->db->insert('user_permission_course', $user_permission_course_data);
		}
		return 'success';
	}

	function deleteUser($id)
	{
		$data = array('status' => '5');
		$this->db->where('id', $id);
		$this->db->update('user_master', $data);
		return 'success';
	}

	function deleteTrash($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('user_master');
		return 'success';
	}

	function checkFundDetails($conditions = array())
	{
		//echo 'aaaaaa'; die('----');
		$this->db->where($conditions);
		$this->db->from('student_fund_info');
		$this->db->select('*');
		$result = $this->db->get()->row();
		return $result;
	}

	function updateFundDetails($insertData = array(), $id)
	{
		$this->db->where('user_id', $id);
		$this->db->update('student_fund_info', $insertData);
		return true;
	}

	function insertFundDetails($data = array())
	{
		$this->db->insert('student_fund_info', $data);
		return $desired_courses_id = $this->db->insert_id();
	}

	function deleteFundDetails($user_id)
	{
		$this->db->where('user_id', $user_id);
		if ($this->db->delete('student_fund_info'))
			return true;
	}

	function checkDesiredCourse($conditions = array())
	{
		//echo 'aaaaaa'; die('----');
		$this->db->where($conditions);
		$this->db->from('desired_courses');
		$this->db->join('user_master', 'user_master.id = desired_courses.student_id', 'left');

		/*$this->db->select('desired_courses.id,desired_courses.student_id,desired_courses.desired_course_name,desired_courses.	desired_course_level,desired_courses.desired_course_intake,desired_courses.desired_course_year');*/

		$this->db->select('*');
		$result = $this->db->get()->row();
		return $result;
	}

	function updateDesiredCourse($insertData = array(), $id)
	{
		$this->db->where('student_id', $id);
		$this->db->update('desired_courses', $insertData);
		return true;
	}

	function insertDesiredCourse($data = array())
	{
		$this->db->insert('desired_courses', $data);
		return $desired_courses_id = $this->db->insert_id();
	}

	function deleteDesiredCourse($student_id)
	{
		$this->db->where('student_id', $student_id);
		if ($this->db->delete('desired_courses'))
			return true;
	}

	function checkSecondaryQualification($conditions = array())
	{
		$this->db->where($conditions);
		$this->db->from('secondary_qualification');
		$this->db->join('user_master', 'user_master.id = secondary_qualification.student_id', 'left');
		$this->db->select('*');
		$result = $this->db->get()->row();
		return $result;
	}

	function updateSecondaryQualification($insertData = array(), $id)
	{
		$this->db->where('student_id', $id);
		$this->db->update('secondary_qualification', $insertData);
		return true;
	}

	function insertSecondaryQualification($data = array())
	{
		$this->db->insert('secondary_qualification', $data);
		return $secondary_qualification_id = $this->db->insert_id();
	}

	function deleteSecondaryQualification($student_id)
	{
		$this->db->where('student_id', $student_id);
		if ($this->db->delete('secondary_qualification'))
			return true;
	}

	function checkDiplomaQualification($conditions = array())
	{
		$this->db->where($conditions);
		$this->db->from('diploma_qualification');
		$this->db->join('user_master', 'user_master.id = diploma_qualification.student_id', 'left');
		$this->db->select('*');
		$result = $this->db->get()->row();
		return $result;
	}

	function updateDiplomaQualification($insertData = array(), $id)
	{
		$this->db->where('student_id', $id);
		$this->db->update('diploma_qualification', $insertData);
		return true;
	}

	function insertDiplomaQualification($data = array())
	{
		$this->db->insert('diploma_qualification', $data);
		return $diploma_qualification_id = $this->db->insert_id();
	}

	function deleteDiplomaQualification($student_id)
	{
		$this->db->where('student_id', $student_id);
		if ($this->db->delete('diploma_qualification'))
			return true;
	}

	function checkHigher_secondary_qualification($conditions = array())
	{
		$this->db->where($conditions);
		$this->db->from('higher_secondary_qualification');
		$this->db->join('user_master', 'user_master.id = higher_secondary_qualification.student_id', 'left');
		$this->db->select('*');
		$result = $this->db->get()->row();
		return $result;
	}

	function updateHigher_secondary_qualification($insertData = array(), $id)
	{
		$this->db->where('student_id', $id);
		$this->db->update('higher_secondary_qualification', $insertData);
		return true;
	}

	function insertHigher_secondary_qualification($data = array())
	{
		$this->db->insert('higher_secondary_qualification', $data);
		return $higher_secondary_qualification_id = $this->db->insert_id();
	}

	function deleteHigher_secondary_qualification($student_id)
	{
		$this->db->where('student_id', $student_id);
		if ($this->db->delete('higher_secondary_qualification'))
			return true;
	}

	// Bachelor
	function checkBachelorQualification($conditions = array())
	{
		$this->db->where($conditions);
		$this->db->from('bachelor_qualification');
		$this->db->join('user_master', 'user_master.id = bachelor_qualification.student_id', 'left');
		$this->db->select('*');
		$result = $this->db->get()->row();
		return $result;
	}

	function updateBachelorQualification($insertData = array(), $id)
	{
		$this->db->where('student_id', $id);
		$this->db->update('bachelor_qualification', $insertData);
		return true;
	}

	function insertBachelorQualification($data = array())
	{
		$this->db->insert('bachelor_qualification', $data);
		return $secondary_qualification_id = $this->db->insert_id();
	}

	function deleteBachelorQualification($student_id)
	{
		$this->db->where('student_id', $student_id);
		if ($this->db->delete('bachelor_qualification'))
			return true;
	}

	// Master
	function checkMasterQualification($conditions = array()) {
		$this->db->where($conditions);
		$this->db->from('master_qualification');
		$this->db->join('user_master', 'user_master.id = master_qualification.student_id', 'left');
		$this->db->select('*');
		$result = $this->db->get()->row();
		return $result;
	}

	function updateMasterQualification($insertData = array(), $id) {
		$this->db->where('student_id', $id);
		$this->db->update('master_qualification', $insertData);
		return true;
	}

	function insertMasterQualification($data = array())	{
		$this->db->insert('master_qualification', $data);
		return $secondary_qualification_id = $this->db->insert_id();
	}

	function deleteMasterQualification($student_id)
	{
		$this->db->where('student_id', $student_id);
		if ($this->db->delete('master_qualification'))
			return true;
	}

	function getUniversityDataByid($universityId, $courseCategoryId)
	{
		$this->db->select('universities.*, country_master.name AS country_name, user_master.email AS email, user_master.image AS image, user_master.id AS login_id, degree_master.name AS degree, colleges.name AS college, courses.id AS course_id,courses.name AS course, campuses.name AS campus, courses.eligibility_gpa AS eligibility_gpa, courses.eligibility_gre AS eligibility_gre, courses.eligibility_toefl AS eligibility_toefl, courses.gpa_scale AS gpa_scale, courses.eligibility_ielts AS eligibility_ielts, courses.course_view_count, courses.course_likes_count, courses.course_fav_count, courses.start_months AS start_months, courses.application_fee_amount AS application_fee_course, courses.no_of_month_work_permit AS stay_back_option, courses.expected_salary AS eligibility_salary, courses.duration AS course_duration, courses.deadline_link AS deadline_link, courses.total_fees AS total_fees, colleges.id AS college_id, colleges.total_enrollment AS college_total_enrollment, colleges.application_deadline_usa AS college_application_deadline_usa, colleges.application_deadline_international AS college_application_deadline_international, colleges.application_fees_usa AS college_application_fees_usa, colleges.application_fees_international AS college_application_fees_international, colleges.average_gre AS college_average_gre, colleges.admission_url AS college_admission_url, departments.id AS department_id, departments.total_enrollment AS department_total_enrollment, departments.application_deadline_usa AS department_application_deadline_usa, departments.application_deadline_international AS department_application_deadline_international, departments.application_fees_usa AS department_application_fees_usa, departments.application_fees_international AS department_application_fees_international, departments.admissions_department_url AS department_admissions_department_url, departments.average_gre AS department_average_gre, departments.name AS department_name, colleges.establishment_date AS college_establishment_date, colleges.ranking AS college_ranking, colleges.college_research_expenditure AS college_research_expenditure, colleges.research_expenditure_per_faculty AS college_research_expenditure_per_faculty, colleges.acceptance_rate AS college_acceptance_rate, country_master.currency AS currency,universities.whatsapp_link AS group_url');
		$this->db->from('universities', 'universities.id = campuses.university_id');
		$this->db->join('country_master', 'universities.country_id = country_master.id');
		$this->db->join('user_master', 'universities.user_id = user_master.id');
		$this->db->join('campuses', 'campuses.university_id = universities.id');
		$this->db->join('colleges', 'colleges.campus_id = campuses.id');
		$this->db->join('departments', 'departments.college_id = colleges.id');
		$this->db->join('courses', 'courses.department_id = departments.id');
		$this->db->join('degree_master', 'courses.degree_id = degree_master.id');
		//$this->db->join('universities_groups', 'universities_groups.university_id = universities.id', 'left');
		$this->db->where('universities.id', $universityId);
		if ($courseCategoryId) {
			$this->db->where('courses.category_id', $courseCategoryId);
		}
		$result = $this->db->get()->result_array();
		return $result;
	}

	function getTrendingDisciplinesByUID($universityId)
	{
		$query1 = "SELECT cc.id, cc.display_name AS name, count(cu.id) AS count, cc.image_path, cc.icon_img, u.id AS university_id
										FROM universities u
										JOIN campuses c ON c.university_id = u.id
										JOIN colleges cl ON cl.campus_id = c.id
										JOIN departments d ON d.college_id = cl.id
										JOIN courses cu ON cu.department_id = d.id
										JOIN colleges_categories cc ON cc.id = cl.category_id
										where u.id = $universityId
										GROUP BY cl.category_id;";

		$query = $this->db->query($query1);
		$result_data = $query->result_array();
		return $result_data;
	}

	function getCourseByCatIdUID($catId, $uId)
	{
		$query1 = "SELECT courses.*, universities.name as university_name, universities.banner as university_banner, departments.name AS department_name, degree_master.name AS degree_name,
										country_master.name AS country_name, user_master.email AS email, user_master.image AS image, user_master.id AS login_id
										FROM universities
										JOIN country_master ON  universities.country_id = country_master.id
										JOIN user_master ON universities.user_id = user_master.id
										JOIN campuses ON campuses.university_id = universities.id
										JOIN colleges ON colleges.campus_id = campuses.id
										JOIN departments ON departments.college_id = colleges.id
										JOIN courses ON courses.department_id = departments.id
										JOIN degree_master ON courses.degree_id = degree_master.id
										WHERE universities.id = $uId
										AND colleges.category_id = $catId
										AND universities.active = 1
										AND courses.status = 1;";

		$query = $this->db->query($query1);
		$result_data = $query->result_array();
		return $result_data;
	}

	function getAvgFeesByUID($uId)
	{
		$query1 = "SELECT avg(courses.total_fees) AS tution_fees,
										avg(courses.cost_of_living_year) AS cost_of_living_fees,
										avg(courses.application_fee_amount) AS application_fees
										FROM universities
										JOIN country_master ON  universities.country_id = country_master.id
										JOIN user_master ON universities.user_id = user_master.id
										JOIN campuses ON campuses.university_id = universities.id
										JOIN colleges ON colleges.campus_id = campuses.id
										JOIN departments ON departments.college_id = colleges.id
										JOIN courses ON courses.department_id = departments.id
										JOIN degree_master ON courses.degree_id = degree_master.id
										WHERE universities.id = $uId
										AND universities.active = 1
										AND courses.status = 1;";

		$query = $this->db->query($query1);
		$result_data = $query->result_array();
		return $result_data;
	}

	function addSlot($slotData)
	{
		$this->db->insert('slots', $slotData);
		return $this->db->insert_id();
	}

	function getSlotByUserAndUniversity($userId, $universityId, $slotType = 'UNIVERSITY')
	{
		$this->db->from('slots');
		$this->db->where('user_id', $userId);
		$this->db->where('university_id', $universityId);
		$this->db->where('slot_type', $slotType);
		$this->db->order_by('slots.id DESC');
		//$this->db->where('(confirmed_slot > now() OR confirmed_slot IS NULL)');
		$result = $this->db->get()->row();
		return $result;
	}

	function editSlot($slotId, $updatedData)
	{
		$this->db->where('id', $slotId);
		$this->db->update('slots', $updatedData);
	}

	function getSlotById($slotId)
	{
		$this->db->from('slots');
		$this->db->where('id', $slotId);
		return $this->db->get()->row_array();
	}

	function getAllUniversities($sortBy)
	{
		$this->db->select('universities.*, country_master.name AS country_name, user_master.email AS email, user_master.image AS image, user_master.id AS login_id');
		$this->db->from('universities', 'universities.id = campuses.university_id');
		$this->db->join('country_master', 'universities.country_id = country_master.id');
		$this->db->join('user_master', 'universities.user_id = user_master.id');
		$this->db->where('universities.active', 1);
		$this->db->order_by($sortBy);
		$result = $this->db->get()->result_array();
		return $result;
	}

	function getUniversitiesById($universityIds)
	{
		$this->db->select('universities.*, country_master.name AS country_name');
		$this->db->from('universities', 'universities.id = campuses.university_id');
		$this->db->join('country_master', 'universities.country_id = country_master.id');
		$this->db->where_in('universities.id', $universityIds);
		$result = $this->db->get()->result_array();
		return $result;
	}

	function getCountriesByIds($countryIds)
	{
		$this->db->from('country_master');
		$this->db->where_in('id', $countryIds);
		$result = $this->db->get()->result_array();
		return $result;
	}

	function getCollegeCategoriesByIds($categoryIds)
	{
		$this->db->from('colleges_categories');
		$this->db->where_in('id', $categoryIds);
		$result = $this->db->get()->result_array();
		return $result;
	}

	function getCourseCategoriesByIds($categoryIds)
	{
		$this->db->from('courses_categories');
		$this->db->where_in('id', $categoryIds);
		$result = $this->db->get()->result_array();
		return $result;
	}

	function getDegreesByIds($degreeIds)
	{
		$this->db->from('degree_master');
		$this->db->where_in('id', $degreeIds);
		$result = $this->db->get()->result_array();
		return $result;
	}

	function checkQualifiedExam($conditions = array())
	{
		$this->db->where($conditions);
		$this->db->from('qualified_exams');
		$this->db->select('*');
		$result = $this->db->get()->row();
		return $result;
	}

	function updateQualifiedExam($insertData = array(), $id)
	{
		$this->db->where('student_id', $id);
		$this->db->update('qualified_exams', $insertData);
		return true;
	}

	function insertQualifiedExam($data = array())
	{
		$this->db->insert('qualified_exams', $data);
		return $this->db->insert_id();
	}

	function getAllGroups()
	{
		$this->db->select('universities.name,universities.whatsapp_link AS group_url');
		$this->db->from('universities');
		$this->db->WHERE('active', '1');
		//$this->db->join('universities', 'universities.id = universities_groups.university_id');
		$result = $this->db->get()->result_array();
		return $result;
	}

	function applyUniversity($data = array())
	{
		$this->db->insert('users_applied_universities', $data);
		return $this->db->insert_id();
	}

	function checkUniversityApplied($userId, $universityId, $collegeId = NULL)
	{
		$this->db->from('users_applied_universities');
		$this->db->where('user_id', $userId);
		$this->db->where('university_id', $universityId);
		if ($collegeId) {
			$this->db->where('college_id', $collegeId);
		}
		$result = $this->db->get();
		return $result->result_array();
	}

	function getApplicationById($applicationId)
	{
		$this->db->select('users_applied_universities.*, universities.name, universities.slug, universities.website, user_master.email, universities.banner, colleges.name AS college_name');
		$this->db->from('users_applied_universities');
		$this->db->join('universities', 'universities.id = users_applied_universities.university_id');
		$this->db->join('user_master', 'user_master.id = universities.user_id');
		$this->db->join('colleges', 'colleges.id = users_applied_universities.college_id', 'left');
		$this->db->where('users_applied_universities.id', $applicationId);
		$result = $this->db->get();
		return $result->result_array();
	}

	function getAppliedColleges($userId, $universityId)
	{
		$this->db->select('users_applied_universities.*, universities.name, universities.slug, universities.website, user_master.email, universities.banner, colleges.name AS college_name');
		$this->db->from('users_applied_universities');
		$this->db->join('universities', 'universities.id = users_applied_universities.university_id');
		$this->db->join('user_master', 'user_master.id = universities.user_id');
		$this->db->join('colleges', 'colleges.id = users_applied_universities.college_id', 'left');
		$this->db->where('users_applied_universities.user_id', $userId);
		$this->db->where('users_applied_universities.university_id', $universityId);
		$result = $this->db->get();
		return $result->result_array();
	}

	function getAppliedUniversitiesByUser($userId)
	{
		$this->db->select('users_applied_universities.*, universities.name, universities.slug, universities.website, user_master.email, universities.banner, colleges.name AS college_name, courses.name AS course_name, fa.name AS facilitator_name, fa.last_name AS facilitator_last_name,fa.email AS facilitator_email, fa.phone AS facilitator_mobile');
		$this->db->from('users_applied_universities');
		$this->db->join('universities', 'universities.id = users_applied_universities.university_id');
		$this->db->join('user_master', 'user_master.id = universities.user_id');
		$this->db->join('colleges', 'colleges.id = users_applied_universities.college_id', 'left');
		$this->db->join('courses', 'courses.id = users_applied_universities.course_id', 'left');
		$this->db->join('user_master fa', 'fa.id = users_applied_universities.facilitator_id', 'left');
		$this->db->where('users_applied_universities.user_id', $userId);
		$this->db->order_by('users_applied_universities.priority DESC');
		//$this->db->group_by('users_applied_universities.university_id');
		$result = $this->db->get();
		return $result->result_array();
	}

	function getAppliedUniversities($studentId)
	{
		$this->db->select('users_applied_universities.*, universities.name, universities.slug, universities.website, user_master.email, universities.banner, colleges.name AS college_name, courses.name AS course_name, user_master.name AS student_name, user_master.phone AS student_phone');
		$this->db->from('users_applied_universities');
		$this->db->join('user_master', 'user_master.id = users_applied_universities.user_id');
		$this->db->join('universities', 'universities.id = users_applied_universities.university_id');
		$this->db->join('colleges', 'colleges.id = users_applied_universities.college_id', 'left');
		$this->db->join('courses', 'courses.id = users_applied_universities.course_id', 'left');
		$this->db->where('user_master.id', $studentId);
		$result = $this->db->get()->result();
		return $result;
	}

	function updateAppliedUniversity($userId, $universityId, $updatedData)
	{
		$this->db->where('user_id', $userId);
		$this->db->where('university_id', $universityId);
		$this->db->update('users_applied_universities', $updatedData);
	}

	function updateAppliedUniversityById($applicationId, $updatedData)
	{
		$this->db->where('id', $applicationId);
		$this->db->update('users_applied_universities', $updatedData);
	}

	function getMySessions($userId, $slotType = 'UNIVERSITY')
	{
		$this->db->select('slots.*, universities.name, universities.slug, universities.website, user_master.email, universities.banner');
		$this->db->from('slots');
		$this->db->join('universities', 'universities.id = slots.university_id');
		$this->db->join('user_master', 'user_master.id = universities.user_id');
		$this->db->where('slots.user_id', $userId);
		$this->db->where('slots.slot_type', $slotType);
		//$this->db->group_by('universities.id');
		$this->db->order_by('slots.id DESC');
		$result = $this->db->get();
		return $result->result_array();
	}

	function getUniversityBySlug($slug)
	{
		$this->db->from('universities');
		$this->db->where('slug', $slug);
		$result = $this->db->get();
		return $result->result_array();
	}

	function getUserMaster($unameOrEmail)
	{
		$this->db->from('user_master');
		$this->db->where('username', $unameOrEmail);
		$this->db->or_where('email', $unameOrEmail);
		$this->db->or_where('phone', $unameOrEmail);
		$result = $this->db->get();
		return $result->result_array();
	}

	function addForgotPassword($data)
	{
		$this->db->insert('forgot_password', $data);
		return $id = $this->db->insert_id();
	}

	function updateForgotPassword($data, $userId)
	{
		$this->db->where('user_id', $userId);
		$this->db->update('forgot_password', $data);
	}

	function getForgotPasswordByUserId($userId)
	{
		$this->db->from('forgot_password');
		$this->db->where('user_id', $userId);
		$result = $this->db->get();
		return $result->result_array();
	}

	function getForgotPasswordByToken($token)
	{
		$this->db->from('forgot_password');
		$this->db->where('token', $token);
		$result = $this->db->get();
		return $result->result_array();
	}

	function getDocumentsList()
	{
		$this->db->from('document_meta');
		$this->db->where('entity_type', 'STUDENT');
		$result = $this->db->get();
		return $result->result_array();
	}

	function getDocumentByStudentType($userId, $documentType)
	{
		$this->db->from('document');
		$this->db->where('user_id', $userId);
		$this->db->where('document_meta_id', $documentType);
		$result = $this->db->get()->row();
		return $result;
	}

	function updateDocumentByStudentType($userId, $documentType, $updatedData)
	{
		$this->db->where('user_id', $userId);
		$this->db->where('document_meta_id', $documentType);
		$this->db->update('document', $updatedData);
	}

	function addDocument($data)
	{
		$this->db->insert('document', $data);
		return $id = $this->db->insert_id();
	}

	function getDocumentsByStudent($userId)
	{
		$this->db->select('document.*, document_meta.name AS document_type');
		$this->db->from('document');
		$this->db->join('document_meta', 'document_meta.id = document.document_meta_id');
		$this->db->where('document.user_id', $userId);
		$this->db->where('document_meta.entity_type', 'STUDENT');
		$result = $this->db->get();
		return $result->result_array();
	}

	function checkVisaByApplication($applicationId)
	{
		$this->db->from('users_visa_applicatios');
		$this->db->where('application_id', $applicationId);
		return $this->db->get()->result_array();
	}

	function addVisa($data)
	{
		$this->db->insert('users_visa_applicatios', $data);
		return $id = $this->db->insert_id();
	}

	function updateVisaByApplication($applicationId, $data)
	{
		$this->db->where('application_id', $applicationId);
		$this->db->update('users_visa_applicatios', $data);
	}

	function getVisaByUser($userId)
	{
		$this->db->from('users_visa_applicatios');
		$this->db->join('users_applied_universities', 'users_applied_universities.id = users_visa_applicatios.application_id');
		$this->db->where('users_applied_universities.user_id', $userId);
		return $this->db->get()->result_array();
	}

	function getUniversityDataByMultipleId($universityIds, $sortBy)
	{
		$list = implode(',', $universityIds);
		$list = rtrim($list, ",");

		$query1 = "SELECT DISTINCT(universities.id) as university_id, universities.*, country_master.name AS country_name, user_master.email AS email, user_master.image AS image, user_master.id AS login_id
								FROM universities
								JOIN country_master ON  universities.country_id = country_master.id
								JOIN user_master ON universities.user_id = user_master.id
								WHERE universities.active = 1
								AND universities.id IN ($list)
							  ORDER BY FIELD(universities.id, $list) limit 50
																;";
		//return $query1;
		$query_1 = $this->db->query($query1);
		$result = $query_1->result_array();
		return $result;
	}

	function getCourseDataByMultipleId($courseIds, $sortBy)
	{
		$list = implode(',', $courseIds);
		$list = rtrim($list, ",");

		$query1 = "SELECT courses.*, universities.name as university_name, universities.banner as university_banner, departments.name AS department_name, degree_master.name AS degree_name,
								country_master.name AS country_name, user_master.email AS email, user_master.image AS image, user_master.id AS login_id
								FROM universities
								JOIN country_master ON  universities.country_id = country_master.id
								JOIN user_master ON universities.user_id = user_master.id
								JOIN campuses ON campuses.university_id = universities.id
								JOIN colleges ON colleges.campus_id = campuses.id
								JOIN departments ON departments.college_id = colleges.id
								JOIN courses ON courses.department_id = departments.id
								JOIN degree_master ON courses.degree_id = degree_master.id
								WHERE universities.active = 1
								AND courses.id IN ($list)
							  ORDER BY FIELD(courses.id, $list) LIMIT 50
																;";
		//return $query1;
		$query_1 = $this->db->query($query1);
		$result = $query_1->result_array();
		return $result;
	}

	function getSearchDataByMultipleField($whereCondition, $usortBy)
	{
		//$list = implode(',', $universityIds);
		//$list = rtrim($list,",");
		$query1 = "SELECT DISTINCT(universities.id) as university_id, universities.*, country_master.name AS country_name
								FROM universities
								JOIN country_master ON  universities.country_id = country_master.id
								JOIN user_master ON universities.user_id = user_master.id
								JOIN campuses ON campuses.university_id = universities.id
								JOIN colleges ON colleges.campus_id = campuses.id
								JOIN departments ON departments.college_id = colleges.id
								JOIN courses ON courses.department_id = departments.id
								JOIN degree_master ON courses.degree_id = degree_master.id
								WHERE
								$whereCondition
								universities.active = 1
								ORDER BY $usortBy DESC
								;";

		//return $query1;
		$query_1 = $this->db->query($query1);
		$result = $query_1->result_array();
		return $result;
	}

	function getSearchDataByMultipleFieldCourse($whereCondition, $csortBy)
	{
		$query1 = "SELECT courses.*, universities.name as university_name, universities.banner as university_banner, departments.name AS department_name, degree_master.name AS degree_name,
								country_master.name AS country_name, user_master.email AS email, user_master.image AS image, user_master.id AS login_id
								FROM universities
								JOIN country_master ON  universities.country_id = country_master.id
								JOIN user_master ON universities.user_id = user_master.id
								JOIN campuses ON campuses.university_id = universities.id
								JOIN colleges ON colleges.campus_id = campuses.id
								JOIN departments ON departments.college_id = colleges.id
								JOIN courses ON courses.department_id = departments.id
								JOIN degree_master ON courses.degree_id = degree_master.id
								WHERE
								$whereCondition
								universities.active = 1
								AND courses.status = 1
								ORDER BY $csortBy DESC
								limit 100							;";

		//return $query1;
		$query_1 = $this->db->query($query1);
		$result = $query_1->result_array();
		return $result;
	}

	function getCourseData($sortBy)
	{
		$query1 = "SELECT courses.*, universities.name as university_name, universities.banner as university_banner, departments.name AS department_name, degree_master.name AS degree_name,
								country_master.name AS country_name, user_master.email AS email, user_master.image AS image, user_master.id AS login_id
								FROM universities
								JOIN country_master ON  universities.country_id = country_master.id
								JOIN user_master ON universities.user_id = user_master.id
								JOIN campuses ON campuses.university_id = universities.id
								JOIN colleges ON colleges.campus_id = campuses.id
								JOIN departments ON departments.college_id = colleges.id
								JOIN courses ON courses.department_id = departments.id
								JOIN degree_master ON courses.degree_id = degree_master.id
								WHERE universities.active = 1
								AND courses.status = 1
							  ORDER BY courses.id DESC LIMIT 50
																;";

		//return $query1;
		$query_1 = $this->db->query($query1);
		$result = $query_1->result_array();
		return $result;
	}

	function getFavouritesCourses($uID)
	{
		$query1 = "SELECT favourites_detail.id, favourites_detail.user_id, favourites_detail.entity_id as course_id,
							country_master.name as countryname, courses.name as coursename, universities.name as name,
							degree_master.name as degreename, universities.banner, degree_master.id as degree_id,
							universities.slug as slug, universities.website as website, universities.id as uni_id,
							courses.course_view_count,courses.course_likes_count,courses.course_fav_count
							FROM favourites_detail
							JOIN courses ON courses.id = favourites_detail.entity_id
							JOIN departments ON departments.id = courses.department_id
							JOIN colleges ON departments.college_id = colleges.id
							JOIN campuses ON colleges.campus_id = campuses.id
							JOIN universities ON campuses.university_id = universities.id
							JOIN country_master ON country_master.id = universities.country_id
							JOIN degree_master ON courses.degree_id = degree_master.id
						  WHERE favourites_detail.user_id = $uID AND favourites_detail.entity_type = 'courses';";

		$query_1 = $this->db->query($query1);
		$result = $query_1->result_array();
		return $result;
	}

	function getFavouritesResearches($uID)
	{
		$query1 = "SELECT favourites_detail.id, favourites_detail.user_id, favourites_detail.entity_id as research_id,
							country_master.name as countryname, researches.topic as researchname, universities.name as name,
						  universities.banner,departments.name AS department_name,
							universities.slug as slug, universities.website as website, universities.id as uni_id,
							researches.research_view_count,researches.research_likes_count,researches.research_fav_count
							FROM favourites_detail
							JOIN researches ON researches.id = favourites_detail.entity_id
							JOIN departments ON departments.id = researches.department_id
							JOIN colleges ON departments.college_id = colleges.id
							JOIN campuses ON colleges.campus_id = campuses.id
							JOIN universities ON campuses.university_id = universities.id
							JOIN country_master ON country_master.id = universities.country_id
						  WHERE favourites_detail.user_id = $uID AND favourites_detail.entity_type = 'researches';";

		$query_1 = $this->db->query($query1);
		$result = $query_1->result_array();
		return $result;
	}

	function userWebinarList($userId)
	{
		$query1 = "SELECT w.* FROM webinar_master w
							JOIN webinar_log_master wm ON wm.webinar_id = w.id
							WHERE w.webinar_date >= CURDATE() AND w.type = 'WEBINAR' AND w.status = 1 AND wm.user_id = $userId;";

		$query_1 = $this->db->query($query1);
		$result = $query_1->result_array();
		return $result;
	}

	function commonCheckAll($userId, $stype)
	{
		if ($stype == 'universities') {
			$query1 = "SELECT id, user_id, entity_id, entity_type, 'likes' AS s_type  FROM likes_detail WHERE user_id = $userId AND entity_type = 'universities'
									UNION ALL
									SELECT id, user_id, university_id as entity_id, 'universities' AS entity_type, 'favourites' AS s_type  FROM favourites WHERE user_id = $userId;
									";
		} else {
			$query1 = "SELECT id, user_id, entity_id, entity_type, 'likes' AS s_type  FROM likes_detail WHERE user_id = $userId AND entity_type = '$stype'
									UNION ALL
									SELECT id, user_id, entity_id, entity_type, 'favourites' AS s_type  FROM favourites_detail WHERE user_id = $userId AND entity_type = '$stype';
									";
		}
		$query_1 = $this->db->query($query1);
		$result = $query_1->result_array();
		return $result;
	}

	function updateLeadInfoByEmail($insertData = array(), $email)
	{
		$this->db->where('email', $email);
		$this->db->update('lead_master', $insertData);
		return true;
	}

	function getBrochuresByUniversity($universityId)
	{
		$this->db->from('universities_brochures');
		$this->db->where('university_id', $universityId);
		$result = $this->db->get();
		return $result->result();
	}

	function getBrochuresById($id)
	{
		$this->db->from('universities_brochures');
		$this->db->where('id', $id);
		$result = $this->db->get();
		return $result->row();
	}

	function addInvitations($data)
	{
		$this->db->insert('invitations', $data);
	}

	function getIndianColleges()
	{
		$this->db->from('indian_colleges');
		$this->db->order_by('college', 'asc');
		$result = $this->db->get();
		return $result->result_array();
	}

	function notesList($userId)
	{
		$query1 = "SELECT c.*,um.name as created_by_name FROM comments c
							JOIN user_master um ON um.id = c.added_by
							WHERE c.student_login_id = $userId ORDER BY c.id DESC ;";

		$query_1 = $this->db->query($query1);
		$result = $query_1->result_array();
		return $result;
	}

	function appDocList($userId)
	{
		$query1 = " SELECT dm.*,d.url as document_url FROM document_meta dm
								LEFT JOIN  document d ON d.document_meta_id = dm.id AND d.user_id = $userId
								WHERE dm.entity_type = 'APPLICATION' ";

		$query_1 = $this->db->query($query1);
		$result = $query_1->result_array();
		return $result;
	}

	function getApplicationDocumentByUniversity($universityId)
	{
		$this->db->from('document_meta');
		$this->db->where('university_id', $universityId);
		$this->db->where('entity_type', 'APPLICATION');
		$query = $this->db->get();
		return $query->result_array();
	}

	function getApplicationDocumentsByStudent($userId)
	{
		$this->db->select('document.*, document_meta.name AS document_type');
		$this->db->from('document');
		$this->db->join('document_meta', 'document_meta.id = document.document_meta_id');
		$this->db->where('document.user_id', $userId);
		$this->db->where('document_meta.entity_type', 'APPLICATION');
		$result = $this->db->get();
		return $result->result_array();
	}

	public function getNewDocumentList($productId = 10000)
	{
		$query1 = " SELECT dm.id AS document_master_id , pdm.id AS product_document_id, pdm.display_name as display_name, pdm.description, dm.display_name AS product_document_name,
								dm.document_type, dgm.group_name, dgm.tab_name
								FROM product_document_master pdm
								JOIN document_master dm ON JSON_CONTAINS(pdm.document_id, CAST(dm.id as JSON), '$')
								JOIN document_group_master dgm ON dgm.id = dm.group_id
								WHERE pdm.product_id = $productId  AND  pdm.status = 1 AND  dm.status = 1 order by dgm.sort_order; ";

		$query = $this->db->query($query1);
		$result_data = $query->result_array();
		return [
			"data" 	=> $result_data,
			"query" => $query1
		];
	}

	public function getNewDocumentUserDocList($entityId, $productId = 10000)
	{
		$query1 = " SELECT dm.id AS document_master_id , pdm.id AS product_document_id, d.name AS document_display_name, d.id AS document_id, pdm.description, dm.display_name, d.url AS document_url, dm.document_type
								FROM product_document_master pdm
								JOIN document_master dm ON JSON_CONTAINS(pdm.document_id, CAST(dm.id as JSON), '$')
							  JOIN document d ON d.document_meta_id = dm.id AND d.user_id = $entityId AND d.user_type = 'STUDENT'
								WHERE pdm.product_id = $productId; ";

		$query = $this->db->query($query1);
		$result_data = $query->result_array();
		return [
			"data" 	=> $result_data,
			"query" => $query1
		];
	}

	public function getNotificationList($counsellorId, $userType = 1)
	{
		if ($userType == 7) {
			$query1 = " SELECT nm.*, um.name, um.last_name
										FROM notification_master nm
										JOIN user_master um ON nm.created_by = um.id
										WHERE nm.user_id IN (
											SELECT user_id
											  FROM users_applied_universities
											  WHERE facilitator_id = $counsellorId
										)
											ORDER BY nm.id DESC;
										 ";
		} else {
			$query1 = " SELECT nm.*, um.name, um.last_name
									FROM notification_master nm
									JOIN user_master um ON nm.created_by = um.id
									ORDER BY nm.id DESC;
									";
		}

		$query = $this->db->query($query1);
		$result_data = $query->result_array();
		return [
			"data" 	=> $result_data,
			"query" => $query1
		];
	}

	public function updateNotificationList($counsellorId, $userType)
	{
		if ($userType == 7) {
			$query1 = " UPDATE notification_master SET seen_by_counsellor = '1' WHERE id !='1' AND user_id IN ( $counsellorId ) ; ";
		} else {
			$query1 = " UPDATE notification_master SET seen_by_admin = '1' WHERE id !='1' ; ";
		}
		$query = $this->db->query($query1);
		return [
			"data" 	=> "SUCCESS",
			"query" => $query1
		];
	}

	function getApplicationInfo($studentId, $appId)
	{
		$this->db->select('users_applied_universities.*, universities.name');
		$this->db->from('users_applied_universities');
		$this->db->join('user_master', 'user_master.id = users_applied_universities.user_id');
		$this->db->join('universities', 'universities.id = users_applied_universities.university_id');
		$this->db->where('users_applied_universities.id', $appId);
		$result = $this->db->get()->result();
		return $result;
	}
}
