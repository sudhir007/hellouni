<?php
class Counsellor_model extends CI_Model {

    function applicationData($counsellorId){

      $condition = $counsellorId ? " WHERE sd.originator_id IN ($counsellorId) " : "";

      $query1 = "SELECT uau.status,count(uau.id) AS count FROM users_applied_universities uau
                  JOIN user_master um ON um.id = uau.user_id
                  JOIN student_details sd ON sd.user_id = um.id
                  $condition
                  group by uau.status
                  ;";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;
    }

    function visaApplicationData($counsellorId){

      $condition = $counsellorId ? "WHERE sd.originator_id IN ($counsellorId)" : "";

      $query1 = "SELECT uva.status,count(uva.id) AS count FROM users_visa_applicatios uva
                  JOIN users_applied_universities uau ON uva.application_id = uau.id
                  JOIN user_master um ON um.id = uau.user_id
                  JOIN student_details sd ON sd.user_id = um.id
                  $condition
                  group by uva.status
                  ;";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;

    }

    function studentData($counsellorId){

      $condition = $counsellorId ? "WHERE originator_id IN ($counsellorId)" : "";

      $query1 = "SELECT status,count(id) AS count FROM student_details
                  $condition
                  group by status
                  ;";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;
    }

    function filterApplicationData($allChild, $startDate, $endDate, $countryIds, $intake_year, $intake_month){

      $counsellorCondition = $allChild ? " WHERE sd.originator_id IN ($allChild)" : "";
      $dateCondition = ($startDate &&  $endDate) ? "AND CAST(um.createdate AS date) BETWEEN '$startDate' AND '$endDate'" : "";
      $countryCondition = $countryIds ? "AND lm.countries IN ('$countryIds')" : "";
      $intakeYearCondition = $intake_year ? "AND sd.intake_year IN ('$intake_year') " : "";
      $intakeCondition = $intake_month ? "AND sd.intake_month IN ('$intake_month') " : "";

      $query1 = "SELECT uau.status,count(uau.id) AS count FROM  users_applied_universities uau
                  JOIN user_master um ON um.id = uau.user_id
                  JOIN student_details sd ON sd.user_id = um.id
                  JOIN lead_master lm ON lm.email = um.email AND um.type = 5
                  $counsellorCondition
                  $dateCondition
                  $countryCondition
                  $intakeYearCondition
                  $intakeCondition
                  GROUP BY uau.status;
                  ";

                  //echo $query1;

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;
    }

    function filterVisaApplicationData($allChild, $startDate, $endDate, $countryIds, $intake_year, $intake_month){

      $counsellorCondition = $allChild ? " WHERE sd.originator_id IN ($allChild)" : "";
      $dateCondition = ($startDate &&  $endDate) ? "AND CAST(um.createdate AS date) BETWEEN '$startDate' AND '$endDate'" : "";
      $countryCondition = $countryIds ? "AND lm.countries IN ('$countryIds')" : "";
      $intakeYearCondition = $intake_year ? "AND sd.intake_year IN ('$intake_year') " : "";
      $intakeCondition = $intake_month ? "AND sd.intake_month IN ('$intake_month') " : "";

      $query1 = "SELECT uva.status,count(uva.id) AS count FROM users_visa_applicatios uva
                  JOIN users_applied_universities uau ON uva.application_id = uau.id
                  JOIN user_master um ON um.id = uau.user_id
                  JOIN student_details sd ON sd.user_id = um.id
                  JOIN lead_master lm ON lm.email = um.email AND um.type = 5
                  $counsellorCondition
                  $dateCondition
                  $countryCondition
                  $intakeCondition
                  group by uva.status
                  ;";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;

    }

    function filterStudentData($allChild, $startDate, $endDate, $countryIds, $intake_year, $intake_month, $statusFilter){

      $counsellorCondition = $allChild ? " WHERE sd.originator_id IN ($allChild)" : "";
      $dateCondition = ($startDate &&  $endDate) ? " AND CAST(um.createdate AS date) BETWEEN '$startDate' AND '$endDate'" : "";
      $countryCondition = $countryIds ? " AND lm.countries IN ('$countryIds')" : "";
      $intakeYearCondition = $intake_year ? " AND sd.intake_year IN ('$intake_year') " : "";
      $intakeCondition = $intake_month ? " AND sd.intake_month IN ('$intake_month') " : "";
      $statusFilterCondition = $statusFilter ? " AND sd.status IN ($statusFilter) " : "";

      $query1 = "SELECT sd.status,count(sd.id) AS count FROM student_details sd
                  JOIN user_master um ON um.id = sd.user_id
                  JOIN lead_master lm ON lm.email = um.email AND um.type = 5
                  $counsellorCondition
                  $dateCondition
                  $countryCondition
                  $intakeYearCondition
                  $intakeCondition
                  $statusFilterCondition
                  group by sd.status
                  ;";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;

    }

    function getChild($counsellorId){

      $query1 = "SELECT group_concat(id) AS child_ids FROM user_master  WHERE parent_id = $counsellorId ;";

              $query = $this->db->query($query1);
              $result_data = $query->result_array();

              return $result_data;

    }

    function getTeamDetail($counsellorId){

      $query1 = "SELECT um.*,p.*,count(sd.id) as student_count FROM user_master um
                  JOIN partners p ON p.user_id = um.id
                  LEFT JOIN student_details sd ON sd.originator_id = um.id
                  WHERE um.parent_id = $counsellorId
                  GROUP BY um.id;";

              $query = $this->db->query($query1);
              $result_data = $query->result_array();

              return $result_data;
    }

    function profileDetail($counsellorId){

      $query1 = "SELECT um.*, p.*, cm.id AS city_id, cm.city_name  FROM user_master um
                  JOIN partners p ON p.user_id = um.id
                  LEFT JOIN city_master cm ON p.city = cm.id
                  WHERE um.id = $counsellorId ;";

              $query = $this->db->query($query1);
              $result_data = $query->result_array();

              return $result_data;
    }

    function applicationList($counsellorId){

      $condition = $counsellorId ? " WHERE sd.originator_id IN ($counsellorId) " : "";

                  $query1 = "SELECT sd.id, sd.user_id, um.name, um.email, um.phone, uau.status, um.createdate, oum.name AS createdby, oum.email as originator_email, u.name AS university_name, c.name AS college_name, co.name AS course_name
                              FROM users_applied_universities uau
                              JOIN user_master um ON um.id = uau.user_id
                              JOIN student_details sd ON sd.user_id = um.id
                              JOIN user_master oum ON oum.id = sd.originator_id
                              JOIN universities u ON u.id = uau.university_id
                              LEFT JOIN colleges c ON c.id = uau.college_id
                              LEFT JOIN courses co ON co.id = uau.course_id
                              $condition
                              ;";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;

    }

    function applicationFilterList($allChild, $startDate, $endDate, $countryIds, $intake_year, $intake_month, $statusFilter){

      $counsellorCondition = $allChild ? " WHERE sd.originator_id IN ($allChild) " : "";
      $dateCondition = ($startDate &&  $endDate) ? "AND CAST(um.createdate AS date) BETWEEN '$startDate' AND '$endDate' " : "";
      $countryCondition = $countryIds ? "AND lm.countries IN ('$countryIds') " : "";
      $intakeYearCondition = $intake_year ? "AND sd.intake_year IN ('$intake_year') " : "";
      $intakeCondition = $intake_month ? "AND sd.intake_month IN ('$intake_month') " : "";
      $statusFilterCondition = $statusFilter ? "AND uau.status IN ($statusFilter) " : "";


                  $query1 = "SELECT sd.id, sd.user_id, um.name, um.email, um.phone, uau.status AS lead_state, um.status, um.createdate, oum.name AS createdby, oum.email as originator_email, u.name AS university_name, c.name AS college_name, co.name AS course_name
                              FROM  users_applied_universities uau
                              JOIN user_master um ON um.id = uau.user_id
                              JOIN student_details sd ON sd.user_id = um.id
                              JOIN user_master oum ON oum.id = sd.originator_id
                              JOIN universities u ON u.id = uau.university_id
                              LEFT JOIN colleges c ON c.id = uau.college_id
                              LEFT JOIN courses co ON co.id = uau.course_id
                              $counsellorCondition
                              $dateCondition
                              $countryCondition
                              $intakeYearCondition
                              $intakeCondition
                              $statusFilterCondition
                              ;";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;

    }

    function studentList($counsellorId) {

      $condition = $counsellorId ? " AND sd.originator_id IN ($counsellorId) " : "";

      $query1 = "SELECT sd.id, sd.user_id, um.name, um.email, um.phone, um.status, um.createdate, oum.name AS createdby,
                  oum.email as originator_email, count(upv.user_id) AS application_count, count(distinct(upv.university_id)) AS university_count
                  FROM student_details sd
                  JOIN user_master um ON um.id = sd.user_id
                  JOIN user_master oum ON oum.id = sd.originator_id
                  LEFT JOIN users_applied_universities upv ON upv.user_id = sd.user_id
                  WHERE um.type=5
                  $condition
                  group by upv.user_id;
                  ;";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;

    }

    function studentFilterList($allChild, $startDate, $endDate, $countryIds, $intake_year, $intake_month, $statusFilter) {

      $counsellorCondition = $allChild ? " WHERE sd.originator_id IN ($allChild)" : "";
      $dateCondition = ($startDate &&  $endDate) ? "AND CAST(um.createdate AS date) BETWEEN '$startDate' AND '$endDate'" : "";
      $countryCondition = $countryIds ? "AND lm.countries IN ('$countryIds')" : "";
      $intakeYearCondition = $intake_year ? "AND sd.intake_year IN ('$intake_year') " : "";
      $intakeCondition = $intake_month ? "AND sd.intake_month IN ('$intake_month')" : "";
      $statusFilterCondition = $statusFilter ? "AND sd.status IN ($statusFilter) " : "";


      $query1 = "SELECT sd.id, sd.user_id, um.name, um.email, um.phone, sd.status AS lead_state, um.status, um.createdate, oum.name AS createdby, oum.email as originator_email
                  FROM student_details sd
                  JOIN user_master um ON um.id = sd.user_id
                  JOIN user_master oum ON oum.id = sd.originator_id
                  $counsellorCondition
                  $dateCondition
                  $countryCondition
                  $intakeYearCondition
                  $intakeCondition
                  $statusFilterCondition
                  ;";

                  //return $query1;

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;

    }

    function graphData($counsellorId) {

      $condition = $counsellorId ? " WHERE sd.originator_id IN ($counsellorId) " : "";

      $query1 = "SELECT MONTH(um.createdate) as graph_month, uau.status, count(uau.id) AS count FROM users_applied_universities uau
                  JOIN user_master um ON um.id = uau.user_id
                  JOIN student_details sd ON sd.user_id = um.id
                  $condition
                  group by uau.status, MONTH(um.createdate)";

      $query2 = "SELECT MONTH(um.createdate) as graph_month, uva.status, count(uva.id) AS count FROM users_visa_applicatios uva
                  JOIN users_applied_universities uau ON uva.application_id = uau.id
                  JOIN user_master um ON um.id = uau.user_id
                  JOIN student_details sd ON sd.user_id = um.id
                  $condition
                  group by uva.status, MONTH(um.createdate)";

      $query3 = "SELECT MONTH(um.createdate) as graph_month,sd.status, count(sd.id) AS count FROM student_details sd
                  JOIN user_master um ON um.id = sd.user_id
                  $condition
                  group by sd.status, MONTH(um.createdate)";

                  $query_a = $this->db->query($query1);
                  $result_data_a = $query_a->result_array();

                  $query_v = $this->db->query($query2);
                  $result_data_v = $query_v->result_array();

                  $query_s = $this->db->query($query3);
                  $result_data_s = $query_s->result_array();

                  $graphData = [
                    "application_data" => $result_data_a,
                    "visa_data" => $result_data_v,
                    "student_data" => $result_data_s
                  ];

                  return $graphData;
    }

    function graphDataFilter($allChild, $startDate, $endDate, $countryIds, $intake_year, $intake_month){

      $counsellorCondition = $allChild ? " WHERE sd.originator_id IN ($allChild)" : "";
      $dateCondition = ($startDate &&  $endDate) ? "AND CAST(um.createdate AS date) BETWEEN '$startDate' AND '$endDate'" : "";
      $countryCondition = $countryIds ? "AND lm.countries IN ('$countryIds')" : "";
      $intakeYearCondition = $intake_year ? "AND sd.intake_year IN ('$intake_year') " : "";
      $intakeCondition = $intake_month ? "AND sd.intake_month IN ('$intake_month')" : "";

      $query1 = "SELECT MONTH(um.createdate) as graph_month, uau.status, count(uau.id) AS count FROM users_applied_universities uau
                  JOIN user_master um ON um.id = uau.user_id
                  JOIN student_details sd ON sd.user_id = um.id
                  JOIN lead_master lm ON lm.email = um.email AND um.type = 5
                  $counsellorCondition
                  $dateCondition
                  $countryCondition
                  $intakeYearCondition
                  $intakeCondition
                  group by uau.status, MONTH(um.createdate)";

      $query2 = "SELECT MONTH(um.createdate) as graph_month, uva.status, count(uva.id) AS count FROM users_visa_applicatios uva
                  JOIN users_applied_universities uau ON uva.application_id = uau.id
                  JOIN user_master um ON um.id = uau.user_id
                  JOIN student_details sd ON sd.user_id = um.id
                  JOIN lead_master lm ON lm.email = um.email AND um.type = 5
                  $counsellorCondition
                  $dateCondition
                  $countryCondition
                  $intakeYearCondition
                  $intakeCondition
                  group by uva.status, MONTH(um.createdate)";

      $query3 = "SELECT MONTH(um.createdate) as graph_month,sd.status, count(sd.id) AS count FROM student_details sd
                  JOIN user_master um ON um.id = sd.user_id
                  JOIN lead_master lm ON lm.email = um.email AND um.type = 5
                  $counsellorCondition
                  $dateCondition
                  $countryCondition
                  $intakeYearCondition
                  $intakeCondition
                  group by sd.status, MONTH(um.createdate)";

                  $query_a = $this->db->query($query1);
                  $result_data_a = $query_a->result_array();

                  $query_v = $this->db->query($query2);
                  $result_data_v = $query_v->result_array();

                  $query_s = $this->db->query($query3);
                  $result_data_s = $query_s->result_array();

                  $graphData = [
                    "application_data" => $result_data_a,
                    "visa_data" => $result_data_v,
                    "student_data" => $result_data_s
                  ];

                  return $graphData;
    }

    function studentofferlist($universityId = 0){

      $universityCondition = $universityId === 0 ? "" : " AND universities.id = $universityId";

      $query1 = "SELECT DISTINCT(universities.id) as university_id, user_master.email AS university_email, universities.name, universities.website, universities.establishment_year, universities.type, universities.carnegie_accreditation, universities.total_students, universities.total_students_international, universities.placement_percentage, universities.logo, universities.slug, universities.ranking_world, country_master.name AS country_name
								FROM universities
								JOIN country_master ON  universities.country_id = country_master.id
								JOIN user_master ON universities.user_id = user_master.id
								JOIN campuses ON campuses.university_id = universities.id
								JOIN colleges ON colleges.campus_id = campuses.id
								JOIN departments ON departments.college_id = colleges.id
								JOIN courses ON courses.department_id = departments.id
								JOIN degree_master ON courses.degree_id = degree_master.id
								WHERE universities.active = 1
                $universityCondition
								ORDER BY  universities.ranking_world DESC;
                  ;";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;


    }

    function courseListByUniversityId($universityId, $searchText){

      $query1 = "SELECT courses.id, courses.name
								FROM universities
								JOIN campuses ON campuses.university_id = universities.id
								JOIN colleges ON colleges.campus_id = campuses.id
								JOIN departments ON departments.college_id = colleges.id
								JOIN courses ON courses.department_id = departments.id
								JOIN degree_master ON courses.degree_id = degree_master.id
								WHERE universities.active = 1
                AND universities.id = $universityId
                AND courses.name LIKE '%$searchText%%'
                  ;";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;


    }

    function getcommentsAllList($userId, $entityId, $userType){

      if($userType == 3){

        $query1 = "SELECT c.*, u.name AS comment_by
                    FROM comments c
                    JOIN user_master u ON u.id = c.added_by
                    WHERE (c.student_login_id = $userId AND c.entity_type != 'UNIVERSITY')
                     OR ( c.student_login_id = $userId AND c.added_by = $entityId ) ";

      } else {

        $query1 = "SELECT c.*, u.name AS comment_by
                    FROM comments c
                    JOIN user_master u ON u.id = c.added_by
                    WHERE c.student_login_id = $userId ";
      }


              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;

    }

    function getCommentsByStudent($studentId)
	{
		$this->db->select('comments.*, user_master.name AS student_name, universities.name AS university_name, um.name AS commented_by, user_master.email AS student_email, user_master.phone AS student_phone');
		$this->db->from('comments');
    $this->db->join('user_master', 'user_master.id = comments.student_login_id');
    $this->db->join('user_master AS um', 'um.id = comments.added_by');
    $this->db->join('universities', 'universities.id = comments.entity_id AND comments.entity_type = "UNIVERSITY" ', 'left');
		$this->db->where('comments.student_login_id', $studentId);
    $this->db->order_by('comments.added_time', 'DESC');
		$result = $this->db->get();
		return $result->result_array();
	}

  function getCommentsByStudentApplication($studentId,$appId) {

  $this->db->select('comments.*, user_master.name AS student_name, universities.name AS university_name, um.name AS commented_by, user_master.email AS student_email, user_master.phone AS student_phone');
  $this->db->from('comments');
  $this->db->join('user_master', 'user_master.id = comments.student_login_id');
  $this->db->join('user_master AS um', 'um.id = comments.added_by');
  $this->db->join('users_applied_universities AS uau', 'uau.id = comments.entity_id');
  $this->db->join('universities ', 'universities.id = uau.university_id');
  $this->db->where('comments.student_login_id', $studentId);
  $this->db->where('comments.entity_id', $appId);
  $this->db->order_by('comments.added_time', 'DESC');
  $result = $this->db->get();
  return $result->result_array();

  }

  }
?>
