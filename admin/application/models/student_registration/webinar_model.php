<?php
class Webinar_model extends CI_Model {

    function checkMasterOtp($mobileNumber){

        $this->db->select('*');
        $this->db->from('otp_master');
        $this->db->where('mobile_number', $mobileNumber);
        $this->db->where('status', 1);
        $query = $this->db->get();
        $result = $query->result_array();

        return $result;
    }

    function insertotp($wheredata, $insertData, $tableName){

				$this->db->select('*');
        $this->db->from($tableName);
        $this->db->where($wheredata);
        $query = $this->db->get();
				$otpData = $query->result_array();

        if($query->num_rows() == 0)
		{
        		$insert_query = $this->db->insert($tableName, $insertData);
            $data = $this->db->insert_id();
			if(isset($insertData['otp'])){
				$sendsms = sendsms($insertData['otp'], $insertData['mobile_number']);
			}
        }
		else
		{
      		//$this->db->where($wheredata);
			//$this->db->update($tableName, $insertData);
			if($tableName == 'forget_password_master'){
				$this->db->where($wheredata);
				$this->db->update($tableName, $insertData);
			}
			if(isset($otpData[0]["otp"])){
				$otp = $otpData[0]["otp"];
				$sendsms = sendsms($otp, $insertData['mobile_number']);
			}
      		$data = "1";
        }

        return $data;
	}

    function checkLogin($mobileNumber, $email) {

		$this->db->select('*');
    $this->db->from('user_master');
    $this->db->where('phone', $mobileNumber);
		$this->db->or_where('email', $email);
    $query = $this->db->get();
		$result = $query->result_array();

    return $result;
	     }

    function checkMobileOTP($mobileNumber, $otp){

      	$this->db->select('*');
        $this->db->from('otp_master');
        $this->db->where('mobile_number', $mobileNumber);
    		$this->db->where('otp', $otp);
        $query = $this->db->get();
    		$result = $query->result_array();

        return $result;
	}

    function checkUserIDLogin($userId) {
      $this->db->select('*');
      $this->db->from('user_master');
      $this->db->where('username', $userId);
      $query = $this->db->get();
      $result = $query->result_array();

      return $result;
    }

    function checkUserLogin($userEmail,$userMobile) {

      $where_in = array('5','8','9');

      $this->db->select('*');
      $this->db->from('user_master');
      $this->db->where_in('type', $where_in);
      $this->db->where("(email = '$userEmail' OR phone = '$userMobile')");
      $query = $this->db->get();
      $result = $query->result_array();

      return $result;
    }

        function webinar_list($userId){

          $query1 = "SELECT *
                      FROM (
                      SELECT w.*,'NO' AS selected, count(wm.webinar_id) as acount FROM webinar_master w
                      LEFT JOIN webinar_log_master wm ON wm.webinar_id = w.id
                      WHERE w.webinar_date >= CURDATE() AND w.type = 'WEBINAR' AND w.status = 1 AND w.id NOT IN (SELECT webinar_id FROM webinar_log_master where user_id = $userId)
                      group by w.id
                      UNION
                      SELECT w.*,'YES' AS selected, count(wm.webinar_id) as acount FROM webinar_master w
                      LEFT JOIN webinar_log_master wm ON wm.webinar_id = w.id
                      WHERE w.webinar_date >= CURDATE() AND w.type = 'WEBINAR' AND w.status = 1 AND w.id IN (SELECT webinar_id FROM webinar_log_master where user_id = $userId)
                      group by w.id
                      ) AS i
                      ORDER BY webinar_date ASC
                      ;";

                  $query = $this->db->query($query1);

                  $result_data = $query->result_array();

                  return $result_data;
        }

        function getOngoingWebinar($userId){

          $query1 = "SELECT wm.* FROM webinar_master wm
                      JOIN webinar_log_master wlm ON wm.id = wlm.webinar_id
                      WHERE wm.webinar_date = CURDATE() AND wm.type = 'WEBINAR' AND wlm.user_id = $userId AND status = 1;
                      ;";

                  $query = $this->db->query($query1);

                  $result_data = $query->result_array();

                  return $result_data;
        }

        function fair_list($userId){

          $query1 = "SELECT *
                      FROM (
                        SELECT w.*,'NO' AS selected, count(wm.fair_id) as acount FROM fair_master w
                        LEFT JOIN fair_log_master wm ON wm.fair_id = w.id
                        WHERE w.fair_date >= CURDATE() AND w.status = 1 AND w.id NOT IN (SELECT fair_id FROM fair_log_master WHERE user_id = $userId AND fair_id IS NOT NULL)
                        group by w.id
                        UNION
                        SELECT w.*,'YES' AS selected, count(wm.fair_id) as acount FROM fair_master w
                        LEFT JOIN fair_log_master wm ON wm.fair_id = w.id
                        WHERE w.fair_date >= CURDATE() AND w.status = 1 AND w.id IN (SELECT fair_id FROM fair_log_master WHERE user_id = $userId AND fair_id IS NOT NULL)
                        group by w.id
                      ) AS i
                      ORDER BY fair_date ASC
                      ;";

                  $query = $this->db->query($query1);

                  $result_data = $query->result_array();

                  return $result_data;

        }

        function checkAppliedFair($userId,$fairId){

          $query1 = "SELECT * FROM fair_log_master WHERE user_id = $userId AND fair_id = $fairId;";

                  $query = $this->db->query($query1);

                  $result_data = $query->result_array();

                  return $result_data;

        }

        function fair_webinar_list($fairId){

          $query1 = "SELECT * FROM webinar_master WHERE fair_id = $fairId;";

                  $query = $this->db->query($query1);

                  $result_data = $query->result_array();

                  return $result_data;

        }

        function getOngoingFair($userId){

          $query1 = "SELECT wm.* FROM webinar_master wm
                      JOIN webinar_log_master wlm ON wm.id = wlm.webinar_id
                      WHERE wm.webinar_date = CURDATE() AND wm.type = 'WEBINAR' AND wlm.user_id = $userId AND status = 1
                      ;";

                  $query = $this->db->query($query1);

                  $result_data = $query->result_array();

                  return $result_data;
        }

        function dataCheck($userId){
          $query1 = "SELECT * FROM student_details
                     WHERE user_id = $userId AND comp_exam IS NULL;";

                  $query = $this->db->query($query1);

                  $result_data = $query->result_array();

                  return $result_data;
        }

      function  virtualFairParticipants($userId,$fairId){

          $query1 = "SELECT * FROM (SELECT w.*, fm.name as fair_name, 'YES' AS selected,wm.id as s_id,wm.start_time AS slot_start_time
                        FROM virtual_fair_participants w
						            JOIN virtual_fair_slots wm ON wm.participant_id = w.id
                        JOIN fair_master fm ON fm.id = w.virtual_fair_id
                        WHERE w.start_time > NOW() AND w.participant_type = 'UNIVERSITY' AND w.virtual_fair_id = $fairId AND wm.id IN (
                          SELECT id FROM virtual_fair_slots WHERE JSON_CONTAINS(student_booked, '[$userId]')
                        ) ORDER BY university_name ASC) as a
                        UNION
                        SELECT * FROM (SELECT w.*, fm.name as fair_name, 'NO' AS selected,'0' as s_id,'0' AS slot_start_time
                        FROM virtual_fair_participants w
						            JOIN virtual_fair_slots wm ON wm.participant_id = w.id
                        JOIN fair_master fm ON fm.id = w.virtual_fair_id
                        WHERE w.start_time > NOW() AND w.participant_type = 'UNIVERSITY' AND w.virtual_fair_id = $fairId AND wm.participant_id NOT IN (
                          SELECT participant_id FROM virtual_fair_slots WHERE JSON_CONTAINS(student_booked, '[$userId]')
                        ) ORDER BY university_name ASC) as b ;";

                  $query = $this->db->query($query1);

                  $result_data = $query->result_array();

                  return $result_data;
        }

        function  availableSlots($participantId){

                    $query1 = "SELECT * FROM virtual_fair_slots
                               WHERE participant_id = $participantId AND start_time >= NOW();";

                            $query = $this->db->query($query1);

                            $result_data = $query->result_array();

                            return $result_data;

          }

          function userSlotInfo($participantId,$userId){

            $query1 = "SELECT vf.id, vf.student_booked, vp.university_name, vp.total_registrations, vp.token_id
                        FROM virtual_fair_slots vf
                        JOIN virtual_fair_participants vp ON vp.id = vf.participant_id
                        WHERE vp.participant_type = 'UNIVERSITY' AND vf.participant_id = $participantId AND JSON_CONTAINS(vf.student_booked, '[$userId]');";

                    $query = $this->db->query($query1);

                    $result_data = $query->result_array();

                    return $result_data;

          }

          function checkSlotByUser($userId,$slotStartTime){

            $query1 = "SELECT vf.id, vf.start_time, vp.university_name FROM virtual_fair_slots vf
                        JOIN virtual_fair_participants vp ON vp.id = vf.participant_id
                        WHERE vp.participant_type = 'UNIVERSITY' AND (CAST('$slotStartTime' AS DATETIME) BETWEEN vf.start_time AND vf.end_time)  AND JSON_CONTAINS(vf.student_booked, '[$userId]');";

                    $query = $this->db->query($query1);

                    $result_data = $query->result_array();

                    return $result_data;
          }

          function preCounsellingAvailabilityCheck(){

            $query1 = "SELECT * FROM virtual_fair_participants
                       WHERE participant_type = 'PRECOUNSELLOR';";

                    $query = $this->db->query($query1);

                    $result_data = $query->result_array();

                    return $result_data;

          }

          function postCounsellingAvailabilityCheck(){

            $query1 = "SELECT * FROM virtual_fair_participants
                       WHERE participant_type = 'POSTCOUNSELLOR';";

                    $query = $this->db->query($query1);

                    $result_data = $query->result_array();

                    return $result_data;

          }

          function eduCounsellingAvailabilityCheck(){

            $query1 = "SELECT * FROM virtual_fair_participants
                       WHERE participant_type = 'EDUCOUNSELLOR';";

                    $query = $this->db->query($query1);

                    $result_data = $query->result_array();

                    return $result_data;

          }

          function queueCheck($fairId, $tableName){
            $query1 = "SELECT * FROM $tableName WHERE fair_id = $fairId AND status = 0 limit 1;";

                    $query = $this->db->query($query1);

                    $result_data = $query->result_array();

                    return $result_data;
          }

          function  virtualFairParticipantsNew($userId,$fairId){

              $query1 = "SELECT * FROM (SELECT vp.*, fm.name as fair_name, vf.id as slot_id, vf.start_time as fair_start_time, vf.end_time as fair_end_time, vf.student_booked, vf.student_attending, 'current_user_slot' AS book_type FROM virtual_fair_slots vf
                          JOIN virtual_fair_participants vp ON vp.id = vf.participant_id
                          JOIN fair_master fm ON fm.id = vp.virtual_fair_id
                          WHERE vp.start_time <= NOW() AND vp.participant_type IN ('UNIVERSITY','GROUP') AND vp.virtual_fair_id = $fairId
                          AND now() BETWEEN vf.start_time AND vf.end_time AND JSON_CONTAINS(vf.student_booked, '[$userId]') GROUP BY vp.id
                          ORDER BY university_name ASC) as a
                          UNION
                          SELECT * FROM (SELECT vp.*, fm.name as fair_name, vf.id as slot_id,vf.start_time as fair_start_time, vf.end_time as fair_end_time, vf.student_booked, vf.student_attending, 'booked_user_slot' AS book_type  FROM virtual_fair_slots vf
                          JOIN virtual_fair_participants vp ON vp.id = vf.participant_id
                          JOIN fair_master fm ON fm.id = vp.virtual_fair_id
                          WHERE vp.start_time <= NOW() AND vp.participant_type IN ('UNIVERSITY','GROUP') AND vp.virtual_fair_id = $fairId
                          AND vf.start_time >= NOW() AND now() NOT BETWEEN vf.start_time AND vf.end_time AND JSON_CONTAINS(vf.student_booked, '[$userId]') GROUP BY vp.id ) as b
                          UNION
                          SELECT * FROM (SELECT vp.*, fm.name as fair_name, vf.id as slot_id,vf.start_time as fair_start_time, vf.end_time as fair_end_time, vf.student_booked, vf.student_attending, 'university_active_slot' AS book_type  FROM virtual_fair_slots vf
                          JOIN virtual_fair_participants vp ON vp.id = vf.participant_id
                          JOIN fair_master fm ON fm.id = vp.virtual_fair_id
                          WHERE vp.start_time <= NOW() AND vp.participant_type IN ('UNIVERSITY','GROUP') AND vp.virtual_fair_id = $fairId
                          AND now() BETWEEN vf.start_time AND vf.end_time  AND  vp.id NOT IN (SELECT participant_id FROM virtual_fair_slots WHERE JSON_CONTAINS(vf.student_booked, '[$userId]')) GROUP BY vp.id) as c
                          UNION
                          SELECT * FROM (SELECT vp.*, fm.name as fair_name, vf.id as slot_id, vf.start_time as fair_start_time, vf.end_time as fair_end_time, vf.student_booked, vf.student_attending, 'university_inactive_slot' AS book_type FROM virtual_fair_slots vf
                          JOIN virtual_fair_participants vp ON vp.id = vf.participant_id
                          JOIN fair_master fm ON fm.id = vp.virtual_fair_id
                          WHERE vp.start_time > NOW() AND vp.participant_type IN ('UNIVERSITY','GROUP') AND vp.virtual_fair_id = $fairId GROUP BY vp.id
                          ORDER BY university_name ASC) as d
                          ;";

                      $query = $this->db->query($query1);

                      $result_data = $query->result_array();

                      return $result_data;
            }

            function getExpiredSlotsCount($slotIds, $userId){
                $query1 = "SELECT count(*) AS count FROM virtual_fair_slots WHERE JSON_CONTAINS(student_booked, '[$userId]') AND (JSON_CONTAINS(student_attending, '[$userId]') != 1 OR student_attending IS NULL) AND start_time < NOW();";

                $query = $this->db->query($query1);

                $result_data = $query->row();

                return $result_data;
            }

            function getExpiredSlots($slotIds, $userId){
                $query1 = "SELECT * FROM virtual_fair_slots WHERE JSON_CONTAINS(student_booked, '[$userId]') AND (JSON_CONTAINS(student_attending, '[$userId]') != 1 OR student_attending IS NULL) AND start_time < NOW();";

                $query = $this->db->query($query1);

                $result_data = $query->result_array();

                return $result_data;
            }

            function bookedSlotReminder($userId){

              $query1 = "SELECT id, participant_id, start_time FROM virtual_fair_slots
                          WHERE JSON_CONTAINS(student_booked, '[$userId]')
                          AND start_time BETWEEN (NOW() - INTERVAL 1 MINUTE) AND (NOW() + INTERVAL 1 MINUTE) ;";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;
            }

            function uniInfoById($uniId){

              $query1 = "SELECT name,logo FROM universities WHERE id = $uniId ;";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;
            }

            function fairRegistrationCount($fairId){

              $query1 = "SELECT fm.name AS fair_name, count(wlm.id) AS total_count FROM hellouni_live.webinar_log_master wlm
                          JOIN fair_master fm ON fm.id = wlm.fair_id
                          WHERE wlm.fair_id = $fairId group by wlm.fair_id;";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;
            }

            function fairLiveStudentCount($fairId){

              $query1 = "SELECT vf.student_booked, vf.student_attending, vf.participant_id FROM virtual_fair_slots vf
                          JOIN virtual_fair_participants vp ON vp.id = vf.participant_id
                          WHERE  vp.virtual_fair_id = $fairId;";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;
            }

            function totalFairSession($fairId){

              $query1 = "SELECT count(vf.id) AS total_count FROM virtual_fair_slots vf
                          JOIN virtual_fair_participants vp ON vp.id = vf.participant_id
                          WHERE vp.virtual_fair_id = $fairId AND vf.end_time <= now() ;";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;
            }

            function totalCounselling($fairId){

              $query1 = "SELECT count(id) as total_count,status,'pre' AS c_type FROM virtualfailr_counselor_booking WHERE fair_id = $fairId
                          GROUP BY status
                          UNION
                          SELECT count(id) as total_count,status,'post' AS c_type FROM virtualfailr_edu_counselor_booking WHERE fair_id = $fairId
                          GROUP BY status
                          UNION
                          SELECT count(id) as total_count,status,'edu' AS c_type FROM virtualfailr_post_counselor_booking WHERE fair_id = $fairId
                          GROUP BY status;";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;
            }

            function checkRegistration($userId, $virtualFairId, $participantId)
            {
                $this->db->from('virtualfair_registrations');
                $this->db->where('user_id', $userId);
                $this->db->where('participant_id', $participantId);
                $this->db->where('virtual_fair_id', $virtualFairId);
                return $this->db->get()->row();
            }

            function checkUpcomingFair(){

              $query1 = " SELECT * FROM fair_master
                          WHERE fair_date >= curdate() AND status = 1
                          ORDER BY fair_date;
                        ";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;
            }

            function userFairInfo(){

              $query1 = " SELECT um.id AS user_id FROM user_master um
                          JOIN fair_log_master flm ON flm.user_id = um.id
                          WHERE flm.fair_id = 1  AND um.login_flag = 1;
                        ";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;
            }

            function checkEduloansUser($userMobile,$loanMelaDay){

              $query1 = " SELECT * FROM eduloans_fair_user
                          WHERE mobile_number = '$userMobile'  AND loan_mela_day = '$loanMelaDay';
                        ";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;

            }

            function imperialEventUserCheck($userEmail,$userMobile) {

              $this->db->select('*');
              $this->db->from('imperial_event_data');
              $this->db->where("(email = '$userEmail' OR phone = '$userMobile')");
              $query = $this->db->get();
              $result = $query->result_array();

              return $result;
            }

}
