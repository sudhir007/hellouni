<?php


class Common_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }


     function post($data, $tableName) {

    $insert_query = $this->db->insert($tableName, $data);

    return "1";
    //$error = $this->db->error();
    $lastInsertedId = 0;
    if ($insert_query)
    {
        $lastInsertedId = $this->db->insert_id();
    }

    $where = ['id' => $lastInsertedId];
    $data = $this->get($where, $tableName);



    return [
        "data" => $data,
        "last_inserted_id" => $lastInsertedId
    ];
}

   function get($where = array(), $tableName) {

        $this->db->select('*');
        $this->db->from($tableName);
        $this->db->where($where);

        $query = $this->db->get();
        $data = $query->result_array();

        return $response = [
            'data' => $data
        ];
    }


   function put ($where = array(), $data = array(), $tableName) {

        $this->db->where($where);
        $this->db->update($tableName, $data);
        //echo $this->db->last_query();

        $this->db->select('*');
        $this->db->from($tableName);
        $this->db->where($where);
        $query = $this->db->get();
        $data = $query->result_array();

        return $data ;
    }

    public function upsert ($wheredata = array(), $insertData = array(), $tableName) {

      $this->db->select('*');
      $this->db->from($tableName);
      $this->db->where($wheredata);
      $query = $this->db->get();
      //$error_result = $this->db->error();
      $otpData = $query->result_array();
      //return $query->num_rows();

      if($query->num_rows() === 0) {

          $insert_query = $this->db->insert($tableName, $insertData);
          //return $insert_query;
          $data = $this->db->insert_id();

      } else {

      $this->db->where($wheredata);
      $this->db->update($tableName, $insertData);

      $data = "1";

      }

        return $response = [
            'data' => $data,
            "last_inserted_id" => "1"
        //    'error' => $error_result
        ];
    }

    public function delete($where = array(), $tableName){

  		$this->db-> where($where);
      $this->db-> delete($tableName);

  		return [
        "data" => 1
  	];

  	}

     //Update data in tablename with condition
    function updateData($tableName, $data, $condition  )
      {
          $this->db->where($condition);
          $this->db->update($tableName, $data);
      }

    //select data from tablename with dynamic table
    function getAllData($tablename,$condition='',$order='')
      {
      if($condition !='')
          $this->db->where($condition);
      if($order !='')
          $this->db->order_by($order);
          return $this->db->get($tablename)->result();
      }

      //select single row from tablename
    function getSingleData($tablename,$condition)
      {
          $this->db->where($condition);
          return $this->db->get($tablename)->row();
      }


      function getAppliedUniversitiesByUser($userId,$appId)
      {
        $this->db->select('users_applied_universities.*, universities.name, universities.slug, universities.website, user_master.email, universities.banner, colleges.name AS college_name, courses.name AS course_name,
        fa.name AS facilitator_name, fa.last_name AS facilitator_last_name,fa.email AS facilitator_email, fa.phone AS facilitator_mobile, fa.type AS facilitator_type');
        $this->db->from('users_applied_universities');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('user_master', 'user_master.id = universities.user_id');
        $this->db->join('colleges', 'colleges.id = users_applied_universities.college_id', 'left');
        $this->db->join('courses', 'courses.id = users_applied_universities.course_id', 'left');
        $this->db->join('user_master fa', 'fa.id = users_applied_universities.facilitator_id', 'left');
        $this->db->where('users_applied_universities.user_id', $userId);
        $this->db->where('users_applied_universities.id', $appId);
        $result = $this->db->get();
        return $result->result_array();
      }


    /*  New Functions for student application start  */

    function studentMyApplicationList($agentId){

      $this->db->select('user_master.name AS student_name, user_master.last_name AS student_last_name, user_master.email AS student_email, user_master.phone AS student_phone, users_applied_universities.id AS application_id, universities.name AS university_name, user_master.id AS user_id, users_applied_universities.status AS status, partners.other_details AS agency_name, um.name AS facilitator_name, um.email AS facilitator_email, oum.name AS originator_name');
        $this->db->from('users_applied_universities');
        $this->db->join('user_master', 'user_master.id = users_applied_universities.user_id');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('student_details', 'student_details.user_id = users_applied_universities.user_id');
        $this->db->join('user_master AS um', 'um.id = users_applied_universities.facilitator_id', 'left');
        $this->db->join('user_master AS oum', 'oum.id = student_details.originator_id');
        $this->db->join('partners', 'partners.user_id = student_details.originator_id');

        $this->db->where("users_applied_universities.facilitator_id = $agentId ");

        $this->db->order_by('users_applied_universities.id', 'ASC');

        return $this->db->get()->result();

    }

    function studentAllApplicationList(){

      $this->db->select('user_master.name AS student_name, user_master.last_name AS student_last_name, user_master.email AS student_email, user_master.phone AS student_phone, users_applied_universities.id AS application_id, universities.name AS university_name, user_master.id AS user_id, users_applied_universities.status AS status, partners.other_details AS agency_name, um.name AS facilitator_name, um.email AS facilitator_email, oum.name AS originator_name');
        $this->db->from('users_applied_universities');
        $this->db->join('user_master', 'user_master.id = users_applied_universities.user_id');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('student_details', 'student_details.user_id = users_applied_universities.user_id');
        $this->db->join('user_master AS um', 'um.id = users_applied_universities.facilitator_id', 'left');
        $this->db->join('user_master AS oum', 'oum.id = student_details.originator_id');
        $this->db->join('partners', 'partners.user_id = student_details.originator_id');
        $this->db->order_by('users_applied_universities.id', 'ASC');

        return $this->db->get()->result();

    }

    function studentSubmitApplicationListNew($agentId){

      $this->db->select('user_master.name AS student_name, user_master.last_name AS student_last_name, user_master.email AS student_email, user_master.phone AS student_phone, users_applied_universities.id AS application_id, universities.name AS university_name, user_master.id AS user_id, users_applied_universities.status AS status, partners.other_details AS agency_name, um.name AS facilitator_name, um.email AS facilitator_email, oum.name AS originator_name');
        $this->db->from('users_applied_universities');
        $this->db->join('user_master', 'user_master.id = users_applied_universities.user_id');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('student_details', 'student_details.user_id = users_applied_universities.user_id');
        $this->db->join('user_master AS um', 'um.id = users_applied_universities.facilitator_id', 'left');
        $this->db->join('user_master AS oum', 'oum.id = student_details.originator_id');
        $this->db->join('partners', 'partners.user_id = student_details.originator_id');

        $this->db->where('users_applied_universities.visit_profile = 0', NULL, FALSE);
        $this->db->where("users_applied_universities.facilitator_id = $agentId ");
        $this->db->order_by('users_applied_universities.id', 'ASC');

        return $this->db->get()->result();

    }

    function studentSubmitApplicationListOld($agentId){

      $this->db->select('user_master.name AS student_name, user_master.last_name AS student_last_name, user_master.email AS student_email, user_master.phone AS student_phone, users_applied_universities.id AS application_id, universities.name AS university_name, user_master.id AS user_id, users_applied_universities.status AS status, partners.other_details AS agency_name, um.name AS facilitator_name, um.email AS facilitator_email, oum.name AS originator_name');
        $this->db->from('users_applied_universities');
        $this->db->join('user_master', 'user_master.id = users_applied_universities.user_id');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('student_details', 'student_details.user_id = users_applied_universities.user_id');
        $this->db->join('user_master AS um', 'um.id = users_applied_universities.facilitator_id', 'left');
        $this->db->join('user_master AS oum', 'oum.id = student_details.originator_id');
        $this->db->join('partners', 'partners.user_id = student_details.originator_id');

        $this->db->where('users_applied_universities.visit_profile = 1', NULL, FALSE);
        $this->db->where("users_applied_universities.facilitator_id = $agentId ");
        $this->db->order_by('users_applied_universities.comment_update_date', 'DESC');
        $this->db->order_by('users_applied_universities.id', 'ASC');

        return $this->db->get()->result();

    }

    function studentSubmitApplicationList(){

      $this->db->select('user_master.name AS student_name, user_master.last_name AS student_last_name, user_master.email AS student_email, user_master.phone AS student_phone, users_applied_universities.id AS application_id, universities.name AS university_name, user_master.id AS user_id, users_applied_universities.status AS status, partners.other_details AS agency_name, um.name AS facilitator_name, um.email AS facilitator_email, oum.name AS originator_name');
        $this->db->from('users_applied_universities');
        $this->db->join('user_master', 'user_master.id = users_applied_universities.user_id');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('student_details', 'student_details.user_id = users_applied_universities.user_id');
        $this->db->join('user_master AS um', 'um.id = users_applied_universities.facilitator_id', 'left');
        $this->db->join('user_master AS oum', 'oum.id = student_details.originator_id');
        $this->db->join('partners', 'partners.user_id = student_details.originator_id');

        $this->db->where('users_applied_universities.status = "APPLICATION_UNDER_REVIEW"');
        $this->db->where('users_applied_universities.facilitator_id is NULL', NULL, FALSE);
        $this->db->order_by('users_applied_universities.id', 'ASC');

        return $this->db->get()->result();

    }

    function studentNewApplicationList(){

      $this->db->select('user_master.name AS student_name, user_master.last_name AS student_last_name, user_master.email AS student_email, user_master.phone AS student_phone, users_applied_universities.id AS application_id, universities.name AS university_name, user_master.id AS user_id, users_applied_universities.status AS status, partners.other_details AS agency_name, um.name AS facilitator_name, um.email AS facilitator_email, oum.name AS originator_name');
        $this->db->from('users_applied_universities');
        $this->db->join('user_master', 'user_master.id = users_applied_universities.user_id');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('student_details', 'student_details.user_id = users_applied_universities.user_id');
        $this->db->join('user_master AS um', 'um.id = users_applied_universities.facilitator_id', 'left');
        $this->db->join('user_master AS oum', 'oum.id = student_details.originator_id');
        $this->db->join('partners', 'partners.user_id = student_details.originator_id');

        $this->db->where('users_applied_universities.facilitator_id is NULL', NULL, FALSE);
        $this->db->order_by('users_applied_universities.id', 'ASC');

        return $this->db->get()->result();

    }

    /*  New Functions for student application end  */


    /* Server side datatables for newlist start */

    var $column_order = array('users_applied_universities.id','user_master.name','users_applied_universities.status','partners.other_details','um.name','um.email','universities.name',null); //set column field database for datatable orderable
    var $column_search = array('users_applied_universities.id','user_master.name','users_applied_universities.status','partners.other_details','um.name','um.email','universities.name'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('users_applied_universities.id' => 'desc'); // default order

    private function _get_datatables_query_studentNewApplicationList()
    {
        $this->db->select('user_master.name AS student_name, user_master.last_name AS student_last_name, user_master.email AS student_email, user_master.phone AS student_phone, users_applied_universities.id AS application_id, universities.name AS university_name, user_master.id AS user_id, users_applied_universities.status AS status, partners.other_details AS agency_name, um.name AS facilitator_name, um.email AS facilitator_email, oum.name AS originator_name');
        $this->db->from('users_applied_universities');
        $this->db->join('user_master', 'user_master.id = users_applied_universities.user_id');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('student_details', 'student_details.user_id = users_applied_universities.user_id');
        $this->db->join('user_master AS um', 'um.id = users_applied_universities.facilitator_id', 'left');
        $this->db->join('user_master AS oum', 'oum.id = student_details.originator_id');
        $this->db->join('partners', 'partners.user_id = student_details.originator_id');

        $this->db->where('users_applied_universities.facilitator_id is NULL', NULL, FALSE);
        $this->db->order_by('users_applied_universities.id', 'ASC');

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //if(count($this->column_search) - 1 == $i) //last loop
                   // $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_studentNewApplicationList()
    {
        $this->_get_datatables_query_studentNewApplicationList();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        //print_r($query); exit;
        return $query->result();
    }

    function count_filtered_studentNewApplicationList()
    {
        $this->_get_datatables_query_studentNewApplicationList();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_studentNewApplicationList()
    {
        $this->db->select('user_master.name AS student_name, user_master.last_name AS student_last_name, user_master.email AS student_email, user_master.phone AS student_phone, users_applied_universities.id AS application_id, universities.name AS university_name, user_master.id AS user_id, users_applied_universities.status AS status, partners.other_details AS agency_name, um.name AS facilitator_name, um.email AS facilitator_email, oum.name AS originator_name');
        $this->db->from('users_applied_universities');
        $this->db->join('user_master', 'user_master.id = users_applied_universities.user_id');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('student_details', 'student_details.user_id = users_applied_universities.user_id');
        $this->db->join('user_master AS um', 'um.id = users_applied_universities.facilitator_id', 'left');
        $this->db->join('user_master AS oum', 'oum.id = student_details.originator_id');
        $this->db->join('partners', 'partners.user_id = student_details.originator_id');

        $this->db->where('users_applied_universities.facilitator_id is NULL', NULL, FALSE);
        $this->db->order_by('users_applied_universities.id', 'ASC');
        
        return $this->db->count_all_results();
    }

    /* Server side datatables for newlist end */

    /* Server side datatables for alllist start */

    private function _get_datatables_query_studentAllApplicationList()
    {
        $this->db->select('user_master.name AS student_name, user_master.last_name AS student_last_name, user_master.email AS student_email, user_master.phone AS student_phone, users_applied_universities.id AS application_id, universities.name AS university_name, user_master.id AS user_id, users_applied_universities.status AS status, partners.other_details AS agency_name, um.name AS facilitator_name, um.email AS facilitator_email, oum.name AS originator_name');
        $this->db->from('users_applied_universities');
        $this->db->join('user_master', 'user_master.id = users_applied_universities.user_id');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('student_details', 'student_details.user_id = users_applied_universities.user_id');
        $this->db->join('user_master AS um', 'um.id = users_applied_universities.facilitator_id', 'left');
        $this->db->join('user_master AS oum', 'oum.id = student_details.originator_id');
        $this->db->join('partners', 'partners.user_id = student_details.originator_id');
        $this->db->order_by('users_applied_universities.id', 'ASC');

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //if(count($this->column_search) - 1 == $i) //last loop
                   // $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_studentAllApplicationList()
    {
        $this->_get_datatables_query_studentAllApplicationList();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        //print_r($query); exit;
        return $query->result();
    }

    function count_filtered_studentAllApplicationList()
    {
        $this->_get_datatables_query_studentAllApplicationList();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_studentAllApplicationList()
    {
        $this->db->select('user_master.name AS student_name, user_master.last_name AS student_last_name, user_master.email AS student_email, user_master.phone AS student_phone, users_applied_universities.id AS application_id, universities.name AS university_name, user_master.id AS user_id, users_applied_universities.status AS status, partners.other_details AS agency_name, um.name AS facilitator_name, um.email AS facilitator_email, oum.name AS originator_name');
        $this->db->from('users_applied_universities');
        $this->db->join('user_master', 'user_master.id = users_applied_universities.user_id');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('student_details', 'student_details.user_id = users_applied_universities.user_id');
        $this->db->join('user_master AS um', 'um.id = users_applied_universities.facilitator_id', 'left');
        $this->db->join('user_master AS oum', 'oum.id = student_details.originator_id');
        $this->db->join('partners', 'partners.user_id = student_details.originator_id');
        $this->db->order_by('users_applied_universities.id', 'ASC');
        
        return $this->db->count_all_results();
    }

    /* Server side datatables for alllist end */

    /* Server side datatables for new app list start */

    private function _get_datatables_query_studentMyApplicationListNew($agentId)
    {
        $this->db->select('user_master.name AS student_name, user_master.last_name AS student_last_name, user_master.email AS student_email, user_master.phone AS student_phone, users_applied_universities.id AS application_id, universities.name AS university_name, user_master.id AS user_id, users_applied_universities.status AS status, partners.other_details AS agency_name, um.name AS facilitator_name, um.email AS facilitator_email, oum.name AS originator_name');
        $this->db->from('users_applied_universities');
        $this->db->join('user_master', 'user_master.id = users_applied_universities.user_id');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('student_details', 'student_details.user_id = users_applied_universities.user_id');
        $this->db->join('user_master AS um', 'um.id = users_applied_universities.facilitator_id', 'left');
        $this->db->join('user_master AS oum', 'oum.id = student_details.originator_id');
        $this->db->join('partners', 'partners.user_id = student_details.originator_id');

        $this->db->where('users_applied_universities.visit_profile = 0', NULL, FALSE);
        $this->db->where("users_applied_universities.facilitator_id = $agentId ");
        $this->db->order_by('users_applied_universities.id', 'ASC');

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //if(count($this->column_search) - 1 == $i) //last loop
                   // $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_studentMyApplicationListNew($agentId)
    {
        $this->_get_datatables_query_studentMyApplicationListNew($agentId);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $query->result();
    }

    function count_filtered_studentMyApplicationListNew($agentId)
    {
        $this->_get_datatables_query_studentMyApplicationListNew($agentId);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_studentMyApplicationListNew($agentId)
    {
        $this->db->select('user_master.name AS student_name, user_master.last_name AS student_last_name, user_master.email AS student_email, user_master.phone AS student_phone, users_applied_universities.id AS application_id, universities.name AS university_name, user_master.id AS user_id, users_applied_universities.status AS status, partners.other_details AS agency_name, um.name AS facilitator_name, um.email AS facilitator_email, oum.name AS originator_name');
        $this->db->from('users_applied_universities');
        $this->db->join('user_master', 'user_master.id = users_applied_universities.user_id');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('student_details', 'student_details.user_id = users_applied_universities.user_id');
        $this->db->join('user_master AS um', 'um.id = users_applied_universities.facilitator_id', 'left');
        $this->db->join('user_master AS oum', 'oum.id = student_details.originator_id');
        $this->db->join('partners', 'partners.user_id = student_details.originator_id');

        $this->db->where('users_applied_universities.visit_profile = 0', NULL, FALSE);
        $this->db->where("users_applied_universities.facilitator_id = $agentId ");
        $this->db->order_by('users_applied_universities.id', 'ASC');
        
        return $this->db->count_all_results();
    }

    /* Server side datatables for new app list end */

    /* Server side datatables for old app list start */

    private function _get_datatables_query_studentMyApplicationListOld($agentId)
    {
        $this->db->select('user_master.name AS student_name, user_master.last_name AS student_last_name, user_master.email AS student_email, user_master.phone AS student_phone, users_applied_universities.id AS application_id, universities.name AS university_name, user_master.id AS user_id, users_applied_universities.status AS status, partners.other_details AS agency_name, um.name AS facilitator_name, um.email AS facilitator_email, oum.name AS originator_name');
        $this->db->from('users_applied_universities');
        $this->db->join('user_master', 'user_master.id = users_applied_universities.user_id');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('student_details', 'student_details.user_id = users_applied_universities.user_id');
        $this->db->join('user_master AS um', 'um.id = users_applied_universities.facilitator_id', 'left');
        $this->db->join('user_master AS oum', 'oum.id = student_details.originator_id');
        $this->db->join('partners', 'partners.user_id = student_details.originator_id');

        $this->db->where('users_applied_universities.visit_profile = 1', NULL, FALSE);
        $this->db->where("users_applied_universities.facilitator_id = $agentId ");
        $this->db->order_by('users_applied_universities.comment_update_date', 'DESC');
        $this->db->order_by('users_applied_universities.id', 'ASC');

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //if(count($this->column_search) - 1 == $i) //last loop
                   // $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_studentMyApplicationListOld($agentId)
    {
        $this->_get_datatables_query_studentMyApplicationListOld($agentId);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $query->result();
    }

    function count_filtered_studentMyApplicationListOld($agentId)
    {
        $this->_get_datatables_query_studentMyApplicationListOld($agentId);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_studentMyApplicationListOld($agentId)
    {
        $this->db->select('user_master.name AS student_name, user_master.last_name AS student_last_name, user_master.email AS student_email, user_master.phone AS student_phone, users_applied_universities.id AS application_id, universities.name AS university_name, user_master.id AS user_id, users_applied_universities.status AS status, partners.other_details AS agency_name, um.name AS facilitator_name, um.email AS facilitator_email, oum.name AS originator_name');
        $this->db->from('users_applied_universities');
        $this->db->join('user_master', 'user_master.id = users_applied_universities.user_id');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('student_details', 'student_details.user_id = users_applied_universities.user_id');
        $this->db->join('user_master AS um', 'um.id = users_applied_universities.facilitator_id', 'left');
        $this->db->join('user_master AS oum', 'oum.id = student_details.originator_id');
        $this->db->join('partners', 'partners.user_id = student_details.originator_id');

        $this->db->where('users_applied_universities.visit_profile = 1', NULL, FALSE);
        $this->db->where("users_applied_universities.facilitator_id = $agentId ");
        $this->db->order_by('users_applied_universities.comment_update_date', 'DESC');
        $this->db->order_by('users_applied_universities.id', 'ASC');
        
        return $this->db->count_all_results();
    }

    /* Server side datatables for old app list end */

    /* Server side datatables for submit application list start */

    private function _get_datatables_query_studentSubmitApplicationList()
    {
        $this->db->select('user_master.name AS student_name, user_master.last_name AS student_last_name, user_master.email AS student_email, user_master.phone AS student_phone, users_applied_universities.id AS application_id, universities.name AS university_name, user_master.id AS user_id, users_applied_universities.status AS status, partners.other_details AS agency_name, um.name AS facilitator_name, um.email AS facilitator_email, oum.name AS originator_name');
        $this->db->from('users_applied_universities');
        $this->db->join('user_master', 'user_master.id = users_applied_universities.user_id');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('student_details', 'student_details.user_id = users_applied_universities.user_id');
        $this->db->join('user_master AS um', 'um.id = users_applied_universities.facilitator_id', 'left');
        $this->db->join('user_master AS oum', 'oum.id = student_details.originator_id');
        $this->db->join('partners', 'partners.user_id = student_details.originator_id');

        $this->db->where('users_applied_universities.status = "APPLICATION_UNDER_REVIEW"');
        $this->db->where('users_applied_universities.facilitator_id is NULL', NULL, FALSE);
        $this->db->order_by('users_applied_universities.id', 'ASC');

        $i = 0;
    
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                //if(count($this->column_search) - 1 == $i) //last loop
                   // $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_studentSubmitApplicationList()
    {
        $this->_get_datatables_query_studentSubmitApplicationList();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        //print_r($query); exit;
        return $query->result();
    }

    function count_filtered_studentSubmitApplicationList()
    {
        $this->_get_datatables_query_studentSubmitApplicationList();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_studentSubmitApplicationList()
    {
        $this->db->select('user_master.name AS student_name, user_master.last_name AS student_last_name, user_master.email AS student_email, user_master.phone AS student_phone, users_applied_universities.id AS application_id, universities.name AS university_name, user_master.id AS user_id, users_applied_universities.status AS status, partners.other_details AS agency_name, um.name AS facilitator_name, um.email AS facilitator_email, oum.name AS originator_name');
        $this->db->from('users_applied_universities');
        $this->db->join('user_master', 'user_master.id = users_applied_universities.user_id');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('student_details', 'student_details.user_id = users_applied_universities.user_id');
        $this->db->join('user_master AS um', 'um.id = users_applied_universities.facilitator_id', 'left');
        $this->db->join('user_master AS oum', 'oum.id = student_details.originator_id');
        $this->db->join('partners', 'partners.user_id = student_details.originator_id');

        $this->db->where('users_applied_universities.status = "APPLICATION_UNDER_REVIEW"');
        $this->db->where('users_applied_universities.facilitator_id is NULL', NULL, FALSE);
        $this->db->order_by('users_applied_universities.id', 'ASC');
        
        return $this->db->count_all_results();
    }

    /* Server side datatables for submit application list end */

    // Team Details

    function getTeamsDetails(){
      $this->db->select('*');
      $this->db->from('user_master');
      $this->db->where('user_master.type IN(1,2,7)');
      $this->db->where('user_master.status = 1');
      $this->db->order_by('user_master.id', 'DESC');
      return $this->db->get()->result();
    }

    function getTeamsSigleDetails($useId){
      $this->db->select('*');
      $this->db->from('user_master');
      $this->db->join('type_master', 'type_master.type_id = user_master.type');
      $this->db->where('user_master.status = 1');
      $this->db->where("user_master.id = $useId");
      //$result = $this->db->get();
      return $this->db->get()->result();
    }


   
}

?>
