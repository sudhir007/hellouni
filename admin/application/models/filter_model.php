<?php
class Filter_model extends CI_Model
{
  function getAllAffiliatedId($id = 0)
  {

    $this->db->select('id,name');
    $this->db->from('affiliation_type_master');
    if ($id) {
      if ($id != 5) {
        $this->db->where('id', $id);
      }
    }

    return $this->db->get()->result_array();
  }

  // function getAllUniversityById($id=0){

  //   $this->db->select('id,name');
  //   $this->db->from('universities');

  //   if($id && $id != 5){
  //     $this->db->where('id', $id);
  //   }
  //   $this->db->order_by('name', 'asc');
  //   return $this->db->get()->result_array();

  // }

  // function getAllUniversityById($id = 0)
  // {
  //   $this->db->select('id, name');
  //   $this->db->from('universities');

  //   if ($id && $id != 5) {
  //     $this->db->where('universities.affiliated', $id);
  //   }

  //   $this->db->order_by('name', 'asc');
  //   return $this->db->get()->result_array();
  // }

  public function getAllUniversityById($affiliatedId = 0)
  {
    if ($affiliatedId && $affiliatedId == 5) {
      $query = $this->db->order_by('name', 'asc')->get('universities');
    } else {
      $query = $this->db->where('affiliated', $affiliatedId)->order_by('name', 'asc')
        ->get('universities');
    }

    return $query->result_array();
  }


  function getAllActiveUniversity()
  {

    $this->db->select('universities.id, universities.name, country_master.name AS country_name');
    $this->db->from('universities');
    $this->db->join('country_master', 'universities.country_id = country_master.id');
    $this->db->where('universities.active', 1);
    $this->db->order_by('universities.name', 'asc');

    //echo $this->db->last_query();
    //die();

    return $this->db->get()->result_array();
  }

  /*function getAllUniversityById($id=0){

      $this->db->select('id,name');
      $this->db->from('universities');
      if($id){
        $this->db->where('id', $id);
      }

      return $this->db->get()->result_array();

    }*/

  function getAllCampusesByUniversityId($id = 0)
  {

    $this->db->select('id,name');
    $this->db->from('campuses');
    if ($id) {
      $this->db->where('university_id', $id);
    }

    return $this->db->get()->result_array();
  }

  function getAllCollegeByCampusId($id = 0)
  {

    $this->db->select('id,name');
    $this->db->from('colleges');
    if ($id) {
      $this->db->where('campus_id', $id);
    }

    return $this->db->get()->result_array();
  }

  function getAllDepartmentByCollegeId($id = 0)
  {

    $this->db->select('id,name');
    $this->db->from('departments');
    if ($id) {
      $this->db->where('college_id', $id);
    }

    return $this->db->get()->result_array();
  }

  function getAllDegreeByDepartmentId($id = 0)
  {

    $this->db->select('distinct(degree_master.id) AS id,degree_master.name AS name');
    $this->db->from('courses');
    $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
    if ($id) {
      $this->db->where('courses.department_id', $id);
    }

    return $this->db->get()->result_array();
  }

  function getAllCoursesByDepartmentId($id = 0, $did = 0)
  {

    $this->db->select('courses.id,courses.name,degree_master.name AS dname');
    $this->db->from('courses');
    $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
    if ($id) {
      $this->db->where('courses.department_id', $id);
    }
    if ($did) {
      $this->db->where('courses.degree_id', $did);
    }

    return $this->db->get()->result_array();
  }

  //course filter
  function getFilterCourses($selectedAffiliatedId, $selectedUniversityId, $selectedCampusId, $selectedCollegeId, $selectedDepartmentId, $selectedDegreeId, $selectedCourseId)
  {

    $this->db->select('courses.*, departments.name AS department_name, degree_master.name AS degree_name, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
    $this->db->from('courses');
    $this->db->join('departments', 'departments.id = courses.department_id');
    $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
    $this->db->join('colleges', 'colleges.id = departments.college_id');
    $this->db->join('campuses', 'campuses.id = colleges.campus_id');
    $this->db->join('universities', 'universities.id = campuses.university_id');
    if ($selectedAffiliatedId) {
      if ($selectedAffiliatedId != 5) {
        $this->db->where('universities.affiliated', $selectedAffiliatedId);
      }
    }

    if ($selectedUniversityId) {
      $this->db->where('universities.id', $selectedUniversityId);
    }

    if ($selectedCampusId) {
      $this->db->where('campuses.id', $selectedCampusId);
    }

    if ($selectedCollegeId) {
      $this->db->where('colleges.id', $selectedCollegeId);
    }

    if ($selectedDegreeId) {
      $this->db->where('degree_master.id', $selectedDegreeId);
    }

    if ($selectedDepartmentId) {
      $this->db->where('departments.id', $selectedDepartmentId);
    }

    if ($selectedCourseId) {
      $this->db->where('courses.id', $selectedCourseId);
    }

    $this->db->order_by('courses.id', 'desc');
    $result = $this->db->get()->result();

    return $result;
  }

  function getFilterCoursesList($selectedUniversityId, $selectedCampusId, $selectedCollegeId, $selectedDepartmentId, $selectedDegreeId, $selectedCourseId, $databaseFieldName)
  {

    $this->db->select("courses.id, courses.name, colleges.name as college_name, degree_master.name AS degree_name, $databaseFieldName");
    $this->db->from('courses');
    $this->db->join('departments', 'departments.id = courses.department_id');
    $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
    $this->db->join('colleges', 'colleges.id = departments.college_id');
    $this->db->join('campuses', 'campuses.id = colleges.campus_id');
    $this->db->join('universities', 'universities.id = campuses.university_id');
    if ($selectedUniversityId) {
      $this->db->where('universities.id', $selectedUniversityId);
    }

    if ($selectedCampusId) {
      $this->db->where('campuses.id', $selectedCampusId);
    }

    if ($selectedCollegeId) {
      $this->db->where('colleges.id', $selectedCollegeId);
    }

    if ($selectedDepartmentId) {
      $this->db->where('departments.id', $selectedDepartmentId);
    }

    if ($selectedDegreeId) {
      $this->db->where('degree_master.id', $selectedDegreeId);
    }

    if ($selectedCourseId) {
      $this->db->where('courses.id', $selectedCourseId);
    }

    $this->db->order_by('courses.id', 'desc');
    $result = $this->db->get()->result_array();

    return $result;
  }

  // Department filter

  function getFilterDepartment($selectedUniversityId, $selectedCampusId, $selectedCollegeId, $selectedDepartmentId)
  {
    $this->db->select('departments.*, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
    $this->db->from('departments');
    $this->db->join('colleges', 'colleges.id = departments.college_id');
    $this->db->join('campuses', 'campuses.id = colleges.campus_id');
    $this->db->join('universities', 'universities.id = campuses.university_id');
    if ($selectedUniversityId) {
      $this->db->where('universities.id', $selectedUniversityId);
    }

    if ($selectedCampusId) {
      $this->db->where('campuses.id', $selectedCampusId);
    }

    if ($selectedCollegeId) {
      $this->db->where('colleges.id', $selectedCollegeId);
    }

    if ($selectedDepartmentId) {
      $this->db->where('departments.id', $selectedDepartmentId);
    }
    $this->db->order_by('departments.id', 'desc');
    $result = $this->db->get()->result();
    return $result;
  }

  //College filter

  function getFilterCollege($selectedUniversityId, $selectedCampusId, $selectedCollegeId)
  {
    $this->db->select('colleges.*, campuses.name AS campus_name, universities.name AS university_name');
    $this->db->from('colleges');
    $this->db->join('campuses', 'campuses.id = colleges.campus_id');
    $this->db->join('universities', 'universities.id = campuses.university_id');
    if ($selectedUniversityId) {
      $this->db->where('universities.id', $selectedUniversityId);
    }

    if ($selectedCampusId) {
      $this->db->where('campuses.id', $selectedCampusId);
    }

    if ($selectedCollegeId) {
      $this->db->where('colleges.id', $selectedCollegeId);
    }
    $this->db->order_by('colleges.id', 'desc');
    $result = $this->db->get()->result();
    return $result;
  }

  // campus filter
  function getFilterCampus($selectedUniversityId, $selectedCampusId)
  {
    $this->db->select('campuses.*, universities.name AS university_name');
    $this->db->from('campuses');
    $this->db->join('universities', 'universities.id = campuses.university_id');
    if ($selectedUniversityId) {
      $this->db->where('universities.id', $selectedUniversityId);
    }

    if ($selectedCampusId) {
      $this->db->where('campuses.id', $selectedCampusId);
    }
    $this->db->order_by('campuses.id', 'desc');
    $result = $this->db->get()->result();
    return $result;
  }


  /* server side datatable code for course filter start */

  var $column_order_course = array('courses.id', 'courses.name', 'colleges.name', 'degree_master.name', 'universities.name', 'courses.duration', 'courses.deadline_link', 'courses.total_fees', null, null); //set column field database for datatable orderable
  var $column_search_course = array('courses.id', 'courses.name', 'colleges.name', 'degree_master.name', 'universities.name', 'courses.duration', 'courses.deadline_link', 'courses.total_fees'); //set column field database for datatable searchable just firstname , lastname , address are searchable
  var $order_course = array('courses.id' => 'desc'); // default order

  private function _get_datatables_query_course_filter($selectedAffiliatedId, $selectedUniversityId, $selectedCampusId, $selectedCollegeId, $selectedDepartmentId, $selectedDegreeId, $selectedCourseId)
  {
    $this->db->select('courses.*, departments.name AS department_name, degree_master.name AS degree_name, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
    $this->db->from('courses');
    $this->db->join('departments', 'departments.id = courses.department_id');
    $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
    $this->db->join('colleges', 'colleges.id = departments.college_id');
    $this->db->join('campuses', 'campuses.id = colleges.campus_id');
    $this->db->join('universities', 'universities.id = campuses.university_id');
    if ($selectedAffiliatedId) {
      if ($selectedAffiliatedId != 5) {
        $this->db->where('universities.affiliated', $selectedAffiliatedId);
      }
    }

    if ($selectedUniversityId) {
      $this->db->where('universities.id', $selectedUniversityId);
    }

    if ($selectedCampusId) {
      $this->db->where('campuses.id', $selectedCampusId);
    }

    if ($selectedCollegeId) {
      $this->db->where('colleges.id', $selectedCollegeId);
    }

    if ($selectedDegreeId) {
      $this->db->where('degree_master.id', $selectedDegreeId);
    }

    if ($selectedDepartmentId) {
      $this->db->where('departments.id', $selectedDepartmentId);
    }

    if ($selectedCourseId) {
      $this->db->where('courses.id', $selectedCourseId);
    }

    $this->db->order_by('courses.id', 'desc');

    $i = 0;

    foreach ($this->column_search_course as $item) // loop column
    {
      if ($_POST['search']['value']) // if datatable send POST for search
      {

        if ($i === 0) // first loop
        {
          //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        //if(count($this->column_search) - 1 == $i) //last loop
        //$this->db->group_end(); //close bracket
      }
      $i++;
    }

    if (isset($_POST['order'])) // here order processing
    {
      $this->db->order_by($this->column_order_course[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else if (isset($this->order_course)) {
      $order = $this->order_course;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables_course_filter($selectedAffiliatedId, $selectedUniversityId, $selectedCampusId, $selectedCollegeId, $selectedDepartmentId, $selectedDegreeId, $selectedCourseId)
  {
    $this->_get_datatables_query_course_filter($selectedAffiliatedId, $selectedUniversityId, $selectedCampusId, $selectedCollegeId, $selectedDepartmentId, $selectedDegreeId, $selectedCourseId);
    if ($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered_course_filter($selectedAffiliatedId, $selectedUniversityId, $selectedCampusId, $selectedCollegeId, $selectedDepartmentId, $selectedDegreeId, $selectedCourseId)
  {
    $this->_get_datatables_query_course_filter($selectedAffiliatedId, $selectedUniversityId, $selectedCampusId, $selectedCollegeId, $selectedDepartmentId, $selectedDegreeId, $selectedCourseId);
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all_course_filter($selectedAffiliatedId, $selectedUniversityId, $selectedCampusId, $selectedCollegeId, $selectedDepartmentId, $selectedDegreeId, $selectedCourseId)
  {
    $this->db->select('courses.*, departments.name AS department_name, degree_master.name AS degree_name, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
    $this->db->from('courses');
    $this->db->join('departments', 'departments.id = courses.department_id');
    $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
    $this->db->join('colleges', 'colleges.id = departments.college_id');
    $this->db->join('campuses', 'campuses.id = colleges.campus_id');
    $this->db->join('universities', 'universities.id = campuses.university_id');
    if ($selectedAffiliatedId) {
      if ($selectedAffiliatedId != 5) {
        $this->db->where('universities.affiliated', $selectedAffiliatedId);
      }
    }

    if ($selectedUniversityId) {
      $this->db->where('universities.id', $selectedUniversityId);
    }

    if ($selectedCampusId) {
      $this->db->where('campuses.id', $selectedCampusId);
    }

    if ($selectedCollegeId) {
      $this->db->where('colleges.id', $selectedCollegeId);
    }

    if ($selectedDegreeId) {
      $this->db->where('degree_master.id', $selectedDegreeId);
    }

    if ($selectedDepartmentId) {
      $this->db->where('departments.id', $selectedDepartmentId);
    }

    if ($selectedCourseId) {
      $this->db->where('courses.id', $selectedCourseId);
    }

    $this->db->order_by('courses.id', 'desc');

    return $this->db->count_all_results();
  }

  /* server side datatable code for course filter start */
}
