<?php
class Event_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_mapped_url()
    {
        $this->db->from('sms_url_mapping');
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function truncate_table()
    {
        $this->db->truncate('sms_url_mapping');
    }

    function add_mapped_url($data)
    {
        $this->db->insert('sms_url_mapping', $data);
    }
}
