<?php
class Virtualfair_model extends CI_Model
{
    function getAllVirtualFairs()
    {
        $this->db->from('fair_master');
        return $this->db->get()->result();
    }

    function getFairById($id)
    {
        $this->db->from('fair_master');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    function getFairByUniversityId($universityId)
    {
        $this->db->from('fair_master');
        $this->db->join('virtual_fair_participants', 'virtual_fair_participants.virtual_fair_id = fair_master.id');
        $this->db->where('virtual_fair_participants.university_id', $universityId);
        return $this->db->get()->row();
    }

    function addToken($tokenData)
    {
        $this->db->insert('tokbox_token', $tokenData);
        return $this->db->insert_id();
    }

    function editToken($tokenData, $tokenId)
    {
        $this->db->where('id', $tokenId);
        $this->db->update('tokbox_token', $tokenData);
    }

    function getTokenByParticipant($participants)
    {
        $query_string = "SELECT * FROM tokbox_token WHERE JSON_CONTAINS(participants, '" . $participants . "') ORDER BY id DESC LIMIT 1";
        //echo $query_string;
        $query = $this->db->query($query_string);
        return $query->row();
    }

    function getHelp()
    {
        $this->db->from('virtual_fair_help');
        $this->db->where('status', '0');
        $this->db->order_by('id', 'asc');
        return $this->db->get()->result_array();
    }

    function updateHelp($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('virtual_fair_help', $data);
    }

    function getHelpById($id)
    {
        $this->db->from('virtual_fair_help');
        $this->db->where('id', $id);
        $this->db->order_by('id', 'asc');
        return $this->db->get()->row();
    }

    function getUserById($id)
    {
        $this->db->from('virtual_fair_participants');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    function getAllParticipants()
    {
        $this->db->from('virtual_fair_participants');
        return $this->db->get()->result_array();
    }
}
?>
