<?php
class Dashboard_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getVisitors()
    {
        $this->db->select('date, count(*) AS count');
        $this->db->from('visitors');
        $this->db->group_by('date');
        $result=$this->db->get()->result_array();
		return $result;
    }

    function getRegisterations()
    {
        $this->db->select('date(createdate) AS date, count(*) AS count');
        $this->db->from('user_master');
        $this->db->where('type', 5);
        $this->db->group_by('date(createdate)');
        $result=$this->db->get()->result_array();
		return $result;
    }

    function getUniversities()
    {
        $this->db->select('date(date_created) AS date, count(*) AS count');
        $this->db->from('universities');
        $this->db->where('date_created IS NOT NULL');
        $this->db->group_by('date(date_created)');
        $result=$this->db->get()->result_array();
		return $result;
    }

    function getSessions()
    {
        $this->db->select('date(created_at) AS date, count(*) AS count');
        $this->db->from('tokbox_token');
        $this->db->where('created_at IS NOT NULL');
        $this->db->group_by('date(created_at)');
        $result=$this->db->get()->result_array();
		return $result;
    }

    function getVisitorsByUniversity($universityId)
    {
        $this->db->select('date, count(*) AS count');
        $this->db->from('visitors');
        $this->db->where('university_id', $universityId);
        $this->db->group_by('date');
        $result=$this->db->get()->result_array();
		return $result;
    }

    function getSessionsByUniversity($universityId)
    {
        $this->db->select('date(date_created) AS date, count(*) AS count');
        $this->db->from('slots');
        $this->db->where('date_created IS NOT NULL');
        $this->db->where('university_id', $universityId);
        $this->db->group_by('date(date_created)');
        $result=$this->db->get()->result_array();
		return $result;
    }

    function getUniversitiesByCounselor($counselorId)
    {
        $this->db->select('date(universities.date_created) AS date, count(*) AS count');
        $this->db->from('universities');
        $this->db->join('university_to_counselor', 'university_to_counselor.university_id = universities.id');
        $this->db->where('university_to_counselor.counselor_id', $counselorId);
        $this->db->where('universities.date_created IS NOT NULL');
        $this->db->group_by('date(universities.date_created)');
        $result=$this->db->get()->result_array();
		return $result;
    }

    function getSessionsByCounselor($counselorId)
    {
        $this->db->select('date(slots.date_created) AS date, count(*) AS count');
        $this->db->from('slots');
        $this->db->join('university_to_counselor', 'slots.university_id = university_to_counselor.university_id');
        $this->db->where('university_to_counselor.counselor_id', $counselorId);
        $this->db->where('slots.date_created IS NOT NULL');
        $this->db->group_by('date(slots.date_created)');
        $result=$this->db->get()->result_array();
		return $result;
    }
}
?>
