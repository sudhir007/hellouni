<?php
class Campus_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getAllCampuses()
    {
        $this->db->select('campuses.*, universities.name AS university_name');
        $this->db->from('campuses');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->order_by('campuses.id', 'desc');
        $result=$this->db->get()->result();
		return $result;
    }

    function getCampusesByUniversity($universityId)
    {
        $this->db->select('campuses.*, universities.name AS university_name');
        $this->db->from('campuses');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->where('campuses.university_id', $universityId);
        $result=$this->db->get()->result();
		return $result;
    }

    function getCampusById($campusId)
    {
        $this->db->from('campuses');
        $this->db->where('id', $campusId);
        $result=$this->db->get()->result();
		return $result[0];
    }

    function getCampusByIdNew($campusId)
    {
      $this->db->select('campuses.*');
      $this->db->from('campuses');
      $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->where('campuses.id', $campusId);
        $result=$this->db->get()->result();
		return $result[0];
    }

    function addCampus($campusData)
    {
   	    $this->db->insert('campuses', $campusData);
        //echo $this->db->_error_message();
        //exit;
        $campusId = $this->db->insert_id();
        return $campusId;
    }

    function editCampus($campusId, $campusData)
    {
        $this->db->where('id', $campusId);
   	    $this->db->update('campuses', $campusData);
    }

    function getCampusByName($campusName, $universityId)
    {
        $this->db->from('campuses');
        $this->db->where('university_id', $universityId);
        $this->db->where('name', $campusName);
        $query=$this->db->get();
		return $query->result_array();
    }
}
?>
