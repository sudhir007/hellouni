<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 */
	 class User_model extends CI_Model {

 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


	function getUser($condition1,$condition2)
	 {
	 	if(count($condition1)>0 || count($condition2)>0  || isset($condition1) || isset($condition2))
		{
	     $this->db->where($condition1);
	     $this->db->where($condition2);
	  }


		$this->db->from('user_master');
		$this->db->join('type_master', 'type_master.type_id = user_master.type','left');
		$this->db->join('status_master', 'status_master.id = user_master.status','left');



		$this->db->select('user_master.id,user_master.name,user_master.email,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,status_master.status as status_name');
		$this->db->order_by('user_master.id', 'desc');



		$result=$this->db->get()->result();
		return $result;

	 }




	function checkUser($conditions=array())
	 {

		//echo 'aaaaaa'; die('----');
		$this->db->where($conditions);
		$this->db->from('user_master');
		$this->db->join('type_master', 'type_master.type_id = user_master.type','left');
		$this->db->join('status_master', 'status_master.id = user_master.status','left');
		$this->db->join('universities', 'universities.user_id = user_master.id','left');



		$this->db->select('user_master.id,user_master.name,user_master.email,user_master.image,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,status_master.status as status_name,user_master.flag,universities.id AS university_id,universities.slug,user_master.username,universities.country_id');



		$result=$this->db->get()->row();
		//echo $this->db->last_query();exit();
		return $result;

	 }


	 function checkUserBy_Email($conditions=array())
	 {


		$this->db->where($conditions);
		$this->db->from('infra_email_master');
		$this->db->join('infra_user_master', 'infra_email_master.id = infra_user_master.email','left');
		$this->db->join('infra_role_master', 'infra_role_master.id = infra_user_master.role_id','left');
		$this->db->join('infra_status_master', 'infra_status_master.id = infra_user_master.status_id','left');

		$this->db->select('infra_user_master.id,infra_user_master.name,infra_user_master.user_name,infra_role_master.name as role,infra_email_master.email,infra_user_master.status_id,infra_status_master.name as statusname');



		$result=$this->db->get()->row();
		return $result;

	 }


	function addRemerberme($insertData=array(),$expire)
	{

		 $this->auth_model->setUserCookie('uname',$insertData['username'], $expire);
		 $this->auth_model->setUserCookie('pwd',$insertData['password'], $expire);

		// echo $val=get_cookie('uname',TRUE); exit;
		 echo $this->input->cookie('uname', TRUE); exit;
		 //die();

	}

	function removeRemeberme()
	{

	  $this->auth_model->clearUserCookie(array('uname','pwd'));

	}


	function insertUser($insertData=array())
	 {

	 $user_master_data = array(
     'name' 		=> $insertData['name'],
	 'username' 	=> $insertData['username'],
     'email' 		=> $insertData['email'],
	 'password' 	=> $insertData['password'],
	 'type' 		=> $insertData['type'] ,
	 'createdate' 	=> $insertData['createdate'],
	 'status' 		=> $insertData['status'],
	 'notification' => $insertData['notification'],
	 'flag'			=> '1'
     );

	 $this->db->insert('user_master', $user_master_data);
	 $user_id = $this->db->insert_id();

	 $user_permission_course_data = array(
     'user_id' 	=> $user_id,
     'view' 	=> $insertData['view'],
	 'create' 	=> $insertData['create'],
	 'edit' 	=> $insertData['edit'],
	 'del' 		=> $insertData['del']
	  );

	 $this->db->insert('user_permission_course', $user_permission_course_data);
	 return 'success';

	 }


	 function insertUniversityDetails($insertData=array())
	 {

	 $user_master_data = array(
     'name' 		=> $insertData['name'],
     'email' 		=> $insertData['email'],
	 'password' 	=> $insertData['password'],
	 'type' 		=> $insertData['type'] ,
	 'createdate' 	=> $insertData['createdate'],
	 'status' 		=> $insertData['status'],
	 'notification' => $insertData['notification'],
	 'flag'			=> '1'
     );

	 $this->db->insert('user_master', $user_master_data);
	 $user_id = $this->db->insert_id();

	 $user_permission_course_data = array(
     'user_id' 	=> $user_id,
     'view' 	=> $insertData['view'],
	 'create' 	=> $insertData['create'],
	 'edit' 	=> $insertData['edit'],
	 'del' 		=> $insertData['del']
	  );

	 $this->db->insert('user_permission_course', $user_permission_course_data);
	 return 'success';

	 }

	 function insertUniDetails($insertData=array())
	 {

	 $user_master_data = array(
	     'name' 		=> $insertData['name'],
	     'email' 		=> $insertData['email'],
		 'password' 	=> $insertData['password'],
		 'type' 		=> $insertData['type'] ,
		 'createdate' 	=> $insertData['createdate'],
		 'status' 		=> $insertData['status'],
		 'notification' => $insertData['notification'],
		 'username'		=> $insertData['username']
     );


	 $this->db->insert('user_master', $user_master_data);
	 $user_id = $this->db->insert_id();

	 $user_permission_course_data = array(
     'user_id' 	=> $user_id,
     'view' 	=> $insertData['view'],
	 'create' 	=> $insertData['create'],
	 'edit' 	=> $insertData['edit'],
	 'del' 		=> $insertData['del']
 );

	 $this->db->insert('user_permission_course', $user_permission_course_data);

	 $universityData = array(
		 'user_id'	=> $user_id,
		 'country_id' => $insertData['country_id'],
		 'name' => $insertData['name'],
		 'website' => $insertData['website'],
		 'about' 		=>  $insertData['about'],
		 'establishment_year' 		=>  $insertData['establishment_year'],
		 'type' 			=>  $insertData['university_type'],
		 'carnegie_accreditation' 	=>  $insertData['carnegie_accreditation'],
		 'total_students' 		=>	$insertData['total_students'],
		 'total_students_UG' 			=>	$insertData['total_students_UG'],
		 'total_students_PG' 		=>	$insertData['total_students_PG'],
		 'total_students_international' 			=>	$insertData['total_students_international'],
		 'ranking_usa' 			=>	$insertData['ranking_usa'],
		 'ranking_world' 			=>	$insertData['ranking_world'],
		 'admission_success_rate' 			=>	$insertData['admission_success_rate'],
		 'unique_selling_points' 	=>	$insertData['unique_selling_points'],
		 'research_spending' 	=>	$insertData['research_spending'],
		 'placement_percentage' 	=>	$insertData['placement_percentage'],
		 'national_ranking_source' 	=>	$insertData['national_ranking_source'],
		 'worldwide_ranking_source' 	=>	$insertData['worldwide_ranking_source'],
		 'map_normal_view' 	=>	$insertData['map_normal_view'],
		 'map_street_view' 	=>	$insertData['map_street_view'],
		 'whatsapp_link' 	=>	$insertData['whatsapp_link'],
		 'date_created' 	=>	$insertData['date_created'],
		 'date_updated' 	=>	$insertData['date_updated'],
		 'added_by' 	=>	$insertData['added_by'],
		 'updated_by' 	=>	$insertData['updated_by'],
		 'slug'			=> str_replace(' ', '-', $insertData['name'])
	 );

	 $this->db->insert('universities', $universityData);
	 /*var_dump($this->db->last_query());
	 echo "<br>";
	 echo $this->db->_error_message();*/
	 $university_id = $this->db->insert_id();
	 return array(
		 'success' => true,
		 'university_id' => $university_id
	 );

	 }




	function insert_Representative($insertData=array())
	 {


	 $user_master_data = array(
     'name' 		=> $insertData['name'],
     'email' 		=> $insertData['email'],
	 'username' 		=> $insertData['username'],
	 'password' 	=> $insertData['password'],
	 'type' 		=> $insertData['type'] ,
	 'createdate' 	=> $insertData['createdate'],
	 'status' 		=> $insertData['status'],
	 'notification' => $insertData['notification']
     );

	 $this->db->insert('user_master', $user_master_data);
	 $user_id = $this->db->insert_id();

	 $user_permission_course_data = array(
     'user_id' 	=> $user_id,
     'view' 	=> $insertData['view'],
	 'create' 	=> $insertData['create'],
	 'edit' 	=> $insertData['edit'],
	 'del' 		=> $insertData['del']
	  );

	 $this->db->insert('user_permission_course', $user_permission_course_data);

	 $uni_to_rep_data = array(
     'university_id' 		=> $insertData['university'],
     'representative_id'	=> $user_id
     );

	 $this->db->insert('university_to_representative', $uni_to_rep_data);

	 return $user_id;


	 }


	 function getCourseByRepresentative($condition)
	 {
	  	$this->db->where($condition);

	   	$this->db->from('course_to_representative');

	 	$this->db->join('course_master', 'course_master.id = course_to_representative.course_id','left');
	 	$this->db->join('status_master', 'status_master.id = course_master.status','left');

		$this->db->select('course_master.id,course_master.name');

		$result=$this->db->get()->result();
		return $result;

	 }

	 function insertCourseToRepresentative($insertData=array())
	 {
	  	$this->db->insert('course_to_representative', $insertData);
	    return $this->db->insert_id();

	 }

	 function deleteCourseToRepresentative($condition)
	 {

		$this->db->where($condition);
	 	$this->db->delete('course_to_representative');
		return true;

	 }




	function getUserByid ($conditions){

	 $this->db->where($conditions);


	 $this->db->from('user_master');
	 $this->db->join('universities', 'universities.user_id = user_master.id','left');
	 $this->db->join('type_master', 'type_master.type_id = user_master.type','left');
	 $this->db->join('status_master', 'status_master.id = user_master.status','left');
	 $this->db->join('university_to_representative', 'university_to_representative.representative_id = user_master.id','left');
	 //$this->db->join('user_permission_course', 'user_permission_course.user_id = user_master.id','left');

	 $this->db->select('user_master.id,user_master.name,user_master.email,user_master.image,universities.about,user_master.phone,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,status_master.status as status_name,user_master.notification,universities.address,universities.country_id,user_master.username,university_to_representative.university_id');

	 $result=$this->db->get()->row();

	 return $result;
	}

	function getUniversityByid ($conditions){

	 $this->db->where($conditions);


	 $this->db->from('user_master');
	 $this->db->join('type_master', 'type_master.type_id = user_master.type','left');
	 $this->db->join('status_master', 'status_master.id = user_master.status','left');
	 $this->db->join('user_permission_course', 'user_permission_course.user_id = user_master.id','left');
	 $this->db->join('universities', 'universities.user_id = user_master.id', 'left');

	 $this->db->select('user_master.id,user_master.name,user_master.email,user_master.password,user_master.image,user_master.about,user_master.phone,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,status_master.status as status_name,user_master.notification,user_permission_course.view,user_permission_course.create,user_permission_course.edit,user_permission_course.del,universities.about as description, universities.establishment_year, universities.type as university_type, universities.carnegie_accreditation, universities.total_students, universities.total_students_UG, universities.total_students_PG, universities.total_students_international, universities.ranking_usa, universities.ranking_world, universities.admission_success_rate, universities.unique_selling_points, universities.research_spending, universities.placement_percentage, universities.country_id, universities.website, user_master.username, universities.national_ranking_source, universities.worldwide_ranking_source,universities.map_normal_view,universities.map_street_view,universities.whatsapp_link');

	 $result=$this->db->get()->row();

	 return $result;
	}

	function getUniversityDetailsByid ($conditions){

	 $this->db->where($conditions);

	 $this->db->from('university_details');
	 /*$this->db->join('user_master', 'user_master.id = university_details.university_id','left');
	 $this->db->join('status_master', 'status_master.id = user_master.status','left');*/


	 $this->db->select('university_details.university_id,university_details.about,university_details.website,university_details.banner,university_details.student,university_details.internationalstudent,university_details.facilities,university_details.address,university_details.country,university_details.state,university_details.city');

	 $result=$this->db->get()->row();

	 return $result;
	}


	function getUniversityByRepresentative ($conditions){

	 $this->db->where($conditions);

	 $this->db->from('university_to_representative');
	 $this->db->join('user_master', 'user_master.id = university_to_representative.university_id','left');
	 $this->db->join('status_master', 'status_master.id = user_master.status','left');


	 $this->db->select('university_to_representative.university_id,user_master.name');

	 $result=$this->db->get()->row();

	 return $result;
	}

	function getRepresentativeByUniversity ($conditions1,$conditions2){

	 $this->db->where($conditions1);
	 $this->db->where($conditions2);
	 $this->db->from('university_to_representative');
	 $this->db->join('user_master', 'user_master.id = university_to_representative.representative_id','left');
	 $this->db->join('status_master', 'status_master.id = user_master.status','left');


	 $this->db->select('user_master.id,university_to_representative.university_id,university_to_representative.representative_id,user_master.name,user_master.email,user_master.createdate,user_master.status,status_master.status as status_name');

	 $result=$this->db->get()->result();

	 return $result;
	}

	function editAdminUser($insertData=array())
	{
		$user_master_data = array(
		 'name' 		=> $insertData['name'],
		 'email' 		=> $insertData['email'],
		 'type' 		=> $insertData['type'] ,
		 'status' 		=> $insertData['status'],
		 'notification' => $insertData['notification']
		 );
$this->db->start_cache();
	 $this->db->where('id',$insertData['id']);
	 $this->db->update('user_master',$user_master_data);
	 $this->db->stop_cache();
	$this->db->flush_cache();

	 $this->db->from('user_permission_course');
	 $this->db->where('user_id',$insertData['id']);
	 $this->db->select('*');

	 $is_user_exists=$this->db->get()->row();
	 //print_r($is_user_exists); exit;

	 // $is_user_exists = $this->db->get_where('user_permission_course', array('user_permission_course.user_id' => $insertData['id']))->row();
	   if($is_user_exists){

		 $user_permission_course_data = array(
		 'view' 	=> $insertData['view'],
		 'create' 	=> $insertData['create'],
		 'edit' 	=> $insertData['edit'],
		 'del' 		=> $insertData['del']
		  );
	 	$this->db->where('user_id',$insertData['id']);
	 	$this->db->update('user_permission_course',$user_permission_course_data);
	   }else{

		$user_permission_course_data = array(
		 'user_id'	=> $insertData['id'],
		 'view' 	=> $insertData['view'],
		 'create' 	=> $insertData['create'],
		 'edit' 	=> $insertData['edit'],
		 'del' 		=> $insertData['del']
		  );
	 	$this->db->insert('user_permission_course', $user_permission_course_data);

	   }
	 return "success";
	}

	function editUser($insertData=array())
	 {

	 // Update User table
		$user_master_data = array(
		 'name' 		=> $insertData['name'],
		 'email' 		=> $insertData['email'],
		 'type' 		=> $insertData['type'] ,
		 'status' 		=> $insertData['status'],
		 'notification' => $insertData['notification']
		 );
		 if(isset($insertData['password']))
		 {
			 $user_master_data['password'] = $insertData['password'];
		 }
		 $this->db->start_cache();
	 $this->db->where('id',$insertData['id']);
	 $this->db->update('user_master',$user_master_data);
	 $this->db->stop_cache();
	 $this->db->flush_cache();


	 $universityData = array(
		 'user_id' => $insertData['id'],
		 'name' => $insertData['name'],
		 'country_id' => $insertData['country_id'],
		 'website' => $insertData['website'],
		 'about' 		=>  $insertData['about'],
		 'establishment_year' 		=>  $insertData['establishment_year'],
		 'type' 			=>  $insertData['university_type'],
		 'carnegie_accreditation' 	=>  $insertData['carnegie_accreditation'],
		 'total_students' 		=>	$insertData['total_students'],
		 'total_students_UG' 			=>	$insertData['total_students_UG'],
		 'total_students_PG' 		=>	$insertData['total_students_PG'],
		 'total_students_international' 			=>	$insertData['total_students_international'],
		 'ranking_usa' 			=>	$insertData['ranking_usa'],
		 'ranking_world' 			=>	$insertData['ranking_world'],
		 'admission_success_rate' 			=>	$insertData['admission_success_rate'],
		 'unique_selling_points' 	=>	$insertData['unique_selling_points'],
		 'research_spending' 	=>	$insertData['research_spending'],
		 'placement_percentage' 	=>	$insertData['placement_percentage'],
		 'national_ranking_source' 	=>	$insertData['national_ranking_source'],
		 'worldwide_ranking_source' 	=>	$insertData['worldwide_ranking_source'],
		 'map_normal_view' 	=>	$insertData['map_normal_view'],
		 'map_street_view' 	=>	$insertData['map_street_view'],
		 'whatsapp_link' 	=>	$insertData['whatsapp_link'],
		 'date_updated' 	=>	$insertData['date_updated'],
		 'added_by' 	=>	$insertData['added_by'],
		 'updated_by' 	=>	$insertData['updated_by']
	 );

	 $this->db->start_cache();
	 $this->db->select('*');
	 $this->db->where('user_id',$insertData['id']);
	 $this->db->from('universities');
	 $result = $this->db->get()->result();
	 $this->db->stop_cache();
	 $this->db->flush_cache();

	 $this->db->start_cache();
	 if($result)
	 {
		 $this->db->where('user_id',$insertData['id']);
		 $this->db->update('universities',$universityData);
	 }
	 else
	 {
		 $this->db->insert('universities',$universityData);
	 }
	 //echo $this->db->last_query();
	 $this->db->stop_cache();
	 $this->db->flush_cache();

	 $this->db->from('user_permission_course');
	 $this->db->where('user_id',$insertData['id']);
	 $this->db->select('*');

	 $is_user_exists=$this->db->get()->row();
	 //print_r($is_user_exists); exit;

	 // $is_user_exists = $this->db->get_where('user_permission_course', array('user_permission_course.user_id' => $insertData['id']))->row();
	   if($is_user_exists){

		 $user_permission_course_data = array(
		 'view' 	=> $insertData['view'],
		 'create' 	=> $insertData['create'],
		 'edit' 	=> $insertData['edit'],
		 'del' 		=> $insertData['del']
		  );
	 	$this->db->where('user_id',$insertData['id']);
	 	$this->db->update('user_permission_course',$user_permission_course_data);
	   }else{

		$user_permission_course_data = array(
		 'user_id'	=> $insertData['id'],
		 'view' 	=> $insertData['view'],
		 'create' 	=> $insertData['create'],
		 'edit' 	=> $insertData['edit'],
		 'del' 		=> $insertData['del']
		  );
	 	$this->db->insert('user_permission_course', $user_permission_course_data);

	   }
	  return 'success';
	 }

	 function editRepresentative($insertData=array())
	 {

	 // Update User table
		$user_master_data = array(
		 'name' 		=> $insertData['name'],
		 'email' 		=> $insertData['email'],
		/* 'password' 	=> $insertData['password'],*/

		 'status' 		=> $insertData['status'],
		 'notification' => $insertData['notification']
		 );

		 if($insertData['password']){
		 $user_master_data['password'] = $insertData['password'];
		 }

		 $this->db->start_cache();
	 $this->db->where('id',$insertData['id']);
	 $this->db->update('user_master',$user_master_data);
	 $this->db->stop_cache();
	 $this->db->flush_cache();

	 $this->db->start_cache();
	 $this->db->from('user_permission_course');
	 $this->db->where('user_id',$insertData['id']);
	 $this->db->select('*');

	 $is_user_exists=$this->db->get()->row();
	 $this->db->stop_cache();
	 $this->db->flush_cache();
	 //print_r($is_user_exists); exit;
	 $this->db->start_cache();

	 // $is_user_exists = $this->db->get_where('user_permission_course', array('user_permission_course.user_id' => $insertData['id']))->row();
	   if($is_user_exists){

		 $user_permission_course_data = array(
		 'view' 	=> $insertData['view'],
		 'create' 	=> $insertData['create'],
		 'edit' 	=> $insertData['edit'],
		 'del' 		=> $insertData['del']
		  );
	 	$this->db->where('user_id',$insertData['id']);
	 	$this->db->update('user_permission_course',$user_permission_course_data);
	   }else{

		$user_permission_course_data = array(
		 'user_id'	=> $insertData['id'],
		 'view' 	=> $insertData['view'],
		 'create' 	=> $insertData['create'],
		 'edit' 	=> $insertData['edit'],
		 'del' 		=> $insertData['del']
		  );
	 	$this->db->insert('user_permission_course', $user_permission_course_data);

	   }

	   $this->db->stop_cache();
	   $this->db->flush_cache();
$this->db->start_cache();
		//University to Representative relation
		 $this->db->from('university_to_representative');
		 $this->db->where('representative_id',$insertData['id']);
		 $this->db->select('*');

	     $is_uni_exists=$this->db->get()->row();
		 $this->db->stop_cache();
		 $this->db->flush_cache();

		 if($is_uni_exists){

		 $uni_to_representative_data = array(
		 'university_id' 		=> $insertData['university'],
		 'representative_id' 	=> $insertData['id']
		  );
	 	$this->db->where('representative_id',$insertData['id']);
	 	$this->db->update('university_to_representative',$uni_to_representative_data);
	   }else{
	    //die('---');
		$uni_to_representative_data = array(
		 'university_id' 		=> $insertData['university'],
		 'representative_id' 	=> $insertData['id']
		  );


	 	$this->db->insert('university_to_representative', $uni_to_representative_data);

	   }
	  return 'success';
	 }

	  function updateUser($insertData=array(),$condition){

	  $this->db->where($condition);
	  $this->db->update('user_master',$insertData);
	  return true;

	  }


	 function deleteUser($id)
	 {
	  $data = array('status' => '5');

	 $this->db->where('id',$id);
	 $this->db->update('user_master',$data);
	 return 'success';
	 }

	function deleteTrash($id)
	 {

	 $this->db->where('id', $id);
	 $this->db->delete('user_master');
	 return 'success';
	 }


	function editUniUser($insertData=array())
	{
		// Update User table
		$user_master_data = array(
			'name' 		=> $insertData['name'],
			'email' 	=> $insertData['email'],
			'phone' 	=> $insertData['phone']
		);

		$this->db->start_cache();

		$this->db->where('id',$insertData['id']);
		$this->db->update('user_master',$user_master_data);

		$this->db->stop_cache();
		$this->db->flush_cache();

		$this->db->start_cache();

		$is_user_exists = $this->db->get_where('universities', array('universities.user_id' => $insertData['id']))->row();
		//print_r($is_user_exists); exit;
		$universityDetails_data = array(
			'name' 							=> $insertData['name'],
			'user_id' 						=> $insertData['id'],
			'about' 						=> $insertData['about'],
			'website' 						=> $insertData['website'],
			'banner' 						=> $insertData['banner'],
			'logo' 							=> $insertData['logo'],
			'total_students' 				=> $insertData['student'],
			'total_students_international' 	=> $insertData['internationalstudent'] ,
			'facilities' 					=> $insertData['facilities'],
			'address' 						=> $insertData['address'],
			'country_id' 					=> $insertData['country_id'],
			'state' 						=> $insertData['state'],
			'city' 							=> $insertData['city'],
			'ranking_usa' 					=> $insertData['ranking_usa'],
			'ranking_world' 				=> $insertData['ranking_world'],
			'placement_percentage' 			=> $insertData['placement_percentage'],
			'admission_success_rate' 		=> $insertData['admission_success_rate'],
			'research_spending' 			=> $insertData['research_spending'],
			'unique_selling_points' 		=> $insertData['unique_selling_points'],
			'slug'							=> str_replace(' ', '-', $insertData['name']),
			'logo_approved' 				=> isset($insertData['logo_approved']) ? $insertData['logo_approved'] : 1,
			'slider_approved' 				=> isset($insertData['slider_approved']) ? $insertData['slider_approved'] : 1
		);

		$this->db->stop_cache();
		$this->db->flush_cache();

		if($is_user_exists)
		{
			$this->db->where('user_id',$insertData['id']);
			$this->db->update('universities',$universityDetails_data);
		}
		else
		{
			$this->db->insert('universities', $universityDetails_data);
		}
		return 'success';
	}


	 function changepassword($insertData=array()){
	 // Update User table
		$user_master_data = array('password' 	=> $insertData['password']);

	 $this->db->where('id',$insertData['id']);
	 $this->db->update('user_master',$user_master_data);

	 return 'success';
	 }


	 function getUniAddress($condition)
	 {
		$this->db->from('address_master');

		$this->db->select('address_master.id,address_master.address,address_master.country');

		$result=$this->db->get()->result();
		return $result;

	 }

	 function hasEditingPermission($id)
	 {
		$this->db->where('user_id',$id);

		$this->db->from('user_permission_course');

		$this->db->select('user_permission_course.id,user_permission_course.edit');

		$result=$this->db->get()->row();
		if($result){
			if($result->edit==1){
			 return true;
			}else if($result->edit==0){
			 return false;
			}
		}
	 }



	 function hasDeletePermission($id)
	 {
		$this->db->where('user_id',$id);

		$this->db->from('user_permission_course');

		$this->db->select('user_permission_course.id,user_permission_course.del');

		$result=$this->db->get()->row();
		if($result){
			if($result->del==1){
			 return true;
			}else if($result->del==0){
			 return false;
			}
		}
	 }


	 function getSendToEmail($type)
	{

		$query="SELECT emailid_master.`Email` FROM `mailtemplate`,`emailid_master` WHERE
emailid_master.`ID`=mailtemplate.`To` and mailtemplate.`MailType`='".$type."'";

		$result=$this->db->query($query);
		return($result->row());
	}


	function TempTop() {

 $top='<table width="963" border="2" cellspacing="0" cellpadding="0" align="center"       style="border-collapse:collapse; border-color:#5cd5f0">
		<tr>
		<td align="left" valign="top" style="padding:10px;">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="left" valign="top" style="background:#333;"><img src="'.base_url().'images/logo-small1.png" width="300" height="72" alt="" /></td></tr>
	</table>';

 return $top;

	             }

	function TempBody($username,$link,$linktext,$text) {


 $top='<table>
		<tr>
		<td align="left" valign="top" style="padding:10px;">

		<p>Hi '.$username.',</p>

		<p>'.$text.'</p>

		<p style="color:#0000CC"><a href="'.$link.'">"'.$linktext.'"</a></p>
		</td>
		</tr>
	</table>';

 return $top;

	             }


function TempBottom() {

 $bottom='<p>Thanks and Regards,</p>
              <p>Super Admin</p>
						<strong><p><a href="'.base_url().'">TripAdapt</a></p></strong></td>
						</tr>
						</table>
						</td>
						</tr>
						</table>';

                return $bottom;

	                  }








function SendCustomEmail($P_Type,$P_To,$id)
{


    if($P_Type=="CustomerMessageToAdmin")
	{

   $result2=$this->BrandInfo($id);
$result1=$this->EmailInfo($P_Type);




$from=$result1->Email;
$to=$P_To;
$subject=$result1->Subject;
$content=$result1->Message;
$body='<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						<td align="left" valign="top" style="font:normal 14px/20px Verdana, Geneva, sans-serif; color:#333; padding:20px 0 20px 0px;">
<p>Dear Administrator,</p>
<p>You have a new message from <font style="color:#0000ff">'.$result2->username.'</font></p>
						'.$content.'

';


	}





	 if($P_Type=="ChangePasswordToCustomer")
	{

   $result2=$this->BrandInfo($id);
$result1=$this->EmailInfo($P_Type);




$from=$result1->Email;
$to="sas.somnath@gmail.com";
$subject=$result1->Subject;
$content=$result1->Message;

$body='<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						<td align="left" valign="top" style="font:normal 14px/20px Verdana, Geneva, sans-serif; color:#333; padding:20px 0 20px 0px;">

						'.$content.'
						<p> Login Id:<font color="#0000ff">'.$result2->email.'</font> <br ></p>


				       <p> Password:<font color="#0000ff">'.$result2->passwrd.'</font></p>
					<strong>  </strong>

';


	}




if($P_Type=="NewQueryToAdmin"){

	$result5=$this->MsgInfoTo($id);
 $result4=$this->MsgInfo($id);

$result1=$this->EmailInfo($P_Type);

$from=$result1->Email;
$to=$P_To;
$subject=$result1->Subject;
$content=$result1->Message;
$body='<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						<td align="left" valign="top" style="font:normal 14px/20px Verdana,
<p>There is a new query from <font style="color:#0000ff">'.$result4['BrandName'].'</font> to <font style="color:#0000ff">'.$result5['BrandName'].'</font></p>
						'.$content.'';


	                                             }


	if($P_Type=="NewAdvertToAdmin"){


$result1=$this->EmailInfo($P_Type);

$from=$result1->Email;
$to=$P_To;
$subject=$result1->Subject;
$content=$result1->Message;
$body='<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						<td align="left" valign="top" style="font:normal 14px/20px Verdana, Geneva, sans-serif; color:#333; padding:20px 0 20px 0px;">
<p>Dear Administrator,</p>
<p>There is a new Advert in RangyBrands.</p>
						'.$content.'

';


	                                             }



$toid=$to;
$sub=$subject;
$fromid=$from;
$message=$this->TempTop().$body.$this->TempBottom();

				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= 'From: '.$fromid . "\r\n";

				$mail=mail($toid, $sub, $message, $headers);

return $mail;






}



	function SendMail($to,$from,$subject,$body) {

                //$to = $query->email;
				//$subject = "Plese Register First";
				//******body
				$message = "<html><head></head><body>";
				$message .= $this->TempTop();
				$message .= $body;
				$message .= $this->TempBottom();

				//$from = $this->config->item('site_admin_mail');

				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
				$headers .= "From:" . $from;
				mail($to,$subject,$message,$headers);

           }


    function CheckReset($conditions){

	$this->db->where($conditions);
	$this->db->from('infra_user_master');
	$this->db->select('*');

	 $result=$this->db->get()->row();

	 return $result;

	}

 function getSecurity_Credential($conditions){

	$this->db->where($conditions);
	$this->db->from('user_security_question');
	$this->db->select('*');

	 $result=$this->db->get()->row();

	 return $result;

	}

	function getUniversityDataByUserid($userId)
 	{
 		$this->db->select('universities.*, country_master.name AS country_name, user_master.email AS email, user_master.image AS image');
         $this->db->from('universities', 'universities.id = campuses.university_id');
 		$this->db->join('country_master', 'universities.country_id = country_master.id');
		$this->db->join('user_master', 'universities.user_id = user_master.id');
 		$this->db->where('universities.user_id', $userId);
         $result=$this->db->get()->result();
 		return $result[0];
 	}

	function getSlotByUniversity($universityLoginId, $slotType="UNIVERSITY")
	{

		$this->db->from('slots');
		$this->db->join('user_master', 'slots.user_id = user_master.id');
		$this->db->join('student_details', 'slots.user_id = student_details.user_id');
		if($universityLoginId)
		{
			$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id');
			$this->db->where('slots.university_login_id', $universityLoginId);
		}
		else
		{
			$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id,universities.name AS university_name');
			$this->db->join('universities', 'universities.id = slots.university_id');
		}
		$this->db->where('slots.status != "Deleted"');
		$this->db->where('slots.slot_type', $slotType);
		$this->db->order_by('slots.date_created', 'DESC');
		$result=$this->db->get()->result();
	   	return $result;
	}

	function getComingSlotByUniversity($universityLoginId, $slotType = 'UNIVERSITY')
	{
		//$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id');
		$this->db->from('slots');
		$this->db->join('user_master', 'slots.user_id = user_master.id');
		$this->db->join('student_details', 'slots.user_id = student_details.user_id');
		$this->db->where('slots.status != "Deleted"');
		if($universityLoginId)
		{
			$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id');
			$this->db->where('slots.university_login_id', $universityLoginId);
		}
		else
		{
			$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id,universities.name AS university_name');
			$this->db->join('universities', 'universities.id = slots.university_id');
		}
		$this->db->where('slots.slot_type', $slotType);
		$this->db->where('slots.confirmed_slot >= now()');
		$this->db->order_by('slots.confirmed_slot', 'ASC');
		$result=$this->db->get()->result();
	   	return $result;
	}

	function getCompletedSlotByUniversity($universityLoginId, $slotType = 'UNIVERSITY')
	{
		//$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id');
		$this->db->from('slots');
		$this->db->join('user_master', 'slots.user_id = user_master.id');
		$this->db->join('student_details', 'slots.user_id = student_details.user_id');
		$this->db->where('slots.status != "Deleted"');
		if($universityLoginId)
		{
			$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id');
			$this->db->where('slots.university_login_id', $universityLoginId);
		}
		else
		{
			$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id,universities.name AS university_name');
			$this->db->join('universities', 'universities.id = slots.university_id');
		}
		$this->db->where('slots.slot_type', $slotType);
		$this->db->where('slots.confirmed_slot < now()');
		$this->db->order_by('slots.confirmed_slot', 'DESC');
		$result=$this->db->get()->result();
	   	return $result;
	}

	function getUnassignedSlotByUniversity($universityLoginId, $slotType = 'UNIVERSITY')
	{
		//$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id, universities.name AS university_name');
		$this->db->from('slots');
		$this->db->join('user_master', 'slots.user_id = user_master.id');
		$this->db->join('student_details', 'slots.user_id = student_details.user_id');
		//$this->db->join('universities', 'universities.id = slots.university_id');
		$this->db->where('slots.status != "Deleted"');
		if($universityLoginId)
		{
			$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id');
			$this->db->where('slots.university_login_id', $universityLoginId);
		}
		else
		{
			$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id,universities.name AS university_name');
			$this->db->join('universities', 'universities.id = slots.university_id');
		}
		$this->db->where('slots.slot_type', $slotType);
		$this->db->where('slots.slot_1 IS NULL');
		$this->db->where('slots.slot_2 IS NULL');
		$this->db->where('slots.slot_3 IS NULL');
		$this->db->order_by('slots.date_created', 'ASC');
		$result=$this->db->get()->result();
	   	return $result;
	}

	function getUnconfirmedSlotByUniversity($universityLoginId, $slotType = 'UNIVERSITY')
	{
		if($universityLoginId)
		{
			$query_string = "SELECT slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id
			FROM slots
			JOIN user_master ON slots.user_id = user_master.id
			JOIN student_details ON slots.user_id = student_details.user_id
			WHERE slots.status != 'Deleted' AND slots.university_login_id = " . $universityLoginId . "
			AND slots.slot_type = '" . $slotType . "' AND (slots.slot_1 IS NOT NULL OR slots.slot_2 IS NOT NULL OR slots.slot_3 IS NOT NULL) AND slots.confirmed_slot IS NULL ORDER BY slots.date_created DESC";
		}
		else
		{
			$query_string = "SELECT slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id, universities.name AS university_name
			FROM slots
			JOIN user_master ON slots.user_id = user_master.id
			JOIN student_details ON slots.user_id = student_details.user_id
			JOIN universities ON universities.id = slots.university_id
			WHERE slots.status != 'Deleted' AND slots.slot_type = '" . $slotType . "' AND (slots.slot_1 IS NOT NULL OR slots.slot_2 IS NOT NULL OR slots.slot_3 IS NOT NULL) AND slots.confirmed_slot IS NULL ORDER BY slots.date_created DESC";
		}
        $query = $this->db->query($query_string);
        return $query->result();
	}

	function preDefineList($universityLoginId){

		if($universityLoginId)
		{
			$query_string = "SELECT predefine_slots.*
			FROM predefine_slots
			WHERE university_id = $universityLoginId AND status = 1 ;";
			} else {
			$query_string = "SELECT predefine_slots.*, universities.name AS university_name
			FROM predefine_slots
			JOIN universities ON universities.id = predefine_slots.university_id
			WHERE predefine_slots.status = 1
			;";
			}
				$query = $this->db->query($query_string);
				return $query->result();

	}

	function getSlotById($slotId)
	{
		$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id');
		$this->db->from('slots');
		$this->db->join('user_master', 'slots.user_id = user_master.id');
		$this->db->join('student_details', 'slots.user_id = student_details.user_id');
		$this->db->where('slots.id', $slotId);
		$result=$this->db->get()->result();
	   	return $result;
	}

	function editSlot($slotId, $updatedData)
	{
		$this->db->where('id', $slotId);
		$this->db->update('slots', $updatedData);
	}

	function getTokenByParticipant($participants)
    {
        $query_string = "SELECT * FROM tokbox_token WHERE JSON_CONTAINS(participants, '" . $participants . "') ORDER BY id DESC LIMIT 1";
        //echo $query_string;
        $query = $this->db->query($query_string);
        return $query->row();
    }

	function checkUserByUsername($username)
	{
		$this->db->select('universities.*');
		$this->db->from('user_master');
		$this->db->join('universities', 'universities.user_id = user_master.id', 'left');
		$this->db->where('user_master.username', $username);
		$query = $this->db->get();
		return $query->result_array();
	}

	function checkUserByEmail($email)
	{
		$this->db->select('universities.*');
		$this->db->from('user_master');
		$this->db->join('universities', 'universities.user_id = user_master.id', 'left');
		$this->db->where('user_master.email', $email);
		$query = $this->db->get();
		return $query->result_array();
	}

	function addRecords($records)
	{
		$this->db->insert('hellouni_sheet', $records);
		return $this->db->insert_id();
	}

	function updateRecords($records, $recordId)
	{
		$this->db->where('id', $recordId);
		$this->db->update('hellouni_sheet', $records);
	}

	function getUserMaster($unameOrEmail)
	{
		$this->db->from('user_master');
		$this->db->where('username', $unameOrEmail);
		$this->db->or_where('email', $unameOrEmail);
		$result = $this->db->get();
		return $result->result_array();
	}

	function addForgotPassword($data)
	{
		$this->db->insert('forgot_password', $data);
 	   	return $id = $this->db->insert_id();
	}

	function updateForgotPassword($data, $userId)
	{
		$this->db->where('user_id', $userId);
		$this->db->update('forgot_password', $data);
	}

	function getForgotPasswordByUserId($userId)
	{
		$this->db->from('forgot_password');
		$this->db->where('user_id', $userId);
		$result = $this->db->get();
		return $result->result_array();
	}

	function getForgotPasswordByToken($token)
	{
		$this->db->from('forgot_password');
		$this->db->where('token', $token);
		$result = $this->db->get();
		return $result->result_array();
	}

	function updateUserInfo($insertData=array(),$id)
	{
		$this->db->where('id',$id);
		$this->db->update('user_master',$insertData);
		return true;

	}

	function getCountriesByIds($countriesId)
	{
		$this->db->select('name');
		$this->db->from('country_master');
		$this->db->where_in('id', $countriesId);
		$result = $this->db->get();
		return $result->result_array();
	}

	function getCounselorByUniversity ($conditions1, $conditions2)
	{
		$this->db->where($conditions1);
		$this->db->where($conditions2);
		$this->db->from('university_to_counselor');
		$this->db->join('user_master', 'user_master.id = university_to_counselor.counselor_id');
		$this->db->join('status_master', 'status_master.id = user_master.status');
		$this->db->select('user_master.id, university_to_counselor.university_id, university_to_counselor.counselor_id, user_master.name, user_master.email, user_master.createdate, user_master.status, status_master.status as status_name');

		$result=$this->db->get()->result();

		return $result;
	}

	function addCounselor($insertData=array())
	{
		$user_master_data = array(
			'name' => $insertData['name'],
			'email' => $insertData['email'],
			'username' => $insertData['username'],
			'password' => $insertData['password'],
			'type' => $insertData['type'] ,
			'createdate' => $insertData['createdate'],
			'status' => $insertData['status'],
			'notification' => $insertData['notification']
		);

		$this->db->insert('user_master', $user_master_data);
		$user_id = $this->db->insert_id();

		$user_permission_course_data = array(
			'user_id' => $user_id,
			'view' => $insertData['view'],
			'create' => $insertData['create'],
			'edit' => $insertData['edit'],
			'del' => $insertData['del']
		);

		$this->db->insert('user_permission_course', $user_permission_course_data);

		/*$uni_to_counselor_data = array(
			'university_id' => $insertData['university'],
			'counselor_id'	=> $user_id
		);

		$this->db->insert('university_to_counselor', $uni_to_counselor_data);*/

		return $user_id;
	}

	function editCounselor($insertData=array())
	{
		$user_master_data = array(
			'name' => $insertData['name'],
			'email' => $insertData['email'],
			'status' => $insertData['status'],
			'notification' => $insertData['notification']
		);

		$this->db->start_cache();
		$this->db->where('id', $insertData['id']);
		$this->db->update('user_master', $user_master_data);
		$this->db->stop_cache();
		$this->db->flush_cache();

		$this->db->start_cache();
		$this->db->from('user_permission_course');
		$this->db->where('user_id',$insertData['id']);
		$this->db->select('*');

		$is_user_exists=$this->db->get()->row();
		$this->db->stop_cache();
		$this->db->flush_cache();
		$this->db->start_cache();
		if($is_user_exists)
		{
			$user_permission_course_data = array(
				'view' 	=> $insertData['view'],
				'create' 	=> $insertData['create'],
				'edit' 	=> $insertData['edit'],
				'del' 		=> $insertData['del']
			);
			$this->db->where('user_id',$insertData['id']);
			$this->db->update('user_permission_course',$user_permission_course_data);
		}
		else
		{
			$user_permission_course_data = array(
				'user_id'	=> $insertData['id'],
				'view' 	=> $insertData['view'],
				'create' 	=> $insertData['create'],
				'edit' 	=> $insertData['edit'],
				'del' 		=> $insertData['del']
			);
			$this->db->insert('user_permission_course', $user_permission_course_data);
		}

		$this->db->stop_cache();
		$this->db->flush_cache();
		/*$this->db->start_cache();

		//University to Representative relation
		$this->db->from('university_to_counselor');
		$this->db->where('counselor_id', $insertData['id']);
		$this->db->select('*');

		$is_uni_exists=$this->db->get()->row();
		$this->db->stop_cache();
		$this->db->flush_cache();

		if($is_uni_exists)
		{
			$uni_to_counselor_data = array(
				'university_id' => $insertData['university'],
				'counselor_id' 	=> $insertData['id']
			);
			$this->db->where('counselor_id',$insertData['id']);
			$this->db->update('university_to_counselor',$uni_to_counselor_data);
		}
		else
		{
			$uni_to_counselor_data = array(
				'university_id' => $insertData['university'],
				'counselor_id' 	=> $insertData['id']
			);

			$this->db->insert('university_to_counselor', $uni_to_counselor_data);

		}*/
		return 'success';
	}

	function getCounselorByid ($conditions)
	{
		$this->db->from('user_master');
		//$this->db->join('university_to_counselor', 'university_to_counselor.counselor_id = user_master.id');
		//$this->db->join('universities', 'universities.id = university_to_counselor.university_id');
		$this->db->join('type_master', 'type_master.type_id = user_master.type');
		$this->db->join('status_master', 'status_master.id = user_master.status');
		$this->db->where($conditions);

		$this->db->select('user_master.id, user_master.name, user_master.email, user_master.image, user_master.phone, user_master.type, type_master.type as typename, user_master.createdate, user_master.modifydate, user_master.status, status_master.status as status_name, user_master.notification, user_master.username');

		$result=$this->db->get()->row();

		return $result;
	}

	function getUniversityConsentByid($universityId)
	{
		$this->db->select('universities.consent_data, universities.name, country_master.name AS country_name');
		$this->db->from('universities');
		$this->db->join('country_master', 'country_master.id = universities.country_id');
		$this->db->where('universities.id', $universityId);
		$result=$this->db->get()->row();
		return $result;
	}

	function getUserInfo($columnName, $columnValue) {

		$query1 = "SELECT * FROM user_master WHERE type = 5 AND $columnName = '$columnValue'";
		$query = $this->db->query($query1);

    $result_data = $query->result_array();

    return $result_data;

	}

}
?>
