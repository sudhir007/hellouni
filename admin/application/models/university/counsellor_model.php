<?php
class Counsellor_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getCounsellorList($conditions1, $conditions2)
    {
        $this->db->select('user_master.id, user_master.name, user_master.email, user_master.createdate, user_master.status, status_master.status as status_name');
        $this->db->from('user_master');
        $this->db->join('status_master', 'status_master.id = user_master.status','left');
        $this->db->where($conditions1);
        $this->db->where($conditions2);

        $result=$this->db->get()->result();

        return $result;
    }

    function getCommunnicationAllList($userId,$counsellorId){

      $query1 = "SELECT c.*, u.name AS comment_by
                  FROM comments c
                  JOIN user_master u ON u.id = c.added_by
                  WHERE (c.added_by = $userId AND c.entity_id = $counsellorId) OR (c.added_by = $counsellorId AND c.entity_id = $userId )";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;

    }

    function getCommunnicationStudentList($userId, $counsellorId, $studentLogin){

      $query1 = "SELECT c.*, u.name AS comment_by
                  FROM comments c
                  JOIN user_master u ON u.id = c.added_by
                  WHERE (c.added_by = $userId AND c.entity_id = $counsellorId AND c.student_login_id = $studentLogin) OR (c.added_by = $counsellorId AND c.entity_id = $userId AND c.student_login_id = $studentLogin )";

              $query = $this->db->query($query1);

              $result_data = $query->result_array();

              return $result_data;

    }

}
?>
