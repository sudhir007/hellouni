<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 */

class Department_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getDepartmentById($departmentId)
    {
        $this->db->select('departments.*, colleges.name AS college_name, campuses.name AS campuse_name, universities.name AS university_name, colleges_categories.display_name AS mainstream');
        $this->db->from('departments');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->join('colleges_categories', 'colleges_categories.id = colleges.category_id', 'left');
        $this->db->where('departments.id', $departmentId);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function getAllDepartments()
    {
        $this->db->select('departments.*, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('departments');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->order_by('departments.id', 'desc');
        $result=$this->db->get()->result();
        return $result;
    }

    function getAllDepartmentsByUniversity($universityId)
    {
        $this->db->select('departments.*, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('departments');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->where('universities.id', $universityId);
        $result=$this->db->get()->result();
        return $result;
    }

    function getAllDepartmentsByCollege($collegeId)
    {
        $this->db->from('departments');
        $this->db->where('college_id', $collegeId);
        $result=$this->db->get()->result();
        return $result;
    }

    // function getDepartmentById($departmentId)
    // {
    //     $this->db->from('departments');
    //     $this->db->where('id', $departmentId);
    //     $result=$this->db->get()->result();
    //     return $result[0];
    // }

    function getDepartmentByIdNew($departmentId) {
      $this->db->select('departments.*, colleges.id AS college_id, campuses.id AS campus_id, universities.id AS university_id');
      $this->db->from('departments');
      $this->db->join('colleges', 'colleges.id = departments.college_id');
      $this->db->join('campuses', 'campuses.id = colleges.campus_id');
      $this->db->join('universities', 'universities.id = campuses.university_id');
      $this->db->where('departments.id', $departmentId);

      $result=$this->db->get()->result();
          return $result[0];
    }

    function addDepartment($departmentData)
    {
        $this->db->insert('departments', $departmentData);
        //echo $this->db->_error_message();
        $departmentId = $this->db->insert_id();
        return $departmentId;
    }

    function editDepartment($departmentId, $departmentData)
    {
        $this->db->where('id', $departmentId);
        $this->db->update('departments', $departmentData);
        //echo $this->db->_error_message();
    }

    function getDepartmentByName($departmentName, $collegeId)
    {
        $this->db->from('departments');
        $this->db->where('college_id', $collegeId);
        $this->db->where('name', $departmentName);
        $query=$this->db->get();
        return $query->result_array();
    }
}
?>
