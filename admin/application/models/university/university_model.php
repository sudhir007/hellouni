<?php

class University_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    // function getAllInternships()
    // {
    //     $this->db->select('universities_internships.*, universities.name AS university_name, colleges.name AS college_name, departments.name AS department_name');
    //     $this->db->from('universities_internships');
    //     $this->db->join('universities', 'universities.id = universities_internships.university_id');
    //     $this->db->join('colleges', 'colleges.id = universities_internships.college_id');
    //     $this->db->join('departments', 'departments.id = universities_internships.department_id');
    //     $result=$this->db->get()->result();
    //     return $result;
    // }

    // function getInternshipById($internshipId)
    // {
    //     $this->db->select('universities_internships.*, universities.name AS university_name, colleges.name AS college_name, departments.name AS department_name, universities.user_id AS university_login_id');
    //     $this->db->from('universities_internships');
    //     $this->db->join('universities', 'universities.id = universities_internships.university_id');
    //     $this->db->join('colleges', 'colleges.id = universities_internships.college_id');
    //     $this->db->join('departments', 'departments.id = universities_internships.department_id');
    //     $this->db->where('universities_internships.id', $internshipId);
    //     $result=$this->db->get()->row();
    //     return $result;
    // }

    // function addInternship($internshipData)
    // {
    //     $this->db->insert('internships_students', $internshipData);
    // }

    // function updateInternship($id, $internshipData)
    // {
    //     $this->db->where('id', $id);
    //     $this->db->update('internships_students', $internshipData);
    // }

    // function checkInternship($internshipId, $studentId)
    // {
    //     $this->db->from('internships_students');
    //     $this->db->where('internship_id', $internshipId);
    //     $this->db->where('student_id', $studentId);
    //     $result = $this->db->get()->row();
    //     return $result;
    // }

    function getDashboardApplicationCount($universityId)
    {
        $queryString = "SELECT status, count(id) AS count FROM users_applied_universities WHERE university_id = $universityId GROUP BY status;";

        $query = $this->db->query($queryString);

        $result_data = $query->result_array();
        return $result_data;
    }

    function getDashboardStudentCount($universityId)
    {
        $queryString = "SELECT status, count(id) AS count FROM student_details WHERE user_id IN (SELECT DISTINCT(user_id) FROM users_applied_universities WHERE university_id = $universityId) GROUP BY status;";

        $query = $this->db->query($queryString);

        $result_data = $query->result_array();
        return $result_data;
    }

    function getFilteredApplications($universityId, $startDate, $endDate, $countryIds, $intake_year, $intake_month, $statusFilter)
    {
        $universityCondition = "WHERE uau.university_id = $universityId";
        $dateCondition = ($startDate &&  $endDate) ? " AND CAST(um.createdate AS date) BETWEEN '$startDate' AND '$endDate'" : "";
        $countryCondition = $countryIds ? " AND lm.countries IN ('$countryIds')" : "";
        $intakeYearCondition = $intake_year ? " AND sd.intake_year IN ('$intake_year')" : "";
        $intakeCondition = $intake_month ? " AND sd.intake_month IN ('$intake_month')" : "";
        $statusFilterCondition = $statusFilter ? " AND uau.status IN ($statusFilter)" : "";

        $queryString = "SELECT
                            sd.id, sd.user_id, um.name, um.email, um.phone, lm.countries, uau.status AS lead_state, um.status, um.createdate, oum.name AS createdby, oum.email as originator_email, u.name AS university_name, c.name AS college_name, co.name AS course_name
                        FROM
                            users_applied_universities uau
                        JOIN
                            user_master um ON um.id = uau.user_id
                        JOIN
                            student_details sd ON sd.user_id = um.id
                        JOIN
                            lead_master lm ON lm.email = um.email AND um.type = 5
                        JOIN
                            user_master oum ON oum.id = sd.originator_id
                        JOIN
                            universities u ON u.id = uau.university_id
                        LEFT JOIN
                            colleges c ON c.id = uau.college_id
                        LEFT JOIN
                            courses co ON co.id = uau.course_id
                        $universityCondition
                        $dateCondition
                        $countryCondition
                        $intakeYearCondition
                        $intakeCondition
                        $statusFilterCondition
                    ;";

          $query = $this->db->query($queryString);
          $result_data = $query->result_array();

          return $result_data;
    }

    function getGrapData($universityId)
    {
        $queryStringApplication = "SELECT
                                        MONTH(um.createdate) AS graph_month, uau.status, COUNT(uau.id) AS count
                                    FROM
                                        users_applied_universities uau
                                    JOIN
                                        user_master um ON um.id = uau.user_id
                                    WHERE
                                        uau.university_id = $universityId
                                    GROUP BY
                                        uau.status, MONTH(um.createdate)";

        $queryStringStudent = "SELECT
                                    MONTH(um.createdate) AS graph_month, sd.status, COUNT(sd.id) AS count
                                FROM
                                    student_details sd
                                JOIN
                                    user_master um ON um.id = sd.user_id
                                WHERE
                                    um.id IN (SELECT DISTINCT(user_id) FROM users_applied_universities WHERE university_id = $universityId)
                                GROUP BY
                                    sd.status, MONTH(um.createdate)";

        $queryApplication = $this->db->query($queryStringApplication);
        $resultApplication = $queryApplication->result_array();

        $queryStudent = $this->db->query($queryStringStudent);
        $resultStudent = $queryStudent->result_array();

        $graphData = [
            "application_data" => $resultApplication,
            "student_data" => $resultStudent
        ];

        return $graphData;
    }

    function getFilteredApplicationsDashboard($universityId, $startDate, $endDate, $countryIds, $intake_year, $intake_month)
    {
        $universityCondition = " WHERE uau.university_id = $universityId";
        $dateCondition = ($startDate &&  $endDate) ? " AND CAST(um.createdate AS date) BETWEEN '$startDate' AND '$endDate'" : "";
        $countryCondition = $countryIds ? " AND lm.countries IN ('$countryIds')" : "";
        $intakeYearCondition = $intake_year ? " AND sd.intake_year IN ('$intake_year') " : "";
        $intakeCondition = $intake_month ? " AND sd.intake_month IN ('$intake_month') " : "";

        $queryString = "SELECT
                            uau.status,count(uau.id) AS count
                        FROM
                            users_applied_universities uau
                        JOIN
                            user_master um ON um.id = uau.user_id
                        JOIN
                            student_details sd ON sd.user_id = um.id
                        JOIN
                            lead_master lm ON lm.email = um.email AND um.type = 5
                        $universityCondition
                        $dateCondition
                        $countryCondition
                        $intakeYearCondition
                        $intakeCondition
                        GROUP BY
                            uau.status;
                    ";

        $query = $this->db->query($queryString);
        $result_data = $query->result_array();
        return $result_data;
    }

    function getFilteredStudentsDashboard($universityId, $startDate, $endDate, $countryIds, $intake_year, $intake_month, $statusFilter)
    {
        $universityCondition = " WHERE um.id IN (SELECT DISTINCT(user_id) FROM users_applied_universities WHERE university_id = $universityId)";
        $dateCondition = ($startDate &&  $endDate) ? " AND CAST(um.createdate AS date) BETWEEN '$startDate' AND '$endDate'" : "";
        $countryCondition = $countryIds ? " AND lm.countries IN ('$countryIds')" : "";
        $intakeYearCondition = $intake_year ? " AND sd.intake_year IN ('$intake_year') " : "";
        $intakeCondition = $intake_month ? " AND sd.intake_month IN ('$intake_month') " : "";
        $statusFilterCondition = $statusFilter ? " AND sd.status IN ($statusFilter) " : "";

        $queryString = "SELECT
                            sd.status,count(sd.id) AS count
                        FROM
                            student_details sd
                        JOIN
                            user_master um ON um.id = sd.user_id
                        JOIN
                            lead_master lm ON lm.email = um.email AND um.type = 5
                        $universityCondition
                        $dateCondition
                        $countryCondition
                        $intakeYearCondition
                        $intakeCondition
                        $statusFilterCondition
                        GROUP BY sd.status
                    ;";

        $query = $this->db->query($queryString);
        $result_data = $query->result_array();
        return $result_data;
    }

    function getFilteredGraph($universityId, $startDate, $endDate, $countryIds, $intake_year, $intake_month)
    {
        $dateCondition = ($startDate &&  $endDate) ? "AND CAST(um.createdate AS date) BETWEEN '$startDate' AND '$endDate'" : "";
        $countryCondition = $countryIds ? "AND lm.countries IN ('$countryIds')" : "";
        $intakeYearCondition = $intake_year ? "AND sd.intake_year IN ('$intake_year') " : "";
        $intakeCondition = $intake_month ? "AND sd.intake_month IN ('$intake_month')" : "";

        $queryStringApplication = "SELECT
                                        MONTH(um.createdate) AS graph_month, uau.status, COUNT(uau.id) AS count
                                    FROM
                                        users_applied_universities uau
                                    JOIN
                                        user_master um ON um.id = uau.user_id
                                    JOIN
                                        student_details sd ON sd.user_id = um.id
                                    JOIN
                                        lead_master lm ON lm.email = um.email AND um.type = 5
                                    WHERE
                                        uau.university_id = $universityId
                                    $dateCondition
                                    $countryCondition
                                    $intakeYearCondition
                                    $intakeCondition
                                    GROUP BY
                                        uau.status, MONTH(um.createdate)";

        $queryStringStudent = "SELECT
                                    MONTH(um.createdate) AS graph_month,sd.status, COUNT(sd.id) AS count
                                FROM
                                    student_details sd
                                JOIN
                                    user_master um ON um.id = sd.user_id
                                JOIN
                                    lead_master lm ON lm.email = um.email AND um.type = 5
                                WHERE
                                    um.id IN (SELECT DISTINCT(user_id) FROM users_applied_universities WHERE university_id = $universityId)
                                $dateCondition
                                $countryCondition
                                $intakeYearCondition
                                $intakeCondition
                                GROUP BY
                                    sd.status, MONTH(um.createdate)";

        $queryApplication = $this->db->query($queryStringApplication);
        $resultApplication = $queryApplication->result_array();

        $queryStudent = $this->db->query($queryStringStudent);
        $resultStudent = $queryStudent->result_array();

        $graphData = [
            "application_data" => $resultApplication,
            "student_data" => $resultStudent
        ];

        return $graphData;
    }

    function getFilteredStudents($universityId, $startDate, $endDate, $countryIds, $intake_year, $intake_month, $statusFilter)
    {
        $dateCondition = ($startDate &&  $endDate) ? " AND CAST(um.createdate AS date) BETWEEN '$startDate' AND '$endDate'" : "";
        $countryCondition = $countryIds ? " AND lm.countries IN ('$countryIds')" : "";
        $intakeYearCondition = $intake_year ? " AND sd.intake_year IN ('$intake_year')" : "";
        $intakeCondition = $intake_month ? " AND sd.intake_month IN ('$intake_month')" : "";
        $statusFilterCondition = $statusFilter ? " AND sd.status IN ($statusFilter)" : "";


        $queryString = "SELECT
                            sd.id, sd.user_id, um.name, um.email, um.phone, lm.countries, sd.status AS lead_state, um.status, um.createdate,
                            oum.name AS createdby, oum.email AS originator_email, fca.name AS facilitator_name, fca.email AS facilitator_email, fca.id AS facilitator_id
                        FROM
                            student_details sd
                        JOIN
                            user_master um ON um.id = sd.user_id
                        JOIN
                            lead_master lm ON lm.email = um.email AND um.type = 5
                        JOIN
                            user_master oum ON oum.id = sd.originator_id
                        JOIN
                            user_master fca ON fca.id = sd.facilitator_id
                        WHERE
                            um.id IN (SELECT DISTINCT(user_id) FROM users_applied_universities WHERE university_id = $universityId)
                        $counsellorCondition
                        $dateCondition
                        $countryCondition
                        $intakeYearCondition
                        $intakeCondition
                        $statusFilterCondition
                    ;";
        $query = $this->db->query($queryString);
        $result_data = $query->result_array();
        return $result_data;
    }

    function getRepresentativeByUniversity($conditions1, $conditions2)
    {
        $this->db->select('user_master.id, university_to_representative.university_id, university_to_representative.representative_id, user_master.name, user_master.email, user_master.createdate, user_master.status, status_master.status as status_name');
        $this->db->where($conditions1);
        $this->db->where($conditions2);
        $this->db->from('university_to_representative');
        $this->db->join('user_master', 'user_master.id = university_to_representative.representative_id','left');
        $this->db->join('status_master', 'status_master.id = user_master.status','left');

        $result=$this->db->get()->result();

        return $result;
    }

    function checkUserByUsername($username)
	{
		$this->db->from('user_master');
		$this->db->where('username', $username);
		$query = $this->db->get();
		return $query->result_array();
	}

	function checkUserByEmail($email)
	{
		$this->db->from('user_master');
		$this->db->where('email', $email);
		$query = $this->db->get();
		return $query->result_array();
	}

    function addRepresentative($insertData=array())
    {
        $user_master_data = array(
            'name' 		=> $insertData['name'],
            'email' 		=> $insertData['email'],
            'username' 		=> $insertData['username'],
            'password' 	=> $insertData['password'],
            'type' 		=> $insertData['type'] ,
            'createdate' 	=> $insertData['createdate'],
            'status' 		=> $insertData['status'],
            'notification' => $insertData['notification']
        );

        $this->db->insert('user_master', $user_master_data);
        $user_id = $this->db->insert_id();

        $user_permission_course_data = array(
            'user_id' 	=> $user_id,
            'view' 	=> $insertData['view'],
            'create' 	=> $insertData['create'],
            'edit' 	=> $insertData['edit'],
            'del' 		=> $insertData['del']
        );

        $this->db->insert('user_permission_course', $user_permission_course_data);

        $uni_to_rep_data = array(
            'university_id' 		=> $insertData['university'],
            'representative_id'	=> $user_id
        );

        $this->db->insert('university_to_representative', $uni_to_rep_data);

        return $user_id;
    }

    function updateRepresentative($representativeId, $data)
    {
        $this->db->where('id', $representativeId);
        $this->db->update('user_master', $data);
    }

    function getAlumniByUniversity($universityId)
    {
        $this->db->select('user_master.name AS name, user_master.email AS email, user_master.createdate, status_master.status AS status_name, universities.id AS university_id, user_master.id AS id');
        $this->db->from('alumnus');
        $this->db->join('universities', 'universities.id = alumnus.university_id');
        $this->db->join('user_master', 'user_master.id = alumnus.user_id');
		$this->db->join('status_master', 'status_master.id = user_master.status');
        $this->db->where('universities.id', $universityId);
        $result=$this->db->get()->result();
		return $result;
    }

    function addAlumnus($insertData=array())
    {
        $user_master_data = array(
            'name' 		=> $insertData['name'],
            'email' 		=> $insertData['email'],
            'username' 		=> $insertData['username'],
            'password' 	=> $insertData['password'],
            'type' 		=> $insertData['type'] ,
            'createdate' 	=> $insertData['createdate'],
            'status' 		=> $insertData['status'],
            'notification' => $insertData['notification']
        );

        $this->db->insert('user_master', $user_master_data);
        $user_id = $this->db->insert_id();

        $user_permission_course_data = array(
            'user_id' 	=> $user_id,
            'view' 	=> $insertData['view'],
            'create' 	=> $insertData['create'],
            'edit' 	=> $insertData['edit'],
            'del' 		=> $insertData['del']
        );

        $this->db->insert('user_permission_course', $user_permission_course_data);

        $data = array(
            'user_id' => $user_id,
            'university_id' => $insertData['university'],
            'created_at' => date('Y-m-d H:i:s')
        );

        $this->db->insert('alumnus', $data);

        return $user_id;
    }

    function updateAlumnus($alumnusId, $data)
    {
        $this->db->where('id', $alumnusId);
        $this->db->update('user_master', $data);
    }

    function getAllResearchesByUniversity($universityId)
    {
        $this->db->from('researches');
        $this->db->join('departments', 'departments.id = researches.department_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->select('researches.*, departments.name AS department_name, colleges.name AS college_name, universities.name AS university_name');
        $this->db->where('universities.id', $universityId);
        $result=$this->db->get()->result();
        return $result;
    }

    function getAllResearches()
    {
        $this->db->from('researches');
        $this->db->join('departments', 'departments.id = researches.department_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->select('researches.*, departments.name AS department_name, colleges.name AS college_name, universities.name AS university_name');
        $result=$this->db->get()->result();
        return $result;
    }

    function getResearchById($researchId)
    {
        $this->db->from('researches');
        $this->db->join('departments', 'departments.id = researches.department_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->select('researches.*, departments.name AS department_name, colleges.name AS college_name, universities.name AS university_name, universities.id AS university_id, colleges.id AS college_id');
        $this->db->where('researches.id', $researchId);
        $result=$this->db->get()->row();
        return $result;
    }

    function addResearch($researchData)
    {
        $this->db->insert('researches', $researchData);
        $research_id = $this->db->insert_id();
        return $research_id;
    }

    function editResearch($researchId, $researchData)
    {
        $this->db->where('id', $researchId);
        $this->db->update('researches', $researchData);
    }

    function getAllUniversities()
    {
        $this->db->from('universities');
        $result=$this->db->get()->result();
        return $result;
    }

    function getAllUniversitiesActive()
    {
        $this->db->select('id,name');
        $this->db->from('universities');
        $this->db->where('active','1');
        $result=$this->db->get()->result_array();
            return $result;
    }

    function getAllCountries()
    {
        $this->db->from('country_master');
        $result=$this->db->get()->result();
        return $result;
    }

    function getBrochuresByUniversity($universityId)
    {
        $this->db->from('universities_brochures');
        $this->db->where('university_id', $universityId);
        $result = $this->db->get();
        return $result->result();
    }

    function getBrochuresById($id)
    {
        $this->db->from('universities_brochures');
        $this->db->where('id', $id);
        $result = $this->db->get();
        return $result->row();
    }

    function addBrochure($data)
    {
        $this->db->insert('universities_brochures', $data);
        return true;
    }

    function getAllInternships()
    {
        $this->db->select('universities_internships.*, universities.name AS university_name, colleges.name AS college_name, departments.name AS department_name');
        $this->db->from('universities_internships');
        $this->db->join('universities', 'universities.id = universities_internships.university_id');
        $this->db->join('colleges', 'colleges.id = universities_internships.college_id');
        $this->db->join('departments', 'departments.id = universities_internships.department_id');
        $result=$this->db->get()->result();
        return $result;
    }

    function getAllInternshipsByUniversity($universityId)
    {
        $this->db->select('universities_internships.*, universities.name AS university_name, colleges.name AS college_name, departments.name AS department_name');
        $this->db->from('universities_internships');
        $this->db->join('universities', 'universities.id = universities_internships.university_id');
        $this->db->join('colleges', 'colleges.id = universities_internships.college_id');
        $this->db->join('departments', 'departments.id = universities_internships.department_id');
        $this->db->where('universities.id', $universityId);
        $result=$this->db->get()->result();
        return $result;
    }

    function getInternshipById($internshipId)
    {
        $this->db->select('universities_internships.*, universities.name AS university_name, colleges.name AS college_name, departments.name AS department_name');
        $this->db->from('universities_internships');
        $this->db->join('universities', 'universities.id = universities_internships.university_id');
        $this->db->join('colleges', 'colleges.id = universities_internships.college_id');
        $this->db->join('departments', 'departments.id = universities_internships.department_id');
        $this->db->where('universities_internships.id', $internshipId);
        $result=$this->db->get()->row();
        return $result;
    }

    function updateInternship($internshipId, $internshipData)
    {
        $this->db->where('id', $internshipId);
        $this->db->update('universities_internships', $internshipData);
    }

    function getInternshipRequestById($internshipId)
    {
        $this->db->select('universities_internships.*, universities.name AS university_name, colleges.name AS college_name, departments.name AS department_name, student_details.other_details AS student_other_details, user_master.name AS student_name, user_master.email AS student_email, user_master.phone AS student_phone, internships_students.other_details AS internship_other_details');
        $this->db->from('internships_students');
        $this->db->join('universities_internships', 'internships_students.internship_id = universities_internships.id');
        $this->db->join('universities', 'universities.id = universities_internships.university_id');
        $this->db->join('colleges', 'colleges.id = universities_internships.college_id');
        $this->db->join('departments', 'departments.id = universities_internships.department_id');
        $this->db->join('student_details', 'student_details.id = internships_students.student_id');
        $this->db->join('user_master', 'user_master.id = student_details.user_id');
        $this->db->where('universities_internships.id', $internshipId);
        $result=$this->db->get()->row();
        return $result;
    }

    function getAllInternshipRequests()
    {
        $this->db->select('universities_internships.*, universities.name AS university_name, colleges.name AS college_name, departments.name AS department_name, student_details.other_details AS student_other_details, user_master.name AS student_name, user_master.email AS student_email, user_master.phone AS student_phone, internships_students.other_details AS internship_other_details');
        $this->db->from('internships_students');
        $this->db->join('universities_internships', 'internships_students.internship_id = universities_internships.id');
        $this->db->join('universities', 'universities.id = universities_internships.university_id');
        $this->db->join('colleges', 'colleges.id = universities_internships.college_id');
        $this->db->join('departments', 'departments.id = universities_internships.department_id');
        $this->db->join('student_details', 'student_details.id = internships_students.student_id');
        $this->db->join('user_master', 'user_master.id = student_details.user_id');
        $result=$this->db->get()->result();
        return $result;
    }

    function getApplicationsByUniversity($universityId)
    {
        $this->db->select('users_applied_universities.*, universities.name, universities.slug, universities.website, user_master.email, universities.banner, colleges.name AS college_name, um.name AS student_name, um.email AS student_email, um.phone AS student_phone');
        $this->db->from('users_applied_universities');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('user_master', 'user_master.id = universities.user_id');
        $this->db->join('user_master um', 'um.id = users_applied_universities.user_id');
        $this->db->join('colleges', 'colleges.id = users_applied_universities.college_id', 'left');
        $this->db->where('users_applied_universities.university_id', $universityId);
        $result = $this->db->get();
        return $result->result_array();
    }

    function getApplicationById($applicationId)
    {
        $this->db->select('users_applied_universities.*, universities.name, universities.slug, universities.website, user_master.email, universities.banner, colleges.name AS college_name, um.name AS student_name, um.email AS student_email, um.phone AS student_phone');
        $this->db->from('users_applied_universities');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('user_master', 'user_master.id = universities.user_id');
        $this->db->join('user_master um', 'um.id = users_applied_universities.user_id');
        $this->db->join('colleges', 'colleges.id = users_applied_universities.college_id', 'left');
        $this->db->where('users_applied_universities.id', $applicationId);
        $result = $this->db->get();
        return $result->result_array();
    }

    function checkVisaByApplication($applicationId)
    {
        $this->db->from('users_visa_applicatios');
        $this->db->where('application_id', $applicationId);
        return $this->db->get()->result_array();
    }

    function getVisaApplicationsByUniversity($universityId)
    {
        $this->db->select('users_visa_applicatios.*, universities.id AS university_id, universities.name, universities.slug, universities.website, user_master.email, universities.banner, colleges.name AS college_name, um.name AS student_name, um.email AS student_email, um.phone AS student_phone');
        $this->db->from('users_visa_applicatios');
        $this->db->join('users_applied_universities', 'users_applied_universities.id = users_visa_applicatios.application_id');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('user_master', 'user_master.id = universities.user_id');
        $this->db->join('user_master um', 'um.id = users_applied_universities.user_id');
        $this->db->join('colleges', 'colleges.id = users_applied_universities.college_id', 'left');
        $this->db->where('users_applied_universities.university_id', $universityId);
        $result = $this->db->get();
        return $result->result_array();
    }

    function getVisaById($visaId)
    {
        $this->db->select('users_visa_applicatios.*, universities.id AS university_id, universities.name, universities.slug, universities.website, user_master.email, universities.banner, colleges.name AS college_name, um.name AS student_name, um.email AS student_email, um.phone AS student_phone');
        $this->db->from('users_visa_applicatios');
        $this->db->join('users_applied_universities', 'users_applied_universities.id = users_visa_applicatios.application_id');
        $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
        $this->db->join('user_master', 'user_master.id = universities.user_id');
        $this->db->join('user_master um', 'um.id = users_applied_universities.user_id');
        $this->db->join('colleges', 'colleges.id = users_applied_universities.college_id', 'left');
        $this->db->where('users_visa_applicatios.id', $visaId);
        $result = $this->db->get();
        return $result->result_array();
    }

}
?>
