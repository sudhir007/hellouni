<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 */

class College_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getCollegeById($collegeId)
    {
        $this->db->select('colleges.*, campuses.name AS campuse_name, universities.name AS university_name, colleges_categories.display_name AS mainstream');
        $this->db->from('colleges');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->join('colleges_categories', 'colleges_categories.id = colleges.category_id', 'left');
        $this->db->where('colleges.id', $collegeId);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function getAllColleges()
    {
        $this->db->select('colleges.*, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('colleges');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->order_by('colleges.id', 'desc');
        $result=$this->db->get()->result();
        return $result;
    }

    function getAllCollegesByUniversity($universityId)
    {
        $this->db->select('colleges.*, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('colleges');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->where('universities.id', $universityId);
        $result=$this->db->get()->result();
        return $result;
    }

    // function getCollegeById($collegeId)
    // {
    //     $this->db->from('colleges');
    //     $this->db->where('id', $collegeId);
    //     $result=$this->db->get()->result();
    //     return $result[0];
    // }

    function getCollegeByIdNew($collegeId)
    {
      $this->db->select('colleges.*, campuses.id AS campus_id, universities.id AS university_id');
      $this->db->from('colleges');
      $this->db->join('campuses', 'campuses.id = colleges.campus_id');
      $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->where('colleges.id', $collegeId);
        $result=$this->db->get()->result();
        return $result[0];
    }

    function addCollege($collegeData)
    {
        $this->db->insert('colleges', $collegeData);
        $collegeId = $this->db->insert_id();
        return $collegeId;
    }

    function editCollege($collegeId, $collegeData)
    {
        $this->db->where('id', $collegeId);
        $this->db->update('colleges', $collegeData);
    }

    function getAllCategories()
    {
        $this->db->from('colleges_categories');
        $result = $this->db->get()->result();
        return $result;
    }

    function getCollegeByName($collegeName, $campusId)
    {
        $this->db->from('colleges');
        $this->db->where('campus_id', $campusId);
        $this->db->where('name', $collegeName);
        $query=$this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }
}
?>
