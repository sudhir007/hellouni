<?php
class Course_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getAllCourses()
    {
        $this->db->select('courses.*, departments.name AS department_name, degree_master.name AS degree_name, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('courses');
        $this->db->join('departments', 'departments.id = courses.department_id');
        $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->order_by('courses.id', 'desc');
        $result=$this->db->get()->result();
		return $result;
    }

    function getAllCoursesByUniversity($universityId)
    {
        $this->db->select('courses.*, departments.name AS department_name, degree_master.name AS degree_name, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
        $this->db->from('courses');
        $this->db->join('departments', 'departments.id = courses.department_id');
        $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
        $this->db->join('colleges', 'colleges.id = departments.college_id');
        $this->db->join('campuses', 'campuses.id = colleges.campus_id');
        $this->db->join('universities', 'universities.id = campuses.university_id');
        $this->db->where('universities.id', $universityId);
        $result=$this->db->get()->result();
		return $result;
    }

    function getAllDegrees()
    {
        $this->db->from('degree_master');
        $result=$this->db->get()->result();
		return $result;
    }

    function getCourseById($courseId)
    {
        $this->db->from('courses');
        $this->db->where('id', $courseId);
        $result=$this->db->get()->result();
		return $result[0];
    }

    function getCourseByIdNew($courseId) {
      $this->db->select('courses.*, departments.id AS department_id, colleges.id AS college_id, campuses.id AS campus_id, universities.id AS university_id');
      $this->db->from('courses');
      $this->db->join('departments', 'departments.id = courses.department_id');
      $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
      $this->db->join('colleges', 'colleges.id = departments.college_id');
      $this->db->join('campuses', 'campuses.id = colleges.campus_id');
      $this->db->join('universities', 'universities.id = campuses.university_id');
      $this->db->where('courses.id', $courseId);
      $result=$this->db->get()->result();
		  return $result[0];
    }

    function getAllCoursesByDepartmentId($departmentId) {
      $this->db->select('courses.*, departments.name AS department_name, degree_master.name AS degree_name, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
      $this->db->from('courses');
      $this->db->join('departments', 'departments.id = courses.department_id');
      $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
      $this->db->join('colleges', 'colleges.id = departments.college_id');
      $this->db->join('campuses', 'campuses.id = colleges.campus_id');
      $this->db->join('universities', 'universities.id = campuses.university_id');
      $this->db->where('courses.department_id', $departmentId);
      $this->db->where('courses.status', '1');
      $result=$this->db->get()->result();
		  return $result;
    }

    function getAllDepartmentByDepartmentId($departmentId){

      $query1 = "SELECT * FROM departments where college_id IN (
                SELECT college_id FROM departments where id = $departmentId);";

      $query = $this->db->query($query1);
      $result_data = $query->result_array();

      return $result_data;
    }

    function addCourse($courseData)
    {
        try
   	    {
            $this->db->insert('courses', $courseData);
            $courseId = $this->db->insert_id();
            return $courseId;
        }
        catch(Exception $e)
        {
            return NULL;
        }
    }

    function editCourse($courseId, $courseData)
    {
        $this->db->where('id', $courseId);
   	    $this->db->update('courses', $courseData);
    }

    function getAllCategories()
    {
        $this->db->from('courses_categories');
        $result = $this->db->get()->result();
        return $result;
    }

    function getCourseByNameDegree($courseName, $degreeId, $departmentId)
    {
        $this->db->from('courses');
        $this->db->where('department_id', $departmentId);
        $this->db->where('name', $courseName);
        $this->db->where('degree_id', $degreeId);
        $query=$this->db->get();
		return $query->result_array();
    }

    function getCategoriesByMainstream($collegeCategoryId)
    {
        $this->db->from('courses_categories');
        $this->db->where('college_category_id', $collegeCategoryId);
        $result = $this->db->get()->result();
        return $result;
    }

    function getAllCoursesByDepartment($departmentId)
    {
        $this->db->select('courses.*, departments.name AS department_name, degree_master.name AS degree_name');
        $this->db->from('courses');
        $this->db->join('departments', 'departments.id = courses.department_id');
        $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
        $this->db->where('departments.id', $departmentId);
        $result=$this->db->get()->result();
		return $result;
    }
}
?>
