<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*

| -------------------------------------------------------------------------

| URI ROUTING

| -------------------------------------------------------------------------

| This file lets you re-map URI requests to specific controller functions.

|

| Typically there is a one-to-one relationship between a URL string

| and its corresponding controller class/method. The segments in a

| URL normally follow this pattern:

|

|	example.com/class/method/id/

|

| In some instances, however, you may want to remap this relationship

| so that a different class/function is called than the one

| corresponding to the URL.

|

| Please see the user guide for complete details:

|

|	http://codeigniter.com/user_guide/general/routing.html

|

| -------------------------------------------------------------------------

| RESERVED ROUTES

| -------------------------------------------------------------------------

|

| There area two reserved routes:

|

|	$route['default_controller'] = 'welcome';

|

| This route indicates which controller class should be loaded if the

| URI contains no data. In the above example, the "welcome" class

| would be loaded.

|

|	$route['404_override'] = 'errors/page_missing';

|

| This route will tell the Router what URI segments to use if those provided

| in the URL cannot be matched to a valid route.

|

*/



$route['default_controller'] = "common/login";

$route['404_override'] = '';



$route['quotations/sortby_([a-z]+)_([a-z]+)'] = "quotations/visitorLeads/$1/$2";

$route['quotations/sortby_([a-z]+)_([a-z]+)/(:num)'] = "quotations/visitorLeads/$1/$2/$3";

//UTA university student List
$route['university/UTA/studentlist'] = "webinar/webinarmaster/universityStudentlistbyid";

//$route['cities/fhgh/(:num)'] = "cities/cities/$1";



/* End of file routes.php */

/* Location: ./application/config/routes.php */


//university info
$route['university/info/(:num)'] = "university/universityinfo/infoForm";
$route['university/info/update'] = "university/universityinfo/updateInfo";
$route['university/info/update/overview'] = "university/universityinfo/updateInfoOverview";
$route['university/info/update/scholarshipsfinancial'] = "university/universityinfo/updateInfoScholarshipsFinancial";
$route['university/info/update/faqinfo'] = "university/universityinfo/updateInfoFaq";
$route['university/info/update/admissioninfo'] = "university/universityinfo/updateInfoAdmission";

// calling data
$route['callingdata/list'] = "callingdata/callingdata/callingList";
$route['callingdata/student/updateform/(:num)'] = "callingdata/callingdata/updateCallingDataForm";
$route['callingdata/student/update'] = "callingdata/callingdata/updateCallingData";
$route['callingdata/comment/(:num)'] = "callingdata/callingdata/commentList";
$route['callingdata/comments/add'] = "callingdata/callingdata/addComments";

$route['callingdata/source/list'] = "callingdata/callingdata/sourceList";
$route['callingdata/source/updateform/(:num)'] = "callingdata/callingdata/updateSourceDataForm";
$route['callingdata/source/update'] = "callingdata/callingdata/updateSourceData";
$route['callingdata/source/addform'] = "callingdata/callingdata/addSourceDataForm";
$route['callingdata/source/add'] = "callingdata/callingdata/addSourceData";

$route['callingdata/bulk/uploadform'] = "callingdata/callingdata/uploadForm";
$route['callingdata/bulk/uploaddata'] = "callingdata/callingdata/uploadData";

$route['university/process/info'] = "university/universityinfo/universityProcessInfo";

$route['university/process/infoform/(:num)'] = "university/universityinfo/universityProcessInfoForm";
$route['university/process/info/add'] = "university/universityinfo/universityProcessInfoAdd";
$route['university/process/doc/update'] = "university/universityinfo/appProcessDocUpdate";

//new codes here
