<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*

| -------------------------------------------------------------------------

| URI ROUTING

| -------------------------------------------------------------------------

| This file lets you re-map URI requests to specific controller functions.

|

| Typically there is a one-to-one relationship between a URL string

| and its corresponding controller class/method. The segments in a

| URL normally follow this pattern:

|

|	example.com/class/method/id/

|

| In some instances, however, you may want to remap this relationship

| so that a different class/function is called than the one

| corresponding to the URL.

|

| Please see the user guide for complete details:

|

|	http://codeigniter.com/user_guide/general/routing.html

|

| -------------------------------------------------------------------------

| RESERVED ROUTES

| -------------------------------------------------------------------------

|

| There area two reserved routes:

|

|	$route['default_controller'] = 'welcome';

|

| This route indicates which controller class should be loaded if the

| URI contains no data. In the above example, the "welcome" class

| would be loaded.

|

|	$route['404_override'] = 'errors/page_missing';

|

| This route will tell the Router what URI segments to use if those provided

| in the URL cannot be matched to a valid route.

|

*/



$route['default_controller'] = "common/login";

$route['404_override'] = '';



$route['quotations/sortby_([a-z]+)_([a-z]+)'] = "quotations/visitorLeads/$1/$2";

$route['quotations/sortby_([a-z]+)_([a-z]+)/(:num)'] = "quotations/visitorLeads/$1/$2/$3";

//UTA university student List
$route['university/UTA/studentlist'] = "webinar/webinarmaster/universityStudentlistbyid";

//$route['cities/fhgh/(:num)'] = "cities/cities/$1";



/* End of file routes.php */

/* Location: ./application/config/routes.php */

// Mapping data
$route['mappingdata/universityList'] = "mappingdata/mappingdata/universityList";
$route['mappingdata/universityEdit/(:num)'] = "mappingdata/mappingdata/universityEdit";
$route['mappingdata/create/(:num)'] = "mappingdata/mappingdata/create";
$route['mappingdata/update/(:num)'] = "mappingdata/mappingdata/update";
$route['mappingdata/courseList'] = "mappingdata/mappingdata/courseList";
$route['mappingdata/courseEdit/(:num)'] = "mappingdata/mappingdata/courseEdit";
$route['mappingdata/courseCreate/(:num)'] = "mappingdata/mappingdata/courseCreate";
$route['mappingdata/courseUpdate/(:num)'] = "mappingdata/mappingdata/courseUpdate";

//university info
$route['university/info/(:num)'] = "university/universityinfo/infoForm";
$route['university/info/update'] = "university/universityinfo/updateInfo";
$route['university/info/update/overview'] = "university/universityinfo/updateInfoOverview";
$route['university/info/update/scholarshipsfinancial'] = "university/universityinfo/updateInfoScholarshipsFinancial";
$route['university/info/update/faqinfo'] = "university/universityinfo/updateInfoFaq";
$route['university/info/update/latestinfo'] = "university/universityinfo/updateInfoLatest";
$route['university/info/update/updateRanking'] = "university/universityinfo/updateInfoRanking";
$route['university/info/update/seoinfo'] = "university/universityinfo/updateInfoSeo";
$route['university/info/update/socialmediainfo'] = "university/universityinfo/updateInfoSocialMedia";
$route['university/info/update/gallaryinfo'] = "university/universityinfo/updateInfoGallary";
$route['university/info/update/universityimageinfo'] = "university/universityinfo/updateInfoUniversityImage";
$route['university/info/update/admissioninfo'] = "university/universityinfo/updateInfoAdmission";

// calling data
$route['callingdata/list'] = "callingdata/callingdata/callingList";
$route['callingdata/student/updateform/(:num)'] = "callingdata/callingdata/updateCallingDataForm";
$route['callingdata/student/update'] = "callingdata/callingdata/updateCallingData";
$route['callingdata/comment/(:num)'] = "callingdata/callingdata/commentList";
$route['callingdata/comments/add'] = "callingdata/callingdata/addComments";

$route['callingdata/source/list'] = "callingdata/callingdata/sourceList";
$route['callingdata/source/updateform/(:num)'] = "callingdata/callingdata/updateSourceDataForm";
$route['callingdata/source/update'] = "callingdata/callingdata/updateSourceData";
$route['callingdata/source/addform'] = "callingdata/callingdata/addSourceDataForm";
$route['callingdata/source/add'] = "callingdata/callingdata/addSourceData";

$route['callingdata/bulk/uploadform'] = "callingdata/callingdata/uploadForm";
$route['callingdata/bulk/uploaddata'] = "callingdata/callingdata/uploadData";

$route['university/process/info'] = "university/universityinfo/universityProcessInfo";

$route['university/process/infoform/(:num)'] = "university/universityinfo/universityProcessInfoForm";
$route['university/process/deleteUniversityKey/(:num)'] = "university/universityinfo/deleteUniversityKey";
$route['university/process/deleteUniversityCacheListKey'] = "university/universityinfo/deleteUniversityCacheListKey";
$route['university/process/deleteCourseCacheListKey'] = "university/universityinfo/deleteCourseCacheListKey";
$route['university/process/info/add'] = "university/universityinfo/universityProcessInfoAdd";
$route['university/process/doc/update'] = "university/universityinfo/appProcessDocUpdate";


//Student Registration process new flow

$route['counsellor/student/registration'] = "student_registration/studentapplication/studentRegistration";
$route['counsellor/student/application/submit'] = "student_registration/studentapplication/studentApplicationSubmit";
$route['counsellor/student/comments/submit'] = "student_registration/registration/add";
$route['counsellor/student/comments/view/(:num)'] = "student_registration/registration/studentCommentListview";

//comment application levels
$route['counsellor/application/comments/view/(:num)/(:num)'] = "student_registration/registration/studentCommentListviewApplication";
$route['counsellor/application/comments/(:num)/(:num)'] = "student_registration/registration/studentCommentlistApplication";
$route['counsellor/application/comments/add'] = "student_registration/registration/addCommentApplication";
$route['counsellor/application/comments/submit'] = "student_registration/registration/addApplication";

$route['student/list/all'] = "student/index/studentList";
$route['student/list/my'] = "student/index/studentListMy";
$route['student/list/unassigned'] = "student/index/studentListUnassigned";

$route['student/application/myList/new'] = "student_registration/studentapplication/studentMyApplicationListNew";
$route['student/application/myList/old'] = "student_registration/studentapplication/studentMyApplicationListOld";
$route['student/application/myList'] = "student_registration/studentapplication/studentMyApplicationList";
$route['student/application/allList'] = "student_registration/studentapplication/studentAllApplicationList";
$route['student/application/submitList'] = "student_registration/studentapplication/studentSubmitApplicationList";
$route['student/application/newList'] = "student_registration/studentapplication/studentNewApplicationList";
$route['student/application/newList/(:num)/(:num)'] = "student_registration/studentapplication/counsellorAssignToMe";

//application forms
$route['counsellor/create/application'] = "student_registration/studentapplication/studentApplicationForm";
$route['counsellor/add/application'] = "student_registration/studentapplication/submitStudentApplication";
$route['counsellor/submit/student'] = "student_registration/registration/studentsignup";
$route['counsellor/student/offerlist/view/(:num)'] = "student_registration/registration/studentofferlistview";
$route['counsellor/application/updatetemp'] = "student_registration/studentapplication/updateStudentApplication";
$route['counsellor/application/addStudentApplication'] = "student_registration/studentapplication/addStudentApplication";
$route['counsellor/application/status/submit'] = "student_registration/studentapplication/applicationStatusSubmit";

$route['counsellor/notification/list'] = "notification/notification/notificationList";
$route['counsellor/notification'] = "notification/notification/notificationListPage";
$route['counsellor/notification/seen'] = "notification/notification/notificationSeen";

$route['counsellor/team/register'] = "student_registration/registration/counsellorTeamForm";
$route['counsellor/registration/counsellorTeamSubmit'] = "student_registration/registration/counsellorTeamSubmit";


$route['student/profile/(:num)'] = "student_registration/account/update";
$route['student/profile/(:num)/(:num)'] = "student_registration/account/update";
$route['student/profile/save'] = "student_registration/account/save";
$route['student/profile/notesupdate'] = "student_registration/account/notesInsert";
$route['student/profile/appdocupdate'] = "student_registration/account/appDocUpdate";
$route['student/profile/deletedoc'] = "student_registration/account/deleteDoc";
$route['student/profile/commentsupdate'] = "user/account/commentsInsert";


//team
$route['team/register'] = "team/registration/teamForm";
$route['team/register/allList'] = "team/registration/teamAllList";
$route['team/register/deleteTeamLoginKey'] = "team/registration/deleteTeamLoginKey";
$route['team/register/update/(:num)'] = "team/registration/teamUpdate";


// university filter

$route['university/textsearch'] = "Croncontroller/freeTextSearchNew";
$route['university/textsuggestions'] = "Croncontroller/searchKeyNew";

$route['translate_uri_dashes'] = TRUE;
//echo "<pre>";print_r($route); exit;
