<?php

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

class applicationList extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
        $this->load->model(array('student_model'));
    }

    function index(){

      $userdata = $this->session->userdata('user');
      if(empty($userdata))
      {
          redirect('common/login');
          exit();
      }

      $userId = $_GET['user_id'] ? $_GET['user_id'] : '';
      $studentId = $_GET['student_id'] ? $_GET['student_id'] : '';
      $userType = $userdata['type'];
      $universityId = 0;
      if($userType == 3){
        $universityId = $userdata['university_id'];
      }

      $offerListObj = $this->student_model->studentapplicationlist($universityId);

      $this->outputData['application_list'] = $offerListObj;
      $this->outputData['user_id'] = $userId;
      $this->outputData['student_id'] = $studentId;
      $this->outputData['university_id'] = $universityId;

      $this->render_page('templates/student/applicationlist',$this->outputData);

    }


    function apply() {

      $userdata=$this->session->userdata('user');
      if(!$userdata){
          redirect('/user/account');
      }

      $data['university_id'] = $_GET['uid'];
      $data['course_id'] = isset($_GET['course_id']) ? $_GET['course_id'] : NULL;
      $data['user_id'] = $_GET['user_id'] ? $_GET['user_id'] : $userdata['id'];

      $uni_details = $this->student_model->getUniversityDataByid($data['university_id'], '');
      $universityName = $uni_details[0]['name'];
      $universityEmail = $uni_details[0]['email'];

      $appliedUniversity = $this->student_model->checkUniversityApplied($data['user_id'], $data['university_id']);
      if(!$appliedUniversity)
      {
          $this->student_model->applyUniversity($data);
          $updateStudent['status'] = 'POTENTIAL';
          $this->student_model->updateStudentDetails($updateStudent, $data['user_id']);
      }

      echo "SUCCESS";

    }
}
?>
