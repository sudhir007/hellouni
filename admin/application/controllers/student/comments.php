<?php

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

class Comments extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
        $this->load->model(array('student_model','webinar/webinar_model'));
    }

    function index()
    {
        $userdata = $this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $student_login_id = $_GET['id'] ? $_GET['id'] : '';
        if(!$student_login_id)
        {
            redirect('/student/index/all');
        }

        $allComments = $this->student_model->getCommentsByStudent($student_login_id);
        $appliedUniversities = $this->student_model->getAppliedUniversities($student_login_id);
        $commentTypes = array(
            'STUDENT' => 'Student',
            'UNIVERSITY' => 'University'
        );

        $leadStateList = array(
            'INTERESTED' => 'INTERESTED',
            'POTENTIAL' => 'POTENTIAL',
            'APPLICATION_PROCESS' => 'APPLICATION_PROCESS',
            'APPLICATION_SUBMITTED' => 'APPLICATION_SUBMITTED',
            'ADMIT_RECEIVED_CONDITIONAL' => 'ADMIT_RECEIVED_CONDITIONAL',
            'ADMIT_RECEIVED_UNCONDITIONAL' => 'ADMIT_RECEIVED_UNCONDITIONAL',
            'VISA_PROCESS_FINANCIAL_DOCUMENTS' => 'VISA_PROCESS_FINANCIAL_DOCUMENTS',
            'VISA_APPLIED' => 'VISA_APPLIED',
            'VISA_APPROVED' => 'VISA_APPROVED',
            'VISA_APPROVED_OFFER' => 'VISA_APPROVED_OFFER',
            'VISA_REJECTED' => 'VISA_REJECTED',
            'ADMIT_DENIED' => 'ADMIT_DENIED',
            'NOT_INTERESTED' => 'NOT_INTERESTED',
            'POTENTIAL_DECLINED' => 'POTENTIAL_DECLINED'
        );


        $this->outputData['applied_universities'] = $appliedUniversities;
        $this->outputData['comments'] = $allComments;
        $this->outputData['comment_types'] = $commentTypes;
        $this->outputData['lead_state_list'] = $leadStateList;
        $this->outputData['student_login_id'] = $student_login_id;
        $this->outputData['view'] = 'list';
        $this->render_page('templates/student/comments', $this->outputData);
    }

    function add()
    {
        $userdata = $this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $postedData = $this->input->post();
        $postedData['added_by'] = $userdata['id'];

        $leadState = $postedData['lead_state'] ? $postedData['lead_state'] : NULL;
        $entityId = $postedData['entity_id'] ? $postedData['entity_id'] : NULL;
        $studentLoginId = $postedData['student_login_id'] ? $postedData['student_login_id'] : NULL;
        $nextFollowUpDate = $postedData['followup_date'] ? $postedData['followup_date'] : NULL;


        if($entityId){

          $whereLeadAppliedCondition = ["id" => $entityId];
          $leadAppliedPut = ["status" => $leadState];

          $leadAppliedDataUpdate = $this->webinar_model->putWithGet($whereLeadAppliedCondition, $leadAppliedPut, "users_applied_universities");
          $postedData['entity_id'] = $leadAppliedDataUpdate['data'][0]['university_id'];
        }

        if($leadState){

          $getLeadStatus = $this->webinar_model->get(["user_id"=>$studentLoginId], "users_applied_universities");

					$stateArray = array('NOT_INTERESTED', 'POTENTIAL_DECLINED', 'APPLICATION_REVIEW_DECLINED', 'WITHDRAWN', 'REJECTED', 'VISA_REJECTED', 'INTERESTED', 'POTENTIAL', 'APPLICATION_PROCESS', 'APPLICATION_SUBMITTED', 'ADMIT_RECEIVED_CONDITIONAL', 'ADMIT_RECEIVED_UNCONDITIONAL', 'VISA_PROCESS_FINANCIAL_DOCUMENTS', 'VISA_APPLIED', 'VISA_APPROVED', 'VISA_APPROVED_OFFER', 'DISBURSAL_DOCUMENTATION', 'PARTIALLY_DISBURSED', 'DISBURSED', 'BEYOND_INTAKE');

							$currentState = -1;
	        		$maxState = -1;

							if($getLeadStatus['data']){

								foreach ($getLeadStatus['data'] as $keyls => $valuels) {
									$currentState = array_search($valuels['status'], $stateArray);
                  echo "$currentState";
									if($maxState < $currentState){
	                        $maxState = $currentState;
	                }
								}
								$leadState = $stateArray[$maxState];

							}

          $dateupdate = ["followup_date" => $nextFollowUpDate, "status" => $leadState];
          $dateWhere = ["user_id" => $studentLoginId];

          $leadAppliedDataUpdate = $this->webinar_model->put($dateWhere, $dateupdate, "student_details");
        }

        unset($postedData['lead_state']);
        unset($postedData['followup_date']);

	      $this->student_model->addComment($postedData);

        redirect('/student/comments?id=' . $postedData['student_login_id']);
    }
}
?>
