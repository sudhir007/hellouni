<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Menu extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
		$this->load->model('elements/menu_model');
    }

	
	function index()
	{   //die('++++++++++++++++');
	     $this->load->helper('cookie_helper'); 
         //$this->load->model('location/country_model');
		 //$this->load->model('location/city_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }		
        
		$cond1 = array('menu_master.status !=' => '5'); 
		
		$this->outputData['menus'] = $this->menu_model->getMenus($cond1);
       /* echo '<pre>';
		print_r($this->outputData['countries']);
		echo '</pre>'; die('++++++');*/
		$this->render_page('templates/elements/menu');
	
} 
	
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
         
		 // $this->load->model('elements/menu_model');
		 
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }		
		 
		 if($this->uri->segment('4')){
		 //die('++++');
		 $cond1 = array('menu_master.id ' => $this->uri->segment('4')); 
		 $menu_info = $this->menu_model->getMenuByid($cond1);
		 
		/* echo '<pre>';
		 print_r($menu_info);
		 echo '</pre>';
		 exit();*/
		 
		 $this->outputData['id'] 			= $menu_info->id;
		 $this->outputData['name'] 			= $menu_info->name;
		 $this->outputData['alias'] 		= $menu_info->alias;
		 $this->outputData['details'] 		= $menu_info->details;
		 $this->outputData['status'] 		= $menu_info->status;
		 
		 
		}
		$this->render_page('templates/elements/menu_form',$this->outputData);
	
     } 
	 
	 
	 
	 function save()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
       
		   $data = array(
			'name' => $this->input->post('name'),
     		'details' => $this->input->post('details')
     		);
	
	  if($this->menu_model->saveDetails(array('menu_master.id'=>$this->uri->segment('4')),$data)==true){
	     $this->session->set_flashdata('flash_message', "You have saved Menu!");
	     redirect('elements/menu');
	    }
	 
	  
	}
	 
	 
	 function edit()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('location/country_model');
		
			  //$logo_info = $this->common_model->getFileinfo($this->input->post('property_logo'));
			 $data = array(
			'id' => $this->uri->segment('4'),
     		'name' => $this->input->post('name'),
     		'code' => $this->input->post('code'),
     		'status' =>$this->input->post('status') 
     		);
			
			
	         if($this->country_model->editCountry($data)=='success'){
	 
	         $this->session->set_flashdata('flash_message', "You have modified Country!");
	         redirect('location/country');
	         }
	 
	 
	  
	}
    
	
	function delete_country()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('location/country_model');
		
		
		if($this->country_model->deleteCountry($this->uri->segment('4'))=='success'){
	 
	         $this->session->set_flashdata('flash_message', "You have deleted Country!");
	        redirect('location/country');
	        
	 
	 
	    }
	}
	
	
	function delete_mult_country(){
	
	    $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('location/country_model');
		
		$array = $this->input->post('chk');
		foreach($array as $id):
		
		$this->country_model->deleteCountry($id);
		
		endforeach;
		$this->session->set_flashdata('flash_message', "Success: You have deleted Country!");
		redirect('location/country/index');
	
	
	}
	 
	 
	 
	 

	

}//End  Home Class





?>