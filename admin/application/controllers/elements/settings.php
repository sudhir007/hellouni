<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Settings extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()

    {

        parent::__construct();

        $this->load->library('template');



      $this->lang->load('enduser/home', $this->config->item('language_code'));
	  $this->load->model('elements/settings_model');

		//$this->load->model('youraccount/global_model');

       		//$this->load->model('youraccount/auth_model');

		$this->load->library('form_validation');



        

    }

	
	function index()
	{    
	     $this->load->helper('cookie_helper'); 
         $this->load->model('elements/settings_model');
		 //$this->load->model('administrator/common_model');
		 

		 $this->load->model('location/country_model');
		// $this->load->model('location/state_model');
		 //$this->load->model('location/city_model');
		 
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }		

        $settings = $this->settings_model->getSettings();
		
		/*echo '<pre>';
		print_r($settings);
		echo '</pre>';
		exit();*/
		
		
		foreach($settings as $setting){
		
		if($setting->key=='config_title'){
		 $this->outputData['config_title'] 	= $setting->value ;  

		 //$this->outputData["".$setting->key.""] = $this->settings_model->getEmail($setting->value);   
		}
	    elseif($setting->key=='config_copywrite') {
		 $this->outputData['config_copywrite'] 	= $setting->value;   
		// $this->outputData["".$setting->key.""]= $this->settings_model->getContact($setting->value);   
		}
		
		elseif($setting->key=='config_facebook') {
		$this->outputData['config_facebook'] 	= $setting->value;   
		 //$this->outputData["".$setting->key.""]= $this->settings_model->getContact($setting->value);   
		}
		elseif($setting->key=='config_twitter') {
		$this->outputData['config_twitter'] 	= $setting->value;   
		 
		  
		}elseif($setting->key=='config_gplus') {
		$this->outputData['config_gplus'] 	= $setting->value;   
		  
		  
		}elseif($setting->key=='config_linkedin') {
		$this->outputData['config_linkedin'] 	= $setting->value;   
		 
		  
		}elseif($setting->key=='config_phone') {
		$this->outputData['config_phone'] 	= $setting->value; 
		//$this->outputData['states'] = $this->state_model->getStatesByCountry($this->outputData['config_country']);  
		
		}elseif($setting->key=='config_site_status') {
		$this->outputData['config_state'] 	= $setting->value;   
		  
		}elseif($setting->key=='config_analytic') {
		$this->outputData['config_analytic'] 	= $setting->value;  
		//$logo_details = $this->common_model->getFileinfo_Byid($setting->value);
		//$this->outputData['path'] = $logo_details->path;
		}elseif($setting->key=='config_email') {
		$this->outputData['config_email'] 	= $setting->value;   
		  
		}elseif($setting->key=='config_meta_title') {
		$this->outputData['config_meta_title'] 	= $setting->value;   
		  
		}elseif($setting->key=='config_meta_desc') {
		$this->outputData['config_meta_desc'] 	= $setting->value;   
		  
		}
		
		
		
		
		
		}
		
		
		/*$cond_country1 = array('infra_country_master.status' => '1'); 
		$cond_country2 = array('infra_country_master.status' => '2'); 	
		$this->outputData['countries'] = $this->country_model->getCountries($cond_country1,$cond_country2);
		
		$cond_state1 = array('infra_state_master.status' => '1'); 
		$cond_state2 = array('infra_state_master.status' => '2'); 	
		//$this->outputData['states'] = $this->state_model->getStates($cond_state1,$cond_state2);  
		
		$cond_city1 = array('infra_city_master.status' => '1'); 
		$cond_city2 = array('infra_city_master.status' => '2'); 		
        $this->outputData['cities'] = $this->city_model->getCities($cond_city1,$cond_city2);
        $this->outputData['currencies']	= $this->common_model->getCurrencies(); */
		
       $this->render_page('templates/elements/settings',$this->outputData);
		
} 
	
	function log_in()

	{
     //Load Models - for this function
	 $this->load->model('user/user_model');
	 $this->load->model('user/auth_model');
	 $this->load->helper('cookie'); 
	
	 
     $username = $this->input->post('username');
	 $password = md5($this->input->post('password'));
	 $conditions 		=  array('user_name'=>$username,'password' => $password,'role_id' => 1,'status_id' => 1);
	 $query				= $this->user_model->checkUser($conditions);
	 
	 
	 if(count($query)> 0){ 
	  
	  $data = array(
     'id' => $query->id,
     'name' => $query->name,
     'username' =>$query->user_name,
     'role' => $query->role,
     'email' => $query->email,
     'is_logged_in' => true
     );
     $this->session->set_userdata('user', $data);
	 
	 if($this->input->post('remember'))
						{ 
						    $insertData=array();
						    $insertData['username']=$this->input->post('username');
						    $insertData['password']=$this->input->post('pwd');
						    $expire=60*60*24*100;
							if( $this->auth_model->getUserCookie('uname')=='')
							{ 
							$this->user_model->addRemerberme($insertData,$expire); 
							
							}		
						}
						else
						{
						   $this->user_model->removeRemeberme(); 
							
						}
							
							
	 redirect('common/dashboard');
	 }else{
	 $this->session->set_flashdata('flash_message', "Sorry!Invalid Login.");
	
	 redirect('common/login');
	 }
} 


function logout()
 { 
  $this->load->model('user/auth_model');
  $this->auth_model->clearUserSession();
  //$this->session->set_flashdata('flash_message', $this->common_model->flash_message('success',$this->lang->line('logout_success')));
  //$this->auth_model->clearUserCookie(array('username','password'));
   //$userdata=$this->session->userdata('user');
   $this->session->unset_userdata('user');
  //unset($userdata);
  //print_r($userdata); exit();
  //$this->auth_model->clearUserCookie(array('user_name','user_password'));
  redirect('common/login');
  //$this->load->view('youraccount/index',$userdata);
    
 } //Function logout End



function update()
    {
	 $this->load->helper(array('form', 'url'));
	 //$this->load->model('administrator/settings_model');
	 
	   
	 $data = array(
     'config_title' 		=> $this->input->post('config_title'),
     'config_copywrite' 	=> $this->input->post('config_copywrite'),
     'config_facebook' 		=> $this->input->post('config_facebook'),
     'config_twitter' 		=> $this->input->post('config_twitter'),
	 'config_gplus' 		=> $this->input->post('config_gplus'),   
	 'config_linkedin' 		=> $this->input->post('config_linkedin'),
	 'config_phone' 		=> $this->input->post('config_phone'),
	 'config_site_status' 	=> $this->input->post('config_site_status'),
     'config_analytic' 		=> $this->input->post('config_analytic'),
	 'config_email' 		=> $this->input->post('config_email'),
	 'config_meta_title' 	=> $this->input->post('config_meta_title'),
     'config_meta_desc' 	=> $this->input->post('config_meta_desc')
     );
	
	 $this->settings_model->updateSettings($data);
	 
	 $this->session->set_flashdata('flash_message', "You have modified settings!");
	 redirect('elements/settings');
	
	
	
	}
	
	
	function ajax_state()
    {
	  $this->load->model('administrator/settings_model');
	  $this->load->model('location/city_model');
	  $this->load->model('location/state_model');
	  if($this->input->post('id')){
	
	  $states = $this->state_model->getStatesByCountry($this->input->post('id'));
	  //$cities = $this->city_model->getCitiesByCountry($this->input->post('id'));
	
	  foreach($states as $st){
	  echo "<option value=".$st->zone_id.">".$st->name."</option>";
	 }
	}	
   }
   
   function ajax_city()
    {
	  $this->load->model('administrator/settings_model');
	  $this->load->model('location/city_model');
	  if($this->input->post('id')){
	
	 // $states = $this->state_model->getStatesByCountry($this->input->post('id'));
	  $cities = $this->city_model->getCitiesByState($this->input->post('id'));
	  
	  foreach($cities as $city){
	  echo "<option value=".$city->id.">".$city->city_name."</option>";
	 }
	}	
   }
   
    
	

	

}//End  Home Class





?>