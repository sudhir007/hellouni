<?php

class Filtercontroller extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

 function commonDropdown() {

   $this->load->model('filter_model');

   $entity_type = $_POST['entity_type'];
   $entity_id = $_POST['entity_id'];
   $allEntityList = [];

   if($entity_type == 'campus'){
     $allEntityList = $this->filter_model->getAllCampusesByUniversityId($entity_id);
   }

   if($entity_type == 'college'){
     $allEntityList = $this->filter_model->getAllCollegeByCampusId($entity_id);
   }

   if($entity_type == 'department'){
     $allEntityList = $this->filter_model->getAllDepartmentByCollegeId($entity_id);
   }

   echo "<option> Select $entity_type </option>";
   foreach ($allEntityList as $key => $value) {
     // code...
     echo "<option value='".$value['id']."'> ".strtoupper($value['name']). "</option>";
   }


 }

}
?>
