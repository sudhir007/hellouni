<?php
/**
 * Reverse bidding system Home Class
 *
 * Permits admin to set the site settings like site title,site mission,site offline status.
 *
 * @package		Reverse bidding system
 * @subpackage	Controllers
 * @category	Common Display
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @created		December 30 2008
 * @link		http://www.freelancephpscript.com
 */

/*ini_set('display_errors', '1');
 ini_set('display_startup_errors', '1');
 error_reporting(E_ALL);*/

require_once APPPATH . 'libraries/Mail/sMail.php';
class Account extends MY_Controller {
  //Global variable
  public $outputData;    //Holds the output data for each view
  public $loggedInUser;
  /**
   * Constructor
   *
   * Loads language files and models needed for this controller
   */
  function __construct()
  {
    parent::__construct();
    $this->load->model('student_registration/user_model');
    $this->load->model('student_registration/auth_model');
    $this->load->model('student_registration/country_model');
    $this->load->model('student_registration/state_model');
    $this->load->model('student_registration/city_model');
    $this->load->model('student_registration/additional_model');
    $this->load->helper('student_registration/track_helper');
    track_user();
  }

  function index()
  {
    $this->load->helper('cookie_helper');
    $this->load->model('student_registration/auth_model');

    $this->load->library('user_agent');
    if ($this->agent->is_referral()) {
      $this->outputData['referrer_url'] = $this->agent->referrer();
    }

    //$userdata=$this->session->userdata('user');
    $userdata = $this->session->userdata('leaduser');
    if (!empty($userdata)) {
      redirect('search/university');
    }

    $this->render_page('templates/user/login', $this->outputData);
    //$this->render_page('templates/common/index');
  }

  function signup()
  {
    $this->load->helper('cookie_helper');
    $this->load->model('student_registration/auth_model');
    $userdata = $this->session->userdata('user');
    if (!empty($userdata)) {
      redirect('location/country');
    }

    $this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status' => 1));
    $this->outputData['locationcountries'] = $this->country_model->getLocationCountries(array('location_country_master.status' => 1));
    $this->outputData['intakes'] = $this->additional_model->getIntakes(array('intake_master.status' => '1'));

    $this->render_page('templates/user/sign_up', $this->outputData);
    //$this->render_page('templates/common/index');
  }

  function create()
  {
    //die('+++++');
    $this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');
    $this->load->model('student_registration/course_model');
    $this->load->model('student_registration/auth_model');
    $leaddata = $this->session->userdata('leaduser');

    //$userdata=$this->session->userdata('user');
    $data = array(
      'name'       =>  $this->input->post('name'),
      'email'     =>  $this->input->post('email'),
      'password'     =>  md5($this->input->post('password')),
      'image'      =>  '',
      'type'       =>  '5',
      'createdate'   =>  date('Y-m-d H:i:s'),
      'activation'   =>  md5(time()),
      'fid'      =>  '',
      'status'     =>  '3'
    );

    $user_id = $this->user_model->insertUserMaster($data);
    $student_data = array(
      'user_id'         =>  $user_id,
      'address'        =>  $this->input->post('address'),
      'city'          =>  $this->input->post('city'),
      'zip'          =>  $this->input->post('zip'),
      'dob'          =>  $this->input->post('dob'),
      'phone'          =>  $this->input->post('phone'),
      'parent_name'      =>   $this->input->post('parent_name'),
      'parent_mob'      =>   $this->input->post('parent_mob'),
      'parent_occupation'    =>   $this->input->post('parent_occupation'),
      'hear_about_us'      =>   $this->input->post('hear_info'),
      'passport'        =>   $this->input->post('passport_availalibility'),
      'desired_destination'  =>   implode(",", $this->input->post('deisred_destinations'))
    );

    //print_r($data); exit;
    if ($this->user_model->insertStudentDetails($student_data)) {
      if ($this->input->post('desired_course_name')) {
        $desired_course = array(
          'student_id'      =>  $user_id,
          'desired_course_name'  =>  $this->input->post('desired_course_name'),
          'desired_course_level'  =>  $this->input->post('desired_coures_level'),
          'desired_course_intake'  =>  $this->input->post('desired_course_intake'),
          'desired_course_year'  =>  $this->input->post('desired_coures_year')
        );
        if (!$this->user_model->checkDesiredCourse(array('student_id' => $user_id))) {
          $this->user_model->insertDesiredCourse($desired_course);
        }
      }

      // Secondary Qualification
      if ($this->input->post('secondary_institution_name')) {
        $secondary_qualification = array(
          'student_id'    =>  $user_id,
          'institution_name'  =>  $this->input->post('secondary_institution_name'),
          'board'        =>  $this->input->post('board'),
          'year_started'    =>  $this->input->post('secondary_year_starrted'),
          'year_finished'    =>  $this->input->post('secondary_year_finished'),
          'grade'        =>  $this->input->post('secondary_grade'),
          'total'        =>  $this->input->post('secondary_total')
        );

        if (!$this->user_model->checkSecondaryQualification(array('student_id' => $user_id))) {
          $this->user_model->insertSecondaryQualification($secondary_qualification);
        }
      }

      // Diploma Or 12th
      if ($this->input->post('diploma_12') == 1) {
        if ($this->input->post('diploma_course_name')) {
          $diploma_qualification = array(
            'student_id'    =>  $user_id,
            'course'      =>  $this->input->post('diploma_course_name'),
            'college'      =>  $this->input->post('diploma_institution_name'),
            'year_started'    =>  $this->input->post('diploma_year_starrted'),
            'year_finished'    =>  $this->input->post('diploma_year_finished'),
            'aggregate'      =>  $this->input->post('diploma_grade'),
            'total'        =>  $this->input->post('diploma_total')
          );

          if (!$this->user_model->checkDiplomaQualification(array('student_id' => $user_id))) {
            $this->user_model->insertDiplomaQualification($diploma_qualification);
          }
        }
      } else if ($this->input->post('diploma_12') == 2) {
        if ($this->input->post('hs_institution_name')) {
          $hs_qualification = array(
            'student_id'    =>  $user_id,
            'institution_name'  =>  $this->input->post('hs_institution_name'),
            'board'        =>  $this->input->post('hs_board'),
            'year_started'    =>  $this->input->post('hs_year_starrted'),
            'year_finished'    =>  $this->input->post('hs_year_finished'),
            'grade'        =>  $this->input->post('hs_grade'),
            'total'        =>  $this->input->post('hs_total')
          );

          if (!$this->user_model->checkHigher_secondary_qualification(array('student_id' => $user_id))) {
            $this->user_model->insertHigher_secondary_qualification($hs_qualification);
          }
        }
      }

      //Bachelor
      if ($this->input->post('bachelor_course')) {
        $bachelor_qualification = array(
          'student_id'    =>  $user_id,
          'course'      =>  $this->input->post('bachelor_course'),
          'university'    =>  $this->input->post('bachelor_university'),
          'college'      =>  $this->input->post('bachelor_college'),
          'major'        =>  $this->input->post('bachelor_major'),
          'year_started'    =>  $this->input->post('bachelor_year_starrted'),
          'year_finished'    =>  $this->input->post('bachelor_year_finished'),
          'grade'        =>  $this->input->post('bachelor_grade'),
          'total'        =>  $this->input->post('bachelor_total')
        );

        if (!$this->user_model->checkBachelorQualification(array('student_id' => $user_id))) {
          $this->user_model->insertBachelorQualification($bachelor_qualification);
        }
      }

      // Masters
      if ($this->input->post('masters') == 1) {
        if ($this->input->post('master_course')) {
          $master_qualification = array(
            'student_id'    =>  $user_id,
            'course'      =>  $this->input->post('master_course'),
            'university'    =>  $this->input->post('master_university'),
            'college'      =>  $this->input->post('master_college'),
            'major'        =>  $this->input->post('master_major'),
            'year_started'    =>  $this->input->post('master_year_started'),
            'year_finished'    =>  $this->input->post('master_year_finished'),
            'grade'        =>  $this->input->post('master_grade'),
            'total'        =>  $this->input->post('master_total')
          );
          if (!$this->user_model->checkMasterQualification(array('student_id' => $user_id))) {
            $this->user_model->insertMasterQualification($master_qualification);
          }
        }
      }

      $leadMasterData = array(
        ' name'      => $this->input->post('name'),
        'email'    => $this->input->post('email'),
        'phone'     => $this->input->post('phone'),
      );
      $this->user_model->updateLeadInfo($leadMasterData, $leaddata['id']);

      $sessiondata = array(
        'id'       => $leaddata['id'],
        'name'      => $this->input->post('name'),
        'email'    => $this->input->post('email'),
        'phone'     => $this->input->post('phone'),
        'mainstream'   => $leaddata['mainstream'],
        'courses'     => $leaddata['courses'],
        'degree'     => $leaddata['degree'],
        'countries'    => $leaddata['countries']
      );

      $this->auth_model->clearUserSession();
      $this->session->unset_userdata('leaduser');
      $this->session->set_userdata('leaduser', $sessiondata);

      // Mail to user for verification
      $from = 'nikhil.gupta@issc.in';
      $to = $this->input->post('email');
      $subject = "Please verify your email";



      $message = "

			<head>

			<title>Please verify your email</title>

			</head>

			<body>

			<p>Hi " . $this->input->post('name') . ",</p>

			<p>Your Account has been sucessfully created under HelloUni.</p>

			<p>Please activate your account by clicking the link below or copy the link and fire it in the browser -</p>

			<p>Activation link - " . base_url() . "user/account/verify/" . $data['activation'] . "</p><br/><br/>

			<p>Your registered login credential is as follows -</p>

			<p>Email - " . $this->input->post('email') . "</p>

			<p>Password -" . $this->input->post('password') . "</p>

			<p>URL - " . base_url() . "</p>

			</body>

			";

      // Always set content-type when sending HTML email
      $headers = "MIME-Version: 1.0" . "\r\n";
      $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

      // More headers
      $headers .= 'From:' . $from . "\r\n";

      mail($to, $subject, $message, $headers);
      //if(mail($to,$subject,$message,$headers)){die('++');}else{die('--');}

      $this->session->set_flashdata('flash_message', "Success: Please verify!");
      redirect('user/account/');
    }
  }

  function update()
  {
    $this->load->helper('cookie_helper');
    $this->load->model('student_registration/common_model');
    $this->load->model('student_registration/auth_model');
    $this->load->model('student_registration/course_model');
    $this->load->model('student_registration/degree_model');

    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    /*echo "<pre>"; print_r($userdata);
		echo "<pre>"; print_r($this->uri->segment('3'));
		echo "<pre>"; print_r($this->uri->segment('4')); exit;*/

    /*if($_GET['id']){
    	$getid = $_GET['id'] ? $_GET['id'] : '';

    	$this->db->select('*');
	  	$this->db->from('student_details');
		$this->db->where('id',$getid);
		$query = $this->db->get();
		$resultPri=$query->row();

      $userdata['id'] = $resultPri->user_id;

    } else {
      $userdata['id'] = $this->uri->segment('3');

    }*/

    if ($this->uri->segment('3') == 'update') {
      //$userdata['id'] = $userdata['id'];
      $userdata['id'] = $this->uri->segment('4');
    } else {
      $userdata['id'] = $this->uri->segment('3');
    }

    $applicationId = $this->uri->segment('4') ? $this->uri->segment('4') : NULL;
    //echo "<pre>"; print_r($applicationId); exit;
    $applicationType = 1;

    $personalProfile = [];
    $qualificationProfile = [];
    $desiredProfile = [];
    $parentProfile = [];
    $workExpProfile = [];
    $emergencyContactProfile = [];

    $condition = array('user_master.id' => $userdata['id'], 'user_master.status' => 1, 'user_master.type' => 5);

    $userDetails = $this->user_model->getStudentDetails($condition);
    /*echo "<pre>"; print_r($this->db->last_query());
    echo "<pre>"; print_r($userDetails); exit;*/
    $this->outputData['logged_in_type'] = $userdata['type'];
    $this->outputData['id'] = $userDetails->id;

    $this->outputData['student_id'] = $userDetails->student_id;

    $this->outputData['name'] = $userDetails->name;

    $this->outputData['last_name'] = $userDetails->last_name;

    $this->outputData['email'] = $userDetails->email;

    $this->outputData['typename'] = $userDetails->typename;

    $this->outputData['address'] = $userDetails->address;

    $this->outputData['country'] = $userDetails->country;

    $this->outputData['city'] = $userDetails->city;
    $this->outputData['gender'] = $userDetails->gender;

    $this->outputData['state'] = $userDetails->state;
    $this->outputData['passport_number'] = $userDetails->passport_number;
    $this->outputData['passport_issue_date'] = $userDetails->passport_issue_date;
    $this->outputData['passport_expiry_date'] = $userDetails->passport_expiry_date;
    $this->outputData['marital_status'] = $userDetails->marital_status;
    $this->outputData['place_of_birth'] = $userDetails->place_of_birth;
    $this->outputData['correspondence_address'] = $userDetails->correspondence_address;


    $this->outputData['company_name'] = $userDetails->company_name;
    $this->outputData['designation'] = $userDetails->designation;
    $this->outputData['work_start_date'] = $userDetails->work_start_date;
    $this->outputData['work_end_date'] = $userDetails->work_end_date;

    $tablename = "student_work_exp";
    $condup = "user_id = '" . $userdata['id'] . "'";
    $workExperiece = $this->user_model->get_data($tablename, $condup);
    //echo "<pre>"; print_r($workExperiece); exit;

    $this->outputData['workExperiece'] = $workExperiece;


    $tablefund = "student_fund_info";
    $condfund = "user_id = '" . $userdata['id'] . "'";
    $fundDetails = $this->user_model->getSingleData($tablefund, $condfund);
    $this->outputData['fundDetails'] = $fundDetails;

    foreach ($fundDetails as $keywe => $valuewe) {
      if ($fundDetails->fund_type == 'self_fund') {
        $this->outputData['fund_detail_status'] = "COMPLETE";
      } elseif ($fundDetails->fund_type == 'family_fund') {
        if ($fundDetails->guarantor_name && $fundDetails->guarantor_relation) {
          $this->outputData['fund_detail_status'] = "COMPLETE";
        } else {
          $this->outputData['fund_detail_status'] = "INCOMPLETE";
        }
      } elseif ($fundDetails->fund_type == 'sponsorship_fund') {
        $this->outputData['fund_detail_status'] = "COMPLETE";
      } else {
        if ($fundDetails->guarantor_name && $fundDetails->guarantor_relation) {
          $this->outputData['fund_detail_status'] = "COMPLETE";
        } else {
          $this->outputData['fund_detail_status'] = "INCOMPLETE";
        }
      }
    }

    $workExpProfile = [

      $workExperiece[0]->company_name,
      $workExperiece[0]->designation,
      $workExperiece[0]->start_date,
      $workExperiece[0]->end_date

    ];

    $emergencyContactProfile = [

      $userDetails->e_name,
      $userDetails->e_mobile_number,
      $userDetails->e_email,
      $userDetails->e_relation

    ];

    $this->outputData['e_name'] = $userDetails->e_name;
    $this->outputData['e_mobile_number'] = $userDetails->e_mobile_number;
    $this->outputData['e_email'] = $userDetails->e_email;
    $this->outputData['e_relation'] = $userDetails->e_relation;

    //print_r($this->state_model->getParticularCity(array('city_master.id' =>$userDetails->city,'city_master.status'=>1)));
    $userState = $this->state_model->getParticularCity(array('city_master.id' => $userDetails->city, 'city_master.status' => 1));
    $this->outputData['selected_state'] = '';
    $this->outputData['selected_country'] = '';
    if ($userState) {
      $this->outputData['selected_state'] = $userState->state_id;
      $this->outputData['selected_country'] = $userState->country_id;
    }


    $this->outputData['related_state_list'] = $this->state_model->getLocationStates(array('state_master.country_id' => $this->outputData['selected_country'], 'state_master.status' => 1));



    $this->outputData['related_city_list'] = $this->state_model->getCities(array('city_master.state_id' => $this->outputData['selected_state'], 'city_master.status' => 1));



    //print_r($this->outputData['related_state_list']); exit;

    $this->outputData['zip'] = $userDetails->zip;

    $this->outputData['dob'] = $userDetails->dob;

    $this->outputData['phone'] = $userDetails->phone;

    $this->outputData['parent_name'] = $userDetails->parent_name;

    $this->outputData['parent_mob'] = $userDetails->parent_mob;

    $this->outputData['parent_email'] = $userDetails->parent_email;

    $this->outputData['parent_occupation'] = $userDetails->parent_occupation;
    $this->outputData['parent_relation'] = $userDetails->parent_relation;
    //echo "<pre>";print_r($userDetails); exit;
    $this->outputData['hear_about_us'] = $userDetails->hear_about_us;

    $this->outputData['passport'] = $userDetails->passport;

    $this->outputData['desired_destination'] = $userDetails->desired_destination;

    $this->outputData['fid'] = $userDetails->fid;
    $this->outputData['intake'] = $userDetails->intake_month . ' ' . $userDetails->intake_year;

    // profile completeness code

    $personalProfile = [

      $userDetails->name,
      $userDetails->last_name,
      $userDetails->email,
      $userDetails->typename,
      $userDetails->address,
      $userDetails->country,
      $userDetails->city,
      $userDetails->gender,
      $userDetails->zip,
      $userDetails->dob,
      $userDetails->phone,
      $userDetails->passport,
      $userDetails->hear_about_us

    ];
    $personalProfileLW = [

      $userDetails->name,
      $userDetails->last_name,
      $userDetails->email,
      $userDetails->phone

    ];

    $parentProfile = [
      $userDetails->parent_name,
      $userDetails->parent_mob,
      $userDetails->parent_occupation
    ];

    $otherDetailProfile = [
      $userDetails->passport_number,
      $userDetails->passport_issue_date,
      $userDetails->passport_expiry_date
    ];

    $this->outputData['personal_detail_status'] = "COMPLETE";
    $this->outputData['personal_detail_status_lw'] = "COMPLETE";
    $this->outputData['other_detail_status'] = "COMPLETE";
    $this->outputData['parent_detail_status'] = "COMPLETE";
    $this->outputData['desired_detail_status'] = "COMPLETE";
    $this->outputData['qualification_detail_status'] = "COMPLETE";
    $this->outputData['work_exp_status'] = "COMPLETE";
    $this->outputData['emergency_contact_detail_status'] = "COMPLETE";

    foreach ($personalProfile as $keyper => $valueper) {

      if (empty($valueper)) {
        $this->outputData['personal_detail_status'] = "INCOMPLETE";
      }
    }

    foreach ($personalProfileLW as $keyper => $valueper) {

      if (empty($valueper)) {
        $this->outputData['personal_detail_status_lw'] = "INCOMPLETE";
      }
    }

    foreach ($otherDetailProfile as $keyper => $valueper) {

      if (empty($valueper)) {
        $this->outputData['other_detail_status'] = "INCOMPLETE";
      }
    }

    foreach ($parentProfile as $keypar => $valuepar) {

      if (empty($valuepar)) {
        $this->outputData['parent_detail_status'] = "INCOMPLETE";
      }
    }

    if (empty($userDetails->desired_destination)) {
      $this->outputData['desired_detail_status'] = "INCOMPLETE";
    }

    foreach ($workExpProfile as $keywe => $valuewe) {

      if (empty($valuewe)) {
        $this->outputData['work_exp_status'] = "INCOMPLETE";
      }
    }

    foreach ($emergencyContactProfile as $keyce => $valuece) {

      if (empty($valuece)) {
        $this->outputData['emergency_contact_detail_status'] = "INCOMPLETE";
      }
    }

    //print_r($userDetails); exit;


    //desired courses

    if ($this->user_model->checkDesiredCourse(array('student_id' => $userDetails->student_id))) {

      $desireInfoObj = $this->user_model->checkDesiredCourse(array('student_id' => $userDetails->student_id));
      $this->outputData['desiredcourse_details'] = $desireInfoObj;

      $this->outputData['desired_destination'] = json_decode($desireInfoObj->desired_country, true);
      $this->outputData['selected_desired_sub_course_name'] = json_decode($desireInfoObj->desired_sub_course_name, true);

      $this->outputData['mainstreams'] = $this->course_model->getAllCollegeCategories();
      $this->outputData['degrees'] = $this->degree_model->getDegree(array('degree_master.status' => 1));
      $this->outputData['submainstreams'] = $this->course_model->getCourseCategoriesByCollegeCategory($desireInfoObj->desired_course_name);

      $this->outputData['intake_month'] = json_decode(json_encode([
        ['id' => 'January', 'name' => 'January'],
        ['id' => 'February', 'name' => 'February'],
        ['id' => 'March', 'name' => 'March'],
        ['id' => 'April', 'name' => 'April'],
        ['id' => 'May', 'name' => 'May'],
        ['id' => 'June', 'name' => 'June'],
        ['id' => 'July', 'name' => 'July'],
        ['id' => 'August', 'name' => 'August'],
        ['id' => 'September', 'name' => 'September'],
        ['id' => 'October', 'name' => 'October'],
        ['id' => 'November', 'name' => 'November'],
        ['id' => 'December', 'name' => 'December']
      ]), FALSE);

      $this->outputData['intake_year'] = json_decode(json_encode([
        ['id' => '2020', 'name' => '2020'],
        ['id' => '2021', 'name' => '2021'],
        ['id' => '2022', 'name' => '2022'],
        ['id' => '2023', 'name' => '2023'],
        ['id' => '2024', 'name' => '2024']
      ]), FALSE);
    } else {

      $this->outputData['degrees'] = $this->degree_model->getDegree(array('degree_master.status' => 1));

      $this->outputData['intake_month'] = json_decode(json_encode([
        ['id' => 'January', 'name' => 'January'],
        ['id' => 'February', 'name' => 'February'],
        ['id' => 'March', 'name' => 'March'],
        ['id' => 'April', 'name' => 'April'],
        ['id' => 'May', 'name' => 'May'],
        ['id' => 'June', 'name' => 'June'],
        ['id' => 'July', 'name' => 'July'],
        ['id' => 'August', 'name' => 'August'],
        ['id' => 'September', 'name' => 'September'],
        ['id' => 'October', 'name' => 'October'],
        ['id' => 'November', 'name' => 'November'],
        ['id' => 'December', 'name' => 'December']
      ]), FALSE);

      $this->outputData['intake_year'] = json_decode(json_encode([
        ['id' => '2020', 'name' => '2020'],
        ['id' => '2021', 'name' => '2021'],
        ['id' => '2022', 'name' => '2022'],
        ['id' => '2023', 'name' => '2023'],
        ['id' => '2024', 'name' => '2024']
      ]), FALSE);

      $this->outputData['desiredcourse_details'] = '';
      $this->outputData['desired_detail_status'] = "INCOMPLETE";
    }



    //Secondary Qualification

    if ($this->user_model->checkSecondaryQualification(array('student_id' => $userDetails->student_id))) {

      $this->outputData['secondaryqualification_details'] = $this->user_model->checkSecondaryQualification(array('student_id' => $userDetails->student_id));
    } else {

      $this->outputData['secondaryqualification_details'] = '';
      $this->outputData['qualification_detail_status'] = "INCOMPLETE";
    }


    //Diploma

    if ($this->user_model->checkDiplomaQualification(array('student_id' => $userDetails->student_id))) {

      $this->outputData['diplomaqualification_details'] = $this->user_model->checkDiplomaQualification(array('student_id' => $userDetails->student_id));
    } else {

      $this->outputData['diplomaqualification_details'] = '';
      $this->outputData['qualification_detail_status'] = "INCOMPLETE";
    }



    // 12th

    if ($this->user_model->checkHigher_secondary_qualification(array('student_id' => $userDetails->student_id))) {

      $this->outputData['highersecondaryqualification_details'] = $this->user_model->checkHigher_secondary_qualification(array('student_id' => $userDetails->student_id));
    } else {

      $this->outputData['highersecondaryqualification_details'] = '';
      $this->outputData['qualification_detail_status'] = "INCOMPLETE";
    }



    //Bachelor

    if ($this->user_model->checkBachelorQualification(array('student_id' => $userDetails->student_id))) {

      $this->outputData['bachelorqualification_details'] = $this->user_model->checkBachelorQualification(array('student_id' => $userDetails->student_id));
    } else {

      $this->outputData['bachelorqualification_details'] = '';
      $this->outputData['qualification_detail_status'] = "INCOMPLETE";
    }



    //Masters

    if ($this->user_model->checkMasterQualification(array('student_id' => $userDetails->student_id))) {

      $this->outputData['mastersqualification_details'] = $this->user_model->checkMasterQualification(array('student_id' => $userDetails->student_id));
    } else {

      $this->outputData['mastersqualification_details'] = '';
      $this->outputData['qualification_detail_status'] = "INCOMPLETE";
    }


    //Qualified Exams
    if ($this->user_model->checkQualifiedExam(array('student_id' => $userDetails->student_id))) {
      $this->outputData['qualified_exams'] = $this->user_model->checkQualifiedExam(array('student_id' => $userDetails->student_id));
      $this->outputData['qualified_exams']->competitive_exam_stage = json_decode($this->outputData['qualified_exams']->competitive_exam_stage, true);
      $this->outputData['qualified_exams']->english_exam_stage = json_decode($this->outputData['qualified_exams']->english_exam_stage, true);
    } else {
      $this->outputData['qualified_exams'] = '';
      $this->outputData['qualification_detail_status'] = "INCOMPLETE";
    }




    $this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status' => 1));

    $this->outputData['locationcountries'] = $this->country_model->getLocationCountries(array('location_country_master.status' => 1));

    //$this->outputData['intakes'] = $this->additional_model->getIntakes(array('intake_master.status' => '1'));

    $this->outputData['documentTypes'] = $this->user_model->getDocumentsList();
    $this->outputData['uploadedDocuments'] = $this->user_model->getDocumentsByStudent($userDetails->id);

    $activeTab = $this->input->get('tab') ? $this->input->get('tab') : 'personal_details';
    $this->outputData['active_tab'] = $activeTab;

    $myAppliedUniversities = $this->user_model->getAppliedUniversitiesByUser($this->outputData['id']);
    /*echo "<pre>"; print_r($this->db->last_query());
        echo "<pre>"; print_r($this->session->userdata('user'));

        $this->db->select('*');
			  	$this->db->from('users_applied_universities');

			  	//$this->db->where('user_id',$userdata['id']);
					$query = $this->db->get();
					$resultPri=$query->result();
					echo "<pre>"; print_r($resultPri);
        echo "<pre>"; print_r($myAppliedUniversities); exit;*/
    if ($myAppliedUniversities) {
      $applicationId = $applicationId ? $applicationId : $myAppliedUniversities[0]['id'];
      $universityId = $myAppliedUniversities[0]['university_id'];

      //$applicationNotes = $this->user_model->notesList($userdata['id']);

      //if($applicationNotes){
      foreach ($myAppliedUniversities as $keyapp => $valueapp) {

        if ($applicationId == $valueapp['id']) {
          $applicationType = $valueapp['application_fill_option'];
          $applicationStatus = $valueapp['status'];
          $universityId = $valueapp['university_id'];
          $this->outputData['application_university_name'] = $valueapp['name'];
          $this->outputData['university_application_link'] = $valueapp['university_application_link'];
          $this->outputData['university_application_id'] = $valueapp['university_application_id'];
          $this->outputData['university_student_id'] = $valueapp['university_student_id'];
          $this->outputData['university_application_username'] = $valueapp['university_application_username'];
          $this->outputData['university_application_password'] = $valueapp['university_application_password'];
          $this->outputData['university_application_course'] = $valueapp['university_application_course'];

          $this->outputData['university_course_link'] = $valueapp['university_course_link'];
          $this->outputData['university_course_name'] = $valueapp['university_course_name'];
          $this->outputData['university_course_level'] = $valueapp['university_course_level'];
          $this->outputData['university_course_intake_month'] = $valueapp['university_course_intake_month'];
          $this->outputData['university_course_intake_year'] = $valueapp['university_course_intake_year'];
        }
        // code...
        //foreach ($applicationNotes as $keynote => $valuenote) {
        //if( $valueapp['id'] == $valuenote['entity_id']){

        //$myAppliedUniversities[$keyapp]['noteslist'][] = $applicationNotes[$keynote];

        //}
        //}
      }
      //  }

      $applicationDocList = $this->user_model->appDocList($userdata['id']);


      $this->outputData['myApplicationDocList'] = $applicationDocList;
    }

    $this->outputData['application_credential_detail_status'] = "COMPLETE";

    $application_credential_detail_status = [
      $this->outputData['university_application_link'],
      $this->outputData['university_application_id'],
      $this->outputData['university_student_id'],
      $this->outputData['university_application_username'],
      $this->outputData['university_application_password'],
      $this->outputData['university_application_course']
    ];

    foreach ($application_credential_detail_status as $keyacds => $valueacds) {

      if (empty($valueacds)) {
        $this->outputData['application_credential_detail_status'] = "INCOMPLETE";
      }
    }

    $this->outputData['desiredCourseProfile'] = "COMPLETE";

    $desiredCourseProfile = [
      $this->outputData['university_course_link'],
      $this->outputData['university_course_name'],
      $this->outputData['university_course_level'],
      $this->outputData['university_course_intake_month'],
      $this->outputData['university_course_intake_year']
    ];

    foreach ($desiredCourseProfile as $keywe => $valuewe) {

      if (empty($valuewe)) {
        $this->outputData['desiredCourseProfile'] = "INCOMPLETE";
      }
    }


    $this->outputData['myApplications'] = $myAppliedUniversities;

    // Application Section
    if ($applicationId) {
      $appliedUniversityDetail = $this->user_model->getApplicationById($applicationId);

      $userId = $userdata['id'];

      $appliedUniversityDetail[0]['sop'] = $appliedUniversityDetail[0]['sop'] ? json_decode($appliedUniversityDetail[0]['sop'], true) : $appliedUniversityDetail[0]['sop'];
      $appliedUniversityDetail[0]['lor'] = $appliedUniversityDetail[0]['lor'] ? json_decode($appliedUniversityDetail[0]['lor'], true) : $appliedUniversityDetail[0]['lor'];
      $appliedUniversityDetail[0]['transcripts'] = $appliedUniversityDetail[0]['transcripts'] ? json_decode($appliedUniversityDetail[0]['transcripts'], true) : $appliedUniversityDetail[0]['transcripts'];
      $appliedUniversityDetail[0]['application_fee'] = $appliedUniversityDetail[0]['application_fee'] ? json_decode($appliedUniversityDetail[0]['application_fee'], true) : $appliedUniversityDetail[0]['application_fee'];
      $this->outputData['applied_university_detail'] = $appliedUniversityDetail[0];

      $uni_detail = $this->user_model->getUniversityDataByid($universityId, '');
      $universityName = $uni_detail[0]['name'];
      $universityLoginId = $uni_detail[0]['user_id'];

      $appliedCollegesId = array();
      /**
       * If student has already applied in some other college for the same university
       * Get all applied college list
       */
      if (!$appliedUniversityDetail[0]['college_id']) {
        $alreadyAppliedColleges = $this->user_model->getAppliedColleges($userId, $universityId);
        if ($alreadyAppliedColleges) {
          foreach ($alreadyAppliedColleges as $appliedCollege) {
            if (!in_array($appliedCollege['college_id'], $appliedCollegesId)) {
              $appliedCollegesId[] = $appliedCollege['college_id'];
            }
          }
        }
      }

      $colleges = array();
      foreach ($uni_detail as $detail) {
        if (!in_array($detail['college'], $colleges) && !in_array($detail['college_id'], $appliedCollegesId)) {
          $colleges[$detail['college_id']] = $detail['college'];
        }
      }

      $courseDetail = array();

      if ($appliedUniversityDetail[0]['course_id']) {
        $this->load->model('student_registration/course_model');
        $courseDetail = $this->course_model->getCourseDetailById($appliedUniversityDetail[0]['course_id']);
        $courseDetail = $courseDetail[0];
      }
      $this->outputData['sop_no_of_words'] = $appliedUniversityDetail[0]['sop'] && isset($appliedUniversityDetail[0]['sop']['sop_needed']) && $appliedUniversityDetail[0]['sop']['sop_needed'] ? 'block' : 'none';
      $this->outputData['lor_required'] = $appliedUniversityDetail[0]['lor'] && isset($appliedUniversityDetail[0]['lor']['lor_needed']) && $appliedUniversityDetail[0]['lor']['lor_needed'] ? 'block' : 'none';
      $this->outputData['lor_type'] = $appliedUniversityDetail[0]['lor'] && isset($appliedUniversityDetail[0]['lor']['lor_needed']) && $appliedUniversityDetail[0]['lor']['lor_needed'] ? 'block' : 'none';

      $visaDetail = $this->user_model->checkVisaByApplication($applicationId);
      $this->outputData['show_visa'] = 0;
      $this->outputData['visa_applied'] = 0;
      if ($appliedUniversityDetail[0]['status'] == 'I 20 Received') {
        $this->outputData['show_visa'] = 1;
      }
      if ($visaDetail) {
        $visaDetail[0]['mock_dates'] = $visaDetail[0]['mock_dates'] ? json_decode($visaDetail[0]['mock_dates'], true) : $visaDetail[0]['mock_dates'];
        $visaDetail = $visaDetail[0];
        $this->outputData['show_visa'] = 1;
      } else {
        $anyVisaApplication = $this->user_model->getVisaByUser($userId);
        if ($anyVisaApplication) {
          $this->outputData['visa_applied'] = 1;
        }
      }

      $this->outputData['university_id'] = $universityId;
      $this->outputData['application_id'] = $applicationId;
      $this->outputData['university_login_id'] = $universityLoginId;
      $this->outputData['university_name'] = $universityName;
      $this->outputData['colleges'] = $colleges;
      $this->outputData['visa_detail'] = $visaDetail;
      $this->outputData['course_detail'] = $courseDetail;
      $this->outputData['current_status'] = $appliedUniversityDetail[0]['status'];
    }

    //document list with uploaded doc

    $documentDetail = $this->user_model->getNewDocumentList(10000);

    $documentUseDocDetail = $this->user_model->getNewDocumentUserDocList($userdata['id'], 10000);

    $finalDocList = array();
    $finalUserDocList = array();
    $docFinalList = array();
    $docUrlCount = [];

    $uniqueListDoc = array();
    $uniqueArray = [];

    if ($documentDetail['data']) {

      foreach ($documentDetail['data'] as $documents) {

        $finalDocList[$documents['product_document_id']][] = [
          "document_master_id" => $documents['document_master_id'],
          "display_name" => $documents['display_name'],
        ];
      }

      foreach ($documentUseDocDetail['data'] as $userDocuments) {
        $finalUserDocList[$userDocuments['product_document_id']][] = [
          "document_master_id" => $userDocuments['document_master_id'],
          "document_id" => $userDocuments['document_id'],
          "display_name" => $userDocuments['display_name'],
          "document_display_name" => $userDocuments['document_display_name'],
          "document_url" => $userDocuments['document_url']
        ];
      }

      /*foreach($documentDetail['data'] as $document1){

				if(!in_array($document1['product_document_id'], $uniqueArray))
				{

					$document1['doc_list'] = $finalDocList[$document1['product_document_id']];
					$document1['user_doc_list'] = isset($finalUserDocList[$document1['product_document_id']]) ? $finalUserDocList[$document1['product_document_id']] : NULL;
					$document1['document_count'] = isset($finalUserDocList[$document1['product_document_id']]) ? count($finalUserDocList[$document1['product_document_id']]) : NULL;
					$uniqueListDoc[] = $document1;
					$uniqueArray[] = $document1['product_document_id'];

				}

			}*/

      foreach ($documentDetail['data'] as $document1) {
        if ($desiredUniversityCourseLevel != 1) {
          if ($document1['product_document_id'] == 7 || $document1['product_document_id'] == 8 || $document1['product_document_id'] == 10) {
            continue;
          } else {
            if (!in_array($document1['product_document_id'], $uniqueArray)) {
              $document1['doc_list'] = $finalDocList[$document1['product_document_id']];
              $document1['user_doc_list'] = isset($finalUserDocList[$document1['product_document_id']]) ? $finalUserDocList[$document1['product_document_id']] : NULL;
              $document1['document_count'] = isset($finalUserDocList[$document1['product_document_id']]) ? count($finalUserDocList[$document1['product_document_id']]) : NULL;
              $uniqueListDoc[] = $document1;
              $uniqueArray[] = $document1['product_document_id'];
            }
          }
        } else {
          if (!in_array($document1['product_document_id'], $uniqueArray)) {
            $document1['doc_list'] = $finalDocList[$document1['product_document_id']];
            $document1['user_doc_list'] = isset($finalUserDocList[$document1['product_document_id']]) ? $finalUserDocList[$document1['product_document_id']] : NULL;
            $document1['document_count'] = isset($finalUserDocList[$document1['product_document_id']]) ? count($finalUserDocList[$document1['product_document_id']]) : NULL;
            $uniqueListDoc[] = $document1;
            $uniqueArray[] = $document1['product_document_id'];
          }
        }
      }

      /*foreach($uniqueListDoc as $document12)
			{
				$docFinalList[$document12['tab_name']][] = $document12;
			}*/
      //echo "<pre>";print_r($uniqueListDoc); exit;

      foreach ($uniqueListDoc as $document12) {
        if ($document12['tab_name'] == 'APPLICATION_SPECIFIC') {
          if ($document12['user_doc_list']) {
            $document12['user_doc_list'] = array_filter($document12['user_doc_list'], function ($item) use ($applicationId) {
              return $item["application_id"] === $applicationId;
            });
            $document12['user_doc_list'] = array_values($document12['user_doc_list']);
            $docFinalList[$document12['tab_name']][] = $document12;
          } else {
            $docFinalList[$document12['tab_name']][] = $document12;
          }
        } else {
          $docFinalList[$document12['tab_name']][] = $document12;
        }
      }
    }
    $this->outputData['new_doc_list'] = $docFinalList;

    //echo"<pre>"; 
    //var_dump($docFinalList);
    //die($applicationType);

    $applicationProcessInfoObj = $this->common_model->get(["entity_id" => $universityId, "entity_type" => "UNIVERSITY"], "university_application_process_info");
    $this->outputData['university_Application_info'] = [];

    if ($applicationProcessInfoObj['data']) {
      $applicationProcessInfoObj['data'][0]['application_fill_option'] = json_decode($applicationProcessInfoObj['data'][0]['application_fill_option'], true);
      $this->outputData['university_Application_info'] = $applicationProcessInfoObj['data'][0];
    }
    $this->outputData['selected_app_id'] = $applicationId;

    //status master
    $statusList = $this->common_model->get(["status" => 1], "lead_state_master");
    $statusValue = 1;

    foreach ($statusList['data'] as $key => $value) {
      if ($applicationStatus == $value['name']) {
        $statusValue = "0";
      }
      $statusList['data'][$key]['COMPLETED'] = $statusValue;
    }

    $this->outputData['lead_state_list'] = $statusList['data'];

    //notifications
    $notificationList = $this->user_model->getNotificationList($userId);
    $this->outputData['notification_list'] = $notificationList['data'];
    $this->outputData['current_lead_state'] = $applicationStatus;

    if ($applicationType == 1) {
      $this->render_page('templates/student_registration/student_profile_new_fill', $this->outputData);
    } else {
      $this->render_page('templates/student_registration/student_profile_new', $this->outputData);
    }
  }

  function notesInsert()
  {
    $this->load->model('student_registration/common_model');
    $userdata = $this->session->userdata('user');
    if (!$userdata) {
      echo "NOTVALIDUSER";
      exit();
    }

    $userId = $userdata['id'];
    $studentId = $this->input->post('student_id');
    $applicationId = $this->input->post('app_id');
    $notes = $this->input->post('notes');

    $insertData = [
      "entity_id" => $applicationId,
      "entity_type" => "UNIVERSITY",
      "student_login_id" => $studentId,
      "added_by" => $userId,
      "time_spent" => 0,
      "comment" => $notes
    ];

    $insertObj = $this->common_model->post($insertData, "comments");
    echo "SUCCESS";
  }

  function commentsInsert()  {
    $this->load->model('student_registration/common_model');

    $userdata = $this->session->userdata('user');
    if (!$userdata) {
      echo "NOTVALIDUSER";
      exit();
    }

    $createdById = $userdata['id'];
    $couselllorEmail = $userdata['email'] ? $userdata['email'] : 0;
    $userId = $this->input->post('user_id');
    $comments = $this->input->post('comments');
    $student_name = $this->input->post('stdName');

    $myAppliedUniversities = $this->common_model->getAppliedUniversitiesByUser($userId, $appId);

    $University_name = $myAppliedUniversities[0]['name'];
    $Course = $myAppliedUniversities[0]['university_application_course'];
    $Intake = $myAppliedUniversities[0]['university_course_intake_month'] . ' ' . $myAppliedUniversities[0]['university_course_intake_year'];
    $facilitatorEmailId = $myAppliedUniversities[0]['facilitator_email'];
    $facilitatorName = $myAppliedUniversities[0]['facilitator_name'] . ' ' . $myAppliedUniversities[0]['facilitator_last_name'];

    $insertData = [
      "user_id" => $userId,
      "title" => "Comments",
      "description" => $comments,
      "created_by" => $createdById
    ];

    $insertObj = $this->common_model->post($insertData, "notification_master");
    if ($facilitatorEmailId != '') {

      $ccMailList = "sudhir.kanojia@imperialplatforms.com,jay.r@imperialplatforms.com,$couselllorEmail";
      //$ccMailList = "priyankadthool@gmail.com";
      $mailAttachments = '';
      //$facilitatorName = '';
      //$facilitatorEmailId = "priyankadthool@gmail.com";

      $mailSubject = "Hellouni – Update – $student_name – $University_name – New Comment Added ";

      $mailTemplate = "Dear Team,

                          There is been an update on the portal for below mentioned Applicant
                          Student Name - $student_name
                          University Name - $University_name
                          Course - $Course
                          Intake - $Intake

                          Comment - $comments


                           <b>Thanks and Regards,
                           Hellouni Co-ordinator
                           </b>

                           <img src='https://www.hellouni.org/img/hello_uni_mail.png'>


                           Kindly Login to the Application portal (portal Link) to check
                          <b>**Note</b>: This is an automated email, please do not reply to this email**
                          ";

      $sendMail = sMail($facilitatorEmailId, $facilitatorName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);
    }
    echo "SUCCESS";
  }

  function save()
  {
    //echo "<pre>"; print_r($_POST); exit;
    $this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');
    $this->load->model('student_registration/course_model');
    $this->load->model('student_registration/auth_model');
    $this->load->model('student_registration/common_model');

    //$leaddata = $this->session->userdata('leaduser');
    $userdata = $this->session->userdata('user');
    if (!$userdata) {
      redirect('common/login');
    }
    //print_r($userdata); exit;
    if ($userdata['type'] == 5) {
      $userdata['id'] = $userdata['id'];
    } else {
      $userdata['id'] = $this->input->post('user_id');
    }

    $userId = $this->input->post('user_id');
    $studentId = $this->input->post('student_id');
    $applicationId = $this->input->post('app_id') ? $this->input->post('app_id') : 0;

    $formType = $this->input->post('form_type');
    switch ($formType) {
      case 'personal_details':
        if (!$userdata['fid']) {
          $details = array(
            'name'       =>  $this->input->post('name'),
            'last_name'     =>  $this->input->post('last_name')
          );
          $this->user_model->updateUserInfo($details, $userdata['id']);
        }

        $data = array(
          'address'        =>  $this->input->post('address') ? $this->input->post('address') : NULL,
          'correspondence_address'        =>  $this->input->post('correspondence_address') ? $this->input->post('correspondence_address') : NULL,
          'city'          =>  $this->input->post('city') ? $this->input->post('city') : NULL,
          'zip'          =>  $this->input->post('zip') ? $this->input->post('zip') : NULL,
          'dob'          =>  $this->input->post('dob') ? $this->input->post('dob') : NULL,
          //'phone'					=>	$this->input->post('phone') ? $this->input->post('phone') : NULL,
          'gender'      =>  $this->input->post('gender') ? $this->input->post('gender') : NULL,
          'state'      =>  $this->input->post('state') ? $this->input->post('state') : NULL,
          'country'    =>  $this->input->post('country') ? $this->input->post('country') : NULL,
          'hear_about_us'      =>  $this->input->post('hear_info') ? $this->input->post('hear_info') : NULL,
          'passport'        =>  $this->input->post('passport_availalibility') ? $this->input->post('passport_availalibility') : 0,
          'passport_number'        =>  $this->input->post('passport_number') ? $this->input->post('passport_number') : NULL,
          'passport_issue_date'        =>  $this->input->post('passport_issue_date') ? $this->input->post('passport_issue_date') : NULL,
          'passport_expiry_date'        =>  $this->input->post('passport_expiry_date') ? $this->input->post('passport_expiry_date') : NULL,
          'marital_status'        =>  $this->input->post('marital_status') ? $this->input->post('marital_status') : NULL,
          'place_of_birth'        =>  $this->input->post('place_of_birth') ? $this->input->post('place_of_birth') : NULL
        );

        $this->user_model->updateStudentDetails($data, $userdata['id']);
        break;

      case 'app_credential_detail':

        $updateObj = [
          'university_application_link'      =>  $this->input->post('university_application_link') ? $this->input->post('university_application_link') : NULL,
          'university_application_id'      =>  $this->input->post('university_application_id') ? $this->input->post('university_application_id') : NULL,
          'university_student_id'      =>  $this->input->post('university_student_id') ? $this->input->post('university_student_id') : NULL,
          'university_application_username'  =>  $this->input->post('university_application_username') ? $this->input->post('university_application_username') : NULL,
          'university_application_password'  =>  $this->input->post('university_application_password') ? $this->input->post('university_application_password') : NULL,
          'university_application_course'    =>  $this->input->post('university_application_course') ? $this->input->post('university_application_course') : NULL
        ];

        $conditionObj = ["id" => $applicationId];

        $updateApplicationObj = $this->common_model->put($conditionObj, $updateObj, "users_applied_universities");
        break;

      case 'university_course_info':

        $updateObj = [
          'university_course_link'      =>  $this->input->post('university_course_link') ? $this->input->post('university_course_link') : NULL,
          'university_course_name'      =>  $this->input->post('university_course_name') ? $this->input->post('university_course_name') : NULL,
          'university_course_level'      =>  $this->input->post('university_course_level') ? $this->input->post('university_course_level') : NULL,
          'university_course_intake_month'  =>  $this->input->post('university_course_intake_month') ? $this->input->post('university_course_intake_month') : NULL,
          'university_course_intake_year'  =>  $this->input->post('university_course_intake_year') ? $this->input->post('university_course_intake_year') : NULL
        ];

        $conditionObj = ["id" => $applicationId];

        $updateApplicationObj = $this->common_model->put($conditionObj, $updateObj, "users_applied_universities");
        break;

      case 'qualification_details':
        if ($this->input->post('secondary_institution_name')) {
          $secondary_qualification = array(
            'student_id'    =>  $studentId,
            'institution_name'  =>  $this->input->post('secondary_institution_name'),
            'board'        =>  $this->input->post('board'),
            'year_started'    =>  $this->input->post('secondary_year_starrted'),
            'year_finished'    =>  $this->input->post('secondary_year_finished'),
            'month_started'    =>  $this->input->post('secondary_month_starrted'),
            'month_finished'    =>  $this->input->post('secondary_month_finished'),
            'grade'        =>  $this->input->post('secondary_grade'),
            'total'        =>  $this->input->post('secondary_total')
          );
          if ($this->user_model->checkSecondaryQualification(array('student_id' => $studentId))) {
            $this->user_model->updateSecondaryQualification($secondary_qualification, $studentId);
          } else {
            $this->user_model->insertSecondaryQualification($secondary_qualification);
          }
        }
        if ($this->input->post('diploma_12') == 1) {
          if ($this->input->post('diploma_course_name')) {
            $diploma_qualification = array(
              'student_id'    =>  $studentId,
              'course'      =>  $this->input->post('diploma_course_name'),
              'college'      =>  $this->input->post('diploma_institution_name'),
              'year_started'    =>  $this->input->post('diploma_year_starrted'),
              'year_finished'    =>  $this->input->post('diploma_year_finished'),
              'month_started'    =>  $this->input->post('diploma_month_starrted'),
              'month_finished'  =>  $this->input->post('diploma_month_finished'),
              'aggregate'      =>  $this->input->post('diploma_grade'),
              'total'        =>  $this->input->post('diploma_total')
            );
            if ($this->user_model->checkDiplomaQualification(array('student_id' => $studentId))) {
              $this->user_model->updateDiplomaQualification($diploma_qualification, $studentId);
            } else {
              $this->user_model->insertDiplomaQualification($diploma_qualification);
            }
          }
        } else if ($this->input->post('diploma_12') == 2) {
          if ($this->input->post('hs_institution_name')) {
            $hs_qualification = array(
              'student_id'    =>  $studentId,
              'institution_name'  =>  $this->input->post('hs_institution_name'),
              'board'        =>  $this->input->post('hs_board'),
              'year_started'    =>  $this->input->post('hs_year_starrted'),
              'year_finished'    =>  $this->input->post('hs_year_finished'),
              'month_started'    =>  $this->input->post('hs_month_starrted'),
              'month_finished'    =>  $this->input->post('hs_month_finished'),
              'grade'        =>  $this->input->post('hs_grade'),
              'total'        =>  $this->input->post('hs_total')
            );
            if ($this->user_model->checkHigher_secondary_qualification(array('student_id' => $studentId))) {
              $this->user_model->updateHigher_secondary_qualification($hs_qualification, $studentId);
            } else {
              $this->user_model->insertHigher_secondary_qualification($hs_qualification);
            }
          }
        }
        if ($this->input->post('bachelor_course')) {
          $bachelor_qualification = array(
            'student_id'    =>  $studentId,
            'course'      =>  $this->input->post('bachelor_course'),
            'university'    =>  $this->input->post('bachelor_university'),
            'college'      =>  $this->input->post('bachelor_college'),
            'major'        =>  $this->input->post('bachelor_major'),
            'year_started'    =>  $this->input->post('bachelor_year_starrted'),
            'year_finished'    =>  $this->input->post('bachelor_year_finished'),
            'month_started'    =>  $this->input->post('bachelor_month_starrted'),
            'month_finished'  =>  $this->input->post('bachelor_month_finished'),
            'number_backlogs'  =>  $this->input->post('bc_no_of_backlogs'),
            'grade'        =>  $this->input->post('bachelor_grade'),
            'total'        =>  $this->input->post('bachelor_total')
          );
          if ($this->user_model->checkBachelorQualification(array('student_id' => $studentId))) {
            $this->user_model->updateBachelorQualification($bachelor_qualification, $studentId);
          } else {
            $this->user_model->insertBachelorQualification($bachelor_qualification);
          }
        }
        if ($this->input->post('masters') == 1) {
          if ($this->input->post('master_course')) {
            $master_qualification = array(
              'student_id'    =>  $studentId,
              'course'      =>  $this->input->post('master_course'),
              'university'    =>  $this->input->post('master_university'),
              'college'      =>  $this->input->post('master_college'),
              'major'        =>  $this->input->post('master_major'),
              'year_started'    =>  $this->input->post('master_year_started'),
              'year_finished'    =>  $this->input->post('master_year_finished'),
              'month_started'    =>  $this->input->post('master_month_started'),
              'month_finished'  =>  $this->input->post('master_month_finished'),
              'number_backlogs'  =>  $this->input->post('ms_no_of_backlogs'),
              'grade'        =>  $this->input->post('master_grade'),
              'total'        =>  $this->input->post('master_total')
            );
            if ($this->user_model->checkMasterQualification(array('student_id' => $studentId))) {
              $this->user_model->updateMasterQualification($master_qualification, $studentId);
            } else {
              $this->user_model->insertMasterQualification($master_qualification);
            }
          }
        }
        if ($this->input->post('competitive_exam')) {
          $competitive_exam = array(
            'student_id' => $studentId,
            'competitive_exam_type' => $this->input->post('competitive_exam'),
            'competitive_exam_stage' => json_encode(array(
              'name' => '',
              'value' => '',
              'exam_score_quant_gre' => '',
              'exam_score_verbal_gre' => '',
              'exam_score_ir' => '',
              'exam_score_awa_gre' => '',
              'exam_score_total_gre' => '',
              'exam_score_quant_gmat' => '',
              'exam_score_verbal_gmat' => '',
              'exam_score_awa_gmat' => '',
              'exam_score_total_gmat' => '',
              'exam_score_cirital_reading' => '',
              'exam_score_writing' => '',
              'exam_score_math' => '',
              'exam_score_total_sat' => '',
              'record_date_quant_gre' => '',
              'record_date_quant_gmat' => '',
              'record_date_quant_sat' => ''
            ))
          );
          if ($this->input->post('competitive_exam') != 'NO' && $this->input->post('competitive_exam') != 'NOT_REQUIRED' && $this->input->post('exam_stage')) {
            $competitive_exam['competitive_exam_stage'] = json_encode(array(
              'name' => $this->input->post('exam_stage')
            ));

            if ($this->input->post('exam_stage') == 'REGISTERED' && $this->input->post('exam_date')) {
              $competitive_exam['competitive_exam_stage'] = json_encode(array(
                'name' => $this->input->post('exam_stage'),
                'value' => $this->input->post('exam_date')
              ));
            } elseif ($this->input->post('exam_stage') == 'RESULT_WAIT' && $this->input->post('result_date')) {
              $competitive_exam['competitive_exam_stage'] = json_encode(array(
                'name' => $this->input->post('exam_stage'),
                'value' => $this->input->post('result_date')
              ));
            } elseif ($this->input->post('exam_stage') == 'RESULT_OUT') {
              if ($this->input->post('competitive_exam') == 'GRE') {
                $competitive_exam['competitive_exam_stage'] = json_encode(array(
                  'name' => $this->input->post('exam_stage'),
                  'exam_registration_gre' => $this->input->post('exam_registration_gre'),
                  'record_date_quant_gre' => $this->input->post('record_date_quant_gre'),
                  'exam_score_quant_gre' => $this->input->post('exam_score_quant_gre'),
                  'exam_score_verbal_gre' => $this->input->post('exam_score_verbal_gre'),
                  'exam_score_awa_gre' => $this->input->post('exam_score_awa_gre'),
                  'exam_score_total_gre' => $this->input->post('exam_score_total_gre')
                ));
              } elseif ($this->input->post('competitive_exam') == 'GMAT') {
                $competitive_exam['competitive_exam_stage'] = json_encode(array(
                  'name' => $this->input->post('exam_stage'),
                  'exam_registration_gmat' => $this->input->post('exam_registration_gmat'),
                  'record_date_quant_gmat' => $this->input->post('record_date_quant_gmat'),
                  'exam_score_quant_gmat' => $this->input->post('exam_score_quant_gmat'),
                  'exam_score_verbal_gmat' => $this->input->post('exam_score_verbal_gmat'),
                  'exam_score_ir' => $this->input->post('exam_score_ir'),
                  'exam_score_awa_gmat' => $this->input->post('exam_score_awa_gmat'),
                  'exam_score_total_gmat' => $this->input->post('exam_score_total_gmat')
                ));
              } elseif ($this->input->post('competitive_exam') == 'SAT') {
                $competitive_exam['competitive_exam_stage'] = json_encode(array(
                  'name' => $this->input->post('exam_stage'),
                  'record_date_quant_sat' => $this->input->post('record_date_quant_sat'),
                  'exam_score_cirital_reading' => $this->input->post('exam_score_cirital_reading'),
                  'exam_score_writing' => $this->input->post('exam_score_writing'),
                  'exam_score_math' => $this->input->post('exam_score_math'),
                  'exam_score_total_sat' => $this->input->post('exam_score_total_sat')
                ));
              }
            }
          }
          if ($this->user_model->checkQualifiedExam(array('student_id' => $studentId))) {
            $this->user_model->updateQualifiedExam($competitive_exam, $studentId);
          } else {
            $this->user_model->insertQualifiedExam($competitive_exam);
          }
        }
        if ($this->input->post('english_proficiency_exam')) {
          $english_exam = array(
            'student_id' => $studentId,
            'english_exam_type' => $this->input->post('english_proficiency_exam'),
            'english_exam_stage' => json_encode(array(
              'name' => '',
              'value' => '',
              'english_exam_record_date' => '',
              'english_exam_score_reading' => '',
              'english_exam_score_listening' => '',
              'english_exam_score_writing' => '',
              'english_exam_score_speaking' => '',
              'english_exam_score_total' => ''
            ))
          );
          if ($this->input->post('english_proficiency_exam') != 'NO' && $this->input->post('english_proficiency_exam') != 'NOT_REQUIRED' && $this->input->post('english_exam_stage')) {
            $english_exam['english_exam_stage'] = json_encode(array(
              'name' => $this->input->post('english_exam_stage')
            ));

            if ($this->input->post('english_exam_stage') == 'REGISTERED' && $this->input->post('english_exam_date')) {
              $english_exam['english_exam_stage'] = json_encode(array(
                'name' => $this->input->post('english_exam_stage'),
                'value' => $this->input->post('english_exam_date')
              ));
            } elseif ($this->input->post('english_exam_stage') == 'RESULT_WAIT' && $this->input->post('english_result_date')) {
              $english_exam['english_exam_stage'] = json_encode(array(
                'name' => $this->input->post('english_exam_stage'),
                'value' => $this->input->post('english_result_date')
              ));
            } elseif ($this->input->post('english_exam_stage') == 'RESULT_OUT') {
              if ($this->input->post('english_proficiency_exam') == 'TOEFL') {
                $english_exam['english_exam_stage'] = json_encode(array(
                  'name' => $this->input->post('english_exam_stage'),
                  'english_exam_appoint_no_toefl' => $this->input->post('english_exam_appoint_no_toefl'),
                  'english_exam_record_date_toefl' => $this->input->post('english_exam_record_date_toefl'),
                  'english_exam_score_reading_toefl' => $this->input->post('english_exam_score_reading_toefl'),
                  'english_exam_score_listening_toefl' => $this->input->post('english_exam_score_listening_toefl'),
                  'english_exam_score_writing_toefl' => $this->input->post('english_exam_score_writing_toefl'),
                  'english_exam_score_speaking_toefl' => $this->input->post('english_exam_score_speaking_toefl'),
                  'english_exam_score_total_toefl' => $this->input->post('english_exam_score_total_toefl')
                ));
              } elseif ($this->input->post('english_proficiency_exam') == 'IELTS') {
                $english_exam['english_exam_stage'] = json_encode(array(
                  'name' => $this->input->post('english_exam_stage'),
                  'english_exam_trf_no_ielts' => $this->input->post('english_exam_trf_no_ielts'),
                  'english_exam_record_date_ielts' => $this->input->post('english_exam_record_date_ielts'),
                  'english_exam_score_reading_ielts' => $this->input->post('english_exam_score_reading_ielts'),
                  'english_exam_score_listening_ielts' => $this->input->post('english_exam_score_listening_ielts'),
                  'english_exam_score_writing_ielts' => $this->input->post('english_exam_score_writing_ielts'),
                  'english_exam_score_speaking_ielts' => $this->input->post('english_exam_score_speaking_ielts'),
                  'english_exam_score_total_ielts' => $this->input->post('english_exam_score_total_ielts')
                ));
              } elseif ($this->input->post('english_proficiency_exam') == 'PTE') {
                $english_exam['english_exam_stage'] = json_encode(array(
                  'name' => $this->input->post('english_exam_stage'),
                  'english_exam_record_date_pte' => $this->input->post('english_exam_record_date_pte'),
                  'english_exam_score_reading_pte' => $this->input->post('english_exam_score_reading_pte'),
                  'english_exam_score_listening_pte' => $this->input->post('english_exam_score_listening_pte'),
                  'english_exam_score_writing_pte' => $this->input->post('english_exam_score_writing_pte'),
                  'english_exam_score_speaking_pte' => $this->input->post('english_exam_score_speaking_pte'),
                  'english_exam_score_total_pte' => $this->input->post('english_exam_score_total_pte')
                ));
              } elseif ($this->input->post('english_proficiency_exam') == 'DUOLINGO') {
                $english_exam['english_exam_stage'] = json_encode(array(
                  'name' => $this->input->post('english_exam_stage'),
                  'english_exam_record_date_duolingo' => $this->input->post('english_exam_record_date_duolingo'),
                  'english_exam_score_rea_duolingoding_duolingo' => $this->input->post('english_exam_score_reading_duolingo'),
                  'english_exam_score_listening_duolingo' => $this->input->post('english_exam_score_listening_duolingo'),
                  'english_exam_score_writing_duolingo' => $this->input->post('english_exam_score_writing_duolingo'),
                  'english_exam_score_speaking_duolingo' => $this->input->post('english_exam_score_speaking_duolingo'),
                  'english_exam_score_total_duolingo' => $this->input->post('english_exam_score_total_duolingo')
                ));
              }
            }
          }
          if ($this->user_model->checkQualifiedExam(array('student_id' => $studentId))) {
            $this->user_model->updateQualifiedExam($english_exam, $studentId);
          } else {
            $this->user_model->insertQualifiedExam($english_exam);
          }
        }
        break;
      case 'desired_courses':

        $desireCountry = is_array($this->input->post('deisred_destinations')) ?  json_encode($this->input->post('deisred_destinations')) : json_encode(explode(",", $this->input->post('deisred_destinations')));
        $desireSubCourse = is_array($this->input->post('desired_sub_course_name')) ?  json_encode($this->input->post('desired_sub_course_name')) : json_encode(explode(",", $this->input->post('desired_sub_course_name')));
        $desired_course = array(
          'student_id'           =>  $this->input->post('student_id'),
          'desired_country' => $desireCountry,
          'desired_course_name'  =>  $this->input->post('desired_course_name'),
          'desired_sub_course_name'  =>  $desireSubCourse,
          'desired_course_level'  =>  $this->input->post('desired_course_level'),
          'desired_course_intake'  =>  $this->input->post('desired_course_intake'),
          'desired_course_year'  =>  $this->input->post('desired_course_year')
        );
        //var_dump($desired_course);die();
        if ($this->user_model->checkDesiredCourse(array('student_id' => $this->input->post('student_id')))) {
          $this->user_model->updateDesiredCourse($desired_course, $this->input->post('student_id'));
        } else {
          $this->user_model->insertDesiredCourse($desired_course);
        }

        break;
      case 'parent_details':
        $data = array(
          'parent_name'    =>  $this->input->post('parent_name'),
          'parent_mob'      =>  $this->input->post('parent_mob'),
          'parent_email'   =>  $this->input->post('parent_email'),
          'parent_relation'   =>  $this->input->post('Relationship'),
          'parent_occupation'    =>  $this->input->post('parent_occupation')
        );
        //echo "<pre>";print_r($data); 
        $this->user_model->updateStudentDetails($data, $userdata['id']);
        //echo "<pre>";print_r($this->db->last_query()); exit;
        break;

      case 'work_exp':
        $tablename = "student_work_exp";
        $conddl = "user_id = '" . $userdata['id'] . "'";
        $this->user_model->delete($tablename, $conddl);
        $workExp = $_POST['company_name'];

        for ($i = 0; $i < count($workExp); $i++) {

          $data = array(
            'company_name'      =>  $_POST['company_name'][$i],
            'designation'      =>  $_POST['designation'][$i],
            'start_date'    =>  $_POST['work_start_date'][$i],
            'end_date'      =>  $_POST['work_end_date'][$i],
            'user_id' => $userdata['id']
          );
          $this->user_model->insertStudentWorkDetails($data);
        }
        break;

      case 'emergency_contact_detail':
        $data = array(
          'e_name'      =>  $this->input->post('e_name'),
          'e_mobile_number'      =>  $this->input->post('e_mobile_number'),
          'e_email'    =>  $this->input->post('e_email'),
          'e_relation'      =>  $this->input->post('e_relation')
        );
        $this->user_model->updateStudentDetails($data, $userdata['id']);
        break;

      case 'personal_details_optional':

        $details = array(
          'name'       =>  $this->input->post('name'),
          'last_name'     =>  $this->input->post('last_name')
        );
        $this->user_model->updateUserInfo($details, $userdata['id']);
        break;


      case 'other_details_optional':

        $data = array(
          'passport_number'        =>  $this->input->post('passport_number') ? $this->input->post('passport_number') : NULL,
          'passport_issue_date'        =>  $this->input->post('passport_issue_date') ? $this->input->post('passport_issue_date') : NULL,
          'passport_expiry_date'        =>  $this->input->post('passport_expiry_date') ? $this->input->post('passport_expiry_date') : NULL,
        );

        $this->user_model->updateStudentDetails($data, $userdata['id']);
        break;

      case 'change_password':
        $password = $this->input->post('password');
        $confirmPassword = $this->input->post('password2');
        if ($password != $confirmPassword) {
          $this->session->set_flashdata('flash_message', "Password and confirm password does not match.");

          redirect('user/account/update');
        }
        $data =  array(
          'password' => md5($password)
        );
        $this->user_model->updateUserInfo($data, $userdata['id']);
        echo '<script type="text/javascript">
                        alert("Your password has been updated successfully. Please login to continue.");
                        window.location.href = "/user/account/logout";
                    </script>';

        break;

      case 'fund_details':

        if ($this->input->post('source_of_funds') == 'self_fund') {

          $fund_data = array(
            'fund_type' => "self_fund",
            'user_id' => $this->input->post('user_id')
          );
        } elseif ($this->input->post('source_of_funds') == 'family') {

          $fund_data = array(
            'fund_type' => "family_fund",
            'user_id' => $this->input->post('user_id'),
            'guarantor_name' => $this->input->post('family_guarantor_name'),
            'guarantor_relation' => $this->input->post('family_Guarantor_relationship')
          );
        } elseif ($this->input->post('source_of_funds') == 'Sponsorship') {

          $fund_data = array(
            'fund_type' => "sponsorship_fund",
            'user_id' => $this->input->post('user_id'),
            'sponsor_name' => $this->input->post('sponsor_guarantor_name')
          );
        } else {

          $fund_data = array(
            'fund_type' => "other_fund",
            'user_id' => $this->input->post('user_id'),
            'guarantor_name' => $this->input->post('other_guarantor_name'),
            'guarantor_relation' => $this->input->post('other_Guarantor_relationship')
          );
        }

        if ($this->user_model->checkFundDetails(array('user_id' => $this->input->post('user_id')))) {
          $this->user_model->updateFundDetails($fund_data, $this->input->post('user_id'));
        } else {
          $this->user_model->insertFundDetails($fund_data);
        }

        break;
    }

    if ($userdata['type'] != 5) {
      redirect('/student/profile/' . $userdata['id'] . '/' . $applicationId);
    }

    if ($formType != 'change_password') {
      $this->session->set_flashdata('flash_message', "Your profile has been updated.");

      redirect('user/account/update');
    }
  }

  function login()
  {
    //Load Models - for this function
    $this->load->model('student_registration/user_model');
    $this->load->model('student_registration/auth_model');
    $this->load->model('student_registration/common_model');
    $this->load->helper('cookie');

    $username = $this->input->post('username');
    $password = md5($this->input->post('password')); //exit;

    $query  = $this->user_model->checkUser($username, $password, 1);

    //var_dump($query);die();

    if (count($query) > 0) {
      $userId = $query->id;
      $userType = $query->type;

      switch ($userType) {
        case 8:
        case 9:
          $data = array(
            'id' => $query->id,
            'name' => $query->name,
            'username' => $query->username,
            'email' => $query->email,
            'type' => $query->type,
            'typename' => $query->typename,
            'createdate' => $query->createdate,
            'fid' => '',
            'is_logged_in' => true
          );
          $this->session->set_userdata('user', $data);

          $partnerData = $this->common_model->get(array('user_id' => $userId), 'partners');

          $sessiondata = array(
            'id'       => $partnerData['data'][0]['id'],
            'user_id'   => $partnerData['data'][0]['user_id'],
            'name'      => $query->name,
            'email'    => $query->email,
            'facilitator_id'   => json_decode($partnerData['data'][0]['facilitator_id'], true),
            'other_details'     => json_decode($partnerData['data'][0]['other_details'], true)
          );

          $this->session->unset_userdata('partnerinfo');
          $this->session->set_userdata('partnerinfo', $sessiondata);
          if (strpos($this->input->post('referrer_url'), 'internship') !== false) {
            redirect($this->input->post('referrer_url'));
          }
          redirect('counsellor/dashboard');
          break;
        case 5:
          $userId = $query->id;

          $studentData = $this->common_model->get(array('user_id' => $userId), 'student_details');

          $studentId = $studentData['data'][0]['id'];

          $data = array(
            'id' => $query->id,
            'name' => $query->name,
            'username' => $query->username,
            'email' => $query->email,
            'type' => $query->type,
            'typename' => $query->typename,
            'fid' => '',
            'is_logged_in' => true,
            'student_id' => $studentId
          );

          $this->session->set_userdata('user', $data);

          $desireData = $this->common_model->get(array('student_id' => $studentId), 'desired_courses');

          $desireCountryArray = json_decode($desireData['data'][0]['desired_country'], true);
          $desireCountry = $desireCountryArray[0];

          $sessiondata = array(
            'id'       => $leadData->id,
            'name'      => $leadData->name,
            'email'    => $leadData->email,
            'phone'     => $leadData->phone,
            'mainstream'   => $desireData['data'][0]['desired_course_name'],
            'coursecategories'     => $desireData['data'][0]['desired_sub_course_name'],
            'degree'     => $desireData['data'][0]['desired_course_level'],
            'countries'    => $desireCountry,
            'student_id' => $studentId
          );
          $this->session->unset_userdata('leaduser');
          $this->session->set_userdata('leaduser', $sessiondata);

          //loginflag update
          $updateObj = ["login_flag" => 1];
          $conditionObj = ["id" => $userId];

          $updateLognFlagObj = $this->common_model->put($conditionObj, $updateObj, "user_master");

          if (strpos($this->input->post('referrer_url'), 'internship') !== false) {
            redirect($this->input->post('referrer_url'));
          }
          redirect('university/textsearch?searchtextvalue=usa+university&afl=yes');
          break;
        case 3:
          $universityData = $this->common_model->get(array('user_id' => $userId), 'universities');

          $data = array(
            'id' => $query->id,
            'name' => $universityData['data'][0]['name'],
            'username' => $query->username,
            'email' => $query->email,
            'type' => $query->type,
            'typename' => $query->typename,
            'createdate' => $query->createdate,
            'fid' => '',
            'university_id' => $universityData['data'][0]['id'],
            'is_logged_in' => true
          );
          $this->session->set_userdata('user', $data);

          redirect('university/dashboard');
          break;
        default:
          $this->session->set_flashdata('flash_message', "Sorry!!! Invalid Login.");
          redirect('user/account');
          break;
      }
    } else {
      $this->session->set_flashdata('flash_message', "Sorry!!! Invalid Login.");
      redirect('user/account');
    }
  }

  function fblogin()
  {
    //Load Models - for this function
    $this->load->model('student_registration/user_model');
    $this->load->model('student_registration/auth_model');
    $this->load->helper('cookie');
    $leaddata = $this->session->userdata('leaduser');



    $fid = $this->input->post('id'); //exit;
    $name = $this->input->post('name');
    $email = $this->input->post('email');
    $image = $this->input->post('image');
    // $password = md5($this->input->post('password')); //exit;
    $conditions     =  array('user_master.email' => $email, 'user_master.status' => 1);
    $query        = $this->user_model->checkUser($conditions);
    if (count($query) > 0) {
      $data = array(
        'name'       =>  $this->input->post('name'),
        'email'     =>  $this->input->post('email'),
        'password'     =>  '',
        'image'      =>  $this->input->post('image'),
        'type'       =>  '5',
        'createdate'   =>  date('Y-m-d H:i:s'),
        'activation'   =>  '',
        'fid'       =>  $this->input->post('id'),
        'status'     =>  '1'
        /*'address'		=>	'',
			'country'		=>	'',
			'city'			=>	'',
			'zip'			=>	'',
			'dob'			=>	'',
			'phone'			=>	''*/
      );

      if ($this->user_model->updateUserInfo($data, $query->id)) {
        $usrdata = array(
          'id' => $query->id,
          'name' => $this->input->post('name'),
          'email' => $this->input->post('email'),
          'fid' => $this->input->post('id'),
          'type' => 5,
          'is_logged_in' => true
        );
        $this->session->set_userdata('user', $usrdata);
        echo 1;
      }

      /* if($this->input->post('remember'))
						{
						    $insertData=array();
						    $insertData['username']=$this->input->post('username');
						    $insertData['password']=$this->input->post('pwd');
						    $expire=60*60*24*100;
							if( $this->auth_model->getUserCookie('uname')=='')
							{
							$this->user_model->addRemerberme($insertData,$expire);
							}
						}
						else
						{
						   $this->user_model->removeRemeberme();
						}*/
      // redirect('search/university');
    } else {
      $data = array(
        'name'       =>  $this->input->post('name'),
        'email'     =>  $this->input->post('email'),
        'password'     =>  '',
        'image'      =>  $this->input->post('image'),
        'type'       =>  '5',
        'createdate'   =>  date('Y-m-d H:i:s'),
        'activation'   =>  '',
        'fid'       =>  $this->input->post('id'),
        'status'     =>  '1',
        'address'    =>  '',
        'country'    =>  '',
        'city'      =>  '',
        'zip'      =>  '',
        'dob'      =>  '',
        'phone'      =>  ''
      );

      $userid = $this->user_model->insertStudent($data);
      if ($userid) {
        if ($leaddata['id']) {
          $leadMasterData = array(
            ' name'      => $this->input->post('name'),
            'email'    => $this->input->post('email'),
            'phone'     => $this->input->post('phone'),
          );
          $this->user_model->updateLeadInfo($leadMasterData, $leaddata['id']);

          $sessiondata = array(
            'id'       => $leaddata['id'],
            'name'      => $this->input->post('name'),
            'email'    => $this->input->post('email'),
            'phone'     => $this->input->post('phone'),
            'mainstream'   => $leaddata['mainstream'],
            'courses'     => $leaddata['courses'],
            'degree'     => $leaddata['degree'],
            'countries'    => $leaddata['countries']
          );

          $this->auth_model->clearUserSession();
          $this->session->unset_userdata('leaduser');
          $this->session->set_userdata('leaduser', $sessiondata);
        }

        $usrdata = array(
          'id' => $userid,
          'name' => $this->input->post('name'),
          'email' => $this->input->post('email'),
          'fid' => $this->input->post('id'),
          'type' => 5,
          'is_logged_in' => true
        );
        $this->session->set_userdata('user', $usrdata);

        // Mail to user for verification
        $from = 'nikhil.gupta@issc.in';
        $to = $this->input->post('email');
        $subject = "Your Account has been sucessfully created under HelloUni.";



        $message = "

			<head>

			<title>Your Account has been sucessfully created under HelloUni.</title>

			</head>

			<body>

			<p>Hi " . $this->input->post('name') . ",</p>

			<p>Your Account has been sucessfully created under HelloUni.</p>

			<br/><br/>

			<p>Your registered email credential is as follows -</p>

			<p>Email - " . $this->input->post('email') . "</p>

			<p>URL - " . base_url() . "</p>

			</body>

			";

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        // More headers
        $headers .= 'From:' . $from . "\r\n";
        mail($to, $subject, $message, $headers);
        //if(mail($to,$subject,$message,$headers)){die('++');}else{die('--');}
        $this->session->set_flashdata('flash_message', "Success: Please verify!");
        //redirect('user/account/');
        echo 1;
      }
      //$this->session->set_flashdata('flash_message', "Sorry! Invalid Login.");
      // redirect('user/account');
    }
  }

  function logout()
  {
    $this->load->model('student_registration/auth_model');
    $this->load->model('student_registration/common_model');
    $usrdata = $this->session->userdata('user');

    $userId = $usrdata['id'];
    $updateObj = ["login_flag" => 0];
    $conditionObj = ["id" => $userId];

    $updateLognFlagObj = $this->common_model->put($conditionObj, $updateObj, "user_master");

    $this->auth_model->clearUserSession();

    //$this->session->set_flashdata('flash_message', $this->common_model->flash_message('success',$this->lang->line('logout_success')));
    //$this->auth_model->clearUserCookie(array('username','password'));
    //$userdata=$this->session->userdata('user');

    $this->session->unset_userdata('user');
    $this->session->unset_userdata('leaduser');
    $this->session->unset_userdata('university');

    //unset($userdata);
    //$userdata=$this->session->userdata('user');
    //$leaduser=$this->session->userdata('leaduser');
    //print_r($userdata); print_r($leaduser); exit();
    //$this->auth_model->clearUserCookie(array('user_name','user_password'));

    redirect('user/account');

    //$this->load->view('youraccount/index',$userdata);
  } //Function logout End

  function getStates()  {
    //echo 1;
    if ($this->input->post('id')) {
      $locationcountries = $this->state_model->getLocationStates(array('state_master.country_id' => $this->input->post('id'), 'state_master.status' => 1));
      foreach ($locationcountries as $location) {
        echo "<option value='" . $location->zone_id . "'>" . $location->name . "</option>";
      }
    }
  }
  function getCities()
  {
    //echo 1;
    if ($this->input->post('id')) {
      $locationcities = $this->state_model->getCities(array('city_master.state_id' => $this->input->post('id'), 'city_master.status' => 1));
      foreach ($locationcities as $location) {
        echo "<option value='" . $location->id . "'>" . $location->city_name . "</option>";
      }
    }
  }

  function forgot()
  {
    //Load Models - for this function
    $this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');
    $this->load->model('student_registration/user_model');
    $this->load->model('student_registration/auth_model');
    $this->load->helper('cookie');

    $this->form_validation->set_rules('credencial', 'Credencial', 'required|trim|xss_clean');
    $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean');

    if ($this->form_validation->run() == FALSE) {
      $this->load->view('templates/common/login');
      //$this->session->set_flashdata('flash_message', $this->form_validation->validation_errors());
      //redirect('common/login');
      //die('sssss');
    } else {
      $credencial   = $this->input->post('credencial');
      $email     = $this->input->post('email');
      $conditions   =  array('infra_email_master.email' => $email, 'infra_email_master.contact_type' => '1');

      $query      = $this->user_model->checkUserBy_Email($conditions);
      $setting_details    = $this->settings_model->getSettings($conditions);
      foreach ($setting_details as $setting) {
        if ($setting->key == 'config_email') {
          $admin_email = $this->settings_model->getEmail($setting->value);
        }
      }
      if (count($query) > 0) {
        if ($query->status_id == 1) {
          if ($credencial == 'username') {
            $to = $query->email;
            $from = $admin_email;
            $subject = 'Your User Name';

            $username = $query->name;



            $link = '#';
            $linktext = $username;

            $text = 'Your User Name is following -';

            $body = $this->user_model->TempBody($username, $link, $linktext, $text);



            $this->user_model->SendMail($to, $from, $subject, $body);

            $this->session->set_flashdata('flash_message', "Please check your mail to get User Name.");
          } elseif ($credencial == 'password') {
            // send the mail
            $to = $query->email;
            $from = $admin_email;
            $subject = 'Please check you mail';

            $username = $query->name;


            // $user_tbl_data['activation_key']     = md5(time());


            $user_tbl_data = array('reset_key' => md5(time()));



            $this->user_model->Update_User_activation($user_tbl_data, $query->id);


            $link = base_url() . 'common/login/confirm/' . $query->id . '/' . $user_tbl_data['reset_key'];

            $linktext = $link;

            $text = 'Please click the link below or copy and fire it in the browser to reset password';



            $body = $this->user_model->TempBody($username, $link, $linktext, $text);



            $this->user_model->SendMail($to, $from, $subject, $body);

            $this->session->set_flashdata('flash_message', "Please check your mail to reset password.");
          }
        } elseif ($query->status_id == 2) {
          $this->session->set_flashdata('flash_message', "Sorry!Your account is Inactive. Please activate the account first.");
        } elseif ($query->status_id == 3) {
          $this->session->set_flashdata('flash_message', "Sorry!Your account is Blocked. Please contact to supaer admin.");
        } elseif ($query->status_id == 4) {
          $this->session->set_flashdata('flash_message', "Sorry!Your account is Deleted.");
        } else {
          $this->session->set_flashdata('flash_message', "Sorry!Invalid email. Please sign up first.");
        }
      } else {
        $this->session->set_flashdata('flash_message', "Sorry!Invalid email. Please sign up first.");
      }
    }
    redirect('common/login');
  }

  function confirm()
  {
    if ($this->uri->segment('5')) {
      $check_reset_data = array('infra_user_master.id' => $this->uri->segment('4'), 'infra_user_master.reset_key' => $this->uri->segment('5'), 'infra_user_master.status_id' => '1');
      $query = $this->user_model->CheckReset($check_reset_data);

      if (count($query) > 0) {
        $check_security_data = array('user_security_question.user_id' => $this->uri->segment('4'));
        $security_query = $this->user_model->getSecurity_Credential($check_security_data);

        $this->outputData['security_question1'] = $security_query->question1;
        $this->outputData['security_question2'] = $security_query->question2;
        $this->load->view('templates/common/security_verify', $this->outputData);
      } else {
      }
      /*echo '<pre>';
	  print_r($query);
	  echo '</pre>';*/
    }
  }

  function verify()
  {
    //echo $this->input->post('currenturl'); exit;
    if ($this->uri->segment('4')) {
      $check_security_data = array('user_security_question.user_id' => $this->uri->segment('4'));
      $query = $this->user_model->checkUser(array('activation' => $this->uri->segment('4')));
      if ($query) {
        $leadData = $this->user_model->getLead(array('email' => $query->email));
        // print_r($leadData); exit;
        $sessiondata = array(
          'id'       => $leadData->id,
          'name'      => $leadData->name,
          'email'    => $leadData->email,
          'phone'     => $leadData->phone,
          'mainstream'   => $leadData->mainstream,
          'courses'     => $leadData->courses,
          'degree'     => $leadData->degree,
          'countries'    => $leadData->countries
        );

        $this->auth_model->clearUserSession();
        $this->session->unset_userdata('leaduser');
        $this->session->set_userdata('leaduser', $sessiondata);

        $allData = $this->session->userdata('leaduser');
        $data = array(
          'id' => $query->id,
          'name' => $query->name,
          'email' => $query->email,
          'type' => $query->type,
          'typename' => $query->typename,
          'is_logged_in' => true
        );

        $this->session->set_userdata('user', $data);
        $this->user_model->updateUserInfo(array('status' => '1', 'activation' => ''), $query->id);
        redirect('search/university');
      } else {
        $this->session->set_flashdata('flash_message', "Sorry!Invalid link.");
        redirect('user/account');
      }
    } else {
      $this->session->set_flashdata('flash_message', "Sorry!Invalid link. Please sign up first.");
      redirect('user/account');
    }
  }

  function checkemail()
  {
    $email = $this->input->post('email');
    $conditions     =  array('user_master.email' => $email);
    $query = $this->user_model->checkUserEmail($conditions);
    if (count($query) > 0) {
      echo 1;
    } else {
      echo 0;
    }
  }

  /*function resetpassword(){
     if($this->input->post('submit')){
	 $user_tbl_data = array('infra_user_master.password' => md5($this->input->post('newpassword')));
	 $this->user_model->Update_User_activation($user_tbl_data,$this->uri->segment('4'));
	  $this->session->set_flashdata('flash_message', "Congrats! Pasword has been reset. Please Log In");
	  redirect('common/login');
	 }
 }*/

  public function web_login()
  { //$this->load->library('facebook');
    //$this->load->helper('url');
    //die('jjjjj');
    echo $this->facebook->logged_in();
  }

  function tokboxdemo()
  {
    $this->load->helper('cookie_helper');
    $this->load->model('student_registration/auth_model');

    //$this->render_page('templates/user/tokboxdemo',$this->outputData);
    $this->load->view('templates/user/tokboxdemo', $this->outputData);
    //$this->render_page('templates/common/index');
  }

  function forgotPassword()
  {
    $this->load->helper('cookie_helper');
    $this->load->model('student_registration/user_model');

    $userdata = $this->session->userdata('leaduser');
    if (!empty($userdata)) {
      redirect('search/university');
    }

    if ($this->input->post()) {
      $unameOrEmail = $this->input->post('uname_email');
      $userAvailable = $this->user_model->getUserMaster($unameOrEmail);
      if (!$userAvailable) {
        $this->session->set_flashdata('flash_message', "Sorry! The username or email doesn't exist");
        redirect('/user/account/forgotPassword');
      }
      $status = $userAvailable[0]['status'];
      $userId = $userAvailable[0]['id'];
      switch ($status) {
        case 2:
          $this->session->set_flashdata('flash_message', "Sorry! The account is inactive. Please activate the acccount or contact to our support.");
          redirect('/user/account/forgotPassword');
          break;
        case 3:
          $this->session->set_flashdata('flash_message', "Sorry! The account is not activated yet. Please activate the acccount or contact to our support.");
          redirect('/user/account/forgotPassword');
          break;
        case 4:
          $this->session->set_flashdata('flash_message', "Sorry! The account is blocked. Please contact to our support.");
          redirect('/user/account/forgotPassword');
          break;
        case 5:
          $this->session->set_flashdata('flash_message', "Sorry! The account is deleted. Please contact to our support.");
          redirect('/user/account/forgotPassword');
          break;
      }

      $tokenExist = $this->user_model->getForgotPasswordByUserId($userId);
      if (!$tokenExist) {
        $token = md5(date('Y-m-d H:i:s') . $userId);
        $data['token'] = $token;
        $data['user_id'] = $userId;
        $data['status'] = 0;
        $data['last_updated'] = date('Y-m-d H:i:s');
        $this->user_model->addForgotPassword($data);
      } else if ($tokenExist[0]['status']) {
        $token = md5(date('Y-m-d H:i:s') . $userId);
        $data['token'] = $token;
        $data['status'] = 0;
        $data['last_updated'] = date('Y-m-d H:i:s');
        $this->user_model->updateForgotPassword($data, $userId);
      } else {
        $token = $tokenExist[0]['token'];
      }

      $studentName = $userAvailable[0]['name'];
      $studentEmail = $userAvailable[0]['email'];
      $forgotPasswordLink = 'https://www.hellouni.org/user/account/resetPassword?token=' . $token;

      $ccMailList = '';
      $mailAttachments = '';

      $mailSubject = "HelloUni - Reset Password Instruction";
      $mailTemplate = "Hi $studentName

                                To reset your password, please click on the following link

                                <a href='$forgotPasswordLink'>$forgotPasswordLink</a>

                                <b>Thanks and Regards,
                                HelloUni Coordinator
                                +91 81049 09690</b>

                                <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

      $sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

      $this->session->set_flashdata('flash_message', "We sent a mail on your email id. Please check it and follow the instructions.");
      redirect('/user/account/forgotPassword');
    }

    $this->render_page('templates/user/forgot_password', $this->outputData);
  }

  function resetPassword()
  {
    $this->load->model('student_registration/user_model');

    $token = $this->input->get('token');
    if (!$token) {
      $this->session->set_flashdata('flash_message', "This link is invalid!");
      redirect('/user/account/forgotPassword');
    }

    $tokenExist = $this->user_model->getForgotPasswordByToken($token);
    if (!$tokenExist) {
      $this->session->set_flashdata('flash_message', "This link is invalid!");
      redirect('/user/account/forgotPassword');
    }

    $userId = $tokenExist[0]['user_id'];
    $this->outputData['user_id'] = $userId;
    $this->outputData['token'] = $token;
    $this->render_page('templates/user/reset_password', $this->outputData);
  }

  function reset()
  {
    $password = $this->input->post('password');
    $confirmPassword = $this->input->post('password2');
    $userId = $this->input->post('user_id');
    $token = $this->input->post('token');

    if (!$password || !$confirmPassword || !$userId) {
      $this->session->set_flashdata('flash_message', "There is some issue occured. Please try again!");
      redirect('/user/account/resetPassword?token=' . $token);
    }

    if ($password != $confirmPassword) {
      $this->session->set_flashdata('flash_message', "Password and confirm password does not match. Please try again!");
      redirect('/user/account/resetPassword?token=' . $token);
    }

    $this->load->model('student_registration/user_model');
    $tokenData['status'] = 1;
    $tokenData['last_updated'] = date('Y-m-d H:i:s');
    $this->user_model->updateForgotPassword($tokenData, $userId);

    $data =  array(
      'password' => md5($password)
    );
    $this->user_model->updateUserInfo($data, $userId);
    echo '<script type="text/javascript">
                alert("Your password has been updated successfully. Please login to continue.");
                window.location.href = "/user/account/logout";
            </script>';
  }

  function document()
  {
    $this->load->helper('cookie_helper');
    $this->load->model('student_registration/user_model');
    $userdata = $this->session->userdata('user');
    if (!$userdata) {
      redirect('user/account');
    }

    $postVariables = $this->input->post();
    $studentId = $userdata['id'];
    $documentType = $postVariables['document_type'];

    if (!file_exists('./uploads/student_' . $studentId)) {
      mkdir('./uploads/student_' . $studentId, 0777, true);
    }

    $uploadFilePath     = './uploads/student_' . $studentId;
    $uploadPath         = '/uploads/student_' . $studentId;
    $pathInfo           = pathinfo($_FILES['document']['name']);
    $tempfilename       = preg_replace('/\s+/', '_', $pathInfo['filename']);
    $tempfilename       = str_replace('.', '_', $tempfilename);
    $tempfilename       = str_replace('&', '_', $tempfilename);
    $tempfilename       = str_replace('-', '_', $tempfilename);
    $tempfilename       = str_replace('+', '_', $tempfilename);

    $fileExt                    = $pathInfo['extension'];
    $fileName                   = $studentId . '_' . $documentType . '_' . $tempfilename . '.' . $fileExt;
    $config['upload_path']      = $uploadFilePath;
    $config['file_name']        = $fileName;
    $config['allowed_types']    = 'gif|jpg|png|JPEG|JPG|jpeg|pdf|PDF|webp|WEBP';
    $config['max_size']         = 2048;
    $config['max_width']        = 10240;
    $config['max_height']       = 7680;

    $this->load->library('upload', $config);

    $studentDocument = $this->user_model->getDocumentByStudentType($studentId, $documentType);
    if ($studentDocument) {
      $oldFile = $_SERVER['DOCUMENT_ROOT'] . $uploadPath . '/' . $studentDocument->name;
      if (file_exists($oldFile)) {
        unlink($oldFile);
      }
    }

    if (!$this->upload->do_upload('document')) {
      $this->outputData['document_error'] = $this->upload->display_errors();
    } else {
      if ($studentDocument) {
        $updatedData = [
          "name" => $fileName,
          "url" => base_url() . $uploadPath . '/' . $fileName
        ];
        $documentUpdate = $this->user_model->updateDocumentByStudentType($studentId, $documentType, $updatedData);
      } else {
        $insertData = [
          "user_id"           => $studentId,
          "name"              => $fileName,
          "url"      => base_url() . $uploadPath . '/' . $fileName,
          "document_meta_id"  => $documentType
        ];

        $documentInsert = $this->user_model->addDocument($insertData);
      }
      redirect('/user/account/update');
    }
  }

  function appDocUpdate()
  {
    $this->load->helper('cookie_helper');
    $this->load->model('student_registration/user_model');
    $userdata = $this->session->userdata('user');
    if (!$userdata) {
      redirect('common/login');
    }

    $postVariables = $this->input->post();

    if ($userdata['type'] == 5) {
      $studentId = $userdata['id'];
    } else {
      $studentId = $postVariables['student_id'];
    }

    $documentType = $postVariables['document_type'];
    if (!file_exists('./uploads/student_' . $studentId)) {
      mkdir('./uploads/student_' . $studentId, 0777, true);
    }

    $uploadFilePath     = './uploads/student_' . $studentId;
    $uploadPath         = '/uploads/student_' . $studentId;
    $pathInfo           = pathinfo($_FILES['document']['name']);
    $tempfilename       = preg_replace('/\s+/', '_', $pathInfo['filename']);
    $tempfilename       = str_replace('.', '_', $tempfilename);
    $tempfilename       = str_replace('&', '_', $tempfilename);
    $tempfilename       = str_replace('-', '_', $tempfilename);
    $tempfilename       = str_replace('+', '_', $tempfilename);

    $fileExt                    = $pathInfo['extension'];
    $fileName                   = $studentId . '_' . $documentType . '_' . $tempfilename . '.' . $fileExt;
    $config['upload_path']      = $uploadFilePath;
    $config['file_name']        = $fileName;
    $config['allowed_types']    = 'gif|jpg|png|JPEG|JPG|jpeg|pdf|PDF|webp|WEBP';
    $config['max_size']         = 2048;
    $config['max_width']        = 10240;
    $config['max_height']       = 7680;

    $this->load->library('upload', $config);

    $studentDocument = $this->user_model->getDocumentByStudentType($studentId, $documentType);
    if ($studentDocument) {
      $oldFile = $_SERVER['DOCUMENT_ROOT'] . $uploadPath . '/' . $studentDocument->name;
      if (file_exists($oldFile)) {
        unlink($oldFile);
      }
    }

    if (!$this->upload->do_upload('document')) {
      //$this->outputData['document_error'] = $this->upload->display_errors();
      echo $this->upload->display_errors();
      exit();
    } else {
      if ($studentDocument) {
        $updatedData = [
          "name" => $fileName,
          "url" => base_url() . $uploadPath . '/' . $fileName
        ];
        $documentUpdate = $this->user_model->updateDocumentByStudentType($studentId, $documentType, $updatedData);
      } else {
        $insertData = [
          "user_id"           => $studentId,
          "name"              => $fileName,
          "url"      => base_url() . $uploadPath . '/' . $fileName,
          "document_meta_id"  => $documentType
        ];

        $documentInsert = $this->user_model->addDocument($insertData);
      }
      echo "SUCCESS";
      //redirect('/user/account/update');
    }
  }

  function deleteDoc()
  {
    $this->load->model('student_registration/common_model');

    $userId = $this->input->post('user_id') ? $this->input->post('user_id') : 0;
    $docId = $this->input->post('doc_master_id') ? $this->input->post('doc_master_id') : 0;
    $deleteDocCondition = [
      "user_id" => $userId,
      "document_meta_id" => $docId
    ];
    //die("SUCCESS");
    $deleteDocObj = $this->common_model->delete($deleteDocCondition, "document");
    $response['status'] = "SUCCESS";
    //die("SUCCESS");
    header('Content-Type: application/json');
    echo (json_encode($response));
  }

  function uta_document($base64Username = NULL)
  {
    if (!$base64Username) {
      echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
      exit;
    }
    $username = base64_decode($base64Username);
    $userDetail = $this->user_model->getUserMaster($username);
    if (!$userDetail) {
      echo '<script type="text/javascript">alert("Please register first."); window.location.href="/";</script>';
      exit;
    }
    $userdata['id'] = $userDetail[0]['id'];
    $userdata['type'] = 5;
    $this->session->set_userdata('user', $userdata);

    $uploadedDocuments = $this->user_model->getApplicationDocumentsByStudent($userdata['id']);
    $documentList = $this->user_model->getApplicationDocumentByUniversity(2);

    foreach ($documentList as $index => $document) {
      foreach ($uploadedDocuments as $uploadedDocument) {
        if ($uploadedDocument['document_meta_id'] == $document['id']) {
          $document['document_url'] = $uploadedDocument['url'];
        }
      }
      $documentList[$index] = $document;
    }

    $this->outputData['documents'] = $documentList;
    $this->outputData['user_id'] = 1;
    $this->render_page('templates/user/uta_document', $this->outputData);
  }
} //End  Home Class
