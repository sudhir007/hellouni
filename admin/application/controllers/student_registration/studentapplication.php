<?php

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);
*/
require_once APPPATH . 'libraries/Mail/sMail.php';

class Studentapplication extends MY_Controller
{

  public $outputData;    //Holds the output data for each view
  public $loggedInUser;


  function __construct()
  {

    parent::__construct();

    $this->load->library('template');

    $this->lang->load('enduser/home', $this->config->item('language_code'));

    $this->load->library('form_validation');

    $this->load->model('student_registration/country_model');
    $this->load->model('student_registration/webinar_model');
    $this->load->model('student_registration/common_model');
    $this->load->model('student_registration/course_model');
    $this->load->model('student_registration/degree_model');
    $this->load->model('student_registration/user_model');
    $this->load->model('student_registration/counsellor_model');
  }

  function studentMyApplicationList()
  {
    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $agentId = $userdata['id'];

    $studentSubAppListObj = $this->common_model->studentMyApplicationList($agentId);
    $this->outputData['students'] = $studentSubAppListObj;
    $this->outputData['view'] = 'list';

    $this->render_page('templates/student_registration/studentMyApplicationList', $this->outputData);
  }

  function studentAllApplicationList()
  {
    //echo "<pre>"; print_r($_POST); exit;
    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $agentId = $userdata['id'];

    $studentSubAppListObj = $this->common_model->studentAllApplicationList();
    $this->outputData['students'] = $studentSubAppListObj;
    $this->outputData['view'] = 'counceller';
    $this->outputData['breadcrumb'] = 'All Application List';
    $this->outputData['view'] = 'list';

    $this->render_page('templates/student_registration/all', $this->outputData);
  }

  function ajax_manage_page_studentAllApplicationList()
  {
    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }
    $list = $this->common_model->get_datatables_studentAllApplicationList();

    $data = array();
    $no = $_POST['start'];

    foreach ($list as $person) {
      $agency_name = json_decode($person->agency_name, true);

      $no++;
      $row = array();
      $row[] = $person->application_id;
      $row[] = $person->student_name;
      $row[] = $person->status;
      $row[] = $agency_name['agency_name'];
      $row[] = $person->facilitator_name;
      $row[] = $person->facilitator_email;
      $row[] = $person->university_name;

      //add html for action
      /*$row[] = "<a href='/admin/student/application/newList/'.$person->user_id.'/'.$person->application_id><button class='btn btn-primary btn-circle'   style='float:left'  value="">Assign to me</button></a>";*/

      $row[] = anchor(base_url('/student/application/newList/' . $person->user_id . '/' . $person->application_id), '<span title="Edit" class="edit"><i class="fa fa-edit"></i></span>');


      $data[] = $row;
    }
    //echo "<pre>"; print_r($data); exit;
    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->common_model->count_all_studentAllApplicationList(),
      "recordsFiltered" => $this->common_model->count_filtered_studentAllApplicationList(),
      "data" => $data,
    );
    //echo "<pre>"; print_r($output); exit;
    //output to json format
    echo json_encode($output);
  }

  function studentMyApplicationListNew()
  {

    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $agentId = $userdata['id'];

    $studentSubAppListObj = $this->common_model->studentSubmitApplicationListNew($agentId);

    $this->outputData['students'] = $studentSubAppListObj;
    $this->outputData['view'] = 'list';

    $this->render_page('templates/student_registration/studentMyApplicationList', $this->outputData);
  }

  function ajax_manage_page_studentMyApplicationListNew()
  {
    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $agentId = $userdata['id'];
    $list = $this->common_model->get_datatables_studentMyApplicationListNew($agentId);
    //echo "<pre>"; print_r($list); exit;
    $data = array();
    $no = $_POST['start'];

    foreach ($list as $person) {
      $agency_name = json_decode($person->agency_name, true);

      $no++;
      $row = array();
      $row[] = $person->application_id;
      $row[] = $person->student_name;
      $row[] = $person->status;
      $row[] = $agency_name['agency_name'];
      $row[] = $person->facilitator_name;
      $row[] = $person->facilitator_email;
      $row[] = $person->university_name;

      //add html for action
      /*$row[] = "<a href='/admin/student/application/newList/'.$person->user_id.'/'.$person->application_id><button class='btn btn-primary btn-circle'   style='float:left'  value="">Assign to me</button></a>";*/

      $row[] = anchor(base_url('/student/profile/' . $person->user_id . '/' . $person->application_id), '<button title="Edit" class="edit"><i class="fa fa-edit"></i></button>');

      $data[] = $row;
    }
    //echo "<pre>"; print_r($this->common_model->count_all_studentMyApplicationListNew($agentId)); exit;
    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->common_model->count_all_studentMyApplicationListNew($agentId),
      "recordsFiltered" => $this->common_model->count_filtered_studentMyApplicationListNew($agentId),
      "data" => $data,
    );
    //print_r($data); exit;
    //output to json format
    echo json_encode($output);
  }

  function studentMyApplicationListOld()
  {

    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $agentId = $userdata['id'];

    $studentSubAppListObj = $this->common_model->studentSubmitApplicationListOld($agentId);
    $this->outputData['students'] = $studentSubAppListObj;
    $this->outputData['view'] = 'old';

    $this->render_page('templates/student_registration/studentMyApplicationListOld', $this->outputData);
  }

  function ajax_manage_page_studentMyApplicationListOld()
  {
    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $agentId = $userdata['id'];
    $list = $this->common_model->get_datatables_studentMyApplicationListOld($agentId);
    //echo "<pre>"; print_r($list); exit;
    $data = array();
    $no = $_POST['start'];

    foreach ($list as $person) {
      $agency_name = json_decode($person->agency_name, true);

      $no++;
      $row = array();
      $row[] = $person->application_id;
      $row[] = $person->student_name;
      $row[] = $person->status;
      $row[] = $agency_name['agency_name'];
      $row[] = $person->facilitator_name;
      $row[] = $person->facilitator_email;
      $row[] = $person->university_name;

      //add html for action
      /*$row[] = "<a href='/admin/student/application/newList/'.$person->user_id.'/'.$person->application_id><button class='btn btn-primary btn-circle'   style='float:left'  value="">Assign to me</button></a>";*/

      $row[] = anchor(base_url('/student/profile/' . $person->user_id . '/' . $person->application_id), '<button title="Edit" class="edit"><i class="fa fa-edit"></i></button>');

      $data[] = $row;
    }
    //echo "<pre>"; print_r($this->common_model->count_all_studentMyApplicationListNew($agentId)); exit;
    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->common_model->count_all_studentMyApplicationListOld($agentId),
      "recordsFiltered" => $this->common_model->count_filtered_studentMyApplicationListOld($agentId),
      "data" => $data,
    );
    //print_r($data); exit;
    //output to json format
    echo json_encode($output);
  }

  function studentSubmitApplicationList()
  {
    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $studentSubAppListObj = $this->common_model->studentSubmitApplicationList();
    $this->outputData['students'] = $studentSubAppListObj;
    $this->outputData['view'] = 'list';

    $this->render_page('templates/student_registration/studentSubmitApplicationList', $this->outputData);
  }

  function ajax_manage_page_studentSubmitApplicationList()
  {
    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }
    $list = $this->common_model->get_datatables_studentSubmitApplicationList();

    $data = array();
    $no = $_POST['start'];

    foreach ($list as $person) {
      $agency_name = json_decode($person->agency_name, true);

      $no++;
      $row = array();
      $row[] = $person->application_id;
      $row[] = $person->student_name;
      $row[] = $person->status;
      $row[] = $agency_name['agency_name'];
      $row[] = $person->facilitator_name;
      $row[] = $person->facilitator_email;
      $row[] = $person->university_name;

      //add html for action
      /*$row[] = "<a href='/admin/student/application/newList/'.$person->user_id.'/'.$person->application_id><button class='btn btn-primary btn-circle'   style='float:left'  value="">Assign to me</button></a>";*/

      $row[] = anchor(base_url('/student/profile/' . $person->user_id . '/' . $person->application_id), '<button class="btn btn-primary btn-circle"   style="float:left"  value="">Assign to me</button>');

      $data[] = $row;
    }
    //echo "<pre>"; print_r($data); exit;
    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->common_model->count_all_studentSubmitApplicationList(),
      "recordsFiltered" => $this->common_model->count_filtered_studentSubmitApplicationList(),
      "data" => $data,
    );
    //echo "<pre>"; print_r($output); exit;
    //output to json format
    echo json_encode($output);
  }

  function studentNewApplicationList()
  {
    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $studentNewAppListObj = $this->common_model->studentNewApplicationList();
    $this->outputData['students'] = $studentNewAppListObj;
    $this->outputData['view'] = 'list';

    $this->render_page('templates/student_registration/studentNewApplicationList', $this->outputData);
  }

  function ajax_manage_page_studentNewApplicationList()
  {
    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }
    $list = $this->common_model->get_datatables_studentNewApplicationList();

    $data = array();
    $no = $_POST['start'];

    foreach ($list as $person) {
      $agency_name = json_decode($person->agency_name, true);

      $no++;
      $row = array();
      $row[] = $person->application_id;
      $row[] = $person->student_name;
      $row[] = $person->status;
      $row[] = $agency_name['agency_name'];
      $row[] = $person->facilitator_name;
      $row[] = $person->facilitator_email;
      $row[] = $person->university_name;

      //add html for action
      /*$row[] = "<a href='/admin/student/application/newList/'.$person->user_id.'/'.$person->application_id><button class='btn btn-primary btn-circle'   style='float:left'  value="">Assign to me</button></a>";*/

      $row[] = anchor(base_url('/student/application/newList/' . $person->user_id . '/' . $person->application_id), '<button class="btn btn-primary btn-circle"   style="float:left"  value="">Assign to me</button>');

      $data[] = $row;
    }
    //echo "<pre>"; print_r($data); exit;
    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->common_model->count_all_studentNewApplicationList(),
      "recordsFiltered" => $this->common_model->count_filtered_studentNewApplicationList(),
      "data" => $data,
    );
    //echo "<pre>"; print_r($output); exit;
    //output to json format
    echo json_encode($output);
  }

  function studentApplicationForm()
  {

    $usrdata = $this->session->userdata('user');

    if (empty($usrdata)) {
      redirect('common/login');
      exit();
    }

    $this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status' => 1));

    $this->render_page('templates/student_registration/student_application_form', $this->outputData);
  }

  function goToUniversityApplyPage($university_id, $country_id)
  {

    $this->load->helper('cookie_helper');
    $usrdata = $this->session->userdata('user');

    if (empty($usrdata)) {
      redirect('common/login');
      exit();
    }

    $userId = $usrdata['id'];

    $postVariables = $this->input->post();

    $applicationCountry       = $country_id;
    $applicationUniversity[]  = $university_id;

    $transactionId = rand(9, 999999999);

    $insertData = [
      "country_id" => $applicationCountry,
      "university_id" => json_encode($applicationUniversity),
      "transaction_id" => $transactionId,
      "created_by" => $userId
    ];

    //var_dump(["university_id" => $applicationUniversity[0]]);
    //die();

    $createApllicationObj = $this->common_model->post($insertData, "student_application_process_temp");
    $this->outputData['transaction_id'] = $transactionId;


    $applicationProcessInfoObj = $this->common_model->get(["entity_id" => $applicationUniversity[0], "entity_type" => "UNIVERSITY"], "university_application_process_info");
    $this->outputData['university_Application_info'] = [];
    //echo "<pre>"; print_r($this->db->last_query()); exit;
    if ($applicationProcessInfoObj['data']) {
      $applicationProcessInfoObj['data'][0]['application_fill_option'] = json_decode($applicationProcessInfoObj['data'][0]['application_fill_option'], true);
      $this->outputData['university_Application_info'] = $applicationProcessInfoObj['data'][0];
    }

    $studentListArray = [];
    $studentList = $this->counsellor_model->studentList($userId);

    if ($studentList) {
      $studentListArray = $studentList;
    }
    //echo "<pre>"; print_r($applicationProcessInfoObj); exit;
    $this->outputData['student_list'] = $studentListArray;

    $universityInfoObj = $this->common_model->get(["id" => $applicationUniversity[0]], "universities");
    $this->outputData['university_info'] = $universityInfoObj['data'][0];

    $this->render_page('templates/student_registration/create_student_form', $this->outputData);
  }

  function submitStudentApplication()
  {

    $this->load->helper('cookie_helper');
    $usrdata = $this->session->userdata('user');

    if (empty($usrdata)) {
      redirect('common/login');
      exit();
    }

    $userId = $usrdata['id'];

    $postVariables = $this->input->post();

    $applicationCountry       = $this->input->post('application_country');
    $applicationUniversity[]       = $this->input->post('application_university');

    $transactionId = rand(9, 999999999);

    $insertData = [
      "country_id" => $applicationCountry,
      "university_id" => json_encode($applicationUniversity),
      "transaction_id" => $transactionId,
      "created_by" => $userId
    ];

    //var_dump(["university_id" => $applicationUniversity[0]]);
    //die();

    $createApllicationObj = $this->common_model->post($insertData, "student_application_process_temp");
    $this->outputData['transaction_id'] = $transactionId;

    $applicationProcessInfoObj = $this->common_model->get(["entity_id" => $applicationUniversity[0], "entity_type" => "UNIVERSITY"], "university_application_process_info");
    $this->outputData['university_Application_info'] = [];

    if ($applicationProcessInfoObj['data']) {
      $applicationProcessInfoObj['data'][0]['application_fill_option'] = json_decode($applicationProcessInfoObj['data'][0]['application_fill_option'], true);
      $this->outputData['university_Application_info'] = $applicationProcessInfoObj['data'][0];
    }

    $studentListArray = [];
    $studentList = $this->counsellor_model->studentList($userId);

    if ($studentList) {
      $studentListArray = $studentList;
    }
    // echo "<pre>"; print_r($applicationProcessInfoObj); exit;
    $this->outputData['student_list'] = $studentListArray;

    $universityInfoObj = $this->common_model->get(["id" => $applicationUniversity[0]], "universities");
    $this->outputData['university_info'] = $universityInfoObj['data'][0];

    $this->render_page('templates/student_registration/create_student_form', $this->outputData);
  }

  function updateStudentApplication()
  {

    $this->load->helper('cookie_helper');
    $usrdata = $this->session->userdata('user');

    if (empty($usrdata)) {
      redirect('common/login');
      exit();
    }

    $userId = $usrdata['id'];

    $postVariables = $this->input->post();

    $transactionId       = $this->input->post('transaction_id');
    $applicationFillOption      = $this->input->post('application_fill_option');

    $updateData = [
      "transaction_id" => $transactionId,
      "application_fill_option" => $applicationFillOption,
      "updated_by" => $userId
    ];

    $updateApplicationObj = $this->common_model->put(["transaction_id" => $transactionId, "created_by" => $userId], $updateData, "student_application_process_temp");

    $response['data'] = $updateApplicationObj;
    $response['status'] = "SUCCESS";

    header('Content-Type: application/json');
    echo (json_encode($response));
  }


  function studentRegistration()
  {

    $usrdata = $this->session->userdata('user');

    if (empty($usrdata)) {
      redirect('common/login');
      exit();
    }

    $counsellorId = $usrdata['id'] ? $usrdata['id'] : 0;

    $userEmail = $this->input->post('user_email');
    $userMobile = $this->input->post('user_mobile');

    $checkUserExist = $this->webinar_model->checkUserLogin($userEmail, $userMobile);

    if ($checkUserExist) {
      //echo "With Email-ID Or Mobile Account Already Exists";
      $response["status"] = "With Email-ID Or Mobile Account Already Exists";
      header('Content-Type: application/json');
      echo (json_encode($response));
      exit();
    }

    $data = array(

      'name'       =>  $this->input->post('user_fname'),
      'last_name' =>  $this->input->post('user_lname'),
      'username' => $this->input->post('user_mobile'),
      'email'     =>  $this->input->post('user_email'),
      'password'     =>  md5("hellouni2022"),
      'image'      =>  '',
      'type'       =>  '5',
      'createdate'   =>  date('Y-m-d H:i:s'),
      'activation'   =>  md5(time()),
      'fid'      =>  '',
      'status'     =>  '1',
      'registration_type'     =>  'COUNSELLOR',
      'phone'        => $this->input->post('user_mobile')

    );

    $user_id = $this->user_model->insertUserMaster($data);

    $student_data = array(

      'user_id'         =>  $user_id,
      'status'         =>  "POTENTIAL",
      'phone'           =>  $this->input->post('user_mobile'),
      'facilitator_id'   =>  1,
      'originator_id'   =>  $counsellorId

    );

    $studentId = $this->user_model->insertStudentDetails($student_data);

    $transactionId = $this->input->post('transaction_id');

    $getApplicationInfoTemp = $this->common_model->get(["transaction_id" => $transactionId], "student_application_process_temp");

    if ($getApplicationInfoTemp['data']) {

      $universityIdArray = json_decode($getApplicationInfoTemp['data'][0]['university_id'], true);

      foreach ($universityIdArray as $universityId) {

        $postData = [
          "user_id" => $user_id,
          "university_id" => $universityId,
          "application_fill_option" => $getApplicationInfoTemp['data'][0]['application_fill_option']
        ];

        $createApplicationObj = $this->common_model->post($postData, "users_applied_universities");
      }
    }

    $response["status"] = "SUCCESS";
    $response["student_id"] = $user_id;
    header('Content-Type: application/json');
    echo (json_encode($response));
  }

  function addStudentApplication()
  {

    $usrdata = $this->session->userdata('user');

    if (empty($usrdata)) {
      redirect('common/login');
      exit();
    }

    $counsellorId = $usrdata['id'] ? $usrdata['id'] : 0;

    $transactionId = $this->input->post('transaction_id');
    $userId = $this->input->post('student_id');

    $getApplicationInfoTemp = $this->common_model->get(["transaction_id" => $transactionId], "student_application_process_temp");

    if ($getApplicationInfoTemp['data']) {

      $universityIdArray = json_decode($getApplicationInfoTemp['data'][0]['university_id'], true);

      foreach ($universityIdArray as $universityId) {

        $postData = [
          "user_id" => $userId,
          "university_id" => $universityId,
          "application_fill_option" => $getApplicationInfoTemp['data'][0]['application_fill_option']
        ];

        $createApplicationObj = $this->common_model->post($postData, "users_applied_universities");
      }
    }

    $response["status"] = "SUCCESS";
    $response["student_id"] = $userId;
    header('Content-Type: application/json');
    echo (json_encode($response));
  }




  public function studentApplicationSubmit()
  {

    $usrdata = $this->session->userdata('user');

    if (empty($usrdata)) {
      redirect('common/login');
      exit();
    }

    $counsellorId = $usrdata['id'] ? $usrdata['id'] : 0;
    $couselllorEmail = $usrdata['email'] ? $usrdata['email'] : 0;
    $appId = $this->input->post('app_id');
    $userId = $this->input->post('user_id');
    $student_name = $this->input->post('stdName');

    $myAppliedUniversities = $this->common_model->getAppliedUniversitiesByUser($userId, $appId);

    $University_name = $myAppliedUniversities[0]['name'];
    $Course = $myAppliedUniversities[0]['university_application_course'];
    $Intake = $myAppliedUniversities[0]['university_course_intake_month'] . ' ' . $myAppliedUniversities[0]['university_course_intake_year'];
    $facilitatorEmailId = $myAppliedUniversities[0]['facilitator_email'];
    $facilitatorName = $myAppliedUniversities[0]['facilitator_name'] . ' ' . $myAppliedUniversities[0]['facilitator_last_name'];

    $updateApplication = $this->common_model->put(["id" => $appId], ["application_completed" => 1, "status" => "APPLICATION_UNDER_REVIEW"], "users_applied_universities");

    $description = "
      Students Application Status AppDated
      <br>
      Application Id : - $appId
      <br>
      Application Status Moved To Application Under Review
      <br>
      Thanks
      ";

    $notificationData = [
      "user_id" => $userId,
      "title" => "Status Update",
      "description" => $description,
      "created_by" => $counsellorId
    ];

    $updateApplication = $this->common_model->post($notificationData, "notification_master");

    if ($facilitatorEmailId != '') {
      $ccMailList = "sudhir.kanojia@imperialplatforms.com,jay.r@imperialplatforms.com,$couselllorEmail";
      //$ccMailList = "priyankadthool@gmail.com";
      $mailAttachments = '';
      //  $facilitatorName = '';
      //  $facilitatorEmailId = "priyankadthool@gmail.com";

      $mailSubject = "Hellouni – Update – $student_name – $University_name – Application Status Updated";

      $mailTemplate = "Dear Team,

                         There is been an update on the portal for below mentioned Applicant
                         Student Name - $student_name
                         University Name - $University_name
                         Course - $Course
                         Intake - $Intake

                         Status - Application Status Moved To Application Under Review

                          <b>Thanks and Regards,
                          Hellouni Co-ordinator
                          </b>

                          <img src='https://www.hellouni.org/img/hello_uni_mail.png'>


                          Kindly Login to the Application portal (portal Link) to check
                         <b>**Note</b>: This is an automated email, please do not reply to this email**
                         ";



      $sendMail = sMail($facilitatorEmailId, $facilitatorName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);
      //print_r($sendMail); exit;s
    }

    $response["status"] = "SUCCESS";
    $response["app_id"] = $appId;
    header('Content-Type: application/json');
    echo (json_encode($response));
  }

  public function universityProcessInfo()
  {

    $usrdata = $this->session->userdata('user');

    if (empty($usrdata)) {
      redirect('common/login');
      exit();
    }

    $universityId = $this->input->post('university_id') ? $this->input->post('university_id') : 0;
    $appId = $this->input->post('app_id');

    $applicationProcessInfoObj = $this->common_model->get(["entity_id" => $appId], "university_application_process_info");

    if ($applicationProcessInfoObj['data']) {
      $applicationProcessInfoObj['data'][0]['application_fill_option'] = json_decode($applicationProcessInfoObj['data'][0]['application_fill_option'], true);
      $response["process_info"] = $applicationProcessInfoObj['data'][0];
    }

    $response["status"] = "SUCCESS";
    $response["app_id"] = $appId;

    header('Content-Type: application/json');
    echo (json_encode($response));
  }

  public function universityProcessSubmit()
  {

    $usrdata = $this->session->userdata('user');

    if (empty($usrdata)) {
      redirect('common/login');
      exit();
    }

    $counsellorId = $usrdata['id'] ? $usrdata['id'] : 0;
    $appId = $this->input->post('app_id');
    $appType = $this->input->post('app_type');

    $updateApplication = $this->common_model->put(["id" => $appId], ["application_fill_option" => $appType], "users_applied_universities");

    $response["status"] = "SUCCESS";
    $response["app_id"] = $appId;

    header('Content-Type: application/json');
    echo (json_encode($response));
  }


  public function applicationStatusSubmit()
  {

    $usrdata = $this->session->userdata('user');

    if (empty($usrdata)) {
      redirect('common/login');
      exit();
    }

    $counsellorId = $usrdata['id'] ? $usrdata['id'] : 0;
    $couselllorEmail = $usrdata['email'] ? $usrdata['email'] : 0;
    $appId = $this->input->post('app_id');
    $appStatus = $this->input->post('app_status');
    $appSubStatus = $this->input->post('app_sub_status');
    $userId = $this->input->post('user_id');
    $student_name = $this->input->post('stdName');

    $myAppliedUniversities = $this->common_model->getAppliedUniversitiesByUser($userId, $appId);

    $University_name = $myAppliedUniversities[0]['name'];
    $Course = $myAppliedUniversities[0]['university_application_course'];
    $Intake = $myAppliedUniversities[0]['university_course_intake_month'] . ' ' . $myAppliedUniversities[0]['university_course_intake_year'];
    $facilitatorEmailId = $myAppliedUniversities[0]['facilitator_email'];
    $facilitatorName = $myAppliedUniversities[0]['facilitator_name'] . ' ' . $myAppliedUniversities[0]['facilitator_last_name'];

    $updateApplication = $this->common_model->put(["id" => $appId], ["status" => $appStatus, "sub_status" => $appSubStatus], "users_applied_universities");

    $description = "
      Students Application Status AppDated
      <br>
      Application Id : - $appId
      <br>
      Application Status Moved To $appStatus $appSubStatus
      <br>
      Thanks
      ";

    $notificationData = [
      "user_id" => $userId,
      "title" => "Status Update",
      "description" => $description,
      "created_by" => $counsellorId
    ];

    $updateApplication = $this->common_model->post($notificationData, "notification_master");

    if ($facilitatorEmailId != '') {
      $ccMailList = "sudhir.kanojia@imperialplatforms.com,jay.r@imperialplatforms.com,$couselllorEmail";
      //$ccMailList = "priyankadthool@gmail.com";
      $mailAttachments = '';
      //$facilitatorName = '';
      //$facilitatorEmailId = "priyankadthool@gmail.com";

      $mailSubject = "Hellouni – Update – $student_name – $University_name – Application Status Updated";

      $mailTemplate = "Dear Team,

                         There is been an update on the portal for below mentioned Applicant
                         Student Name - $student_name
                         University Name - $University_name
                         Course - $Course
                         Intake - $Intake

                         Status - Application Status Moved To $appStatus $appSubStatus

                          <b>Thanks and Regards,
                          Hellouni Co-ordinator
                          </b>

                          <img src='https://www.hellouni.org/img/hello_uni_mail.png'>


                          Kindly Login to the Application portal (portal Link) to check
                         <b>**Note</b>: This is an automated email, please do not reply to this email**
                         ";



      $sendMail = sMail($facilitatorEmailId, $facilitatorName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);
    }


    $response["status"] = "SUCCESS";
    $response["app_id"] = $appId;

    header('Content-Type: application/json');
    echo (json_encode($response));
  }

  function counsellorAssignToMe()
  {

    $usrdata = $this->session->userdata('user');

    if (empty($usrdata)) {
      redirect('common/login');
      exit();
    }


    $tablename = "users_applied_universities";
    $assignData = array('facilitator_id' => $usrdata['id']);
    $condup = "user_id = '" . $this->uri->segment('4') . "' AND id='" . $this->uri->segment('5') . "'";
    $updatedata = $this->common_model->updateData($tablename, $assignData, $condup);

    $this->session->set_flashdata('flash_message', "Success: You have successfully assign application!");
    redirect('student/application/newList');
  }
}
