<?php
require_once APPPATH . 'libraries/Mail/sMail.php';
class Index extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
        $this->load->model(array('student_model', 'user/user_model', 'user/university_model', 'location/country_model', 'user/college_model', 'user/course_model'));
    }

    function all()
    {
        $userdata = $this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $universityId = $userdata['university_id'] ? $userdata['university_id'] : '';
        $allStudents = $this->student_model->getAll($universityId);
        if($allStudents)
        {
            foreach($allStudents as $index => $student)
            {
                if($student->desired_destination)
                {
                    $explodeIds = explode(',', $student->desired_destination);
                    $desiredDestinations = $this->user_model->getCountriesByIds($explodeIds);
                    $countryNames = array();
                    foreach($desiredDestinations as $destination)
                    {
                        $countryNames[] = $destination['name'];
                    }
                    $desiredCountries = implode(', ', $countryNames);
                    $student->desired_destination = $desiredCountries;
                    $allStudents[$index] = $student;
                }
            }
        }

        $allCounselors = $this->student_model->getAllCounselors($universityId);
        $defaultCounselor = new stdClass();
        $defaultCounselor->id = 1;
        $defaultCounselor->name = 'Super Admin';
        $allCounselors[] = $defaultCounselor;

        $this->outputData['students'] = $allStudents;
        $this->outputData['counselors'] = $allCounselors;
        $this->outputData['view'] = 'list';
        $this->render_page('templates/student_registration/all', $this->outputData);
    }

    function counselor()
    {
        $userdata = $this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $allStudents = $this->student_model->getAllForCounselors($userdata['id']);
        if($allStudents)
        {
            foreach($allStudents as $index => $student)
            {
                if($student->desired_destination)
                {
                    $explodeIds = explode(',', $student->desired_destination);
                    $desiredDestinations = $this->user_model->getCountriesByIds($explodeIds);
                    $countryNames = array();
                    foreach($desiredDestinations as $destination)
                    {
                        $countryNames[] = $destination['name'];
                    }
                    $desiredCountries = implode(', ', $countryNames);
                    $student->desired_destination = $desiredCountries;
                    $allStudents[$index] = $student;
                }
            }
        }

        $this->outputData['students'] = $allStudents;
        $this->outputData['view'] = 'list';
        $this->render_page('templates/student/counselor', $this->outputData);
    }

    function applied()
    {
        $userdata = $this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $universityId = $this->input->get('uid') ? $this->input->get('uid') : '';
        if($universityId)
        {
            $allUniversities = $this->university_model->getAllUniversities();
            $allStudents = $this->student_model->getAll($universityId);
            if($allStudents)
            {
                foreach($allStudents as $index => $student)
                {
                    if($student->desired_destination)
                    {
                        $explodeIds = explode(',', $student->desired_destination);
                        $desiredDestinations = $this->user_model->getCountriesByIds($explodeIds);
                        $countryNames = array();
                        foreach($desiredDestinations as $destination)
                        {
                            $countryNames[] = $destination['name'];
                        }
                        $desiredCountries = implode(', ', $countryNames);
                        $student->desired_destination = $desiredCountries;
                        $allStudents[$index] = $student;
                    }
                }
            }

            $allCounselors = $this->student_model->getAllCounselors($universityId);
            $defaultCounselor = new stdClass();
            $defaultCounselor->id = 1;
            $defaultCounselor->name = 'Super Admin';
            $allCounselors[] = $defaultCounselor;

            $this->outputData['students'] = $allStudents;
            $this->outputData['counselors'] = $allCounselors;
            $this->outputData['universities'] = $allUniversities;
        }
        $this->outputData['selected_university'] = $universityId;
        $this->outputData['view'] = 'list';
        $this->render_page('templates/student/applied', $this->outputData);
    }

    function update()
    {
        $userdata = $this->session->userdata('user');
        if(empty($userdata))
        {
            echo "Session is expired. Login again";
            exit;
        }

        $postedData = $this->input->post();

        if(!$postedData['student_id'])
        {
            echo "Student Id is missing.";
            exit;
        }

        if(!$postedData['facilitator_id'])
        {
            echo "Counselor Id is missing.";
            exit;
        }

        $studentId = $postedData['student_id'];
        $updatedData['facilitator_id'] = $postedData['facilitator_id'];
        $this->student_model->updateStudentDetail($studentId, $updatedData);
        echo 1;
    }

    function profile()
    {
        $userdata = $this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $studentId = $_GET['id'] ? $_GET['id'] : '';
        if(!$studentId)
        {
            redirect('/student/index/all');
        }
        $userId = 0;
        $condition = array(
            'student_details.id' => $studentId,
            'user_master.status' => 1,
            'user_master.type' => 5
        );
        $studentDetails = $this->student_model->getStudentDetails($condition);

        if($studentDetails)
        {
            $userId = $studentDetails->id;
            $this->outputData['id']= $studentDetails->id;
            $this->outputData['student_id']= $studentDetails->student_id;
    	 	    $this->outputData['name']= $studentDetails->name;
            $this->outputData['email']= $studentDetails->email;
            $this->outputData['typename']= $studentDetails->typename;
            $this->outputData['address']= $studentDetails->address;
            $this->outputData['country']= $studentDetails->country;
            $this->outputData['city']= $studentDetails->city;
            $this->outputData['zip']= $studentDetails->zip;
            $this->outputData['dob']= $studentDetails->dob;
            $this->outputData['phone']= $studentDetails->phone;
            $this->outputData['parent_name']= $studentDetails->parent_name;
            $this->outputData['parent_mob']= $studentDetails->parent_mob;
            $this->outputData['parent_occupation']= $studentDetails->parent_occupation;
            $this->outputData['hear_about_us']= $studentDetails->hear_about_us;
            $this->outputData['gender']= $studentDetails->gender;
            $this->outputData['passport']= $studentDetails->passport;
            $this->outputData['desired_destination']= $studentDetails->desired_destination;
            $this->outputData['fid']= $studentDetails->fid;
            $this->outputData['registration_type']= $studentDetails->registration_type;
            $this->outputData['status']= $studentDetails->status;

            $this->outputData['state']= $studentDetails->state;
            $this->outputData['passport_number']= $studentDetails->passport_number;
            $this->outputData['passport_issue_date']= $studentDetails->passport_issue_date;
            $this->outputData['passport_expiry_date']= $studentDetails->passport_expiry_date;
            $this->outputData['marital_status']= $studentDetails->marital_status;
            $this->outputData['place_of_birth']= $studentDetails->place_of_birth;
            $this->outputData['correspondence_address']= $studentDetails->correspondence_address;

            $this->outputData['company_name']= $studentDetails->company_name;
            $this->outputData['designation']= $studentDetails->designation;
            $this->outputData['work_start_date']= $studentDetails->work_start_date;
            $this->outputData['work_end_date']= $studentDetails->work_end_date;

            $this->outputData['e_name']= $studentDetails->e_name;
            $this->outputData['e_mobile_number']= $studentDetails->e_mobile_number;
            $this->outputData['e_email']= $studentDetails->e_email;
            $this->outputData['e_relation']= $studentDetails->e_relation;

            $this->outputData['destination']= explode(',', $studentDetails->desired_destination);

            $cityCondition = array(
                'city_master.id' => $studentDetails->city,
                'city_master.status' => 1
            );
            $studentState = $this->student_model->getParticularCity($cityCondition);

            $this->outputData['selected_state'] = '';
            $this->outputData['selected_country'] = '';
            if($studentState)
            {
                $this->outputData['selected_state'] = $studentState->state_id;
                $this->outputData['selected_country'] = $studentState->country_id;
            }

            $relatedStateCondition = array(
                'state_master.country_id' => $this->outputData['selected_country'],
                'state_master.status' => 1
            );
    		$this->outputData['related_state_list'] = $this->student_model->getLocationStates($relatedStateCondition);

            $relatedCityCondition = array(
                'city_master.state_id' => $this->outputData['selected_state'],
                'city_master.status' => 1
            );
    		$this->outputData['related_city_list'] = $this->student_model->getCities($relatedCityCondition);

            //desired courses
            $studentCondition = array(
                'student_details.id' => $studentId
            );
            //$desiredCourses = $this->student_model->checkDesiredCourse($studentCondition);
            //$this->outputData['desiredcourse_details'] = $desiredCourses ? $desiredCourses : '';


            $desireInfoObj = $this->student_model->checkDesiredCourse($studentCondition);
  			    $this->outputData['desiredcourse_details'] = $desireInfoObj;
            $this->outputData['desired_destination'] = implode(",", json_decode($desireInfoObj->desired_country,true));

            $this->outputData['mainstreams'] = $this->course_model->getAllCollegeCategories();
        		$this->outputData['degrees'] = $this->course_model->getDegree(array('degree_master.status'=>1));
            $this->outputData['submainstreams'] = $this->course_model->getCourseCategoriesByCollegeCategory($desireInfoObj->desired_course_name);

            $this->outputData['intake_month'] = json_decode (json_encode ( [
              [ 'id' => 'January', 'name' => 'January'],
              [ 'id' => 'February', 'name' => 'February'],
              [ 'id' => 'March', 'name' => 'March'],
              [ 'id' => 'April', 'name' => 'April'],
              [ 'id' => 'May', 'name' => 'May'],
              [ 'id' => 'June', 'name' => 'June'],
              [ 'id' => 'July', 'name' => 'July'],
              [ 'id' => 'August', 'name' => 'August'],
              [ 'id' => 'September', 'name' => 'September'],
              [ 'id' => 'October', 'name' => 'October'],
              [ 'id' => 'November', 'name' => 'November'],
              [ 'id' => 'December', 'name' => 'December']
            ]), FALSE);

            $this->outputData['intake_year'] = json_decode (json_encode ( [
              [ 'id' => '2020', 'name' => '2020'],
              [ 'id' => '2021', 'name' => '2021'],
              [ 'id' => '2022', 'name' => '2022'],
              [ 'id' => '2023', 'name' => '2023'],
              [ 'id' => '2024', 'name' => '2024']
            ]), FALSE);


            //Secondary Qualification
            $secondaryQualifactions = $this->student_model->checkSecondaryQualification($studentCondition);
    		$this->outputData['secondaryqualification_details'] = $secondaryQualifactions ? $secondaryQualifactions : '';

            //Diploma
            $diplomaQualifactions = $this->student_model->checkDiplomaQualification($studentCondition);
    		$this->outputData['diplomaqualification_details'] = $diplomaQualifactions ? $diplomaQualifactions : '';

            // 12th
            $higherSecondaryQualifactions = $this->student_model->checkHigherSecondaryQualification($studentCondition);
    		$this->outputData['highersecondaryqualification_details'] = $higherSecondaryQualifactions ? $higherSecondaryQualifactions : '';

            //Bachelor
            $bachelorQualifactions = $this->student_model->checkBachelorQualification($studentCondition);
    		$this->outputData['bachelorqualification_details'] = $bachelorQualifactions ? $bachelorQualifactions : '';

            //Masters
            $masterQualifactions = $this->student_model->checkMasterQualification($studentCondition);
    		    $this->outputData['mastersqualification_details'] = $masterQualifactions ? $masterQualifactions : '';
            //var_dump($studentCondition);
            //var_dump($masterQualifactions);
            //var_dump($this->outputData['mastersqualification_details']);die();

            //Qualified Exams
            $qualifiedExams = $this->student_model->checkQualifiedExam($studentCondition);
            $this->outputData['qualified_exams'] = '';
            if($qualifiedExams)
            {
                $this->outputData['qualified_exams'] = $qualifiedExams;
                $this->outputData['qualified_exams']->competitive_exam_stage = json_decode($this->outputData['qualified_exams']->competitive_exam_stage, true);
                $this->outputData['qualified_exams']->english_exam_stage = json_decode($this->outputData['qualified_exams']->english_exam_stage, true);
    		}

            $countryCondition = array(
                'country_master.status' => 1
            );
            $this->outputData['countries'] = $this->student_model->getCountries($countryCondition);

            $locationCondition = array(
                'location_country_master.status' => 1
            );
    		$this->outputData['locationcountries'] = $this->student_model->getLocationCountries($locationCondition);

            $intakeCondition = array(
                'intake_master.status' => '1'
            );
    		$this->outputData['intakes'] = $this->student_model->getIntakes($intakeCondition);

            $this->outputData['selected_application'] = $this->input->get('aid');
            $this->outputData['application_detail'] = '';
            $this->outputData['visa_detail'] = '';
            $appliedUniversities = $this->student_model->getAppliedUniversities($userId);
            if($appliedUniversities && !$this->outputData['selected_application'])
            {
                $this->outputData['selected_application'] = $appliedUniversities[0]->id;

                $applicationNotes = $this->student_model->notesList($userId);

                if($applicationNotes){
                  foreach ($appliedUniversities as $keyapp => $valueapp) {
                    // code...
                    foreach ($applicationNotes as $keynote => $valuenote) {
                      if( $valueapp->id == $valuenote['application_id']){

                        $appliedUniversities[$keyapp]->noteslist[] = $applicationNotes[$keynote];

                      }
                    }
                  }
                }

                $applicationDocList = $this->student_model->appDocList($userId);

                $this->outputData['myApplicationDocList'] = $applicationDocList;

            }
            if($appliedUniversities)
            {
                foreach($appliedUniversities as $appliedUniversity)
                {
                    if($this->outputData['selected_application'] == $appliedUniversity->id)
                    {
                        $appliedUniversity->sop = $appliedUniversity->sop ? json_decode($appliedUniversity->sop) : $appliedUniversity->sop;
                        $appliedUniversity->lor = $appliedUniversity->lor ? json_decode($appliedUniversity->lor) : $appliedUniversity->lor;
                        $appliedUniversity->transcripts = $appliedUniversity->transcripts ? json_decode($appliedUniversity->transcripts) : $appliedUniversity->transcripts;
                        $appliedUniversity->application_fee = $appliedUniversity->application_fee ? json_decode($appliedUniversity->application_fee) : $appliedUniversity->application_fee;
                        $this->outputData['sop_no_of_words'] = $appliedUniversity->sop && isset($appliedUniversity->sop->sop_needed) && $appliedUniversity->sop->sop_needed? 'block' : 'none';
                        $this->outputData['lor_required'] = $appliedUniversity->lor && isset($appliedUniversity->lor->lor_needed) && $appliedUniversity->lor->lor_needed ? 'block' : 'none';
                        $this->outputData['lor_type'] = $appliedUniversity->lor && isset($appliedUniversity->lor->lor_needed) && $appliedUniversity->lor->lor_needed ? 'block' : 'none';

                        $this->outputData['application_detail'] = $appliedUniversity;

                        $visa_detail = $this->student_model->getVisaByApplicationId($appliedUniversity->id);
                        if($visa_detail)
                        {
                            $visa_detail[0]['mock_dates'] = $visa_detail[0]['mock_dates'] ? json_decode($visa_detail[0]['mock_dates'], true) : $visa_detail[0]['mock_dates'];

                            $this->outputData['visa_detail'] = $visa_detail[0];
                        }
                    }
                }
            }
            $this->outputData['applied_universities'] = $appliedUniversities;

            $studentSessions = $this->student_model->getMySessions($studentDetails->id);
            $this->outputData['sessions'] = $studentSessions;

            $this->outputData['documentTypes'] = $this->student_model->getDocumentsList();
            $this->outputData['uploadedDocuments'] = $this->student_model->getDocumentsByStudent($studentDetails->id);

            $activeTab = $this->input->get('tab') ? $this->input->get('tab') : 'personal_details';
            $this->outputData['active_tab'] = $activeTab;
            $this->outputData['view'] = 'list';
            $this->outputData['student_id'] = $studentId;

            $this->outputData['sessions_list'] = $this->student_model->getUserSessions($userId);
            $this->outputData['webinar_list'] = $this->student_model->webinarListById($userId);
            $this->outputData['favourites'] = $this->student_model->favouritesListById($userId);
            $this->outputData['likes'] = $this->student_model->likesListById($userId);
            $this->outputData['views'] = $this->student_model->viewsListById($userId);

            //document list with uploaded doc

            $documentDetail = $this->student_model->getNewDocumentList(10000);
        		$documentUseDocDetail = $this->student_model->getNewDocumentUserDocList($userId, 10000);

        		$finalDocList = array();
        		$finalUserDocList = array();
        		$docFinalList = array();
        		$docUrlCount = [];

        		$uniqueListDoc = array();
        		$uniqueArray = [];

        		if($documentDetail['data'])
        		{

        			foreach($documentDetail['data'] as $documents) {

        				$finalDocList[$documents['product_document_id']][] = ["document_master_id" => $documents['document_master_id'],
"display_name" => $documents['display_name'],];

        			}

        			foreach($documentUseDocDetail['data'] as $userDocuments) {

        				$finalUserDocList[$userDocuments['product_document_id']][] = ["document_master_id" => $userDocuments['document_master_id'],
"document_id" => $userDocuments['document_id'],
"display_name" => $userDocuments['display_name'],
"document_display_name" => $userDocuments['document_display_name'],
"document_url" => $userDocuments['document_url'] ];

        			}

        			foreach($documentDetail['data'] as $document1){

        				if(!in_array($document1['product_document_id'], $uniqueArray))
        				{

        					$document1['doc_list'] = $finalDocList[$document1['product_document_id']];
        					$document1['user_doc_list'] = isset($finalUserDocList[$document1['product_document_id']]) ? $finalUserDocList[$document1['product_document_id']] : NULL;
        					$document1['document_count'] = isset($finalUserDocList[$document1['product_document_id']]) ? count($finalUserDocList[$document1['product_document_id']]) : NULL;
        					$uniqueListDoc[] = $document1;
        					$uniqueArray[] = $document1['product_document_id'];

        				}

        			}

        			foreach($uniqueListDoc as $document12)
        			{
        				$docFinalList[$document12['tab_name']][] = $document12;
        			}

        		}

            $this->outputData['new_doc_list'] = $docFinalList;

            $this->render_page('/templates/student/profile', $this->outputData);
        }
    }

    function save()
    {

        $userdata = $this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $studentId = $this->input->post('student_id') ? $this->input->post('student_id') : '';
        if(!$studentId)
        {
            redirect('/student/index/all');
        }

        $userId = $this->input->post('user_id') ? $this->input->post('user_id') : '';
        if(!$userId)
        {
            redirect('/student/index/all');
        }

        /*echo '<pre>';
        print_r($this->input->post());
        exit;*/

        $formType = $this->input->post('form_type');
        switch($formType){
            case 'personal_details':

            $userEmail = $this->input->post('email') ? $this->input->post('email') : 'NA';
            $userPhone = $this->input->post('phone') ? $this->input->post('phone') : 'NA';

            $userOldEmail = $this->input->post('old_email') ? $this->input->post('old_email') : 'NA';
            $userOldPhone = $this->input->post('old_phone') ? $this->input->post('old_phone') : 'NA';

            if($userEmail != $userOldEmail){
              $cType = "email";
              $getInfo = $this->user_model->getUserInfo($cType, $userEmail);
            }

            if($userPhone != $userOldPhone){
              $cType = "phone";
              $getInfo = $this->user_model->getUserInfo("", $userPhone);
            }

                if($getInfo){
                  echo '<script type="text/javascript">
                  alert("$cType Already Exists");
                  window.location.href = "/admin/student/index/profile?id=' . $studentId . '";
                  </script>';

                } else {

                  $details = array(
                      'name' 			=>  $this->input->post('name'),
                      'email' 			=>  $this->input->post('email'),
                      'phone' 			=>  $this->input->post('phone')
                  );

                  $this->user_model->updateUserInfo($details, $userId)	;

                  $data = array(
                    'address'				=>	$this->input->post('address') ? $this->input->post('address') : NULL,
                    'correspondence_address'				=>	$this->input->post('correspondence_address') ? $this->input->post('correspondence_address') : NULL,
                    'city'					=>	$this->input->post('city') ? $this->input->post('city') : NULL,
                    'zip'					=>	$this->input->post('zip') ? $this->input->post('zip') : NULL,
                    'dob'					=>	$this->input->post('dob') ? $this->input->post('dob') : NULL,
                    'phone'					=>	$this->input->post('phone') ? $this->input->post('phone') : NULL,
                    'gender'			=>	$this->input->post('gender') ? $this->input->post('gender') : NULL,
                    'state'			=>	$this->input->post('state') ? $this->input->post('state') : NULL,
                    'country'		=>	$this->input->post('country') ? $this->input->post('country') : NULL,
                    'hear_about_us'			=>	$this->input->post('hear_info') ? $this->input->post('hear_info') : NULL,
                    'passport'				=>	$this->input->post('passport_availalibility') ? $this->input->post('passport_availalibility') : 0,
                    'passport_number'				=>	$this->input->post('passport_number') ? $this->input->post('passport_number') : NULL,
                    'passport_issue_date'				=>	$this->input->post('passport_issue_date') ? $this->input->post('passport_issue_date') : NULL,
                    'passport_expiry_date'				=>	$this->input->post('passport_expiry_date') ? $this->input->post('passport_expiry_date') : NULL,
                    'marital_status'				=>	$this->input->post('marital_status') ? $this->input->post('marital_status') : NULL,
                    'place_of_birth'				=>	$this->input->post('place_of_birth') ? $this->input->post('place_of_birth') : NULL
                  );
                  $this->student_model->updateStudentDetails($data, $studentId);
                }
                break;
            case 'qualification_details':
                if($this->input->post('secondary_institution_name')){
                    $secondary_qualification = array(
                        'student_id'		=>	$studentId,
                        'institution_name'	=>	$this->input->post('secondary_institution_name'),
                        'board'				=>	$this->input->post('board'),
                        'year_started'		=>	$this->input->post('secondary_year_starrted'),
                        'year_finished'		=>	$this->input->post('secondary_year_finished'),
                        'grade'				=>	$this->input->post('secondary_grade'),
                        'total'				=>	$this->input->post('secondary_total')
                    );
                    if($this->student_model->checkSecondaryQualification(array('student_id' => $studentId))){
                        $this->student_model->updateSecondaryQualification($secondary_qualification, $studentId);
                    }
                    else{
                        $this->student_model->insertSecondaryQualification($secondary_qualification);
                    }
                }
                if($this->input->post('diploma_12')==1){
                    if($this->input->post('diploma_course_name')){
                        $diploma_qualification = array(
                            'student_id'		=>	$studentId,
                            'course'			=>	$this->input->post('diploma_course_name'),
                            'college'			=>	$this->input->post('diploma_institution_name'),
                            'year_started'		=>	$this->input->post('diploma_year_starrted'),
                            'year_finished'		=>	$this->input->post('diploma_year_finished'),
                            'aggregate'			=>	$this->input->post('diploma_grade'),
                            'total'				=>	$this->input->post('diploma_total')
                        );
                        if($this->student_model->checkDiplomaQualification(array('student_id' => $studentId))){
                            $this->student_model->updateDiplomaQualification($diploma_qualification, $studentId);
                        }
                        else{
                            $this->student_model->insertDiplomaQualification($diploma_qualification);
                        }
                    }
                }
                else if($this->input->post('diploma_12')==2){
                    if($this->input->post('hs_institution_name')){
                        $hs_qualification = array(
                            'student_id'		=>	$studentId,
                            'institution_name'	=>	$this->input->post('hs_institution_name'),
                            'board'				=>	$this->input->post('hs_board'),
                            'year_started'		=>	$this->input->post('hs_year_starrted'),
                            'year_finished'		=>	$this->input->post('hs_year_finished'),
                            'grade'				=>	$this->input->post('hs_grade'),
                            'total'				=>	$this->input->post('hs_total')
                        );
                        if($this->student_model->checkHigherSecondaryQualification(array('student_id' => $studentId))){
                            $this->student_model->updateHigherSecondaryQualification($hs_qualification, $studentId);
                        }
                        else{
                            $this->student_model->insertHigherSecondaryQualification($hs_qualification);
                        }
                    }
                }
                if($this->input->post('bachelor_course')){
                    $bachelor_qualification = array(
                        'student_id'		=>	$studentId,
                        'course'			=>	$this->input->post('bachelor_course'),
                        'university'		=>	$this->input->post('bachelor_university'),
                        'college'			=>	$this->input->post('bachelor_college'),
                        'major'				=>	$this->input->post('bachelor_major'),
                        'year_started'		=>	$this->input->post('bachelor_year_starrted'),
                        'year_finished'		=>	$this->input->post('bachelor_year_finished'),
                        'grade'				=>	$this->input->post('bachelor_grade'),
                        'total'				=>	$this->input->post('bachelor_total')
                    );
                    if($this->student_model->checkBachelorQualification(array('student_id' => $studentId))){
                        $this->student_model->updateBachelorQualification($bachelor_qualification, $studentId);
                    }
                    else{
                        $this->student_model->insertBachelorQualification($bachelor_qualification);
                    }
                }
                if($this->input->post('masters')==1){
                    if($this->input->post('master_course')){
                        $master_qualification = array(
                            'student_id'		=>	$studentId,
                            'course'			=>	$this->input->post('master_course'),
                            'university'		=>	$this->input->post('master_university'),
                            'college'			=>	$this->input->post('master_college'),
                            'major'				=>	$this->input->post('master_major'),
                            'year_started'		=>	$this->input->post('master_year_started'),
                            'year_finished'		=>	$this->input->post('master_year_finished'),
                            'grade'				=>	$this->input->post('master_grade'),
                            'total'				=>	$this->input->post('master_total')
                        );
                        if($this->student_model->checkMasterQualification(array('student_id' => $studentId))){
                            $this->student_model->updateMasterQualification($master_qualification, $studentId);
                        }
                        else{
                            $this->student_model->insertMasterQualification($master_qualification);
                        }
                    }
                }
                if($this->input->post('competitive_exam')){
                    $competitive_exam = array(
                        'student_id' => $studentId,
                        'competitive_exam_type' => $this->input->post('competitive_exam'),
                        'competitive_exam_stage' => json_encode(array(
                            'name' => '',
                            'value' => '',
                            'exam_score_quant_gre' => '',
                            'exam_score_verbal_gre' => '',
                            'exam_score_ir' => '',
                            'exam_score_awa_gre' => '',
                            'exam_score_total_gre' => '',
                            'exam_score_quant_gmat' => '',
                            'exam_score_verbal_gmat' => '',
                            'exam_score_awa_gmat' => '',
                            'exam_score_total_gmat' => '',
                            'exam_score_cirital_reading' => '',
                            'exam_score_writing' => '',
                            'exam_score_math' => '',
                            'exam_score_total_sat' => ''
                        ))
                    );
                    if($this->input->post('competitive_exam') != 'NO' && $this->input->post('competitive_exam') != 'NOT_REQUIRED' && $this->input->post('exam_stage')){
                        $competitive_exam['competitive_exam_stage'] = json_encode(array(
                            'name' => $this->input->post('exam_stage')
                        ));

                        if($this->input->post('exam_stage') == 'REGISTERED' && $this->input->post('exam_date')){
                            $competitive_exam['competitive_exam_stage'] = json_encode(array(
                                'name' => $this->input->post('exam_stage'),
                                'value' => $this->input->post('exam_date')
                            ));
                        }
                        elseif($this->input->post('exam_stage') == 'RESULT_WAIT' && $this->input->post('result_date')){
                            $competitive_exam['competitive_exam_stage'] = json_encode(array(
                                'name' => $this->input->post('exam_stage'),
                                'value' => $this->input->post('result_date')
                            ));
                        }
                        elseif($this->input->post('exam_stage') == 'RESULT_OUT'){
                            if($this->input->post('competitive_exam') == 'GRE'){
                                $competitive_exam['competitive_exam_stage'] = json_encode(array(
                                'name' => $this->input->post('exam_stage'),
                                'exam_score_quant_gre' => $this->input->post('exam_score_quant_gre'),
                                'exam_score_verbal_gre' => $this->input->post('exam_score_verbal_gre'),
                                'exam_score_awa_gre' => $this->input->post('exam_score_awa_gre'),
                                'exam_score_total_gre' => $this->input->post('exam_score_total_gre')
                                ));
                            }
                            elseif($this->input->post('competitive_exam') == 'GMAT'){
                                $competitive_exam['competitive_exam_stage'] = json_encode(array(
                                'name' => $this->input->post('exam_stage'),
                                'exam_score_quant_gmat' => $this->input->post('exam_score_quant_gmat'),
                                'exam_score_verbal_gmat' => $this->input->post('exam_score_verbal_gmat'),
                                'exam_score_ir' => $this->input->post('exam_score_ir'),
                                'exam_score_awa_gmat' => $this->input->post('exam_score_awa_gmat'),
                                'exam_score_total_gmat' => $this->input->post('exam_score_total_gmat')
                                ));
                            }
                            elseif($this->input->post('competitive_exam') == 'SAT'){
                                $competitive_exam['competitive_exam_stage'] = json_encode(array(
                                'name' => $this->input->post('exam_stage'),
                                'exam_score_cirital_reading' => $this->input->post('exam_score_cirital_reading'),
                                'exam_score_writing' => $this->input->post('exam_score_writing'),
                                'exam_score_math' => $this->input->post('exam_score_math'),
                                'exam_score_total_sat' => $this->input->post('exam_score_total_sat')
                                ));
                            }
                        }
                    }
                    if($this->student_model->checkQualifiedExam(array('student_id' => $studentId))){
                        $this->student_model->updateQualifiedExam($competitive_exam, $studentId);
                    }
                    else{
                        $this->student_model->insertQualifiedExam($competitive_exam);
                    }
                }
                if($this->input->post('english_proficiency_exam')){
                    $english_exam = array(
                        'student_id' => $studentId,
                        'english_exam_type' => $this->input->post('english_proficiency_exam'),
                        'english_exam_stage' => json_encode(array(
                            'name' => '',
                            'value' => '',
                            'english_exam_score_reading' => '',
                            'english_exam_score_listening' => '',
                            'english_exam_score_writing' => '',
                            'english_exam_score_speaking' => '',
                            'english_exam_score_total' => ''
                        ))
                    );
                    if($this->input->post('english_proficiency_exam') != 'NO' && $this->input->post('english_proficiency_exam') != 'NOT_REQUIRED' && $this->input->post('english_exam_stage')){
                        $english_exam['english_exam_stage'] = json_encode(array(
                            'name' => $this->input->post('english_exam_stage')
                        ));

                        if($this->input->post('english_exam_stage') == 'REGISTERED' && $this->input->post('english_exam_date')){
                            $english_exam['english_exam_stage'] = json_encode(array(
                                'name' => $this->input->post('english_exam_stage'),
                                'value' => $this->input->post('english_exam_date')
                            ));
                        }
                        elseif($this->input->post('english_exam_stage') == 'RESULT_WAIT' && $this->input->post('english_result_date')){
                            $english_exam['english_exam_stage'] = json_encode(array(
                                'name' => $this->input->post('english_exam_stage'),
                                'value' => $this->input->post('english_result_date')
                            ));
                        }
                        elseif($this->input->post('english_exam_stage') == 'RESULT_OUT'){
                            $english_exam['english_exam_stage'] = json_encode(array(
                                'name' => $this->input->post('english_exam_stage'),
                                'english_exam_score_reading' => $this->input->post('english_exam_score_reading'),
                                'english_exam_score_listening' => $this->input->post('english_exam_score_listening'),
                                'english_exam_score_writing' => $this->input->post('english_exam_score_writing'),
                                'english_exam_score_speaking' => $this->input->post('english_exam_score_speaking'),
                                'english_exam_score_total' => $this->input->post('english_exam_score_total')
                            ));
                        }
                    }
                    if($this->student_model->checkQualifiedExam(array('student_id' => $studentId))){
                        $this->student_model->updateQualifiedExam($english_exam, $studentId);
                    }
                    else{
                        $this->student_model->insertQualifiedExam($english_exam);
                    }
                }
                break;
            case 'desired_courses':
                $desired_course = array(
                    'student_id'			=>	$studentId,
                    'desired_course_name'	=>	$this->input->post('desired_course_name'),
                    'desired_course_level'	=>	$this->input->post('desired_coures_level'),
                    'desired_course_intake'	=>	$this->input->post('desired_course_intake'),
                    'desired_course_year'	=>	$this->input->post('desired_coures_year')
                );
                if($this->student_model->checkDesiredCourse(array('student_id' => $studentId))){
                    $this->student_model->updateDesiredCourse($desired_course, $studentId);
                }else{
                    $this->student_model->insertDesiredCourse($desired_course);
                }
                $data = array(
                    'desired_destination' 	=>	implode(",",$this->input->post('deisred_destinations'))
                );

                $this->student_model->updateStudentDetails($data, $studentId);
                break;
            case 'parent_details':
                $data = array(
                    'parent_name'			=>	$this->input->post('parent_name'),
                    'parent_mob'			=>	$this->input->post('parent_mob'),
                    'parent_occupation'		=>	$this->input->post('parent_occupation')
                );
                $this->student_model->updateStudentDetails($data, $studentId);
                break;

          case 'work_exp':
                $data = array(
                  'company_name'			=>	$this->input->post('company_name'),
                  'designation'			=>	$this->input->post('designation'),
                  'work_start_date'		=>	$this->input->post('work_start_date'),
                  'work_end_date'			=>	$this->input->post('work_end_date')
                );

             $this->student_model->updateStudentDetails($data, $studentId);
              break;

          case 'emergency_contact_detail':
                $data = array(
                    'e_name'			=>	$this->input->post('e_name'),
                    'e_mobile_number'			=>	$this->input->post('e_mobile_number'),
                    'e_email'		=>	$this->input->post('e_email'),
                    'e_relation'			=>	$this->input->post('e_relation')
                  );
                $this->student_model->updateStudentDetails($data, $studentId);
                break;

            case 'change_password':
                $password = $this->input->post('password');
                $confirmPassword = $this->input->post('password2');
                if($password != $confirmPassword){
                    $this->session->set_flashdata('flash_message', "Password and confirm password does not match.");
                    redirect('/student/index/profile?id=' . $studentId);
                }
                $data =  array(
                    'password' => md5($password)
                );
                $this->user_model->updateUserInfo($data, $userId);
                echo '<script type="text/javascript">
                alert("The password has been updated successfully");
                window.location.href = "/admin/student/index/profile?id=' . $studentId . '";
                </script>';
                break;
        }

        if($formType != 'change_password'){
            $this->session->set_flashdata('flash_message', "Your profile has been updated.");
            redirect('/student/index/profile?id=' . $studentId);
        }
    }

    function add()
    {
        $whereCountryCond = array(
            'country_master.status' => 1
        );
        $allCountries = $this->country_model->getCountries($whereCountryCond);
        $allCategories = $this->college_model->getAllCategories();
        $allDegrees = $this->course_model->getAllDegrees();
        $this->outputData['countries'] = $allCountries;
        $this->outputData['mainstreams'] = $allCategories;
        $this->outputData['degrees'] = $allDegrees;
        $this->render_page('templates/student/add_form', $this->outputData);
    }

    function substream()
    {
        $mainstreamId = $this->input->get('cat_id');
        $allSubstreams = $this->course_model->getCategoriesByMainstream($mainstreamId);
        echo json_encode($allSubstreams);
    }

    function create()
    {
        $userdata = $this->session->userdata('user');
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $username = $this->input->post('mobile');
        $mobile = $this->input->post('mobile');
        $country = $this->input->post('country');
        $mainstream = $this->input->post('college_category_id');
        $substream = $this->input->post('course_category_id');
        $level = $this->input->post('degree_id');
        $originatorId = $facilitatorId = $userdata['id'];

        if(!$name || !$email || !$mobile || !$country || !$mainstream || !$substream || !$level)
        {
            $this->session->set_flashdata('flash_message', "Please fill all mandatory fields.");
            redirect('/student/index/add');
        }

        $checkUserEmail = $this->student_model->getUserMaster($email);
        $checkUserPhone = $this->student_model->getUserMaster($mobile);

        if($checkUserEmail)
        {
            $this->session->set_flashdata('flash_message', "Email-Id already exists.");
            redirect('/student/index/add');
        }

        if($checkUserPhone)
        {
            $this->session->set_flashdata('flash_message', "Mobile Number already exists.");
            redirect('/student/index/add');
        }

        /*$data = array(
            'name'          => $name,
            'email' 		=> $email,
            'phone' 		=> $mobile,
            'mainstream'    => $mainstream,
            'courses' 		=> $substream,
            'degree' 		=> $level,
            'countries'     => $country
   		);

        $leadId = $this->student_model->insertLead($data);*/

        $password = 'hellouni' . rand(1000, 9999);

        $data = array(
            'name' 			=> $name,
            'username'      => $username,
            'email' 		=> $email,
            'password' 		=> md5($password),
            'type' 			=> '5',
            'createdate' 	=> date('Y-m-d'),
            'activation' 	=> md5(time()),
            'fid'			=> '',
            'status' 		=> '1',
            'phone'         => $mobile,
            'registration_type' => 'FAIR',
            'registration_id' => 1
        );
        $user_id = $this->student_model->insertUserMaster($data);

        $student_data = array(
            'user_id'           => $user_id,
            'phone'             => $mobile,
            'facilitator_id'    => $facilitatorId,
            'originator_id'     => $originatorId
        );
        $studentId = $this->student_model->insertStudentDetails($student_data);

        $countriesId = is_array($this->input->post('country')) ? json_encode($this->input->post('country')) : json_encode(explode(",", $this->input->post('country'))) ;

        $desireInfoObj = [

          'student_id' => $studentId,
          'desired_country' => $countriesId,
          'desired_course_name' => $this->input->post('college_category_id'),
          'desired_sub_course_name' => $this->input->post('course_category_id'),
          'desired_course_level' => $this->input->post('degree_id')

        ];

        $this->student_model->insertDesireDetails($desireInfoObj);

        $this->session->set_flashdata('flash_message', "Student has been added successfully.");

        $studentName = $name;
        $studentEmail = $email;
        $mailSubject = "Welcome to the HelloUni-verse!";
        $mailTemplate = "Hi $studentName,

                           Welcome to HelloUni!

                           Your account has been created with following credentials

                           Username : $username
                           Password : $password

                           If you are seeking information about colleges and universities abroad, you are in the right place, and we are delighted to be able to help you.

                           Here is a list of all things HelloUni can offer you

                           &bull; Selection of the right course, in the right university, in the right country
                           &bull; Direct communication with the in-campus representative of the university of your choice
                           &bull; Assistance with the end-to-end application process
                           &bull; Connection with friends and alumnus from your choice of university

                           At HelloUni, we are always going to be by your side on the path towards great education and a bright future ahead.

                           Wishing you lots of luck and success!

                           <b>Thanks and Regards,
                           HelloUni Coordinator
                           +91 81049 09690</b>

                           <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

        $ccMailList = '';
        $mailAttachments = '';

        //$studentEmail = 'nikhil.gupta@issc.in';

        $sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

        if($userdata['type'] == 7)
        {
            redirect('/student/index/counselor');
        }
        redirect('/student/index/all');
    }

    function document()
    {
        $this->load->helper('cookie_helper');
		$userdata = $this->session->userdata('user');

        $postVariables = $this->input->post();
        $studentId = $postVariables['student_login_id'];
        $documentType = $postVariables['document_type'];

		if (!file_exists('./../uploads/student_' . $studentId))
        {
    		mkdir('./../uploads/student_' . $studentId, 0777, true);
		}

        $uploadFilePath     = './../uploads/student_' . $studentId;
        $uploadPath         = '/uploads/student_' . $studentId;
        $pathInfo           = pathinfo($_FILES['document']['name']);
        $tempfilename       = preg_replace('/\s+/', '_', $pathInfo['filename']);
        $tempfilename       = str_replace('.', '_', $tempfilename);
        $tempfilename       = str_replace('&', '_', $tempfilename);
        $tempfilename       = str_replace('-', '_', $tempfilename);
        $tempfilename       = str_replace('+', '_', $tempfilename);

        $fileExt                    = $pathInfo['extension'];
        $fileName                   = $studentId . '_' . $documentType . '_' . $tempfilename . '.' . $fileExt;
        $config['upload_path']      = $uploadFilePath;
        $config['file_name']        = $fileName;
        $config['allowed_types']    = 'gif|jpg|png|JPEG|JPG|jpeg|pdf|PDF';
        $config['max_size']         = 2048;
        $config['max_width']        = 10240;
        $config['max_height']       = 7680;

        $this->load->library('upload', $config);

        $studentDocument = $this->student_model->getDocumentByStudentType($studentId, $documentType);
        if($studentDocument)
        {
            $oldFile = $_SERVER['DOCUMENT_ROOT'] . $uploadPath . '/' . $studentDocument->name;
            if(file_exists($oldFile))
            {
                unlink($oldFile);
            }
        }

        if(!$this->upload->do_upload('document'))
        {
            $this->outputData['document_error'] = $this->upload->display_errors();
        }
        else
        {
            if($studentDocument)
            {
                $updatedData = [
                    "name" => $fileName,
                    "url" => str_replace('/admin', '', base_url()) . $uploadPath . '/' . $fileName
                ];
                $documentUpdate = $this->student_model->updateDocumentByStudentType($studentId, $documentType, $updatedData);
            }
            else
            {
                $insertData = [
                    "user_id"           => $studentId,
                    "name"              => $fileName,
                    "url"      => str_replace('/admin', '', base_url()) . $uploadPath . '/' . $fileName,
                    "document_meta_id"  => $documentType
                ];

                $documentInsert = $this->student_model->addDocument($insertData);
            }

            redirect('/student/index/all');
        }
    }

    function delete()
    {
        $userdata = $this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $userId = $this->input->get('uid') ? $this->input->get('uid') : '';
        if(!$userId)
        {
            redirect('/student/index/all');
        }

        $updatedData['status'] = 2;
        $this->student_model->updateUser($userId, $updatedData);
    }

    function notesInsert(){

      $this->load->model('webinar/webinar_model');

      $userdata=$this->session->userdata('user');

      if(!$userdata){  echo"NOTVALIDUSER"; exit(); }

      $userId = $userdata['id'];
      $studentId = $this->input->post('student_id');
      $applicationId = $this->input->post('app_id');
      $notes = $this->input->post('notes');

      $insertData = [
        "user_id" => $studentId,
        "application_id" => $applicationId,
        "notes" => $notes,
        "created_by" => $userId,
        "status" => 1
      ];

      $insertObj = $this->webinar_model->post($insertData, "application_notes");

      echo "SUCCESS";

    }

    function appDocUpdate(){

    $this->load->helper('cookie_helper');
    $this->load->model('webinar/webinar_model');

		$userdata = $this->session->userdata('user');
	    if(!$userdata)
        {
            redirect('user/account');
        }

          $postVariables = $this->input->post();
          $studentId = $postVariables['student_id'];
          $documentType = $postVariables['document_type'];


        if (!file_exists('./../uploads/student_' . $studentId))
            {
        		mkdir('./../uploads/student_' . $studentId, 0777, true);
    		}

        $uploadFilePath     = './../uploads/student_' . $studentId;
        $uploadPath         = '/uploads/student_' . $studentId;
        $pathInfo           = pathinfo($_FILES['document']['name']);
        $tempfilename       = preg_replace('/\s+/', '_', $pathInfo['filename']);
        $tempfilename       = str_replace('.', '_', $tempfilename);
        $tempfilename       = str_replace('&', '_', $tempfilename);
        $tempfilename       = str_replace('-', '_', $tempfilename);
        $tempfilename       = str_replace('+', '_', $tempfilename);

        $fileExt                    = $pathInfo['extension'];
        $fileName                   = $studentId . '_' . $documentType . '_' . $tempfilename . '.' . $fileExt;
        $config['upload_path']      = $uploadFilePath;
        $config['file_name']        = $fileName;
        $config['allowed_types']    = 'gif|jpg|png|JPEG|JPG|jpeg|pdf|PDF';
        $config['max_size']         = 2048;
        $config['max_width']        = 10240;
        $config['max_height']       = 7680;

        $this->load->library('upload', $config);

        $studentDocument = $this->student_model->getDocumentByStudentType($studentId, $documentType);



        if($studentDocument)
        {
            $oldFile = $_SERVER['DOCUMENT_ROOT'] . $uploadPath . '/' .$studentDocument->name;
            if(file_exists($oldFile))
            {
                unlink($oldFile);
            }
        }

        if(!$this->upload->do_upload('document'))
        {
            //$this->outputData['document_error'] = $this->upload->display_errors();
            echo $this->upload->display_errors();
            exit();
        }
        else
        {
            if($studentDocument)
            {
                $updatedData = [
                    "name" => $fileName,
                    "url" => str_replace('/admin', '', base_url()) . $uploadPath . '/' . $fileName
                ];
                $documentUpdate = $this->student_model->updateDocumentByStudentType($studentId, $documentType, $updatedData);
            }
            else
            {
                $insertData = [
                    "user_id"           => $studentId,
                    "name"              => $fileName,
                    "url"      => str_replace('/admin', '', base_url()) . $uploadPath . '/' . $fileName,
                    "document_meta_id"  => $documentType
                ];

                $documentInsert = $this->student_model->addDocument($insertData);
            }

            echo "SUCCESS";

            //redirect('/user/account/update');
        }
    }

}
?>
