<?php
require_once APPPATH . 'libraries/Mail/sMail.php';
class Session extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
        $this->load->model(array('user/university_model', 'student_model', 'user/user_model'));
    }

    function universities()
    {
        $userdata = $this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $sortBy = "universities.id DESC";
        $allUniversities = $this->university_model->getActiveUniversities($sortBy);

        $this->outputData['universities'] = $allUniversities;
        $this->outputData['view'] = 'list';
        $this->render_page('templates/student/session_university', $this->outputData);
    }

    function students()
    {
        $userdata = $this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $universityId = $this->input->get('uid');
        $universityLoginId = $this->input->get('ulid');
        if(!$universityId)
        {
            $this->session->set_flashdata('flash_message', "University id is missing");
            redirect('student/session/universities');
        }
        //$allStudents = $this->student_model->getAll();
        $allStudents = $this->student_model->getAllForCounselors($userdata['id']);
        if($allStudents)
        {
            foreach($allStudents as $index => $student)
            {
                if($student->desired_destination)
                {
                    $explodeIds = explode(',', $student->desired_destination);
                    $desiredDestinations = $this->user_model->getCountriesByIds($explodeIds);
                    $countryNames = array();
                    foreach($desiredDestinations as $destination)
                    {
                        $countryNames[] = $destination['name'];
                    }
                    $desiredCountries = implode(', ', $countryNames);
                    $student->desired_destination = $desiredCountries;
                    $allStudents[$index] = $student;
                }
            }
        }

        $this->outputData['students'] = $allStudents;
        $this->outputData['university_id'] = $universityId;
        $this->outputData['university_login_id'] = $universityLoginId;
        $this->outputData['view'] = 'list';
        $this->render_page('templates/student/session_student', $this->outputData);
    }

    function book()
    {
        $userdata = $this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $postedData = $this->input->post();
        $universityId = $postedData['university_id'];
        $universityLoginId = $postedData['university_login_id'];
        $studentIds = $postedData['chk'];
        $alreadyConnectedStudents = array();
        $bookedSession = array();
        foreach($studentIds as $studentId)
        {
            $data['user_id'] = $studentId;
            $data['university_id'] = $universityId;
            $data['university_login_id'] = $universityLoginId;
            $data['date_created'] = date('Y-m-d H:i:s');

            $alreadyConnected = $this->student_model->getSlotByUserAndUniversity($data['user_id'], $data['university_id']);
            /*echo '<pre>';
            print_r($alreadyConnected);*/
            if($alreadyConnected)
            {
                $alreadyConnectedStudents[] = $studentId;
            }

            else
            {
                if($this->student_model->addSlot($data))
                {
                    $condition_1 = array("user_master.id" => $data['university_login_id']);
                    $condition_2 = array();
                    $universityDetail = $this->user_model->getUser($condition_1, $condition_2);

                    $condition_1 = array("user_master.id" => $studentId);
                    $condition_2 = array();
                    $studentDetail = $this->user_model->getUser($condition_1, $condition_2);

                    if($universityDetail && $studentDetail)
                    {
                        $universityEmail = $universityDetail[0]->email;
                        $universityName = $universityDetail[0]->name;
                        $studentName = $studentDetail[0]->name;
                        $studentEmail = $studentDetail[0]->email;

                        /*echo "uemail - " . $universityEmail . "<br>";
                        echo "uname - " . $universityName . "<br>";
                        echo "semail - " . $studentEmail . "<br>";
                        echo "sname - " . $studentName . "<br>";*/

                        $ccMailList = '';
                        $mailAttachments = '';

                        $mailSubject = "One To One Request For $universityName Successful";
                        $mailTemplate = "Hi $studentName

                                            It looks like $universityName has found a place in your wishlist!

                                            A representative from the university will be glad to answer all your queries.

                                            We will intimate the representative about your interest in knowing more about the university and request them to share their availability.

                                            <b>Thanks and Regards,
                                            HelloUni Coordinator
                                            +91 81049 09690</b>

                                            <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

                        $sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

                        $mailSubject = "Student's Request For One To One Session";
                        $mailTemplate = "Hi $universityName,

                                        The following student has sent a request for one to one session.

                                        Name = $studentName
                                        Email = $studentEmail

                                        <b>Thanks and Regards,
                                        HelloUni Coordinator
                                        +91 81049 09690</b>

                                        <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

                        $sendMail = sMail($universityEmail, $universityName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);
                    }
                    $bookedSession[] = $studentId;
                }
            }
        }
        //exit;

        /*echo '<pre>';
        print_r($alreadyConnectedStudents);
        print_r($bookedSession);*/

        $this->session->set_flashdata('flash_message', "Session requests have been created successfully and email has been sent to student and university.");
        redirect('student/session/universities');
    }
}
?>
