<?php

include_once APPPATH . 'mongo/autoload.php';

class Event extends CI_Controller
{
    private $_fair_universities_collection;
    private $_fair_delegates_collection;
    private $_help_collection;
    private $_allowedRooms = [];

    private $_attender_collection;

    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));

        $mongo_server = '13.233.46.52';
        $mongo_port = '27017';
        $mongo_client = 'mongodb://' . $mongo_server . ':' . $mongo_port;
        $client = new MongoDB\Client($mongo_client);
        $mongo_db = $client->HelloUni;

        $this->_attender_collection = $mongo_db->selectCollection('attenders');
        $this->_fair_universities_collection = $mongo_db->selectCollection('fair_universities');
        $this->_fair_delegates_collection = $mongo_db->selectCollection('fair_delegates');
        $this->_help_collection = $mongo_db->selectCollection('help_tech');
    }

    function index($encodedUsername)
    {
        $helpCursor = $this->_help_collection->find();
        foreach($helpCursor as $help)
        {
            $helpRequests[] = $help;
        }

        /*if($helpRequests)
        {
            //redirect('/event/help/' . $encodedUsername);
        }*/

        $username = base64_decode($encodedUsername);
        $fair_universites = [];
        $fair_delegates = [];

        $fair_universites_cursor = $this->_fair_universities_collection->find();
        $fair_delegates_cursor = $this->_fair_delegates_collection->find();

        foreach($fair_universites_cursor as $fair_university)
        {
            $fair_universites[] = $fair_university;
        }
        foreach($fair_delegates_cursor as $fair_delegate)
        {
            $fair_delegates[] = $fair_delegate;
        }

        foreach($fair_delegates as $fair_delegate)
        {
            if($fair_delegate['designation'] == 'Admin')
            {
                continue;
            }
            //$tokbox_id = $fair_delegate['mapping_id'];
            $tokbox_id = $fair_delegate['tokbox_token_id'];
            $fair_university_id = $fair_delegate['fair_university_id'];
            $this->_allowedRooms[] = $tokbox_id;

            if(isset($allPanelists[$tokbox_id]))
            {
                /*$allPanelists[$tokbox_id] = [
                    'type' => $fair_delegate['designation'],
                    'panelists' => $allPanelists[$tokbox_id]['panelists'] . ', ' . $fair_delegate['name'],
                    'token_id' => $tokbox_id,
                    'attended' => 0
                ];*/

                if($fair_delegate['name'] == $allPanelists[$tokbox_id]['panelists'])
                {
                    $allPanelists[$tokbox_id] = [
                        'type' => $allPanelists[$tokbox_id]['type'] . ', ' . $fair_delegate['designation'],
                        'panelists' => $fair_delegate['name'],
                        'token_id' => $tokbox_id,
                        'attended' => 0
                    ];
                }
                else
                {
                    $allPanelists[$tokbox_id] = [
                        'type' => $fair_delegate['designation'],
                        'panelists' => $allPanelists[$tokbox_id]['panelists'] . ', ' . $fair_delegate['name'],
                        'token_id' => $tokbox_id,
                        'attended' => 0
                    ];
                }
            }
            else
            {
                $allPanelists[$tokbox_id] = [
                    'type' => $fair_delegate['designation'],
                    'panelists' => $fair_delegate['name'],
                    'token_id' => $tokbox_id,
                    'attended' => 0
                ];
            }

            /*foreach($fair_universites as $fair_university)
            {
                if($fair_university_id == $fair_university['id'])
                {
                    $allPanelists[$tokbox_id] = [
                        'type' => $fair_university['name'],
                        'panelists' => $fair_delegate['name'],
                        'token_id' => $tokbox_id,
                        'attended' => 0
                    ];
                }
                else
                {
                    $allPanelists[$tokbox_id] = [
                        'type' => $fair_delegate['name'],
                        'panelists' => $fair_delegate['name'],
                        'token_id' => $tokbox_id,
                        'attended' => 0
                    ];
                }
            }*/
        }

        $attenderCursor = $this->_attender_collection->find();
        $tracked_users = [];
        foreach($attenderCursor as $attender)
        {
            foreach($allPanelists as $tokboxId => $panelist)
            {
                if($tokboxId == $attender['tokbox_id'])
                {
                    if(!isset($tracked_users[$tokboxId]))
                    {
                        $tracked_users[$tokboxId] = [];
                    }
                    if(!in_array($attender['user_id'], $tracked_users[$tokboxId]))
                    {
                        $tracked_users[$tokboxId][] = $attender['user_id'];
                        $tracked_users[$tokboxId][] = $attender['username'];
                        $allPanelists[$tokboxId]['attended']++;
                    }
                }
            }
        }

        $this->outputData['rooms'] = $allPanelists;
        $this->outputData['allowedRooms'] = $this->_allowedRooms;
        $this->outputData['encodedUsername'] = $encodedUsername;
        $this->outputData['username'] = $username;
        $this->outputData['view'] = 'list';

        $this->load->view('templates/common/header', $this->outputData);
        $this->load->view('templates/meeting/event_list', $this->outputData);
        $this->load->view('templates/common/footer', $this->outputData);

    }

    function url()
    {
        $this->load->model('event_model');
        $current_mapping = $this->event_model->get_mapped_url();
        $this->outputData['mapped_url'] = '';
        if($current_mapping)
        {
            $this->outputData['mapped_url'] = $current_mapping->mapped_url;
        }

        if($this->input->post())
        {
            $this->event_model->truncate_table();
            $data['mapped_url'] = $this->input->post('url');
            $this->event_model->add_mapped_url($data);
            redirect('/event/url');
        }
        $this->load->view('templates/common/header', $this->outputData);
        $this->load->view('templates/meeting/event_url', $this->outputData);
        $this->load->view('templates/common/footer', $this->outputData);
    }

    function help($encodedUsername)
    {
        $helpCursor = $this->_help_collection->find();
        $helpRequests = [];
        $allPanelists = [];
        foreach($helpCursor as $help)
        {
            $helpRequests[] = $help;
        }

        if($helpRequests)
        {
            $fair_universites = [];
            $fair_delegates = [];

            $fair_universites_cursor = $this->_fair_universities_collection->find();
            $fair_delegates_cursor = $this->_fair_delegates_collection->find();

            foreach($fair_universites_cursor as $fair_university)
            {
                $fair_universites[] = $fair_university;
            }
            foreach($fair_delegates_cursor as $fair_delegate)
            {
                $fair_delegates[] = $fair_delegate;
            }

            foreach($helpRequests as $request)
            {
                $roomId = $request['tokbox_id'];

                foreach($fair_delegates as $fair_delegate)
                {
                    //$tokbox_id = $fair_delegate['mapping_id'];
                    $tokbox_id = $fair_delegate['tokbox_token_id'];
                    if($tokbox_id == $roomId)
                    {
                        $allPanelists[$tokbox_id] = [
                            'type' => $fair_delegate['designation'],
                            'panelists' => $fair_delegate['name'],
                            'token_id' => $tokbox_id
                        ];

                        /*$fair_university_id = $fair_delegate['fair_university_id'];

                        foreach($fair_universites as $fair_university)
                        {
                            if($fair_university_id == $fair_university['id'])
                            {
                                $allPanelists[$tokbox_id] = [
                                    'type' => $fair_university['name'],
                                    'panelists' => $fair_delegate['name'],
                                    'token_id' => $tokbox_id
                                ];
                            }
                        }*/
                    }
                }
            }
        }

        $this->outputData['rooms'] = $allPanelists;
        $this->outputData['encodedUsername'] = $encodedUsername;
        $this->outputData['view'] = 'list';

        $this->load->view('templates/common/header', $this->outputData);
        $this->load->view('templates/meeting/event_help', $this->outputData);
        $this->load->view('templates/common/footer', $this->outputData);
    }
}
