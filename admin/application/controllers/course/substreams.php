<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Substreams extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
		
		 $this->load->model('additional/additional_model');
    }

	
	function index()
	{   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('course/course_model');
		 $userdata=$this->session->userdata('user');
		 /*echo '<pre>';
		 print_r($userdata);
		 echo '</pre>';
		 exit;*/
	     if(empty($userdata)){  redirect('common/login'); }
		
		$cond2 = array('course_master.status' => '1', 'course_master.type' => '2'); 
		$cond1 = array('course_to_university.university_id' => $userdata['id']); 			
        $this->outputData['university_courses'] = $this->course_model->getUniversityCourses($cond1,$cond2);
		
		
		
		
		 /*echo '<pre>';
		 print_r($this->outputData['selected_university_courses']);
		 echo '</pre>';
		 exit;*/
		
		
		//$this->outputData['course_types'] = $this->course_model->getAllCourseTypes();
		$this->outputData['view'] = 'list';
		
		
		/* echo '<pre>';
		 print_r($this->outputData['university_courses']);
		 echo '</pre>';
		 exit;
		*/
					
		//$this->load->view('templates/user/body',$this->outputData);
        $this->render_page('templates/course/substreams',$this->outputData);
	
    } 
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('course/course_model');
		 $this->load->model('course/degree_model');
		$this->load->model('additional/additional_model');

		 $userdata=$this->session->userdata('user');
		 //print_r($userdata); exit;
	     if(empty($userdata)){  redirect('common/login'); }	
		 
		 
		 $this->outputData['degrees'] = $this->additional_model->getRelatedDegrees(array('university_to_degree_to_course.course_id'=>$this->uri->segment('4'),'university_to_degree_to_course.university_id'=>$userdata['id']));
		// print_r($this->outputData['degrees']); exit;
		 
		 $conditions = array('course_master.id' => $this->uri->segment('4'));
		 $course_details = $this->course_model->getCourseByid($conditions);
		 
					
		$this->outputData['id'] 			= $course_details->id;
		$this->outputData['name'] 			= $course_details->name;
		$this->outputData['details'] 		= $course_details->details;
		$this->outputData['type'] 			= $course_details->type;
		$this->outputData['parent'] 		= $course_details->course_id;
		$this->outputData['status'] 		= $course_details->status;
		
		$this->outputData['parent'] = $this->course_model->getParentCourse(array('course_to_subCourse.subcourse_id'=>$this->uri->segment('4'),'course_master.status'=>1));
		//print_r($this->outputData['parent']); exit;
		
		
		 $condition2 = array(
			
			'university_to_degree_to_course.course_id' 			=>  $this->uri->segment('4'),
        	'university_to_degree_to_course.university_id'		=>  $userdata['id']
		);
		 $course_details2 = $this->additional_model->getuniversity_to_degree_to_course($condition2);
		 $this->outputData['info'] = $this->additional_model->getuniversity_to_degree_to_course($condition2);
		//echo '<pre>'; print_r($course_details2); echo '</pre>'; exit;
		 
		
		
        $this->render_page('templates/course/substreams_form',$this->outputData);
	
     } 
	 
	 function save()
	 {   
	  
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('user/user_model');
	   $this->load->model('course/course_model');
	   $this->load->model('additional/additional_model');
	   
	   
	   $userdata=$this->session->userdata('user');
	   if(empty($userdata)){  redirect('common/login'); }	
		 
	   //die('+++');
	   
	   $data = array(
	        
        	'language' 				=>	$this->input->post('courselanguage'),
			'admissionsemester' 	=>	$this->input->post('admissionsemester'),
			'beginning' 			=>	$this->input->post('beginning'),
			'duration' 				=>	$this->input->post('programduration'),
			'application_deadline' 	=>	$this->input->post('applicationdeadline'),
			'details' 				=>	$this->input->post('course_details'),
			'tutionfee' 			=>	$this->input->post('tutionfees'),
			'enrolmentfee' 			=>	$this->input->post('enrolmentfees'),
			'livingcost' 			=>	$this->input->post('livecost'),
			'jobopportunities' 		=>	$this->input->post('jobopportunities'),
			'required_language' 	=>	$this->input->post('language_requirement'),
			'academicrequirement'	=>	$this->input->post('academic_requirement')
				
		);
		
		
		
		$condition = array(
			'university_to_degree_to_course.degree_id' 			=>  $this->input->post('degreeId'),
			'university_to_degree_to_course.course_id' 			=>  $this->input->post('itemId'),
        	'university_to_degree_to_course.university_id'			=>  $userdata['id']
		);
		
		//print_r($condition); exit;
		
		$this->additional_model->editUniversitySubcourseDetails($condition,$data);
							 
	    $this->session->set_flashdata('flash_message', "Success: You have saved Course!");
	    redirect('course/substreams');
		
		
	   
	}
	 
	 
	 function edit()
	 {   
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('user/user_model');
	   $this->load->model('course/course_model');
       
	  $userdata=$this->session->userdata('user');
	   if(empty($userdata)){  redirect('common/login'); }
	   
	   $condition1 = array('course_master.status' => '1'); 
	   $condition2 = array('course_to_subCourse.course_id' => $this->uri->segment('4'),'course_to_university.university_id' => $userdata['id'],'course_to_university.course_type' => '2'); 		
	   
	   $conditionData = array('course_id' => $this->uri->segment('4'),'university_id' => $userdata['id'],'course_type' => '2');
	   $data = array('subcourse' => $this->input->post('subcourse'));
       
	   
	   $this->course_model->updateUniversityCourses($conditionData,$data);
			
			
		
		 
		// if($this->course_model->editCourse($data)=='success'){
		 		 	 
	    // $this->session->set_flashdata('flash_message', "You have modified Course!");
	      redirect('course/streams');
	    // }
	 
	   
	}
	
	
	function delete()
	 {   
	   
	   $this->load->helper(array('form', 'url'));
	   $this->load->model('course/course_model');
	   $userdata=$this->session->userdata('user');
	   //echo $this->uri->segment('4'); exit;
	   $conditionData = array('course_id' => $this->uri->segment('4'),'university_id' => $userdata['id'],'course_type' => '2');
	   $this->course_model->deleteStream($conditionData);
	   $this->session->set_flashdata('flash_message', "Success: You have deleted Course!");
	   redirect('course/streams');
	   
	}
	
	
	function multidelete(){
	
	    $this->load->helper(array('form', 'url'));
		$this->load->model('course/course_model');
		
		 $userdata=$this->session->userdata('user');
		
		$array = $this->input->post('chk');
		
		
		foreach($array as $id){
		
		 $conditionData = array('course_id' => $id,'university_id' => $userdata['id'],'course_type' => '2');
	    $this->course_model->deleteStream($conditionData);
		
		}
		$this->session->set_flashdata('flash_message', "You have deleted Streams!");
		redirect('course/streams');
	
	
	}
	
	function getAllParentCourses()
	 {   
	  
	   $this->load->helper(array('form', 'url'));
	   $this->load->model('course/course_model');
		
	   if($this->input->post('typeid')==2){
		$cond1 = array('course_master.status' => '1','course_master.type' => '1','course_master.id !=' => $this->input->post('id') ); 
			
       // print_r($this->course_model->getCourse($cond1)); //exit;
		foreach($this->course_model->getCourse($cond1) as $course){
		echo '<option value="'.$course->id.'">'.$course->name.'</option>';
	    }
	 
	 }
	// print_r($this->course_model->getCourse($cond1));
	         
	   
	}
	
	function getSubCourses()
	{   
	    // $this->load->helper('cookie_helper'); 
         $this->load->model('course/course_model');
		
		
		 $conditions = array('course_to_subCourse.course_id' => $this->input->post('id'), 'course_master.type' => '2', 'course_master.status' => '1' );
		 $subCourses = $this->course_model->getsubCourses($conditions);
		
		foreach($subCourses as $subcourse){
		echo '<option value="'.$subcourse->id.'">'.$subcourse->name.'</option>';
		}
		
					
		//$this->load->view('templates/user/body',$this->outputData);
       // $this->render_page('templates/course/streams',$this->outputData);
	
    } 
	 
}//End  Home Class

?>