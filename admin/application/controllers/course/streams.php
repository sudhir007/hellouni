<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Streams extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

	
	function index()
	{   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('course/course_model');
		 $userdata=$this->session->userdata('user');
		 /*echo '<pre>';
		 print_r($userdata);
		 echo '</pre>';
		 exit;*/
	     if(empty($userdata)){  redirect('common/login'); }
		
		$cond2 = array('course_master.status' => '1', 'course_master.type' => '1'); 
		$cond1 = array('course_to_university.university_id' => $userdata['id']); 			
        $this->outputData['university_courses'] = $this->course_model->getUniversityCourses($cond1,$cond2);
		
		foreach($this->outputData['university_courses'] as $selectedCourse){
		$condition1 = array('course_master.status' => '1'); 
		$condition2 = array('course_to_subCourse.course_id' => $selectedCourse->idd,'course_to_university.university_id' => $userdata['id'],'course_to_university.course_type' => '2'); 			
        $this->outputData['selected_university_courses']['course'.$selectedCourse->idd] = $this->course_model->getUniversityCourses($condition1,$condition2);
		}
		
		
		 /*echo '<pre>';
		 print_r($this->outputData['selected_university_courses']);
		 echo '</pre>';
		 exit;*/
		
		
		//$this->outputData['course_types'] = $this->course_model->getAllCourseTypes();
		$this->outputData['view'] = 'list';
		
		
		/* echo '<pre>';
		 print_r($this->outputData['university_courses']);
		 echo '</pre>';
		 exit;
		*/
					
		//$this->load->view('templates/user/body',$this->outputData);
        $this->render_page('templates/course/streams',$this->outputData);
	
    } 
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('course/course_model');

		 $userdata=$this->session->userdata('user');
		 //print_r($userdata); exit;
	     if(empty($userdata)){  redirect('common/login'); }		
		 
		 $conditions1 = array('course_master.type' => '1', 'course_master.status' => '1');
		 //$conditions2 = array('course_to_university.university_id' => $userdata['id']);
		//16 $this->outputData['courses'] = $this->course_model->getStreams($conditions1,$userdata['id']);
		  $this->outputData['courses'] = $this->course_model->getCourse($conditions1);
		// $allcourses = $this->course_model->getCourse($conditions1);
		 	
		///////////////////////////////////////
		$condition1 = array('course_master.status' => '1'); 
		$condition2 = array('course_to_university.university_id' => $userdata['id'],'course_to_university.course_type' => '1'); 			
        $existCourse = $this->course_model->getUniversityCourses($condition1,$condition2);		
		 /*echo '<pre>';
		print_r($existCourse); 
		echo '</pre>'; exit;*/
		
		
		/*echo '<pre>';
		print_r($this->outputData['courses']); 
		echo '</pre>'; exit;*/
		
		///////////////////////////////////////
				 
		 if($this->uri->segment('4')){
		 		
		 $found = false;
		 foreach($existCourse as $key => $var){
		  if($this->uri->segment('4') == $var->idd){
		   $found = true;
           $info = 1;
		   break;
		   }		   
		}
		
		if ($found) unset($existCourse[$key]);
				
		//16 $conditions1 = array('course_master.type' => '1', 'course_master.status' => '1');
		 //$conditions2 = array('course_to_university.university_id' => $userdata['id']);
		 //16$this->outputData['courses'] = $this->course_model->getCourse($conditions1);
		 
		 $conditions = array('course_master.id' => $this->uri->segment('4'));
		 $course_details = $this->course_model->getCourseByid($conditions);
		 //print_r($course_details); exit;
					
		$this->outputData['id'] 			= $course_details->id;
		/*$this->outputData['name'] 			= $course_details->name;
		$this->outputData['details'] 		= $course_details->details;
		$this->outputData['type'] 			= $course_details->type;
		$this->outputData['parent'] 		= $course_details->course_id;
		$this->outputData['status'] 		= $course_details->status;*/
		
		  $cond1 = array('course_master.type' => '2', 'course_master.status' => '1', 'course_to_subCourse.course_id' => $this->uri->segment('4'));
		 $this->outputData['subcourses'] = $this->course_model->getsubCourses($cond1);
		 
		$condition1 = array('course_master.status' => '1'); 
		$condition2 = array('course_to_subCourse.course_id' => $this->uri->segment('4'),'course_to_university.university_id' => $userdata['id'],'course_to_university.course_type' => '2'); 			
        $this->outputData['selected_university_courses'] = $this->course_model->getUniversityCourses($condition1,$condition2);
		/*echo '<pre>';
		print_r($this->outputData['selected_university_courses']); echo '</pre>'; exit;*/
	    
		}
		  
				 
		$found = false;
		foreach($this->outputData['courses'] as $key => $var){
		
		  foreach($existCourse as $ec){
		  if($ec->idd == $var->id){
		   $found = true;
           $info = 1;
		   break;
		   }
		   
		  }
		  
		  if ($found){
		  unset($this->outputData['courses'][$key]);
		  $found = false;
		  
		  }
		
		}
		
		
        $this->render_page('templates/course/stream_form',$this->outputData);
	
     } 
	 
	 function create()
	 {   
	  
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('user/user_model');
	   $this->load->model('course/course_model');
	   
	   $userdata=$this->session->userdata('user');
	   if(empty($userdata)){  redirect('common/login'); }	
		 
	   $data = array(
        	'course_id' 	=>  $this->input->post('maincourse'),
        	'course_type' 	=>  '1',
			'university_id'		=> $userdata['id'],
        	'status' 		=>	$this->input->post('status')
			
			
		);
		$this->course_model->insertUniversityCourse($data);
		
		foreach($this->input->post('subcourse') as $subcourse){	
		
		$subcoursedata = array(
        	'course_id' 	=>  $subcourse,
        	'course_type' 	=>  '2',
			'university_id'	=> $userdata['id'],
        	'status' 		=>	$this->input->post('status')
		);
		
		$this->course_model->insertUniversityCourse($subcoursedata);
		
		}
			 
	    $this->session->set_flashdata('flash_message', "Success: You have saved Course!");
	    redirect('course/streams');
	   
	}
	 
	 
	 function edit()
	 {   
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('user/user_model');
	   $this->load->model('course/course_model');
       
	  $userdata=$this->session->userdata('user');
	   if(empty($userdata)){  redirect('common/login'); }
	   
	   $condition1 = array('course_master.status' => '1'); 
	   $condition2 = array('course_to_subCourse.course_id' => $this->uri->segment('4'),'course_to_university.university_id' => $userdata['id'],'course_to_university.course_type' => '2'); 		
	   
	   $conditionData = array('course_id' => $this->uri->segment('4'),'university_id' => $userdata['id'],'course_type' => '2');
	   $data = array('subcourse' => $this->input->post('subcourse'));
       
	   
	   $this->course_model->updateUniversityCourses($conditionData,$data);
			
			
		
		 
		// if($this->course_model->editCourse($data)=='success'){
		 		 	 
	    // $this->session->set_flashdata('flash_message', "You have modified Course!");
	      redirect('course/streams');
	    // }
	 
	   
	}
	
	
	function delete()
	 {   
	   
	   $this->load->helper(array('form', 'url'));
	   $this->load->model('course/course_model');
	   $userdata=$this->session->userdata('user');
	   //echo $this->uri->segment('4'); exit;
	   $conditionData = array('course_id' => $this->uri->segment('4'),'university_id' => $userdata['id'],'course_type' => '2');
	   $this->course_model->deleteStream($conditionData);
	   $this->session->set_flashdata('flash_message', "Success: You have deleted Course!");
	   redirect('course/streams');
	   
	}
	
	
	function multidelete(){
	
	    $this->load->helper(array('form', 'url'));
		$this->load->model('course/course_model');
		
		 $userdata=$this->session->userdata('user');
		
		$array = $this->input->post('chk');
		
		
		foreach($array as $id){
		
		 $conditionData = array('course_id' => $id,'university_id' => $userdata['id'],'course_type' => '2');
	    $this->course_model->deleteStream($conditionData);
		
		}
		$this->session->set_flashdata('flash_message', "You have deleted Streams!");
		redirect('course/streams');
	
	
	}
	
	function getAllParentCourses()
	 {   
	  
	   $this->load->helper(array('form', 'url'));
	   $this->load->model('course/course_model');
		
	   if($this->input->post('typeid')==2){
		$cond1 = array('course_master.status' => '1','course_master.type' => '1','course_master.id !=' => $this->input->post('id') ); 
			
       // print_r($this->course_model->getCourse($cond1)); //exit;
		foreach($this->course_model->getCourse($cond1) as $course){
		echo '<option value="'.$course->id.'">'.$course->name.'</option>';
	    }
	 
	 }
	// print_r($this->course_model->getCourse($cond1));
	         
	   
	}
	
	function getSubCourses()
	{   
	    // $this->load->helper('cookie_helper'); 
         $this->load->model('course/course_model');
		
		
		 $conditions = array('course_to_subCourse.course_id' => $this->input->post('id'), 'course_master.type' => '2', 'course_master.status' => '1' );
		 $subCourses = $this->course_model->getsubCourses($conditions);
		
		foreach($subCourses as $subcourse){
		echo '<option value="'.$subcourse->id.'">'.$subcourse->name.'</option>';
		}
		
					
		//$this->load->view('templates/user/body',$this->outputData);
       // $this->render_page('templates/course/streams',$this->outputData);
	
    } 
	 
}//End  Home Class

?>