<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Degree extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

	
	function index()
	{   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('course/course_model');
		 $this->load->model('course/degree_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }
		
		$cond1 = array('degree_master.status !=' => '5'); 
		//$cond_user2 = array('course_master.type !=' => '1'); 			
        $this->outputData['degrees'] = $this->degree_model->getDegree($cond1);
		
		$this->outputData['view'] = 'list';
		
			
		//$this->load->view('templates/user/body',$this->outputData);
        $this->render_page('templates/course/degree',$this->outputData);
	
    } 
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('course/course_model');
		 $this->load->model('course/degree_model');

		 $userdata=$this->session->userdata('user');
		 //print_r($userdata); exit;
	     if(empty($userdata)){  redirect('common/login'); }		
		 
		 if($this->uri->segment('4')){
		 $conditions = array('degree_master.id' => $this->uri->segment('4'));
		 $degree_details = $this->degree_model->getDegreeByid($conditions);
		// print_r($degree_details); exit;
		
		 $cond = array('degree_to_course.degree_id' => $this->uri->segment('4'));
		// $courses_details = $this->course_model->getCoursesRelatedToDegreeByid($cond);
		 // print_r($courses_details); exit;
		 
		 
					
		$this->outputData['id'] 			= $degree_details->id;
		$this->outputData['name'] 			= $degree_details->name;
		$this->outputData['status'] 		= $degree_details->status;
		
		$this->outputData['courses'] 		= $this->course_model->getCoursesRelatedToDegreeByid($cond);
		
		/*echo '<pre>';
        print_r($this->outputData['courses']); 
		echo '</pre>';
		exit;*/
		
		}
		
		
		$condition_course = array('course_master.type' => 1, 'course_master.status' => 1);
		$parent_courses = $this->course_model->getCourse($condition_course);
		
		$fullCourseList = array();
		$fullCourseList[]['subcourses'] 	= array();
		$subCourses = array();
		$i = 0;
		
		foreach($parent_courses as $parent){
		
		
		 $fullCourseList[$i]['id'] 			= $parent->id;
		 $fullCourseList[$i]['name'] 		= $parent->name;
		 $fullCourseList[$i]['details'] 	= $parent->details;
		 $fullCourseList[$i]['type'] 		= $parent->type;
		 $fullCourseList[$i]['status'] 		= $parent->status;
		 $fullCourseList[$i]['subcourses'] 	= $this->course_model->getsubCourses(array('course_to_subCourse.course_id' => $parent->id, 'course_master.status' => 1));
		
		$i++;
		
		
		// array_push($fullCourseList,$parent);
		
		/* $subCourses = $this->course_model->getsubCourses(array('course_to_subCourse.course_id' => $parent->id, 'course_master.status' => 1));
		 
		 if($subCourses){
		 array_push($fullCourseList, $subCourses);
		 }
		 */
		  //unset($subCourses);
		
		}
		
		
		  
		/* echo '<pre>'; 
		 print_r($fullCourseList);
		 echo '</pre>';
		 
		 exit;*/
		 
		 $this->outputData['allList'] 		= $fullCourseList;
				 
		//$this->outputData['cities']	= $this->city_model->getCities();
        $this->render_page('templates/course/degree_form',$this->outputData);
	
     } 
	 
	 function create()
	 {   
	   //die('+++++');
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('user/user_model');
	   $this->load->model('course/course_model');
	   $this->load->model('course/degree_model');
	   
	   $userdata=$this->session->userdata('user');
	   
      	$data = array(
        	'name' 			=>  $this->input->post('name'),
        	'createdate' 	=>  date('Y-m-d'),
			'createdby' 	=>	$userdata['id'],
			'status' 		=>	$this->input->post('status'),
			
			'course_id' 	=> $this->input->post('courses')
			
			
		);
		
		
		
		//print_r($data); exit;	 
	     if($this->degree_model->insertDegree($data))
		 {
		 	$this->session->set_flashdata('flash_message', "Success: You have saved Degree!");
	     	redirect('course/degree');
	     }
	}
	 
	 
	 function edit()
	 {   
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('user/user_model');
	   $this->load->model('course/course_model');
	   $this->load->model('course/degree_model');
       
	  $userdata=$this->session->userdata('user');
	   
      	$data = array(
		    'id' 			=>  $this->uri->segment('4'),
        	'name' 			=>  $this->input->post('name'),
        	'modifiedby' 	=>	$userdata['id'],
			'status' 		=>	$this->input->post('status'),
			'course_id' 	=> $this->input->post('courses')
			
		);
		 
		 if($this->degree_model->editDegree($data)){
		 		 	 
	     $this->session->set_flashdata('flash_message', "You have modified Degree!");
	      redirect('course/degree');
	     }
	 
	   
	}
	
	
	function delete()
	 {   
	   
	   $this->load->helper(array('form', 'url'));
	   $this->load->model('course/course_model');
		
		if($this->course_model->deleteCourse($this->uri->segment('4'))=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have deleted Course!");
	         redirect('course/course');
	    }
	}
	
	
	function multidelete(){
	
	    $this->load->helper(array('form', 'url'));

		$this->load->model('course/course_model');
		
		$array = $this->input->post('chk');
		foreach($array as $id):
		
		$this->course_model->deleteCourse($id);
		
		endforeach;
		$this->session->set_flashdata('flash_message', "You have deleted Course!");
		 redirect('course/course');
	
	
	}
	
	function getAllParentCourses()
	 {   
	  
	   $this->load->helper(array('form', 'url'));
	   $this->load->model('course/course_model');
		
	   if($this->input->post('typeid')==2){
		$cond1 = array('course_master.status' => '1','course_master.type' => '1','course_master.id !=' => $this->input->post('id') ); 
			
       // print_r($this->course_model->getCourse($cond1)); //exit;
		foreach($this->course_model->getCourse($cond1) as $course){
		echo '<option value="'.$course->id.'">'.$course->name.'</option>';
	    }
	 
	 }
	// print_r($this->course_model->getCourse($cond1));
	         
	   
	}
	 
}//End  Home Class

?>