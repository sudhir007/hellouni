<?php

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

require_once APPPATH . 'libraries/Mail/sMail.php';

class Notification extends MY_Controller {

  public $outputData;		//Holds the output data for each view
  public $loggedInUser;


      function __construct() {

          parent::__construct();

          $this->load->library('template');

          $this->lang->load('enduser/home', $this->config->item('language_code'));

          $this->load->library('form_validation');

          $this->load->model('student_registration/country_model');
          $this->load->model('student_registration/webinar_model');
          $this->load->model('student_registration/common_model');
          $this->load->model('student_registration/course_model');
          $this->load->model('student_registration/degree_model');
          $this->load->model('student_registration/user_model');
          $this->load->model('student_registration/counsellor_model');
          $this->load->model('redi_model');

      }

      public function notificationListPage(){

        $this->load->helper('cookie_helper');
        $usrdata=$this->session->userdata('user');

        if(empty($usrdata)){
          redirect('common/login');
          exit();
        }

        $this->render_page('templates/notification/notification_list',$this->outputData);

      }

      public function notificationList(){

        $usrdata=$this->session->userdata('user');
//print_r($usrdata); 
        if(empty($usrdata)){
          redirect('common/login');
          exit();
        }


        $response['notification_list'] = [];

        if($usrdata['type'] == '5'){

          $whereStudentId = $usrdata['id'];

        } else {
          $counsellorId = $usrdata['id'] ? $usrdata['id'] : 5826;
          $key = "COUNSELLOR-CHILD-".$counsellorId;

          //$keyExist = $this->redi_model->getKey($key);
          $allChild = 35;

          /*if($keyExist){

             $allChild = $keyExist;

          } else{

            $getChild = $this->counsellor_model->getChild($counsellorId);

            $allChild = $getChild && $getChild[0]['child_ids'] ? $getChild[0]['child_ids'].", $counsellorId" : $counsellorId;

            $setKey = $this->redi_model->setKey($key, $allChild);
          }*/

          $studentList = $this->counsellor_model->studentList($allChild);
          //echo "<pre>";print_r($studentList); 


          $whereStudentId = "";
          foreach ($studentList as $keyrm => $valuerm) {
  									// code...
  									$whereStudentId .= $valuerm['user_id'].", " ;
  								}
  				$whereStudentId = rtrim($whereStudentId,", ");

        }

        $notificationList = $this->user_model->getNotificationList($whereStudentId);

        $studentNotificationCount = 0;
        $agentNotificationCount = 0;

        foreach ($notificationList['data'] as $keysa => $valuesa) {
//echo "<pre>";print_r($valuesa); exit;
          if($valuesa['seen_by_student'] == 0){
            $studentNotificationCount = $studentNotificationCount + 1;
          }

          if($valuesa['seen_by_agent'] == 0){
            $agentNotificationCount = $agentNotificationCount + 1;
          }
        }

        $response['notification_list'] = $notificationList['data'];
        $response['student_notification_count'] = $studentNotificationCount;
        $response['agent_notification_count'] = $agentNotificationCount;
        $response['notification_count'] = $usrdata['type'] == '5' ? $studentNotificationCount : $agentNotificationCount;

        $response['status'] = "SUCCESS";

       // echo $response['notification_list'];

        header('Content-Type: application/json');
        echo (json_encode($response));

      }

      public function notificationSeen(){

        $usrdata=$this->session->userdata('user');

        if(empty($usrdata)){
          redirect('common/login');
          exit();
        }

        $response['notification_list'] = [];

        if($usrdata['type'] == '5'){

          $whereStudentId = $usrdata['id'];

        } else {

          $counsellorId = $usrdata['id'] ? $usrdata['id'] : 5826;
          $key = "COUNSELLOR-CHILD-".$counsellorId;

          $keyExist = $this->redi_model->getKey($key);
          $allChild = 35;

          /*if($keyExist){

             $allChild = $keyExist;

          } else{

            $getChild = $this->counsellor_model->getChild($counsellorId);

            $allChild = $getChild && $getChild[0]['child_ids'] ? $getChild[0]['child_ids'].", $counsellorId" : $counsellorId;

            $setKey = $this->redi_model->setKey($key, $allChild);
          }*/

          $studentList = $this->counsellor_model->studentList($allChild);

          $whereStudentId = "";
          foreach ($studentList as $keyrm => $valuerm) {
  									// code...
  									$whereStudentId .= $valuerm['user_id'].", " ;
  								}
  				$whereStudentId = rtrim($whereStudentId,", ");

        }

        $notificationList = $this->user_model->updateNotificationList($whereStudentId, $usrdata['type']);

        $response['status'] = "SUCCESS";

        header('Content-Type: application/json');
        echo (json_encode($response));

      }

}
