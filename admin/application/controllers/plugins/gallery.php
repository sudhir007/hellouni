<?php
error_reporting(E_ALL);
/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Gallery extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

	
	function index()
	{   
	     $this->load->helper('cookie_helper'); 
		 $this->load->model('plugins/gallery_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }	
		 
		 $cond_gallery1 = array('gallery_master.status' => '1'); 
		 $cond_gallery2 = array('gallery_master.status' => '2'); 
		 
		 if($this->uri->segment('4')=='photo'){
		 
		  $this->outputData['gallery'] = $this->gallery_model->getPhotoGallaries($cond_gallery1,$cond_gallery2);
		 $this->render_page('templates/plugins/photo_gallery',$this->outputData);
		 
		 } elseif($this->uri->segment('4')=='video'){
		  $this->outputData['gallery'] = $this->gallery_model->getVideoGallaries($cond_gallery1,$cond_gallery2);
		 $this->render_page('templates/plugins/video_gallery',$this->outputData);
		 
		 }
	
     } 
	
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
		 $this->load->model('plugins/gallery_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }	
		 
		if($this->uri->segment('5')){
		
		$conditions = array('gallery_master.id' => $this->uri->segment('5'));
		$this->outputData['gallery_info'] = $this->gallery_model->getGalleryByid($conditions);
		
		$this->outputData['gallery_images'] = $this->gallery_model->getGalleryImages($this->uri->segment('5'));
		$this->outputData['row']=count($this->outputData['gallery_images']);
		
		
		 if($this->uri->segment('4')=='photo'){
		 
		 $this->render_page('templates/plugins/photo_gallery_form',$this->outputData);
		 
		 } elseif($this->uri->segment('4')=='video'){
		 
		 $this->render_page('templates/plugins/video_gallery_form',$this->outputData);
		 
		 }
		
		} else {
		
       $this->outputData['row']=0;
		
         if($this->uri->segment('4')=='photo'){
		 
		 $this->render_page('templates/plugins/photo_gallery_form',$this->outputData);
		 
		 } elseif($this->uri->segment('4')=='video'){
		 
		 $this->render_page('templates/plugins/video_gallery_form',$this->outputData);
		 
		 }		
		}
	
     } 
//	
	function create()
	 {   
	   
	   $this->load->helper(array('form', 'url'));
	   $this->load->model('plugins/gallery_model');
	   $links=$this->input->post('gallery');
	   
	if($this->uri->segment('4')=='photo'){   
	   $general = array(
                     'name' => $this->input->post('gallery_name'),
	                 'sort_order' => $this->input->post('gallery_sort_order'),
					 'type' => 'photo',
                     'status' => $this->input->post('gallery_status')
	                 );
	   
	   
	    $this->gallery_model->insertGallery($general,$links);
	   $this->session->set_flashdata('flash_message', "Success: You have modified Gallery!");
	   redirect('plugins/gallery/index/photo');
     }
	 elseif($this->uri->segment('4')=='video'){  
	 
	 
	   $general = array(
                     'name' => $this->input->post('gallery_name'),
	                 'sort_order' => $this->input->post('gallery_sort_order'),
					 'type' => 'video',
                     'status' => $this->input->post('gallery_status')
	                 );
	   
	   
	    $this->gallery_model->insertGallery($general,$links);
	   $this->session->set_flashdata('flash_message', "Success: You have modified Gallery!");
	   redirect('plugins/gallery/index/video');
	 
	 
	 
	   }
	}





function edit()
	 {   
	   
	   $this->load->helper(array('form', 'url'));
       $this->load->model('plugins/gallery_model');
	   $this->load->library('form_validation');
       $links=$this->input->post('gallery');
	   
	   if($this->uri->segment('4')=='photo'){  
	   $general = array(
                     'name' => $this->input->post('gallery_name'),
	                 'sort_order' => $this->input->post('gallery_sort_order'),
					 'type' => 'photo',
                     'status' => $this->input->post('gallery_status')
	                 );
	   
	    $this->gallery_model->updateGallery($this->uri->segment('5'),$general,$links);
	  
	   $this->session->set_flashdata('flash_message', "Success: You have modified Gallery!");
	   redirect('plugins/gallery/index/photo');
	   
	   
	   }
	   
	   
	   elseif($this->uri->segment('4')=='video'){  
	   
	     $general = array(
                     'name' => $this->input->post('gallery_name'),
	                 'sort_order' => $this->input->post('gallery_sort_order'),
					 'type' => 'video',
                     'status' => $this->input->post('gallery_status')
	                 );
	   
	    $this->gallery_model->updateGallery($this->uri->segment('5'),$general,$links);
	  
	   $this->session->set_flashdata('flash_message', "Success: You have modified Gallery!");
	   redirect('plugins/gallery/index/video');
	   
	   
	   }
	}
	
	
	
	

	function delete()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
       $this->load->model('plugins/gallery_model');
		
		
		if($this->gallery_model->deleteGallery($this->uri->segment('5'))=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have deleted Gallery!");
			 
			 if($this->uri->segment('4')=='photo'){
              redirect('plugins/gallery/index/photo');	        
	          }
			  
			  elseif($this->uri->segment('4')=='video'){
			  
			  redirect('plugins/gallery/index/video');	  
			  }
	 
	    }
	}


	
	function delete_multi()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
       $this->load->model('plugins/gallery_model');
		
		$array = $this->input->post('chk');
		foreach($array as $id):
		
		$this->gallery_model->deleteGallery($id);
		
		endforeach;
		$this->session->set_flashdata('flash_message', "Success: You have deleted Gallery!");
		
		
		
		
		 if($this->uri->segment('4')=='photo'){
              redirect('plugins/gallery/index/photo');	        
	          }
			  
			  elseif($this->uri->segment('4')=='video'){
			  
			  redirect('plugins/gallery/index/video');	  
			  }
		
	
	}
	
	

}//End  Home Class





?>