<?php

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

require_once APPPATH . 'libraries/Mail/sMail.php';
//require_once APPPATH . 'libraries/codeigniter-predis/src/Redis.php';


class Registration extends MY_Controller
{

  public $outputData;    //Holds the output data for each view
  public $loggedInUser;


  function __construct()
  {

    parent::__construct();
    $this->load->library('template');
    $this->lang->load('enduser/home', $this->config->item('language_code'));
    $this->load->library('form_validation');
    $this->load->model('location/country_model');
    $this->load->model('webinar/webinar_model');
    $this->load->model('student_registration/common_model');
    $this->load->model('course/course_model');
    $this->load->model('course/degree_model');
    $this->load->model('student_registration/user_model');
    $this->load->model('redi_model');

    //  track_user();
  }

  //team registration

  function teamForm()
  {

    $this->load->helper('cookie_helper');
    $usrdata = $this->session->userdata('user');

    if (empty($usrdata)) {
      redirect('common/login');
      exit();
    }

    $roleId = $usrdata['type'];
    $userId = $usrdata['id'];

    $superAdminList = $this->common_model->get(['type' => 1], 'user_master');
    $adminList = $this->common_model->get(['type' => 2], 'user_master');

    $this->outputData['superAdmin_list'] = $superAdminList['data'];
    $this->outputData['admin_list'] = $adminList['data'];
    $this->outputData['role_id'] = $roleId;

    $this->render_page('templates/team/team_registration_form', $this->outputData);
  }

  function teamSubmit()
  {

    $this->load->helper('cookie_helper');
    $usrdata = $this->session->userdata('user');

    if (empty($usrdata)) {
      redirect('common/login');
      exit();
    }

    $userName       = $this->input->post('user_name');
    $userEmail       = $this->input->post('user_email');
    $userMobile     = $this->input->post('user_mobile');
    $userPassword    = $this->input->post('user_password');
    $userId         = $this->input->post('user_mobile');
    $roleId         = $this->input->post('role_id');
    $getParentId    = $this->input->post('parent_id');

    if ($getParentId) {
      $parentId  = $this->input->post('parent_id');
    } else {
      $parentId  = $usrdata['id'];
    }

    $checkUserExist = $this->webinar_model->checkUserLogin($userEmail, $userMobile);

    if ($checkUserExist) {
      echo "With Email-ID Or Mobile Account Already Exists";
      exit();
    }

    $checkUserIDExist = $this->webinar_model->checkUserIDLogin($userId);

    if ($checkUserIDExist) {
      echo "User Name Already Exists. Please Select Different User Name ...!!!";
      exit();
    }

    $data = array(

      'name'       =>  $this->input->post('user_name'),
      'username'  => $this->input->post('user_mobile'),
      'email'     => $this->input->post('user_email'),
      'password'   =>  md5($this->input->post('user_password')),
      'image'      =>  '',
      'type'       =>  $roleId,
      'parent_id' =>  $parentId,
      'createdate'   =>  date('Y-m-d H:i:s'),
      'activation'   =>  md5(time()),
      'fid'        =>  '',
      'status'     =>  '1',
      'phone'     => $this->input->post('user_mobile'),
      'user_for'  => 'IMPERIALPLATFORMS_AGENT'

    );

    $user_id = $this->user_model->insertUserMaster($data);


    /*$agentName = $this->input->post('user_name');
      $agentEmail = $this->input->post('user_email');
      $username = $this->input->post('user_mobile');
      $password = $this->input->post('user_password');

      $mailSubject = "Thank you for Registering with Imperial Platforms! ";
      $mailTemplate = "Hi $agentName,

                        Greetings From Imperial Platforms

                        You have successfully registered on our portal. Please find the below given portal credentials

                        Portal Link : https://agent.imperialplatforms.com
                        Username : $username
                        Password : $password

                        Regards,
                        Imperial Platform Assist
                        Operations Head
                        +91 9167663090

                        <img src='https://agent.imperialplatforms.com/img/Imperial_Platforms_email.png'>

                        Kindly Login to the Application portal ( https://agent.imperialplatforms.com/ ) to check
                        <b>**Note</b>: This is an automated email, please do not reply to this email**

                        ";

      $ccMailList = '';
      $mailAttachments = '';

      $sendMail = sMail($agentEmail, $agentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

      $mailSubject = "New Agency Registration - $agencyName";
      $mailTemplate = "Hello,

                        Greetings

                        We have a new Agency registered on our portal

                        Name - $agentName
                        Email - $agentEmail
                        Mobile No - $username
                        Agency Name - $agencyName
                        State - $userState
                        City - $userCity
                        Address - $userAdd

                        Regards,
                        Imperial Platform Assist
                        Operations Head
                        +91 9167663090

                        <img src='https://agent.imperialplatforms.com/img/Imperial_Platforms_email.png'>

                        Kindly Login to the Application portal ( https://agent.imperialplatforms.com/ ) to check
                        <b>**Note</b>: This is an automated email, please do not reply to this email**

                        ";

      $ccMailList = 'sudhir.kanojia@imperialplatforms.com,banit@imperialplatforms.com';
      $mailAttachments = '';

      $sendMail = sMail("assist@imperialplatforms.com", $agentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);*/

    echo "SUCCESS";
  }

  function teamAllList()
  {

    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $teamAppListObj = $this->common_model->getTeamsDetails();

    $this->outputData['teamsDetails'] = $teamAppListObj;
    $this->outputData['view'] = 'partner';
    $this->outputData['breadcrumb'] = 'All Application List';
    $this->outputData['view'] = 'list';

    $this->render_page('templates/team/teamApplicationAllList', $this->outputData);
  }

  function deleteTeamLoginKey()
  {

    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $userLoginId = $this->input->post('login_id') ? $this->input->post('login_id') : 5826;

    $counsellorChildListKey = "COUNSELLOR-CHILD-" . $userLoginId;
    $counsellorChildListWithParentKey = "counsellorListCommaSeprated-" . $userLoginId;


    $counsellorChildListCommaSepratedObj = $this->redi_model->getKey($counsellorChildListKey);
    $counsellorChildListWithParentCommaSepratedObj = $this->redi_model->getKey($counsellorChildListWithParentKey);

    $counsellorChildListCommaSepratedObj = json_decode($counsellorChildListCommaSepratedObj, true);
    $counsellorChildListWithParentCommaSepratedObj = json_decode($counsellorChildListWithParentCommaSepratedObj, true);

    $counsellorChildDistroyKey = $this->redi_model->delLogin($counsellorChildListKey);
    $counsellorChildListWithParentDistroyKey = $this->redi_model->delLogin($counsellorChildListWithParentKey);

    $response['counsellorChildListKey'] = $counsellorChildListKey;
    $response['counsellorChildListWithParentKey'] = $counsellorChildListWithParentKey;
    $response['status'] = "SUCCESS";

    header('Content-Type: application/json');
    echo (json_encode($response));
  }

  // function teamUpdate()
  // {
  //   $this->load->helper('cookie_helper');
  //   $usrdata = $this->session->userdata('user');

  //   if (empty($usrdata)) {
  //     redirect('user/account');
  //     exit();
  //   }

  //   $roleId = $usrdata['type'];
  //   $useId = $this->uri->segment('4');
  //   $teamAppListObj = $this->common_model->getTeamsSigleDetails($useId);
  //   echo "<pre>"; print_r($teamAppListObj); exit;

  //   $this->outputData['user_name'] = $teamAppListObj[0]->name;
  //   $this->outputData['user_email'] = $teamAppListObj[0]->email;
  //   $this->outputData['user_mobile'] = $teamAppListObj[0]->phone;
  //   $this->outputData['user_role'] = $teamAppListObj[0]->type_id;
  //   $this->outputData['type_id'] = $teamAppListObj[0]->type_id;
  //   $this->outputData['user_status'] = $teamAppListObj[0]->status;
  //   $this->outputData['role_id'] = $roleId;

  //   $superAdminList = $this->common_model->get(['type' => 1], 'user_master');
  //   $adminList = $this->common_model->get(['type' => 2], 'user_master');

  //   $this->outputData['superAdmin_list'] = $superAdminList['data'];
  //   $this->outputData['admin_list'] = $adminList['data'];

  //   $this->outputData['selected_super_admin'] = ($teamAppListObj[0]->type_id == 1) ? $teamAppListObj[0]->id : null;
  //   $this->outputData['selected_admin'] = ($teamAppListObj[0]->type_id == 2) ? $teamAppListObj[0]->id : null;

  //   $this->render_page('templates/team/team_registration_update_form', $this->outputData);
  // }

  function teamUpdate()
  {
    $this->load->helper('cookie_helper');
    $usrdata = $this->session->userdata('user');

    if (empty($usrdata)) {
      redirect('user/account');
      exit();
    }

    $roleId = $usrdata['type'];
    $useId = $this->uri->segment('4');
    $teamAppListObj = $this->common_model->getTeamsSigleDetails($useId);

    $this->outputData['user_name'] = $teamAppListObj[0]->name;
    $this->outputData['user_email'] = $teamAppListObj[0]->email;
    $this->outputData['user_mobile'] = $teamAppListObj[0]->phone;
    $this->outputData['user_role'] = $teamAppListObj[0]->type_id;
    $this->outputData['type_id'] = $teamAppListObj[0]->type_id;
    $this->outputData['parentId'] = $teamAppListObj[0]->parent_id;
    $this->outputData['user_status'] = $teamAppListObj[0]->status;
    $this->outputData['role_id'] = $roleId;

    $superAdminList = $this->common_model->get(['type' => 1], 'user_master');
    $adminList = $this->common_model->get(['type' => 2], 'user_master');

    $this->outputData['superAdmin_list'] = $superAdminList['data'];
    $this->outputData['admin_list'] = $adminList['data'];

    $this->render_page('templates/team/team_registration_update_form', $this->outputData);
  }

  function teamUpdate_action()
  {
    $usrdata = $this->session->userdata('user');
    if (empty($usrdata)) {
      redirect('user/account');
      exit();
    }

    $partnerId = $this->input->post('partnerId');
    $user_role = $this->input->post('user_role');

    if ($user_role == 2) {
      $parentId = $this->input->post('super_admin_member');
    } else {
      $parentId = $this->input->post('admin_member');
    }

    $teamDetail = $this->common_model->get(['id' => $partnerId], 'user_master');

    if ($teamDetail) {

      if ($teamDetail['data'][0]['parent_id'] == $parentId) {
        $data = array(
          'name'      =>  $this->input->post('user_name'),
          'username'  => $this->input->post('user_mobile'),
          'email'     => $this->input->post('user_email'),
          'status'     => $this->input->post('user_status'),
          'modifydate'  =>  date('Y-m-d H:i:s'),
          'phone'     => $this->input->post('user_mobile')
        );
      } else {
        $data = array(
          'name'      =>  $this->input->post('user_name'),
          'username'  => $this->input->post('user_mobile'),
          'email'     => $this->input->post('user_email'),
          'status'     => $this->input->post('user_status'),
          'modifydate'  =>  date('Y-m-d H:i:s'),
          'phone'     => $this->input->post('user_mobile'),
          'type'      =>  $user_role,
          'parent_id' =>  $parentId
        );
      }
    }

    $user_id = $this->user_model->updateUserInfo($data, $partnerId);

    $this->session->set_flashdata('flash_message', "Team Registration Updated Successfully!!");

    redirect('/team/register/allList');
  }
}
