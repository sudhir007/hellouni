<?php


// ini_set('display_errors', '1');
// ini_set('display_startup_errors', '1');
// error_reporting(E_ALL);
/**



 * Reverse bidding system Home Class



 *



 * Permits admin to set the site settings like site title,site mission,site offline status.



 *



 * @package     Reverse bidding system



 * @subpackage  Controllers



 * @category    Common Display



 * @author      FreeLance PHP Script Team



 * @version     Version 1.0



 * @created     December 30 2008



 * @link        http://www.freelancephpscript.com





 */



class University extends MY_Controller {







    //Global variable



    public $outputData;     //Holds the output data for each view



    public $loggedInUser;







    /**



     * Constructor



     *



     * Loads language files and models needed for this controller



     */



     function __construct()

    {

        parent::__construct();

        $this->load->library('template');

        $this->lang->load('enduser/home', $this->config->item('language_code'));

        $this->load->library('form_validation');

        $this->load->model('location/country_model');

        $this->load->model('course/degree_model');

        //track_user();


    }





    function index()

    {   //die('++++');

    $this->load->model('user/auth_model');

  /*$this->auth_model->clearUserSession();



   $this->session->unset_userdata('leaduser');*/


         $this->load->helper('cookie_helper');

         $this->load->model('course/course_model');

         $this->load->model('user/user_model');
         $this->load->model('webinar/webinar_model');

         $usrdata=$this->session->userdata('user');

         //print_r($usrdata); exit;

         if(empty($usrdata)){  redirect('user/account'); }

         $leaddata=$this->session->userdata('leaduser');

         $userId = $usrdata['id'];
         /*echo '<pre>';
         print_r($leaddata); exit;*/
         //Set searched key

         $this->outputData['selected_country'] = '';

         $this->outputData['selected_mainstream'] = '';

         $this->outputData['selected_subcourse'] = '';

         $this->outputData['related_subcourses'] ='';

         $this->outputData['selected_degrees'] = '';

         $this->outputData['userLoggedIn'] = true;

        // if(empty($leaddata)){  redirect('user/account'); }

         // echo $leaddata['countries']; exit;



         //echo $countryList = $leaddata['countries']; exit;

         if($this->input->post('search')){

             $course_categories = array('0'=>$this->input->post('subcourse'));

             $degree = $this->input->post('degree');

             $country_array = array_map("strtoupper", array('0'=>$this->input->post('country')));  //array('0'=>$this->input->post('country'));





             $sessiondata2=$this->session->userdata('leaduser');

             $sessiondata2 = array(

             'mainstream'   => $this->input->post('mainstream'),

             'coursecategories'         => $this->input->post('subcourse'),

             'degree'       => $this->input->post('degree'),

             'countries'    => $this->input->post('country')

             );





            $this->session->set_userdata('leaduser', $sessiondata2);





            //Set searched key

             $this->outputData['selected_country'] = $this->input->post('country');

             $this->outputData['selected_mainstream'] = $this->input->post('mainstream');

             $this->outputData['selected_subcourse'] = $this->input->post('subcourse');

             $this->outputData['related_subcourses'] = $this->course_model->getCourseCategoriesByCollegeCategory($this->outputData['selected_mainstream']);



             $this->outputData['selected_degrees'] = $this->input->post('degree');





             /*$leaddata225=$this->session->userdata('leaduser');

             echo '<pre>';

            print_r($leaddata225);

            echo '</pre>'; exit;*/



        }elseif($leaddata){



             $course_category_string = $leaddata['coursecategories'];

             $degree = $leaddata['degree'];

             $course_categories = explode(",",$course_category_string);

             $country_array = array_map("strtoupper", explode(",",$leaddata['countries']));

             $this->outputData['selected_country'] = $leaddata['countries'];
             $this->outputData['selected_mainstream'] = $leaddata['mainstream'];
             $this->outputData['selected_subcourse'] = $leaddata['coursecategories'];
             $this->outputData['related_subcourses'] = $this->course_model->getCourseCategoriesByCollegeCategory($this->outputData['selected_mainstream']);
             $this->outputData['selected_degrees'] = $leaddata['degree'];
         }

         elseif($usrdata){
             $condition = array('email' => $usrdata['email']);
             $user_selection = $this->user_model->getLead($condition);
             $course_category_string = $user_selection->courses;
             $degree = $user_selection->degree;
             $course_categories = explode(",",$course_category_string);
             $country_array = array_map("strtoupper", explode(",",$user_selection->countries));

             $this->outputData['selected_country'] = $user_selection->countries;
             $this->outputData['selected_mainstream'] = $user_selection->mainstream;
             $this->outputData['selected_subcourse'] = $user_selection->courses;
             $this->outputData['related_subcourses'] = $this->course_model->getCourseCategoriesByCollegeCategory($this->outputData['selected_mainstream']);
             $this->outputData['selected_degrees'] = $user_selection->degree;
         }

         $sessiondata = array(
             'mainstream'   => $this->outputData['selected_mainstream'],
             'subcourse'    => $this->outputData['selected_subcourse'],
             'degree'       => $this->outputData['selected_degrees'],
             'country'      => $this->outputData['selected_country']
         );

         $this->session->set_userdata('uni_search_criteria', $sessiondata);


         $selectedCollegeCategories = explode(",", $this->outputData['selected_mainstream']);
         $selectedCourseCategories = explode(",", $this->outputData['selected_subcourse']);
         $selectedDegrees = explode(",", $this->outputData['selected_degrees']);

         $selectedCollegeCategoriesData = $this->user_model->getCollegeCategoriesByIds($selectedCollegeCategories);
         $selectedCourseCategoriesData = $this->user_model->getCourseCategoriesByIds($selectedCourseCategories);
         $selectedDegreesData = $this->user_model->getDegreesByIds($selectedDegrees);

         $this->outputData['selected_countries_name'] = '';
         $this->outputData['selected_mainstreams_name'] = '';
         $this->outputData['selected_subcourses_name'] = '';
         $this->outputData['selected_degrees_name'] = '';
         foreach($selectedCollegeCategoriesData as $collegeCategoryData)
         {
             $this->outputData['selected_mainstreams_name'] .= $collegeCategoryData['display_name'] . ', ';
         }
         foreach($selectedCourseCategoriesData as $courseCategoryData)
         {
             $this->outputData['selected_subcourses_name'] .= $courseCategoryData['display_name'] . ', ';
         }
         foreach($selectedDegreesData as $degreeData)
         {
             $this->outputData['selected_degrees_name'] .= $degreeData['name'] . ', ';
         }

         $this->outputData['selected_countries_name'] = $this->outputData['selected_country'];
         $this->outputData['selected_mainstreams_name'] = substr($this->outputData['selected_mainstreams_name'], 0, -2);
         $this->outputData['selected_subcourses_name'] = substr($this->outputData['selected_subcourses_name'], 0, -2);
         $this->outputData['selected_degrees_name'] = substr($this->outputData['selected_degrees_name'], 0, -2);


          /*echo '<pre>';

         print_r($course_categories);

          echo '</pre>';*/



         $result[] = array();

         /*$cond1 = array('university_to_degree_to_course.course_id' => '9', 'university_to_degree_to_course.degree_id' => '1');

         $result2 = $this->course_model->getuniversity_to_degree_to_course($cond1);

          echo '<pre>';

         print_r($result2);

          echo '</pre>';*/



         //array_push($result,$this->course_model->getuniversity_to_degree_to_course($cond1));

          /*foreach($courses as $substream){

          echo $substream;

          }*/

//          if($this->input->post('search')){

            $sortBy = $this->input->get('sort') ? $this->input->get('sort') : 'ranking_world';
            $this->outputData['sort_by'] = $sortBy;
            $criteria = 'DESC';
            $sortBy = "universities." . $sortBy;
            if($sortBy == 'universities.ranking_world')
            {
                $sortBy = "ISNULL(" . $sortBy . "), " . $sortBy . " ASC";
            }
            else
            {
                $sortBy = $sortBy . " " . $criteria;
            }

            $count = 0;
            $selectedCountry = $this->outputData['selected_country'];

              foreach($selectedCourseCategories as $substream){

                  foreach($selectedCollegeCategories as $mainstream){
                      foreach($selectedDegrees as $degree){
                          $details = $this->course_model->getCoursesByCategoryAndDegree($substream, $degree, $mainstream, $selectedCountry, $sortBy);

                          if($details){

                              foreach($details as $detail){


                         if (in_array($detail->country_name, $country_array) || in_array(strtoupper($detail->country_name), $country_array)){

                                    $result[$count]['id'] = $detail->university_id;
                                    $result[$count]['university'] = $detail->university_name;
                                    $result[$count]['name'] = $detail->university_name;
                                    $result[$count]['country_name'] = $detail->country_name;
                                    $result[$count]['degree'] = $detail->degree_name;
                                    $result[$count]['mainstream'] = $detail->mainstream;
                                    $result[$count]['substream'] = $detail->substream;
                                    $result[$count]['banner'] = $detail->banner;
                                    $result[$count]['total_students_international'] = $detail->total_students_international;
                                    $result[$count]['ranking_world'] = $detail->ranking_world;
                                    $result[$count]['ranking_usa'] = $detail->ranking_usa;
                                    $result[$count]['admission_success_rate'] = $detail->admission_success_rate;
                                    $result[$count]['placement_percentage'] = $detail->placement_percentage;
                                    $result[$count]['establishment_year'] = $detail->establishment_year;
                                    $result[$count]['total_students'] = $detail->total_students;
                                    $result[$count]['slug'] = $detail->slug;
                                    $result[$count]['university_view_count'] = $detail->university_view_count;
                                    $result[$count]['university_likes_count'] = $detail->university_likes_count;
                                    $result[$count]['university_fav_count'] = $detail->university_fav_count;
                                    $result[$count]['likes_link'] = 0;
                                    $result[$count]['favourites_link'] = '';

                                    $comparesession = $this->session->userdata('university');

                                    if($this->session->userdata('university')){

                                        foreach($comparesession as $compare){

                                            if($compare['university_id']==$detail->university_id){

                                                $result[$count]['details_link'] = '';

                                            }else{

                                                $result[$count]['details_link'] = base_url().$detail->slug;

                                            }

                                        }

                                    }else{

                                        $result[$count]['details_link'] = base_url().$detail->slug;

                                    }



                                  /*  $query = $this->user_model->checkFavourites(array('favourites.user_id'=>$usrdata['id'],'favourites.university_id'=>$detail->university_id,'favourites.degree'=>$leaddata['degree'],'favourites.course'=>$leaddata['coursecategories']));



                                    if(count($query)<1){

                                        //$result[$count]['favourites_link'] = base_url().'user/favourites/add/'.$detail->university_id;

                                        $result[$count]['favourites_link'] = '';
                                    }else{
                                        $result[$count]['fav_id'] = $query->id;
                                        $result[$count]['favourites_link'] = 1;

                                    }


                                    $conditionArray = [
                                      'user_id' => $usrdata['id'],
                                      'entity_id' => $detail->university_id,
                                      'entity_type' => 'universities'
                                    ];

                                    $likesQuery = $this->user_model->commonCheck($conditionArray,'likes_detail');

                                    if(count($likesQuery)<1){
                                      $result[$count]['likes_link'] = 0;
                                    } else {
                                      $result[$count]['likes_link'] = 1;
                                    }*/

                                    $result[$count]['details_link'] = base_url().$detail->slug;

                                    $result[$count]['compare_link'] = base_url().'university/compare/index/'.$detail->university_id;



                                    /*$cond2 = array('university_to_degree_to_course.university_id' => $detail->university_id, 'university_to_degree_to_course.degree_id' => $degree);

                                    $universityCourses = $this->course_model->getCoursesRelatedToUniversityDegree($cond2);
*/
                                    $universityCourses = $this->course_model->getCoursesByUniversityAndDegree($detail->university_id, $degree);
                                    $courseinfo = array();

                                    $j = 0;

                                    foreach($course_categories as $crs){

                                        foreach($universityCourses as $unicourse){

                                            if($unicourse->id==$crs){

                                                $courseinfo[$j]['id'] = $unicourse->id;

                                                $courseinfo[$j]['course_name'] = $unicourse->name;

                                                $j++;

                                            }

                                        }

                                    }

                                    $result[$count]['course']= $courseinfo;

                                    unset($courseinfo);

                                    /* echo '<pre>';

                                    print_r($universityCourses);

                                    echo '</pre>'; exit;*/



                                    // $result[$count]['course'] = $substream;



                                    $count++;

                                } //if country

                            }

                        }
                    }
                }
            }

//        }

        /* echo '<pre>';

         print_r($result);

          echo '</pre>';



          echo count($result);*/



         if($result[0]){

           $allQuery = $this->user_model->commonCheckAll($userId,'universities');

           foreach ($allQuery as $keyaq => $valueaq) {

             $keyr = array_search($valueaq['entity_id'], array_column($result, 'id'));
             if(is_numeric($keyr)){

               if($valueaq['s_type'] == 'likes'){
                 $result[$keyr]['likes_link'] = 1;
                 //echo "1<br>";
               }

               if($valueaq['s_type'] == 'favourites'){
                 $result[$keyr]['fav_id'] = $valueaq['id'];
                 $result[$keyr]['favourites_link'] = 1;
               }

             }

           }

          $duplicate=array();



            foreach ($result as $index=>$t) {

                if (isset($duplicate[$t["university"]])) {

                    unset($result[$index]);

                    continue;

                }

                $duplicate[$t["university"]]=true;

            }



        }   //print_r($result);



           /*echo '<pre>';

         print_r($result);

          echo '</pre>'; exit;*/


         $this->outputData['uni'] = array();
         $this->outputData['uni'] = $result;


     $courseListData = $this->user_model->getCourseData('ranking_world');

     foreach ($courseListData as $ldkey => $ldvalue) {

        $courseListData[$ldkey]['favourites_link'] = '';
        $courseListData[$ldkey]['likes_link'] = 0;
     }

     $allQueryc = $this->user_model->commonCheckAll($userId,'courses');

     foreach ($allQueryc as $keyaqc => $valueaqc) {

       $keyrc = array_search($valueaqc['entity_id'], array_column($courseListData, 'id'));
       if(is_numeric($keyrc)){

         if($valueaqc['s_type'] == 'likes'){
           $courseListData[$keyrc]['likes_link'] = 1;

         }

         if($valueaqc['s_type'] == 'favourites'){
           $courseListData[$keyrc]['fav_id'] = $valueaqc['id'];
           $courseListData[$keyrc]['favourites_link'] = 1;
         }

       }

     }

     $this->outputData['cou'] = $courseListData;

     $userWebinarListObj = $this->user_model->userWebinarList($userId);

     $this->outputData['webinar_list'] = $userWebinarListObj;

     $fairListObj = $this->webinar_model->fair_list($userId);
     $this->outputData['fair_list'] = $fairListObj;
     $this->outputData['fair_id'] = $fairListObj[0]['id'];
     $this->outputData['appliedFair'] = $fairListObj[0]['selected'] == 'YES' ? 'YES' : 'NO';

         /*

          header("Content-type:application/json");

         echo json_encode($this->outputData['uni']); exit;*/





        // foreach(array_unique($result) as $key){



        // $cond = array('user_master.id' => $key, 'user_master.status' => 1,'user_master.type'=>3);

        // $unidetails = $this->user_model->getUniversityByid($cond);



         //print_r($unidetails); exit;

        // $this->outputData['uni'][$k]['name'] = $unidetails->name;

        // $this->outputData['uni'][$k]['name'] = $unidetails->;



        // }







         /*echo '<pre>';

         print_r(array_unique($result));

          echo '</pre>';

          exit;*/



        /*$cond_course1 = array('course_master.status !=' => '5', 'course_master.type' => '1');

        //$cond_user2 = array('course_master.type !=' => '1');

        $this->outputData['courses'] = $this->course_model->getCourse($cond_course1);

        $this->outputData['course_types'] = $this->course_model->getAllCourseTypes();

        //$this->outputData['view'] = 'list';







         $this->outputData['business_management'] = $this->course_model->getSubCourses(array('course_to_subCourse.course_id' => 1 ,'course_master.status' => 1));

         $this->outputData['art'] = $this->course_model->getSubCourses(array('course_id' => 2 ,'course_master.status' => 1));

         $this->outputData['achitecture'] = $this->course_model->getSubCourses(array('course_id' => 3,'course_master.status' => 1));

         $this->outputData['engineering'] = $this->course_model->getSubCourses(array('course_id' => 4,'course_master.status' => 1));

         $this->outputData['medicnie'] = $this->course_model->getSubCourses(array('course_id' => 5,'course_master.status' => 1));

         $this->outputData['humanities'] = $this->course_model->getSubCourses(array('course_id' => 6,'course_master.status' => 1));

         $this->outputData['hospitality'] = $this->course_model->getSubCourses(array('course_id' => 7,'course_master.status' => 1));

         $this->outputData['science'] = $this->course_model->getSubCourses(array('course_id' => 8,'course_master.status' => 1));





        //exit;

        */



        //$this->load->view('templates/user/body',$this->outputData);



        $this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));

        $this->outputData['mainstreams'] = $this->course_model->getAllCollegeCategories();

        $this->outputData['degrees'] = $this->degree_model->getDegree(array('degree_master.status'=>1));


        $mySessions = $this->user_model->getMySessions($userId);
        $myFavourites = $this->user_model->getFavourites(array('favourites.user_id' => $userId));
        $myAppliedUniversities = $this->user_model->getAppliedUniversitiesByUser($userId);
        $appliedUniversities = array();
        if($myAppliedUniversities)
        {
            foreach($myAppliedUniversities as $index => $appliedUniversity)
            {
                if(!in_array($appliedUniversity['university_id'], $appliedUniversities))
                {
                    $appliedUniversities[] = $appliedUniversity['university_id'];
                    switch($appliedUniversity['status'])
                    {
                        case 'In Process':
                            $myAppliedUniversities[$index]['action'] = "Submit Application Form";
                            break;
                        case 'Submitted':
                            $myAppliedUniversities[$index]['action'] = "Awaiting Decision";
                            break;
                        case 'Admit Received':
                            $myAppliedUniversities[$index]['action'] = "Wait For I20";
                            break;
                        case 'I 20 Received':
                            $myAppliedUniversities[$index]['action'] = "Start Filling VISA Form";
                            break;
                        case 'VISA In Process':
                            $myAppliedUniversities[$index]['action'] = "Wait For VISA Date Booking";
                            break;
                        case 'VISA Date Booked':
                            $myAppliedUniversities[$index]['action'] = "Wait For VISA Approval";
                            break;
                        case 'VISA Approved':
                            $myAppliedUniversities[$index]['action'] = "Congratulation!! Pack Your Bags Now!!";
                            break;
                    }
                    $myAppliedUniversities[$index]['check_application'] = '<form method="get" action="/user/sessions/applications">
                                                                     <div>
                                                                          <input type="submit" class="btn btn-default" name="submit" value="Check All Colleges Application">
                                                                     </div>
                                                                     <input type="hidden" name="uid" value="' . $appliedUniversity['university_id'] . '">
                                                                     <input type="hidden" name="tab" value="application_form">
                                                                 </form>';
                }
                else
                {
                    unset($myAppliedUniversities[$index]);
                }
            }
        }
        if($myFavourites)
        {
            foreach($myFavourites as $index => $myFavourite)
            {
                if(!in_array($myFavourite['uni_id'], $appliedUniversities))
                {
                    $myFavourite['university_id'] = $myFavourite['uni_id'];
                    $myFavourite['status'] = 'Shortlisted';
                    //$myFavourite['confirmed_slot'] = '';
                    $myFavourite['action'] = '<form method="post" action="/university/details/connect">
                                                         <div>
                                                              <input type="submit" class="btn btn-default" name="submit" value="Book Session">
                                                              <input type="button" class="btn btn-default" name="apply_button" value="Apply" onclick="applyUniversity(' . $myFavourite['university_id'] . ', \'' . $myFavourite['name'] . '\', \'' . $myFavourite['email'] . '\')">
                                                         </div>
                                                         <input type="hidden" name="university_id" value="' . $myFavourite['university_id'] . '">
                                                         <input type="hidden" name="university_login_id" value="' . $myFavourite['university_login_id'] . '">
                                                         <input type="hidden" name="slug" value="' . $myFavourite['slug'] . '">
                                                     </form>';
                    $myFavourites[$index] = $myFavourite;
                }
                else
                {
                    unset($myFavourites[$index]);
                }
            }
        }
        /*echo '<pre>';
        echo $userId;
        print_r($mySessions);
        print_r($myFavourites);
        exit;*/
        if($mySessions)
        {
            $this->load->model(array('tokbox_model'));
            $traversedUniversities = array();
            foreach($mySessions as $index => $sessionArray)
            {
                if(!in_array($sessionArray['university_id'], $traversedUniversities))
                {
                    $key = array_search($sessionArray['university_id'], array_column($myFavourites, 'university_id'));
                    if($key !== FALSE)
                    {
                        array_splice($myFavourites, $key, 1);
                    }
                    $traversedUniversities[] = $sessionArray['university_id'];
                    $mySessions[$index]['action'] = '';

                    switch($sessionArray['status'])
                    {
                        case 'Attained':
                            if(!in_array($sessionArray['university_id'], $appliedUniversities))
                            {
                                $mySessions[$index]['action'] = '<form method="get" action="/university/details/application">
                                                                 <div>
                                                                      <input type="submit" class="btn btn-default" name="submit" value="Start Documentation">
                                                                 </div>
                                                                 <input type="hidden" name="uid" value="' . $sessionArray['university_id'] . '">
                                                             </form>
                                                             <form method="post" action="/university/details/connect">
                                                                 <div>
                                                                      <input type="submit" class="btn btn-default" name="submit" value="Connect Again">
                                                                 </div>
                                                                 <input type="hidden" name="university_id" value="' . $sessionArray['university_id'] . '">
                                                                 <input type="hidden" name="university_login_id" value="' . $sessionArray['university_login_id'] . '">
                                                                 <input type="hidden" name="slug" value="' . $sessionArray['slug'] . '">
                                                             </form>';
                            }
                            else
                            {
                                unset($mySessions[$index]);
                            }
                            break;
                        case 'Rejected':
                            $mySessions[$index]['action'] = '<form method="post" action="/university/details/connect">
                                                                 <div>
                                                                      <input type="submit" class="btn btn-default" name="submit" value="Connect Again">
                                                                 </div>
                                                                 <input type="hidden" name="university_id" value="' . $sessionArray['university_id'] . '">
                                                                 <input type="hidden" name="university_login_id" value="' . $sessionArray['university_login_id'] . '">
                                                                 <input type="hidden" name="slug" value="' . $sessionArray['slug'] . '">
                                                             </form>';
                            break;
                        case 'Missed':
                            $mySessions[$index]['action'] = '<form method="post" action="/university/details/connect">
                                                                 <div>
                                                                      <input type="submit" class="btn btn-default" name="submit" value="Connect Again">
                                                                 </div>
                                                                 <input type="hidden" name="university_id" value="' . $sessionArray['university_id'] . '">
                                                                 <input type="hidden" name="university_login_id" value="' . $sessionArray['university_login_id'] . '">
                                                                 <input type="hidden" name="slug" value="' . $sessionArray['slug'] . '">
                                                             </form>';
                            break;
                        case 'Requested':
                            $mySessions[$index]['action'] = "Wait For Slots";
                            $mySessions[$index]['confirmed_slot'] = $mySessions[$index]['date_created'];
                            break;
                        case 'Scheduled':
                            if($sessionArray['confirmed_slot'])
                            {
                                $mySessions[$index]['action'] = "Wait For Session";

                                $participants = array(
                                    'slot_id' => $sessionArray['id'],
                                    'user_id' => $sessionArray['user_id'],
                                    'university_id' => $sessionArray['university_id']
                                );
                                $json_participant = json_encode($participants);

                                $token = $this->tokbox_model->getTokenByParticipant($json_participant);
                                if($token && $token->token)
                                {
                                    $confirmed_slot = $sessionArray['confirmed_slot'];

                                    $meetingStartTime = strtotime($confirmed_slot);
                                    $currentTime = strtotime(date('Y-m-d H:i:s'));

                                    $meetingEndTime = $meetingStartTime + 1200000;
                                    if($meetingEndTime < $currentTime)
                                    {
                                        $expired = true;
                                    }
                                    else
                                    {
                                        $expired = false;
                                        $encodedTokboxId = base64_encode($token->id);
                                        $tokboxUrl = "<a href='/meeting/oneonone?id=" . $encodedTokboxId . "' target='_blank' style='color:#337ab7;'>Join Conference</a>";
                                    }

                                    /*if($token->expiry_time)
                                    {
                                        $expiryTime = $token->expiry_time;
                                        $currentTime = date('Y-m-d H:i:s');

                                        $time1 = strtotime($expiryTime);
                                        $time2 = strtotime($currentTime);

                                        if($expiryTime && $time2 < $time1)
                                        {
                                            $expired = false;
                                        }
                                    }*/
                                    if($expired)
                                    {
                                        $mySessions[$index]['action'] = "Session Expired";
                                    }
                                    else
                                    {
                                        $mySessions[$index]['action'] = $tokboxUrl;
                                    }
                                }
                            }
                            else
                            {
                                $mySessions[$index]['action'] = "<a href='/" . $sessionArray['slug'] . "' target='_blank' style='color:#337ab7;'>Pick A Slot</a>";
                                $mySessions[$index]['confirmed_slot'] = $mySessions[$index]['date_created'];
                            }
                            break;
                    }
                }
                else
                {
                    unset($mySessions[$index]);
                }
            }
        }
        $mySessions = array_merge($myFavourites, $mySessions);
        /*echo '<pre>';
        print_r($mySessions);
        exit;*/
        $this->outputData['my_sessions'] = $mySessions;
        $this->outputData['my_applied_universities'] = $myAppliedUniversities;
        /*echo '<pre>';
        print_r($this->outputData); exit;
        exit;*/
        $this->render_page('templates/search/universitylist',$this->outputData);



    }





    function search()

    {



    //echo $this->input->get('course'); die('++++');

    $this->load->model('user/auth_model');



  /*$this->auth_model->clearUserSession();



   $this->session->unset_userdata('leaduser');*/







         $this->load->helper('cookie_helper');

         $this->load->model('course/course_model');

         $this->load->model('user/user_model');

        // $leaddata=$this->session->userdata('leaduser');

         //if(empty($userdata)){  redirect('common/login'); }

         // echo $leaddata['countries']; exit;



         //echo $countryList = $leaddata['countries']; exit;

        // if($this->input->get('search')){

        /* $courses = array('0'=>$this->input->get('subcourse'));

         $degree = $this->input->get('degree');

         $country_array = array_map("strtoupper", array('0'=>$this->input->get('country')));  //array('0'=>$this->input->post('country'));*/



         $course_string = $this->input->get('subcourse');

         $degree = $this->input->get('degree');

         $courses = explode(",",$course_string);

         $country_array = array_map("strtoupper", explode(",",$this->input->get('country')));



         /*}else{



         $course_string = $leaddata['courses'];

         $degree = $leaddata['degree'];

         $courses = explode(",",$course_string);

         $country_array = array_map("strtoupper", explode(",",$leaddata['countries']));

         }*/







          /*echo '<pre>';

         print_r($courses);

          echo '</pre>';*/



         $result[] = array();

         /*$cond1 = array('university_to_degree_to_course.course_id' => '9', 'university_to_degree_to_course.degree_id' => '1');

         $result2 = $this->course_model->getuniversity_to_degree_to_course($cond1);

          echo '<pre>';

         print_r($result2);

          echo '</pre>';*/



         //array_push($result,$this->course_model->getuniversity_to_degree_to_course($cond1));

          /*foreach($courses as $substream){

          echo $substream;

          }*/

         $count = 0;

         foreach($courses as $substream){



         $cond = array('university_to_degree_to_course.course_id' => $substream, 'university_to_degree_to_course.degree_id' => $degree);

         $details = $this->course_model->getuniversity_to_degree_to_course($cond);

        //print_r($details);



          if($details){

             foreach($details as $detail){



            $uniinfo = $this->user_model->getUniversityByid(array('user_master.id'=>$detail->university_id));



            //print_r($uniinfo);





            //$sample_array = array("mark","steve","adam");







            //print_r($country_array);

            //exit;



            if (in_array($uniinfo->code, $country_array)){



             $result[$count]['id'] = $detail->university_id;

             $result[$count]['university'] = $detail->name;



             $cond2 = array('university_to_degree_to_course.university_id' => $detail->university_id, 'university_to_degree_to_course.degree_id' => $degree);

             $universityCourses = $this->course_model->getCoursesRelatedToUniversityDegree($cond2);

             $courseinfo = array();

             $j = 0;

             foreach($courses as $crs){



                 foreach($universityCourses as $unicourse){

                 if($unicourse->id==$crs){



                 $courseinfo[$j]['id'] = $unicourse->id;

                 $courseinfo[$j]['course_name'] = $unicourse->name;

                 $j++;

                 }

                 }





             }

             $result[$count]['course']= $courseinfo;

             unset($courseinfo);





             /* echo '<pre>';

         print_r($universityCourses);

          echo '</pre>'; exit;*/



            // $result[$count]['course'] = $substream;



             $count++;



              } //if country



            }



            }







         }

        /* echo '<pre>';

         print_r($result);

          echo '</pre>';



          echo count($result);*/



         if($result[0]){



          $duplicate=array();



            foreach ($result as $index=>$t) {

                if (isset($duplicate[$t["university"]])) {

                    unset($result[$index]);

                    continue;

                }

                $duplicate[$t["university"]]=true;

            }



        }   //print_r($result);



          /* echo '<pre>';

         print_r($result);

          echo '</pre>';*/ //exit;



         //$k = 0;

         $this->outputData['uni'] = array();



         $this->outputData['uni'] = $result;

         $data = array(

            'name'          =>  $this->input->get('name'),

            'email'         =>  $this->input->get('email'),

            'phone'         =>  $this->input->get('phone')

        );





     if($data)

     {$leadId = $this->user_model->insertLead($data);

     }

         header("Content-type:application/json");

         echo json_encode($this->outputData['uni']);





        // foreach(array_unique($result) as $key){



        // $cond = array('user_master.id' => $key, 'user_master.status' => 1,'user_master.type'=>3);

        // $unidetails = $this->user_model->getUniversityByid($cond);



         //print_r($unidetails); exit;

        // $this->outputData['uni'][$k]['name'] = $unidetails->name;

        // $this->outputData['uni'][$k]['name'] = $unidetails->;



        // }







         /*echo '<pre>';

         print_r(array_unique($result));

          echo '</pre>';

          exit;*/



        /*$cond_course1 = array('course_master.status !=' => '5', 'course_master.type' => '1');

        //$cond_user2 = array('course_master.type !=' => '1');

        $this->outputData['courses'] = $this->course_model->getCourse($cond_course1);

        $this->outputData['course_types'] = $this->course_model->getAllCourseTypes();

        //$this->outputData['view'] = 'list';







         $this->outputData['business_management'] = $this->course_model->getSubCourses(array('course_to_subCourse.course_id' => 1 ,'course_master.status' => 1));

         $this->outputData['art'] = $this->course_model->getSubCourses(array('course_id' => 2 ,'course_master.status' => 1));

         $this->outputData['achitecture'] = $this->course_model->getSubCourses(array('course_id' => 3,'course_master.status' => 1));

         $this->outputData['engineering'] = $this->course_model->getSubCourses(array('course_id' => 4,'course_master.status' => 1));

         $this->outputData['medicnie'] = $this->course_model->getSubCourses(array('course_id' => 5,'course_master.status' => 1));

         $this->outputData['humanities'] = $this->course_model->getSubCourses(array('course_id' => 6,'course_master.status' => 1));

         $this->outputData['hospitality'] = $this->course_model->getSubCourses(array('course_id' => 7,'course_master.status' => 1));

         $this->outputData['science'] = $this->course_model->getSubCourses(array('course_id' => 8,'course_master.status' => 1));





        //exit;

        */



        //$this->load->view('templates/user/body',$this->outputData);



        //$this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));

        //$this->outputData['mainstreams'] = $this->course_model->getCourse(array('course_master.status'=>1,'course_master.type'=>1));

        //$this->outputData['degrees'] = $this->degree_model->getDegree(array('degree_master.status'=>1));

       // $this->render_page('templates/search/universitylist',$this->outputData);



    }



    function test()

    {

         $this->load->helper('cookie_helper');

         $this->load->model('course/course_model');

        // $userdata=$this->session->userdata('user');

         //if(empty($userdata)){  redirect('common/login'); }



        $cond_user1 = array('course_master.status !=' => '5');

        //$cond_user2 = array('course_master.type !=' => '1');

        $this->outputData['courses'] = $this->course_model->getCourse($cond_user1);

        $this->outputData['course_types'] = $this->course_model->getAllCourseTypes();

        //$this->outputData['view'] = 'list';





        $this->load->view('templates/course/dynamic_pop',$this->outputData);

       // $this->render_page('templates/course/course',$this->outputData);



    }



     function form()

     {

         $this->load->helper('cookie_helper');

         $this->load->model('course/course_model');



         $userdata=$this->session->userdata('user');

         //print_r($userdata); exit;

         if(empty($userdata)){  redirect('common/login'); }



         $this->outputData['course_types'] = $this->course_model->getAllCourseTypes();



         if($this->uri->segment('4')){

         $conditions = array('course_master.id' => $this->uri->segment('4'));

         $course_details = $this->course_model->getCourseByid($conditions);

         //print_r($course_details); exit;



        $this->outputData['id']             = $course_details->id;

        $this->outputData['name']           = $course_details->name;

        $this->outputData['details']        = $course_details->details;

        $this->outputData['type']           = $course_details->type;

        $this->outputData['parent']         = $course_details->course_id;

        $this->outputData['status']         = $course_details->status;



        if($this->outputData['parent']){



        $cond1 = array('course_master.status' => '1','course_master.type' => '1');

        $this->outputData['parent_courses'] = $this->course_model->getCourse($cond1);

        // print_r($this->outputData['parent_courses']); exit;

        }



        }





        //$this->outputData['cities']   = $this->city_model->getCities();

        $this->render_page('templates/course/course_form',$this->outputData);



     }



     function create()

     {

       //die('+++++');

       $this->load->helper(array('form', 'url'));



       $this->load->library('form_validation');

       $this->load->model('user/user_model');

       $this->load->model('course/course_model');



       $userdata=$this->session->userdata('user');



        $data = array(

            'name'          =>  $this->input->post('coursename'),

            'details'       =>  $this->input->post('course_details'),

            'type'          =>  $this->input->post('coursetype'),

            'parent'        =>  $this->input->post('parent_course'),

            'createdate'    =>  date('Y-m-d H:i:s'),

            'createdby'     =>  $userdata['id'],

            'status'        =>  $this->input->post('status')



        );

        //print_r($data); exit;

         if($this->course_model->insertCourse($data)=='success')

         {

            $this->session->set_flashdata('flash_message', "Success: You have saved Course!");

            redirect('course/course');

         }

    }





     function edit()

     {

       $this->load->helper(array('form', 'url'));



       $this->load->library('form_validation');

       $this->load->model('user/user_model');

       $this->load->model('course/course_model');



      $userdata=$this->session->userdata('user');



        $data = array(

            'id'            =>  $this->uri->segment('4'),

            'name'          =>  $this->input->post('coursename'),

            'details'       =>  $this->input->post('course_details'),

            'type'          =>  $this->input->post('coursetype'),

            'parent'        =>  $this->input->post('parent_course'),

            'modifiedby'    =>  $userdata['id'],

            'status'        =>  $this->input->post('status')



        );



         if($this->course_model->editCourse($data)=='success'){



         $this->session->set_flashdata('flash_message', "You have modified Course!");

          redirect('course/course');

         }





    }





    function delete()

     {



       $this->load->helper(array('form', 'url'));

       $this->load->model('course/course_model');



        if($this->course_model->deleteCourse($this->uri->segment('4'))=='success'){



             $this->session->set_flashdata('flash_message', "Success: You have deleted Course!");

             redirect('course/course');

        }

    }





    function multidelete(){



        $this->load->helper(array('form', 'url'));



        $this->load->model('course/course_model');



        $array = $this->input->post('chk');

        foreach($array as $id):



        $this->course_model->deleteCourse($id);



        endforeach;

        $this->session->set_flashdata('flash_message', "You have deleted Course!");

         redirect('course/course');





    }



    function getAllParentCourses()

     {



       $this->load->helper(array('form', 'url'));

       $this->load->model('course/course_model');



       if($this->input->post('typeid')==2){

        $cond1 = array('course_master.status' => '1','course_master.type' => '1','course_master.id !=' => $this->input->post('id') );



       // print_r($this->course_model->getCourse($cond1)); //exit;

        foreach($this->course_model->getCourse($cond1) as $course){

        echo '<option value="'.$course->id.'">'.$course->name.'</option>';

        }



     }

    // print_r($this->course_model->getCourse($cond1));





    }





    function getCourseInfo()

     {



       $this->load->helper(array('form', 'url'));

       $this->load->model('course/course_model');

        $coursedata = array();

      $coursedata = json_decode(stripslashes($this->input->post('data')));



      //print_r($coursedata);



    $this->load->view('templates/user/body',$this->outputData);

    // print_r($this->course_model->getCourse($cond1));





    }



    function getSubcoursesList()

    {

      $this->load->model('course/course_model');

      //$this->outputData['test'] = json_decode(stripslashes($this->input->post('email')));

      //$condition = array('course_master.status' => '1','course_master.type' => '2','course_to_subCourse.course_id' => $this->input->post('id'));

      //$subcourses = $this->course_model->getSubCourses($condition);

      $courseCategories = $this->course_model->getCourseCategoriesByCollegeCategory($this->input->post('id'));


      // print_r($subcourses);

      echo "<option value='' disabled selected>Select Sub Categories</option>";

      foreach($courseCategories as $course){

      echo "<option value='".$course->id."'> ".strtoupper($course->display_name). "</option>";

      }



      //$this->load->view('templates/course/dynamic_pop',$this->outputData);

}

function getSubcoursesListArray() {

  $this->load->model('course/course_model');

  $idArray = $this->input->post('selected_category_id') ? $this->input->post('selected_category_id') : NULL;


  $courseCategories = $this->course_model->getCourseCategoriesByCollegeCategory($idArray);

  //echo "<option value='0' disabled selected>Select Sub Categories</option>";
  foreach($courseCategories as $course){
    echo "<option value='".$course->id."'> ".strtoupper($course->display_name). "</option>";
  }

}

    function getUniversityList() {

          $this->load->model('webinar/common_model');

          $countryId = $this->input->post('selected_country_id') ? $this->input->post('selected_country_id') : NULL;

          $universityListObj = $this->common_model->get(["country_id" => $countryId, "active" => "1"], "universities");

          if($universityListObj['data']){

            foreach($universityListObj['data'] as $universityKey => $universityValue){

              echo "<option value='".$universityValue['id']."'> ".strtoupper($universityValue['name']). "</option>";
            }

          }

    }

    function getPartneredUniversityList() {

          $this->load->model('webinar/common_model');

          $countryId = $this->input->post('selected_country_id') ? $this->input->post('selected_country_id') : NULL;

          $universityListObj = $this->common_model->get(["country_id" => $countryId, "active" => "1", "affiliated" => "1"], "universities");

          if($universityListObj['data']){

            foreach($universityListObj['data'] as $universityKey => $universityValue){

              echo "<option value='".$universityValue['id']."'> ".strtoupper($universityValue['name']). "</option>";
            }

          }

    }


}
?>
