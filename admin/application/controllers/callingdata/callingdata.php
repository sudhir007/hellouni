<?php

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

class Callingdata extends MY_Controller {

    public $outputData;		//Holds the output data for each view

	  public $loggedInUser;

	 function __construct() {

        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		    $this->load->library('form_validation');
        $this->load->model(array('webinar/webinar_model'));
        $this->load->library('csvimport');
   }



   function callingList(){

       $this->load->helper('cookie_helper');
       $this->load->model('callingdata/callingdata_model');
       $userdata=$this->session->userdata('user');

       if(empty($userdata)){  redirect('common/login'); }

       $facilitatorList = 1;

       $studentListObj = $this->callingdata_model->callingDataList($facilitatorList);

       $this->outputData['students'] = $studentListObj;
       $this->outputData['view'] = 'list';

       $this->render_page('templates/callingdata/calling_list',$this->outputData);

   }

   function ajax_manage_page_callingList()
      {
        $this->load->helper('cookie_helper');
        $this->load->model('callingdata/callingdata_model');
        $userdata=$this->session->userdata('user');

        if(empty($userdata)){  redirect('common/login'); }

        $facilitatorList = 1;
        $list = $this->callingdata_model->get_datatables_callingList($facilitatorList);

        $data = array();
        $no = $_POST['start'];

        foreach ($list as $person) {
          //echo "<pre>"; print_r($person); exit;
          
            $no++;
            $row = array();
            $row[] = anchor(base_url('/callingdata/student/updateform/'.$person->id),'<span>'.$person->name.'</span>');
            $row[] = $person->email;
            $row[] = $person->mobile;
            $row[] = $person->mobile1;
            $row[] = $person->lead_state;
            $row[] = $person->assign_to_name;
            $row[] = $person->source_name;
            $row[] = $person->date_created;
            $row[] = $person->next_followup_date;

            //add html for action
            /*$row[] = "<a href='/admin/student/application/newList/'.$person->user_id.'/'.$person->application_id><button class='btn btn-primary btn-circle'   style='float:left'  value="">Assign to me</button></a>";*/

            $row[] = anchor(base_url('/callingdata/comment/'.$person->id),'<span> Comment </span>');
            

            $data[] = $row;
        }
//echo "<pre>"; print_r($data); exit;
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->callingdata_model->count_all_callingList($facilitatorList),
                        "recordsFiltered" => $this->callingdata_model->count_filtered_callingList($facilitatorList),
                        "data" => $data,
                );
        //echo "<pre>"; print_r($output); exit;
        //output to json format
        echo json_encode($output);
 
      }

	function updateCallingDataForm() {

    $this->load->helper('cookie_helper');
    $this->load->model('callingdata/callingdata_model');
    $userdata=$this->session->userdata('user');

    if(empty($userdata)){  redirect('common/login'); }

    $callingId = $this->uri->segment('4');

    $studentInfoObj = $this->webinar_model->get(["id" => $callingId], "calling_data");

    $this->outputData['student_info'] = $studentInfoObj['data'][0];
    $this->outputData['calling_id'] = $callingId;

    $this->render_page('templates/callingdata/student_update',$this->outputData);

  }

  function updateCallingData()
  {
      $userdata = $this->session->userdata('user');
      if(empty($userdata))
      {
          redirect('common/login');
      }

      $postedData = $this->input->post();
      $postedData['updated_by'] = $userdata['id'];

      $entityId = $postedData['id'];

      $whereCondition = ["id"=>$entityId];

      $this->webinar_model->put($whereCondition, $postedData, "calling_data");

      redirect('/callingdata/student/updateform/' . $postedData['id']);

    }

//comment modules

function commentList(){

    $this->load->helper('cookie_helper');
    $this->load->model('callingdata/callingdata_model');
    $userdata=$this->session->userdata('user');

    if(empty($userdata)){  redirect('common/login'); }

    $callingId = $this->uri->segment('3');

    $commentListObj = $this->callingdata_model->commentsList($callingId);

    $commentTypes = array(
        'STUDENT' => 'Student',
        'UNIVERSITY' => 'University'
    );

    $leadStateList = array(
        'INTERESTED' => 'INTERESTED',
        'POTENTIAL' => 'POTENTIAL',
        'CONVERTED' => 'CONVERTED',
        'NOT_INTERESTED' => 'NOT_INTERESTED',
        'POTENTIAL_DECLINED' => 'POTENTIAL_DECLINED'
    );

    $callingInfoObj = $this->webinar_model->get(["id" => $callingId],'calling_data');

    $this->outputData['comments'] = $commentListObj;
    $this->outputData['comment_types'] = $commentTypes;
    $this->outputData['lead_state_list'] = $leadStateList;
    $this->outputData['calling_id'] = $callingId;
    $this->outputData['calling_data_info'] = $callingInfoObj['data'];

    $this->render_page('templates/callingdata/comments',$this->outputData);

}

function addComments()
{
    $userdata = $this->session->userdata('user');
    if(empty($userdata))
    {
        redirect('common/login');
    }

    $postedData = $this->input->post();
    $postedData['added_by'] = $userdata['id'];

    $entityId = $postedData['entity_id'];

    $whereCondition = ["id"=>$entityId];
    $updateData = ["lead_state"=> $postedData['lead_state'], "next_followup_date" => $postedData['followup_date']];

    $this->webinar_model->put($whereCondition, $updateData, "calling_data");

    unset($postedData['followup_date']);
    unset($postedData['lead_state']);

    $this->webinar_model->post($postedData,"calling_data_comments");

    redirect('/callingdata/comment/' . $postedData['entity_id']);

  }

//Source Module

  function sourceList(){

    $this->load->helper('cookie_helper');
    $this->load->model('callingdata/callingdata_model');
    $userdata=$this->session->userdata('user');

    if(empty($userdata)){  redirect('common/login'); }

    $userId = $userdata['id'];

    $sourceListObj = $this->callingdata_model->sourceList($userId);

    $this->outputData['sources'] = $sourceListObj['data'];
    $this->outputData['view'] = 'list';

    $this->render_page('templates/callingdata/data_source_list',$this->outputData);

    }

    function ajax_manage_page_sourceList()
      {
        $this->load->helper('cookie_helper');
        $this->load->model('callingdata/callingdata_model');
        $userdata=$this->session->userdata('user');

        if(empty($userdata)){  redirect('common/login'); }

        $userId = $userdata['id'];
        $list = $this->callingdata_model->get_datatables_sourceList($userId);

        $data = array();
        $no = $_POST['start'];

        foreach ($list as $person) {
          //echo "<pre>"; print_r($person); exit;
          
            $no++;
            $row = array();
            $row[] = $person->name;
            $row[] = $person->display_name;
            $row[] = $person->status;
            $row[] = $person->date_created;
            
            $data[] = $row;
        }
//echo "<pre>"; print_r($data); exit;
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->callingdata_model->count_all_sourceList($userId),
                        "recordsFiltered" => $this->callingdata_model->count_filtered_sourceList($userId),
                        "data" => $data,
                );
        //echo "<pre>"; print_r($output); exit;
        //output to json format
        echo json_encode($output);
 
      }

    function updateSourceDataForm() {

      $this->load->helper('cookie_helper');
      $this->load->model('callingdata/callingdata_model');
      $userdata=$this->session->userdata('user');

      if(empty($userdata)){  redirect('common/login'); }

      $sourceId = $this->uri->segment('4');

      $sourceInfoObj = $this->webinar_model->get(["id" => $sourceId], "data_source_name");

      $this->outputData['source_info'] = $sourceInfoObj['data'][0];
      $this->outputData['source_id'] = $sourceId;
      $this->outputData['type_of_form'] = "UPDATE_FORM";

      $this->render_page('templates/callingdata/source_update',$this->outputData);

    }

    function updateSourceData()
    {
        $userdata = $this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $postedData = $this->input->post();
        $postedData['updated_by'] = $userdata['id'];

        $entityId = $postedData['id'];

        $whereCondition = ["id"=>$entityId];

        $this->webinar_model->put($whereCondition, $postedData, "data_source_name");

        redirect('/callingdata/source/updateform/' . $postedData['id']);

      }

      function addSourceDataForm() {

        $this->load->helper('cookie_helper');
        $this->load->model('callingdata/callingdata_model');
        $userdata=$this->session->userdata('user');

        if(empty($userdata)){  redirect('common/login'); }

        $sourceInfo = [
          'id' => '0',
          'name' => '',
          'display_name' => '',
          'status' => '1'
        ];
        $this->outputData['source_info'] = $sourceInfo;
        $this->outputData['source_id'] = 0;
        $this->outputData['type_of_form'] = "ADD_FORM";

        $this->render_page('templates/callingdata/source_update',$this->outputData);

      }

      function addSourceData() {
          $userdata = $this->session->userdata('user');
          if(empty($userdata))
          {
              redirect('common/login');
          }

          $postedData = $this->input->post();

          $postedData['created_by'] = $userdata['id'];
          $postedData['updated_by'] = $userdata['id'];

          unset($postedData['id']);
          $this->webinar_model->post($postedData, "data_source_name");

          redirect('/callingdata/source/list/');

        }

    // upload callingdata modules

    function uploadForm(){

          $this->load->helper('cookie_helper');
          $this->load->model('callingdata/callingdata_model');
          $userdata=$this->session->userdata('user');

          if(empty($userdata)){  redirect('common/login'); }

          $userId = $userdata['id'];

          $sourceListObj = $this->callingdata_model->sourceList($userId);

          $this->outputData['source_list'] = $sourceListObj;

          $this->render_page('templates/callingdata/upload_calling_data',$this->outputData);

        }

        function uploadData() {

            $userdata = $this->session->userdata('user');
            if(empty($userdata))
            {
                redirect('common/login');
            }

          $postVariables = $this->input->post();
          $sourceId = isset($postVariables['source_id']) &&  $postVariables['source_id'] != "" ? $postVariables['source_id'] : 0;

          $config['upload_path']          = $_SERVER['DOCUMENT_ROOT'] . "/admin/uploads/";
					$config['allowed_types']        = '*';
					$config['max_size']             = 500000;


					$this->load->library('upload', $config);

          if ( ! $this->upload->do_upload('userfile'))
                {
                        $error = array('message' => $this->upload->display_errors());
                        var_dump($config);
                        var_dump($error);
                        exit();

			  } else  {

                $data = array('upload_data' => $this->upload->data());
                $file_path =  $data['upload_data']['full_path'];
                $errorData = [];
								$alreadyRegisteredStudents = [];
								$bulkUploadInsert['data'] = [];

                if ($this->csvimport->get_array($file_path)) {

						            $csv_array = $this->csvimport->get_array($file_path);
						            $bulkUploadInsert = array();

                          foreach ($csv_array as $row) {

                            $insertData = array(

                                  'name'=>$row['name'],
                                  'email'=>$row['email'],
                                  'mobile'=>$row['mobile'],
																	'mobile1'=>$row['mobile1'],
																	'city'=>$row['city'],
																	'intake'=>$row['intake'],
																	'gre_score'=>$row['gre_score'],
																	'database_city'=>$row['database_city'],
                                  'source_id'=>$sourceId,
																	'lead_state'=>'FRESH',
																	'created_by'=> $userdata['id'],
                                  'assigned_to'=> $userdata['id'],
                                  'assigned_by'=> $userdata['id']
                              );

										          $finalInsertData = array_filter($insertData);

                              $bulkUploadInsert = $this->webinar_model->post($finalInsertData, "calling_data");

                          }

                        }

          redirect('/callingdata/list');

        }

    }


}
