<?php
//require_once APPPATH . 'libraries/Mail/sMail.php';
class Visaseminar extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
    }

    function index($encodedUsername)
    {
        /*$userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }
        $username = $userdata['username'];*/
        $username = base64_decode($encodedUsername);

        $allRooms = [
            [
                'type' => 'Common Room',
                'counsellors' => 'All',
                'token_id' => 157
            ],
            [
                'type' => 'Landing Page',
                'counsellors' => 'All',
                'token_id' => 157
            ],
            [
                'type' => 'Country - USA',
                'counsellors' => 'Dr. Srivathsa Vaidya & Kavita M',
                'token_id' => 158
            ],
            [
                'type' => 'Country - USA',
                'counsellors' => 'Rahul Bhat, Pavni Gaurangi & Priyanka More',
                'token_id' => 159
            ],
            [
                'type' => 'Country - USA',
                'counsellors' => 'Yamnesh Patel & Amruta Udawant',
                'token_id' => 160
            ],
            [
                'type' => 'Country - USA',
                'counsellors' => 'Sanjana Sanzgiri-Bhagwat & Anil Kulkarni',
                'token_id' => 161
            ],
            [
                'type' => 'Country - USA',
                'counsellors' => 'Saloni Magre, Priyanka Mirkar & Mercy Selvam',
                'token_id' => 162
            ],
            [
                'type' => 'Country - USA',
                'counsellors' => 'Komal Mirkar, Alisha Chhabra & Shagufta Sayyed',
                'token_id' => 163
            ],
            [
                'type' => 'Country - Canada',
                'counsellors' => 'Sonali Palkar, Sanchan Jaiswal & Manasi Rayanade',
                'token_id' => 164
            ],
            [
                'type' => 'Country - Europe',
                'counsellors' => 'Akshay Bhandari & Sanhita Bhattacharya',
                'token_id' => 165
            ],
            [
                'type' => 'Country - Australia, NZ, UK, Ireland',
                'counsellors' => 'Ranjana Singh, Vaidehi Purohit & Shiji George',
                'token_id' => 166
            ],
            [
                'type' => 'Branch - Thane 1',
                'counsellors' => 'Ranbir Matharoo',
                'token_id' => 167
            ],
            [
                'type' => 'Branch - Thane 2',
                'counsellors' => 'Akshay Dhumal',
                'token_id' => 168
            ],
            [
                'type' => 'Branch - Borivalli 1',
                'counsellors' => 'Jenny Mednonca',
                'token_id' => 169
            ],
            [
                'type' => 'Branch - Borivalli 2',
                'counsellors' => 'Zainab B',
                'token_id' => 170
            ],
            [
                'type' => 'Branch - Santacruz 1',
                'counsellors' => 'Aman Kaundal',
                'token_id' => 171
            ],
            [
                'type' => 'Branch - Santacruz 2',
                'counsellors' => 'Mansi Bandre',
                'token_id' => 172
            ],
            [
                'type' => 'Branch - Pune',
                'counsellors' => 'Rakesh Baleshwar',
                'token_id' => 173
            ],
            [
                'type' => 'Forex Partner - Unimoni',
                'counsellors' => 'Darayus Minoo',
                'token_id' => 174
            ],
            [
                'type' => 'Forex Partner - FRR',
                'counsellors' => 'Dev Mehta',
                'token_id' => 175
            ],
            [
                'type' => 'Forex Partner - Axis Bank',
                'counsellors' => 'Rajesh Dubey',
                'token_id' => 176
            ],
            [
                'type' => 'Forex Partner - Thmoas Cook',
                'counsellors' => 'Amol Dhake',
                'token_id' => 177
            ]
        ];

        $base64Username = base64_encode($username);
        $username = str_replace("=", "", $base64Username);

        $this->outputData['rooms'] = $allRooms;
        $this->outputData['username'] = $username;
        $this->outputData['view'] = 'list';
        //$this->render_page('templates/meeting/visaseminar_list', $this->outputData);

        $this->load->view('templates/common/header', $this->outputData);

        //$this->load->view('templates/common/sidebar', $this->outputData);

        $this->load->view('templates/meeting/visaseminar_list', $this->outputData);

        $this->load->view('templates/common/footer', $this->outputData);

    }
}
