<?php

include_once APPPATH . 'mongo/autoload.php';

class Seminar extends CI_Controller
{
    private $_panelist_collection;
    private $_tech_support_collection;
    private $_seminar_students_collection;
    private $_queue_collection;

    private $_allRooms = [
        200 => 'BOB (Bank Of Baroda)',
        //201 => 'SBI (State Bank Of India)',
        202 => 'HDFC Credila',
        203 => 'Avanse',
        204 => 'Incred',
        205 => 'Auxilo',
        206 => 'ICICI',
        207 => 'Axis Bank',
        208 => 'MPower',
        209 => 'Prodigy',
        210 => 'US Bank'
    ];

    private $_allowedRooms = [
        'banit_sawhney' => [200, 202, 203, 204, 205, 206, 207, 208, 209, 210],
        'hiren_rathod' => [200, 202, 203, 204, 205, 206, 207, 208, 209, 210],
        'harkiran_sawhney' => [200, 202, 203, 204, 205, 206, 207, 208, 209, 210],
        'tanmay_kudyadi' => [200, 202, 203, 204, 205, 206, 207, 208, 209, 210],
        'nik_admin' => [200, 202, 203, 204, 205, 206, 207, 208, 209, 210],
        'sudhir_admin' => [200, 202, 203, 204, 205, 206, 207, 208, 209, 210],
        'deepika_admin' => [200, 202, 203, 204, 205, 206, 207, 208, 209, 210],
        'jenny_mednonca' => [200, 205],
        'manasi_bandre' => [207],
        'rakesh_baleshwar' => [206],
        'zainab_b' => [203],
        'ranbir_matharoo' => [209],
        'aman_kaundal' => [208],
        'akshay_dhumal' => [202, 204]
    ];

    private $_panelists = [
        'jenny_mednonca',
        'manasi_bandre',
        'rakesh_baleshwar',
        'zainab_b',
        'ranbir_matharoo',
        'aman_kaundal',
        'akshay_dhumal'
    ];

    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));

        $mongo_server = 'localhost';
        $mongo_port = '27017';
        $mongo_client = 'mongodb://' . $mongo_server . ':' . $mongo_port;
        $client = new MongoDB\Client($mongo_client);
        $mongo_db = $client->HelloUni;

        $this->_panelist_collection = $mongo_db->selectCollection('panelists');
        $this->_tech_support_collection = $mongo_db->selectCollection('tech_support');
        $this->_seminar_students_collection = $mongo_db->selectCollection('seminar_students');
        $this->_queue_collection = $mongo_db->selectCollection('queue');
    }

    function index($encodedUsername)
    {
        $username = base64_decode($encodedUsername);

        $allPanelists = [
            200 => [
                'type' => $this->_allRooms[200],
                'counsellors' => 'BOB - Shabaaz Shaikh, Jenny',
                'token_id' => 200
            ],
            /*201 => [
                'type' => $this->_allRooms[201],
                'counsellors' => 'SBI - Rakesh',
                'token_id' => 201
            ],*/
            202 => [
                'type' => $this->_allRooms[202],
                'counsellors' => 'HDFC Credila - Amit Premchandani, Amar Joshi, Akshay',
                'token_id' => 202
            ],
            203 => [
                'type' => $this->_allRooms[203],
                'counsellors' => 'Avanse - Ankit Agarwal, Vivek Singh, Zainab',
                'token_id' => 203
            ],
            204 => [
                'type' => $this->_allRooms[204],
                'counsellors' => 'Incred - Gautam Kotak, Shrikant Baheti, Akshay',
                'token_id' => 204
            ],
            205 => [
                'type' => $this->_allRooms[205],
                'counsellors' => 'Auxilo - Swapnil Suvarna, Sagar Sura, Jenny',
                'token_id' => 205
            ],
            206 => [
                'type' => $this->_allRooms[206],
                'counsellors' => 'ICICI - Nikhilesh Kada, Nikita Chokawala, Rakesh',
                'token_id' => 206
            ],
            207 => [
                'type' => $this->_allRooms[207],
                'counsellors' => 'Axis Bank - Neeraj Panwar, Prashant Singh, Manasi',
                'token_id' => 207
            ],
            208 => [
                'type' => $this->_allRooms[208],
                'counsellors' => 'MPower - Arbaaz Hashmi, Yuvish Singh, Aman',
                'token_id' => 208
            ],
            209 => [
                'type' => $this->_allRooms[209],
                'counsellors' => 'Prodigy - Sakshi Kumari, Ranbir',
                'token_id' => 209
            ],
            210 => [
                'type' => $this->_allRooms[210],
                'counsellors' => 'US Bank - Banit',
                'token_id' => 210
            ]
        ];

        /*$panelistCursor = $this->_panelist_collection->find();
        foreach($panelistCursor as $panelist)
        {
            $temp['type'] = $this->_allRooms[$panelist['tokbox_id']];
            $temp['counsellors'] = $panelist['name'];
            $temp['token_id'] = $panelist['tokbox_id'];

            if(isset($allPanelists[$panelist['tokbox_id']]))
            {
                $allPanelists[$panelist['tokbox_id']]['counsellors'] = $allPanelists[$panelist['tokbox_id']]['counsellors'] . ', ' . $panelist['name'];
            }
            else
            {
                $allPanelists[$panelist['tokbox_id']] = $temp;
            }
        }*/

        $this->outputData['rooms'] = $allPanelists;
        $this->outputData['allowedRooms'] = $this->_allowedRooms[$username];
        $this->outputData['decodedUsername'] = $username;
        $this->outputData['username'] = $encodedUsername;
        $this->outputData['panelists'] = $this->_panelists;
        //$this->outputData['panelists'] = json_encode($allPanelists);
        $this->outputData['view'] = 'list';

        $this->load->view('templates/common/header', $this->outputData);
        $this->load->view('templates/meeting/seminar_list', $this->outputData);
        $this->load->view('templates/common/footer', $this->outputData);

    }

    function help()
    {
        $helps = [];
        $helpCursor = $this->_tech_support_collection->find();
        foreach($helpCursor as $help)
        {
            $helps[] = $help;
        }
        echo json_encode($helps);
    }

    function queue()
    {
        $mongoQueueQuery['tokbox_id'] = 210;
	//$queueCount = $this->_queue_collection->count($mongoQueueQuery);
	$queueCount = 0;
        echo $queueCount;
    }

    function room()
    {
        $students = [];
        $studentCursor = $this->_seminar_students_collection->find();
        foreach($studentCursor as $student)
        {
            $students[] = $student;
        }

        krsort($students);

        $this->outputData['students'] = $students;
        $this->outputData['rooms'] = $this->_allRooms;
        $this->outputData['view'] = 'list';

        $this->load->view('templates/common/header', $this->outputData);
        $this->load->view('templates/meeting/seminar_student_list', $this->outputData);
        $this->load->view('templates/common/footer', $this->outputData);
    }

    function change_room()
    {
        $this->load->model('seminar_model');

        $studentId = $this->input->get('sid');
        $roomId = $this->input->get('rid');

        $mongoStudentQuery['user_id'] = $studentId;
        $studentDetail = $this->_seminar_students_collection->findOne($mongoStudentQuery);
        if($studentDetail)
        {
            $updatedData['tokbox_id'] = $roomId;
            $updatedData['room'] = $this->_allRooms[$roomId];
            $this->seminar_model->updateRoom($studentId, $updatedData);

            $this->_seminar_students_collection->deleteMany($mongoStudentQuery);
            $studentDetail['tokbox_id'] = $roomId;
            $studentDetail['room'] = $this->_allRooms[$roomId];
            $this->_seminar_students_collection->insertOne($studentDetail);
        }
    }
}
