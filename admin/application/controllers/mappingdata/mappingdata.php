<?php

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

class Mappingdata extends MY_Controller
{

  public $outputData;    //Holds the output data for each view

  public $loggedInUser;

  function __construct()
  {

    parent::__construct();
    $this->load->library('template');
    $this->lang->load('enduser/home', $this->config->item('language_code'));
    $this->load->library('form_validation');
    $this->load->model(array('webinar/webinar_model'));
    $this->load->library('csvimport');
  }

  function universityList()
  {
    $this->load->helper('cookie_helper');
    $this->load->model('mappingdata/mappingdata_model');
    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    //$facilitatorList = 1;

    $universityListObj = $this->mappingdata_model->getAllSuggestedUniversity();
    //echo "<pre>"; print_r($this->db->last_query()); exit;

    $this->outputData['university'] = $universityListObj;
    $this->outputData['view'] = 'list';

    $this->render_page('templates/mappingdata/universitymapping_list', $this->outputData);
  }

  function universityEdit()
  {
    $this->load->helper('cookie_helper');

    $this->load->model('mappingdata/mappingdata_model');

    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    if ($this->uri->segment('3')) {
      $getid = $this->uri->segment('3');
      //$conditions = "id='".$id."'";

      $this->db->select('*');
      $this->db->from('suggested_university');
      $this->db->where('id', $getid);
      $query = $this->db->get();
      $suggested_university_info = $query->row();

      $universityListObj = $this->mappingdata_model->getAllData("universities", "Active = 1");

      // $university_info = $this->mappingdata_model->GetData("suggested_university",$conditions);
      //print_r($this->db->last_query()); exit; 
      /*echo '<pre>';
     print_r($universityListObj[0]);
     echo '</pre>';
     exit();*/

      $this->outputData['id']      = $suggested_university_info->id;
      $this->outputData['mapped_id']      = $suggested_university_info->mapped_id;
      $this->outputData['university_name']      = $suggested_university_info->university_name;
      $this->outputData['student_name']      = $suggested_university_info->university_name;
      $this->outputData['status']    = $suggested_university_info->status;
      $this->outputData['universityListObj']    = $universityListObj;
    }
    $this->render_page('templates/mappingdata/universitymapping_form', $this->outputData);
  }

  function create()
  {
    $this->load->helper('cookie_helper');
    $this->load->model('user/user_model');
    $this->load->model('user/university_model');
    //$this->load->model('location/country_model');
    //$this->load->model('location/city_model');
    //$this->load->model('location/state_model');


    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }
    $total_unique_selling_points = 0;

    if ($this->uri->segment('4')) {
      $conditions = array('user_master.id' => $this->uri->segment('4'));
      $user_details = $this->user_model->getUniversityByid($conditions);

      //echo '<pre>'; print_r($user_details); echo '</pre>'; exit;

      $this->outputData['id']         = $user_details->id;
      $this->outputData['country']      = $user_details->country_id;
      $this->outputData['name']         = $user_details->name;
      $this->outputData['username']       = $user_details->username;
      $this->outputData['email']        = $user_details->email;
      $this->outputData['password']       = $user_details->password;
      $this->outputData['website']      = $user_details->website;
      $this->outputData['status']       = $user_details->status;
      $this->outputData['notification']     = $user_details->notification;
      $this->outputData['create']       = $user_details->create;
      $this->outputData['view']         = $user_details->view;
      $this->outputData['edit']         = $user_details->edit;
      $this->outputData['del']        = $user_details->del;
      $this->outputData['description']    = $user_details->description;
      $this->outputData['establishment_year'] = $user_details->establishment_year;
      $this->outputData['university_type']  = $user_details->university_type;
      $this->outputData['carnegie_accreditation'] = $user_details->carnegie_accreditation;
      $this->outputData['total_students']   = $user_details->total_students;
      $this->outputData['total_students_UG']  = $user_details->total_students_UG;
      $this->outputData['total_students_PG']  = $user_details->total_students_PG;
      $this->outputData['total_students_international'] = $user_details->total_students_international;
      $this->outputData['ranking_usa']    = $user_details->ranking_usa;
      $this->outputData['ranking_world']    = $user_details->ranking_world;
      $this->outputData['admission_success_rate'] = $user_details->admission_success_rate;
      $unique_selling_points = explode('--', $user_details->unique_selling_points);
      $all_unique_selling_points = [];
      if ($unique_selling_points) {
        foreach ($unique_selling_points as $unique_selling_point) {
          if ($unique_selling_point) {
            $all_unique_selling_points[] = $unique_selling_point;
          }
        }
      }
      $this->outputData['unique_selling_points']  = $all_unique_selling_points;
      $this->outputData['research_spending']  = $user_details->research_spending;
      $this->outputData['placement_percentage']   = $user_details->placement_percentage;
      $this->outputData['national_ranking_source']  = $user_details->national_ranking_source;
      $this->outputData['worldwide_ranking_source']   = $user_details->worldwide_ranking_source;
      $this->outputData['map_normal_view']  = $user_details->map_normal_view;
      $this->outputData['map_street_view']  = $user_details->map_street_view;
      $this->outputData['whatsapp_link']  = $user_details->whatsapp_link;
      $this->outputData['edit']   = true;
      $total_unique_selling_points = count($all_unique_selling_points);
    }

    $getid = $this->uri->segment('3');

    $this->db->select('*');
    $this->db->from('suggested_university');
    $this->db->where('id', $getid);
    $query = $this->db->get();
    $suggested_university_info = $query->row();
    // echo "<pre>";print_r($suggested_university_info); exit;

    $allCountries = $this->university_model->getAllCountries();
    $this->outputData['countries'] = $allCountries;
    $this->outputData['name'] = $suggested_university_info->university_name;
    $this->outputData['total_unique_selling_points'] = $total_unique_selling_points;
    $this->render_page('templates/user/university_form', $this->outputData);
  }

  function update()
  {
    $this->load->helper('cookie_helper');
    $this->load->model('mappingdata/mappingdata_model');
    $userdata = $this->session->userdata('user');
    //echo "<pre>";print_r($userdata['id']); exit;
    if (empty($userdata)) {
      redirect('common/login');
    }

    $id = $this->input->post('mapId');
    $tablename = "suggested_university";
    $data = array(
      'mapped_id' => $this->input->post('university'),
      'updated_by' => $userdata['id'],
      'status' => 1,
      'date_updated' => date("Y-m-d H:i:s"),
    );
    //$condition_up = "id='". $id ."'";    
    $this->mappingdata_model->updateData($tablename, $data, $id);
    //echo "<pre>"; print_r($this->db->last_query()); exit;
    $this->session->set_flashdata('message', '<div class="alert alert-block alert-success text-center" style="margin-bottom:0px;"><p>University mapping has been updated successfully<p></div>');

    //redirect('/callingdata/comment/' . $postedData['entity_id']);
    redirect(base_url('mappingdata/universityList'));
  }

  function courseList()
  {
    $this->load->helper('cookie_helper');
    $this->load->model('mappingdata/mappingdata_model');
    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    //$facilitatorList = 1;

    $courseListObj = $this->mappingdata_model->getAllCourseData();
    //echo "<pre>"; print_r($courseListObj); exit;

    $this->outputData['course'] = $courseListObj;
    $this->outputData['view'] = 'list';

    $this->render_page('templates/mappingdata/coursemapping_list', $this->outputData);
  }

  function courseEdit()
  {

    $this->load->helper('cookie_helper');

    $this->load->model('mappingdata/mappingdata_model');
    $this->load->model('user/course_model');

    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    if ($this->uri->segment('3')) {
      $getid = $this->uri->segment('3');

      $suggested_course_info = $this->mappingdata_model->getAllCourseDataById($getid);

      $courseListObj = $this->course_model->getAllCourses();
      //echo "<pre>"; print_r($suggested_course_info); exit;
      $this->outputData['id']      = $suggested_course_info->id;
      $this->outputData['mapped_id']      = $suggested_course_info->mapped_id;
      $this->outputData['course_name']      = $suggested_course_info->course_name;
      $this->outputData['student_name']      = $suggested_course_info->created_by_name;
      $this->outputData['status']    = $suggested_course_info->status;
      $this->outputData['courseListObj']    = $courseListObj;
    }
    $this->render_page('templates/mappingdata/coursemapping_form', $this->outputData);
  }

  function courseCreate()
  {
    $this->load->helper('cookie_helper');
    $this->load->model('user/department_model');
    $this->load->model('user/course_model');
    $this->load->model('filter_model');
    //$this->load->model('location/country_model');
    //$this->load->model('location/city_model');
    //$this->load->model('location/state_model');


    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }
    $this->outputData['sop_no_of_words'] = "none";
    $this->outputData['lor_required'] = "none";
    $this->outputData['lor_type'] = "none";

    $this->outputData['workPermit'] = 'none';
    $this->outputData['backlog'] = 'none';
    $this->outputData['applicationFee'] = 'none';

    if ($this->uri->segment('4')) {
      $courseData = $this->course_model->getCourseByIdNew($this->uri->segment('4'));
      $eligibility_gre = $courseData->eligibility_gre ? json_decode($courseData->eligibility_gre) : '';
      $backlogs = $courseData->backlogs ? json_decode($courseData->backlogs) : '';
      $workPermit = $courseData->work_permit ? json_decode($courseData->work_permit) : '';

      $this->outputData['id']         = $courseData->id;
      $this->outputData['name']         = $courseData->name;
      $this->outputData['department']         = $courseData->department_id;
      $this->outputData['category']         = $courseData->category_id;
      $this->outputData['degree']         = $courseData->degree_id;
      $this->outputData['duration']         = $courseData->duration;
      $this->outputData['deadline_link']        = $courseData->deadline_link;
      $this->outputData['admission_link']         = $courseData->admission_link;
      $this->outputData['program_link']         = $courseData->program_link;
      /*$this->outputData['deadline_date_spring']         = $courseData->deadline_date_spring;
        $this->outputData['deadline_date_fall']         = $courseData->deadline_date_fall;
    $this->outputData['intake']         = $courseData->intake;*/
      $this->outputData['total_fees']         = $courseData->total_fees;
      $this->outputData['contact_title']        = $courseData->contact_title;
      $this->outputData['contact_name']         = $courseData->contact_name;
      $this->outputData['contact_email']        = $courseData->contact_email;
      $this->outputData['eligibility_gpa']        = $courseData->eligibility_gpa;
      $this->outputData['gpa_scale']        = $courseData->gpa_scale;
      $this->outputData['eligibility_gre_quant']        = isset($eligibility_gre->quant) ? $eligibility_gre->quant : '';
      $this->outputData['eligibility_gre_verbal']         = isset($eligibility_gre->verbal) ? $eligibility_gre->verbal : '';
      $this->outputData['eligibility_toefl']        = $courseData->eligibility_toefl;
      $this->outputData['eligibility_ielts']        = $courseData->eligibility_ielts;
      $this->outputData['expected_salary']        = $courseData->expected_salary;
      $this->outputData['eligibility_work_experience']        = $courseData->eligibility_work_experience;
      $this->outputData['sop']        = $courseData->sop ? json_decode($courseData->sop) : '';
      $this->outputData['lor']        = $courseData->lor ? json_decode($courseData->lor) : '';

      $this->outputData['degree_substream']         = $courseData->degree_substream;
      $this->outputData['no_of_intake']         = $courseData->no_of_intake;
      $this->outputData['intake_months']        = $courseData->intake_months ? implode(',', json_decode($courseData->intake_months, true)) : '';
      $this->outputData['application_fee']        = $courseData->application_fee;
      $this->outputData['application_fee_amount']         = $courseData->application_fee_amount;
      $this->outputData['application_fee_waiver_imperial']        = $courseData->application_fee_waiver_imperial;
      $this->outputData['cost_of_living_year']        = $courseData->cost_of_living_year;
      $this->outputData['test_required']        = $courseData->test_required;
      $this->outputData['eligibility_gmat']         = $courseData->eligibility_gmat;
      $this->outputData['eligibility_sat']        = $courseData->eligibility_sat;
      $this->outputData['eligibility_english_proficiency']        = $courseData->eligibility_english_proficiency;
      $this->outputData['eligibility_pte']        = $courseData->eligibility_pte;
      $this->outputData['eligibility_duolingo']         = $courseData->eligibility_duolingo;
      $this->outputData['resume_required']        = $courseData->resume_required;
      $this->outputData['admission_requirements']         = $courseData->admission_requirements;
      $this->outputData['backlog_accepted']         = isset($backlogs->backlog_accepted) ? $backlogs->backlog_accepted : '';
      $this->outputData['number_of_backlogs']         = isset($backlogs->number_of_backlogs) ? $backlogs->number_of_backlogs : '';
      $this->outputData['drop_years']         = $courseData->drop_years;
      $this->outputData['scholarship_available']        = $courseData->scholarship_available;
      $this->outputData['with_15_years_education']        = $courseData->with_15_years_education;
      $this->outputData['wes_requirement']        = $courseData->wes_requirement;
      $this->outputData['wes_type']         = $courseData->wes_type;
      $this->outputData['wes_type_div'] = $this->outputData['wes_requirement'] == 'Yes' ? 'block' : 'none';

      $this->outputData['work_while_studying']        = $courseData->work_while_studying;
      $this->outputData['cooperate_internship_participation']         = $courseData->cooperate_internship_participation;
      $this->outputData['work_permit_needed']         = isset($workPermit->work_permit_needed) ? $workPermit->work_permit_needed : '';
      $this->outputData['no_of_months']         = isset($workPermit->no_of_months) ? $workPermit->no_of_months : '';
      $this->outputData['start_months']   = $courseData->start_months ? implode(',', json_decode($courseData->start_months, true)) : '';
      $this->outputData['decision_months']  = $courseData->decision_months ? implode(',', json_decode($courseData->decision_months, true)) : '';
      $this->outputData['conditional_admit']        = $courseData->conditional_admit;

      $this->outputData['sop_no_of_words'] = isset($this->outputData['sop']->sop_needed) && $this->outputData['sop']->sop_needed ? 'block' : 'none';
      $this->outputData['lor_required'] = isset($this->outputData['lor']->lor_needed) && $this->outputData['lor']->lor_needed ? 'block' : 'none';
      $this->outputData['lor_type'] = isset($this->outputData['lor']->lor_needed) && $this->outputData['lor']->lor_needed ? 'block' : 'none';
      $this->outputData['workPermit'] = $this->outputData['work_permit_needed'] == '1' ? 'block' : 'none';
      $this->outputData['backlog'] = $this->outputData['backlog_accepted'] == '1' ? 'block' : 'none';
      $this->outputData['applicationFee'] = $this->outputData['application_fee'] == 'Yes' ? 'block' : 'none';


      $this->outputData['tuition_waivers']        = isset($courseData->tuition_waivers) ? $courseData->tuition_waivers : '';
      $this->outputData['research_assistantships']        = isset($courseData->research_assistantships) ? $courseData->research_assistantships : '';
      $this->outputData['teaching_assistantships']        = isset($courseData->teaching_assistantships) ? $courseData->teaching_assistantships : '';
      $this->outputData['graduate_assistantships']        = isset($courseData->graduate_assistantships) ? $courseData->graduate_assistantships : '';
      $this->outputData['others_link']        = isset($courseData->others_link) ? $courseData->others_link : '';
      $this->outputData['transcripts']        = isset($courseData->transcripts) ? $courseData->transcripts : '';
      $this->outputData['financial_documentation']        = isset($courseData->financial_documentation) ? $courseData->financial_documentation : '';
      $this->outputData['test_score_waivers']         = isset($courseData->test_score_waivers) ? $courseData->test_score_waivers : '';
      $this->outputData['test_score_reporting']         = isset($courseData->test_score_reporting) ? $courseData->test_score_reporting : '';
      $this->outputData['gre_code']         = isset($courseData->gre_code) ? $courseData->gre_code : '';
      $this->outputData['tofel_code']         = isset($courseData->tofel_code) ? $courseData->tofel_code : '';
      $this->outputData['gmat_code']        = isset($courseData->gmat_code) ? $courseData->gmat_code : '';
      $this->outputData['sat_code']         = isset($courseData->sat_code) ? $courseData->sat_code : '';
      $this->outputData['pre_requisites']         = isset($courseData->pre_requisites) ? $courseData->pre_requisites : '';
      $this->outputData['pre_requisites_link']        = isset($courseData->pre_requisites_link) ? $courseData->pre_requisites_link : '';
      $this->outputData['supervisor_details_canada']        = isset($courseData->supervisor_details_canada) ? $courseData->supervisor_details_canada : '';


      $this->outputData['scholarship_detail_div'] = isset($courseData->scholarship_available) && $courseData->scholarship_available == "Yes" ? 'block' : 'none';
      $this->outputData['pre_requisites_div'] = isset($courseData->pre_requisites) && $courseData->pre_requisites == "Yes" ? 'block' : 'none';
      $this->outputData['test_score_reporting_div'] = isset($courseData->test_score_reporting) && $courseData->test_score_reporting == "Official" ? 'block' : 'none';


      $this->outputData['selected_university_id'] = $courseData->university_id;
      $this->outputData['selected_campus_id'] = $courseData->campus_id;
      $this->outputData['selected_college_id'] = $courseData->college_id;
      $this->outputData['selected_department_id'] = $courseData->department_id;
      $this->outputData['description'] = $courseData->description;

      $selectedUniversityId = $courseData->university_id ? $courseData->university_id : 0;
      $selectedCampusId = $courseData->campus_id ? $courseData->campus_id : 0;
      $selectedCollegeId = $courseData->college_id ? $courseData->college_id : 0;

      if ($selectedUniversityId) {
        $allCampusList = $this->filter_model->getAllCampusesByUniversityId($selectedUniversityId);
      }

      if ($selectedCampusId) {
        $allCollegeList = $this->filter_model->getAllCollegeByCampusId($selectedCampusId);
      }

      if ($selectedCollegeId) {
        $allDepartmentList = $this->filter_model->getAllDepartmentByCollegeId($selectedCollegeId);
      }

      $this->outputData['campus_list'] = $allCampusList;
      $this->outputData['college_list'] = $allCollegeList;
      $this->outputData['department_list'] = $allDepartmentList;
    }

    if ($userdata['university_id']) {
      $allUniversityList = $this->filter_model->getAllUniversityById($userdata['university_id']);
    } else {
      $allUniversityList = $this->filter_model->getAllUniversityById();
    }

    $allDegrees = $this->course_model->getAllDegrees();
    $allCategories = $this->course_model->getAllCategories();

    $getid = $this->uri->segment('3');

    $this->db->select('*');
    $this->db->from('suggested_course');
    $this->db->where('id', $getid);
    $query = $this->db->get();
    $suggested_course_info = $query->row();

    $this->outputData['name'] = $suggested_course_info->course_name;
    $this->outputData['degrees'] = $allDegrees;
    $this->outputData['categories'] = $allCategories;
    $this->outputData['university_list'] = $allUniversityList;
    $this->outputData['degree_substreams'] = ['Grade (1-8)', 'Grade (9-10)', 'High School (11th-12th)', 'UG Certificate (1 Year)', 'UG Diploma (2 Year)', 'UG Advanced Diploma (3 year)', 'UG Degree (3 Years)', 'UG Degree (4 years)', 'PG Diploma/Certificate', 'PG (Masters)', 'UG+PG (Accelerated) Degree', 'Doctoral Degree (PHd)', 'Foundation', 'Short Term Programs (Non Degree)', 'Pathway Programs', 'Twinning Programmes (UG)', 'Twinning Programmes (PG)'];
    $this->outputData['filter_level'] = $this->uri->segment('5') ? $this->uri->segment('5') : 0;

    $this->render_page('templates/user/course_form_new', $this->outputData);
  }

  function courseUpdate()
  {
    $this->load->helper('cookie_helper');
    $this->load->model('mappingdata/mappingdata_model');
    $userdata = $this->session->userdata('user');
    //echo "<pre>";print_r($userdata['id']); exit;
    if (empty($userdata)) {
      redirect('common/login');
    }

    $id = $this->input->post('mapId');
    $tablename = "suggested_course";
    $data = array(
      'mapped_id' => $this->input->post('university'),
      'updated_by' => $userdata['id'],
      'status' => 1,
      'date_updated' => date("Y-m-d H:i:s"),
    );
    //$condition_up = "id='". $id ."'";    
    $this->mappingdata_model->updateDataCourse($tablename, $data, $id);
    //echo "<pre>"; print_r($this->db->last_query()); exit;
    $this->session->set_flashdata('message', '<div class="alert alert-block alert-success text-center" style="margin-bottom:0px;"><p>University mapping has been updated successfully<p></div>');

    //redirect('/callingdata/comment/' . $postedData['entity_id']);
    redirect(base_url('mappingdata/courseList'));
  }

  function callingList()
  {

    $this->load->helper('cookie_helper');
    $this->load->model('callingdata/callingdata_model');
    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $facilitatorList = 1;

    $studentListObj = $this->callingdata_model->callingDataList($facilitatorList);

    $this->outputData['students'] = $studentListObj;
    $this->outputData['view'] = 'list';

    $this->render_page('templates/callingdata/calling_list', $this->outputData);
  }

  function ajax_manage_page_callingList()
  {
    $this->load->helper('cookie_helper');
    $this->load->model('callingdata/callingdata_model');
    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $facilitatorList = 1;
    $list = $this->callingdata_model->get_datatables_callingList($facilitatorList);

    $data = array();
    $no = $_POST['start'];

    foreach ($list as $person) {
      //echo "<pre>"; print_r($person); exit;

      $no++;
      $row = array();
      $row[] = anchor(base_url('/callingdata/student/updateform/' . $person->id), '<span>' . $person->name . '</span>');
      $row[] = $person->email;
      $row[] = $person->mobile;
      $row[] = $person->mobile1;
      $row[] = $person->lead_state;
      $row[] = $person->assign_to_name;
      $row[] = $person->source_name;
      $row[] = $person->date_created;
      $row[] = $person->next_followup_date;

      //add html for action
      /*$row[] = "<a href='/admin/student/application/newList/'.$person->user_id.'/'.$person->application_id><button class='btn btn-primary btn-circle'   style='float:left'  value="">Assign to me</button></a>";*/

      $row[] = anchor(base_url('/callingdata/comment/' . $person->id), '<span> Comment </span>');


      $data[] = $row;
    }
    //echo "<pre>"; print_r($data); exit;
    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->callingdata_model->count_all_callingList($facilitatorList),
      "recordsFiltered" => $this->callingdata_model->count_filtered_callingList($facilitatorList),
      "data" => $data,
    );
    //echo "<pre>"; print_r($output); exit;
    //output to json format
    echo json_encode($output);
  }

  function updateCallingDataForm()
  {

    $this->load->helper('cookie_helper');
    $this->load->model('callingdata/callingdata_model');
    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $callingId = $this->uri->segment('4');

    $studentInfoObj = $this->webinar_model->get(["id" => $callingId], "calling_data");

    $this->outputData['student_info'] = $studentInfoObj['data'][0];
    $this->outputData['calling_id'] = $callingId;

    $this->render_page('templates/callingdata/student_update', $this->outputData);
  }

  function updateCallingData()
  {
    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    $postedData = $this->input->post();
    $postedData['updated_by'] = $userdata['id'];

    $entityId = $postedData['id'];

    $whereCondition = ["id" => $entityId];

    $this->webinar_model->put($whereCondition, $postedData, "calling_data");

    redirect('/callingdata/student/updateform/' . $postedData['id']);
  }

  //comment modules

  function commentList()
  {

    $this->load->helper('cookie_helper');
    $this->load->model('callingdata/callingdata_model');
    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $callingId = $this->uri->segment('3');

    $commentListObj = $this->callingdata_model->commentsList($callingId);

    $commentTypes = array(
      'STUDENT' => 'Student',
      'UNIVERSITY' => 'University'
    );

    $leadStateList = array(
      'INTERESTED' => 'INTERESTED',
      'POTENTIAL' => 'POTENTIAL',
      'CONVERTED' => 'CONVERTED',
      'NOT_INTERESTED' => 'NOT_INTERESTED',
      'POTENTIAL_DECLINED' => 'POTENTIAL_DECLINED'
    );

    $callingInfoObj = $this->webinar_model->get(["id" => $callingId], 'calling_data');

    $this->outputData['comments'] = $commentListObj;
    $this->outputData['comment_types'] = $commentTypes;
    $this->outputData['lead_state_list'] = $leadStateList;
    $this->outputData['calling_id'] = $callingId;
    $this->outputData['calling_data_info'] = $callingInfoObj['data'];

    $this->render_page('templates/callingdata/comments', $this->outputData);
  }

  function addComments()
  {
    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    $postedData = $this->input->post();
    $postedData['added_by'] = $userdata['id'];

    $entityId = $postedData['entity_id'];

    $whereCondition = ["id" => $entityId];
    $updateData = ["lead_state" => $postedData['lead_state'], "next_followup_date" => $postedData['followup_date']];

    $this->webinar_model->put($whereCondition, $updateData, "calling_data");

    unset($postedData['followup_date']);
    unset($postedData['lead_state']);

    $this->webinar_model->post($postedData, "calling_data_comments");

    redirect('/callingdata/comment/' . $postedData['entity_id']);
  }

  //Source Module

  function sourceList()
  {

    $this->load->helper('cookie_helper');
    $this->load->model('callingdata/callingdata_model');
    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $userId = $userdata['id'];

    $sourceListObj = $this->callingdata_model->sourceList($userId);

    $this->outputData['sources'] = $sourceListObj['data'];
    $this->outputData['view'] = 'list';

    $this->render_page('templates/callingdata/data_source_list', $this->outputData);
  }

  function ajax_manage_page_sourceList()
  {
    $this->load->helper('cookie_helper');
    $this->load->model('callingdata/callingdata_model');
    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $userId = $userdata['id'];
    $list = $this->callingdata_model->get_datatables_sourceList($userId);

    $data = array();
    $no = $_POST['start'];

    foreach ($list as $person) {
      //echo "<pre>"; print_r($person); exit;

      $no++;
      $row = array();
      $row[] = $person->name;
      $row[] = $person->display_name;
      $row[] = $person->status;
      $row[] = $person->date_created;

      $data[] = $row;
    }
    //echo "<pre>"; print_r($data); exit;
    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->callingdata_model->count_all_sourceList($userId),
      "recordsFiltered" => $this->callingdata_model->count_filtered_sourceList($userId),
      "data" => $data,
    );
    //echo "<pre>"; print_r($output); exit;
    //output to json format
    echo json_encode($output);
  }

  function updateSourceDataForm()
  {

    $this->load->helper('cookie_helper');
    $this->load->model('callingdata/callingdata_model');
    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $sourceId = $this->uri->segment('4');

    $sourceInfoObj = $this->webinar_model->get(["id" => $sourceId], "data_source_name");

    $this->outputData['source_info'] = $sourceInfoObj['data'][0];
    $this->outputData['source_id'] = $sourceId;
    $this->outputData['type_of_form'] = "UPDATE_FORM";

    $this->render_page('templates/callingdata/source_update', $this->outputData);
  }

  function updateSourceData()
  {
    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    $postedData = $this->input->post();
    $postedData['updated_by'] = $userdata['id'];

    $entityId = $postedData['id'];

    $whereCondition = ["id" => $entityId];

    $this->webinar_model->put($whereCondition, $postedData, "data_source_name");

    redirect('/callingdata/source/updateform/' . $postedData['id']);
  }

  function addSourceDataForm()
  {

    $this->load->helper('cookie_helper');
    $this->load->model('callingdata/callingdata_model');
    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $sourceInfo = [
      'id' => '0',
      'name' => '',
      'display_name' => '',
      'status' => '1'
    ];
    $this->outputData['source_info'] = $sourceInfo;
    $this->outputData['source_id'] = 0;
    $this->outputData['type_of_form'] = "ADD_FORM";

    $this->render_page('templates/callingdata/source_update', $this->outputData);
  }

  function addSourceData()
  {
    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    $postedData = $this->input->post();

    $postedData['created_by'] = $userdata['id'];
    $postedData['updated_by'] = $userdata['id'];

    unset($postedData['id']);
    $this->webinar_model->post($postedData, "data_source_name");

    redirect('/callingdata/source/list/');
  }

  // upload callingdata modules

  function uploadForm()
  {

    $this->load->helper('cookie_helper');
    $this->load->model('callingdata/callingdata_model');
    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $userId = $userdata['id'];

    $sourceListObj = $this->callingdata_model->sourceList($userId);

    $this->outputData['source_list'] = $sourceListObj;

    $this->render_page('templates/callingdata/upload_calling_data', $this->outputData);
  }

  function uploadData()
  {

    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    $postVariables = $this->input->post();
    $sourceId = isset($postVariables['source_id']) &&  $postVariables['source_id'] != "" ? $postVariables['source_id'] : 0;

    $config['upload_path']          = $_SERVER['DOCUMENT_ROOT'] . "/admin/uploads/";
    $config['allowed_types']        = '*';
    $config['max_size']             = 500000;


    $this->load->library('upload', $config);

    if (! $this->upload->do_upload('userfile')) {
      $error = array('message' => $this->upload->display_errors());
      var_dump($config);
      var_dump($error);
      exit();
    } else {

      $data = array('upload_data' => $this->upload->data());
      $file_path =  $data['upload_data']['full_path'];
      $errorData = [];
      $alreadyRegisteredStudents = [];
      $bulkUploadInsert['data'] = [];

      if ($this->csvimport->get_array($file_path)) {

        $csv_array = $this->csvimport->get_array($file_path);
        $bulkUploadInsert = array();

        foreach ($csv_array as $row) {

          $insertData = array(

            'name' => $row['name'],
            'email' => $row['email'],
            'mobile' => $row['mobile'],
            'mobile1' => $row['mobile1'],
            'city' => $row['city'],
            'intake' => $row['intake'],
            'gre_score' => $row['gre_score'],
            'database_city' => $row['database_city'],
            'source_id' => $sourceId,
            'lead_state' => 'FRESH',
            'created_by' => $userdata['id'],
            'assigned_to' => $userdata['id'],
            'assigned_by' => $userdata['id']
          );

          $finalInsertData = array_filter($insertData);

          $bulkUploadInsert = $this->webinar_model->post($finalInsertData, "calling_data");
        }
      }

      redirect('/callingdata/list');
    }
  }
}
