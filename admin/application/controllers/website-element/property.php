<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Property extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

	
	function index()
	{   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('website-element/property_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }	
		 $cond1 = array('property_master.status_id' => '1');
		 $cond2 = array('property_master.status_id' => '2');  	
        $this->outputData['properties'] = $this->property_model->getProperties($cond1,$cond2);
        $this->render_page('templates/website-element/property',$this->outputData);
	
} 
	
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('website-element/property_model');
		 $this->load->model('website-element/place_model');
		 $this->load->model('website-element/amenity_model');
		 $this->load->model('website-element/seo_model');
		 $this->load->model('location/country_model');
		 $this->load->model('location/state_model');
		 $this->load->model('location/city_model');
		 $this->load->model('plugins/gallery_model');
		 
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }		
		 
		 if($this->uri->segment('4')){
		 $conditions = array(
	 				  'property_master.id' => $this->uri->segment('4')
	 					);
		 $prop_in = $this->property_model->getPropertyByid($conditions);
		
		 
		$this->outputData['id'] 			= $prop_in->id;
		$this->outputData['name'] 			= $prop_in->name;
		$this->outputData['title'] 			= $prop_in->title;
		$this->outputData['description']	= $prop_in->description;
		$this->outputData['addressline1'] 	= $prop_in->addressline1;
		$this->outputData['postcode'] 		= $prop_in->postcode;
		$this->outputData['latitude'] 		= $prop_in->latitude;
		$this->outputData['longitude'] 		= $prop_in->longitude;
		$this->outputData['country_id'] 	= $prop_in->country_id;
		$this->outputData['city_id'] 		= $prop_in->city_id;
		$this->outputData['state_id'] 		= $prop_in->stateid;
		$this->outputData['email'] 			= $prop_in->email;
		
		$this->outputData['fileid'] 		= $prop_in->fileid;
		$this->outputData['file_name'] 		= $prop_in->file_name;
		$this->outputData['file_type'] 		= $prop_in->file_type;
		$this->outputData['path'] 			= $prop_in->path;
		$this->outputData['file_size'] 		= $prop_in->file_size;
	
		$this->outputData['status'] 		= $prop_in->status;
		$this->outputData['virtual_tour'] 	= $prop_in->virtual_tour;
		
		$this->outputData['states'] = $this->state_model->getStatesByCountry($prop_in->country_id);
		
		// Seo details
		$seomaster_details = $this->seo_model->getSeoById($prop_in->seo_id);
		
		
		$this->outputData['meta_tag'] 			=  $seomaster_details->meta_tag;
		$this->outputData['meta_description'] 	=  $seomaster_details->meta_description;
		
		// Phone details
		$phone = array(
	 				  'infra_contact_master.id' => $prop_in->telephone,
					  'infra_contact_master.contact_type' =>'3'
	 					);
		$contact_master_phone = $this->property_model->getContactmaster_info($phone);
	    
		foreach($contact_master_phone as $contact_phone):
		$this->outputData['telephone'] 	= $contact_phone->value;
		endforeach;
		
		// Fax details
		$fax = array(
	 				  'infra_contact_master.id' => $prop_in->fax,
					  'infra_contact_master.contact_type' =>'2'
	 					);
		$contact_master_fax = $this->property_model->getContactmaster_info($fax);
		
		foreach($contact_master_fax as $contact_fax):
		$this->outputData['fax'] 	= $contact_fax->value;
		endforeach;
		
		// Tollfree details
		$tollfree_info = array(
	 				  'infra_contact_master.id' => $prop_in->tollfree,
					  'infra_contact_master.contact_type' =>'4'
	 					);
		$contact_master_tollfree = $this->property_model->getContactmaster_info($tollfree_info);
		
		foreach($contact_master_tollfree as $contact_tollfree):
		$this->outputData['tollfree'] 	= $contact_tollfree->value;
		endforeach;
		
		// Sort order data
		$this->outputData['sort_order'] 	= $prop_in->sort_order;
		
		// Photo gallery data
		$photo_gallery = array(
	 				 'property_id' => $prop_in->id,
					 'type' 		=>'photo'
	 				);
		$this->outputData['selected_photo']			=	$this->gallery_model->getGallery_By_Property($photo_gallery);
		
		// Video gallery data
		$video_gallery = array(
	 				 'property_id' => $prop_in->id,
					 'type' 		=>'video'
	 				);
		$this->outputData['selected_video']			=	$this->gallery_model->getGallery_By_Property($video_gallery);
		
		
		// Selected Places data
		$property_dataarray = array(
	 				 'property_id' => $prop_in->id
					);
		
		$this->outputData['selected_places']			=	$this->place_model->getPlaces_By_Property($property_dataarray);
		
		// Selected Amenities data
		$amenity_data 							= array('amenity_to_property.property_id' =>$prop_in->id);
		$this->outputData['selected_amenities']	=	$this->amenity_model->getAmenities_from_amenity_to_property($amenity_data);
		
		
		 }
		 
		$cond2 = array('gallery_master.status' => '1'); 
		$this->outputData['photos']		=	$this->gallery_model->getPhotoGallaries($cond2,$array=array());
		$this->outputData['videos']		=	$this->gallery_model->getVideoGallaries($cond2,$array=array());
		
		$cond2 = array('gallery_master.status' => '1'); 
		$this->outputData['places']		=	$this->place_model->getPlaces();
		
		$conditions = array('amenities_master.for' =>'property','amenities_master.status' =>'1');
		$this->outputData['amenities']	=	$this->amenity_model->getAmenities_by_Attribute($conditions);
		
		
		$cond_country1 = array('infra_country_master.status' => '1'); 
		$this->outputData['countries'] = $this->country_model->getCountries($cond_country1,$cond_country2=array());
		
		$cond_state1 = array('infra_state_master.status' => '1'); 
		//$cond_state2 = array('infra_state_master.status' => '2'); 	
		if($this->uri->segment('4')==''){	
        $this->outputData['states'] = $this->state_model->getStates($cond_state1,$cond_state2=array());
		}
		$cond_city1 = array('infra_city_master.status' => '1'); 
		//$cond_city2 = array('infra_city_master.status' => '2'); 		
        $this->outputData['cities'] = $this->city_model->getCities($cond_city1,$cond_city2=array());
		$this->render_page('templates/website-element/property_form',$this->outputData);
	
     } 
	 
	 
	 
	 function create()
	 {   
	   
	    $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/property_model'); 
		$this->load->model('administrator/common_model');
		$this->load->model('location/country_model');
		$this->load->model('location/city_model');
				
		// Form Validation
		$this->form_validation->set_rules('property_name', 'Property Name', 'required|trim|xss_clean');
		$this->form_validation->set_rules('property_country', 'Country', 'required|greater_than[0]');
		$this->form_validation->set_rules('property_city', 'City', 'required|greater_than[0]');
		$this->form_validation->set_rules('property_address', 'Address', 'required|trim|xss_clean');
		$this->form_validation->set_rules('property_postcode', 'Postcode', 'required|trim|xss_clean');
		

		if ($this->form_validation->run() == FALSE)
		{
			
			  $this->render_page('templates/website-element/property_form');
		}
		else
		{
			$logo_info = $this->common_model->getFileinfo($this->input->post('property_logo'));
	 
	  		$data = array(
     		'title' 			=>	$this->input->post('property_title'),
			'name' 				=>	$this->input->post('property_name'),
     		'description'		=>	$this->input->post('property_description'),
     		'sort_order' 		=>	$this->input->post('property_sort_order'),
	 		'status' 			=>	$this->input->post('property_status'),
	 		'country' 			=>	$this->input->post('property_country'),
	 		'city'				=>	$this->input->post('property_city'),
	 		'address' 			=>	$this->input->post('property_address'),
	 		'postcode' 			=>	$this->input->post('property_postcode'),
	 		'email' 			=>	$this->input->post('property_email'),
	 		'telephone' 		=>	$this->input->post('property_telephone'),
	 		'tollfree' 			=>	$this->input->post('property_tollfree'),
	 		'fax' 				=>	$this->input->post('property_fax'),
	 		'logo_path' 		=>	$this->input->post('property_logo'),
	 		'logo_name' 		=>	$logo_info['logo_name'],
	 		'logo_type' 		=>	$logo_info['logo_type'],
	 		'logo_size' 		=>	$logo_info['logo_size'],
	 		'latitude' 			=>	$this->input->post('property_lat'),
			'longitude' 		=>	$this->input->post('property_long'),
	 		'photo' 			=>	$this->input->post('photo_gallery'),
	 		'video' 			=>	$this->input->post('video_gallery'),
	 		'near_place' 		=>	$this->input->post('near_place'),
	 		'amenity' 			=>	$this->input->post('amenities'),
	 		'virtual_tour' 		=>	$this->input->post('property_virtual_tour'),
			'meta_tag'			=>  $this->input->post('property_meta_tag'),
			'meta_description'	=>  $this->input->post('property_meta_description')
	 		);
	 
	 		if($this->property_model->insertProperty($data)=='success'){
	 
	 		$this->session->set_flashdata('flash_message', "Success: You have saved Property!");
	  		redirect('website-element/property/index');
	 		 }
	 
	 
		}
	}
	 
	 
	 function edit_property()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/property_model');
		$this->load->model('administrator/common_model');
		$this->form_validation->set_rules('property_name', 'Propertyname', 'required|trim|xss_clean');
		$this->form_validation->set_rules('property_description', 'Property_Description', 'required|trim|xss_clean');
	
		if ($this->form_validation->run() == FALSE)
		{
			
			  $this->render_page('templates/website-element/property_form');
		}
		else
		{
			  $logo_info = $this->common_model->getFileinfo($this->input->post('property_logo'));
			  
			 $data = array(
             'property_id' 			=> $this->uri->segment('4'),
			 'title' 				=>	$this->input->post('property_title'),
	         'property_name' 		=> $this->input->post('property_name'),
             'property_description' => $this->input->post('property_description'),
			 'property_status' 		=> $this->input->post('property_status'),
             'property_sort_order' 	=> $this->input->post('property_sort_order'),
			 'property_country' 	=> $this->input->post('property_country'),
			 'property_city' 		=> $this->input->post('property_city'),
			 
			 'property_address' 	=> $this->input->post('property_address'),
			 'property_postcode' 	=> $this->input->post('property_postcode'),
			 'property_email' 		=> $this->input->post('property_email'),
			 'property_telephone' 	=> $this->input->post('property_telephone'),
			 'property_tollfree' 	=> $this->input->post('property_tollfree'),
			 'property_fax' 		=> $this->input->post('property_fax'),
			 'property_lat' 		=> $this->input->post('property_lat'),
			 'property_long' 		=> $this->input->post('property_long'),
			 'property_logo' 		=> $this->input->post('file_id'),
			 'logo_path' 			=> $this->input->post('property_logo'),
	 		 'logo_name' 			=> $logo_info['logo_name'],
	 		 'logo_type' 			=> $logo_info['logo_type'],
	 		 'logo_size' 			=> $logo_info['logo_size'],
			 'latitude' 			=> $this->input->post('property_lat'),
	 		 'longitude' 			=> $this->input->post('property_long'),
			 'photo' 				=> $this->input->post('photo_gallery'),
	 		 'video' 				=> $this->input->post('video_gallery'),
			 'near_place' 			=> $this->input->post('near_place'),
			 'virtual_tour' 		=> $this->input->post('property_virtual_tour'),
			 'meta_tag'				=>  $this->input->post('property_meta_tag'),
			 'meta_description'		=>  $this->input->post('property_meta_description')
	            );
				
			if($this->property_model->editProperty($data)=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have modified Property!");
	         redirect('website-element/property/index');
	         }
	      }
	   }
    
	
	function delete_property()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/property_model');
		
		if($this->property_model->deleteProperty($this->uri->segment('4'))=='success'){
	       $this->session->set_flashdata('flash_message', "Success: You have deleted Property!");
	       redirect('website-element/property/index');
	       }
	 }
	
	
	function delete_mult_properties(){
	
	    $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/property_model');
		
		$array = $this->input->post('chk');
		foreach($array as $id){
		 $this->property_model->deleteProperty($id);
		}
		$this->session->set_flashdata('flash_message', "Success: You have deleted Brands!");
		redirect('website-element/property/index');
	
	
	}
	 
	 
	 
	 

	

}//End  Home Class





?>