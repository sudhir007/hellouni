<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Brand extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

	
	function index()
	{   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('administrator/settings_model');
		 $this->load->model('website-element/brand_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }	
		 
		 $cond_brand1 = array('brand_master.status' => '1'); 
		 $cond_brand2 = array('brand_master.status' => '2'); 	
		 $this->outputData['brands'] = $this->brand_model->getBrands($cond_brand1,$cond_brand2);
		
		 $this->render_page('templates/website-element/brand',$this->outputData);
	
     } 
	
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('administrator/settings_model');
		 $this->load->model('website-element/property_model');
		 $this->load->model('website-element/brand_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }	
		
         if($this->uri->segment('4')){
		
		 $conditions = array(
	 				  'brand_master.id' => $this->uri->segment('4')
	 					);
		 $brand_info = $this->brand_model->getBrandByid($conditions);
		
		
		foreach($brand_info as $brnd_in):
		$this->outputData['id'] 	= $brnd_in->id;
		$this->outputData['name'] 	= $brnd_in->name;
		$this->outputData['description'] 	= $brnd_in->description;
		$this->outputData['meta_tag'] 	= $brnd_in->meta_tag;
		$this->outputData['meta_description'] 	= $brnd_in->meta_description;
		$this->outputData['status'] 	= $brnd_in->stname;
		$this->outputData['sort_order'] 	= $brnd_in->sort_order;
		
		$this->outputData['fileid'] 	= $brnd_in->fileid;
		$this->outputData['file_name'] 	= $brnd_in->file_name;
		$this->outputData['file_type'] 	= $brnd_in->file_type;
		$this->outputData['path'] 	= $brnd_in->path;
		$this->outputData['file_size'] 	= $brnd_in->file_size;
		endforeach;
		
		$cond = array(
	 				  'brand_to_property.brand_id' => $this->outputData['id']
	 					);
		
		$this->outputData['prop_brand'] = $this->brand_model->getPropertiesBy_brand($cond);
		
		}
		$cond_prop = array('property_master.status_id' => '1'); 
		$this->outputData['properties'] = $this->property_model->getProperties($cond_prop, $array=array());
        $this->render_page('templates/website-element/brand_form',$this->outputData);
	
     } 
	
	function create_brand()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/brand_model');
		$this->form_validation->set_rules('brand_name', 'Brandname', 'required|trim|xss_clean'); //xss_clean
		$this->form_validation->set_rules('brand_description', 'Brand_Description', 'required|trim|xss_clean');
		$this->form_validation->set_rules('brand_sort_order', 'Brand Sort Order', 'numeric|integer');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->render_page('templates/website-element/brand_form');
		}
		else
		{
		$logo_info = $this->getFileinfo($this->input->post('brand_logo'));
		$data = array(
        'brand_name' => $this->input->post('brand_name'),
        'brand_description' => $this->input->post('brand_description'),
		 'brand_meta_description' => $this->input->post('brand_meta_description'),
             'brand_sort_order' =>$this->input->post('brand_sort_order'),
        'brand_sort_order' =>$this->input->post('brand_sort_order'),
	    'properties' =>$this->input->post('mult_properties'),
	    'logo_path' => $this->input->post('brand_logo'),
	    'logo_name' => $logo_info['logo_name'],
	    'logo_type' =>$logo_info['logo_type'],
	    'logo_size' => $logo_info['logo_size']
	     );
	 
	 
	   if($this->brand_model->createBrand($data)=='success'){
	     $this->session->set_flashdata('flash_message', "Success: You have saved Brand!");
	     redirect('website-element/brand/index');
	     }
	    }
	}
	
	
	
	
	function edit_brand()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/brand_model');
		$this->form_validation->set_rules('brand_name', 'Brandname', 'required|trim|xss_clean');
		$this->form_validation->set_rules('brand_description', 'Brand_Description', 'required|trim|xss_clean');
	

		if ($this->form_validation->run() == FALSE)
		{
			
			  $this->render_page('templates/website-element/brand_form');
		}
		else
		{    
		     $logo_info = $this->getFileinfo($this->input->post('brand_logo'));
		
		
			 $data = array(
             'brand_id' => $this->uri->segment('4'),
	         'brand_name' => $this->input->post('brand_name'),
             'brand_description' => $this->input->post('brand_description'),
			 'brand_meta_tag' => $this->input->post('brand_meta_tag'),
			 'brand_meta_description' => $this->input->post('brand_meta_description'),
             'brand_sort_order' =>$this->input->post('brand_sort_order'),
			 'brand_logo' =>$this->input->post('file_id'),
			 'properties' =>$this->input->post('mult_properties'),
			 'logo_path' => $this->input->post('brand_logo'),
	 		 'logo_name' => $logo_info['logo_name'],
	 		 'logo_type' =>$logo_info['logo_type'],
	 		 'logo_size' => $logo_info['logo_size']
	            );
				
				
				
	 
	         if($this->brand_model->editBrand($data)=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have modified Brand!");
	         redirect('website-element/brand/index');
	         }
	 
	 
	    }
	}


	function delete_brand()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/brand_model');
		
		
		if($this->brand_model->deleteBrand($this->uri->segment('4'))=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have deleted Brand!");
	         redirect('website-element/brand/index');
	        
	 
	 
	    }
	}
	
	
	function delete_mult_brand()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/brand_model');
		
		$array = $this->input->post('chk');
		foreach($array as $id):
		
		$this->brand_model->deleteBrand($id);
		
		endforeach;
		$this->session->set_flashdata('flash_message', "Success: You have deleted Brands!");
		redirect('website-element/brand/index');
		
	
	}
	
	
	function getFileinfo($logo)
	{
	 	$ch = curl_init($logo);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, TRUE);
		curl_setopt($ch, CURLOPT_NOBODY, TRUE);

		$data = curl_exec($ch);
		$size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
		$contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
		curl_close($ch);
		
		$logo_details = array('logo_name' => basename($logo), 'logo_type' => $contentType, 'logo_size' => $size);
	    return $logo_details;
	
	}

}//End  Home Class





?>