<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Options extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

	
	function index()
	{   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('website-element/options_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }		
        $this->outputData['options'] = $this->options_model->getOptions();
        $this->render_page('templates/website-element/options',$this->outputData);
	
     } 
	
	
	
	 function form()
	{   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('website-element/options_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }		
		 
         if($this->uri->segment('4')){
		 $conditions = array(
	 				  'option_master.id' => $this->uri->segment('4')
	 					);
		$this->outputData['option'] = $this->options_model->getOptionByid($conditions);
		
				  
		  }		
        $this->render_page('templates/website-element/options_form',$this->outputData);
	
     } 
	 
	 
	 
	 
	  function edit()
	{   
	     $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
       $this->load->model('website-element/options_model');
	   
	   $this->form_validation->set_rules('option_name', 'Option Name', 'required|trim|xss_clean');
	   
	   if ($this->form_validation->run() == FALSE)
		{
		
			  $this->render_page('templates/website-element/options_form');
		}
		else
		{
			//$this->load->view('formsuccess');
		 $data = array(
        'name' => $this->input->post('option_name'),
        'type' =>$this->input->post('option_type')
	     );
	 
	     if($this->options_model->editOption($data,$this->uri->segment('4'))=='success'){
	 
	     $this->session->set_flashdata('flash_message', "Success: You have modified Option!");
	     redirect('website-element/options/index');
	     }
	 
	 
	   }
	
     } 
	 
	 
	 
	 
	 
	 
	  function create()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
       $this->load->model('website-element/options_model');
	   
	   $this->form_validation->set_rules('option_name', 'Option Name', 'required|trim|xss_clean');
	   
	   if ($this->form_validation->run() == FALSE)
		{
		
			  $this->render_page('templates/website-element/options_form');
		}
		else
		{
			//$this->load->view('formsuccess');
		 $data = array(
        'name' => $this->input->post('option_name'),
        'type' =>$this->input->post('option_type'),
		'status' =>'1'
	     );
	 
	     if($this->options_model->insertOption($data)=='success'){
	 
	     $this->session->set_flashdata('flash_message', "Success: You have saved Option!");
	     redirect('website-element/options/index');
	     }
	 
	 
	   }
	}

	
	
	
	
	function delete()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
       $this->load->model('website-element/options_model');
		
		
		if($this->options_model->deleteOption($this->uri->segment('4'))=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have deleted Option!");
	         redirect('website-element/options/index');
	        
	 
	 
	    }
	}
	
	
	function delete_multi(){
	
	    $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/options_model');
		
		$array = $this->input->post('chk');
		foreach($array as $id):
		
		$this->options_model->deleteOption($id);
		
		endforeach;
		$this->session->set_flashdata('flash_message', "Success: You have deleted Options!");
		redirect('website-element/options/index');
	
	
	}
	
	
	

}//End  Home Class





?>