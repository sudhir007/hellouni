<?php

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

require APPPATH . "vendor/autoload.php";
use OpenTok\OpenTok;
use OpenTok\MediaMode;

class Webinarmaster extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		    $this->load->library('form_validation');
        $this->load->library('zip');

    }

    function index(){

      $this->load->helper('cookie_helper');
        $this->load->model('user/campus_model');
    $userdata=$this->session->userdata('user');
      if(empty($userdata)){  redirect('common/login'); }



       $this->render_page('templates/webinar/webinarform',$this->outputData);

    }

    function form(){

      $this->load->helper('cookie_helper');
      $this->load->model('user/campus_model');
      $this->load->model('webinar/webinar_model');
      $userdata=$this->session->userdata('user');
      if(empty($userdata)){  redirect('common/login'); }

      if($this->uri->segment('4')){

        $webinarData = $this->webinar_model->getWebinarById($this->uri->segment('4'));

        $this->outputData['w_id'] = $webinarData->id;
        $this->outputData['webinar_name'] = $webinarData->webinar_name;
        $this->outputData['webinar_topics'] = $webinarData->webinar_topics;
        $this->outputData['webinar_host_name'] = $webinarData->webinar_host_name;
        $this->outputData['webinar_description'] = $webinarData->webinar_description;
        $this->outputData['webinar_date'] = $webinarData->webinar_date;
        $this->outputData['webinar_time'] = $webinarData->webinar_time;
        $this->outputData['webinar_img'] = $webinarData->webinar_img;
        $this->outputData['webinar_link'] = $webinarData->webinar_link;
        $this->outputData['webinar_display_link'] = $webinarData->webinar_display_link;
        $this->outputData['webinar_display_description'] = $webinarData->webinar_display_description;
        $this->outputData['status'] = $webinarData->status;
        $this->outputData['default_attend_count'] = $webinarData->default_attend_count;
        $this->outputData['webinar_live'] = $webinarData->webinar_live;

      }

      $this->outputData['new'] = "new";

       $this->render_page('templates/webinar/webinarform',$this->outputData);

    }

    function list(){

      $this->load->helper('cookie_helper');
        $this->load->model('user/campus_model');
        $this->load->model('webinar/webinar_model');
    $userdata=$this->session->userdata('user');
      if(empty($userdata)){  redirect('common/login'); }

      $this->outputData['view'] = 'list';
      $this->outputData['webinarlist'] = $this->webinar_model->webinar_list();

       $this->render_page('templates/webinar/webinarlist',$this->outputData);

    }

    function studentlist(){

      $this->load->helper('cookie_helper');
        $this->load->model('user/campus_model');
        $this->load->model('webinar/webinar_model');
    $userdata=$this->session->userdata('user');
      if(empty($userdata)){  redirect('common/login'); }

      $this->outputData['view'] = 'list';
      $this->outputData['webinarstudentlist'] = $this->webinar_model->webinar_student_list();

       $this->render_page('templates/webinar/webinarstudentlist',$this->outputData);

    }

    function studentlistbyid(){

      $this->load->helper('cookie_helper');
        $this->load->model('user/campus_model');
        $this->load->model('webinar/webinar_model');
    $userdata=$this->session->userdata('user');
      if(empty($userdata)){  redirect('common/login'); }

      $webinarId = $this->uri->segment('4');

      $this->outputData['view'] = 'list';
      $this->outputData['webinar_id'] = $webinarId;

      $stdentList = $this->webinar_model->webinar_student_list_by_id($webinarId);
      $tokboxTokenId = $stdentList[0]['tokbox_token_id'];

      $this->outputData['webinarstudentlist'] = $stdentList;
      $this->outputData['tokbox_token_id'] = $tokboxTokenId;

       $this->render_page('templates/webinar/webinarstudentlistbyid',$this->outputData);

    }

    public function exportCSV(){

    $this->load->helper('cookie_helper');
    $this->load->model('user/campus_model');
    $this->load->model('webinar/webinar_model');
    $userdata=$this->session->userdata('user');
    $this->load->helper('url');
      if(empty($userdata)){  redirect('common/login'); }

    $webinarId = $this->uri->segment('4');

   // file name
   $filename = 'StudentListWebinar_'.$webinarId."_".date('Ymd').'.csv';
   header("Content-Description: File Transfer");
   header("Content-Disposition: attachment; filename=$filename");
   header("Content-Type: application/csv; ");

   $studentData = $this->webinar_model->webinar_student_list_by_id($webinarId);

   // file creation
   $file = fopen('php://output', 'w');

   $header = array("id1","id2","id3","Student Name","Student Email","Student Phone","Desired Destination","Originator Name","Facilitator Name", "Webinar Name", "Webinar Attended");
   fputcsv($file, $header);
   foreach ($studentData as $key=>$line){
     fputcsv($file,$line);
   }
   fclose($file);
   exit;
  }

    function create(){

      $this->load->helper('cookie_helper');
      $this->load->model('user/campus_model');
      $this->load->model('webinar/webinar_model');
      $userdata=$this->session->userdata('user');
      if(empty($userdata)){  redirect('common/login'); }

      $webinar_name = $this->input->post('webinar_name');
      $webinar_topics = $this->input->post('webinar_topics');
      $webinar_host_name = $this->input->post('webinar_host_name');
      $webinar_description = $this->input->post('webinar_description');
      $webinar_date = $this->input->post('webinar_date');
      $webinar_time = $this->input->post('webinar_time');
      $webinarimg = $this->input->post('webinarimg');
      $webinar_img = '';
      $webinar_link = $this->input->post('webinar_link');
      $webinar_display_link = $this->input->post('webinar_display_link');
      $webinar_display_description = $this->input->post('webinar_display_description');
      $status = $this->input->post('status');
      $default_attend_count = $this->input->post('default_attend_count');
      $webinar_live = $this->input->post('webinar_live');

      $webinarimpath = "";

      if($_FILES["webinar_img"]["name"])
      {
          $this->load->library('upload');

          $banner_random_digit=round(microtime(true));
          $banner_temp = explode(".", $_FILES["webinar_img"]["name"]);

          $banner_target_file =  BASEPATH . "../../".$webinarimg;
          $webinarimpath = "/uploads/webinar/" . $_FILES["webinar_img"]["name"];
          //copy($_FILES["bannerToUpload"]["tmp_name"], $banner_target_file);
          if(file_exists($banner_target_file))
          {
              unlink($banner_target_file);
          }

          $config = array();
          $config['upload_path']      = BASEPATH . "../../uploads/webinar";
          $config['file_name']        = $_FILES["webinar_img"]["name"];
          $config['allowed_types']    = 'gif|jpg|png|JPEG';
          $config['max_size']         = 500000;
          $config['max_width']        = 1500;
          $config['max_height']       = 1500;

          $this->upload->initialize($config);

          if (!$this->upload->do_upload('webinar_img')) {
              echo '<script type="text/javascript">alert("' . $this->upload->display_errors() . '");window.location.href="webinar/webinarmaster/list/";</script>';
              exit;
          }
      }

      $webinarimpathfinal = $webinarimpath == "" ? $webinarimg : $webinarimpath;

      // Tokbox Code Starts
      $this->load->model(array('tokbox_model'));
      $opentok = new OpenTok('46220952', '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2');
      $session = $opentok->createSession(array(
          'mediaMode' => MediaMode::ROUTED
      ));
      $sessionId = $session->getSessionId();
      $participants = array(
          "organizer" => (string)$userdata['id']
      );
      $tokbox_data['participants'] = json_encode($participants);
      $tokbox_data['session_id'] = $sessionId;
      $tokbox_data['created_at'] = date('Y-m-d H:i:s');
      $tokbox_data['type'] = 'WEBINAR';
      $tokenId = $this->tokbox_model->addToken($tokbox_data);
      //$tokboxUrl = base_url() . '/toxbox/webinar?id=' . $tokenId;

      $WebinarPostData = array(
        'webinar_name' 			=>  $this->input->post('webinar_name'),
        'webinar_topics'=> $this->input->post('webinar_topics'),
        'webinar_host_name' 		=>  $this->input->post('webinar_host_name'),
        'webinar_description' 		=>  $this->input->post('webinar_description'),
        'webinar_date'			=>  $this->input->post('webinar_date'),
        'webinar_time' 			=>  $this->input->post('webinar_time'),
        'webinar_img' 	=>  $webinarimpathfinal,
        'webinar_link' 	=>	$this->input->post('webinar_link'),
        'webinar_display_link'			=>  $this->input->post('webinar_display_link'),
        'webinar_display_description' 		=>	$this->input->post('webinar_display_description'),
        'status' 		=>	$this->input->post('status'),
        'default_attend_count'        => $this->input->post('default_attend_count'),
        'webinar_live' => $this->input->post('webinar_live'),
        //'webinar_link' => $tokboxUrl,
        'tokbox_token_id' => $tokenId
      );

      $WebinarAdd = $this->webinar_model->post($WebinarPostData,'webinar_master');

      $this->session->set_flashdata('flash_message', "Success: You have Created New Webinar!");
      redirect('webinar/webinarmaster/list');

    }

    function edit(){

      $this->load->helper('cookie_helper');
      $this->load->model('user/campus_model');
      $this->load->model('webinar/webinar_model');
      $userdata=$this->session->userdata('user');
      if(empty($userdata)){  redirect('common/login'); }

      $webinar_id = $this->uri->segment('4');

      $webinar_name = $this->input->post('webinar_name');
      $webinar_topics = $this->input->post('webinar_topics');
      $webinar_host_name = $this->input->post('webinar_host_name');
      $webinar_description = $this->input->post('webinar_description');
      $webinar_date = $this->input->post('webinar_date');
      $webinar_time = $this->input->post('webinar_time');
      $webinarimg = $this->input->post('webinarimg');
      $webinar_img = '';
      $webinar_link = $this->input->post('webinar_link');
      $webinar_display_link = $this->input->post('webinar_display_link');
      $webinar_display_description = $this->input->post('webinar_display_description');
      $status = $this->input->post('status');
      $default_attend_count = $this->input->post('default_attend_count');
      $webinar_live = $this->input->post('webinar_live');


      $webinarimpath = "";

      if($_FILES["webinar_img"]["name"])
      {
          $this->load->library('upload');

          $banner_random_digit=round(microtime(true));
          $banner_temp = explode(".", $_FILES["webinar_img"]["name"]);

          $banner_target_file =  BASEPATH . "../../".$webinarimg;
          $webinarimpath = "/uploads/webinar/" . $_FILES["webinar_img"]["name"];
          //copy($_FILES["bannerToUpload"]["tmp_name"], $banner_target_file);
          if(file_exists($banner_target_file))
          {
              unlink($banner_target_file);
          }

          $config = array();
          $config['upload_path']      = BASEPATH . "../../uploads/webinar";
          $config['file_name']        = $_FILES["webinar_img"]["name"];
          $config['allowed_types']    = 'gif|jpg|png|JPEG|jpeg';
          $config['max_size']         = 500000;
          $config['max_width']        = 1500;
          $config['max_height']       = 1500;

          $this->upload->initialize($config);

          if (!$this->upload->do_upload('webinar_img')) {
              echo '<script type="text/javascript">alert("' . $this->upload->display_errors() . '");window.location.href="/webinar/webinarmaster/list/";</script>';
              exit;
          }
      }

      $webinarimpathfinal = $webinarimpath == "" ? $webinarimg : $webinarimpath;

      $webinarData = array(
          'webinar_name' 			=>  $this->input->post('webinar_name'),
          'webinar_topics'=> $this->input->post('webinar_topics'),
          'webinar_host_name' 		=>  $this->input->post('webinar_host_name'),
          'webinar_description' 		=>  $this->input->post('webinar_description'),
          'webinar_date'			=>  $this->input->post('webinar_date'),
          'webinar_time' 			=>  $this->input->post('webinar_time'),
          'webinar_img' 	=>  $webinarimpathfinal,
          'webinar_link' 	=>	$this->input->post('webinar_link'),
          'webinar_display_link'			=>  $this->input->post('webinar_display_link'),
          'webinar_display_description' 		=>	$this->input->post('webinar_display_description'),
          'status' 		=>	$this->input->post('status'),
          'default_attend_count'        => $this->input->post('default_attend_count'),
          'webinar_live' => $this->input->post('webinar_live')
      );

      $whereCondition = ["id" => $webinar_id];

      $updateWebinar = $this->webinar_model->put($whereCondition, $webinarData, 'webinar_master');

      $this->session->set_flashdata('flash_message', "Success: You have modified Webinar!");
       redirect('webinar/webinarmaster/list');

    }

    function polls($webinarId = NULL)
    {
        $this->load->helper('cookie_helper');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $this->load->model('webinar/webinar_model');
        $allPolls = $this->webinar_model->getAllPolls();

        $this->outputData['view'] = 'list';
        $this->outputData['polls'] = $allPolls;
        $this->render_page('templates/webinar/polls_list',$this->outputData);
    }

    function pollForm($pollId = NULL)
    {
        $this->load->helper('cookie_helper');
        $this->load->model('webinar/webinar_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata)){  redirect('common/login'); }

        if($pollId)
        {

          $webinarData = $this->webinar_model->getWebinarById($this->uri->segment('4'));

          $this->outputData['w_id'] = $webinarData->id;
          $this->outputData['webinar_name'] = $webinarData->webinar_name;
          $this->outputData['webinar_topics'] = $webinarData->webinar_topics;
          $this->outputData['webinar_host_name'] = $webinarData->webinar_host_name;
          $this->outputData['webinar_description'] = $webinarData->webinar_description;
          $this->outputData['webinar_date'] = $webinarData->webinar_date;
          $this->outputData['webinar_time'] = $webinarData->webinar_time;
          $this->outputData['webinar_img'] = $webinarData->webinar_img;
          $this->outputData['webinar_link'] = $webinarData->webinar_link;
          $this->outputData['webinar_display_link'] = $webinarData->webinar_display_link;
          $this->outputData['webinar_display_description'] = $webinarData->webinar_display_description;
          $this->outputData['status'] = $webinarData->status;
          $this->outputData['default_attend_count'] = $webinarData->default_attend_count;
          $this->outputData['webinar_live'] = $webinarData->webinar_live;

        }

        $this->outputData['new'] = "new";
        $this->render_page('templates/webinar/poll_form',$this->outputData);
    }

    function createPoll()
    {
        $postData = $this->input->post();
        $type = $postData['type'];

        $pollData['type'] = $type;
        $pollData['title'] = $postData['title'];
        $pollData['time'] = $postData['time'] * 60;

        switch($type)
        {
            case 'SINGLE_MCQ':
                $pollQuestion['question'] = $postData['single_question'];
                $pollQuestion['options'] = json_encode($postData['single_options']);
                $pollQuestion['answer'] = $postData['single_correct_ans'];
                $pollQuestion['poll_id'] = $pollId;
                break;
            case 'MULTIPLE_MCQ':
                $questions = $postData['questions'];
                foreach($questions as $question)
                {

                }
            case 'SINGLE_SUBJECTIVE':
            case 'MULTIPLE_SUBJECTIVE':
        }
    }

    //fair Student list

    function fairstudentlist(){

      $this->load->helper('cookie_helper');
        $this->load->model('user/campus_model');
        $this->load->model('webinar/webinar_model');

    $userdata=$this->session->userdata('user');
      if(empty($userdata)){  redirect('common/login'); }

      $userId = $userdata['id'];

      $this->outputData['view'] = 'list';
      $this->outputData['fairstudentlist'] = $this->webinar_model->studentListByUniversityFair($userId);

       $this->render_page('templates/webinar/fairStudentListByUniversity',$this->outputData);

    }

    function updateWebinarLogMasterBywebinarId(){

    $this->load->model('webinar/webinar_model');

    $userdata=$this->session->userdata('user');
    if(empty($userdata)){  redirect('common/login'); }

    $tokboxId = $this->input->get('tokbox_id');
    $webinarId = $this->input->get('webinar_id');

    $whereConditiontokbox = ["tokbox_id" => $tokboxId, "role" => "Attendee"];
    $webinarData = $this->webinar_model->get($whereConditiontokbox, 'tokbox_connections');

    if(!$webinarData['data'])
    {
        echo "NO DATA ...!!!";
        exit();
    }

    foreach ($webinarData['data'] as $key => $value) {

      $userId = $value['user_id'];

      if($userId){
        $whereCondition = ["user_id" => $userId ,"webinar_id" => $webinarId];
        $upsertData = ["webinar_applied" => 1, "webinar_attented" => 1];
        $updateWebinarInfo = $this->webinar_model->upsert($whereCondition, $upsertData, "webinar_log_master");
      }

    }

    echo "List Updated ...!!!";
    exit();

    }

    function universityStudentlistbyid(){

        $this->load->helper('cookie_helper');
        $this->load->model('webinar/webinar_model');

    //$userdata=$this->session->userdata('user');
      //if(empty($userdata)){  redirect('common/login'); }

      $webinarId = 25; //$this->uri->segment('4');

      $this->outputData['view'] = 'list';
      $this->outputData['webinar_id'] = $webinarId;

      //$stdentList = $this->webinar_model->university_student_list_by_id($webinarId);
      $mandatoryDocuments = [10, 11, 12, 13, 14, 15, 16, 17, 18];
      $finalList = [];
      $stdentList = $this->webinar_model->getSeminarStudents();
      $prevUserId = '';
      $uploadedDoc = [];
      foreach($stdentList as $student)
      {
          if($prevUserId != $student['id'])
          {
              if($prevUserId)
              {
                  if(count($uploadedDoc) == 0)
                  {
                      $finalList[$prevUserId]['document_status'] = 'Not Uploaded';
                  }
                  else if(count($uploadedDoc) < count($mandatoryDocuments))
                  {
                      $finalList[$prevUserId]['document_status'] = 'Incomplete';
                  }
                  else
                  {
                      $finalList[$prevUserId]['document_status'] = 'Completed';
                  }
              }
              $finalList[$student['id']] = $student;
              $prevUserId = $student['id'];
              $uploadedDoc = [];
          }
          if(in_array($student['document_meta_id'], $mandatoryDocuments))
          {
              $uploadedDoc[] = $student['document_meta_id'];
          }
      }
      if(count($uploadedDoc) == 0)
      {
          $finalList[$prevUserId]['document_status'] = 'Not Uploaded';
      }
      else if(count($uploadedDoc) < count($mandatoryDocuments))
      {
          $finalList[$prevUserId]['document_status'] = 'Incomplete';
      }
      else
      {
          $finalList[$prevUserId]['document_status'] = 'Completed';
      }
      //$tokboxTokenId = $stdentList[0]['tokbox_token_id'];

      $this->outputData['universitystudentlist'] = $finalList;
      $this->outputData['username'] = "UTN Event";

       //$this->render_page('templates/webinar/universitystudentlistbyid',$this->outputData);
       $this->load->view('templates/common/header', $this->outputData);

       $this->load->view('templates/webinar/universitystudentlistbyid', $this->outputData);

       $this->load->view('templates/common/footer', $this->outputData);

    }

    function downloadDocs(){

      $this->load->model('webinar/webinar_model');

      $userId = $this->input->get('id');
      $dataYes = $this->input->get('data_value');
      $whereCondition = ["user_id" => $userId];

      $customerDocList = $this->webinar_model->get($whereCondition, 'document');

      if(!$customerDocList['data'] ){
        echo 'NODOCUMENT';
        die();
      } elseif ( $dataYes == 'NO') {
        echo 'YESDOCUMENT';
        die();
      }

			$fileName = "";
			foreach ($customerDocList['data'] as $row => $value)
        {
          $fileName = $_SERVER['DOCUMENT_ROOT'] . "/uploads/student_".$userId."/".$value['name'];
          $this->zip->read_file($fileName);
        }

        ob_end_clean();

       $archive_file_name = 'student_' . $userId . '.zip';
       $this->zip->download($archive_file_name);
			 ob_end_clean();

			 exit($fileName);


			ini_set('memory_limit', '-1');
      $zipFile = $_SERVER['DOCUMENT_ROOT'] . '/uploads/student_'.$userId.'/.zip';
			$this->zip->read_dir($path, false);
			$this->zip->archive($zipFile);

			ob_end_clean();

			header("Content-type: application/zip");
			header("Content-Disposition: attachment; filename=$archive_file_name");
			header("Content-length: " . filesize($zipFile));
			header("Pragma: no-cache");
			header("Expires: 0");

      ob_end_clean();

			readfile($zipFile);


    }

    function viewDocs(){

      $this->load->model('webinar/webinar_model');

      $userId = $this->input->get('id');
      $dataYes = $this->input->get('data_value');
      $whereCondition = ["user_id" => $userId];
      $allDocs = $this->webinar_model->getDocumentsByUniversity(2);

      $customerDocList = $this->webinar_model->getDocument($userId);
      //echo '<pre>';
      if($customerDocList)
      {
          foreach($allDocs as $doc)
          {
              //print_r($doc);
              $found = false;
              foreach($customerDocList as $index => $docList)
              {
                  if($docList['document_meta_id'] == $doc['id'])
                  {
                      $found = true;
                  }
              }
              if(!$found)
              {
                  $temp['document_name'] = $doc['name'];
                  $temp['url'] = '';
                  $temp['required'] = $doc['required'];
                  $temp['document_meta_id'] = $doc['id'];
                  $temp['user_id'] = $userId;
                  //echo "Not Found ---------------------";
                  //print_r($temp);
                  $customerDocList[] = $temp;
              }
          }
      }
      else
      {
          foreach($allDocs as $doc)
          {
              $temp['document_name'] = $doc['name'];
              $temp['url'] = '';
              $temp['required'] = $doc['required'];
              $customerDocList[] = $temp;
          }
      }

      echo json_encode($customerDocList);


    }


}

 ?>
