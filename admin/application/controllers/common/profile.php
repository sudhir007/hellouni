<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com



 <Reverse bidding system>

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or

    contact us from http://www.freelancephpscript.com/contact

 */
    /*ini_set('display_errors', '1');
 ini_set('display_startup_errors', '1');
 error_reporting(E_ALL);*/
require_once APPPATH . 'libraries/Mail/sMail.php';
class Profile extends MY_Controller {



	//Global variable

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;



    /**

	 * Constructor

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()

    {

         parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
        //Models
		$this->load->model('location/country_model');
		$this->load->model('user/user_model');
   }





	function index()

	{
       $userdata=$this->session->userdata('user');
		$conditions = array('user_master.id' => $userdata['id']);
		  $cond_user1 = array('country_master.status ' => '1');
		 if($userdata['type']==3){
		//die('test1');
		 $user_details = $this->user_model->getUserByid($conditions);

		// print_r($userdata); exit;
		 //echo '<pre>'; print_r($user_details); echo '</pre>'; exit;

		$this->outputData['id'] 				= $user_details->id;
		$this->outputData['name'] 				= $user_details->name;
		$this->outputData['email'] 				= $user_details->email;
		$this->outputData['image'] 				= $user_details->image;
		$this->outputData['phone'] 				= $user_details->phone;
        $this->outputData['about'] 				= $user_details->about;
        $this->outputData['address']            = $user_details->address;
        $this->outputData['country_id']         = $user_details->country_id;

		 $this->outputData['countries'] = $this->country_model->getCountries($cond_user1);
		//$this->outputData['address_details'] = $this->user_model->getUniAddress(array('address_master.user_id' => $userdata['id']));

		// echo '<pre>'; print_r($this->outputData['address_details']); echo '</pre>'; exit;
		 $this->render_page('templates/user/university_profile',$this->outputData);
         }else if($userdata['type']==2){

		 $this->render_page('templates/user/admin_profile');

		 }


	}

	function save()

	{  $this->load->helper(array('form', 'url'));
       $userdata=$this->session->userdata('user');

		 if(empty($userdata)){  redirect('common/login'); }
		 //print_r($userdata);
		 if($userdata['type']==3){


		 $data = array(
        	'id'            =>  $this->uri->segment('4'),
			'name' 			=>  $this->input->post('uniname'),
        	'email' 		=>  $this->input->post('email'),
			'password'		=>  md5($this->input->post('password')),
			'image' 		=>	$this->input->post('logopath'),
        	'about' 		=>	$this->input->post('about'),
			'phone' 		=>	$this->input->post('phone'),
            'address'       => $this->input->post('address'),
            'country_id'    => $this->input->post('country_id')
			);

			/*if($this->input->post('password')!=''){
			$data['password'] = md5($this->input->post('password'));
			}*/

			//print_r($this->input->post('country')); exit;

			/*$data['fulladdress'][] = array();
			$i = 0;
		 	foreach($this->input->post('address') as $add){

		 	$data['fulladdress'][$i] = array('address'=>$add, 'country'=>$this->input->post('country')[$i]);
		 	$i++;
        }*/
		 	//echo '<pre>';
		  	//print_r($data['fulladdress']); echo '</pre>'; exit;
		 if($this->user_model->editUniUser($data)=='success'){

		  redirect('common/profile');

		 }

         }else if($userdata['type']==2){

		 $this->render_page('templates/user/admin_profile');

		 }


	}

   function updateProfile()
   {
        $userdata=$this->session->userdata('user');
        if(empty($userdata)){  redirect('common/login'); }
 
        if($_POST){
            
            $data['name'] = $this->input->post('name');
            $data['username'] = $this->input->post('username');
            $data['email'] = $this->input->post('email');
            $data['phone'] = $this->input->post('mobile');

            $this->user_model->updateProfile($userdata['id'],$data);

            $this->session->set_flashdata('flash_message', "Success: You have successfully updated profile!");
            redirect('common/profile/updateProfile');
        }else{

            $this->db->select('*');
            $this->db->from('user_master');
            $this->db->where('username',$userdata['username']);
            $query = $this->db->get();
            $result=$query->row();

            $this->outputData['name'] = $result->name;
            $this->outputData['email'] = $result->email;
            $this->outputData['username'] = $result->username;
            $this->outputData['mobile'] = $result->phone;

            $this->render_page('templates/common/updateProfile', $this->outputData);
    
        }
   }

	function changepassword(){

	$userdata=$this->session->userdata('user');
	if(empty($userdata)){  redirect('common/login'); }

	$this->render_page('templates/common/changepassword');

	}

	function updatepassword(){

	$userdata=$this->session->userdata('user');
	if(empty($userdata)){  redirect('common/login'); }


	 $data = array('id' => $userdata['id'], 'password' =>  md5($this->input->post('newpassword1')));

	 if($this->user_model->changepassword($data)=='success'){

		  $this->session->unset_userdata('user');
		  $this->session->set_flashdata('flash_message', 'You have successfully changed password. Please log in to continue');
		  redirect('common/login');

		 }



	}

    function forgotPassword()
    {
        $this->load->model('user/user_model');

        $userdata = $this->session->userdata('user');
        if(!empty($userdata))
        {
            redirect('common/dashboard');
        }

        if($this->input->post())
        {
            $unameOrEmail = $this->input->post('uname_email');
            $userAvailable = $this->user_model->getUserMaster($unameOrEmail);
            if(!$userAvailable)
            {
                $this->session->set_flashdata('flash_message', "Sorry! The username or email doesn't exist");
                redirect('/common/profile/forgotPassword');
            }
            $status = $userAvailable[0]['status'];
            $userId = $userAvailable[0]['id'];
            switch($status)
            {
                case 2:
                    $this->session->set_flashdata('flash_message', "Sorry! The account is inactive. Please activate the acccount or contact to our support.");
                    redirect('/common/profile/forgotPassword');
                    break;
                case 3:
                    $this->session->set_flashdata('flash_message', "Sorry! The account is not activated yet. Please activate the acccount or contact to our support.");
                    redirect('/common/profile/forgotPassword');
                    break;
                case 4:
                    $this->session->set_flashdata('flash_message', "Sorry! The account is blocked. Please contact to our support.");
                    redirect('/common/profile/forgotPassword');
                    break;
                case 5:
                    $this->session->set_flashdata('flash_message', "Sorry! The account is deleted. Please contact to our support.");
                    redirect('/common/profile/forgotPassword');
                    break;
            }

            $tokenExist = $this->user_model->getForgotPasswordByUserId($userId);
            if(!$tokenExist)
            {
                $token = md5(date('Y-m-d H:i:s') . $userId);
                $data['token'] = $token;
                $data['user_id'] = $userId;
                $data['status'] = 0;
                $data['last_updated'] = date('Y-m-d H:i:s');
                $this->user_model->addForgotPassword($data);
            }
            else if($tokenExist[0]['status'])
            {
                $token = md5(date('Y-m-d H:i:s') . $userId);
                $data['token'] = $token;
                $data['status'] = 0;
                $data['last_updated'] = date('Y-m-d H:i:s');
                $this->user_model->updateForgotPassword($data, $userId);
            }
            else
            {
                $token = $tokenExist[0]['token'];
            }

            $studentName = $userAvailable[0]['name'];
            $studentEmail = $userAvailable[0]['email'];
            $forgotPasswordLink = 'https://www.hellouni.org/admin/common/profile/resetPassword?token=' . $token;

            $ccMailList = '';
            $mailAttachments = '';

            $mailSubject = "HelloUni - Reset Password Instruction";
            $mailTemplate = "Hi $studentName

                                To reset your password, please click on the following link

                                <a href='$forgotPasswordLink'>$forgotPasswordLink</a>

                                <b>Thanks and Regards,
                                HelloUni Coordinator
                                +91 81049 09690</b>

                                <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

            $sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

            $this->session->set_flashdata('flash_message', "We sent a mail on your email id. Please check it and follow the instructions.");
            redirect('/common/profile/forgotPassword');
        }

        $this->load->view('templates/user/forgot_password',$this->outputData);
    }

    function resetPassword()
    {
        $this->load->model('user/user_model');

        $token = $this->input->get('token');
        if(!$token)
        {
            $this->session->set_flashdata('flash_message', "This link is invalid!");
            redirect('/common/profile/forgotPassword');
        }

        $tokenExist = $this->user_model->getForgotPasswordByToken($token);
        if(!$tokenExist)
        {
            $this->session->set_flashdata('flash_message', "This link is invalid!");
            redirect('/common/profile/forgotPassword');
        }

        $userId = $tokenExist[0]['user_id'];
        $this->outputData['user_id'] = $userId;
        $this->outputData['token'] = $token;
        $this->load->view('templates/user/reset_password', $this->outputData);
    }

    function reset()
    {
        $password = $this->input->post('password');
        $confirmPassword = $this->input->post('password2');
        $userId = $this->input->post('user_id');
        $token = $this->input->post('token');

        if(!$password || !$confirmPassword || !$userId)
        {
            $this->session->set_flashdata('flash_message', "There is some issue occured. Please try again!");
            redirect('/common/profile/resetPassword?token=' . $token);
        }

        if($password != $confirmPassword)
        {
            $this->session->set_flashdata('flash_message', "Password and confirm password does not match. Please try again!");
            redirect('/common/profile/resetPassword?token=' . $token);
        }

        $this->load->model('user/user_model');
        $tokenData['status'] = 1;
        $tokenData['last_updated'] = date('Y-m-d H:i:s');
        $this->user_model->updateForgotPassword($tokenData, $userId);

        $data =  array(
            'password' => md5($password)
        );
        $this->user_model->updateUserInfo($data, $userId);
        echo '<script type="text/javascript">
                alert("Your password has been updated successfully. Please login to continue.");
                window.location.href = "/common/login/logout";
            </script>';
    }


}//End  Home Class
?>
