<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 */

class Dashboard extends MY_Controller {



	//Global variable

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;



    /**

	 * Constructor

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()

    {

         parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
        $this->load->model(array('common/dashboard_model'));
   }




	function index()
	{
        $userdata=$this->session->userdata('user');

        if(empty($userdata)){  redirect('common/login'); }

        $visitors = $this->dashboard_model->getVisitors();
        $registerations = $this->dashboard_model->getRegisterations();

        $totalSessionText = 'Total Sessions';
        $totalUniversityText = 'Total Universities';
        $universityText = 'Universities';
        $sessionText = 'Sessions';

        if($userdata['type'] == 1 || $userdata['type'] == 2){
            $universities = $this->dashboard_model->getUniversities();
            $sessions = $this->dashboard_model->getSessions();
        }
        else if($userdata['type'] == 7){
            $universities = $this->dashboard_model->getUniversitiesByCounselor($userdata['id']);
            $sessions = $this->dashboard_model->getSessionsByCounselor($userdata['id']);
        }
        else if($userdata['type'] == 3){
            $universities = $this->dashboard_model->getVisitorsByUniversity($userdata['university_id']);
            $sessions = $this->dashboard_model->getSessionsByUniversity($userdata['university_id']);

            $totalSessionText = 'My Registrations';
            $totalUniversityText = 'My Visitors';
            $universityText = 'My Visitors';
            $sessionText = 'My Registrations';
        }

        $totalVisitors = 0;
        $totalRegisterations = 0;
        $totalUniversities = 0;
        $totalSessions = 0;

        if($visitors)
        {
            foreach($visitors as $index => $visitor)
            {
                $totalVisitors += $visitor['count'];
                $visitor['y'] = $visitor['date'];
                $visitor['item1'] = $visitor['count'];
                $visitors[$index] = $visitor;
            }
        }

        if($registerations)
        {
            foreach($registerations as $index => $registeration)
            {
                $totalRegisterations += $registeration['count'];
                $registeration['y'] = $registeration['date'];
                $registeration['item1'] = $registeration['count'];
                $registerations[$index] = $registeration;
            }
        }

        if($universities)
        {
            foreach($universities as $index => $university)
            {
                $totalUniversities += $university['count'];
                $university['y'] = $university['date'];
                $university['item1'] = $university['count'];
                $universities[$index] = $university;
            }
        }

        if($sessions)
        {
            foreach($sessions as $index => $session)
            {
                $totalSessions += $session['count'];
                $session['y'] = $session['date'];
                $session['item1'] = $session['count'];
                $sessions[$index] = $session;
            }
        }

        $this->outputData['visitors'] = json_encode($visitors);
        $this->outputData['registerations'] = json_encode($registerations);
        $this->outputData['universities'] = json_encode($universities);
        $this->outputData['sessions'] = json_encode($sessions);
        $this->outputData['totalVisitors'] = $totalVisitors;
        $this->outputData['totalRegisterations'] = $totalRegisterations;
        $this->outputData['totalUniversities'] = $totalUniversities;
        $this->outputData['totalSessions'] = $totalSessions;
        $this->outputData['totalSessionText'] = $totalSessionText;
        $this->outputData['totalUniversityText'] = $totalUniversityText;
        $this->outputData['universityText'] = $universityText;
        $this->outputData['sessionText'] = $sessionText;

		 $this->render_page('templates/common/dashboard', $this->outputData);
	}

	function profile()

	{
	     $userdata=$this->session->userdata('user');

		 if(empty($userdata)){  redirect('common/login'); }
		 //print_r($userdata);
		 if($userdata['type']==3){

		 $this->render_page('templates/user/university_profile');

         }else{

		 $this->render_page('templates/user/admin_profile');

		 }




	}


}//End  Home Class
?>
