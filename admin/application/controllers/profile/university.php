<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 */

class University extends MY_Controller {



	//Global variable

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;



    /**

	 * Constructor

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()

    {

         parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
        //Models
		$this->load->model('location/country_model');
		$this->load->model('user/user_model');
   }

    function index()
    {
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('');
        }

        $conditions = array('user_master.id' => $userdata['id']);
        $cond_user1 = array('country_master.status ' => '1');
        if($userdata['type']==3)
        {
            $user_details = $this->user_model->getUserByid($conditions);
            //echo '<pre>'; print_r($user_details); echo '</pre>'; exit;
            $university_details = $this->user_model->getUniversityDataByUserid($userdata['id']);
            // print_r($userdata); exit;
            //echo '<pre>'; print_r($university_details); echo '</pre>'; exit;

            $this->outputData['id'] 					= $user_details->id;
            $this->outputData['name'] 					= $user_details->name;
            $this->outputData['email'] 					= $user_details->email;
            $this->outputData['image'] 					= $user_details->image;
            $this->outputData['logo'] 					= $university_details->logo;
            $this->outputData['banner'] 				= $university_details->banner;
            $this->outputData['country_id']             = $university_details->country_id;
            $this->outputData['about'] 					= $university_details->about;
            $this->outputData['website'] 				= $university_details->website;
            $this->outputData['student'] 				= $university_details->total_students;
            $this->outputData['internationalstudent'] 	= $university_details->total_students_international;
            $this->outputData['facilities'] 			= $university_details->facilities;
            $this->outputData['address'] 				= $university_details->address;
            $this->outputData['state'] 					= $university_details->state;
            $this->outputData['city'] 					= $university_details->city;
            $this->outputData['phone'] 					= $user_details->phone;
            $this->outputData['ranking_usa'] 			= $university_details->ranking_usa;
            $this->outputData['ranking_world'] 			= $university_details->ranking_world;
            $this->outputData['placement_percentage'] 	= $university_details->placement_percentage;
            $this->outputData['admission_success_rate'] = $university_details->admission_success_rate;
            $this->outputData['unique_selling_points']  = $university_details->unique_selling_points;
            $this->outputData['research_spending']      = $university_details->research_spending;

            $this->outputData['countries']              = $this->country_model->getCountries($cond_user1);
            $this->outputData['address_details']        = $this->user_model->getUniAddress(array('address_master.user_id' => $userdata['id']));

            //echo '<pre>'; print_r($this->outputData); echo '</pre>'; exit;
            $this->render_page('templates/profile/university_profile',$this->outputData);
        }
    }

    function save()
    {
        if(!$this->input->post('uniname') || !$this->input->post('email') || !$this->input->post('country'))
        {
            $this->session->set_flashdata('flash_message', "Error: Please fill all mandatory fields!");
            redirect('profile/university');
        }

        /*echo '<pre>';
        print_r($this->input->post());
        exit;*/
        $this->load->helper(array('form', 'url'));
        $userdata=$this->session->userdata('user');

        if(empty($userdata)){  redirect('common/login'); }
        //print_r($userdata);
        $sliderApproved = 1;
        $logoApproved = 1;
        if($userdata['type']==3)
        {
            $this->load->library('upload');
            //echo $_FILES["fileToUpload"]["tmp_name"];
            //$target_dir = "upload/";
            if($_FILES["fileToUpload"]["name"])
            {
                $random_digit=round(microtime(true));
                $temp = explode(".", $_FILES["fileToUpload"]["name"]);
                // if(end($temp)=='gif') echo 'hi'; exit;
                //if(end($temp)!='jpg' || end($temp)!='JPG' || end($temp)!='jpeg' || end($temp)!='JPEG' || )

                if(end($temp)!='gif' && end($temp)!='jpg' && end($temp)!='JPG' && end($temp)!='jpeg' && end($temp)!='JPEG')
                {
                    //$this->session->set_flashdata('flash_message', 'File should be in jpg/png/gif formate !');
                    echo '<script type="text/javascript">alert("File should be in jpg/png/gif formate !");window.location.href="/admin/profile/university";</script>';
                    exit;
                    //redirect('profile/university');
                }

                if ($_FILES["fileToUpload"]["size"] > 500000)
                {
                    //$this->session->set_flashdata('flash_message', 'File should be in less than 5 MB in size!');
                    echo '<script type="text/javascript">alert("File should be in less than 5 MB in size!");window.location.href="/admin/profile/university";</script>';
                    exit;
                    //redirect('profile/university');
                }

                if(!is_dir(BASEPATH . "../../uploads/" . $userdata['username']))
                {
                    mkdir(BASEPATH . "../../uploads/" . $userdata['username']);
                }
                $target_file = BASEPATH . "../../uploads/" . $userdata['username'] . "/logo." . end($temp);
                $logoPath = "/uploads/" . $userdata['username'] . "/logo." . end($temp);
                if(file_exists($target_file))
                {
                    unlink($target_file);
                }

                //echo $_FILES["fileToUpload"]["tmp_name"];
                //copy($_FILES["fileToUpload"]["tmp_name"], $target_file);
                //exit;
                $config = array();
                $config['upload_path']      = BASEPATH . "../../uploads/" . $userdata['username'];
                $config['file_name']        = "logo." . end($temp);
                $config['allowed_types']    = 'gif|jpg|png|JPEG';
                $config['max_size']         = 500000;
                $config['max_width']        = 1500;
                $config['max_height']       = 1500;

                $this->upload->initialize($config);

                if (!$this->upload->do_upload('fileToUpload')) {
                    echo '<script type="text/javascript">alert("' . $this->upload->display_errors() . '");window.location.href="/admin/profile/university";</script>';
                    exit;
                }
                $logoApproved = 0;
                $sliderApproved = 0;
            }
            else if(!$this->input->post('profileimage'))
            {
                $logoPath = '';
            }
            else
            {
                $logoPath = $this->input->post('profileimage');
            }

            // FOR BANNR IMAGE
            if($_FILES["bannerToUpload"]["name"])
            {
                $banner_random_digit=round(microtime(true));
                $banner_temp = explode(".", $_FILES["bannerToUpload"]["name"]);
                // if(end($temp)=='gif') echo 'hi'; exit;
                //if(end($temp)!='jpg' || end($temp)!='JPG' || end($temp)!='jpeg' || end($temp)!='JPEG' || )

                if(end($banner_temp)!='gif' && end($banner_temp)!='jpg' && end($banner_temp)!='JPG' && end($banner_temp)!='jpeg' && end($banner_temp)!='JPEG')
                {
                    $this->session->set_flashdata('flash_message', 'Banner should be in jpg/png/gif formate !');
                    redirect('profile/university');
                }

                if ($_FILES["bannerToUpload"]["size"] > 500000)
                {
                    $this->session->set_flashdata('flash_message', 'Banner should be in less than 5 MB in size!');
                    redirect('profile/university');
                }

                if(!is_dir(BASEPATH . "../../uploads/" . $userdata['username']))
                {
                    mkdir(BASEPATH . "../../uploads/" . $userdata['username']);
                }
                //$banner_target_file = "upload/" .$userdata['name'].'_banner_'.$banner_random_digit.'.'.end($banner_temp);
                $banner_target_file =  BASEPATH . "../../uploads/" . $userdata['username'] . "/banner." . end($banner_temp);
                $bannerPath = "/uploads/" . $userdata['username'] . "/banner." . end($banner_temp);
                //copy($_FILES["bannerToUpload"]["tmp_name"], $banner_target_file);
                if(file_exists($banner_target_file))
                {
                    unlink($banner_target_file);
                }

                $config = array();
                $config['upload_path']      = BASEPATH . "../../uploads/" . $userdata['username'];
                $config['file_name']        = "banner." . end($banner_temp);
                $config['allowed_types']    = 'gif|jpg|png|JPEG';
                $config['max_size']         = 500000;
                $config['max_width']        = 1500;
                $config['max_height']       = 1500;

                $this->upload->initialize($config);

                if (!$this->upload->do_upload('bannerToUpload')) {
                    echo '<script type="text/javascript">alert("' . $this->upload->display_errors() . '");window.location.href="/admin/profile/university";</script>';
                    exit;
                }
            }
            else if(!$this->input->post('banner'))
            {
                $bannerPath = '';
            }
            else
            {
                $bannerPath = $this->input->post('banner');
            }

            $data = array(
                'id'                    =>  $userdata['id'],
                'name' 					=>  $this->input->post('uniname'),
                'email' 				=>  $this->input->post('email'),
                'image'                 =>	$logoPath,
                'about' 				=>	$this->input->post('about'),
                'website' 				=>	$this->input->post('website'),
                'student' 				=>	$this->input->post('student') ? $this->input->post('student') : NULL,
                'internationalstudent' 	=>	$this->input->post('internationalstudent') ? $this->input->post('internationalstudent') : NULL,
                'address'  				=>	$this->input->post('address'),
                'country_id'  			=>	$this->input->post('country'),
                'state'  				=>	$this->input->post('state'),
                'city'  				=>	$this->input->post('city'),
                'phone' 				=>	$this->input->post('phone'),
                'ranking_usa' 			=>	$this->input->post('ranking_usa'),
                'ranking_world' 		=>	$this->input->post('ranking_world'),
                'placement_percentage' 	=>	$this->input->post('placement_percentage'),
                'admission_success_rate'=>	$this->input->post('admission_success_rate'),
                'research_spending' 	=>	$this->input->post('research_spending'),
                'unique_selling_points' =>	$this->input->post('unique_selling_points'),
                'facilities' 			=>	$this->input->post('facilities'),
                'logo_approved'         =>  $logoApproved,
                'slider_approved'       =>  $sliderApproved
            );

            if($logoPath)
            {
                $data['logo'] = $logoPath;
            }
            else
            {
                $data['logo'] = '';
            }

            if($bannerPath)
            {
                $data['banner'] = $bannerPath;
            }
            else
            {
                $data['banner'] = '';
            }

            if($this->user_model->editUniUser($data)=='success')
            {
                $this->session->set_flashdata('flash_message', 'Profile has been updated');
                redirect('profile/university');
            }
        }
    }

	function changepassword(){

	$this->render_page('templates/common/changepassword');

	}

	function updatepassword(){



	 $data = array('id' => $this->uri->segment(4), 'password' =>  md5($this->input->post('newpassword1')));

	 if($this->user_model->changepassword($data)=='success'){

		  $this->session->unset_userdata('user');
		  $this->session->set_flashdata('flash_message', 'You have successfully changed password. Please log in to continue');
		  redirect('common/login');

		 }



	}


}//End  Home Class
?>
