<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Representative extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()

    {

         parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
        //Models
		$this->load->model('location/country_model');
		$this->load->model('user/user_model');
   }

	

	

	function index()

	{
       $userdata=$this->session->userdata('user');
		$conditions = array('user_master.id' => $userdata['id']);
		  $cond_user1 = array('country_master.status ' => '1'); 
		 if($userdata['type']==4){
		
		 $user_details = $this->user_model->getUserByid($conditions);
		 $representative_details = $this->user_model->getUniversityByRepresentative(array('university_to_representative.representative_id'=>$userdata['id']));
		// print_r($userdata); exit;
		// echo '<pre>'; print_r($representative_details); echo '</pre>'; exit;
			
		$this->outputData['id'] 					= $user_details->id;
		$this->outputData['name'] 					= $user_details->name;
		$this->outputData['email'] 					= $user_details->email;
		$this->outputData['phone'] 					= $user_details->phone;
		$this->outputData['university'] 			= $representative_details->name;
		
		 $this->render_page('templates/profile/representative_profile.php',$this->outputData);
         }
		
	

	} 
	
	function save()

	{  $this->load->helper(array('form', 'url'));
       $userdata=$this->session->userdata('user');
		 
		 if(empty($userdata)){  redirect('common/login'); }
		 //print_r($userdata);
		 if($userdata['type']==4){
		 
		$condition = array('user_master.id' => $userdata['id']);
		 $data = array(
        	
			'name' 					=>  $this->input->post('name'),
        	'email' 				=>  $this->input->post('email'),
			
			/*'image' 		=>	$this->input->post('logopath'),*/
        	
			
			'phone' 				=>	$this->input->post('phone'),
			'flag'                  =>  '2'
			
			
			);
			
			//print_r($data); exit;
			
			/*if($this->input->post('password')!=''){
			$data['password'] = md5($this->input->post('password'));
			}*/
			
			//print_r($this->input->post('country')); exit;
			
			/*$data['fulladdress'][] = array();
			$i = 0;
		 	foreach($this->input->post('address') as $add){
		 
		 	$data['fulladdress'][$i] = array('address'=>$add, 'country'=>$this->input->post('country')[$i]);
		 	$i++;
		 	}*/
		 	//echo '<pre>';
		  	//print_r($data['fulladdress']); echo '</pre>'; exit;
		 if($this->user_model->updateUser($data,$condition)){
		
		 redirect('profile/representative');
		  
		 }
		 
         }
	

	} 
	
	function changepassword(){
	
	$this->render_page('templates/common/changepassword');
	
	}
	
	function updatepassword(){
	
	
	 
	 $data = array('id' => $this->uri->segment(4), 'password' =>  md5($this->input->post('newpassword1')));
	 
	 if($this->user_model->changepassword($data)=='success'){
		  
		  $this->session->unset_userdata('user');
		  $this->session->set_flashdata('flash_message', 'You have successfully changed password. Please log in to continue');
		  redirect('common/login');
		  
		 }
	 
	
	
	}


}//End  Home Class
?>

