<?php

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);
 */
require_once APPPATH . 'libraries/Mail/sMail.php';
class Universityinfo extends MY_Controller
{
    public $outputData;
    public $loggedInUser;

    function __construct()
    {
        parent::__construct();
        $this->load->library(array('template', 'form_validation'));
        $this->lang->load('enduser/home', $this->config->item('language_code'));
        $this->load->helper(array('cookie_helper', 'form', 'url'));
        $this->load->model(array('user/user_model', 'universitydata_model', 'redi_model', 'webinar/webinar_model'));

    }

    function infoForm(){

      $userdata=$this->session->userdata('user');
      if(empty($userdata))
      {
          redirect('common/login');
      }

      $uId = $this->uri->segment('3');

      $universityObj = $this->webinar_model->get(["id" => $uId], "universities");
      $universityId = $universityObj['data'][0]['id'];
      $universityInfoObj = $this->webinar_model->get(["university_id" => $universityId], "university_info");
      $universityOverviewInfoObj = $this->webinar_model->get(["university_id" => $universityId], "university_overview_info");
      $universityScholarshipsFinancialInfoObj = $this->webinar_model->get(["university_id" => $universityId], "university_scholarships_financial_info");
      $universityFaqInfoObj = $this->webinar_model->get(["university_id" => $universityId], "university_faq_info");
      $universitySeoInfoObj = $this->webinar_model->get(["entity_id" => $universityId], "university_seo");
      $universityAdmissionInfoObj = $this->webinar_model->get(["university_id" => $universityId], "university_admission_info");

      if($universityOverviewInfoObj['data']){
        $universityOverviewInfoObj['data'][0]['average_eligibility'] = json_decode($universityOverviewInfoObj['data'][0]['average_eligibility'], true);
      }

      if(count(json_decode($universityFaqInfoObj['data'][0]['other_details'], true)) == 0){
        $faqQusCount = 1;
      }else{
        $faqQusCount = count(json_decode($universityFaqInfoObj['data'][0]['other_details'], true));
      }

      if(count(json_decode($universityFaqInfoObj['data'][0]['latest_news'], true)) == 0){
        $latestNewsCount = 1;
      }else{
        $latestNewsCount = count(json_decode($universityFaqInfoObj['data'][0]['latest_news'], true));
      }

      $this->outputData['university'] = $universityObj['data'];
      $this->outputData['university_info'] = $universityInfoObj['data'];
      $this->outputData['university_ranking'] = json_decode($universityInfoObj['data'][0]['university_ranking']);
      $this->outputData['university_overview_info'] = $universityOverviewInfoObj['data'];
      $this->outputData['university_scholarships_financial_info'] = $universityScholarshipsFinancialInfoObj['data'];
      $this->outputData['university_faq_info'] = $universityFaqInfoObj['data'];
      $this->outputData['university_seo_info'] = $universitySeoInfoObj['data'];
      $this->outputData['university_admission_info'] = $universityAdmissionInfoObj['data'];
      $this->outputData['university_social_media_info'] = $universitySocialMediaInfoObj['data'];
      $this->outputData['enrollment_count'] = $faqQusCount;
      $this->outputData['latest_news_count'] = $latestNewsCount;
      $this->outputData['faqQusAns'] = json_decode($universityFaqInfoObj['data'][0]['other_details'], true);
      $this->outputData['latestNews'] = json_decode($universityFaqInfoObj['data'][0]['latest_news'], true);
//echo "<pre>"; print_r(json_decode($universityFaqInfoObj['data'][0]['other_details'], true));

      //var_dump($this->outputData);die();

      $this->render_page('templates/university/university_info', $this->outputData);

    }

    function updateInfo(){

      $userdata=$this->session->userdata('user');
      if(empty($userdata))
      {
          redirect('common/login');
      }

      $universityId = $this->input->post('u_id') ? $this->input->post('u_id') : NULL;
      $tabName = $this->input->post('tab_name') ? $this->input->post('tab_name') : NULL;
      $tabValue = $this->input->post('tab_value') ? $this->input->post('tab_value') : NULL;

      $universityInfoObj = $this->webinar_model->upsert(["university_id" => $universityId], [ "university_id" => $universityId, $tabName => $tabValue ], "university_info");
      $this->outputData['university_info'] = $universityInfoObj['data'];

      $responseObj['status'] = "SUCCESS" ;

      header('Content-Type: application/json');
      echo json_encode($responseObj);

    }

    function updateInfoRanking(){
      $userdata=$this->session->userdata('user');
      if(empty($userdata))
      {
          redirect('common/login');
      }

      $uni_ranking_info = [];

      $university_id = $this->input->post('university_id') ? $this->input->post('university_id') : NULL;
      $tab_three = $this->input->post('tab_three') ? $this->input->post('tab_three') : NULL;
      $uni_ranking_info['qs_global_value'] = $this->input->post('qs_global_value') ? $this->input->post('qs_global_value') : NULL;
      $uni_ranking_info['usNews_global_value'] = $this->input->post('usNews_global_value') ? $this->input->post('usNews_global_value') : NULL;
      $uni_ranking_info['usNews_national_value'] = $this->input->post('usNews_national_value') ? $this->input->post('usNews_national_value') : NULL;
      $uni_ranking_info['the_value'] = $this->input->post('the_value') ? $this->input->post('the_value') : NULL;
      $uni_ranking_info['arwu_value'] = $this->input->post('arwu_value') ? $this->input->post('arwu_value') : NULL;

      $universityInfoObj = $this->webinar_model->upsert(["university_id" => $university_id], [ "university_id" => $university_id, "tab_three" => $tab_three, "tab_three" => $tab_three, "university_ranking" => json_encode($uni_ranking_info) ], "university_info");

      $this->outputData['university_info'] = $universityInfoObj['data'];

      $responseObj['status'] = "SUCCESS" ;

      header('Content-Type: application/json');
      echo json_encode($responseObj);

    }

    function updateInfoOverview(){

      $userdata=$this->session->userdata('user');
      if(empty($userdata))
      {
          redirect('common/login');
      }

      $userId = $userdata['id'];

      $university_id = $this->input->post('university_id') ? $this->input->post('university_id') : NULL;
      $total_students = $this->input->post('total_students') ? $this->input->post('total_students') : NULL;
      $international_students = $this->input->post('international_students') ? $this->input->post('international_students') : NULL;
      $total_degree_programs = $this->input->post('total_degree_programs') ? $this->input->post('total_degree_programs') : NULL;
      $establishment_year = $this->input->post('establishment_year') ? $this->input->post('establishment_year') : NULL;
      $type = $this->input->post('type') ? $this->input->post('type') : NULL;
      $location = $this->input->post('location') ? $this->input->post('location') : NULL;
      $acceptance_rate = $this->input->post('acceptance_rate') ? $this->input->post('acceptance_rate') : NULL;
      $internships = $this->input->post('internships') ? $this->input->post('internships') : NULL;
      $application_fee = $this->input->post('application_fee') ? $this->input->post('application_fee') : NULL;
      $average_tuition_fee = $this->input->post('average_tuition_fee') ? $this->input->post('average_tuition_fee') : NULL;
      $average_cost_living = $this->input->post('average_cost_living') ? $this->input->post('average_cost_living') : NULL;

      $general_admission_req_ug = $this->input->post('general_admission_req_ug') ? $this->input->post('general_admission_req_ug') : NULL;
      $general_admission_req_pg = $this->input->post('general_admission_req_pg') ? $this->input->post('general_admission_req_pg') : NULL;

      $overview_paragraph_one = $this->input->post('overview_paragraph_one') ? $this->input->post('overview_paragraph_one') : NULL;
      $overview_paragraph_two = $this->input->post('overview_paragraph_two') ? $this->input->post('overview_paragraph_two') : NULL;
      $overview_paragraph_three = $this->input->post('overview_paragraph_three') ? $this->input->post('overview_paragraph_three') : NULL;

      $read_more_international_students = $this->input->post('read_more_international_students') ? $this->input->post('read_more_international_students') : NULL;
      $read_more_student_life = $this->input->post('read_more_student_life') ? $this->input->post('read_more_student_life') : NULL;
      $read_more_employment_figures = $this->input->post('read_more_employment_figures') ? $this->input->post('read_more_employment_figures') : NULL;
      $read_more_programs = $this->input->post('read_more_programs') ? $this->input->post('read_more_programs') : NULL;
      $read_more_research = $this->input->post('read_more_research') ? $this->input->post('read_more_research') : NULL;
      $read_more_alumni = $this->input->post('read_more_alumni') ? $this->input->post('read_more_alumni') : NULL;
      $assistantantships = $this->input->post('assistantantships') ? $this->input->post('assistantantships') : NULL;

      $average_eligibility_pg_gpa = $this->input->post('average_eligibility_pg_gpa') ? $this->input->post('average_eligibility_pg_gpa') : NULL;
      $average_eligibility_pg_gre = $this->input->post('average_eligibility_pg_gre') ? $this->input->post('average_eligibility_pg_gre') : NULL;
      $average_eligibility_ug_gpa = $this->input->post('average_eligibility_ug_gpa') ? $this->input->post('average_eligibility_ug_gpa') : NULL;
      $average_eligibility_ug_gre = $this->input->post('average_eligibility_ug_gre') ? $this->input->post('average_eligibility_ug_gre') : NULL;

      $average_eligibility = [
        "pg_gpa" => $average_eligibility_pg_gpa,
        "pg_gre" => $average_eligibility_pg_gre,
        "ug_gpa" => $average_eligibility_ug_gpa,
        "ug_gre" => $average_eligibility_ug_gre
      ];

      $upsertData = [

        "university_id" => $university_id,
        "total_students" => $total_students,
        "international_students" => $international_students,
        "total_degree_programs" => $total_degree_programs,
        "establishment_year" => $establishment_year,
        "type" => $type,
        "location" => $location,
        "acceptance_rate" => $acceptance_rate,
        "internships" => $internships,
        "application_fee" => $application_fee,
        "average_tuition_fee" => $average_tuition_fee,
        "average_cost_living" => $average_cost_living,
        "average_eligibility" => json_encode($average_eligibility),

        "general_admission_req_ug" => $general_admission_req_ug,
        "general_admission_req_pg" => $general_admission_req_pg,

        "overview_paragraph_one" => $overview_paragraph_one,
        "overview_paragraph_two" => $overview_paragraph_two,
        "overview_paragraph_three" => $overview_paragraph_three,
        "assistantantships" => $assistantantships,
        "read_more_international_students" => $read_more_international_students,
        "read_more_student_life" => $read_more_student_life,
        "read_more_employment_figures" => $read_more_employment_figures,
        "read_more_programs" => $read_more_programs,
        "read_more_research" => $read_more_research,
        "read_more_alumni" => $read_more_alumni,
        "created_by" => $userId

      ];


      $universityInfoObj = $this->webinar_model->upsert(["university_id" => $university_id], $upsertData, "university_overview_info");

      $responseObj['status'] = "SUCCESS" ;

      header('Content-Type: application/json');
      echo json_encode($responseObj);

    }


    function updateInfoScholarshipsFinancial(){

      $userdata=$this->session->userdata('user');
      if(empty($userdata))
      {
          redirect('common/login');
      }

      $userId = $userdata['id'];

      $university_id = $this->input->post('university_id') ? $this->input->post('university_id') : NULL;
      $financial_assistance = $this->input->post('financial_assistance') ? $this->input->post('financial_assistance') : NULL;
      $financial_aid = $this->input->post('financial_aid') ? $this->input->post('financial_aid') : NULL;
      $accommodations_offered = $this->input->post('accommodations_offered') ? $this->input->post('accommodations_offered') : NULL;
      $on_campus = $this->input->post('on_campus') ? $this->input->post('on_campus') : NULL;
      $off_campus = $this->input->post('off_campus') ? $this->input->post('off_campus') : NULL;


      $upsertData = [

        "university_id" => $university_id,
        "financial_assistance" => $financial_assistance,
        "financial_aid" => $financial_aid,
        "accommodations_offered" => $accommodations_offered,
        "on_campus" => $on_campus,
        "off_campus" => $off_campus,
        "created_by" => $userId

      ];


      $universityInfoObj = $this->webinar_model->upsert(["university_id" => $university_id], $upsertData, "university_scholarships_financial_info");

      $responseObj['status'] = "SUCCESS" ;

      header('Content-Type: application/json');
      echo json_encode($responseObj);

    }


    function updateInfoFaq(){

      $userdata=$this->session->userdata('user');
      if(empty($userdata))
      {
          redirect('common/login');
      }

      $userId = $userdata['id'];

      $faqQusAns = array();

      $university_id = $this->input->post('university_id') ? $this->input->post('university_id') : NULL;


      foreach($this->input->post('question') AS $index => $faqQus)
       {
           $faqQusAns[$index]['question'] = $faqQus;
           $faqQusAns[$index]['answer'] = $this->input->post('answer')[$index];
       }

      // echo "<pre>"; print_r($faqQusAns); exit;

      $q_one = $this->input->post('q_one') ? $this->input->post('q_one') : NULL;
      $a_one = $this->input->post('a_one') ? $this->input->post('a_one') : NULL;

     /* $q_two = $this->input->post('q_two') ? $this->input->post('q_two') : NULL;
      $a_two = $this->input->post('a_two') ? $this->input->post('a_two') : NULL;

      $q_three = $this->input->post('q_three') ? $this->input->post('q_three') : NULL;
      $a_three = $this->input->post('a_three') ? $this->input->post('a_three') : NULL;

      $q_four = $this->input->post('q_four') ? $this->input->post('q_four') : NULL;
      $a_four = $this->input->post('a_four') ? $this->input->post('a_four') : NULL;

      $q_five = $this->input->post('q_five') ? $this->input->post('q_five') : NULL;
      $a_five = $this->input->post('a_five') ? $this->input->post('a_five') : NULL;

      $q_six = $this->input->post('q_six') ? $this->input->post('q_six') : NULL;
      $a_six = $this->input->post('a_six') ? $this->input->post('a_six') : NULL;

      $q_seven = $this->input->post('q_seven') ? $this->input->post('q_seven') : NULL;
      $a_seven = $this->input->post('a_seven') ? $this->input->post('a_seven') : NULL;

      $q_eight = $this->input->post('q_eight') ? $this->input->post('q_eight') : NULL;
      $a_eight = $this->input->post('a_eight') ? $this->input->post('a_eight') : NULL;*/

      $upsertData = [

        "university_id" => $university_id,
        "q_one" => $q_one,
        "a_one" => $a_one,
        "other_details" => json_encode($faqQusAns),
        "created_by" => $userId

      ];


      $universityInfoObj = $this->webinar_model->upsert(["university_id" => $university_id], $upsertData, "university_faq_info");


      $responseObj['status'] = "SUCCESS" ;

      header('Content-Type: application/json');
      echo json_encode($responseObj);

    }


    function updateInfoLatest(){

      $userdata=$this->session->userdata('user');
      if(empty($userdata))
      {
          redirect('common/login');
      }

      $userId = $userdata['id'];

      $latestNews = array();

      $university_id = $this->input->post('university_id') ? $this->input->post('university_id') : NULL;


      foreach($this->input->post('question') AS $index => $faqQus)
       {
           $latestNews[$index]['question'] = $faqQus;
           $latestNews[$index]['answer'] = $this->input->post('answer')[$index];
           $latestNews[$index]['origineDate'] = $this->input->post('origineDate')[$index];
           $latestNews[$index]['archive'] = $this->input->post('archive')[$index];
       }

       //echo "<pre>"; print_r($faqQusAns); exit;


      $upsertData = [

        "university_id" => $university_id,
        "latest_news" => json_encode($latestNews),
        "created_by" => $userId

      ];


      $universityInfoObj = $this->webinar_model->upsert(["university_id" => $university_id], $upsertData, "university_faq_info");


      $responseObj['status'] = "SUCCESS" ;

      header('Content-Type: application/json');
      echo json_encode($responseObj);

    }

    function updateInfoSeo(){

      $userdata=$this->session->userdata('user');
      if(empty($userdata))
      {
          redirect('common/login');
      }

      $userId = $userdata['id'];

      $university_id = $this->input->post('university_id') ? $this->input->post('university_id') : NULL;

      $seo_title = $this->input->post('seo_title') ? $this->input->post('seo_title') : NULL;
      $seo_description = $this->input->post('seo_description') ? $this->input->post('seo_description') : NULL;
      $seo_canonical = $this->input->post('seo_canonical') ? $this->input->post('seo_canonical') : NULL;
      $seo_index_option = $this->input->post('seo_index_option') ? $this->input->post('seo_index_option') : NULL;
      $seo_schema = $this->input->post('seo_schema') ? $this->input->post('seo_schema') : NULL;


      $upsertData = [

        "entity_id" => $university_id,
        "entity_type" => "UNIVERSITY",
        "seo_title" => $seo_title,
        "seo_description" => $seo_description,
        "seo_canonical" => $seo_canonical,
        "seo_index_option" => $seo_index_option,
        "seo_schema" => $seo_schema,
        "status" => 1,
        "date_created" => date('Y-m-d H:i:s')

      ];


      $universityInfoObj = $this->webinar_model->upsert(["entity_id" => $university_id], $upsertData, "university_seo");

      $responseObj['status'] = "SUCCESS" ;

      header('Content-Type: application/json');
      echo json_encode($responseObj);

    }


    function updateInfoUniversityImage(){

      $userdata=$this->session->userdata('user');
      if(empty($userdata))
      {
          redirect('common/login');
      }

      $userId = $userdata['id'];

      $universityId = $this->input->post('university_id') ? $this->input->post('university_id') : NULL;

      if($_FILES['banner']){

        if (!file_exists('./uploads/university_images_' . $universityId))
        {
          mkdir('./uploads/university_images_' . $universityId, 0777, true);
        }

          $uploadFilePath     = './uploads/university_images_' . $universityId;
          $uploadPath         = '/uploads/university_images_' . $universityId;
          $pathInfo           = pathinfo($_FILES['banner']['name']);
          $tempfilename       = preg_replace('/\s+/', '_', $pathInfo['filename']);
          $tempfilename       = str_replace('.', '_', $tempfilename);
          $tempfilename       = str_replace('&', '_', $tempfilename);
          $tempfilename       = str_replace('-', '_', $tempfilename);
          $tempfilename       = str_replace('+', '_', $tempfilename);

          $fileExt                    = $pathInfo['extension'];
          $fileName                   = $universityId . '_' . $tempfilename . '.' . $fileExt;
          $config['upload_path']      = $uploadFilePath;
          $config['file_name']        = $fileName;
          $config['allowed_types']    = 'gif|jpg|png|JPEG|JPG|jpeg|pdf|PDF';
          //$config['max_size']         = 2048;
          //$config['max_width']        = 10240;
          //$config['max_height']       = 7680;

          $this->load->library('upload', $config);

          $universityAppProcessDocument = $this->webinar_model->get(["id" => $universityId], "universities");

          if($universityAppProcessDocument['data'])
          {

              $oldFile = $_SERVER['DOCUMENT_ROOT'] . $uploadPath . '/' . $universityAppProcessDocument['data'][0][$fName];

              if(file_exists($oldFile))
              {
                  unlink($oldFile);
              }
          }

          if(!$this->upload->do_upload('banner'))
          {
              //$this->outputData['document_error'] = $this->upload->display_errors();
              echo $this->upload->display_errors();
              exit();
          }
          else
          {

                  $insertData = [
                      "id"           => $universityId,
                      "banner"      => base_url() . $uploadPath . '/' . $fileName,
                  ];

                  $universityInfoObj = $this->webinar_model->upsert(["id" => $universityId], $insertData, "universities");



          }

      }

      if($_FILES['logo']){

        if (!file_exists('./uploads/university_images_' . $universityId))
        {
          mkdir('./uploads/university_images_' . $universityId, 0777, true);
        }

          $uploadFilePath     = './uploads/university_images_' . $universityId;
          $uploadPath         = '/uploads/university_images_' . $universityId;
          $pathInfo           = pathinfo($_FILES['logo']['name']);
          $tempfilename       = preg_replace('/\s+/', '_', $pathInfo['filename']);
          $tempfilename       = str_replace('.', '_', $tempfilename);
          $tempfilename       = str_replace('&', '_', $tempfilename);
          $tempfilename       = str_replace('-', '_', $tempfilename);
          $tempfilename       = str_replace('+', '_', $tempfilename);

          $fileExt                    = $pathInfo['extension'];
          $fileName                   = $universityId . '_' . $tempfilename . '.' . $fileExt;
          $config['upload_path']      = $uploadFilePath;
          $config['file_name']        = $fileName;
          $config['allowed_types']    = 'gif|jpg|png|JPEG|JPG|jpeg|pdf|PDF';
          //$config['max_size']         = 2048;
          //$config['max_width']        = 10240;
          //$config['max_height']       = 7680;

          $this->load->library('upload', $config);

          $universityAppProcessDocument = $this->webinar_model->get(["id" => $universityId], "universities");

          if($universityAppProcessDocument['data'])
          {

              $oldFile = $_SERVER['DOCUMENT_ROOT'] . $uploadPath . '/' . $universityAppProcessDocument['data'][0][$fName];

              if(file_exists($oldFile))
              {
                  unlink($oldFile);
              }
          }

          if(!$this->upload->do_upload('logo'))
          {
              //$this->outputData['document_error'] = $this->upload->display_errors();
              echo $this->upload->display_errors();
              exit();
          }
          else
          {
                  $insertData = [
                      "id"           => $universityId,
                      "logo"      => base_url() . $uploadPath . '/' . $fileName,
                  ];

                  $universityInfoObj = $this->webinar_model->upsert(["id" => $universityId], $insertData, "universities");



          }
      }

        $responseObj['status'] = "SUCCESS" ;

        header('Content-Type: application/json');
        echo json_encode($responseObj);


    }

    function updateInfoSocialMedia(){

      $userdata=$this->session->userdata('user');
      if(empty($userdata))
      {
          redirect('common/login');
      }

      $userId = $userdata['id'];

      $university_id = $this->input->post('university_id') ? $this->input->post('university_id') : NULL;

      $campus_youtube = $this->input->post('campus_youtube') ? $this->input->post('campus_youtube') : NULL;
      $smm_facebook = $this->input->post('smm_facebook') ? $this->input->post('smm_facebook') : NULL;
      $smm_twitter = $this->input->post('smm_twitter') ? $this->input->post('smm_twitter') : NULL;
      $smm_instagram = $this->input->post('smm_instagram') ? $this->input->post('smm_instagram') : NULL;
      $smm_youtube = $this->input->post('smm_youtube') ? $this->input->post('smm_youtube') : NULL;
      $smm_linkedin = $this->input->post('smm_linkedin') ? $this->input->post('smm_linkedin') : NULL;
      $smm_whats = $this->input->post('smm_whats') ? $this->input->post('smm_whats') : NULL;


      $upsertData = [

        "university_id" => $university_id,
        "campus_youtube" => $campus_youtube,
        "smm_facebook" => $smm_facebook,
        "smm_twitter" => $smm_twitter,
        "smm_instagram" => $smm_instagram,
        "smm_youtube" => $smm_youtube,
        "smm_linkedin" => $smm_linkedin,
        "smm_whats" => $smm_whats

      ];


      $universityInfoObj = $this->webinar_model->upsert(["university_id" => $university_id], $upsertData, "university_aditional_data");

      $responseObj['status'] = "SUCCESS" ;

      header('Content-Type: application/json');
      echo json_encode($responseObj);

    }


    function updateInfoAdmission(){

      $userdata=$this->session->userdata('user');
      if(empty($userdata))
      {
          redirect('common/login');
      }

      $userId = $userdata['id'];

      $university_id = $this->input->post('university_id') ? $this->input->post('university_id') : NULL;

      $a_one = $this->input->post('a_one') ? $this->input->post('a_one') : NULL;
      $a_two = $this->input->post('a_two') ? $this->input->post('a_two') : NULL;
      $a_three = $this->input->post('a_three') ? $this->input->post('a_three') : NULL;
      $a_four = $this->input->post('a_four') ? $this->input->post('a_four') : NULL;

      $a_five = $this->input->post('a_five') ? $this->input->post('a_five') : NULL;
      $a_five_a = $this->input->post('a_five_a') ? $this->input->post('a_five_a') : NULL;
      $a_five_b = $this->input->post('a_five_b') ? $this->input->post('a_five_b') : NULL;
      $a_five_c = $this->input->post('a_five_c') ? $this->input->post('a_five_c') : NULL;
      $a_five_d = $this->input->post('a_five_d') ? $this->input->post('a_five_d') : NULL;

      $a_six = $this->input->post('a_six') ? $this->input->post('a_six') : NULL;
      $a_six_a = $this->input->post('a_six_a') ? $this->input->post('a_six_a') : NULL;

      $a_seven = $this->input->post('a_seven') ? $this->input->post('a_seven') : NULL;
      $a_eight = $this->input->post('a_eight') ? $this->input->post('a_eight') : NULL;
      $a_nine = $this->input->post('a_nine') ? $this->input->post('a_nine') : NULL;
      $a_ten = $this->input->post('a_ten') ? $this->input->post('a_ten') : NULL;

      $upsertData = [

        "university_id" => $university_id,
        "a_one" => $a_one,
        "a_two" => $a_two,
        "a_three" => $a_three,
        "a_four" => $a_four,

        "a_five" => $a_five,
        "a_five_a" => $a_five_a,
        "a_five_b" => $a_five_b,
        "a_five_c" => $a_five_c,
        "a_five_d" => $a_five_d,

        "a_six" => $a_six,
        "a_six_a" => $a_six_a,

        "a_seven" => $a_seven,
        "a_eight" => $a_eight,
        "a_nine" => $a_nine,
        "a_ten" => $a_ten,

        "created_by" => $userId

      ];


      $universityInfoObj = $this->webinar_model->upsert(["university_id" => $university_id], $upsertData, "university_admission_info");

      $responseObj['status'] = "SUCCESS" ;

      header('Content-Type: application/json');
      echo json_encode($responseObj);

    }

    function universityProcessInfo(){

      $userdata=$this->session->userdata('user');
      if(empty($userdata))
      {
          redirect('common/login');
      }

      $universityProcessInfoList = $this->universitydata_model->universityProcessInfoList();

      $this->outputData['view'] = 'list';
      $this->outputData['university_process_info_list'] = $universityProcessInfoList['data'];

      $this->render_page('templates/university/university_process_info_list', $this->outputData);

    }

    function deleteUniversityKey(){

      $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $universityId = $this->uri->segment('4');

        $universityDetailKey = "university-detail-".$universityId;
        $universityDetailDistroyKey = $this->redi_model->delLogin($universityDetailKey);

        $universitySeoKey = "university-seo-".$universityId;
        $university_seoDistroyKey = $this->redi_model->delLogin($universitySeoKey);

        $this->session->set_flashdata('flash_message', "University Detail & SEO Key Deleted Successfully!!!");

        redirect(base_url('user/university/'));
    }

    function deleteUniversityCacheListKey(){

      $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $universityDetailKey = "university-list";
        $universityDetailDistroyKey = $this->redi_model->delLogin($universityDetailKey);

        $this->session->set_flashdata('flash_message', "University Cache List Deleted Successfully!!!");

        redirect(base_url('user/university/'));
    }

    function deleteCourseCacheListKey(){

      $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $universityDetailKey = "course-list";
        $universityDetailDistroyKey = $this->redi_model->delLogin($universityDetailKey);

        $this->session->set_flashdata('flash_message', "Course Cache List Deleted Successfully!!!");

        redirect(base_url('user/university/'));
    }

    function universityProcessInfoForm(){

      $userdata=$this->session->userdata('user');
      if(empty($userdata))
      {
          redirect('common/login');
      }

      $uId = $this->uri->segment('4');

      $universityObj = $this->webinar_model->get(["id" => $uId], "universities");
      $universityId = $universityObj['data'][0]['id'];
      $universityApplicationProcessInfoObj = $this->webinar_model->get(["entity_id" => $universityId, "entity_type"=> 'UNIVERSITY'], "university_application_process_info");

      if($universityApplicationProcessInfoObj['data']){
        $universityApplicationProcessInfoObj['data'][0]['application_fill_option'] = json_decode($universityApplicationProcessInfoObj['data'][0]['application_fill_option'],true);
      }

      $this->outputData['university'] = $universityObj['data'];
      $this->outputData['university_application_process_info'] = $universityApplicationProcessInfoObj['data'];

      $this->render_page('templates/university/university_process_info_form', $this->outputData);

    }

    function universityProcessInfoAdd(){

        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $universityId = $this->input->post('u_id') ? $this->input->post('u_id') : NULL;
        $fName = $this->input->post('f_name') ? $this->input->post('f_name') : NULL;
        $fValue = $this->input->post('f_value') ? $this->input->post('f_value') : NULL;

        if($fName === 'application_fill_option'){
          $fValue = json_encode($fValue);
        }

        $universityInfoObj = $this->webinar_model->upsert(["entity_id" => $universityId, "entity_type" => "UNIVERSITY"], [ "entity_id" => $universityId, "entity_type" => "UNIVERSITY", $fName => $fValue ], "university_application_process_info");

        $responseObj['status'] = "SUCCESS" ;

        header('Content-Type: application/json');
        echo json_encode($responseObj);

    }

    function appProcessDocUpdate()
    {

    $this->load->helper('cookie_helper');
		$this->load->model('user/user_model');
		$userdata = $this->session->userdata('user');
	    if(!$userdata)
        {
            redirect('user/account');
        }

        $postVariables = $this->input->post();

        $universityId = $postVariables['university_id'];
        $docName = $postVariables['doc_name'];
        $fName = $postVariables['f_name'];


		if (!file_exists('./uploads/university_' . $universityId))
        {
    		mkdir('./uploads/university_' . $universityId, 0777, true);
		}

        $uploadFilePath     = './uploads/university_' . $universityId;
        $uploadPath         = '/uploads/university_' . $universityId;
        $pathInfo           = pathinfo($_FILES['document']['name']);
        $tempfilename       = preg_replace('/\s+/', '_', $pathInfo['filename']);
        $tempfilename       = str_replace('.', '_', $tempfilename);
        $tempfilename       = str_replace('&', '_', $tempfilename);
        $tempfilename       = str_replace('-', '_', $tempfilename);
        $tempfilename       = str_replace('+', '_', $tempfilename);

        $fileExt                    = $pathInfo['extension'];
        $fileName                   = $universityId . '_' . $docName . '_' . $tempfilename . '.' . $fileExt;
        $config['upload_path']      = $uploadFilePath;
        $config['file_name']        = $fileName;
        $config['allowed_types']    = 'gif|jpg|png|JPEG|JPG|jpeg|pdf|PDF';
        $config['max_size']         = 2048;
        $config['max_width']        = 10240;
        $config['max_height']       = 7680;

        $this->load->library('upload', $config);

        $universityAppProcessDocument = $this->webinar_model->get(["entity_id" => $universityId, "entity_type"=> 'UNIVERSITY'], "university_application_process_info");

        if($universityAppProcessDocument['data'])
        {
            $oldFile = $_SERVER['DOCUMENT_ROOT'] . $uploadPath . '/' . $universityAppProcessDocument['data'][0][$fName];
            if(file_exists($oldFile))
            {
                unlink($oldFile);
            }
        }

        if(!$this->upload->do_upload('document'))
        {
            //$this->outputData['document_error'] = $this->upload->display_errors();
            echo $this->upload->display_errors();
            exit();
        }
        else
        {

                $insertData = [
                    "entity_id"           => $universityId,
                    "entity_type"              => 'UNIVERSITY',
                    "$fName"      => base_url() . $uploadPath . '/' . $fileName,
                ];

                $universityInfoObj = $this->webinar_model->upsert(["entity_id" => $universityId, "entity_type" => "UNIVERSITY"], $insertData, "university_application_process_info");

                $responseObj['status'] = "SUCCESS" ;

                header('Content-Type: application/json');
                echo json_encode($responseObj);

        }
    }

  }

?>
