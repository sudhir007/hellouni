<?php

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

require_once APPPATH . 'libraries/Mail/sMail.php';
class Additionaldata extends MY_Controller
{
  public $outputData;
  public $loggedInUser;

  function __construct()
  {
    parent::__construct();
    $this->load->library(array('template', 'form_validation'));
    $this->lang->load('enduser/home', $this->config->item('language_code'));
    $this->load->helper(array('cookie_helper', 'form', 'url'));
    $this->load->model(array('user/user_model', 'universitydata_model'));
  }

  function additionaldatauniversitylist()
  {

    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    $universityAdditionalDataList = $this->universitydata_model->getUniversityAddDataList();
    $this->outputData['university_aditional_data_list'] = $universityAdditionalDataList['data'];

    $this->render_page('templates/university/additional_data_list', $this->outputData);
  }

  function additionaldataform()
  {
    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    $universityId = isset($userdata['type']) && $userdata['type'] == '3' ? $userdata['university_id'] : $this->uri->segment('4');

    //$universityAdditionalData = $this->universitydata_model->get(['university_id' => $universityId],'university_aditional_data');
    $universityAdditionalData = $this->universitydata_model->getUniversityAddData($universityId);
    //echo "<pre>"; print_r($universityAdditionalData); exit;
    if ($universityAdditionalData['data']) {

      $universityAdditionalData['data'][0]['general_brochures'] = json_decode($universityAdditionalData['data'][0]['general_brochures'], true);
      $universityAdditionalData['data'][0]['pg_brochures'] = json_decode($universityAdditionalData['data'][0]['pg_brochures'], true);
      $universityAdditionalData['data'][0]['ug_brochures'] = json_decode($universityAdditionalData['data'][0]['ug_brochures'], true);
      $universityAdditionalData['data'][0]['course_catlog'] = json_decode($universityAdditionalData['data'][0]['course_catlog'], true);
      $universityAdditionalData['data'][0]['course_matrix'] = json_decode($universityAdditionalData['data'][0]['course_matrix'], true);
      $universityAdditionalData['data'][0]['general_videos'] = json_decode($universityAdditionalData['data'][0]['general_videos'], true);
      $universityAdditionalData['data'][0]['testimonial_videos'] = json_decode($universityAdditionalData['data'][0]['testimonial_videos'], true);
      $universityAdditionalData['data'][0]['campus_photos'] = json_decode($universityAdditionalData['data'][0]['campus_photos'], true);
      $universityAdditionalData['data'][0]['student_life_photos'] = json_decode($universityAdditionalData['data'][0]['student_life_photos'], true);
      $universityAdditionalData['data'][0]['convocation_photos'] = json_decode($universityAdditionalData['data'][0]['convocation_photos'], true);
      $universityAdditionalData['data'][0]['event_photos'] = json_decode($universityAdditionalData['data'][0]['event_photos'], true);
      $universityAdditionalData['data'][0]['facilities_labs_research_center'] = json_decode($universityAdditionalData['data'][0]['facilities_labs_research_center'], true);
      $universityAdditionalData['data'][0]['college_pg_list'] = json_decode($universityAdditionalData['data'][0]['college_pg_list'], true);
      $universityAdditionalData['data'][0]['college_ug_list'] = json_decode($universityAdditionalData['data'][0]['college_ug_list'], true);
      $universityAdditionalData['data'][0]['university_banner'] = json_decode($universityAdditionalData['data'][0]['university_banner'], true);
      $universityAdditionalData['data'][0]['university_logo'] = json_decode($universityAdditionalData['data'][0]['university_logo'], true);
      $universityAdditionalData['data'][0]['university_map_preview'] = json_decode($universityAdditionalData['data'][0]['university_map_preview'], true);


      $this->outputData['universityAdditionalData'] = $universityAdditionalData['data'][0];
    }

    $this->outputData['universityId'] = $universityId;
    $this->outputData['universityName'] = $universityAdditionalData['data'][0]['university_name'];
    // echo "<pre>"; print_r($this->outputData); exit;
    $this->render_page('templates/university/additional_data', $this->outputData);
  }
  
  function uploadDoc()
  {
    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    $errorList = [];
    $imageUrl = [];
    $universityId = (isset($userdata['type']) && $userdata['type'] == '3') ? $userdata['university_id'] : $this->input->post('university_id');
    $docType = $this->input->post('document_type');
    $alt_tag = $this->input->post('alt_tag');

    if (isset($_FILES['documents']['name']) && !empty($_FILES['documents']['name'])) {
      $this->load->library('upload');
      $all_images = $_FILES['documents']['name'];
      $total_images = sizeof($all_images);

      for ($i = 0; $i < $total_images; $i++) {
        if (!empty($_FILES['documents']['name'][$i])) {
          $uploadDir = './../uploads/university_' . $universityId;
          if (!file_exists($uploadDir)) {
            mkdir($uploadDir, 0777, true);
          }

          $config = array();
          $uploadFilePath = $uploadDir; 
          $pathInfo = pathinfo($_FILES['documents']['name'][$i]);
          $tempfilename = preg_replace('/\s+/', '_', $pathInfo['filename']);
          $tempfilename = str_replace(array('.', '&', '-', '+'), '_', $tempfilename);

          $imageId = mt_rand(10000, 99999);
          $fileExt = $pathInfo['extension'];
          $fileName = 'university_' . $universityId . '_' . $docType . '_' . $imageId . '_' . $i . '.' . $fileExt;

          $config['upload_path']   = $uploadFilePath;
          $config['file_name']     = $fileName;
          $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|doc|docx|webp';
          $config['max_size']      = 10240;

          $this->upload->initialize($config);

          $_FILES['images']['name']     = $_FILES['documents']['name'][$i];
          $_FILES['images']['type']     = $_FILES['documents']['type'][$i];
          $_FILES['images']['tmp_name'] = $_FILES['documents']['tmp_name'][$i];
          $_FILES['images']['error']    = $_FILES['documents']['error'][$i];
          $_FILES['images']['size']     = $_FILES['documents']['size'][$i];

          if (!$this->upload->do_upload('images')) {
            $errorList[] = strip_tags($this->upload->display_errors());

            $response["status"]    = "FAILED";
            $response["data"]      = $errorList;
            $response["file_name"] = $_FILES['images']['name'];
            header('Content-Type: application/json');
            echo json_encode($response);
            exit();
          } else {
            $brochDetails = $this->input->post('brochures_details');
            $photoUrl = str_replace('admin/', '', base_url()) . 'uploads/university_' . $universityId . '/university_' . $universityId . '_' . $docType . '_' . $imageId . '_' . $i . '.' . $fileExt;

            $imageUrl[] = [
              "image_id"      => $imageId,
              "image_url"     => $photoUrl,
              "image_details" => $brochDetails,
              "alt_tag" => $alt_tag
            ];
          }
        }
      }

      if ($docType == 'banner' || $docType == 'logo' || $docType == 'map_preview') {
        $imageData = $this->universitydata_model->getImageData(["id" => $universityId], $docType, "universities");
        $imageIdValue = !empty($imageData['data'][0][$docType]) ? json_decode($imageData['data'][0][$docType], true) : [];
       
        $finalImageArray = $imageUrl;
        $docEntryObj = $this->universitydata_model->upsert(['id' => $universityId], ['id' => $universityId, $docType => json_encode($finalImageArray)], 'universities');
      } else {
        $imageData = $this->universitydata_model->getImageData(["university_id" => $universityId], $docType, "university_aditional_data");
        $imageIdValue = !empty($imageData['data'][0][$docType]) ? json_decode($imageData['data'][0][$docType], true) : [];
        
        $finalImageArray = array_merge($imageIdValue, $imageUrl);
        $docEntryObj = $this->universitydata_model->upsert(['university_id' => $universityId], ['university_id' => $universityId, $docType => json_encode($finalImageArray)], 'university_aditional_data');
      }

      $response["status"] = "SUCCESS";
      $response["docType"] = $docType;
      $response["data"] = $finalImageArray;
      $response["university_id"] = $universityId;
      header('Content-Type: application/json');
      echo json_encode($response);
    } else {
      
      $response["status"] = "FAILED";
      $response["data"] = "No file selected.";
      header('Content-Type: application/json');
      echo json_encode($response);
    }
  }

  function updateImageData()
  {
    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    $universityId = (isset($userdata['type']) && $userdata['type'] == '3') ? $userdata['university_id'] : $this->input->post('university_id');
    $columnName   = $this->input->post('column_name'); 
    $imageId      = $this->input->post('image_id');
    $imageDetails = $this->input->post('image_details');
    $alt_tag      = $this->input->post('alt_tag');

    $updateData = array(
      'image_details' => $imageDetails,
      'alt_tag'       => $alt_tag
    );

    if (isset($_FILES['images']['name']) && !empty($_FILES['images']['name'])) {
      $this->load->library('upload');
      $uploadDir = './../uploads/university_' . $universityId;
      if (!file_exists($uploadDir)) {
        mkdir($uploadDir, 0777, true);
      }

      $pathInfo     = pathinfo($_FILES['images']['name']);
      $tempfilename = preg_replace('/\s+/', '_', $pathInfo['filename']);
      $tempfilename = str_replace(array('.', '&', '-', '+'), '_', $tempfilename);
      $newImageId   = mt_rand(10000, 99999);
      $fileExt      = $pathInfo['extension'];
      $fileName     = 'university_' . $universityId . '_' . $columnName . '_' . $newImageId . '.' . $fileExt;

      $config['upload_path']   = $uploadDir;
      $config['file_name']     = $fileName;
      $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|doc|docx|webp';
      $config['max_size']      = 10240;

      $this->upload->initialize($config);

      if (!$this->upload->do_upload('images')) {
        $errorList[] = strip_tags($this->upload->display_errors());
        $response["status"] = "FAILED";
        $response["data"]   = $errorList;
        header('Content-Type: application/json');
        echo json_encode($response);
        exit();
      } else {
        $photoUrl = str_replace('admin/', '', base_url()) . 'uploads/university_' . $universityId . '/' . $fileName;
        $updateData['image_url'] = $photoUrl;
        $updateData['image_id'] = $newImageId;
      }
    }

    if ($columnName == 'banner' || $columnName == 'logo' || $columnName == 'map_preview') {
      $imageData = $this->universitydata_model->getImageData(array("id" => $universityId), $columnName, "universities");
      $table     = "universities";
      $where     = array("id" => $universityId);
    } else {
      $imageData = $this->universitydata_model->getImageData(array("university_id" => $universityId), $columnName, "university_aditional_data");
      $table     = "university_aditional_data";
      $where     = array("university_id" => $universityId);
    }

    $existingData = array();
    if (!empty($imageData['data'])) {
      $existingData = json_decode($imageData['data'][0][$columnName], true);
    }

    $found = false;
    for ($i = 0; $i < count($existingData); $i++) {
      if ($existingData[$i]['image_id'] == $imageId) {
        $existingData[$i]['image_details'] = $imageDetails;
        $existingData[$i]['alt_tag']       = $alt_tag;
        if (isset($updateData['image_url'])) {
          $existingData[$i]['image_url'] = $updateData['image_url'];
        }
        if (isset($updateData['image_id'])) {
          $existingData[$i]['image_id'] = $updateData['image_id'];
        }
        $found = true;
        break;
      }
    }

    if (!$found) {
      $response["status"] = "FAILED";
      $response["data"]   = "Image not found.";
      header('Content-Type: application/json');
      echo json_encode($response);
      exit();
    }

    $finalJson    = json_encode($existingData);
    $dataToUpdate = array($columnName => $finalJson);
    $docEntryObj  = $this->universitydata_model->upsert($where, array_merge($where, $dataToUpdate), $table);

    $response["status"]       = "SUCCESS";
    $response["data"]         = $existingData;
    $response["university_id"] = $universityId;
    header('Content-Type: application/json');
    echo json_encode($response);
  }

  function universityDataImageGet()
  {
    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    $universityId = isset($userdata['type']) && $userdata['type'] == '3' ? $userdata['university_id'] : $this->input->post('university_id');
    $imageId     = $this->input->post('image_id') ? $this->input->post('image_id') : NULL;
    $columnName  = $this->input->post('column_name') ? $this->input->post('column_name') : NULL;

    if ($imageId) {
     
      if ($columnName == 'banner' || $columnName == 'logo' || $columnName == 'map_preview') {
        $imageData = $this->universitydata_model->getImageData(["id" => $universityId], $columnName, "universities");
      } else {
        $imageData = $this->universitydata_model->getImageData(["university_id" => $universityId], $columnName, "university_aditional_data");
      }
      $imageIdArray = $imageData['data'] ? json_decode($imageData['data'][0][$columnName], true) : [];
    
      $foundImage = null;
      foreach ($imageIdArray as $image) {
        if ($image['image_id'] == $imageId) {
          $foundImage = $image;
          break;
        }
      }

      if ($foundImage) {
        $response["status"] = "SUCCESS";
        $response["data"]   = $foundImage;
      } else {
        $response["status"]  = "FAILED";
        $response["message"] = "Image not found";
      }
      header('Content-Type: application/json');
      echo json_encode($response);
    } else {
      $response["status"]  = "FAILED";
      $response["message"] = "Invalid image ID";
      header('Content-Type: application/json');
      echo json_encode($response);
    }
  }

  function uploadVideo()
  {
    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    $errorList = [];
    $imageUrl = [];

    $universityId = isset($userdata['type']) && $userdata['type'] == '3' ? $userdata['university_id'] : $this->input->post('university_id'); //$userdata['university_id'];
    $docType = $this->input->post('document_type');

    if ($_FILES['documents']['name']) {

      $this->load->library('upload');

      $all_images = array();
      $all_images = $_FILES['documents']['name'];
      $total_images = sizeof($all_images);

      for ($i = 0; $i < $total_images; $i++) {

        if ($_FILES['documents']['name'][$i]) {
          if (!file_exists('./../uploads/university_' . $universityId)) {
            mkdir('./../uploads/university_' . $universityId, 0777, true);
          }

          $config = array();
          $uploadFilePath     = './../uploads/university_' . $universityId;
          $uploadPath         = './../uploads/university_' . $universityId;
          $pathInfo           = pathinfo($_FILES['documents']['name'][$i]);
          $tempfilename       = preg_replace('/\s+/', '_', $pathInfo['filename']);
          $tempfilename       = str_replace('.', '_', $tempfilename);
          $tempfilename       = str_replace('&', '_', $tempfilename);
          $tempfilename       = str_replace('-', '_', $tempfilename);
          $tempfilename       = str_replace('+', '_', $tempfilename);

          $imageId = mt_rand(10000, 99999);

          $fileExt                    = $pathInfo['extension'];
          $fileName                   = 'university_' . $universityId . '_' . $docType . '_' . $imageId . '_' . $i . '.' . $fileExt;
          $config['upload_path']      = $uploadFilePath;
          $config['file_name']        = $fileName;
          $config['allowed_types']    = "*";
          $config['max_size']         = 65536;
          //$config['max_width']        = 10240;
          //$config['max_height']       = 7680;

          $this->upload->initialize($config);

          $_FILES['images']['name'] = $_FILES['documents']['name'][$i];
          $_FILES['images']['tmp_name'] = $_FILES['documents']['tmp_name'][$i];
          $_FILES['images']['error'] = $_FILES['documents']['error'][$i];
          $_FILES['images']['size'] = $_FILES['documents']['size'][$i];

          $videoType = explode("/", $_FILES['documents']['type'][$i]);

          $_FILES['images']['type'] = $videoType[1];


          if (!$this->upload->do_upload('images')) {
            $errorList[] = strip_tags($this->upload->display_errors());

            $response["status"] = "FAILED";
            $response["data"] = $errorList;
            header('Content-Type: application/json');
            echo (json_encode($response));
            exit();
          } else {

            // $photoUrl = str_replace('admin/', '', base_url()) . 'uploads/university_add/university_' . $universityId . '_' . $docType . '_'. $imageId . '_' . $i . '.' . $fileExt;
            $videoDetails = $this->input->post('videos_details');
            $photoUrl = str_replace('admin/', '', base_url()) . 'uploads/university_' . $universityId . '/university_' . $universityId . '_' . $docType . '_' . $imageId . '_' . $i . '.' . $fileExt;
            $imageUrl[] = ["image_id" => $imageId, "image_url" => $photoUrl, "videos_details" => $videoDetails];
          }
        }
      }
    }

    $imageData = $this->universitydata_model->getImageData(["university_id" => $universityId], $docType, "university_aditional_data");

    $imageIdValue = !empty($imageData['data'][0][$docType]) ? json_decode($imageData['data'][0][$docType], true) : [];

    $finalImageArray = array_merge($imageIdValue, $imageUrl);

    $docEntryObj = $this->universitydata_model->upsert(['university_id' => $universityId], ['university_id' => $universityId, $docType => json_encode($finalImageArray)], 'university_aditional_data');

    $response["status"] = "SUCCESS";
    $response["data"] = $finalImageArray;
    $response["university_id"] = $universityId;

    header('Content-Type: application/json');
    echo (json_encode($response));
  }

  function universityDataImageDelete()
  {

    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    $universityId = isset($userdata['type']) && $userdata['type'] == '3' ? $userdata['university_id'] : $this->input->post('university_id'); //$userdata['university_id'];
    $imageId = $this->input->post('image_id') ? $this->input->post('image_id') : NULL;
    $columnName = $this->input->post('column_name') ? $this->input->post('column_name') : NULL;

    if ($imageId) {

      if ($columnName == 'banner' || $columnName == 'logo' || $columnName == 'map_preview') {
        $imageData = $this->universitydata_model->getImageData(["id" => $universityId], $columnName, "universities");

        $imageIdValue = $imageData['data'] ? json_decode($imageData['data'][0][$columnName], true) : [];

        $finalImageArray = [];

        foreach ($imageIdValue as $key => $value) {

          if ($value['image_id'] == $imageId) {
            unset($imageIdValue[$key]);
          }
        }


        $finalImageArray = array_values($imageIdValue);

        $docEntryObj = $this->universitydata_model->upsert(['id' => $universityId], [$columnName => json_encode($finalImageArray)], 'universities');
      } else {
        $imageData = $this->universitydata_model->getImageData(["university_id" => $universityId], $columnName, "university_aditional_data");

        $imageIdValue = $imageData['data'] ? json_decode($imageData['data'][0][$columnName], true) : [];

        $finalImageArray = [];

        foreach ($imageIdValue as $key => $value) {

          if ($value['image_id'] == $imageId) {
            unset($imageIdValue[$key]);
          }
        }


        $finalImageArray = array_values($imageIdValue);

        $docEntryObj = $this->universitydata_model->upsert(['university_id' => $universityId], [$columnName => json_encode($finalImageArray)], 'university_aditional_data');
      }

      $response["status"] = "SUCCESS";
      $response["data"] = $finalImageArray;
      $response["university_id"] = $universityId;
      header('Content-Type: application/json');
      echo (json_encode($response));
    } else {
      $response["status"] = "FAILED";
      $response["message"] = $user_id;
      header('Content-Type: application/json');
      echo (json_encode($response));
    }
  }

  function universityDataSubmit()
  {

    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    $univesityData = [];
    $universityId = isset($userdata['type']) && $userdata['type'] == '3' ? $userdata['university_id'] : $this->uri->segment('4');
    //print_r($userdata); exit;
    //print_r($_POST); exit;
    $universityId = isset($userdata['type']) && $userdata['type'] == '3' ? $userdata['university_id'] : $this->input->post('university_id'); //$userdata['university_id'];
    $univesityData = $this->input->post();
    $univesityData['university_id'] = $universityId;

    $collegeNamePg = $this->input->post('college_name_pg') ? $this->input->post('college_name_pg') : [];
    $gpaPg = $this->input->post('gpa_score_pg') ? $this->input->post('gpa_score_pg') : [];
    $grePg = $this->input->post('gre_score_pg') ? $this->input->post('gre_score_pg') : [];
    $tofelPg = $this->input->post('tofel_score_pg') ? $this->input->post('tofel_score_pg') : [];
    $costPg = $this->input->post('cost_pg') ? $this->input->post('cost_pg') : [];

    $collegeNameUg = $this->input->post('college_name_ug') ? $this->input->post('college_name_ug') : [];
    $gpaUg = $this->input->post('gpa_score_ug') ? $this->input->post('gpa_score_ug') : [];
    $greUg = $this->input->post('gre_score_ug') ? $this->input->post('gre_score_ug') : [];
    $tofelUg = $this->input->post('tofel_score_ug') ? $this->input->post('tofel_score_ug') : [];
    $costUg = $this->input->post('cost_ug') ? $this->input->post('cost_ug') : [];

    $pgCollegeObj = [];
    $ugCollegeObj = [];

    if (!empty($collegeNamePg)) {
      foreach ($collegeNamePg as $keyPg => $valuePg) {
        $pgCollegeObj[] =  [
          'college_name' => $valuePg,
          'gpa' => isset($gpaPg[$keyPg]) ? $gpaPg[$keyPg] : null,
          'gre' => isset($grePg[$keyPg]) ? $grePg[$keyPg] : null,
          'tofel' => isset($tofelPg[$keyPg]) ? $tofelPg[$keyPg] : null,
          'cost' => isset($costPg[$keyPg]) ? $costPg[$keyPg] : null
        ];
      }
    }

    if (!empty($collegeNameUg)) {
      foreach ($collegeNameUg as $keyUg => $valueUg) {
        $ugCollegeObj[] = [
          'college_name' => $valueUg,
          'gpa' => isset($gpaUg[$keyUg]) ? $gpaUg[$keyUg] : null,
          'gre' => isset($greUg[$keyUg]) ? $greUg[$keyUg] : null,
          'tofel' => isset($tofelUg[$keyUg]) ? $tofelUg[$keyUg] : null,
          'cost' => isset($costUg[$keyUg]) ? $costUg[$keyUg] : null
        ];
      }
    }

    $univesityData['college_pg_list'] = json_encode($pgCollegeObj);
    $univesityData['college_ug_list'] = json_encode($ugCollegeObj);

    unset($univesityData['college_name_pg']);
    unset($univesityData['gpa_score_pg']);
    unset($univesityData['gre_score_pg']);
    unset($univesityData['tofel_score_pg']);
    unset($univesityData['cost_pg']);

    unset($univesityData['college_name_ug']);
    unset($univesityData['gpa_score_ug']);
    unset($univesityData['gre_score_ug']);
    unset($univesityData['tofel_score_ug']);
    unset($univesityData['cost_ug']);

    $univesityData['smm_facebook'] = $this->input->post('smm_facebook') ? $this->input->post('smm_facebook') : NULL;
    $univesityData['smm_twitter'] = $this->input->post('smm_twitter') ? $this->input->post('smm_twitter') : NULL;
    $univesityData['smm_instagram'] = $this->input->post('smm_instagram') ? $this->input->post('smm_instagram') : NULL;
    $univesityData['smm_youtube'] = $this->input->post('smm_youtube') ? $this->input->post('smm_youtube') : NULL;
    $univesityData['smm_linkedin'] = $this->input->post('smm_linkedin') ? $this->input->post('smm_linkedin') : NULL;


    $universityDataEntryObj = $this->universitydata_model->upsert(['university_id' => $universityId], $univesityData, 'university_aditional_data');

    echo "SUCCESS";
  }
}
