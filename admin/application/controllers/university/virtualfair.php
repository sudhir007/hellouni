<?php
//require_once APPPATH . 'libraries/Mail/sMail.php';
class Virtualfair extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
        $this->load->model('virtualfair_model');
    }

    function index()
    {
        $this->load->helper('cookie_helper');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }
        $universityUserId = $userdata['id'];

        $fairDetail = $this->virtualfair_model->getFairByUniversityId($universityUserId);
        if($fairDetail)
        {
            $pre_counselor = [5331, 5332, 5333, 5334, 5335, 5336, 5337];
            $post_counselor = [5338, 5339, 5340, 5341, 5342, 5343, 5344];
            $edu_counselor = [5345, 5576, 5577, 5578, 5580];

            if(in_array($universityUserId, $pre_counselor))
            {
                $fairDetail->url = SITE_URL . '/meeting/virtualfair/pre_counselling?id=' . base64_encode($fairDetail->token_id);
            }
            else if(in_array($universityUserId, $post_counselor))
            {
                $fairDetail->url = SITE_URL . '/meeting/virtualfair/post_counselling?id=' . base64_encode($fairDetail->token_id);
            }
            else if(in_array($universityUserId, $edu_counselor))
            {
                $fairDetail->url = SITE_URL . '/meeting/virtualfair/edu_counselling?id=' . base64_encode($fairDetail->token_id);
            }
            else
            {
                $fairDetail->url = SITE_URL . '/meeting/virtualfair/panelist?id=' . base64_encode($fairDetail->token_id);
            }
        }

        $this->outputData['vfairs'] = [$fairDetail];
        $this->outputData['view'] = 'list';
        $this->render_page('templates/university/virtualfair_list', $this->outputData);
    }

    function form()
    {
        $this->load->helper('cookie_helper');

        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        if($this->uri->segment('4'))
        {
            $fair_details = $this->virtualfair_model->getFairById($this->uri->segment('4'));

            $this->outputData['id'] = $fair_details->id;
            $this->outputData['name'] = $fair_details->name;
            $this->outputData['date'] = $fair_details->fair_date;
            $this->outputData['time'] = $fair_details->fair_time;
            $this->outputData['edit'] = true;
        }

        $this->render_page('templates/university/virtualfair_form',$this->outputData);
    }

    function help()
    {
        $allHelps = $this->virtualfair_model->getHelp();
        if($allHelps)
        {
            foreach($allHelps as $index => $help)
            {
                $userDetail = $this->virtualfair_model->getUserById($help['user_id']);
                $help['url'] = '/admin/university/virtualfair/help_update?id=' . $help['id'];
                $help['name'] = $userDetail->university_name;
                $allHelps[$index] = $help;
            }
        }
        $this->outputData['helps'] = $allHelps;
        $this->outputData['view'] = 'list';
        $this->render_page('templates/university/virtualfair_help', $this->outputData);
    }

    function help_update()
    {
        $helpId = $this->input->get('id');
        $helpDetail = $this->virtualfair_model->getHelpById($helpId);
        $updateData['status'] = 1;
        $this->virtualfair_model->updateHelp($helpId, $updateData);
        $url = SITE_URL . '/meeting/virtualfair/organizer?id=' . base64_encode($helpDetail->token_id);
        redirect($url);
    }

    function all_participants()
    {
        $allParticipants = $this->virtualfair_model->getAllParticipants();
        foreach($allParticipants as $index => $participant)
        {
            $participant['url'] = SITE_URL . '/meeting/virtualfair/organizer?id=' . base64_encode($participant['token_id']);
            $allParticipants[$index] = $participant;
        }
        $this->outputData['participants'] = $allParticipants;
        $this->outputData['view'] = 'list';
        $this->render_page('templates/university/virtualfair_participants', $this->outputData);
    }
}
