<?php
require_once APPPATH . 'libraries/Mail/sMail.php';
class Application extends MY_Controller
{
    public $outputData;
    public $loggedInUser;

    function __construct()
    {
        parent::__construct();
        $this->load->library(array('template', 'form_validation'));
        $this->lang->load('enduser/home', $this->config->item('language_code'));
        $this->load->helper(array('cookie_helper', 'form', 'url'));
        $this->load->model(array('user/user_model', 'user/university_model', 'user/college_model', 'user/department_model'));

        $this->outputData['time_array'] = array('00:00', '00:15', '00:30', '00:45', '01:00', '01:15', '01:30', '01:45', '02:00', '02:15', '02:30', '02:45', '03:00', '03:15', '03:30', '03:45', '04:00', '04:15', '04:30', '04:45', '05:00', '05:15', '05:30', '05:45', '06:00', '06:15', '06:30', '06:45', '07:00', '07:15', '07:30', '07:45', '08:00', '08:15', '08:30', '08:45', '09:00', '09:15', '09:30', '09:45', '10:00', '10:15', '10:30', '10:45', '11:00', '11:15', '11:30', '11:45', '12:00', '12:15', '12:30', '12:45', '13:00', '13:15', '13:30', '13:45', '14:00', '14:15', '14:30', '14:45', '15:00', '15:15', '15:30', '15:45', '16:00', '16:15', '16:30', '16:45', '17:00', '17:15', '17:30', '17:45', '18:00', '18:15', '18:30', '18:45', '19:00', '19:15', '19:30', '19:45', '20:00', '20:15', '20:30', '20:45', '21:00', '21:15', '21:30', '21:45', '22:00', '22:15', '22:30', '22:45', '23:00', '23:15', '23:30', '23:45');
    }

    function list()
    {
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $allApplications = $this->university_model->getApplicationsList();
        $this->outputData['applications'] = $allApplications;
        $this->outputData['view'] = 'list';
        $this->render_page('templates/university/application_list', $this->outputData);
    }

    function ajax_manage_page()
      {
        $this->load->helper('cookie_helper');
        $this->load->model('user/university_model');
        
        $userdata=$this->session->userdata('user');
       if(empty($userdata)){  redirect('common/login'); }

       $list = $this->university_model->get_datatables_application(); 
        
       // echo "<pre>"; print_r($list); exit;
        $data = array();
        $no = $_POST['start'];

        foreach ($list as $person) {

            $btn = anchor(base_url('/university/application/?uid='.$person->university_id),'<span title="Check Applications" target="_blank">Check Applications</span>');

          //echo "<pre>"; print_r($person); exit;
          $no++;
            $row = array();
            $row[] = '<input type="checkbox" class="multichk" name="chk[]" value="<?php echo $person->university_id;?>"/>';
            $row[] = $person->name;
            $row[] = $person->student_name;
            $row[] = $person->student_email;
            $row[] = $person->student_phone;
            $row[] = $btn;

            $data[] = $row;
        }
        
//echo "<pre>"; print_r(count($list)); exit;
          $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->university_model->count_all_application(),
                        //"recordsTotal" => count($list),
                        "recordsFiltered" => $this->university_model->count_filtered_application(),
                        "data" => $data,
                );
        
        //echo "<pre>"; print_r($output); exit;
        //output to json format
        echo json_encode($output);
 
      }

    function index()
    {
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        switch($userdata['type'])
        {
            case 1:
            case 2:
                $universityId = $_GET['uid'];
            case 3:
                $universityId = $userdata['university_id'];
                break;
        }
        $allApplications = $this->university_model->getApplicationsByUniversity($universityId);

        $this->outputData['applications'] = $allApplications;
        $this->render_page('templates/university/application', $this->outputData);
    }

    function detail()
    {
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $applicationId = $_GET['aid'];
        $universityId = $_GET['uid'];
        $applicationDetail = $this->university_model->getApplicationById($applicationId);
        $applicationDetail[0]['sop'] = $applicationDetail[0]['sop'] ? json_decode($applicationDetail[0]['sop'], true) : $applicationDetail[0]['sop'];
        $applicationDetail[0]['lor'] = $applicationDetail[0]['lor'] ? json_decode($applicationDetail[0]['lor'], true) : $applicationDetail[0]['lor'];
        $applicationDetail[0]['transcripts'] = $applicationDetail[0]['transcripts'] ? json_decode($applicationDetail[0]['transcripts'], true) : $applicationDetail[0]['transcripts'];
        $applicationDetail[0]['application_fee'] = $applicationDetail[0]['application_fee'] ? json_decode($applicationDetail[0]['application_fee'], true) : $applicationDetail[0]['application_fee'];

        $applicationDetail = $applicationDetail[0];

        $visaDetail = $this->university_model->checkVisaByApplication($applicationId);
        if($visaDetail)
        {
            $visaDetail[0]['mock_dates'] = $visaDetail[0]['mock_dates'] ? json_decode($visaDetail[0]['mock_dates'], true) : $visaDetail[0]['mock_dates'];

            $visaDetail = $visaDetail[0];
        }

        $this->outputData['id'] 				    = $applicationDetail['id'];
        $this->outputData['university_name'] 		= $applicationDetail['name'];
        $this->outputData['college_name'] 			= $applicationDetail['college_name'];
        $this->outputData['student_name'] 			= $applicationDetail['student_name'];
        $this->outputData['student_email'] 	        = $applicationDetail['student_email'];
        $this->outputData['student_phone'] 	        = $applicationDetail['student_phone'];
        $this->outputData['login_url'] 	            = $applicationDetail['login_url'];
        $this->outputData['username']               = $applicationDetail['username'];
        $this->outputData['password']               = $applicationDetail['password'];
        $this->outputData['application_deadline'] 	= $applicationDetail['dead_line'];
        $this->outputData['gre_code']               = $applicationDetail['gre_code'];
        $this->outputData['toefl_code'] 			= $applicationDetail['toefl_code'];
        $this->outputData['sop_needed']             = isset($applicationDetail['sop']['sop_needed']) ? $applicationDetail['sop']['sop_needed'] : '';
        $this->outputData['sop_no_of_words']        = isset($applicationDetail['sop']['sop_no_of_words']) ? $applicationDetail['sop']['sop_no_of_words'] : '';
        $this->outputData['lor_needed']             = isset($applicationDetail['lor']['lor_needed']) ? $applicationDetail['lor']['lor_needed'] : '';
        $this->outputData['lor_required']           = isset($applicationDetail['lor']['lor_required']) ? $applicationDetail['lor']['lor_required'] : '';
        $this->outputData['lor_type']               = isset($applicationDetail['lor']['lor_type']) ? $applicationDetail['lor']['lor_type'] : '';
        $this->outputData['transcript']             = isset($applicationDetail['transcripts']['transcript']) ? $applicationDetail['transcripts']['transcript'] : '';
        $this->outputData['transcript_submitted']   = isset($applicationDetail['transcripts']['transcript_submitted']) ? $applicationDetail['transcripts']['transcript_submitted'] : '';
        $this->outputData['application_fee']        = isset($applicationDetail['application_fee']['application_fee']) ? $applicationDetail['application_fee']['application_fee'] : '';
        $this->outputData['application_fee_paid']   = isset($applicationDetail['application_fee']['application_fee_paid']) ? $applicationDetail['application_fee']['application_fee_paid'] : '';
        $this->outputData['request_url'] 			= $applicationDetail['requested_url'];
        $this->outputData['status']                 = $applicationDetail['status'];

        $this->render_page('templates/university/application_detail',$this->outputData);
    }
}
?>
