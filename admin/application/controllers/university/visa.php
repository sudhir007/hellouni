<?php
require_once APPPATH . 'libraries/Mail/sMail.php';
class Visa extends MY_Controller
{
    public $outputData;
    public $loggedInUser;

    function __construct()
    {
        parent::__construct();
        $this->load->library(array('template', 'form_validation'));
        $this->lang->load('enduser/home', $this->config->item('language_code'));
        $this->load->helper(array('cookie_helper', 'form', 'url'));
        $this->load->model(array('user/user_model', 'user/university_model', 'user/college_model', 'user/department_model'));

        $this->outputData['time_array'] = array('00:00', '00:15', '00:30', '00:45', '01:00', '01:15', '01:30', '01:45', '02:00', '02:15', '02:30', '02:45', '03:00', '03:15', '03:30', '03:45', '04:00', '04:15', '04:30', '04:45', '05:00', '05:15', '05:30', '05:45', '06:00', '06:15', '06:30', '06:45', '07:00', '07:15', '07:30', '07:45', '08:00', '08:15', '08:30', '08:45', '09:00', '09:15', '09:30', '09:45', '10:00', '10:15', '10:30', '10:45', '11:00', '11:15', '11:30', '11:45', '12:00', '12:15', '12:30', '12:45', '13:00', '13:15', '13:30', '13:45', '14:00', '14:15', '14:30', '14:45', '15:00', '15:15', '15:30', '15:45', '16:00', '16:15', '16:30', '16:45', '17:00', '17:15', '17:30', '17:45', '18:00', '18:15', '18:30', '18:45', '19:00', '19:15', '19:30', '19:45', '20:00', '20:15', '20:30', '20:45', '21:00', '21:15', '21:30', '21:45', '22:00', '22:15', '22:30', '22:45', '23:00', '23:15', '23:30', '23:45');
    }

    function list()
    {
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $allApplications = $this->university_model->getApplicationsList();
        $this->outputData['applications'] = $allApplications;
        $this->outputData['view'] = 'list';
        $this->render_page('templates/university/application_list', $this->outputData);
    }

    function index()
    {
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        switch($userdata['type'])
        {
            case 1:
            case 2:
                $universityId = $_GET['uid'];
            case 3:
                $universityId = $userdata['university_id'];
                break;
        }
        $allApplications = $this->university_model->getVisaApplicationsByUniversity($universityId);

        $this->outputData['applications'] = $allApplications;
        $this->render_page('templates/university/visa', $this->outputData);
    }

    function detail()
    {
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $applicationId = $_GET['aid'];
        $universityId = $_GET['uid'];
        $applicationDetail = $this->university_model->getVisaById($applicationId);
        $applicationDetail[0]['mock_dates'] = $applicationDetail[0]['mock_dates'] ? json_decode($applicationDetail[0]['mock_dates'], true) : $applicationDetail[0]['mock_dates'];

        $applicationDetail = $applicationDetail[0];

        $this->outputData['id'] 				    = $applicationDetail['id'];
        $this->outputData['university_name'] 		= $applicationDetail['name'];
        $this->outputData['college_name'] 			= $applicationDetail['college_name'];
        $this->outputData['student_name'] 			= $applicationDetail['student_name'];
        $this->outputData['student_email'] 	        = $applicationDetail['student_email'];
        $this->outputData['student_phone'] 	        = $applicationDetail['student_phone'];
        $this->outputData['ofc_date'] 	            = $applicationDetail['ofc_date'];
        $this->outputData['interview_date']         = $applicationDetail['interview_date'];
        $this->outputData['ds_160']                 = $applicationDetail['ds_160'];
        $this->outputData['cgi_federal']            = $applicationDetail['cgi_federal'];
        $this->outputData['sevis']                  = $applicationDetail['sevis'];
        $this->outputData['mock_date_1']            = isset($applicationDetail['mock_dates']['mock_date_1']) ? $applicationDetail['mock_dates']['mock_date_1'] : '';
        $this->outputData['mock_date_2']            = isset($applicationDetail['mock_dates']['mock_date_2']) ? $applicationDetail['mock_dates']['mock_date_2'] : '';
        $this->outputData['mock_date_3']            = isset($applicationDetail['mock_dates']['mock_date_3']) ? $applicationDetail['mock_dates']['mock_date_3'] : '';
        $this->outputData['mock_slot_1']            = isset($applicationDetail['mock_dates']['mock_slot_1']) ? $applicationDetail['mock_dates']['mock_slot_1'] : '';
        $this->outputData['mock_slot_2']            = isset($applicationDetail['mock_dates']['mock_slot_2']) ? $applicationDetail['mock_dates']['mock_slot_2'] : '';
        $this->outputData['mock_slot_3']            = isset($applicationDetail['mock_dates']['mock_slot_3']) ? $applicationDetail['mock_dates']['mock_slot_3'] : '';
        $this->outputData['status']                 = $applicationDetail['status'];

        $this->render_page('templates/university/visa_detail',$this->outputData);
    }
}
?>
