<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Universitycountry extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

	
	function index()
	{   //die('++++++++++++++++');
	     $this->load->helper('cookie_helper'); 
         $this->load->model('location/country_model');
		 //$this->load->model('location/city_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }		
        
		$cond1 = array('country_to_university.university_id' => $userdata['id']); 
		$this->outputData['view'] = 'list';
		$this->outputData['countries'] = $this->country_model->getuniversityCountry($cond1);
       /* echo '<pre>';
		print_r($this->outputData['countries']);
		echo '</pre>'; die('++++++');*/
		$this->render_page('templates/location/universitycountry',$this->outputData);
	
} 
	
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
         
		  $this->load->model('location/country_model');
		 
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }		
		 
		 if($this->uri->segment('4')){
		 $conditions = array(
	 				  'country_master.id' => $this->uri->segment('4')
	 					);
		 $country_info = $this->country_model->getCountryByid($conditions);
		 
		 /*echo '<pre>';
		 print_r($country_info);
		 echo '</pre>';
		 exit();*/
		 
		 $this->outputData['id'] 			= $country_info->id;
		 $this->outputData['name'] 			= $country_info->name;
		 $this->outputData['code'] 			= $country_info->code;
		 $this->outputData['status'] 		= $country_info->status;
		 
		 
		}
		
		$cond1 = array('country_to_university.university_id' => $userdata['id']); 
		$existUniversity = $this->country_model->getuniversityCountry($cond1);
		
		$cond_user1 = array('country_master.status' => '1'); 
		$this->outputData['countries'] = $this->country_model->getCountries($cond_user1);
		
		$found = false;
		foreach($this->outputData['countries'] as $key => $var){
		
		  foreach($existUniversity as $ec){
		  if($ec->id == $var->id){
		   $found = true;
           $info = 1;
		   break;
		   }
		   
		  }
		  
		  if ($found){
		  unset($this->outputData['countries'][$key]);
		  $found = false;
		  
		  }
		
		}
		
		
		$this->render_page('templates/location/universitycountry_form',$this->outputData);
	
     } 
	 
	 
	 
	 function create()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
      
		$this->load->model('location/country_model');
		
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }	
		//$this->load->model('location/city_model');
		//$this->outputData['countries']	= $this->country_model->getCountries();   
		//$this->outputData['cities']	= $this->city_model->getCities();
		
		//$this->form_validation->set_rules('country_name', 'Country_Name', 'required|trim|xss_clean');
		

		//if ($this->form_validation->run() == FALSE)
		//{
			// redirect('website-element/brand/form');
			 // $this->render_page('templates/location/country_form');
		//}
		//else
		//{  
		   $data = array(
			'country_id' => $this->input->post('country'),
     		'university_id' => $userdata['id']
     		
     		);
			
			//print_r($data); exit();
	 
	       
	        $this->country_model->insertUniversityCountry($data);
	        $this->session->set_flashdata('flash_message', "You have added Country!");
	        redirect('location/universitycountry/index');
	   
	 
	  // }
	}
	 
	 
	 function edit()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('location/country_model');
		
			  //$logo_info = $this->common_model->getFileinfo($this->input->post('property_logo'));
			 $data = array(
			'id' => $this->uri->segment('4'),
     		'name' => $this->input->post('name'),
     		'code' => $this->input->post('code'),
     		'status' =>$this->input->post('status') 
     		);
			
			
	         if($this->country_model->editCountry($data)=='success'){
	 
	         $this->session->set_flashdata('flash_message', "You have modified Country!");
	         redirect('location/country');
	         }
	 
	 
	  
	}
    
	
	function delete()
	 {   
	   //die('+++');
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('location/country_model');
		$userdata=$this->session->userdata('user');
		
		/*if($this->country_model->deleteCountry($this->uri->segment('4'))=='success'){
	 
	         $this->session->set_flashdata('flash_message', "You have deleted Country!");
	        redirect('location/country');
	        
	 
	 
	    }*/
		
		$data = array('country_id' => $this->uri->segment('4'),'university_id'=>$userdata['id']);
		
		$this->country_model->deleteUniversityCountry($data);
		redirect('location/universitycountry');
		
	}
	
	
	function multidelete(){
	
	    $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('location/country_model');
		
		$array = $this->input->post('chk');
		foreach($array as $id):
		
		$this->country_model->deleteCountry($id);
		
		endforeach;
		$this->session->set_flashdata('flash_message', "Success: You have deleted Country!");
		redirect('location/country/index');
	
	
	}
	 
	 
	 
	 

	

}//End  Home Class
?>

