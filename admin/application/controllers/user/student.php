<?php
require_once APPPATH . 'libraries/Mail/sMail.php';
class Student extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

    function index()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/user_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata)){  redirect('common/login'); }

        $user_id = $userdata['id'];
        $all_slots = $this->user_model->getSlotByUniversity($user_id);
        if($all_slots)
        {
            foreach($all_slots as $index => $slotData)
            {
                $all_slots[$index]->tokbox_url = '';
                $slot_user_id = $slotData->user_id;
                $participants = array("user_id" => $slot_user_id);
                $json_participant = json_encode($participants);
                $token = $this->user_model->getTokenByParticipant($json_participant);
                if($token)
                {
                    $all_slots[$index]->tokbox_url = '/tokbox?id=' . $token->id;
                }
            }
        }

        $this->outputData['all_slots'] = $all_slots;
        $this->render_page('templates/user/student',$this->outputData);
    }

    function form()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/user_model');
        $this->load->model('user/university_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata)){  redirect('common/login'); }

        //$universityDetail = $this->university_model->getUniversityById($userdata['university_id']);
        //$timezone = $universityDetail[0]['timezone'];

        if(isset($userdata['university_id']))
        {
            $universityDetail = $this->university_model->getUniversityById($userdata['university_id']);
            $timezone = isset($universityDetail[0]['timezone']) ? $universityDetail[0]['timezone'] : '';
        }
        else
        {
            $timezone = 5.5;
        }

        $this->outputData['time_array'] = array('12 AM', '12:15 AM', '12:30 AM', '12:45 AM', '01:00 AM', '01:15 AM', '01:30 AM', '01:45 AM', '02:00 AM', '02:15 AM', '02:30 AM', '02:45 AM', '03:00 AM', '03:15 AM', '03:30 AM', '03:45 AM', '04:00 AM', '04:15 AM', '04:30 AM', '04:45 AM', '05:00 AM', '05:15 AM', '05:30 AM', '05:45 AM', '06:00 AM', '06:15 AM', '06:30 AM', '06:45 AM', '07:00 AM', '07:15 AM', '07:30 AM', '07:45 AM', '08:00 AM', '08:15 AM', '08:30 AM', '08:45 AM', '09:00 AM', '09:15 AM', '09:30 AM', '09:45 AM', '10:00 AM', '10:15 AM', '10:30 AM', '10:45 AM', '11:00 AM', '11:15 AM', '11:30 AM', '11:45 AM', '12:00 PM', '12:15 PM', '12:30 PM', '12:45 PM', '01:00 PM', '01:15 PM', '01:30 PM', '01:45 PM', '02:00 PM', '02:15 PM', '02:30 PM', '02:45 PM', '03:00 PM', '03:15 PM', '03:30 PM', '03:45 PM', '04:00 PM', '04:15 PM', '04:30 PM', '04:45 PM', '05:00 PM', '05:15 PM', '05:30 PM', '05:45 PM', '06:00 PM', '06:15 PM', '06:30 PM', '06:45 PM', '07:00 PM', '07:15 PM', '07:30 PM', '07:45 PM', '08:00 PM', '08:15 PM', '08:30 PM', '08:45 PM', '09:00 PM', '09:15 PM', '09:30 PM', '09:45 PM', '10:00 PM', '10:15 PM', '10:30 PM', '10:45 PM', '11:00 PM', '11:15 PM', '11:30 PM', '11:45 PM');

        $time_array = array('00:00:00' => '12 AM', '00:15:00' => '12:15 AM', '00:30:00' => '12:30 AM', '00:45:00' => '12:45 AM', '01:00:00' => '01:00 AM', '01:15:00' => '01:15 AM', '01:30:00' => '01:30 AM', '01:45:00' => '01:45 AM', '02:00:00' => '02:00 AM', '02:15:00' => '02:15 AM', '02:30:00' => '02:30 AM', '02:45:00' => '02:45 AM', '03:00:00' => '03:00 AM', '03:15:00' => '03:15 AM', '03:30:00' => '03:30 AM', '03:45:00' => '03:45 AM', '04:00:00' => '04:00 AM', '04:15:00' => '04:15 AM', '04:30:00' => '04:30 AM', '04:45:00' => '04:45 AM', '05:00:00' => '05:00 AM', '05:15:00' => '05:15 AM', '05:30:00' => '05:30 AM', '05:45:00' => '05:45 AM', '06:00:00' => '06:00 AM', '06:15:00' => '06:15 AM', '06:30:00' => '06:30 AM', '06:45:00' => '06:45 AM', '07:00:00' => '07:00 AM', '07:15:00' => '07:15 AM', '07:30:00' => '07:30 AM', '07:45:00' => '07:45 AM', '08:00:00' => '08:00 AM', '08:15:00' => '08:15 AM', '08:30:00' => '08:30 AM', '08:45:00' => '08:45 AM', '09:00:00' => '09:00 AM', '09:15:00' => '09:15 AM', '09:30:00' => '09:30 AM', '09:45:00' => '09:45 AM', '10:00:00' => '10:00 AM', '10:15:00' => '10:15 AM', '10:30:00' => '10:30 AM', '10:45:00' => '10:45 AM', '11:00:00' => '11:00 AM', '11:15:00' => '11:15 AM', '11:30:00' => '11:30 AM', '11:45:00' => '11:45 AM', '12:00:00' => '12:00 PM', '12:15:00' => '12:15 PM', '12:30:00' => '12:30 PM', '12:45:00' => '12:45 PM', '13:00:00' => '01:00 PM', '13:15:00' => '01:15 PM', '13:30:00' => '01:30 PM', '13:45:00' => '01:45 PM', '14:00:00' => '02:00 PM', '14:15:00' => '02:15 PM', '14:30:00' => '02:30 PM', '14:45:00' => '02:45 PM', '15:00:00' => '03:00 PM', '15:15:00' => '03:15 PM', '15:30:00' => '03:30 PM', '15:45:00' => '03:45 PM', '16:00:00' => '04:00 PM', '16:15:00' => '04:15 PM', '16:30:00' => '04:30 PM', '16:45:00' => '04:45 PM', '17:00:00' => '05:00 PM', '17:15:00' => '05:15 PM', '17:30:00' => '05:30 PM', '17:45:00' => '05:45 PM', '18:00:00' => '06:00 PM', '18:15:00' => '06:15 PM', '18:30:00' => '06:30 PM', '18:45:00' => '06:45 PM', '19:00:00' => '07:00 PM', '19:15:00' => '07:15 PM', '19:30:00' => '07:30 PM', '19:45:00' => '07:45 PM', '20:00:00' => '08:00 PM', '20:15:00' => '08:15 PM', '20:30:00' => '08:30 PM', '20:45:00' => '08:45 PM', '21:00:00' => '09:00 PM', '21:15:00' => '09:15 PM', '21:30:00' => '09:30 PM', '21:45:00' => '09:45 PM', '22:00:00' => '10:00 PM', '22:15:00' => '10:15 PM', '22:30:00' => '10:30 PM', '22:45:00' => '10:45 PM', '23:00:00' => '11:00 PM', '23:15:00' => '11:15 PM', '23:30:00' => '11:30 PM', '23:45:00' => '11:45 PM');

        if($this->uri->segment('4'))
        {
            $slotDetail = $this->user_model->getSlotById($this->uri->segment('4'));
            $this->outputData['slot_1_date'] = '';
            $this->outputData['slot_2_date'] = '';
            $this->outputData['slot_3_date'] = '';
            $this->outputData['slot_1_time'] = '';
            $this->outputData['slot_2_time'] = '';
            $this->outputData['slot_3_time'] = '';
            $this->outputData['timezone'] = isset($slotDetail[0]->timezone) ? $slotDetail[0]->timezone : $timezone;
            $localTimezone = 5.5 - (float)$this->outputData['timezone'];

            if($slotDetail[0]->slot_1)
            {
                $slotDetail[0]->slot_1 = date('Y-m-d H:i:s', strtotime($slotDetail[0]->slot_1) - $localTimezone * 60 * 60);

                $slot1 = explode(" ", $slotDetail[0]->slot_1);
                $this->outputData['slot_1_date'] = $slot1[0];
                $this->outputData['slot_1_time'] = $time_array[$slot1[1]];
            }
            if($slotDetail[0]->slot_2)
            {
                $slotDetail[0]->slot_2 = date('Y-m-d H:i:s', strtotime($slotDetail[0]->slot_2) - $localTimezone * 60 * 60);
                $slot2 = explode(" ", $slotDetail[0]->slot_2);
                $this->outputData['slot_2_date'] = $slot2[0];
                $this->outputData['slot_2_time'] = $time_array[$slot2[1]];
            }
            if($slotDetail[0]->slot_3)
            {
                $slotDetail[0]->slot_3 = date('Y-m-d H:i:s', strtotime($slotDetail[0]->slot_3) - $localTimezone * 60 * 60);
                $slot3 = explode(" ", $slotDetail[0]->slot_3);
                $this->outputData['slot_3_date'] = $slot3[0];
                $this->outputData['slot_3_time'] = $time_array[$slot3[1]];
            }
            $this->outputData['slot_id'] = $this->uri->segment('4');
        }
        else
        {
            $slotIds = $this->input->post('slot_ids');
            if(!$slotIds)
            {
                $this->session->set_flashdata('flash_message', "Please select one of the session to create slot");
                redirect('user/session/unassigned');
            }

            $slotIdsStr = '';
            foreach($slotIds as $slotId)
            {
                $slotIdsStr .= $slotId . ",";
            }
            $slotIdsStr = substr($slotIdsStr, 0, -1);
            $this->outputData['slot_id'] = $slotIdsStr;
        }
        $this->render_page('templates/user/student_form', $this->outputData);
    }

    function edit()
    {
        $time_array = array('00:00:00', '00:15:00', '00:30:00', '00:45:00', '01:00:00', '01:15:00', '01:30:00', '01:45:00', '02:00:00', '02:15:00', '02:30:00', '02:45:00', '03:00:00', '03:15:00', '03:30:00', '03:45:00', '04:00:00', '04:15:00', '04:30:00', '04:45:00', '05:00:00', '05:15:00', '05:30:00', '05:45:00', '06:00:00', '06:15:00', '06:30:00', '06:45:00', '07:00:00', '07:15:00', '07:30:00', '07:45:00', '08:00:00', '08:15:00', '08:30:00', '08:45:00', '09:00:00', '09:15:00', '09:30:00', '09:45:00', '10:00:00', '10:15:00', '10:30:00', '10:45:00', '11:00:00', '11:15:00', '11:30:00', '11:45:00', '12:00:00', '12:15:00', '12:30:00', '12:45:00', '13:00:00', '13:15:00', '13:30:00', '13:45:00', '14:00:00', '14:15:00', '14:30:00', '14:45:00', '15:00:00', '15:15:00', '15:30:00', '15:45:00', '16:00:00', '16:15:00', '16:30:00', '16:45:00', '17:00:00', '17:15:00', '17:30:00', '17:45:00', '18:00:00', '18:15:00', '18:30:00', '18:45:00', '19:00:00', '19:15:00', '19:30:00', '19:45:00', '20:00:00', '20:15:00', '20:30:00', '20:45:00', '21:00:00', '21:15:00', '21:30:00', '21:45:00', '22:00:00', '22:15:00', '22:30:00', '22:45:00', '23:00:00', '23:15:00', '23:30:00', '23:45:00');

        $this->load->helper('cookie_helper');
        $this->load->model('user/user_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata)){  redirect('common/login'); }

        $slotIdStr = $this->input->post('slot_id');
        $slotIds = explode(",", $slotIdStr);
        $slug = $userdata['slug'];
        /*$data['slot_1_date'] = $this->input->post('slot_1_date');
        $data['slot_1_time'] = $this->input->post('slot_1_time');
        $data['slot_2_date'] = $this->input->post('slot_2_date');
        $data['slot_2_time'] = $this->input->post('slot_2_time');
        $data['slot_3_date'] = $this->input->post('slot_3_date');
        $data['slot_3_time'] = $this->input->post('slot_3_time');*/

        $istTimezone = 5.5 - (float)$this->input->post('timezone');
        $data['timezone'] = $this->input->post('timezone');

        foreach($slotIds as $slotId)
        {
            $slot1 = '';
            $slot2 = '';
            $slot3 = '';

            if($this->input->post('slot_1_date'))
            {
                $data['slot_1'] = $this->input->post('slot_1_date') . " " . $time_array[$this->input->post('slot_1_time')];
                $data['slot_1'] = date('Y-m-d H:i:s', strtotime($data['slot_1']) + $istTimezone*60*60);
                $slot1 = $data['slot_1'];
            }
            if($this->input->post('slot_2_date'))
            {
                $data['slot_2'] = $this->input->post('slot_2_date') . " " . $time_array[$this->input->post('slot_2_time')];
                $data['slot_2'] = date('Y-m-d H:i:s', strtotime($data['slot_2']) + $istTimezone*60*60);
                $slot2 = $data['slot_2'];
            }
            if($this->input->post('slot_3_date'))
            {
                $data['slot_3'] = $this->input->post('slot_3_date') . " " . $time_array[$this->input->post('slot_3_time')];
                $data['slot_3'] = date('Y-m-d H:i:s', strtotime($data['slot_3']) + $istTimezone*60*60);
                $slot3 = $data['slot_3'];
            }

            $data['status'] = 'Scheduled';

            $this->user_model->editSlot($slotId, $data);

            $slotData = $this->user_model->getSlotById($slotId);
            $userEmail = $slotData[0]->email;
            $userName = $slotData[0]->name;
            $universityEmail = $userdata['email'];
            $universityName = $userdata['name'];
            $universityUrl = 'https://www.hellouni.org/' . $slug;

            $mailSubject = "Slots For $universityName";
            $mailTemplate = "Hi $userName,

                                $universityName has shared the following slots for the chat session with you.

                                &bull; $slot1 IST
                                &bull; $slot2 IST
                                &bull; $slot3 IST

                                Select one as per your availability <a href='$universityUrl'>$universityUrl</a>

                                <b>Thanks and Regards,
                                HelloUni Coordinator
                                +91 81049 09690</b>

                                <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

            $ccMailList = '';
            $mailAttachments = '';

            $sendMail = sMail($userEmail, $userName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);
        }

        redirect('/user/session/unconfirmed');
    }

    function delete()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/user_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $slotIdStr = $this->input->get('slot_id');
        $slotIds = explode(",", $slotIdStr);

        foreach($slotIds as $slotId)
        {
            $data['status'] = 'Deleted';
            $this->user_model->editSlot($slotId, $data);
        }

        $redirectUrl = $this->input->get('redirectUrl');
        redirect($redirectUrl);
    }

    function slotform()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/user_model');
        $this->load->model('user/university_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata)){  redirect('common/login'); }

        //$universityDetail = $this->university_model->getUniversityById($userdata['university_id']);
        //$timezone = $universityDetail[0]['timezone'];

        if(isset($userdata['university_id']))
        {
            $universityDetail = $this->university_model->getUniversityById($userdata['university_id']);
            $timezone = isset($universityDetail[0]['timezone']) ? $universityDetail[0]['timezone'] : '';
            $universityList = ["name"=>$userdata['name'], "id"=>$userdata['university_id']];
        }
        else
        {
            $timezone = 5.5;
            $universityList = $this->university_model->getAllUniversitiesActive();
        }

        $this->outputData['time_array'] = array('12 AM', '12:15 AM', '12:30 AM', '12:45 AM', '01:00 AM', '01:15 AM', '01:30 AM', '01:45 AM', '02:00 AM', '02:15 AM', '02:30 AM', '02:45 AM', '03:00 AM', '03:15 AM', '03:30 AM', '03:45 AM', '04:00 AM', '04:15 AM', '04:30 AM', '04:45 AM', '05:00 AM', '05:15 AM', '05:30 AM', '05:45 AM', '06:00 AM', '06:15 AM', '06:30 AM', '06:45 AM', '07:00 AM', '07:15 AM', '07:30 AM', '07:45 AM', '08:00 AM', '08:15 AM', '08:30 AM', '08:45 AM', '09:00 AM', '09:15 AM', '09:30 AM', '09:45 AM', '10:00 AM', '10:15 AM', '10:30 AM', '10:45 AM', '11:00 AM', '11:15 AM', '11:30 AM', '11:45 AM', '12:00 PM', '12:15 PM', '12:30 PM', '12:45 PM', '01:00 PM', '01:15 PM', '01:30 PM', '01:45 PM', '02:00 PM', '02:15 PM', '02:30 PM', '02:45 PM', '03:00 PM', '03:15 PM', '03:30 PM', '03:45 PM', '04:00 PM', '04:15 PM', '04:30 PM', '04:45 PM', '05:00 PM', '05:15 PM', '05:30 PM', '05:45 PM', '06:00 PM', '06:15 PM', '06:30 PM', '06:45 PM', '07:00 PM', '07:15 PM', '07:30 PM', '07:45 PM', '08:00 PM', '08:15 PM', '08:30 PM', '08:45 PM', '09:00 PM', '09:15 PM', '09:30 PM', '09:45 PM', '10:00 PM', '10:15 PM', '10:30 PM', '10:45 PM', '11:00 PM', '11:15 PM', '11:30 PM', '11:45 PM');

        $time_array = array('00:00:00' => '12 AM', '00:15:00' => '12:15 AM', '00:30:00' => '12:30 AM', '00:45:00' => '12:45 AM', '01:00:00' => '01:00 AM', '01:15:00' => '01:15 AM', '01:30:00' => '01:30 AM', '01:45:00' => '01:45 AM', '02:00:00' => '02:00 AM', '02:15:00' => '02:15 AM', '02:30:00' => '02:30 AM', '02:45:00' => '02:45 AM', '03:00:00' => '03:00 AM', '03:15:00' => '03:15 AM', '03:30:00' => '03:30 AM', '03:45:00' => '03:45 AM', '04:00:00' => '04:00 AM', '04:15:00' => '04:15 AM', '04:30:00' => '04:30 AM', '04:45:00' => '04:45 AM', '05:00:00' => '05:00 AM', '05:15:00' => '05:15 AM', '05:30:00' => '05:30 AM', '05:45:00' => '05:45 AM', '06:00:00' => '06:00 AM', '06:15:00' => '06:15 AM', '06:30:00' => '06:30 AM', '06:45:00' => '06:45 AM', '07:00:00' => '07:00 AM', '07:15:00' => '07:15 AM', '07:30:00' => '07:30 AM', '07:45:00' => '07:45 AM', '08:00:00' => '08:00 AM', '08:15:00' => '08:15 AM', '08:30:00' => '08:30 AM', '08:45:00' => '08:45 AM', '09:00:00' => '09:00 AM', '09:15:00' => '09:15 AM', '09:30:00' => '09:30 AM', '09:45:00' => '09:45 AM', '10:00:00' => '10:00 AM', '10:15:00' => '10:15 AM', '10:30:00' => '10:30 AM', '10:45:00' => '10:45 AM', '11:00:00' => '11:00 AM', '11:15:00' => '11:15 AM', '11:30:00' => '11:30 AM', '11:45:00' => '11:45 AM', '12:00:00' => '12:00 PM', '12:15:00' => '12:15 PM', '12:30:00' => '12:30 PM', '12:45:00' => '12:45 PM', '13:00:00' => '01:00 PM', '13:15:00' => '01:15 PM', '13:30:00' => '01:30 PM', '13:45:00' => '01:45 PM', '14:00:00' => '02:00 PM', '14:15:00' => '02:15 PM', '14:30:00' => '02:30 PM', '14:45:00' => '02:45 PM', '15:00:00' => '03:00 PM', '15:15:00' => '03:15 PM', '15:30:00' => '03:30 PM', '15:45:00' => '03:45 PM', '16:00:00' => '04:00 PM', '16:15:00' => '04:15 PM', '16:30:00' => '04:30 PM', '16:45:00' => '04:45 PM', '17:00:00' => '05:00 PM', '17:15:00' => '05:15 PM', '17:30:00' => '05:30 PM', '17:45:00' => '05:45 PM', '18:00:00' => '06:00 PM', '18:15:00' => '06:15 PM', '18:30:00' => '06:30 PM', '18:45:00' => '06:45 PM', '19:00:00' => '07:00 PM', '19:15:00' => '07:15 PM', '19:30:00' => '07:30 PM', '19:45:00' => '07:45 PM', '20:00:00' => '08:00 PM', '20:15:00' => '08:15 PM', '20:30:00' => '08:30 PM', '20:45:00' => '08:45 PM', '21:00:00' => '09:00 PM', '21:15:00' => '09:15 PM', '21:30:00' => '09:30 PM', '21:45:00' => '09:45 PM', '22:00:00' => '10:00 PM', '22:15:00' => '10:15 PM', '22:30:00' => '10:30 PM', '22:45:00' => '10:45 PM', '23:00:00' => '11:00 PM', '23:15:00' => '11:15 PM', '23:30:00' => '11:30 PM', '23:45:00' => '11:45 PM');

        $this->outputData['university_list'] = $universityList;
        $this->render_page('templates/user/predefined_slot_form', $this->outputData);
    }

    function predefineslotcreate(){

      $this->load->helper('cookie_helper');
      $this->load->model('user/user_model');
      $this->load->model('webinar/webinar_model');
      $userdata=$this->session->userdata('user');
      if(empty($userdata)){  redirect('common/login'); }

      $time_array = array('00:00:00', '00:15:00', '00:30:00', '00:45:00', '01:00:00', '01:15:00', '01:30:00', '01:45:00', '02:00:00', '02:15:00', '02:30:00', '02:45:00', '03:00:00', '03:15:00', '03:30:00', '03:45:00', '04:00:00', '04:15:00', '04:30:00', '04:45:00', '05:00:00', '05:15:00', '05:30:00', '05:45:00', '06:00:00', '06:15:00', '06:30:00', '06:45:00', '07:00:00', '07:15:00', '07:30:00', '07:45:00', '08:00:00', '08:15:00', '08:30:00', '08:45:00', '09:00:00', '09:15:00', '09:30:00', '09:45:00', '10:00:00', '10:15:00', '10:30:00', '10:45:00', '11:00:00', '11:15:00', '11:30:00', '11:45:00', '12:00:00', '12:15:00', '12:30:00', '12:45:00', '13:00:00', '13:15:00', '13:30:00', '13:45:00', '14:00:00', '14:15:00', '14:30:00', '14:45:00', '15:00:00', '15:15:00', '15:30:00', '15:45:00', '16:00:00', '16:15:00', '16:30:00', '16:45:00', '17:00:00', '17:15:00', '17:30:00', '17:45:00', '18:00:00', '18:15:00', '18:30:00', '18:45:00', '19:00:00', '19:15:00', '19:30:00', '19:45:00', '20:00:00', '20:15:00', '20:30:00', '20:45:00', '21:00:00', '21:15:00', '21:30:00', '21:45:00', '22:00:00', '22:15:00', '22:30:00', '22:45:00', '23:00:00', '23:15:00', '23:30:00', '23:45:00');

      $data['university_id'] = $this->input->post('university_id');

      $istTimezone = 5.5 - (float)$this->input->post('timezone');
      $data['timezone'] = $this->input->post('timezone');

      if($this->input->post('slot_1_date'))
      {
          $data['slot_1'] = $this->input->post('slot_1_date') . " " . $time_array[$this->input->post('slot_1_time')];
          $data['slot_1'] = date('Y-m-d H:i:s', strtotime($data['slot_1']) + $istTimezone*60*60);
      }
      if($this->input->post('slot_2_date'))
      {
          $data['slot_2'] = $this->input->post('slot_2_date') . " " . $time_array[$this->input->post('slot_2_time')];
          $data['slot_2'] = date('Y-m-d H:i:s', strtotime($data['slot_2']) + $istTimezone*60*60);
      }
      if($this->input->post('slot_3_date'))
      {
          $data['slot_3'] = $this->input->post('slot_3_date') . " " . $time_array[$this->input->post('slot_3_time')];
          $data['slot_3'] = date('Y-m-d H:i:s', strtotime($data['slot_3']) + $istTimezone*60*60);
      }

      //$data['status'] = '1';
      $data['slots_created_by'] = $userdata['id'];

      $slotAdd = $this->webinar_model->post($data,'predefine_slots');

      redirect('/user/session/predefineslots');

    }

    function slotedit(){

    }

    function predefineslotdelete(){

      $this->load->helper('cookie_helper');
      $this->load->model('user/user_model');
      $this->load->model('webinar/webinar_model');
      $userdata=$this->session->userdata('user');
      if(empty($userdata)){  redirect('common/login'); }

      $predefineSlotId = $this->uri->segment('4');

      $whereCondition['id'] = $predefineSlotId;
      $webinarData['status'] = '0';

      $deletePreDefineSlotsId = $this->webinar_model->put($whereCondition, $webinarData, 'predefine_slots');

      redirect('/user/session/predefineslots');

    }

    function slotlist(){

    }
}
?>
