<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com


 */

// ini_set('display_errors', '1');
//  ini_set('display_startup_errors', '1');
//  error_reporting(E_ALL);
ini_set('memory_limit', '2048M');

class Course extends MY_Controller
{
  //Global variable
  public $outputData;    //Holds the output data for each view
  public $loggedInUser;

  /**
   * Constructor
   *
   * Loads language files and models needed for this controller
   */

  function __construct()
  {
    parent::__construct();
    $this->load->library('template');
    $this->lang->load('enduser/home', $this->config->item('language_code'));
    $this->load->library('form_validation');
  }

  function ajax_manage_page()
  {
    //echo "<pre>"; print_r($this->uri->segment(4)); exit;
    $this->load->helper('cookie_helper');

    $this->load->model('user/course_model');
    $this->load->model('filter_model');

    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    $affiliated_id = $this->uri->segment(4);
    $university_id = $this->uri->segment(5);
    $campus_id = $this->uri->segment(6);
    $college_id = $this->uri->segment(7);
    $department_id = $this->uri->segment(8);
    $degree_id = $this->uri->segment(9);
    $course_id = $this->uri->segment(10);

    if ($affiliated_id != 0) {

      // $selectedAffiliatedId = $this->uri->segment(4);
      $selectedAffiliatedId = isset($affiliated_id) &&  $affiliated_id != "" ? $affiliated_id : 0;
      $selectedUniversityId = isset($university_id) &&  $university_id != "" ? $university_id : 0;
      $selectedCampusId = isset($campus_id) && $campus_id != "" ? $campus_id : 0;
      $selectedCollegeId = isset($college_id) && $college_id != "" ? $college_id : 0;
      $selectedDepartmentId = isset($department_id) && $department_id != "" ? $department_id : 0;
      $selectedDegreeId = isset($degree_id) && $degree_id != "" ? $degree_id : 0;
      $selectedCourseId = isset($course_id) && $course_id != "" ? $course_id : 0;
      //echo "<pre>"; print_r($selectedAffiliatedId); exit;
      $filterLevel = 1;

      $allAffiliatedList = [];
      $allUniversityList = [];
      $allCampusList = [];
      $allCollegeList = [];
      $allDepartmentList = [];
      $allCourseList = [];

      $allDegreeList = $this->filter_model->getAllDegreeByDepartmentId($selectedDepartmentId);

      $allAffiliatedList = $this->filter_model->getAllAffiliatedId();
      //echo "<pre>";print_r($this->db->last_query());
      //echo "<pre>";print_r($allAffiliatedList);
      if ($selectedAffiliatedId) {
        $allUniversityList = $this->filter_model->getAllUniversityById($selectedAffiliatedId);
        $filterLevel = 1;
      }

      //echo "<pre>";print_r($this->db->last_query());
      //echo "<pre>";print_r(count($allUniversityList));   exit;

      if ($selectedUniversityId) {
        $allCampusList = $this->filter_model->getAllCampusesByUniversityId($selectedUniversityId);
        $filterLevel = 2;
      }

      if ($selectedCampusId) {
        $allCollegeList = $this->filter_model->getAllCollegeByCampusId($selectedCampusId);
        $filterLevel = 3;
      }

      if ($selectedCollegeId) {
        $allDepartmentList = $this->filter_model->getAllDepartmentByCollegeId($selectedCollegeId);
        $filterLevel = 4;
      }

      if ($selectedDepartmentId) {
        $allDegreeList = $this->filter_model->getAllDegreeByDepartmentId($selectedDepartmentId);

        $filterLevel = 5;
      }

      if ($selectedDegreeId) {
        $allCourseList = $this->filter_model->getAllCoursesByDepartmentId($selectedDepartmentId, $selectedDegreeId);

        $filterLevel = 6;
      }


      /*$courseListObj = $this->filter_model->getFilterCourses($selectedAffiliatedId,$selectedUniversityId,$selectedCampusId,$selectedCollegeId,$selectedDepartmentId,$selectedDegreeId,$selectedCourseId);*/

      $courseListObj = $this->filter_model->get_datatables_course_filter($selectedAffiliatedId, $selectedUniversityId, $selectedCampusId, $selectedCollegeId, $selectedDepartmentId, $selectedDegreeId, $selectedCourseId);

      $columnArray = [
        "name",
        "duration",
        "deadline_link",
        "intake",
        "total_fees",
        "contact_title",
        "contact_name",
        "contact_email",
        "eligibility_gpa",
        "eligibility_gre",
        "eligibility_toefl",
        "eligibility_ielts",
        "expected_salary",
        "eligibility_work_experience",
        "gpa_scale",
        "lor",
        "sop",
        "eligibility_gmat",
        "eligibility_sat",
        "degree_substream",
        "no_of_intake",
        "intake_months",
        "application_fee",
        "application_fee_amount",
        "application_fee_waiver_imperial",
        "cost_of_living_year",
        "test_required",
        "eligibility_english_proficiency",
        "eligibility_pte",
        "eligibility_duolingo",
        "resume_required",
        "admission_requirements",
        "backlogs",
        "drop_years",
        "scholarship_available",
        "with_15_years_education",
        "wes_requirement",
        "work_while_studying",
        "part_time_work_details",
        "cooperate_internship_participation",
        "work_permit",
        "start_months",
        "decision_months",
        "conditional_admit",
        "no_of_month_work_permit",
        "description"
      ];

      $columnCount = count($columnArray);

      $courseListArray = json_decode(json_encode($courseListObj, true), true);

      foreach ($courseListArray as $keyl => $valuel) {

        $countf = 0;

        foreach ($columnArray as $keyv) {

          if ($valuel[$keyv] != NULL) {

            $countf = $countf + 1;
          }
        }

        $courseListArray[$keyl]['filled_value'] = $countf;
        $courseListArray[$keyl]['column_count'] = $columnCount;
        $courseListArray[$keyl]['completed'] = round(($countf / $columnCount) * 100);
      }

      $courses = json_decode(json_encode($courseListArray), FALSE);

      $data = array();
      $no = $_POST['start'];

      foreach ($courses as $person) {

        $btn = anchor(base_url('/user/course/form/' . $person->id . '/' . $filterLevel), '<span title="Modify" class="edit"><i class="fa fa-edit"></i></span>');
        $btn .= '&nbsp;|&nbsp;' . '<a href="javascript:void();" title="Delete" class="del btn btn-danger btn-circle btn-sm" id="<?php echo $person->id;?>"><i class="fa fa-trash-o"></i></a>';

        //echo "<pre>"; print_r($person); exit;
        $no++;
        $row = array();
        $row[] = '<input type="checkbox" class="multichk" name="chk[]" value="<?php echo $person->id;?>"/>' . $no;
        $row[] = $person->name;
        $row[] = $person->college_name;
        $row[] = $person->university_name;
        $row[] = $person->degree_name;
        $row[] = $person->duration;
        $row[] = $person->deadline_link;
        $row[] = $person->total_fees;
        $row[] = $person->completed;
        $row[] = $btn;

        $data[] = $row;
      }


      $output = array(
        "draw" => $_POST['draw'],
        "recordsTotal" => $this->filter_model->count_all_course_filter($selectedAffiliatedId, $selectedUniversityId, $selectedCampusId, $selectedCollegeId, $selectedDepartmentId, $selectedDegreeId, $selectedCourseId),
        "recordsFiltered" => $this->filter_model->count_filtered_course_filter($selectedAffiliatedId, $selectedUniversityId, $selectedCampusId, $selectedCollegeId, $selectedDepartmentId, $selectedDegreeId, $selectedCourseId),
        "data" => $data,
        "selected_affiliated_id" => $selectedAffiliatedId,
        "affiliated_list" => $allAffiliatedList,
        "selected_university_id" => $selectedUniversityId,
        "university_list" => $allUniversityList,
        "selected_campus_id" => $selectedCampusId,
        "campus_list" => $allCampusList,
        "selected_college_id" => $selectedCollegeId,
        "college_list" => $allCollegeList,
        "selected_department_id" => $selectedDepartmentId,
        "department_list" => $allDepartmentList,
        "selected_course_id" => $selectedCourseId,
        "course_list" => $allCourseList,
        "selected_degree_id" => $selectedDegreeId,
        "degree_list" => $allDegreeList,
        "filter_level" => $filterLevel,

      );
    } else {

      $columnArray = [
        "name",
        "duration",
        "deadline_link",
        "intake",
        "total_fees",
        "contact_title",
        "contact_name",
        "contact_email",
        "eligibility_gpa",
        "eligibility_gre",
        "eligibility_toefl",
        "eligibility_ielts",
        "expected_salary",
        "eligibility_work_experience",
        "gpa_scale",
        "lor",
        "sop",
        "eligibility_gmat",
        "eligibility_sat",
        "degree_substream",
        "no_of_intake",
        "intake_months",
        "application_fee",
        "application_fee_amount",
        "application_fee_waiver_imperial",
        "cost_of_living_year",
        "test_required",
        "eligibility_english_proficiency",
        "eligibility_pte",
        "eligibility_duolingo",
        "resume_required",
        "admission_requirements",
        "backlogs",
        "drop_years",
        "scholarship_available",
        "with_15_years_education",
        "wes_requirement",
        "work_while_studying",
        "part_time_work_details",
        "cooperate_internship_participation",
        "work_permit",
        "start_months",
        "decision_months",
        "conditional_admit",
        "no_of_month_work_permit",
        "description",
        "admission_link",
        "program_link"
      ];

      $columnCount = count($columnArray);

      if ($userdata['type'] == 3) {
        // $courseListObj = $this->course_model->getAllCoursesByUniversity($userdata['university_id']);
        $courseListObj = $this->course_model->get_datatables($userdata['university_id']);

        $courseListArray = json_decode(json_encode($courseListObj, true), true);

        foreach ($courseListArray as $keyl => $valuel) {

          $countf = 0;

          foreach ($columnArray as $keyv) {

            if ($valuel[$keyv] != NULL) {

              $countf = $countf + 1;
            }
          }

          $courseListArray[$keyl]['filled_value'] = $countf;
          $courseListArray[$keyl]['column_count'] = $columnCount;
          $courseListArray[$keyl]['completed'] = round(($countf / $columnCount) * 100);
        }

        $courses = json_decode(json_encode($courseListArray), FALSE);
        $affiliated_list = $this->filter_model->getAllAffiliatedId();

        //$this->outputData['university_list'] = $this->filter_model->getAllUniversityById($userdata['university_id']);
      } else {
        // $courseListObj = $this->course_model->getAllCourses();
        $courseListObj = $this->course_model->get_datatables_course();

        $courseListArray = json_decode(json_encode($courseListObj, true), true);

        foreach ($courseListArray as $keyl => $valuel) {

          $countf = 0;

          foreach ($columnArray as $keyv) {

            if ($valuel[$keyv] != NULL) {

              $countf = $countf + 1;
            }
          }

          $courseListArray[$keyl]['filled_value'] = $countf;
          $courseListArray[$keyl]['column_count'] = $columnCount;
          $courseListArray[$keyl]['completed'] = round(($countf / $columnCount) * 100);
        }

        $courses = json_decode(json_encode($courseListArray), FALSE);

        $affiliated_list = $this->filter_model->getAllAffiliatedId();
      }

      $filter_level = 1;

      $data = array();
      $no = $_POST['start'];

      foreach ($courses as $person) {

        $btn = anchor(base_url('/user/course/form/' . $person->id . '/' . $filter_level), '<span title="Modify" class="edit"><i class="fa fa-edit"></i></span>');
        $btn .= '&nbsp;|&nbsp;' . '<a href="javascript:void();" title="Delete" class="del btn btn-danger btn-circle btn-sm" id="<?php echo $person->id;?>"><i class="fa fa-trash-o"></i></a>';

        //echo "<pre>"; print_r($person); exit;
        $no++;
        $row = array();
        $row[] = '<input type="checkbox" class="multichk" name="chk[]" value="<?php echo $person->id;?>"/>' . $no;
        $row[] = $person->name;
        $row[] = $person->college_name;
        $row[] = $person->university_name;
        $row[] = $person->degree_name;
        $row[] = $person->duration;
        $row[] = $person->deadline_link;
        $row[] = $person->total_fees;
        $row[] = $person->completed;
        $row[] = $btn;

        $data[] = $row;
      }


      $allDegreeList = $this->filter_model->getAllDegreeByDepartmentId($selectedDepartmentId = 0);
      $degree_list = $allDegreeList;
      $selected_affiliated_id = isset($_POST['affiliated']) &&  $_POST['affiliated'] != "" ? $_POST['affiliated'] : 0;

      if ($userdata['type'] == 3) {
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->course_model->count_all($universityId),
          "recordsFiltered" => $this->course_model->count_filtered($universityId),
          "data" => $data,
          "affiliated_list" => $affiliated_list,
          "degree_list" => $degree_list,
          "filter_level" => $filter_level,
        );
      } else {
        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->course_model->count_all_course(),
          "recordsFiltered" => $this->course_model->count_filtered_course(),
          "data" => $data,
          "affiliated_list" => $affiliated_list,
          "degree_list" => $degree_list,
          "filter_level" => $filter_level,
          "selected_affiliated_id" => $selected_affiliated_id,

        );
      }
    }




    //echo "<pre>"; print_r($output); exit;
    //output to json format
    echo json_encode($output);
  }

  function index()
  {
    $this->load->helper('cookie_helper');

    $this->load->model('user/course_model');
    $this->load->model('filter_model');

    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }
    //echo "<pre>";print_r($userdata); exit;
    $columnArray = [
      "name",
      "duration",
      "deadline_link",
      "intake",
      "total_fees",
      "contact_title",
      "contact_name",
      "contact_email",
      "eligibility_gpa",
      "eligibility_gre",
      "eligibility_toefl",
      "eligibility_ielts",
      "expected_salary",
      "eligibility_work_experience",
      "gpa_scale",
      "lor",
      "sop",
      "eligibility_gmat",
      "eligibility_sat",
      "degree_substream",
      "no_of_intake",
      "intake_months",
      "application_fee",
      "application_fee_amount",
      "application_fee_waiver_imperial",
      "cost_of_living_year",
      "test_required",
      "eligibility_english_proficiency",
      "eligibility_pte",
      "eligibility_duolingo",
      "resume_required",
      "admission_requirements",
      "backlogs",
      "drop_years",
      "scholarship_available",
      "with_15_years_education",
      "wes_requirement",
      "work_while_studying",
      "part_time_work_details",
      "cooperate_internship_participation",
      "work_permit",
      "start_months",
      "decision_months",
      "conditional_admit",
      "no_of_month_work_permit",
      "description",
      "admission_link",
      "program_link"
    ];

    $columnCount = count($columnArray);

    if ($userdata['type'] == 3) {
      $courseListObj = $this->course_model->getAllCoursesByUniversity($userdata['university_id']);

      $courseListArray = json_decode(json_encode($courseListObj, true), true);

      foreach ($courseListArray as $keyl => $valuel) {

        $countf = 0;

        foreach ($columnArray as $keyv) {

          if ($valuel[$keyv] != NULL) {

            $countf = $countf + 1;
          }
        }

        $courseListArray[$keyl]['filled_value'] = $countf;
        $courseListArray[$keyl]['column_count'] = $columnCount;
        $courseListArray[$keyl]['completed'] = round(($countf / $columnCount) * 100);
      }

      $this->outputData['courses'] = json_decode(json_encode($courseListArray), FALSE);

      $this->outputData['university_list'] = $this->filter_model->getAllUniversityById($userdata['university_id']);
    } else {
      $courseListObj = $this->course_model->getAllCourses();

      $courseListArray = json_decode(json_encode($courseListObj, true), true);

      foreach ($courseListArray as $keyl => $valuel) {

        $countf = 0;

        foreach ($columnArray as $keyv) {

          if ($valuel[$keyv] != NULL) {

            $countf = $countf + 1;
          }
        }

        $courseListArray[$keyl]['filled_value'] = $countf;
        $courseListArray[$keyl]['column_count'] = $columnCount;
        $courseListArray[$keyl]['completed'] = round(($countf / $columnCount) * 100);
      }

      $this->outputData['courses'] = json_decode(json_encode($courseListArray), FALSE);

      //  $this->outputData['university_list'] = $this->filter_model->getAllUniversityById();
      $this->outputData['affiliated_list'] = $this->filter_model->getAllAffiliatedId();
    }

    $allDegreeList = $this->filter_model->getAllDegreeByDepartmentId($selectedDepartmentId = 0);
    $this->outputData['degree_list'] = $allDegreeList;

    $this->outputData['view'] = 'list';
    $this->outputData['filter_level'] = 1;
    $this->outputData['selected_affiliated_id'] = isset($_POST['affiliated']) &&  $_POST['affiliated'] != "" ? $_POST['affiliated'] : 0;
    $this->outputData['selected_university_id'] = isset($_POST['university_id']) &&  $_POST['university_id'] != "" ? $_POST['university_id'] : 0;
    $this->outputData['selected_campus_id'] = isset($_POST['campus_id']) && $_POST['campus_id'] != "" ? $_POST['campus_id'] : 0;
    $this->outputData['selected_college_id'] = isset($_POST['college_id']) && $_POST['college_id'] != "" ? $_POST['college_id'] : 0;
    $this->outputData['selected_department_id'] = isset($_POST['department_id']) && $_POST['department_id'] != "" ? $_POST['department_id'] : 0;
    $this->outputData['selected_degree_id'] = isset($_POST['degree_id']) && $_POST['degree_id'] != "" ? $_POST['degree_id'] : 0;
    $this->outputData['selected_course_id'] = isset($_POST['course_id']) && $_POST['course_id'] != "" ? $_POST['course_id'] : 0;

    /*echo '<pre>';
		print_r($this->outputData);
		echo '</pre>';
		exit();*/

    $this->render_page('templates/user/course', $this->outputData);
  }

  function coursebydepartmentid()
  {
    $this->load->helper('cookie_helper');
    $this->load->model('user/course_model');

    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    $departmentId = $_GET['department_id'];

    $this->outputData['courses'] = $this->course_model->getAllCoursesByDepartmentId($departmentId);
    $this->outputData['department_list'] = $this->course_model->getAllDepartmentByDepartmentId($departmentId);
    $this->outputData['selected_department_id'] = $departmentId;
    $this->outputData['view'] = 'list';

    $this->render_page('templates/user/coursebydepartment', $this->outputData);
  }

  function filter()
  {
    $this->load->helper('cookie_helper');
    $this->load->model('user/course_model');
    $this->load->model('filter_model');

    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    $selectedAffiliatedId = isset($_POST['affiliated']) &&  $_POST['affiliated'] != "" ? $_POST['affiliated'] : 0;
    $selectedUniversityId = isset($_POST['university_id']) &&  $_POST['university_id'] != "" ? $_POST['university_id'] : 0;
    $selectedCampusId = isset($_POST['campus_id']) && $_POST['campus_id'] != "" ? $_POST['campus_id'] : 0;
    $selectedCollegeId = isset($_POST['college_id']) && $_POST['college_id'] != "" ? $_POST['college_id'] : 0;
    $selectedDepartmentId = isset($_POST['department_id']) && $_POST['department_id'] != "" ? $_POST['department_id'] : 0;
    $selectedDegreeId = isset($_POST['degree_id']) && $_POST['degree_id'] != "" ? $_POST['degree_id'] : 0;
    $selectedCourseId = isset($_POST['course_id']) && $_POST['course_id'] != "" ? $_POST['course_id'] : 0;
    $filterLevel = 1;

    $allAffiliatedList = [];
    $allUniversityList = [];
    $allCampusList = [];
    $allCollegeList = [];
    $allDepartmentList = [];
    $allCourseList = [];

    $allDegreeList = $this->filter_model->getAllDegreeByDepartmentId($selectedDepartmentId);

    $allAffiliatedList = $this->filter_model->getAllAffiliatedId();

    if ($selectedAffiliatedId && $selectedAffiliatedId != 5) {
      $allUniversityList = $this->filter_model->getAllUniversityById($selectedAffiliatedId);
      $filterLevel = 1;
    } else {
      $allUniversityList = $this->filter_model->getAllUniversityById(0);
      $filterLevel = 1;
    }
    // echo "<pre>"; print_r($allUniversityList); exit;


    if ($selectedUniversityId) {
      $allCampusList = $this->filter_model->getAllCampusesByUniversityId($selectedUniversityId);
      $filterLevel = 2;
    }

    if ($selectedCampusId) {
      $allCollegeList = $this->filter_model->getAllCollegeByCampusId($selectedCampusId);
      $filterLevel = 3;
    }

    if ($selectedCollegeId) {
      $allDepartmentList = $this->filter_model->getAllDepartmentByCollegeId($selectedCollegeId);
      $filterLevel = 4;
    }

    if ($selectedDepartmentId) {
      $allDegreeList = $this->filter_model->getAllDegreeByDepartmentId($selectedDepartmentId);

      $filterLevel = 5;
    }

    if ($selectedDegreeId) {
      $allCourseList = $this->filter_model->getAllCoursesByDepartmentId($selectedDepartmentId, $selectedDegreeId);

      $filterLevel = 6;
    }


    $courseListObj = $this->filter_model->getFilterCourses($selectedAffiliatedId, $selectedUniversityId, $selectedCampusId, $selectedCollegeId, $selectedDepartmentId, $selectedDegreeId, $selectedCourseId);
    // echo "<pre>";print_r($this->db->last_query());
    //       echo "<pre>"; print_r(count($courseListObj)); exit;
    //$this->outputData['courses'] = $courseListObj;

    $columnArray = [
      "name",
      "duration",
      "deadline_link",
      "intake",
      "total_fees",
      "contact_title",
      "contact_name",
      "contact_email",
      "eligibility_gpa",
      "eligibility_gre",
      "eligibility_toefl",
      "eligibility_ielts",
      "expected_salary",
      "eligibility_work_experience",
      "gpa_scale",
      "lor",
      "sop",
      "eligibility_gmat",
      "eligibility_sat",
      "degree_substream",
      "no_of_intake",
      "intake_months",
      "application_fee",
      "application_fee_amount",
      "application_fee_waiver_imperial",
      "cost_of_living_year",
      "test_required",
      "eligibility_english_proficiency",
      "eligibility_pte",
      "eligibility_duolingo",
      "resume_required",
      "admission_requirements",
      "backlogs",
      "drop_years",
      "scholarship_available",
      "with_15_years_education",
      "wes_requirement",
      "work_while_studying",
      "part_time_work_details",
      "cooperate_internship_participation",
      "work_permit",
      "start_months",
      "decision_months",
      "conditional_admit",
      "no_of_month_work_permit",
      "description"
    ];

    $columnCount = count($columnArray);

    $courseListArray = json_decode(json_encode($courseListObj, true), true);

    foreach ($courseListArray as $keyl => $valuel) {

      $countf = 0;

      foreach ($columnArray as $keyv) {

        if ($valuel[$keyv] != NULL) {

          $countf = $countf + 1;
        }
      }

      $courseListArray[$keyl]['filled_value'] = $countf;
      $courseListArray[$keyl]['column_count'] = $columnCount;
      $courseListArray[$keyl]['completed'] = round(($countf / $columnCount) * 100);
    }

    $this->outputData['courses'] = json_decode(json_encode($courseListArray), FALSE);

    $this->outputData['selected_affiliated_id'] = $selectedAffiliatedId;
    $this->outputData['affiliated_list'] = $allAffiliatedList;
    $this->outputData['selected_university_id'] = $selectedUniversityId;
    $this->outputData['university_list'] = $allUniversityList;
    $this->outputData['selected_campus_id'] = $selectedCampusId;
    $this->outputData['campus_list'] = $allCampusList;
    $this->outputData['selected_college_id'] = $selectedCollegeId;
    $this->outputData['college_list'] = $allCollegeList;
    $this->outputData['selected_department_id'] = $selectedDepartmentId;
    $this->outputData['department_list'] = $allDepartmentList;
    $this->outputData['selected_course_id'] = $selectedCourseId;
    $this->outputData['course_list'] = $allCourseList;
    $this->outputData['degree_list'] = $allDegreeList;
    $this->outputData['selected_degree_id'] = $selectedDegreeId;
    $this->outputData['filter_level'] = $filterLevel;

    $this->outputData['view'] = 'list';
    $this->render_page('templates/user/course', $this->outputData);
  }

  function getUniversity()
  {
    $this->load->model('filter_model');
    $selectedAffiliatedId = isset($_POST['affiliated_id']) && $_POST['affiliated_id'] != "" ? $_POST['affiliated_id'] : 0;
    $allUniversityList = $this->filter_model->getAllUniversityById($selectedAffiliatedId);
    $response['university_list'] = $allUniversityList;
    header('Content-Type: application/json');
    echo json_encode($response);
  }

  function getCampus()
  {
    $this->load->model('filter_model');
    $selectedUniversityId = isset($_POST['university_id']) &&  $_POST['university_id'] != "" ? $_POST['university_id'] : 0;
    $allCampusList = $this->filter_model->getAllCampusesByUniversityId($selectedUniversityId);
    $response['campus_list'] = $allCampusList;
    header('Content-Type: application/json');
    echo json_encode($response);
  }

  function getCollege()
  {
    $this->load->model('filter_model');
    $selectedCampusId = isset($_POST['campus_id']) && $_POST['campus_id'] != "" ? $_POST['campus_id'] : 0;
    $allCollegeList = $this->filter_model->getAllCollegeByCampusId($selectedCampusId);
    $response['college_list'] = $allCollegeList;
    header('Content-Type: application/json');
    echo json_encode($response);
  }

  function getDepartments()
  {
    $this->load->model('filter_model');
    $selectedCollegeId = isset($_POST['college_id']) && $_POST['college_id'] != "" ? $_POST['college_id'] : 0;
    $allDepartmentList = $this->filter_model->getAllDepartmentByCollegeId($selectedCollegeId);
    $response['department_list'] = $allDepartmentList;
    header('Content-Type: application/json');
    echo json_encode($response);
  }

  function getDegree()
  {
    $this->load->model('filter_model');
    $selectedDepartmentId = isset($_POST['department_id']) && $_POST['department_id'] != "" ? $_POST['department_id'] : 0;
    $allDegreeList = $this->filter_model->getAllDegreeByDepartmentId($selectedDepartmentId);
    $response['degree_list'] = $allDegreeList;
    header('Content-Type: application/json');
    echo json_encode($response);
  }

  function getCourse()
  {
    $this->load->model('filter_model');
    $selectedDepartmentId = isset($_POST['department_id']) && $_POST['department_id'] != "" ? $_POST['department_id'] : 0;
    $selectedDegreeId = isset($_POST['degree_id']) && $_POST['degree_id'] != "" ? $_POST['degree_id'] : 0;
    $allCourseList = $this->filter_model->getAllCoursesByDepartmentId($selectedDepartmentId, $selectedDegreeId);
    $response['course_list'] = $allCourseList;
    header('Content-Type: application/json');
    echo json_encode($response);
  }

  public function filterData()
  {
    $this->load->model('filter_model');

    $selectedAffiliatedId = isset($_POST['affiliated']) && $_POST['affiliated'] != "" ? $_POST['affiliated'] : 0;
    $selectedUniversityId = isset($_POST['university_id']) && $_POST['university_id'] != "" ? $_POST['university_id'] : 0;
    $selectedCampusId = isset($_POST['campus_id']) && $_POST['campus_id'] != "" ? $_POST['campus_id'] : 0;
    $selectedCollegeId = isset($_POST['college_id']) && $_POST['college_id'] != "" ? $_POST['college_id'] : 0;
    $selectedDepartmentId = isset($_POST['department_id']) && $_POST['department_id'] != "" ? $_POST['department_id'] : 0;
    $selectedDegreeId = isset($_POST['degree_id']) && $_POST['degree_id'] != "" ? $_POST['degree_id'] : 0;
    $selectedCourseId = isset($_POST['course_id']) && $_POST['course_id'] != "" ? $_POST['course_id'] : 0;

    $courseListObj = $this->filter_model->getFilterCourses(
      $selectedAffiliatedId,
      $selectedUniversityId,
      $selectedCampusId,
      $selectedCollegeId,
      $selectedDepartmentId,
      $selectedDegreeId,
      $selectedCourseId
    );

    $columnArray = [
      "name",
      "duration",
      "deadline_link",
      "intake",
      "total_fees",
      "contact_title",
      "contact_name",
      "contact_email",
      "eligibility_gpa",
      "eligibility_gre",
      "eligibility_toefl",
      "eligibility_ielts",
      "expected_salary",
      "eligibility_work_experience",
      "gpa_scale",
      "lor",
      "sop",
      "eligibility_gmat",
      "eligibility_sat",
      "degree_substream",
      "no_of_intake",
      "intake_months",
      "application_fee",
      "application_fee_amount",
      "application_fee_waiver_imperial",
      "cost_of_living_year",
      "test_required",
      "eligibility_english_proficiency",
      "eligibility_pte",
      "eligibility_duolingo",
      "resume_required",
      "admission_requirements",
      "backlogs",
      "drop_years",
      "scholarship_available",
      "with_15_years_education",
      "wes_requirement",
      "work_while_studying",
      "part_time_work_details",
      "cooperate_internship_participation",
      "work_permit",
      "start_months",
      "decision_months",
      "conditional_admit",
      "no_of_month_work_permit",
      "description"
    ];

    $columnCount = count($columnArray);

    $courseListArray = json_decode(json_encode($courseListObj, true), true);

    foreach ($courseListArray as $keyl => $valuel) {
      $countf = 0;

      foreach ($columnArray as $keyv) {
        if (!empty($valuel[$keyv])) {
          $countf++;
        }
      }

      $courseListArray[$keyl]['filled_value'] = $countf;
      $courseListArray[$keyl]['column_count'] = $columnCount;
      $courseListArray[$keyl]['completed'] = round(($countf / $columnCount) * 100);
    }

    $courses = json_decode(json_encode($courseListArray), FALSE);

    header('Content-Type: application/json');
    if (!empty($courses)) {
      echo json_encode(['status' => 'success', 'data' => $courses]);
    } else {
      echo json_encode(['status' => 'success', 'data' => []]);
    }
  }


  /*function filter() {

         $this->load->helper('cookie_helper');
         $this->load->model('user/course_model');
         $this->load->model('filter_model');

         $userdata=$this->session->userdata('user');
        // echo "<pre>"; print_r($userdata); exit;
         if(empty($userdata)){
           redirect('common/login');
         }

          $selectedUniversityId = isset($_POST['university_id']) &&  $_POST['university_id'] != "" ? $_POST['university_id'] : 0;
          $selectedCampusId = isset($_POST['campus_id']) && $_POST['campus_id'] !="" ? $_POST['campus_id'] : 0;
          $selectedCollegeId = isset($_POST['college_id']) && $_POST['college_id'] !="" ? $_POST['college_id'] : 0;
          $selectedDepartmentId = isset($_POST['department_id']) && $_POST['department_id'] !="" ? $_POST['department_id'] : 0;
          $selectedDegreeId = isset($_POST['degree_id']) && $_POST['degree_id'] !="" ? $_POST['degree_id'] : 0;
          $selectedCourseId = isset($_POST['course_id']) && $_POST['course_id'] !="" ? $_POST['course_id'] : 0;

          $filterLevel = 1;

          $allUniversityList = [];
          $allCampusList = [];
          $allCollegeList = [];
          $allDepartmentList = [];
          $allCourseList = [];

          $allDegreeList = $this->filter_model->getAllDegreeByDepartmentId($selectedDepartmentId);

          if($userdata['university_id']) {
             $allUniversityList = $this->filter_model->getAllUniversityById($userdata['university_id']);
         } else {
             $allUniversityList = $this->filter_model->getAllUniversityById();
         }


          if($selectedUniversityId){
            $allCampusList = $this->filter_model->getAllCampusesByUniversityId($selectedUniversityId);
            $filterLevel = 1;
          }

          if($selectedCampusId){
            $allCollegeList = $this->filter_model->getAllCollegeByCampusId($selectedCampusId);
            $filterLevel = 2;
          }

          if($selectedCollegeId){
            $allDepartmentList = $this->filter_model->getAllDepartmentByCollegeId($selectedCollegeId);
            $filterLevel = 3;
          }

          if($selectedDepartmentId){
            $allDegreeList = $this->filter_model->getAllDegreeByDepartmentId($selectedDepartmentId);

            $filterLevel = 4;
          }

          if($selectedDegreeId){
            $allCourseList = $this->filter_model->getAllCoursesByDepartmentId($selectedDepartmentId,$selectedDegreeId);

            $filterLevel = 5;
          }


          $courseListObj = $this->filter_model->getFilterCourses($selectedUniversityId,$selectedCampusId,$selectedCollegeId,$selectedDepartmentId,$selectedDegreeId,$selectedCourseId);
          //$this->outputData['courses'] = $courseListObj;

          $columnArray =[
            "name","duration","deadline_link","intake","total_fees","contact_title","contact_name","contact_email",
            "eligibility_gpa", "eligibility_gre", "eligibility_toefl", "eligibility_ielts", "expected_salary", "eligibility_work_experience",
            "gpa_scale", "lor", "sop", "eligibility_gmat", "eligibility_sat", "degree_substream", "no_of_intake", "intake_months", "application_fee",
            "application_fee_amount", "application_fee_waiver_imperial", "cost_of_living_year", "test_required", "eligibility_english_proficiency",
            "eligibility_pte", "eligibility_duolingo", "resume_required", "admission_requirements", "backlogs", "drop_years", "scholarship_available",
            "with_15_years_education", "wes_requirement", "work_while_studying", "cooperate_internship_participation", "work_permit", "start_months",
            "decision_months", "conditional_admit", "no_of_month_work_permit", "description"
          ];

          $columnCount = count($columnArray);

          $courseListArray = json_decode(json_encode($courseListObj ,true),true);

          foreach ($courseListArray as $keyl => $valuel) {

            $countf = 0;

            foreach ( $columnArray as $keyv ) {

              if($valuel[$keyv] != NULL ){

                $countf = $countf + 1;

              }

            }

            $courseListArray[$keyl]['filled_value'] = $countf;
            $courseListArray[$keyl]['column_count'] = $columnCount;
            $courseListArray[$keyl]['completed'] = round( ( $countf / $columnCount ) * 100 );

          }

          $this->outputData['courses'] = json_decode(json_encode($courseListArray), FALSE);

          //echo"<pre>";
          //var_dump($this->outputData['courses']);
          //die();

          $this->outputData['selected_university_id'] = $selectedUniversityId;
          $this->outputData['university_list'] = $allUniversityList;
          $this->outputData['selected_campus_id'] = $selectedCampusId;
          $this->outputData['campus_list'] = $allCampusList;
          $this->outputData['selected_college_id'] = $selectedCollegeId;
          $this->outputData['college_list'] = $allCollegeList;
          $this->outputData['selected_department_id'] = $selectedDepartmentId;
          $this->outputData['department_list'] = $allDepartmentList;
          $this->outputData['selected_course_id'] = $selectedCourseId;
          $this->outputData['course_list'] = $allCourseList;
          $this->outputData['degree_list'] = $allDegreeList;
          $this->outputData['selected_degree_id'] = $selectedDegreeId;
          $this->outputData['filter_level'] = $filterLevel;

      $this->outputData['view'] = 'list';

      /*echo '<pre>';
      print_r($this->outputData);
      echo '</pre>';
      exit();*/

  /*    $this->render_page('templates/user/course',$this->outputData);

    }*/

  function filterCourseList()
  {

    $this->load->helper('cookie_helper');
    $this->load->model('user/course_model');
    $this->load->model('filter_model');

    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $selectedUniversityId = isset($_POST['university_id']) &&  $_POST['university_id'] != "" ? $_POST['university_id'] : 0;
    $selectedCampusId = isset($_POST['campus_id']) && $_POST['campus_id'] != "" ? $_POST['campus_id'] : 0;
    $selectedCollegeId = isset($_POST['college_id']) && $_POST['college_id'] != "" ? $_POST['college_id'] : 0;
    $selectedDepartmentId = isset($_POST['department_id']) && $_POST['department_id'] != "" ? $_POST['department_id'] : 0;
    $selectedDegreeId = isset($_POST['degree_id']) && $_POST['degree_id'] != "" ? $_POST['degree_id'] : 0;
    $selectedCourseId = 0;
    $filterLevel = isset($_POST['filter_level']) &&  $_POST['filter_level'] != "" ? $_POST['filter_level'] : 0;
    $databaseFieldName = isset($_POST['database_field_name']) &&  $_POST['database_field_name'] != "" ? "courses." . $_POST['database_field_name'] : "";

    $databaseFieldName1 = isset($_POST['database_field_name_one']) &&  $_POST['database_field_name_one'] != "" ? "courses." . $_POST['database_field_name_one'] : "";
    $databaseFieldName2 = isset($_POST['database_field_name_two']) &&  $_POST['database_field_name_two'] != "" ? "courses." . $_POST['database_field_name_two'] : "";

    if ($databaseFieldName) {
      $databaseFieldName = $databaseFieldName;
    } else {
      $databaseFieldName = "$databaseFieldName1, $databaseFieldName2";
    }

    if (isset($_POST['database_field_name']) && $_POST['database_field_name'] == "SCHOLARSHIPDETAIL") {
      $databaseFieldName = "courses.scholarship_available, courses.tuition_waivers, courses.research_assistantships, courses.teaching_assistantships, courses.graduate_assistantships, courses.others_link, courses.pte_accepted, courses.duolingo_accepted";
    }

    if ($filterLevel == 1) {
      $selectedCampusId = 0;
      $selectedCollegeId = 0;
      $selectedDepartmentId = 0;
      $selectedDegreeId = 0;
    }

    if ($filterLevel == 2) {
      $selectedCollegeId = 0;
      $selectedDepartmentId = 0;
      $selectedDegreeId = 0;
    }

    if ($filterLevel == 3) {
      $selectedDepartmentId = 0;
      $selectedDegreeId = 0;
    }

    if ($filterLevel == 4) {
      $selectedDegreeId = 0;
    }

    $courseListObj = $this->filter_model->getFilterCoursesList($selectedUniversityId, $selectedCampusId, $selectedCollegeId, $selectedDepartmentId, $selectedDegreeId, $selectedCourseId, $databaseFieldName);

    foreach ($courseListObj as $key => $value) {
      if (isset($value['eligibility_gre'])) {
        $courseListObj[$key]['eligibility_gre'] = json_decode($value['eligibility_gre'], true);
      }

      if (isset($value['lor'])) {
        $courseListObj[$key]['lor'] = json_decode($value['lor'], true);
      }

      if (isset($value['sop'])) {
        $courseListObj[$key]['sop'] = json_decode($value['sop'], true);
      }

      if (isset($value['backlogs'])) {
        $courseListObj[$key]['backlogs'] = json_decode($value['backlogs'], true);
      }

      if (isset($value['work_permit'])) {
        $courseListObj[$key]['work_permit'] = json_decode($value['work_permit'], true);
      }
    }

    $responseObj['status'] = 'SUCCESS';
    $responseObj['course_list'] = $courseListObj;

    header('Content-Type: application/json');
    echo json_encode($responseObj);
  }

  function multipleUpdate()
  {

    $this->load->helper('cookie_helper');
    $this->load->model('webinar/webinar_model');

    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $arrayCount = isset($_POST['array_count']) &&  $_POST['array_count'] != "" ? $_POST['array_count'] : 0;
    $databaseFieldName = isset($_POST['update_field']) &&  $_POST['update_field'] != "" ? $_POST['update_field'] : 0;

    for ($i = 0; $i < $arrayCount; $i++) {

      if ($databaseFieldName == "intake_months" || $databaseFieldName == "start_months" || $databaseFieldName == "decision_months") {
        $upateValue = [
          $databaseFieldName => $_POST[$databaseFieldName . $i] ? json_encode(explode(',', $_POST[$databaseFieldName . $i])) : NULL
        ];
      } else {
        $upateValue = [
          $databaseFieldName => $_POST[$databaseFieldName . $i] ? $_POST[$databaseFieldName . $i] : NULL
        ];
      }


      $updateCondition = [
        "id" => $_POST["course_id" . $i]
      ];

      $updateCourseObj = $this->webinar_model->put($updateCondition, $upateValue, 'courses');
    }

    $courseId = $_POST["s_course_id"];
    $filterLevel = $_POST["filter_level"];

    redirect('/user/course/form/' . $courseId . '/' . $filterLevel);
  }

  function multipleUpdateCondition()
  {

    $this->load->helper('cookie_helper');
    $this->load->model('webinar/webinar_model');

    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $arrayCount = isset($_POST['array_count']) &&  $_POST['array_count'] != "" ? $_POST['array_count'] : 0;
    $updateJson = isset($_POST['update_json']) &&  $_POST['update_json'] != "" ? $_POST['update_json'] : 0;
    $databaseFieldNameOne = isset($_POST['update_field_one']) &&  $_POST['update_field_one'] != "" ? $_POST['update_field_one'] : 0;
    $databaseFieldNameTwo = isset($_POST['update_field_two']) &&  $_POST['update_field_two'] != "" ? $_POST['update_field_two'] : 0;
    $databaseFieldNameThree = isset($_POST['update_field_three']) &&  $_POST['update_field_three'] != "" ? $_POST['update_field_three'] : 0;

    if ($updateJson === 'eligibility_gre' || $updateJson === 'sop' || $updateJson === 'backlogs' || $updateJson === 'work_permit') {

      for ($i = 0; $i < $arrayCount; $i++) {

        $variableData = [
          "$databaseFieldNameOne"  => $_POST["$databaseFieldNameOne" . $i],
          "$databaseFieldNameTwo" => $_POST["$databaseFieldNameTwo" . $i]
        ];

        $upateValue = [
          $updateJson => json_encode($variableData)
        ];

        $updateCondition = [
          "id" => $_POST["course_id" . $i]
        ];

        /*echo"<pre>";
          echo "1--<br/>";
          var_dump($upateValue);
          var_dump($updateCondition);*/

        $updateCourseObj = $this->webinar_model->put($updateCondition, $upateValue, 'courses');
      }

      //die();

    } else if ($updateJson === 'lor') {

      for ($i = 0; $i < $arrayCount; $i++) {

        $variableData = [
          "$databaseFieldNameOne"  => $_POST["$databaseFieldNameOne" . $i],
          "$databaseFieldNameTwo" => $_POST["$databaseFieldNameTwo" . $i],
          "$databaseFieldNameThree" => $_POST["$databaseFieldNameThree" . $i]
        ];

        $upateValue = [
          $updateJson => json_encode($variableData)
        ];

        $updateCondition = [
          "id" => $_POST["course_id" . $i]
        ];

        /*echo"<pre>";
          echo "2--<br/>";
          var_dump($upateValue);
          var_dump($updateCondition);*/

        $updateCourseObj = $this->webinar_model->put($updateCondition, $upateValue, 'courses');
      }

      //die();

    } else {

      for ($i = 0; $i < $arrayCount; $i++) {

        $upateValue = [
          $databaseFieldNameOne => $_POST[$databaseFieldNameOne . $i],
          $databaseFieldNameTwo => $_POST[$databaseFieldNameTwo . $i]
        ];

        $updateCondition = [
          "id" => $_POST["course_id" . $i]
        ];

        /*echo"<pre>";
          echo "3--<br/>";
          var_dump($upateValue);
          var_dump($updateCondition);*/

        $updateCourseObj = $this->webinar_model->put($updateCondition, $upateValue, 'courses');
      }

      //die();

    }

    $courseId = $_POST["s_course_id"];
    $filterLevel = $_POST["filter_level"];

    redirect('/user/course/form/' . $courseId . '/' . $filterLevel);
  }

  function multipleUpdateScholarshipAvailbility()
  {

    $this->load->helper('cookie_helper');
    $this->load->model('webinar/webinar_model');

    $userdata = $this->session->userdata('user');

    if (empty($userdata)) {
      redirect('common/login');
    }

    $arrayCount = isset($_POST['array_count']) &&  $_POST['array_count'] != "" ? $_POST['array_count'] : 0;
    $databaseFieldName = isset($_POST['update_field']) &&  $_POST['update_field'] != "" ? $_POST['update_field'] : 0;

    for ($i = 0; $i < $arrayCount; $i++) {

      $upateValue = [
        "scholarship_available" => $_POST["scholarship_available" . $i] ? $_POST["scholarship_available" . $i] : NULL,
        "tuition_waivers" => $_POST["tuition_waivers" . $i] ? $_POST["tuition_waivers" . $i] : NULL,
        "research_assistantships" => $_POST["research_assistantships" . $i] ? $_POST["research_assistantships" . $i] : NULL,
        "teaching_assistantships" => $_POST["teaching_assistantships" . $i] ? $_POST["teaching_assistantships" . $i] : NULL,
        "graduate_assistantships" => $_POST["graduate_assistantships" . $i] ? $_POST["graduate_assistantships" . $i] : NULL,
        "others_link" => $_POST["others_link" . $i] ? $_POST["others_link" . $i] : NULL
      ];

      $updateCondition = [
        "id" => $_POST["course_id" . $i]
      ];

      $updateCourseObj = $this->webinar_model->put($updateCondition, $upateValue, 'courses');
    }

    $courseId = $_POST["s_course_id"];
    $filterLevel = $_POST["filter_level"];

    redirect('/user/course/form/' . $courseId . '/' . $filterLevel);
  }

  function form()
  {
    $this->load->helper('cookie_helper');
    $this->load->model('user/department_model');
    $this->load->model('user/course_model');
    $this->load->model('filter_model');
    //$this->load->model('location/country_model');
    //$this->load->model('location/city_model');
    //$this->load->model('location/state_model');

    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    $this->outputData['sop_no_of_words'] = "none";
    $this->outputData['lor_required'] = "none";
    $this->outputData['lor_type'] = "none";
    $this->outputData['workPermit'] = 'none';
    $this->outputData['backlog'] = 'none';
    $this->outputData['applicationFee'] = 'none';
    $this->outputData['course_total_fee_count']   = 1;

    $courseId = $this->uri->segment('4');

    if ($courseId) {
      $courseData       = $this->course_model->getCourseByIdNew($courseId);
      $eligibility_gre  = $courseData->eligibility_gre ? json_decode($courseData->eligibility_gre) : '';
      $deadlines        = $courseData->deadlines_detail ? json_decode($courseData->deadlines_detail) : '';
      $backlogs         = $courseData->backlogs ? json_decode($courseData->backlogs) : '';
      $workPermit       = $courseData->work_permit ? json_decode($courseData->work_permit) : '';
      
      $this->outputData['id']               = $courseData->id;
      $this->outputData['name']             = $courseData->name;
      $this->outputData['department']       = $courseData->department_id;
      $this->outputData['category']         = $courseData->category_id;
      $this->outputData['degree']           = $courseData->degree_id;
      $this->outputData['duration']         = $courseData->duration;
      $this->outputData['deadline_link']    = $courseData->deadline_link;      
      $this->outputData['stem']             = $courseData->stem;

      $this->outputData['i_twenty_tuition_deposite']         = $courseData->i_twenty_tuition_deposite;
      $this->outputData['avg_tuition_fee']                   = $courseData->avg_tuition_fee;
      $this->outputData['deadline']                          = $courseData->deadline;
      $this->outputData['admission_link']                    = $courseData->admission_link;
      $this->outputData['program_link']                      = $courseData->program_link;      
      $this->outputData['total_fees']                        = $courseData->total_fees;
      $this->outputData['contact_title']                     = $courseData->contact_title;
      $this->outputData['contact_name']                      = $courseData->contact_name;
      $this->outputData['contact_email']                     = $courseData->contact_email;
      $this->outputData['eligibility_gpa']                   = $courseData->eligibility_gpa;
      $this->outputData['gpa_scale']                         = $courseData->gpa_scale;
      $this->outputData['eligibility_gre_quant']             = isset($eligibility_gre->quant) ? $eligibility_gre->quant : '';
      $this->outputData['eligibility_gre_verbal']            = isset($eligibility_gre->verbal) ? $eligibility_gre->verbal : '';
      $this->outputData['spring_deadline']                   = isset($deadlines->spring) ? $deadlines->spring : '';
      $this->outputData['summer_deadline']                   = isset($deadlines->summer) ? $deadlines->summer : '';
      $this->outputData['fall_deadline']                     = isset($deadlines->fall) ? $deadlines->fall : '';
      $this->outputData['roll_over']                         = isset($deadlines->fall) ? $deadlines->roll_over : '';
      $this->outputData['eligibility_toefl']                 = $courseData->eligibility_toefl;
      $this->outputData['eligibility_ielts']                 = $courseData->eligibility_ielts;
      $this->outputData['expected_salary']                   = $courseData->expected_salary;
      $this->outputData['eligibility_work_experience']       = $courseData->eligibility_work_experience;
      $this->outputData['sop']                               = $courseData->sop ? json_decode($courseData->sop) : '';
      $this->outputData['lor']                               = $courseData->lor ? json_decode($courseData->lor) : '';      
      $this->outputData['degree_substream']                  = $courseData->degree_substream;
      $this->outputData['no_of_intake']                      = $courseData->no_of_intake;
      $this->outputData['selectedMonths']                    = $courseData->intake_months ? json_decode($courseData->intake_months, true): '';
      $this->outputData['application_fee']                   = $courseData->application_fee;
      $this->outputData['application_fee_amount']            = $courseData->application_fee_amount;

      $this->outputData['application_fee_waiver_imperial']         = $courseData->application_fee_waiver_imperial;
      $this->outputData['cost_of_living_year']                     = $courseData->cost_of_living_year;
      $this->outputData['test_required']                           = $courseData->test_required;
      $this->outputData['eligibility_gmat']                        = $courseData->eligibility_gmat;
      $this->outputData['eligibility_sat']                         = $courseData->eligibility_sat;
      $this->outputData['eligibility_english_proficiency']         = $courseData->eligibility_english_proficiency;
      $this->outputData['eligibility_pte']                         = $courseData->eligibility_pte;
      $this->outputData['eligibility_duolingo']                    = $courseData->eligibility_duolingo;
      $this->outputData['resume_required']                         = $courseData->resume_required;
      $this->outputData['admission_requirements']                  = $courseData->admission_requirements;
      $this->outputData['backlog_accepted']                        = isset($backlogs->backlog_accepted) ? $backlogs->backlog_accepted : '';
      $this->outputData['number_of_backlogs']                      = isset($backlogs->number_of_backlogs) ? $backlogs->number_of_backlogs : '';
      $this->outputData['drop_years']                              = $courseData->drop_years;
      $this->outputData['scholarship_available']                   = $courseData->scholarship_available;
      $this->outputData['with_15_years_education']                 = $courseData->with_15_years_education;
      $this->outputData['wes_requirement']                         = $courseData->wes_requirement;
      $this->outputData['wes_type']                                = $courseData->wes_type;
      $this->outputData['wes_type_div']                            = $this->outputData['wes_requirement'] == 'Yes' ? 'block' : 'none';      
      $this->outputData['work_while_studying']                     = $courseData->work_while_studying;
      $this->outputData['part_time_work_details']                  = $courseData->part_time_work_details;
      $this->outputData['cooperate_internship_participation']      = $courseData->cooperate_internship_participation;
      $this->outputData['work_permit_needed']                      = isset($workPermit->work_permit_needed) ? $workPermit->work_permit_needed : '';
      $this->outputData['no_of_months']                            = isset($workPermit->no_of_months) ? $workPermit->no_of_months : '';
      $this->outputData['start_months']                            = $courseData->start_months ? implode(',', json_decode($courseData->start_months, true)) : '';
      $this->outputData['decision_months']                         = $courseData->decision_months ? implode(',', json_decode($courseData->decision_months, true)) : '';
      $this->outputData['conditional_admit']                       = $courseData->conditional_admit;
      
      $this->outputData['sop_no_of_words']            = isset($this->outputData['sop']->sop_needed) && $this->outputData['sop']->sop_needed ? 'block' : 'none';
      $this->outputData['lor_required']               = isset($this->outputData['lor']->lor_needed) && $this->outputData['lor']->lor_needed ? 'block' : 'none';
      $this->outputData['lor_type']                   = isset($this->outputData['lor']->lor_needed) && $this->outputData['lor']->lor_needed ? 'block' : 'none';
      $this->outputData['workPermit']                 = $this->outputData['work_permit_needed'] == '1' ? 'block' : 'none';
      $this->outputData['backlog']                    = $this->outputData['backlog_accepted'] == '1' ? 'block' : 'none';
      $this->outputData['applicationFee']             = $this->outputData['application_fee'] == 'Yes' ? 'block' : 'none';      
      $this->outputData['tuition_waivers']            = isset($courseData->tuition_waivers) ? $courseData->tuition_waivers : '';
      $this->outputData['research_assistantships']    = isset($courseData->research_assistantships) ? $courseData->research_assistantships : '';
      $this->outputData['teaching_assistantships']    = isset($courseData->teaching_assistantships) ? $courseData->teaching_assistantships : '';
      $this->outputData['graduate_assistantships']    = isset($courseData->graduate_assistantships) ? $courseData->graduate_assistantships : '';
      $this->outputData['others_link']                = isset($courseData->others_link) ? $courseData->others_link : '';
      $this->outputData['transcripts']                = isset($courseData->transcripts) ? $courseData->transcripts : '';
      $this->outputData['financial_documentation']    = isset($courseData->financial_documentation) ? $courseData->financial_documentation : '';
      $this->outputData['test_score_waivers']         = isset($courseData->test_score_waivers) ? $courseData->test_score_waivers : '';
      $this->outputData['test_score_reporting']       = isset($courseData->test_score_reporting) ? $courseData->test_score_reporting : '';
      $this->outputData['gre_code']                   = isset($courseData->gre_code) ? $courseData->gre_code : '';
      $this->outputData['tofel_code']                 = isset($courseData->tofel_code) ? $courseData->tofel_code : '';
      $this->outputData['gmat_code']                  = isset($courseData->gmat_code) ? $courseData->gmat_code : '';
      $this->outputData['sat_code']                   = isset($courseData->sat_code) ? $courseData->sat_code : '';
      $this->outputData['pre_requisites']             = isset($courseData->pre_requisites) ? $courseData->pre_requisites : '';
      $this->outputData['pre_requisites_link']        = isset($courseData->pre_requisites_link) ? $courseData->pre_requisites_link : '';
      $this->outputData['supervisor_details_canada']  = isset($courseData->supervisor_details_canada) ? $courseData->supervisor_details_canada : '';
      $this->outputData['pte_accepted']               = isset($courseData->pte_accepted) ? $courseData->pte_accepted : '';
      $this->outputData['duolingo_accepted']          = isset($courseData->duolingo_accepted) ? $courseData->duolingo_accepted : '';
      $this->outputData['change_course']              = isset($courseData->change_course) ? $courseData->change_course : '';      
      $this->outputData['scholarship_detail_div']     = isset($courseData->scholarship_available) && $courseData->scholarship_available == "Yes" ? 'block' : 'none';
      $this->outputData['pre_requisites_div']         = isset($courseData->pre_requisites) && $courseData->pre_requisites == "Yes" ? 'block' : 'none';
      $this->outputData['test_score_reporting_div']   = isset($courseData->test_score_reporting) && $courseData->test_score_reporting == "Official" ? 'block' : 'none';
      $this->outputData['course_total_fee']           = json_decode($courseData->course_total_fee, true);      
      $this->outputData['course_total_fee_count']     = count($this->outputData['course_total_fee']);      
      $this->outputData['selected_university_id']     = $courseData->university_id;
      $this->outputData['selected_campus_id']         = $courseData->campus_id;
      $this->outputData['selected_college_id']        = $courseData->college_id;
      $this->outputData['selected_department_id']     = $courseData->department_id;
      $this->outputData['description']                = $courseData->description;
      $this->outputData['status']                     = $courseData->status;
      // $this->outputData['intake_months']           = $courseData->intake_months ? implode(',', json_decode($courseData->intake_months, true)) : '';
      // $this->outputData['deadline_date_spring'] 		= $courseData->deadline_date_spring;
      // $this->outputData['deadline_date_fall'] 			= $courseData->deadline_date_fall;
      // $this->outputData['intake'] 				          = $courseData->intake;
      
      $selectedUniversityId   = $courseData->university_id ? $courseData->university_id : 0;
      $selectedCampusId       = $courseData->campus_id ? $courseData->campus_id : 0;
      $selectedCollegeId      = $courseData->college_id ? $courseData->college_id : 0;
      
      if ($selectedUniversityId) {
        $allCampusList = $this->filter_model->getAllCampusesByUniversityId($selectedUniversityId);
      }
      
      if ($selectedCampusId) {
        $allCollegeList = $this->filter_model->getAllCollegeByCampusId($selectedCampusId);
      }
      
      if ($selectedCollegeId) {
        $allDepartmentList = $this->filter_model->getAllDepartmentByCollegeId($selectedCollegeId);
      }

      $this->outputData['campus_list'] = $allCampusList;
      $this->outputData['college_list'] = $allCollegeList;
      $this->outputData['department_list'] = $allDepartmentList;
    } else {
      $this->outputData['id'] = 0;
    }

    if ($userdata['university_id']) {
      $allUniversityList = $this->filter_model->getAllUniversityById($userdata['university_id']);
    } else {
      $allUniversityList = $this->filter_model->getAllActiveUniversity();
    }

    $allDegrees = $this->course_model->getAllDegrees();
    $allCategories = $this->course_model->getAllCategories();

    $this->outputData['degrees'] = $allDegrees;
    $this->outputData['categories'] = $allCategories;
    $this->outputData['university_list'] = $allUniversityList;
    $this->outputData['stem_list'] = ['YES', 'NO'];
    $this->outputData['degree_substreams'] = ['Grade (1-8)', 'Grade (9-10)', 'High School (11th-12th)', 'UG Certificate (1 Year)', 'UG Diploma (2 Year)', 'UG Advanced Diploma (3 year)', 'UG Degree (3 Years)', 'UG Degree (4 years)', 'PG Diploma/Certificate', 'PG (Masters)', 'UG+PG (Accelerated) Degree', 'Doctoral Degree (PHd)', 'Foundation', 'Short Term Programs (Non Degree)', 'Pathway Programs', 'Twinning Programmes (UG)', 'Twinning Programmes (PG)'];
    $this->outputData['filter_level'] = $this->uri->segment('5') ? $this->uri->segment('5') : 0;

    $this->render_page('templates/user/course_form_new', $this->outputData);
  }

  function create()
  {
    if (!$this->input->post('name') || !$this->input->post('department') || !$this->input->post('category_id') || !$this->input->post('degree')) {
      $this->session->set_flashdata('flash_message', "Error: Please fill all mandatory fields!");
      redirect('user/course/form');
    }
    //die('+++++');
    $this->load->helper(array('form', 'url'));

    $this->load->library('form_validation');
    $this->load->model('user/course_model');

    $eligibility_gre = array(
      'quant' => $this->input->post('eligibility_gre_quant'),
      'verbal' => $this->input->post('eligibility_gre_verbal')
    );

    $deadlines = array(
      'spring' => $this->input->post('spring_deadline'),
      'summer' => $this->input->post('summer_deadline'),
      'fall' => $this->input->post('fall_deadline'),
      'roll_over' => $this->input->post('roll_over')
    );

    $sop = array();
    $lor = array();
    $backlogs = array();
    $workPermit = array();

    if ($this->input->post('sop_needed') != '') {
      $sop = array(
        'sop_needed'        => $this->input->post('sop_needed'),
        'sop_no_of_words'   => $this->input->post('sop_no_of_words')
      );
    }

    if ($this->input->post('lor_needed') != '') {
      $lor = array(
        'lor_needed'    => $this->input->post('lor_needed'),
        'lor_required'  => $this->input->post('lor_required'),
        'lor_type'      => $this->input->post('lor_type')
      );
    }

    if ($this->input->post('backlog_accepted') != '') {
      $backlogs = array(
        'backlog_accepted'        => $this->input->post('backlog_accepted'),
        'number_of_backlogs'   => $this->input->post('number_of_backlogs')
      );
    }

    if ($this->input->post('work_permit_needed') != '') {
      $workPermit = array(
        'work_permit_needed'        => $this->input->post('work_permit_needed'),
        'no_of_months'   => $this->input->post('no_of_months')
      );
    }

    foreach ($this->input->post('course_total_fee_currency') as $index => $feesCurrency) {
      $courseTotalFee[$index]['currency'] = $feesCurrency;
      $courseTotalFee[$index]['amount'] = $this->input->post('course_total_fee_amount')[$index];
    }

    $data = array(
      'name'               => $this->input->post('name'),
      'department_id'      => $this->input->post('department'),
      'category_id'        => $this->input->post('category_id') ? $this->input->post('category_id') : NULL,
      'degree_id'          => $this->input->post('degree'),
      'duration'           => $this->input->post('duration') ? $this->input->post('duration') : NULL,
      'stem'               => $this->input->post('stem'),

      'i_twenty_tuition_deposite'     => $this->input->post('i_twenty_tuition_deposite'),
      'avg_tuition_fee'               => $this->input->post('avg_tuition_fee'),
      'deadlines_detail'              => json_encode($deadlines),
      'admission_link'                => $this->input->post('admission_link'),
      'program_link'                  => $this->input->post('program_link'),

      'deadline_link'                   => $this->input->post('deadline_link'),
      'course_total_fee'                => json_encode($courseTotalFee),
      'total_fees'                      => $this->input->post('total_fees') ? $this->input->post('total_fees') : NULL,
      'contact_title'                   => $this->input->post('contact_title'),
      'contact_name'                    => $this->input->post('contact_name'),
      'contact_email'                   => $this->input->post('contact_email'),
      'eligibility_gpa'                 => $this->input->post('eligibility_gpa') ? $this->input->post('eligibility_gpa') : NULL,
      'gpa_scale'                       => $this->input->post('gpa_scale') ? $this->input->post('gpa_scale') : NULL,
      'eligibility_gre'                 => json_encode($eligibility_gre),
      'eligibility_toefl'               => $this->input->post('eligibility_toefl') ? $this->input->post('eligibility_toefl') : NULL,
      'eligibility_ielts'               => $this->input->post('eligibility_ielts') ? $this->input->post('eligibility_ielts') : NULL,
      'expected_salary'                 => $this->input->post('expected_salary') ? $this->input->post('expected_salary') : NULL,
      'eligibility_work_experience'     =>  $this->input->post('eligibility_work_experience') ? $this->input->post('eligibility_work_experience') : NULL,

      'date_created'        => date('Y-m-d H:i:s'),
      'date_updated'        => date('Y-m-d H:i:s'),
      'status'              => $this->input->post('status') ? $this->input->post('status') : 1,
      'added_by'            => 1,
      'updated_by'          => 1,
      'sop'                 => json_encode($sop),
      'lor'                 => json_encode($lor),

      'degree_substream'                => $this->input->post('degree_substream'),
      'no_of_intake'                    => $this->input->post('no_of_intake') ? $this->input->post('no_of_intake') : NULL,
      'intake_months'                   => $this->input->post('intake_months') ? json_encode($this->input->post('intake_months')) : NULL,
      'application_fee'                 => $this->input->post('application_fee') ? $this->input->post('application_fee') : NULL,
      'application_fee_amount'          => $this->input->post('application_fee_amount') ? $this->input->post('application_fee_amount') : NULL,
      'application_fee_waiver_imperial' => $this->input->post('application_fee_waiver_imperial') ? $this->input->post('application_fee_waiver_imperial') : NULL,
      'cost_of_living_year'             => $this->input->post('cost_of_living_year') ? $this->input->post('cost_of_living_year') : NULL,
      'test_required'                   => $this->input->post('test_required') ? $this->input->post('test_required') : NULL,
      'eligibility_gmat'                => $this->input->post('eligibility_gmat') ? $this->input->post('eligibility_gmat') : NULL,
      'eligibility_sat'                 => $this->input->post('eligibility_sat') ? $this->input->post('eligibility_sat') : NULL,
      'eligibility_english_proficiency' => $this->input->post('eligibility_english_proficiency') ? $this->input->post('eligibility_english_proficiency') : NULL,
      'eligibility_pte'                 => $this->input->post('eligibility_pte') ? $this->input->post('eligibility_pte') : NULL,
      'eligibility_duolingo'            => $this->input->post('eligibility_duolingo') ? $this->input->post('eligibility_duolingo') : NULL,
      'resume_required'                 => $this->input->post('resume_required') ? $this->input->post('resume_required') : NULL,
      'admission_requirements'          => $this->input->post('admission_requirements'),
      'backlogs'                        => json_encode($backlogs),
      'drop_years'                      => $this->input->post('drop_years') ? $this->input->post('drop_years') : NULL,
      'scholarship_available'           => $this->input->post('scholarship_available') ? $this->input->post('scholarship_available') : NULL,
      'with_15_years_education'         => $this->input->post('with_15_years_education') ? $this->input->post('with_15_years_education') : NULL,
      'wes_requirement'                 => $this->input->post('wes_requirement') ? $this->input->post('wes_requirement') : NULL,
      'wes_type'                        => $this->input->post('wes_type') ? $this->input->post('wes_type') : NULL,
      'work_while_studying'             => $this->input->post('work_while_studying') ? $this->input->post('work_while_studying') : NULL,
      'part_time_work_details'          => $this->input->post('part_time_work_details') ? $this->input->post('part_time_work_details') : NULL,

      'cooperate_internship_participation'     =>  $this->input->post('cooperate_internship_participation') ? $this->input->post('cooperate_internship_participation') : NULL,

      'work_permit'                     => json_encode($workPermit),
      'start_months'                    => $this->input->post('start_months') ? json_encode(explode(',', $this->input->post('start_months'))) : NULL,
      'decision_months'                 => $this->input->post('decision_months') ? json_encode(explode(',', $this->input->post('decision_months'))) : NULL,
      'conditional_admit'               => $this->input->post('conditional_admit') ? $this->input->post('conditional_admit') : NULL,
      'description'                     => $this->input->post('description'),
      'tuition_waivers'                 => $this->input->post('tuition_waivers') ? $this->input->post('tuition_waivers') : NULL,
      'research_assistantships'         => $this->input->post('research_assistantships') ? $this->input->post('research_assistantships') : NULL,
      'teaching_assistantships'         => $this->input->post('teaching_assistantships') ? $this->input->post('teaching_assistantships') : NULL,
      'graduate_assistantships'         => $this->input->post('graduate_assistantships') ? $this->input->post('graduate_assistantships') : NULL,
      'others_link'                     => $this->input->post('others_link') ? $this->input->post('others_link') : NULL,
      'transcripts'                     => $this->input->post('transcripts') ? $this->input->post('transcripts') : NULL,
      'financial_documentation'         => $this->input->post('financial_documentation') ? $this->input->post('financial_documentation') : NULL,
      'test_score_waivers'              => $this->input->post('test_score_waivers') ? $this->input->post('test_score_waivers') : NULL,
      'test_score_reporting'            => $this->input->post('test_score_reporting') ? $this->input->post('test_score_reporting') : NULL,
      'gre_code'                        => $this->input->post('gre_code') ? $this->input->post('gre_code') : NULL,
      'tofel_code'                      => $this->input->post('tofel_code') ? $this->input->post('tofel_code') : NULL,
      'gmat_code'                       => $this->input->post('gmat_code') ? $this->input->post('gmat_code') : NULL,
      'sat_code'                        => $this->input->post('sat_code') ? $this->input->post('sat_code') : NULL,
      'pre_requisites'                  => $this->input->post('pre_requisites') ? $this->input->post('pre_requisites') : NULL,
      'pre_requisites_link'             => $this->input->post('pre_requisites_link') ? $this->input->post('pre_requisites_link') : NULL,
      'supervisor_details_canada'       => $this->input->post('supervisor_details_canada') ? $this->input->post('supervisor_details_canada') : NULL,
      'pte_accepted'                    => $this->input->post('pte_accepted') ? $this->input->post('pte_accepted') : NULL,
      'duolingo_accepted'               => $this->input->post('duolingo_accepted') ? $this->input->post('duolingo_accepted') : NULL,
      'change_course'                   => $this->input->post('change_course') ? $this->input->post('change_course') : NULL,
      // 'intake_months'                => $this->input->post('intake_months') ? json_encode(explode(',', $this->input->post('intake_months'))) : NULL,
      // 'deadline_date_spring'         => $this->input->post('deadline_date_spring') ? $this->input->post('deadline_date_spring') : NULL,
      // 'deadline_date_fall'           => $this->input->post('deadline_date_fall') ? $this->input->post('deadline_date_fall') : NULL,
      // 'intake'                       => $this->input->post('intake') ? $this->input->post('intake') : NULL,
    );

    if ($this->course_model->addCourse($data)) {
      $this->session->set_flashdata('flash_message', "Success: You have successfully added course!");
      redirect('user/course/');
    }
    $this->session->set_flashdata('flash_message', "Failed: Some issue occured while adding course!");
    redirect('user/course/');
  }

  function edit()
  {
    $this->load->model('user/course_model');
    $courseId = $this->uri->segment('4');
    $filterLevel = $this->uri->segment('5') ? $this->uri->segment('5') : 0;
    $sop = array();
    $lor = array();
    $backlogs = array();
    $workPermit = array();

    if ($this->input->post('sop_needed') != '') {
      $sop = array(
        'sop_needed'        => $this->input->post('sop_needed'),
        'sop_no_of_words'   => $this->input->post('sop_no_of_words')
      );
    }

    if ($this->input->post('lor_needed') != '') {
      $lor = array(
        'lor_needed'    => $this->input->post('lor_needed'),
        'lor_required'  => $this->input->post('lor_required'),
        'lor_type'      => $this->input->post('lor_type')
      );
    }

    $eligibility_gre = array(
      'quant' => $this->input->post('eligibility_gre_quant'),
      'verbal' => $this->input->post('eligibility_gre_verbal')
    );

    $deadlines = array(
      'spring' => $this->input->post('spring_deadline'),
      'summer' => $this->input->post('summer_deadline'),
      'fall' => $this->input->post('fall_deadline'),
      'roll_over' => $this->input->post('roll_over')
    );

    if ($this->input->post('backlog_accepted') != '') {
      $backlogs = array(
        'backlog_accepted'        => $this->input->post('backlog_accepted'),
        'number_of_backlogs'   => $this->input->post('number_of_backlogs')
      );
    }

    if ($this->input->post('work_permit_needed') != '') {
      $workPermit = array(
        'work_permit_needed'        => $this->input->post('work_permit_needed'),
        'no_of_months'   => $this->input->post('no_of_months')
      );
    }

    foreach ($this->input->post('course_total_fee_currency') as $index => $feesCurrency) {
      $courseTotalFee[$index]['currency'] = $feesCurrency;
      $courseTotalFee[$index]['amount'] = $this->input->post('course_total_fee_amount')[$index];
    }

    $data = array(
      'name'                => $this->input->post('name'),
      'department_id'       => $this->input->post('department'),
      'category_id'         => $this->input->post('category_id') ? $this->input->post('category_id') : NULL,
      'degree_id'           => $this->input->post('degree'),
      'duration'            => $this->input->post('duration') ? $this->input->post('duration') : NULL,
      'deadline_link'       => $this->input->post('deadline_link'),
      'deadlines_detail'    => json_encode($deadlines),
      'stem'                => $this->input->post('stem'),

      'i_twenty_tuition_deposite'       => $this->input->post('i_twenty_tuition_deposite'),
      'avg_tuition_fee'                 => $this->input->post('avg_tuition_fee'),
      'program_link'                    => $this->input->post('program_link'),
      'admission_link'                  => $this->input->post('admission_link'),
      'course_total_fee'                => json_encode($courseTotalFee),
      'contact_title'                   => $this->input->post('contact_title'),
      'contact_name'                    => $this->input->post('contact_name'),
      'contact_email'                   => $this->input->post('contact_email'),
      'eligibility_gpa'                 => $this->input->post('eligibility_gpa') ? $this->input->post('eligibility_gpa') : NULL,
      'gpa_scale'                       => $this->input->post('gpa_scale') ? $this->input->post('gpa_scale') : NULL,
      'eligibility_gre'                 => json_encode($eligibility_gre),
      'eligibility_toefl'               => $this->input->post('eligibility_toefl') ? $this->input->post('eligibility_toefl') : NULL,
      'eligibility_ielts'               => $this->input->post('eligibility_ielts') ? $this->input->post('eligibility_ielts') : NULL,
      'expected_salary'                 => $this->input->post('expected_salary') ? $this->input->post('expected_salary') : NULL,
      'eligibility_work_experience'     => $this->input->post('eligibility_work_experience') ? $this->input->post('eligibility_work_experience') : NULL,
      'date_created'                    => date('Y-m-d H:i:s'),
      'date_updated'                    => date('Y-m-d H:i:s'),
      'status'                          => $this->input->post('status') ? $this->input->post('status') : 1,
      'added_by'                        => 1,
      'updated_by'                      => 1,
      'sop'                             => json_encode($sop),
      'lor'                             => json_encode($lor),
      'degree_substream'                => $this->input->post('degree_substream'),
      'no_of_intake'                    => $this->input->post('no_of_intake') ? $this->input->post('no_of_intake') : NULL,
      'intake_months'                   => $this->input->post('intake_months') ? json_encode($this->input->post('intake_months')) : NULL,
      'application_fee'                 => $this->input->post('application_fee') ? $this->input->post('application_fee') : NULL,
      'application_fee_amount'          => $this->input->post('application_fee_amount') ? $this->input->post('application_fee_amount') : NULL,
      'application_fee_waiver_imperial' => $this->input->post('application_fee_waiver_imperial') ? $this->input->post('application_fee_waiver_imperial') : NULL,
      'cost_of_living_year'             => $this->input->post('cost_of_living_year') ? $this->input->post('cost_of_living_year') : NULL,
      'test_required'                   => $this->input->post('test_required') ? $this->input->post('test_required') : NULL,
      'eligibility_gmat'                => $this->input->post('eligibility_gmat') ? $this->input->post('eligibility_gmat') : NULL,
      'eligibility_sat'                 => $this->input->post('eligibility_sat') ? $this->input->post('eligibility_sat') : NULL,

      'eligibility_english_proficiency'         => $this->input->post('eligibility_english_proficiency') ? $this->input->post('eligibility_english_proficiency') : NULL,
      'eligibility_pte'                         => $this->input->post('eligibility_pte') ? $this->input->post('eligibility_pte') : NULL,
      'eligibility_duolingo'                    => $this->input->post('eligibility_duolingo') ? $this->input->post('eligibility_duolingo') : NULL,
      'resume_required'                         => $this->input->post('resume_required') ? $this->input->post('resume_required') : NULL,
      'admission_requirements'                  => $this->input->post('admission_requirements'),
      'backlogs'                                => json_encode($backlogs),
      'drop_years'                              => $this->input->post('drop_years') ? $this->input->post('drop_years') : NULL,
      'scholarship_available'                   => $this->input->post('scholarship_available') ? $this->input->post('scholarship_available') : NULL,
      'with_15_years_education'                 => $this->input->post('with_15_years_education') ? $this->input->post('with_15_years_education') : NULL,
      'wes_requirement'                         => $this->input->post('wes_requirement') ? $this->input->post('wes_requirement') : NULL,
      'wes_type'                                => $this->input->post('wes_type') ? $this->input->post('wes_type') : NULL,
      'work_while_studying'                     => $this->input->post('work_while_studying') ? $this->input->post('work_while_studying') : NULL,
      'part_time_work_details'                  => $this->input->post('part_time_work_details') ? $this->input->post('part_time_work_details') : NULL,
      'cooperate_internship_participation'      => $this->input->post('cooperate_internship_participation') ? $this->input->post('cooperate_internship_participation') : NULL,
      
      'work_permit'                 => json_encode($workPermit),
      'start_months'                => $this->input->post('start_months') ? json_encode(explode(',', $this->input->post('start_months'))) : NULL,
      'decision_months'             => $this->input->post('decision_months') ? json_encode(explode(',', $this->input->post('decision_months'))) : NULL,
      'conditional_admit'           => $this->input->post('conditional_admit') ? $this->input->post('conditional_admit') : NULL,
      'description'                 => $this->input->post('description'),
      'tuition_waivers'             => $this->input->post('tuition_waivers') ? $this->input->post('tuition_waivers') : NULL,
      'research_assistantships'     => $this->input->post('research_assistantships') ? $this->input->post('research_assistantships') : NULL,
      'teaching_assistantships'     => $this->input->post('teaching_assistantships') ? $this->input->post('teaching_assistantships') : NULL,
      'graduate_assistantships'     => $this->input->post('graduate_assistantships') ? $this->input->post('graduate_assistantships') : NULL,
      'others_link'                 => $this->input->post('others_link') ? $this->input->post('others_link') : NULL,
      'transcripts'                 => $this->input->post('transcripts') ? $this->input->post('transcripts') : NULL,
      'financial_documentation'     => $this->input->post('financial_documentation') ? $this->input->post('financial_documentation') : NULL,
      'test_score_waivers'          => $this->input->post('test_score_waivers') ? $this->input->post('test_score_waivers') : NULL,
      'test_score_reporting'        => $this->input->post('test_score_reporting') ? $this->input->post('test_score_reporting') : NULL,
      'gre_code'                    => $this->input->post('gre_code') ? $this->input->post('gre_code') : NULL,
      'tofel_code'                  => $this->input->post('tofel_code') ? $this->input->post('tofel_code') : NULL,
      'gmat_code'                   => $this->input->post('gmat_code') ? $this->input->post('gmat_code') : NULL,
      'sat_code'                    => $this->input->post('sat_code') ? $this->input->post('sat_code') : NULL,
      'pre_requisites'              => $this->input->post('pre_requisites') ? $this->input->post('pre_requisites') : NULL,
      'pre_requisites_link'         => $this->input->post('pre_requisites_link') ? $this->input->post('pre_requisites_link') : NULL,
      'supervisor_details_canada'   => $this->input->post('supervisor_details_canada') ? $this->input->post('supervisor_details_canada') : NULL,
      'pte_accepted'                => $this->input->post('pte_accepted') ? $this->input->post('pte_accepted') : NULL,
      'duolingo_accepted'           => $this->input->post('duolingo_accepted') ? $this->input->post('duolingo_accepted') : NULL,
      'change_course'               => $this->input->post('change_course') ? $this->input->post('change_course') : NULL
      // 'intake_months'            => $this->input->post('intake_months') ? json_encode(explode(',', $this->input->post('intake_months'))) : NULL,
      // 'deadline_date_spring' 		=> $this->input->post('deadline_date_spring') ? $this->input->post('deadline_date_spring') : NULL,
      // 'deadline'                        => $this->input->post('deadline'),
      //  'deadline_date_fall' 		  => $this->input->post('deadline_date_fall') ? $this->input->post('deadline_date_fall') : NULL,
      //  'intake' 		              => $this->input->post('intake') ? $this->input->post('intake') : NULL,
      //  'total_fees' 		          => $this->input->post('total_fees') ? $this->input->post('total_fees') : NULL,
    );    

    $this->course_model->editCourse($courseId, $data);
    $this->session->set_flashdata('flash_message', "Success: You have modified course!");
    redirect("user/course/form/$courseId/$filterLevel");
  }


  function delete()
  {

    $this->load->helper(array('form', 'url'));
    $this->load->model('user/user_model');

    if ($this->user_model->deleteUser($this->uri->segment('4')) == 'success') {

      $this->session->set_flashdata('flash_message', "Success: You have deleted University!");
      redirect('user/course');
    }
  }


  function multidelete()
  {

    $this->load->helper(array('form', 'url'));

    $this->load->model('user/user_model');

    $data = ["status" => 0];
    $array = $this->input->post('chk');
    foreach ($array as $id):

      $this->course_model->editCourse($id, $data);

    endforeach;
    $this->session->set_flashdata('flash_message', "You have deleted User!");
    redirect('user/course');
  }

  function selectedoptions()
  {

    $this->load->helper(array('form', 'url'));

    $this->load->model('user/user_model');
    $this->load->model('user/course_model');
    $this->load->model('user/department_model');

    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    $array = $this->input->post('chk');
    $optionsId = $this->input->post('options_id') ? $this->input->post('options_id') : 0;
    $assignId = $this->input->post('assign_id') != '' ? $this->input->post('assign_id') : 0;
    $selectedDepartmentId = $this->input->post('selected_department_id');

    if ($optionsId == "ASSIGN" && $assignId == 0) {
      $this->session->set_flashdata('flash_message', "For Assign You Have To Select Department !");
      redirect('user/course/coursebydepartmentid?department_id=' . $selectedDepartmentId);
    }

    if ($optionsId == "DELETEDEPARTMENT") {
      $data = ["status" => 0];
      $this->department_model->editDepartment($selectedDepartmentId, $data);
      $this->session->set_flashdata('flash_message', "Department Deleted!");
    } else if ($optionsId == "ASSIGN") {
      $data = ["department_id" => $assignId];

      foreach ($array as $id) {
        $this->course_model->editCourse($id, $data);
      }

      $this->session->set_flashdata('flash_message', "You have Assigned Course!");
    } else {
      $data = ["status" => 0];

      foreach ($array as $id) {
        $this->course_model->editCourse($id, $data);
      }

      $this->session->set_flashdata('flash_message', "You have Deleted Course!");
    }

    redirect('user/course/coursebydepartmentid?department_id=' . $selectedDepartmentId);
  }

  function trash()
  {
    $this->load->helper('cookie_helper');
    $this->load->model('user/user_model');
    $userdata = $this->session->userdata('user');
    if (empty($userdata)) {
      redirect('common/login');
    }

    $cond_user1 = array('user_master.status ' => '5');
    $cond_user2 = array();
    $this->outputData['users'] = $this->user_model->getUser($cond_user1, $cond_user2);
    $this->outputData['view'] = 'list';

    $this->render_page('templates/user/trash_admins', $this->outputData);
  }

  function deletetrash()
  {

    $this->load->helper(array('form', 'url'));
    $this->load->model('user/user_model');

    if ($this->user_model->deleteTrash($this->uri->segment('4')) == 'success') {

      $this->session->set_flashdata('flash_message', "Success: You have deleted User!");
      redirect('user/university/trash');
    }
  }

  function multidelete_trash()
  {

    $this->load->helper(array('form', 'url'));

    $this->load->model('user/user_model');

    $array = $this->input->post('chk');
    foreach ($array as $id):

      $this->user_model->deleteTrash($id);

    endforeach;
    $this->session->set_flashdata('flash_message', "You have deleted User!");
    redirect('user/university/trash');
  }
} //End  Home Class
