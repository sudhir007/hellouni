<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com



 <Reverse bidding system>

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or

    contact us from http://www.freelancephpscript.com/contact

 */
 require_once APPPATH . 'libraries/Mail/sMail.php';

class Admin extends MY_Controller {



	//Global variable

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;



    /**

	 * Constructor

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }


	function index()
	{
	     $this->load->helper('cookie_helper');
         $this->load->model('user/user_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }

		$cond_user1 = array('user_master.status !=' => '5');
		$cond_user2 = array('user_master.type ' => '2');
        $this->outputData['users'] = $this->user_model->getUser($cond_user1,$cond_user2);
		$this->outputData['view'] = 'list';

		/*echo '<pre>';
		print_r($this->outputData['users']);
		echo '</pre>';
		exit();*/



		//$this->load->view('templates/user/body',$this->outputData);
        $this->render_page('templates/user/admins',$this->outputData);

    }

	 function form()
	 {
	     $this->load->helper('cookie_helper');
         $this->load->model('user/user_model');
		 //$this->load->model('location/country_model');
		 //$this->load->model('location/city_model');
		 //$this->load->model('location/state_model');


		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }

		 if($this->uri->segment('4')){
		 $conditions = array('user_master.id' => $this->uri->segment('4'));
		 $user_details = $this->user_model->getUserByid($conditions);

		//echo '<pre>'; print_r($user_details); echo '</pre>'; exit;

		$this->outputData['id'] 				= $user_details->id;
		$this->outputData['name'] 				= $user_details->name;
		$this->outputData['email'] 				= $user_details->email;
		$this->outputData['status'] 			= $user_details->status;
		$this->outputData['notification'] 		= $user_details->notification;
		/*$this->outputData['create'] 			= $user_details->create;
		$this->outputData['view'] 				= $user_details->view;
		$this->outputData['edit'] 				= $user_details->edit;
		$this->outputData['del'] 				= $user_details->del;*/
        $this->outputData['username'] 				= $user_details->username;
        $this->outputData['edit'] = true;

		}

        $this->render_page('templates/user/admin_form',$this->outputData);

     }

	 function create()
	 {
         if(!$this->input->post('adminname') || !$this->input->post('username') || !$this->input->post('email') || !$this->input->post('password'))
         {
             $this->session->set_flashdata('flash_message', "Error: Please fill all mandatory fields!");
    	     redirect('user/admin/form');
         }
	   //die('+++++');
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('user/user_model');
       /*$this->load->model('website-element/package_model');
       $this->load->model('administrator/common_model');
	   $this->form_validation->set_rules('name', 'Name', 'required|trim|xss_clean');
	   $this->form_validation->set_rules('user_name', 'User Name', 'required|trim|xss_clean');
	   $this->form_validation->set_rules('user_role', 'User Role', 'required|trim|xss_clean');
	   $this->form_validation->set_rules('country', 'Country', 'required|trim|xss_clean');

	   $this->form_validation->set_rules('city', 'City', 'required|trim|xss_clean');
	   $this->form_validation->set_rules('address', 'Address', 'required|trim|xss_clean');
	   $this->form_validation->set_rules('postcode', 'Postcode', 'required|trim|xss_clean');
	   $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean');*/

	 /* if ($this->form_validation->run() == FALSE)
		{
			$this->render_page('templates/user/user_form');
		}
		else
		{ */


		 	$data = array(
        	'name' 			=>  $this->input->post('adminname'),
            'username' 			=>  $this->input->post('username'),
        	'email' 		=>  $this->input->post('email'),
        	'password' 		=>  md5($this->input->post('password')),
			'type' 			=>  '2',
			'createdate' 	=>  date('Y-m-d'),
			'status' 		=>	$this->input->post('status'),
			'view' 			=>	$this->input->post('view'),
			'create' 		=>	$this->input->post('creation'),
			'edit' 			=>	$this->input->post('edit'),
			'del' 			=>	$this->input->post('deletion'),
			'notification' 	=>	$this->input->post('notification'),


			);


			/*echo '<pre>';
			print_r($data);
			echo '</pre>';
			exit(); */

	     if($this->user_model->insertUser($data)=='success'){

		 if($this->input->post('notification')==1){

             $userEmail = $this->input->post('email');
             $name = $this->input->post('adminname');
             $username = $this->input->post('username');
             $password = $this->input->post('password');

             $mailSubject = "Notification on creation of your Admin Account Under HelloUni";
             $mailTemplate = "Hi $name,

                                 Your Admin Account has been sucessfully created by Super Admin under HelloUni. Your login credential is as follows

                                 Username: $username
                                 Password: $password

                                 Kindly login to your account and explore the options.

                                 <a href='https://www.hellouni.org/admin'>https://www.hellouni.org/admin</a>

                                 <b>Thanks and Regards,
                                 HelloUni Coordinator
                                 +91 81049 09690</b>

                                 <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

             $ccMailList = '';
             $mailAttachments = '';

             $sendMail = sMail($userEmail, $userName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

			}
		 $this->session->set_flashdata('flash_message', "Success: You have saved User!");
	     redirect('user/admin/');
	     }
	  // }
	}


	 function edit()
	 {
         if(!$this->input->post('adminname') || !$this->input->post('email'))
         {
             $this->session->set_flashdata('flash_message', "Error: Please fill all mandatory fields!");
    	     redirect('user/admin/form/' . $this->uri->segment('4'));
         }
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('user/user_model');

	  /* $this->form_validation->set_rules('name', 'Name', 'required|trim|xss_clean');

	   $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean');

	   if ($this->form_validation->run() == FALSE)
		{

			 $this->render_page('templates/user/admin_form');
		}
		else
		{*/
			$data = array(
        	'id'            =>  $this->uri->segment('4'),
			'name' 			=>  $this->input->post('adminname'),
        	'email' 		=>  $this->input->post('email'),
			'type' 			=>  '2',
			'status' 		=>	$this->input->post('status'),
			'view' 			=>	$this->input->post('view'),
			'create' 		=>	$this->input->post('creation'),
			'edit' 			=>	$this->input->post('edit'),
			'del' 			=>	$this->input->post('deletion'),
			'notification' 	=>	$this->input->post('notification')

			);


		 if($this->user_model->editAdminUser($data)=='success'){

		  if($this->input->post('notification')==1){

              $userEmail = $this->input->post('email');
              $name = $this->input->post('adminname');
              $username = $this->input->post('username');

              $mailSubject = "Notification on modification of your Admin Account Under HelloUni";
              $mailTemplate = "Hi $name,

                                  Your Admin Account has been sucessfully modified by Super Admin under HelloUni. Your login credential is as follows

                                  Kindly login to your account and explore the options.

                                  <a href='https://www.hellouni.org/admin'>https://www.hellouni.org/admin</a>

                                  <b>Thanks and Regards,
                                  HelloUni Coordinator
                                  +91 81049 09690</b>

                                  <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

              $ccMailList = '';
              $mailAttachments = '';

              $sendMail = sMail($userEmail, $userName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

			}

	     $this->session->set_flashdata('flash_message', "Success: You have modified User!");
	      redirect('user/admin');
	     }

	   //}
	}


	function delete()
	 {

	   $this->load->helper(array('form', 'url'));
	   $this->load->model('user/user_model');

		if($this->user_model->deleteUser($this->uri->segment('4'))=='success'){

	         $this->session->set_flashdata('flash_message', "Success: You have deleted User!");
	         redirect('user/admin');
	    }
	}


	function multidelete(){

	    $this->load->helper(array('form', 'url'));

		$this->load->model('user/user_model');

		$array = $this->input->post('chk');
		foreach($array as $id):

		$this->user_model->deleteUser($id);

		endforeach;
		$this->session->set_flashdata('flash_message', "You have deleted User!");
		 redirect('user/admin');


	}

	function trash()
	{
	     $this->load->helper('cookie_helper');
         $this->load->model('user/user_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }

		$cond_user1 = array('user_master.status ' => '5');
		$cond_user2 = array();
        $this->outputData['users'] = $this->user_model->getUser($cond_user1,$cond_user2);
		$this->outputData['view'] = 'list';

        $this->render_page('templates/user/trash_admins',$this->outputData);

    }

	function deletetrash()
	 {

	   $this->load->helper(array('form', 'url'));
	   $this->load->model('user/user_model');

		if($this->user_model->deleteTrash($this->uri->segment('4'))=='success'){

	         $this->session->set_flashdata('flash_message', "Success: You have deleted User!");
	         redirect('user/admin/trash');
	    }
	}

	function multidelete_trash(){

	    $this->load->helper(array('form', 'url'));

		$this->load->model('user/user_model');

		$array = $this->input->post('chk');
		foreach($array as $id):

		$this->user_model->deleteTrash($id);

		endforeach;
		$this->session->set_flashdata('flash_message', "You have deleted User!");
		 redirect('user/admin/trash');


	}



}//End  Home Class

?>
