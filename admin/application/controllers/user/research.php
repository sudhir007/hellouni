<?php
// ini_set('display_errors', '1');
// ini_set('display_startup_errors', '1');
// error_reporting(E_ALL);
class Research extends MY_Controller
{ 
    //Global variable
    public $outputData;    //Holds the output data for each view
    public $loggedInUser;

    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
        $this->load->library('form_validation');
    }

    function index()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/university_model');
        $userdata = $this->session->userdata('user');
        if (empty($userdata)) {
            redirect('common/login');
        }

        if ($userdata['university_id']) {
            $this->outputData['researches'] = $this->university_model->getAllResearchesByUniversity($userdata['university_id']);
        } else {
            $this->outputData['researches'] = $this->university_model->getAllResearches();
        }

        $user_id = $userdata['id'];
        $this->render_page('templates/user/researches', $this->outputData);
    }

    function ajax_manage_page()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/university_model');
        $this->load->model('filter_model');

        $userdata = $this->session->userdata('user');
        if (empty($userdata)) {
            redirect('common/login');
        }

        if ($userdata['university_id']) {
            $list = $this->university_model->get_datatables_getAllResearchesByUniversity($userdata['university_id']);
        } else {
            $list = $this->university_model->get_datatables_getAllResearches();
        }


        // echo "<pre>"; print_r($list); exit;
        $data = array();
        $no = $_POST['start'];

        foreach ($list as $person) {

            $btn = anchor(base_url('/user/research/form/' . $person->id), '<span title="Edit" class="edit"><i class="fa fa-edit"></i></span>');
            $btn .= '&nbsp;|&nbsp;' . '<a href="javascript:void();" title="Delete" class="del btn btn-danger btn-circle btn-sm" id="<?php echo $person->id;?>"><i class="fa fa-trash-o"></i></a>';

            //echo "<pre>"; print_r($person); exit;
            $no++;
            $row = array();
            $row[] = '<input type="checkbox" class="multichk" name="chk[]" value="<?php echo $person->id;?>"/>' . $no;
            $row[] = $person->university_name;
            $row[] = $person->college_name;
            $row[] = $person->department_name;
            $row[] = $person->topic;
            $row[] = $person->research_url;
            $row[] = $person->funding;
            $row[] = $btn;


            $data[] = $row;
        }
        //echo "<pre>"; print_r($data); exit;
        if ($userdata['university_id']) {
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->university_model->count_all_getAllResearchesByUniversity($userdata['university_id']),
                "recordsFiltered" => $this->university_model->count_filtered_getAllResearchesByUniversity($userdata['university_id']),
                "data" => $data,
            );
        } else {
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->university_model->count_all_getAllResearches(),
                "recordsFiltered" => $this->university_model->count_filtered_getAllResearches(),
                "data" => $data,
            );
        }

        //echo "<pre>"; print_r($output); exit;
        //output to json format
        echo json_encode($output);
    }

    public function getCollege()
    {
        $this->load->model('user/college_model');
        
        $selectedUniversityId = isset($_POST['university_id']) && $_POST['university_id'] != "" ? $_POST['university_id'] : 0;
        $allColleges = $this->college_model->getAllCollegesByUniversity($selectedUniversityId);
        $response['college_list'] = $allColleges;
        header('Content-Type: application/json');
        echo json_encode($response);
        
    }

    public function getDepartments()
    {
        $this->load->model('user/department_model');
        
        $selectedCollegeId = isset($_POST['college']) && $_POST['college'] != "" ? $_POST['college'] : 0;
        $allDepartments = $this->department_model->getAllDepartmentsByCollege($selectedCollegeId);
        $response['department_list'] = $allDepartments;
        header('Content-Type: application/json');
        echo json_encode($response);
        
    }

    function form()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/university_model');
        $this->load->model('user/college_model');
        $this->load->model('user/department_model');
        $userdata = $this->session->userdata('user');
        if (empty($userdata)) {
            redirect('common/login');
        }
        $allUniversities = $this->university_model->getAllUniversities();
        // if ($userdata['university_id']) {
        //     $allColleges = $this->college_model->getAllCollegesByUniversity($userdata['university_id']);
        //     $allDepartments = $this->department_model->getAllDepartmentsByUniversity($userdata['university_id']);
        // } else {
        //     $allColleges = $this->college_model->getAllColleges();
        //     $allDepartments = $this->department_model->getAllDepartments();
        // }

        if ($this->uri->segment('4')) {
            $researchData = $this->university_model->getResearchById($this->uri->segment('4'));
            $this->outputData['university'] = $researchData->university_id;
            $this->outputData['college'] = $researchData->college_id;
            $this->outputData['department'] = $researchData->department_id;
            $this->outputData['topic'] = $researchData->topic;
            $this->outputData['description'] = $researchData->description;
            $this->outputData['research_url'] = $researchData->research_url;
            $this->outputData['faculty_url'] = $researchData->faculty_url;
            $this->outputData['funding'] = $researchData->funding;
            $this->outputData['id'] = $this->uri->segment('4');

            // Fetch preselected colleges and departments
            $this->outputData['colleges'] = $this->college_model->getAllCollegesByUniversity($researchData->university_id);
            $this->outputData['departments'] = $this->department_model->getAllDepartmentsByCollege($researchData->college_id);
        }

        $this->outputData['university_id'] = $userdata['university_id'];
        $this->outputData['universities'] = $allUniversities;
        // $this->outputData['colleges'] = $allColleges;
        // $this->outputData['departments'] = $allDepartments;
        $this->render_page('templates/user/research_form', $this->outputData);
    }

    function create()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/university_model');
        $userdata = $this->session->userdata('user');
        if (empty($userdata)) {
            redirect('common/login');
        }

        $data['department_id'] = $this->input->post('department');
        $data['topic'] = $this->input->post('topic');
        $data['description'] = $this->input->post('description');
        $data['research_url'] = $this->input->post('research_url');
        $data['faculty_url'] = $this->input->post('faculty_url');
        $data['funding'] = $this->input->post('funding') ? $this->input->post('funding') : NULL;

        $this->university_model->addResearch($data);
        redirect('/user/research');
    }

    function edit()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/university_model');
        $userdata = $this->session->userdata('user');
        if (empty($userdata)) {
            redirect('common/login');
        }

        $data['department_id'] = $this->input->post('department');
        $data['topic'] = $this->input->post('topic');
        $data['description'] = $this->input->post('description');
        $data['research_url'] = $this->input->post('research_url');
        $data['faculty_url'] = $this->input->post('faculty_url');
        $data['funding'] = $this->input->post('funding') ? $this->input->post('funding') : NULL;

        $research_id = $this->uri->segment('4');

        $this->university_model->editResearch($research_id, $data);
        redirect('/user/research');
    }
}
