<?php
class Alumnus extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

    function index()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/user_model');
        $this->load->model('user/university_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata)){  redirect('common/login'); }

        if($userdata['university_id'])
        {
            $this->outputData['users'] = $this->university_model->getAlumnusByUniversity($userdata['university_id']);
        }
        else
        {
            $this->outputData['users'] = $this->university_model->getAllAlumnus();
        }

        $this->outputData['view'] = 'list';

        $this->render_page('templates/user/alumnus',$this->outputData);
    }

    function ajax_manage_page(){
        //print_r('m here'); exit;

        $this->load->helper('cookie_helper');
        $this->load->model('user/user_model');
        $this->load->model('user/university_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata)){  redirect('common/login'); }

        if($userdata['university_id'])
        {
            //echo "<pre>";print_r('m hre if');exit; 
            $list = $this->university_model->get_datatables_AlumnusByUniversity($userdata['university_id']);
           // $this->outputData['users'] = $this->university_model->getAlumnusByUniversity($userdata['university_id']);
        }
        else
        {
            $list = $this->university_model->get_datatables_AllAlumnus();
            //echo "<pre>";print_r($list);exit; 
            //$this->outputData['users'] = $this->university_model->getAllAlumnus();
        }

       // $list = $this->person->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $person) {

            $btn = anchor(base_url('/user/alumnus/form/'.$person->id),'<span title="Modify" class="edit"><i class="fa fa-edit"></i></span>');
            $btn .='&nbsp;|&nbsp;'.'<a href="javascript:void();" title="Delete" class="del btn btn-danger btn-circle btn-sm" id="<?php echo $person->user_id;?>"><i class="fa fa-trash-o"></i></a>';

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $person->name;
            $row[] = $person->email;
            $row[] = $person->university_name;
            $row[] = $person->createdate;
            $row[] = $person->status_name;

            //add html for action
            $row[] = $btn;
        
            $data[] = $row;
        }

        if($userdata['university_id'])
        {
            $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->university_model->count_all(),
                        "recordsFiltered" => $this->university_model->count_filtered(),
                        "data" => $data,
                );
        }else{
            $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->university_model->count_all_AllAlumnus(),
                        "recordsFiltered" => $this->university_model->count_filtered_AllAlumnus(),
                        "data" => $data,
                );
        }

        
        //output to json format
        echo json_encode($output);
    }

    function form()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/user_model');
        $this->load->model('user/university_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata)){  redirect('common/login'); }
        $this->outputData = array();
        if($userdata['type'] == 1)
        {
            $this->outputData['universities'] = $this->university_model->getAllUniversities();
        }
        if($this->uri->segment('4')){
            $alumnuId = $this->uri->segment('4');
            $alumnuData = $this->university_model->getAlumnuById($alumnuId);
            $this->outputData['id'] = $alumnuData->id;
            $this->outputData['user_id'] = $alumnuData->user_id;
            $this->outputData['name'] = $alumnuData->name;
            $this->outputData['username'] = $alumnuData->username;
            $this->outputData['email'] = $alumnuData->email;
            $this->outputData['university'] = $alumnuData->university_id;
            $this->outputData['status'] = $alumnuData->status;
            $this->outputData['edit'] = true;
        }
        $this->render_page('templates/user/alumnu_form', $this->outputData);
    }

    function create()
    {
        if(!$this->input->post('name') || !$this->input->post('username') || !$this->input->post('email') || !$this->input->post('password'))
        {
            $this->session->set_flashdata('flash_message', "Error: Please fill all mandatory fields!");
            redirect('user/alumnus/form');
        }

        $this->load->helper('cookie_helper');
        $this->load->model('user/user_model');
        $this->load->model('user/university_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata)){  redirect('common/login'); }
        if($userdata['type'] == 3)
        {
            $universityId = $userdata['university_id'];
        }
        else
        {
            $universityId = $this->input->post('university_id');
        }
        $data = array(
            'university_id' => $universityId,
            'name' => $this->input->post('name'),
            'username' => $this->input->post('username'),
            'email' => $this->input->post('email'),
            'password' => md5($this->input->post('password')),
            'status' => $this->input->post('status'),
            'type' => 6
        );

        $this->university_model->addAlumnu($data);

        $this->session->set_flashdata('flash_message', "Success: You have successfully added alumnu!");
        redirect('user/alumnus/');
    }

    function edit()
    {
        if(!$this->input->post('name') || !$this->input->post('email'))
        {
            $this->session->set_flashdata('flash_message', "Error: Please fill all mandatory fields!");
            redirect('user/alumnus/form/' . $this->uri->segment('4'));
        }
        $this->load->helper('cookie_helper');
        $this->load->model('user/course_model');
        $this->load->model('user/university_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata)){  redirect('common/login'); }
        if($userdata['type'] == 3)
        {
            $universityId = $userdata['university_id'];
        }
        else
        {
            $universityId = $this->input->post('university_id');
        }

        $data = array(
            'user_id' => $this->input->post('user_id'),
            'alumnu_id' => $this->uri->segment('4'),
            'university_id' => $universityId,
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'status' => $this->input->post('status'),
        );

        /*if($this->input->post('password'))
        {
            $data['password'] = md5($this->input->post('password'));
        }*/

        $this->university_model->editAlumnu($data);
        $this->session->set_flashdata('flash_message', "Success: You have successfully updated alumnu!");
        redirect('user/alumnus/');
    }

    function profile()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/course_model');
        $this->load->model('user/university_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata)){  redirect('common/login'); }

        $user_id = $userdata['id'];
        $alumnuData = $this->university_model->getAlumnuByUserId($user_id);

        $universityId = $alumnuData->university_id;
        $all_courses = $this->course_model->getAllCoursesByUniversity($universityId);

        $this->outputData['id'] = $alumnuData->id;
        $this->outputData['user_id'] = $user_id;
        $this->outputData['all_courses'] = $all_courses;
        $this->outputData['university_id'] = $alumnuData->university_id;
        $this->outputData['course_id'] = $alumnuData->course_id;
        $this->outputData['joining_date'] = $alumnuData->joining_date;
        $this->outputData['passout_date'] = $alumnuData->passout_date;
        $this->outputData['my_story'] = $alumnuData->my_story;
        $this->outputData['my_college'] = $alumnuData->my_college;
        $this->outputData['current_placement'] = $alumnuData->current_placement;
        $this->outputData['current_job_profile'] = $alumnuData->current_job_profile;
        $this->outputData['current_salary'] = $alumnuData->current_salary;
        $this->outputData['my_insights'] = $alumnuData->my_insights;
        $this->outputData['connect_with_me'] = $alumnuData->connect_with_me;
        $this->outputData['my_video'] = $alumnuData->my_video;
        //$this->outputData['images'] = json_decode($alumnuData->images);
        $this->render_page('templates/user/alumnu_profile', $this->outputData);
    }

    function update()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/user_model');
        $this->load->model('user/university_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata)){  redirect('common/login'); }

        $data['course_id'] = $this->input->post('course_id') ? $this->input->post('course_id') : NULL;
        $data['joining_date'] = $this->input->post('joining_date') ? $this->input->post('joining_date') : NULL;
        $data['passout_date'] = $this->input->post('passout_date') ? $this->input->post('passout_date') : NULL;
        $data['my_insights'] = $this->input->post('my_insights') ? $this->input->post('my_insights') : NULL;
        $data['connect_with_me'] = $this->input->post('connect_with_me') == 'on' ? 1 : 0;
        $data['my_story'] = $this->input->post('my_story') ? $this->input->post('my_story') : NULL;
        $data['my_college'] = $this->input->post('my_college') ? $this->input->post('my_college') : NULL;
        $data['current_placement'] = $this->input->post('current_placement') ? $this->input->post('current_placement') : NULL;
        $data['current_job_profile'] = $this->input->post('current_job_profile') ? $this->input->post('current_job_profile') : NULL;
        $data['current_salary'] = $this->input->post('current_salary') ? $this->input->post('current_salary') : NULL;

        if($_FILES["imageToUpload"]["name"])
        {
            $random_digit = round(microtime(true));
            $temp = explode(".", $_FILES["imageToUpload"]["name"]);
            if(end($temp)!='gif' && end($temp)!='jpg' && end($temp)!='JPG' && end($temp)!='jpeg' && end($temp)!='JPEG')
            {
                $this->session->set_flashdata('flash_message', 'File should be in jpg/png/gif formate !');
                redirect('user/alumnus/profile');
            }

            if ($_FILES["imageToUpload"]["size"] > 500000)
            {
                $this->session->set_flashdata('flash_message', 'File should be in less than 5 MB in size!');
                redirect('user/alumnus/profile');
            }

            $target_image = APPPATH . "../upload/" . $userdata['name'] . $random_digit . '_image.' . end($temp);

            $file_name =  $userdata['name'] . $random_digit . '_image.' . end($temp);
            $data['images'] = json_encode(array($file_name));

            copy($_FILES["imageToUpload"]["tmp_name"], $target_image);
        }
        else if(!$this->input->post('image'))
        {
            $target_image = '';
        }
        else
        {
            $target_image = $this->input->post('image');
        }

        if($_FILES["videoToUpload"]["name"])
        {
            $random_digit = round(microtime(true));
            $temp = explode(".", $_FILES["videoToUpload"]["name"]);

            $target_video = APPPATH . "../upload/" . $userdata['name'] . $random_digit . '_video.' . end($temp);

            $file_name = $userdata['name'] . $random_digit . '_video.' . end($temp);
            $data['my_video'] = $file_name;

            copy($_FILES["videoToUpload"]["tmp_name"], $target_video);
        }
        else if(!$this->input->post('video'))
        {
            $target_video = '';
        }
        else
        {
            $target_video = $this->input->post('video');
        }

        $alumnuId = $this->uri->segment('4');

        $this->university_model->updateAlumnu($alumnuId, $data);
        $this->session->set_flashdata('flash_message', "Success: You have successfully updated alumnu!");
        redirect('user/alumnus/profile');
    }

    function delete()
	 {

	   $this->load->helper(array('form', 'url'));
	   $this->load->model('user/user_model');

		if($this->user_model->deleteUser($this->uri->segment('4'))=='success'){

	         $this->session->set_flashdata('flash_message', "Success: You have deleted University!");
	         redirect('user/alumnus');
	    }
	}
}
?>
