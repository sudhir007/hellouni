<?php
require APPPATH . 'libraries/php_excel/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Upload extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('user/country_model', 'user/user_model', 'user/campus_model', 'user/college_model', 'user/department_model', 'user/course_model', 'user/university_model'));
    }

    function university()
    {
        $this->render_page('templates/user/upload_universities');
    }

    function upload_universities()
    {
        $config['upload_path']   = './upload/';
        $config['allowed_types'] = 'gif|jpg|png|xlsx|csv|xls|octet-stream|xml';
        /*$config['max_size']      = 100;
        $config['max_width']     = 1024;
        $config['max_height']    = 768;*/
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('userfile'))
        {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('templates/user/upload_universities', $error);
        }
        else
        {
            $fileInfo = $this->upload->data();
            $inputFileName = $fileInfo['full_path'];
            //$inputFileName = './upload/hellouni_university.xlsx';
            /**  Identify the type of $inputFileName  **/
            $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
            /**  Advise the Reader that we only want to load cell data  **/
            //$reader->setReadDataOnly(true);
            /**  Load $inputFileName to a Spreadsheet Object  **/
            $spreadsheet = $reader->load($inputFileName);
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

            $countryNames = array();
            $universityUserNames = array();

            $universityDetail = array();
            $universityIndex = 0;

            $campusDetail = array();
            $collegeDetail = array();
            $departmentDetail = array();
            $courseDetail = array();

            $allCollegeCategories = $this->college_model->getAllCategories();
            /*foreach($allCollegeCategories as $index => $collegeCategory)
            {
                $collegeCategories[$collegeCategory->display_name] = $collegeCategory->id;
            }*/

            $allCourseCategories = $this->course_model->getAllCategories();
            /*foreach($allCourseCategories as $index => $courseCategory)
            {
                $courseCategories[$courseCategory->display_name] = $courseCategory->id;
            }*/

            $allDegrees = $this->course_model->getAllDegrees();
            /*foreach($allDegrees as $index => $degree)
            {
                $degrees[$degree->name] = $degree->id;
            }*/

            // The following code is for testing matching words
            /*$allCourseSubstreams = array();
            foreach($sheetData as $index => $data)
            {
                if($index == 1)
                {
                    continue;
                }
                foreach($data as $dataIndex => $dataValue)
                {
                    if($dataIndex == 'AH' && !in_array($dataValue, $allCourseSubstreams))
                    {
                        $allCourseSubstreams[] = $dataValue;
                    }
                }
            }
            echo '<pre>';
            $matchedArray = array();
            $unmatchedArray = array();
            foreach($allCourseSubstreams as $index => $value)
            {
                $matched = 0;
                foreach($allCourseCategories as $catIndex => $catValue)
                {
                    if(strpos($value, $catValue->display_name) !== FALSE)
                    {
                        $matched = 1;
                        $matchedArray[] = $value;
                    }
                }
                if(!$matched)
                {
                    $unmatchedArray[] = $value;
                }
            }
            print_r($matchedArray);
            print_r($unmatchedArray);
            exit;*/

            foreach($sheetData as $index => $data)
            {
                if($index == 1)
                {
                    continue;
                }

                if($data['A'] && !in_array(trim($data['A']), $countryNames))
                {
                    $countryNames[] = trim($data['A']);
                }

                if($data['B'] && !in_array($data['B'], $universityUserNames))
                {
                    $universityUserNames[] = $data['B'];
                    $universityDetail[$universityIndex]['country'] = trim($data['A']);
                    $universityDetail[$universityIndex]['username'] = $data['B'];
                    $universityDetail[$universityIndex]['email'] = $data['C'];
                    $universityDetail[$universityIndex]['password'] = $data['D'];
                    $universityDetail[$universityIndex]['name'] = $data['E'];
                    $universityDetail[$universityIndex]['website'] = $data['F'];
                    $universityDetail[$universityIndex]['description'] = $data['G'];
                    $universityDetail[$universityIndex]['establishment_year'] = $data['H'];
                    $universityDetail[$universityIndex]['type'] = $data['I'];
                    $universityDetail[$universityIndex]['carnegie_accreditation'] = $data['J'];
                    $universityDetail[$universityIndex]['total_student_ug'] = $data['K'];
                    $universityDetail[$universityIndex]['total_student_pg'] = $data['L'];
                    $universityDetail[$universityIndex]['total_student'] = $data['M'];
                    $universityDetail[$universityIndex]['total_student_international'] = $data['N'];
                    $universityDetail[$universityIndex]['national_ranking_source'] = $data['O'];
                    $universityDetail[$universityIndex]['usa_rank'] = $data['P'];
                    $universityDetail[$universityIndex]['worldwide_ranking_source'] = $data['Q'];
                    $universityDetail[$universityIndex]['worldwide_rank'] = $data['R'];
                    $universityDetail[$universityIndex]['admission_success_rate'] = $data['S'];
                    $universityDetail[$universityIndex]['usp'] = $data['T'];
                    $universityDetail[$universityIndex]['research_spendings'] = $data['U'];
                    $universityDetail[$universityIndex]['placement'] = $data['V'];
                    $universityDetail[$universityIndex]['campuses'] = array();

                    $campusNames = array();
                    $campusIndex = 0;
                    foreach($sheetData as $key_1 => $data_1)
                    {
                        if($key_1 == 1)
                        {
                            continue;
                        }

                        if($data_1['B'] == $data['B'] && $data_1['W'] && !in_array($data_1['W'], $campusNames))
                        {
                            $campusNames[] = $data_1['W'];
                            $campusDetail['name'] = $data_1['W'];
                            $campusDetail['state'] = $data_1['X'];
                            $campusDetail['city'] = $data_1['Y'];
                            $campusDetail['size'] = $data_1['Z'];
                            $campusDetail['accomodation_type'] = $data_1['AA'];
                            $campusDetail['size_unit'] = $data_1['AB'];
                            $campusDetail['timezone'] = $data_1['AC'];
                            $campusDetail['locale'] = $data_1['AD'];
                            $campusDetail['colleges'] = array();
                            $universityDetail[$universityIndex]['campuses'][$campusIndex] = $campusDetail;

                            $collegeNames = array();
                            $collegeIndex = 0;
                            foreach($sheetData as $key_2 => $data_2)
                            {
                                if($key_2 == 1)
                                {
                                    continue;
                                }

                                if($data_2['B'] == $data['B'] && $data_2['W'] == $data_1['W'] && $data_2['AF'] && !in_array($data_2['AF'], $collegeNames))
                                {
                                    $collegeCategoryId = NULL;
                                    foreach($allCollegeCategories as $index => $collegeCategory)
                                    {
                                        if(strpos($data_2['AE'], $collegeCategory->display_name) !== FALSE)
                                        {
                                            $collegeCategoryId = $collegeCategory->id;
                                        }
                                    }
                                    $collegeNames[] = $data_2['AF'];
                                    $collegeDetail['category'] = $collegeCategoryId;
                                    $collegeDetail['name'] = $data_2['AF'];
                                    $collegeDetail['departments'] = array();
                                    $universityDetail[$universityIndex]['campuses'][$campusIndex]['colleges'][$collegeIndex] = $collegeDetail;

                                    $departmentNames = array();
                                    $departmentIndex = 0;

                                    foreach($sheetData as $key_3 => $data_3)
                                    {
                                        if($key_3 == 1)
                                        {
                                            continue;
                                        }

                                        if($data_3['B'] == $data['B'] && $data_3['W'] == $data_1['W'] && $data_3['AF'] == $data_2['AF'] && $data_3['AG'] && !in_array($data_3['AG'], $departmentNames))
                                        {
                                            $departmentNames[] = $data_3['AG'];
                                            $departmentDetail['name'] = $data_3['AG'];
                                            $departmentDetail['contact_person_title'] = $data_3['AH'];
                                            $departmentDetail['contact_person_name'] = $data_3['AI'];
                                            $departmentDetail['contact_person_email'] = $data_3['AJ'];
                                            $departmentDetail['courses'] = array();
                                            $universityDetail[$universityIndex]['campuses'][$campusIndex]['colleges'][$collegeIndex]['departments'][$departmentIndex] = $departmentDetail;

                                            $courseNames = array();
                                            $courseIndex = 0;

                                            foreach($sheetData as $key_4 => $data_4)
                                            {
                                                if($key_4 == 1)
                                                {
                                                    continue;
                                                }

                                                if($data_4['B'] == $data['B'] && $data_4['W'] == $data_1['W'] && $data_4['AF'] == $data_2['AF'] && $data_4['AG'] == $data_3['AG'] && $data_4['AN'] && !in_array($data_4['AN'] . '-' . $data_4['AL'], $courseNames))
                                                {
                                                    /*$helloUniData = $data_4;
                                                    $helloUniData['BD'] = "FAILED";
                                                    $sheetId = $this->user_model->addRecords($helloUniData);*/

                                                    $courseCategoryId = NULL;
                                                    $degreeId = NULL;

                                                    foreach($allCourseCategories as $index => $courseCategory)
                                                    {
                                                        if(strpos($data_4['AK'], $courseCategory->display_name) !== FALSE)
                                                        {
                                                            $courseCategoryId = $courseCategory->id;
                                                        }
                                                    }

                                                    foreach($allDegrees as $index => $degree)
                                                    {
                                                        if(strpos($data_4['AL'], $degree->name) !== FALSE)
                                                        {
                                                            $degreeId = $degree->id;
                                                        }
                                                    }

                                                    $courseNames[] = $data_4['AN'] . '-' . $data_4['AL'];
                                                    $courseDetail['name'] = $data_4['AN'];
                                                    $courseDetail['substream'] = $courseCategoryId;
                                                    $courseDetail['degree'] = $degreeId;
                                                    $courseDetail['degree_substream'] = $data_4['AM'];
                                                    $courseDetail['duration'] = $data_4['AO'];
                                                    $courseDetail['deadline_url'] = $data_4['AP'];
                                                    $courseDetail['no_of_intake'] = $data_4['AQ'];
                                                    $courseDetail['intake_months'] = json_encode(explode(',', $data_4['AR']));
                                                    $courseDetail['application_fee'] = $data_4['AS'];
                                                    $courseDetail['application_fee_amount'] = $data_4['AT'];
                                                    $courseDetail['application_fee_waiver_imperial'] = $data_4['AU'];
                                                    $courseDetail['total_fees'] = $data_4['AV'];
                                                    $courseDetail['cost_of_living_year'] = $data_4['AW'];
                                                    $courseDetail['contact_person_title'] = $data_4['AX'];
                                                    $courseDetail['contact_person_name'] = $data_4['AY'];
                                                    $courseDetail['contact_person_email'] = $data_4['AZ'];
                                                    $courseDetail['eligibility_gpa'] = $data_4['BA'];
                                                    $courseDetail['gpa_scale'] = $data_4['BB'];
                                                    $courseDetail['test_required'] = $data_4['BC'];

                                                    $courseDetail['eligibility_gre'] = json_encode(array(
                                                        'quant' => $data_4['BD'] ? $data_4['BD'] : '',
                                                        'verbal' => $data_4['BD'] ? $data_4['BD'] : ''
                                                    ));
                                                    $courseDetail['eligibility_gmat'] = $data_4['BE'];
                                                    $courseDetail['eligibility_sat'] = $data_4['BF'];
                                                    $courseDetail['eligibility_english_proficiency'] = $data_4['BG'];
                                                    $courseDetail['eligibility_toefl'] = $data_4['BH'];
                                                    $courseDetail['eligibility_ielts'] = $data_4['BI'];
                                                    $courseDetail['eligibility_pte'] = $data_4['BJ'];
                                                    $courseDetail['eligibility_duolingo'] = $data_4['BK'];
                                                    $courseDetail['eligibility_work_exp'] = $data_4['BL'];
                                                    $lorNeeded = ($data_4['BM'] && $data_4['BM'] == 'Yes') ? 1 : 0;
                                                    $lorRequired = $data_4['BN'];
                                                    $courseDetail['lor'] = json_encode(array(
                                                        'lor_needed' => $lorNeeded,
                                                        'lor_required' => $lorRequired
                                                    ));
                                                    $sopNeeded = ($data_4['BO'] && $data_4['BO'] == 'Yes') ? 1 : 0;
                                                    $sopWords = $data_4['BP'];
                                                    $courseDetail['sop'] = json_encode(array(
                                                        'sop_needed' => $sopNeeded,
                                                        'sop_no_of_words' => $sopWords
                                                    ));
                                                    $courseDetail['resume_required'] = $data_4['BQ'];
                                                    $courseDetail['admission_requirements'] = $data_4['BR'];
                                                    $courseDetail['expected_salary'] = $data_4['BS'];
                                                    $backlogsAccepted = ($data_4['BT'] && $data_4['BT'] == 'Yes') ? 1 : 0;
                                                    $noOfBacklogs = $data_4['BU'];
                                                    $courseDetail['backlogs'] = json_encode(array(
                                                        'backlog_accepted' => $backlogsAccepted,
                                                        'number_of_backlogs' => $noOfBacklogs
                                                    ));
                                                    $courseDetail['drop_years'] = $data_4['BV'];
                                                    $courseDetail['scholarship_available'] = $data_4['BW'];
                                                    $courseDetail['with_15_years_education'] = $data_4['BX'];
                                                    $courseDetail['wes_requirement'] = $data_4['BY'];
                                                    $courseDetail['work_while_studying'] = $data_4['BZ'];
                                                    $courseDetail['cooperate_internship_participation'] = $data_4['CA'];
                                                    $workPermitNeeded = ($data_4['CB'] && $data_4['CB'] == 'Yes') ? 1 : 0;
                                                    $noOfMonths = $data_4['CC'];
                                                    $courseDetail['work_permit'] = json_encode(array(
                                                        'work_permit_needed' => $workPermitNeeded,
                                                        'no_of_months' => $noOfMonths
                                                    ));
                                                    $courseDetail['start_months'] = json_encode(explode(',', $data_4['CD']));
                                                    $courseDetail['decision_months'] = json_encode(explode(',', $data_4['CE']));
                                                    $courseDetail['conditional_admit'] = $data_4['CF'];

                                                    //$courseDetail['sheet_id'] = $sheetId;
                                                    $universityDetail[$universityIndex]['campuses'][$campusIndex]['colleges'][$collegeIndex]['departments'][$departmentIndex]['courses'][$courseIndex] = $courseDetail;
                                                    $courseIndex++;
                                                }
                                            }
                                            $departmentIndex++;
                                        }
                                    }
                                    $collegeIndex++;
                                }
                            }
                            $campusIndex++;
                        }
                    }
                    $universityIndex++;
                }
            }
            //echo '<pre>';
            //print_r($universityDetail);
            //print_r($countryNames);

            if($universityDetail)
            {
                //echo '<pre>';
                $countriesDetail = $this->country_model->getCountriesByNames($countryNames);
                $helloUniDataId = 0;
                foreach($universityDetail as $universityIndex => $universityData)
                {
                    $countryId = '';
                    $universityId = '';
                    //echo "Country Name - " . $universityData['country'] . "<br>";
                    foreach($countriesDetail as $countryIndex => $countryDetail)
                    {
                        if(strpos($countryDetail['name'], $universityData['country']) !== FALSE)
                        {
                            $countryId = $countryDetail['id'];
                        }
                    }
                    /*echo "Country Id - " . $countryId . "<br>";
                    echo "University Name - " . $universityData['username'] . "<br>";
                    exit;*/
                    $checkUser = $this->user_model->checkUserByUsername($universityData['username']);
                    if($checkUser && $checkUser[0]['id'])
                    {
                        $universityId = $checkUser[0]['id'];
                        $universityLoginId = $checkUser[0]['user_id'];

                        $userUpdatedData['name'] = $universityData['name'];
                        $userUpdatedData['email'] = $universityData['email'] ? $universityData['email'] : NULL;
                        $userUpdatedData['password'] = md5($universityData['password']);

                        $this->user_model->updateUserInfo($userUpdatedData, $universityLoginId);

                        $universityUpdatedData['country_id'] = $countryId;
                        $universityUpdatedData['website'] = $universityData['website'];
                        $universityUpdatedData['about'] = $universityData['description'];
                        $universityUpdatedData['establishment_year'] = $universityData['establishment_year'] ? $universityData['establishment_year'] : NULL;
                        $universityUpdatedData['type'] = $universityData['type'] ? $universityData['type'] : NULL;
                        $universityUpdatedData['carnegie_accreditation'] = $universityData['carnegie_accreditation'];
                        $universityUpdatedData['total_students'] = $universityData['total_student'] ? $universityData['total_student'] : NULL;
                        $universityUpdatedData['total_students_UG'] = $universityData['total_student_ug'] ? $universityData['total_student_ug'] : NULL;
                        $universityUpdatedData['total_students_PG'] = $universityData['total_student_pg'] ? $universityData['total_student_pg'] : NULL;
                        $universityUpdatedData['total_students_international'] = $universityData['total_student_international'] ? $universityData['total_student_international'] : NULL;
                        $universityUpdatedData['ranking_usa'] = $universityData['usa_rank'] ? $universityData['usa_rank'] : NULL;
                        $universityUpdatedData['ranking_world'] = $universityData['worldwide_rank'] ? $universityData['worldwide_rank'] : NULL;
                        $universityUpdatedData['admission_success_rate'] = $universityData['admission_success_rate'] ? $universityData['admission_success_rate'] : NULL;
                        $universityUpdatedData['unique_selling_points'] = $universityData['usp'];
                        $universityUpdatedData['research_spending'] = $universityData['research_spendings'] ? $universityData['research_spendings'] : NULL;
                        $universityUpdatedData['placement_percentage'] = $universityData['placement'] ? $universityData['placement'] : NULL;
                        $universityUpdatedData['national_ranking_source'] = $universityData['national_ranking_source'] ? $universityData['national_ranking_source'] : NULL;
                        $universityUpdatedData['worldwide_ranking_source'] = $universityData['worldwide_ranking_source'] ? $universityData['worldwide_ranking_source'] : NULL;
                        $universityUpdatedData['date_updated'] = date('Y-m-d H:i:s');

                        $this->university_model->updateUniversity($universityId, $universityUpdatedData);

                        //echo "University Id - " . $universityId . "<br>";
                    }
                    else if($countryId)
                    {
                        $data = array(
                        	'name' 			=>  $universityData['name'],
                            'username' 		=>  $universityData['username'],
                        	'email' 		=>  $universityData['email'] ? $universityData['email'] : NULL,
                        	'password' 		=>  md5($universityData['password']),
                			'type' 			=>  '3',
                			'createdate' 	=>  date('Y-m-d'),
                			'status' 		=>	1,
                			'view' 			=>	1,
                			'create' 		=>	1,
                			'edit' 			=>	1,
                			'del' 			=>	1,
                			'notification' 	=>	1,
                            'country_id'    => $countryId,
                            'website' 		=>  $universityData['website'],
                            'about' 		=>  $universityData['description'],
                            'establishment_year' =>  $universityData['establishment_year'] ? $universityData['establishment_year'] : NULL,
                 			'university_type' 		=>  $universityData['type'] ? $universityData['type'] : NULL,
                 			'carnegie_accreditation' =>  $universityData['carnegie_accreditation'],
                 			'total_students' =>	$universityData['total_student'] ? $universityData['total_student'] : NULL,
                 			'total_students_UG' =>	$universityData['total_student_ug'] ? $universityData['total_student_ug'] : NULL,
                 			'total_students_PG' =>	$universityData['total_student_pg'] ? $universityData['total_student_pg'] : NULL,
                 			'total_students_international' 	=>	$universityData['total_student_international'] ? $universityData['total_student_international'] : NULL,
                            'ranking_usa' 	=>	$universityData['usa_rank'] ? (int)trim($universityData['usa_rank']) : NULL,
                            'ranking_world' =>	$universityData['worldwide_rank'] ? (int)trim($universityData['worldwide_rank']) : NULL,
                 			'admission_success_rate' =>	$universityData['admission_success_rate'] ? $universityData['admission_success_rate'] : NULL,
                 			'unique_selling_points' =>	$universityData['usp'],
                            'research_spending' 	=>	$universityData['research_spendings'] ? $universityData['research_spendings'] : NULL,
                            'placement_percentage' 	=>	$universityData['placement'] ? $universityData['placement'] : NULL,
                            'national_ranking_source' => $universityData['national_ranking_source'] ? $universityData['national_ranking_source'] : NULL,
                            'worldwide_ranking_source' => $universityData['worldwide_ranking_source'] ? $universityData['worldwide_ranking_source'] : NULL,
                            'date_created' 	=>	date('Y-m-d H:i:s'),
                            'date_updated' 	=>	date('Y-m-d H:i:s'),
                            'added_by' 	=>	1,
                            'updated_by' 	=>	1
            			);
                        /*echo '<pre>';
                        print_r($data);
                        echo "<br>";*/

                        $insertUniversity = $this->user_model->insertUniDetails($data);
                        //print_r($insertUniversity);
                        if($insertUniversity['success'])
                        {
                            $universityId = $insertUniversity['university_id'];
                        }
                        //echo "University Id - " . $universityId . "<br>";
                        //exit;
                    }
                    if($universityId)
                    {
                        foreach($universityData['campuses'] as $campusIndex => $campusDetail)
                        {
                            //echo "Campus Name - " . $campusDetail['name'] . "<br>";
                            $campusId = '';
                            $campusExist = $this->campus_model->getCampusByName($campusDetail['name'], $universityId);
                            if($campusExist)
                            {
                                $campusId = $campusExist[0]['id'];
                                $campusUpdatedData['state'] = $campusDetail['state'];
                                $campusUpdatedData['city'] = $campusDetail['city'];
                                $campusUpdatedData['size'] = $campusDetail['size'] ? $campusDetail['size'] : NULL;
                                $campusUpdatedData['size_unit'] = $campusDetail['size_unit'] ? $campusDetail['size_unit'] : NULL;
                                $campusUpdatedData['timezone'] = $campusDetail['timezone'];
                                $campusUpdatedData['locale'] = $campusDetail['locale'];
                                $campusUpdatedData['accomodation_type'] = $campusDetail['accomodation_type'];
                                $campusUpdatedData['date_updated'] = date('Y-m-d H:i:s');

                                $this->campus_model->editCampus($campusId, $campusUpdatedData);
                                //echo "Campus Id - " . $campusId . "<br>";
                            }
                            else
                            {
                                $data = array(
                                	'name' 			=>  $campusDetail['name'],
                                	'university_id' =>  $universityId,
                                	'state' 		=>  $campusDetail['state'],
                        			'city' 		    =>	$campusDetail['city'],
                        			'size' 			=>	$campusDetail['size'] ? $campusDetail['size'] : NULL,
                                    'size_unit' 	=>	$campusDetail['size_unit'] ? $campusDetail['size_unit'] : NULL,
                        			'timezone' 		=>	$campusDetail['timezone'],
                        			'locale' 		=>	$campusDetail['locale'],
                                    'accomodation_type' => $campusDetail['accomodation_type'],
                                    'date_created' 	=>	date('Y-m-d H:i:s'),
                                    'date_updated' 	=>	date('Y-m-d H:i:s'),
                                    'added_by' 	=>	1,
                                    'updated_by' 	=>	1
                    			);

                                //print_r($data);
                                //echo "<br>";
                                $campusId = $this->campus_model->addCampus($data);
                                //echo "Campus Id - " . $campusId . "<br>";
                                //exit
                            }

                            if($campusId)
                            {
                                foreach($campusDetail['colleges'] as $collegeIndex => $collegeDetail)
                                {
                                    //echo "College Name - " . $collegeDetail['name'] . "<br>";
                                    $collegeId = '';
                                    $collegeExist = $this->college_model->getCollegeByName($collegeDetail['name'], $campusId);
                                    if($collegeExist)
                                    {
                                        $collegeId = $collegeExist[0]['id'];

                                        $collegeUpdatedData['category_id'] = $collegeDetail['category'] ? $collegeDetail['category'] : NULL;
                                        $collegeUpdatedData['date_updated'] = date('Y-m-d H:i:s');
                                        $this->college_model->editCollege($collegeId, $collegeUpdatedData);
                                        //echo "College Id - " . $collegeId . "<br>";
                                    }
                                    else
                                    {
                                        $data = array(
                                        	'name' 			=>  $collegeDetail['name'],
                                        	'campus_id' 	=>  $campusId,
                                            'category_id' 	=>  $collegeDetail['category'] ? $collegeDetail['category'] : NULL,
                                            'date_created' 	=>	date('Y-m-d H:i:s'),
                                            'date_updated' 	=>	date('Y-m-d H:i:s'),
                                            'added_by' 	=>	1,
                                            'updated_by' 	=>	1
                            			);

                                        //print_r($data);
                                        //echo "<br>";

                                        $collegeId = $this->college_model->addCollege($data);
                                        //echo "College Id - " . $collegeId . "<br>";
                                    }

                                    if($collegeId)
                                    {
                                        foreach($collegeDetail['departments'] as $departmentIndex => $departmentDetail)
                                        {
                                            //echo "Department Name - " . $departmentDetail['name'] . "<br>";
                                            $departmentId = '';
                                            $departmentExist = $this->department_model->getDepartmentByName($departmentDetail['name'], $collegeId);
                                            if($departmentExist)
                                            {
                                                $departmentId = $departmentExist[0]['id'];
                                                $departmentUpdatedData['contact_title'] = $departmentDetail['contact_person_title'];
                                                $departmentUpdatedData['contact_name'] = $departmentDetail['contact_person_name'];
                                                $departmentUpdatedData['contact_email'] = $departmentDetail['contact_person_email'];
                                                $departmentUpdatedData['date_updated'] = date('Y-m-d H:i:s');

                                                $this->department_model->editDepartment($departmentId, $departmentUpdatedData);
                                                //echo "Department Id - " . $departmentId . "<br>";
                                            }
                                            else
                                            {
                                                $data = array(
                                                	'name' 			=>  $departmentDetail['name'],
                                                	'college_id' 	=>  $collegeId,
                                                    'contact_title' => $departmentDetail['contact_person_title'],
                                                    'contact_name'  => $departmentDetail['contact_person_name'],
                                                    'contact_email' => $departmentDetail['contact_person_email'],
                                                    'date_created' 	=>	date('Y-m-d H:i:s'),
                                                    'date_updated' 	=>	date('Y-m-d H:i:s'),
                                                    'added_by' 	=>	1,
                                                    'updated_by' 	=>	1
                                    			);

                                                //print_r($data);
                                                //echo "<br>";

                                                $departmentId = $this->department_model->addDepartment($data);
                                                //echo "Department Id - " . $departmentId . "<br>";
                                            }

                                            if($departmentId)
                                            {
                                                foreach($departmentDetail['courses'] as $courseIndex => $courseDetail)
                                                {
                                                    //$helloUniDataId = $helloUniDataId + 1;
                                                    //echo "Course Name - " . $courseDetail['name'] . " Degree - " . $courseDetail['degree'] . "<br>";
                                                    $courseId = '';
                                                    $courseExist = $this->course_model->getCourseByNameDegree($courseDetail['name'], $courseDetail['degree'], $departmentId);
                                                    if($courseExist)
                                                    {
                                                        $courseId = $courseExist[0]['id'];

                                                        $courseUpdatedData['category_id'] = $courseDetail['substream'] ? $courseDetail['substream'] : NULL;
                                                        $courseUpdatedData['degree_id'] = $courseDetail['degree'];
                                                        $courseUpdatedData['degree_substream'] = $courseDetail['degree_substream'] ? $courseDetail['degree_substream'] : NULL;
                                                        $courseUpdatedData['duration'] = $courseDetail['duration'] ? $courseDetail['duration'] : NULL;
                                                        $courseUpdatedData['deadline_link'] = $courseDetail['deadline_url'];
                                                        $courseUpdatedData['no_of_intake'] = $courseDetail['no_of_intake'] ? $courseDetail['no_of_intake'] : NULL;
                                                        $courseUpdatedData['intake_months'] = $courseDetail['intake_months'] ? $courseDetail['intake_months'] : NULL;
                                                        $courseUpdatedData['application_fee'] = $courseDetail['application_fee'] ? $courseDetail['application_fee'] : NULL;
                                                        $courseUpdatedData['application_fee_amount'] = $courseDetail['application_fee_amount'] ? $courseDetail['application_fee_amount'] : NULL;
                                                        $courseUpdatedData['application_fee_waiver_imperial'] = $courseDetail['application_fee_waiver_imperial'] ? $courseDetail['application_fee_waiver_imperial'] : NULL;
                                                        $courseUpdatedData['total_fees'] = $courseDetail['total_fees'] ? $courseDetail['total_fees'] : NULL;
                                                        $courseUpdatedData['cost_of_living_year'] = $courseDetail['cost_of_living_year'] ? $courseDetail['cost_of_living_year'] : NULL;
                                                        $courseUpdatedData['contact_title'] = $courseDetail['contact_person_title'];
                                                        $courseUpdatedData['contact_name'] = $courseDetail['contact_person_name'];
                                                        $courseUpdatedData['contact_email'] = $courseDetail['contact_person_email'];
                                                        $courseUpdatedData['eligibility_gpa'] = $courseDetail['eligibility_gpa'] ? $courseDetail['eligibility_gpa'] : NULL;
                                                        $courseUpdatedData['gpa_scale'] = $courseDetail['gpa_scale'] ? $courseDetail['gpa_scale'] : NULL;
                                                        $courseUpdatedData['test_required'] = $courseDetail['test_required'] ? $courseDetail['test_required'] : NULL;
                                                        $courseUpdatedData['eligibility_gre'] = $courseDetail['eligibility_gre'];
                                                        $courseUpdatedData['eligibility_gmat'] = $courseDetail['eligibility_gmat'] ? $courseDetail['eligibility_gmat'] : NULL;
                                                        $courseUpdatedData['eligibility_sat'] = $courseDetail['eligibility_sat'] ? $courseDetail['eligibility_sat'] : NULL;
                                                        $courseUpdatedData['eligibility_english_proficiency'] = $courseDetail['eligibility_english_proficiency'] ? $courseDetail['eligibility_english_proficiency'] : NULL;
                                                        $courseUpdatedData['eligibility_toefl'] = $courseDetail['eligibility_toefl'] ? $courseDetail['eligibility_toefl'] : NULL;
                                                        $courseUpdatedData['eligibility_ielts'] = $courseDetail['eligibility_ielts'] ? $courseDetail['eligibility_ielts'] : NULL;
                                                        $courseUpdatedData['eligibility_pte'] = $courseDetail['eligibility_pte'] ? $courseDetail['eligibility_pte'] : NULL;
                                                        $courseUpdatedData['eligibility_duolingo'] = $courseDetail['eligibility_duolingo'] ? $courseDetail['eligibility_duolingo'] : NULL;
                                                        $courseUpdatedData['eligibility_work_experience'] = $courseDetail['eligibility_work_exp'] ? $courseDetail['eligibility_work_exp'] : NULL;
                                                        $courseUpdatedData['lor'] = $courseDetail['lor'];
                                                        $courseUpdatedData['sop'] = $courseDetail['sop'];
                                                        $courseUpdatedData['resume_required'] = $courseDetail['resume_required'] ? $courseDetail['resume_required'] : NULL;
                                                        $courseUpdatedData['admission_requirements'] = $courseDetail['admission_requirements'] ? $courseDetail['admission_requirements'] : NULL;
                                                        $courseUpdatedData['expected_salary'] = $courseDetail['expected_salary'] ? $courseDetail['expected_salary'] : NULL;
                                                        $courseUpdatedData['backlogs'] = $courseDetail['backlogs'] ? $courseDetail['backlogs'] : NULL;
                                                        $courseUpdatedData['drop_years'] = $courseDetail['drop_years'] ? $courseDetail['drop_years'] : NULL;
                                                        $courseUpdatedData['scholarship_available'] = $courseDetail['scholarship_available'] ? $courseDetail['scholarship_available'] : NULL;
                                                        $courseUpdatedData['with_15_years_education'] = $courseDetail['with_15_years_education'] ? $courseDetail['with_15_years_education'] : NULL;
                                                        $courseUpdatedData['wes_requirement'] = $courseDetail['wes_requirement'] ? $courseDetail['wes_requirement'] : NULL;
                                                        $courseUpdatedData['work_while_studying'] = $courseDetail['work_while_studying'] ? $courseDetail['work_while_studying'] : NULL;
                                                        $courseUpdatedData['cooperate_internship_participation'] = $courseDetail['cooperate_internship_participation'] ? $courseDetail['cooperate_internship_participation'] : NULL;
                                                        $courseUpdatedData['work_permit'] = $courseDetail['work_permit'] ? $courseDetail['work_permit'] : NULL;
                                                        $courseUpdatedData['start_months'] = $courseDetail['start_months'] ? $courseDetail['start_months'] : NULL;
                                                        $courseUpdatedData['decision_months'] = $courseDetail['decision_months'] ? $courseDetail['decision_months'] : NULL;
                                                        $courseUpdatedData['conditional_admit'] = $courseDetail['conditional_admit'] ? $courseDetail['conditional_admit'] : NULL;
                                                        $courseUpdatedData['date_updated'] = date('Y-m-d H:i:s');

                                                        $this->course_model->editCourse($courseId, $courseUpdatedData);
                                                        //echo "Course Id - " . $courseId . "<br>";
                                                    }
                                                    else
                                                    {
                                                        $data = array(
                                                        	'name' 			=>  $courseDetail['name'],
                                                        	'department_id' =>  $departmentId,
                                                            'category_id' 	=>  $courseDetail['substream'] ? $courseDetail['substream'] : NULL,
                                                            'degree_substream' 	=>  $courseDetail['degree_substream'] ? $courseDetail['degree_substream'] : NULL,
                                                            'degree_id' 	=>  $courseDetail['degree'],
                                                            'duration' 		=>  $courseDetail['duration'] ? $courseDetail['duration'] : NULL,
                                                            'deadline_link' =>  $courseDetail['deadline_url'],
                                                            'no_of_intake' 		=>  $courseDetail['no_of_intake'] ? $courseDetail['no_of_intake'] : NULL,
                                                            'intake_months' 		=>  $courseDetail['intake_months'] ? $courseDetail['intake_months'] : NULL,
                                                            'application_fee' 	=>  $courseDetail['application_fee'] ? $courseDetail['application_fee'] : NULL,
                                                            'application_fee_amount' 	=>  $courseDetail['application_fee_amount'] ? $courseDetail['application_fee_amount'] : NULL,
                                                            'application_fee_waiver_imperial' 	=>  $courseDetail['application_fee_waiver_imperial'] ? $courseDetail['application_fee_waiver_imperial'] : NULL,
                                                            'total_fees' 	=>  $courseDetail['total_fees'] ? $courseDetail['total_fees'] : NULL,
                                                            'cost_of_living_year' 	=>  $courseDetail['cost_of_living_year'] ? $courseDetail['cost_of_living_year'] : NULL,
                                                            'contact_title' =>  $courseDetail['contact_person_title'],
                                                            'contact_name' 	=>  $courseDetail['contact_person_name'],
                                                            'contact_email' =>  $courseDetail['contact_person_email'],
                                                            'eligibility_gpa' =>  $courseDetail['eligibility_gpa'] ? $courseDetail['eligibility_gpa'] : NULL,
                                                            'gpa_scale' 	=>  $courseDetail['gpa_scale'] ? $courseDetail['gpa_scale'] : NULL,
                                                            'test_required' 	=>  $courseDetail['test_required'] ? $courseDetail['test_required'] : NULL,
                                                            'eligibility_gre' =>  $courseDetail['eligibility_gre'],
                                                            'eligibility_gmat' =>  $courseDetail['eligibility_gmat'] ? $courseDetail['eligibility_gmat'] : NULL,
                                                            'eligibility_sat' =>  $courseDetail['eligibility_sat'] ? $courseDetail['eligibility_sat'] : NULL,
                                                            'eligibility_english_proficiency' =>  $courseDetail['eligibility_english_proficiency'] ? $courseDetail['eligibility_english_proficiency'] : NULL,
                                                            'eligibility_toefl' =>  $courseDetail['eligibility_toefl'] ? $courseDetail['eligibility_toefl'] : NULL,
                                                            'eligibility_ielts' =>  $courseDetail['eligibility_ielts'] ? $courseDetail['eligibility_ielts'] : NULL,
                                                            'eligibility_pte' =>  $courseDetail['eligibility_pte'] ? $courseDetail['eligibility_pte'] : NULL,
                                                            'eligibility_duolingo' =>  $courseDetail['eligibility_duolingo'] ? $courseDetail['eligibility_duolingo'] : NULL,
                                                            'eligibility_work_experience' =>  $courseDetail['eligibility_work_exp'] ? $courseDetail['eligibility_work_exp'] : NULL,
                                                            'lor' => $courseDetail['lor'],
                                                            'sop' => $courseDetail['sop'],
                                                            'resume_required' =>  $courseDetail['resume_required'] ? $courseDetail['resume_required'] : NULL,
                                                            'admission_requirements' =>  $courseDetail['admission_requirements'] ? $courseDetail['admission_requirements'] : NULL,
                                                            'expected_salary' =>  $courseDetail['expected_salary'] ? $courseDetail['expected_salary'] : NULL,
                                                            'backlogs' =>  $courseDetail['backlogs'] ? $courseDetail['backlogs'] : NULL,
                                                            'drop_years' =>  $courseDetail['drop_years'] ? $courseDetail['drop_years'] : NULL,
                                                            'scholarship_available' =>  $courseDetail['scholarship_available'] ? $courseDetail['scholarship_available'] : NULL,
                                                            'with_15_years_education' =>  $courseDetail['with_15_years_education'] ? $courseDetail['with_15_years_education'] : NULL,
                                                            'wes_requirement' =>  $courseDetail['wes_requirement'] ? $courseDetail['wes_requirement'] : NULL,
                                                            'work_while_studying' =>  $courseDetail['work_while_studying'] ? $courseDetail['work_while_studying'] : NULL,
                                                            'cooperate_internship_participation' =>  $courseDetail['cooperate_internship_participation'] ? $courseDetail['cooperate_internship_participation'] : NULL,
                                                            'work_permit' =>  $courseDetail['work_permit'] ? $courseDetail['work_permit'] : NULL,
                                                            'start_months' =>  $courseDetail['start_months'] ? $courseDetail['start_months'] : NULL,
                                                            'decision_months' =>  $courseDetail['decision_months'] ? $courseDetail['decision_months'] : NULL,
                                                            'conditional_admit' =>  $courseDetail['conditional_admit'] ? $courseDetail['conditional_admit'] : NULL,
                                                            'date_created' 	=>	date('Y-m-d H:i:s'),
                                                            'date_updated' 	=>	date('Y-m-d H:i:s'),
                                                            'added_by' 	=>	1,
                                                            'updated_by' 	=>	1
                                            			);

                                                        /*echo '<pre>';
                                                        print_r($data);
                                                        echo "<br>";*/
                                                        $courseId = $this->course_model->addCourse($data);
                                                        //echo "Course Id - " . $courseId . "<br>";
                                                        //exit;
                                                        /*if($courseId)
                                                        {
                                                            $helloUniData = [];
                                                            $helloUniData['BD'] = "SUCCESS";
                                                            $this->user_model->updateRecords($helloUniData, $courseDetail['sheet_id']);
                                                        }*/
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //exit;
                $this->session->set_flashdata('flash_message', "Success: You have successfully uploaded the file!");
       	        redirect('user/university/');
            }
        }
    }

    function research()
    {
        $this->render_page('templates/user/upload_researches');
    }

    function upload_researches()
    {
        $config['upload_path']   = './upload/';
        $config['allowed_types'] = 'gif|jpg|png|xlsx|csv|xls|octet-stream|xml';
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('userfile'))
        {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('templates/user/upload', $error);
        }
        else
        {
            $fileData = $this->upload->data();
            $inputFileName = $fileData['full_path'];

            $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
            $spreadsheet = $reader->load($inputFileName);
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

            $universityNames = array();
            $countryNames = array();
            $researchData = array();
            $collegeData = array();
            $departmentData = array();
            $researches = array();

            $universityIndex = 0;
            foreach($sheetData as $index => $data)
            {
                if($index == 1)
                {
                    continue;
                }

                if(!$data['A'])
                {
                    break;
                }

                if($data['A'] && !in_array($data['A'], $countryNames))
                {
                    $countryNames[] = $data['A'];
                }

                if($data['B'] && !in_array($data['B'], $universityNames))
                {
                    $universityNames[] = $data['B'];
                    $researches[$universityIndex]['country'] = $data['A'];
                    $researches[$universityIndex]['university_name'] = $data['B'];
                    $researches[$universityIndex]['colleges'] = array();

                    $collegeIndex = 0;
                    $collegeNames = array();
                    foreach($sheetData as $key_1 => $data_1)
                    {
                        if($key_1 == 1)
                        {
                            continue;
                        }
                        if($data_1['B'] == $data['B'] && $data_1['C'] && !in_array($data_1['C'], $collegeNames))
                        {
                            $collegeNames[] = $data_1['C'];
                            $collegeData['name'] = $data_1['C'];
                            $collegeData['departments'] = array();
                            $researches[$universityIndex]['colleges'][$collegeIndex] = $collegeData;

                            $departmentIndex = 0;
                            $departmentNames = array();
                            foreach($sheetData as $key_2 => $data_2)
                            {
                                if($key_2 == 1)
                                {
                                    continue;
                                }
                                if($data_2['C'] == $data_1['C'] && $data_2['D'] && !in_array($data_2['D'], $departmentNames))
                                {
                                    $departmentNames[] = $data_2['D'];
                                    $departmentData['name'] = $data_2['D'];
                                    $departmentData['researches'] = array();
                                    $researches[$universityIndex]['colleges'][$collegeIndex]['departments'][$departmentIndex] = $departmentData;

                                    $researchIndex = 0;
                                    $researchURLs = array();
                                    foreach($sheetData as $key_3 => $data_3)
                                    {
                                        if($key_3 == 1)
                                        {
                                            continue;
                                        }
                                        if($data_3['D'] == $data_2['D'] && $data_3['G'] && !in_array($data_3['G'], $researchURLs))
                                        {
                                            $researchURLs[] = $data_3['G'];
                                            $researchData['topic'] = $data_3['E'];
                                            $researchData['description'] = $data_3['F'];
                                            $researchData['url'] = $data_3['G'];
                                            $researchData['faculty_url'] = $data_3['H'];
                                            $researchData['funding'] = $data_3['I'];
                                            $researches[$universityIndex]['colleges'][$collegeIndex]['departments'][$departmentIndex]['researches'][$researchIndex] = $researchData;

                                            $researchIndex++;
                                        }
                                    }
                                    $departmentIndex++;
                                }
                            }
                            $collegeIndex++;
                        }
                    }
                    $universityIndex++;
                }
            }

            if($researches)
            {
                $countriesDetail = $this->country_model->getCountriesByNames($countryNames);
                foreach($researches as $universityIndex => $universityData)
                {
                    $countryId = '';
                    foreach($countriesDetail as $countryIndex => $countryDetail)
                    {
                        if($countryDetail['name'] == $universityData['country'])
                        {
                            $countryId = $countryDetail['id'];
                        }
                    }
                    if($countryId)
                    {
                        $universityId = '';
                        $universityExist = $this->university_model->getUniversityByName($universityData['university_name'], $countryId);
                        if($universityExist)
                        {
                            $universityId = $universityExist[0]['id'];
                        }
                        if($universityId)
                        {
                            $collegeId = '';
                            $allColleges = $this->college_model->getAllCollegesByUniversity($universityId);
                            foreach($universityData['colleges'] as $collegeIndex => $collegeData)
                            {
                                foreach($allColleges as $collegeDetail)
                                {
                                    if($collegeDetail->name == $collegeData['name'])
                                    {
                                        $collegeId = $collegeDetail->id;
                                    }
                                }
                                /*echo "University Id - " . $universityId . "\n";
                                echo "College Id - " . $collegeId;
                                echo '<pre>';
                                print_r($researches);
                                exit;*/

                                if($collegeId)
                                {
                                    $departmentId = '';
                                    foreach($collegeData['departments'] as $departmentIndex => $departmentData)
                                    {
                                        $departmentExist = $this->department_model->getDepartmentByName($departmentData['name'], $collegeId);
                                        if($departmentExist)
                                        {
                                            $departmentId = $departmentExist[0]['id'];
                                        }

                                        if($departmentId)
                                        {
                                            foreach($departmentData['researches'] as $researchIndex => $researchData)
                                            {
                                                $researchId = '';
                                                $researchExist = $this->university_model->getresearchByURL($researchData['url']);
                                                if($researchExist)
                                                {
                                                    $researchId = $researchExist[0]['id'];
                                                }
                                                else if($researchData)
                                                {
                                                    $data = array(
                                                        'department_id' => $departmentId,
                                                        'topic' => $researchData['topic'],
                                                        'description' => $researchData['description'],
                                                        'research_url' => $researchData['url'],
                                                        'faculty_url' => $researchData['faculty_url'],
                                                        'funding' => $researchData['funding'] ? $researchData['funding'] : NULL
                                                    );

                                                    $researchId = $this->university_model->addResearch($data);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            redirect('/user/research');
        }
    }
}
?>
