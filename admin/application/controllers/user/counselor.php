<?php

// ini_set('display_errors', '1');
// ini_set('display_startup_errors', '1');
// error_reporting(E_ALL);

require_once APPPATH . 'libraries/Mail/sMail.php';
class Counselor extends MY_Controller
{
    public $outputData;    //Holds the output data for each view

    public $loggedInUser;

    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
        $this->load->library('form_validation');
    }

    function join_councelling()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/user_model');
        $userdata = $this->session->userdata('user');

        $user_id = $userdata['id'];

        if (empty($userdata)) {
            redirect('common/login');
        }


        $universityData = $this->user_model->getUniversityForJoinCouncelling($user_id);
        if ($universityData) {
            $this->outputData['id'] = $universityData->id;
        }

        $this->outputData['view'] = 'list';
        $this->render_page('templates/user/join_councelling', $this->outputData);
    }

    function index()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/user_model');
        $userdata = $this->session->userdata('user');
        if (empty($userdata)) {
            redirect('common/login');
        }
        if ($userdata['type'] == 1 || $userdata['type'] == 2) {
            $cond_user1 = array('user_master.status !=' => '5');
            $cond_user2 = array('user_master.type' => '7');
            $this->outputData['users'] = $this->user_model->getUser($cond_user1, $cond_user2);
        }
        /*else if($userdata['type']==3)
        {
            $cond_user1 = array('user_master.status !=' => '5');
            $cond_user2 = array('university_to_counselor.university_id' => $userdata['university_id']);
            $this->outputData['users'] = $this->user_model->getCounselorByUniversity($cond_user1, $cond_user2);
        }*/
        $this->outputData['view'] = 'list';
        $this->render_page('templates/user/counselor', $this->outputData);
    }

    function ajax_manage_page_counselor()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/user_model');
        $userdata = $this->session->userdata('user');

        if (empty($userdata)) {
            redirect('common/login');
        }

        if ($userdata['type'] == 1 || $userdata['type'] == 2) {
            $cond_user1 = array('user_master.status !=' => '5');
            $cond_user2 = array('user_master.type' => '7');
            $list = $this->user_model->get_datatables_counselor($cond_user1, $cond_user2);
        }

        // echo "<pre>"; print_r($list); exit;
        $data = array();
        $no = $_POST['start'];

        foreach ($list as $person) {
            //echo "<pre>"; print_r($person); exit;
            $no++;
            $row = array();
            $row[] = '<input type="checkbox" class="multichk" name="chk[]" value="<?php echo $person->id;?>"/>' . $no;
            $row[] = $person->name;
            $row[] = $person->email;
            $row[] = $person->createdate;
            $row[] = $person->status_name;
            //$row[] = $person->dob;

            //add html for action
            /*$row[] = "<a href='/admin/student/application/newList/'.$person->user_id.'/'.$person->application_id><button class='btn btn-primary btn-circle'   style='float:left'  value="">Assign to me</button></a>";*/

            $row[] = anchor(base_url('/user/counselor/form/' . $person->id), '<span title="Edit" class="edit"><i class="fa fa-edit"></i></span>');


            $data[] = $row;
        }
        //echo "<pre>"; print_r($data); exit;
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->user_model->count_all_counselor($cond_user1, $cond_user2),
            "recordsFiltered" => $this->user_model->count_filtered_counselor($cond_user1, $cond_user2),
            "data" => $data,
        );
        //echo "<pre>"; print_r($output); exit;
        //output to json format
        echo json_encode($output);
    }

    function form()
    {
        $this->load->helper('cookie_helper');
        $this->load->model(array('user/university_model', 'user/user_model'));

        $userdata = $this->session->userdata('user');
        if (empty($userdata)) {
            redirect('common/login');
        }

        if ($this->uri->segment('4')) {
            $conditions = array('user_master.id' => $this->uri->segment('4'));
            $user_details = $this->user_model->getCounselorByid($conditions);

            $this->outputData['id'] = $user_details->id;
            $this->outputData['name'] = $user_details->name;
            $this->outputData['email'] = $user_details->email;
            $this->outputData['status'] = $user_details->status;
            $this->outputData['notification'] = $user_details->notification;
            $this->outputData['username'] = $user_details->username;
            //$this->outputData['counselorUniversity'] = $user_details->university_id;
            $this->outputData['edit'] = true;
        }

        //$this->outputData['universities'] = $this->university_model->getAllUniversities();
        $this->outputData['universities'] = [];

        $this->render_page('templates/user/counselor_form', $this->outputData);
    }

    function create()
    {
        if (!$this->input->post('counselor_name') || !$this->input->post('username') || !$this->input->post('email') || !$this->input->post('password')) {
            $this->session->set_flashdata('flash_message', "Error: Please fill all mandatory fields!");
            redirect('user/counselor/form');
        }
        $this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');
        $this->load->model('user/user_model');

        $userdata = $this->session->userdata('user');
        if (empty($userdata)) {
            redirect('common/login');
        }

        $data = array(
            'name'             =>  $this->input->post('counselor_name'),
            'email'         =>  $this->input->post('email'),
            'username'         =>  $this->input->post('username'),
            'password'         =>  md5($this->input->post('password')),
            'type'             =>  '7',
            'createdate'     =>  date('Y-m-d'),
            //'university'	=>	$this->input->post('university'),
            'status'         =>    $this->input->post('status'),
            'view'             =>    $this->input->post('view'),
            'create'         =>    $this->input->post('creation'),
            'edit'             =>    $this->input->post('edit'),
            'del'             =>    $this->input->post('deletion'),
            'notification'     =>    $this->input->post('notification')
        );

        $userExist = $this->user_model->checkUserByUsername($this->input->post('username'));
        if ($userExist) {
            $this->session->set_flashdata('flash_message', "Error: The username already exists!");
            redirect('user/counselor/');
        }

        $userExist = $this->user_model->checkUserByEmail($this->input->post('email'));
        if ($userExist) {
            $this->session->set_flashdata('flash_message', "Error: The email already exists!");
            redirect('user/counselor/');
        }

        $counselorId = $this->user_model->addCounselor($data);

        if ($counselorId) {
            if ($this->input->post('notification') == 1) {
                $userEmail = $this->input->post('email');
                $name = $this->input->post('adminname');
                $username = $this->input->post('username');
                $password = $this->input->post('password');

                $mailSubject = "Notification on creation of your Counselor Account Under HelloUni";
                $mailTemplate = "Hi $name,

                Your Counselor Account has been sucessfully created by Super Admin under HelloUni. Your login credential is as follows

                Username: $username
                Password: $password

                Kindly login to your account and explore the options.

                <a href='https://www.hellouni.org/admin'>https://www.hellouni.org/admin</a>

                <b>Thanks and Regards,
                HelloUni Coordinator
                +91 81049 09690</b>

                <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

                $ccMailList = '';
                $mailAttachments = '';

                $sendMail = sMail($userEmail, $userName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);
            }
            $this->session->set_flashdata('flash_message', "Success: You have saved Counselor!");
            redirect('user/counselor/');
        }
    }

    function edit()
    {
        if (!$this->input->post('counselor_name') || !$this->input->post('email')) {
            $this->session->set_flashdata('flash_message', "Error: Please fill all mandatory fields!");
            redirect('user/counselor/form/' . $this->uri->segment('4'));
        }

        $this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');
        $this->load->model('user/user_model');

        $userdata = $this->session->userdata('user');
        if (empty($userdata)) {
            redirect('common/login');
        }

        $data = array(
            'id'            =>  $this->uri->segment('4'),
            'name'             =>  $this->input->post('counselor_name'),
            'email'         =>  $this->input->post('email'),
            'type'             =>  '7',
            'createdate'     =>  date('Y-m-d'),
            //'university'	=>	$this->input->post('university'),
            'status'         =>    $this->input->post('status'),
            'view'             =>    $this->input->post('view'),
            'create'         =>    $this->input->post('creation'),
            'edit'             =>    $this->input->post('edit'),
            'del'             =>    $this->input->post('deletion'),
            'notification'     =>    $this->input->post('notification')
        );

        if ($this->input->post('password')) {
            $data['password'] = md5($this->input->post('password'));
        }

        if ($this->user_model->editCounselor($data) == 'success') {
            if ($this->input->post('notification') == 1) {
                $userEmail = $this->input->post('email');
                $name = $this->input->post('counselor_name');
                $username = $this->input->post('username');

                $mailSubject = "Notification on modification of your Counselor Account Under HelloUni";
                $mailTemplate = "Hi $name,

                Your Counselor Account has been sucessfully modified by Super Admin under HelloUni.

                Kindly login to your account and explore the options.

                <a href='https://www.hellouni.org/admin'>https://www.hellouni.org/admin</a>

                <b>Thanks and Regards,
                HelloUni Coordinator
                +91 81049 09690</b>

                <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

                $ccMailList = '';
                $mailAttachments = '';

                $sendMail = sMail($userEmail, $userName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);
            }

            $this->session->set_flashdata('flash_message', "Success: You have modified Counselor!");
            redirect('user/counselor');
        }
    }
}
