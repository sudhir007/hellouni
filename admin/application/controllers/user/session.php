<?php

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

require APPPATH . "vendor/autoload.php";
require_once APPPATH . 'libraries/Mail/sMail.php';

use OpenTok\OpenTok;
class Session extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library(array('form_validation', 'uri'));
        $this->load->model(array('user/user_model', 'tokbox_model', 'user/university_model'));

        $this->outputData['time_array'] = array('00:00', '00:15', '00:30', '00:45', '01:00', '01:15', '01:30', '01:45', '02:00', '02:15', '02:30', '02:45', '03:00', '03:15', '03:30', '03:45', '04:00', '04:15', '04:30', '04:45', '05:00', '05:15', '05:30', '05:45', '06:00', '06:15', '06:30', '06:45', '07:00', '07:15', '07:30', '07:45', '08:00', '08:15', '08:30', '08:45', '09:00', '09:15', '09:30', '09:45', '10:00', '10:15', '10:30', '10:45', '11:00', '11:15', '11:30', '11:45', '12:00', '12:15', '12:30', '12:45', '13:00', '13:15', '13:30', '13:45', '14:00', '14:15', '14:30', '14:45', '15:00', '15:15', '15:30', '15:45', '16:00', '16:15', '16:30', '16:45', '17:00', '17:15', '17:30', '17:45', '18:00', '18:15', '18:30', '18:45', '19:00', '19:15', '19:30', '19:45', '20:00', '20:15', '20:30', '20:45', '21:00', '21:15', '21:30', '21:45', '22:00', '22:15', '22:30', '22:45', '23:00', '23:15', '23:30', '23:45');
    }

    function index()
    {
        $userdata=$this->session->userdata('user');

        if(empty($userdata)){  redirect('common/login'); }
        $universityId = 0;
        if(isset($userdata['university_id']))
        {
            $universityId = $userdata['id'];
            $universityDetail = $this->university_model->getUniversityById($userdata['id']);
            $timezone = isset($universityDetail[0]['timezone']) ? $universityDetail[0]['timezone'] : '';
        }
        else
        {
            $timezone = 5.5;
        }

        $allSessions = $this->user_model->getSlotByUniversity($universityId);
        
        if($allSessions)
        {
            foreach($allSessions as $index => $session)
            {
                $localTimezone = $session->timezone ? 5.5 - (float)$session->timezone : 5.5 - (float)$timezone;
                $session->slot_1 = $session->slot_1 ? date('Y-m-d H:i:s', strtotime($session->slot_1) - $localTimezone * 60 * 60) : '';
                $session->slot_2 = $session->slot_2 ? date('Y-m-d H:i:s', strtotime($session->slot_2) - $localTimezone * 60 * 60) : '';
                $session->slot_3 = $session->slot_3 ? date('Y-m-d H:i:s', strtotime($session->slot_3) - $localTimezone * 60 * 60) : '';
                $session->confirmed_slot = $session->confirmed_slot ? date('Y-m-d H:i:s', strtotime($session->confirmed_slot) - $localTimezone * 60 * 60) : '';
                $allSessions[$index] = $session;
            }
        }
        $this->outputData['session_page'] = 'All Sessions';
        $this->outputData['sessions'] = $allSessions;
        $this->outputData['redirect_url'] = '/user/session';
        $this->outputData['user_detail'] = $userdata;
        //$this->outputData['timezone'] = $localTimezone;
        $this->render_page('templates/user/session', $this->outputData);
    }

    function coming()
    {
        $userdata=$this->session->userdata('user');

        if(empty($userdata)){  redirect('common/login'); }
        $universityId = 0;
        if(isset($userdata['university_id']))
        {
            $universityId = $userdata['id'];
            $universityDetail = $this->university_model->getUniversityById($userdata['id']);
            $timezone = isset($universityDetail[0]['timezone']) ? $universityDetail[0]['timezone'] : '';
        }
        else
        {
            $timezone = 5.5;
        }
        //$localTimezone = 5 - (int)$timezone;

        $allSessions = $this->user_model->getComingSlotByUniversity($universityId);
        if($allSessions)
        {
            foreach($allSessions as $index => $session)
            {
                $user_id = $session->user_id;
                $universityId = $session->university_id;
                $participants = array("user_id" => $user_id,"university_id" => $universityId);
                $json_participant = json_encode($participants);
                $token = $this->tokbox_model->getTokenByParticipant($json_participant);
                $allSessions[$index]->tokbox_url = "";
                if($token && $token->token)
                {
                    $encodedTokboxId = base64_encode($token->id);
                    $encodedUniversityId = base64_encode($userdata['id']);
                    $allSessions[$index]->tokbox_url = str_replace('/admin', '', base_url()) . "/meeting/oneonone/panelist?id=" . $encodedTokboxId;
                }
                $localTimezone = $session->timezone ? 5.5 - (float)$session->timezone : 5.5 - (float)$timezone;
                $session->slot_1 = $session->slot_1 ? date('Y-m-d H:i:s', strtotime($session->slot_1) - $localTimezone * 60 * 60) : '';
                $session->slot_2 = $session->slot_2 ? date('Y-m-d H:i:s', strtotime($session->slot_2) - $localTimezone * 60 * 60) : '';
                $session->slot_3 = $session->slot_3 ? date('Y-m-d H:i:s', strtotime($session->slot_3) - $localTimezone * 60 * 60) : '';
                $session->confirmed_slot = $session->confirmed_slot ? date('Y-m-d H:i:s', strtotime($session->confirmed_slot) - $localTimezone * 60 * 60) : '';
                $allSessions[$index] = $session;
            }
        }
        $this->outputData['sessions'] = $allSessions;
        //$this->outputData['timezone'] = $localTimezone;
        $this->outputData['session_page'] = 'Coming Sessions';
        $this->outputData['redirect_url'] = '/user/session/coming';
        $this->outputData['user_detail'] = $userdata;
        $this->render_page('templates/user/session', $this->outputData);
    }

    function completed()
    {
        $userdata=$this->session->userdata('user');

        if(empty($userdata)){  redirect('common/login'); }
        $universityId = 0 ;
        if(isset($userdata['university_id']))
        {
            $universityId = $userdata['id'];
            $universityDetail = $this->university_model->getUniversityById($userdata['id']);
            $timezone = isset($universityDetail[0]['timezone']) ? $universityDetail[0]['timezone'] : '';
        }
        else
        {
            $timezone = 5.5;
        }
        //$localTimezone = 5 - (int)$timezone;

        $allSessions = $this->user_model->getCompletedSlotByUniversity($universityId);
        if($allSessions)
        {
            foreach($allSessions as $index => $session)
            {
                $user_id = $session->user_id;
                $universityId = $session->university_id;
                $participants = array("user_id" => $user_id,"university_id" => $universityId);
                $json_participant = json_encode($participants);
                $token = $this->tokbox_model->getTokenByParticipant($json_participant);
                $session->recording = 0;
                if($token && $token->archive_id)
                {
                    $session->recording = 1;
                    $session->archive_id = $token->archive_id;
                }
                $localTimezone = $session->timezone ? 5.5 - (float)$session->timezone : 5.5 - (float)$timezone;
                $session->slot_1 = $session->slot_1 ? date('Y-m-d H:i:s', strtotime($session->slot_1) - $localTimezone * 60 * 60) : '';
                $session->slot_2 = $session->slot_2 ? date('Y-m-d H:i:s', strtotime($session->slot_2) - $localTimezone * 60 * 60) : '';
                $session->slot_3 = $session->slot_3 ? date('Y-m-d H:i:s', strtotime($session->slot_3) - $localTimezone * 60 * 60) : '';
                $session->confirmed_slot = $session->confirmed_slot ? date('Y-m-d H:i:s', strtotime($session->confirmed_slot) - $localTimezone * 60 * 60) : '';
                $allSessions[$index] = $session;
            }
        }
        $this->outputData['sessions'] = $allSessions;
        $this->outputData['session_page'] = 'Completed Sessions';
        $this->outputData['redirect_url'] = '/user/session/completed';
        $this->outputData['user_detail'] = $userdata;
        //$this->outputData['timezone'] = $localTimezone;
        $this->render_page('templates/user/session', $this->outputData);
    }

    function unassigned()
    {
        $userdata=$this->session->userdata('user');

        if(empty($userdata)){  redirect('common/login'); }
        $universityId = 0;
        if(isset($userdata['university_id']))
        {
            $universityId = $userdata['id'];
            $universityDetail = $this->university_model->getUniversityById($userdata['id']);
            $timezone = isset($universityDetail[0]['timezone']) ? $universityDetail[0]['timezone'] : '';
        }
        else
        {
            $timezone = 5.5;
        }
        //$localTimezone = 5 - (int)$timezone;

        $allSessions = $this->user_model->getUnassignedSlotByUniversity($universityId);
        if($allSessions)
        {
            foreach($allSessions as $index => $session)
            {
                $localTimezone = $session->timezone ? 5.5 - (float)$session->timezone : 5.5 - (float)$timezone;
                $session->slot_1 = $session->slot_1 ? date('Y-m-d H:i:s', strtotime($session->slot_1) - $localTimezone * 60 * 60) : '';
                $session->slot_2 = $session->slot_2 ? date('Y-m-d H:i:s', strtotime($session->slot_2) - $localTimezone * 60 * 60) : '';
                $session->slot_3 = $session->slot_3 ? date('Y-m-d H:i:s', strtotime($session->slot_3) - $localTimezone * 60 * 60) : '';
                $allSessions[$index] = $session;
            }
        }
        $this->outputData['sessions'] = $allSessions;
        $this->outputData['user_detail'] = $userdata;
        $this->outputData['session_page'] = 'Unassigned Sessions';
        $this->outputData['redirect_url'] = '/user/session/unassigned';
        //$this->outputData['timezone'] = $localTimezone;
        $this->render_page('templates/user/session', $this->outputData);
    }

    function unconfirmed()
    {
        $userdata=$this->session->userdata('user');

        if(empty($userdata)){  redirect('common/login'); }
        $universityId = 0;
        if(isset($userdata['university_id']))
        {
            $universityId = $userdata['id'];
            $universityDetail = $this->university_model->getUniversityById($userdata['id']);
            $timezone = isset($universityDetail[0]['timezone']) ? $universityDetail[0]['timezone'] : '';
        }
        else
        {
            $timezone = 5.5;
        }
        //$localTimezone = 5 - (int)$timezone;

        $allSessions = $this->user_model->getUnconfirmedSlotByUniversity($universityId);
        if($allSessions)
        {
            foreach($allSessions as $index => $session)
            {
                $localTimezone = $session->timezone ? 5.5 - (float)$session->timezone : 5.5 - (float)$timezone;
                $session->slot_1 = $session->slot_1 ? date('Y-m-d H:i:s', strtotime($session->slot_1) - $localTimezone * 60 * 60) : '';
                $session->slot_2 = $session->slot_2 ? date('Y-m-d H:i:s', strtotime($session->slot_2) - $localTimezone * 60 * 60) : '';
                $session->slot_3 = $session->slot_3 ? date('Y-m-d H:i:s', strtotime($session->slot_3) - $localTimezone * 60 * 60) : '';
                $allSessions[$index] = $session;
            }
        }
        $this->outputData['sessions'] = $allSessions;
        $this->outputData['session_page'] = 'Unconfirmed Sessions';
        $this->outputData['redirect_url'] = '/user/session/unconfirmed';
        $this->outputData['user_detail'] = $userdata;
        //$this->outputData['timezone'] = $localTimezone;
        $this->render_page('templates/user/session', $this->outputData);
    }

    function predefineslots(){

      $userdata=$this->session->userdata('user');

      if(empty($userdata)){  redirect('common/login'); }

      $universityId = 0;
      if(isset($userdata['university_id']))
      {
          $universityId = $userdata['id'];
      }

      $preDefineListObj = $this->user_model->preDefineList($universityId);

      $this->outputData['predefined_slots'] = $preDefineListObj;
      $this->outputData['user_detail'] = $userdata;

      //var_dump($preDefineListObj);
      //die();

      $this->render_page('templates/user/predefine_slots', $this->outputData);

    }

    function generate_url()
    {
        $participants = array(
            'slot_id' => $_GET['id'],
            'user_id' => $_GET['user_id'],
            'university_id' => $_GET['university_id']
        );
        $json_participant = json_encode($participants);

        $tokenDetail = $this->tokbox_model->getTokenByParticipant($json_participant);

        if($tokenDetail)
        {
            if($tokenDetail->session_id)
            {
                $opentok = new OpenTok('46220952', '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2');
                $sessionId = $tokenDetail->session_id;
                $tokenId = $tokenDetail->id;
                $token = $opentok->generateToken($sessionId);
                $tokbox_data['token'] = $token;
                $currentTime = date('Y-m-d H:i:s');
                $expiry = strtotime($currentTime) + 3600;
                $expiryTime = date('Y-m-d H:i:s', $expiry);
                $tokbox_data['expiry_time'] = $expiryTime;
                $this->tokbox_model->editToken($tokbox_data, $tokenId);

                $studentId = $_GET['user_id'];
                $universityLoginId = $_GET['university_login_id'];

                $condition = array('user_master.id' => $studentId);
                $studentDetail = $this->user_model->getUserByid($condition);

                $condition = array('user_master.id' => $universityLoginId);
                $universityDetail = $this->user_model->getUserByid($condition);

                $universityEmail = $universityDetail->email;
                $universityName = $universityDetail->name;

                $studentName = $studentDetail->name;
                $studentEmail = $studentDetail->email;

                /*echo "Student Mail - " . $studentEmail . "\n";
                echo "University Mail - " . $universityEmail . "\n";

                $studentEmail = 'nikhil.gupta@issc.in';
                $universityEmail = 'nikhil.gupta@blocmatrix.com';*/

                $ccMailList = '';
                $mailAttachments = '';

                $encodedTokboxId = base64_encode($tokenId);
                $encodedUniversityId = base64_encode($universityLoginId);

                $mailSubject = "Conference URL For One To One With $universityName";
                $mailTemplate = "Hi $studentName

                                    Please click on the following URL to join the conference with $universityName

                                    <a href='" . str_replace('/admin', '', base_url()) . "/meeting/oneonone?id=" . $encodedTokboxId . "'>" . str_replace('/admin', '', base_url()) . "/meeting/oneonone?id=" . $encodedTokboxId . "</a>

                                    <b>Thanks and Regards,
                                    HelloUni Coordinator
                                    +91 81049 09690</b>

                                    <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

                $sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

                $mailSubject = "Conference URL For One To One With $studentName";
                $mailTemplate = "Hi $universityName,

                                Please click on the following URL to join the conference with $studentName

                                <a href='" . str_replace('/admin', '', base_url()) . "/meeting/oneonone/panelist?id=" . $encodedTokboxId . "'>" . str_replace('/admin', '', base_url()) . "/meeting/oneonone/panelist?id=" . $encodedTokboxId . "</a>

                                <b>Thanks and Regards,
                                HelloUni Coordinator
                                +91 81049 09690</b>

                                <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

                $sendMail = sMail($universityEmail, $universityName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

                echo '<script type="text/javascript">alert("Conference url has been generated successfully."); window.location.href="/admin/user/session/coming"</script>';
            }
        }
    }

    function pdf()
    {
        $slot_id = $this->input->get('sid');
        $slot_detail = $this->user_model->getSlotById($slot_id);
        if(!$slot_detail)
        {
            redirect('/');
        }
        $universityId = $slot_detail[0]->university_id;
        $consentData = $this->user_model->getUniversityConsentByid($universityId);
        $consent = $consentData->consent_data;

        $studentName = $slot_detail[0]->name;
        $date = date('Y-m-d', strtotime($slot_detail[0]->date_created));
        $universityName = $consentData->name;
        $countryName = $consentData->country_name;
        $sessionType = 'One On One Session';
        $studentDOB = "1985-09-09";
        $passport = "P1234B";
        $resident = "Hasmathpet";
        $program = "Compuer Science";
        $intake = "Jan-Jul";
        $studentEmail = "nik@abc.com";
        $studentMobile = "923123123";

        for($i = strlen($studentName); $i < 80; $i++){
            $studentName .= '&nbsp;';
        }
        for($i = strlen($studentDOB); $i < 40; $i++){
            $studentDOB .= '&nbsp;';
        }
        for($i = strlen($passport); $i < 65; $i++){
            $passport .= '&nbsp;';
        }
        for($i = strlen($resident); $i < 50; $i++){
            $resident .= '&nbsp;';
        }
        for($i = strlen($program); $i < 70; $i++){
            $program .= '&nbsp;';
        }
        for($i = strlen($intake); $i < 50; $i++){
            $intake .= '&nbsp;';
        }
        for($i = strlen($universityName); $i < 70; $i++){
            $universityName .= '&nbsp;';
        }
        for($i = strlen($studentEmail); $i < 70; $i++){
            $studentEmail .= '&nbsp;';
        }
        for($i = strlen($studentMobile); $i < 70; $i++){
            $studentMobile .= '&nbsp;';
        }
        for($i = strlen($date); $i < 40; $i++){
            $date .= '&nbsp;';
        }

        $path = APPPATH . '/../b46e9c18-c34b-11ea-8b25-0cc47a792c0a_id_b46e9c18-c34b-11ea-8b25-0cc47a792c0a_files/background2.jpg';
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $imageData = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($imageData);
        $backgroundImage = '<img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $base64) . '">';

        $img = '<img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $slot_detail[0]->student_signature) . '">';

        $consent = str_replace('<img src="b46e9c18-c34b-11ea-8b25-0cc47a792c0a_id_b46e9c18-c34b-11ea-8b25-0cc47a792c0a_files/background2.jpg" style="width:110%">', $backgroundImage, $consent);
        $consent = str_replace('<span id="student_name_1"></span>', '<span id="student_name_1"><u>' . $studentName . '</u></span>', $consent);
        $consent = str_replace('<span id="student_name_2"></span>', '<span id="student_name_2"><u>' . $studentName . '</u></span>', $consent);
        $consent = str_replace('<span id="dob"></span>', '<span id="dob"><u>' . $studentDOB . '</u></span>', $consent);
        $consent = str_replace('<span id="passport"></span>', '<span id="passport"><u>' . $passport . '</u></span>', $consent);
        $consent = str_replace('<span id="resident"></span>', '<span id="resident"><u>' . $resident . '</u></span>', $consent);
        $consent = str_replace('<span id="program"></span>', '<span id="program"><u>' . $program . '</u></span>', $consent);
        $consent = str_replace('<span id="intake"></span>', '<span id="intake"><u>' . $intake . '</u></span>', $consent);
        $consent = str_replace('<span id="university"></span>', '<span id="university"><u>' . $universityName . '</u></span>', $consent);
        $consent = str_replace('<span id="email"></span>', '<span id="email"><u>' . $studentEmail . '</u></span>', $consent);
        $consent = str_replace('<span id="mobile"></span>', '<span id="mobile"><u>' . $studentMobile . '</u></span>', $consent);
        $consent = str_replace('<span id="date"></span>', '<span id="date"><u>' . $date . '</u></span>', $consent);

        $consent = str_replace('<div style="position:absolute;left:11.80px;top:645.66px" class="cls_005"><span class="cls_005"><div id="signature-box" class="wrapper"><textarea class="form-control" rows="4" cols="50" name="comment" disabled>Your signature here...</textarea><canvas id="signature-pad" class="signature-pad" width=325 height=75></canvas></div></span></div>', '<div style="position:absolute;left:11.80px;top:645.66px" class="cls_005"><span class="cls_005">' . $img . '</span></div>', $consent);
        $consent = str_replace('<div style="position:absolute;left:415.80px;top:645.66px" class="cls_005"><span class="cls_005"><span class="cls_005"><b> OR </b></span></span></div>', '', $consent);
        $consent = str_replace('<div style="position:absolute;left:600px;top:645.66px" class="cls_005"><span class="cls_005"><div id="upload-signature-box" class="wrapper"><form name="upload-signature" enctype="multipart/form-data" method="post"><input type="file" name="signature" id="upload-signature"><input type="submit" value="Upload Signature" name="submit" style="margin-top: 10px;"></form></div></span></div>', '', $consent);
        $this->load->helper('pdf_helper');

        $data['studentName'] = $studentName;
        $data['dob'] = $studentDOB;
        $data['passport'] = $passport;
        $data['resident'] = $resident;
        $data['program'] = $program;
        $data['intake'] = $intake;
        $data['email'] = $studentEmail;
        $data['mobile'] = $studentMobile;
        $data['universityName'] = $universityName;
        $data['date'] = $date;

        $data['signature'] = $slot_detail[0]->student_signature;
        $data['content'] = $consent;

        $this->load->view('templates/user/consent', $data);
    }

    function recording()
    {
        $archieveId = $this->input->get('aid');
        $bucketUrl = "https://hellouni.s3.ap-south-1.amazonaws.com";
        $tokboxPartnerId = "46220952";
        $file = "archive.mp4";
        $this->outputData['source_url'] = $bucketUrl . "/" . $tokboxPartnerId . "/" . $archieveId . "/" . $file;
        $this->render_page('templates/user/recording', $this->outputData);
    }
}
