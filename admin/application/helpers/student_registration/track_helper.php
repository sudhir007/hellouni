<?php
function track_user(){
    $clientIp = get_client_ip();
    $currentTime = new DateTime();
    $todayDate = $currentTime->format('Y-m-d');
    $universityId = -1;
    $uriString = uri_string();
    if($uriString && strpos($uriString, 'university/details/index') !== FALSE){
        $explodeUri = explode("/", $uriString);
        $universityId = $explodeUri[3];
    }

    $ci =& get_instance();
    $ci->db->from('visitors');
    $ci->db->where('ip', $clientIp);
    $ci->db->where('date', $todayDate);
    $ci->db->where('university_id', $universityId);
    $query = $ci->db->get();
    $result = $query->row();
    if(!$result){
        $data['ip'] = $clientIp;
        $data['date'] = $todayDate;
        $data['university_id'] = $universityId;
        $ci->db->insert('visitors', $data);
    }
}

function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
?>
