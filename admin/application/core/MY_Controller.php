<?php

class MY_Controller extends CI_Controller {

  protected $data = array();

  function __construct() {
    parent::__construct();
  }

  function render_page($view) {
   
   
    $userdata=$this->session->userdata('user');
	//print_r($userdata); exit;
	if($userdata != null && $userdata['type']!=5){ 
	
	
	$this->outputData['user'] = $userdata;
	
	
    $this->load->view('templates/common/header', $this->outputData);
    $this->load->view('templates/common/sidebar', $this->data);
    $this->load->view($view, $this->data);
    $this->load->view('templates/common/footer', $this->data);
   }else{
   
   redirect('common/external');
   }
  }

}  // end-class MY_Controller
//...


?>