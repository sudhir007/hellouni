<style>
    .css-display-box, .save{
        display: none;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>University<small>Sliders</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="#">Slider Approval</a></li>
            <li class="active">Slider</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>

                <div class="box">
                    <form action="<?php echo base_url();?>user/counselor/multidelete" method="post">
                        <div class="box-body">
                            <table id="newAppExample" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Sl No</th>
                                        <th>University Name</th>
                                        <th>Style</th>
                                        <th>Preview</th>
                                        <th>Option</th>
                                        <th>Approve</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- <?php $i=1;
                                    foreach($all_sliders as $usr)
                                    {
                                        ?>
                                        <tr>
                                            <td><?php echo $i++;?></td>
                                            <td><?php echo $usr->name;?></td>
                                            <td class="css-display" id="css-<?=$usr->id?>"><?php echo $usr->slider_css;?></td>
                                            <td class="css-display-box" id="css-box-<?=$usr->id?>"><input type="text" id="css-input-<?=$usr->id?>" name="slider_css" value='<?php echo $usr->slider_css;?>'></td>
                                            <td><a href="/?preview=<?=$usr->id?>" target="_blank">Preview</a></td>
                                            <td>
                                                <a href="javascript::void();" title="Modify" class="edit" id="edit-<?php echo $usr->id;?>" onclick="editStyle('<?=$usr->id?>')"><i class="fa fa-edit"></i></a>
                                                <a href="javascript::void();" title="Save" class="save" id="save-<?php echo $usr->id;?>" onclick="saveStyle('<?=$usr->id?>')"><i class="fa fa-save"></i></a>
                                            </td>
                                            <td><input type="checkbox" class="multichk" name="chk[]" id="css-checkbox-<?php echo $usr->id;?>" onclick="approveSlider('<?=$usr->id?>')"></td>
                                        </tr>
                                        <?php
                                    }
                                    ?> -->
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Sl No</th>
                                        <th>University Name</th>
                                        <th>Style</th>
                                        <th>Preview</th>
                                        <th>Option</th>
                                        <th>Approve</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
    function editStyle(universityId)
    {
        document.getElementById("css-" + universityId).style.display = "none";
        document.getElementById("css-box-" + universityId).style.display = "block";

        document.getElementById("edit-" + universityId).style.display = "none";
        document.getElementById("save-" + universityId).style.display = "block";
    }

    function saveStyle(universityId)
    {
        var sliderCss = document.getElementById("css-input-" + universityId).value;
        $.ajax({
            type: "POST",
            url: "/admin/user/university/sliderCss",
            data: {"university_id": universityId, "slider_css": sliderCss},
            cache: false,
            success: function(result){
                document.getElementById("css-" + universityId).innerHTML = sliderCss;
                document.getElementById("css-" + universityId).style.display = "block";
                document.getElementById("css-box-" + universityId).style.display = "none";

                document.getElementById("edit-" + universityId).style.display = "block";
                document.getElementById("save-" + universityId).style.display = "none";
            }
        });
    }

    function approveSlider(universityId)
    {
        var checked = 0;
        if($("#css-checkbox-" + universityId).prop('checked') == true){
            checked = 1;
        }

        $.ajax({
            type: "POST",
            url: "/admin/user/university/sliderApprove",
            data: {"university_id": universityId, "slider_approved": checked},
            cache: false,
            success: function(result){
                alert("Success");
            }
        });
    }
</script>

<script>
var table;
  $(document).ready(function() {

    //datatables
    table = $('#newAppExample').DataTable({ 
//alert('hi');
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url();?>user/university/ajax_manage_page_slider",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
                { "data": "id" },
                { "data": "university_name" },
                { "data": "Style" },
                { "data": "Preview" },
                { "data": "Option" },
                { "data": "Approve" }
            ],
        

    });

  });
  
</script>
