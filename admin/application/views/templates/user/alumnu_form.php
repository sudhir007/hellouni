

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            University
            <small>Manage University</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">User Management</a></li>
            <?php
            if(isset($edit))
            {
                ?>
                <li class="active">Edit Alumnu</li>
                <?php
            }
            else
            {
                ?>
                <li class="active">Add Alumnu</li>
                <?php
            }
            ?>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">

            <!-- right column -->
            <div class="col-md-10">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <?php
                  if(isset($edit))
                  {
                      ?>
                      <h3 class="box-title">Edit Alumnu</h3>
                      <?php
                  }
                  else
                  {
                      ?>
                      <h3 class="box-title">Add Alumnu</h3>
                      <?php
                  }
                  ?>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php if($this->session->flashdata('flash_message')){ ?>
  				<div class="alert alert-error alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                  <h4><i class="icon fa fa-circle-o"></i> Error</h4>
                  <?php echo $this->session->flashdata('flash_message'); ?>
                </div>
  			  <?php } ?>

				 <?php if($this->uri->segment('4')){?>
				<form class="form-horizontal" action="<?php echo base_url();?>user/alumnus/edit/<?php if(isset($id)) echo $id;?>" method="post" name="addadmin" autocomplete="off">
				<?php }else{?>
                <form class="form-horizontal" action="<?php echo base_url();?>user/alumnus/create" method="post" name="adduni" autocomplete="off">
                <?php } ?>



                  <div class="box-body">
                      <?php
                      if(isset($universities))
                      {
                          ?>
                          <div class="form-group">
                              <label for="university" class="col-sm-2 control-label">University</label>
                              <div class="col-sm-10">
                                  <select class="form-control" name="university_id">
                                      <?php
                                      foreach($universities as $universityData)
                                      {
                                          ?>
                                          <option value="<?=$universityData->id?>" <?php if(isset($university) && $universityData->id==$university){ echo 'selected="selected"'; } ?>><?=$universityData->name?></option>
                                          <?php
                                      }
                                      ?>
                                  </select>
                              </div>
                          </div>
                          <?php
                      }
                      ?>
                    <div class="form-group">
                      <label for="name" class="col-sm-2 control-label">Name *</label>
                      <div class="col-sm-10">
                        <input type="name" class="form-control" id="name" name="name" placeholder="Alumnu Name" value="<?php if(isset($name)) echo $name; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="username" class="col-sm-2 control-label">Username *</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="<?php if(isset($username)) echo $username; ?>" <?php if(isset($username)) echo "disabled" ?>>
                      </div>
                    </div>

					<div class="form-group">
                      <label for="email" class="col-sm-2 control-label">Email *</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Alumnu Email" value="<?php if(isset($email)) echo $email; ?>">
                      </div>
                    </div>

                    <?php
                    if(!isset($edit))
                    {
                        ?>
                        <div class="form-group">
                          <label for="password" class="col-sm-2 control-label">Password *</label>
                          <div class="col-sm-10">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Alumnu Password">
                          </div>
                        </div>
                        <?php
                    }
                    ?>

					<div class="form-group">
                  		 <label for="status" class="col-sm-2 control-label">Status</label>
						  <div class="col-sm-10">
						  <select class="form-control" name="status">
							<option value="1" <?php if(isset($status) && $status=='1'){ echo 'selected="selected"'; } ?>>Active</option>
							<option value="2" <?php if(isset($status) && $status=='2'){ echo 'selected="selected"'; } ?>>Inactive</option>
						  </select>
						  </div>
               		</div>

                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
					<button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                    <?php
                    if($this->uri->segment('4'))
                    {
                        ?>
                        <button type="submit" class="btn btn-info pull-right">Update</button>
                        <input type="hidden" name="user_id" value="<?=$user_id?>">
                        <?php
                    }
                    else
                    {
                        ?>
                        <button type="submit" class="btn btn-info pull-right">Create</button>
                        <?php
                    }
                    ?>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              <!-- general form elements disabled -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
