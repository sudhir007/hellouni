<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      University
      <small>Manage University</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">University Management</a></li>
      <li class="active">Add University</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">

      <!-- right column -->
      <div class="col-md-10">
        <!-- Horizontal Form -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Add University</h3>
          </div><!-- /.box-header -->
          <!-- form start -->

          <?php if ($this->session->flashdata('flash_message')) { ?>
            <div class="alert alert-error alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
              <h4><i class="icon fa fa-circle-o"></i> Error</h4>
              <?php echo $this->session->flashdata('flash_message'); ?>
            </div>
          <?php } ?>
          <?php if ($this->uri->segment('4')) { ?>
            <form class="form-horizontal" action="<?php echo base_url(); ?>user/university/edit/<?php if (isset($id)) echo $id; ?>" method="post" name="addadmin" autocomplete="off">
            <?php } else { ?>
              <form class="form-horizontal" action="<?php echo base_url(); ?>user/university/create" method="post" name="adduni" autocomplete="off">
              <?php } ?>



              <div class="box-body">
                <div class="form-group">
                  <label for="affiliation" class="col-sm-2 control-label">Affiliation Type *</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="affiliation">
                      <option>Select</option>
                      <?php
                      foreach ($affiliations as $affiliationData) {
                      ?>
                        <option value="<?= $affiliationData->id ?>" <?php if (isset($affiliated) && $affiliationData->id == $affiliated) {
                                                                      echo 'selected="selected"';
                                                                    } ?>><?= $affiliationData->name ?></option>
                      <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="Territory" class="col-sm-2 control-label">Territory *</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="territory">
                      <option>Select Territory</option>
                      <option value="Global" <?php echo (isset($territory) && $territory == 'Global') ? 'selected' : ''; ?>>Global</option>
                      <option value="India Only" <?php echo (isset($territory) && $territory == 'India Only') ? 'selected' : ''; ?>>India Only</option>
                      <option value="South Asia Only" <?php echo (isset($territory) && $territory == 'South Asia Only') ? 'selected' : ''; ?>>South Asia Only</option>
                      <option value="India & UAE" <?php echo (isset($territory) && $territory == 'India & UAE') ? 'selected' : ''; ?>>India & UAE</option>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="country" class="col-sm-2 control-label">Country *</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="country" id="country">
                      <option>Select</option>
                      <?php
                      foreach ($countries as $countryData) {
                      ?>
                        <option value="<?= $countryData->id ?>" <?php if (isset($country) && $countryData->id == $country) {
                                                                  echo 'selected="selected"';
                                                                } ?>><?= $countryData->name ?></option>
                      <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="State" class="col-sm-2 control-label">State *</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="state_id" id="state_id">
                      <option>Select State</option>

                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="city" class="col-sm-2 control-label">City *</label>
                  <div class="col-sm-10">
                    <input type="name" class="form-control" id="city" name="city" placeholder="City Name" value="<?php if (isset($city)) echo $city; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Name *</label>
                  <div class="col-sm-10">
                    <input type="name" class="form-control" id="adminname" name="uniname" placeholder="University Name" value="<?php if (isset($name)) echo $name; ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Slug </label>
                  <div class="col-sm-10">
                    <input type="name" class="form-control" id="slug" name="slug" placeholder="University Slug" value="<?php if (isset($slug)) echo $slug; ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="username" class="col-sm-2 control-label">Username *</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="<?php if (isset($username)) echo $username; ?>" <?php if (isset($username)) echo "disabled" ?>>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Email *</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="email" name="email" placeholder="University Email" value="<?php if (isset($email)) echo $email; ?>">
                  </div>
                </div>

                <?php
                if (!isset($edit)) {
                ?>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Password *</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="password" name="password" placeholder="University Password">
                    </div>
                  </div>
                <?php
                }
                ?>

                <div class="form-group">
                  <label for="website" class="col-sm-2 control-label">Website</label>
                  <div class="col-sm-10">
                    <input type="textbox" class="form-control" id="website" name="website" placeholder="Website" value="<?php if (isset($website)) echo $website; ?>">
                  </div>
                </div>

                <!-- <div class="form-group">
                      <label for="description" class="col-sm-2 control-label">Description</label>
                      <div class="col-sm-10">
                        input type="textbox" class="form-control" id="about" name="about" placeholder="Breif Description"
                        <textarea rows="4" class="form-control" id="about" name="about" placeholder="Breif Description"><?php if (isset($description)) echo $description; ?></textarea>
                      </div>
                    </div> -->

                <!-- <div class="form-group">
                      <label for="establishment_year" class="col-sm-2 control-label">Establishment Year</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="establishment_year" name="establishment_year" placeholder="Establishment Year" value="<?php if (isset($establishment_year)) echo $establishment_year; ?>">
                      </div>
                    </div> -->

                <div class="form-group">
                  <label for="type" class="col-sm-2 control-label">Type</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="type">
                      <option value="University Private" <?php if (isset($university_type) && $university_type == 'University Private') {
                                                            echo 'selected="selected"';
                                                          } ?>>University Private</option>
                      <option value="University Public" <?php if (isset($university_type) && $university_type == 'University Public') {
                                                          echo 'selected="selected"';
                                                        } ?>>University Public</option>
                      <option value="University Private Not for Profit" <?php if (isset($university_type) && $university_type == 'University Private Not for Profit') {
                                                                          echo 'selected="selected"';
                                                                        } ?>>University Private Not for Profit</option>
                      <option value="College Private" <?php if (isset($university_type) && $university_type == 'College Private') {
                                                        echo 'selected="selected"';
                                                      } ?>>College Private</option>
                      <option value="College Public" <?php if (isset($university_type) && $university_type == 'College Public') {
                                                        echo 'selected="selected"';
                                                      } ?>>College Public</option>
                      <option value="College Private not for Profit" <?php if (isset($university_type) && $university_type == 'College Private not for Profit') {
                                                                        echo 'selected="selected"';
                                                                      } ?>>College Private not for Profit</option>
                      <option value="Non Degree Colleges" <?php if (isset($university_type) && $university_type == 'Non Degree Colleges') {
                                                            echo 'selected="selected"';
                                                          } ?>>Non Degree Colleges</option>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="carnegie_accreditation" class="col-sm-2 control-label">Carnegie Accreditation</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="carnegie_accreditation" name="carnegie_accreditation" placeholder="Carnegie Accreditation" value="<?php if (isset($carnegie_accreditation)) echo $carnegie_accreditation; ?>">
                  </div>
                </div>

                <!-- <div class="form-group">
                      <label for="total_students" class="col-sm-2 control-label">Total Students</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="total_students" name="total_students" placeholder="Total Students" value="<?php if (isset($total_students)) echo $total_students; ?>">
                      </div>
                    </div> -->

                <!-- <div class="form-group">
                      <label for="total_students_UG" class="col-sm-2 control-label">Total Students UG</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="total_students_UG" name="total_students_UG" placeholder="Total Students UG" value="<?php if (isset($total_students_UG)) echo $total_students_UG; ?>">
                      </div>
                    </div> -->

                <!-- <div class="form-group">
                      <label for="total_students_PG" class="col-sm-2 control-label">Total Students PG</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="total_students_PG" name="total_students_PG" placeholder="Total Students PG" value="<?php if (isset($total_students_PG)) echo $total_students_PG; ?>">
                      </div>
                    </div> -->

                <!-- <div class="form-group">
                      <label for="total_students_international" class="col-sm-2 control-label">Total Students International</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="total_students_international" name="total_students_international" placeholder="Total Students International" value="<?php if (isset($total_students_international)) echo $total_students_international; ?>">
                      </div>
                    </div> -->

                <!-- <div class="form-group">
                      <label for="ranking_usa" class="col-sm-2 control-label">USA Ranking (National Universities)</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="ranking_usa" name="ranking_usa" placeholder="USA Ranking" value="<?php if (isset($ranking_usa)) echo $ranking_usa; ?>">
                      </div>
                    </div> -->

                <!-- <div class="form-group">
                      <label for="national_ranking_source" class="col-sm-2 control-label">National Ranking Source</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="national_ranking_source" name="national_ranking_source" placeholder="National Ranking Source" value="<?php if (isset($national_ranking_source)) echo $national_ranking_source; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="ranking_world" class="col-sm-2 control-label">Wordwide Ranking</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="ranking_world" name="ranking_world" placeholder="Wordwide Ranking" value="<?php if (isset($ranking_world)) echo $ranking_world; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="worldwide_ranking_source" class="col-sm-2 control-label">Worldwide Ranking Source</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="worldwide_ranking_source" name="worldwide_ranking_source" placeholder="Worldwide Ranking Source" value="<?php if (isset($worldwide_ranking_source)) echo $worldwide_ranking_source; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="admission_success_rate" class="col-sm-2 control-label">Admission Success Rate (%)</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="admission_success_rate" name="admission_success_rate" placeholder="Admission Success Rate" value="<?php if (isset($admission_success_rate)) echo $admission_success_rate; ?>">
                      </div>
                    </div> -->

                <!-- <div class="form-group">
                      <label for="unique_selling_points" class="col-sm-2 control-label">Unique Selling Points</label>
                      <div class="col-sm-10" id="all_usp">
                          <?php
                          if ($total_unique_selling_points) {
                            foreach ($unique_selling_points as $index => $unique_selling_point) {
                          ?>
                                  <div style="margin-top: 10px;" id="unique_selling_point_<?= $index ?>">
                                      <div class="col-sm-8">
                                          <textarea rows="2" class="form-control col-sm-8" name="unique_selling_points[]" placeholder="Unique Selling Point"><?= $unique_selling_point ?></textarea>
                                      </div>
                                      <?php
                                      if (!$index) {
                                      ?>
                                          <div class="col-sm-4"><input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addUSP()"></div>
                                          <?php
                                        } else {
                                          ?>
                                          <div class="col-sm-4"><input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteUSP(<?= $index ?>)"></div>
                                          <?php
                                        }
                                          ?>
                                  </div>
                                  <?php
                                }
                              } else {
                                  ?>
                              <div style="margin-top: 10px;" id="unique_selling_point_0">
                                  <div class="col-sm-8">
                                      <textarea rows="2" class="form-control col-sm-8" name="unique_selling_points[]" placeholder="Unique Selling Point"></textarea>
                                  </div>
                                  <div class="col-sm-4"><input type="button" name="add" value="Add" style="margin-left: 20px; margin-top: 10px;" onclick="addUSP()"></div>
                              </div>
                              <?php
                              }
                              ?>
                      </div>
                    </div> -->

                <div class="form-group">
                  <label for="research_spending" class="col-sm-2 control-label">Research Spending (In Million)</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="research_spending" name="research_spending" placeholder="Research Spending" value="<?php if (isset($research_spending)) echo $research_spending; ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="placement_percentage" class="col-sm-2 control-label">Placement (%)</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="placement_percentage" name="placement_percentage" placeholder="Placement" value="<?php if (isset($placement_percentage)) echo $placement_percentage; ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="placement_percentage" class="col-sm-2 control-label">Map Normal View Code</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="map_normal_view" name="map_normal_view" placeholder="Map Normal View Code" value="<?php if (isset($map_normal_view)) echo $map_normal_view; ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="placement_percentage" class="col-sm-2 control-label">Map Street View Code</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="map_street_view" name="map_street_view" placeholder="Map Street View Code" value="<?php if (isset($map_street_view)) echo $map_street_view; ?>">
                  </div>
                </div>

                <!-- <div class="form-group">
                      <label for="placement_percentage" class="col-sm-2 control-label">WhatsApp Group Link</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="whatsapp_link" name="whatsapp_link" placeholder="WhatsApp Group Link Code" value="<?php if (isset($whatsapp_link)) echo $whatsapp_link; ?>">
                      </div>
                    </div> -->

                <div class="form-group">
                  <label for="status" class="col-sm-2 control-label">Status</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="status">
                      <option value="1" <?php if (isset($status) && $status == '1') {
                                          echo 'selected="selected"';
                                        } ?>>Active</option>
                      <option value="2" <?php if (isset($status) && $status == '2') {
                                          echo 'selected="selected"';
                                        } ?>>Inactive</option>
                    </select>
                  </div>
                </div>


                <style type="text/css">
                  .chkbx {
                    float: left;
                    margin: 8px 23px 1px 0px;
                  }
                </style>
                <div class="form-group">
                  <label for="permission" class="col-sm-2 control-label">Permission</label>
                  <?php //echo $create.$view.$edit.$del; 
                  ?>
                  <div class="col-sm-10">
                    <div class="chkbx"><label>Course</label></div>
                    <div class="chkbx"><input type="checkbox" name="creation" value="1" <?php if (isset($create) && $create == true) { ?> checked="checked" <?php } ?>> Create</div>
                    <div class="chkbx"><input type="checkbox" name="view" value="1" <?php if (isset($view) && $view == true) { ?> checked="checked" <?php } ?>> View</div>
                    <div class="chkbx"><input type="checkbox" name="edit" value="1" <?php if (isset($edit) && $edit == true) { ?> checked="checked" <?php } ?>> Edit</div>
                    <div class="chkbx"> <input type="checkbox" name="deletion" value="1" <?php if (isset($del) && $del == true) { ?> checked="checked" <?php } ?>> Delete</div>

                  </div>
                </div>

                <div class="form-group">
                  <label for="status" class="col-sm-2 control-label">Notification</label>
                  <div class="col-sm-10">
                    <div class="radio">
                      <label>
                        <input type="radio" name="notification" id="notify_yes" value="1" checked="<?php if (isset($notification) == 1) {
                                                                                                      echo 'checked';
                                                                                                    } ?>">Yes (Send email on <?php if (!$this->uri->segment('4')) { ?>creation<?php } else { ?>modification <?php } ?>)

                      </label>
                    </div>
                    <div class="radio">
                      <label>

                        <input type="radio" name="notification" id="notify_no" value="0" checked="<?php if (isset($notification) == 0) {
                                                                                                    echo 'checked';
                                                                                                  } ?>">No (Don't send email on<?php if (!$this->uri->segment('4')) { ?> creation<?php } else { ?> modification <?php } ?>)
                      </label>
                    </div>
                  </div>
                </div>

              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                <?php
                if ($this->uri->segment('4')) {
                ?>
                  <button type="submit" class="btn btn-info pull-right">Update</button>
                <?php
                } else {
                ?>
                  <button type="submit" class="btn btn-info pull-right">Create</button>
                <?php
                }
                ?>
              </div><!-- /.box-footer -->
              </form>
        </div><!-- /.box -->
        <!-- general form elements disabled -->

      </div><!--/.col (right) -->
    </div> <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
  var totalUniqueSellingPoints = '<?= $total_unique_selling_points ?>';
  totalUniqueSellingPoints--;

  function addUSP() {
    var html = '<div id="unique_selling_point_' + (totalUniqueSellingPoints + 1) + '" style="margin-top:10px;"><div class="col-sm-8"><textarea rows="2" class="form-control col-sm-8" name="unique_selling_points[]" placeholder="Unique Selling Point"></textarea></div><div class="col-sm-4"><input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteUSP(' + (totalUniqueSellingPoints + 1) + ')"></div></div>';

    $("#unique_selling_point_" + totalUniqueSellingPoints).after(html);
    totalUniqueSellingPoints++;
    console.log(totalUniqueSellingPoints);
  }

  function deleteUSP(sellingPointId) {
    $("#unique_selling_point_" + sellingPointId).remove();
    totalUniqueSellingPoints--;
  }
</script>

<!-- <script type="text/javascript">
  $(document).ready(function() {
    $('#country').change(function() {
      var country_id = $('#country').val();
      if (country_id !== '') {
        $.ajax({
          url: "<?php echo base_url('user/university/getStates'); ?>",
          method: "POST",
          data: {
            country_id: country_id
          },
          dataType: "json",
          success: function(data) {
            var options = '<option value="">Select State</option>'; // Add default empty option
            $.each(data, function(index, state) {
              options += '<option value="' + state.zone_id + '">' + state.name + '</option>';
            });
            $('#state_id').html(options);
          },
          error: function() {
            $('#state_id').html('<option value="">Error loading states</option>');
          }
        });
      } else {
        $('#state_id').html('<option value="">Select State</option>');
      }
    });
  });
</script> -->

<script type="text/javascript">
  $(document).ready(function() {
    var initialCountryId = $('#country').val();
    var initialStateId = "<?php echo isset($state_id) ? $state_id : ''; ?>";
    function populateStates(country_id, selected_state_id = '') {
      if (country_id !== '') {
        $.ajax({
          url: "<?php echo base_url('user/university/getStates'); ?>",
          method: "POST",
          data: {
            country_id: country_id
          },
          dataType: "json",
          success: function(data) {
            var options = '<option value="">Select State</option>';
            $.each(data, function(index, state) {
              var selected = (state.zone_id == selected_state_id) ? 'selected="selected"' : '';
              options += '<option value="' + state.zone_id + '" ' + selected + '>' + state.name + '</option>';
            });
            $('#state_id').html(options);
          },
          error: function() {
            $('#state_id').html('<option value="">Error loading states</option>');
          }
        });
      } else {
        $('#state_id').html('<option value="">Select State</option>');
      }
    }

    
    if (initialCountryId) {
      populateStates(initialCountryId, initialStateId);
    }

    $('#country').change(function() {
      populateStates($(this).val());
    });
  });
</script>