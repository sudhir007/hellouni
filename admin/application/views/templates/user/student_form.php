

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            University
            <small>Manage University Users</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">User Management</a></li>
            <li class="active">Add University</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">

            <!-- right column -->
            <div class="col-md-10">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Add University User</h3>
                </div><!-- /.box-header -->
                <!-- form start -->

                <!--div style="font-size: 16px;font-weight: bold;text-align: center;margin-bottom: 15px;">Your provided timezone is GMT<?=$timezone?>. By default all the slots will be created in this timezone OR you can select your preferred timezone.</div-->
				 <?php if($slot_id){?>
				<form class="form-horizontal" action="<?php echo base_url();?>user/student/edit/<?php if(isset($id)) echo $id;?>" method="post" name="addadmin" autocomplete="off">
				<?php }else{?>
                <form class="form-horizontal" action="<?php echo base_url();?>user/student/create" method="post" name="adduni" autocomplete="off">
                <?php } ?>



                  <div class="box-body">

                      <div class="form-group">
                        <label for="timezone" class="col-sm-2 control-label">Timezone</label>
                        <div class="col-sm-2">
                            <select class="form-control" name="timezone">
                                <?php
                                for($i = 12; $i > 0; $i-=.5)
                                {
                                    ?>
                                    <option value="-<?=$i?>" <?php if(isset($timezone) && $timezone=="-".$i){ echo 'selected="selected"'; } ?>>GMT-<?=$i?></option>
                                    <?php
                                }
                                ?>
                                <option value="0" <?php if(isset($timezone) && $timezone==0){ echo 'selected="selected"'; } ?>>GMT+0</option>
                                <?php
                                for($i = .5; $i <= 12; $i+=.5)
                                {
                                    ?>
                                    <option value="+<?=$i?>" <?php if(isset($timezone) && $timezone=="+".$i){ echo 'selected="selected"'; } ?>>GMT+<?=$i?></option>
                                    <?php
                                }
                                 ?>
                            </select>
                        </div>
                      </div>

                      <div class="form-group">
                          <input type="hidden" name="slot_id" value="<?=$slot_id?>">
                        <label for="slot_1_date" class="col-sm-2 control-label">Slot 1 Date</label>
                        <div class="col-sm-2">
                          <input type="text" class="mws-datepicker small" id="datepicker" name="slot_1_date" value="<?php if(isset($slot_1_date)) echo $slot_1_date; ?>">
                        </div>
                        <label for="slot_1_time" class="col-sm-2 control-label">Slot 1 Time</label>
                        <div class="col-sm-4">
                            <select class="form-control small" name="slot_1_time">
                            <?php
                            foreach($time_array as $index => $time)
                            {
                                ?>
                                <option value="<?=$index?>" <?php if(isset($slot_1_time) && $slot_1_time==$time){ echo 'selected="selected"'; } ?>><?=$time?></option>
                                <?php
                            }
                            ?>
                        </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="slot_2_date" class="col-sm-2 control-label">Slot 2 Date</label>
                        <div class="col-sm-2">
                          <input type="text" class="mws-datepicker small" id="datepicker_1" name="slot_2_date" value="<?php if(isset($slot_2_date)) echo $slot_2_date; ?>">
                        </div>
                        <label for="slot_2_time" class="col-sm-2 control-label">Slot 2 Time</label>
                        <div class="col-sm-4">
                            <select class="form-control small" name="slot_2_time">
                            <?php
                            foreach($time_array as $index => $time)
                            {
                                ?>
                                <option value="<?=$index?>" <?php if(isset($slot_2_time) && $slot_2_time==$time){ echo 'selected="selected"'; } ?>><?=$time?></option>
                                <?php
                            }
                            ?>
                        </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="slot_3_date" class="col-sm-2 control-label">Slot 3 Date</label>
                        <div class="col-sm-2">
                          <input type="text" class="mws-datepicker small" id="datepicker_2" name="slot_3_date" value="<?php if(isset($slot_3_date)) echo $slot_3_date; ?>">
                        </div>
                        <label for="slot_3_time" class="col-sm-2 control-label">Slot 3 Time</label>
                        <div class="col-sm-4">
                            <select class="form-control small" name="slot_3_time">
                            <?php
                            foreach($time_array as $index => $time)
                            {
                                ?>
                                <option value="<?=$index?>" <?php if(isset($slot_3_time) && $slot_3_time==$time){ echo 'selected="selected"'; } ?>><?=$time?></option>
                                <?php
                            }
                            ?>
                        </select>
                        </div>
                      </div>






                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
					<button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                    <!--input type="hidden" name="timezone" value="<?=$timezone?>"-->
                    <button type="submit" class="btn btn-info pull-right">Create</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              <!-- general form elements disabled -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
