<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      University
      <small>Add Research</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">University Management</a></li>
      <li class="active">Add Research</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">

      <!-- right column -->
      <div class="col-md-10">
        <!-- Horizontal Form -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Add Research</h3>
          </div><!-- /.box-header -->
          <!-- form start -->

          <?php if ($this->uri->segment('4')) { ?>
            <form class="form-horizontal" action="<?php echo base_url(); ?>user/research/edit/<?php if (isset($id)) echo $id; ?>" method="post" name="addadmin" autocomplete="off">
            <?php } else { ?>
              <form class="form-horizontal" action="<?php echo base_url(); ?>user/research/create" method="post" name="adduni" autocomplete="off">
              <?php } ?>



              <div class="box-body">
                <div class="form-group">
                  <label for="university" class="col-sm-2 control-label">University</label>
                  <div class="col-sm-10">
                    <?php
                    if (!$university_id) {
                    ?>
                      <select class="form-control" name="university" id="university" onchange="getCollege();">
                        <option value="">Select University</option>
                        <?php
                        foreach ($universities as $universityData) {
                        ?>
                          <option value="<?= $universityData->id ?>" <?php if (isset($university) && $universityData->id == $university) {
                                                                        echo 'selected="selected"';
                                                                      } ?>><?= $universityData->name ?></option>
                        <?php
                        }
                        ?>
                      </select>
                    <?php
                    } else {
                    ?>
                      <select class="form-control" name="university" id="university" onchange="getCollege();" disabled>
                        <?php
                        foreach ($universities as $universityData) {
                        ?>
                          <option value="<?= $universityData->id ?>" <?php if ($universityData->id == $university_id) {
                                                                        echo 'selected="selected"';
                                                                      } ?>><?= $universityData->name ?></option>
                        <?php
                        }
                        ?>
                      </select>
                    <?php
                    }
                    ?>
                  </div>
                </div>

                <div class="form-group">
                  <label for="college" class="col-sm-2 control-label">College</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="college" id="college" onchange="getDepartments();">
                      <option value="">Select College</option>
                      <?php if (isset($colleges)) { ?>
                        <?php foreach ($colleges as $college_data) { ?>
                          <option value="<?= $college_data['id'] ?>" <?php if ($college_data['id'] == $college) {
                                                                        echo 'selected="selected"';
                                                                      } ?>><?= $college_data['name'] ?>
                          </option>
                        <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="department" class="col-sm-2 control-label">Department</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="department" id="department">
                      <option value="">Select Department</option>
                      <?php if (isset($departments)) { ?>
                        <?php foreach ($departments as $department_data) { ?>
                          <option value="<?= $department_data['id'] ?>" <?php if ($department_data['id'] == $department) {
                                                                        echo 'selected="selected"';
                                                                      } ?>><?= $department_data['name'] ?>
                          </option>
                        <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="topic" class="col-sm-2 control-label">Topic</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="topic" name="topic" placeholder="Research topic" value="<?php if (isset($topic)) echo $topic; ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="description" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                    <textarea rows="4" class="form-control" id="description" name="description" placeholder="Breif about research"><?php if (isset($description)) echo $description; ?></textarea>
                    <script type="text/javascript">
                      CKEDITOR.replace('description', {
                        extraPlugins: 'confighelper'
                      });
                    </script>
                  </div>
                </div>

                <div class="form-group">
                  <label for="research_url" class="col-sm-2 control-label">Research URL</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="research_url" name="research_url" placeholder="Research URL" value="<?php if (isset($research_url)) echo $research_url; ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="faculty_url" class="col-sm-2 control-label">Faculty URL</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="faculty_url" name="faculty_url" placeholder="Faculty URL" value="<?php if (isset($faculty_url)) echo $faculty_url; ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="funding" class="col-sm-2 control-label">Funding</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="funding" name="funding" placeholder="Funding" value="<?php if (isset($funding)) echo $funding; ?>">
                  </div>
                </div>

              </div><!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                <?php
                if ($this->uri->segment('4')) {
                ?>
                  <button type="submit" class="btn btn-info pull-right">Update</button>
                <?php
                } else {
                ?>
                  <button type="submit" class="btn btn-info pull-right">Create</button>
                <?php
                }
                ?>
              </div><!-- /.box-footer -->
              </form>
        </div><!-- /.box -->
        <!-- general form elements disabled -->

      </div><!--/.col (right) -->
    </div> <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
  
  function getCollege() {
    var universityId = $('#university').val();
    var dataString = "university_id=" + universityId;

    $.ajax({
      type: "POST",
      url: "<?php echo base_url('user/research/getCollege'); ?>",
      data: dataString,
      cache: false,
      success: function(result) {
        var collegeList = result.college_list || [];
        var collegeDropDown = "<option value=''>Select College</option>";

        collegeList.forEach(function(value) {
          collegeDropDown += "<option value='" + value.id + "'>" + value.name + "</option>";
        });

        $('#college').html(collegeDropDown);
        $('#department').html("<option value=''>Select Department</option>");
      }
    });
  }

  function getDepartments() {
    var collegeId = $('#college').val();
    var dataString = "college=" + collegeId;

    $.ajax({
      type: "POST",
      url: "<?php echo base_url('user/research/getDepartments'); ?>",
      data: dataString,
      cache: false,
      success: function(result) {
        var departmentList = result.department_list || [];
        var departmentDropDown = "<option value=''>Select Department</option>";

        departmentList.forEach(function(value) {
          departmentDropDown += "<option value='" + value.id + "'>" + value.name + "</option>";
        });

        $('#department').html(departmentDropDown);
      }
    });
  }
</script>