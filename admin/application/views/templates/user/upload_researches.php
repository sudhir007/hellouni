<div class="content-wrapper">
    <section class="content-header">
        <h1>University<small>Upload</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="#">University Management</a></li>
            <li class="active">Upload</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-10">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Upload File</h3>
                    </div>
                    <?php echo $error = isset($error) ? $error : '';?>
                    <?php echo form_open_multipart(base_url() . 'user/upload/upload_researches');?>
                    <form class="form-horizontal" action = "" method = "" name="upload_file" autocomplete="off">
                        <div class="box-body">
                            <div class="form-group">
                                <input type = "file" name = "userfile" size = "20" />
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <input type = "submit" value = "Upload" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
