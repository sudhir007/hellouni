<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<!-- Include jQuery UI CSS and JS -->
<link rel="stylesheet" href="https://code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">


<style>
  #ajaxSpinnerImage {
    display: none;
  }

  #ajaxSpinnerContainer {
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .modal-dialog {
    width: 690px;
    margin: 5% auto;
  }

  .modal-content {
    max-height: 545px;
    overflow: auto;
  }

  .select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #3c8dbc;
    padding: 0px;
    padding-left: 20px;
  }

  .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
    background-color: transparent;
    color: #ffffff;
  }
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      University
      <small>Add Course</small>
    </h1>

    <div id="ajaxSpinnerContainer">
      <img src="<?php echo base_url(); ?>images/ajax-loader.gif" id="ajaxSpinnerImage" title="working..." />
    </div>

    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">University Management</a></li>
      <li class="active">Add Course</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">

      <!-- right column -->
      <div class="col-md-10">
        <!-- Horizontal Form -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Add Course</h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          <?php if ($this->session->flashdata('flash_message')) { ?>
            <div class="alert alert-info alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
              <h4><i class="icon fa fa-circle-o"></i> Alert Msg : </h4>
              <?php echo $this->session->flashdata('flash_message'); ?>
            </div>
          <?php } ?>

          <?php if ($this->uri->segment('4')) { ?>
            <form autocomplete="off" class="form-horizontal" action="<?php echo base_url(); ?>user/course/edit/<?php if (isset($id)) echo $id; ?>/<?= $filter_level ?>" method="post" name="addadmin">
            <?php } else { ?>
              <form autocomplete="off" class="form-horizontal" action="<?php echo base_url(); ?>user/course/create" method="post" name="adduni">
              <?php } ?>


              <div class="box-body">

                <div class="form-group">
                  <label for="department" class="col-sm-2 control-label">University *</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="university" id="university" onchange="filter('campus');">
                      <option value="">Select University</option>
                      <?php
                      if (isset($university_list) && !empty($university_list)) {
                        foreach ($university_list as $ulkey => $ulvalue) {
                      ?>
                          <option value="<?php echo $ulvalue['id']; ?>" <?php if (isset($selected_university_id) && $selected_university_id == $ulvalue['id']) {
                                                                          echo "selected";
                                                                        } ?>> <?php echo $ulvalue['name']; ?> ( <?php echo $ulvalue['country_name']; ?> )</option>
                      <?php }
                      } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="department" class="col-sm-2 control-label">Campus *</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="campus" id="campus" onchange="filter('college')">
                      <option value="">Select Campus</option>
                      <?php
                      if (isset($campus_list) && !empty($campus_list)) {
                        foreach ($campus_list as $clkey => $clvalue) {
                      ?>
                          <option value="<?php echo $clvalue['id']; ?>" <?php if (isset($selected_campus_id) && $selected_campus_id == $clvalue['id']) {
                                                                          echo "selected";
                                                                        } ?>><?php echo $clvalue['name']; ?></option>
                      <?php }
                      } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="department" class="col-sm-2 control-label">College *</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="college" id="college" onchange="filter('department')">
                      <option value="">Select College</option>
                      <?php
                      if (isset($college_list) && !empty($college_list)) {
                        foreach ($college_list as $cgkey => $cgvalue) {
                      ?>
                          <option value="<?php echo $cgvalue['id']; ?>" <?php if (isset($selected_college_id) && $selected_college_id == $cgvalue['id']) {
                                                                          echo "selected";
                                                                        } ?>><?php echo $cgvalue['name']; ?></option>
                      <?php }
                      } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="department" class="col-sm-2 control-label">Department *</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="department" id="department">
                      <option value="">Select department</option>
                      <?php
                      if (isset($department_list) && !empty($department_list)) {
                        foreach ($department_list as $dlkey => $dlvalue) {
                      ?>
                          <option value="<?php echo $dlvalue['id']; ?>" <?php if (isset($selected_department_id) && $selected_department_id == $dlvalue['id']) {
                                                                          echo "selected";
                                                                        } ?>><?php echo $dlvalue['name']; ?></option>
                      <?php }
                      } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="degree" class="col-sm-2 control-label">Degree *</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="degree" id="degree">
                      <?php
                      foreach ($degrees as $degreeData) {
                      ?>
                        <option value="<?= $degreeData->id ?>" <?php if (isset($degree) && $degreeData->id == $degree) {
                                                                  echo 'selected="selected"';
                                                                } ?>><?= $degreeData->name ?></option>
                      <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="category_id" class="col-sm-2 control-label">Sub Stream *</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="category_id" id="category_id">
                      <option value="">Please Select</option>
                      <?php
                      foreach ($categories as $categoryData) {
                      ?>
                        <option value="<?= $categoryData->id ?>" <?php if (isset($category) && $categoryData->id == $category) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?= $categoryData->display_name ?></option>
                      <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="degree_substream" class="col-sm-2 control-label">Course Degree Substream</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="degree_substream" id="degree_substream">
                      <option value="">Please Select</option>
                      <?php
                      foreach ($degree_substreams as $degree_substream_name) {
                      ?>
                        <option value="<?= $degree_substream_name ?>" <?php if (isset($degree_substream) && $degree_substream_name == $degree_substream) {
                                                                        echo 'selected="selected"';
                                                                      } ?>><?= $degree_substream_name ?></option>
                      <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="degree_substream" class="col-sm-2 control-label">Stem *</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="stem" id="stem">
                      <option value="">Please Select</option>
                      <?php
                      foreach ($stem_list as $stem_list_name) {
                      ?>
                        <option value="<?= $stem_list_name ?>" <?php if (isset($stem) && $stem == $stem_list_name) {
                                                                  echo 'selected="selected"';
                                                                } ?>><?= $stem_list_name ?></option>
                      <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Name *</label>
                  <div class="col-sm-10">
                    <input type="name" class="form-control" id="name" name="name" placeholder="Course Name" value="<?php if (isset($name)) echo $name; ?>">
                  </div>
                </div>



                <div class="form-group">
                  <label for="deadline_link" class="col-sm-2 control-label">Course Link *</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="program_link" name="program_link" placeholder="Program Link" value="<?php if (isset($program_link)) echo $program_link; ?>">
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Program Link','program_link','program_link')">
                      Apply to all course
                    </button>
                  </div>
                </div>

                <div class="form-group">
                  <label for="deadline_link" class="col-sm-2 control-label">Application Link *</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="admission_link" name="admission_link" placeholder="Admission Link" value="<?php if (isset($admission_link)) echo $admission_link; ?>">
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Admission Link','admission_link','admission_link')">
                      Apply to all course
                    </button>
                  </div>
                </div>

                <div class="form-group">
                  <label for="deadline_link" class="col-sm-2 control-label">Deadline *</label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control datepicker" id="spring_deadline" name="spring_deadline" placeholder="Spring Deadline" value="<?php echo isset($spring_deadline) ? $spring_deadline : ''; ?>">
                  </div>
                  <div class="col-sm-2">
                    <input type="text" class="form-control datepicker" id="summer_deadline" name="summer_deadline" placeholder="Summer Deadline" value="<?php echo isset($summer_deadline) ? $summer_deadline : ''; ?>">
                  </div>
                  <div class="col-sm-2">
                    <input type="text" class="form-control datepicker" id="fall_deadline" name="fall_deadline" placeholder="Fall Deadline" value="<?php echo isset($fall_deadline) ? $fall_deadline : ''; ?>">
                  </div>
                  <div class="col-sm-2">
                    <select name="roll_over" id="roll_over" class="form-control">
                      <option value="">Select Roll Over</option>
                      <option value="yes" <?php if (isset($roll_over) && $roll_over == "yes") {
                                            echo 'selected="selected"';
                                          } ?>>Yes</option>
                      <option value="no" <?php if (isset($roll_over) && $roll_over == "no") {
                                            echo 'selected="selected"';
                                          } ?>>No</option>

                    </select>
                  </div>
                  <div class="col-sm-2">
                    <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Deadline','deadline','deadline')">
                      Apply to all course
                    </button>
                  </div>
                </div>

                <div class="form-group">
                  <label for="application_fee" class="col-sm-2 control-label">Application Fee *</label>
                  <div class="col-sm-6">
                    <label>Yes &nbsp;</label><input type="radio" name="application_fee" value="Yes" <?= isset($application_fee) && $application_fee == 'Yes' ? 'checked' : '' ?> onclick="showApplicationAmount()">
                    <label>No &nbsp;</label><input type="radio" name="application_fee" value="No" <?= isset($application_fee) && $application_fee == 'No' ? 'checked' : '' ?> onclick="hideApplicationAmount()"> &nbsp; &nbsp;
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allRadioDependencyUpdate('Application Fee','Application Fee Amount','application_fee','application_fee_amount', 'application_fee', 'application_fee_amount')">
                      Apply to all course
                    </button>
                  </div>
                </div>

                <div class="form-group" id="application-fee-amount-required" style="display:<?= $applicationFee ?>">
                  <label for="application_fee_amount" class="col-sm-2 control-label">Application Fee Amount</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="application_fee_amount" name="application_fee_amount" placeholder="Application Fee Amount" value="<?php if (isset($application_fee_amount)) echo $application_fee_amount; ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="duration" class="col-sm-2 control-label"> I-20 / Tuition Deposit*</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="i_twenty_tuition_deposite" name="i_twenty_tuition_deposite" placeholder="I-20 / Tuition Deposit" value="<?php if (isset($i_twenty_tuition_deposite)) echo $i_twenty_tuition_deposite; ?>">

                  </div>

                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('I-20 / Tuition Deposit','duration','duration')">
                      Apply to all course
                    </button>
                  </div>
                </div>

                <div class="form-group">
                  <label for="duration" class="col-sm-2 control-label"> Average Tuition Fees*</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="avg_tuition_fee" name="avg_tuition_fee" placeholder="Average Tuition Fees" value="<?php if (isset($avg_tuition_fee)) echo $avg_tuition_fee; ?>">

                  </div>

                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Average Tuition Fees','avg_tuition_fee','avg_tuition_fee')">
                      Apply to all course
                    </button>
                  </div>
                </div>


                <div class="form-group">
                  <label for="duration" class="col-sm-2 control-label">Duration</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="duration" name="duration" placeholder="Duration (In Months) e.g. 24" value="<?php if (isset($duration)) echo $duration; ?>">

                  </div>

                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Duration','duration','duration')">
                      Apply to all course
                    </button>
                  </div>
                </div>


                <div class="form-group">
                  <label for="deadline_link" class="col-sm-2 control-label">Deadline Link</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="deadline_link" name="deadline_link" placeholder="Deadline Link" value="<?php if (isset($deadline_link)) echo $deadline_link; ?>">
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Deadline Link','deadline_link','deadline_link')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="no_of_intake" class="col-sm-2 control-label">Number Of Intake</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="no_of_intake" name="no_of_intake" placeholder="Number Of Intake" value="<?php if (isset($no_of_intake)) echo $no_of_intake; ?>">
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Number Of Intake','no_of_intake','no_of_intake')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="intake_months" class="col-sm-2 control-label">Intake Months</label>
                  <div class="col-md-6">
                    <select class="select-multiple-intake form-control" name="intake_months[]" multiple="multiple">
                      <option value="January" <?php echo in_array("January", $selectedMonths) ? 'selected' : ''; ?>>Jan</option>
                      <option value="February" <?php echo in_array("February", $selectedMonths) ? 'selected' : ''; ?>>Feb</option>
                      <option value="March" <?php echo in_array("March", $selectedMonths) ? 'selected' : ''; ?>>Mar</option>
                      <option value="April" <?php echo in_array("April", $selectedMonths) ? 'selected' : ''; ?>>Apr</option>
                      <option value="May" <?php echo in_array("May", $selectedMonths) ? 'selected' : ''; ?>>May</option>
                      <option value="June" <?php echo in_array("June", $selectedMonths) ? 'selected' : ''; ?>>Jun</option>
                      <option value="July" <?php echo in_array("July", $selectedMonths) ? 'selected' : ''; ?>>Jul</option>
                      <option value="August" <?php echo in_array("August", $selectedMonths) ? 'selected' : ''; ?>>Aug</option>
                      <option value="September" <?php echo in_array("September", $selectedMonths) ? 'selected' : ''; ?>>Sep</option>
                      <option value="October" <?php echo in_array("October", $selectedMonths) ? 'selected' : ''; ?>>Oct</option>
                      <option value="November" <?php echo in_array("November", $selectedMonths) ? 'selected' : ''; ?>>Nov</option>
                      <option value="December" <?php echo in_array("December", $selectedMonths) ? 'selected' : ''; ?>>Dec</option>

                    </select>
                    <!-- <input type="text" class="form-control" id="intake_months" name="intake_months" placeholder="Intake Months(Comma Separated)" value="<?php if (isset($intake_months)) echo $intake_months; ?>"> -->
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Intake Months','intake_months','intake_months')">
                      Apply to all course
                    </button>
                  </div>
                </div>


                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Total Tuition (Per Year)</label>
                  <div class="col-sm-10">
                    <?php
                    if (isset($course_total_fee)) {
                      foreach ($course_total_fee as $index => $feesInternational) {
                    ?>
                        <div id="course_total_fee_<?= ($index + 1) ?>" <?= $index ? 'style="margin-top:10px;"' : '' ?>>
                          <div style="float:left; padding-left: 0px;" class="col-sm-4">
                            <label for="course_total_fee_currency" class="col-sm-3" style="padding-left: 0px;">Currency</label>
                            <input type="text" id="course_total_fee_currency_<?= ($index + 1) ?>" name="course_total_fee_currency[]" placeholder="Currency" value="<?= $feesInternational['currency'] ?>">
                          </div>
                          <div style="float:left; padding-left: 0px;" class="col-sm-4">
                            <label for="course_total_fee_amount" class="col-sm-4" style="padding-left: 0px;">Amount</label>
                            <input type="text" id="course_total_fee_amount_<?= ($index + 1) ?>" name="course_total_fee_amount[]" placeholder="Amount" value="<?= $feesInternational['amount'] ?>">
                          </div>
                          <?php
                          if (!$index) {
                          ?>
                            <div><input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addFeesInternational()"></div>
                          <?php
                          } else {
                          ?>
                            <div><input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteFeesInternational(<?= ($index + 1) ?>)"></div>
                          <?php
                          }
                          ?>

                        </div>
                      <?php
                      }
                    } else {
                      ?>
                      <div id="course_total_fee_1">
                        <div style="float:left; padding-left: 0px;" class="col-sm-4">
                          <label for="course_total_fee_currency" class="col-sm-3" style="padding-left: 0px;">Currency</label>
                          <input type="text" id="course_total_fee_currency_1" name="course_total_fee_currency[]" placeholder="Currency" value="<?php if (isset($enrollment)) echo $enrollment; ?>">
                        </div>
                        <div style="float:left; padding-left: 0px;" class="col-sm-4">
                          <label for="course_total_fee_amount" class="col-sm-4" style="padding-left: 0px;">Amount</label>
                          <input type="text" id="course_total_fee_amount_1" name="course_total_fee_amount[]" placeholder="Amount" value="<?php if (isset($enrollment)) echo $enrollment; ?>">
                        </div>
                        <div><input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addFeesInternational()"></div>
                      </div>
                    <?php
                    }
                    ?>


                  </div>
                </div>


                <div class="form-group">
                  <label for="application_fee_waiver_imperial" class="col-sm-2 control-label">Application Fee Waiver Imperial</label>
                  <div class="col-sm-6">
                    <label>Yes &nbsp;</label><input type="radio" name="application_fee_waiver_imperial" value="Yes" <?= isset($application_fee_waiver_imperial) && $application_fee_waiver_imperial == 'Yes' ? 'checked' : '' ?>>
                    <label>No &nbsp;</label><input type="radio" name="application_fee_waiver_imperial" value="No" <?= isset($application_fee_waiver_imperial) && $application_fee_waiver_imperial == 'No' ? 'checked' : '' ?>>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allRadioUpdate('Application Fee Waiver Imperial','application_fee_waiver_imperial','application_fee_waiver_imperial')">
                      Apply to all course
                    </button>
                  </div>

                </div>
                <?php
                /*
                    <div class="form-group">
                      <label for="deadline_date_spring" class="col-sm-2 control-label">Deadline Date Spring</label>
                      <div class="col-sm-10">
                        <input type="text" class="mws-datepicker small" id="datepicker" name="deadline_date_spring" value="<?php if(isset($deadline_date_spring)) echo $deadline_date_spring; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="deadline_date_fall" class="col-sm-2 control-label">Deadline Date Fall</label>
                      <div class="col-sm-10">
                        <input type="text" class="mws-datepicker small" id="datepicker_1" name="deadline_date_fall" value="<?php if(isset($deadline_date_fall)) echo $deadline_date_fall; ?>">
                      </div>
                    </div>
                    <!--div class="form-group">
                      <label for="intake" class="col-sm-2 control-label">Intake</label>
                      <div class="col-sm-10">
                          <select class="form-control" name="intake">
                              <option value="FALL" <?php if(isset($intake) && $intake=='FALL'){ echo 'selected="selected"'; } ?>>Fall</option>
                              <option value="SPRING" <?php if(isset($intake) && $intake=='SPRING'){ echo 'selected="selected"'; } ?>>Spring</option>
                          </select>
                      </div>
                  </div-->
                    */
                ?>
                <!-- <div class="form-group">
                      <label for="total_fees" class="col-sm-2 control-label">Total Tuition (Per Year)</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="total_fees" name="total_fees" placeholder="Total Fees" value="<?php if (isset($total_fees)) echo $total_fees; ?>">
                      </div>
                      <div class="col-sm-4">
                        <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Total Fees','total_fees','total_fees')">
                          Apply to all course
                        </button>
                      </div>
                    </div> -->

                <div class="form-group">
                  <label for="cost_of_living_year" class="col-sm-2 control-label">Cost Of Living (Off-campus per Year)</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="cost_of_living_year" name="cost_of_living_year" placeholder="Cost Of Living (Year)" value="<?php if (isset($cost_of_living_year)) echo $cost_of_living_year; ?>">
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Cost Of Living (Year)','cost_of_living_year','cost_of_living_year')">
                      Apply to all course
                    </button>
                  </div>
                </div>

                <!-- <div class="form-group">
                      <label for="contact_title" class="col-sm-2 control-label">Contact Title</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="contact_title" name="contact_title" placeholder="Contact Title" value="<?php if (isset($contact_title)) echo $contact_title; ?>">
                      </div>
                      <div class="col-sm-4">
                        <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Contact Title','contact_title','contact_title')">
                          Apply to all course
                        </button>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="contact_name" class="col-sm-2 control-label">Contact Name</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="contact_name" name="contact_name" placeholder="Contact Name" value="<?php if (isset($contact_name)) echo $contact_name; ?>">
                      </div>
                      <div class="col-sm-4">
                        <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Contact Name','contact_name','contact_name')">
                          Apply to all course
                        </button>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="contact_email" class="col-sm-2 control-label">Contact Email</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="contact_email" name="contact_email" placeholder="Contact Email" value="<?php if (isset($contact_email)) echo $contact_email; ?>">
                      </div>
                      <div class="col-sm-4">
                        <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Contact Email','contact_email','contact_email')">
                          Apply to all course
                        </button>
                      </div>
                    </div> -->
                <div class="form-group">
                  <label for="eligibility_gpa" class="col-sm-2 control-label">Eligibility GPA</label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control" id="eligibility_gpa" name="eligibility_gpa" placeholder="Eligibility GPA" value="<?php if (isset($eligibility_gpa)) echo $eligibility_gpa; ?>">
                  </div>
                  <label for="gpa_scale" class="col-sm-2 control-label">GPA Scale</label>
                  <div class="col-sm-2">
                    <select class="form-control" name="gpa_scale" id="gpa_scale">
                      <option value="">Please Select</option>
                      <option value="4" <?php if (isset($gpa_scale) && $gpa_scale == 4) {
                                          echo 'selected="selected"';
                                        } ?>>4.0</option>
                      <option value="5" <?php if (isset($gpa_scale) && $gpa_scale == 5) {
                                          echo 'selected="selected"';
                                        } ?>>5.0</option>
                      <option value="10" <?php if (isset($gpa_scale) && $gpa_scale == 10) {
                                            echo 'selected="selected"';
                                          } ?>>10</option>
                      <option value="100" <?php if (isset($gpa_scale) && $gpa_scale == 100) {
                                            echo 'selected="selected"';
                                          } ?>>100</option>
                    </select>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="gpaUpdate('Eligibility GPA','eligibility_gpa','GPA Scale','gpa_scale')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="test_required" class="col-sm-2 control-label">GRE Waiver Available</label>
                  <div class="col-sm-6">
                    <label>Yes &nbsp;</label><input type="radio" name="test_required" value="Yes" <?= isset($test_required) && $test_required == 'Yes' ? 'checked' : '' ?>>
                    <label>No &nbsp;</label><input type="radio" name="test_required" value="No" <?= isset($test_required) && $test_required == 'No' ? 'checked' : '' ?>>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allRadioUpdate('Test Required','test_required','test_required')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="eligibility_gre_quant" class="col-sm-2 control-label">Eligibility GRE (Quant)</label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control" id="eligibility_gre_quant" name="eligibility_gre_quant" placeholder="Eligibility GRE (Quant)" value="<?php if (isset($eligibility_gre_quant)) echo $eligibility_gre_quant; ?>">
                  </div>

                  <label for="eligibility_gre_verbal" class="col-sm-2 control-label">Eligibility GRE (Verbal)</label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control" id="eligibility_gre_verbal" name="eligibility_gre_verbal" placeholder="Eligibility GRE (Verbal)" value="<?php if (isset($eligibility_gre_verbal)) echo $eligibility_gre_verbal; ?>">
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="greUpdate('Eligibility GRE - Quant', 'quant', 'Eligibility GRE - Verbal', 'verbal', 'eligibility_gre')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="eligibility_gmat" class="col-sm-2 control-label">Eligibility GMAT</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="eligibility_gmat" name="eligibility_gmat" placeholder="Eligibility GMAT" value="<?php if (isset($eligibility_gmat)) echo $eligibility_gmat; ?>">
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Eligibility GMAT','eligibility_gmat','eligibility_gmat')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="eligibility_sat" class="col-sm-2 control-label">Eligibility SAT</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="eligibility_sat" name="eligibility_sat" placeholder="Eligibility SAT" value="<?php if (isset($eligibility_sat)) echo $eligibility_sat; ?>">
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Eligibility SAT','eligibility_sat','eligibility_sat')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="eligibility_english_proficiency" class="col-sm-2 control-label">Eligibility English Proficiency</label>
                  <div class="col-sm-6">
                    <label>Yes &nbsp;</label><input type="radio" name="eligibility_english_proficiency" value="Yes" <?= isset($eligibility_english_proficiency) && $eligibility_english_proficiency == 'Yes' ? 'checked' : '' ?>>
                    <label>No &nbsp;</label><input type="radio" name="eligibility_english_proficiency" value="No" <?= isset($eligibility_english_proficiency) && $eligibility_english_proficiency == 'No' ? 'checked' : '' ?>>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allRadioUpdate('Eligibility English Proficiency','eligibility_english_proficiency','eligibility_english_proficiency')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="eligibility_toefl" class="col-sm-2 control-label">Eligibility TOEFL</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="eligibility_toefl" name="eligibility_toefl" placeholder="Eligibility TOEFL" value="<?php if (isset($eligibility_toefl)) echo $eligibility_toefl; ?>">
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Eligibility TOEFL','eligibility_toefl','eligibility_toefl')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="eligibility_ielts" class="col-sm-2 control-label">Eligibility IELTS</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="eligibility_ielts" name="eligibility_ielts" placeholder="Eligibility IELTS" value="<?php if (isset($eligibility_ielts)) echo $eligibility_ielts; ?>">
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Eligibility IELTS','eligibility_ielts','eligibility_ielts')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="pte_accepted" class="col-sm-2 control-label">PTE Accepted</label>
                  <div class="col-sm-6">
                    <label>Yes &nbsp;</label><input type="radio" name="pte_accepted" value="Yes" <?= isset($pte_accepted) && $pte_accepted == 'Yes' ? 'checked' : '' ?>>
                    <label>No &nbsp;</label><input type="radio" name="pte_accepted" value="No" <?= isset($pte_accepted) && $pte_accepted == 'No' ? 'checked' : '' ?>>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allRadioUpdate('PTE Accepted','pte_accepted','pte_accepted')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="duolingo_accepted" class="col-sm-2 control-label">Duolingo Accepted</label>
                  <div class="col-sm-6">
                    <label>Yes &nbsp;</label><input type="radio" name="duolingo_accepted" value="Yes" <?= isset($duolingo_accepted) && $duolingo_accepted == 'Yes' ? 'checked' : '' ?>>
                    <label>No &nbsp;</label><input type="radio" name="duolingo_accepted" value="No" <?= isset($duolingo_accepted) && $duolingo_accepted == 'No' ? 'checked' : '' ?>>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allRadioUpdate('Duolingo Accepted','duolingo_accepted','duolingo_accepted')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="eligibility_pte" class="col-sm-2 control-label">Eligibility PTE</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="eligibility_pte" name="eligibility_pte" placeholder="Eligibility PTE" value="<?php if (isset($eligibility_pte)) echo $eligibility_pte; ?>">
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Eligibility PTE','eligibility_pte','eligibility_pte')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="eligibility_duolingo" class="col-sm-2 control-label">Eligibility Duolingo</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="eligibility_duolingo" name="eligibility_duolingo" placeholder="Eligibility Duolingo" value="<?php if (isset($eligibility_duolingo)) echo $eligibility_duolingo; ?>">
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Eligibility Duolingo','eligibility_duolingo','eligibility_duolingo')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="eligibility_work_experience" class="col-sm-2 control-label">Eligibility Work Experience</label>
                  <div class="col-sm-6">
                    <label>Yes &nbsp;</label><input type="radio" name="eligibility_work_experience" value="Yes" <?= isset($eligibility_work_experience) && $eligibility_work_experience == 'Yes' ? 'checked' : '' ?>>
                    <label>No &nbsp;</label><input type="radio" name="eligibility_work_experience" value="No" <?= isset($eligibility_work_experience) && $eligibility_work_experience == 'No' ? 'checked' : '' ?>>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allRadioUpdate('Eligibility Work Experience','eligibility_work_experience','eligibility_work_experience')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">LOR (Letter Of Recommendation)</label>
                  <div class="col-sm-6">
                    <label>Yes &nbsp;</label><input type="radio" name="lor_needed" value="1" onclick="showLOR()" <?= isset($lor->lor_needed) && $lor->lor_needed == 1 ? 'checked' : '' ?>>
                    <label>No &nbsp;</label><input type="radio" name="lor_needed" value="0" onclick="hideLOR()" <?= isset($lor->lor_needed) && $lor->lor_needed == 0 ? 'checked' : '' ?>>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="LORUpdateJson('LOR (Letter Of Recommendation)','LOR Required','LOR Type','lor_needed','lor_required','lor_type','lor_needed','lor_required','lor_type','lor')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group" id="lor-required" style="display:<?= $lor_required ?>">
                  <label class="col-sm-2 control-label">LOR Required</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="lor_required" id="lor_required">
                      <?php
                      for ($i = 1; $i <= 10; $i++) {
                      ?>
                        <option value="<?= $i ?>" <?= isset($lor->lor_required) && $lor->lor_required == $i ? 'selected' : '' ?>><?php echo $i; ?></option>
                      <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="form-group" id="lor-type" style="display:<?= $lor_type ?>">
                  <label class="col-sm-2 control-label">LOR Type</label>
                  <div class="col-sm-10">
                    <label>Offline &nbsp;</label><input type="radio" name="lor_type" value="offline" <?= isset($lor->lor_type) && $lor->lor_type == "offline" ? 'checked' : '' ?>>
                    <label>Online &nbsp;</label><input type="radio" name="lor_type" value="online" <?= isset($lor->lor_type) && $lor->lor_type == "online" ? 'checked' : '' ?>>
                    <label>Online (After Application) &nbsp;</label><input type="radio" name="lor_type" value="online_after_application" <?= isset($lor->lor_type) && $lor->lor_type == "online_after_application" ? 'checked' : '' ?>>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">SOP (Statement Of Purpose)</label>
                  <div class="col-sm-6">
                    <label>Yes &nbsp;</label><input type="radio" name="sop_needed" value="1" onclick="showNoOfWords()" <?= isset($sop->sop_needed) && $sop->sop_needed == 1 ? 'checked' : '' ?>>
                    <label>No &nbsp;</label><input type="radio" name="sop_needed" value="0" onclick="hideNoOfWords()" <?= isset($sop->sop_needed) && $sop->sop_needed == 0 ? 'checked' : '' ?>>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allRadioDependencyUpdateJson('SOP (Statement Of Purpose)','SOP - Number Of Words', 'sop_needed', 'sop_no_of_words', 'sop_needed', 'sop_no_of_words','sop')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group" id="sop-no-of-words" style="display:<?= $sop_no_of_words ?>">
                  <label class="col-sm-2 control-label">SOP - Number Of Words</label>
                  <div class="col-sm-10">
                    <input type="text" name="sop_no_of_words" class="form-control" value="<?= isset($sop->sop_no_of_words) ? $sop->sop_no_of_words : '' ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="resume_required" class="col-sm-2 control-label">Resume Required</label>
                  <div class="col-sm-6">
                    <label>Yes &nbsp;</label><input type="radio" name="resume_required" value="Yes" <?= isset($resume_required) && $resume_required == 'Yes' ? 'checked' : '' ?>>
                    <label>No &nbsp;</label><input type="radio" name="resume_required" value="No" <?= isset($resume_required) && $resume_required == 'No' ? 'checked' : '' ?>>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allRadioUpdate('Resume Required','resume_required','resume_required')">
                      Apply to all course
                    </button>
                  </div>
                </div>

                <!-- <div class="form-group">
                      <label for="admission_requirements" class="col-sm-2 control-label">Admission Requirements</label>
                      <div class="col-sm-6">
                        <textarea rows="4" class="form-control" id="admission_requirements" name="admission_requirements" placeholder="Admission Requirements"><?php if (isset($admission_requirements)) echo $admission_requirements; ?></textarea>
                      </div>
                      <div class="col-sm-4">
                        <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Admission Requirements','admission_requirements','admission_requirements')">
                          Apply to all course
                        </button>
                      </div>
                    </div> -->
                <div class="form-group">
                  <label for="expected_salary" class="col-sm-2 control-label">Salary Expected</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="expected_salary" name="expected_salary" placeholder="Salary Expected" value="<?php if (isset($expected_salary)) echo $expected_salary; ?>">
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Salary Expected','expected_salary','expected_salary')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="backlog_accepted" class="col-sm-2 control-label">Backlog Accepted</label>
                  <div class="col-sm-6">
                    <label>Yes &nbsp;</label><input type="radio" name="backlog_accepted" value="1" <?= isset($backlog_accepted) && $backlog_accepted == '1' ? 'checked' : '' ?> onclick="showNoOfBacklogs()">
                    <label>No &nbsp;</label><input type="radio" name="backlog_accepted" value="0" <?= isset($backlog_accepted) && $backlog_accepted == '0' ? 'checked' : '' ?> onclick="hideNoOfBacklogs()">
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allRadioDependencyUpdateJson('Backlog Accepted', 'Number Of Backlogs','backlog_accepted','number_of_backlogs','backlog_accepted','number_of_backlogs','backlogs')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group" id="number-of-backlogs-accepted" style="display:<?= $backlog ?>">
                  <label for="number_of_backlogs" class="col-sm-2 control-label">Number Of Backlogs</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="number_of_backlogs" name="number_of_backlogs" placeholder="Number Of Backlogs" value="<?php if (isset($number_of_backlogs)) echo $number_of_backlogs; ?>">

                    <select class="form-control" name="number_of_backlogs" id="number_of_backlogs">
                      <option value="">Please Select</option>
                      <option value="1" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '1') {
                                          echo 'selected="selected"';
                                        } ?>>1</option>
                      <option value="2" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '2') {
                                          echo 'selected="selected"';
                                        } ?>>2</option>
                      <option value="3" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '3') {
                                          echo 'selected="selected"';
                                        } ?>>3</option>
                      <option value="4" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '4') {
                                          echo 'selected="selected"';
                                        } ?>>4</option>
                      <option value="5" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '5') {
                                          echo 'selected="selected"';
                                        } ?>>5</option>
                      <option value="6" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '6') {
                                          echo 'selected="selected"';
                                        } ?>>6</option>
                      <option value="7" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '7') {
                                          echo 'selected="selected"';
                                        } ?>>7</option>
                      <option value="8" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '8') {
                                          echo 'selected="selected"';
                                        } ?>>8</option>
                      <option value="9" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '9') {
                                          echo 'selected="selected"';
                                        } ?>>9</option>
                      <option value="10" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '10') {
                                            echo 'selected="selected"';
                                          } ?>>10</option>
                    </select>

                  </div>
                </div>
                <div class="form-group">
                  <label for="drop_years" class="col-sm-2 control-label">Gap Years Accepted</label>
                  <div class="col-sm-6">
                    <label>Yes &nbsp;</label><input type="radio" name="drop_years" value="Yes" <?= isset($drop_years) && $drop_years == 'Yes' ? 'checked' : '' ?>>
                    <label>No &nbsp;</label><input type="radio" name="drop_years" value="No" <?= isset($drop_years) && $drop_years == 'No' ? 'checked' : '' ?>>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allRadioUpdate('Drop Years','drop_years','drop_years')">
                      Apply to all course
                    </button>
                  </div>
                </div>

                <div class="form-group">
                  <label for="scholarship_available" class="col-sm-2 control-label">Scholarship Available</label>
                  <div class="col-sm-6">
                    <label>Yes &nbsp;</label><input type="radio" name="scholarship_available" value="Yes" <?= isset($scholarship_available) && $scholarship_available == 'Yes' ? 'checked' : '' ?> onclick="showScholarship();">
                    <label>No &nbsp;</label><input type="radio" name="scholarship_available" value="No" <?= isset($scholarship_available) && $scholarship_available == 'No' ? 'checked' : '' ?> onclick="hideScholarship();">
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="scholarshipUpdate('Scholarship Available', 'Tuition Waivers', 'Research Assistantships', 'Teaching Assistantships', 'Graduate Assistantships', 'Others Link', 'scholarship_available', 'tuition_waivers', 'research_assistantships', 'teaching_assistantships', 'graduate_assistantships', 'others_link')">
                      Apply to all course
                    </button>
                  </div>
                </div>

                <!--<div id="scholarship_detail_div" style="display:<?= $scholarship_detail_div ?>">

                     <div class="form-group">
                      <label for="expected_salary" class="col-sm-2 control-label"> Tuition Waivers </label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="tuition_waivers" name="tuition_waivers" placeholder="Tuition waivers" value="<?php if (isset($tuition_waivers)) echo $tuition_waivers; ?>">
                      </div>

                    </div>

                    <div class="form-group">
                      <label for="expected_salary" class="col-sm-2 control-label"> Research Assistantships </label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="research_assistantships" name="research_assistantships" placeholder="Research Assistantships" value="<?php if (isset($research_assistantships)) echo $research_assistantships; ?>">
                      </div>

                    </div>

                    <div class="form-group">
                      <label for="expected_salary" class="col-sm-2 control-label"> Teaching Assistantships </label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="teaching_assistantships" name="teaching_assistantships" placeholder="Teaching Assistantships" value="<?php if (isset($teaching_assistantships)) echo $teaching_assistantships; ?>">
                      </div>

                    </div>

                    <div class="form-group">
                      <label for="expected_salary" class="col-sm-2 control-label"> Graduate Assistantships </label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="graduate_assistantships" name="graduate_assistantships" placeholder="Graduate Assistantships" value="<?php if (isset($graduate_assistantships)) echo $graduate_assistantships; ?>">
                      </div>

                    </div>

                    <div class="form-group">
                      <label for="expected_salary" class="col-sm-2 control-label"> Others - link </label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="others_link" name="others_link" placeholder="Others - link" value="<?php if (isset($others_link)) echo $others_link; ?>">
                      </div>

                    </div>

                  </div> -->

                <div class="form-group">
                  <label for="with_15_years_education" class="col-sm-2 control-label">Within 15 Years Education</label>
                  <div class="col-sm-6">
                    <label>Yes &nbsp;</label><input type="radio" name="with_15_years_education" value="Yes" <?= isset($with_15_years_education) && $with_15_years_education == 'Yes' ? 'checked' : '' ?>>
                    <label>No &nbsp;</label><input type="radio" name="with_15_years_education" value="No" <?= isset($with_15_years_education) && $with_15_years_education == 'No' ? 'checked' : '' ?>>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allRadioUpdate('Within 15 Years Education','with_15_years_education','with_15_years_education')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="wes_requirement" class="col-sm-2 control-label">Credential Evaluation Required</label>
                  <div class="col-sm-6">
                    <label>Yes &nbsp;</label><input type="radio" name="wes_requirement" value="Yes" <?= isset($wes_requirement) && $wes_requirement == 'Yes' ? 'checked' : '' ?> onclick="showDynamicOne('wes_type_div')">
                    <label>No &nbsp;</label><input type="radio" name="wes_requirement" value="No" <?= isset($wes_requirement) && $wes_requirement == 'No' ? 'checked' : '' ?> onclick="hideDynamicOne('wes_type_div')">
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allRadioUpdate('WES Requirement','wes_requirement','wes_requirement')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group" id="wes_type_div" style="display:<?= $wes_type_div ?>">
                  <label for="wes_requirement" class="col-sm-2 control-label"> Type </label>
                  <div class="col-sm-10">
                    <select class="form-control" name="wes_type" id="wes_type">
                      <option value="">Please Select</option>
                      <option value="WES" <?php if (isset($wes_type) && $wes_type == 'WES') {
                                            echo 'selected="selected"';
                                          } ?>>World Education Services (WES)</option>
                      <option value="Spantran" <?php if (isset($wes_type) && $wes_type == 'Spantran') {
                                                  echo 'selected="selected"';
                                                } ?>>Spantran(The Evaluation Company)</option>
                      <option value="IEE" <?php if (isset($wes_type) && $wes_type == 'IEE') {
                                            echo 'selected="selected"';
                                          } ?>>International Education Evaluators(IEE)</option>
                      <option value="ECE" <?php if (isset($wes_type) && $wes_type == 'ECE') {
                                            echo 'selected="selected"';
                                          } ?>>Educational Credential Evaluators (ECE)</option>
                      <option value="ACEI" <?php if (isset($wes_type) && $wes_type == 'ACEI') {
                                              echo 'selected="selected"';
                                            } ?>>Academic Credentials Evaluation Institute (ACEI)</option>
                      <option value="ALL" <?php if (isset($wes_type) && $wes_type == 'ALL') {
                                            echo 'selected="selected"';
                                          } ?>>All (NACES Approved)</option>

                    </select>
                  </div>

                </div>
                <div class="form-group">
                  <label for="work_while_studying" class="col-sm-2 control-label">Part-time Work While Studying</label>
                  <div class="col-sm-6">
                    <label>Yes &nbsp;</label><input type="radio" name="work_while_studying" value="Yes" <?= isset($work_while_studying) && $work_while_studying == 'Yes' ? 'checked' : '' ?>>
                    <label>No &nbsp;</label><input type="radio" name="work_while_studying" value="No" <?= isset($work_while_studying) && $work_while_studying == 'No' ? 'checked' : '' ?>>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allRadioUpdate('Work While Studying','work_while_studying','work_while_studying')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="work_while_studying" class="col-sm-2 control-label">Part-Time Work Details (number of hours)</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="part_time_work_details" name="part_time_work_details" placeholder="Part-Time Work Details" value="<?php if (isset($part_time_work_details)) echo $part_time_work_details; ?>">
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Work While Studying','part_time_work_details','part_time_work_details')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="cooperate_internship_participation" class="col-sm-2 control-label">Co-Op / CPT / Internship Opportunity</label>
                  <div class="col-sm-6">
                    <label>Yes &nbsp;</label><input type="radio" name="cooperate_internship_participation" value="Yes" <?= isset($cooperate_internship_participation) && $cooperate_internship_participation == 'Yes' ? 'checked' : '' ?>>
                    <label>No &nbsp;</label><input type="radio" name="cooperate_internship_participation" value="No" <?= isset($cooperate_internship_participation) && $cooperate_internship_participation == 'No' ? 'checked' : '' ?>>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allRadioUpdate('Co-operate Internship Participation','cooperate_internship_participation','cooperate_internship_participation')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="work_permit_needed" class="col-sm-2 control-label">Post-Study Stay Back Available</label>
                  <div class="col-sm-6">
                    <label>Yes &nbsp;</label><input type="radio" name="work_permit_needed" value="1" <?= isset($work_permit_needed) && $work_permit_needed == '1' ? 'checked' : '' ?> onclick="showNoOfMonths()">
                    <label>No &nbsp;</label><input type="radio" name="work_permit_needed" value="0" <?= isset($work_permit_needed) && $work_permit_needed == '0' ? 'checked' : '' ?> onclick="hideNoOfMonths()">
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allRadioDependencyUpdateJson('Work Permit After Course', 'Number Of Months', 'work_permit_needed','no_of_months','work_permit_needed','no_of_months','work_permit')">
                      Apply to all course
                    </button>
                  </div>
                </div>
                <div class="form-group" id="no-of-months-accepted" style="display:<?= $workPermit ?>">
                  <label for="no_of_months" class="col-sm-2 control-label">Number Of Months</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="no_of_months" name="no_of_months" placeholder="Number Of Months" value="<?php if (isset($no_of_months)) echo $no_of_months; ?>">

                    <select class="form-control" name="number_of_backlogs" id="number_of_backlogs">
                      <option value="">Please Select</option>
                      <option value="1" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '1') {
                                          echo 'selected="selected"';
                                        } ?>>1</option>
                      <option value="3" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '3') {
                                          echo 'selected="selected"';
                                        } ?>>3</option>
                      <option value="6" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '6') {
                                          echo 'selected="selected"';
                                        } ?>>6</option>
                      <option value="9" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '9') {
                                          echo 'selected="selected"';
                                        } ?>>9</option>
                      <option value="12" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '12') {
                                            echo 'selected="selected"';
                                          } ?>>12</option>
                      <option value="15" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '15') {
                                            echo 'selected="selected"';
                                          } ?>>15</option>
                      <option value="18" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '18') {
                                            echo 'selected="selected"';
                                          } ?>>18</option>
                      <option value="21" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '21') {
                                            echo 'selected="selected"';
                                          } ?>>21</option>
                      <option value="24" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '24') {
                                            echo 'selected="selected"';
                                          } ?>>24</option>
                      <option value="27" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '27') {
                                            echo 'selected="selected"';
                                          } ?>>27</option>
                      <option value="30" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '30') {
                                            echo 'selected="selected"';
                                          } ?>>30</option>
                      <option value="33" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '33') {
                                            echo 'selected="selected"';
                                          } ?>>33</option>
                      <option value="36" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '36') {
                                            echo 'selected="selected"';
                                          } ?>>36</option>
                      <option value="39" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '39') {
                                            echo 'selected="selected"';
                                          } ?>>39</option>
                      <option value="42" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '42') {
                                            echo 'selected="selected"';
                                          } ?>>42</option>
                      <option value="45" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '45') {
                                            echo 'selected="selected"';
                                          } ?>>45</option>
                      <option value="48" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '48') {
                                            echo 'selected="selected"';
                                          } ?>>48</option>
                      <option value="51" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '51') {
                                            echo 'selected="selected"';
                                          } ?>>51</option>
                      <option value="54" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '54') {
                                            echo 'selected="selected"';
                                          } ?>>54</option>
                      <option value="57" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '57') {
                                            echo 'selected="selected"';
                                          } ?>>57</option>
                      <option value="60" <?php if (isset($number_of_backlogs) && $number_of_backlogs == '60') {
                                            echo 'selected="selected"';
                                          } ?>>60</option>
                    </select>
                  </div>
                </div>
                <!-- <div class="form-group">
                      <label for="start_months" class="col-sm-2 control-label">Course Start Months</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="start_months" name="start_months" placeholder="Course Start Months(Comma Separated)" value="<?php if (isset($start_months)) echo $start_months; ?>">
                      </div>
                      <div class="col-sm-4">
                        <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Course Start Months','start_months','start_months')">
                          Apply to all course
                        </button>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="decision_months" class="col-sm-2 control-label">Course Decision Months</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" id="decision_months" name="decision_months" placeholder="Course Decision Months(Comma Separated)" value="<?php if (isset($decision_months)) echo $decision_months; ?>">
                      </div>
                      <div class="col-sm-4">
                        <button type="button" class="form-control btn btn-primary" onclick="allTextUpdate('Course Decision Months','decision_months','decision_months')">
                          Apply to all course
                        </button>
                      </div>
                    </div> -->
                <div class="form-group">
                  <label for="conditional_admit" class="col-sm-2 control-label">Conditional Admit</label>
                  <div class="col-sm-6">
                    <label>Yes &nbsp;</label><input type="radio" name="conditional_admit" value="Yes" <?= isset($conditional_admit) && $conditional_admit == 'Yes' ? 'checked' : '' ?>>
                    <label>No &nbsp;</label><input type="radio" name="conditional_admit" value="No" <?= isset($conditional_admit) && $conditional_admit == 'No' ? 'checked' : '' ?>>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="form-control btn btn-primary" onclick="allRadioUpdate('Conditional Admit','conditional_admit','conditional_admit')">
                      Apply to all course
                    </button>
                  </div>
                </div>



                <div class="form-group">

                  <label for="gpa_scale" class="col-sm-2 control-label">Transcripts </label>
                  <div class="col-sm-10">
                    <select class="form-control" name="transcripts" id="transcripts">
                      <option value="">Please Select</option>
                      <option value="ONLINE" <?php if (isset($transcripts) && $transcripts == 'ONLINE') {
                                                echo 'selected="selected"';
                                              } ?>>ONLINE</option>
                      <option value="COURIER" <?php if (isset($transcripts) && $transcripts == 'COURIER') {
                                                echo 'selected="selected"';
                                              } ?>>COURIER</option>
                      <option value="NOT-MENTIONED" <?php if (isset($transcripts) && $transcripts == 'NOT-MENTIONED') {
                                                      echo 'selected="selected"';
                                                    } ?>>NOT-MENTIONED</option>
                    </select>
                  </div>

                </div>

                <div class="form-group">

                  <label for="gpa_scale" class="col-sm-2 control-label">Financial Documentation </label>
                  <div class="col-sm-10">
                    <select class="form-control" name="financial_documentation" id="financial_documentation">
                      <option value="">Please Select</option>
                      <option value="During-Application" <?php if (isset($financial_documentation) && $financial_documentation == 'During-Application') {
                                                            echo 'selected="selected"';
                                                          } ?>>During-Application</option>
                      <option value="After-Application" <?php if (isset($financial_documentation) && $financial_documentation == 'After-Application') {
                                                          echo 'selected="selected"';
                                                        } ?>>After-Application</option>
                    </select>
                  </div>

                </div>

                <!-- <div class="form-group">

                      <label for="gpa_scale" class="col-sm-2 control-label"> Test score waivers GRE/TOEFL/IELTS/GMAT/SAT  </label>
                      <div class="col-sm-10">
                          <select class="form-control" name="test_score_waivers" id="test_score_waivers">
                              <option value="">Please Select</option>
                              <option value="Yes" <?php if (isset($test_score_waivers) && $test_score_waivers == 'Yes') {
                                                    echo 'selected="selected"';
                                                  } ?>>Yes</option>
                              <option value="No" <?php if (isset($test_score_waivers) && $test_score_waivers == 'No') {
                                                    echo 'selected="selected"';
                                                  } ?>>No</option>
                          </select>
                      </div>

                    </div>

                    <div class="form-group">

                      <label for="gpa_scale" class="col-sm-2 control-label"> Test Score Reporting  </label>
                      <div class="col-sm-6">

                          <label>Official &nbsp;</label><input type="radio" name="test_score_reporting" value="Official" <?= isset($test_score_reporting) && $test_score_reporting == 'No' ? 'Official' : '' ?> onclick="showDynamicOne('test_score_reporting_div')">
                          <label>Online / Unoffical &nbsp;</label><input type="radio" name="test_score_reporting" value="Unoffical" <?= isset($test_score_reporting) && $test_score_reporting == 'Yes' ? 'Unoffical' : '' ?> onclick="hideDynamicOne('test_score_reporting_div')">

                      </div>

                    </div> -->

                <!-- <div id="test_score_reporting_div" style="display:<?= $test_score_reporting_div ?>"> -->

                <div class="form-group">
                  <label for="expected_salary" class="col-sm-2 control-label"> GRE Code </label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="gre_code" name="gre_code" placeholder="GRE Code" value="<?php if (isset($gre_code)) echo $gre_code; ?>">
                  </div>

                </div>

                <div class="form-group">
                  <label for="expected_salary" class="col-sm-2 control-label"> TOFEL Code </label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="tofel_code" name="tofel_code" placeholder="TOFEL Code" value="<?php if (isset($tofel_code)) echo $tofel_code; ?>">
                  </div>

                </div>

                <div class="form-group">
                  <label for="expected_salary" class="col-sm-2 control-label"> GMAT Code </label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="gmat_code" name="gmat_code" placeholder="GMAT Code" value="<?php if (isset($gmat_code)) echo $gmat_code; ?>">
                  </div>

                </div>

                <div class="form-group">
                  <label for="expected_salary" class="col-sm-2 control-label"> SAT Code </label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="sat_code" name="sat_code" placeholder="SAT Code" value="<?php if (isset($sat_code)) echo $sat_code; ?>">
                  </div>

                </div>

                <!-- </div> -->

                <div class="form-group">

                  <label for="gpa_scale" class="col-sm-2 control-label"> Change Of Course </label>
                  <div class="col-sm-6">

                    <label>Yes &nbsp;</label><input type="radio" name="change_course" value="Yes" <?= isset($change_course) && $change_course == 'Yes' ? 'checked' : '' ?> onclick="showDynamicOne('change_course_div')">
                    <label>No &nbsp;</label><input type="radio" name="change_course" value="No" <?= isset($change_course) && $change_course == 'No' ? 'checked' : '' ?> onclick="hideDynamicOne('change_course_div')">
                  </div>

                </div>

                <div class="form-group">

                  <label for="gpa_scale" class="col-sm-2 control-label"> Pre Requisites Needed </label>
                  <div class="col-sm-6">

                    <label>Yes &nbsp;</label><input type="radio" name="pre_requisites" value="Yes" <?= isset($pre_requisites) && $pre_requisites == 'Yes' ? 'checked' : '' ?> onclick="showDynamicOne('pre_requisites_div')">
                    <label>No &nbsp;</label><input type="radio" name="pre_requisites" value="No" <?= isset($pre_requisites) && $pre_requisites == 'No' ? 'checked' : '' ?> onclick="hideDynamicOne('pre_requisites_div')">
                  </div>

                </div>

                <div id="pre_requisites_div" style="display:<?= $pre_requisites_div ?>">

                  <!-- <div class="form-group">
                        <label for="expected_salary" class="col-sm-2 control-label"> Link For Pre Requisites </label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="pre_requisites_link" name="pre_requisites_link" placeholder="Link For Pre Requisites" value="<?php if (isset($pre_requisites_link)) echo $pre_requisites_link; ?>">
                        </div>

                      </div> -->

                </div>

                <div class="form-group" style="display:<?php if ($country) {
                                                          echo 'block';
                                                        } else {
                                                          echo 'none';
                                                        } ?>">

                  <label for="gpa_scale" class="col-sm-2 control-label"> Supervisor details for all Canada universities </label>
                  <div class="col-sm-10">
                    <select class="form-control" name="supervisor_details_canada" id="supervisor_details_canada">
                      <option value="">Please Select</option>
                      <option value="Supervisor-Approval-Required" <?php if (isset($supervisor_details_canada) && $supervisor_details_canada == 'Supervisor-Approval-Required') {
                                                                      echo 'selected="selected"';
                                                                    } ?>>Supervisor-Approval-Required</option>
                      <option value="Not-Required" <?php if (isset($supervisor_details_canada) && $supervisor_details_canada == 'Not-Required') {
                                                      echo 'selected="selected"';
                                                    } ?>>Not-Required</option>
                    </select>
                  </div>

                </div>

              </div><!-- /.box-body -->

              <div class="form-group">
                <label for="degree_substream" class="col-sm-2 control-label"> Status </label>
                <div class="col-sm-10">
                  <select class="form-control" name="status">
                    <option value="">Please Select Status</option>
                    <option value="1" <?php if (isset($status) && $status == '1') {
                                        echo 'selected="selected"';
                                      } ?>>Active</option>
                    <option value="2" <?php if (isset($status) && $status == '2') {
                                        echo 'selected="selected"';
                                      } ?>>In-Active</option>
                  </select>
                </div>
              </div>

              <div class="box-footer">
                <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                <?php
                if ($this->uri->segment('4')) {
                ?>
                  <button type="submit" class="btn btn-info pull-right">Update</button>
                <?php
                } else {
                ?>
                  <button type="submit" class="btn btn-info pull-right">Create</button>
                <?php
                }
                ?>

              </div><!-- /.box-footer -->
              </form>
        </div><!-- /.box -->
        <!-- general form elements disabled -->

      </div><!--/.col (right) -->
    </div> <!-- /.row -->
  </section><!-- /.content -->
  <div class="modal" id="modalChangeValue">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <h3 class="modal-title"> Multiple Update Form </h3>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <div class="modal-body" id="display_body">
          <div class="row">
            <h4 class="modal-title"><?php echo $cgvalue['name']; ?></h4>
            <div class="col-md-12">
              <label for="application_fee" class="col-sm-4 control-label">Application Fee</label>
              <label>Yes &nbsp;</label><input type="radio" name="application_fee" value="Yes" <?= isset($application_fee) && $application_fee == 'Yes' ? 'checked' : '' ?> onclick="showApplicationAmount()">
              <label>No &nbsp;</label><input type="radio" name="application_fee" value="No" <?= isset($application_fee) && $application_fee == 'No' ? 'checked' : '' ?> onclick="hideApplicationAmount()">
            </div>
            <div class="col-md-12" id="application-fee-amount-required" style="display:<?= $applicationFee ?>">
              <label for="application_fee_amount" class="col-sm-4 control-label">Application Fee Amount</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="application_fee_amount" name="application_fee_amount" placeholder="Application Fee Amount" value="<?php if (isset($application_fee_amount)) echo $application_fee_amount; ?>">
              </div>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-12">
              <label for="application_fee" class="control-label">Course Name</label>
              <h4 class="modal-title"><?php echo $cgvalue['name']; ?></h4>
            </div>
            <br>
            <div class="col-md-12">
              <label for="application_fee" class="col-sm-4 control-label">Application Fee</label>
              <label>Yes &nbsp;</label><input type="radio" name="application_fee" value="Yes" <?= isset($application_fee) && $application_fee == 'Yes' ? 'checked' : '' ?> onclick="showApplicationAmount()">
              <label>No &nbsp;</label><input type="radio" name="application_fee" value="No" <?= isset($application_fee) && $application_fee == 'No' ? 'checked' : '' ?> onclick="hideApplicationAmount()">
            </div>
            <div class="col-md-12" id="application-fee-amount-required" style="display:<?= $applicationFee ?>">
              <label for="application_fee_amount" class="col-sm-4 control-label">Application Fee Amount</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="application_fee_amount" name="application_fee_amount" placeholder="Application Fee Amount" value="<?php if (isset($application_fee_amount)) echo $application_fee_amount; ?>">
              </div>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-12">
              <label for="application_fee" class="col-sm-4 control-label">Application Fee</label>
              <label>Yes &nbsp;</label><input type="radio" name="application_fee" value="Yes" <?= isset($application_fee) && $application_fee == 'Yes' ? 'checked' : '' ?> onclick="showApplicationAmount()">
              <label>No &nbsp;</label><input type="radio" name="application_fee" value="No" <?= isset($application_fee) && $application_fee == 'No' ? 'checked' : '' ?> onclick="hideApplicationAmount()">
            </div>
            <div class="col-md-12" id="application-fee-amount-required" style="display:<?= $applicationFee ?>">
              <label for="application_fee_amount" class="col-sm-4 control-label">Application Fee Amount</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="application_fee_amount" name="application_fee_amount" placeholder="Application Fee Amount" value="<?php if (isset($application_fee_amount)) echo $application_fee_amount; ?>">
              </div>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-12">
              <label for="application_fee" class="col-sm-4 control-label">Application Fee</label>
              <label>Yes &nbsp;</label><input type="radio" name="application_fee" value="Yes" <?= isset($application_fee) && $application_fee == 'Yes' ? 'checked' : '' ?> onclick="showApplicationAmount()">
              <label>No &nbsp;</label><input type="radio" name="application_fee" value="No" <?= isset($application_fee) && $application_fee == 'No' ? 'checked' : '' ?> onclick="hideApplicationAmount()">
            </div>
            <div class="col-md-12" id="application-fee-amount-required" style="display:<?= $applicationFee ?>">
              <label for="application_fee_amount" class="col-sm-4 control-label">Application Fee Amount</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="application_fee_amount" name="application_fee_amount" placeholder="Application Fee Amount" value="<?php if (isset($application_fee_amount)) echo $application_fee_amount; ?>">
              </div>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-12">
              <label for="application_fee" class="col-sm-4 control-label">Application Fee</label>
              <label>Yes &nbsp;</label><input type="radio" name="application_fee" value="Yes" <?= isset($application_fee) && $application_fee == 'Yes' ? 'checked' : '' ?> onclick="showApplicationAmount()">
              <label>No &nbsp;</label><input type="radio" name="application_fee" value="No" <?= isset($application_fee) && $application_fee == 'No' ? 'checked' : '' ?> onclick="hideApplicationAmount()">
            </div>
            <div class="col-md-12" id="application-fee-amount-required" style="display:<?= $applicationFee ?>">
              <label for="application_fee_amount" class="col-sm-4 control-label">Application Fee Amount</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="application_fee_amount" name="application_fee_amount" placeholder="Application Fee Amount" value="<?php if (isset($application_fee_amount)) echo $application_fee_amount; ?>">
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div><!-- /.content-wrapper -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.min.js"></script>

<script>
  $(document).ready(function() {
    $('.datepicker').datepicker({
      dateFormat: "dd-mm",
      changeMonth: true,
      changeYear: false,
      onSelect: function(dateText, inst) {
        // Force the year to a default value (here we ignore it)
        var dateParts = dateText.split("-");
        // dateParts[0] = day, dateParts[1] = month
        $(this).val(dateParts[0] + "-" + dateParts[1]);
      }
    });
  });
</script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
  $(document).ready(function() {
    $('.select-multiple-intake').select2();
  });
</script>

<script type="text/javascript">
  function showNoOfWords() {
    document.getElementById("sop-no-of-words").style.display = "block";
  }

  function hideNoOfWords() {
    document.getElementById("sop-no-of-words").style.display = "none";
  }

  function showLOR() {
    document.getElementById("lor-required").style.display = "block";
    document.getElementById("lor-type").style.display = "block";
  }

  function hideLOR() {
    document.getElementById("lor-required").style.display = "none";
    document.getElementById("lor-type").style.display = "none";
  }

  function showApplicationAmount() {
    document.getElementById("application-fee-amount-required").style.display = "block";
  }

  function hideApplicationAmount() {
    document.getElementById("application-fee-amount-required").style.display = "none";
  }

  function showNoOfBacklogs() {
    document.getElementById("number-of-backlogs-accepted").style.display = "block";
  }

  function hideNoOfBacklogs() {
    document.getElementById("number-of-backlogs-accepted").style.display = "none";
  }

  function showNoOfMonths() {
    document.getElementById("no-of-months-accepted").style.display = "block";
  }

  function hideNoOfMonths() {
    document.getElementById("no-of-months-accepted").style.display = "none";
  }

  function showScholarship() {
    document.getElementById("scholarship_detail_div").style.display = "block";
  }

  function hideScholarship() {
    document.getElementById("scholarship_detail_div").style.display = "none";
  }
</script>

<script type="text/javascript">
  function filter(entity_type) {

    var entityId = 0;
    if (entity_type == "campus") {
      entityId = $('#university').val();
    }

    if (entity_type == "college") {
      entityId = $('#campus').val();
    }

    if (entity_type == "department") {
      entityId = $('#college').val();
    }

    sataString = {
      "entity_id": entityId,
      "entity_type": entity_type
    }

    $("#ajaxSpinnerImage").show();

    $.ajax({

      type: "POST",

      url: "/admin/filtercontroller/commonDropdown",

      data: sataString,

      cache: false,

      success: function(result) {

        $("#ajaxSpinnerImage").hide();
        document.getElementById(entity_type).innerHTML = result;

      }

    });

  }

  function allTextUpdate(displayFieldName, fieldId, databaseFieldName) {

    var filterLevel = <?= $filter_level ?>;

    var universityId = $('#university').val();
    var campusId = $('#campus').val();
    var collegeId = $('#college').val();
    var departmentId = $('#department').val();
    var degreeId = $('#degree').val();
    var courseId = <?= $id ?>;
    var fieldValue = $('#' + fieldId).val();

    var sataString = {
      "university_id": universityId,
      "campus_id": campusId,
      "college_id": collegeId,
      "department_id": departmentId,
      "degree_id": degreeId,
      "course_id": courseId,
      "filter_level": filterLevel,
      "database_field_name": databaseFieldName
    };

    $("#ajaxSpinnerImage").show();

    event.preventDefault();

    $.ajax({

      type: "POST",

      url: "/admin/user/course/filterCourseList",

      data: sataString,

      cache: false,

      success: function(result) {

        $("#ajaxSpinnerImage").hide();

        $('[id*="modalChangeValue"]').modal('show');

        if (result.course_list.length) {

          var arrayCount = result.course_list.length;

          var selectHTML = '<form name="updateForm" autocomplete="off" action="<?php echo base_url(); ?>user/course/multipleUpdate" method="post" >' +
            '<input type="hidden" name="s_university_id" id="s_university_id" value="' + universityId + '">' +
            '<input type="hidden" name="s_campus_id" id="s_campus_id" value="' + campusId + '">' +
            '<input type="hidden" name="s_college_id" id="s_college_id" value="' + collegeId + '">' +
            '<input type="hidden" name="s_department_id" id="s_department_id" value="' + departmentId + '">' +
            '<input type="hidden" name="s_degree_id" id="s_degree_id" value="' + degreeId + '">' +
            '<input type="hidden" name="s_course_id" id="s_course_id" value="' + courseId + '">' +
            '<input type="hidden" name="filter_level" id="university_id" value="' + filterLevel + '">' +
            '<input type="hidden" name="update_field" id="update_field" value="' + databaseFieldName + '">' +
            '<input type="hidden" name="array_count" id="array_count" value="' + arrayCount + '">';

          result.course_list.forEach(function(value, index) {

            var fieldValueDatabase = value[databaseFieldName] && value[databaseFieldName] != null ? value[databaseFieldName] : fieldValue;

            selectHTML += '<div class="row">' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">College Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.college_name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">Course Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">Degree Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.degree_name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee_amount" class="col-sm-4 control-label">' + displayFieldName + ' : </label>' +
              '<div class="col-sm-8">' +
              '<input type="text" class="form-control" id="' + databaseFieldName + index + '" name="' + databaseFieldName + index + '" value="' + fieldValueDatabase + '">' +
              '<input type="hidden" name="course_id' + index + '" id="course_id' + index + '" value="' + value.id + '">' +
              '</div>' +
              '</div>' +
              '</div>' +
              '<hr>';

          });
        }

        selectHTML += '<div class="col-md-12">' +
          '<button type="submit" class="btn btn-info pull-right">Update</button>' +
          '</div>';
        selectHTML += '</form>';

        document.getElementById("display_body").innerHTML = selectHTML;


      }

    });

  }

  function allRadioUpdate(displayFieldName, fieldId, databaseFieldName) {

    var filterLevel = <?= $filter_level ?>;

    var universityId = $('#university').val();
    var campusId = $('#campus').val();
    var collegeId = $('#college').val();
    var departmentId = $('#department').val();
    var degreeId = $('#degree').val();
    var courseId = <?= $id ?>;
    var fieldValue = $('input[name="' + fieldId + '"]:checked').val(); //$('#'+fieldId).val();

    console.log(fieldId, fieldValue);

    var sataString = {
      "university_id": universityId,
      "campus_id": campusId,
      "college_id": collegeId,
      "department_id": departmentId,
      "degree_id": degreeId,
      "course_id": courseId,
      "filter_level": filterLevel,
      "database_field_name": databaseFieldName
    };

    $("#ajaxSpinnerImage").show();

    event.preventDefault();

    $.ajax({

      type: "POST",

      url: "/admin/user/course/filterCourseList",

      data: sataString,

      cache: false,

      success: function(result) {

        $("#ajaxSpinnerImage").hide();

        $('[id*="modalChangeValue"]').modal('show');

        if (result.course_list.length) {

          var arrayCount = result.course_list.length;

          var selectHTML = '<form name="updateForm" autocomplete="off" action="<?php echo base_url(); ?>user/course/multipleUpdate" method="post" >' +
            '<input type="hidden" name="s_university_id" id="s_university_id" value="' + universityId + '">' +
            '<input type="hidden" name="s_campus_id" id="s_campus_id" value="' + campusId + '">' +
            '<input type="hidden" name="s_college_id" id="s_college_id" value="' + collegeId + '">' +
            '<input type="hidden" name="s_department_id" id="s_department_id" value="' + departmentId + '">' +
            '<input type="hidden" name="s_degree_id" id="s_degree_id" value="' + degreeId + '">' +
            '<input type="hidden" name="s_course_id" id="s_course_id" value="' + courseId + '">' +
            '<input type="hidden" name="filter_level" id="filter_level" value="' + filterLevel + '">' +
            '<input type="hidden" name="update_field" id="update_field" value="' + databaseFieldName + '">' +
            '<input type="hidden" name="array_count" id="array_count" value="' + arrayCount + '">';

          result.course_list.forEach(function(value, index) {

            var fieldValueDatabase = value[databaseFieldName] && value[databaseFieldName] != null ? value[databaseFieldName] : fieldValue;
            var checkedYes = "checked";
            var checkedNo = "checked";

            if (fieldValueDatabase == 'Yes') {
              checkedNo = "";
            } else {
              checkedYes = "";
            }

            selectHTML += '<div class="row">' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">College Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.college_name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">Course Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">Degree Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.degree_name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee_amount" class="col-sm-4 control-label">' + displayFieldName + ' : </label>' +
              '<div class="col-sm-8">' +
              '<label>Yes &nbsp;</label><input type="radio" name="' + databaseFieldName + index + '" value="Yes" ' + checkedYes + '> ' +
              '<label>No &nbsp;</label><input type="radio" name="' + databaseFieldName + index + '" value="No" ' + checkedNo + '> ' +
              '<input type="hidden" name="course_id' + index + '" id="course_id' + index + '" value="' + value.id + '">' +
              '</div>' +
              '</div>' +
              '</div>' +
              '<hr>';

          });
        }

        selectHTML += '<div class="col-md-12">' +
          '<button type="submit" class="btn btn-info pull-right">Update</button>' +
          '</div>';
        selectHTML += '</form>';

        document.getElementById("display_body").innerHTML = selectHTML;

      }

    });

  }

  function gpaUpdate(displayFieldName1, fieldId1, displayFieldName2, fieldId2) {

    var filterLevel = <?= $filter_level ?>;

    var universityId = $('#university').val();
    var campusId = $('#campus').val();
    var collegeId = $('#college').val();
    var departmentId = $('#department').val();
    var degreeId = $('#degree').val();
    var courseId = <?= $id ?>;
    var fieldValue1 = $('#' + fieldId1).val();
    var fieldValue2 = $('#' + fieldId2).val();

    var sataString = {
      "university_id": universityId,
      "campus_id": campusId,
      "college_id": collegeId,
      "department_id": departmentId,
      "degree_id": degreeId,
      "course_id": courseId,
      "filter_level": filterLevel,
      "database_field_name_one": fieldId1,
      "database_field_name_two": fieldId2
    };

    $("#ajaxSpinnerImage").show();

    event.preventDefault();

    $.ajax({

      type: "POST",

      url: "/admin/user/course/filterCourseList",

      data: sataString,

      cache: false,

      success: function(result) {

        $("#ajaxSpinnerImage").hide();

        $('[id*="modalChangeValue"]').modal('show');

        if (result.course_list.length) {

          var arrayCount = result.course_list.length;

          var selectHTML = '<form name="updateForm" autocomplete="off" action="<?php echo base_url(); ?>user/course/multipleUpdateCondition" method="post" >' +
            '<input type="hidden" name="s_university_id" id="s_university_id" value="' + universityId + '">' +
            '<input type="hidden" name="s_campus_id" id="s_campus_id" value="' + campusId + '">' +
            '<input type="hidden" name="s_college_id" id="s_college_id" value="' + collegeId + '">' +
            '<input type="hidden" name="s_department_id" id="s_department_id" value="' + departmentId + '">' +
            '<input type="hidden" name="s_degree_id" id="s_degree_id" value="' + degreeId + '">' +
            '<input type="hidden" name="s_course_id" id="s_course_id" value="' + courseId + '">' +
            '<input type="hidden" name="filter_level" id="filter_level" value="' + filterLevel + '">' +
            '<input type="hidden" name="update_field_one" id="update_field_one" value="' + fieldId1 + '">' +
            '<input type="hidden" name="update_field_two" id="update_field_two" value="' + fieldId2 + '">' +
            '<input type="hidden" name="array_count" id="array_count" value="' + arrayCount + '">';

          result.course_list.forEach(function(value, index) {

            var fieldValueDatabase1 = value[fieldId1] && value[fieldId1] != null ? value[fieldId1] : "";
            var fieldValueDatabase2 = value[fieldId2] && value[fieldId2] != null ? value[fieldId2] : "";

            var selecte1 = "";
            var selecte2 = "";
            var selecte3 = "";
            var selecte4 = "";

            if (fieldValueDatabase2 == 4) {
              selecte1 = "selected";
            }

            if (fieldValueDatabase2 == 5) {
              selecte2 = "selected";
            }

            if (fieldValueDatabase2 == 10) {
              selecte3 = "selected";
            }

            if (fieldValueDatabase2 == 100) {
              selecte4 = "selected";
            }


            selectHTML += '<div class="row">' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">College Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.college_name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">Course Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">Degree Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.degree_name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee_amount" class="col-sm-4 control-label">' + displayFieldName1 + ' : </label>' +
              '<div class="col-sm-8">' +
              '<input type="text" class="form-control" id="' + fieldId1 + index + '" name="' + fieldId1 + index + '" value="' + fieldValueDatabase1 + '">' +
              '<input type="hidden" name="course_id' + index + '" id="course_id' + index + '" value="' + value.id + '">' +
              '</div>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee_amount" class="col-sm-4 control-label">' + displayFieldName2 + ' : </label>' +
              '<div class="col-sm-8">' +
              '<select class="form-control" name="' + fieldId2 + index + '" id="' + fieldId2 + index + '">' +
              '<option value=""> Select Option</option>' +
              '<option value="4" ' + selecte1 + '>4.0</option>' +
              '<option value="5" ' + selecte2 + '>5.0</option>' +
              '<option value="10" ' + selecte3 + '>10</option>' +
              '<option value="100" ' + selecte4 + '>100</option>' +
              '</select>' +
              '</div>' +
              '</div>' +
              '</div>' +
              '<hr>';


          });

        }

        selectHTML += '<div class="col-md-12">' +
          '<button type="submit" class="btn btn-info pull-right">Update</button>' +
          '</div>';
        selectHTML += '</form>';

        document.getElementById("display_body").innerHTML = selectHTML;

      }

    });

  }

  function greUpdate(displayFieldName1, fieldId1, displayFieldName2, fieldId2, jsonField) {

    var filterLevel = <?= $filter_level ?>;

    var universityId = $('#university').val();
    var campusId = $('#campus').val();
    var collegeId = $('#college').val();
    var departmentId = $('#department').val();
    var degreeId = $('#degree').val();
    var courseId = <?= $id ?>;
    var fieldValue1 = $('#' + fieldId1).val();
    var fieldValue2 = $('#' + fieldId2).val();

    var sataString = {
      "university_id": universityId,
      "campus_id": campusId,
      "college_id": collegeId,
      "department_id": departmentId,
      "degree_id": degreeId,
      "course_id": courseId,
      "filter_level": filterLevel,
      "database_field_name": jsonField,
    };

    $("#ajaxSpinnerImage").show();

    event.preventDefault();

    $.ajax({

      type: "POST",

      url: "/admin/user/course/filterCourseList",

      data: sataString,

      cache: false,

      success: function(result) {

        $("#ajaxSpinnerImage").hide();

        $('[id*="modalChangeValue"]').modal('show');

        if (result.course_list.length) {

          var arrayCount = result.course_list.length;

          var selectHTML = '<form name="updateForm" autocomplete="off" action="<?php echo base_url(); ?>user/course/multipleUpdateCondition" method="post" >' +
            '<input type="hidden" name="s_university_id" id="s_university_id" value="' + universityId + '">' +
            '<input type="hidden" name="s_campus_id" id="s_campus_id" value="' + campusId + '">' +
            '<input type="hidden" name="s_college_id" id="s_college_id" value="' + collegeId + '">' +
            '<input type="hidden" name="s_department_id" id="s_department_id" value="' + departmentId + '">' +
            '<input type="hidden" name="s_degree_id" id="s_degree_id" value="' + degreeId + '">' +
            '<input type="hidden" name="s_course_id" id="s_course_id" value="' + courseId + '">' +
            '<input type="hidden" name="filter_level" id="filter_level" value="' + filterLevel + '">' +
            '<input type="hidden" name="update_json" id="update_json" value="' + jsonField + '">' +
            '<input type="hidden" name="update_field_one" id="update_field_one" value="' + fieldId1 + '">' +
            '<input type="hidden" name="update_field_two" id="update_field_two" value="' + fieldId2 + '">' +
            '<input type="hidden" name="array_count" id="array_count" value="' + arrayCount + '">';

          result.course_list.forEach(function(value, index) {

            //console.log(value[fieldId1],value[fieldId1]['quant'])

            var fieldValueDatabase1 = typeof(value[jsonField]) != "undefined" && value[jsonField] !== null ? value[jsonField][fieldId1] : fieldValue1;
            var fieldValueDatabase2 = typeof(value[jsonField]) != "undefined" && value[jsonField] !== null ? value[jsonField][fieldId2] : fieldValue2;

            selectHTML += '<div class="row">' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">College Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.college_name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">Course Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">Degree Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.degree_name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee_amount" class="col-sm-4 control-label">' + displayFieldName1 + ' : </label>' +
              '<div class="col-sm-8">' +
              '<input type="text" class="form-control" id="quant' + index + '" name="quant' + index + '" value="' + fieldValueDatabase1 + '">' +
              '<input type="hidden" name="course_id' + index + '" id="course_id' + index + '" value="' + value.id + '">' +
              '</div>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee_amount" class="col-sm-4 control-label">' + displayFieldName2 + ' : </label>' +
              '<div class="col-sm-8">' +
              '<input type="text" class="form-control" id="verbal' + index + '" name="verbal' + index + '" value="' + fieldValueDatabase2 + '">' +
              '</div>' +
              '</div>' +
              '</div>' +
              '<hr>';


          });

        }

        selectHTML += '<div class="col-md-12">' +
          '<button type="submit" class="btn btn-info pull-right">Update</button>' +
          '</div>';
        selectHTML += '</form>';

        document.getElementById("display_body").innerHTML = selectHTML;

      }

    });

  }

  function allRadioDependencyUpdate(displayFieldName1, displayFieldName2, fieldId1, fieldId2, databaseFieldName1, databaseFieldName2) {

    var filterLevel = <?= $filter_level ?>;

    var universityId = $('#university').val();
    var campusId = $('#campus').val();
    var collegeId = $('#college').val();
    var departmentId = $('#department').val();
    var degreeId = $('#degree').val();
    var courseId = <?= $id ?>;
    var fieldValue1 = $('input[name="' + fieldId1 + '"]:checked').val();
    var fieldValue2 = $('#' + fieldId2).val();

    console.log(fieldValue1, fieldId1);

    var sataString = {
      "university_id": universityId,
      "campus_id": campusId,
      "college_id": collegeId,
      "department_id": departmentId,
      "degree_id": degreeId,
      "course_id": courseId,
      "filter_level": filterLevel,
      "database_field_name_one": fieldId1,
      "database_field_name_two": fieldId2
    };

    $("#ajaxSpinnerImage").show();

    event.preventDefault();

    $.ajax({

      type: "POST",

      url: "/admin/user/course/filterCourseList",

      data: sataString,

      cache: false,

      success: function(result) {

        $("#ajaxSpinnerImage").hide();

        $('[id*="modalChangeValue"]').modal('show');

        if (result.course_list.length) {

          var arrayCount = result.course_list.length;

          var selectHTML = '<form name="updateForm" autocomplete="off" action="<?php echo base_url(); ?>user/course/multipleUpdateCondition" method="post" >' +
            '<input type="hidden" name="s_university_id" id="s_university_id" value="' + universityId + '">' +
            '<input type="hidden" name="s_campus_id" id="s_campus_id" value="' + campusId + '">' +
            '<input type="hidden" name="s_college_id" id="s_college_id" value="' + collegeId + '">' +
            '<input type="hidden" name="s_department_id" id="s_department_id" value="' + departmentId + '">' +
            '<input type="hidden" name="s_degree_id" id="s_degree_id" value="' + degreeId + '">' +
            '<input type="hidden" name="s_course_id" id="s_course_id" value="' + courseId + '">' +
            '<input type="hidden" name="filter_level" id="filter_level" value="' + filterLevel + '">' +
            '<input type="hidden" name="update_field_one" id="update_field_one" value="' + fieldId1 + '">' +
            '<input type="hidden" name="update_field_two" id="update_field_two" value="' + fieldId2 + '">' +
            '<input type="hidden" name="array_count" id="array_count" value="' + arrayCount + '">';

          result.course_list.forEach(function(value, index) {

            var fieldValueDatabase1 = value[fieldId1] && value[fieldId1] != null ? value[fieldId1] : fieldValue1;
            var checkedYes = "checked";
            var checkedNo = "checked";
            var displayType = "block";

            if (fieldValueDatabase1 == 'Yes') {
              checkedNo = "";
            } else {
              checkedYes = "";
              displayType = "none"
            }

            var fieldValueDatabase2 = value[fieldId2] && value[fieldId2] != null ? value[fieldId2] : fieldValue2;

            selectHTML += '<div class="row">' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">College Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.college_name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">Course Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">Degree Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.degree_name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee_amount" class="col-sm-4 control-label">' + displayFieldName1 + ' : </label>' +
              '<div class="col-sm-8">' +
              '<label>Yes &nbsp;</label><input type="radio" name="' + fieldId1 + index + '" value="Yes" ' + checkedYes + ' onclick="showDynamicOne(\'' + fieldId2 + index + filterLevel + '\')"> ' +
              '<label>No &nbsp;</label><input type="radio" name="' + fieldId1 + index + '" value="No" ' + checkedNo + ' onclick="hideDynamicOne(\'' + fieldId2 + index + filterLevel + '\')"> ' +
              '</div>' +
              '</div>' +
              '<div class="col-md-12" id="' + fieldId2 + index + filterLevel + '" style="display:' + displayType + ';">' +
              '<label for="application_fee_amount" class="col-sm-4 control-label">' + displayFieldName2 + ' : </label>' +
              '<div class="col-sm-8">' +
              '<input type="text" class="form-control" id="' + fieldId2 + index + '" name="' + fieldId2 + index + '" value="' + fieldValueDatabase2 + '">' +
              '<input type="hidden" name="course_id' + index + '" id="course_id' + index + '" value="' + value.id + '">' +
              '</div>' +
              '</div>' +
              '</div>' +
              '<hr>';

          });
        }

        selectHTML += '<div class="col-md-12">' +
          '<button type="submit" class="btn btn-info pull-right">Update</button>' +
          '</div>';
        selectHTML += '</form>';

        document.getElementById("display_body").innerHTML = selectHTML;

      }

    });

  }


  function allRadioDependencyUpdateJson(displayFieldName1, displayFieldName2, fieldId1, fieldId2, databaseFieldName1, databaseFieldName2, jsonField) {

    var filterLevel = <?= $filter_level ?>;

    var universityId = $('#university').val();
    var campusId = $('#campus').val();
    var collegeId = $('#college').val();
    var departmentId = $('#department').val();
    var degreeId = $('#degree').val();
    var courseId = <?= $id ?>;
    var fieldValue1 = $('input[name="' + fieldId1 + '"]:checked').val();;
    var fieldValue2 = $('#' + fieldId2).val();

    var sataString = {
      "university_id": universityId,
      "campus_id": campusId,
      "college_id": collegeId,
      "department_id": departmentId,
      "degree_id": degreeId,
      "course_id": courseId,
      "filter_level": filterLevel,
      "database_field_name": jsonField
    };

    $("#ajaxSpinnerImage").show();

    event.preventDefault();

    $.ajax({

      type: "POST",

      url: "/admin/user/course/filterCourseList",

      data: sataString,

      cache: false,

      success: function(result) {

        $("#ajaxSpinnerImage").hide();

        $('[id*="modalChangeValue"]').modal('show');

        if (result.course_list.length) {

          var arrayCount = result.course_list.length;

          var selectHTML = '<form name="updateForm" autocomplete="off" action="<?php echo base_url(); ?>user/course/multipleUpdateCondition" method="post" >' +
            '<input type="hidden" name="s_university_id" id="s_university_id" value="' + universityId + '">' +
            '<input type="hidden" name="s_campus_id" id="s_campus_id" value="' + campusId + '">' +
            '<input type="hidden" name="s_college_id" id="s_college_id" value="' + collegeId + '">' +
            '<input type="hidden" name="s_department_id" id="s_department_id" value="' + departmentId + '">' +
            '<input type="hidden" name="s_degree_id" id="s_degree_id" value="' + degreeId + '">' +
            '<input type="hidden" name="s_course_id" id="s_course_id" value="' + courseId + '">' +
            '<input type="hidden" name="filter_level" id="filter_level" value="' + filterLevel + '">' +
            '<input type="hidden" name="update_json" id="update_json" value="' + jsonField + '">' +
            '<input type="hidden" name="update_field_one" id="update_field_one" value="' + fieldId1 + '">' +
            '<input type="hidden" name="update_field_two" id="update_field_two" value="' + fieldId2 + '">' +
            '<input type="hidden" name="array_count" id="array_count" value="' + arrayCount + '">';

          result.course_list.forEach(function(value, index) {

            var fieldValueDatabase1 = typeof(value[jsonField]) != "undefined" && value[jsonField] !== null ? value[jsonField][fieldId1] : fieldValue1;
            var fieldValueDatabase2 = typeof(value[jsonField]) != "undefined" && value[jsonField] !== null ? value[jsonField][fieldId2] : fieldValue2;

            var checkedYes = "checked";
            var checkedNo = "checked";
            var displayType = "block";

            if (fieldValueDatabase1 == '1') {
              checkedNo = "";
            } else {
              checkedYes = "";
              displayType = "none";
            }

            selectHTML += '<div class="row">' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">College Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.college_name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">Course Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">Degree Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.degree_name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee_amount" class="col-sm-4 control-label">' + displayFieldName1 + ' : </label>' +
              '<div class="col-sm-8">' +
              '<label>Yes &nbsp;</label><input type="radio" name="' + fieldId1 + index + '" value="1" ' + checkedYes + ' onclick="showDynamicOne(\'' + jsonField + fieldId2 + index + '\')"> ' +
              '<label>No &nbsp;</label><input type="radio" name="' + fieldId1 + index + '" value="0" ' + checkedNo + ' onclick="hideDynamicOne(\'' + jsonField + fieldId2 + index + '\')"> ' +
              '</div>' +
              '</div>' +
              '<div class="col-md-12" id="' + jsonField + fieldId2 + index + '" style="display:' + displayType + ';">' +
              '<label for="application_fee_amount" class="col-sm-4 control-label">' + displayFieldName2 + ' : </label>' +
              '<div class="col-sm-8">' +
              '<input type="text" class="form-control" id="' + fieldId2 + index + '" name="' + fieldId2 + index + '" value="' + fieldValueDatabase2 + '">' +
              '<input type="hidden" name="course_id' + index + '" id="course_id' + index + '" value="' + value.id + '">' +
              '</div>' +
              '</div>' +
              '</div>' +
              '<hr>';

          });
        }

        selectHTML += '<div class="col-md-12">' +
          '<button type="submit" class="btn btn-info pull-right">Update</button>' +
          '</div>';
        selectHTML += '</form>';

        document.getElementById("display_body").innerHTML = selectHTML;

      }

    });

  }

  function LORUpdateJson(displayFieldName1, displayFieldName2, displayFieldName3, fieldId1, fieldId2, fieldId3, databaseFieldName1, databaseFieldName2, databaseFieldName3, jsonField) {

    var filterLevel = <?= $filter_level ?>;

    var universityId = $('#university').val();
    var campusId = $('#campus').val();
    var collegeId = $('#college').val();
    var departmentId = $('#department').val();
    var degreeId = $('#degree').val();
    var courseId = <?= $id ?>;
    var fieldValue1 = $('input[name="' + fieldId1 + '"]:checked').val();
    var fieldValue2 = $('#' + fieldId2).val();
    var fieldValue3 = $('input[name="' + fieldId3 + '"]:checked').val();

    console.log(fieldValue1, fieldValue2, fieldValue3);

    var sataString = {
      "university_id": universityId,
      "campus_id": campusId,
      "college_id": collegeId,
      "department_id": departmentId,
      "degree_id": degreeId,
      "course_id": courseId,
      "filter_level": filterLevel,
      "database_field_name": jsonField
    };

    $("#ajaxSpinnerImage").show();

    event.preventDefault();

    $.ajax({

      type: "POST",

      url: "/admin/user/course/filterCourseList",

      data: sataString,

      cache: false,

      success: function(result) {

        $("#ajaxSpinnerImage").hide();

        $('[id*="modalChangeValue"]').modal('show');

        if (result.course_list.length) {

          var arrayCount = result.course_list.length;

          var selectHTML = '<form name="updateForm" autocomplete="off" action="<?php echo base_url(); ?>user/course/multipleUpdateCondition" method="post" >' +
            '<input type="hidden" name="s_university_id" id="s_university_id" value="' + universityId + '">' +
            '<input type="hidden" name="s_campus_id" id="s_campus_id" value="' + campusId + '">' +
            '<input type="hidden" name="s_college_id" id="s_college_id" value="' + collegeId + '">' +
            '<input type="hidden" name="s_department_id" id="s_department_id" value="' + departmentId + '">' +
            '<input type="hidden" name="s_degree_id" id="s_degree_id" value="' + degreeId + '">' +
            '<input type="hidden" name="s_course_id" id="s_course_id" value="' + courseId + '">' +
            '<input type="hidden" name="filter_level" id="filter_level" value="' + filterLevel + '">' +
            '<input type="hidden" name="update_json" id="update_json" value="' + jsonField + '">' +
            '<input type="hidden" name="update_field_one" id="update_field_one" value="' + fieldId1 + '">' +
            '<input type="hidden" name="update_field_two" id="update_field_two" value="' + fieldId2 + '">' +
            '<input type="hidden" name="update_field_three" id="update_field_three" value="' + fieldId3 + '">' +
            '<input type="hidden" name="array_count" id="array_count" value="' + arrayCount + '">';

          result.course_list.forEach(function(value, index) {

            var fieldValueDatabase1 = typeof(value[jsonField]) != "undefined" && value[jsonField] !== null ? value[jsonField][fieldId1] : fieldValue1;
            var fieldValueDatabase2 = typeof(value[jsonField]) != "undefined" && value[jsonField] !== null ? value[jsonField][fieldId2] : fieldValue2;
            var fieldValueDatabase3 = typeof(value[jsonField]) != "undefined" && value[jsonField] !== null ? value[jsonField][fieldId3] : fieldValue3;

            var checkedYes = "checked";
            var checkedNo = "checked";
            var displayType1 = "block";
            var displayType2 = "block";

            var checkedOff = "checked";
            var checkedOn = "checked";
            var checkedAfter = "checked";

            if (fieldValueDatabase1 == '1') {
              checkedNo = "";
            } else {
              checkedYes = "";
              displayType1 = "none";
              displayType2 = "none";
            }

            if (fieldValueDatabase3 == 'offline') {
              checkedOn = "";
              checkedAfter = "";
            } else if (fieldValueDatabase3 == 'online') {
              checkedOff = "";
              checkedAfter = "";
            } else {
              checkedOff = "";
              checkedOn = "";
            }

            var selecte1 = "";
            var selecte2 = "";
            var selecte3 = "";
            var selecte4 = "";
            var selecte5 = "";
            var selecte6 = "";
            var selecte7 = "";
            var selecte8 = "";
            var selecte9 = "";
            var selecte10 = "";


            if (fieldValueDatabase2 == 1) {
              selecte1 = "selected";
            }

            if (fieldValueDatabase2 == 2) {
              selecte2 = "selected";
            }

            if (fieldValueDatabase2 == 3) {
              selecte3 = "selected";
            }

            if (fieldValueDatabase2 == 4) {
              selecte4 = "selected";
            }

            if (fieldValueDatabase2 == 5) {
              selecte5 = "selected";
            }

            if (fieldValueDatabase2 == 6) {
              selecte6 = "selected";
            }

            if (fieldValueDatabase2 == 7) {
              selecte7 = "selected";
            }

            if (fieldValueDatabase2 == 8) {
              selecte8 = "selected";
            }

            if (fieldValueDatabase2 == 9) {
              selecte9 = "selected";
            }

            if (fieldValueDatabase2 == 10) {
              selecte10 = "selected";
            }

            selectHTML += '<div class="row">' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">College Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.college_name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">Course Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">Degree Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.degree_name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee_amount" class="col-sm-4 control-label">' + displayFieldName1 + ' : </label>' +
              '<div class="col-sm-8">' +
              '<label>Yes &nbsp;</label><input type="radio" name="' + fieldId1 + index + '" value="1" ' + checkedYes + ' onclick="showDynamicTwo(\'' + jsonField + fieldId2 + index + '\',\'' + jsonField + fieldId3 + index + '\')"> ' +
              '<label>No &nbsp;</label><input type="radio" name="' + fieldId1 + index + '" value="0" ' + checkedNo + ' onclick="hideDynamicTwo(\'' + jsonField + fieldId2 + index + '\',\'' + jsonField + fieldId3 + index + '\')"> ' +
              '</div>' +
              '</div>' +
              '<div class="col-md-12" id="' + jsonField + fieldId2 + index + '" style="display:' + displayType1 + ';">' +
              '<label for="application_fee_amount" class="col-sm-4 control-label">' + displayFieldName3 + ' : </label>' +
              '<div class="col-sm-8">' +
              '<select class="form-control" name="' + fieldId2 + index + '" id="' + fieldId2 + index + '">' +
              '<option value=""> Select Option</option>' +
              '<option value="1" ' + selecte1 + '>1</option>' +
              '<option value="2" ' + selecte2 + '>2</option>' +
              '<option value="3" ' + selecte3 + '>3</option>' +
              '<option value="4" ' + selecte4 + '>4</option>' +
              '<option value="5" ' + selecte5 + '>5</option>' +
              '<option value="6" ' + selecte6 + '>6</option>' +
              '<option value="7" ' + selecte7 + '>7</option>' +
              '<option value="8" ' + selecte8 + '>8</option>' +
              '<option value="9" ' + selecte9 + '>9</option>' +
              '<option value="10" ' + selecte10 + '>10</option>' +
              '</select>' +
              '</div>' +
              '</div>' +
              '<div class="col-md-12" id="' + jsonField + fieldId3 + index + '" style="display:' + displayType2 + ';">' +
              '<label for="application_fee_amount" class="col-sm-4 control-label">' + displayFieldName3 + ' : </label>' +
              '<div class="col-sm-8">' +
              '<label>Offline &nbsp;</label><input type="radio" name="' + fieldId3 + index + '" value="offline" ' + checkedOff + '> ' +
              '<label>Online &nbsp;</label><input type="radio" name="' + fieldId3 + index + '" value="online" ' + checkedOn + '> ' +
              '<label>Online (After Application) &nbsp;</label><input type="radio" name="' + fieldId3 + index + '" value="online_after_application" ' + checkedAfter + '> ' +
              '<input type="hidden" name="course_id' + index + '" id="course_id' + index + '" value="' + value.id + '">' +
              '</div>' +
              '</div>' +
              '</div>' +
              '<hr>';

          });
        }

        selectHTML += '<div class="col-md-12">' +
          '<button type="submit" class="btn btn-info pull-right">Update</button>' +
          '</div>';
        selectHTML += '</form>';

        document.getElementById("display_body").innerHTML = selectHTML;

      }

    });

  }

  function scholarshipUpdate(displayFieldName1, displayFieldName2, displayFieldName3, displayFieldName4, displayFieldName5, displayFieldName6, fieldId1, fieldId2, fieldId3, fieldId4, fieldId5, fieldId6) {

    var filterLevel = <?= $filter_level ?>;

    var universityId = $('#university').val();
    var campusId = $('#campus').val();
    var collegeId = $('#college').val();
    var departmentId = $('#department').val();
    var degreeId = $('#degree').val();
    var courseId = <?= $id ?>;
    var fieldValue1 = $('input[name="' + fieldId1 + '"]:checked').val();
    var fieldValue2 = $('#' + fieldId2).val();
    var fieldValue3 = $('#' + fieldId3).val();
    var fieldValue4 = $('#' + fieldId4).val();
    var fieldValue5 = $('#' + fieldId5).val();
    var fieldValue6 = $('#' + fieldId6).val();

    var sataString = {
      "university_id": universityId,
      "campus_id": campusId,
      "college_id": collegeId,
      "department_id": departmentId,
      "degree_id": degreeId,
      "course_id": courseId,
      "filter_level": filterLevel,
      "database_field_name": "SCHOLARSHIPDETAIL"
    };

    $("#ajaxSpinnerImage").show();

    event.preventDefault();

    $.ajax({

      type: "POST",

      url: "/admin/user/course/filterCourseList",

      data: sataString,

      cache: false,

      success: function(result) {

        $("#ajaxSpinnerImage").hide();

        $('[id*="modalChangeValue"]').modal('show');

        if (result.course_list.length) {

          var arrayCount = result.course_list.length;

          var selectHTML = '<form name="updateForm" autocomplete="off" action="<?php echo base_url(); ?>user/course/multipleUpdateCondition" method="post" >' +
            '<input type="hidden" name="s_university_id" id="s_university_id" value="' + universityId + '">' +
            '<input type="hidden" name="s_campus_id" id="s_campus_id" value="' + campusId + '">' +
            '<input type="hidden" name="s_college_id" id="s_college_id" value="' + collegeId + '">' +
            '<input type="hidden" name="s_department_id" id="s_department_id" value="' + departmentId + '">' +
            '<input type="hidden" name="s_degree_id" id="s_degree_id" value="' + degreeId + '">' +
            '<input type="hidden" name="s_course_id" id="s_course_id" value="' + courseId + '">' +
            '<input type="hidden" name="filter_level" id="filter_level" value="' + filterLevel + '">' +
            '<input type="hidden" name="array_count" id="array_count" value="' + arrayCount + '">';

          result.course_list.forEach(function(value, index) {

            var fieldValueDatabase1 = value[fieldId1] && value[fieldId1] != null ? value[fieldId1] : fieldValue1;
            var fieldValueDatabase2 = value[fieldId2] && value[fieldId2] != null ? value[fieldId2] : fieldValue2;
            var fieldValueDatabase3 = value[fieldId3] && value[fieldId3] != null ? value[fieldId3] : fieldValue3;
            var fieldValueDatabase4 = value[fieldId4] && value[fieldId4] != null ? value[fieldId4] : fieldValue4;
            var fieldValueDatabase5 = value[fieldId5] && value[fieldId5] != null ? value[fieldId5] : fieldValue5;
            var fieldValueDatabase6 = value[fieldId6] && value[fieldId6] != null ? value[fieldId6] : fieldValue6;

            var checkedYes = "checked";
            var checkedNo = "checked";
            var displayType = "block";

            if (fieldValueDatabase1 == 'Yes') {
              checkedNo = "";
            } else {
              checkedYes = "";
              displayType = "none";
            }

            selectHTML += '<div class="row">' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">College Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.college_name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">Course Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee" class="control-label col-sm-5">Degree Name : </label>' +
              '<h5 class="modal-title col-sm-5">' + value.degree_name + '</h5>' +
              '</div>' +
              '<div class="col-md-12">' +
              '<label for="application_fee_amount" class="col-sm-4 control-label">' + displayFieldName1 + ' : </label>' +
              '<div class="col-sm-8">' +
              '<label>Yes &nbsp;</label><input type="radio" name="' + fieldId1 + index + '" value="Yes" ' + checkedYes + ' onclick="showDynamicOne(\'scholarship_detail_div' + index + '\')"> ' +
              '<label>No &nbsp;</label><input type="radio" name="' + fieldId1 + index + '" value="No" ' + checkedNo + ' onclick="hideDynamicOne(\'scholarship_detail_div' + index + '\')"> ' +
              '</div>' +
              '</div>' +
              '<div id="scholarship_detail_div' + index + '" style="display:' + displayType + '">' +
              '<div class="form-group">' +
              '<label for="expected_salary" class="col-sm-4 control-label"> Tuition Waivers </label>' +
              '<div class="col-sm-8">' +
              '<input type="text" class="form-control" id="tuition_waivers' + index + '" name="tuition_waivers' + index + '" placeholder="Tuition waivers" value="' + fieldValueDatabase2 + '">' +
              '</div>' +
              '</div>' +
              '<div class="form-group">' +
              '<label for="expected_salary" class="col-sm-4 control-label"> Research Assistantships </label>' +
              '<div class="col-sm-8">' +
              '<input type="text" class="form-control" id="research_assistantships' + index + '" name="research_assistantships' + index + '" placeholder="Research Assistantships" value="' + fieldValueDatabase3 + '">' +
              '</div>' +
              '</div>' +
              '<div class="form-group">' +
              '<label for="expected_salary" class="col-sm-4 control-label"> Teaching Assistantships </label>' +
              '<div class="col-sm-8">' +
              '<input type="text" class="form-control" id="teaching_assistantships' + index + '" name="teaching_assistantships' + index + '" placeholder="Teaching Assistantships" value="' + fieldValueDatabase4 + '">' +
              '</div>' +
              '</div>' +
              '<div class="form-group">' +
              '<label for="expected_salary" class="col-sm-4 control-label"> Graduate Assistantships </label>' +
              '<div class="col-sm-8">' +
              '<input type="text" class="form-control" id="graduate_assistantships' + index + '" name="graduate_assistantships' + index + '" placeholder="Graduate Assistantships" value="' + fieldValueDatabase5 + '">' +
              '</div>' +
              '</div>' +
              '<div class="form-group">' +
              '<label for="expected_salary" class="col-sm-4 control-label"> Others - link </label>' +
              '<div class="col-sm-8">' +
              '<input type="text" class="form-control" id="others_link' + index + '" name="others_link' + index + '" placeholder="Others - link" value="' + fieldValueDatabase6 + '">' +
              '</div>' +
              '</div>' +
              '</div>' +
              '</div>' +
              '<hr>';

          });
        }

        selectHTML += '<div class="col-md-12">' +
          '<button type="submit" class="btn btn-info pull-right">Update</button>' +
          '</div>';
        selectHTML += '</form>';

        document.getElementById("display_body").innerHTML = selectHTML;

      }

    });

  }

  function showDynamicOne(dynamicId) {
    document.getElementById(dynamicId).style.display = "block";
  }

  function hideDynamicOne(dynamicId) {
    document.getElementById(dynamicId).style.display = "none";
  }

  function showDynamicTwo(dynamicId1, dynamicId2) {
    document.getElementById(dynamicId1).style.display = "block";
    document.getElementById(dynamicId2).style.display = "block";
  }

  function hideDynamicTwo(dynamicId1, dynamicId2) {
    document.getElementById(dynamicId1).style.display = "none";
    document.getElementById(dynamicId2).style.display = "none";
  }
</script>

<script type="text/javascript">
  var totalFeesInternational = <?= $course_total_fee_count ?>;

  function addFeesInternational() {
    var html = '<div id="course_total_fee_' + (totalFeesInternational + 1) + '" style="margin-top:10px;"><div style="float:left; padding-left: 0px;" class="col-sm-4"><label for="course_total_fee_currency" class="col-sm-3" style="padding-left: 0px;">Currency</label><input type="text" id="course_total_fee_currency_' + (totalFeesInternational + 1) + '" name="course_total_fee_currency[]" placeholder="Currency" value=""></div><div style="float:left; padding-left: 0px;" class="col-sm-4"><label for="course_total_fee_amount" class="col-sm-4" style="padding-left: 0px;">Amount</label><input type="text" id="course_total_fee_amount_' + (totalFeesInternational + 1) + '" name="course_total_fee_amount[]" placeholder="Amount" value=""></div><div ><input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteFeesInternational(' + (totalFeesInternational + 1) + ')"></div></div>';

    $("#course_total_fee_" + totalFeesInternational).after(html);
    totalFeesInternational++;
  }

  function deleteFeesInternational(feesInternationalId) {
    $("#course_total_fee_" + feesInternationalId).remove();
    totalFeesInternational--;
  }
</script>