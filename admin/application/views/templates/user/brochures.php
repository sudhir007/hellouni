<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>University <small>Manage Brochures</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">University Management</a></li>
            <li class="active">Brochures</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php
                if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>

                <div class="box">
                    <form action="<?php echo base_url();?>user/university/multidelete" method="post">
                        <div class="box-header">
                            <a href="<?php echo base_url();?>user/brochure/form"><button type="button" style="width:12%; float:left; margin-right:5px;" class="btn btn-block btn-primary"><i class="fa fa-plus"></i> Add Brochure</button></a>
                            <button type="submit" class="btn btn-block btn-danger multidel" style="width:11%; display:none;"><i class="fa fa-minus"></i> Delete University</button>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Title</th>
                                        <th>Option</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    foreach($brochures as $brochure)
                                    {
                                        ?>
                                        <tr>
                                            <td><input type="checkbox" class="multichk" name="chk[]" value="<?php echo $brochure->id;?>"/></td>
                                            <td><?php echo $brochure->brochure_title;?></td>
                                            <td>
                                                <a href="<?php echo base_url();?>user/brochure/form/<?php echo $brochure->id;?>" title="Modify"><i class="fa fa-edit"></i></a> &nbsp;
                                                <a href="javascript:void();" title="Delete" id="<?php echo $brochure->id;?>"><i class="fa fa-trash-o"></i></a> &nbsp;
                                                <a href="<?php echo $brochure->brochure_file_name;?>" title="Download" target="_blank"><i class="fa fa-download"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th>Title</th>
                                        <th>Option</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
