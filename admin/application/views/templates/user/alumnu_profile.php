<style type="text/css">.tt{ height:0px !important;}
.profileimg{ width:150px; height:150px; }
</style>
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
     <h1>
       University
       <small>Manage Alumnu Profile</small>
     </h1>
     <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
       <li><a href="#">University Management</a></li>
       <li class="active">Alumnu Profile</li>
     </ol>
   </section>

   <!-- Main content -->
   <section class="content">
     <div class="row">

       <!-- right column -->
       <div class="col-md-10">
         <!-- Horizontal Form -->
         <div class="box box-info">
           <div class="box-header with-border">
             <h3 class="box-title">Alumnu Profile</h3>

           </div><!-- /.box-header -->
           <!-- form start -->

           <?php //$userdata = $this->session->userdata;?>
           <form class="form-horizontal" action="<?php echo base_url();?>user/alumnus/update/<?php if(isset($id)) echo $id; ?>" method="post" name="adduniversity" enctype="multipart/form-data">




             <div class="box-body">
               <div class="form-group">

                 <div class="col-sm-10" style="color:red;">
                  <?php if($this->session->flashdata('flash_message')){
                           echo $this->session->flashdata('flash_message');
                   } ?>
                 </div>
               </div>

               <div class="form-group">
                    <label for="course" class="col-sm-2 control-label">Course</label>
                     <div class="col-sm-10">
                     <select class="form-control" name="course_id">
                     <?php foreach($all_courses as $courseData){?>
                       <option value="<?php echo $courseData->id;?>" <?php if(isset($course_id) && $course_id==$courseData->id){ echo 'selected="selected"'; } ?>><?php echo $courseData->name;?></option>

                       <?php } ?>
                     </select>
                     </div>
               </div>

               <div class="form-group">
                 <label for="joining_date" class="col-sm-2 control-label">Joining Date</label>
                 <div class="col-sm-10">
                   <input type="text" class="mws-datepicker small" id="datepicker" name="joining_date" value="<?php if(isset($joining_date)) echo $joining_date; ?>">
                 </div>
               </div>

               <div class="form-group">
                 <label for="passout_date" class="col-sm-2 control-label">Passout Date</label>
                 <div class="col-sm-10">
                   <input type="text" class="mws-datepicker small" id="datepicker_1" name="passout_date" value="<?php if(isset($passout_date)) echo $passout_date; ?>">
                 </div>
               </div>

               <div class="form-group">
                 <label for="my_story" class="col-sm-2 control-label">My Story</label>
                 <div class="col-sm-10">
                   <!--input type="textbox" class="form-control" id="about" name="about" placeholder="Breif Description"-->
                   <textarea rows="4" class="form-control" id="my_story" name="my_story" placeholder="Tell your story before college."><?php if(isset($my_story)) echo $my_story; ?></textarea>
                   <script type="text/javascript">CKEDITOR.replace( 'my_story', {extraPlugins: 'confighelper'} );</script>
                 </div>
               </div>

               <div class="form-group">
                 <label for="my_college" class="col-sm-2 control-label">My College</label>
                 <div class="col-sm-10">
                   <!--input type="textbox" class="form-control" id="about" name="about" placeholder="Breif Description"-->
                   <textarea rows="4" class="form-control" id="my_college" name="my_college" placeholder="Tell about your life in college."><?php if(isset($my_college)) echo $my_college; ?></textarea>
                   <script type="text/javascript">CKEDITOR.replace( 'my_college', {extraPlugins: 'confighelper'} );</script>
                 </div>
               </div>

               <div class="form-group">
                 <label for="current_placement" class="col-sm-2 control-label">My Current Placement</label>
                 <div class="col-sm-10">
                     <textarea rows="4" class="form-control" id="current_placement" name="current_placement" placeholder="Tell about your life after college."><?php if(isset($current_placement)) echo $current_placement; ?></textarea>
                     <script type="text/javascript">CKEDITOR.replace( 'current_placement', {extraPlugins: 'confighelper'} );</script>
                 </div>
               </div>

               <div class="form-group">
                 <label for="current_job_profile" class="col-sm-2 control-label">My Job Profile</label>
                 <div class="col-sm-10">
                   <textarea class="form-control" id="current_job_profile" name="current_job_profile" cols="" rows="5" placeholder="Tell about your life at company."><?php if(isset($current_job_profile)) echo $current_job_profile; ?></textarea>
                   <script type="text/javascript">CKEDITOR.replace( 'current_job_profile', {extraPlugins: 'confighelper'} );</script>
                 </div>
               </div>

               <div class="form-group">
                    <label for="current_salary" class="col-sm-2 control-label">Current Salary</label>
                     <div class="col-sm-10">
                      <input type="text" class="form-control" id="current_salary" name="current_salary" placeholder="Current Salary" value="<?php if(isset($current_salary)){ echo $current_salary; }?>">
                     </div>
               </div>

               <div class="form-group">
                 <label for="my_insights" class="col-sm-2 control-label">My Insights</label>
                 <div class="col-sm-10">
                   <textarea class="form-control" id="my_insights" name="my_insights" cols="" rows="5" placeholder="Tell about your life, how it's evolved and future."><?php if(isset($my_insights)) echo $my_insights; ?></textarea>
                   <script type="text/javascript">CKEDITOR.replace( 'my_insights', {extraPlugins: 'confighelper'} );</script>
                 </div>
               </div>

               <div class="form-group">
                    <label for="connect_with_me" class="col-sm-2 control-label">Connect With Me</label>
                     <div class="col-sm-10">
                      <input type="checkbox"  id="connect_with_me" name="connect_with_me" <?= isset($connect_with_me) && $connect_with_me ? 'checked' : ''?>>
                     </div>
               </div>

               <div class="form-group">
                 <label for="image" class="col-sm-2 control-label">My Image</label>
                 <div class="col-sm-10">
                 <?php if(isset($image)){ ?>
                 <img src="<?php echo base_url().$image; ?>" class="profileimg"/>
                 <input type="hidden" name="image" value="<?php echo $image; ?>"/>
                 <?php } ?>
                 <input type="file" name="imageToUpload" id="imageToUpload" />
                 </div>
               </div>

               <div class="form-group">
                 <label for="video" class="col-sm-2 control-label">My Video</label>
                 <div class="col-sm-10">
                 <?php if(isset($video)){ ?>
                 <img src="<?php echo base_url().$video; ?>" class="profileimg"/>
                 <input type="hidden" name="video" value="<?php echo $video; ?>"/>
                 <?php } ?>
                 <input type="file" name="videoToUpload" id="videoToUpload" />
                 </div>
               </div>

               <input type="hidden" name="user_id" value="<?=$user_id?>">



             </div><!-- /.box-body -->
             <div class="box-footer">
               <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
               <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
               <button type="submit" name="submit" class="btn btn-info pull-right">Update</button>
             </div><!-- /.box-footer -->
           </form>
         </div><!-- /.box -->
         <!-- general form elements disabled -->

       </div><!--/.col (right) -->
     </div>   <!-- /.row -->
   </section><!-- /.content -->
 </div><!-- /.content-wrapper -->

<script type="text/javascript">
// $( document ).ready(function() {
 $().ready(function() {
$('#browse-1').click(function(){
var f = $('#elfileimg-1').elfinder({
   url : 'http://inventifweb.net/UNI/elfinder/php/connector.php',
   height: 490,
   docked: false,
   dialog: { width: 400, modal: true },
   closeOnEditorCallback: true,
   getFileCallback: function(url) {
       $('#elfileurl-1').val(url);
       // CLOSE ELFINDER HERE
       $('#elfinder').remove();  //remove Elfinder
       location.reload();   //reload Page for second selection
   }
}).elfinder('instance');
});

//alert('+++');
});
</script>
