<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      University
      <small>Manage Courses</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">University Management</a></li>
      <li class="active">Course</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <!-- /.box -->
        <?php if ($this->session->flashdata('flash_message')) { ?>
          <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
            <h4><i class="icon fa fa-check"></i> Success</h4>
            <?php echo $this->session->flashdata('flash_message'); ?>
          </div>
        <?php } ?>

        <div class="box" style="overflow-x: scroll;">

          <div class="box-header">
            <label> Filter
            </label>
            <!-- <input type="hidden" name="selected_affiliated_id" value="<?php echo $selected_affiliated_id; ?>"> -->
            <!-- <form name="filter" id="filter" action="<?php echo base_url(); ?>user/course/filter" method="POST">

              <div class="col-md-12">

                <div class="col-md-3 form-group">

                  <select class="form-control" name="affiliated" id="affiliated" onchange="this.form.submit()">
                    <option value="">Select Affilation Type</option>
                    <?php
                    if (isset($affiliated_list) && !empty($affiliated_list)) {
                      foreach ($affiliated_list as $ulkey => $alvalue) {
                    ?>
                        <option value="<?php echo $alvalue['id']; ?>" <?php if (isset($selected_affiliated_id) && $selected_affiliated_id == $alvalue['id']) {
                                                                        echo "selected";
                                                                      } ?>><?php echo $alvalue['name']; ?></option>
                    <?php }
                    } ?>
                    <option value="5" <?php if (isset($selected_affiliated_id) && $selected_affiliated_id == 5) {
                                        echo "selected";
                                      } ?>>All</option>
                  </select>
                </div>

                <div class="col-md-3 form-group">
                  <select class="form-control" name="university_id" id="university_id" onchange="this.form.submit()">
                    <option value="">Select University</option>
                    <?php
                    if (isset($university_list) && !empty($university_list)) {
                      foreach ($university_list as $ulkey => $ulvalue) {
                    ?>
                        <option value="<?php echo $ulvalue['id']; ?>" <?php if (isset($selected_university_id) && $selected_university_id == $ulvalue['id']) {
                                                                        echo "selected";
                                                                      } ?>><?php echo $ulvalue['name']; ?></option>
                    <?php }
                    } ?>
                  </select>
                </div>

                <div class="col-md-3 form-group">
                  <select class="form-control" name="campus_id" id="campus_id" onchange="this.form.submit()">
                    <option value="">Select Campus</option>
                    <?php
                    if (isset($campus_list) && !empty($campus_list)) {
                      foreach ($campus_list as $clkey => $clvalue) {
                    ?>
                        <option value="<?php echo $clvalue['id']; ?>" <?php if (isset($selected_campus_id) && $selected_campus_id == $clvalue['id']) {
                                                                        echo "selected";
                                                                      } ?>><?php echo $clvalue['name']; ?></option>
                    <?php }
                    } ?>
                  </select>
                </div>

                <div class="col-md-3 form-group">
                  <select class="form-control" name="college_id" id="college_id" onchange="this.form.submit()">
                    <option value="">Select College</option>
                    <?php
                    if (isset($college_list) && !empty($college_list)) {
                      foreach ($college_list as $cgkey => $cgvalue) {
                    ?>
                        <option value="<?php echo $cgvalue['id']; ?>" <?php if (isset($selected_college_id) && $selected_college_id == $cgvalue['id']) {
                                                                        echo "selected";
                                                                      } ?>><?php echo $cgvalue['name']; ?></option>
                    <?php }
                    } ?>
                  </select>
                </div>

                <div class="col-md-3 form-group">
                  <select class="form-control" name="department_id" id="department_id" onchange="this.form.submit()">
                    <option value="">Select Department</option>
                    <?php
                    if (isset($department_list) && !empty($department_list)) {
                      foreach ($department_list as $dlkey => $dlvalue) {
                    ?>
                        <option value="<?php echo $dlvalue['id']; ?>" <?php if (isset($selected_department_id) && $selected_department_id == $dlvalue['id']) {
                                                                        echo "selected";
                                                                      } ?>> <?php echo $dlvalue['name']; ?></option>
                    <?php }
                    } ?>
                  </select>
                </div>

                <div class="col-md-3 form-group">
                  <select class="form-control" name="degree_id" id="degree_id" onchange="this.form.submit()">
                    <option value="">Select Degree</option>
                    <?php
                    if (isset($degree_list) && !empty($degree_list)) {
                      foreach ($degree_list as $dgkey => $dgvalue) {
                    ?>
                        <option value="<?php echo $dgvalue['id']; ?>" <?php if (isset($selected_degree_id) && $selected_degree_id == $dgvalue['id']) {
                                                                        echo "selected";
                                                                      } ?>><?php echo $dgvalue['name']; ?></option>
                    <?php }
                    } ?>
                  </select>
                </div>

                <div class="col-md-3 form-group">
                  <select class="form-control" name="course_id" id="course_id" onchange="this.form.submit()">
                    <option value="">Select Course</option>
                    <?php
                    if (isset($course_list) && !empty($course_list)) {
                      foreach ($course_list as $cokey => $covalue) {
                    ?>
                        <option value="<?php echo $covalue['id']; ?>" <?php if (isset($selected_course_id) && $selected_course_id == $covalue['id']) {
                                                                        echo "selected";
                                                                      } ?>> <?php echo $covalue['name'] . ' - ' . $covalue['dname']; ?></option>
                    <?php }
                    } ?>
                  </select>
                </div>
              </div>
            </form> -->

            <input type="hidden" name="selected_affiliated_id" value="<?php echo $selected_affiliated_id; ?>">
            <form name="filter" id="filter" action="javascript:void(0);" method="POST">
              <div class="col-md-12">
                <div class="col-md-3 form-group">
                  <select class="form-control search" name="affiliated" id="affiliated" onchange="getUniversity();">
                    <option value="">Select Affiliation Type</option>
                    <?php foreach ($affiliated_list as $alvalue) : ?>
                      <option value="<?php echo $alvalue['id']; ?>" <?php echo ($selected_affiliated_id == $alvalue['id']) ? 'selected' : ''; ?>>
                        <?php echo $alvalue['name']; ?>
                      </option>
                    <?php endforeach; ?>
                    <option value="5" <?php echo ($selected_affiliated_id == 5) ? 'selected' : ''; ?>>All</option>
                  </select>
                </div>

                <div class="col-md-3 form-group">
                  <select class="form-control search" name="university_id" id="university_id" onchange="getCampus();">
                    <option value="">Select University</option>
                  </select>
                </div>

                <div class="col-md-3 form-group">
                  <select class="form-control search" name="campus_id" id="campus_id" onchange="getCollege();">
                    <option value="">Select Campus</option>
                  </select>
                </div>

                <div class="col-md-3 form-group">
                  <select class="form-control search" name="college_id" id="college_id" onchange="getDepartments();">
                    <option value="">Select College</option>
                  </select>
                </div>

                <div class="col-md-3 form-group">
                  <select class="form-control search" name="department_id" id="department_id" onchange="getDegree();">
                    <option value="">Select Departments</option>
                  </select>
                </div>

                <div class="col-md-3 form-group">
                  <select class="form-control search" name="degree_id" id="degree_id" onchange="getCourse();">
                    <option value="">Select Degree</option>
                  </select>
                </div>

                <div class="col-md-3 form-group">
                  <select class="form-control search" name="course_id" id="course_id">
                    <option value="">Select Course</option>
                  </select>
                </div>

                <div class="col-md-3 form-group">
                  <button type="submit" class="btn btn-primary">Show</button>
                </div>

              </div>
            </form>
          </div>
          <div class="box-header">
            <a href="<?php echo base_url(); ?>user/course/form"><button type="button" style="width:12%; float:left; margin-right:5px;" class="btn btn-block btn-primary"><i class="fa fa-plus"></i> Add Course</button></a>
            <button type="submit" class="btn btn-block btn-danger multidel" style="width:11%; display:none;"><i class="fa fa-minus"></i> Delete University</button>
          </div>
          <div class="box-body">
            <div id="filter-table-wrapper" style="display: none;">
              <table class="table table-bordered" id="filter-table">

                <thead>
                  <tr>
                    <th></th>
                    <th>Name</th>
                    <th>College</th>
                    <th>University</th>
                    <th>Degree</th>
                    <th>Duration</th>
                    <th>Deadline Link</th>
                    <th>Total Fees</th>
                    <th>Complete</th>
                    <th>Status</th>
                    <th>Option</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>

          <!-- <form action="<?php echo base_url(); ?>user/university/multidelete" method="post">
            <div class="box-header">
              <a href="<?php echo base_url(); ?>user/course/form"><button type="button" style="width:12%; float:left; margin-right:5px;" class="btn btn-block btn-primary"><i class="fa fa-plus"></i> Add Course</button></a>
              <button type="submit" class="btn btn-block btn-danger multidel" style="width:11%; display:none;"><i class="fa fa-minus"></i> Delete University</button>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th></th>
                    <th>Name</th>
                    <th>College</th>
                    <th>University</th>
                    <th>Degree</th>
                    <th>Duration</th>
                    <th>Deadline Link</th>
                    <th>Total Fees</th>
                    <th>Complete</th>
                    <th>Status</th>
                    <th>Option</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i = 1;
                  foreach ($courses as $usr) { ?>
                    <tr>
                      <td><input type="checkbox" class="multichk" name="chk[]" value="<?php echo $usr->id; ?>" /></td>
                      <td><?php echo $usr->name; ?></td>
                      <td><?php echo $usr->college_name; ?></td>
                      <td><?php echo $usr->university_name; ?></td>
                      <td><?php echo $usr->degree_name; ?></td>
                      <td><?php echo $usr->duration; ?></td>
                      <td><?php echo $usr->deadline_link; ?></td>
                      <td><?php echo $usr->total_fees; ?></td>
                      <td><?php echo $usr->completed; ?> % </td>
                      <td><?php echo $usr->status == 1 ? 'Active' : 'Inactive'; ?></td>
                      <td><a href="<?php echo base_url(); ?>user/course/form/<?php echo $usr->id; ?>/<?= $filter_level ?>" title="Modify" target="_blank"><i class="fa fa-edit"></i></a> &nbsp; <a href="javascript:void();" title="Delete" class="del" id="<?php echo $usr->id; ?>"><i class="fa fa-trash-o"></i></a></td>
                    <?php } ?>
                    </tr>

                </tbody>
                <tfoot>
                  <tr>
                    <th></th>
                    <th>Name</th>
                    <th>College</th>
                    <th>University</th>
                    <th>Degree</th>
                    <th>Duration</th>
                    <th>Deadline Link</th>
                    <th>Total Fees</th>
                    <th>Complete</th>
                    <th>Status</th>
                    <th>Option</th>
                  </tr>
                </tfoot>
              </table>
            </div>

          </form> -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<!-- <script type="text/javascript"> -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<!-- // In your Javascript (external .js resource or <script> tag) -->
<script>
  $(document).ready(function() {
    $('.search').select2();
  });
</script>

<script type="text/javascript">
  var base_url = "<?php echo base_url(); ?>";

  $('#filter').on('submit', function(e) {
    e.preventDefault();

    var formData = $(this).serialize();

    $.ajax({
      type: "POST",
      url: base_url + "user/course/filterData",
      data: formData,
      success: function(result) {

        if ($.fn.DataTable.isDataTable('#filter-table')) {

          $('#filter-table').DataTable().clear().destroy();
        }

        var tableRows = "";
        if (result && result.data && result.data.length > 0) {
          result.data.forEach(function(row) {
            tableRows += `<tr>
              <td><input type="checkbox" class="multichk" name="chk[]" value="${row.id}" /></td>
              <td>${row.name}</td>
              <td>${row.college_name}</td>
              <td>${row.university_name}</td>
              <td>${row.degree_name}</td>
              <td>${row.duration}</td>
              <td><a href="${row.deadline_link}" target="_blank">Link</a></td>
              <td>${row.total_fees ? row.total_fees : 'N/A'}</td>
              <td>${row.completed ? row.completed : 'N/A'}%</td>
              <td>${row.status == 1 ? 'Active' : 'Inactive'}</td>
              <td>
                  <a href="${base_url}user/course/form/${row.id}" title="Modify" target="_blank">
                      <i class="fa fa-edit"></i>
                  </a> &nbsp;
                  <a href="javascript:void(0);" title="Delete" class="del" id="${row.id}">
                      <i class="fa fa-trash-o"></i>
                  </a>
              </td>
          </tr>`;
          });

          $('#filter-table tbody').html(tableRows);
          $('#filter-table-wrapper').show();

          $('#filter-table').DataTable();
        } else {
          $('#filter-table tbody').html('<tr><td colspan="10">No data found</td></tr>');
          $('#filter-table-wrapper').show();

          $('#filter-table').DataTable();
        }
      },
      error: function(xhr, status, error) {
        console.error("AJAX Error: ", error);
        alert("Error while fetching data.");
      }
    });
  });
</script>

<script type="text/javascript">
  function getUniversity() {
    var affiliatedId = $('#affiliated').val();
    var dataString = "affiliated_id=" + affiliatedId;

    $.ajax({
      type: "POST",
      url: "<?php echo base_url('user/course/getUniversity'); ?>",
      data: dataString,
      cache: false,
      success: function(result) {
        var universityList = result.university_list || [];
        var universityDropDown = "<option value=''>Select University</option>";

        if (universityList.length) {
          universityList.forEach(function(value) {
            universityDropDown += "<option value='" + value.id + "'>" + value.name + "</option>";
          });
        }

        $('#university_id').html(universityDropDown);
      }
    });
  }

  function getCampus() {
    var universityId = $('#university_id').val();
    var dataString = "university_id=" + universityId;

    $.ajax({
      type: "POST",
      url: "<?php echo base_url('user/course/getCampus'); ?>",
      data: dataString,
      cache: false,
      success: function(result) {
        var campusList = result.campus_list || [];
        var campusDropDown = "<option value=''>Select Campus</option>";

        if (campusList.length) {
          campusList.forEach(function(value) {
            campusDropDown += "<option value='" + value.id + "'>" + value.name + "</option>";
          });
        }

        $('#campus_id').html(campusDropDown);
      }
    });
  }

  function getCollege() {
    var campusId = $('#campus_id').val();
    var dataString = "campus_id=" + campusId;

    $.ajax({
      type: "POST",
      url: "<?php echo base_url('user/course/getCollege'); ?>",
      data: dataString,
      cache: false,
      success: function(result) {
        var campusList = result.college_list || [];
        var campusDropDown = "<option value=''>Select College</option>";

        if (campusList.length) {
          campusList.forEach(function(value) {
            campusDropDown += "<option value='" + value.id + "'>" + value.name + "</option>";
          });
        }

        $('#college_id').html(campusDropDown);
      }
    });
  }

  function getDepartments() {
    var collegeId = $('#college_id').val();
    var dataString = "college_id=" + collegeId;

    $.ajax({
      type: "POST",
      url: "<?php echo base_url('user/course/getDepartments'); ?>",
      data: dataString,
      cache: false,
      success: function(result) {
        var departmentList = result.department_list || [];
        var departmentDropDown = "<option value=''>Select Department</option>";

        if (departmentList.length) {
          departmentList.forEach(function(value) {
            departmentDropDown += "<option value='" + value.id + "'>" + value.name + "</option>";
          });
        }

        $('#department_id').html(departmentDropDown);
      }
    });
  }

  function getDegree() {
    var departmentId = $('#department_id').val();
    var dataString = "department_id=" + departmentId;

    $.ajax({
      type: "POST",
      url: "<?php echo base_url('user/course/getDegree'); ?>",
      data: dataString,
      cache: false,
      success: function(result) {
        var degreeList = result.degree_list || [];
        var degreeDropDown = "<option value=''>Select Degree</option>";

        if (degreeList.length) {
          degreeList.forEach(function(value) {
            degreeDropDown += "<option value='" + value.id + "'>" + value.name + "</option>";
          });
        }

        $('#degree_id').html(degreeDropDown);
      }
    });
  }

  function getCourse() {
    var departmentId = $('#department_id').val();
    var degreeId = $('#degree_id').val();
    var dataString = "department_id=" + departmentId + "&degree_id=" + degreeId;

    $.ajax({
      type: "POST",
      url: "<?php echo base_url('user/course/getCourse'); ?>",
      data: dataString,
      cache: false,
      success: function(result) {
        var courseList = result.course_list || [];
        var courseDropDown = "<option value=''>Select Course</option>";

        if (courseList.length) {
          courseList.forEach(function(value) {
            courseDropDown += "<option value='" + value.id + "'>" + value.name + " - " + value.dname + "</option>";
          });
        }

        $('#course_id').html(courseDropDown);
      }
    });
  }
</script>


<!-- <script>
  var table;
  $(document).ready(function() {
    table = $('#newAppExample').DataTable({
      "processing": true,
      "serverSide": true,
      "order": [],

      "ajax": {
        "url": "<?php echo base_url(); ?>user/course/ajax_manage_page/<?= $selected_affiliated_id; ?>/<?= $selected_university_id; ?>/<?= $selected_campus_id; ?>/<?= $selected_college_id; ?>/<?= $selected_department_id; ?>/<?= $selected_degree_id; ?>/<?= $selected_course_id; ?>",
        "type": "POST",
      },

      "columnDefs": [{
          "data": "id"
        },
        {
          "data": "name"
        },
        {
          "data": "college_name"
        },
        {
          "data": "university_name"
        },
        {
          "data": "degree_name"
        },
        {
          "data": "duration"
        },
        {
          "data": "deadline_link"
        },
        {
          "data": "total_fees"
        },
        {
          "data": "completed"
        },
        {
          "data": "Action"
        }
      ],
    });
  });
</script> -->