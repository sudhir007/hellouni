
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            University
            <small>Manage Courses</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">University Management</a></li>
            <li class="active">Course</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

			  <!-- /.box -->
			  <?php if($this->session->flashdata('flash_message')){ ?>
				<div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                <h4><i class="icon fa fa-check"></i> Info : </h4>
                <?php echo $this->session->flashdata('flash_message'); ?>
              </div>
			  <?php } ?>

              <div class="box">

			  <form action="<?php echo base_url();?>user/course/selectedoptions" method="post">

          <div class="box-header">
            <div class="col-xs-12">

              <div class="col-md-4">
            <select style="width: 200px" name="options_id" id="options_id" onchange="showoption()">
              <option value="">Select Option</option>
              <option value="ASSIGN"> ASSIGN </option>
              <option value="DELETE"> DELETE </option>
              <option value="DELETEDEPARTMENT"> DELETE DEPARTMENT</option>
            </select>
          </div>

        <div class="col-md-4">
        <select style="width: 200px" name="assign_id" id="assign_id">
          <option value="">Select Assign To</option>
          <?php
          if(isset($department_list) && !empty($department_list)){
          foreach ($department_list as $dlkey => $dlvalue) {
          ?>
          <option value="<?php echo $dlvalue['id'] ; ?>" <?php if(isset($selected_department_id) && $selected_department_id == $dlvalue['id']){ echo "selected"; } ?>> <?php echo $dlvalue['name'] ; ?></option>
        <?php } } ?>
        </select>
        <input type="hidden" name="selected_department_id" id="selected_department_id" value="<?php echo $selected_department_id;?>">
      </div>

      <div class="col-md-4">
        <button type="submit" class="btn btn-info "> Submit</button>
      </div>
          </div>
          </div>

                <!--<div class="box-header">
                 <a href="<?php echo base_url();?>user/course/form"><button type="button" style="width:12%; float:left; margin-right:5px;" class="btn btn-block btn-primary"><i class="fa fa-plus"></i> Add Course</button></a>
				 <button type="submit" class="btn btn-block btn-danger multidel" style="width:11%; display:none;"><i class="fa fa-minus"></i> Delete University</button>
       </div>--><!-- /.box-header -->

                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th></th>
                        <th>Name</th>
                        <th>College</th>
                        <th>University</th>
                        <th>Degree</th>
                        <th>Duration</th>
                        <th>Deadline Link</th>
                        <th>Total Fees</th>
                        <th>Option</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php $i=1;
					 foreach($courses as $usr){ ?>
                      <tr>
                        <td><input type="checkbox" class="multichk" name="chk[]" value="<?php echo $usr->id;?>"/></td>
                        <td><?php echo $usr->name;?></td>
                        <td><?php echo $usr->college_name;?></td>
                        <td><?php echo $usr->university_name;?></td>
                        <td><?php echo $usr->degree_name;?></td>
                        <td><?php echo $usr->duration;?></td>
                        <td><?php echo $usr->deadline_link;?></td>
                        <td><?php echo $usr->total_fees;?></td>
						 <td><a href="<?php echo base_url();?>user/course/form/<?php echo $usr->id;?>" title="Modify"><i class="fa fa-edit"></i></a> &nbsp; <a href="javascript:void();" title="Delete" class="del" id="<?php echo $usr->id;?>"><i class="fa fa-trash-o"></i></a></td>
					<?php } ?>
                      </tr>

                    </tbody>
                    <tfoot>
                      <tr>
                          <th></th>
                          <th>Name</th>
                          <th>College</th>
                          <th>University</th>
                          <th>Degree</th>
                          <th>Duration</th>
                          <th>Deadline Link</th>
                          <th>Total Fees</th>
                          <th>Option</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->

				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <script type="text/javascript">

      function showoption(){
        var selectedoption = $('#options_id').val();

        if(selectedoption == 'ASSIGN'){
          $('#assign_id').show();

        } else {
          $('#assign_id').hide();
        }
      }

      </script>
