
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            University
            <small>Manage University Users</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">University Management</a></li>
            <li class="active">University</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

			  <!-- /.box -->
			  <?php if($this->session->flashdata('flash_message')){ ?>
				<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                <?php echo $this->session->flashdata('flash_message'); ?>
              </div>
			  <?php } ?>

              <div class="box">

			  <form action="<?php echo base_url();?>user/university/multidelete" method="post">

                <div class="box-header">
                 <a href="<?php echo base_url();?>user/alumnus/form"><button type="button" style="width:12%; float:left; margin-right:5px;" class="btn btn-block btn-primary"><i class="fa fa-plus"></i> Add Alumnu</button></a>
				 <button type="submit" class="btn btn-block btn-danger multidel" style="width:11%; display:none;"><i class="fa fa-minus"></i> Delete University</button>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="alumnusTable" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th></th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>University Name</th>
                        <th>Created Date</th>
                        <th>Status</th>
						            <th>Option</th>
                      </tr>
                    </thead>
                    <tbody>
					
                    </tbody>
                    <tfoot>
                      <tr>
                       <th></th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>University Name</th>
                        <th>Created Date</th>
                        <th>Status</th>
						<th>Option</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->

				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>     

<script>
var table;
  $(document).ready(function() {

    //datatables
    table = $('#alumnusTable').DataTable({ 
//alert('hi');
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url();?>user/alumnus/ajax_manage_page",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
                { "data": "id" },
                { "data": "name" },
                { "data": "email" },
                { "data": "university_name" },
                { "data": "createdate" },
                { "data": "status_name" },
                 { "data": "action" }
            ],
        

    });

  });
   // var url = '<?= site_url('Sub_menus/ajax_manage_page')?>';
    //var url = '<?php echo base_url();?>user/representative/ajax_manage_page';
    //var actioncolumn=5;
</script>