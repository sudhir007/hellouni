
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Sessions
            <small><?=$session_page?></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Session Management</a></li>
            <li class="active"><?=$session_page?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
                <?php
                if($this->session->flashdata('flash_message'))
                {
                    ?>
      				<div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                      <h4><i class="icon fa fa-times"></i> Error</h4>
                      <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
      			  <?php
                }
                ?>
			  <!-- /.box -->
              <form action="<?php echo base_url();?>user/student/form/" method="POST">
                  <?php
                  if($session_page == 'Unassigned Sessions')
                  {
                      ?>
                      <input type="submit" name="fix_slots" value="Create Slots" style="float:right;font-size: 16px;font-weight: bold;">
                      <div style="clear:both;"></div>
                      <?php
                  }
                  ?>
              <div class="box">

                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sl No</th>
                        <?=($user_detail['type'] != 3) ? '<th>University</th>' : ''?>
                        <th>Student Name</th>
                        <th>Student Email</th>
                        <th>Slot 1</th>
                        <th>Slot 2</th>
                        <th>Slot 3</th>
                        <th>Confirmed Slot</th>
                        <th>Status</th>
                        <th>Timezone</th>
                        <?=($session_page == 'Completed Sessions') ? '<th>Recording</th>' : ''?>
                        <?=($session_page == 'Coming Sessions') ? '<th>Video Conference URL</th>' : ''?>
                        <th>Option</th>
                        <?=($session_page == 'Coming Sessions') ? '<th></th>' : ''?>
                      </tr>
                    </thead>
                    <tbody>
					<?php $i=1;
					 foreach($sessions as $session){ ?>
                      <tr>
                        <td><input type="checkbox" class="multichk" name="slot_ids[]" value="<?php echo $session->id;?>"/>&nbsp;<?=$i++?></td>
                        <?=($user_detail['type'] != 3 && isset($session->university_name)) ? '<td>' . $session->university_name . '</td>' : ''?>
                        <td><a href="/admin/student/index/profile?id=<?php echo $session->student_id;?>" target="_blank"><?php echo $session->name;?></a></td>
                        <td><?php echo $session->email;?></td>
                        <td><?=$session->slot_1?></td>
                        <td><?=$session->slot_2?></td>
                        <td><?=$session->slot_3?></td>
                        <td><?=$session->confirmed_slot ? $session->confirmed_slot : ''?></td>
                        <td><?php echo $session->status;?></td>
                        <td><?php echo 'GMT' . $session->timezone;?></td>
                        <?php
                        if($session_page == 'Coming Sessions')
                        {
                            ?>
                            <td><?=isset($session->tokbox_url) ? "<a href='" . $session->tokbox_url . "' target='_blank'>" . $session->tokbox_url . "</a>" : ""?></td>
                            <?php
                        }
                        if($session_page == 'Completed Sessions')
                        {
                            ?>
                            <td><?=isset($session->archive_id) ? "<a href='" . base_url() . 'user/session/recording?aid=' . $session->archive_id . "' target='_blank'>Click Here</a>" : ""?></td>
                            <?php
                        }
                        ?>
                        <td>
                            <a href="<?php echo base_url();?>user/student/form/<?php echo $session->id;?>" title="Modify"><i class="fa fa-edit"></i></a> &nbsp;
                            <a href="<?php echo base_url();?>user/student/delete?slot_id=<?php echo $session->id;?>&redirectUrl=<?=$redirect_url?>" title="Delete" class="del" id="<?php echo $session->id;?>"><i class="fa fa-trash-o"></i></a>
                        </td>
                        <?php
                        if($session_page == 'Coming Sessions')
                        {
                            ?>
                            <td>
                                <a href="<?php echo base_url();?>user/session/generate_url?id=<?php echo $session->id;?>&user_id=<?=$session->user_id?>&university_id=<?=$session->university_id?>&university_login_id=<?=$session->university_login_id?>" title="Generate Conference URL">Generate Conference URL</a>
                            </td>
                            <?php
                        }
                        ?>
                    </tr>
					<?php } ?>


                    </tbody>
                    <tfoot>
                      <tr>
                          <th>Sl No</th>
                          <?=($user_detail['type'] != 3) ? '<th>University</th>' : ''?>
                          <th>Student Name</th>
                          <th>Student Email</th>
                          <th>Slot 1</th>
                          <th>Slot 2</th>
                          <th>Slot 3</th>
                          <th>Confirmed Slot</th>
                          <th>Status</th>
                          <th>Timezone</th>
                          <?=($session_page == 'Completed Sessions') ? '<th>Recording</th>' : ''?>
                          <?=($session_page == 'Coming Sessions') ? '<th>Video Conference URL</th>' : ''?>
                          <th>Option</th>
                          <?=($session_page == 'Coming Sessions') ? '<th></th>' : ''?>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
          </form>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
