
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            University
            <small>Manage Campuses</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">University Management</a></li>
            <li class="active">Campus</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

			  <!-- /.box -->
			  <?php if($this->session->flashdata('flash_message')){ ?>
				<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                <?php echo $this->session->flashdata('flash_message'); ?>
              </div>
			  <?php } ?>

              <div class="box">

                <div class="box-header">
                  <label>
                    Filter
                  </label>
                  <div class="col-xs-12">
                    <form name="filter" id="filter" action="<?php echo base_url();?>user/campus/filter" method="POST">
                    <div class="col-md-4">
                      <select style="width: 150px" name="university_id" id="university_id" onchange="this.form.submit()">
                        <option value="">Select University</option>
                        <?php
                        if(isset($university_list) && !empty($university_list)){
                        foreach ($university_list as $ulkey => $ulvalue) {
                        ?>
                        <option value="<?php echo $ulvalue['id'] ; ?>" <?php if(isset($selected_university_id) && $selected_university_id == $ulvalue['id']){ echo "selected"; } ?>><?php echo $ulvalue['name'] ; ?></option>
                      <?php } } ?>
                      </select>
                    </div>

                    <div class="col-md-4">
                      <select style="width: 150px" name="campus_id" id="campus_id" onchange="this.form.submit()">
                        <option value="">Select Campus</option>
                        <?php
                        if(isset($campus_list) && !empty($campus_list)){
                        foreach ($campus_list as $clkey => $clvalue) {
                        ?>
                        <option value="<?php echo $clvalue['id'] ; ?>" <?php if(isset($selected_campus_id) && $selected_campus_id == $clvalue['id']){ echo "selected"; } ?>><?php echo $clvalue['name'] ; ?></option>
                      <?php } } ?>
                      </select>
                    </div>

                  </form>
                  </div>
                </div>

			  <form action="<?php echo base_url();?>user/university/multidelete" method="post">

                <div class="box-header">
                 <a href="<?php echo base_url();?>user/campus/form"><button type="button" style="width:12%; float:left; margin-right:5px;" class="btn btn-block btn-primary"><i class="fa fa-plus"></i> Add Campus</button></a>
				 <button type="submit" class="btn btn-block btn-danger multidel" style="width:11%; display:none;"><i class="fa fa-minus"></i> Delete University</button>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <!--<table id="newAppExample" class="table table-bordered table-striped">-->
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th></th>
                        <th>Name</th>
                        <th>University Name</th>
                        <th>Status</th>
                        <th>Option</th>
                      </tr>
                    </thead>
                    <tbody>
					 <?php $i=1;
					 foreach($campuses as $usr){ ?>
                      <tr>
                        <td><input type="checkbox" class="multichk" name="chk[]" value="<?php echo $usr->id;?>"/></td>
                        <td><?php echo $usr->name;?></td>
                        <td><?php echo $usr->university_name;?></td>
                        <td><?php echo $usr->status;?></td>
						 <td><a href="<?php echo base_url();?>user/campus/form/<?php echo $usr->id;?>" title="Modify"><i class="fa fa-edit"></i></a> &nbsp; <a href="javascript:void();" title="Delete" class="del" id="<?php echo $usr->id;?>"><i class="fa fa-trash-o"></i></a></td>
             </tr>
        	<?php } ?>


                    </tbody>
                    <tfoot>
                      <tr>
                       <th></th>
                        <th>Name</th>
                        <th>University Name</th>
                        <th>Status</th>
						            <th>Option</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->

				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<script>
var table;
  $(document).ready(function() {

    //datatables
    table = $('#newAppExample').DataTable({
//alert('hi');

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url();?>user/campus/ajax_manage_page",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
                { "data": "id" },
                { "data": "name" },
                { "data": "university_name" },
                { "data": "action" }
            ],


    });

  });

</script>
