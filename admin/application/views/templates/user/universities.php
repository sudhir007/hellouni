<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      University
      <small>Manage Universities</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">University Management</a></li>
      <li class="active">University</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <!-- /.box -->
        <?php if ($this->session->flashdata('flash_message')) { ?>
          <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
            <h4><i class="icon fa fa-check"></i> Success</h4>
            <?php echo $this->session->flashdata('flash_message'); ?>
          </div>
        <?php } ?>

        <div class="box">

          <form action="<?php echo base_url(); ?>user/university/multidelete" method="post">

            <div class="box-header">
              <a href="<?php echo base_url(); ?>user/university/form"><button type="button" style="width:12%; float:left; margin-right:5px;" class="btn btn-block btn-primary"><i class="fa fa-plus"></i> Add University</button></a>
              <a href="<?php echo base_url(); ?>university/process/deleteUniversityCacheListKey"><button type="button" style="width:15%; float:left; margin-right:5px;" class="btn btn-block btn-primary pull-right">Delete Cache University List</button></a>
              <button type="submit" class="btn btn-block btn-danger multidel" style="width:11%; display:none;"><i class="fa fa-minus"></i> Delete University</button>
            </div><!-- /.box-header -->
            <div class="box-body">
              <table id="newAppExample" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Country</th>
                    <th>Affiliation Type</th>
                    <th>Created Date</th>
                    <th>Status</th>
                    <th>University Detail</th>
                    <th>Detail Frontend Info</th>
                    <th>University Additional Info</th>
                    <th>Application Process Info</th>
                    <th>Delete Key</th>
                  </tr>
                </thead>
                <tbody>
                  <!-- <?php $i = 1;
                        foreach ($users as $usr) { ?>
                      <tr>
                        <td><input type="checkbox" class="multichk" name="chk[]" value="<?php echo $usr->id; ?>"/></td>
                        <td><?php echo $usr->name; ?></td>
                        <td><?php echo $usr->email; ?></td>
                        <td><?php echo $usr->affiliated_type; ?></td>
                        <td><?php echo $usr->createdate; ?></td>
                        <td><?php echo $usr->status_name; ?></td>
						 <td><a href="<?php echo base_url(); ?>user/university/form/<?php echo $usr->id; ?>" title="Modify"><i class="fa fa-edit"></i></a> &nbsp; <a href="javascript:void();" title="Delete" class="del" id="<?php echo $usr->id; ?>"><i class="fa fa-trash-o"></i></a></td>
             <td><a href="<?php echo base_url(); ?>university/info/<?php echo $usr->id; ?>" title="Modify"><i class="fa fa-edit"></i></a> </td>
             <td><a href="<?php echo base_url(); ?>university/additionaldata/additionaldataform/<?php echo $usr->id; ?>" title="Modify"><i class="fa fa-edit"></i></a> </td>
             <td><a href="<?php echo base_url(); ?>university/process/infoform/<?php echo $usr->id; ?>" title="Add / Modify"><i class="fa fa-edit"></i></a> </td>
          <?php } ?>
                      </tr> -->

                </tbody>
                <tfoot>
                  <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Country</th>
                    <th>Affiliation Type</th>
                    <th>Created Date</th>
                    <th>Status</th>
                    <th>University Detail</th>
                    <th>Detail Frontend Info</th>
                    <th>University Additional Info</th>
                    <th>Application Process Info</th>
                    <th>Delete Key</th>
                  </tr>
                </tfoot>
              </table>
            </div><!-- /.box-body -->

          </form>
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
  var table;
  $(document).ready(function() {

    //datatables
    table = $('#newAppExample').DataTable({
      //alert('hi');

      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "order": [], //Initial no order.

      // Load data for the table's content from an Ajax source
      "ajax": {
        "url": "<?php echo base_url(); ?>user/university/ajax_manage_page_university",
        "type": "POST"
      },

      //Set column definition initialisation properties.
      "columnDefs": [{
          "data": "id"
        },
        {
          "data": "name"
        },
        {
          "data": "email"
        },
        {
          "data": "country_name"
        },
        {
          "data": "affiliated"
        },
        {
          "data": "createdate"
        },
        {
          "data": "status_name"
        },
        {
          "data": "option"
        },
        {
          "data": "Detail Frontend Info"
        },
        {
          "data": "Application Process Info"
        },
        {
          "data": "Delete Key"
        }
      ],


    });

  });
</script>