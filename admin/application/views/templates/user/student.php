
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            University
            <small>Manage University Users</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">University Management</a></li>
            <li class="active">University</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

			  <!-- /.box -->
			  <?php if($this->session->flashdata('flash_message')){ ?>
				<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                <?php echo $this->session->flashdata('flash_message'); ?>
              </div>
			  <?php } ?>

              <div class="box">

			  <form action="<?php echo base_url();?>user/university/multidelete" method="post">

                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sl No</th>
                        <th>Student Name</th>
                        <th>Student Email</th>
                        <th>Student Phone</th>
                        <!--th>Conference URL</th-->
                        <th>Option</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php $i=1;
					 foreach($all_slots as $usr){ ?>
                      <tr>
                        <td><?php echo $i++;?></td>
                        <td><?php echo $usr->name;?></td>
                        <td><?php echo $usr->email;?></td>
                        <td><?php echo $usr->phone;?></td>
                        <!--td><a href="<?php echo str_replace("/admin/", "", base_url()) . $usr->tokbox_url;?>" target="_blank"><?php echo str_replace("/admin/", "", base_url()) . $usr->tokbox_url;?></a></td-->
						 <td><a href="<?php echo base_url();?>user/student/form/<?php echo $usr->id;?>" title="Modify"><i class="fa fa-edit"></i></a> &nbsp; <a href="javascript:void();" title="Delete" class="del" id="<?php echo $usr->id;?>"><i class="fa fa-trash-o"></i></a></td>
					<?php } ?>
                      </tr>

                    </tbody>
                    <tfoot>
                      <tr>
                       <th>Sl No</th>
                       <th>Student Name</th>
                       <th>Student Email</th>
                       <th>Student Phone</th>
                       <!-- <th>Conference URL</th> -->
						<th>Option</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->

				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
