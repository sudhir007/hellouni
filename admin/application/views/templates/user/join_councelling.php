<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Join Councelling<small>Manage Join Councelling</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="#">Join Councelling Management</a></li>
            <li class="active">Join Councelling</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>

                <div class="box">
                    <form action="<?php echo base_url();?>user/counselor/multidelete" method="post">
                        <div class="box-header">
                            <?php if(isset($id)){?>

                              <div class="form-group">
                                <label for="name" class="col-sm-4 control-label">University Joining Link : </label>
                                <div class="col-sm-8">
                                  https://hellouni.org/imperialCounsellingTokbox/event/panelist/NTQ3/aW1wZXJpYWxfY291bnNlbGxpbmc/<?=$id?>
                                </div>
                              </div>

                              <div class="form-group">
                                <label for="name" class="col-sm-4 control-label">Student Joining Link : </label>
                                <div class="col-sm-8">
                                  https://hellouni.org/imperialCounsellingTokbox/event/virtualfair/<?=$id?>
                                </div>
                              </div>


                            <?php }else{ ?>

                              <div class="form-group">
                                <label for="name" class="col-sm-4 control-label">University Joining Link : </label>
                                <div class="col-sm-8">
                                  https://hellouni.org/imperialCounsellingTokbox/event/panelist/NTQ3/aW1wZXJpYWxfY291bnNlbGxpbmc
                                </div>
                              </div>

                              <div class="form-group">
                                <label for="name" class="col-sm-4 control-label">Student Joining Link : </label>
                                <div class="col-sm-8">
                                  https://hellouni.org/imperialCounsellingTokbox/event/virtualfair
                                </div>
                              </div>

                            <?php } ?>




                        </div><!-- /.box-header -->

                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
