<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Sessions <small>Recording</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Session Management</a></li>
            <li class="active">Recording</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <video width="100%" height="100%" controls>
                <source src="<?=$source_url?>" type="video/mp4">
                <source src="movie.ogg" type="video/ogg">Your browser does not support the video tag.
            </video>
        </div>
    </section>
</div>
