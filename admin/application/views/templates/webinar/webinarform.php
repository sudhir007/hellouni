<style type="text/css">.tt{ height:0px !important;}
.profileimg{ width:150px; height:150px; }
</style>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Webinar
            <small>Add Webinar</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Webinar Management</a></li>
            <li class="active">Add Webinar</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">

            <!-- right column -->
            <div class="col-md-10">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Webinar</h3>
                </div><!-- /.box-header -->
                <!-- form start -->

                <?php if($this->session->flashdata('flash_message')){ ?>
  				<div class="alert alert-error alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                  <h4><i class="icon fa fa-circle-o"></i> Error</h4>
                  <?php echo $this->session->flashdata('flash_message'); ?>
                </div>
  			  <?php } ?>

				 <?php if($this->uri->segment('4')){?>
				<form class="form-horizontal" action="<?php echo base_url();?>webinar/webinarmaster/edit/<?php if(isset($w_id)) echo $w_id;?>" method="post" name="editwebinar" enctype="multipart/form-data">
				<?php }else{?>
                <form class="form-horizontal" action="<?php echo base_url();?>webinar/webinarmaster/create" method="post" name="addwebinar" enctype="multipart/form-data">
                <?php } ?>



                  <div class="box-body">

                    <div class="form-group">
                      <label for="name" class="col-sm-2 control-label">Webinar Name *</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="webinar_name" name="webinar_name" placeholder="Webinar Name" value="<?php if(isset($webinar_name)) echo $webinar_name; ?>" required>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="name" class="col-sm-2 control-label"> Webinar Host Name *</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="webinar_host_name" name="webinar_host_name" placeholder="Webinar Host Name" value="<?php if(isset($webinar_host_name)) echo $webinar_host_name; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="name" class="col-sm-2 control-label">Webinar Topic *</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="webinar_topics" name="webinar_topics" placeholder="Webinar Topic" value="<?php if(isset($webinar_topics)) echo $webinar_topics; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="name" class="col-sm-2 control-label">Webinar Date *</label>
                      <div class="col-sm-10">
                        <input type="date" class="form-control" id="webinar_date" name="webinar_date" placeholder="Webinar Date" value="<?php if(isset($webinar_date)) echo $webinar_date; ?>" required>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="name" class="col-sm-2 control-label">Webinar Time *</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="webinar_time" name="webinar_time" placeholder="Webinar Time" value="<?php if(isset($webinar_time)) echo $webinar_time; ?>" required>
                      </div>
                    </div>

                    <?php
                    /*
                    <div class="form-group">
                      <label for="name" class="col-sm-2 control-label">Webinar Link *</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="webinar_link" name="webinar_link" placeholder="Webinar Link" value="<?php if(isset($webinar_link)) echo $webinar_link; ?>">
                      </div>
                    </div>
                    */
                    ?>

                    <div class="form-group">
                      <label for="name" class="col-sm-2 control-label">Webinar Description *</label>
                      <div class="col-sm-10">
                        <textarea class="form-control" id="webinar_description" name="webinar_description" placeholder="Webinar Description" ><?php if(isset($webinar_description)) echo htmlspecialchars($webinar_description);?></textarea>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="name" class="col-sm-2 control-label">Webinar Banner *</label>
                      <div class="col-sm-10">

                        <?php if(isset($webinar_img)){ ?>
            					  <img src="<?php echo base_url() . '..' . $webinar_img . '?t=' . time(); ?>" class="profileimg"/>
            					  <?php } ?>
            					  <input type="hidden" name="webinarimg" value="<?php if(isset($webinar_img)) echo $webinar_img; ?>"/>

                        <input type="file" class="form-control" id="webinar_img" name="webinar_img" />
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="name" class="col-sm-2 control-label">Webinar Default Count *</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="default_attend_count" name="default_attend_count" placeholder="Webinar Default Attend Count" value="<?php if(isset($default_attend_count)) echo $default_attend_count; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="name" class="col-sm-2 control-label">Webinar Live Status *</label>
                      <div class="col-sm-10">
                        <select class="form-control" name="webinar_live">
                            <option value="">Please Select</option>
                            <option value="1" <?php if(isset($webinar_live) && $webinar_live=="1"){ echo 'selected="selected"'; } ?>>YES</option>
                            <option value="0" <?php if(isset($webinar_live) && $webinar_live=="0"){ echo 'selected="selected"'; } ?>>NO</option>

                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="name" class="col-sm-2 control-label">Webinar Status *</label>
                      <div class="col-sm-10">
                        <select class="form-control" name="status">
                        <option value="">Please Select</option>
                        <option value="1" <?php if(isset($status) && $status=="1"){ echo 'selected="selected"'; } ?>>YES</option>
                        <option value="0" <?php if(isset($status) && $status=="0"){ echo 'selected="selected"'; } ?>>NO</option>

                    </select>
                      </div>
                    </div>

                    <hr>

                    <label for="city" class="control-label">Webinar Display Detail</label>
                    <br><br>

					         <div class="form-group">
                      <label for="state" class="col-sm-2 control-label">Webinar Display Link</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="webinar_display_link" name="webinar_display_link" placeholder="Webinar Display Link" value="<?php if(isset($webinar_display_link)) echo $webinar_display_link; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="city" class="col-sm-2 control-label">Webinar Display Description</label>
                      <div class="col-sm-10">
                        <textarea class="form-control" id="webinar_display_description" name="webinar_display_description" placeholder="Webinar Display Description" ><?php if(isset($webinar_display_description)) echo htmlspecialchars($webinar_display_description); ?></textarea>
                      </div>
                    </div>


                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
					<button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                    <?php
                    if($this->uri->segment('4'))
                    {
                        ?>
                        <button type="submit" class="btn btn-info pull-right">Update</button>
                        <?php
                    }
                    else
                    {
                        ?>
                        <button type="submit" class="btn btn-info pull-right">Create</button>
                        <?php
                    }
                    ?>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              <!-- general form elements disabled -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
