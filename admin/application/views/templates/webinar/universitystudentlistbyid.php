<style>
.attendeesListModal{
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 8%;
    top: 20%;
    width: 70%; /* Full width */
    height: 60%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    z-index: 105;
}

.attendeesList-content{
    background-color: #e8dede;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
}

.listclose{
    color: #8e0f0f;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.listclose:hover{
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

</style>
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            UTA Event
            <small>Student List</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#"> Student </a></li>
            <li class="active">UTA Student List </li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <!--<a class="btn btn-info" href="<?= base_url() ?>webinar/webinarmaster/exportCSV/<?=$webinar_id?>">Export</a>
              <a class="btn btn-info pull-right"  href="#" onclick="refreshList(<?=$webinar_id?>,<?=$tokbox_token_id?>)"> Refresh Student List </a>
            -->
			  <!-- /.box -->
			  <?php if($this->session->flashdata('flash_message')){ ?>
				<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                <?php echo $this->session->flashdata('flash_message'); ?>
              </div>
			  <?php } ?>

              <div class="box">

			  <form method="post">


                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Student Name</th>
                        <th>Student Email</th>
                        <th>Student ID (UTA)</th>
                        <th>Document Status</th>
                        <th>View Document</th>
                        <th>Downloads</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php

					 foreach($universitystudentlist as $university_student_list){ ?>
                      <tr>
                        <td><?php echo $university_student_list['name'];?></td>
                        <td><?php echo $university_student_list['email'];?></td>
                        <td><?php echo $university_student_list['student_id'];?></td>
                        <?php
                        switch($university_student_list['document_status'])
                        {
                            case 'Not Uploaded':
                                ?>
                                <td style="background-color: red; color: white; font-weight: bold;"><?php echo $university_student_list['document_status'];?></td>
                                <?php
                                break;
                            case 'Incomplete':
                                ?>
                                <td style="background-color: #75753e; color: white; font-weight: bold;"><?php echo $university_student_list['document_status'];?></td>
                                <?php
                                break;
                            case 'Completed':
                                ?>
                                <td style="font-weight: bold;"><?php echo $university_student_list['document_status'];?></td>
                                <?php
                                break;
                        }
                        if($university_student_list['document_id'])
                        {
                            ?>
                            <td><input type="button" onclick="ViewDocuments(<?php echo $university_student_list['id'] ; ?>, '<?php echo $university_student_list['name'] ; ?>')" value="View" /></td>
                            <td><input type="button" onclick="DownloadDocList(<?php echo $university_student_list['id'] ; ?>)" value="Download" /></td>
                            <?php
                        }
                        else
                        {
                            ?>
                            <td></td>
                            <td></td>
                            <?php
                        }
                        ?>
                        </tr>
					<?php } ?>


                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Student Name</th>
                        <th>Student Email</th>
                        <th>Student ID (UTA)</th>
                        <th>Document Status</th>
                        <th>View Document</th>
                        <th>Downloads</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->

				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <div id="attendeesList" class="attendeesListModal" >
          <!-- Modal content -->
          <div class="row attendeesList-content">
              <span class="listclose" id="closeModal">&times;</span>
              <div class="col-md-12 content">
                  <div class="panel panel-default">
                      <div class="panel-body">
                          <div class="table-container">
                              <h2 id="student-name" style="text-align:center;"></h2>
                              <table class="table table-filter">
                                  <thead>
                                      <tr>
                                          <th>Document Name</th>
                                          <th></th>
                                      </tr>
                                  </thead>
                                  <tbody id="attendees-list"></tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>

<script type="text/javascript">

function DownloadDocList(student_id,tokbox_id){

  event.preventDefault();

  $.ajax({

  type: "GET",

  url: "/admin/webinar/webinarmaster/downloadDocs/?data_value=NO&id="+student_id,

  cache: false,

  success: function(result){

    if(result == 'NODOCUMENT'){
      alert("Documents Not Uploaded ...!!!");
    } else {
      window.open("/admin/webinar/webinarmaster/downloadDocs/?data_value=YES&id="+student_id, "_blank");
    }

  }

      });

}

function ViewDocuments(student_id, student_name){
    $.ajax({
        url: "/admin/webinar/webinarmaster/viewDocs/?data_value=NO&id="+student_id,
        type: 'GET',
        complete: function complete() {
        },
        success: function success(data) {
            data = JSON.parse(data);
            if(data.length){
                document.getElementById("attendees-list").innerHTML = '';
                data.forEach(function(value){
                    var tr_node = document.createElement("tr");
                    tr_node.setAttribute("id", "attendees-list-" + value.user_id + "-" + value.document_meta_id);

                    var tdHtml = '<td>' + value.document_name + '</td>';
                    if(value.url){
                        tdHtml += '<td><a href="' + value.url + '" target="_blank">Open</a></td>';
                    }
                    else{
                        tdHtml += '<td>Not Uploaded</td>';
                    }

                    document.getElementById("attendees-list").appendChild(tr_node);
                    document.getElementById("attendees-list-" + value.user_id + "-" + value.document_meta_id).innerHTML = tdHtml;
                    document.getElementById("student-name").innerHTML = student_name;
                })
            }
        },
        error: function error() {
        }
    });
    attendeesListModal.style.display = "block";
}

var attendeesListModal = document.getElementById("attendeesList");
var coursespan = document.getElementById("closeModal");

coursespan.onclick = function() {
    attendeesListModal.style.display = "none";
}

window.onclick = function(event) {
    if (event.target == attendeesListModal) {
        attendeesListModal.style.display = "none";
    }
}

</script>
