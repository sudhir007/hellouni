
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Webinar
            <small>Manage Webinar</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Webinar Management</a></li>
            <li class="active">Webinar List</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

			  <!-- /.box -->
			  <?php if($this->session->flashdata('flash_message')){ ?>
				<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                <?php echo $this->session->flashdata('flash_message'); ?>
              </div>
			  <?php } ?>

              <div class="box">

			  <form method="post">

                <div class="box-header">
                 <a href="<?php echo base_url();?>webinar/webinarmaster/form">
                   <button type="button" style="width:12%; float:left; margin-right:5px;" class="btn btn-block btn-primary">
                     <i class="fa fa-plus"></i> Add Webinar</button>
                   </a>

                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Webinar By</th>
                        <th>Date Time</th>
                        <th>Status</th>
                        <th>Student Count</th>
                        <th>Organizer Link</th>
                        <th>Panelist Link</th>
                        <th>Student Link</th>
                        <th>Option</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php

					 foreach($webinarlist as $webinar_list){
                         $organizerLink = ($webinar_list['tokbox_token_id']) ? str_replace('/admin', '', base_url()) . 'meeting/webinar/organizer?id=' . base64_encode($webinar_list['tokbox_token_id']) : "";
                         $panelistLink = ($webinar_list['tokbox_token_id']) ? str_replace('/admin', '', base_url()) . 'meeting/webinar/panelist?id=' . base64_encode($webinar_list['tokbox_token_id']) : "";
                         $studentLink = ($webinar_list['tokbox_token_id']) ? str_replace('/admin', '', base_url()) . 'meeting/webinar/attendee?id=' . base64_encode($webinar_list['tokbox_token_id']) : "";
                         ?>
                      <tr>
                        <td><?php echo $webinar_list['id'];?></td>
                        <td><?php echo $webinar_list['webinar_name'];?></td>
                        <td><?php echo $webinar_list['webinar_host_name'];?></td>
                        <td><?php echo $webinar_list['webinar_date']." ". $webinar_list['webinar_time'];?></td>
                        <td><?php $status = $webinar_list['status'] == 1 ? "YES" : "NO"; echo $status;?></td>
                        <td><a href="<?php echo base_url();?>webinar/webinarmaster/studentlistbyid/<?php echo $webinar_list['id'];?>" title="Student List"><?php echo $webinar_list['acount'];?></a></td>
                        <td><?=($organizerLink) ? '<a href="' . $organizerLink . '" target="_blank">' . $organizerLink . '</a>' : "";?></td>
                        <td><?=($panelistLink) ? '<a href="' . $panelistLink . '" target="_blank">' . $panelistLink . '</a>' : "";?></td>
                        <td><?=($studentLink) ? '<a href="' . $studentLink . '" target="_blank">' . $studentLink . '</a>' : "";?></td>
			            <td><a href="<?php echo base_url();?>webinar/webinarmaster/form/<?php echo $webinar_list['id'];?>" title="Modify"><i class="fa fa-edit"></i></a></td>
                        </tr>
					<?php } ?>


                    </tbody>
                    <tfoot>
                      <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Webinar By</th>
                        <th>Date Time</th>
                        <th>Status</th>
                        <th>Student Count</th>
                        <th>Organizer Link</th>
                        <th>Panelist Link</th>
                        <th>Student Link</th>
                        <th>Option</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->

				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
