<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Webinar<small>Add Webinar</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Webinar Management</a></li>
            <li class="active">Create Poll</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-10">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Create Poll</h3>
                    </div><!-- /.box-header -->
                    <?php
                    if($this->session->flashdata('flash_message'))
                    {
                        ?>
                        <div class="alert alert-error alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                            <h4><i class="icon fa fa-circle-o"></i> Error</h4>
                            <?php echo $this->session->flashdata('flash_message'); ?>
                        </div>
                        <?php
                    }
                    if($this->uri->segment('4'))
                    {
                        ?>
                        <form class="form-horizontal" action="<?php echo base_url();?>webinar/webinarmaster/edit/<?php if(isset($w_id)) echo $w_id;?>" method="post" name="editwebinar" enctype="multipart/form-data">
                        <?php
                    }
                    else
                    {
                        ?>
                        <form class="form-horizontal" action="<?php echo base_url();?>webinar/webinarmaster/createPoll" method="post" name="addwebinar" enctype="multipart/form-data">
                        <?php
                    }
                    ?>
                    <div class="box-body">
                        <div class="form-group">
                          <label for="name" class="col-sm-2 control-label">Poll Title *</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" name="title" placeholder="Poll Title" value="<?php if(isset($title)) echo $title; ?>" >
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="name" class="col-sm-2 control-label">Time (In Minutes)</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" name="time" placeholder="Time (In Minutes)" value="<?php if(isset($time)) echo $time; ?>" >
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="name" class="col-sm-2 control-label">Select Question Type *</label>
                          <div class="col-sm-10">
                            <select class="form-control" name="type" onchange="showQuestion(this.value)">
                                <option value="">Please Select</option>
                                <option value="SINGLE_MCQ" <?php if(isset($type) && $type=="SINGLE_MCQ"){ echo 'selected="selected"'; } ?>>Single Question - Multiple Choice?</option>
                                <option value="MULTIPLE_MCQ" <?php if(isset($type) && $type=="MULTIPLE_MCQ"){ echo 'selected="selected"'; } ?>>Multiple Questions - Multiple Choice?</option>
                                <option value="SINGLE_SUBJECTIVE" <?php if(isset($type) && $type=="SINGLE_SUBJECTIVE"){ echo 'selected="selected"'; } ?>>Single Question - Subjective Answer?</option>
                                <option value="MULTIPLE_SUBJECTIVE" <?php if(isset($type) && $type=="MULTIPLE_SUBJECTIVE"){ echo 'selected="selected"'; } ?>>Multiple Questions - Subjective Answer?</option>
                            </select>
                          </div>
                        </div>

                        <div id = "single-mcq" style="display:none">
                            <div class="form-group">
                              <label for="name" class="col-sm-2 control-label">Question *</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control" name="single_question" placeholder="Poll Question" value="<?php if(isset($question)) echo $question; ?>" >
                              </div>
                            </div>

                            <div id="single-mcq-options">
                                <div class="form-group">
                                  <label for="name" class="col-sm-2 control-label">Option 1</label>
                                  <div class="col-sm-8">
                                    <input type="text" class="form-control" name="single_options[]" placeholder="Option 1" value="<?php if(isset($option)) echo $option; ?>" >
                                  </div>
                                  <div ><input type="button" name="add_ans" value="Add More" style="margin-left: 20px;" onclick="addSingleOptions()"></div>
                                </div>
                            </div>

                            <div class="form-group">
                              <label for="name" class="col-sm-2 control-label">Correct Answer?</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control" name="single_correct_ans" placeholder="Correct Answer" value="<?php if(isset($correct_ans)) echo $correct_ans; ?>" >
                              </div>
                            </div>
                        </div>

                        <div id = "multiple-mcq" style="display:none">
                            <div class="form-group">
                              <label for="name" class="col-sm-2 control-label">Question 1 *</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control" name="questions[]" placeholder="Poll Question 1" value="<?php if(isset($question)) echo $question; ?>" >
                              </div>
                              <!--div ><input type="button" name="add_que" value="Add More" style="margin-left: 20px;" onclick="addQuestions()"></div-->
                            </div>

                            <div id="multiple-mcq-options-1">
                                <div class="form-group">
                                  <label for="name" class="col-sm-2 control-label">Option 1</label>
                                  <div class="col-sm-8">
                                    <input type="text" class="form-control" name="options[]" placeholder="Option 1" value="<?php if(isset($option)) echo $option; ?>" >
                                  </div>
                                  <div ><input type="button" name="add_ans" value="Add More" style="margin-left: 20px;" onclick="addOptions(1)"></div>
                                </div>
                            </div>

                            <div class="form-group">
                              <label for="name" class="col-sm-2 control-label">Correct Answer?</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control" name="correct_ans[]" placeholder="Correct Answer" value="<?php if(isset($correct_ans)) echo $correct_ans; ?>" >
                              </div>
                            </div>

                        </div>
                    </div>
                    <div class="box-footer">
                      <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
  					<button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                      <?php
                      if($this->uri->segment('4'))
                      {
                          ?>
                          <button type="submit" class="btn btn-info pull-right">Update</button>
                          <?php
                      }
                      else
                      {
                          ?>
                          <button type="submit" class="btn btn-info pull-right">Create</button>
                          <?php
                      }
                      ?>
                    </div><!-- /.box-footer -->
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    var mCurrentOption = 1;
    var sCurrentOption = 1;
    function showQuestion(selectedOption)
    {
        if(selectedOption == 'SINGLE_MCQ')
        {
            document.getElementById("multiple-mcq").style.display = "none";
            document.getElementById("single-mcq").style.display = "block";
        }
        else if(selectedOption == 'MULTIPLE_MCQ')
        {
            document.getElementById("single-mcq").style.display = "none";
            document.getElementById("multiple-mcq").style.display = "block";
        }
    }
    function addOptions(questionNumber)
    {
        mCurrentOption++;
        var mainDiv = document.createElement("div");
        mainDiv.setAttribute("class", "form-group");
        mainDiv.setAttribute("id", "option-" + mCurrentOption);
        document.getElementById('multiple-mcq-options-' + questionNumber).appendChild(mainDiv);

        var html = '<label for="name" class="col-sm-2 control-label">Option ' + mCurrentOption + '</label>'
                + '<div class="col-sm-8">'
                + '<input type="text" class="form-control" name="options[]" placeholder="Option ' + mCurrentOption + '" >'
                + '</div>';
        document.getElementById('option-' + mCurrentOption).innerHTML = html;
    }
    function addSingleOptions()
    {
        sCurrentOption++;
        var mainDiv = document.createElement("div");
        mainDiv.setAttribute("class", "form-group");
        mainDiv.setAttribute("id", "single-option-" + sCurrentOption);
        document.getElementById('single-mcq-options').appendChild(mainDiv);

        var html = '<label for="name" class="col-sm-2 control-label">Option ' + sCurrentOption + '</label>'
                + '<div class="col-sm-8">'
                + '<input type="text" class="form-control" name="single_options[]" placeholder="Option ' + sCurrentOption + '" >'
                + '</div>';
        document.getElementById('single-option-' + sCurrentOption).innerHTML = html;
    }
</script>
