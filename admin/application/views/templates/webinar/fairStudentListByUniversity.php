
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Fair
            <small>Student List</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Fair Student </a></li>
            <li class="active">Student List </li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

			  <!-- /.box -->
			  <?php if($this->session->flashdata('flash_message')){ ?>
				<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                <?php echo $this->session->flashdata('flash_message'); ?>
              </div>
			  <?php } ?>

              <div class="box">

			  <form method="post">


                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Student Name</th>
                        <th>Student Email</th>
                        <th>Student Phone</th>
                        <th>Intake Detail</th>
                        <th>Competitive Exam</th>
                        <th>Competitive Exam</th>
                        <th>GPA Exam</th>
                        <th>GPA Score</th>
                        <th>Work Exp.</th>
                        <th>Work Exp. Month</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php

					 foreach($fairstudentlist as $webinar_student_list){ ?>
                      <tr>
                        <td><?php echo $webinar_student_list['name'];?></td>
                        <td><?php echo $webinar_student_list['email'];?></td>
                        <td><?php echo $webinar_student_list['phone'];?></td>
                        <td><?php echo $webinar_student_list['intake_month']." - ".$webinar_student_list['intake_year'];?></td>
                        <td><?php echo $webinar_student_list['comp_exam'];?></td>
                        <td><?php echo $webinar_student_list['comp_exam_score'];?></td>
                        <td><?php echo $webinar_student_list['gpa_exam'];?></td>
                        <td><?php echo $webinar_student_list['gpa_exam_score'];?></td>
                        <td><?php echo $webinar_student_list['work_exp'];?></td>
                        <td><?php echo $webinar_student_list['work_exp_month'];?></td>
						            <td><a href="<?php echo base_url();?>student/index/profile/?id=<?php echo $webinar_student_list['student_id'];?>" title="Modify" target="_blank"><i class="fa fa-edit"></i></a></td>
                        </tr>
					<?php } ?>


                    </tbody>
                    <tfoot>
                      <tr>

                          <th>Student Name</th>
                          <th>Student Email</th>
                          <th>Student Phone</th>
                          <th>Intake Detail</th>
                          <th>Competitive Exam</th>
                          <th>Competitive Exam</th>
                          <th>GPA Exam</th>
                          <th>GPA Score</th>
                          <th>Work Exp.</th>
                          <th>Work Exp. Month</th>
                          <th>Action</th>

                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->

				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
