

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Intake
            <small>Manage Intakes</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Intake Management</a></li>
            <li class="active">Add Intake</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
           
            <!-- right column -->
            <div class="col-md-10">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
				 <?php if($this->uri->segment('4')){?>
				 	<h3 class="box-title">Update Intake</h3>
				 <?php }else{?>
				 	<h3 class="box-title">Add Intake</h3>
				   <?php } ?>
                  
                </div><!-- /.box-header -->
                <!-- form start -->
				
				 <?php if($this->uri->segment('4')){?>
				<form class="form-horizontal" action="<?php echo base_url();?>course/intake/edit/<?php if(isset($id)) echo $id;?>" method="post" name="editintake">
				<?php }else{?>
                <form class="form-horizontal" action="<?php echo base_url();?>course/intake/create" method="post" name="addintake">
                <?php } ?>
				
				
                
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                      <div class="col-sm-10">
                        <input type="name" class="form-control" id="name" name="name" placeholder="Name" value="<?php if(isset($name)) echo $name; ?>">
                      </div>
                    </div>
					
					
					
					<div class="form-group">
                  		 <label for="status" class="col-sm-2 control-label">Status</label>
						  <div class="col-sm-10">
						  <select class="form-control" name="status">
							<option value="1" <?php if(isset($status) && $status=='1'){ echo 'selected="selected"'; } ?>>Active</option>
							<option value="2" <?php if(isset($status) && $status=='2'){ echo 'selected="selected"'; } ?>>Inactive</option>	
						  </select>
						  </div>
               		</div>
					
					 
                    
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
					<button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                    <button type="submit" class="btn btn-info pull-right">
					<?php if(!$this->uri->segment('4')){?>Create <?php }else{?> Update <?php }?></button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              <!-- general form elements disabled -->
             
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
     