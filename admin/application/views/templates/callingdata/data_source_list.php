<style>
    .counselors-list
    {
        display: none;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Calling Data<small>Source List</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Calling Data</a></li>
            <li class="active">Source List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php
                if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>

                <div class="box">

                        <div class="box-header">

                            <a href="<?php echo base_url();?>callingdata/source/addform"><button type="button" style="width:12%; float:left; margin-right:5px;" class="btn btn-block btn-primary"><i class="fa fa-plus"></i> Add Source </button></a>

                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table id="newAppExample" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th> Name</th>
                                        <th> Display Name </th>
                                        <th> Status </th>
                                        <th> Create Date </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    foreach($sources as $source)
                                    {
                                        ?>
                                        <tr>
                                            <td><a target="_blank" href='<?php echo base_url();?>callingdata/source/updateform/<?php echo $source['id'];?>'> <?php echo $source['name'];?> </a></td>
                                            <td><?php echo $source['display_name'];?></td>
                                            <td><?php echo $source['status'] == 1 ? 'ACTIVE' : 'DEACTIVE';?></td>
                                            <td><?php echo $source['date_created'];?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                      <th> Name</th>
                                      <th> Display Name </th>
                                      <th> Status </th>
                                      <th> Create Date </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">

    function showCounselors(studentId)
    {
        document.getElementById("assigned-counselor-" + studentId).style.display = "none";
        document.getElementById("counselors-list-" + studentId).style.display = "block";
    }

    function deleteUser(userId)
    {
        $.ajax({
            type: "GET",
            url: "/admin/student/index/delete?uid=" + userId,
            cache: false,
            success: function(result){
                alert("Student has been deleted successfully");
                window.location.href = '/admin/student/index/all';
            }
        });
    }

    function updateCounselor(studentId, counselor)
    {
        var splitCounselor = counselor.split('|');
        var counselorId = splitCounselor[0];
        var counselorName = splitCounselor[1];
        $.ajax({
            type: "POST",
            url: "/admin/student/index/update",
            data: {"student_id": studentId, "facilitator_id": counselorId},
            cache: false,
            success: function(result){
                if(result == 1){
                    alert('Success');
                    document.getElementById("assigned-counselor-" + studentId).innerHTML = counselorName;
                }
                else{
                    alert(result);
                }
                document.getElementById("assigned-counselor-" + studentId).style.display = "block";
                document.getElementById("counselors-list-" + studentId).style.display = "none";
            }
        });
    }
</script>

<script>
var table;
  $(document).ready(function() {

    //datatables
    table = $('#newAppExample').DataTable({ 
//alert('hi');
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url();?>callingdata/callingdata/ajax_manage_page_sourceList",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
                { "data": "name" },
                { "data": "display_name" },
                { "data": "status" },
                { "data": "date_created" }
            ],
        

    });

  });
  
</script>
