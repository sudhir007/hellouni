<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Student <small>Add Student</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Student Management</a></li>
            <li class="active">Add Student</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-10">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Student</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php
                    if($this->session->flashdata('flash_message'))
                    {
                        ?>
                        <div class="alert alert-error alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                            <h4><i class="icon fa fa-circle-o"></i> Error</h4>
                            <?php
                            echo $this->session->flashdata('flash_message');
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                    <form class="form-horizontal" action="<?php echo base_url();?>callingdata/bulk/uploaddata" method="post" enctype="multipart/form-data" name="upload_student" autocomplete="off">
                        <div class="box-body">


                            <div class="form-group">
                                <label for="country" class="col-sm-2 control-label">Source Name *</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="source_id" id="source_id" required>
                                      <option value="" > Select Source</option>
                                        <?php
                                        foreach($source_list as $sourceData)
                                        {
                                            ?>
                                            <option value="<?=$sourceData['id']?>"><?=$sourceData['name']?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="main_stream" class="col-sm-2 control-label">Upload CSV *</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control" name="userfile" id="userfile" />
                                </div>
                            </div>

                        </div>

                        <div class="box-footer">
                            <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                            <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                            <button type="submit" class="btn btn-info pull-right">Create</button>
                        </div><!-- /.box-footer -->
                    </form>
                </div><!-- /.box -->
                <!-- general form elements disabled -->
            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
    function showSubStream(mainstreamId)
    {
        $("#course_category_id").empty();
        $.ajax({
            type: "GET",
            url: "/admin/student/index/substream?cat_id=" + mainstreamId,
            cache: false,
            success: function(result){
                result = JSON.parse(result);
                result.forEach(function(value){
                    var option = document.createElement("option");
                    option.text = value.display_name;
                    option.value = value.id;
                    var select = document.getElementById("course_category_id");
                    select.appendChild(option);
                })
            }
        });
    }
</script>
