<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Calling Data <small>Update Source</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Calling Data</a></li>
            <li class="active">Update Source</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-10">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Update Source</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php
                    if($this->session->flashdata('flash_message'))
                    {
                        ?>
                        <div class="alert alert-error alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                            <h4><i class="icon fa fa-circle-o"></i> Error</h4>
                            <?php
                            echo $this->session->flashdata('flash_message');
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                    <?php if($type_of_form == "UPDATE_FORM") {?>
                    <form class="form-horizontal" action="<?php echo base_url();?>callingdata/source/update" method="post" name="update_source" autocomplete="off">
                    <?php } else { ?>
                    <form class="form-horizontal" action="<?php echo base_url();?>callingdata/source/add" method="post" name="add_source" autocomplete="off">
                    <?php } ?>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label"> Name *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Source Name" value="<?=$source_info['name']?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-sm-2 control-label">Display Name *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="display_name" name="display_name" placeholder="Display Name" value="<?=$source_info['display_name']?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="mobile" class="col-sm-2 control-label">Status *</label>
                                <div class="col-sm-10">
                                  <select class="form-control" name="status" required>
                                    <option value="" > Select Status</option>
                                    <option value="1" <?php echo $source_info['status'] == 1 ? "selected ": '';?>> ACTIVE </option>
                                    <option value="0" <?php echo $source_info['status'] != 1 ? 'selected': '';?>> DEACTIVE </option>
                                  </select>

                                </div>
                            </div>


                        <div class="box-footer">
                            <input type="hidden" name="id" id="id" value="<?=$source_info['id']?>">
                            <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                            <button type="submit" class="btn btn-info pull-right"> Update </button>
                        </div><!-- /.box-footer -->
                    </form>
                </div><!-- /.box -->
                <!-- general form elements disabled -->
            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
