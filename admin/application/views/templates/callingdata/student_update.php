<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Calling Data <small>Update Student</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Calling Data</a></li>
            <li class="active">Update Student</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-10">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Update Student</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php
                    if($this->session->flashdata('flash_message'))
                    {
                        ?>
                        <div class="alert alert-error alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                            <h4><i class="icon fa fa-circle-o"></i> Error</h4>
                            <?php
                            echo $this->session->flashdata('flash_message');
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                    <form class="form-horizontal" action="<?php echo base_url();?>callingdata/student/update" method="post" name="update_student" autocomplete="off">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">Student Name *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Student Name" value="<?=$student_info['name']?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-sm-2 control-label">Student Email *</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Student Email" value="<?=$student_info['email']?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="mobile" class="col-sm-2 control-label">Student Mobile *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Student Mobile Number" value="<?=$student_info['mobile']?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="mobile" class="col-sm-2 control-label">Student Mobile 1 *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="mobile1" name="mobile1" placeholder="Student Mobile Number" value="<?=$student_info['mobile1']?>">
                                </div>
                            </div>

                        <div class="box-footer">
                            <input type="hidden" name="id" id="id" value="<?=$student_info['id']?>">
                            <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                            <button type="submit" class="btn btn-info pull-right"> Update </button>
                        </div><!-- /.box-footer -->
                    </form>
                </div><!-- /.box -->
                <!-- general form elements disabled -->
            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
