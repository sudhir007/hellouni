     <style type="text/css">
       .tt {
         height: 0px !important;
       }
     </style>
     <!-- Content Wrapper. Contains page content -->
     <div class="content-wrapper">
       <!-- Content Header (Page header) -->
       <section class="content-header">
         <h1>
           Team
           <small>Manage Team Registration</small>
         </h1>
         <ol class="breadcrumb">
           <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
           <li><a href="#">Team Management</a></li>
           <li class="active">Update Team Registration</li>
         </ol>
       </section>

       <!-- Main content -->
       <section class="content">
         <div class="row">

           <!-- right column -->
           <div class="col-md-10">
             <!-- Horizontal Form -->
             <div class="box box-info">
               <div class="box-header with-border">
                 <h3 class="box-title">Update Team Registration</h3>
               </div><!-- /.box-header -->
               <!-- form start -->

               <?php //$userdata = $this->session->userdata;
                ?>


               <form class="form-horizontal" action="<?php echo base_url(); ?>team/registration/teamUpdate_action/<?php echo $this->uri->segment(4); ?>" method="post" enctype="multipart/form-data">

                 <div class="box-body">

                   <div class="form-group">
                     <div class="col-sm-10" style="color:green;">
                       <?php if ($this->session->flashdata('flash_message')) {
                          echo $this->session->flashdata('flash_message');
                        } ?>
                     </div>
                   </div>
                   <div class="form-group">
                     <label for="inputEmail3" class="col-sm-2 control-label"> Name * </label>
                     <div class="col-sm-10">
                       <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Name" value="<?php if (isset($user_name)) echo $user_name; ?>" required>
                     </div>
                   </div>

                   <div class="form-group">
                     <label for="inputEmail3" class="col-sm-2 control-label">Email *</label>
                     <div class="col-sm-10">
                       <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email" value="<?php if (isset($user_email)) echo $user_email; ?>" required>
                     </div>
                   </div>

                   <div class="form-group">
                     <label for="inputEmail3" class="col-sm-2 control-label">Mobile Number ( WhatsApp ) * </label>
                     <div class="col-sm-10">
                       <input type="text" class="form-control" id="user_mobile" name="user_mobile" placeholder="Mobile Number" value="<?php if (isset($user_mobile)) echo $user_mobile; ?>" required>
                     </div>
                   </div>

                   <!-- <div class="form-group">
                     <label for="inputEmail3" class="col-sm-2 control-label"> Role * </label>
                     <div class="col-sm-10">
                       //<input type="text" class="form-control" id="user_role" name="user_role" placeholder="Role" value="<?php if (isset($user_role)) echo $user_role; ?>" readonly>
                       <select name="user_role" id="user_role" class="form-control searchoptions" onchange="getSubRole();" style="width:100%; height:30px; font-size:12px;" required>
                         <option value="">Select Role</option>
                         <?php if ($role_id == 1) { ?> <option <?php if ($user_role == 2) echo 'selected="selected"'; ?> value="2"> Admin </option>
                           <option value="7" <?php if ($user_role == 7) echo 'selected="selected"'; ?>> Counselor </option> <?php } ?>
                         <?php if ($role_id == 2) { ?><option <?php if ($user_role == 7) echo 'selected="selected"'; ?> value="7"> Counselor </option><?php } ?>
                       </select>
                     </div>
                   </div>

                   <div class="form-group" id="super_admin_list" style="display:none" ;>
                     <label for="inputEmail3" class="col-sm-2 control-label"> Super Admin Member * </label>
                     <div class="col-sm-10">
                       <select name="super_admin_member" id="super_admin_member" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;">
                         <option value=""> Select Super Admin </option>
                         <?php foreach ($superAdmin_list as $rowData) {  ?>
                           <option value="<?= $rowData['id']; ?>"><?= $rowData['name']; ?> </option>
                         <?php } ?>
                       </select>
                     </div>
                   </div>

                   <div class="form-group" id="admin_list" style="display:none" ;>
                     <label for="inputEmail3" class="col-sm-2 control-label"> Admin Member * </label>
                     <div class="col-sm-10">
                       <select name="admin_member" id="admin_member" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;">
                         <option value=""> Select Admin </option>
                         <?php foreach ($admin_list as $rowData) {  ?>
                           <option value="<?= $rowData['id']; ?>"><?= $rowData['name']; ?> </option>
                         <?php } ?>
                       </select>
                     </div>
                   </div> -->

                   <div class="form-group">
                     <label for="user_role" class="col-sm-2 control-label">Role *</label>
                     <div class="col-sm-10">
                       <select name="user_role" id="user_role" class="form-control searchoptions" onchange="getSubRole();" style="width:100%; height:30px; font-size:12px;" required>
                         <option value="">Select Role</option>
                         <?php if ($role_id == 1) { ?>
                           <option value="2" <?= ($user_role == 2) ? 'selected="selected"' : ''; ?>>Admin</option>
                           <option value="7" <?= ($user_role == 7) ? 'selected="selected"' : ''; ?>>Counselor</option>
                         <?php } ?>
                         <?php if ($role_id == 2) { ?>
                           <option value="7" <?= ($user_role == 7) ? 'selected="selected"' : ''; ?>>Counselor</option>
                         <?php } ?>
                       </select>
                     </div>
                   </div>

                   <div class="form-group" id="super_admin_list" style="display:none;">
                     <label for="super_admin_member" class="col-sm-2 control-label">Super Admin Member *</label>
                     <div class="col-sm-10">
                       <select name="super_admin_member" id="super_admin_member" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;">
                         <option value="">Select Super Admin</option>
                         <?php foreach ($superAdmin_list as $rowData) { ?>
                           <option value="<?= $rowData['id']; ?>" <?= ($selected_super_admin == $rowData['id']) ? 'selected="selected"' : ''; ?>><?= $rowData['name']; ?></option>
                         <?php } ?>
                       </select>
                     </div>
                   </div>

                   <div class="form-group" id="admin_list" style="display:none;">
                     <label for="admin_member" class="col-sm-2 control-label">Admin Member *</label>
                     <div class="col-sm-10">
                       <select name="admin_member" id="admin_member" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;">
                         <option value="">Select Admin</option>
                         <?php foreach ($admin_list as $rowData) { ?>
                           <option value="<?= $rowData['id']; ?>" <?= ($parentId == $rowData['id']) ? 'selected="selected"' : ''; ?>><?= $rowData['name']; ?></option>
                         <?php } ?>
                       </select>
                     </div>
                   </div>

                   <div class="form-group">
                     <label for="inputEmail3" class="col-sm-2 control-label"> Status * </label>
                     <div class="col-sm-10">
                       <select name="user_status" id="user_status" class="form-control searchoptions">
                         <option value="">Select State</option>
                         <option value="1" <?php if ($user_status == 1) echo 'selected="selected"'; ?>>Active</option>
                         <option value="0" <?php if ($user_status == 0) echo 'selected="selected"'; ?>>Inactive</option>
                       </Select>
                     </div>
                   </div>

                 </div><!-- /.box-body -->
                 <div class="box-footer">
                   <input type="hidden" name="partnerId" id="partnerId" value="<?php echo $this->uri->segment(4); ?>">
                   <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                   <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                   <button type="submit" class="btn btn-info pull-right">Update</button>
                 </div><!-- /.box-footer -->
               </form>
             </div><!-- /.box -->
             <!-- general form elements disabled -->

           </div><!--/.col (right) -->
         </div> <!-- /.row -->
       </section><!-- /.content -->
     </div><!-- /.content-wrapper -->


     <script type="text/javascript">
       //  $( document ).ready(function() {

       //registration

       function resgistration() {

         var user_name = $('#user_name').val();
         var user_email = $('#user_email').val();
         //var user_id = $('#user_id').val();
         var user_mobile = $('#user_mobile').val();
         var user_password = $('#user_password').val();
         var agency_name = $('#agency_name').val();
         var sstate = $('#sstate').val();
         var ccity = $('#ccity').val();
         var user_add = $('#user_add').val();

         var dataString = 'user_name=' + user_name + '&user_email=' + user_email + '&user_mobile=' + user_mobile + '&agency_name=' + agency_name +
           '&user_password=' + user_password + '&state_id=' + sstate + '&city_id=' + ccity + '&user_add=' + user_add;

         event.preventDefault();

         $.ajax({

           type: "POST",

           url: "/admin/counsellor/registration/counsellorSubmit",

           data: dataString,

           cache: false,

           success: function(result) {

             if (result == "SUCCESS") {

               alert("Thank You For Registration With Hellouni Platform..!!!");
               window.location.href = '/admin/counsellor/register';

             } else if (result == 'With Email-ID Or Mobile Account Already Exists') {

               alert(result);
               window.location.href = '/common/login';

             } else {

               alert(result);

             }

           }

         });

       }


       // Get City 
       function getCity() {
         //alert('hi');
         var stateId = $('#sstate').val();
         var dataString = "state_id=" + stateId;
         $.ajax({
           type: "POST",
           url: "/admin/counsellor/registration/cityListByStateId",
           data: dataString,
           cache: false,
           success: function(result) {
             var cityList = result.city_list;
             if (cityList.length) {
               var cityDropDown = '';
               cityList.forEach(function(value, index) {
                 cityDropDown += "<option value='" + value.id + "' >" + value.city_name + "</option>";
               });
             }
             cityDropDown += "<option value='1000000' > OTHER </option>";
             document.getElementById("ccity").innerHTML = cityDropDown;
           }
         });
       }

       function getSubRole() {
         var user_role = $('#user_role').val();
         var role_type = $('#role_type').val(); 

         if (user_role == 2) {
           $('#super_admin_list').show();
           $('#admin_list').hide();
         } else if (user_role == 7) {
           $('#super_admin_list').hide();
           $('#admin_list').show();
         } else {
           $('#super_admin_list').hide();
           $('#admin_list').hide();
         }
       }

       $(document).ready(function() {
         getSubRole();
       });
     </script>