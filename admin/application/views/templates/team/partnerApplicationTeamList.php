
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Partner Module<small>My Partner Team List</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Partner Module</a></li>
            <li class="active">My Partner Team List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php
                if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>

                <div class="box">
                      <div class="box-header">
                        <!-- <a href="/admin/counsellor/register"><button type="button" style="width:15%; float:left; margin-right:5px;" class="btn btn-block btn-secondary"><i class="fa fa-plus"></i> Create New Partner </button></a> -->
                        </div><!-- /.box-header -->
                          <div class="box-body">
                            <div class="table-responsive">
                            <table id="example" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th> Partner id </th>
                                        <th> Partner Name </th>
                                        <th> Partner Email </th>
                                        <th> Partner Username </th>
                                        <th> Partner phone </th>
                                        <th> Agency Name </th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    foreach($partnersDetails as $partner)
                                    {
                                        $agency_name = json_decode($partner->other_details, true);
                                        ?>
                                        <tr>
                                            <td><?php echo $partner->user_id;?></td>
                                            <td><?php echo $partner->name . " ". $partner->last_name;?></td>
                                            <td><?php echo $partner->email;?></td>
                                            <td><?php echo $partner->username;?></td>
                                            <td><?php echo $partner->phone;?></td>
                                            <td><?php echo $agency_name['agency_name'];?></td>
                                            
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th> Partner id </th>
                                        <th> Partner Name </th>
                                        <th> Partner Email </th>
                                        <th> Partner Username </th>
                                        <th> Partner phone </th>
                                        <th> Agency Name </th>
                                        
                                    </tr>
                                </tfoot>
                            </table>
                          </div>
                        </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">

$(document).ready(function() {
    $('#example thead tr').clone(true).appendTo( '#example thead' );
    $('#example thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text"  />' );
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                .column(i)
                .search( this.value )
                .draw();
            }
        });
    });

    var table = $('#example').DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        "columnDefs": [
            { "width": "10%", "targets": 0 },
            { "width": "14%", "targets": 1 },
            { "width": "15%", "targets": 2 },
            { "width": "10%", "targets": 3 },
            { "width": "14%", "targets": 4 },
            { "width": "10%", "targets": 5 },
            { "width": "10%", "targets": 5 }
        ]
    });
});

</script>
