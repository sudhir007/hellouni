
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Team Module<small>My Team List</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Team Module</a></li>
            <li class="active">My Team List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php
                if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>

                <div class="box">
                      <div class="box-header">
                        <a href="/admin/team/register"><button type="button" style="width:15%; float:left; margin-right:5px;" class="btn btn-block btn-secondary"><i class="fa fa-plus"></i> Create New Team </button></a>
                        </div><!-- /.box-header -->
                          <div class="box-body">
                            <div class="table-responsive">
                            <table id="example" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th> Team id </th>
                                        <th> Team Name </th>
                                        <th> Team Email </th>
                                        <th> Team Username </th>
                                        <th> Team phone </th>
                                        <th> Delete Key </th>
                                        <th> Action </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    foreach($teamsDetails as $team)
                                    {
                                        ?>
                                        <tr>
                                            <td><?php echo $team->id;?></td>
                                            <td><?php echo $team->name . " ". $team->last_name;?></td>
                                            <td><?php echo $team->email;?></td>
                                            <td><?php echo $team->username;?></td>
                                            <td><?php echo $team->phone;?></td>
                                            <td><button type="button" class="btn btn-info btn-sm" onclick="destroyLoginKey(<?php echo $team->id; ?>);">Delete Key </button></td>
                                            <td><a href='/admin/team/register/update/<?php echo $team->id; ?>' title="Modify" class="edit" ><i class="fa fa-edit"></i></a></td>

                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th> Team id </th>
                                        <th> Team Name </th>
                                        <th> Team Email </th>
                                        <th> Team Username </th>
                                        <th> Team phone </th>
                                        <th> Delete Key </th>
                                        <th> Action </th>

                                    </tr>
                                </tfoot>
                            </table>
                          </div>
                        </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">

$(document).ready(function() {
    $('#example thead tr').clone(true).appendTo( '#example thead' );
    $('#example thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text"  />' );
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                .column(i)
                .search( this.value )
                .draw();
            }
        });
    });

    var table = $('#example').DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        "columnDefs": [
            { "width": "10%", "targets": 0 },
            { "width": "14%", "targets": 1 },
            { "width": "15%", "targets": 2 },
            { "width": "10%", "targets": 3 },
            { "width": "14%", "targets": 4 },
            { "width": "10%", "targets": 5 },
            { "width": "7%", "targets": 5 }
        ]
    });
});

</script>

<script type="text/javascript">
    function destroyLoginKey(loginKey){
       // alert(loginKey);
        var dataString = 'login_id=' + loginKey;

        event.preventDefault();
  $.ajax({
        type: "POST",
        url: "/admin/team/register/deleteTeamLoginKey",
        data: dataString,
        cache: false,
        success: function(result) {
          //alert(result);
          if(result.status == "SUCCESS") {
            alert("Counsellor Login Key Destroy Successfully!!!");
          } else if(result.status == 'NOTVALIDUSER') {
            //alert(result);
            window.location.href = '/user/account';
          } else {
            alert(result);
            location.reload();
          }
          
        }
      });
    }
</script>

