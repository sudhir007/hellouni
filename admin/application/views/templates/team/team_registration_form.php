     <style type="text/css">.tt{ height:0px !important;}</style>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Team
            <small>Manage Team Registration</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Team Management</a></li>
            <li class="active">Team Registration</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
           
            <!-- right column -->
            <div class="col-md-10">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Team Registration</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
				
				<?php //$userdata = $this->session->userdata;?>
                
				
				<form class="form-horizontal" onsubmit="resgistration()" method="post" name="changepassword" enctype="multipart/form-data">
                
                  <div class="box-body">

                  <div class="form-group">
                    <div class="col-sm-10" style="color:green;">
                      <?php if($this->session->flashdata('flash_message')){
                               echo $this->session->flashdata('flash_message');
                       } ?>
                    </div>
                  </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label"> Name * </label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Name" required>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Email *</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email" required>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Mobile Number ( WhatsApp ) * </label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="user_mobile" name="user_mobile" placeholder="Mobile Number"  required>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label"> Password * </label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" id="user_password" name="user_password" placeholder="Password"  required>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label"> Role * </label>
                      <div class="col-sm-10">
                        <select name="user_role" id="user_role" class="form-control searchoptions" onchange="getSubRole();" style="width:100%; height:30px; font-size:12px;" required>
                             <option value="">Select Role</option>
                            <?php if($role_id == 1 ) { ?> 
                              <option value="2"> Admin </option> 
                              <option value="7"> Counselor </option>
                              <option value="11">Marketing Manager</option>
                              <option value="12">Marketing Counselor</option> 
                            <?php } ?>
                             <?php if($role_id == 2 ) { ?><option value="7"> Counselor </option><?php } ?>
                          </select>

                      </div>
                    </div>

                    <div class="form-group" id="super_admin_list" style="display:none";>
                      <label for="inputEmail3" class="col-sm-2 control-label"> Super Admin Member * </label>
                      <div class="col-sm-10">
                        <select name="super_admin_member" id="super_admin_member" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;">
                             <option value=""> Select Super Admin </option>
                             <?php foreach ($superAdmin_list as $rowData) {  ?>
                                  <option value="<?= $rowData['id']; ?>"><?= $rowData['name']; ?> </option>
                             <?php } ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group" id="admin_list" style="display:none";>
                      <label for="inputEmail3" class="col-sm-2 control-label"> Admin Member * </label>
                      <div class="col-sm-10">
                        <select name="admin_member" id="admin_member" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;">
                             <option value=""> Select Admin </option>
                             <?php foreach ($admin_list as $rowData) {  ?>
                                  <option value="<?= $rowData['id']; ?>"><?= $rowData['name']; ?> </option>
                             <?php } ?>
                        </select>
                      </div>
                    </div>
                    
					</div><!-- /.box-body -->
                  <div class="box-footer">
                    <input type="hidden" name="role_type" id="role_type" value="<?=$role_id?>">
                    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
					<button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                    <button type="submit" class="btn btn-info pull-right">Register</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              <!-- general form elements disabled -->
             
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     

<script type="text/javascript">
//  $( document ).ready(function() {

    //registration

   function resgistration(){

     var user_name = $('#user_name').val();
     var user_email = $('#user_email').val();
     var user_mobile = $('#user_mobile').val();
     var user_password = $('#user_password').val();
     var user_role = $('#user_role').val();
     var role_type = $('#role_type').val();
     
     if(user_role == 2 || user_role == 11 || user_role == 12){ 
        var parentId = $('#super_admin_member').val();
      }else{
        var parentId = $('#admin_member').val();
     }
     
     
    var dataString = 'user_name=' + user_name + '&user_email=' + user_email + '&user_mobile=' + user_mobile + '&user_password=' + user_password + '&role_id=' + user_role + '&parent_id=' + parentId + '&role_type=' + role_type;
    
   
     event.preventDefault();

     $.ajax({

     type: "POST",

     url: "/admin/team/registration/teamSubmit",

     data: dataString,

     cache: false,

     success: function(result){

      if(result == "SUCCESS"){

        alert("Thank You For Registration With Hellouni Platform..!!!");
        window.location.href='/admin/team/register/allList';

      } else if(result == 'With Email-ID Or Mobile Account Already Exists'){

        alert(result);
        window.location.href='/common/login';

      } else {

        alert(result);

      }

     }

         });

   }


   


      // get sub role

      function getSubRole(){

        var user_role = $('#user_role').val();
        var role_type = $('#role_type').val();
        
        if(user_role == 2 || user_role == 11 || user_role == 12){
          $('#super_admin_list').show();
          $('#admin_list').hide();
        }else if(user_role == 7){
          $('#super_admin_list').hide();
          $('#admin_list').show();
        }else{
          if(user_role == role_type){
            $('#super_admin_list').hide();
            $('#admin_list').show();
          }else{
            $('#super_admin_list').hide();
            $('#admin_list').hide();
          }
          
        }
        
      }

     
//  }
</script>