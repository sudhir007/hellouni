<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>HelloUni | Control Panel</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/AdminLTE.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>plugins/datatables/dataTables.bootstrap.css">
  <!-- Font awesome -->
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
	 <!-- Select2 -->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />

	<!-- el finder -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>plugins/elfinder/css/elfinder.css"media="screen" >

	<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>


		<!-- elFinder CSS (REQUIRED) -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>elfinder/css/elfinder.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>elfinder/css/theme.css">

	<!-- Bootstrap core CSS -->
   <!-- <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">-->

	 <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/main.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/croppic.css" rel="stylesheet">




	<script type="text/javascript" src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>

	<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>-->
	<script src="<?php echo base_url();?>assets/js/jquery.plugin.js"></script>

	<script src="<?php echo base_url(); ?>plugins/elfinder/js/elfinder.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
      .icon-button {
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 30px;
  height: 30px;
  color: #f9992f;
  background: #f5f5f5;
  border: none;
  outline: none;
  border-radius: 50%;
  top: 10px;
}

.icon-button:hover {
  cursor: pointer;
}

.icon-button:active {
  background: #cccccc;
}

.icon-button__badge {
  position: absolute;
  top: -5px;
  right: -5px;
  width: 20px;
  height: 20px;
  background: red;
  color: #ffffff;
  font-weight: 600;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
}

#notification-list {
  width: 300px;
  max-height: 400px;
  overflow-y: scroll;
}

#notification-list .dropdown-menu > .panel {
  border: none;
  margin: -5px 0;
}

.panel-heading {
  background-color: #f1f1f1;
  border-bottom: 1px solid #dedede;
}

.activity-item i {
  float: left;
  margin-top: 3px;
  font-size: 16px;
}

div.activity {
  margin-left: 28px;
}

div.activity-item {
  padding: 7px 12px;
}

#notification-list div.activity-item {
  border-top: 1px solid #f5f5f5;
}

#notification-list div.activity-item a {
  font-weight: 600;
}

div.activity span {
  display: block;
  color: #999;
  font-size: 11px;
  line-height: 16px;
}

#notifications i.fa {
  font-size: 17px;
}

.noty_type_error * {
  font-weight: normal !important;
}

.noty_type_error a {
  font-weight: bold !important;
}

.noty_bar.noty_type_error a, .noty_bar.noty_type_error i {
  color: #fff
}

.noty_bar.noty_type_information a {
  color: #fff;
  font-weight: bold;
}

.noty_type_error div.activity span
{
  color: #fff
}

.noty_type_information div.activity span
{
  color: #fefefe
}

.no-notification {
  padding: 10px 5px;
  text-align: center;
}








.noty-manager-wrapper {
  position: relative;
  display: inline-block !important;
}

.noty-manager-bubble
{
  position: absolute;
  top: -8px;
  background-color: #fb6b5b;
  color: #fff;
  padding: 2px 5px !important;
  font-size: 9px;
  line-height: 12px;
  text-align: center;
  white-space: nowrap;
  vertical-align: baseline;
  cursor: pointer;
  height: 15px;
  font-weight: bold;

  border-radius: 2px;
  -moz-border-radius: 2px;
  -webkit-border-radius: 2px;
  box-shadow:1px 1px 1px rgba(0,0,0,.1);
  opacity: 0;
}

    </style>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="javascript:void();" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>H</b>U</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b> <?php if($this->session->userdata('user')['typename']){ echo $this->session->userdata('user')['typename']; }?></b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <?php
              /*
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>
                  <span class="label label-success">4</span>                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 4 messages</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li><!-- start message -->
                        <a href="#">
                          <div class="pull-left">
                            <img src="<?php echo base_url(); ?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            Support Team
                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li><!-- end message -->
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            AdminLTE Design Team
                            <small><i class="fa fa-clock-o"></i> 2 hours</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            Developers
                            <small><i class="fa fa-clock-o"></i> Today</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            Sales Department
                            <small><i class="fa fa-clock-o"></i> Yesterday</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            Reviewers
                            <small><i class="fa fa-clock-o"></i> 2 days</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="footer"><a href="#">See All Messages</a></li>
                </ul>
              </li>
              <!-- Notifications: style can be found in dropdown.less -->
              <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning">10</span>                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 10 notifications</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li>
                        <a href="#">
                          <i class="fa fa-users text-aqua"></i> 5 new members joined today
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-users text-red"></i> 5 new members joined
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-user text-red"></i> You changed your username
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="footer"><a href="#">View all</a></li>
                </ul>
              </li>
              <!-- Tasks: style can be found in dropdown.less -->
              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-flag-o"></i>
                  <span class="label label-danger">9</span>                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 9 tasks</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Design some buttons
                            <small class="pull-right">20%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">20% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Create a nice theme
                            <small class="pull-right">40%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">40% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Some task I need to do
                            <small class="pull-right">60%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">60% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Make beautiful transitions
                            <small class="pull-right">80%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">80% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                    </ul>
                  </li>
                  <li class="footer">
                    <a href="#">View all tasks</a>
                  </li>
                </ul>
              </li>
              */
              ?>
              <!-- User Account: style can be found in dropdown.less -->
              <li id="notificationsli">
                       <button id="notifications" href="#" id="drop3" role="button" onclick="notificationUpdate()" class="dropdown-toggle icon-button" data-toggle="dropdown">
                        <span class="material-icons">notifications</span>
                        <span class="icon-button__badge" id="notification_count">0</span>
                      </button>

                     <div id="notification-container" class="dropdown-menu" role="menu" aria-labelledby="drop3">

                        <section class="panel">
                            <header class="panel-heading">
                                <strong>Notifications</strong>
                            </header>
                            <div id="notification-list" class="list-group list-group-alt">

                              <div style="">
                                <div class="noty-manager-list-item noty-manager-list-item-error">
                                  <div class="activity-item"> <i class="fa fa-envelope "></i>
                                    <div class="activity">
                                      AAA Application Status changed<span>2 minutes ago</span>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div style="">
                                <div class="noty-manager-list-item noty-manager-list-item-error">
                                  <div class="activity-item"> <i class="fa fa-envelope "></i>
                                    <div class="activity">
                                      PQR Status changed<span>14 minutes ago</span>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div style="">
                                <div class="noty-manager-list-item noty-manager-list-item-error">
                                  <div class="activity-item"> <i class="fa fa-envelope"></i>
                                    <div class="activity">
                                      AAA Application Status changed<span>20 minutes ago</span>
                                    </div>
                                  </div>
                                </div>
                              </div>


                              <!-- <div style=""><div class="noty-manager-list-item noty-manager-list-item-error"><div class="activity-item"> <i class="fa fa-shopping-cart text-success"></i> <div class="activity"> <a href="#">Eugene</a> ordered 2 copies of <a href="#">OEM license</a> <span>14 minutes ago</span> </div> </div></div></div> -->

                            </div>
                            <footer class="panel-footer">
                                <!-- <a href="#" class="pull-right"><i class="fa fa-cog"></i></a> -->
                                <a href="<?php echo base_url();?>counsellor/notification" data-toggle="class:show animated fadeInRight" style="color: #f9992f">See all the notifications</a>
                            </footer>
                        </section>

                    </div>
                    </li>
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                   <?php if($this->session->userdata('user')['image']){?>
				   <img src="<?php echo $this->session->userdata('user')['image']; ?>" class="user-image" alt="User Image">
				   <?php }else{ ?>
				   <img src="<?php echo base_url();?>/images/no_image.jpg" class="user-image" alt="User Image">
				    <?php } ?>

                  <span class="hidden-xs"><?php echo $this->session->userdata('user')['typename'];?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <?php if($this->session->userdata('user')['image']){?>
					<img src="<?php echo $this->session->userdata('user')['image']; ?>" class="img-circle" alt="User Image">
					<?php } ?>
                    <p>
                      <?php echo $this->session->userdata('user')['name'];?> - <?php echo $this->session->userdata('user')['typename'];?>
                      <!--small>Member since Nov. 2012</small-->
                    </p>
                  </li>

                  <!-- Menu Body -->
                  <!--li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                </li-->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <!--div class="pull-left">
                      <a href="<?php echo base_url(); ?>common/profile" class="btn btn-default btn-flat">Profile</a>
                  </div-->
                    <div class="pull-right">
                      <a href="<?php echo base_url(); ?>common/login/logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <!--<li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>-->
            </ul>
          </div>
        </nav>
      </header>
      <script type="text/javascript">
        $( document ).ready(function() {

    $('#notifications').click(function( event ) {

        console.log( "Thanks for visiting!" );

      $('#notificationsli').toggleClass('open');

    });

});


        function notificationInfo(){

  $.ajax({
    type: "GET",
    url: "/admin/counsellor/notification/list",
    cache: false,
    success: function(result) {
//alert(result.notification_list);
      if(result.status == "SUCCESS") {

        document.getElementById("notification_count").innerHTML = result.notification_count;

        var  data = result.notification_list;

        if(data){

          console.log(data);
            document.getElementById("notification-list").innerHTML = '';

            var dataHtml = "";

            data.forEach(function(value, index){

                  dataHtml += "<div style=''>";
                  dataHtml += "<div class='noty-manager-list-item noty-manager-list-item-error'>";
                  dataHtml += "<div class='activity-item'> <i class='fa fa-envelope'></i>";
                  dataHtml += "<div class='activity'>";
                  dataHtml +=  "" + value.description + "";
                  dataHtml +=  "<span>"+value.date_created+"</span>";
                  dataHtml += "</div>";
                  dataHtml += "</div>";
                  dataHtml += "</div>";
                  dataHtml += "</div>";

            })

            document.getElementById("notification-list").innerHTML = dataHtml;
        }

      } else if(result.status == 'NOTVALIDUSER') {
        //alert(result);
        window.location.href = '/user/account';
      } else {
        alert(result);
        //location.reload();
      }
    }
  });

}

//notificationInfo();

function notificationUpdate(){

  $.ajax({
    type: "GET",
    url: "/admin/counsellor/notification/seen",
    cache: false,
    success: function(result) {

      console.log(result);

    }
  });

}
      </script>
