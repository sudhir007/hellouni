<footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 0.0.1
        </div>
        <strong>Copyright &copy; 2015 <a href="http://www.inventifweb.com/">Hello Uni</a>.</strong> All rights reserved.
      </footer>

      <?php
      /*
      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
          <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-user bg-yellow"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>
                    <p>New phone +1(800)555-1234</p>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>
                    <p>nora@example.com</p>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-file-code-o bg-green"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>
                    <p>Execution time 5 seconds</p>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->

            <h3 class="control-sidebar-heading">Tasks Progress</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <h4 class="control-sidebar-subheading">
                    Custom Template Design
                    <span class="label label-danger pull-right">70%</span>
                  </h4>
                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript::;">
                  <h4 class="control-sidebar-subheading">
                    Update Resume
                    <span class="label label-success pull-right">95%</span>
                  </h4>
                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript::;">
                  <h4 class="control-sidebar-subheading">
                    Laravel Integration
                    <span class="label label-warning pull-right">50%</span>
                  </h4>
                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript::;">
                  <h4 class="control-sidebar-subheading">
                    Back End Framework
                    <span class="label label-primary pull-right">68%</span>
                  </h4>
                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->

          </div><!-- /.tab-pane -->
          <!-- Stats tab content -->
          <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div><!-- /.tab-pane -->
          <!-- Settings tab content -->
          <div class="tab-pane" id="control-sidebar-settings-tab">
            <form method="post">
              <h3 class="control-sidebar-heading">General Settings</h3>
              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Report panel usage
                  <input type="checkbox" class="pull-right" checked>
                </label>
                <p>
                  Some information about this general settings option
                </p>
              </div><!-- /.form-group -->

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Allow mail redirect
                  <input type="checkbox" class="pull-right" checked>
                </label>
                <p>
                  Other sets of options are available
                </p>
              </div><!-- /.form-group -->

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Expose author name in posts
                  <input type="checkbox" class="pull-right" checked>
                </label>
                <p>
                  Allow the user to show his name in blog posts
                </p>
              </div><!-- /.form-group -->

              <h3 class="control-sidebar-heading">Chat Settings</h3>

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Show me as online
                  <input type="checkbox" class="pull-right" checked>
                </label>
              </div><!-- /.form-group -->

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Turn off notifications
                  <input type="checkbox" class="pull-right">
                </label>
              </div><!-- /.form-group -->

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Delete chat history
                  <a href="javascript::;" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                </label>
              </div><!-- /.form-group -->
            </form>
          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
      */
      ?>
    </div><!-- ./wrapper -->


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

	<script type="text/javascript">

	$( document ).ready(function() {

		$('.multichk').on('click', function() {

		var count = $('.multichk:checked').size();
		if(count > 1){

		$('.multidel').show();

		}else{
		$('.multidel').hide();
		}
		});
	});
	</script>



	<?php if(isset($view)){ ?>

	 <!-- jQuery 2.1.4 -->
    <!--<script src="<?php echo base_url();?>plugins/jQuery/jQuery-2.1.4.min.js"></script>-->
	<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->







	<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>-->
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url();?>bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url();?>plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>plugins/datatables/dataTables.bootstrap.min.js"></script>

    <!-- SlimScroll -->
    <script src="<?php echo base_url();?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url();?>dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url();?>dist/js/demo.js"></script>
    <!-- page script -->
    <script type="text/javascript">
      $(function () {
        $("#example1").DataTable({
          "iDisplayLength": 100
        });
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>

	<?php }else{ ?>


	<!-- jQuery 2.1.4 -->
   <!-- <script src="<?php echo base_url(); ?>plugins/jQuery/jQuery-2.1.4.min.js"></script>-->
  <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->

   <script type="text/javascript">


	$( document ).ready(function() {

	$('#coursetype').on('change', function() {

	  if(this.value==2){
	   $('#parent_level').fadeIn();


			<?php if($this->uri->segment('4')){?>
				var dataString = 'typeid='+2+ '&id=' + <?php echo $this->uri->segment('4');?>;
			<?php }else{ ?>
				var dataString = 'typeid='+2;
			<?php } ?>

			$.ajax({

			type: "POST",

			url: "http://inventifweb.net/UNI/admin/course/course/getAllParentCourses",

			data: dataString,

			cache: false,

			success: function(result){
			 $('#parent_course').html(result);
			}

          });
	  }else{
	   $('#parent_course').html('');
	   $('#parent_level').hide();

	  }
	});

    });
	</script>




  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>-->
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>

	
    <!-- InputMask -->
    <script src="<?php echo base_url(); ?>plugins/input-mask/jquery.inputmask.js"></script>
    <script src="<?php echo base_url(); ?>plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="<?php echo base_url(); ?>plugins/input-mask/jquery.inputmask.extensions.js"></script>

    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/morris/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php echo base_url(); ?>plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="<?php echo base_url(); ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?php echo base_url(); ?>plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="<?=base_url()?>/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php echo base_url(); ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>dist/js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="<?php echo base_url(); ?>dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url(); ?>dist/js/demo.js"></script>


	<?php
} ?>
	<script src="<?php echo base_url();?>assets/js/jquery.plugin.js"></script>

	<script type="text/javascript">

	$( document ).ready(function() {

	    $( ".del" ).click(function(e) {

		var del = confirm("Do you really want to delete this ?");
		if(del==true){
		var url = "<?php echo base_url(); echo $this->uri->segment('1').'/'; echo $this->uri->segment('2').'/'; ?>delete/"+this.id;
		window.location.href=url;
		}

		 });

		 $( ".delTrash" ).click(function(e) {

		var del = confirm("Do you really want to delete this ?");
		if(del==true){
		var url = "<?php echo base_url(); echo $this->uri->segment('1').'/'; echo $this->uri->segment('2').'/'; ?>deletetrash/"+this.id;
		window.location.href=url;
		}

		 });



     });
     var visitors = new Morris.Area({
       element: 'visitors-chart',
       resize: true,
       data: <?=isset($visitors) ? $visitors : 0?>,
       xkey: 'y',
       ykeys: ['item1'],
       labels: ['Visitors'],
       lineColors: ['#a0d0e0'],
       hideHover: 'auto'
     });

     var registerations = new Morris.Area({
       element: 'registerations-chart',
       resize: true,
       data: <?=isset($registerations) ? $registerations : 0?>,
       xkey: 'y',
       ykeys: ['item1'],
       labels: ['Registerations'],
       lineColors: ['#a0d0e0'],
       hideHover: 'auto'
     });

     var universities = new Morris.Area({
       element: 'universities-chart',
       resize: true,
       data: <?=isset($universities) ? $universities : 0?>,
       xkey: 'y',
       ykeys: ['item1'],
       labels: ['Universities'],
       lineColors: ['#a0d0e0'],
       hideHover: 'auto'
     });

     var sessions = new Morris.Area({
       element: 'sessions-chart',
       resize: true,
       data: <?=isset($sessions) ? $sessions : 0?>,
       xkey: 'y',
       ykeys: ['item1'],
       labels: ['Sessions'],
       lineColors: ['#a0d0e0'],
       hideHover: 'auto'
     });

	</script>
	<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->



<script type="text/javascript">
//create three initial fields
var startingNo = 1;
var $node = "";
var displayCount = 1;
for(varCount=1;varCount<=startingNo;varCount++){
    //var displayCount = varCount+1;
	displayCount = displayCount+1;
	$node += '<div class="form-group"><label for="inputAddress'+varCount+'" class="col-sm-2 control-label">Address '+varCount+'</label><div class="col-sm-10"><input type="name'+varCount+'" class="form-control" id="address'+varCount+'" name="address[]" placeholder="Address"></div></div><div class="form-group"><label for="inputCountry'+varCount+'" class="col-sm-2 control-label">Country</label><div class="col-sm-10"><select class="form-control" name="country[]" id="country'+varCount+'"><?php foreach($countries as $country){?><option value="<?php echo $country->id; ?>"><?php echo $country->name; ?></option><?php } ?></select></div></div>';
}
//add them to the DOM
//$('form').prepend($node);
//remove a textfield
$('form').on('click', '.removeVar', function(){
   $(this).parent().remove();

});
//add a new node
$('#addVar').on('click', function(){

$node = '<div><div class="form-group"><label for="inputAddress'+varCount+'" class="col-sm-2 control-label">Address '+varCount+'</label><div class="col-sm-10"><input type="name'+varCount+'" class="form-control" id="address'+varCount+'" name="address[]" placeholder="Address"></div></div><div class="form-group"><label for="inputCountry'+varCount+'" class="col-sm-2 control-label">Country</label><div class="col-sm-10"><select class="form-control" name="country[]" id="country'+varCount+'"><?php foreach($countries as $country){?><option value="<?php echo $country->id; ?>"><?php echo $country->name; ?></option><?php } ?></select></div></div><div class="removeVar col-sm-10" style="margin-left: 87%;margin-bottom: 2%;"><button class="btn btn-block btn-danger" title="Remove Address" style="width: 9%;"><span class="glyphicon glyphicon-minus"></span></button></div></div>';
varCount++;
$(this).parent().before($node);
//$(this).before($node);
});
</script>








	<!--<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>-->
	<script src="<?php echo base_url(); ?>assets/js/jquery.mousewheel.min.js"></script>
   	<script src="<?php echo base_url(); ?>croppic.min.js?v=1.2"></script>
    <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
    <script>
		var croppicHeaderOptions = {
				//uploadUrl:'img_save_to_file.php',
				cropData:{
					"dummyData":1,
					"dummyData2":"asdas"
				},
				cropUrl:'<?php echo base_url(); ?>img_crop_to_file.php',
				customUploadButtonId:'cropContainerHeaderButton',
				modal:false,
				processInline:true,
				loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
				onBeforeImgUpload: function(){ console.log('onBeforeImgUpload') },
				onAfterImgUpload: function(){ console.log('onAfterImgUpload') },
				onImgDrag: function(){ console.log('onImgDrag') },
				onImgZoom: function(){ console.log('onImgZoom') },
				onBeforeImgCrop: function(){ console.log('onBeforeImgCrop') },
				onAfterImgCrop:function(){ console.log('onAfterImgCrop') },
				onReset:function(){ console.log('onReset') },
				onError:function(errormessage){ console.log('onError:'+errormessage) }
		}
		var croppic = new Croppic('croppic', croppicHeaderOptions);


		var croppicContainerModalOptions = {
				uploadUrl:'<?php echo base_url(); ?>img_save_to_file.php',
				cropUrl:'<?php echo base_url(); ?>img_crop_to_file.php',
				modal:true,
				imgEyecandyOpacity:0.4,
				loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
				onBeforeImgUpload: function(){ console.log('onBeforeImgUpload') },
				onAfterImgUpload: function(){ console.log('onAfterImgUpload') },
				onImgDrag: function(){ console.log('onImgDrag') },
				onImgZoom: function(){ console.log('onImgZoom') },
				onBeforeImgCrop: function(){ console.log('onBeforeImgCrop') },
				onAfterImgCrop:function(){ $('#logopath').val($('.croppedImg').attr('src')); console.log('onAfterImgCrop') },
				onReset:function(){ console.log('onReset') },
				onError:function(errormessage){ console.log('onError:'+errormessage) }
		}
		var cropContainerModal = new Croppic('cropContainerModal', croppicContainerModalOptions);


		var croppicContaineroutputOptions = {
				uploadUrl:'<?php echo base_url(); ?>img_save_to_file.php',
				cropUrl:'<?php echo base_url(); ?>img_crop_to_file.php',
				outputUrlId:'cropOutput',
				modal:false,
				loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
				onBeforeImgUpload: function(){ console.log('onBeforeImgUpload') },
				onAfterImgUpload: function(){ console.log('onAfterImgUpload') },
				onImgDrag: function(){ console.log('onImgDrag') },
				onImgZoom: function(){ console.log('onImgZoom') },
				onBeforeImgCrop: function(){ console.log('onBeforeImgCrop') },
				onAfterImgCrop:function(){ console.log('onAfterImgCrop') },
				onReset:function(){ console.log('onReset') },
				onError:function(errormessage){ console.log('onError:'+errormessage) }
		}

		var cropContaineroutput = new Croppic('cropContaineroutput', croppicContaineroutputOptions);

		var croppicContainerEyecandyOptions = {
				uploadUrl:'<?php echo base_url(); ?>img_save_to_file.php',
				cropUrl:'<?php echo base_url(); ?>img_crop_to_file.php',
				imgEyecandy:false,
				loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
				onBeforeImgUpload: function(){ console.log('onBeforeImgUpload') },
				onAfterImgUpload: function(){ console.log('onAfterImgUpload') },
				onImgDrag: function(){ console.log('onImgDrag') },
				onImgZoom: function(){ console.log('onImgZoom') },
				onBeforeImgCrop: function(){ console.log('onBeforeImgCrop') },
				onAfterImgCrop:function(){ console.log('onAfterImgCrop') },
				onReset:function(){ console.log('onReset') },
				onError:function(errormessage){ console.log('onError:'+errormessage) }
		}

		var cropContainerEyecandy = new Croppic('cropContainerEyecandy', croppicContainerEyecandyOptions);


		var croppicContaineroutputMinimal = {
				uploadUrl:'<?php echo base_url(); ?>img_save_to_file.php',
				cropUrl:'<?php echo base_url(); ?>img_crop_to_file.php',
				modal:false,
				doubleZoomControls:false,
			    rotateControls: false,
				loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
				onBeforeImgUpload: function(){ console.log('onBeforeImgUpload') },
				onAfterImgUpload: function(){ console.log('onAfterImgUpload') },
				onImgDrag: function(){ console.log('onImgDrag') },
				onImgZoom: function(){ console.log('onImgZoom') },
				onBeforeImgCrop: function(){ console.log('onBeforeImgCrop') },
				onAfterImgCrop:function(){ console.log('onAfterImgCrop') },
				onReset:function(){ console.log('onReset') },
				onError:function(errormessage){ console.log('onError:'+errormessage) }
		}
		var cropContaineroutput = new Croppic('cropContainerMinimal', croppicContaineroutputMinimal);

		var croppicContainerPreloadOptions = {
				uploadUrl:'<?php echo base_url(); ?>img_save_to_file.php',
				cropUrl:'<?php echo base_url(); ?>img_crop_to_file.php',
				loadPicture:'<?php echo base_url(); ?>assets/img/night.jpg',
				enableMousescroll:true,
				loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
				onBeforeImgUpload: function(){ console.log('onBeforeImgUpload') },
				onAfterImgUpload: function(){ console.log('onAfterImgUpload') },
				onImgDrag: function(){ console.log('onImgDrag') },
				onImgZoom: function(){ console.log('onImgZoom') },
				onBeforeImgCrop: function(){ console.log('onBeforeImgCrop') },
				onAfterImgCrop:function(){ console.log('onAfterImgCrop') },
				onReset:function(){ console.log('onReset') },
				onError:function(errormessage){ console.log('onError:'+errormessage) }
		}
		var cropContainerPreload = new Croppic('cropContainerPreload', croppicContainerPreloadOptions);

        function showUniversities(countryId){
            document.getElementById("loader").style.display = 'block';
            $(".wrapper").css("opacity", "0.5");
            $.ajax({
                type: "GET",
                url: "/admin/university/internship/universities?cid=" + countryId,
                success: function(result){
                    $('#universities_list').html(result);
                    document.getElementById("loader").style.display = 'none';
                    $(".wrapper").css("opacity", "1");
                }
            });
        }

        function showColleges(universityId){
            document.getElementById("loader").style.display = 'block';
            $(".wrapper").css("opacity", "0.5");
            $.ajax({
                type: "GET",
                url: "/admin/university/internship/colleges?uid=" + universityId,
                success: function(result){
                    $('#colleges_list').html(result);
                    document.getElementById("loader").style.display = 'none';
                    $(".wrapper").css("opacity", "1");
                }
            });
        }

        function showDepartments(collegeId){
            document.getElementById("loader").style.display = 'block';
            $(".wrapper").css("opacity", "0.5");
            $.ajax({
                type: "GET",
                url: "/admin/university/internship/departments?cid=" + collegeId,
                success: function(result){
                    $('#departments_list').html(result);
                    document.getElementById("loader").style.display = 'none';
                    $(".wrapper").css("opacity", "1");
                }
            });
        }

        var questionCount = <?php echo $question_count; ?>;
        var professorCount = <?php echo $professor_count; ?>;
        var otherFeeCount = <?php echo $other_fee_count; ?>;

        function addQuestion(){
            questionCount++;
            var html = '<div class="form-group"><label for="other_fees" class="col-sm-4 control-label">Question ' + questionCount + '</label><div class="col-sm-8"><input type="text" class="form-control" name="questions[]"></div></div>';
            $("#extra-question").before(html);
        }

        function addProfessor(){
            professorCount++;
            var html = '<div class="form-group"><label for="professor_guide_name" class="col-sm-4 control-label">Professor/Guide Name-' + professorCount + '</label><div class="col-sm-8"><input type="text" class="form-control" name="professor_guide_name[]" placeholder="Professor / Guide Name"></div></div><div class="form-group"><label for="professor_guide_link" class="col-sm-4 control-label">Professor/Guide Profile Link-' + professorCount + '</label><div class="col-sm-8"><input type="text" class="form-control" name="professor_guide_link[]" placeholder="Professor / Guide Link"></div></div><div class="form-group"><label for="professor_guide_photo" class="col-sm-4 control-label">Professor/Guide Photo-' + professorCount + '</label><div class="col-sm-8"><input class="col-md-6" type="file" name="photo[]"></div></div>';
            $("#professors-guides").before(html);
        }

        function addOtherFee(){
            otherFeeCount++;
            var html = '<div class="form-group"><label for="other_fees" class="col-sm-4 control-label">Other Incidental Fees-' + otherFeeCount + '</label><div class="col-sm-8"><input type="text" class="form-control" name="other_fees[]" placeholder="Other Incidental Fees"></div></div>';
            $("#other-fees").before(html);
        }

	</script>

    <script>
    $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd',  minDate:new Date() });
    $( "#datepicker_1" ).datepicker({ dateFormat: 'yy-mm-dd',  minDate:new Date() });
    $( "#datepicker_2" ).datepicker({ dateFormat: 'yy-mm-dd',  minDate:new Date() });
    $( "#datepicker_3" ).datepicker({ dateFormat: 'yy-mm-dd',  minDate:new Date() });
    //$( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd',  minDate:new Date() });
    $('body').on('focus',".datepicker", function(){
        $(this).datepicker({ dateFormat: 'yy-mm-dd',  minDate:new Date() });
    });
    $( "#datepicker-prev" ).datepicker({ dateFormat: 'yy-mm-dd',  maxDate:new Date() });
    </script>



  </body>
  <div id="loader"></div>
