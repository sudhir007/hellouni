     <style type="text/css">.tt{ height:0px !important;}</style>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $this->session->userdata('user')['typename'];?>
            <small>Manage <?php echo $this->session->userdata('user')['typename'];?> Profile</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#"><?php echo $this->session->userdata('user')['typename'];?> Management</a></li>
            <li class="active">Update Profile</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
           
            <!-- right column -->
            <div class="col-md-10">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Update Profile</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
				
				<?php //$userdata = $this->session->userdata;?>
                <form class="form-horizontal" action="<?php echo base_url();?>common/profile/updateProfile/<?php //echo $this->uri->segment(4); ?>" method="post" name="changepassword" enctype="multipart/form-data">
         
				
				
                
                  <div class="box-body">

                  <div class="form-group">
                    <div class="col-sm-10" style="color:green;">
                      <?php if($this->session->flashdata('flash_message')){
                               echo $this->session->flashdata('flash_message');
                       } ?>
                    </div>
                  </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php if(isset($name)) echo $name; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="<?php if(isset($username)) echo $username; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php if(isset($email)) echo $email; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Mobile</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile" maxlength="10" value="<?php if(isset($mobile)) echo $mobile; ?>">
                      </div>
                    </div>

                    
					
					
					</div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
					<button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                    <button type="submit" class="btn btn-info pull-right">Update</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              <!-- general form elements disabled -->
             
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     