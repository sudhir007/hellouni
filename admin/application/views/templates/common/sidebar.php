<!-- Left side column. contains the logo and sidebar -->

<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->

    <section class="sidebar">

        <!-- Sidebar user panel -->

        <div class="user-panel">

            <div class="pull-left">

                <img src="<?= $this->session->userdata('user')['image'] ? base_url() . '..' . $this->session->userdata('user')['image'] : base_url() . 'dist/img/avatar.png' ?>" width="45px" height="45px"  alt="User Image">

            </div>

            <div class="pull-left info">

                <p><?php echo $this->session->userdata('user')['name'];?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $this->session->userdata('user')['typename'];?></a>

            </div>

        </div>

        <ul class="sidebar-menu">
<?php
if($this->session->userdata('user')['type'] != 11)
{
    ?>
    <li class="header">MAIN NAVIGATION</li>

    <?php
    if($this->session->userdata('user')['type'] != 3)
    {
    ?>
    <li class="treeview <?php if($this->uri->segment(2)=='dashboard') echo " active"; ?>"><a href="/admin/common/dashboard">Dashboard</a></li>
  <?php } ?>

    <li class="treeview <?php if($this->uri->segment(2)=='profile') echo " active"; ?>">

        <a href="#">

            <span class="glyphicon glyphicon-user"></span>
            <span>Manage Profile</span>
            <i class="fa fa-angle-left pull-right"></i>

        </a>

        <ul class="treeview-menu">

            <?php
            if($this->session->userdata('user')['type']=='3')
            {
                ?>

                <li class="treeview <?php if($this->uri->segment(2)=='university') echo " active"; ?>"><a href="<?php echo base_url(); ?>profile/university"><i class="fa fa-user"></i> My Profile</a></li>

                <?php
            }
            else if($this->session->userdata('user')['type']=='4')
            {
                ?>

                <li class="treeview <?php if($this->uri->segment(2)=='representative') echo " active"; ?>"><a href="<?php echo base_url(); ?>profile/representative"><i class="fa fa-user"></i> My Profile</a></li>

                <?php
            }
            else if($this->session->userdata('user')['type']=='6')
            {
                ?>
                <li class="treeview <?php if($this->uri->segment(2)=='alumnus') echo " active"; ?>"><a href="<?php echo base_url(); ?>user/alumnus/profile"><i class="fa fa-user"></i> My Profile</a></li>
                <?php
            }
            ?>

            <li class="treeview <?php if($this->uri->segment(3)=='updateProfile') echo " active"; ?>"><a href="/admin/common/profile/updateProfile/"><i class="fa fa-user"></i> Update Profile </a></li>
            <li class="treeview <?php if($this->uri->segment(3)=='changepassword') echo " active"; ?>"><a href="/admin/common/profile/changepassword/"><i class="fa fa-user"></i> Change Password</a></li>

        </ul>

    </li>
    <?php
}


            if($this->session->userdata('user')['type']==1 || $this->session->userdata('user')['type']==2 || $this->session->userdata('user')['type']==3)
            {
                ?>

                <li class="treeview <?php if($this->uri->segment(1)=='team' && $this->uri->segment(2)=='register') echo " active"; ?>">
                    <a href="#">
                        <i class="fa fa-user"></i>
                        <span>Team Module</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url(); ?>team/register"><i class="fa fa-user-plus"></i> Create New Team </a></li>
                        <li><a href="<?php echo base_url(); ?>team/register/allList"><i class="fa fa-user-plus"></i> Team List </a></li>
                    </ul>
                </li>
                
                <li class="treeview <?php if($this->uri->segment(2)=='alumnus' || $this->uri->segment(2)=='representative') echo " active"; ?>">
                    <a href="#">
                        <i class="fa fa-user"></i>
                        <span>User Managment</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php
                        if($this->session->userdata('user')['type']==1)
                        {
                            ?>
                            <li class="active"><a href="<?php echo base_url(); ?>user/admin/"><i class="fa fa-user-plus"></i> Admin</a></li>
                            <?php
                        }
                        if($this->session->userdata('user')['type']==1 || $this->session->userdata('user')['type']==2 || $this->session->userdata('user')['type']==3)
                        {
                            ?>
                            <li><a href="<?php echo base_url(); ?>user/representative/"><i class="fa fa-user-plus"></i> Representative</a></li>
                            <li><a href="<?php echo base_url(); ?>user/alumnus/"><i class="fa fa-user-plus"></i> Alumnus</a></li>
                            <?php
                        }
                        if($this->session->userdata('user')['type']==1 || $this->session->userdata('user')['type']==2 )
                        {
                            ?>
                            <li><a href="<?php echo base_url(); ?>user/counselor/"><i class="fa fa-user-plus"></i> Counselor</a></li>
                            <?php
                        }
                        ?>
                    </ul>
                </li>
                <?php
            }
            if($this->session->userdata('user')['type']==1 || $this->session->userdata('user')['type']==2)
            {
                ?>
                <li class="treeview <?php if($this->uri->segment(1)=='location') echo " active"; ?>">
                    <a href="#">
                        <i class="fa fa-flag"></i>
                        <span>Country Managment</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url(); ?>location/country/"><i class="fa fa-flag-o"></i> Country</a></li>
                    </ul>
                </li>
                <?php
            }
            if($this->session->userdata('user')['type']==1 || $this->session->userdata('user')['type']==2)
            {
              ?>

              <!--<li class="treeview <?php if($this->uri->segment(1)=='webinar') echo " active"; ?>">
                  <a href="#">
                      <i class="fa fa-flag"></i>
                      <span>Webinar Managment</span>
                      <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                      <li><a href="<?php echo base_url(); ?>webinar/webinarmaster/list/"><i class="fa fa-flag-o"></i> Webinar List</a></li>
                      <li><a href="<?php echo base_url(); ?>webinar/webinarmaster/form/"><i class="fa fa-flag-o"></i> Webinar Create Form</a></li>
                      <li><a href="<?php echo base_url(); ?>webinar/webinarmaster/studentlist/"><i class="fa fa-flag-o"></i> Student List</a></li>
                      <li><a href="<?php echo base_url(); ?>webinar/webinarmaster/polls/"><i class="fa fa-flag-o"></i> Polls</a></li>
                  </ul>
              </li>-->

            <?php
          }
            if($this->session->userdata('user')['type']==1 || $this->session->userdata('user')['type']==2 || $this->session->userdata('user')['type']==3 || $this->session->userdata('user')['type']==7)
            {
                ?>
                <!-- <li class="treeview <?php if($this->uri->segment(1)=='student') echo " active"; ?>">
                    <a href="#">
                        <i class="fa fa-user"></i>
                        <span>Student Managment</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php
                        if($this->session->userdata('user')['type']!=7)
                        {
                            ?>
                            <li class="active"><a href="<?php echo base_url(); ?>student/index/all"><i class="fa fa-user-plus"></i> All</a></li>
                            <?php
                        }
                        if($this->session->userdata('user')['type']==7)
                        {
                            ?>
                            <li class="active"><a href="<?php echo base_url(); ?>student/index/counselor"><i class="fa fa-user-plus"></i> All</a></li>
                            <li class="active"><a href="<?php echo base_url(); ?>student/session/universities"><i class="fa fa-user-plus"></i> Book Session</a></li>
                            <?php
                        }
                        if($this->session->userdata('user')['type']==1 || $this->session->userdata('user')['type']==2)
                        {
                            ?>
                            <li class="active"><a href="<?php echo base_url(); ?>student/index/applied"><i class="fa fa-user-plus"></i> Applied</a></li>
                            <?php
                        }
                        ?>
                        <li class="active"><a href="<?php echo base_url(); ?>student/index/add"><i class="fa fa-user-plus"></i> Add Student</a></li>
                        <li class="active"><a href="<?php echo base_url(); ?>university/internship/requests"><i class="fa fa-user-plus"></i> Internship Requests</a></li>
                        <li class="active"><a href="<?php echo base_url(); ?>webinar/webinarmaster/fairstudentlist"><i class="fa fa-user-plus"></i> Fair Student List</a></li>
                    </ul>
                </li>-->
                <li class="treeview <?php if($this->uri->segment(1)=='user') echo " active"; ?>">
                    <a href="#">
                        <i class="fa fa-user"></i> <span>University Management</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php
                        if($this->session->userdata('user')['type']==1 || $this->session->userdata('user')['type']==2 || $this->session->userdata('user')['type']==3)
                        {
                            if($this->session->userdata('user')['type']==1 || $this->session->userdata('user')['type']==2 || $this->session->userdata('user')['type']==3)
                            {
                                ?>
                                <li class="active"><a href="<?php echo base_url(); ?>user/counselor/join_councelling"><i class="fa fa-user-plus"></i> Join Councelling</a></li>
                                <li class="active"><a href="<?php echo base_url(); ?>user/university/"><i class="fa fa-user-plus"></i> Universities</a></li>
                                <?php
                            }
                            if($this->session->userdata('user')['type']==1 || $this->session->userdata('user')['type']==2 || $this->session->userdata('user')['type']==3)
                            {
                                ?>
                                <li><a href="<?php echo base_url(); ?>user/campus/"><i class="fa fa-user-plus"></i> Campuses</a></li>
                                <li><a href="<?php echo base_url(); ?>user/college/"><i class="fa fa-user-plus"></i> Colleges</a></li>
                                <li><a href="<?php echo base_url(); ?>user/department/"><i class="fa fa-user-plus"></i> Departments</a></li>
                                <li><a href="<?php echo base_url(); ?>user/course/"><i class="fa fa-user-plus"></i> Courses</a></li>
                                <li><a href="<?php echo base_url(); ?>user/research/"><i class="fa fa-user-plus"></i> Researches</a></li>
                                <?php
                                if($this->session->userdata('user')['type']==1 || $this->session->userdata('user')['type']==2)
                                {
                                    ?>
                                    <li><a href="<?php echo base_url(); ?>user/upload/university/"><i class="fa fa-user-plus"></i> Upload Universities</a></li>
                                    <li><a href="<?php echo base_url(); ?>user/upload/research/"><i class="fa fa-user-plus"></i> Upload Researches</a></li>
                                    <?php
                                }
                            }
                            if($this->session->userdata('user')['type']==1 || $this->session->userdata('user')['type']==2)
                            {
                                ?>
                                <li><a href="<?php echo base_url(); ?>user/university/logo"><i class="fa fa-user-plus"></i> Logo Requests</a></li>
                                <li><a href="<?php echo base_url(); ?>user/university/slider"><i class="fa fa-user-plus"></i> Slider Requests</a></li>
                                <?php
                            }
                            if($this->session->userdata('user')['type']==3)
                            {
                                ?>
                                <li class="active"><a href="<?php echo base_url(); ?>user/brochure"><i class="fa fa-user-plus"></i> Brochures</a></li>
                                <?php
                            }
                        }
                        ?>
                        <li class="active"><a href="<?php echo base_url(); ?>university/internship"><i class="fa fa-user-plus"></i> Internship</a></li>
                        <?php
                        if($this->session->userdata('user')['type']==1 || $this->session->userdata('user')['type']==2)
                        {
                            ?>
                            <li class="active"><a href="<?php echo base_url(); ?>university/application/list"><i class="fa fa-user-plus"></i> Applications</a></li>
                            <li class="active"><a href="<?php echo base_url(); ?>university/visa/list"><i class="fa fa-user-plus"></i> VISA Applications</a></li>
                            <?php
                        }
                        if($this->session->userdata('user')['type']==3)
                        {
                            ?>
                            <li class="active"><a href="<?php echo base_url(); ?>university/application"><i class="fa fa-user-plus"></i> Applications</a></li>
                            <li class="active"><a href="<?php echo base_url(); ?>university/visa"><i class="fa fa-user-plus"></i> VISA Applications</a></li>
                            <?php
                        }
                        ?>
                    </ul>
                </li>



                <!-- <li class="treeview <?php if($this->uri->segment(1)=='university') echo " active"; ?>">
                    <a href="#">
                        <i class="fa fa-user"></i> <span>University Field Select</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">

                                <li><a href="<?php echo base_url(); ?>university/fileds"><i class="fa fa-user-plus"></i> University Fields</a></li>
                                <li><a href="<?php echo base_url(); ?>university/process/info"><i class="fa fa-user-plus"></i> University Process Info</a></li>

                    </ul>
                </li>-->
                <?php
                if($this->session->userdata('user')['type'] == 1 || $this->session->userdata('user')['type']==3 || $this->session->userdata('user')['type']==7)
                {
                    ?>
                    <li class="treeview <?php if($this->uri->segment(2)=='session') echo " active"; ?>">
                        <a href="#">
                            <i class="fa fa-user"></i>
                            <span>Session Management</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url(); ?>user/session/"><i class="fa fa-user-plus"></i> All Sessions</a></li>
                            <li><a href="<?php echo base_url(); ?>user/session/coming"><i class="fa fa-user-plus"></i> Coming Sessions</a></li>
                            <li><a href="<?php echo base_url(); ?>user/session/unassigned"><i class="fa fa-user-plus"></i> Unassigned Sessions</a></li>
                            <li><a href="<?php echo base_url(); ?>user/session/unconfirmed"><i class="fa fa-user-plus"></i> Unconfirmed Sessions</a></li>
                            <li><a href="<?php echo base_url(); ?>user/session/completed"><i class="fa fa-user-plus"></i> Completed Sessions</a></li>
                            <li><a href="<?php echo base_url(); ?>user/session/predefineslots"><i class="fa fa-user-plus"></i> PreDefine Slots University</a></li>
                        </ul>
                    </li>
                    <li class="treeview <?php if($this->uri->segment(2)=='internship') echo " active"; ?>">
                        <a href="#">
                            <i class="fa fa-user"></i>
                            <span>Internship Management</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url(); ?>university/internship/session/all"><i class="fa fa-user-plus"></i> All Sessions</a></li>
                            <li><a href="<?php echo base_url(); ?>university/internship/session/coming"><i class="fa fa-user-plus"></i> Coming Sessions</a></li>
                            <li><a href="<?php echo base_url(); ?>university/internship/session/unassigned"><i class="fa fa-user-plus"></i> Unassigned Sessions</a></li>
                            <li><a href="<?php echo base_url(); ?>university/internship/session/unconfirmed"><i class="fa fa-user-plus"></i> Unconfirmed Sessions</a></li>
                            <li><a href="<?php echo base_url(); ?>university/internship/session/completed"><i class="fa fa-user-plus"></i> Completed Sessions</a></li>
                        </ul>
                    </li>
                    <?php
                }


            }
            if($this->session->userdata('user')['type'] == 1 || $this->session->userdata('user')['type'] == 30)
            {
                ?>
                <!-- <li class="treeview <?php if($this->uri->segment(2)=='virtualfair') echo " active"; ?>">
                    <a href="#">
                        <i class="fa fa-flag"></i>
                        <span>Virtual Fair</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php
                        if($this->session->userdata('user')['type'] == 30)
                        {
                            ?>
                            <li><a href="/admin/university/virtualfair">All Fairs</a></li>
                            <?php
                        }
                        if($this->session->userdata('user')['type'] == 1)
                        {
                            ?>
                            <li><a href="/admin/university/virtualfair/help">Help</a></li>
                            <li><a href="/admin/university/virtualfair/all_participants">All Participants</a></li>
                            <?php
                        }
                        ?>
                    </ul>
                </li>

                <li class="treeview <?php if($this->uri->segment(1)=='meeting') echo " active"; ?>">
                    <a href="#">
                        <i class="fa fa-flag"></i>
                        <span>Visa Seminar</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php
                        if($this->session->userdata('user')['type'] == 1)
                        {
                            ?>
                            <li><a href="/admin/meeting/visaseminar">All Rooms</a></li>
                            <?php
                        }
                        ?>
                    </ul>
                </li>-->
                <?php
            }
            ?>

            <li class="treeview <?php if($this->uri->segment(1)=='student' && $this->uri->segment(2)=='application') echo " active"; ?>">
                  <a href="#">
                      <i class="fa fa-user"></i>
                      <span>Student Application</span>
                      <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">


                      <?php if( $this->session->userdata('user')['type'] != 3 ) { ?>
                        <li><a href="<?php echo base_url(); ?>student/list/all"><i class="fa fa-user-plus"></i> All Student List</a></li>
                        <li><a href="<?php echo base_url(); ?>student/list/my"><i class="fa fa-user-plus"></i> My Student List</a></li>
                        <li><a href="<?php echo base_url(); ?>student/list/unassigned"><i class="fa fa-user-plus"></i> Unassigned Student List</a></li>

                         <!-- <li><a href="<?php echo base_url(); ?>student/application/allList"><i class="fa fa-user-plus"></i> All Application List</a></li>
                        <li><a href="<?php echo base_url(); ?>counsellor/create/application"><i class="fa fa-user-plus"></i> Create New Application </a></li>
                        <li><a href="<?php echo base_url('university/textsearch?searchtextvalue=usa+university&afl=yes'); ?>"><i class="fa fa-user-plus"></i> University Filter </a></li> -->
                      <?php } ?>
                      <!-- <li><a href="<?php echo base_url(); ?>student/application/myList/new"><i class="fa fa-user-plus"></i> My Application List</a></li> -->
                      <?php if( $this->session->userdata('user')['type'] != 3 ) { ?>
                      <!-- <li><a href="<?php echo base_url(); ?>student/application/submitList"><i class="fa fa-user-plus"></i> Submitted Application </a></li> -->
                      <li><a href="<?php echo base_url(); ?>student/application/newList"><i class="fa fa-user-plus"></i> New Application</a></li>
                    <?php } ?>
                  </ul>
              </li>

              <?php if( $this->session->userdata('user')['type'] != 3 ) { ?>
              <li class="treeview <?php if($this->uri->segment(1)=='callingdata') echo " active"; ?>">
                  <a href="#">
                      <i class="fa fa-user"></i> <span>Calling Data</span> <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">

                              <li><a href="<?php echo base_url(); ?>callingdata/list"><i class="fa fa-user-plus"></i> Calling List</a></li>
                              <li><a href="<?php echo base_url(); ?>callingdata/source/list/"><i class="fa fa-user-plus"></i> Source List</a></li>
                              <li><a href="<?php echo base_url(); ?>callingdata/bulk/uploadform/"><i class="fa fa-user-plus"></i> Upload Calling List</a></li>


                  </ul>
              </li>
            <?php } ?>

            <?php if( $this->session->userdata('user')['type'] != 3 ) { ?>
            <li class="treeview <?php if($this->uri->segment(1)=='Mappingdata') echo " active"; ?>">
                  <a href="#">
                      <i class="fa fa-user"></i> <span>Data Mapping</span> <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">

                              <li><a href="<?php echo base_url(); ?>mappingdata/universityList"><i class="fa fa-user-plus"></i> University Mapping</a></li>
                              <li><a href="<?php echo base_url(); ?>mappingdata/courseList"><i class="fa fa-user-plus"></i> Course Mapping</a></li>


                  </ul>
              </li>
              <?php } ?>

        </ul>

    </section>

    <!-- /.sidebar -->

</aside>
