<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Seminar Students<small>Manage Seminar Students</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="#">Seminar Student Management</a></li>
            <li class="active">Seminar Students</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>

                <div class="box">
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Sl No</th>
                                        <th>Student Name</th>
                                        <th>Email</th>
                                        <th>Room</th>
                                        <th>Timeslot</th>
                                        <th>URL</th>
                                        <th>Change Room</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1;
                                    if($students)
                                    {
                                        foreach($students as $student)
                                        {
                                            ?>
                                            <tr>
                                                <td><?php echo $i++;?></td>
                                                <td><?php echo $student['name'];?></td>
                                                <td><?php echo $student['email'];?></td>
                                                <td id="student-room-<?=$student['user_id']?>"><?php echo $student['room'];?></td>
                                                <td id="rooms-<?=$student['user_id']?>" style="display:none;">
                                                    <select name="change_room" onclick="changeRoom('<?=$student['user_id']?>', this.value)">
                                                        <?php
                                                        foreach($rooms as $id => $room)
                                                        {
                                                            ?>
                                                            <option value="<?=$id?>" <?=($id == $student['tokbox_id']) ? 'selected' : ''?>><?=$room?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </td>
                                                <td><?=$student['timeslot']?></td>
                                                <td><?=$student['url']?></td>
                                                <td id="click-<?=$student['user_id']?>"><button onclick="showRooms('<?=$student['user_id']?>')">Click</button></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Sl No</th>
                                        <th>Student Name</th>
                                        <th>Email</th>
                                        <th>Room</th>
                                        <th>Timeslot</th>
                                        <th>URL</th>
                                        <th>Change Room</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
    function changeRoom(userId, roomId){
        $.ajax({
            url: '/admin/meeting/seminar/change_room?sid=' + userId + '&rid=' + roomId,
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                alert('Room is updated successfully');
                window.location.href = '/admin/meeting/seminar/room';
            },
            error: function error() {
            }
        });
    }

    function showRooms(userId){
        document.getElementById('student-room-' + userId).style.display = 'none';
        document.getElementById('rooms-' + userId).style.display = 'block';
    }

</script>
