<style>
.modal{
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    z-index: 112;
}

.modal-content{
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 50%;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Seminar<small>Manage Seminar</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="#">Seminar Management</a></li>
            <li class="active">Seminar</li>
        </ol>
        <span style="font-size: 18px; font-weight: bold; margin-left: 20px;"><a href="/admin/event/help/<?=$encodedUsername?>">Help Page</a></span>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>

                <div class="box">
                    <form action="<?php echo base_url();?>university/virtualfair/multidelete" method="post">
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Sl No</th>
                                        <th>Room Type</th>
                                        <th>Panelists</th>
                                        <th>URL</th>
                                        <th>Attended</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1;
                                    if($rooms)
                                    {
                                        foreach($rooms as $room)
                                        {
                                            if(in_array($room['token_id'], $allowedRooms))
                                            {
                                                $base64TokenId = base64_encode($room['token_id']);
                                                $tokenId = str_replace("=", "", $base64TokenId);

                                                $url = 'https://admin.hellouni.org/event/organizer/' . $tokenId . '/' . $encodedUsername;
                                                ?>
                                                <tr>
                                                    <td><?php echo $i++;?></td>
                                                    <td><?php echo $room['type'];?></td>
                                                    <td><?php echo $room['panelists'];?></td>
                                                    <td><?php echo '<a href="javascript::void()" onClick=goToPage(\'' . $url . '\')>' . $url . '</a>'?></td>
                                                    <td><?php echo $room['attended'];?></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Sl No</th>
                                        <th>Room Type</th>
                                        <th>Counselors</th>
                                        <th>URL</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<div id="modal-join-room" class="modal">
    <!-- Modal content -->
    <div class="row modal-content">
        <div>
            <div style="font-size: 16px; text-align:center;">Students are in queue for US Bank</div>
            <button id="exit-button" class="btn btn-md btn-primary" style="float:left;" onclick="joinLater('')">Join Later</button>
            <button id="exit-button" class="btn btn-md btn-primary" style="float:right;" onclick="join('')">Join</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    function goToPage(url){
        var href = url;
        var win = window.open(href, 'visaseminar');
        win.focus();
    }

    var pageRefresh = setInterval(function() {
        window.location.href = "/admin/event/index/<?=$encodedUsername?>"
    }, 30000);
</script>
