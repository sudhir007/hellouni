<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Visa Seminar<small>Manage Visa Seminar</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="#">Visa Seminar Management</a></li>
            <li class="active">Visa Seminar</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>

                <div class="box">
                    <form action="<?php echo base_url();?>university/virtualfair/multidelete" method="post">
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Sl No</th>
                                        <th>Room Type</th>
                                        <th>Counselors</th>
                                        <th>URL</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1;
                                    if($rooms)
                                    {
                                        foreach($rooms as $room)
                                        {
                                            $base64TokenId = base64_encode($room['token_id']);
                                            $tokenId = str_replace("=", "", $base64TokenId);

                                            switch($room['type'])
                                            {
                                                case 'Landing Page':
                                                    $url = 'https://www.hellouni.org/meeting/visaseminar/rooms/' . $username;
                                                    break;
                                                case 'Common Room':
                                                    $url = 'https://www.hellouni.org/meeting/visaseminar/organizer/w/' . $tokenId . '/' . $username;
                                                    break;
                                                default:
                                                    $url = 'https://www.hellouni.org/meeting/visaseminar/organizer/o/' . $tokenId . '/' . $username;
                                                    break;
                                            }
                                            if($room['type'] == 'Landing Page')
                                            {

                                            }
                                            ?>
                                            <tr>
                                                <td><?php echo $i++;?></td>
                                                <td><?php echo $room['type'];?></td>
                                                <td><?php echo $room['counsellors'];?></td>
                                                <td><?php echo '<a href="javascript::void()" onClick=goToPage(\'' . $url . '\')>' . $url . '</a>'?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Sl No</th>
                                        <th>Room Type</th>
                                        <th>Counselors</th>
                                        <th>URL</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
    function goToPage(url){
        var href = url;
        var win = window.open(href, 'visaseminar');
        win.focus();
    }
</script>
