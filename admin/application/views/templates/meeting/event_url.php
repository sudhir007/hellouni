<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Event <small>Add Map URL</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Event Management</a></li>
            <li class="active">Add Map URL</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-10">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Map URL</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php
                    if($this->session->flashdata('flash_message'))
                    {
                        ?>
                        <div class="alert alert-error alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                            <h4><i class="icon fa fa-circle-o"></i> Error</h4>
                            <?php
                            echo $this->session->flashdata('flash_message');
                            ?>
                        </div>
                        <?php
                    }
                    if($mapped_url)
                    {
                        ?><div style="padding:10px;"><?='<b>Current Mapped Url - </b>' . $mapped_url?></div>
                        <?php
                    }
                    ?>
                    <form class="form-horizontal" action="<?php echo base_url();?>event/url" method="post" name="add_map_url" autocomplete="off">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">URL *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="url" name="url" placeholder="Mapped URL" value="" required>
                                </div>
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">Submit</button>
                        </div><!-- /.box-footer -->
                    </form>
                </div><!-- /.box -->
                <!-- general form elements disabled -->
            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
