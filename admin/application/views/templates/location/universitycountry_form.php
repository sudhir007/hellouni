

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Country
            <small>Manage Country</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Country Management</a></li>
            <li class="active">Add Country</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
           
            <!-- right column -->
            <div class="col-md-10">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Country</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
				
				 <?php if($this->uri->segment('4')){?>
				<form class="form-horizontal" action="<?php echo base_url();?>location/universitycountry/edit/<?php if(isset($id)) echo $id;?>" method="post" name="addcourse">
				<?php }else{?>
                <form class="form-horizontal" action="<?php echo base_url();?>location/universitycountry/create" method="post" name="addadmin">
                <?php } ?>
				
				
                
                  <div class="box-body">
                    
					
					
					<div class="form-group">
                  		 <label for="status" class="col-sm-2 control-label">Country</label>
						  <div class="col-sm-10">
						  <select class="form-control" name="country">
						  <?php foreach($countries as $country){ ?>
						  
						<option value="<?php echo $country->id;?>" <?php //if(isset($status) && $status=='1'){ echo 'selected="selected"'; } ?>><?php echo $country->name;?></option>
							
							
						  <?php } ?>
						  </select>
						  </div>
               		</div>
					
					    
	              
					
					
                    
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
					<button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                    <button type="submit" class="btn btn-info pull-right">
					 <?php if(!$this->uri->segment('4')){?>Add <?php }else{?>Update <?php }?>
					 
					 
					 
					 </button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              <!-- general form elements disabled -->
             
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     