
    
        <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- Statistics Button Container -->
            	
                
                <!-- Panels Start -->
                 
            	 <?php if($msg = $this->session->flashdata('flash_message')){?> 
    <div class="mws-form-message success"><?php echo $msg;?></div>
     <?php } ?>
	 
	 
	  <?php if($ermsg = $this->session->flashdata('error_message')){?> 
    <div class="mws-form-message error"><?php echo $ermsg;?></div>
     <?php } ?>
				<div class="mws-panel grid_8">
				<form action="<?php echo base_url();?>location/state/multidelete" method="post">
                	<div class="mws-panel-header">
					
                    	<span><i class="icon-table"></i> State
						<a href="<?php echo base_url();?>location/state/form"><button type="button" class="btn btn-small">Insert</button></a>
						<button type="submit" name="delete-State" id="del" class="btn btn-small">Delete</button>
						
						</span>
						
					
						
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-datatable-fn mws-table">
                            <thead>
                                <tr>
                                    <th style="width: 1px;">Select</th>
								    <th>Name</th>
                                    <th>Country</th>
                                    <th>Status</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($states as $state):?>
								<tr>
                                    <td><input type="checkbox" name="chk[]" class="city_chk" id="<?php echo $state->id;?>" value="<?php echo $state->id;?>"/></td>
									<td><?php echo $state->name;?></td>
                                    <td><?php echo $state->country_name;?></td>
									<td><?php echo $state->status_name;?></td>
									<td class=" ">
                                        <span class="btn-group">
										
										<a class="btn btn-small" href="<?php echo base_url();?>location/state/form/<?php echo $state->id;?>/view"><i class="icon-search"></i></a>
                                       <a class="btn btn-small" href="<?php echo base_url();?>location/state/form/<?php echo $state->id;?>"><i class="icon-pencil"></i></a>
                                       <a class="btn btn-small delete-row" id="<?php echo base_url(); ?>location/state/delete/<?php echo $state->id;?>" href="javascript:void(o);"><i class="icon-trash"></i></a></span>
                                    </td>
                                </tr>
                               <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
					
					</form>
                </div>

                                 

                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->
			
