
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Country
            <small>Manage Country</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Country Management</a></li>
            <li class="active">Country</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              
			  <!-- /.box -->
			  
			  <?php if($this->session->flashdata('flash_message')){ ?>
				<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                <?php echo $this->session->flashdata('flash_message'); ?>
              </div>
			  <?php } ?>

              <div class="box">
			   <form action="<?php echo base_url();?>location/country/multidelete" method="post">
                <div class="box-header">
                 <a href="<?php echo base_url();?>location/country/form"><button type="button" style="width:10%; float:left; margin-right:5px;" class="btn btn-primary"><i class="fa fa-plus"></i> Add Country</button></a>
				 <button type="submit" class="btn btn-danger multidel" style="width:11%; display:none;"><i class="fa fa-minus"></i> Delete Country</button> 
                </div><!-- /.box-header -->
				
			  
                <div class="box-body">
                  <table id="newAppExample" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Select</th>
                        <th>Name</th>
                      
						<th>Code</th>
                       
                        <th>Status</th>
						<th>Option</th>
                      </tr>
                    </thead>
                    <tbody>
					<!-- <?php 
					
						foreach($countries as $country){ ?>
                      <tr>
                        <td align="center" width="10%"><input type="checkbox" class="multichk" name="chk[]" value="<?php echo $country->id;?>"/>&nbsp;<?php //echo $count++; ?></td>
                        <td><?php echo $country->name;?></td>
                       
						<td><?php echo $country->code;?></td>
                       
                        <td><?php echo $country->status_name;?></td> 
						 <td><a href="<?php echo base_url();?>location/country/form/<?php echo $country->id;?>" title="Modify"><i class="fa fa-edit"></i></a> &nbsp; <a href="javascript:void();" title="Delete" class="del" id="<?php echo $country->id;?>"><i class="fa fa-trash-o"></i></a></td>
					<?php  }
					?>
                      </tr> -->
                      
                    </tbody>
                    <tfoot>
                      <tr>
                       <th>Select</th>
                        <th>Name</th>
                      
						<th>Code</th>
                       
                        <th>Status</th>
						<th>Option</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     
     <script>
var table;
  $(document).ready(function() {

    //datatables
    table = $('#newAppExample').DataTable({ 
//alert('hi');
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url();?>location/country/ajax_manage_page",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
                { "data": "id" },
                { "data": "name" },
                { "data": "code" },
                { "data": "status_name" },
                { "data": "action" }
            ],
        

    });

  });
  
</script>