

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            University Mapping
            <small>Manage University</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">University Mapping Management</a></li>
            <li class="active">Edit Mapping University</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
           
            <!-- right column -->
            <div class="col-md-10">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Mapping University</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
				<?php $id = $this->uri->segment('3'); ?>
				<form class="form-horizontal" action="<?php echo base_url();?>mappingdata/update/<?php if(isset($id)) echo $id;?>" method="post" name="addcourse">
				       <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label">Suggested university name</label>
                      <div class="col-sm-8">
                        <input type="name" class="form-control" id="name" name="name" placeholder="Name" value="<?php if(isset($university_name)) echo $university_name; ?>">
                      </div>
                      <div class="col-sm-1">
                        <button type="button" class="btn btn-info pull-left"><a href="<?php echo base_url();?>mappingdata/create/<?php echo $id;?>" title="Modify">Create</a></button>
                      </div>
                    </div>
					
					
					<div class="form-group">
                  		 <label for="status" class="col-sm-3 control-label">Available University</label>
						  <div class="col-sm-8">
						  <select class="form-control" name="university">
                <option>Select University</option>
                <?php foreach($universityListObj as $row){?>
							   <option value="<?=$row->id?>" <?php if(isset($mapped_id) && $mapped_id == $row->id){ echo "selected"; } ?>><?=$row->name?></option>
								<?php } ?>
						  </select>
						  </div>
               		</div>
					
					    
	              
					
					
                    
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                    <input type="hidden" name="mapId" value="<?=$id?>">
					<!-- <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button> -->
                    <button type="submit" class="btn btn-info">Submit</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              <!-- general form elements disabled -->
             
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     