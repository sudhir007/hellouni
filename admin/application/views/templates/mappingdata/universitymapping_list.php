
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>University Mapping Data<small>University Mapping List</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">University Mapping Data</a></li>
            <li class="active">University List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php
                if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>

                <div class="box">
                      <div class="box-header">
                        </div><!-- /.box-header -->
                          <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th> Id </th>
                                        <th> Suggested University Name </th>
                                        <th> Suggested By </th>
                                        <th> Status </th>
                                        <th> Action </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    foreach($university as $row)
                                    { 
                                        ?>
                                        <tr>
                                            <td><?php echo $row->id;?></td>
                                            <td><?php echo $row->university_name;?></td>
                                            <td><?php echo $row->university_name;?></td>
                                            <td><?php echo $row->status;?></td>
                                            <td> <a href="<?php echo base_url();?>mappingdata/universityEdit/<?php echo $row->id;?>" title="Modify"><i class="fa fa-edit"></i></a>
                                            </td>

                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                      <th> Id </th>
                                        <th> Suggested University Name </th>
                                        <th> Suggested By </th>
                                        <th> Status </th>
                                        <th> Action </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">

</script>
<script>
var table;
  $(document).ready(function() {

    //datatables
    table = $('#newAppExample').DataTable({ 
//alert('hi');
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url();?>callingdata/callingdata/ajax_manage_page_callingList",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
                { "data": "name" },
                { "data": "email" },
                { "data": "mobile" },
                { "data": "mobile1" },
                { "data": "lead_state" },
                { "data": "assign_to_name" },
                { "data": "source_name" },
                { "data": "date_created" },
                { "data": "next_followup_date" },
                 { "data": "action" }
            ],
        

    });

  });
  
</script>
