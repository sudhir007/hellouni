
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Student Application<small>New Application List</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Student Application</a></li>
            <li class="active">New Application List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php
                if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>

                <div class="box">
                      <div class="box-header">
                        <a href="/admin/counsellor/create/application"><button type="button" style="width:15%; float:left; margin-right:5px;" class="btn btn-block btn-secondary"><i class="fa fa-plus"></i> Create New Application </button></a>
                        </div><!-- /.box-header -->
                          <div class="box-body">
                            <div class="table-responsive">
                            <table id="newAppExample" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th> Application id </th>
                                        <th> Student Name </th>
                                        <th> Status </th>
                                        <th> Agency Name </th>
                                        <th> Counselor Name </th>
                                        <th> Counselor Email </th>
                                        <th> University Name </th>
                                        <th> Action </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th> Application id </th>
                                        <th> Student Name </th>
                                        <th> Status </th>
                                        <th> Agency Name </th>
                                        <th> Counselor Name </th>
                                        <th> Counselor Email </th>
                                        <th> University Name </th>
                                        <th> Action </th>
                                    </tr>
                                </tfoot>
                            </table>
                          </div>
                        </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>     

<script type="text/javascript">

$(document).ready(function() {
    $('#newAppExample thead tr').clone(true).appendTo( '#newAppExample thead' );
    $('#newAppExample thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text"  />' );
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                .column(i)
                .search( this.value )
                .draw();
            }
        });
    });

    
});

</script>

<script>
var table;
  $(document).ready(function() {

    //datatables
    table = $('#newAppExample').DataTable({ 
//alert('hi');
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url();?>student_registration/studentapplication/ajax_manage_page_studentNewApplicationList",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
                { "data": "application_id" },
                { "data": "student_name" },
                { "data": "status" },
                { "data": "agency_name" },
                { "data": "facilitator_name" },
                { "data": "facilitator_email" },
                { "data": "university_name" },
                 { "data": "action" }
            ],
        

    });

  });
  
</script>


