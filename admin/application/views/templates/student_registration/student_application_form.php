<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<!--  -->
<style type="text/css">
  @import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");

  .login-block {
    background: #004b7a;
    /* fallback for old browsers */
    background: -webkit-linear-gradient(to bottom, #fafafb, #d8d9da);
    /* Chrome 10-25, Safari 5.1-6 */
    background: linear-gradient(to bottom, #fafafb, #d8d9da);
    /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    float: left;
    width: 100%;
    padding: 50px 0;
  }

  .banner-sec {
    background: url('https://img.freepik.com/free-vector/study-abroad-concept-illustration_114360-7673.jpg?w=826&t=st=1668159349~exp=1668159949~hmac=2a38e542ab6954c88dd817bb1f1f4cc063f80bbcf8e170fa302f4ece1db3b5c2') no-repeat center center;
    background-size: contain;
    min-height: 600px;
    border-radius: 0 10px 10px 0;
    padding: 0;
    background-position: center;
    margin-top: 25px;
  }

  .banner-sec1 {
    background: url('https://img.freepik.com/free-vector/study-abroad-concept-illustration_114360-7493.jpg?w=826&t=st=1668159399~exp=1668159999~hmac=1a0f183f39b6d7f939faa8e9b19666f941d90310244352eda6c9e99f73b0edcf') no-repeat center center !important;
    background-size: contain !important;
    min-height: 515px !important;
  }

  .banner-sec2 {
    background: url('https://img.freepik.com/free-vector/recruit-agent-analyzing-candidates_74855-4565.jpg?w=1380&t=st=1668159566~exp=1668160166~hmac=b651e7c425df9fca5edc05a4efa00aab4ce0c19ba65f9a206c371729128f42af') no-repeat center center !important;
    background-size: contain !important;
  }

  .banner-sec3 {
    background: url('https://img.freepik.com/free-vector/employees-cv-candidates-resume-corporate-workers-students-id-isolate-flat-design-element-job-applications-avatars-personal-information-concept-illustration_335657-1661.jpg?w=826&t=st=1668159690~exp=1668160290~hmac=b8b32533616d9215d931c26e9e7b66a73178ff3607883211159cef0e9b8d7fb4') no-repeat center center !important;
    background-size: contain !important;
  }

  .banner-sec4 {
    background: url('https://img.freepik.com/free-vector/global-data-security-personal-data-security-cyber-data-security-online-concept-illustration-internet-security-information-privacy-protection_1150-37336.jpg?w=826&t=st=1668159740~exp=1668160340~hmac=775762d62fc6aa3b33d3f36812ed962ceb136e0dbb56b3e7074fd978e31c8e54') no-repeat center center !important;
    background-size: contain !important;
  }

  .login-sec {
    padding: 50px 30px;
    position: relative;
  }

  .login-sec .copy-text {
    position: absolute;
    width: 80%;
    bottom: 20px;
    font-size: 13px;
    text-align: center;
  }

  .login-sec h2 {
    margin-bottom: 30px;
    font-weight: 800;
    font-size: 30px;
    color: #004b7a;
  }

  .login-sec h2:after {
    content: " ";
    width: 100px;
    height: 5px;
    background: #f9992f;
    display: block;
    margin-top: 20px;
    border-radius: 3px;
    margin-left: auto;
    margin-right: auto
  }

  .btn-login {
    background: #f9992f;
    color: #fff;
    font-weight: 600;
  }

  .form-control {
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
  }

  .formFlow .nav-tabs {
    display: none;
  }

  .cat {
    margin: 4px;
    background-color: #815dd5;
    border-radius: 4px;
    border: 1px solid #fff;
    overflow: hidden;
    /*float: left;*/
  }

  .cat label {
    float: left;
    line-height: 3.0em;
    width: 100%;
    /*height: 3.0em;*/
    padding: 5px 32px;
    font-size: 14px;
    letter-spacing: 0.8px;
  }

  .cat label span {
    text-align: center;
    padding: 3px 0;
    display: block;
  }

  .cat label input {
    position: absolute;
    /*display: none;*/
    color: #fff !important;
  }


  /* selects all of the text within the input element and changes the color of the text */

  .cat label input+span {
    color: #fff;
  }


  /* This will declare how a selected input will look giving generic properties */

  .cat input:checked+span {
    color: #ffffff;
    text-shadow: 0 0 6px rgba(0, 0, 0, 0.8);
  }

  /*.action input:checked + span {
  background-color: #F75A1B;
 }*/

  .sp {
    background-color: #815dd5 !important;
  }

  #mainselection select {
    border: 0;
    color: #fff;
    background: transparent;
    font-size: 16px;
    font-weight: bold;
    padding: 2px 10px;
    width: -webkit-fill-available;
    height: 30px;
  }

  #mainselection {
    overflow: hidden;
    /* width: 350px; */
    -moz-border-radius: 9px 9px 9px 9px;
    -webkit-border-radius: 9px 9px 9px 9px;
    border-radius: 5px;
    box-shadow: 1px 1px 4px #ccc9cc;
    background: #f6872c;
  }

  .buttonn {

    text-align: right !important;
  }

  .oldList table {
    border-collapse: collapse;
    width: 100%;
  }

  .oldList th,
  td {
    text-align: left;
    padding: 8px;
  }

  /*.oldList tr:nth-child(even){background-color: #f2f2f2}*/

  .oldList th {
    background-color: #815dd5;
    color: white;
    font-size: 15px;
    font-weight: 600;
    letter-spacing: 0.8px;
  }

  /* Create a custom checkbox */
  .checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #eee;
  }

  /* On mouse-over, add a grey background color */
  .containerCbk:hover input~.checkmark {
    background-color: #ccc;
  }

  /* When the checkbox is checked, add a blue background */
  .containerCbk input:checked~.checkmark {
    background-color: #2196F3;
  }

  /* Create the checkmark/indicator (hidden when not checked) */
  .containerCbk:after {
    content: "";
    position: absolute;
    display: none;
  }

  /* Show the checkmark when checked */
  .containerCbk input:checked~.checkmark:after {
    display: block;
  }

  /* Style the checkmark/indicator */
  .containerCbk .checkmark:after {
    left: 9px;
    top: 5px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
  }

  input.larger {
    width: 25px;
    height: 40px;
  }

  #rBtn1,
  #rBtn2 {
    width: 25px;
    height: 40px;
  }

  .btn-website {
    padding: 2px 15px 0px 15px;
    width: auto;
    height: 30px;
    background-color: #F6881F;
    border: medium none;
    border-radius: 100px;
    font-size: 14px;
    color: #FFF;
  }

  .select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #3c8dbc;
    border: 1px solid #3c8dbc;
  }
</style>
<div class="content-wrapper" style="min-height: 800px;">
  <section class="login-block" id="forgot-password-page">
    <div class="container" style="background:#fff; border-radius: 10px; box-shadow: 5px 5px 0px rgba(0,0,0,0.1);">
      <div class="row formFlow">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab1" data-toggle="tab">tab1</a></li>

        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab1">
            <div class="col-md-6 banner-sec"></div>
            <div class="col-md-6 login-sec">
              <h2 class="text-center">Start Your Application</h2>
              <form action="<?php echo base_url() ?>counsellor/add/application" method="post" id="create-application-form" class="form-horizontal" enctype="multipart/form-data">
                <div class="col-md-12 form-group">
                  <label for="inputEmail3" class="text-uppercase"> Desired Country : &nbsp;&nbsp; </label>
                  <select name="application_country" id="application_country" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                    <option value="0">Select Country</option>
                    <?php foreach ($countries as $country) { ?>
                      <option value="<?php echo $country->id; ?>" <?php if (isset($selected_country) && $selected_country == $country->name) {
                                                                    echo 'selected="selected"';
                                                                  } ?>>
                        <?php echo strtoupper($country->name); ?>
                      </option>
                    <?php } ?>
                    <!--<option value="India">INDIA</option>-->
                  </select>
                </div>
                <div class="col-md-12 form-group">
                  <label for="inputEmail3" class="text-uppercase"> Desired University &nbsp;&nbsp; </label>
                  <div id="mainselection">
                    <select name="application_university" id="application_university" class="form-control searchoptions js-select2" multiple="multiple" style="width:100%; height:30px; font-size:12px;" required>
                    </select>
                  </div>
                </div>
                <script type="text/javascript">
                  function selectRefresh() {
                    $(".js-select2").select2({
                      //alert('js inside');
                      tags: true,
                      //closeOnSelect : false,
                      placeholder: "Select University",
                      //allowHtml: true,
                      allowClear: true,
                      width: '100%'
                    });
                  }
                </script>
                <div class="form-check" style="text-align: center;">

                  <input type="submit" style="text-align: center; border-radius:5px;" value="Next" class="btn btn-lg btn-primary">

                </div>
              </form>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    selectRefresh();
  });
  $(document).ready(function() {

    //country
    $('#application_country').on('change', function() {
      var dataString = 'selected_country_id=' + this.value;
      $.ajax({
        type: "POST",
        url: "/admin/student_registration/university/getPartneredUniversityList",
        data: dataString,
        cache: false,
        success: function(result) {
          $('#application_university').html(result);
        }
      });
    });

  })

  // $(document).ready(function() {

  //     //country
  //     $('#application_country').on('change', function() {
  //       alert('on change 1');
  //       var dataString = 'selected_country_id=' + this.value;

  //       $.ajax({
  //         type: "POST",
  //         url: "/admin/student_registration/university/getPartneredUniversityList",
  //         data: dataString,
  //         cache: false,
  //         success: function(result) {
  //           alert('on change result');
  //           //alert(result);
  //           $('#application_university').html(result);

  //         }
  //       });
  //     });



  //   })
  //registration
  function resgistration() {
    var user_name = $('#user_name').val();
    var user_email = $('#user_email').val();
    var user_mobile = $('#user_mobile').val();
    var desired_country = $('#desired_country').val();
    var desired_mainstream = $('#desired_mainstream').val();
    var desired_subcourse = $('#desired_subcourse').val();
    var desired_levelofcourse = $('#desired_levelofcourse').val();
    var userPassword = $('#password').val();
    //var userId = $('#user_id').val();
    var dataString = 'user_name=' + user_name + '&user_email=' + user_email + '&user_mobile=' + user_mobile + '&desired_country=' + desired_country + '&desired_mainstream=' + desired_mainstream + '&desired_subcourse=' + desired_subcourse + '&desired_levelofcourse=' + desired_levelofcourse + '&user_password=' + userPassword;
    event.preventDefault();
    $.ajax({
      type: "POST",
      url: "/admin/counsellor/submit/student",
      data: dataString,
      cache: false,
      success: function(result) {
        if (result.status == "SUCCESS") {
          alert("Student Created successFully. Now apply for university from list..!!!");
          window.location.href = '/admin/counsellor/student/offerlist/view/' + result.student_id;
        } else if (result.status == 'With Email-ID Or Mobile Account Already Exists') {
          alert(result);
          window.location.href = '/user/account';
        } else {
          alert(result);
        }
      }
    });
  }
  $('.btnNext').click(function() {
    $('.nav-tabs > .active').next('li').find('a').trigger('click');
  });
  $('.btnPrevious').click(function() {
    $('.nav-tabs > .active').prev('li').find('a').trigger('click');
  });

  $(document).ready(function() {
    $('input[type="radio"]').click(function() {
      if ($(this).attr('id') == 'rBtn1') {
        $('ul li a[href="#tab6"]').click();
      }
      if ($(this).attr('id') == 'rBtn2') {
        $('ul li a[href="#tab5"]').click();
      }
    });
  });

  $('.btnOldPev').click(function() {
    $('ul li a[href="#tab4"]').click();
  });

  $('.btnNewPev').click(function() {
    $('ul li a[href="#tab4"]').click();
  });

  $('a[href="#tab5"]').click(function() {
    $(this).addClass('active');
  });

  $('a[href="#tab6"]').click(function() {
    $(this).addClass('active');
  });

  $('a[href="#tab4"]').click(function() {
    $(this).addClass('active');
    //$('.trchkOptions input[type="checkbox"]').not(this).prop('checked', false);
  });
</script>