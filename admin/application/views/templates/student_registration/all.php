<style>
    .counselors-list
    {
        display: none;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Student<small>Manage Students</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Student Management</a></li>
            <li class="active">Student</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php
                if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>

                <div class="box">
                    <form action="<?php echo base_url();?>student/edit" method="post">
                        <div class="box-header">
                            <a href="<?php echo base_url();?>student/index/add"><button type="button" style="width:12%; float:left; margin-right:5px;" class="btn btn-block btn-primary"><i class="fa fa-plus"></i> Add Student</button></a>
                            <button type="submit" class="btn btn-block btn-danger multidel" style="width:11%; display:none;"><i class="fa fa-minus"></i> Delete University</button>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                          <div class="table-responsive">
                            <table id="allAppExample" class="table table-bordered table-striped">
                              <thead>
                                  <tr>
                                      <th> Application id </th>
                                      <th> Student Name </th>
                                      <th> Status </th>
                                      <th> Agency Name </th>
                                      <th> Counselor Name </th>
                                      <th> Counselor Email </th>
                                      <th> University Name </th>
                                      <th> Action </th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <!-- <?php
                                  $i=1;
                                  foreach($students as $student)
                                  {
                                      $agency_name = json_decode($student->agency_name, true);
                                      ?>
                                      <tr>
                                          <td><?php echo $student->application_id;?></td>
                                          <td><?php echo $student->student_name . " ". $student->student_last_name;?></td>
                                          <td><?php echo $student->status;?></td>
                                          <td><?php echo $agency_name['agency_name'];?></td>
                                          <td><?php echo $student->facilitator_name;?></td>
                                          <td><?php echo $student->facilitator_email;?></td>
                                          <td><?php echo $student->university_name;?></td>
                                          <td><a href='/admin/student/profile/<?php echo $student->user_id; ?>/<?php echo $student->application_id; ?>' title="Modify" class="edit" ><i class="fa fa-edit"></i></a></td>

                                      </tr>
                                      <?php
                                  }
                                  ?> -->
                              </tbody>
                              <tfoot>
                                  <tr>
                                      <th> Application id </th>
                                      <th> Student Name </th>
                                      <th> Status </th>
                                      <th> Agency Name </th>
                                      <th> Counselor Name </th>
                                      <th> Counselor Email </th>
                                      <th> University Name </th>
                                      <th> Action </th>

                                  </tr>
                              </tfoot>
                            </table>
                          </div>
                        </div><!-- /.box-body -->
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>

<script type="text/javascript">

    function showCounselors(studentId)
    {
        document.getElementById("assigned-counselor-" + studentId).style.display = "none";
        document.getElementById("counselors-list-" + studentId).style.display = "block";
    }

    function deleteUser(userId)
    {
        $.ajax({
            type: "GET",
            url: "/admin/student/index/delete?uid=" + userId,
            cache: false,
            success: function(result){
                alert("Student has been deleted successfully");
                window.location.href = '/admin/student/index/all';
            }
        });
    }

    function updateCounselor(studentId, counselor)
    {
        var splitCounselor = counselor.split('|');
        var counselorId = splitCounselor[0];
        var counselorName = splitCounselor[1];
        $.ajax({
            type: "POST",
            url: "/admin/student/index/update",
            data: {"student_id": studentId, "facilitator_id": counselorId},
            cache: false,
            success: function(result){
                if(result == 1){
                    alert('Success');
                    document.getElementById("assigned-counselor-" + studentId).innerHTML = counselorName;
                }
                else{
                    alert(result);
                }
                document.getElementById("assigned-counselor-" + studentId).style.display = "block";
                document.getElementById("counselors-list-" + studentId).style.display = "none";
            }
        });
    }
</script>

<script type="text/javascript">

$(document).ready(function() {
    $('#allAppExample thead tr').clone(true).appendTo( '#allAppExample thead' );
    $('#allAppExample thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text"  />' );
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                .column(i)
                .search( this.value )
                .draw();
            }
        });
    });

    /*var table = $('#example').DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        "columnDefs": [
            { "width": "10%", "targets": 0 },
            { "width": "14%", "targets": 1 },
            { "width": "15%", "targets": 2 },
            { "width": "10%", "targets": 3 },
            { "width": "14%", "targets": 4 },
            { "width": "10%", "targets": 5 },
            { "width": "10%", "targets": 6 },
            { "width": "10%", "targets": 7 },
            { "width": "7%", "targets": 7 }
        ]
    });*/
});

</script>

<script>
var table;
  $(document).ready(function() {

    //datatables
    table = $('#allAppExample').DataTable({ 
//alert('hi');
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url();?>student_registration/studentapplication/ajax_manage_page_studentAllApplicationList",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
                { "data": "application_id" },
                { "data": "student_name" },
                { "data": "status" },
                { "data": "agency_name" },
                { "data": "facilitator_name" },
                { "data": "facilitator_email" },
                { "data": "university_name" },
                 { "data": "action" }
            ],
        

    });

  });
  
</script>
