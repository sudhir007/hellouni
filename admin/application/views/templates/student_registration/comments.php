<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<style type="text/css">
    .styleCss{
        font-size: 14px;
        background-color: #f6882c;
        color: white;
        font-weight: 600;
        letter-spacing: 0.8px;
    }
    .formControl{
        color: #fff !important;
        background-color: #8c57c5 !important;
        font-size: 12px;
        border: 0px !important;
        box-shadow: inset 0 0 0 rgba(0,0,0,.075) !important;
        margin-top: 15px;
    }
    #dt-example td.details-control {
        cursor: pointer;
    }
    #dt-example tr.shown td {
        background-color: #815dd5;
        color: #fff !important;
        border: none;
        border-right: 1px solid #815dd5;
    }
    #dt-example tr.shown td:last-child {
        border-right: none;
    }
    #dt-example tr.details-row .details-table tr:first-child td {
        color: #fff;
        background: #f6882c;
        border: none;
    }
    #dt-example tr.details-row .details-table > td {
        padding: 0;
    }
    #dt-example tr.details-row .details-table .fchild td:first-child {
        cursor: pointer;
    }
    #dt-example tr.details-row .details-table .fchild td:first-child:hover {
        background: #fff;
    }
    #dt-example .form-group.agdb-dt-lst {
        padding: 2px;
        height: 23px;
        margin-bottom: 0;
    }
    #dt-example .form-group.agdb-dt-lst .form-control {
        height: 23px;
        padding: 2px;
    }
    #dt-example .adb-dtb-gchild {
        padding-left: 2px;
    }
    #dt-example .adb-dtb-gchild td {
        background: #f5fafc;
        padding-left: 15px;
    }
    #dt-example .adb-dtb-gchild td:first-child {
        cursor: default;
    }
    .dataTables_wrapper{
        overflow: auto;
    }
    .listTableView thead tr{
        background-color: #f6882c;
        color: white;
    }

.content-wrapper{
  padding-left: 1%;
  min-height: 900px !important;
}
</style>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<div class="content-wrapper" >
        <div class="panel panel-default">
            <div class="panel-heading purplePanel"><h4 class="text-center"> Communication Formss </h4></div>
            <div class="panel-body">

              <form action="<?php echo base_url()?>counsellor/student/comments/submit" class="form-group"  method="post"  style="background-color: white;padding: 30px;">

                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                          <h4> <b>Add Your Comments : </b></h4>

                          <div class="form-group col-md-6" style="margin: 2px 0px;">
                            <label class="col-md-5"> Application Info : </label>
                            <label class="col-md-5">
                                <?php
                                   foreach($application_info as $applications)
                                   {
                                       ?>
                                        <?php echo $applications->name." ".$applications->university_course_name." ".$applications->status ." ".$applications->sub_status ;?>
                                        <?php
                                   }
                                   ?>
                            </label>

                          </div>

                              <!--<div class="form-group col-md-6" style="margin: 2px 0px;">
                                  <label class="col-md-5">Comment Type : </label>
                                  <select class="form-control bRadius col-md-3" id="entity_type" name="entity_type" required>
                                    <option value=""> Select Type </option>
                                     <?php
                                        foreach($comment_types as $key => $value)
                                        {
                                            ?>
                                             <option value="<?php echo $key;?>"><?php echo $value;?></option>
                                             <?php
                                        }
                                        ?>
                                  </select>
                              </div>-->

                              <!-- <div class="form-group col-md-6" >
                                  <label class="col-md-5">Lead State : </label>
                                  <select class="form-control bRadius col-md-3" id="lead_state" name="lead_state" >
                                    <option value=""> Select Type </option>
                                     <?php
                                        foreach($lead_state_list as $keyl => $valuel)
                                        {
                                            ?>
                                             <option value="<?php echo $keyl;?>"><?php echo $valuel;?></option>
                                             <?php
                                        }
                                        ?>
                                  </select>
                              </div> -->

                              <!--<div class="form-group col-md-6">
                                  <label class="col-md-5">Application Name : </label>
                                  <select class="form-control bRadius col-md-3" id="entity_id" name="entity_id" required>
                                    <option value=""> Select Application </option>
                                     <?php
                                        $applicationId = 12;
                                        foreach($applied_universities as $university)
                                        {
                                            ?>
                                             <option value="<?php echo $university->id;?>"><?php echo $university->name." ".$university->college_name." ".$university->course_name ;?></option>
                                             <?php
                                        }
                                        ?>
                                  </select>
                              </div>-->

                          <div class="form-group col-md-6" >
                              <label class="col-md-5">Comment : </label>
                              <textarea name="comment" id="comment" cols="80" rows="5" required></textarea>
                          </div>
                        </div>
                    </div>
                </div>

                <div class="add_comment_button">
                      <input type="hidden" name="student_login_id" id="student_login_id" value="<?=$student_login_id?>">
                      <input type="hidden" name="app_id" id="app_id" value="<?=$app_id?>">
                      <button type="button" class="button_sign_in btn btn-md btn-primary pull-right" onclick="addComment()">Add Comment</button>
                </div>
              </form>
            </div>
        </div>

        <div class="container" style="clear:both;">
        <div class="row" style="padding: 30px 0px 30px 0px;display: flex;">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body listTableView">
                        <table id="example1" class="stripe row-border order-column cl-table dataTable no-footer" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th> Type </th>
                                    <th> Student Name </th>
                                    <th> University Name </th>
                                    <th> Comment </th>
                                    <th> Comment By </th>
                                    <th> Date </th>
                                </tr>
                            </thead>
                            <tbody>

                              <?php

                              foreach($comment_list as $comments)
                              {

                              ?>
                                  <tr>
                                      <td><?php echo $comments->entity_type;?></td>
                                      <td><?php echo $comments->student_name;?></td>
                                      <td><?php echo $comments->university_name;?></td>
                                      <td><?php echo $comments->comment;?></td>
                                      <td><?php echo $comments->commented_by;?></td>
                                      <td><?php echo $comments->added_time;?></td>
                                  </tr>
                            <?php
                              }
                              ?>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th> Type </th>
                                    <th> Student Name </th>
                                    <th> University Name </th>
                                    <th> Comment </th>
                                    <th> Comment By </th>
                                    <th> Date </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <!--<div class="container" style="clear:both;">
        <div class="row" style="padding: 30px 0px 30px 0px;display: flex;">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body listTableView">
                        <table id="dt-example" class="stripe row-border order-column cl-table dataTable no-footer" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th> Type </th>
                                    <th> Student Name </th>
                                    <th> University Name </th>
                                    <th> Comment </th>
                                    <th> Comment By </th>
                                    <th> Date </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
</div>



<script type="text/javascript">
    $(document).ready(function() {

      var dataentityId =<?=$student_login_id?>; //$('#user_id').val();
      var appId =<?=$app_id?>; //$('#user_id').val();

        $('#data-table').dataTable();
        $(".dataTable").on("draw.dt", function (e) {
            setCustomPagingSigns.call($(this));
        }).each(function () {
            setCustomPagingSigns.call($(this)); // initialize
        });

        function setCustomPagingSigns() {
            var wrapper = this.parent();
            wrapper.find("a.previous").text("<");
            wrapper.find("a.next").text(">");
        }

        var data = [];
        var dataString = '';
        var dataurl = "/admin/counsellor/application/comments/"+dataentityId+"/"+appId;

        $.ajax({
            type: "POST",
            url: dataurl,
            data: dataString,
            cache: false,
            success: function(result){
              console.log("hello",result);
                var studentdata = result;
                if(studentdata.length){
                    studentdata.forEach(function(value, index){
                        data.push({
                            "Type" : value.entity_type,
                            "Student_Name" : value.student_name,
                            "University_Name" : value.university_name,
                            "Message" : value.comment,
                            "Added_by" : value.commented_by,
                            "DateTime" : value.added_time
                        });
                    });

                    var table = $('.dataTable').DataTable({
                        columns : [
                          {data : 'Type'},
                          {data : 'Student_Name'},
                          {data : 'University_Name'},
                          {
                                data : 'Message',
                                className : 'details-control',
                            },
                            {data : 'Added_by'},
                            {data : 'DateTime'}
                        ],
                        data : data,
                        pagingType : 'full_numbers',
                        language : {
                            emptyTable     : 'No data to display.',
                            zeroRecords    : 'No records found!',
                            thousands      : ',',
                            loadingRecords : 'Loading...',
                            search         : 'Search:',
                            paginate       : {
                                next     : 'next',
                                previous : 'previous'
                            }
                        },
                        "bDestroy": true
                    });

                    $('.dataTable tbody').on('click', 'td.details-control', function () {
                        var tr  = $(this).closest('tr'),
                        row = table.row(tr);

                        if (row.child.isShown()) {
                            tr.next('tr').removeClass('details-row');
                            row.child.hide();
                            tr.removeClass('shown');
                        }
                        else {
                            row.child(format(row.data())).show();
                            tr.next('tr').addClass('details-row');
                            tr.addClass('shown');
                        }
                    });

                    $('#dt-example').on('click','.details-table tr',function(){
                        $('.details-table').find('.adb-dtb-gchild:not(:first-child)').slideToggle();
                    });
                }
            }
        });
    });

    function addComment(){
//alert('hi');
        var userId = $('#student_login_id').val();
        var appId = $('#app_id').val();
        //var universityId = $('#university_id').val() ? $('#university_id').val() : 0;
        var comment = $('#comment').val();

        var dataString = 'student_login_id=' + userId + '&app_id=' + appId + '&comment=' + comment;

        event.preventDefault();

        $.ajax({
            type: "POST",
            url: "/admin/counsellor/application/comments/add",
            data: dataString,
            cache: false,
            success: function(result){
                if(result == "SUCCESS"){
                    alert("Comment Sent Successfully...!!!");
                    window.location.href='/admin/counsellor/application/comments/view/'+userId+'/'+appId;
                } else {
                    alert(result);
                    window.location.href='/admin/user/account';
                }
            }
        });
    }
</script>
