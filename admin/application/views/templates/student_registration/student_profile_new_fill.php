<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.7/js/dataTables.fixedHeader.min.js"></script>
<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
<style type="text/css">
  .mytabs {
    margin: 2% 0px;
  }

  .mytabs .nav-tabs {
    background-color: #815dd5;
  }

  .mytabs .nav-tabs>li>a: hover {
    color: black;
  }

  .prfileBox3 {
    background-color: #815dd5;
    margin: 2% 0px;
    padding: 1%;
    border-radius: 7px;
  }

  .margin2 {
    margin: 2px 0 2px !important;
  }

  .ptag {
    margin: 0px;
    font-size: 13px;
    line-height: 1.8;
    letter-spacing: 1px;
  }

  .h3Heading {
    background-color: #f6882c;
    padding: 8px 2px;
    text-align: center;
    color: white;
  }

  .boxAppList {
    box-shadow: 0px 0px 12px #d8d1d1;
    border-radius: 7px;
  }

  .listtableMain thead tr {
    color: #815dd5;
    font-size: 13px;
    /* font-weight: 600; */
    letter-spacing: 0.8px;
  }

  table.dataTable.stripe tbody tr.odd,
  table.dataTable.display tbody tr.odd {
    background-color: #ffffff !important;
  }

  table.dataTable.display tbody tr.odd>.sorting_1,
  table.dataTable.order-column.stripe tbody tr.odd>.sorting_1 {
    background-color: #ffffff !important;
  }

  table.dataTable.display tbody tr.even>.sorting_1,
  table.dataTable.order-column.stripe tbody tr.even>.sorting_1 {
    background-color: #ffffff !important;
  }

  .form-control {
    height: 30px !important;
  }

  .prplButton {
    color: #fff;
    background: #8703c5;
    border-color: #8703c5;
    border-radius: 12px;
  }

  .resetButton {
    color: #8c57c6;
    background: #f1f1f1;
    border-color: #8703c5;
    border-radius: 12px;
  }

  radio-item {
    display: inline-block;
    position: relative;
    padding: 0 6px;
    /*margin: 10px 0 0;*/
  }

  .radio-item input[type='radio'] {
    display: none;
  }

  .radio-item label {
    color: #666;
    font-weight: normal;
  }

  .radio-item label:before {
    content: " ";
    display: inline-block;
    position: relative;
    top: 4px;
    margin: 0 5px 0 0;
    width: 15px;
    height: 15px;
    border-radius: 11px;
    border: 2px solid #f6882c;
    background-color: transparent;
  }

  .radio-item input[type=radio]:checked+label:after {
    border-radius: 11px;
    width: 7px;
    height: 7px;
    position: absolute;
    top: 8px;
    left: 10px;
    content: " ";
    display: block;
    background: #f6882c;
  }

  .tabName {
    letter-spacing: 0.6px;
    font-size: 14px;
  }

  #exam_stage,
  #exam_date,
  #result_date,
  #exam_score,
  #english_exam_stage,
  #english_exam_date,
  #english_result_date,
  #english_exam_score,
  #exam_score_gre,
  #exam_score_gmat,
  #exam_score_sat,
  #english_exam_tofel,
  #english_exam_ielts,
  #english_exam_pte,
  #english_exam_duolingo,
  #diploma_id,
  #masters_id {
    display: none;
  }

  .checkBoxDesign {
    box-shadow: 0 0 black !important;
    /*margin-top: -4px !important;*/
  }


  /* Style tab links */

  .tablink,
  .tablink1 {
    background-color: #f7f7f7;
    color: #f39d3f;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 10px 10px;
    font-size: 18px;
    font-weight: 600;
    letter-spacing: 1.3px;
    width: 33%;
  }

  .tablink:hover,
  .tablink1:hover {
    background-color: #eae7e7;
  }


  /* Style the tab content (and add height:100% for full page content) */

  .tabcontent {
    color: black;
    display: none;
    padding: 70px 20px;
    height: 100%;
  }

  .tabcontent1 {
    color: black;
    display: none;
    padding: 70px 20px;
    height: 100%;
  }


  /*application*/

  h1 {
    color: #333;
    font-family: arial, sans-serif;
    margin: 1em auto;
    width: 80%;
  }

  .tabordion,
  .tabordion1 {
    color: #333;
    display: block;
    font-family: arial, sans-serif;
    margin: auto;
    position: relative;
    width: 100%;
  }

  .tabordion input[name="sections"] {
    left: -9999px;
    position: absolute;
    top: -9999px;
  }

  .tabordion1 input[name="sections1"] {
    left: -9999px;
    position: absolute;
    top: -9999px;
  }

  .tabordion section,
  .tabordion1 section {
    display: block;
  }

  .tabordion section label,
  .tabordion1 section label {
    background: #ffffff;
    border-right: 3px solid #815dd5;
    cursor: pointer;
    display: block;
    font-size: 1.2em;
    font-weight: bold;
    padding: 1px 15px;
    position: relative;
    width: 26%;
    z-index: 100;
  }

  .tabordion section article,
  .tabordion1 section article {
    background: white;
    display: none;
    left: 27%;
    min-width: 300px;
    /* padding: 0 0 0 21px;*/
    position: absolute;
    top: 0;
  }

  .tabordion section article:after {
    /*background-color: #ccc;*/
    bottom: 0;
    content: "";
    display: block;
    left: -229px;
    position: absolute;
    top: 0;
    width: 220px;
    z-index: 1;
  }

  .tabordion1 section .a1:after {
    /*background-color: #ccc;*/
    bottom: 0;
    content: "";
    display: block;
    left: -229px;
    position: absolute;
    top: 0;
    width: 220px;
    z-index: 1;
  }

  .tabordion input[name="sections"]:checked+label {
    background: white;
    color: #fff;
    border-left: 3px solid #f39d3f;
  }

  .tabordion input[name="sections"]:checked+label i {
    color: #f6882c;
  }

  .tabordion input[name="sections"]:checked~article {
    display: block;
  }

  .tabordion1 input[name="sections1"]:checked+label {
    background: white;
    color: #fff;
    border-left: 3px solid #f39d3f;
  }

  .tabordion1 input[name="sections1"]:checked+label i {
    color: #f6882c;
  }

  .tabordion1 input[name="sections1"]:checked~.a1 {
    display: block;
  }


  /*@media (max-width: 533px) {
   h1 {
   width: 100%;
   }
   .tabordion {
   width: 100%;
   }
   .tabordion section label {
   font-size: 1em;
   width: 160px;
   }
   .tabordion section article {
   left: 200px;
   min-width: 270px;
   }
   .tabordion section article:after {
   background-color: #ccc;
   bottom: 0;
   content: "";
   display: block;
   left: -199px;
   position: absolute;
   top: 0;
   width: 200px;
   }
   }*/

  .tabordion section label h6,
  .tabordion1 section label h6 {
    text-transform: initial;
    font-size: 13px;
  }

  .tabordion section label h6 i,
  .tabordion1 section label h6 i {
    color: #815dd5;
  }

  @media (max-width: 768px) {
    h1 {
      width: 96%;
    }

    .tabordion {
      width: 100%;
    }
  }

  @media (min-width: 1366px) {
    h1 {
      width: 70%;
    }

    .tabordion,
    .tabordion1 {
      width: 100%;
    }
  }


  /* Style the tab */

  .newTab .tab,
  .newTab1 .tab1 {
    overflow: hidden;
    border: 1px solid #815dd5;
    /*background-color: #f1f1f1;*/
  }


  /* Style the buttons inside the tab */

  .newTab .tab button,
  .newTab1 .tab1 button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
  }


  /* Change background color of buttons on hover */

  .newTab .tab button:hover,
  .newTab1 .tab1 button:hover {
    background-color: #815dd533;
  }


  /* Create an active/current tablink class */

  .newTab .tab button.active,
  .newTab1 .tab1 button.active {
    background-color: #815dd5;
    color: white;
  }


  /* Style the tab content */

  .newTab .tabcontent1,
  .newTab1 .tabcontent111 {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
    max-height: 520px;
    overflow: overlay;
  }

  .textVertical {
    writing-mode: tb-rl;
    transform: rotate(-180deg);
    color: white;
  }

  .verticalDiv {
    padding: 20px;
    /*background-color: #5d82d5;*/
    color: white;
    /* margin-top: 12px; */
    text-align: -webkit-center;
  }

  .verticalDiv .col-md-11 {
    padding-left: 0px;
  }

  .requiredDoc {
    background-color: #5d82d5;
  }

  .optDoc {
    background-color: #5dd5c4;
  }

  .docUplodedName {
    background-color: ghostwhite;
  }

  .docUploadNameLink {
    color: #815dd5 !important;
  }

  .docUploadBox {
    box-shadow: 1px 1px 1px 1px #efe7e7;
    padding-left: 0px !important;
    margin-bottom: 2%;
  }

  .docName h5 {
    text-transform: capitalize;
    font-family: inherit;
    margin: 10px 0px;
    color: #000000;
    font-weight: inherit;
    letter-spacing: 1.5px;
  }

  .docName p {
    margin: 8px;
  }

  .statusBox {
    padding: 2% 1%;
    text-align: center;
  }

  .fa-times {
    font-size: 30px;
    color: red;
  }

  .fa-check {
    font-size: 30px;
    color: green;
  }

  .comment-box {
    margin-top: 30px !important;
  }


  /* CSS Test end */

  .comment-box img {
    width: 50px;
    height: 50px;
  }

  .comment-box .media-left {
    padding-right: 10px;
    width: 9%;
  }

  .comment-box .media-body p {
    border: 1px solid #ddd;
    padding: 10px;
  }

  .comment-box .media-body .media p {
    margin-bottom: 0;
  }

  .comment-box .media-heading {
    background-color: #815dd5;
    border: 1px solid #815dd5;
    padding: 7px 10px;
    position: relative;
    margin-bottom: -1px;
    color: white;
  }

  .comment-box .media-heading:before {
    content: "";
    width: 12px;
    height: 12px;
    background-color: #f5f5f5;
    border: 1px solid #ddd;
    border-width: 1px 0 0 1px;
    -webkit-transform: rotate(-45deg);
    transform: rotate(-45deg);
    position: absolute;
    top: 10px;
    left: -6px;
  }

  .sep {
    margin-top: 20px;
    margin-bottom: 20px;
    background-color: #e6c3c3;
  }

  .requiredInfoForm label {
    border-right: 3px solid #ffffff !important;
    cursor: pointer !important;
    display: inline-block !important;
  }


  /* Style the tab */

  .docTab .tab,
  .docTab1 .tab {
    overflow: hidden;
    border: 1px solid #815dd5;
    background-color: #815dd5e6;
    color: white;
  }


  /* Style the buttons inside the tab */

  .docTab .tab button,
  .docTab1 .tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
  }


  /* Change background color of buttons on hover */

  .docTab .tab button:hover,
  .docTab1 .tab button:hover {
    background-color: #f6882c;
  }


  /* Create an active/current tablink class */

  .docTab .tab button.active,
  .docTab1 .tab button.active {
    background-color: #fff;
    color: #f6882c;
  }


  /* Style the tab content */

  .docTab .tabcontent2,
  .docTab1 .tabcontent21 {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
  }

  /*document modal */

  #modalDocument .modal,
  #modalDocument1 .modal {
    z-index: 10000 !important;
  }

  #modalDocument .modal-title,
  #modalDocument1 .modal-title {
    text-align: center;
    text-transform: uppercase;
    color: #ffffff;
  }

  #modalDocument .modal-content,
  #modalDocument1 .modal-content {
    background-color: white;
    color: #000;
    padding: 20px 50px;
  }

  #modalDocument .modal-header,
  #modalDocument1 .modal-header {
    border-bottom: none;
    background: #00c0ef;
    color: #ffffff;
    text-align: center;
  }

  #modalDocument .modal-dialog,
  #modalDocument1 .modal-dialog {
    z-index: 10000;
    width: auto;
  }

  #modalDocument .modal-header .close,
  #modalDocument1 .modal-header .close {
    color: #ffffff;
  }

  #modalDocument .modal-body,
  #modalDocument1 .modal-body {
    font-size: 16px;
    font-weight: 300;
  }

  #modalDocument .modal-body p,
  #modalDocument1 .modal-body p {
    font-size: 12px;
    font-weight: 300;
  }

  .appListTable thead {
    background-color: #815dd5;
    color: white;
    font-size: 13px;
    font-weight: 600;
  }

  .firtsTabs .firstTab .btntab {
    width: 50%;
  }

  .firtsTabs .firstTab .btntab.active {
    background-color: #f9992f;
  }

  .firstTab .tabsublinks {
    color: #fff;
    background-color: #815dd5;
    border-color: #815dd5;
  }

  .secondTabs .tabsublinks:hover {
    background-color: #815dd5;
  }

  .secondTabs .firstTab .btntab {
    width: 50%;
  }

  .secondTabs .firstTab .btntab.active {
    background-color: #f9992f;
  }

  .secondTabs .tabsublinks {
    color: #fff;
    background-color: #815dd5;
    border-color: #815dd5;
  }

  .secondTabs .tabsublinks:hover {
    background-color: #815dd5;
  }


  /* The Modal (background) */

  .modal {
    display: none;
    /* Hidden by default */
    position: fixed;
    /* Stay in place */
    z-index: 1;
    /* Sit on top */
    padding-top: 100px;
    /* Location of the box */
    left: 0;
    top: 0;
    width: 100%;
    /* Full width */
    height: 100%;
    /* Full height */
    /*z-index: 100000;*/
    overflow: auto;
    /* Enable scroll if needed */
    background-color: rgb(0, 0, 0);
    /* Fallback color */
    background-color: rgba(0, 0, 0, 0.4);
    /* Black w/ opacity */
  }


  /* Modal Content */

  .modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 40%;
    z-index: 10000;
  }

  #fairModal .modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: auto;
    z-index: 10000;
  }

  .fairModal a.pre-order-btn {
    color: #000;
    background-color: gold;
    border-radius: 1em;
    padding: 1em;
    display: block;
    margin: 1.5em auto;
    width: 50%;
    font-size: 1.25em;
    font-weight: 6600;
  }

  .fairModal a.pre-order-btn:hover {
    background-color: #3c1e4c;
    text-decoration: none;
    color: gold;
  }


  /* The Close Button */

  .close {
    color: #8e0f0f;
    float: right;
    font-size: 28px;
    font-weight: bold;
  }

  .close:hover,
  .close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
  }

  .accordion {
    background-color: #fff;
    color: black;
    cursor: pointer;
    padding: 12px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
  }

  .active,
  .accordion:hover {
    background-color: #fff;
  }

  .accordion:after {
    content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
  }

  .accordion .active:after {
    content: "\2212";
  }

  .accordianContainer .panel {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
  }

  .accordianContainer .fa-2x {
    font-size: 1.2em;
  }


  .wrapper {
    width: auto;
    font-size: 14px;
    background-color: white !important;
    border-radius: 5px;
    /*padding:1pc;*/
    margin-bottom: 1pc;
  }

  .StepProgress {
    position: relative;
    padding-left: 45px;
    list-style: none;
  }

  .StepProgress::before {
    display: inline-block;
    content: '';
    position: absolute;
    top: 0;
    left: 15px;
    width: 10px;
    height: 100%;
    border-left: 2px solid #CCC;
  }

  .StepProgress-item {
    position: relative;
    counter-increment: list;
  }

  .StepProgress-item:not(:last-child) {
    padding-bottom: 20px;
  }

  .StepProgress-item::before {
    display: inline-block;
    content: '';
    position: absolute;
    left: -30px;
    height: 100%;
    width: 10px;
  }

  .StepProgress-item::after {
    content: '';
    display: inline-block;
    position: absolute;
    top: 0;
    left: -40px;
    width: 21px;
    height: 21px;
    border: 2px solid #CCC;
    border-radius: 50%;
    background-color: #FFF;
  }

  .StepProgress-item.is-done::before {
    border-left: 2px solid #f6882c;
  }

  .StepProgress-item.is-done::after {
    content: "✔";
    font-size: 13px;
    color: #FFF;
    text-align: center;
    border: 2px solid #f6882c;
    background-color: #f6882c;
  }

  .StepProgress-item.current::before {
    border-left: 2px solid #f6882c;
  }

  .StepProgress-item.current::after {
    content: "✔";
    padding-top: 0px;
    width: 21px;
    height: 21px;
    top: 0px;
    left: -40px;
    font-size: 13px;
    text-align: center;
    color: #fff;
    border: 2px solid #f6882c;
    background-color: #f6882c;
  }

  .StepProgress strong {
    display: block;
  }

  /* Button used to open the chat form - fixed at the bottom of the page */
  .open-button {
    padding: 9px 12px;
    background-color: #f39c12;
    color: white;
    font-size: 17px;
    max-width: 230px;
    border-radius: 20px;
    border: none;
    cursor: pointer;
    position: fixed;
    bottom: 50px;
    right: 15px;
  }

  /* The popup chat - hidden by default */
  .chat-popup {
    display: none;
    position: fixed;
    bottom: 0;
    right: 15px;
    z-index: 9;
    background-color: #ffffff;
    box-shadow: -3px 4px 8px 2px #00000029;
    border-radius: 14px;
    padding: 0.5% 1.2%;

  }

  /* Add styles to the form container */
  .form-container {
    max-width: 340px;
    padding: 10px;
    border-radius: 20px;
    /*background-color: white;*/
    color: #777;
  }

  /* Full-width textarea */
  .form-container textarea {
    width: 100%;
    padding: 15px;
    margin: 5px 0 10px 0;
    border: 1px solid #0000004d;
    border-radius: 20px;
    background: #f5f5f5;
    resize: none;
    min-height: 105px;
    color: #777;
  }

  /* When the textarea gets focus, do something */
  .form-container textarea:focus {
    background-color: #ddd;
    outline: none;
  }

  /* Set a style for the submit/send button */
  .form-container .btn-sm {
    background-color: #f9992f;
    color: white;
    font-size: 12px;
    font-weight: 600;
    padding: 10px 15px;
    border: none;
    border-radius: 20px;
    cursor: pointer;
    width: 40%;
    margin-bottom: 10px;
  }

  /* Add a red background color to the cancel button */
  .form-container .cancel {
    background-color: #815dd5;
    margin-left: 20%;
  }

  /* Add some hover effects to buttons */
  .form-container .btn:hover,
  .open-button:hover {
    opacity: 0.8;
  }

  .chat-popup .a {
    height: 350px;
    overflow: auto;
    margin: 10px 0px;
  }

  .chat-popup .content {
    height: 300px;
    width: 100%;
    background: #fff;
  }

  .chat-popup .p_bar {
    width: 1%;
    height: 5px;
    background: linear-gradient(to right, #f9992f 0%, #815dd5 100%);
    position: sticky;
    top: 20px;
    border-radius: 20px;

  }

  .chat-popup p {
    box-shadow: 0px 3px 7px 0px #00000024;
    padding: 3% 6%;
    margin-bottom: 8%;
    font-size: 14px;
    color: #484646;
  }


  .pointsView ul {
    list-style: disc !important;
  }

  .pointsView a {
    color: #ff8100 !important;
  }


  .main-sidebar,
  .left-side {
    min-height: 222%;
  }

  .content-wrapper {
    padding-left: 1%;
    min-height: 1495px !important;
  }
</style>
<div class="content-wrapper">
  <div class="col-sm-3 prfileBox3">
    <!--left col-->
    <div class="text-center"> <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar img-circle img-thumbnail" alt="avatar" style="width: 40%;">
      <?php $fullName = $name . ' ' . $last_name; ?>
      <input type="hidden" name="student_name_submit" id="student_name_submit" value="<?= $fullName ?>">
      <h4 style="color: white"><?= $name ?> <?= $last_name ?></h4>
    </div>
    </hr>
    <br>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h5 class="margin2">Basic Details &nbsp;<i class="far fa-address-card"></i></h5>
      </div>
      <div class="panel-body">
        <p class="ptag"><b class="purple">Email id : </b>
          <?= $email ?>
          <br><b class="purple">Ph. No. : </b>
          <?= $phone ?>
        </p>
      </div>
    </div>
    <div class="wrapper" style="padding: 1pc;">
      <div class="panel-heading" style="margin-bottom: 5px;">
        <h5 class="margin2">Application Status</h5>
      </div>
      <ul class="StepProgress">

        <?php

        $leadStateClass = "";
        $leadStateClass2 = "";

        foreach ($lead_state_list as $keyls => $valuels) {

          $leadStateClass = $valuels['COMPLETED'] == 1 ? "is-done" : "";
          $leadStateClass2 = $valuels['name'] == $current_lead_state ? "is-done" : "";
        ?>

          <li class="StepProgress-item<?php echo " $leadStateClass $leadStateClass2"; ?>"><strong> <?php echo $valuels['display_name']; ?> </strong></li>

        <?php
        }
        ?>
      </ul>

    </div>
    <!-- <div class="wrapper " style="padding:1pc;">
      <ul class="StepProgress">
        <li class="StepProgress-item is-done"><strong>one</strong></li>
          <li class="StepProgress-item is-done"><strong>Two</strong>
          </li>
          <li class="StepProgress-item current"><strong>Three</strong></li>
          <li class="StepProgress-item"><strong>Four</strong></li>
          <li class="StepProgress-item"><strong>Five</strong></li>
      </ul>
       <ul class="StepProgress">

        <?php

        $leadStateClass = "";
        $leadStateClass2 = "";

        foreach ($lead_state_list as $keyls => $valuels) {

          $leadStateClass = $valuels['COMPLETED'] == 1 ? "is-done" : "";
          $leadStateClass2 = $valuels['name'] == $current_lead_state ? "is-done" : "";
        ?>

        <li class="StepProgress-item<?php echo " $leadStateClass $leadStateClass2"; ?>"><strong> <?php echo $valuels['display_name']; ?> </strong></li>        

        <?php
        }
        ?>
      </ul> 
    </div>-->
    <div class="accordianContainer">
      <button class="accordion">Activity &nbsp;<i class="fa fa-dashboard fa-2x"></i></button>
      <div class="panel">
        <ul class="list-group">
          <li class="list-group-item text-right"><span class="pull-left"><strong class="purple">Assigned To</strong></span> Banit Sawhney</li>
          <li class="list-group-item text-right"><span class="pull-left"><strong class="purple">Intake</strong></span>
            <?= $intake ?>&nbsp;</li>
          <li class="list-group-item text-right"><span class="pull-left"><strong class="purple">City</strong></span> Mumbai</li>
          <!-- <li class="list-group-item text-right"><span class="pull-left"><strong class="purple"> <a href="/counsellor/student/comments/view/<?= $id; ?>" target="_blank" style="color: #f6882c"> Comment History </a> </strong></span>
            <a href="/counsellor/student/offerlist/view/<?= $id; ?>" style="color: #f6882c"> <b>Shortlist Universities / Courses</b> </a>
          </li> -->
        </ul>
      </div>

      <button class="accordion">Application Process &nbsp;<i class="fa fa fa-address-card-o fa-2x"></i></button>
      <div class="panel">
        <ul class="list-group">
          <li class="list-group-item text-justify" style="max-height: 195px;overflow: auto;">
            <div class="text-justify pointsView ">
              <?php if ($university_Application_info) {
                echo $university_Application_info['process_description'];
              } ?>
            </div>
          </li>
        </ul>
      </div>

      <button class="accordion">Download Document &nbsp;<i class="fa fa-cloud-download fa-2x"></i></button>
      <div class="panel">
        <ul class="list-group">
          <li class="list-group-item text-right"><span class="pull-left"><strong class="purple">Document 1</strong></span> <a href="<?php if ($university_Application_info) {
                                                                                                                                      echo $university_Application_info['checklist_url'];
                                                                                                                                    } ?>" download="checklist_doc" style="text-align: center; border-radius:5px;" class="btn btn-sm btn-primary">Download</a></li>
          <li class="list-group-item text-right"><span class="pull-left"><strong class="purple">Consent Form</strong></span> <a href="<?php if ($university_Application_info) {
                                                                                                                                        echo $university_Application_info['consentform_url'];
                                                                                                                                      } ?>" download="consent_form" style="text-align: center; border-radius:5px;" class="btn btn-sm btn-primary">Download</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="col-md-9">
    <div class="col-sm-12 appListTable">
      <div class="box">
        <div class="box-header bg-aqua">
          <h3 class="box-title">University Application List</h3>
        </div>
        <div class="box-body table-responsive no-padding boxAppList" style="padding: 2% 2%;background-color: white;">
          <table class="table table-bordered table-striped table-hover" show-filter="true">
            <thead>
              <tr>
                <th title="" class="header sortable"> University Name</th>
                <th title="" class="header sortable"> Course Name</th>
                <th title="" class="header sortable"> Status</th>
                <th title="" class="header sortable"> Application Type</th>
                <th title="" class="header sortable"> Date Created</th>
                <th title="" class="header sortable"> Comments</th>
                <th title="" class="header sortable"> Assigned to</th>

                <th title="" class="header sortable"> Submit Application</th>
                <th title="" class="header sortable"> Edit Application Type</th>
                <th title="" class="header sortable"> Edit Status</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if ($myApplications) {
                $i = 1;
                foreach ($myApplications as $myApplication) {
              ?>
                  <tr>
                    <td><a style="color:#3a66ad" href='/admin/student/profile/<?php echo $myApplication['user_id']; ?>/<?php echo $myApplication['id']; ?>'> <?php echo $myApplication['name']; ?> </a></td>
                    <td>
                      <?php echo $myApplication['university_application_course']; ?>
                    </td>
                    <td>
                      <?php echo $myApplication['status']; ?>
                    </td>
                    <td>
                      <?php if ($myApplication['application_fill_option'] == 1) {
                        echo "Imperial Will Fill Application";
                      } else {
                        echo "Imperial Will Check Application";
                      } ?>
                    </td>
                    <td>
                      <?php echo $myApplication['date_created']; ?>
                    </td>
                    <td> <a class="btn btn-info btn-sm" href="/admin/counsellor/application/comments/view/<?php echo $myApplication['user_id']; ?>/<?php echo $myApplication['id']; ?>" target="_blank"> Comment </a> </strong></span> </td>
                    <td class="iconOne">
                      <?php if ($myApplication['facilitator_name']) { ?>
                        <?php echo $myApplication['facilitator_name']; ?>
                        <?php echo $myApplication['facilitator_last_name']; ?><br />
                        <i class="fa fa-phone" title="<?php echo $myApplication['facilitator_mobile']; ?>"></i><br />
                        <i class="fa fa-envelope" title="<?php echo $myApplication['facilitator_email']; ?>"></i>
                      <?php } else {
                        echo "Not Assigned";
                      } ?>
                    </td>
                    <td><button class="btn btn-info btn-sm" onclick="submitApplication(<?php echo $myApplication['id']; ?>,<?= $id ?>)"> Submit </button></td>
                    <td><button class="btn btn-info btn-sm" onclick="editUniversityApplication(<?php echo $myApplication['id']; ?>,<?php echo $myApplication['university_id']; ?>)"> Edit </button></td>
                    <td><button class="btn btn-info btn-sm" onclick="editStatusApplication(<?php echo $myApplication['id']; ?>)"> Edit </button></td>
                  </tr>
              <?php
                }
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-sm-12 appListTable">
      <div class="box">
        <div class="box-header bg-aqua">
          <h3 class="box-title"> Selected Application - <?= $selected_app_id ?> ( <?= $application_university_name ?> )</h3>
        </div>
      </div>
    </div>
    <div class="col-md-12 firtsTabs" id="checkDiv" style="margin-top: 20px;">
      <div class="tab firstTab col-md-12" style="display: flex;">
        <button id="defaultOpenFirstTab" class="btn btn-lg btntab tabsublinks active" onclick="openFirstTab(event, 'mandatory')"><b>Mandatory Requirements</b></button>
        <button class="btn btn-lg btntab tabsublinks" onclick="openFirstTab(event, 'optional')"><b>Optional Requirements</b></button>
      </div>
      <div class="tab-content">
        <div id="mandatory" class="tabsubcontent">
          <div class="col-sm-12 mytabs">
            <button class="tablink" onclick="openPage('Profile', this, '#f7f7f7')" id="defaultOpen">Profile</button>
            <button class="tablink" onclick="openPage('Documents', this, '#f7f7f7')" id="documentTab">Documents</button>
            <!--<button class="tablink" onclick="openPage('Applications', this, '#f7f7f7')" id="applicationsTab">Applications (
      <?php if ($myApplications) {
        echo count($myApplications);
      } else {
        echo count($myApplications);
      } ?> ) </button>-->
            <button class="tablink" onclick="openPage('Payments', this, '#f7f7f7')" id="paymentsTab">Payments</button>
            <div id="Profile" class="tabcontent">
              <!-- tabs inside -->
              <div class="col-md-12 boxAppList" style="padding: 2% 2%;    background-color: white;">
                <ul class="nav nav-tabs" style="padding-top: 9px;padding-left: 9px;">
                  <li class="active"><a data-toggle="tab" href="#home"><b class="tabName">
                        Personal Detail
                        <?php if ($personal_detail_status == "COMPLETE") {
                          echo "(COMPLETED)";
                        } else {
                          echo "(INCOMPLETED)";
                        } ?>
                      </b></a></li>
                  <li><a data-toggle="tab" href="#qualification_detail"><b class="tabName">
                        Qualification Detail <?php if ($qualified_exams->competitive_exam_type != '' && $qualified_exams->english_exam_type != '') {
                                                echo "(COMPLETED)";
                                              } else {
                                                echo "(INCOMPLETED)";
                                              } ?>
                      </b></a></li>
                  <li><a data-toggle="tab" href="#desired_courses"><b class="tabName">
                        Desired Courses <?php if ($desiredCourseProfile == "COMPLETE") {
                                          echo "(COMPLETED)";
                                        } else {
                                          echo "(INCOMPLETED)";
                                        } ?>
                      </b></a></li>
                  <li><a data-toggle="tab" href="#parent_detail"><b class="tabName">
                        Parent Detail <?php if ($parent_detail_status == "COMPLETE") {
                                        echo "(COMPLETED)";
                                      } else {
                                        echo "(INCOMPLETED)";
                                      } ?>
                      </b></a></li>
                  <li><a data-toggle="tab" href="#work_exp"><b class="tabName">
                        Work Experience <?php if ($work_exp_status == "COMPLETE") {
                                          echo "(COMPLETED)";
                                        } else {
                                          echo "(INCOMPLETED)";
                                        } ?>
                      </b></a></li>
                  <li><a data-toggle="tab" href="#emergency_contact_detail"><b class="tabName">
                        Emergency Contact details <?php if ($emergency_contact_detail_status == "COMPLETE") {
                                                    echo "(COMPLETED)";
                                                  } else {
                                                    echo "(INCOMPLETED)";
                                                  } ?>
                      </b></a></li>
                  <li><a data-toggle="tab" href="#fund_details"><b class="tabName">Fund Details <?php if ($fund_detail_status == "COMPLETE") {
                                                                                                  echo "(COMPLETED)";
                                                                                                } else {
                                                                                                  echo "(INCOMPLETED)";
                                                                                                } ?></b></a></li>
                  <!-- <li><a data-toggle="tab" href="#document_upload"><b class="tabName">Upload Document</b></a></li> -->
                  <!-- <li><a data-toggle="tab" href="#password_change"><b class="tabName">Change Password</b></a></li> -->
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="home">
                    <hr>
                    <form class="form" action="/admin/student_registration/account/save" method="post" id="personal-detail-form">
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="first_name">
                            <h5>First Name (as per passport)</h5>
                          </label>
                          <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?= $name ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="first_name">
                            <h5>Last Name (as per passport)</h5>
                          </label>
                          <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Name" value="<?= $last_name ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="phone">
                            <h5>Mobile</h5>
                          </label>
                          <input type="text" class="form-control" name="phone" id="student_phone" placeholder="Mobile" value="<?= $phone ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="email">
                            <h5>Email</h5>
                          </label>
                          <input type="email" class="form-control" name="email" id="student_email" placeholder="Email" value="<?= $email ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="dob">
                            <h5>DOB as per passport</h5>
                          </label>
                          <input type="date" name="dob" class="form-control" id="dob" value="<?= $dob ?>" />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="address">
                            <h5>Place of Birth</h5>
                          </label>
                          <input type="text" class="form-control" name="place_of_birth" id="place_of_birth" placeholder="Place of birth" value="<?= $place_of_birth ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="country">
                            <h5>Country</h5>
                          </label>
                          <select name="locationcountries" id="student_locationcountries" class="form-control">
                            <option value="0">Select Country</option>
                            <?php
                            foreach ($locationcountries as $location) {
                            ?>
                              <option value="<?= $location->id ?>" <?php if ($location->id == $selected_country) echo 'selected="selected"'; ?>>
                                <?= $location->name ?>
                              </option>
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="state">
                            <h5>State</h5>
                          </label>
                          <select name="state" id="student_state" class="form-control">
                            <option value="0">Select State</option>
                            <?php
                            foreach ($related_state_list as $location) {
                            ?>
                              <option value="<?= $location->zone_id ?>" <?php if ($location->zone_id == $selected_state) echo 'selected="selected"'; ?>>
                                <?= $location->name ?>
                              </option>
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="city">
                            <h5>City</h5>
                          </label>
                          <select name="city" id="student_city" class="form-control">
                            <option value="0">Select City</option>
                            <?php
                            foreach ($related_city_list as $location) {
                            ?>
                              <option value="<?= $location->id ?>" <?php if ($location->id == $city) echo 'selected="selected"'; ?>>
                                <?= $location->city_name ?>
                              </option>
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="zipcode">
                            <h5>Zipcode</h5>
                          </label>
                          <input type="text" class="form-control" name="zip" id="student_zip" placeholder="Zipcode" value="<?= $zip ?>">
                        </div>
                      </div>
                      <!--<div class="form-group">
      <div class="col-xs-6">
        <label for="hear_about_us">
          <h5>Where Did You Hear About Us?</h5> </label>
        <select name="hear_info" id="student_hear_info" class="form-control">
          <option value="0">Select Option</option>
          <option value="Friends / Family" <?php if (isset($hear_about_us) && $hear_about_us == 'Friends / Family') echo 'selected="selected"'; ?> >Friends / Family</option>
          <option value="Google / Facebook" <?php if (isset($hear_about_us) && $hear_about_us == 'Google / Facebook') echo 'selected="selected"'; ?>>Google / Facebook</option>
          <option value="Stupidsid Seminar" <?php if (isset($hear_about_us) && $hear_about_us == 'Stupidsid Seminar') echo 'selected="selected"'; ?>>Stupidsid Seminar</option>
          <option value="Stupidsid Website" <?php if (isset($hear_about_us) && $hear_about_us == 'Stupidsid Website') echo 'selected="selected"'; ?>>Stupidsid Website</option>
          <option value="College Seminar" <?php if (isset($hear_about_us) && $hear_about_us == 'College Seminar') echo 'selected="selected"'; ?>>College Seminar</option>
          <option value="News Paper" <?php if (isset($hear_about_us) && $hear_about_us == 'News Paper') echo 'selected="selected"'; ?>>News Paper</option>
          <option value="Calling from Imperial" <?php if (isset($hear_about_us) && $hear_about_us == 'Calling from Imperial') echo 'selected="selected"'; ?>>Calling from Imperial</option>
          <option value="Shiksha" <?php if (isset($hear_about_us) && $hear_about_us == 'Shiksha') echo 'selected="selected"'; ?>>Shiksha</option>
        </select>
      </div>
      </div>-->
                      <div class="form-group">
                        <div class="col-xs-6" style="    margin: 10px 0px;">
                          <label for="gender">
                            <h5>Gender</h5>
                          </label>
                          <div class="col-md-12">
                            <label class="radio-inline">
                              <input type="radio" name="gender" value="M" <?= $gender == 'M' ? 'checked' : '' ?>>Male </label>
                            <label class="radio-inline">
                              <input type="radio" name="gender" value="F" <?= $gender == 'F' ? 'checked' : '' ?>>Female </label>
                            <label class="radio-inline">
                              <input type="radio" name="gender" value="O" <?= $gender == 'O' ? 'checked' : '' ?>>Other </label>
                          </div>
                        </div>
                      </div>
                      <!--<div class="form-group">
      <div class="col-xs-6">
        <label for="passport_availalibility">
          <h5>Do you have passport?</h5> </label>
        <div class="col-md-12">
          <label class="radio-inline">
            <input type="radio" name="passport_availalibility" value="1" id="chk-Yes" <?= $passport == '1' ? 'checked' : '' ?> />Yes </label>
          <label class="radio-inline">
            <input type="radio" name="passport_availalibility" value="0" id="chk-No" <?= $passport == '0' ? 'checked' : '' ?> />No </label>
        </div>
      </div>
      </div>-->
                      <div id="passport_group" style="display: <?= $passport == '0' ? 'block' : 'block' ?>">
                        <div class="form-group">
                          <div class="col-xs-6">
                            <label for="passport_availalibility">
                              <h5>Passport Number</h5>
                            </label>
                            <input type="text" class="form-control" name="passport_number" id="passport_number" placeholder="Passport Number" value="<?= $passport_number ?>">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-xs-6">
                            <label for="passport_availalibility">
                              <h5>Passport Issue Date</h5>
                            </label>
                            <input type="date" name="passport_issue_date" class="form-control" id="passport_issue_date" value="<?= $passport_issue_date ?>" />
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-xs-6">
                            <label for="passport_availalibility">
                              <h5>Passport Expiry Date</h5>
                            </label>
                            <input type="date" name="passport_expiry_date" class="form-control" id="passport_expiry_date" value="<?= $passport_expiry_date ?>" />
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-12">
                          <br>
                          <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                          <button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                        </div>
                      </div>
                      <input type="hidden" name="form_type" value="personal_details" />
                      <input type="hidden" name="user_id" value="<?= $id ?>" />
                      <input type="hidden" name="student_id" value="<?= $student_id ?>" />
                      <input type="hidden" name="app_id" value="<?= $selected_app_id ?>" />
                    </form>
                    <hr>
                  </div>
                  <!--/tab-pane-->
                  <div class="tab-pane" id="qualification_detail">
                    <hr>
                    <form class="form" action="/admin/student_registration/account/save" method="post" id="qualification-detail-form" onsubmit="return validate()">
                      <div class="form-group" id="competitive_exam">
                        <div class="col-xs-12">
                          <label for="competitive_exam">
                            <h5>Have you appeared any of the following exam?</h5>
                          </label>
                          <div class="col-md-12">
                            <label class="radio-inline">
                              <input type="radio" name="competitive_exam" <?php if ($qualified_exams && $qualified_exams->competitive_exam_type == 'GRE') echo 'checked="checked"'; ?> id="gre" value="GRE">GRE </label>
                            <label class="radio-inline">
                              <input type="radio" name="competitive_exam" <?php if ($qualified_exams && $qualified_exams->competitive_exam_type == 'GMAT') echo 'checked="checked"'; ?> id="gmat" value="GMAT">GMAT </label>
                            <label class="radio-inline">
                              <input type="radio" name="competitive_exam" <?php if ($qualified_exams && $qualified_exams->competitive_exam_type == 'SAT') echo 'checked="checked"'; ?> id="sat" value="SAT">SAT </label>
                            <label class="radio-inline">
                              <input type="radio" name="competitive_exam" <?php if ($qualified_exams && $qualified_exams->competitive_exam_type == 'NO') echo 'checked="checked"'; ?> id="no_competitive_exam" value="NO">NO </label>
                            <label class="radio-inline">
                              <input type="radio" name="competitive_exam" <?php if ($qualified_exams && $qualified_exams->competitive_exam_type == 'NOT_REQUIRED') echo 'checked="checked"'; ?> id="not_required_competitive_exam" value="NOT_REQUIRED">NOT_REQUIRED </label>
                          </div>
                        </div> <span class="sep col-lg-12"></span>
                      </div>
                      <div class="form-group" id="exam_stage" <?php if ($qualified_exams && $qualified_exams->competitive_exam_stage['name']) echo 'style=display:block;' ?>>
                        <div class="col-xs-12">
                          <label for="exam_stage">
                            <h5>Which stage of the exam have you reached?</h5>
                          </label>
                          <div class="col-md-12">
                            <label class="radio-inline">
                              <input type="radio" name="exam_stage" <?php if ($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'REGISTERED') echo 'checked="checked"'; ?> id="registered" value="REGISTERED">Registered </label>
                            <label class="radio-inline">
                              <input type="radio" name="exam_stage" <?php if ($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_WAIT') echo 'checked="checked"'; ?> id="result_wait" value="RESULT_WAIT">Waiting For Result </label>
                            <label class="radio-inline">
                              <input type="radio" name="exam_stage" <?php if ($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT') echo 'checked="checked"'; ?> id="result_out" value="RESULT_OUT">Have Result </label>
                          </div>
                        </div> <span class="sep col-lg-12"></span>
                      </div>
                      <div class="form-group" id="exam_date" <?php if ($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'REGISTERED') echo 'style=display:block;' ?>>
                        <div class="col-xs-12">
                          <label for="exam_date">
                            <h5>Exam Date</h5>
                          </label>
                          <input type="date" name="exam_date" class="form-control" value="<?php if ($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'REGISTERED') echo $qualified_exams->competitive_exam_stage['value'] ?>" />
                        </div> <span class="sep col-lg-12"></span>
                      </div>
                      <div class="form-group" id="result_date" <?php if ($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_WAIT') echo 'style=display:block;' ?>>
                        <div class="col-xs-12">
                          <label for="result_date">
                            <h5>Expected Result Date</h5>
                          </label>
                          <input type="date" name="result_date" class="form-control" value="<?php if ($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_WAIT') echo $qualified_exams->competitive_exam_stage['value'] ?>" />
                        </div> <span class="sep col-lg-12"></span>
                      </div>
                      <div class="form-group" id="exam_score" <?php if ($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT') echo 'style=display:block;' ?>>
                        <div class="col-xs-12" id="exam_score_gre" <?php if ($qualified_exams && $qualified_exams->competitive_exam_type == 'GRE' && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT') echo 'style=display:block;' ?>>
                          <div class="col-xs-3">
                            <label for="exam_registration_gre">
                              <h5>Registration No.</h5>
                            </label>
                            <input name="exam_registration_gre" class="form-control" type="text" value="<?php if ($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_registration_gre'])) echo $qualified_exams->competitive_exam_stage['exam_registration_gre'] ?>">
                          </div>
                          <div class="col-xs-2">
                            <label for="exam_score_quant_gre">
                              <h5>Record Date</h5>
                            </label>
                            <input name="record_date_quant_gre" class="form-control" type="date" value="<?php if ($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['record_date_quant_gre'])) echo $qualified_exams->competitive_exam_stage['record_date_quant_gre'] ?>">
                          </div>
                          <div class="col-xs-3">
                            <label for="exam_score_quant_gre">
                              <h5>Quant</h5>
                            </label>
                            <select class="form-control" id="exam_score_quant_gre" name="exam_score_quant_gre">
                              <option>Select</option>
                              <?php for ($i = 130; $i < 171; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->competitive_exam_stage['exam_score_quant_gre'] == $i) {
                                                                    echo "selected";
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-3">
                            <label for="exam_score_verbal_gre">
                              <h5>Verbal</h5>
                            </label>
                            <select class="form-control" id="exam_score_verbal_gre" name="exam_score_verbal_gre">
                              <option>Select</option>
                              <?php for ($i = 130; $i < 171; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->competitive_exam_stage['exam_score_verbal_gre'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-3">
                            <label for="exam_score_awa_gre">
                              <h5>AWA</h5>
                            </label>
                            <select class="form-control" id="exam_score_awa_gre" name="exam_score_awa_gre">
                              <option>Select</option>
                              <?php for ($i = 0; $i <= 6; $i = $i + 0.5) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->competitive_exam_stage['exam_score_awa_gre'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-2">
                            <label for="exam_score_total_gre">
                              <h5>Total</h5>
                            </label>
                            <select class="form-control" id="exam_score_total_gre" name="exam_score_total_gre">
                              <option>Select</option>
                              <?php for ($i = 200; $i < 800; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->competitive_exam_stage['exam_score_total_gre'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-xs-12" id="exam_score_gmat" <?php if ($qualified_exams && $qualified_exams->competitive_exam_type == 'GMAT' && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT') echo 'style=display:block;' ?>>
                          <div class="col-xs-3">
                            <label for="exam_registration_gmat">
                              <h5>Registration No.</h5>
                            </label>
                            <input name="exam_registration_gmat" class="form-control" type="text" value="<?php if ($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_registration_gmat'])) echo $qualified_exams->competitive_exam_stage['exam_registration_gmat'] ?>">
                          </div>

                          <div class="col-xs-3">
                            <label for="exam_score_quant_gre">
                              <h5>Record Date</h5>
                            </label>
                            <input name="record_date_quant_gmat" class="form-control" type="date" value="<?php if ($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['record_date_quant_gmat'])) echo $qualified_exams->competitive_exam_stage['record_date_quant_gmat'] ?>">
                          </div>
                          <div class="col-xs-2">
                            <label for="exam_score_quant_gmat">
                              <h5>Quant</h5>
                            </label>
                            <select class="form-control" id="exam_score_quant_gmat" name="exam_score_quant_gmat">
                              <option>Select</option>
                              <?php for ($i = 6; $i <= 51; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->competitive_exam_stage['exam_score_quant_gmat'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-2">
                            <label for="exam_score_verbal_gmat">
                              <h5>Verbal</h5>
                            </label>
                            <select class="form-control" id="exam_score_verbal_gmat" name="exam_score_verbal_gmat">
                              <option>Select</option>
                              <?php for ($i = 6; $i <= 51; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->competitive_exam_stage['exam_score_verbal_gmat'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-2">
                            <label for="exam_score_ir">
                              <h5>IR</h5>
                            </label>
                            <select class="form-control" id="exam_score_ir" name="exam_score_ir">
                              <option>Select</option>
                              <?php for ($i = 1; $i <= 8; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->competitive_exam_stage['exam_score_ir'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-2">
                            <label for="exam_score_awa_gmat">
                              <h5>AWA</h5>
                            </label>
                            <select class="form-control" id="exam_score_awa_gmat" name="exam_score_awa_gmat">
                              <option>Select</option>
                              <?php for ($i = 0; $i <= 6; $i = $i + 0.5) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->competitive_exam_stage['exam_score_awa_gmat'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-3">
                            <label for="exam_score_total_gmat">
                              <h5>Total</h5>
                            </label>
                            <select class="form-control" id="exam_score_total_gmat" name="exam_score_total_gmat">
                              <option>Select</option>
                              <?php for ($i = 200; $i <= 800; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->competitive_exam_stage['exam_score_total_gmat'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-xs-12" id="exam_score_sat" <?php if ($qualified_exams && $qualified_exams->competitive_exam_type == 'SAT' && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT') echo 'style=display:block;' ?>>
                          <div class="col-xs-3">
                            <label for="exam_score_quant_gre">
                              <h5>Record Date</h5>
                            </label>
                            <input name="record_date_quant_sat" class="form-control" type="date" value="<?php if ($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['record_date_quant_sat'])) echo $qualified_exams->competitive_exam_stage['record_date_quant_sat'] ?>">
                          </div>
                          <div class="col-xs-3">
                            <label for="exam_score_cirital_reading">
                              <h5>Critical Reading</h5>
                            </label>
                            <select class="form-control" id="exam_score_cirital_reading" name="exam_score_cirital_reading">
                              <option>Select</option>
                              <?php for ($i = 200; $i <= 800; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->competitive_exam_stage['exam_score_cirital_reading'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-3">
                            <label for="exam_score_writing">
                              <h5>Writing</h5>
                            </label>
                            <select class="form-control" id="exam_score_writing" name="exam_score_writing">
                              <option>Select</option>
                              <?php for ($i = 200; $i <= 800; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->competitive_exam_stage['exam_score_writing'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-3">
                            <label for="exam_score_math">
                              <h5>Math</h5>
                            </label>
                            <select class="form-control" id="exam_score_math" name="exam_score_math">
                              <option>Select</option>
                              <?php for ($i = 200; $i <= 800; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->competitive_exam_stage['exam_score_math'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-3">
                            <label for="exam_score_total_sat">
                              <h5>Total</h5>
                            </label>
                            <select class="form-control" id="exam_score_total_sat" name="exam_score_total_sat">
                              <option>Select</option>
                              <?php for ($i = 400; $i <= 1600; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->competitive_exam_stage['exam_score_total_sat'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div> <span class="sep col-lg-12"></span>
                      </div>
                      <div class="form-group" id="english_proficiency_exam">
                        <div class="col-xs-12">
                          <label for="english_proficiency_exam">
                            <h5>Have you appeared any of the following exam?</h5>
                          </label>
                          <div class="col-md-12">
                            <label class="radio-inline">
                              <input type="radio" name="english_proficiency_exam" <?php if ($qualified_exams && $qualified_exams->english_exam_type == 'TOEFL') echo 'checked="checked"'; ?> id="toefl" value="TOEFL">TOEFL </label>
                            <label class="radio-inline">
                              <input type="radio" name="english_proficiency_exam" <?php if ($qualified_exams && $qualified_exams->english_exam_type == 'IELTS') echo 'checked="checked"'; ?> id="ielts" value="IELTS">IELTS </label>
                            <label class="radio-inline">
                              <input type="radio" name="english_proficiency_exam" <?php if ($qualified_exams && $qualified_exams->english_exam_type == 'PTE') echo 'checked="checked"'; ?> id="pte" value="PTE">PTE </label>
                            <label class="radio-inline">
                              <input type="radio" name="english_proficiency_exam" <?php if ($qualified_exams && $qualified_exams->english_exam_type == 'DUOLINGO') echo 'checked="checked"'; ?> id="duolingo" value="DUOLINGO">DUOLINGO </label>
                            <label class="radio-inline">
                              <input type="radio" name="english_proficiency_exam" <?php if ($qualified_exams && $qualified_exams->english_exam_type == 'NO') echo 'checked="checked"'; ?> id="no_english_proficiency_exam" value="NO">NO </label>
                            <label class="radio-inline">
                              <input type="radio" name="english_proficiency_exam" <?php if ($qualified_exams && $qualified_exams->english_exam_type == 'NOT_REQUIRED') echo 'checked="checked"'; ?> id="not_required_english_proficiency_exam" value="NOT_REQUIRED">NOT_REQUIRED </label>
                          </div>
                        </div> <span class="sep col-lg-12"></span>
                      </div>
                      <div class="form-group" id="english_exam_stage" <?php if ($qualified_exams && $qualified_exams->english_exam_stage['name']) echo 'style=display:block;' ?>>
                        <div class="col-xs-12">
                          <label for="english_exam_stage">
                            <h5>Which stage of the exam have you reached?</h5>
                          </label>
                          <div class="col-md-12">
                            <label class="radio-inline">
                              <input type="radio" name="english_exam_stage" <?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'REGISTERED') echo 'checked="checked"'; ?> id="english_registered" value="REGISTERED">Registered </label>
                            <label class="radio-inline">
                              <input type="radio" name="english_exam_stage" <?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_WAIT') echo 'checked="checked"'; ?> id="english_result_wait" value="RESULT_WAIT">Waiting For Result </label>
                            <label class="radio-inline">
                              <input type="radio" name="english_exam_stage" <?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT') echo 'checked="checked"'; ?> id="english_result_out" value="RESULT_OUT">Have Result </label>
                          </div>
                        </div> <span class="sep col-lg-12"></span>
                      </div>
                      <div class="form-group" id="english_exam_date" <?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'REGISTERED') echo 'style=display:block;' ?>>
                        <div class="col-xs-12">
                          <label for="english_exam_date">
                            <h5>Exam Date</h5>
                          </label>
                          <input type="date" name="english_exam_date" class="form-control" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'REGISTERED') echo $qualified_exams->english_exam_stage['value'] ?>" />
                        </div> <span class="sep col-lg-12"></span>
                      </div>
                      <div class="form-group" id="english_result_date" <?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_WAIT') echo 'style=display:block;' ?>>
                        <div class="col-xs-12">
                          <label for="english_result_date">
                            <h5>Expected Result Date</h5>
                          </label>
                          <input type="date" name="english_result_date" class="form-control" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_WAIT') echo $qualified_exams->english_exam_stage['value'] ?>" />
                        </div> <span class="sep col-lg-12"></span>
                      </div>

                      <div class="form-group" id="english_exam_score" <?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT') echo 'style=display:block;' ?>>

                        <div class="col-xs-12" id="english_exam_tofel" <?php if ($qualified_exams && $qualified_exams->english_exam_type == 'TOEFL' && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT') echo 'style=display:block;' ?>>

                          <div class="col-xs-3">
                            <label for="english_exam_appoint_no_toefl">
                              <h5>Appointment No.</h5>
                            </label>
                            <input name="english_exam_appoint_no_toefl" class="form-control" type="text" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->english_exam_stage['english_exam_appoint_no_toefl'])) echo $qualified_exams->english_exam_stage['english_exam_appoint_no_toefl'] ?>">
                          </div>

                          <div class="col-xs-3">
                            <label for="english_exam_score_reading">
                              <h5>Record Date</h5>
                            </label>
                            <input name="english_exam_record_date_toefl" class="form-control" type="date" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->english_exam_stage['english_exam_record_date_toefl'])) echo $qualified_exams->english_exam_stage['english_exam_record_date_toefl'] ?>">
                          </div>

                          <div class="col-xs-2">
                            <label for="english_exam_score_reading_toefl">
                              <h5>Reading</h5>
                            </label>
                            <!-- <input name="english_exam_score_reading" class="form-control" type="text" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_reading']) echo $qualified_exams->english_exam_stage['english_exam_score_reading'] ?>"> -->
                            <select class="form-control" id="english_exam_score_reading_toefl" name="english_exam_score_reading_toefl">
                              <option>Select</option>

                              <?php for ($i = 1; $i <= 30; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->english_exam_stage['english_exam_score_reading_toefl'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>


                            </select>

                          </div>
                          <div class="col-xs-2">
                            <label for="english_exam_score_listening_toefl">
                              <h5>Listening</h5>
                            </label>
                            <!-- <input name="english_exam_score_listening" class="form-control" type="text" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_listening']) echo $qualified_exams->english_exam_stage['english_exam_score_listening'] ?>"> -->
                            <select class="form-control" id="english_exam_score_listening_toefl" name="english_exam_score_listening_toefl">
                              <option>Select</option>
                              <?php for ($i = 1; $i <= 30; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->english_exam_stage['english_exam_score_listening_toefl'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-2">
                            <label for="english_exam_score_writing_toefl">
                              <h5>Writing</h5>
                            </label>
                            <!-- <input name="english_exam_score_writing" class="form-control" type="text" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_writing']) echo $qualified_exams->english_exam_stage['english_exam_score_writing'] ?>"> -->
                            <select class="form-control" id="english_exam_score_writing_toefl" name="english_exam_score_writing_toefl">
                              <option>Select</option>
                              <?php for ($i = 1; $i <= 30; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->english_exam_stage['english_exam_score_writing_toefl'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-2">
                            <label for="english_exam_score_speaking_toefl">
                              <h5>Speaking</h5>
                            </label>
                            <!-- <input name="english_exam_score_speaking" class="form-control" type="text" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_speaking']) echo $qualified_exams->english_exam_stage['english_exam_score_speaking'] ?>"> -->
                            <select class="form-control" id="english_exam_score_speaking_toefl" name="english_exam_score_speaking_toefl">
                              <option>Select</option>
                              <?php for ($i = 1; $i <= 30; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->english_exam_stage['english_exam_score_speaking_toefl'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-3">
                            <label for="english_exam_score_total_toefl">
                              <h5>Total</h5>
                            </label>
                            <!-- <input name="english_exam_score_total" class="form-control" type="text" id="english_exam_score_total" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_total']) echo $qualified_exams->english_exam_stage['english_exam_score_total'] ?>"> -->
                            <select class="form-control" id="english_exam_score_total_toefl" name="english_exam_score_total_toefl">
                              <option>Select</option>
                              <?php for ($i = 1; $i <= 120; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->english_exam_stage['english_exam_score_total_toefl'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>

                        <div class="col-xs-12" id="english_exam_ielts" <?php if ($qualified_exams && $qualified_exams->english_exam_type == 'IELTS' && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT') echo 'style=display:block;' ?>>

                          <div class="col-xs-3">
                            <label for="english_exam_trf_no_ielts">
                              <h5>TRF No.</h5>
                            </label>
                            <input name="english_exam_trf_no_ielts" class="form-control" type="text" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->english_exam_stage['english_exam_trf_no_ielts'])) echo $qualified_exams->english_exam_stage['english_exam_trf_no_ielts'] ?>">
                          </div>

                          <div class="col-xs-3">
                            <label for="english_exam_score_reading_ielts">
                              <h5>Record Date</h5>
                            </label>
                            <input name="english_exam_record_date_ielts" class="form-control" type="date" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->english_exam_stage['english_exam_record_date_ielts'])) echo $qualified_exams->english_exam_stage['english_exam_record_date_ielts'] ?>">
                          </div>

                          <div class="col-xs-2">
                            <label for="english_exam_score_reading_ielts">
                              <h5>Reading</h5>
                            </label>
                            <!-- <input name="english_exam_score_reading" class="form-control" type="text" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_reading']) echo $qualified_exams->english_exam_stage['english_exam_score_reading'] ?>"> -->
                            <select class="form-control" id="english_exam_score_reading_ielts" name="english_exam_score_reading_ielts">
                              <option>Select</option>

                              <?php for ($i = 0.5; $i <= 9; $i = $i + 0.5) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->english_exam_stage['english_exam_score_reading_ielts'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>


                            </select>

                          </div>
                          <div class="col-xs-2">
                            <label for="english_exam_score_listening_ielts">
                              <h5>Listening</h5>
                            </label>
                            <!-- <input name="english_exam_score_listening" class="form-control" type="text" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_listening']) echo $qualified_exams->english_exam_stage['english_exam_score_listening'] ?>"> -->
                            <select class="form-control" id="english_exam_score_listening_ielts" name="english_exam_score_listening_ielts">
                              <option>Select</option>
                              <?php for ($i = 0.5; $i <= 9; $i = $i + 0.5) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->english_exam_stage['english_exam_score_listening_ielts'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-2">
                            <label for="english_exam_score_writing_ielts">
                              <h5>Writing</h5>
                            </label>
                            <!-- <input name="english_exam_score_writing" class="form-control" type="text" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_writing']) echo $qualified_exams->english_exam_stage['english_exam_score_writing'] ?>"> -->
                            <select class="form-control" id="english_exam_score_writing_ielts" name="english_exam_score_writing_ielts">
                              <option>Select</option>
                              <?php for ($i = 0.5; $i <= 9; $i = $i + 0.5) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->english_exam_stage['english_exam_score_writing_ielts'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-2">
                            <label for="english_exam_score_speaking_ielts">
                              <h5>Speaking</h5>
                            </label>
                            <!-- <input name="english_exam_score_speaking" class="form-control" type="text" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_speaking']) echo $qualified_exams->english_exam_stage['english_exam_score_speaking'] ?>"> -->
                            <select class="form-control" id="english_exam_score_speaking_ielts" name="english_exam_score_speaking_ielts">
                              <option>Select</option>
                              <?php for ($i = 0.5; $i <= 9; $i = $i + 0.5) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->english_exam_stage['english_exam_score_speaking_ielts'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-3">
                            <label for="english_exam_score_total_ielts">
                              <h5>Total</h5>
                            </label>
                            <!-- <input name="english_exam_score_total" class="form-control" type="text" id="english_exam_score_total" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_total']) echo $qualified_exams->english_exam_stage['english_exam_score_total'] ?>"> -->
                            <select class="form-control" id="english_exam_score_total_ielts" name="english_exam_score_total_ielts">
                              <option>Select</option>
                              <?php for ($i = 0.5; $i <= 36; $i = $i + 0.5) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->english_exam_stage['english_exam_score_total_ielts'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>

                        <div class="col-xs-12" id="english_exam_pte" <?php if ($qualified_exams && $qualified_exams->english_exam_type == 'PTE' && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT') echo 'style=display:block;' ?>>

                          <div class="col-xs-3">
                            <label for="english_exam_score_reading_pte">
                              <h5>Record Date</h5>
                            </label>
                            <input name="english_exam_record_date_pte" class="form-control" type="date" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->english_exam_stage['english_exam_record_date_pte'])) echo $qualified_exams->english_exam_stage['english_exam_record_date_pte'] ?>">
                          </div>

                          <div class="col-xs-2">
                            <label for="english_exam_score_reading_pte">
                              <h5>Reading</h5>
                            </label>
                            <!-- <input name="english_exam_score_reading" class="form-control" type="text" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_reading']) echo $qualified_exams->english_exam_stage['english_exam_score_reading'] ?>"> -->
                            <select class="form-control" id="english_exam_score_reading_pte" name="english_exam_score_reading_pte">
                              <option>Select</option>

                              <?php for ($i = 1; $i <= 90; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->english_exam_stage['english_exam_score_reading_pte'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>


                            </select>

                          </div>
                          <div class="col-xs-2">
                            <label for="english_exam_score_listening_pte">
                              <h5>Listening</h5>
                            </label>
                            <!-- <input name="english_exam_score_listening" class="form-control" type="text" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_listening']) echo $qualified_exams->english_exam_stage['english_exam_score_listening'] ?>"> -->
                            <select class="form-control" id="english_exam_score_listening_pte" name="english_exam_score_listening_pte">
                              <option>Select</option>
                              <?php for ($i = 1; $i <= 90; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->english_exam_stage['english_exam_score_listening_pte'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-2">
                            <label for="english_exam_score_writing_pte">
                              <h5>Writing</h5>
                            </label>
                            <!-- <input name="english_exam_score_writing" class="form-control" type="text" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_writing']) echo $qualified_exams->english_exam_stage['english_exam_score_writing'] ?>"> -->
                            <select class="form-control" id="english_exam_score_writing_pte" name="english_exam_score_writing_pte">
                              <option>Select</option>
                              <?php for ($i = 1; $i <= 90; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->english_exam_stage['english_exam_score_writing_pte'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-2">
                            <label for="english_exam_score_speaking_pte">
                              <h5>Speaking</h5>
                            </label>
                            <!-- <input name="english_exam_score_speaking" class="form-control" type="text" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_speaking']) echo $qualified_exams->english_exam_stage['english_exam_score_speaking'] ?>"> -->
                            <select class="form-control" id="english_exam_score_speaking_pte" name="english_exam_score_speaking_pte">
                              <option>Select</option>
                              <?php for ($i = 1; $i <= 90; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->english_exam_stage['english_exam_score_speaking_pte'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-3">
                            <label for="english_exam_score_total_pte">
                              <h5>Total</h5>
                            </label>
                            <!-- <input name="english_exam_score_total" class="form-control" type="text" id="english_exam_score_total" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_total']) echo $qualified_exams->english_exam_stage['english_exam_score_total'] ?>"> -->
                            <select class="form-control" id="english_exam_score_total_pte" name="english_exam_score_total_pte">
                              <option>Select</option>
                              <?php for ($i = 1; $i <= 360; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->english_exam_stage['english_exam_score_total_pte'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>

                        <div class="col-xs-12" id="english_exam_duolingo" <?php if ($qualified_exams && $qualified_exams->english_exam_type == 'DUOLINGO' && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT') echo 'style=display:block;' ?>>

                          <div class="col-xs-3">
                            <label for="english_exam_score_reading_duolingo">
                              <h5>Record Date</h5>
                            </label>
                            <input name="english_exam_record_date_duolingo" class="form-control" type="date" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->english_exam_stage['english_exam_record_date_duolingo'])) echo $qualified_exams->english_exam_stage['english_exam_record_date_duolingo'] ?>">
                          </div>

                          <div class="col-xs-2">
                            <label for="english_exam_score_reading_duolingo">
                              <h5>Reading</h5>
                            </label>
                            <!-- <input name="english_exam_score_reading" class="form-control" type="text" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_reading']) echo $qualified_exams->english_exam_stage['english_exam_score_reading'] ?>"> -->
                            <select class="form-control" id="english_exam_score_reading_duolingo" name="english_exam_score_reading_duolingo">
                              <option>Select</option>

                              <?php for ($i = 5; $i <= 160; $i = $i + 5) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->english_exam_stage['english_exam_score_reading_duolingo'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>


                            </select>

                          </div>
                          <div class="col-xs-2">
                            <label for="english_exam_score_listening_duolingo">
                              <h5>Listening</h5>
                            </label>
                            <!-- <input name="english_exam_score_listening" class="form-control" type="text" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_listening']) echo $qualified_exams->english_exam_stage['english_exam_score_listening'] ?>"> -->
                            <select class="form-control" id="english_exam_score_listening_duolingo" name="english_exam_score_listening_duolingo">
                              <option>Select</option>
                              <?php for ($i = 5; $i <= 160; $i = $i + 5) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->english_exam_stage['english_exam_score_listening_duolingo'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-2">
                            <label for="english_exam_score_writing_duolingo">
                              <h5>Writing</h5>
                            </label>
                            <!-- <input name="english_exam_score_writing" class="form-control" type="text" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_writing']) echo $qualified_exams->english_exam_stage['english_exam_score_writing'] ?>"> -->
                            <select class="form-control" id="english_exam_score_writing_duolingo" name="english_exam_score_writing_duolingo">
                              <option>Select</option>
                              <?php for ($i = 5; $i <= 160; $i = $i + 5) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->english_exam_stage['english_exam_score_writing_duolingo'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-2">
                            <label for="english_exam_score_speaking_duolingo">
                              <h5>Speaking</h5>
                            </label>
                            <!-- <input name="english_exam_score_speaking" class="form-control" type="text" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_speaking']) echo $qualified_exams->english_exam_stage['english_exam_score_speaking'] ?>"> -->
                            <select class="form-control" id="english_exam_score_speaking_duolingo" name="english_exam_score_speaking_duolingo">
                              <option>Select</option>
                              <?php for ($i = 5; $i <= 160; $i = $i + 5) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->english_exam_stage['english_exam_score_speaking_duolingo'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-xs-3">
                            <label for="english_exam_score_total_duolingo">
                              <h5>Total</h5>
                            </label>
                            <!-- <input name="english_exam_score_total" class="form-control" type="text" id="english_exam_score_total" value="<?php if ($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_total']) echo $qualified_exams->english_exam_stage['english_exam_score_total'] ?>"> -->
                            <select class="form-control" id="english_exam_score_total_duolingo" name="english_exam_score_total_duolingo">
                              <option>Select</option>
                              <?php for ($i = 5; $i <= 640; $i = $i + 5) { ?>
                                <option value="<?php echo $i; ?>" <?php if ($qualified_exams->english_exam_stage['english_exam_score_total_duolingo'] == $i) {
                                                                    echo 'selected="selected"';
                                                                  } ?>><?php echo $i; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div> <span class="sep col-lg-12"></span>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-12" style="overflow: auto;">
                          <label for="secondary_qualification">
                            <h5>Secondary Qualification</h5>
                          </label>
                          <table style="width: max-content;">
                            <th>
                              <label style="font-weight: 600;">Qualification</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Institution Name</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Board</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Month Started</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Year Started</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Month Finished</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Year Finished</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Grade/ %</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Out Of Total</label>
                            </th>
                            <tbody>
                              <tr>
                                <td>
                                  <label>Grade X</label>
                                  </< /td>
                                <td>
                                  <input type="text" name="secondary_institution_name" id="secondary_institution_name" class="form-control" value="<?php if ($secondaryqualification_details != '') echo $secondaryqualification_details->institution_name; ?>" />
                                </td>
                                <td>
                                  <input type="text" name="board" id="board" class="form-control" value="<?php if ($secondaryqualification_details != '') echo $secondaryqualification_details->board; ?>" />
                                </td>
                                <td>
                                  <select class="form-control" id="secondary_month_starrted" name="secondary_month_starrted">
                                    <option selected value="January" <?php if ($secondaryqualification_details->month_started == 'January') {
                                                                        echo "selected";
                                                                      } ?>>January</option>
                                    <option value="February" <?php if ($secondaryqualification_details->month_started == 'February') {
                                                                echo "selected";
                                                              } ?>>February</option>
                                    <option value="March" <?php if ($secondaryqualification_details->month_started == 'March') {
                                                            echo "selected";
                                                          } ?>>March</option>
                                    <option value="April" <?php if ($secondaryqualification_details->month_started == 'April') {
                                                            echo "selected";
                                                          } ?>>April</option>
                                    <option value="May" <?php if ($secondaryqualification_details->month_started == 'May') {
                                                          echo "selected";
                                                        } ?>>May</option>
                                    <option value="June" <?php if ($secondaryqualification_details->month_started == 'June') {
                                                            echo "selected";
                                                          } ?>>June</option>
                                    <option value="July" <?php if ($secondaryqualification_details->month_started == 'July') {
                                                            echo "selected";
                                                          } ?>>July</option>
                                    <option value="August" <?php if ($secondaryqualification_details->month_started == 'August') {
                                                              echo "selected";
                                                            } ?>>August</option>
                                    <option value="September" <?php if ($secondaryqualification_details->month_started == 'September') {
                                                                echo "selected";
                                                              } ?>>September</option>
                                    <option value="October" <?php if ($secondaryqualification_details->month_started == 'October') {
                                                              echo "selected";
                                                            } ?>>October</option>
                                    <option value="November" <?php if ($secondaryqualification_details->month_started == 'November') {
                                                                echo "selected";
                                                              } ?>>November</option>
                                    <option value="December" <?php if ($secondaryqualification_details->month_started == 'December') {
                                                                echo "selected";
                                                              } ?>>December</option>
                                  </select>
                                </td>
                                <td>
                                  <select class="form-control" id="secondary_year_starrted" name="secondary_year_starrted">
                                    <?php
                                    for ($i = 1980; $i <= 2030; $i++) {
                                    ?>
                                      <option value="<?php echo $i; ?>" <?php if ($secondaryqualification_details != '' && $secondaryqualification_details->year_started == $i) echo 'selected="selected"'; ?>>
                                        <?php echo $i; ?>
                                      </option>
                                    <?php
                                    } ?>
                                  </select>
                                </td>
                                <td>
                                  <select class="form-control" id="secondary_month_finished" name="secondary_month_finished">
                                    <option selected value="January" <?php if ($secondaryqualification_details->month_finished == 'January') {
                                                                        echo "selected";
                                                                      } ?>>January</option>
                                    <option value="February" <?php if ($secondaryqualification_details->month_finished == 'February') {
                                                                echo "selected";
                                                              } ?>>February</option>
                                    <option value="March" <?php if ($secondaryqualification_details->month_finished == 'March') {
                                                            echo "selected";
                                                          } ?>>March</option>
                                    <option value="April" <?php if ($secondaryqualification_details->month_finished == 'April') {
                                                            echo "selected";
                                                          } ?>>April</option>
                                    <option value="May" <?php if ($secondaryqualification_details->month_finished == 'May') {
                                                          echo "selected";
                                                        } ?>>May</option>
                                    <option value="June" <?php if ($secondaryqualification_details->month_finished == 'June') {
                                                            echo "selected";
                                                          } ?>>June</option>
                                    <option value="July" <?php if ($secondaryqualification_details->month_finished == 'July') {
                                                            echo "selected";
                                                          } ?>>July</option>
                                    <option value="August" <?php if ($secondaryqualification_details->month_finished == 'August') {
                                                              echo "selected";
                                                            } ?>>August</option>
                                    <option value="September" <?php if ($secondaryqualification_details->month_finished == 'September') {
                                                                echo "selected";
                                                              } ?>>September</option>
                                    <option value="October" <?php if ($secondaryqualification_details->month_finished == 'October') {
                                                              echo "selected";
                                                            } ?>>October</option>
                                    <option value="November" <?php if ($secondaryqualification_details->month_finished == 'November') {
                                                                echo "selected";
                                                              } ?>>November</option>
                                    <option value="December" <?php if ($secondaryqualification_details->month_finished == 'December') {
                                                                echo "selected";
                                                              } ?>>December</option>
                                  </select>
                                </td>
                                <td>
                                  <select class="form-control" id="secondary_year_finished" name="secondary_year_finished">
                                    <?php
                                    for ($i = 1980; $i <= 2030; $i++) {
                                    ?>
                                      <option value="<?php echo $i; ?>" <?php if ($secondaryqualification_details != '' && $secondaryqualification_details->year_finished == $i) echo 'selected="selected"'; ?>>
                                        <?php echo $i; ?>
                                      </option>
                                    <?php
                                    }
                                    ?>
                                  </select>
                                </td>
                                <td>
                                  <input type="text" name="secondary_grade" id="secondary_grade" class="form-control" value="<?php if ($secondaryqualification_details != '') echo $secondaryqualification_details->grade; ?>" />
                                </td>
                                <td>
                                  <select class="form-control" id="secondary_total" name="secondary_total">
                                    <option value="100" <?php if ($secondaryqualification_details != '' && $secondaryqualification_details->total == 100) echo 'selected="selected"'; ?>>100</option>
                                    <option value="10" <?php if ($secondaryqualification_details != '' && $secondaryqualification_details->total == 10) echo 'selected="selected"'; ?>>10</option>
                                    <option value="7" <?php if ($secondaryqualification_details != '' && $secondaryqualification_details->total == 7) echo 'selected="selected"'; ?>>7</option>
                                    <option value="4" <?php if ($secondaryqualification_details != '' && $secondaryqualification_details->total == 4) echo 'selected="selected"'; ?>>4</option>
                                  </select>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div> <span class="sep col-lg-12"></span>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-12">
                          <label for="diploma_12">
                            <h5>Have you done Diploma or 12th After School?</h5>
                          </label>
                          <div class="col-md-12">
                            <label class="radio-inline">
                              <input type="radio" name="diploma_12" <?php if ($diplomaqualification_details != '') echo 'checked="checked"'; ?> id="diploma" value="1">Diploma </label>
                            <label class="radio-inline">
                              <input type="radio" name="diploma_12" <?php if ($highersecondaryqualification_details != '') echo 'checked="checked"'; ?> id="12th" value="2">12TH </label>
                          </div>
                        </div> <span class="sep col-lg-12"></span>
                      </div>
                      <div class="form-group diploma_hs" id="12thidd" <?php if ($highersecondaryqualification_details != '') { ?> style="display:block;"
                        <?php } else { ?>style="display:none;"
                        <?php } ?>>
                        <div class="col-xs-12" style="overflow: auto;">
                          <label for="higher_secondary_qualification">
                            <h5>Higher Secondary Qualification</h5>
                          </label>
                          <table style="width: max-content;">
                            <th>
                              <label style="font-weight: 600;">Qualification</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Institution Name</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Board</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Month Started</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Year Started</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Month Finished</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Year Finished</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Grade/ %</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Out Of Total</label>
                            </th>
                            <tbody>
                              <tr>
                                <td>
                                  <label>Grade XII</label>
                                  </< /td>
                                <td>
                                  <input type="text" name="hs_institution_name" id="hs_institution_name" class="form-control" value="<?php if ($highersecondaryqualification_details != '') echo $highersecondaryqualification_details->institution_name; ?>" />
                                </td>
                                <td>
                                  <input type="text" name="hs_board" id="hs_board" class="form-control" value="<?php if ($highersecondaryqualification_details != '') echo $highersecondaryqualification_details->board; ?>" />
                                </td>

                                <td>
                                  <select class="form-control" id="hs_month_starrted" name="hs_month_starrted">
                                    <option selected value="January" <?php if ($highersecondaryqualification_details->month_started == 'January') {
                                                                        echo "selected";
                                                                      } ?>>January</option>
                                    <option value="February" <?php if ($highersecondaryqualification_details->month_started == 'February') {
                                                                echo "selected";
                                                              } ?>>February</option>
                                    <option value="March" <?php if ($highersecondaryqualification_details->month_started == 'March') {
                                                            echo "selected";
                                                          } ?>>March</option>
                                    <option value="April" <?php if ($highersecondaryqualification_details->month_started == 'April') {
                                                            echo "selected";
                                                          } ?>>April</option>
                                    <option value="May" <?php if ($highersecondaryqualification_details->month_started == 'May') {
                                                          echo "selected";
                                                        } ?>>May</option>
                                    <option value="June" <?php if ($highersecondaryqualification_details->month_started == 'June') {
                                                            echo "selected";
                                                          } ?>>June</option>
                                    <option value="July" <?php if ($highersecondaryqualification_details->month_started == 'July') {
                                                            echo "selected";
                                                          } ?>>July</option>
                                    <option value="August" <?php if ($highersecondaryqualification_details->month_started == 'August') {
                                                              echo "selected";
                                                            } ?>>August</option>
                                    <option value="September" <?php if ($highersecondaryqualification_details->month_started == 'September') {
                                                                echo "selected";
                                                              } ?>>September</option>
                                    <option value="October" <?php if ($highersecondaryqualification_details->month_started == 'October') {
                                                              echo "selected";
                                                            } ?>>October</option>
                                    <option value="November" <?php if ($highersecondaryqualification_details->month_started == 'November') {
                                                                echo "selected";
                                                              } ?>>November</option>
                                    <option value="December" <?php if ($highersecondaryqualification_details->month_started == 'December') {
                                                                echo "selected";
                                                              } ?>>December</option>
                                  </select>
                                </td>
                                <td>
                                  <select class="form-control" id="hs_year_starrted" name="hs_year_starrted">
                                    <?php
                                    for ($i = 1980; $i <= 2030; $i++) {
                                    ?>
                                      <option value="<?php echo $i; ?>" <?php if ($highersecondaryqualification_details != '' && $highersecondaryqualification_details->year_started == $i) echo 'selected="selected"'; ?>>
                                        <?php echo $i; ?>
                                      </option>
                                    <?php
                                    }
                                    ?>
                                  </select>
                                </td>
                                <td>
                                  <select class="form-control" id="hs_month_finished" name="hs_month_finished">
                                    <option selected value="January" <?php if ($highersecondaryqualification_details->month_finished == 'January') {
                                                                        echo "selected";
                                                                      } ?>>January</option>
                                    <option value="February" <?php if ($highersecondaryqualification_details->month_finished == 'February') {
                                                                echo "selected";
                                                              } ?>>February</option>
                                    <option value="March" <?php if ($highersecondaryqualification_details->month_finished == 'March') {
                                                            echo "selected";
                                                          } ?>>March</option>
                                    <option value="April" <?php if ($highersecondaryqualification_details->month_finished == 'April') {
                                                            echo "selected";
                                                          } ?>>April</option>
                                    <option value="May" <?php if ($highersecondaryqualification_details->month_finished == 'May') {
                                                          echo "selected";
                                                        } ?>>May</option>
                                    <option value="June" <?php if ($highersecondaryqualification_details->month_finished == 'June') {
                                                            echo "selected";
                                                          } ?>>June</option>
                                    <option value="July" <?php if ($highersecondaryqualification_details->month_finished == 'July') {
                                                            echo "selected";
                                                          } ?>>July</option>
                                    <option value="August" <?php if ($highersecondaryqualification_details->month_finished == 'August') {
                                                              echo "selected";
                                                            } ?>>August</option>
                                    <option value="September" <?php if ($highersecondaryqualification_details->month_finished == 'September') {
                                                                echo "selected";
                                                              } ?>>September</option>
                                    <option value="October" <?php if ($highersecondaryqualification_details->month_finished == 'October') {
                                                              echo "selected";
                                                            } ?>>October</option>
                                    <option value="November" <?php if ($highersecondaryqualification_details->month_finished == 'November') {
                                                                echo "selected";
                                                              } ?>>November</option>
                                    <option value="December" <?php if ($highersecondaryqualification_details->month_finished == 'December') {
                                                                echo "selected";
                                                              } ?>>December</option>
                                  </select>
                                </td>
                                <td>
                                  <select class="form-control" id="hs_year_finished" name="hs_year_finished">
                                    <?php
                                    for ($i = 1980; $i <= 2030; $i++) {
                                    ?>
                                      <option value="<?php echo $i; ?>" <?php if ($highersecondaryqualification_details != '' && $highersecondaryqualification_details->year_finished == $i) echo 'selected="selected"'; ?>>
                                        <?php echo $i; ?>
                                      </option>
                                    <?php
                                    }
                                    ?>
                                  </select>
                                </td>
                                <td>
                                  <input type="text" name="hs_grade" id="hs_grade" class="form-control" value="<?php if ($highersecondaryqualification_details != '') echo $highersecondaryqualification_details->grade; ?>" />
                                </td>
                                <td>
                                  <select class="form-control" id="hs_total" name="hs_total">
                                    <option value="100" <?php if ($highersecondaryqualification_details != '' && $highersecondaryqualification_details->total == 100) echo 'selected="selected"'; ?>>100</option>
                                    <option value="10" <?php if ($highersecondaryqualification_details != '' && $highersecondaryqualification_details->total == 10) echo 'selected="selected"'; ?>>10</option>
                                    <option value="7" <?php if ($highersecondaryqualification_details != '' && $highersecondaryqualification_details->total == 7) echo 'selected="selected"'; ?>>7</option>
                                    <option value="4" <?php if ($highersecondaryqualification_details != '' && $highersecondaryqualification_details->total == 4) echo 'selected="selected"'; ?>>4</option>
                                  </select>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div> <span class="sep col-lg-12"></span>
                      </div>
                      <div class="form-group diploma_hs" id="diploma_id" <?php if ($diplomaqualification_details != '') { ?> style="display:block;"
                        <?php } else { ?>style="display:none;"
                        <?php } ?>>
                        <div class="col-xs-12" style="overflow: auto;">
                          <label for="diploma">
                            <h5>Diploma</h5>
                          </label>
                          <table style="width: max-content;">
                            <th>
                              <label style="font-weight: 600;">Name of Course</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">College Name</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Month Started</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Year Started</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Month Finished</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Year Finished</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Aggregate Percentage (1 to 6 Sem)</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Out Of Total</label>
                            </th>
                            <tbody>
                              <tr>
                                <td>
                                  <input type="text" name="diploma_course_name" id="diploma_course_name" class="form-control" value="<?php if ($diplomaqualification_details != '') echo $diplomaqualification_details->course; ?>" /> </< /td>
                                <td>
                                  <input type="text" name="diploma_institution_name" id="diploma_institution_name" class="form-control" value="<?php if ($diplomaqualification_details != '') echo $diplomaqualification_details->college; ?>" />
                                </td>
                                <td>
                                  <select class="form-control" id="diploma_month_starrted" name="diploma_month_starrted">
                                    <option selected value="January" <?php if ($secondaryqualification_details->month_started == 'January') {
                                                                        echo "selected";
                                                                      } ?>>January</option>
                                    <option value="February" <?php if ($diplomaqualification_details->month_started == 'February') {
                                                                echo "selected";
                                                              } ?>>February</option>
                                    <option value="March" <?php if ($diplomaqualification_details->month_started == 'March') {
                                                            echo "selected";
                                                          } ?>>March</option>
                                    <option value="April" <?php if ($diplomaqualification_details->month_started == 'April') {
                                                            echo "selected";
                                                          } ?>>April</option>
                                    <option value="May" <?php if ($diplomaqualification_details->month_started == 'May') {
                                                          echo "selected";
                                                        } ?>>May</option>
                                    <option value="June" <?php if ($diplomaqualification_details->month_started == 'June') {
                                                            echo "selected";
                                                          } ?>>June</option>
                                    <option value="July" <?php if ($diplomaqualification_details->month_started == 'July') {
                                                            echo "selected";
                                                          } ?>>July</option>
                                    <option value="August" <?php if ($diplomaqualification_details->month_started == 'August') {
                                                              echo "selected";
                                                            } ?>>August</option>
                                    <option value="September" <?php if ($diplomaqualification_details->month_started == 'September') {
                                                                echo "selected";
                                                              } ?>>September</option>
                                    <option value="October" <?php if ($diplomaqualification_details->month_started == 'October') {
                                                              echo "selected";
                                                            } ?>>October</option>
                                    <option value="November" <?php if ($diplomaqualification_details->month_started == 'November') {
                                                                echo "selected";
                                                              } ?>>November</option>
                                    <option value="December" <?php if ($diplomaqualification_details->month_started == 'December') {
                                                                echo "selected";
                                                              } ?>>December</option>
                                  </select>
                                </td>
                                <td>
                                  <select class="form-control" id="diploma_year_starrted" name="diploma_year_starrted">
                                    <?php
                                    for ($i = 1980; $i <= 2030; $i++) {
                                    ?>
                                      <option value="<?php echo $i; ?>" <?php if ($diplomaqualification_details != '' && $diplomaqualification_details->year_started == $i) echo 'selected="selected"'; ?>>
                                        <?php echo $i; ?>
                                      </option>
                                    <?php
                                    }
                                    ?>
                                  </select>
                                </td>
                                <td>
                                  <select class="form-control" id="diploma_month_finished" name="diploma_month_finished">
                                    <option selected value="January" <?php if ($secondaryqualification_details->month_finished == 'January') {
                                                                        echo "selected";
                                                                      } ?>>January</option>
                                    <option value="February" <?php if ($diplomaqualification_details->month_finished == 'February') {
                                                                echo "selected";
                                                              } ?>>February</option>
                                    <option value="March" <?php if ($diplomaqualification_details->month_finished == 'March') {
                                                            echo "selected";
                                                          } ?>>March</option>
                                    <option value="April" <?php if ($diplomaqualification_details->month_finished == 'April') {
                                                            echo "selected";
                                                          } ?>>April</option>
                                    <option value="May" <?php if ($diplomaqualification_details->month_finished == 'May') {
                                                          echo "selected";
                                                        } ?>>May</option>
                                    <option value="June" <?php if ($diplomaqualification_details->month_finished == 'June') {
                                                            echo "selected";
                                                          } ?>>June</option>
                                    <option value="July" <?php if ($diplomaqualification_details->month_finished == 'July') {
                                                            echo "selected";
                                                          } ?>>July</option>
                                    <option value="August" <?php if ($diplomaqualification_details->month_finished == 'August') {
                                                              echo "selected";
                                                            } ?>>August</option>
                                    <option value="September" <?php if ($diplomaqualification_details->month_finished == 'September') {
                                                                echo "selected";
                                                              } ?>>September</option>
                                    <option value="October" <?php if ($diplomaqualification_details->month_finished == 'October') {
                                                              echo "selected";
                                                            } ?>>October</option>
                                    <option value="November" <?php if ($diplomaqualification_details->month_finished == 'November') {
                                                                echo "selected";
                                                              } ?>>November</option>
                                    <option value="December" <?php if ($diplomaqualification_details->month_finished == 'December') {
                                                                echo "selected";
                                                              } ?>>December</option>
                                  </select>
                                </td>
                                <td>
                                  <select class="form-control" id="diploma_year_finished" name="diploma_year_finished">
                                    <?php
                                    for ($i = 1980; $i <= 2030; $i++) {
                                    ?>
                                      <option value="<?php echo $i; ?>" <?php if ($diplomaqualification_details != '' && $diplomaqualification_details->year_finished == $i) echo 'selected="selected"'; ?>>
                                        <?php echo $i; ?>
                                      </option>
                                    <?php
                                    }
                                    ?>
                                  </select>
                                </td>
                                <td>
                                  <input type="text" name="diploma_grade" id="diploma_grade" class="form-control" value="<?php if ($diplomaqualification_details != '') echo $diplomaqualification_details->aggregate; ?>" />
                                </td>
                                <td>
                                  <select class="form-control" id="diploma_total" name="diploma_total">
                                    <option value="100" <?php if ($diplomaqualification_details != '' && $diplomaqualification_details->total == 100) echo 'selected="selected"'; ?>>100</option>
                                    <option value="10" <?php if ($diplomaqualification_details != '' && $diplomaqualification_details->total == 10) echo 'selected="selected"'; ?>>10</option>
                                    <option value="7" <?php if ($diplomaqualification_details != '' && $diplomaqualification_details->total == 7) echo 'selected="selected"'; ?>>7</option>
                                    <option value="4" <?php if ($diplomaqualification_details != '' && $diplomaqualification_details->total == 10) echo 'selected="selected"'; ?>>4</option>
                                  </select>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div> <span class="sep col-lg-12"></span>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-12" style="overflow: auto;">
                          <label for="bachelor_graduation">
                            <h5>Bachelor's/Graduation</h5>
                          </label>
                          <table style="width: max-content;">
                            <th>
                              <label style="font-weight: 600;">Name of Course</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">University</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">College Name</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Major</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Start Month</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Year Started</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">End Month</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Year Finished</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Grade / %</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Out Of Total</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">No of Backlogs(if any)</label>
                            </th>
                            <tbody>
                              <tr>
                                <td>
                                  <input type="text" name="bachelor_course" id="bachelor_course" class="form-control" value="<?php if ($bachelorqualification_details != '') echo $bachelorqualification_details->course; ?>" />
                                </td>
                                <td>
                                  <input type="text" name="bachelor_university" id="bachelor_university" class="form-control" value="<?php if ($bachelorqualification_details != '') echo $bachelorqualification_details->university; ?>" />
                                </td>
                                <td>
                                  <input type="text" name="bachelor_college" id="bachelor_college" class="form-control" value="<?php if ($bachelorqualification_details != '') echo $bachelorqualification_details->college; ?>" />
                                </td>
                                <td>
                                  <input type="text" name="bachelor_major" id="bachelor_major" class="form-control" value="<?php if ($bachelorqualification_details != '') echo $bachelorqualification_details->major; ?>" />
                                </td>
                                <td>
                                  <select class="form-control" id="bachelor_month_starrted" name="bachelor_month_starrted">
                                    <option selected value="January" <?php if ($bachelorqualification_details->month_started == 'January') {
                                                                        echo "selected";
                                                                      } ?>>January</option>
                                    <option value="February" <?php if ($bachelorqualification_details->month_started == 'February') {
                                                                echo "selected";
                                                              } ?>>February</option>
                                    <option value="March" <?php if ($bachelorqualification_details->month_started == 'March') {
                                                            echo "selected";
                                                          } ?>>March</option>
                                    <option value="April" <?php if ($bachelorqualification_details->month_started == 'April') {
                                                            echo "selected";
                                                          } ?>>April</option>
                                    <option value="May" <?php if ($bachelorqualification_details->month_started == 'May') {
                                                          echo "selected";
                                                        } ?>>May</option>
                                    <option value="June" <?php if ($bachelorqualification_details->month_started == 'June') {
                                                            echo "selected";
                                                          } ?>>June</option>
                                    <option value="July" <?php if ($bachelorqualification_details->month_started == 'July') {
                                                            echo "selected";
                                                          } ?>>July</option>
                                    <option value="August" <?php if ($bachelorqualification_details->month_started == 'August') {
                                                              echo "selected";
                                                            } ?>>August</option>
                                    <option value="September" <?php if ($bachelorqualification_details->month_started == 'September') {
                                                                echo "selected";
                                                              } ?>>September</option>
                                    <option value="October" <?php if ($bachelorqualification_details->month_started == 'October') {
                                                              echo "selected";
                                                            } ?>>October</option>
                                    <option value="November" <?php if ($bachelorqualification_details->month_started == 'November') {
                                                                echo "selected";
                                                              } ?>>November</option>
                                    <option value="December" <?php if ($bachelorqualification_details->month_started == 'December') {
                                                                echo "selected";
                                                              } ?>>December</option>
                                  </select>
                                </td>
                                <td>
                                  <select class="form-control" id="bachelor_year_starrted" name="bachelor_year_starrted">
                                    <?php
                                    for ($i = 1980; $i <= 2030; $i++) {
                                    ?>
                                      <option value="<?php echo $i; ?>" <?php if ($bachelorqualification_details != '' && $bachelorqualification_details->year_started == $i) echo 'selected="selected"'; ?>>
                                        <?php echo $i; ?>
                                      </option>
                                    <?php
                                    }
                                    ?>
                                  </select>
                                </td>
                                <td>
                                  <select class="form-control" id="bachelor_month_finished" name="bachelor_month_finished">
                                    <option selected value="January" <?php if ($bachelorqualification_details->month_finished == 'January') {
                                                                        echo "selected";
                                                                      } ?>>January</option>
                                    <option value="February" <?php if ($bachelorqualification_details->month_finished == 'February') {
                                                                echo "selected";
                                                              } ?>>February</option>
                                    <option value="March" <?php if ($bachelorqualification_details->month_finished == 'March') {
                                                            echo "selected";
                                                          } ?>>March</option>
                                    <option value="April" <?php if ($bachelorqualification_details->month_finished == 'April') {
                                                            echo "selected";
                                                          } ?>>April</option>
                                    <option value="May" <?php if ($bachelorqualification_details->month_finished == 'May') {
                                                          echo "selected";
                                                        } ?>>May</option>
                                    <option value="June" <?php if ($bachelorqualification_details->month_finished == 'June') {
                                                            echo "selected";
                                                          } ?>>June</option>
                                    <option value="July" <?php if ($bachelorqualification_details->month_finished == 'July') {
                                                            echo "selected";
                                                          } ?>>July</option>
                                    <option value="August" <?php if ($bachelorqualification_details->month_finished == 'August') {
                                                              echo "selected";
                                                            } ?>>August</option>
                                    <option value="September" <?php if ($bachelorqualification_details->month_finished == 'September') {
                                                                echo "selected";
                                                              } ?>>September</option>
                                    <option value="October" <?php if ($bachelorqualification_details->month_finished == 'October') {
                                                              echo "selected";
                                                            } ?>>October</option>
                                    <option value="November" <?php if ($bachelorqualification_details->month_finished == 'November') {
                                                                echo "selected";
                                                              } ?>>November</option>
                                    <option value="December" <?php if ($bachelorqualification_details->month_finished == 'December') {
                                                                echo "selected";
                                                              } ?>>December</option>
                                  </select>
                                </td>
                                <td>
                                  <select class="form-control" id="bachelor_year_finished" name="bachelor_year_finished">
                                    <?php
                                    for ($i = 1980; $i <= 2030; $i++) {
                                    ?>
                                      <option value="<?php echo $i; ?>" <?php if ($bachelorqualification_details != '' && $bachelorqualification_details->year_finished == $i) echo 'selected="selected"'; ?>>
                                        <?php echo $i; ?>
                                      </option>
                                    <?php
                                    }
                                    ?>
                                  </select>
                                </td>
                                <td>
                                  <input type="text" name="bachelor_grade" id="bachelor_grade" class="form-control" value="<?php if ($bachelorqualification_details != '') echo $bachelorqualification_details->grade; ?>" />
                                </td>
                                <td>
                                  <select class="form-control" id="bachelor_total" name="bachelor_total">
                                    <option value="100" <?php if ($bachelorqualification_details != '' && $bachelorqualification_details->total == 100) echo 'selected="selected"'; ?>>100</option>
                                    <option value="10" <?php if ($bachelorqualification_details != '' && $bachelorqualification_details->total == 10) echo 'selected="selected"'; ?>>10</option>
                                    <option value="7" <?php if ($bachelorqualification_details != '' && $bachelorqualification_details->total == 7) echo 'selected="selected"'; ?>>7</option>
                                    <option value="4" <?php if ($bachelorqualification_details != '' && $bachelorqualification_details->total == 4) echo 'selected="selected"'; ?>>4</option>
                                  </select>
                                </td>
                                <td>
                                  <input type="text" name="bc_no_of_backlogs" id="no_of_backlogs" class="form-control" value="<?php if ($bachelorqualification_details != '') echo $bachelorqualification_details->number_backlogs; ?>" />
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div> <span class="sep col-lg-12"></span>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-12">
                          <label for="masters">
                            <h5>Have you done Master?</h5>
                          </label>
                          <div class="col-md-12">
                            <label class="radio-inline">
                              <input type="radio" name="masters" <?php if ($mastersqualification_details != '') echo 'checked="checked"'; ?> id="master_yes" value="1">Yes </label>
                            <label class="radio-inline">
                              <input type="radio" name="masters" <?php if ($mastersqualification_details == '') echo 'checked="checked"'; ?> id="master_no" value="2">No </label>
                          </div>
                        </div> <span class="sep col-lg-12"></span>
                      </div>
                      <div class="form-group" id="masters_id" <?php if ($mastersqualification_details != '') { ?> style="display:block;"
                        <?php } else { ?>style="display:none;"
                        <?php } ?>>
                        <div class="col-xs-12" style="overflow: auto;">
                          <label for="master_degree">
                            <h5>Master's</h5>
                          </label>
                          <table style="width: max-content;">
                            <th>
                              <label style="font-weight: 600;">Name of Course</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">University</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">College Name</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Major</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Start Month</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Year Started</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">End Month</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Year Finished</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Grade / %</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">Out Of Total</label>
                            </th>
                            <th>
                              <label style="font-weight: 600;">No of Backlogs(if any)</label>
                            </th>
                            <tbody>
                              <tr>
                                <td>
                                  <input type="text" name="master_course" id="master_course" class="form-control" value="<?php if ($mastersqualification_details != '') echo $mastersqualification_details->course; ?>" />
                                </td>
                                <td>
                                  <input type="text" name="master_university" id="master_university" class="form-control" value="<?php if ($mastersqualification_details != '') echo $mastersqualification_details->university; ?>" />
                                </td>
                                <td>
                                  <input type="text" name="master_college" id="master_college" class="form-control" value="<?php if ($mastersqualification_details != '') echo $mastersqualification_details->college; ?>" />
                                </td>
                                <td>
                                  <input type="text" name="master_major" id="master_major" class="form-control" value="<?php if ($mastersqualification_details != '') echo $mastersqualification_details->major; ?>" />
                                </td>
                                <td>
                                  <select class="form-control" id="master_month_started" name="master_month_started">
                                    <option selected value="January" <?php if ($mastersqualification_details->month_started == 'January') {
                                                                        echo "selected";
                                                                      } ?>>January</option>
                                    <option value="February" <?php if ($mastersqualification_details->month_started == 'February') {
                                                                echo "selected";
                                                              } ?>>February</option>
                                    <option value="March" <?php if ($mastersqualification_details->month_started == 'March') {
                                                            echo "selected";
                                                          } ?>>March</option>
                                    <option value="April" <?php if ($mastersqualification_details->month_started == 'April') {
                                                            echo "selected";
                                                          } ?>>April</option>
                                    <option value="May" <?php if ($mastersqualification_details->month_started == 'May') {
                                                          echo "selected";
                                                        } ?>>May</option>
                                    <option value="June" <?php if ($mastersqualification_details->month_started == 'June') {
                                                            echo "selected";
                                                          } ?>>June</option>
                                    <option value="July" <?php if ($mastersqualification_details->month_started == 'July') {
                                                            echo "selected";
                                                          } ?>>July</option>
                                    <option value="August" <?php if ($mastersqualification_details->month_started == 'August') {
                                                              echo "selected";
                                                            } ?>>August</option>
                                    <option value="September" <?php if ($mastersqualification_details->month_started == 'September') {
                                                                echo "selected";
                                                              } ?>>September</option>
                                    <option value="October" <?php if ($mastersqualification_details->month_started == 'October') {
                                                              echo "selected";
                                                            } ?>>October</option>
                                    <option value="November" <?php if ($mastersqualification_details->month_started == 'November') {
                                                                echo "selected";
                                                              } ?>>November</option>
                                    <option value="December" <?php if ($mastersqualification_details->month_started == 'December') {
                                                                echo "selected";
                                                              } ?>>December</option>
                                  </select>
                                </td>
                                <td>
                                  <select class="form-control" id="master_year_started" name="master_year_started">
                                    <?php
                                    for ($i = 1980; $i <= 2030; $i++) {
                                    ?>
                                      <option value="<?php echo $i; ?>" <?php if ($mastersqualification_details != '' && $mastersqualification_details->year_started == $i) echo 'selected="selected"'; ?>>
                                        <?php echo $i; ?>
                                      </option>
                                    <?php
                                    }
                                    ?>
                                  </select>
                                </td>
                                <td>
                                  <select class="form-control" id="master_month_finished" name="master_month_finished">
                                    <option selected value="January" <?php if ($mastersqualification_details->month_finished == 'January') {
                                                                        echo "selected";
                                                                      } ?>>January</option>
                                    <option value="February" <?php if ($mastersqualification_details->month_finished == 'February') {
                                                                echo "selected";
                                                              } ?>>February</option>
                                    <option value="March" <?php if ($mastersqualification_details->month_finished == 'March') {
                                                            echo "selected";
                                                          } ?>>March</option>
                                    <option value="April" <?php if ($mastersqualification_details->month_finished == 'April') {
                                                            echo "selected";
                                                          } ?>>April</option>
                                    <option value="May" <?php if ($mastersqualification_details->month_finished == 'May') {
                                                          echo "selected";
                                                        } ?>>May</option>
                                    <option value="June" <?php if ($mastersqualification_details->month_finished == 'June') {
                                                            echo "selected";
                                                          } ?>>June</option>
                                    <option value="July" <?php if ($mastersqualification_details->month_finished == 'July') {
                                                            echo "selected";
                                                          } ?>>July</option>
                                    <option value="August" <?php if ($mastersqualification_details->month_finished == 'August') {
                                                              echo "selected";
                                                            } ?>>August</option>
                                    <option value="September" <?php if ($mastersqualification_details->month_finished == 'September') {
                                                                echo "selected";
                                                              } ?>>September</option>
                                    <option value="October" <?php if ($mastersqualification_details->month_finished == 'October') {
                                                              echo "selected";
                                                            } ?>>October</option>
                                    <option value="November" <?php if ($mastersqualification_details->month_finished == 'November') {
                                                                echo "selected";
                                                              } ?>>November</option>
                                    <option value="December" <?php if ($mastersqualification_details->month_finished == 'December') {
                                                                echo "selected";
                                                              } ?>>December</option>
                                  </select>
                                </td>
                                <td>
                                  <select class="form-control" id="master_year_finished" name="master_year_finished">
                                    <?php
                                    for ($i = 1980; $i <= 2030; $i++) {
                                    ?>
                                      <option value="<?php echo $i; ?>" <?php if ($mastersqualification_details != '' && $mastersqualification_details->year_finished == $i) echo 'selected="selected"'; ?>>
                                        <?php echo $i; ?>
                                      </option>
                                    <?php
                                    }
                                    ?>
                                  </select>
                                </td>
                                <td>
                                  <input type="text" name="master_grade" id="master_grade" class="form-control" value="<?php if ($mastersqualification_details != '') echo $mastersqualification_details->grade; ?>" />
                                </td>
                                <td>
                                  <select class="form-control" id="master_total" name="master_total">
                                    <option value="100" <?php if ($mastersqualification_details != '' && $mastersqualification_details->total == 100) echo 'selected="selected"'; ?>>100</option>
                                    <option value="10" <?php if ($mastersqualification_details != '' && $mastersqualification_details->total == 10) echo 'selected="selected"'; ?>>10</option>
                                    <option value="7" <?php if ($mastersqualification_details != '' && $mastersqualification_details->total == 7) echo 'selected="selected"'; ?>>7</option>
                                    <option value="4" <?php if ($mastersqualification_details != '' && $mastersqualification_details->total == 4) echo 'selected="selected"'; ?>>4</option>
                                  </select>
                                </td>
                                <td>
                                  <input type="text" name="ms_no_of_backlogs" id="no_of_backlogs" class="form-control" value="<?php if ($mastersqualification_details != '') echo $mastersqualification_details->number_backlogs; ?>" />
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div> <span class="sep col-lg-12"></span>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-12">
                          <br>
                          <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                          <button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                        </div>
                      </div>
                      <input type="hidden" name="form_type" value="qualification_details" />
                      <input type="hidden" name="user_id" value="<?= $id ?>" />
                      <input type="hidden" name="app_id" value="<?= $selected_app_id ?>" />
                      <input type="hidden" name="student_id" value="<?= $student_id ?>" />
                    </form>
                  </div>
                  <div class="tab-pane" id="desired_courses">
                    <hr>
                    <?php
                    $destination = $desired_destination;
                    ?>
                    <form class="form" action="/admin/student_registration/account/save" method="post" id="university-course-info-details">
                      <div class="form-group">
                        <div class="col-md-12">
                          <label for="desired_destination">
                            <h5>Desired Course Info</h5>
                          </label>
                        </div>
                        <!--<div class="col-md-12">
      <?php
      foreach ($countries as $val) {
      ?>
        <div class="col-md-4">
          <div class="row" style="display: flex;">
            <div class="col-md-8">
              <label>
                <h6><?php echo $val->name; ?></h6> </label>
            </div>
            <div class="col-md-4">
              <input type="checkbox" class="form-control checkBoxDesign" name="deisred_destinations[]" value="<?php echo $val->id; ?>" <?php if (isset($destination) && in_array($val->id, $destination)) echo 'checked="checked"'; ?>/> </div>
             <label><?php echo $val->name; ?></label>&nbsp;&nbsp;&nbsp;&nbsp;
          </div>
        </div>
        <?php
      }
        ?>
      </div> -->
                      </div>
                      <div class="form-group">
                        <div class="col-xs-12">
                          <!-- <label for="desired_courses">
      desired course link
      desired course name
      desired level of course
      intake
        <h5>Desired Course</h5> </label> -->
                          <div class="col-md-12 form-group">
                            <div class="col-md-6">
                              <label for="inputEmail3" class="text-uppercase"> University course link &nbsp;&nbsp; </label>
                              <input type="text" class="form-control" name="university_course_link" id="university_course_link" placeholder="Course Link" value="<?php if ($university_course_link) echo $university_course_link; ?>">
                            </div>
                            <div class="col-md-6">
                              <label for="inputEmail3" class="text-uppercase"> University course name &nbsp;&nbsp; </label>
                              <input type="text" class="form-control" name="university_course_name" id="university_course_name" placeholder="Course Name" value="<?php if ($university_course_name) echo $university_course_name; ?>">
                            </div>
                          </div>
                          <div class="col-md-12 form-group">
                            <div class="col-md-6">
                              <label for="inputEmail3" class="text-uppercase"> University Level Of Course &nbsp;&nbsp; </label>
                              <select name="university_course_level" id="university_course_level" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                                <option value="0">Select Level Of Course</option>
                                <?php
                                $selecteddegree = "";
                                foreach ($degrees as $degree) {
                                  if ($degree->id == $university_course_level) {
                                    $selecteddegree = "selected";
                                  } else {
                                    $selecteddegree = "";
                                  }
                                ?>
                                  <option value="<?php echo $degree->id; ?>" <?php echo $selecteddegree; ?>>
                                    <?php echo strtoupper($degree->name); ?>
                                  </option>
                                <?php } ?>
                              </select>
                            </div>
                            <div class="col-md-6">
                              <label for="inputEmail3" class="text-uppercase"> University Intake Month &nbsp;&nbsp; </label>
                              <select name="university_course_intake_month" id="university_course_intake_month" class="form-control" style="width:100%; height:30px; font-size:12px;" required>
                                <option value="0">Select Intake Month</option>
                                <?php
                                $slectedMonth = "";
                                foreach ($intake_month as $month) {
                                  if ($month->id == $university_course_intake_month) {
                                    $slectedMonth = "selected";
                                  } else {
                                    $slectedMonth = "";
                                  }
                                ?>
                                  <option value="<?php echo $month->id; ?>" <?php echo $slectedMonth; ?>>
                                    <?php echo strtoupper($month->name); ?>
                                  </option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-12 form-group">
                            <div class="col-md-6">
                              <label for="inputEmail3" class="text-uppercase"> University Intake Year &nbsp;&nbsp; </label>
                              <select name="university_course_intake_year" id="university_course_intake_year" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                                <option value="0">Select Intake Year</option>
                                <?php
                                $slectedYear = "";
                                foreach ($intake_year as $year) {
                                  if ($year->id == $university_course_intake_year) {
                                    $slectedYear = "selected";
                                  } else {
                                    $slectedYear = "";
                                  }
                                ?>
                                  <option value="<?php echo $year->id; ?>" <?php echo $slectedYear; ?>>
                                    <?php echo strtoupper($year->name); ?>
                                  </option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div> <span class="sep col-lg-12"></span>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-12">
                          <br>
                          <input type="hidden" name="form_type" value="university_course_info" />
                          <input type="hidden" name="user_id" value="<?= $id ?>" />
                          <input type="hidden" name="student_id" value="<?= $student_id ?>" />
                          <input type="hidden" name="app_id" value="<?= $selected_app_id ?>" />
                          <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                          <!--button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button-->
                        </div>
                      </div>
                    </form>
                  </div>
                  <!--/tab-pane-->
                  <div class="tab-pane" id="parent_detail">
                    <hr>
                    <form class="form" action="/admin/student_registration/account/save" method="post" id="parent-detail-form">
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="parent_name">
                            <h5>Parent Name</h5>
                          </label>
                          <input type="text" class="form-control" name="parent_name" id="parent_name" placeholder="Parent Name" value="<?php if ($parent_name) echo $parent_name; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="parent_occupation">
                            <h5>Parent Occupation</h5>
                          </label>
                          <input type="text" class="form-control" name="parent_occupation" id="parent_occupation" placeholder="Parent Occupation" value="<?php if ($parent_occupation) echo $parent_occupation; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="parent_mob">
                            <h5>Parent Mobile</h5>
                          </label>
                          <input type="text" class="form-control" name="parent_mob" id="parent_mob" placeholder="Parent Mobile" value="<?php if ($parent_mob) echo $parent_mob; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="parent_mob">
                            <h5>Parent Email</h5>
                          </label>
                          <input type="text" class="form-control" name="parent_email" id="parent_email" placeholder="Parent Email" value="<?php if ($parent_email) echo $parent_email; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="parent_mob">
                            <h5>Relationship</h5>
                          </label>
                          <select class="form-control" id="Relationship" name="Relationship">
                            <option value="Father" <?php if ($parent_relation == 'Father') {
                                                      echo "selected";
                                                    } ?>>Father</option>
                            <option value="Mother" <?php if ($parent_relation == 'Mother') {
                                                      echo "selected";
                                                    } ?>>Mother</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-12">
                          <br>
                          <button class="btn btn-lg prplButton pull-right" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                          <!--<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>-->
                        </div>
                      </div>
                      <input type="hidden" name="form_type" value="parent_details" />
                      <input type="hidden" name="user_id" value="<?= $id ?>" />
                      <input type="hidden" name="student_id" value="<?= $student_id ?>" />
                      <input type="hidden" name="app_id" value="<?= $selected_app_id ?>" />
                    </form>
                  </div>
                  <div class="tab-pane" id="work_exp">
                    <hr>
                    <form class="form" action="/admin/student_registration/account/save" method="post" id="parent-detail-form">
                      <div class="col-md-12" id="add_work_exp">
                        <?php foreach ($workExperiece as $row) { ?>
                          <div class="row">
                            <div class="form-group">
                              <div class="col-xs-6">
                                <label for="parent_name">
                                  <h5>Company Name</h5>
                                </label>
                                <input type="text" class="form-control" name="company_name[]" id="company_name" placeholder="Company Name" value="<?php if ($row->company_name) echo $row->company_name; ?>">
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-xs-6">
                                <label for="parent_occupation">
                                  <h5>Designation</h5>
                                </label>
                                <input type="text" class="form-control" name="designation[]" id="designation" placeholder="Designation" value="<?php if ($row->designation) echo $row->designation; ?>">
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-xs-6">
                                <label for="parent_mob">
                                  <h5>Start Date</h5>
                                </label>
                                <input type="date" name="work_start_date[]" class="form-control" id="work_start_date" value="<?= $row->start_date ?>" />
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-xs-6">
                                <label for="parent_mob">
                                  <h5>End Date</h5>
                                </label>
                                <input type="date" name="work_end_date[]" class="form-control" id="work_end_date" value="<?= $row->end_date ?>" />
                              </div>
                            </div>
                            <input class="button-remove btn btn-lg btn-danger pull-right" type="button" value="Delete" style="border-radius: 12px;margin-top: 5px" onclick="removeRow(this)" />
                          </div>
                        <?php } ?>
                      </div>

                      <button class="btn btn-lg prplButton pull-right" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                      <input type="button" value="add" onclick="addRow()" class="btn btn-lg btn-info" style="border-radius: 12px;">
                      <input type="hidden" name="form_type" value="work_exp" />
                      <input type="hidden" name="user_id" value="<?= $id ?>" />
                      <input type="hidden" name="student_id" value="<?= $student_id ?>" />
                      <input type="hidden" name="app_id" value="<?= $selected_app_id ?>" />

                    </form>
                  </div>
                  <div class="tab-pane" id="emergency_contact_detail">
                    <hr>
                    <form class="form" action="/admin/student_registration/account/save" method="post" id="Emergency-Contact-Details">
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="parent_name">
                            <h5> Name</h5>
                          </label>
                          <input type="text" class="form-control" name="e_name" id="e_name" placeholder="Name" value="<?= $e_name ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="parent_occupation">
                            <h5>Mobile Number</h5>
                          </label>
                          <input type="text" class="form-control" name="e_mobile_number" id="e_mobile_number" placeholder="Mobile Number" value="<?= $e_mobile_number ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="parent_mob">
                            <h5>Email-ID</h5>
                          </label>
                          <input type="text" class="form-control" name="e_email" id="e_email" placeholder="Email" value="<?= $e_email ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="parent_mob">
                            <h5>Relation With The Applicant </h5>
                          </label>
                          <input type="text" class="form-control" name="e_relation" id="e_relation" placeholder="Relation ex.- Father, Brother etc" value="<?= $e_relation ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-12">
                          <br>
                          <button class="btn btn-lg prplButton pull-right" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                          <!--<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>-->
                        </div>
                      </div>
                      <input type="hidden" name="form_type" value="emergency_contact_detail" />
                      <input type="hidden" name="user_id" value="<?= $id ?>" />
                      <input type="hidden" name="student_id" value="<?= $student_id ?>" />
                      <input type="hidden" name="app_id" value="<?= $selected_app_id ?>" />
                    </form>
                  </div>
                  <div class="tab-pane" id="document_upload">
                    <hr>
                    <form class="form" action="<?php echo base_url() ?>user/account/document" method="post" id="document-upload-form" enctype="multipart/form-data">
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="document_type">
                            <h5>Select Document</h5>
                          </label>
                          <select class="form-control" id="document_type" name="document_type">
                            <?php
                            foreach ($documentTypes as $documentType) {
                            ?>
                              <option value="<?php echo $documentType['id']; ?>" <?php if ($documentType['id'] == $selected_country) echo 'selected="selected"'; ?>>
                                <?php echo $documentType['name']; ?>
                              </option>
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="document">
                            <h5>Upload File</h5>
                          </label>
                          <input class="form-control" type="file" name="document" id="document">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="documents_list">
                            <h5>Uploaded Documents</h5>
                          </label>
                          <table>
                            <thead>
                              <tr>
                                <th>Type</th>
                                <th>Name</th>
                                <th>URL</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              foreach ($uploadedDocuments as $index => $uploadedDocument) {
                              ?>
                                <tr>
                                  <td>
                                    <?= $uploadedDocument['document_type'] ?>
                                  </td>
                                  <td>
                                    <?= $uploadedDocument['name'] ?>
                                  </td>
                                  <td>
                                    <a href="<?= $uploadedDocument['url'] ?>" target="_blank" style="color:#3a66ad">
                                      <?= $uploadedDocument['url'] ?>
                                    </a>
                                  </td>
                                </tr>
                              <?php
                              }
                              ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-12">
                          <br>
                          <button class="btn btn-lg prplButton pull-right" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                          <!--<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>-->
                        </div>
                      </div>
                    </form>
                  </div>

                  <div class="tab-pane" id="fund_details">
                    <hr>

                    <form class="form" action="/admin/student_registration/account/save" method="post" id="fund_detail-form">

                      <div class="form-group" id="source_of_funds">
                        <div class="col-xs-12">
                          <label for="source_of_funds">
                            <h5>Source of Funds</h5>
                          </label>
                          <div class="col-md-12">
                            <label class="radio-inline">
                              <input type="radio" name="source_of_funds" <?php if ($fundDetails->fund_type == 'self_fund') echo 'checked="checked"'; ?> id="self_fund" value="self_fund">I am self-funding</label><br>
                            <label class="radio-inline">
                              <input type="radio" name="source_of_funds" <?php if ($fundDetails->fund_type == 'family_fund') echo 'checked="checked"'; ?> id="family" value="family">I am family-funded</label><br>
                            <label class="radio-inline">
                              <input type="radio" name="source_of_funds" <?php if ($fundDetails->fund_type == 'sponsorship_fund') echo 'checked="checked"'; ?> id="Sponsorship" value="Sponsorship">I have sponsorship form a government, organization or individual</label><br>
                            <label class="radio-inline">
                              <input type="radio" name="source_of_funds" <?php if ($fundDetails->fund_type == 'other_fund') echo 'checked="checked"'; ?> id="other" value="other">Other</label><br>
                            <!-- <label class="radio-inline">
                              <input type="radio" name="competitive_exam" <?php if ($qualified_exams && $qualified_exams->competitive_exam_type == 'NOT_REQUIRED') echo 'checked="checked"'; ?> id="not_required_competitive_exam" value="NOT_REQUIRED">NOT_REQUIRED </label> -->
                            <br><small>You must show proof of funding as per program requirements, before ASU can issue your l-20 document. You need an 1-20 to apply for a student visa.</small>
                          </div>
                        </div></span>
                      </div>
                      <div class="form-group" id="self" name="self_fund" style="display: block;">
                        <div class="form-group">
                          <div class="col-xs-12">

                          </div>
                        </div>
                      </div>
                      <?php if ($fundDetails->fund_type == 'family_fund') { ?>
                        <div class="form-group" id="myfamliy" name="famliy_fund">

                          <div class="form-group">
                            <div class="col-xs-12">
                              <label for="parent_name">
                                <h5>Guarantor name</h5>
                              </label>

                              <input type="text" class="form-control" name="family_guarantor_name" id="family_guarantor_name" placeholder="guarantor_name Name" value="<?php if ($fundDetails->guarantor_name) echo $fundDetails->guarantor_name; ?>">

                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-xs-12">
                              <label for="Guarantor_relationship">
                                <h5>Guarantor relationship to student</h5>
                              </label>
                              <select class="form-control" id="family_Guarantor_relationship" name="family_Guarantor_relationship">
                                <option value="Father" <?php if ($fundDetails->guarantor_relation == 'Father') {
                                                          echo "selected";
                                                        } ?>>Father</option>
                                <option value="Mother" <?php if ($fundDetails->guarantor_relation == 'Mother') {
                                                          echo "selected";
                                                        } ?>>Mother</option>
                              </select>
                            </div>
                          </div>

                        </div>
                      <?php } else { ?>
                        <div class="form-group" id="myfamliy" name="famliy_fund" style="display: none;">

                          <div class="form-group">
                            <div class="col-xs-12">
                              <label for="parent_name">
                                <h5>Guarantor name</h5>
                              </label>

                              <input type="text" class="form-control" name="family_guarantor_name" id="family_guarantor_name" placeholder="guarantor_name Name" value="">

                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-xs-12">
                              <label for="Guarantor_relationship">
                                <h5>Guarantor relationship to student</h5>
                              </label>
                              <select class="form-control" id="family_Guarantor_relationship" name="family_Guarantor_relationship">
                                <option value="Father">Father</option>
                                <option value="Mother">Mother</option>
                              </select>
                            </div>
                          </div>

                        </div>
                      <?php } ?>
                      <?php if ($fundDetails->fund_type == 'sponsorship_fund') { ?>
                        <div class="form-group" id="sponsor" name="sponsorship_fund">

                          <div class="form-group">
                            <div class="col-xs-12">
                              <label for="parent_name">
                                <h5>Sponsor name</h5>
                              </label>
                              <input type="text" class="form-control" name="sponsor_guarantor_name" id="sponsor_guarantor_name" placeholder="guarantor_name Name" value="<?php if ($fundDetails->sponsor_name) echo $fundDetails->sponsor_name; ?>">
                            </div>
                          </div>

                        </div>
                      <?php } else { ?>
                        <div class="form-group" id="sponsor" name="sponsorship_fund" style="display: none;">

                          <div class="form-group">
                            <div class="col-xs-12">
                              <label for="parent_name">
                                <h5>Sponsor name</h5>
                              </label>
                              <input type="text" class="form-control" name="sponsor_guarantor_name" id="sponsor_guarantor_name" placeholder="guarantor_name Name" value="">
                            </div>
                          </div>

                        </div>
                      <?php } ?>
                      <?php if ($fundDetails->fund_type == 'other_fund') { ?>
                        <div class="form-group" id="otheroption" name="other_fund">

                          <div class="form-group">
                            <div class="col-xs-12">
                              <label for="parent_name">
                                <h5>Guarantor name</h5>
                              </label>
                              <input type="text" class="form-control" name="other_guarantor_name" id="other_guarantor_name" placeholder="guarantor_name Name" value="<?php if ($fundDetails->guarantor_name) echo $fundDetails->guarantor_name; ?>">

                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-xs-12">
                              <label for="Guarantor_relationship">
                                <h5>Guarantor relationship to student</h5>
                              </label>
                              <select class="form-control" id="other_Guarantor_relationship" name="other_Guarantor_relationship">
                                <option value="Father" <?php if ($fundDetails->guarantor_relation == 'Father') {
                                                          echo "selected";
                                                        } ?>>Father</option>
                                <option value="Mother" <?php if ($fundDetails->guarantor_relation == 'Mother') {
                                                          echo "selected";
                                                        } ?>>Mother</option>
                              </select>
                            </div>
                          </div>

                        </div>
                      <?php } else { ?>
                        <div class="form-group" id="otheroption" name="other_fund" style="display: none;">

                          <div class="form-group">
                            <div class="col-xs-12">
                              <label for="parent_name">
                                <h5>Guarantor name</h5>
                              </label>
                              <input type="text" class="form-control" name="other_guarantor_name" id="other_guarantor_name" placeholder="guarantor_name Name" value="">

                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-xs-12">
                              <label for="Guarantor_relationship">
                                <h5>Guarantor relationship to student</h5>
                              </label>
                              <select class="form-control" id="other_Guarantor_relationship" name="other_Guarantor_relationship">
                                <option value="Father">Father</option>
                                <option value="Mother">Mother</option>
                              </select>
                            </div>
                          </div>

                        </div>
                      <?php } ?>
                      <div class="form-group">
                        <div class="col-xs-12">
                          <br>
                          <button class="btn btn-lg prplButton pull-right" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                          <!--<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>-->
                        </div>
                      </div>
                      <input type="hidden" name="form_type" value="fund_details" />
                      <input type="hidden" name="user_id" value="<?= $id ?>" />
                      <input type="hidden" name="student_id" value="<?= $student_id ?>" />
                      <input type="hidden" name="app_id" value="<?= $selected_app_id ?>" />
                    </form>
                  </div>

                </div>
                <!--/tab-pane-->
              </div>
            </div>

            <script type="text/javascript">
              function addRow() {
                document.querySelector('#add_work_exp').insertAdjacentHTML(
                  'afterend',
                  `<div class="col-md-12">
                          <div class="form-group">
                            <div class="col-xs-6">
                              <label for="parent_name">
                                <h5>Company Name</h5> </label>
                              <input type="text" class="form-control" name="company_name[]" id="company_name" placeholder="Company Name" /> </div>
                          </div>
                          <div class="form-group">
                            <div class="col-xs-6">
                              <label for="parent_occupation">
                                <h5>Designation</h5> </label>
                              <input type="text" class="form-control" name="designation[]" id="designation" placeholder="Designation" /> </div>
                          </div>
                          <div class="form-group">
                            <div class="col-xs-6">
                              <label for="parent_mob">
                                <h5>Start Date</h5> </label>
                              <input type="date" name="work_start_date[]" class="form-control" id="work_start_date"  /> </div>
                          </div>
                          <div class="form-group">
                            <div class="col-xs-6">
                              <label for="parent_mob">
                                <h5>End Date</h5> </label>
                              <input type="date" name="work_end_date[]" class="form-control" id="work_end_date"  /> </div>
                          </div>
                          <input class="button-remove btn btn-lg btn-danger pull-right" type="button" value="Delete" style="border-radius: 12px;margin-top: 5px" onclick="removeRow(this)" />
                          <input type="hidden" name="form_type" value="work_exp" />
                          <input type="hidden" name="user_id" value="<?= $id ?>" />
                        </div>`
                )
              }

              // function removeRow (input) {
              //   //$(this).closest(".box").remove();
              //   input.closest(".box")remove();
              // }

              function removeRow(input) {
                input.parentNode.remove()
              }
            </script>

            <div id="Applications" class="tabcontent">
              <!-- <div class="col-sm-4">
      <h3>Applications1</h3>
      <p>Upload Documents</p>
      </div>
      <div class="col-sm-8">
      <h3>Applications2</h3>
      <p>Upload Documents</p>
      </div> -->
              <div class="tabordion">
                <?php
                if ($myApplications) {
                  $i = 1;
                  foreach ($myApplications as $myApplication) {
                ?>
                    <section id="section<?= $i ?>">
                      <input type="radio" name="sections" id="option<?= $i ?>">
                      <label for="option<?= $i ?>">
                        <h6><i class="fas fa-book">&nbsp;</i><?= $myApplication['course_name'] ?></h6>
                        <h6><i class="fas fa-graduation-cap">&nbsp;</i><?= $myApplication['college_name'] ?></h6>
                        <h6><i class="fas fa-university">&nbsp;</i><?= $myApplication['name'] ?></h6>
                      </label>
                      <article>
                        <div class="newTab" id="tab<?= $i ?>">
                          <div class="tab">
                            <button class="tablinks1 active" onclick="openApplicationTab(event, 'DocUp<?= $myApplication['name'] ?>')" id="defaultOpenthis">Required Documents</button>
                            <button class="tablinks1" onclick="openApplicationTab(event, 'Comments<?= $myApplication['name'] ?>')">Comments</button>
                            <button class="tablinks1" onclick="openApplicationTab(event, 'RequiredInfo')">Required Info</button>
                          </div>
                          <div id="DocUp<?= $myApplication['name'] ?>" class="tabcontent1 col-md-12">
                            <h3>Doc Upload</h3>
                            <!-- <h2><?= $myApplication['name'] ?></h2>
      <p><?= $myApplication['college_name'] ?></p>
      <p><?= $myApplication['course_name'] ?></p> -->
                            <?php
                            foreach ($myApplicationDocList as $keyd => $valued) {
                              // code...
                            ?>
                              <div class="col-md-12 docUploadBox noPaddingCol">
                                <?php if ($valued['required'] == 1) {  ?>
                                  <div class="col-md-1 verticalDiv requiredDoc">
                                    <h6 class="textVertical">
                                      Required
                                    </h6>
                                  </div>
                                <?php } else { ?>
                                  <div class="col-md-1 verticalDiv optDoc">
                                    <h6 class="textVertical">
                                      Optional
                                    </h6>
                                  </div>
                                <?php } ?>
                                <div class="col-md-11 noPaddingCol">
                                  <div class="col-md-12">
                                    <div class="col-md-2 statusBox">
                                      <h5>
                                        <?php if ($valued['document_url']) {
                                          echo " <i class='fas fa-check'></i><br> Completed";
                                        } else {
                                          echo " <i class='fas fa-times'></i><br> Pending";
                                        } ?>
                                      </h5>
                                    </div>
                                    <div class="col-md-9 noPaddingCol">
                                      <div class="col-md-12 noPaddingCol">
                                        <div class="col-md-8 docName">
                                          <h5><?= $valued['name'] ?></h5>
                                          <p>Default Certificate Education History Prototype</p>
                                        </div>
                                        <div class="col-md-4">
                                          <form method="post" action="" enctype="multipart/form-data" id="myform<?= $myApplication[" id "] ?><?= $valued["id "] ?>">
                                            <input class="form-control" type="file" name="document<?= $myApplication[" id "] ?><?= $valued["id "] ?>" id="document<?= $myApplication[" id "] ?><?= $valued["id "] ?>">
                                            <button class="btn btn-md prplButton" style="margin-top: 50%;" onclick="uploadAppDoc(<?= $myApplication[" id "] ?>,<?= $valued["id "] ?>,<?= $myApplication["user_id "] ?>)"><i class="fas fa-upload">&nbsp;</i>Upload</button>
                                          </form>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-12 docUplodedName">
                                    <?php if ($valued['document_url']) { ?>
                                      <h6><a href="<?= $valued['document_url'] ?>" target="_blank" class="docUploadNameLink"> <?= $valued['name'] ?> View </a></h6>
                                    <?php } else { ?>
                                      <h6>No Document</h6>
                                    <?php } ?>
                                  </div>
                                </div>
                              </div>
                            <?php } ?>
                            <!--<div class="col-md-12 docUploadBox noPaddingCol">
      <div class="col-md-1 verticalDiv requiredDoc">
         <h6 class="textVertical">Required</h6>
      </div>
      <div class="col-md-11 noPaddingCol" >
         <div class="col-md-12">
            <div class="col-md-2 statusBox">
               <h5><i class="fas fa-check"></i><br>
            Completed</h5>
            </div>
            <div class="col-md-9 noPaddingCol" >
               <div class="col-md-12 noPaddingCol">
                  <div class="col-md-10 docName">
                     <h5>IELTS (Automated)</h5>
                     <p>IELTS</p>
                  </div>
                  <div class="col-md-2">
                     <button class="btn btn-md prplButton" style="    margin-top: 50%;"><i class="fas fa-upload">&nbsp;</i>Upload</button>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-12 docUplodedName">
            <h6><a href="#" target="_blank" class="docUploadNameLink">IELTS.pdf</a></h6>
         </div>
      </div>
      </div>
      <div class="col-md-12 docUploadBox noPaddingCol">
      <div class="col-md-1 verticalDiv optDoc">
         <h6 class="textVertical">Optional</h6>
      </div>
      <div class="col-md-11 noPaddingCol" >
         <div class="col-md-12">
            <div class="col-md-2 statusBox">
               <h5><i class="fas fa-times"></i><br>
                  Pending
               </h5>
            </div>
            <div class="col-md-9 noPaddingCol" >
               <div class="col-md-12 noPaddingCol">
                  <div class="col-md-10 docName">
                     <h5>Backup/Alternative Program Option</h5>
                     <p>Backup/Alternative Program Option</p>
                  </div>
                  <div class="col-md-2">
                     <button class="btn btn-md prplButton" style="    margin-top: 50%;"><i class="fas fa-upload">&nbsp;</i>Upload</button>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-12 docUplodedName">
            <h6>No Document</h6>
         </div>
      </div>
      </div>-->
                          </div>
                          <div id="Comments<?= $myApplication['name'] ?>" class="tabcontent1 col-md-12">
                            <h3>Comments</h3>
                            <button type="button" class="btn btn-md prplButton btnComment" data-toggle="collapse" data-target="#<?= $myApplication[" id "] ?>">New Comment</button>
                            <div id="<?= $myApplication[" id "] ?>" class="collapse">
                              <form>
                                <div class="form-group">
                                  <label for="comment" style="border-right: 3px solid #ffffff;">Comment:</label>
                                  <textarea class="form-control" rows="5" id="notes<?= $myApplication[" id "] ?>" style="height: auto !important;"></textarea>
                                  <button class="btn btn-md prplButton" type="button" onclick="addNotes(<?= $myApplication[" id "] ?>,<?= $myApplication["user_id "] ?>)"><i class="glyphicon glyphicon-ok-sign"></i>Add Comment</button>
                                </div>
                              </form>
                            </div>
                            <?php if (isset($myApplication['noteslist'])) {
                              foreach ($myApplication['noteslist'] as $keyn => $valuen) {

                            ?>
                                <div class="media comment-box">
                                  <div class="media-left">
                                    <a href="#"> <img class="img-responsive user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png"> </a>
                                  </div>
                                  <div class="media-body">
                                    <h5 class="media-heading"><?php echo $valuen['created_by_name']; ?> <span style="float: right;"><?php $webinarDate = date('D, d F Y H:m:s', strtotime($valuen['added_time']));
                                                                                                                                    echo "$webinarDate"; ?></span></h5>
                                    <p>
                                      <?= $valuen['comment'] ?>
                                    </p>
                                  </div>
                                </div>
                            <?php }
                            } ?>
                            <!--  <div class="media comment-box">
      <div class="media-left">
         <a href="#">
         <img class="img-responsive user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
         </a>
      </div>
      <div class="media-body">
         <h5 class="media-heading">ABC Span <span style="float: right;">July 22nd 2020, 23:59</span></h5>
         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
      </div>
      </div>
      <h2><?= $myApplication['name'] ?></h2>
        <p><?= $myApplication['college_name'] ?></p>
        <p><?= $myApplication['course_name'] ?></p>  -->
                          </div>
                          <div id="RequiredInfo" class="tabcontent1 col-md-12">
                            <h3 style="display: contents;">Required Extra Information</h3>
                            <div class="col-md-12">
                              <form class="form requiredInfoForm" action="##" method="post" id="">
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="first_name">
                                      <h5>Name</h5>
                                    </label>
                                    <input type="text" class="form-control" name="name" id="student_name" placeholder="Name" value="">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="phone">
                                      <h5>Mobile</h5>
                                    </label>
                                    <input type="text" class="form-control" name="phone" id="student_phone" placeholder="Mobile" value="">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="email">
                                      <h5>Email</h5>
                                    </label>
                                    <input type="email" class="form-control" name="email" id="student_email" placeholder="Email" value="">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="dob">
                                      <h5>DOB</h5>
                                    </label>
                                    <input type="date" name="dob" class="form-control" id="student_dob" value="" />
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="address">
                                      <h5>Address</h5>
                                    </label>
                                    <input type="text" class="form-control" name="address" id="student_address" placeholder="Address" value="">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="country">
                                      <h5>Country</h5>
                                    </label>
                                    <select name="locationcountries" id="student_locationcountries" class="form-control">
                                      <option value="0">Select Country</option>
                                      <?php
                                      foreach ($locationcountries as $location) {
                                      ?>
                                        <option value="<?= $location->id ?>" <?php if ($location->id == $selected_country) echo 'selected="selected"'; ?>>
                                          <?= $location->name ?>
                                        </option>
                                      <?php
                                      }
                                      ?>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="state">
                                      <h5>State</h5>
                                    </label>
                                    <select name="state" id="student_state" class="form-control">
                                      <option value="0">Select State</option>
                                      <?php
                                      foreach ($related_state_list as $location) {
                                      ?>
                                        <option value="<?= $location->zone_id ?>" <?php if ($location->zone_id == $selected_state) echo 'selected="selected"'; ?>>
                                          <?= $location->name ?>
                                        </option>
                                      <?php
                                      }
                                      ?>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="city">
                                      <h5>City</h5>
                                    </label>
                                    <select name="city" id="student_city" class="form-control">
                                      <option value="0">Select City</option>
                                      <?php
                                      foreach ($related_city_list as $location) {
                                      ?>
                                        <option value="<?= $location->id ?>" <?php if ($location->id == $city) echo 'selected="selected"'; ?>>
                                          <?= $location->city_name ?>
                                        </option>
                                      <?php
                                      }
                                      ?>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="zipcode">
                                      <h5>Zipcode</h5>
                                    </label>
                                    <input type="text" class="form-control" name="zip" id="student_zip" placeholder="Zipcode" value="">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="type">
                                      <h5>Type</h5>
                                    </label>
                                  </div>
                                  <div class="col-xs-6" style="display: inline-flex;">
                                    <input type="checkbox" id="Type1" name="Type1" value="Bike">
                                    <label for="Type1"> Type1</label>
                                    <br>
                                    <input type="checkbox" id="Type2" name="Type2" value="Car">
                                    <label for="Type2"> Type2</label>
                                    <br>
                                    <input type="checkbox" id="Type3" name="Type3" value="Boat">
                                    <label for="Type3"> Type3</label>
                                    <br>
                                    <br>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="gender">
                                      <h5>Gender</h5>
                                    </label>
                                    <div class="col-md-12">
                                      <label class="radio-inline">
                                        <input type="radio" name="gender" <?= $gender == 'M' ? 'checked' : '' ?>>Male </label>
                                      <label class="radio-inline">
                                        <input type="radio" name="gender" <?= $gender == 'F' ? 'checked' : '' ?>>Female </label>
                                      <label class="radio-inline">
                                        <input type="radio" name="gender" <?= $gender == 'O' ? 'checked' : '' ?>>Other </label>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-12">
                                    <br>
                                    <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                                    <button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </article>
                    </section>
                <?php
                    $i++;
                  }
                } else {
                  echo "No Application Selected ...!!! <br/> Please Select University From List  <br/>";
                  echo "<a class='btn btn-large btn-danger' href='/counsellor/student/offerlist/view/" . $id . "' style='color: #f6882c'> <b>Shortlist Universities / Courses</b> </a>";
                }
                ?>
              </div>
            </div>
            <div id="Documents" class="tabcontent">
              <div class="col-md-12 boxAppList docTab" style="padding: 2% 2%;    background-color: white;">
                <h3>Documents</h3>
                <hr>
                <div class="tab">
                  <button class="tablinks2" onclick="openDocType(event, 'BASIC')" id="defaultOpenTab">Basic</button>
                  <button class="tablinks2" onclick="openDocType(event, 'ACADEMIC')">Academic </button>
                  <button class="tablinks2" onclick="openDocType(event, 'TESTS')">Entrance Exam</button>
                  <button class="tablinks2" onclick="openDocType(event, 'APPLICATION')">Other Applications</button>
                  <button class="tablinks2" onclick="openDocType(event, 'APPLICATION_SPECIFIC')"> Specific Application Materials</button>
                </div>
                <?php
                foreach ($new_doc_list as $keydl => $valuedl) {
                  // code...

                ?>
                  <div id="<?php echo "$keydl"; ?>" class="tabcontent2">
                    <h3><?php echo "$keydl"; ?></h3>
                    <table class="table table-bordered">
                      <thead>
                        <th style="width: 20%;">Name</th>
                        <th style="width: 40%;">Description</th>
                        <th>Upload</th>
                        <th style="width: 10%;">Status</th>
                      </thead>
                      <tbody>
                        <?php
                        foreach ($valuedl as $keyl => $valuel) {

                        ?>
                          <tr>
                            <td>
                              <?php echo $valuel['display_name']; ?>
                            </td>
                            <td>
                              <?php echo $valuel['description']; ?>
                            </td>
                            <td>
                              <div class="col-md-12"> <a class="btn btn-info" onclick='popup(<?php echo json_encode($valuel); ?>);return false;'> Upload Doc </a> </div>
                            </td>
                            <td>
                              <?php $status =  $valuel['user_doc_list'] ? 'UPLOADED' : 'PENDING';
                              echo $status; ?>
                            </td>
                            <!-- <td ng-if="listvalue.document_count"><a class="btn btn-info" data-toggle="modal" data-target="#modalDocument" ng-click="viewDocumentkById(listvalue)" >{{listvalue.document_count}} Document Uploaded</a></td>
      <td ng-if="!listvalue.document_count"> Pending </a></td> -->
                          </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                <?php } ?>

              </div>
            </div>
            <div class="modal fade" id="modalDocument" role="dialog" style="z-index: 10000;">
              <div class="modal-dialog" style="    width: auto;">
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Upload Document</h4>
                  </div>
                  <div class="modal-body">
                    <form name="" class="col-md-12 ">
                      <div class="col-md-12">
                        <label id="title_name"> Doc Name </label>
                        <br />
                        <br />
                        <select class="form-control" name="doc_id" id="doc_id" required>
                          <option value=""> Select Uploading Document</option>
                        </select>
                      </div>
                      <br />
                      <br />
                      <div class="col-md-12">
                        <input type="file" name="userfile" id="userfile" required>
                      </div>
                      <br />
                      <br />
                      <div class="col-md-12">
                        <label> Document Name </label>
                        <input type="text" class="form-group form-control" placeholder="Input Document Name" name="userdocname" id="userdocname" style="height: auto;"></input>
                      </div>
                      <div class="col-md-12" style="text-align: center;">
                        <button class="btn btn-primary btn-sm" type="button" onclick="uploadDoc()"> Upload </button>
                      </div>
                    </form>
                    <br>
                    <hr>
                    <div>
                      <div class="row">
                        <div class="col-md-12">
                          <h3>Uploaded Document List</h3>
                          <table class="table table-bordered">
                            <thead>
                              <th>Document Name</th>
                              <th>File Name</th>
                              <th>View</th>
                              <th>Delete</th>
                            </thead>
                            <tbody id="table_content">
                              <tr>
                                <td>display_name</td>
                                <td style="max-width: 250px;overflow: auto;">url</td>
                                <td><a href="" target="_blank" class="btn btn-sm btn-success">View</a></td>
                                <td>
                                  <button class="btn btn-danger" type="button" ng-click=""> Delete</button>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
            <div id="Payments" class="tabcontent">
              <h3>Payments</h3>
              <div class="col-md-12" style="    background-color: white;
      padding: 2% 1%">
                <!--  <form class="form" action="##" method="post" id="">
      <div class="form-group">
         <div class="col-xs-6">
            <label for="first_name">
               <h5>Name</h5>
            </label>
            <input type="text" class="form-control" name="name" id="student_name" placeholder="Name" value="">
         </div>
      </div>
      <div class="form-group">
         <div class="col-xs-6">
            <label for="phone">
               <h5>Mobile</h5>
            </label>
            <input type="text" class="form-control" name="phone" id="student_phone" placeholder="Mobile" value="">
         </div>
      </div>
      <div class="form-group">
         <div class="col-xs-6">
            <label for="email">
               <h5>Email</h5>
            </label>
            <input type="email" class="form-control" name="email" id="student_email" placeholder="Email" value="">
         </div>
      </div>
      <div class="form-group">
         <div class="col-xs-6">
            <label for="dob">
               <h5>DOB</h5>
            </label>
            <input type="date" name="dob"  class="form-control" id="student_dob" value=""/>
         </div>
      </div>
      <div class="form-group">
         <div class="col-xs-6">
            <label for="address">
               <h5>Address</h5>
            </label>
            <input type="text" class="form-control" name="address" id="student_address" placeholder="Address" value="">
         </div>
      </div>
      <div class="form-group">
         <div class="col-xs-6">
            <label for="country">
               <h5>Country</h5>
            </label>
            <select name="locationcountries" id="student_locationcountries" class="form-control">
               <option value="0">Select Country</option>
               <?php
                foreach ($locationcountries as $location) {
                ?>
               <option value="<?= $location->id ?>" <?php if ($location->id == $selected_country) echo 'selected="selected"'; ?>><?= $location->name ?></option>
               <?php
                }
                ?>
            </select>
         </div>
      </div>
      <div class="form-group">
         <div class="col-xs-6">
            <label for="state">
               <h5>State</h5>
            </label>
            <select name="state" id="student_state" class="form-control">
               <option value="0">Select State</option>
               <?php
                foreach ($related_state_list as $location) {
                ?>
               <option value="<?= $location->zone_id ?>" <?php if ($location->zone_id == $selected_state) echo 'selected="selected"'; ?>><?= $location->name ?></option>
               <?php
                }
                ?>
            </select>
         </div>
      </div>
      <div class="form-group">
         <div class="col-xs-6">
            <label for="city">
               <h5>City</h5>
            </label>
            <select name="city" id="student_city" class="form-control">
               <option value="0">Select City</option>
               <?php
                foreach ($related_city_list as $location) {
                ?>
               <option value="<?= $location->id ?>" <?php if ($location->id == $city) echo 'selected="selected"'; ?>><?= $location->city_name ?></option>
               <?php
                }
                ?>
            </select>
         </div>
      </div>
      <div class="form-group">
         <div class="col-xs-6">
            <label for="zipcode">
               <h5>Zipcode</h5>
            </label>
            <input type="text" class="form-control" name="zip" id="student_zip" placeholder="Zipcode" value="">
         </div>
      </div>
      <div class="form-group">
         <div class="col-xs-6">
            <label for="gender">
               <h5>Gender</h5>
            </label>
            <div class="col-md-12">
               <label class="radio-inline">
               <input type="radio" name="gender" <?= $gender == 'M' ? 'checked' : '' ?>>Male
               </label>
               <label class="radio-inline">
               <input type="radio" name="gender" <?= $gender == 'F' ? 'checked' : '' ?>>Female
               </label>
               <label class="radio-inline">
               <input type="radio" name="gender" <?= $gender == 'O' ? 'checked' : '' ?>>Other
               </label>
            </div>
         </div>
      </div>
      <div class="form-group">
         <div class="col-xs-12">
            <br>
            <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
            <button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
         </div>
      </div>
      </form>-->
              </div>
            </div>
          </div>
        </div>
        <div id="optional" class="tabsubcontent">
          <div class="col-sm-12 mytabs">
            <button class="tablink1" onclick="openPage1('Profile1', this, '#f7f7f7')" id="defaultOpen1">Profile</button>
            <button class="tablink1" onclick="openPage1('Documents1', this, '#f7f7f7')" id="documentTab1">Documents</button>
            <!--<button class="tablink1" onclick="openPage1('Applications1', this, '#f7f7f7')" id="applicationsTab1">Applications (
      <?php if ($myApplications) {
        echo count($myApplications);
      } else {
        echo count($myApplications);
      } ?> ) </button>-->
            <button class="tablink1" onclick="openPage1('Payments1', this, '#f7f7f7')" id="paymentsTab1">Payments</button>
            <div id="Profile1" class="tabcontent11" style=" margin-top: 7%;">
              <!-- tabs inside -->
              <div class="col-md-12 boxAppList" style="padding: 2% 2%;    background-color: white;">
                <ul class="nav nav-tabs" style="padding-top: 9px;padding-left: 9px;">
                  <li class="active"><a data-toggle="tab" href="#home1"><b class="tabName">
                        Personal Detail
                        <?php if ($personal_detail_status_lw == "COMPLETE") {
                          echo "(COMPLETED)";
                        } else {
                          echo "(INCOMPLETED)";
                        } ?>
                      </b></a></li>
                  <li><a data-toggle="tab" href="#app_credential_detail"><b class="tabName">
                        Application credential Detail <?php if ($application_credential_detail_status == "COMPLETE") {
                                                        echo "(COMPLETED)";
                                                      } else {
                                                        echo "(INCOMPLETED)";
                                                      } ?>
                      </b></a></li>
                  <li><a data-toggle="tab" href="#other"><b class="tabName">
                        Other Detail <?php if ($other_detail_status == "COMPLETE") {
                                        echo "(COMPLETED)";
                                      } else {
                                        echo "(INCOMPLETED)";
                                      } ?>
                      </b></a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="home1">
                    <hr>
                    <form class="form" action="/admin/student_registration/account/save" method="post" id="personal-detail-optional-form">
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="first_name">
                            <h5>First Name (as per passport)</h5>
                          </label>
                          <input type="text" class="form-control" name="name" id="name" placeholder="First Name" value="<?= $name ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="first_name">
                            <h5>Last Name (as per passport)</h5>
                          </label>
                          <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" value="<?= $last_name ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="phone">
                            <h5>Mobile</h5>
                          </label>
                          <input type="text" class="form-control" name="phone" id="student_phone" placeholder="Mobile" value="<?= $phone ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="email">
                            <h5>Email</h5>
                          </label>
                          <input type="email" class="form-control" name="email" id="student_email" placeholder="Email" value="<?= $email ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-12">
                          <br>
                          <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                          <button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                        </div>
                      </div>
                      <input type="hidden" name="form_type" value="personal_details_optional" />
                      <input type="hidden" name="user_id" value="<?= $id ?>" />
                      <input type="hidden" name="student_id" value="<?= $student_id ?>" />
                      <input type="hidden" name="app_id" value="<?= $selected_app_id ?>" />
                    </form>
                    <hr>
                  </div>
                  <!--/tab-pane-->
                  <div class="tab-pane" id="app_credential_detail">
                    <hr>
                    <form class="form" action="/admin/student_registration/account/save" method="post" id="qualification-detail-form">
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="">
                            <h5>University application login link</h5>
                          </label>
                          <input type="text" class="form-control" name="university_application_link" id="university_application_link" placeholder="University Application Link" value="<?php echo $university_application_link; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="">
                            <h5>University application Id</h5>
                          </label>
                          <input type="text" class="form-control" name="university_application_id" id="university_application_id" placeholder="University Application Id" value="<?php echo $university_application_id; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="">
                            <h5>University Student Id</h5>
                          </label>
                          <input type="text" class="form-control" name="university_student_id" id="university_student_id" placeholder="University Student Id" value="<?php echo $university_student_id; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="">
                            <h5>Username (logged in for university)</h5>
                          </label>
                          <input type="text" class="form-control" name="university_application_username" id="university_application_username" placeholder="University Application Username" value="<?php echo $university_application_username; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="">
                            <h5>Password</h5>
                          </label>
                          <input type="text" class="form-control" name="university_application_password" id="university_application_password" placeholder="University Application Password" value="<?php echo $university_application_password; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="">
                            <h5>Course applied for</h5>
                          </label>
                          <input type="text" class="form-control" name="university_application_course" id="university_application_course" placeholder="University Application Course" value="<?php echo $university_application_course; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-12">
                          <br>
                          <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                          <button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                        </div>
                      </div>
                      <input type="hidden" name="form_type" value="app_credential_detail" />
                      <input type="hidden" name="user_id" value="<?= $id ?>" />
                      <input type="hidden" name="student_id" value="<?= $student_id ?>" />
                      <input type="hidden" name="app_id" value="<?= $selected_app_id ?>" />
                    </form>
                  </div>
                  <!--/tab-pane-->
                  <div class="tab-pane" id="other">
                    <hr>
                    <form class="form" action="/admin/student_registration/account/save" method="post" id="other-details-optional-form">
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="passport_availalibility">
                            <h5>Passport Number</h5>
                          </label>
                          <input type="text" class="form-control" name="passport_number" id="passport_number" placeholder="Passport Number" value="<?= $passport_number ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="dob">
                            <h5>Passport Issue Date</h5>
                          </label>
                          <input type="date" name="passport_issue_date" class="form-control" id="passport_issue_date" value="<?= $passport_issue_date ?>" />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="dob">
                            <h5>Passport Expiry Date</h5>
                          </label>
                          <input type="date" name="passport_expiry_date" class="form-control" id="passport_expiry_date" value="<?= $passport_expiry_date ?>" />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-12">
                          <br>
                          <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                          <button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                        </div>
                      </div>
                      <input type="hidden" name="form_type" value="other_details_optional" />
                      <input type="hidden" name="user_id" value="<?= $id ?>" />
                      <input type="hidden" name="student_id" value="<?= $student_id ?>" />
                      <input type="hidden" name="app_id" value="<?= $selected_app_id ?>" />
                    </form>
                  </div>
                  <div class="tab-pane" id="work_exp1">
                    <hr>
                    <form class="form" action="/admin/student_registration/account/save" method="post" id="parent-detail-form">
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="parent_name">
                            <h5>Company Name</h5>
                          </label>
                          <input type="text" class="form-control" name="company_name" id="company_name" placeholder="Company Name" value="<?php if ($company_name) echo $company_name; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="parent_occupation">
                            <h5>Designation</h5>
                          </label>
                          <input type="text" class="form-control" name="designation" id="designation" placeholder="Designation" value="<?php if ($designation) echo $designation; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="parent_mob">
                            <h5>Start Date</h5>
                          </label>
                          <input type="date" name="work_start_date" class="form-control" id="work_start_date" value="<?= $work_start_date ?>" />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="parent_mob">
                            <h5>End Date</h5>
                          </label>
                          <input type="date" name="work_end_date" class="form-control" id="work_end_date" value="<?= $work_end_date ?>" />
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-12">
                          <br>
                          <button class="btn btn-lg prplButton pull-right" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                          <!--<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>-->
                        </div>
                      </div>
                      <input type="hidden" name="form_type" value="work_exp" />
                      <input type="hidden" name="user_id" value="<?= $id ?>" />
                      <input type="hidden" name="student_id" value="<?= $student_id ?>" />
                      <input type="hidden" name="app_id" value="<?= $selected_app_id ?>" />
                    </form>
                  </div>
                  <div class="tab-pane" id="emergency_contact_detail1">
                    <hr>
                    <form class="form" action="/admin/student_registration/account/save" method="post" id="Emergency-Contact-Details">
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="parent_name">
                            <h5> Name</h5>
                          </label>
                          <input type="text" class="form-control" name="e_name" id="e_name" placeholder="Name" value="<?= $e_name ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="parent_occupation">
                            <h5>Mobile Number</h5>
                          </label>
                          <input type="text" class="form-control" name="e_mobile_number" id="e_mobile_number" placeholder="Mobile Number" value="<?= $e_mobile_number ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="parent_mob">
                            <h5>Email-ID</h5>
                          </label>
                          <input type="text" class="form-control" name="e_email" id="e_email" placeholder="Email" value="<?= $e_email ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="parent_mob">
                            <h5>Relation With The Applicant </h5>
                          </label>
                          <input type="text" class="form-control" name="e_relation" id="e_relation" placeholder="Relation ex.- Father, Brother etc" value="<?= $e_relation ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-12">
                          <br>
                          <button class="btn btn-lg prplButton pull-right" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                          <!--<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>-->
                        </div>
                      </div>
                      <input type="hidden" name="form_type" value="emergency_contact_detail" />
                      <input type="hidden" name="user_id" value="<?= $id ?>" />
                      <input type="hidden" name="student_id" value="<?= $student_id ?>" />
                      <input type="hidden" name="app_id" value="<?= $selected_app_id ?>" />
                    </form>
                  </div>
                  <div class="tab-pane" id="document_upload1">
                    <hr>
                    <form class="form" action="<?php echo base_url() ?>user/account/document" method="post" id="document-upload-form" enctype="multipart/form-data">
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="document_type">
                            <h5>Select Document</h5>
                          </label>
                          <select class="form-control" id="document_type" name="document_type">
                            <?php
                            foreach ($documentTypes as $documentType) {
                            ?>
                              <option value="<?php echo $documentType['id']; ?>" <?php if ($documentType['id'] == $selected_country) echo 'selected="selected"'; ?>>
                                <?php echo $documentType['name']; ?>
                              </option>
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="document">
                            <h5>Upload File</h5>
                          </label>
                          <input class="form-control" type="file" name="document" id="document">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-6">
                          <label for="documents_list">
                            <h5>Uploaded Documents</h5>
                          </label>
                          <table>
                            <thead>
                              <tr>
                                <th>Type</th>
                                <th>Name</th>
                                <th>URL</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              foreach ($uploadedDocuments as $index => $uploadedDocument) {
                              ?>
                                <tr>
                                  <td>
                                    <?= $uploadedDocument['document_type'] ?>
                                  </td>
                                  <td>
                                    <?= $uploadedDocument['name'] ?>
                                  </td>
                                  <td>
                                    <a href="<?= $uploadedDocument['url'] ?>" target="_blank" style="color:#3a66ad">
                                      <?= $uploadedDocument['url'] ?>
                                    </a>
                                  </td>
                                </tr>
                              <?php
                              }
                              ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-12">
                          <br>
                          <button class="btn btn-lg prplButton pull-right" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                          <!--<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>-->
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                <!--/tab-pane-->
              </div>
            </div>
            <div id="Applications1" class="tabcontent11" style=" margin-top: 7%;">
              <div class="tabordion1">
                <?php
                if ($myApplications) {
                  $i = 1;
                  foreach ($myApplications as $myApplication) {
                ?>
                    <section id="section<?= $i ?>1">
                      <input type="radio" name="sections1" id="option<?= $i ?>1">
                      <label for="option<?= $i ?>1">
                        <h6><i class="fas fa-book">&nbsp;</i><?= $myApplication['course_name'] ?></h6>
                        <h6><i class="fas fa-graduation-cap">&nbsp;</i><?= $myApplication['college_name'] ?></h6>
                        <h6><i class="fas fa-university">&nbsp;</i><?= $myApplication['name'] ?></h6>
                      </label>
                      <article class="a1">
                        <div class="newTab1" id="tab<?= $i ?>1">
                          <div class="tab tab1">
                            <button class="tablinks111 active" onclick="openApplicationTab1(event, 'DocUp<?= $myApplication['name'] ?>1')" id="defaultOpenthis1">Required Documents</button>
                            <button class="tablinks111" onclick="openApplicationTab1(event, 'Comments<?= $myApplication['name'] ?>1')">Comments</button>
                            <button class="tablinks111" onclick="openApplicationTab1(event, 'RequiredInfo1')">Required Info</button>
                          </div>
                          <div id="DocUp<?= $myApplication['name'] ?>1" class="tabcontent111 col-md-12">
                            <h3>Doc Upload</h3>
                            <!-- <h2><?= $myApplication['name'] ?></h2>
      <p><?= $myApplication['college_name'] ?></p>
      <p><?= $myApplication['course_name'] ?></p> -->
                            <?php
                            foreach ($myApplicationDocList as $keyd => $valued) {
                              // code...
                            ?>
                              <div class="col-md-12 docUploadBox noPaddingCol">
                                <?php if ($valued['required'] == 1) {  ?>
                                  <div class="col-md-1 verticalDiv requiredDoc">
                                    <h6 class="textVertical">
                                      Required
                                    </h6>
                                  </div>
                                <?php } else { ?>
                                  <div class="col-md-1 verticalDiv optDoc">
                                    <h6 class="textVertical">
                                      Optional
                                    </h6>
                                  </div>
                                <?php } ?>
                                <div class="col-md-11 noPaddingCol">
                                  <div class="col-md-12">
                                    <div class="col-md-2 statusBox">
                                      <h5>
                                        <?php if ($valued['document_url']) {
                                          echo " <i class='fas fa-check'></i><br> Completed";
                                        } else {
                                          echo " <i class='fas fa-times'></i><br> Pending";
                                        } ?>
                                      </h5>
                                    </div>
                                    <div class="col-md-9 noPaddingCol">
                                      <div class="col-md-12 noPaddingCol">
                                        <div class="col-md-8 docName">
                                          <h5><?= $valued['name'] ?></h5>
                                          <p>Default Certificate Education History Prototype</p>
                                        </div>
                                        <div class="col-md-4">
                                          <form method="post" action="" enctype="multipart/form-data" id="myform<?= $myApplication[" id "] ?><?= $valued["id "] ?>">
                                            <input class="form-control" type="file" name="document<?= $myApplication[" id "] ?><?= $valued["id "] ?>" id="document<?= $myApplication[" id "] ?><?= $valued["id "] ?>">
                                            <button class="btn btn-md prplButton" style="margin-top: 50%;" onclick="uploadAppDoc(<?= $myApplication[" id "] ?>,<?= $valued["id "] ?>,<?= $myApplication["user_id "] ?>)"><i class="fas fa-upload">&nbsp;</i>Upload</button>
                                          </form>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-12 docUplodedName">
                                    <?php if ($valued['document_url']) { ?>
                                      <h6><a href="<?= $valued['document_url'] ?>" target="_blank" class="docUploadNameLink"> <?= $valued['name'] ?> View </a></h6>
                                    <?php } else { ?>
                                      <h6>No Document</h6>
                                    <?php } ?>
                                  </div>
                                </div>
                              </div>
                            <?php } ?>
                            <!--<div class="col-md-12 docUploadBox noPaddingCol">
      <div class="col-md-1 verticalDiv requiredDoc">
         <h6 class="textVertical">Required</h6>
      </div>
      <div class="col-md-11 noPaddingCol" >
         <div class="col-md-12">
            <div class="col-md-2 statusBox">
               <h5><i class="fas fa-check"></i><br>
            Completed</h5>
            </div>
            <div class="col-md-9 noPaddingCol" >
               <div class="col-md-12 noPaddingCol">
                  <div class="col-md-10 docName">
                     <h5>IELTS (Automated)</h5>
                     <p>IELTS</p>
                  </div>
                  <div class="col-md-2">
                     <button class="btn btn-md prplButton" style="    margin-top: 50%;"><i class="fas fa-upload">&nbsp;</i>Upload</button>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-12 docUplodedName">
            <h6><a href="#" target="_blank" class="docUploadNameLink">IELTS.pdf</a></h6>
         </div>
      </div>
      </div>
      <div class="col-md-12 docUploadBox noPaddingCol">
      <div class="col-md-1 verticalDiv optDoc">
         <h6 class="textVertical">Optional</h6>
      </div>
      <div class="col-md-11 noPaddingCol" >
         <div class="col-md-12">
            <div class="col-md-2 statusBox">
               <h5><i class="fas fa-times"></i><br>
                  Pending
               </h5>
            </div>
            <div class="col-md-9 noPaddingCol" >
               <div class="col-md-12 noPaddingCol">
                  <div class="col-md-10 docName">
                     <h5>Backup/Alternative Program Option</h5>
                     <p>Backup/Alternative Program Option</p>
                  </div>
                  <div class="col-md-2">
                     <button class="btn btn-md prplButton" style="    margin-top: 50%;"><i class="fas fa-upload">&nbsp;</i>Upload</button>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-12 docUplodedName">
            <h6>No Document</h6>
         </div>
      </div>
      </div>-->
                          </div>
                          <div id="Comments<?= $myApplication['name'] ?>1" class="tabcontent111 col-md-12">
                            <h3>Comments</h3>
                            <button type="button" class="btn btn-md prplButton btnComment1" data-toggle="collapse" data-target="#<?= $myApplication[" id "] ?>1">New Comment</button>
                            <div id="<?= $myApplication[" id "] ?>1" class="collapse">
                              <form>
                                <div class="form-group">
                                  <label for="comment" style="border-right: 3px solid #ffffff;">Comment:</label>
                                  <textarea class="form-control" rows="5" id="notes<?= $myApplication[" id "] ?>" style="height: auto !important;"></textarea>
                                  <button class="btn btn-md prplButton" type="button" onclick="addNotes(<?= $myApplication[" id "] ?>,<?= $myApplication["user_id "] ?>)"><i class="glyphicon glyphicon-ok-sign"></i>Add Comment</button>
                                </div>
                              </form>
                            </div>
                            <?php if (isset($myApplication['noteslist'])) {
                              foreach ($myApplication['noteslist'] as $keyn => $valuen) {

                            ?>
                                <div class="media comment-box">
                                  <div class="media-left">
                                    <a href="#"> <img class="img-responsive user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png"> </a>
                                  </div>
                                  <div class="media-body">
                                    <h5 class="media-heading"><?php echo $valuen['created_by_name']; ?> <span style="float: right;"><?php $webinarDate = date('D, d F Y H:m:s', strtotime($valuen['added_time']));
                                                                                                                                    echo "$webinarDate"; ?></span></h5>
                                    <p>
                                      <?= $valuen['comment'] ?>
                                    </p>
                                  </div>
                                </div>
                            <?php }
                            } ?>
                            <!--  <div class="media comment-box">
      <div class="media-left">
         <a href="#">
         <img class="img-responsive user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
         </a>
      </div>
      <div class="media-body">
         <h5 class="media-heading">ABC Span <span style="float: right;">July 22nd 2020, 23:59</span></h5>
         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
      </div>
      </div>
      <h2><?= $myApplication['name'] ?></h2>
        <p><?= $myApplication['college_name'] ?></p>
        <p><?= $myApplication['course_name'] ?></p>  -->
                          </div>
                          <div id="RequiredInfo1" class="tabcontent111 col-md-12">
                            <h3 style="display: contents;">Required Extra Information</h3>
                            <div class="col-md-12">
                              <form class="form requiredInfoForm" action="##" method="post" id="">
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="first_name">
                                      <h5>Name</h5>
                                    </label>
                                    <input type="text" class="form-control" name="name" id="student_name" placeholder="Name" value="">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="phone">
                                      <h5>Mobile</h5>
                                    </label>
                                    <input type="text" class="form-control" name="phone" id="student_phone" placeholder="Mobile" value="">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="email">
                                      <h5>Email</h5>
                                    </label>
                                    <input type="email" class="form-control" name="email" id="student_email" placeholder="Email" value="">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="dob">
                                      <h5>DOB</h5>
                                    </label>
                                    <input type="date" name="dob" class="form-control" id="student_dob" value="" />
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="address">
                                      <h5>Address</h5>
                                    </label>
                                    <input type="text" class="form-control" name="address" id="student_address" placeholder="Address" value="">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="country">
                                      <h5>Country</h5>
                                    </label>
                                    <select name="locationcountries" id="student_locationcountries" class="form-control">
                                      <option value="0">Select Country</option>
                                      <?php
                                      foreach ($locationcountries as $location) {
                                      ?>
                                        <option value="<?= $location->id ?>" <?php if ($location->id == $selected_country) echo 'selected="selected"'; ?>>
                                          <?= $location->name ?>
                                        </option>
                                      <?php
                                      }
                                      ?>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="state">
                                      <h5>State</h5>
                                    </label>
                                    <select name="state" id="student_state" class="form-control">
                                      <option value="0">Select State</option>
                                      <?php
                                      foreach ($related_state_list as $location) {
                                      ?>
                                        <option value="<?= $location->zone_id ?>" <?php if ($location->zone_id == $selected_state) echo 'selected="selected"'; ?>>
                                          <?= $location->name ?>
                                        </option>
                                      <?php
                                      }
                                      ?>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="city">
                                      <h5>City</h5>
                                    </label>
                                    <select name="city" id="student_city" class="form-control">
                                      <option value="0">Select City</option>
                                      <?php
                                      foreach ($related_city_list as $location) {
                                      ?>
                                        <option value="<?= $location->id ?>" <?php if ($location->id == $city) echo 'selected="selected"'; ?>>
                                          <?= $location->city_name ?>
                                        </option>
                                      <?php
                                      }
                                      ?>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="zipcode">
                                      <h5>Zipcode</h5>
                                    </label>
                                    <input type="text" class="form-control" name="zip" id="student_zip" placeholder="Zipcode" value="">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="type">
                                      <h5>Type</h5>
                                    </label>
                                  </div>
                                  <div class="col-xs-6" style="display: inline-flex;">
                                    <input type="checkbox" id="Type1" name="Type1" value="Bike">
                                    <label for="Type1"> Type1</label>
                                    <br>
                                    <input type="checkbox" id="Type2" name="Type2" value="Car">
                                    <label for="Type2"> Type2</label>
                                    <br>
                                    <input type="checkbox" id="Type3" name="Type3" value="Boat">
                                    <label for="Type3"> Type3</label>
                                    <br>
                                    <br>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-6">
                                    <label for="gender">
                                      <h5>Gender</h5>
                                    </label>
                                    <div class="col-md-12">
                                      <label class="radio-inline">
                                        <input type="radio" name="gender" <?= $gender == 'M' ? 'checked' : '' ?>>Male </label>
                                      <label class="radio-inline">
                                        <input type="radio" name="gender" <?= $gender == 'F' ? 'checked' : '' ?>>Female </label>
                                      <label class="radio-inline">
                                        <input type="radio" name="gender" <?= $gender == 'O' ? 'checked' : '' ?>>Other </label>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-12">
                                    <br>
                                    <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                                    <button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </article>
                    </section>
                <?php
                    $i++;
                  }
                } else {
                  echo "No Application Selected ...!!! <br/> Please Select University From List  <br/>";
                  echo "<a class='btn btn-large btn-danger' href='/counsellor/student/offerlist/view/" . $id . "' style='color: #f6882c'> <b>Shortlist Universities / Courses</b> </a>";
                }
                ?>
              </div>
            </div>
            <div id="Documents1" class="tabcontent" style=" margin-top: 1%;">
              <div class="col-md-12 boxAppList docTab1" style="padding: 2% 2%;    background-color: white;">
                <h3>Documents</h3>
                <hr>
                <div class="tab">
                  <button class="tablinks21" onclick="openDocType1(event, 'BASIC1')" id="defaultOpenTab1">Basic</button>
                  <button class="tablinks21" onclick="openDocType1(event, 'ACADEMIC1')">Academic </button>
                  <button class="tablinks21" onclick="openDocType1(event, 'TESTS1')">Entrance Exam</button>
                  <button class="tablinks21" onclick="openDocType1(event, 'APPLICATION1')">Other Applications</button>
                  <button class="tablinks21" onclick="openDocType1(event, 'APPLICATION_SPECIFIC1')">Specific Application Materials</button>
                </div>
                <?php
                foreach ($new_doc_list as $keydl => $valuedl) {

                ?>
                  <div id="<?php echo "$keydl"; ?>1" class="tabcontent21">
                    <h3><?php echo "$keydl"; ?></h3>
                    <table class="table table-bordered">
                      <thead>
                        <th style="width: 20%;">Name</th>
                        <th style="width: 40%;">Description</th>
                        <th>Upload</th>
                        <th style="width: 10%;">Status</th>
                      </thead>
                      <tbody>
                        <?php
                        foreach ($valuedl as $keyl => $valuel) {

                        ?>
                          <tr>
                            <td>
                              <?php echo $valuel['display_name']; ?>
                            </td>
                            <td>
                              <?php echo $valuel['description']; ?>
                            </td>
                            <td>
                              <div class="col-md-12"> <a class="btn btn-info" onclick='popupOne(<?php echo json_encode($valuel); ?>);return false;'> Upload Doc </a> </div>
                            </td>
                            <td>
                              <?php if ($keydl == 'APPLICATION_SPECIFIC') {
                                if ($valuel['user_doc_list']) {
                                  if ($valuel['user_doc_list'][0]['application_id'] == $selected_app_id) {
                                    echo 'UPLOADED';
                                  } else {
                                    echo 'PENDING';
                                  }
                                } else {
                                  $status =  $valuel['user_doc_list'] ? 'UPLOADED' : 'PENDING';
                                  echo $status;
                                }
                              } else {
                                $status =  $valuel['user_doc_list'] ? 'UPLOADED' : 'PENDING';
                                echo $status;
                              } ?>
                              <!-- <?php $status =  $valuel['user_doc_list'] ? 'UPLOADED' : 'PENDING';
                                    echo $status; ?> -->
                            </td>
                          </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                <?php } ?>
                <div class="modal fade" id="modalDocument1" role="dialog" style="z-index: 10000;">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Upload Document</h4>
                      </div>
                      <div class="modal-body">
                        <form name="" class="col-md-12 ">
                          <div class="col-md-12">
                            <label id="title_name"> Doc Name </label>
                            <br />
                            <br />
                            <select class="form-control" name="doc_id" id="doc_id" required>
                              <option value=""> Select Uploading Document</option>
                            </select>
                          </div>
                          <br />
                          <br />
                          <div class="col-md-12">
                            <input type="file" name="userfile" id="userfile" required>
                          </div>
                          <br />
                          <br />
                          <div class="col-md-12">
                            <label> Document Name </label>
                            <input type="text" class="form-group form-control" placeholder="Input Document Name" name="userdocname" id="userdocname" style="height: auto;"></input>
                          </div>
                          <div class="col-md-12" style="text-align: center;">
                            <button class="btn btn-primary btn-sm" type="button" onclick="uploadDoc()"> Upload </button>
                          </div>
                        </form>
                        <br>
                        <hr>
                        <div>
                          <div class="row">
                            <div class="col-md-12">
                              <h3>Uploaded Document List</h3>
                              <table class="table table-bordered">
                                <thead>
                                  <th>Document Name</th>
                                  <th>File Name</th>
                                  <th>View</th>
                                  <th>Delete</th>
                                </thead>
                                <tbody id="table_content">
                                  <tr>
                                    <td>display_name</td>
                                    <td style="max-width: 250px;overflow: auto;">url</td>
                                    <td><a href="" target="_blank" class="btn btn-sm btn-success">View</a></td>
                                    <td>
                                      <button class="btn btn-danger" type="button" ng-click=""> Delete</button>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="Payments1" class="tabcontent11" style=" margin-top: 7%;">
              <h3>Payments</h3>
              <div class="col-md-12" style="    background-color: white;
      padding: 2% 1%">
                <!--  <form class="form" action="##" method="post" id="">
      <div class="form-group">
         <div class="col-xs-6">
            <label for="first_name">
               <h5>Name</h5>
            </label>
            <input type="text" class="form-control" name="name" id="student_name" placeholder="Name" value="">
         </div>
      </div>
      <div class="form-group">
         <div class="col-xs-6">
            <label for="phone">
               <h5>Mobile</h5>
            </label>
            <input type="text" class="form-control" name="phone" id="student_phone" placeholder="Mobile" value="">
         </div>
      </div>
      <div class="form-group">
         <div class="col-xs-6">
            <label for="email">
               <h5>Email</h5>
            </label>
            <input type="email" class="form-control" name="email" id="student_email" placeholder="Email" value="">
         </div>
      </div>
      <div class="form-group">
         <div class="col-xs-6">
            <label for="dob">
               <h5>DOB</h5>
            </label>
            <input type="date" name="dob"  class="form-control" id="student_dob" value=""/>
         </div>
      </div>
      <div class="form-group">
         <div class="col-xs-6">
            <label for="address">
               <h5>Address</h5>
            </label>
            <input type="text" class="form-control" name="address" id="student_address" placeholder="Address" value="">
         </div>
      </div>
      <div class="form-group">
         <div class="col-xs-6">
            <label for="country">
               <h5>Country</h5>
            </label>
            <select name="locationcountries" id="student_locationcountries" class="form-control">
               <option value="0">Select Country</option>
               <?php
                foreach ($locationcountries as $location) {
                ?>
               <option value="<?= $location->id ?>" <?php if ($location->id == $selected_country) echo 'selected="selected"'; ?>><?= $location->name ?></option>
               <?php
                }
                ?>
            </select>
         </div>
      </div>
      <div class="form-group">
         <div class="col-xs-6">
            <label for="state">
               <h5>State</h5>
            </label>
            <select name="state" id="student_state" class="form-control">
               <option value="0">Select State</option>
               <?php
                foreach ($related_state_list as $location) {
                ?>
               <option value="<?= $location->zone_id ?>" <?php if ($location->zone_id == $selected_state) echo 'selected="selected"'; ?>><?= $location->name ?></option>
               <?php
                }
                ?>
            </select>
         </div>
      </div>
      <div class="form-group">
         <div class="col-xs-6">
            <label for="city">
               <h5>City</h5>
            </label>
            <select name="city" id="student_city" class="form-control">
               <option value="0">Select City</option>
               <?php
                foreach ($related_city_list as $location) {
                ?>
               <option value="<?= $location->id ?>" <?php if ($location->id == $city) echo 'selected="selected"'; ?>><?= $location->city_name ?></option>
               <?php
                }
                ?>
            </select>
         </div>
      </div>
      <div class="form-group">
         <div class="col-xs-6">
            <label for="zipcode">
               <h5>Zipcode</h5>
            </label>
            <input type="text" class="form-control" name="zip" id="student_zip" placeholder="Zipcode" value="">
         </div>
      </div>
      <div class="form-group">
         <div class="col-xs-6">
            <label for="gender">
               <h5>Gender</h5>
            </label>
            <div class="col-md-12">
               <label class="radio-inline">
               <input type="radio" name="gender" <?= $gender == 'M' ? 'checked' : '' ?>>Male
               </label>
               <label class="radio-inline">
               <input type="radio" name="gender" <?= $gender == 'F' ? 'checked' : '' ?>>Female
               </label>
               <label class="radio-inline">
               <input type="radio" name="gender" <?= $gender == 'O' ? 'checked' : '' ?>>Other
               </label>
            </div>
         </div>
      </div>
      <div class="form-group">
         <div class="col-xs-12">
            <br>
            <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
            <button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
         </div>
      </div>
      </form>-->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--/tab-content-->
</div>
<!--/col-9-->

<button class="open-button" onclick="openForm()" title="Alerts & Notification"><i class="fa fa-bell"></i></button>

<!-- <button class="open-button"onclick="openForm()">Notification</button> -->

<div class="chat-popup" id="myForm">
  <button type="button" class="close" onclick="closeForm()" style="    margin-top: 5%;">×</button>
  <form name="comments_insert" id="comments_insert" class="form-container">
    <h5>Notifications</h5>

    <div class="p_bar"></div>
    <div class="a">
      <div class="content">
        <?php
        foreach ($notification_list as $keynl => $valuenl) {
        ?>
          <p>
            <small>Created By : - <b><?php echo $valuenl['name'] . " " . $valuenl['last_name']; ?></b></small>
            <br>
            <small> Titile : - <b><?php echo $valuenl['title']; ?></b></small>
            <br>
            <b>
              <?php echo $valuenl['description']; ?>
            </b>

            <br>
            <small> Date : - <b><?php echo $valuenl['date_created']; ?></b></small>

          </p>
        <?php
        }
        ?>
      </div>
    </div>

    <i class="fa-solid fa-chalkboard-user"></i>
    <h5>Send your Query</h5>
    <!-- <label for="msg">Message</label> -->
    <textarea name="comments" id="comments" placeholder="Type your message.." name="msg" required></textarea>

    <div class="col-md-12" style="display: inline-flex;">
      <button type="button" onclick="addComments(<?= $id ?>)" class="col-md-6 btn-sm">Send</button>
      <button type="button" class="col-md-6 btn-sm cancel" onclick="closeForm()">Close</button>
    </div>
  </form>
</div>
</div>
<div id="courseDataModal" class="modal">
  <!-- Modal content style="display: block;"-->
  <div class="modal-content"> <span class="close closeFillFairData">&times;</span>
    <div id="fair-data-modal" style="text-align: center;">
      <div class="row">
        <div class="col-md-12 login-sec">
          <h2 class="text-center" style="color: #8c57c5;">Select Course</h2>
          <form class="login-form" class="form-horizontal" onsubmit="applyUniversity()" method="post" autocomplete="off">
            <div class="col-md-12 form-group">
              <div class="col-md-12">
                <label for="inputEmail3" class="text-uppercase"> University Name </label>
                <input type="text" class="form-control" style="width:100%; height:30px; font-size:12px;" name="university_name" id="university_name">
              </div>
            </div>
            <div class="col-md-12 form-group">
              <div class="col-md-12">
                <label for="inputEmail3" class="text-uppercase"> Select Course </label>
                <input type="text" name="searchtextvalue" id="searchtextvalue" class="form-control search-slt typeahead" data-provide="typeahead" placeholder="Type Course Name & Select From Dropdown" style="width:100%; height:30px; font-size:12px;">
              </div>
            </div>
            <div class="form-check" style="text-align: center;">
              <input type="submit" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Apply"></input>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="applicationEditModal" class="modal">
  <!-- Modal content style="display: block;"-->
  <div class="modal-content"> <span class="close closeFillFairData">&times;</span>
    <div id="fair-edit-modal" style="text-align: center;">
      <div class="row">
        <div class="col-md-12 login-sec">
          <h2 class="text-center" style="color: #8c57c5;">Select Application Type</h2>
          <form class="login-form" class="form-horizontal" autocomplete="off">
            <div class="col-md-12 form-group">
              <div class="col-md-12">
                <label for="inputEmail3" class="text-uppercase"> Application Type </label>
                <select name="application_type" id="application_type">
                  <option value="">Select Application</option>
                  <option value="1">Imperial Will Fill Application</option>
                  <option value="2">Imperial Will Check Application</option>
                </select>
                <input type="hidden" name="update_app_id" id="update_app_id" value="" />
              </div>
            </div>
            <div class="form-check" style="text-align: center;">
              <input type="button" onclick="submitUniversityApplication()" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Apply"></input>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="applicationStatusEditModal" class="modal">
  <!-- Modal content style="display: block;"-->
  <div class="modal-content">
    <span class="close closestatusData">&times;</span>
    <div id="fair-edit-modal" style="text-align: center;">

      <div class="row">
        <div class="col-md-12 login-sec">
          <h2 class="text-center" style="color: #8c57c5;">Select Application Type</h2>
          <form class="login-form" class="form-horizontal" autocomplete="off">

            <div class="col-md-12 form-group">
              <div class="col-md-12">
                <label for="inputEmail3" class="text-uppercase"> Application Status </label>
                <select name="app_status" id="app_status" onchange="checkSubStatus()">
                  <option value="">Select Application Status</option>

                  <option value="APPLICATION_UNDER_REVIEW">Application Under Review</option>
                  <option value="APPLICATION_COMPLETED_READY_SUBMISSION">Application Completed and Ready for Submission</option>
                  <option value="APPLICATION_SUBMITTED_UNIVERSITY">Application Submitted to University</option>
                  <option value="ADMISSION_STATUS">Admission Status (Approved / Denied / Deffered)</option>
                  <option value="FINAL_UNCONDITIONAL_ADMIT">Final Unconditional Admit (I20 / COE / CAS / ECOE / UCO )</option>
                  <option value="VISA_APPROVED">Visa Approved</option>

                </select>
                <input type="hidden" name="update_app_id" id="update_app_id" value="" />
              </div>
            </div>

            <div class="col-md-12 form-group" id="subStatusOne" style="display:none;">
              <div class="col-md-12">
                <label for="inputEmail3" class="text-uppercase"> Application Sub Status </label>
                <select name="app_sub_status" id="app_sub_status">
                  <option value="">Select Application Sub Status</option>

                  <option value="Approved">Approved</option>
                  <option value="Denied">Denied</option>
                  <option value="Deffered">Deffered</option>

                </select>

              </div>
            </div>

            <div class="col-md-12 form-group" id="subStatusTwo" style="display:none;">
              <div class="col-md-12">
                <label for="inputEmail3" class="text-uppercase"> Application Sub Status </label>
                <select name="app_sub_status" id="app_sub_status">
                  <option value="">Select Application Sub Status</option>

                  <option value="I20">I20</option>
                  <option value="COE">COE</option>
                  <option value="CAS">CAS</option>
                  <option value="ECOE">ECOE</option>
                  <option value="UCO">UCO</option>

                </select>

              </div>
            </div>

            <div class="form-check" style="text-align: center;">

              <input type="button" onclick="submitStatusApplication()" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Apply"></input>

            </div>


          </form>
        </div>
      </div>

    </div>
  </div>
</div>

<script src="<?php echo base_url(); ?>js/bootstrap-typeahead-min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $(".docTab .tab button:first").addClass("active");
    document.getElementById("BASIC").style.display = "block";
    $(".docTab1 .tab button:first").addClass("active");
    document.getElementById("BASIC1").style.display = "block";
  });

  var acc = document.getElementsByClassName("accordion");
  var i;

  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var panel = this.nextElementSibling;
      if (panel.style.maxHeight) {
        panel.style.maxHeight = null;
      } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
      }
    });
  }

  function editApplication(appId) {
    $("#applicationEditModal").modal('show');
    document.getElementById("university_name").value = university_name;
    searchUniversityId = university_id;
    console.log(searchUniversityId);
  }
</script>
<script type="text/javascript">
  var searchUniversityId = 0;
  var courseId = 0;

  function courseModal(university_id, university_name) {
    $("#courseDataModal").modal('show');
  }
  var $input = $(".typeahead");
  $input.typeahead({
    source: function(query, process) {
      return $.get('/offer/course/textsuggestions/' + searchUniversityId, {
        query: query
      }, function(data) {
        return process(data.options);
      });
    }
  });
  $input.change(function() {
    var current = $input.typeahead("getActive");
    if (current) {
      // Some item from your model is active!
      if (current.name == $input.val()) {
        courseId = current.id;
        // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
      } else {
        // This means it is only a partial match, you can either add a new item
        // or take the active if you don't want new items
      }
    } else {
      // Nothing is active so it is a new value (or maybe empty value)
    }
  });
</script>
<script type="text/javascript">
  var modalFillFairData = document.getElementById("courseDataModal");
  var spanFillFair = document.getElementsByClassName("closeFillFairData")[0];
  var modalFillFairEdit = document.getElementById("applicationEditModal");
  var spanFillFairEdit = document.getElementsByClassName("closeFillFairData")[1];
  spanFillFair.onclick = function() {
    //modalFillFairData.style.display = "none";
    $("#courseDataModal").modal('hide');
  }
  spanFillFairEdit.onclick = function() {
    //modalFillFairData.style.display = "none";
    $("#applicationEditModal").modal('hide');
  }

  function applyUniversity() {
    var university_id = searchUniversityId;
    var course_id = courseId;
    var user_id = <?= $id ?>;
    console.log(course_id);
    event.preventDefault();
    jQuery.ajax({
      type: "GET",
      url: "/university/details/apply/?uid=" + university_id + "&course_id=" + course_id + "&user_id=" + user_id,
      success: function(response) {
        alert("You have applied successfully");
        $("#courseDataModal").modal('hide');
        //window.location.href = '/search/university';
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var readURL = function(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('.avatar').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }
    $(".file-upload").on('change', function() {
      readURL(this);
    });
  });
  $(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#example thead tr').clone(true).appendTo('#example thead');
    $('#example thead tr:eq(1) th').each(function(i) {
      var title = $(this).text();
      $(this).html('<input type="text" placeholder="Search ' + title + '" />');
      $('input', this).on('keyup change', function() {
        if (table.column(i).search() !== this.value) {
          table.column(i).search(this.value).draw();
        }
      });
    });
    var table = $('#example').DataTable({
      orderCellsTop: true
    });
  });
  $('#student_locationcountries').on('change', function() {
    var id = $(this).find(":selected").val();
    var dataSet = 'id=' + id;
    $.ajax({
      type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url: '/user/account/getStates', // the url where we want to POST
      data: dataSet, // our data object
      success: function(data1) {
        $('#student_state').html(data1);
      }
    });
  });
  $('#student_state').on('change', function() {
    var id = $(this).find(":selected").val();
    var dataSet2 = 'id=' + id;
    $.ajax({
      type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url: '/user/account/getCities', // the url where we want to POST
      data: dataSet2, // our data object
      success: function(data2) {
        $('#student_city').html(data2);
      }
    });
  });
  $('input[type=radio][name=diploma_12]').change(function() {
    if (this.value == '1') {
      $('.diploma_hs').hide();
      $('#diploma_id').show();
    } else if (this.value == '2') {
      $('.diploma_hs').hide();
      $('#12thidd').show();
    }
  });
  $('input[type=radio][name=competitive_exam]').change(function() {
    if (this.value == 'NO' || this.value == 'NOT_REQUIRED') {
      $('#exam_stage').hide();
      $('#result_date').hide();
      $('#exam_date').hide();
      $('#exam_score').hide();
    } else {
      $('#exam_stage').show();
      if ($('input[type=radio][name=exam_stage]:checked').val() == 'REGISTERED') {
        $('#result_date').hide();
        $('#exam_score').hide();
        $('#exam_date').show();
      } else if ($('input[type=radio][name=exam_stage]:checked').val() == 'RESULT_WAIT') {
        $('#exam_date').hide();
        $('#exam_score').hide();
        $('#result_date').show();
      } else if ($('input[type=radio][name=exam_stage]:checked').val() == 'RESULT_OUT') {
        $('#result_date').hide();
        $('#exam_date').hide();
        $('#exam_score').show();
        if ($('input[type=radio][name=competitive_exam]:checked').val() == 'GRE') {
          $('#exam_score_gmat').hide();
          $('#exam_score_sat').hide();
          $('#exam_score_gre').show();
        } else if ($('input[type=radio][name=competitive_exam]:checked').val() == 'GMAT') {
          $('#exam_score_sat').hide();
          $('#exam_score_gre').hide();
          $('#exam_score_gmat').show();
        } else if ($('input[type=radio][name=competitive_exam]:checked').val() == 'SAT') {
          $('#exam_score_gmat').hide();
          $('#exam_score_gre').hide();
          $('#exam_score_sat').show();
        }
      }
    }
  });
  $('input[type=radio][name=exam_stage]').change(function() {
    if (this.value == 'REGISTERED') {
      $('#result_date').hide();
      $('#exam_score').hide();
      $('#exam_date').show();
    } else if (this.value == 'RESULT_WAIT') {
      $('#exam_date').hide();
      $('#exam_score').hide();
      $('#result_date').show();
    } else if (this.value == 'RESULT_OUT') {
      $('#result_date').hide();
      $('#exam_date').hide();
      $('#exam_score').show();
      if ($('input[type=radio][name=competitive_exam]:checked').val() == 'GRE') {
        $('#exam_score_gmat').hide();
        $('#exam_score_sat').hide();
        $('#exam_score_gre').show();
      } else if ($('input[type=radio][name=competitive_exam]:checked').val() == 'GMAT') {
        $('#exam_score_sat').hide();
        $('#exam_score_gre').hide();
        $('#exam_score_gmat').show();
      } else if ($('input[type=radio][name=competitive_exam]:checked').val() == 'SAT') {
        $('#exam_score_gmat').hide();
        $('#exam_score_gre').hide();
        $('#exam_score_sat').show();
      }
    }
  });

  $('input[type=radio][name=source_of_funds]').change(function() {
    if (this.value == 'self_fund') {
      $('#myfamliy').hide();
      $('#sponsor').hide();
      $('#otheroption').hide();
      $('#self').show();
    } else if (this.value == 'family') {
      $('#myfamliy').show();
      $('#sponsor').hide();
      $('#otheroption').hide();
      $('#self').hide();
    } else if (this.value == 'Sponsorship') {
      $('#myfamliy').hide();
      $('#sponsor').show();
      $('#otheroption').hide();
      $('#self').hide();
    } else if (this.value == 'other') {
      $('#myfamliy').hide();
      $('#sponsor').hide();
      $('#otheroption').show();
      $('#self').hide();
    }
  });

  /*$('input[type=radio][name=english_proficiency_exam]').change(function() {
      if(this.value == 'NO' || this.value == 'NOT_REQUIRED'){
          $('#english_exam_stage').hide();
      }
      else{
          $('#english_exam_stage').show();
      }
  });*/
  $('input[type=radio][name=english_proficiency_exam]').change(function() {
    if (this.value == 'NO' || this.value == 'NOT_REQUIRED') {
      $('#english_exam_stage').hide();
      $('#english_result_date').hide();
      $('#english_exam_date').hide();
      $('#english_exam_score').hide();
    } else {
      $('#english_exam_stage').show();
      if ($('input[type=radio][name=english_exam_stage]:checked').val() == 'REGISTERED') {
        $('#english_result_date').hide();
        $('#english_exam_score').hide();
        $('#english_exam_date').show();
      } else if ($('input[type=radio][name=english_exam_stage]:checked').val() == 'RESULT_WAIT') {
        $('#english_exam_date').hide();
        $('#english_exam_score').hide();
        $('#english_result_date').show();
      } else if ($('input[type=radio][name=english_exam_stage]:checked').val() == 'RESULT_OUT') {
        $('#english_result_date').hide();
        $('#english_exam_date').hide();
        $('#english_exam_score').show();
        if ($('input[type=radio][name=english_proficiency_exam]:checked').val() == 'TOEFL') {

          $('#english_exam_ielts').hide();
          $('#english_exam_pte').hide();
          $('#english_exam_duolingo').hide();
          $('#english_exam_tofel').show();
        } else if ($('input[type=radio][name=english_proficiency_exam]:checked').val() == 'IELTS') {
          $('#english_exam_ielts').show();
          $('#english_exam_pte').hide();
          $('#english_exam_duolingo').hide();
          $('#english_exam_tofel').hide();
        } else if ($('input[type=radio][name=english_proficiency_exam]:checked').val() == 'PTE') {
          $('#english_exam_ielts').hide();
          $('#english_exam_pte').show();
          $('#english_exam_duolingo').hide();
          $('#english_exam_tofel').hide();
        } else if ($('input[type=radio][name=english_proficiency_exam]:checked').val() == 'DUOLINGO') {
          $('#english_exam_ielts').hide();
          $('#english_exam_pte').hide();
          $('#english_exam_duolingo').show();
          $('#english_exam_tofel').hide();
        }
      }
    }
  });
  $('input[type=radio][name=english_exam_stage]').change(function() {
    if (this.value == 'REGISTERED') {
      $('#english_result_date').hide();
      $('#english_exam_score').hide();
      $('#english_exam_date').show();
    } else if (this.value == 'RESULT_WAIT') {
      $('#english_exam_date').hide();
      $('#english_exam_score').hide();
      $('#english_result_date').show();
    } else if (this.value == 'RESULT_OUT') {
      $('#english_result_date').hide();
      $('#english_exam_date').hide();
      $('#english_exam_score').show();
      if ($('input[type=radio][name=english_proficiency_exam]:checked').val() == 'TOEFL') {

        $('#english_exam_ielts').hide();
        $('#english_exam_pte').hide();
        $('#english_exam_duolingo').hide();
        $('#english_exam_tofel').show();
      } else if ($('input[type=radio][name=english_proficiency_exam]:checked').val() == 'IELTS') {
        $('#english_exam_ielts').show();
        $('#english_exam_pte').hide();
        $('#english_exam_duolingo').hide();
        $('#english_exam_tofel').hide();
      } else if ($('input[type=radio][name=english_proficiency_exam]:checked').val() == 'PTE') {
        $('#english_exam_ielts').hide();
        $('#english_exam_pte').show();
        $('#english_exam_duolingo').hide();
        $('#english_exam_tofel').hide();
      } else if ($('input[type=radio][name=english_proficiency_exam]:checked').val() == 'DUOLINGO') {
        $('#english_exam_ielts').hide();
        $('#english_exam_pte').hide();
        $('#english_exam_duolingo').show();
        $('#english_exam_tofel').hide();
      }
    }
  });
  $('input[type=radio][name=masters]').change(function() {
    if (this.value == '1') {
      $('#masters_id').show();
    } else if (this.value == '2') {
      $('#masters_id').hide();
    }
  });

  function validate() {
    //english test
    var engTestType = $('input[type=radio][name=english_proficiency_exam][checked=checked]').val();
    var engTestStatus = $('input[type=radio][name=english_exam_stage][checked=checked]').val();
    var totalMarks = $('#english_exam_score_total').val() ? $('#english_exam_score_total').val() : 0;
    //competitive_exam
    var comTestType = $('input[type=radio][name=competitive_exam][checked=checked]').val();
    var comTestStatus = $('input[type=radio][name=exam_stage][checked=checked]').val();
    var quantMarks = $('#exam_score_quant_gre').val() ? $('#exam_score_quant_gre').val() : 0;
    var verbalMarks = $('#exam_score_verbal_gre').val() ? $('#exam_score_verbal_gre').val() : 0;
    var awaMarks = $('#exam_score_awa_gre').val() ? $('#exam_score_awa_gre').val() : 0;

    /*var bachelor_university = $('#bachelor_university').val();

    if(bachelor_university == ''){
      alert("University Name is Required");
      document.getElementById('bachelor_university').style.borderColor = "red";
      return false;
    }*/

    /*if(comTestStatus == "RESULT_OUT") {
      if(quantMarks > 129 && quantMarks < 171) {
        return true;
      } else {
        alert(" Quant marks should be between 130 to 170");
        document.getElementById('exam_score_quant_gre').style.borderColor = "red";
      }
      if(verbalMarks > 129 && verbalMarks < 171) {
        return true;
      } else {
        alert(" Verbal marks should be between 130 to 170");
        document.getElementById('exam_score_verbal_gre').style.borderColor = "red";
      }
      if(awaMarks > 0 && awaMarks < 6) {
        return true;
      } else {
        alert(" AWA marks should be between 0 to 5");
        document.getElementById('exam_score_awa_gre').style.borderColor = "red";
        return false;
      }
    }
    if(engTestType == "TOEFL" && engTestStatus == "RESULT_OUT") {
      if(totalMarks > 0 && totalMarks < 31) {
        return true;
      } else {
        alert(engTestType + " total marks should be between 1 to 30");
        document.getElementById('english_exam_score_total').style.borderColor = "red";
        return false;
      }
    }
    if(engTestType == "TOEFL" && engTestStatus == "RESULT_OUT") {
      if(totalMarks > 0 && totalMarks < 31) {
        return true;
      } else {
        alert(engTestType + " total marks should be between 1 to 30");
        document.getElementById('english_exam_score_total').style.borderColor = "red";
        return false;
      }
    }
    if(engTestType == "IELTS" && engTestStatus == "RESULT_OUT") {
      if(totalMarks > 0 && totalMarks < 10) {
        return true;
      } else {
        alert(engTestType + " total marks should be between 1 to 30");
        document.getElementById('english_exam_score_total').style.borderColor = "red";
        return false;
      }
    }
    if(engTestType == "PTE" && engTestStatus == "RESULT_OUT") {
      if(totalMarks > 9 && totalMarks < 91) {
        return true;
      } else {
        alert(engTestType + " total marks should be between 1 to 30");
        document.getElementById('english_exam_score_total').style.borderColor = "red";
        return false;
      }
    }
    if(engTestType == "DUOLINGO" && engTestStatus == "RESULT_OUT") {
      if(totalMarks > 9 && totalMarks < 161) {
        return true;
      } else {
        alert(engTestType + " total marks should be between 1 to 30");
        document.getElementById('english_exam_score_total').style.borderColor = "red";
        return false;
      }
    }*/
  }

  function openPage(pageName, elmnt, color) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].style.backgroundColor = "";
      tablinks[i].style.color = "";
      tablinks[i].style.fontWeight = "";
      tablinks[i].style.borderBottom = "";
    }
    document.getElementById(pageName).style.display = "block";
    elmnt.style.backgroundColor = color;
    elmnt.style.color = "#815dd5";
    elmnt.style.fontWeight = "600";
    elmnt.style.borderBottom = "3px solid #815dd5";
  }
  // Get the element with id="defaultOpen" and click on it
  document.getElementById("defaultOpen").click();

  function openPage1(pageName1, elmnt1, color1) {
    var i, tabcontent11, tablinks1;
    tabcontent11 = document.getElementsByClassName("tabcontent11");
    for (i = 0; i < tabcontent11.length; i++) {
      tabcontent11[i].style.display = "none";
    }
    tablinks1 = document.getElementsByClassName("tablink1");
    for (i = 0; i < tablinks1.length; i++) {
      tablinks1[i].style.backgroundColor = "";
      tablinks1[i].style.color = "";
      tablinks1[i].style.fontWeight = "";
      tablinks1[i].style.borderBottom = "";
    }
    document.getElementById(pageName1).style.display = "block";
    elmnt1.style.backgroundColor = color1;
    elmnt1.style.color = "#815dd5";
    elmnt1.style.fontWeight = "600";
    elmnt1.style.borderBottom = "3px solid #815dd5";
  }
  // Get the element with id="defaultOpen" and click on it
  document.getElementById("defaultOpen1").click();

  function openApplicationTab(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent1");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks1");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
  }
  // Get the element with id="defaultOpen" and click on it
  document.getElementById("defaultOpenthis").click();

  function openApplicationTab1(evt1, cityName1) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent111");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks111");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName1).style.display = "block";
    evt1.currentTarget.className += " active";
  }
  // Get the element with id="defaultOpen" and click on it
  document.getElementById("defaultOpenthis1").click();
  $(".btnComment").click(function() {
    $(this).text(function(i, v) {
      return v === 'New Comment' ? 'Hide' : 'New Comment'
    })
  });
  $(".btnComment1").click(function() {
    $(this).text(function(ii, vv) {
      return vv === 'New Comment' ? 'Hide' : 'New Comment'
    })
  });
</script>
<script type="text/javascript">
  function addNotes(appId, studentId) {
    var notes = $('#notes' + appId).val();
    //alert(appId);
    var dataString = 'app_id=' + appId + '&student_id=' + studentId + '&notes=' + notes;
    event.preventDefault();
    $.ajax({
      type: "POST",
      url: "/student/profile/notesupdate",
      data: dataString,
      cache: false,
      success: function(result) {
        if (result.status == "SUCCESS") {
          alert("Notes Update Successfully!!!");
        } else if (result.status == 'NOTVALIDUSER') {
          //alert(result);
          window.location.href = '/user/account';
        } else {
          alert(result);
          location.reload();
        }
      }
    });
  }

  function uploadAppDoc(appId, docId, studentId) {
    var fd = new FormData();
    var files = $('#document' + appId + docId)[0].files;
    // Check file selected or not
    if (files.length > 0) {
      fd.append('document', files[0]);
      fd.append('app_id', appId);
      fd.append('document_type', docId);
      fd.append('student_id', studentId);
      event.preventDefault();
      $.ajax({
        url: '/student/profile/appdocupdate',
        type: 'post',
        data: fd,
        contentType: false,
        processData: false,
        success: function(response) {
          console.log(response);
          if (response == 'SUCCESS') {
            alert('file uploaded');
          } else {
            alert('file not uploaded');
          }
        },
      });
    } else {
      alert("Please select a file.");
    }
  }
  $('input[name="sections"]').first().prop('checked', true);
  $('input[name="sections1"]').first().prop('checked', true);
  $(function() {
    $("input[name='passport_availalibility']").click(function() {
      if ($("#chk-Yes").is(":checked")) {
        $("#passport_group").show();
      } else {
        $("#passport_group").hide();
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#desired_course_name').on('change', function() {
      var dataString = 'id=' + this.value;
      $.ajax({
        type: "POST",
        url: "/search/university/getSubcoursesList",
        data: dataString,
        cache: false,
        success: function(result) {
          $('#desired_sub_course_name').html(result);
        }
      });
    });
  })




  function openFirstTab(subevt, tabNameFirst) {
    var i, tabsubcontent, tabsublinks;
    tabsubcontent = document.getElementsByClassName("tabsubcontent");
    for (i = 0; i < tabsubcontent.length; i++) {
      tabsubcontent[i].style.display = "none";
    }
    tabsublinks = document.getElementsByClassName("tabsublinks");
    for (i = 0; i < tabsublinks.length; i++) {
      tabsublinks[i].className = tabsublinks[i].className.replace(" active", "");
    }
    document.getElementById(tabNameFirst).style.display = "block";
    subevt.currentTarget.className += " active";
  }
  // Get the element with id="defaultOpen" and click on it
  document.getElementById("defaultOpenFirstTab").click();

  function openFirstTab1(subevt, tabNameFirst1) {
    var i, tabsubcontent, tabsublinks;
    tabsubcontent = document.getElementsByClassName("tabsubcontent1");
    for (i = 0; i < tabsubcontent.length; i++) {
      tabsubcontent[i].style.display = "none";
    }
    tabsublinks = document.getElementsByClassName("tabsublinks1");
    for (i = 0; i < tabsublinks.length; i++) {
      tabsublinks[i].className = tabsublinks[i].className.replace(" active", "");
    }
    document.getElementById(tabNameFirst1).style.display = "block";
    subevt.currentTarget.className += " active";
  }
  // Get the element with id="defaultOpen" and click on it
  document.getElementById("defaultOpenFirstTab1").click();

  function openDocType(evtd, docNameTabd) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent2");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks2");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(docNameTabd).style.display = "block";
    evtd.currentTarget.className += " active";
  }
  // Get the element with id="defaultOpen" and click on it
  document.getElementById("defaultOpenTab").click();

  function openDocType1(evtd1, docNameTabd1) {
    var i, tabcontentd1, tablinksd1;
    tabcontentd1 = document.getElementsByClassName("tabcontent21");
    for (i = 0; i < tabcontentd1.length; i++) {
      tabcontentd1[i].style.display = "none";
    }
    tablinksd1 = document.getElementsByClassName("tablinks21");
    for (i = 0; i < tablinksd1.length; i++) {
      tablinksd1[i].className = tablinksd1[i].className.replace(" active", "");
    }
    document.getElementById(docNameTabd1).style.display = "block";
    evtd1.currentTarget.className += " active";
  }
  // Get the element with id="defaultOpen" and click on it
  document.getElementById("defaultOpenTab1").click();

  function popup(modalcontent) {
    $('[id*="modalDocument"]').modal('show');
    var userId = <?= $id ?>;
    console.log(modalcontent);
    if (modalcontent.doc_list.length) {
      //var courseHTMLL = '<form id="compare-course-form" name="compare-course-form" action="/course/compare" method="POST">';
      var selectHTML = '<option value=""> Select Uploading Document </option>';
      modalcontent.doc_list.forEach(function(value, index) {
        selectHTML += '<option value="' + value.document_master_id + '">' + value.display_name + '</option>';
      });
    }
    document.getElementById("doc_id").innerHTML = selectHTML;
    document.getElementById("title_name").innerHTML = modalcontent.display_name;
    if (modalcontent.user_doc_list.length) {
      var tableHTML = '';
      modalcontent.user_doc_list.forEach(function(valueu, indexu) {
        tableHTML += '<tr>' + '<td> ' + valueu.display_name + ' </td>' + '<td> ' + valueu.document_display_name + ' </td>' + '<td> <a href="' + valueu.document_url + '" target="_blank" class="btn btn-sm btn-success">View</a> </td>' + '<td> <button class="btn btn-danger" type="button" onclick="deleteDoc(' + valueu.document_master_id + ',' + userId + ')"> Delete</button> </td>' + '</tr>';
      });
    }
    document.getElementById("table_content").innerHTML = tableHTML;
  }

  function popupOne(modalcontent) {
    $('[id*="modalDocument1"]').modal('show');
    var userId = <?= $id ?>;
    console.log(modalcontent);
    if (modalcontent.doc_list.length) {
      //var courseHTMLL = '<form id="compare-course-form" name="compare-course-form" action="/course/compare" method="POST">';
      var selectHTML = '<option value=""> Select Uploading Document </option>';
      modalcontent.doc_list.forEach(function(value, index) {
        selectHTML += '<option value="' + value.document_master_id + '">' + value.display_name + '</option>';
      });
    }
    document.getElementById("doc_id").innerHTML = selectHTML;
    document.getElementById("title_name").innerHTML = modalcontent.display_name;
    if (modalcontent.user_doc_list.length) {
      var tableHTML = '';
      modalcontent.user_doc_list.forEach(function(valueu, indexu) {
        tableHTML += '<tr>' + '<td> ' + valueu.display_name + ' </td>' + '<td> ' + valueu.document_display_name + ' </td>' + '<td> <a href="' + valueu.document_url + '" target="_blank" class="btn btn-sm btn-success">View</a> </td>' + '<td> <button class="btn btn-danger" type="button" onclick="deleteDoc(' + valueu.document_master_id + ',' + userId + ')"> Delete</button> </td>' + '</tr>';
      });
    }
    document.getElementById("table_content").innerHTML = tableHTML;
  }

  function deleteDoc(docMasterId, userId) {
    var dataString = 'doc_master_id=' + docMasterId + '&user_id=' + userId;
    event.preventDefault();
    $.ajax({
      type: "POST",
      url: "/student/profile/deletedoc",
      data: dataString,
      cache: false,
      success: function(result) {
        if (result.status == "SUCCESS") {
          alert("Document Deleted Successfully!!!");
          window.location.reload();
        } else if (result.status == 'NOTVALIDUSER') {
          //alert(result);
          window.location.href = '/user/account';
        } else {
          alert(result);
          location.reload();
        }
      }
    });
  }

  function uploadDoc() {
    var fd = new FormData();
    var files = $('#userfile')[0].files;
    var docId = $('#doc_id').val();
    var appId = 1;
    var studentId = <?= $id ?>;
    var userDoc = $('#userdocname').val();
    // Check file selected or not
    if (files.length > 0) {
      fd.append('document', files[0]);
      fd.append('app_id', appId);
      fd.append('document_type', docId);
      fd.append('student_id', studentId);
      fd.append('doc_name', userDoc);
      event.preventDefault();
      $.ajax({
        url: '/admin/student/profile/appdocupdate',
        type: 'post',
        data: fd,
        contentType: false,
        processData: false,
        success: function(response) {
          console.log(response);
          if (response == 'SUCCESS') {
            alert('file uploaded');
            window.location.reload();
          } else {
            alert('file not uploaded');
          }
        },
      });
    } else {
      alert("Please select a file.");
    }
  }

  function editUniversityApplication(appId, universityId) {
    $("#applicationEditModal").modal('show');
    var dataString = 'app_id=' + appId + '&university_id=' + universityId;
    event.preventDefault();
    $.ajax({
      type: "POST",
      url: "/admin/counsellor/university/process/info",
      data: dataString,
      cache: false,
      success: function(result) {
        if (result.status == "SUCCESS") {
          var data = result.process_info;
          if (data) {
            document.getElementById("application_type").innerHTML = '';
            var opHtml = "<option value=''> Select Application Type </option>";
            data.application_fill_option.forEach(function(value) {
              if (value == 1) {
                opHtml += "<option value='1'> Imperial Will Fill Application </option>";
              }
              if (value == 2) {
                opHtml += "<option value='2'> Imperial Will Check Application </option>";
              }
            })
            document.getElementById("application_type").innerHTML = opHtml;
            document.getElementById("update_app_id").value = appId;
          }
        } else if (result.status == 'NOTVALIDUSER') {
          //alert(result);
          window.location.href = '/user/account';
        } else {
          alert(result);
          location.reload();
        }
      }
    });
  }

  function editStatusApplication(appId) {

    $("#applicationStatusEditModal").modal('show');
    document.getElementById("update_app_id").value = appId;

  }

  function checkSubStatus() {

    var leadStateValue = $('#app_status').find(":selected").val();

    if (leadStateValue == "ADMISSION_STATUS") {
      document.getElementById("subStatusOne").style.display = "block";
      document.getElementById("subStatusTwo").style.display = "none";
    } else if (leadStateValue == "FINAL_UNCONDITIONAL_ADMIT") {
      document.getElementById("subStatusOne").style.display = "none";
      document.getElementById("subStatusTwo").style.display = "block";
    } else {
      document.getElementById("subStatusOne").style.display = "none";
      document.getElementById("subStatusTwo").style.display = "none";
    }

  }


  function submitUniversityApplication() {
    var appType = $("#application_type").val();
    var appId = $("#update_app_id").val();
    var dataString = 'app_id=' + appId + '&app_type=' + appType;
    event.preventDefault();
    $.ajax({
      type: "POST",
      url: "/admin/counsellor/university/process/submit",
      data: dataString,
      cache: false,
      success: function(result) {
        if (result.status == "SUCCESS") {
          alert("Application Update Successfully!!!");
        } else if (result.status == 'NOTVALIDUSER') {
          //alert(result);
          window.location.href = '/user/account';
        } else {
          alert(result);
          location.reload();
        }
      }
    });
  }

  function submitStatusApplication() {

    var appStatus = $('#app_status').val() !== undefined ? $('#app_status').val() : "";
    var appSubStatus = $('#app_sub_status').val() !== undefined ? $('#app_sub_status').val() : "NULL";
    var appId = $("#update_app_id").val();
    var userId = <?= $id ?>;

    var dataString = 'app_status=' + appStatus + '&app_sub_status=' + appSubStatus + '&app_id=' + appId + '&user_id=' + userId;

    event.preventDefault();
    $.ajax({
      type: "POST",
      url: "/admin/counsellor/application/status/submit",
      data: dataString,
      cache: false,
      success: function(result) {
        if (result.status == "SUCCESS") {
          alert("Application Status Update Successfully!!!");
          location.reload();
        } else if (result.status == 'NOTVALIDUSER') {
          //alert(result);
          window.location.href = '/user/account';
        } else {
          alert(result);
          location.reload();
        }
      }
    });

  }

  function submitApplication(appId) {
    var stdName = $('#student_name_submit').val();
    var dataString = 'app_id=' + appId + '&user_id=' + userId + '&stdName=' + stdName;

    event.preventDefault();
    $.ajax({
      type: "POST",
      url: "/admin/counsellor/student/application/submit",
      data: dataString,
      cache: false,
      success: function(result) {
        if (result.status == "SUCCESS") {
          alert("Application Submitted Successfully!!!");
          window.location.reload();
        } else if (result.status == 'NOTVALIDUSER') {
          //alert(result);
          window.location.href = '/user/account';
        } else {
          alert(result);
          location.reload();
        }
      }
    });
  }

  var divScroll = document.querySelector('.a');

  function stretch() {
    var heightOrWidth = divScroll.scrollHeight - divScroll.clientHeight;
    var scrolled = (divScroll.scrollTop / heightOrWidth) * 100;
    document.querySelector(".p_bar").style.width = scrolled + "%";
  }
  divScroll.addEventListener('scroll', stretch);




  function openForm() {
    document.getElementById("myForm").style.display = "block";
  }

  function closeForm() {
    document.getElementById("myForm").style.display = "none";
  }

  function addComments(userId) {

    var comments = $("#comments").val();;
    var stdName = $('#student_name_submit').val();
    //alert(appId);
    var dataString = 'user_id=' + userId + '&comments=' + comments + '&stdName=' + stdName;
    event.preventDefault();
    $.ajax({
      type: "POST",
      url: "/admin/student/profile/commentsupdate",
      data: dataString,
      cache: false,
      success: function(result) {
        if (result.status == "SUCCESS") {
          alert("Comments Update Successfully!!!");
        } else if (result.status == 'NOTVALIDUSER') {
          //alert(result);
          window.location.href = '/user/account';
        } else {
          alert(result);
          location.reload();
        }
      }
    });
  }
</script>