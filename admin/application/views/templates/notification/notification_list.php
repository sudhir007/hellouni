<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<script type="text/javascript" src="
https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<style type="text/css">
   .styleCss{
   font-size: 14px;
   background-color: #f6882c;
   color: white;
   font-weight: 600;
   letter-spacing: 0.8px;
   }
   .formControl{
   color: #fff !important;
   background-color: #8c57c5 !important;
   /* border: 0px; */
   font-size: 12px;
   border: 0px !important;
   box-shadow: inset 0 0 0 rgba(0,0,0,.075) !important;
   margin-top: 15px;
   }

   #dt-example td.details-control {
     cursor: pointer;
}
 #dt-example tr.shown td {
     background-color: #815dd5;
     color: #fff !important;
     border: none;
     border-right: 1px solid #815dd5;
}
 #dt-example tr.shown td:last-child {
     border-right: none;
}
 #dt-example tr.details-row .details-table tr:first-child td {
     color: #fff;
     background: #f6882c;
     border: none;
}
 #dt-example tr.details-row .details-table > td {
     padding: 0;
}
 #dt-example tr.details-row .details-table .fchild td:first-child {
     cursor: pointer;
}
 #dt-example tr.details-row .details-table .fchild td:first-child:hover {
     background: #fff;
}
 #dt-example .form-group.agdb-dt-lst {
     padding: 2px;
     height: 23px;
     margin-bottom: 0;
}
 #dt-example .form-group.agdb-dt-lst .form-control {
     height: 23px;
     padding: 2px;
}
 #dt-example .adb-dtb-gchild {
     padding-left: 2px;
}
 #dt-example .adb-dtb-gchild td {
     background: #f5fafc;
     padding-left: 15px;
}
 #dt-example .adb-dtb-gchild td:first-child {
     cursor: default;
}
 #dt-example .fchild ~ .adb-dtb-gchild {
    /* display: none;
     */
}
 .dataTables_wrapper{
    overflow: auto;
 }

.listTableView thead tr{
 background-color: #f6882c;
    color: white;
}
</style>
<div class="container">
   <div class="row" style="padding: 30px 0px 30px 0px;display: flex;">

      <div class="col-md-12" >
        <div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-body listTableView">
          <div class="form-check" style="text-align: right;">
            <!-- <button type="submit" class="btn btn-login" ng-click="submitForm()">Submit</button> -->
              <a   style="text-align: center; border-radius:5px;" class="btn btn-md btn-info" href="/admin/counsellor/team/register"> ADD TEAM MEMBER </a>
          </div>
          <br/>
<!-- Dynamic table was here -->
<table id="dt-example" class="stripe row-border order-column cl-table dataTable no-footer" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th>Id</th>
      <th>Title</th>
      <th>Description</th>
      <th>Created By</th>
      <th>Created Date Time</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>
  </div>

 </div>
</div>
      </div>
   </div>
</div>
<div>
</div>

<script type="text/javascript">

        $('#data-table').dataTable();
        $(".dataTable").on("draw.dt", function (e) {
          //console.log("drawing");
          setCustomPagingSigns.call($(this));
        }).each(function () {
          setCustomPagingSigns.call($(this)); // initialize
        });
        function setCustomPagingSigns() {
          var wrapper = this.parent();
          wrapper.find("a.previous").text("<");
          wrapper.find("a.next").text(">");
        }
// =============Display details onclick===============
var dataTeam = [
    {"Name":"Deepika","Email":"deepika@gmail.com","Mobile":"India","number_of_student":"27.159.97.60","status":"active","action":"22"},
    {"Name":"Sudhir","Email":"s@gmail.com","Mobile":"USA","number_of_student":"67.135.55.135","status":"active","action":"22"},
    {"Name":"Nikhil","Email":"n@gmail.com","Mobile":"UK","number_of_student":"74.193.127.5","status":"active","action":"22"},
    {"Name":"Banit","Email":"b@gmail.com","Mobile":"Australia","number_of_student":"4.104.253.210","status":"active","action":"22"}
  ];

  var data = [];

  $.ajax({

  type: "GET",

  url: "/admin/counsellor/notification/list",

  cache: false,

  success: function(result){
//alert(result);
    console.log(result);

   var notificationdata = result.notification_list;

   if(notificationdata.length){

     notificationdata.forEach(function(value, index){

       data.push({
         "Id" : value.id,
         "Title" : value.title,
         "Description" : value.description,
         "Created By" : value.name,
         "Created Date Time" : value.date_created,
         "Action" : "<a class='btn btn-mg btn-info' href='/admin/student/profile/"+ value.user_id +"/0' target='_blank'> View </a>"

       });

     });

     console.log(data,"hi");

     var table = $('.dataTable').DataTable({

       // Column definitions
       columns : [
           {
               data : 'Id',
               className : 'details-control',
           },
           {data : 'Title'},
           {data : 'Description'},
           {data : 'Created By'},
           {data : 'Created Date Time'},
           {data : 'Action'}
       ],
       data : data,
       pagingType : 'full_numbers',
       // Localization
       language : {
         emptyTable     : 'No data to display.',
         zeroRecords    : 'No records found!',
         thousands      : ',',
         // processing     : 'Processing...',
         loadingRecords : 'Loading...',
         search         : 'Search:',
         paginate       : {
           next     : 'next',
           previous : 'previous'
         }
       }
     });

     $('.dataTable tbody').on('click', 'td.details-control', function () {
        var tr  = $(this).closest('tr'),
            row = table.row(tr);

        if (row.child.isShown()) {
          tr.next('tr').removeClass('details-row');
          row.child.hide();
          tr.removeClass('shown');
        }
        else {
          row.child(format(row.data())).show();
          tr.next('tr').addClass('details-row');
          tr.addClass('shown');
        }
     });

   $('#dt-example').on('click','.details-table tr',function(){
     $('.details-table').find('.adb-dtb-gchild:not(:first-child)').slideToggle();
       });


   }

  }

      });





</script>
