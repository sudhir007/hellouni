     <style type="text/css">.tt{ height:0px !important;}
	 .profileimg{ width:150px; height:150px; }
	 </style>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            University
            <small>Manage University Profile</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">University Management</a></li>
            <li class="active">University Profile</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">

            <!-- right column -->
            <div class="col-md-10">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">University Profile</h3>

                </div><!-- /.box-header -->
                <!-- form start -->
                <?php if($this->session->flashdata('flash_message')){ ?>
  				<div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                  <h4><i class="icon fa fa-check"></i> Success</h4>
                  <?php echo $this->session->flashdata('flash_message'); ?>
                </div>
  			  <?php } ?>

				<?php //$userdata = $this->session->userdata;?>
                <form class="form-horizontal" action="<?php echo base_url();?>profile/university/save/<?php if(isset($id)) echo $id; ?>" method="post" name="adduniversity" enctype="multipart/form-data">




                  <div class="box-body">

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Name *</label>
                      <div class="col-sm-10">
                        <input type="name" class="form-control" id="uniname" name="uniname" placeholder="Name" value="<?php if(isset($name)) echo $name; ?>">
                      </div>
                    </div>

					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Email *</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php if(isset($email)) echo $email; ?>">
                      </div>
                    </div>


					<div class="form-group">
                      <label for="website" class="col-sm-2 control-label">Website</label>
                      <div class="col-sm-10">
                        <input type="website" class="form-control" id="website" name="website" placeholder="Website" value="<?php if(isset($website)) echo $website; ?>">
                      </div>
                    </div>

					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Phone</label>
                      <div class="col-sm-10">
                        <input type="name" class="form-control" id="phone" name="phone" placeholder="Phone" value="<?php if(isset($phone)){ echo $phone; }?>">
                      </div>
                    </div>


					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Address</label>
                      <div class="col-sm-10">
                       <!-- <input type="name" class="form-control" id="address" name="address" placeholder="Address" value="<?php if(isset($address)) echo $address; ?>">-->
						<textarea class="form-control" id="address" name="address" cols="" rows="5"><?php if(isset($address)) echo $address; ?></textarea>
						<script type="text/javascript">CKEDITOR.replace( 'address' );</script>
                      </div>
                    </div>

					<div class="form-group">
                  		 <label for="country" class="col-sm-2 control-label">Country *</label>
						  <div class="col-sm-10">
						  <select class="form-control" name="country">
						  <?php foreach($countries as $country){?>
							<option value="<?php echo $country->id;?>" <?php if(isset($country_id) && $country_id==$country->id){ echo 'selected="selected"'; } ?>><?php echo $country->name;?></option>

							<?php } ?>
						  </select>
						  </div>
               		</div>

					<div class="form-group">
                  		 <label for="state" class="col-sm-2 control-label">State</label>
						  <div class="col-sm-10">
						   <input type="name" class="form-control" id="state" name="state" placeholder="State" value="<?php if(isset($state)){ echo $state; }?>">
						  </div>
               		</div>


					<div class="form-group">
                  		 <label for="city" class="col-sm-2 control-label">City</label>
						  <div class="col-sm-10">
						   <input type="name" class="form-control" id="city" name="city" placeholder="City" value="<?php if(isset($city)){ echo $city; }?>">
						  </div>
               		</div>

					<div class="box-header with-border">
                  <h3 class="box-title">About University</h3>
                </div>

					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">About</label>
                      <div class="col-sm-10">

						<textarea class="form-control" id="about" name="about" cols="" rows="5"><?php if(isset($about)){ echo $about; }?></textarea>
						<script type="text/javascript">CKEDITOR.replace( 'about' );</script>
                      </div>
                    </div>
					<div class="form-group">
                      <label for="student" class="col-sm-2 control-label">Total Student</label>
                      <div class="col-sm-10">
                        <input type="website" class="form-control" id="student" name="student" placeholder="Total Number of Student" value="<?php if(isset($student)) echo $student; ?>">
                      </div>
                    </div>

					<div class="form-group">
                      <label for="internationalstudent" class="col-sm-2 control-label">Total % of International Students</label>
                      <div class="col-sm-10">
                        <input type="internationalstudent" class="form-control" id="internationalstudent" name="internationalstudent" placeholder="Total % of International Students" value="<?php if(isset($internationalstudent)) echo $internationalstudent; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="national_ranking" class="col-sm-2 control-label">National Ranking</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="national_ranking" name="ranking_usa" placeholder="National Ranking" value="<?php if(isset($ranking_usa)) echo $ranking_usa; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="world_ranking" class="col-sm-2 control-label">World Ranking</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="world_ranking" name="ranking_world" placeholder="World Ranking" value="<?php if(isset($ranking_world)) echo $ranking_world; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="placement_percentage" class="col-sm-2 control-label">Placement Percentage</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="placement_percentage" name="placement_percentage" placeholder="Placement Percentage" value="<?php if(isset($placement_percentage)) echo $placement_percentage; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="admission_success_rate" class="col-sm-2 control-label">Admission Success Rate</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="admission_success_rate" name="admission_success_rate" placeholder="Admission Success Rate" value="<?php if(isset($admission_success_rate)) echo $admission_success_rate; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="research_spending" class="col-sm-2 control-label">Research Spending</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="research_spending" name="research_spending" placeholder="Research Spending" value="<?php if(isset($research_spending)) echo $research_spending; ?>">
                      </div>
                    </div>

					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Facilities</label>
                      <div class="col-sm-10">
						<textarea class="form-control" id="facilities" name="facilities" cols="" rows="5"><?php if(isset($facilities)) echo $facilities; ?></textarea>
						<script type="text/javascript">CKEDITOR.replace( 'facilities' );</script>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="unique_selling_points" class="col-sm-2 control-label">USPs</label>
                      <div class="col-sm-10">
						<textarea class="form-control" id="unique_selling_points" name="unique_selling_points" cols="" rows="5"><?php if(isset($unique_selling_points)) echo $unique_selling_points; ?></textarea>
						<script type="text/javascript">CKEDITOR.replace( 'unique_selling_points' );</script>
                      </div>
                    </div>

					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Image</label>
                      <div class="col-sm-10">
					  <?php if(isset($logo)){ ?>
					  <img src="<?php echo base_url() . '..' . $logo . '?t=' . time(); ?>" class="profileimg"/>
					  <?php } ?>
					  <input type="hidden" name="profileimage" value="<?php echo $logo; ?>"/>
                      <input type="file" name="fileToUpload" id="fileToUpload" />
                      </div>
                    </div>

					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Banner Image</label>
                      <div class="col-sm-10">
					  <?php if(isset($banner)){ ?>
					  <img src="<?php echo base_url() .'..' .$banner . '?t=' . time(); ?>" class="profileimg"/>
					  <?php } ?>
					  <input type="hidden" name="banner" value="<?php echo $banner; ?>"/>
                      <input type="file" name="bannerToUpload" id="bannerToUpload" />
                      </div>
                    </div>


                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
					<button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                    <button type="submit" name="submit" class="btn btn-info pull-right">Update</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              <!-- general form elements disabled -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

	<script type="text/javascript">
	 // $( document ).ready(function() {
	  $().ready(function() {
$('#browse-1').click(function(){
    var f = $('#elfileimg-1').elfinder({
        url : 'http://inventifweb.net/UNI/elfinder/php/connector.php',
        height: 490,
        docked: false,
        dialog: { width: 400, modal: true },
        closeOnEditorCallback: true,
        getFileCallback: function(url) {
            $('#elfileurl-1').val(url);
            // CLOSE ELFINDER HERE
            $('#elfinder').remove();  //remove Elfinder
            location.reload();   //reload Page for second selection
        }
    }).elfinder('instance');
});

//alert('+++');
});
</script>
