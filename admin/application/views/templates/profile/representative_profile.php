     <style type="text/css">.tt{ height:0px !important;}</style>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Representative
            <small>Manage Representative Profile</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Manage Profile</a></li>
            <li class="active">My Profile</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
           
            <!-- right column -->
            <div class="col-md-10">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Representative Profile</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
				
				<?php //$userdata = $this->session->userdata;?>
                <form class="form-horizontal" action="<?php echo base_url();?>profile/representative/save/<?php //if(isset($id)) echo $id; ?>" method="post" name="adduniversity" enctype="multipart/form-data">
         
				
				
                
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                      <div class="col-sm-10">
                        <input type="name" class="form-control" id="name" name="name" placeholder="Name" value="<?php if(isset($name)) echo $name; ?>">
                      </div>
                    </div>
					
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php if(isset($email)) echo $email; ?>">
                      </div>
                    </div>
					
					
					
					
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Phone</label>
                      <div class="col-sm-10">
                        <input type="name" class="form-control" id="phone" name="phone" placeholder="Phone" value="<?php if(isset($phone)){ echo $phone; }?>">
                      </div>
                    </div>
					
					<div class="form-group">
                      <label for="university" class="col-sm-2 control-label">University</label>
                      <div class="col-sm-10">
                        <input type="website" class="form-control" id="university" name="university" value="<?php if(isset($university)) echo $university; ?>" disabled="disabled">
                      </div>
                    </div>
					
                 					
					
					
					
					
                    
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
					<button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                    <button type="submit" class="btn btn-info pull-right">Update</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              <!-- general form elements disabled -->
             
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     