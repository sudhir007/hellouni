
    
        <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- Statistics Button Container -->
            	
                
                <!-- Panels Start -->
                 
            	 <?php if($msg = $this->session->flashdata('flash_message')){?> 
    <div class="mws-form-message success"><?php echo $msg;?></div>
     <?php } ?>
	 
	 
	  <?php if($ermsg = $this->session->flashdata('error_message')){?> 
    <div class="mws-form-message error"><?php echo $ermsg;?></div>
     <?php } ?>
				<div class="mws-panel grid_8">
				<form action="<?php echo base_url();?>plugins/gallery/delete_multi/photo" method="post">
                	<div class="mws-panel-header">
					
                    	<span><i class="icon-table"></i> Photo Gallery
						<a href="<?php echo base_url();?>plugins/gallery/form/photo"><button type="button" class="btn btn-small">Insert</button></a>
						<button type="submit" name="delete-Gallery" id="del" class="btn btn-small">Delete</button>
						
						</span>
						
					
						
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-datatable-fn mws-table">
                            <thead>
                                <tr>
                                    <th style="width: 1px;">Select</th>
									<th>Name</th>
									<th>Sort Order</th>
                                  
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
							
							<tbody>
                                <?php foreach($gallery as $gal):?>
								<tr>
                                    <td align="center"><input type="checkbox" name="chk[]" class="brand_chk" id="<?php echo $gal->id;?>" value="<?php echo $gal->id;?>"/></td>
									<td><?php echo $gal->name;?></td>
									<td><?php echo $gal->sort_order;?></td>
                                    <td><?php echo $gal->stname;?></td>
									<td class=" ">
                                        <span class="btn-group">
										 <a class="btn btn-small" href="<?php echo base_url();?>plugins/gallery/form/photo/<?php echo $gal->id;?>/view"><i class="icon-search"></i></a>
											 
										
                                            <a class="btn btn-small" href="<?php echo base_url();?>plugins/gallery/form/photo/<?php echo $gal->id;?>"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small delete-row" id="<?php echo base_url(); ?>plugins/gallery/delete/photo/<?php echo $gal->id;?>" href="javascript:void(o);"><i class="icon-trash"></i></a></span>
                                    </td>
                                </tr>
                               <?php endforeach;?>
                            </tbody>
                            
                        </table>
                    </div>
					
					</form>
                </div>


                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->
			
