<style type="text/css">
    .chkbx{
        float: left;
        margin: 8px 23px 1px 0px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>University<small>Application Detail</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">University Management</a></li>
            <li class="active">Application Detail</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-10">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <!-- form start -->
                    <form class="form-horizontal" action="" method="post" name="adduni" autocomplete="off">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="university" class="col-sm-2 control-label">University</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="university_name" value="<?php echo $university_name;?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="college" class="col-sm-2 control-label">College</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="college_name" value="<?php echo $college_name;?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="university" class="col-sm-2 control-label">Student Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="student_name" value="<?php echo $student_name;?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="student_email" class="col-sm-2 control-label">Student Email</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="student_email" value="<?php echo $student_email;?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                              <label for="student_phone" class="col-sm-2 control-label">Student Phone</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control" id="student_phone" name="student_phone" value="<?php echo $student_phone;?>" disabled>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="ofc_date" class="col-sm-2 control-label">OFC Date</label>
                              <div class="col-sm-10">
                                <input type="text" class="mws-datepicker small" id="ofc_date" name="ofc_date" value="<?php echo $ofc_date;?>" disabled>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="interview_date" class="col-sm-2 control-label">Interview Date</label>
                              <div class="col-sm-10">
                                <input type="text" class="mws-datepicker small" id="interview_date" name="interview_date" value="<?php echo $interview_date;?>" disabled>
                              </div>
                            </div>
                            <div class="form-group">
                                <label for="ds_160" class="col-sm-2 control-label">DS 160</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="ds_160" value="<?php echo $ds_160;?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="cgi_federal" class="col-sm-2 control-label">CGI Federal</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="cgi_federal" value="<?php echo $cgi_federal;?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="sevis">Sevis</label>
                                <div class="col-sm-10">
                                    <input type="text" name="sevis" class="form-control" value="<?=$sevis?>">
                                </div>
                            </div>
                            <div class="form-group">
                              <label for="mock_date_1" class="col-sm-2 control-label">Mock Date 1</label>
                              <div class="col-sm-4">
                                <input type="text" class="mws-datepicker small" id="mock_date_1" name="mock_date_1" value="<?php echo $mock_date_1;?>" disabled>
                              </div>
                              <label for="mock_slot_1" class="col-sm-2 control-label">Mock Slot 1</label>
                              <div class="col-md-4">
                                  <select name="mock_slot_1" disabled>
                                     <option value="10AM_to_2PM" <?=$mock_slot_1 == '10AM_to_2PM' ? 'selected' : ''?>>10AM to 2PM</option>
                                     <option value="3PM_to_7PM" <?=$mock_slot_1 == '3PM_to_7PM' ? 'selected' : ''?>>3PM to 7PM</option>
                                   </select>
                               </div>
                            </div>
                            <div class="form-group">
                              <label for="mock_date_2" class="col-sm-2 control-label">Mock Date 2</label>
                              <div class="col-sm-4">
                                <input type="text" class="mws-datepicker small" id="mock_date_2" name="mock_date_2" value="<?php echo $mock_date_2;?>" disabled>
                              </div>
                              <label for="mock_slot_2" class="col-sm-2 control-label">Mock Slot 2</label>
                              <div class="col-md-4">
                                  <select name="mock_slot_2" disabled>
                                     <option value="10AM_to_2PM" <?=$mock_slot_2 == '10AM_to_2PM' ? 'selected' : ''?>>10AM to 2PM</option>
                                     <option value="3PM_to_7PM" <?=$mock_slot_2 == '3PM_to_7PM' ? 'selected' : ''?>>3PM to 7PM</option>
                                   </select>
                               </div>
                            </div>
                            <div class="form-group">
                              <label for="mock_date_3" class="col-sm-2 control-label">Mock Date 3</label>
                              <div class="col-sm-4">
                                <input type="text" class="mws-datepicker small" id="mock_date_3" name="mock_date_3" value="<?php echo $mock_date_3;?>" disabled>
                              </div>
                              <label for="mock_slot_3" class="col-sm-2 control-label">Mock Slot 3</label>
                              <div class="col-md-4">
                                  <select name="mock_slot_1" disabled>
                                     <option value="10AM_to_2PM" <?=$mock_slot_3 == '10AM_to_2PM' ? 'selected' : ''?>>10AM to 2PM</option>
                                     <option value="3PM_to_7PM" <?=$mock_slot_3 == '3PM_to_7PM' ? 'selected' : ''?>>3PM to 7PM</option>
                                   </select>
                               </div>
                            </div>
                            <div class="form-group">
                              <label for="status" class="col-sm-2 control-label">Status</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control" id="status" name="status" value="<?php echo $status;?>" disabled>
                              </div>
                            </div>
                        </div><!-- /.box-body -->
                    </form>
                </div><!-- /.box -->
                <!-- general form elements disabled -->
            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
