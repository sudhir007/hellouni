<style type="text/css">
    .chkbx{
        float: left;
        margin: 8px 23px 1px 0px;
    }
    #loader {
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 120px;
        height: 120px;
        -webkit-animation: spin 2s linear infinite; /* Safari */
        animation: spin 2s linear infinite;
        display: none;
        position: fixed;
        top: 40%;
        left: 50%;
    }
    /* Safari */
    @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
    }
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
    .form-group label{
        text-align: left !important;
        padding-left: 40px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Student<small>Manage Internships</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Student Management</a></li>
            <li class="active">Internship Form</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-10">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Internship</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php
                    if($this->session->flashdata('flash_message'))
                    {
                        ?>
                        <div class="alert alert-error alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                            <h4><i class="icon fa fa-circle-o"></i> Error</h4>
                            <?php echo $this->session->flashdata('flash_message'); ?>
                        </div>
                        <?php
                    }
                    if($this->uri->segment('4'))
                    {
                        $action = base_url() . 'university/internship/edit/' . $id;
                    }
                    else
                    {
                        $action = base_url() . 'university/internship/create';
                    }
                    ?>
                    <form class="form-horizontal" action="<?=$action?>" method="post" name="adduni" autocomplete="off" enctype="multipart/form-data">
                        <div class="box-body">
                            <?php
                            if(!$edit)
                            {
                                if(!$isUniversity)
                                {
                                    ?>
                                    <div class="form-group">
                                        <label for="country" class="col-sm-4 control-label">Country *</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="country" onclick="showUniversities(this.value)">
                                                <option value="">Please Select</option>
                                                <?php
                                                foreach($countries as $countryData)
                                                {
                                                    ?>
                                                    <option value="<?=$countryData->id?>"><?=$countryData->name?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="university" class="col-sm-4 control-label">University *</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="university" id="universities_list" onclick="showColleges(this.value)"></select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="college" class="col-sm-4 control-label">College *</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="college" id="colleges_list" onclick="showDepartments(this.value)"></select>
                                        </div>
                                    </div>
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <div class="form-group">
                                        <label for="country" class="col-sm-4 control-label">Country *</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="country" disabled>
                                                <option value="">Please Select</option>
                                                <?php
                                                foreach($countries as $countryData)
                                                {
                                                    ?>
                                                    <option value="<?=$countryData->id?>" <?=($countryData->id == $country_id) ? 'selected' : ''?>><?=$countryData->name?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="university" class="col-sm-4 control-label">University *</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="university" value="<?php echo $university_name;?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="college" class="col-sm-4 control-label">College *</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="college" id="colleges_list" onclick="showDepartments(this.value)">
                                                <option value="">Please Select</option>
                                                <?php
                                                foreach($colleges as $college)
                                                {
                                                    ?>
                                                    <option value="<?=$college->id?>"><?=$college->name?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="form-group">
                                    <label for="department" class="col-sm-4 control-label">Department *</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="department" id="departments_list"></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="col-sm-4 control-label">Title *</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="title" name="title" placeholder="Title">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="description" class="col-sm-4 control-label">Description *</label>
                                    <div class="col-sm-8">
                                        <textarea rows="4" class="form-control" id="description" name="description" placeholder="Description for Internship Offered & Predicted Outcomes"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label for="application_deadline" class="col-sm-4 control-label">Deadline To Receive Application *</label>
                                  <div class="col-sm-8">
                                    <input type="text" class="mws-datepicker small" id="datepicker" name="application_deadline">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="internship_start_date" class="col-sm-4 control-label">Internship From *</label>
                                  <div class="col-sm-8">
                                    <input type="text" class="mws-datepicker small" id="datepicker_1" name="internship_start_date">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="internship_end_date" class="col-sm-4 control-label">Internship To *</label>
                                  <div class="col-sm-8">
                                    <input type="text" class="mws-datepicker small" id="datepicker_2" name="internship_end_date">
                                  </div>
                                </div>
                                <div class="form-group">
                                    <label for="professor_guide_name" class="col-sm-4 control-label">Professor/Guide Name-1</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="professor_guide_name[]" placeholder="Professor / Guide Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="professor_guide_link" class="col-sm-4 control-label">Professor/Guide Profile Link-1</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="professor_guide_link[]" placeholder="Professor / Guide Link">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="professor_guide_photo" class="col-sm-4 control-label">Professor/Guide Photo-1</label>
                                    <div class="col-sm-8">
                                        <input class="col-md-6" type="file" name="photo[]">
                                    </div>
                                </div>
                                <div class="form-group" id="professors-guides">
                                    <button type="button" style="width:12%; float:right; margin-right:5px;" class="btn btn-block btn-primary" onclick="addProfessor()"><i class="fa fa-plus"></i> Add Professor / Guide</button>
                                </div>
                                <div class="form-group">
                                    <label for="charges" class="col-sm-4 control-label">Charges</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="charges" name="charges" placeholder="Stipend / Accommodation / Food Expenses">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="other_fees" class="col-sm-4 control-label">Other Fees</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="other_fees[]" placeholder="Other Incidental Fees">
                                    </div>
                                </div>
                                <!--div class="form-group" id="other-fees">
                                    <button type="button" style="width:12%; float:right; margin-right:5px;" class="btn btn-block btn-primary" onclick="addOtherFee()"><i class="fa fa-plus"></i> Add Other Fee</button>
                                </div-->
                                <div class="form-group" id="extra-question">
                                    <button type="button" style="width:12%; float:right; margin-right:5px;" class="btn btn-block btn-primary" onclick="addQuestion()"><i class="fa fa-plus"></i> Add Question</button>
                                </div>
                                <?php
                            }
                            else
                            {
                                ?>
                                <div class="form-group">
                                    <label for="university" class="col-sm-2 control-label">University *</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="university" value="<?php echo $university_name;?>" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="college" class="col-sm-2 control-label">College *</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="college" value="<?php echo $college_name;?>" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="department" class="col-sm-2 control-label">Department *</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="department" value="<?php echo $department_name;?>" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="col-sm-2 control-label">Title *</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="<?php echo $title;?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="description" class="col-sm-2 control-label">Description *</label>
                                    <div class="col-sm-10">
                                        <textarea rows="4" class="form-control" id="description" name="description" placeholder="Description"><?php echo $description; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label for="application_deadline" class="col-sm-2 control-label">Deadline To Receive Application *</label>
                                  <div class="col-sm-10">
                                    <input type="text" class="mws-datepicker small" id="datepicker" name="application_deadline" value="<?php echo $application_deadline;?>">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="internship_start_date" class="col-sm-2 control-label">Internship From *</label>
                                  <div class="col-sm-10">
                                    <input type="text" class="mws-datepicker small" id="datepicker_1" name="internship_start_date" value="<?php echo $internship_start_date;?>">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="internship_end_date" class="col-sm-2 control-label">Internship To *</label>
                                  <div class="col-sm-10">
                                    <input type="text" class="mws-datepicker small" id="datepicker_2" name="internship_end_date" value="<?php echo $internship_end_date;?>">
                                  </div>
                                </div>
                                <?php
                                if($professors_guide)
                                {
                                    foreach($professors_guide as $index => $value)
                                    {
                                        ?>
                                        <div class="form-group">
                                            <label for="professor_guide_name" class="col-sm-2 control-label">Professor/Guide Name-<?=($index+1)?></label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="professor_guide_name[]" placeholder="Professor / Guide Name" value="<?php echo $value['name'];?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="professor_guide_link" class="col-sm-2 control-label">Professor/Guide Profile Link-<?=($index+1)?></label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="professor_guide_link[]" placeholder="Professor / Guide Link" value="<?php echo $value['link'];?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="professor_guide_photo" class="col-sm-2 control-label">Professor/Guide Photo-<?=($index+1)?></label>
                                            <div class="col-sm-10">
                                                <input class="col-md-6" type="file" name="photo[]">
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                                <div class="form-group" id="professors-guides">
                                    <button type="button" style="width:12%; float:right; margin-right:5px;" class="btn btn-block btn-primary" onclick="addProfessor()"><i class="fa fa-plus"></i> Add Professor / Guide</button>
                                </div>
                                <div class="form-group">
                                    <label for="charges" class="col-sm-2 control-label">Charges</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="charges" name="charges" placeholder="Charges" value="<?php echo $charges; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="other_fees" class="col-sm-2 control-label">Other Fees</label>
                                    <div class="col-sm-10">
                                        <textarea rows="4" class="form-control" id="other_fees" name="other_fees[]" placeholder="Other Fees"><?php echo isset($other_fees[0]) ? $other_fees[0] : ''; ?></textarea>
                                    </div>
                                </div>
                                <?php
                                if($other_details)
                                {
                                    foreach($other_details as $index => $question)
                                    {
                                        ?>
                                        <div class="form-group">
                                            <label for="charges" class="col-sm-2 control-label">Question <?=($index+1)?></label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="questions[]" value="<?php echo $question; ?>">
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                                <div class="form-group" id="extra-question">
                                    <button type="button" style="width:12%; float:right; margin-right:5px;" class="btn btn-block btn-primary" onclick="addQuestion()"><i class="fa fa-plus"></i> Add Question</button>
                                </div>
                                <?php
                            }
                            ?>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                            <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                            <?php
                            if($this->uri->segment('4'))
                            {
                                ?>
                                <button type="submit" class="btn btn-info pull-right">Update</button>
                                <?php
                            }
                            else
                            {
                                ?>
                                <button type="submit" class="btn btn-info pull-right">Create</button>
                                <?php
                            }
                            ?>
                        </div><!-- /.box-footer -->
                    </form>
                </div><!-- /.box -->
                <!-- general form elements disabled -->
            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
