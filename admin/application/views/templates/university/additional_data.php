<style type="text/css">
   .chkbx {
      float: left;
      margin: 8px 23px 1px 0px;
   }

   /* Style tab links */
   .tablink {
      background-color: #555;
      color: white;
      float: left;
      border: none;
      outline: none;
      cursor: pointer;
      padding: 14px 16px;
      font-size: 17px;
      width: 50%;
   }

   .tablink:hover {
      background-color: #777;
   }

   /* Style the tab content (and add height:100% for full page content) */
   .tabcontent {
      color: white;
      display: none;
      padding: 50px 20px;
      height: 100%;
   }

   .accordion {
      background-color: white;
      color: #367fa9;
      cursor: pointer;
      padding: 10px;
      width: 100%;
      border: none;
      text-align: left;
      outline: none;
      font-size: 22px;
      transition: 0.4s;
      box-shadow: 3px 3px 7px #00000042;
   }

   .active,
   .accordion:hover {
      background-color: #367fa9;
      color: #fff;
   }

   .accordion:after {
      content: '\002B';
      color: #777;
      font-weight: bold;
      float: right;
      margin-left: 5px;
   }

   .active:after {
      content: "\2212";
      color: #fff;
   }

   .panel {
      padding: 0 18px;
      background-color: white;
      max-height: 0;
      overflow: hidden;
      transition: max-height 0.2s ease-out;
      border: 1px solid #367fa9;
   }

   .panel .form-group {
      color: black;
   }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1> University <small> Info <b>(<?= $universityName ?>)</b> </small></h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="#">University</a></li>
         <li class="active"> Additional Data</li>
      </ol>
   </section>
   <!--  <section class="content">
      <div class="col-md-12">

        <div id="Doc" class="tabcontent" style="background-color: white;color: black;">
            <h3>Document Upload Section</h3>
            <button class="accordion">Section 1</button>
            <div class="panel">
               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            </div>
            <button class="accordion">Section 2</button>
            <div class="panel">
               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            </div>
            <button class="accordion">Section 3</button>
            <div class="panel">
               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            </div>
        </div>
        <div id="Data" class="tabcontent" style="background-color: white;color: black;">
            <h3>Data</h3>
            <p>Some news this fine day!</p>
        </div>
      </div>
   </section> -->
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- right column -->
         <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
               <div class="box-header with-border">
                  <h3 class="box-title"> Additional Data (All fields / documents are non mandatory. Kindly fill, what best possible) </h3>
               </div>
               <button class="tablink" onclick="openPage('Doc', this, '#367fa9')" id="defaultOpen">Document Upload Section</button>
               <button class="tablink" onclick="openPage('Data', this, '#367fa9')"> Text Data Section </button>
               <!-- /.box-header -->
               <!-- form start -->
               <?php if ($this->session->flashdata('flash_message')) {
               ?>
                  <div class="alert alert-error alert-dismissible">
                     <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                     <h4><i class="icon fa fa-circle-o"></i> Error</h4>
                     <?php echo $this->session->flashdata('flash_message'); ?>
                  </div>
               <?php } ?>
               <div class="box-body">
                  <div id="Doc" class="tabcontent" style="background-color: white;color: black;">
                     <form class="form-horizontal" name="doc">

                        <h2>Document Upload Section</h2>
                        <div class="col-md-12">
                           <button class="accordion active">University Banner</button>
                           <div class="panel" style="max-height: fit-content">
                              <div class="form-group">
                                 <div class="col-md-12">
                                    <h3>Upload Document (Max file size is 10mb/per file. Format recommended are PDF / DOCX / PNG / JPG)</h3>
                                    <br>
                                    <div class="col-md-8">
                                       <label for="name" class="col-md-4 control-label">Image Details</label>
                                       <div class="col-md-8">
                                          <input class="form-control" id="images_eleven_details" name="images_eleven_details" placeholder="Image Details" value="">
                                       </div>
                                    </div>
                                    <div class="col-md-8">
                                       <input type="file" class="form-control" name="images_eleven[]" id="images_eleven" multiple />
                                    </div>
                                    <div class="col-md-4">
                                       <button type="button" class="btn btn-info" onclick="upload_image('images_eleven','banner',<?= $universityId ?>,'images_eleven_data')"> Submit </button>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <h2>Uploaded Document List</h2>
                                    <table class="table table-bordered">
                                       <thead>
                                          <th style="width: 20%;">ID</th>
                                          <th style="width: 40%;">URL</th>
                                          <th style="width: 40%;">Details</th>
                                          <th>View</th>
                                          <th>Delete</th>
                                       </thead>
                                       <tbody id="images_eleven_data">
                                          <?php

                                          if ($universityAdditionalData['university_banner']) {

                                             foreach ($universityAdditionalData['university_banner'] as $itwokey => $itwovalue) {

                                          ?>
                                                <tr>

                                                   <td><?php echo $itwovalue['image_id']; ?></td>
                                                   <td><?php echo $itwovalue['image_url']; ?></td>
                                                   <td><?php if (isset($itwovalue['image_details'])) {
                                                            echo $itwovalue['image_details'];
                                                         } else {
                                                            echo "N.A";
                                                         } ?></td>
                                                   <td><a href="<?php echo $itwovalue['image_url']; ?>" target="_blank" class="btn btn-sm"><i class="fas fa-eye"></i></a></td>
                                                   <td><button class="btn btn-sm" type="button" onclick="deleteUniversityImage(<?php echo $itwovalue['image_id']; ?>,'banner','images_eleven_data',<?= $universityId ?>)"><i class="fas fa-trash"></i></button></td>

                                                </tr>

                                          <?php }
                                          } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                           <button class="accordion">University LOGO</button>
                           <div class="panel">
                              <div class="form-group">
                                 <div class="col-md-12">
                                    <h3>Upload Document (Max file size is 10mb/per file. Format recommended are PDF / DOCX / PNG / JPG)</h3>
                                    <br>
                                    <div class="col-md-8">
                                       <label for="name" class="col-md-4 control-label">Image Details</label>
                                       <div class="col-md-8">
                                          <input class="form-control" id="images_twelve_details" name="images_twelve_details" placeholder="Image Details" value="">
                                       </div>
                                    </div>
                                    <div class="col-md-8">
                                       <input type="file" class="form-control" name="images_twelve[]" id="images_twelve" multiple />
                                    </div>
                                    <div class="col-md-4">
                                       <button type="button" class="btn btn-info" onclick="upload_image('images_twelve','logo',<?= $universityId ?>,'images_twelve_data')"> Submit </button>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <h2>Uploaded Document List</h2>
                                    <table class="table table-bordered">
                                       <thead>
                                          <th style="width: 20%;">ID</th>
                                          <th style="width: 40%;">URL</th>
                                          <th style="width: 40%;">Details</th>
                                          <th>View</th>
                                          <th>Delete</th>
                                       </thead>
                                       <tbody id="images_twelve">
                                          <?php

                                          if ($universityAdditionalData['university_logo']) {

                                             foreach ($universityAdditionalData['university_logo'] as $itwokey => $itwovalue) {

                                          ?>
                                                <tr>

                                                   <td><?php echo $itwovalue['image_id']; ?></td>
                                                   <td><?php echo $itwovalue['image_url']; ?></td>
                                                   <td><?php if (isset($itwovalue['image_details'])) {
                                                            echo $itwovalue['image_details'];
                                                         } else {
                                                            echo "N.A";
                                                         } ?></td>
                                                   <td><a href="<?php echo $itwovalue['image_url']; ?>" target="_blank" class="btn btn-sm"><i class="fas fa-eye"></i></a></td>
                                                   <td><button class="btn btn-sm" type="button" onclick="deleteUniversityImage(<?php echo $itwovalue['image_id']; ?>,'logo','images_twelve_data',<?= $universityId ?>)"><i class="fas fa-trash"></i></button></td>

                                                </tr>

                                          <?php }
                                          } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                           <button class="accordion">University Map Preview</button>
                           <div class="panel">
                              <div class="form-group">
                                 <div class="col-md-12">
                                    <h3>Upload Document (Max file size is 10mb/per file. Format recommended are PDF / DOCX / PNG / JPG)</h3>
                                    <br>
                                    <div class="col-md-8">
                                       <label for="name" class="col-md-4 control-label">Image Details</label>
                                       <div class="col-md-8">
                                          <input class="form-control" id="images_thirteen_details" name="images_thirteen_details" placeholder="Image Details" value="">
                                       </div>
                                    </div>
                                    <div class="col-md-8">
                                       <input type="file" class="form-control" name="images_thirteen[]" id="images_thirteen" multiple />
                                    </div>
                                    <div class="col-md-4">
                                       <button type="button" class="btn btn-info" onclick="upload_image('images_thirteen','map_preview',<?= $universityId ?>,'images_thirteen_data')"> Submit </button>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <h2>Uploaded Document List</h2>
                                    <table class="table table-bordered">
                                       <thead>
                                          <th style="width: 20%;">ID</th>
                                          <th style="width: 40%;">URL</th>
                                          <th style="width: 40%;">Details</th>
                                          <th>View</th>
                                          <th>Delete</th>
                                       </thead>
                                       <tbody id="images_thirteen">
                                          <?php

                                          if ($universityAdditionalData['university_map_preview']) {

                                             foreach ($universityAdditionalData['university_map_preview'] as $itwokey => $itwovalue) {

                                          ?>
                                                <tr>

                                                   <td><?php echo $itwovalue['image_id']; ?></td>
                                                   <td><?php echo $itwovalue['image_url']; ?></td>
                                                   <td><?php if (isset($itwovalue['image_details'])) {
                                                            echo $itwovalue['image_details'];
                                                         } else {
                                                            echo "N.A";
                                                         } ?></td>
                                                   <td><a href="<?php echo $itwovalue['image_url']; ?>" target="_blank" class="btn btn-sm"><i class="fas fa-eye"></i></a></td>
                                                   <td><button class="btn btn-sm" type="button" onclick="deleteUniversityImage(<?php echo $itwovalue['image_id']; ?>,'map_preview','images_thirteen_data',<?= $universityId ?>)"> <i class="fas fa-trash"></i></button></td>

                                                </tr>

                                          <?php }
                                          } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                           <button class="accordion">General Brochures for International Students</button>
                           <div class="panel">
                              <div class="form-group">
                                 <div class="col-md-12">
                                    <h3>Upload Document (Max file size is 10mb/per file. Format recommended are PDF / DOCX / PNG / JPG)</h3>
                                    <br>
                                    <div class="col-md-8">
                                       <label for="name" class="col-md-4 control-label">Brochures Details</label>
                                       <div class="col-md-8">
                                          <input class="form-control" id="images_two_details" name="images_two_details" placeholder="Brochures Details" value="">
                                       </div>
                                    </div>

                                    <div class="col-md-8">
                                       <input type="file" class="form-control" name="images_two[]" id="images_two" multiple />
                                    </div>
                                    <div class="col-md-4">
                                       <button type="button" class="btn btn-info" onclick="upload_image('images_two','general_brochures',<?= $universityId ?>,'images_two_data')"> Submit </button>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <h2>Uploaded Document List</h2>
                                    <table class="table table-bordered">
                                       <thead>
                                          <th style="width: 20%;">ID</th>
                                          <th style="width: 40%;">URL</th>
                                          <th style="width: 40%;">Details</th>
                                          <th>View</th>
                                          <th>Delete</th>
                                       </thead>
                                       <tbody id="images_two_data">
                                          <?php

                                          if ($universityAdditionalData['general_brochures']) {

                                             foreach ($universityAdditionalData['general_brochures'] as $itwokey => $itwovalue) {

                                          ?>
                                                <tr>

                                                   <td><?php echo $itwovalue['image_id']; ?></td>
                                                   <td><?php echo $itwovalue['image_url']; ?></td>
                                                   <td><?php if (isset($itwovalue['image_details'])) {
                                                            echo $itwovalue['image_details'];
                                                         } else {
                                                            echo "N.A";
                                                         } ?></td>
                                                   <td><a href="<?php echo $itwovalue['image_url']; ?>" target="_blank" class="btn btn-sm"><i class="fas fa-eye"></i></a></td>
                                                   <td><button class="btn btn-sm" type="button" onclick="deleteUniversityImage(<?php echo $itwovalue['image_id']; ?>,'general_brochures','images_two_data',<?= $universityId ?>)"> <i class="fas fa-trash"></i></button></td>

                                                </tr>

                                          <?php }
                                          } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                           <button class="accordion">PG Brochures with Courses, Eligibility & Fees</button>
                           <div class="panel">
                              <div class="form-group">
                                 <div class="col-md-12">
                                    <h3>Upload Document (Max file size is 10mb/per file. Format recommended are PDF / DOCX / PNG / JPG)</h3>
                                    <br>
                                    <div class="col-md-8">
                                       <label for="name" class="col-md-4 control-label">Brochures Details</label>
                                       <div class="col-md-8">
                                          <input class="form-control" id="images_three_details" name="images_three_details" placeholder="Brochures Details" value="">
                                       </div>
                                    </div>
                                    <div class="col-md-8">
                                       <input type="file" class="form-control" name="images_three[]" id="images_three" multiple />
                                    </div>
                                    <div class="col-md-4">
                                       <button type="button" class="btn btn-info" onclick="upload_image('images_three','pg_brochures',<?= $universityId ?>,'images_three_data')"> Submit </button>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <h2>Uploaded Document List</h2>
                                    <table class="table table-bordered">
                                       <thead>
                                          <th style="width: 20%;">Name</th>
                                          <th style="width: 40%;">URL</th>
                                          <th style="width: 40%;">Details</th>
                                          <th>View</th>
                                          <th>Delete</th>
                                       </thead>
                                       <tbody id="images_three_data">
                                          <?php

                                          if ($universityAdditionalData['pg_brochures']) {

                                             foreach ($universityAdditionalData['pg_brochures'] as $itwokey => $itwovalue) {

                                          ?>
                                                <tr>

                                                   <td><?php echo $itwovalue['image_id']; ?></td>
                                                   <td><?php echo $itwovalue['image_url']; ?></td>
                                                   <td><?php if (isset($itwovalue['image_details'])) {
                                                            echo $itwovalue['image_details'];
                                                         } else {
                                                            echo "N.A";
                                                         } ?></td>
                                                   <td><a href="<?php echo $itwovalue['image_url']; ?>" target="_blank" class="btn btn-sm"><i class="fas fa-eye"></i></a></td>
                                                   <td><button class="btn btn-sm" type="button" onclick="deleteUniversityImage(<?php echo $itwovalue['image_id']; ?>,'pg_brochures','images_three_data',<?= $universityId ?>)"> <i class="fas fa-trash"></i></button></td>

                                                </tr>

                                          <?php }
                                          } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                           <button class="accordion"> UG Brochures with Courses, Eligibility & Fees </button>
                           <div class="panel">
                              <div class="form-group">
                                 <div class="col-md-12">
                                    <h3>Upload Document (Max file size is 10mb/per file. Format recommended are PDF / DOCX / PNG / JPG)</h3>
                                    <br>
                                    <div class="col-md-8">
                                       <label for="name" class="col-md-4 control-label">Brochures Details</label>
                                       <div class="col-md-8">
                                          <input class="form-control" id="images_four_details" name="images_four_details" placeholder="Brochures Details" value="">
                                       </div>
                                    </div>
                                    <div class="col-md-8">
                                       <input type="file" class="form-control" name="images_four[]" id="images_four" multiple />
                                    </div>
                                    <div class="col-md-4">
                                       <button type="button" class="btn btn-info" onclick="upload_image('images_four','ug_brochures',<?= $universityId ?>,'images_four_data')"> Submit </button>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <h2>Uploaded Document List</h2>
                                    <table class="table table-bordered">
                                       <thead>
                                          <th style="width: 20%;">ID</th>
                                          <th style="width: 40%;">URL</th>
                                          <th style="width: 40%;">Details</th>
                                          <th>View</th>
                                          <th>Delete</th>
                                       </thead>
                                       <tbody id="images_four_data">
                                          <?php

                                          if ($universityAdditionalData['ug_brochures']) {

                                             foreach ($universityAdditionalData['ug_brochures'] as $itwokey => $itwovalue) {

                                          ?>
                                                <tr>

                                                   <td><?php echo $itwovalue['image_id']; ?></td>
                                                   <td><?php echo $itwovalue['image_url']; ?></td>
                                                   <td><?php if (isset($itwovalue['image_details'])) {
                                                            echo $itwovalue['image_details'];
                                                         } else {
                                                            echo "N.A";
                                                         } ?></td>
                                                   <td><a href="<?php echo $itwovalue['image_url']; ?>" target="_blank" class="btn btn-sm"><i class="fas fa-eye"></i></a></td>
                                                   <td><button class="btn btn-sm" type="button" onclick="deleteUniversityImage(<?php echo $itwovalue['image_id']; ?>,'ug_brochures','images_four_data',<?= $universityId ?>)"> <i class="fas fa-trash"></i></button></td>

                                                </tr>

                                          <?php }
                                          } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                           <button class="accordion"> Course Catalog </button>
                           <div class="panel">
                              <div class="form-group">
                                 <div class="col-md-12">
                                    <h3>Upload Document (Max file size is 10mb/per file. Format recommended are PDF / DOCX / PNG / JPG)</h3>
                                    <br>
                                    <div class="col-md-8">
                                       <label for="name" class="col-md-4 control-label">Image Details</label>
                                       <div class="col-md-8">
                                          <input class="form-control" id="images_five_details" name="images_five_details" placeholder="Image Details" value="">
                                       </div>
                                    </div>
                                    <div class="col-md-8">
                                       <input type="file" class="form-control" name="images_five[]" id="images_five" multiple />
                                    </div>
                                    <div class="col-md-4">
                                       <button type="button" class="btn btn-info" onclick="upload_image('images_five','course_catlog',<?= $universityId ?>,'images_five_data')"> Submit </button>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <h2>Uploaded Document List</h2>
                                    <table class="table table-bordered">
                                       <thead>
                                          <th style="width: 20%;">ID</th>
                                          <th style="width: 40%;">URL</th>
                                          <th style="width: 40%;">Details</th>
                                          <th>View</th>
                                          <th>Delete</th>
                                       </thead>
                                       <tbody id="images_five_data">
                                          <?php

                                          if ($universityAdditionalData['course_catlog']) {

                                             foreach ($universityAdditionalData['course_catlog'] as $itwokey => $itwovalue) {

                                          ?>
                                                <tr>

                                                   <td><?php echo $itwovalue['image_id']; ?></td>
                                                   <td><?php echo $itwovalue['image_url']; ?></td>
                                                   <td><?php if (isset($itwovalue['image_details'])) {
                                                            echo $itwovalue['image_details'];
                                                         } else {
                                                            echo "N.A";
                                                         } ?></td>
                                                   <td><a href="<?php echo $itwovalue['image_url']; ?>" target="_blank" class="btn btn-sm"><i class="fas fa-eye"></i></a></td>
                                                   <td><button class="btn btn-sm" type="button" onclick="deleteUniversityImage(<?php echo $itwovalue['image_id']; ?>,'course_catlog','images_five_data',<?= $universityId ?>)"> <i class="fas fa-trash"></i></button></td>

                                                </tr>

                                          <?php }
                                          } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                           <button class="accordion"> Course Matrix (if prepared and can be shared) </button>
                           <div class="panel">
                              <div class="form-group">
                                 <div class="col-md-12">
                                    <h3>Upload Document (Max file size is 10mb/per file. Format recommended are PDF / DOCX / PNG / JPG)</h3>
                                    <br>
                                    <div class="col-md-8">
                                       <label for="name" class="col-md-4 control-label">Image Details</label>
                                       <div class="col-md-8">
                                          <input class="form-control" id="images_six_details" name="images_six_details" placeholder="Image Details" value="">
                                       </div>
                                    </div>
                                    <div class="col-md-8">
                                       <input type="file" class="form-control" name="images_six[]" id="images_six" multiple />
                                    </div>
                                    <div class="col-md-4">
                                       <button type="button" class="btn btn-info" onclick="upload_image('images_six','course_matrix',<?= $universityId ?>,'images_six_data')"> Submit </button>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <h2>Uploaded Document List</h2>
                                    <table class="table table-bordered">
                                       <thead>
                                          <th style="width: 20%;">ID</th>
                                          <th style="width: 40%;">URL</th>
                                          <th style="width: 40%;">Details</th>
                                          <th>View</th>
                                          <th>Delete</th>
                                       </thead>
                                       <tbody id="images_six_data">
                                          <?php

                                          if ($universityAdditionalData['course_matrix']) {

                                             foreach ($universityAdditionalData['course_matrix'] as $itwokey => $itwovalue) {
                                          ?>
                                                <tr>
                                                   <td><?php echo $itwovalue['image_id']; ?></td>
                                                   <td><?php echo $itwovalue['image_url']; ?></td>
                                                   <td><?php if (isset($itwovalue['image_details'])) {
                                                            echo $itwovalue['image_details'];
                                                         } else {
                                                            echo "N.A";
                                                         } ?></td>
                                                   <td><a href="<?php echo $itwovalue['image_url']; ?>" target="_blank" class="btn btn-sm"><i class="fas fa-eye"></i></a></td>
                                                   <td><button class="btn btn-sm" type="button" onclick="deleteUniversityImage(<?php echo $itwovalue['image_id']; ?>,'course_matrix','images_six_data',<?= $universityId ?>)"> <i class="fas fa-trash"></i></button></td>

                                                </tr>

                                          <?php }
                                          } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                           <button class="accordion"> General Videos for International Students (File Upload) </button>
                           <div class="panel">
                              <div class="form-group">
                                 <div class="col-md-12">
                                    <h2>Upload Document (Max file size is 50mb. For greater than 50mb, please send file on mail.)</h2>
                                    <br />
                                    <div class="col-md-8">
                                       <label for="name" class="col-md-4 control-label">Video Details</label>
                                       <div class="col-md-8">
                                          <input class="form-control" id="images_seven_details" name="images_seven_details" placeholder="Video Details" value="">
                                       </div>
                                    </div>
                                    <div class="col-md-8">
                                       <input type="file" class="form-control" name="images_seven[]" id="images_seven" multiple />
                                    </div>
                                    <div class="col-md-4">
                                       <button type="button" class="btn btn-info" onclick="upload_video('images_seven','general_videos',<?= $universityId ?>,'images_seven_data')"> Submit </button>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <h2>Uploaded Document List</h2>
                                    <table class="table table-bordered">
                                       <thead>
                                          <th style="width: 20%;">ID</th>
                                          <th style="width: 40%;">URL</th>
                                          <th style="width: 40%;">Details</th>
                                          <th>View</th>
                                          <th>Delete</th>
                                       </thead>
                                       <tbody id="images_seven_data">
                                          <?php

                                          if ($universityAdditionalData['general_videos']) {

                                             foreach ($universityAdditionalData['general_videos'] as $itwokey => $itwovalue) {

                                          ?>
                                                <tr>

                                                   <td><?php echo $itwovalue['image_id']; ?></td>
                                                   <td><?php echo $itwovalue['image_url']; ?></td>
                                                   <td><?php if (isset($itwovalue['videos_details'])) {
                                                            echo $itwovalue['videos_details'];
                                                         } else {
                                                            echo "N.A";
                                                         } ?></td>
                                                   <td><a href="<?php echo $itwovalue['image_url']; ?>" target="_blank" class="btn btn-sm"><i class="fas fa-eye"></i></a></td>
                                                   <td><button class="btn btn-sm" type="button" onclick="deleteUniversityImage(<?php echo $itwovalue['image_id']; ?>,'general_videos','images_seven_data',<?= $universityId ?>)"> <i class="fas fa-trash"></i></button></td>

                                                </tr>

                                          <?php }
                                          } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                           <button class="accordion"> Testimonial Videos of International Students (File Upload) </button>
                           <div class="panel">
                              <div class="form-group">
                                 <div class="col-md-12">
                                    <h2>Upload Document (Max file size is 50mb. For greater than 50mb, please send file on mail.)</h2>
                                    <br />
                                    <div class="col-md-8">
                                       <label for="name" class="col-md-4 control-label">Video Details</label>
                                       <div class="col-md-8">
                                          <input class="form-control" id="images_eight_details" name="images_eight_details" placeholder="Video Details" value="">
                                       </div>
                                    </div>
                                    <div class="col-md-8">
                                       <input type="file" class="form-control" name="images_eight[]" id="images_eight" multiple />
                                    </div>
                                    <div class="col-md-4">
                                       <button type="button" class="btn btn-info" onclick="upload_video('images_eight','testimonial_videos',<?= $universityId ?>,'images_eight_data')"> Submit </button>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <h2>Uploaded Document List</h2>
                                    <table class="table table-bordered">
                                       <thead>
                                          <th style="width: 20%;">ID</th>
                                          <th style="width: 40%;">URL</th>
                                          <th style="width: 40%;">Details</th>
                                          <th>View</th>
                                          <th>Delete</th>
                                       </thead>
                                       <tbody id="images_eight_data">
                                          <?php

                                          if ($universityAdditionalData['testimonial_videos']) {

                                             foreach ($universityAdditionalData['testimonial_videos'] as $itwokey => $itwovalue) {

                                          ?>
                                                <tr>

                                                   <td><?php echo $itwovalue['image_id']; ?></td>
                                                   <td><?php echo $itwovalue['image_url']; ?></td>
                                                   <td><?php if (isset($itwovalue['videos_details'])) {
                                                            echo $itwovalue['videos_details'];
                                                         } else {
                                                            echo "N.A";
                                                         } ?></td>
                                                   <td><a href="<?php echo $itwovalue['image_url']; ?>" target="_blank" class="btn btn-sm"><i class="fas fa-eye"></i></a></td>
                                                   <td><button class="btn btn-sm" type="button" onclick="deleteUniversityImage(<?php echo $itwovalue['image_id']; ?>,'testimonial_videos','images_eight_data',<?= $universityId ?>)"> <i class="fas fa-trash"></i></button></td>

                                                </tr>

                                          <?php }
                                          } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>

                           <button class="accordion"> Campus Photos (Preferable if 5-10+) </button>
                           <div class="panel">
                              <div class="form-group">
                                 <div class="col-md-12">
                                    <h3>Upload Document (Max file size is 10mb/per file. Format recommended are PDF / DOCX / PNG / JPG)</h3>
                                    <br>
                                    <div class="col-md-8">
                                       <label for="name" class="col-md-4 control-label">Image Details</label>
                                       <div class="col-md-8">
                                          <input class="form-control" id="images_nine_details" name="images_nine_details" placeholder="Image Details" value="">
                                       </div>
                                    </div>

                                    <div class="col-md-8">
                                       <label for="name" class="col-md-4 control-label">Alt Tag</label>
                                       <div class="col-md-8">
                                          <input type="text" class="form-control" name="alt_tag" id="alt_tag_nine" placeholder="Alt Tag">
                                       </div>
                                    </div>

                                    <div class="col-md-8">
                                       <input type="file" class="form-control" name="images_nine[]" id="images_nine" multiple />
                                    </div>
                                    <div class="col-md-4">
                                       <button type="button" class="btn btn-info" onclick="upload_image('images_nine','campus_photos',<?= $universityId ?>,'images_nine_data')"> Submit </button>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <h2>Uploaded Document List</h2>
                                    <table class="table table-bordered">
                                       <thead>
                                          <th style="width: 20%;">ID</th>
                                          <th style="width: 20%;">Alt Tag</th>
                                          <th style="width: 40%;">URL</th>
                                          <th style="width: 40%;">Details</th>
                                          <th>View</th>
                                          <th>Edit</th>
                                          <th>Delete</th>
                                       </thead>
                                       <tbody id="images_nine_data">
                                          <?php
                                          if ($universityAdditionalData['campus_photos']) {
                                             foreach ($universityAdditionalData['campus_photos'] as $itwokey => $itwovalue) {
                                          ?>
                                                <tr>
                                                   <td><?php echo $itwovalue['image_id']; ?></td>
                                                   <!-- <td><?php echo $itwovalue['alt_tag']; ?></td> -->
                                                   <td><?php echo isset($itwovalue['alt_tag']) ? $itwovalue['alt_tag'] : 'N/A'; ?></td>

                                                   <td><?php echo $itwovalue['image_url']; ?></td>
                                                   <td><?php if (isset($itwovalue['image_details'])) {
                                                            echo $itwovalue['image_details'];
                                                         } else {
                                                            echo "N.A";
                                                         } ?></td>
                                                   <td><a href="<?php echo $itwovalue['image_url']; ?>" target="_blank" class="btn btn-sm"><i class="fas fa-eye"></i></a></td>

                                                   <td>
                                                      <button class="btn btn-sm" type="button" onclick="showUniversityImageModal(<?php echo $itwovalue['image_id']; ?>, 'campus_photos', <?= $universityId ?>)">
                                                         <i class="fas fa-edit"></i>
                                                      </button>
                                                   </td>

                                                   <td><button class="btn btn-sm" type="button" onclick="deleteUniversityImage(<?php echo $itwovalue['image_id']; ?>,'campus_photos','images_nine_data',<?= $universityId ?>)"> <i class="fas fa-trash"></i></button></td>
                                                </tr>
                                          <?php }
                                          } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>

                           <button class="accordion">Student Life Photos (Preferable if 5-10+) </button>
                           <div class="panel">
                              <div class="form-group">
                                 <div class="col-md-12">
                                    <h3>Upload Document (Max file size is 10mb/per file. Format recommended are PDF / DOCX / PNG / JPG)</h3>
                                    <br>
                                    <div class="col-md-8">
                                       <label for="name" class="col-md-4 control-label">Image Details</label>
                                       <div class="col-md-8">
                                          <input class="form-control" id="images_ten_details" name="images_ten_details" placeholder="Image Details" value="">
                                       </div>
                                    </div>
                                    <div class="col-md-8">
                                       <label for="name" class="col-md-4 control-label">Alt Tag</label>
                                       <div class="col-md-8">
                                          <input type="text" class="form-control" name="alt_tag" id="alt_tag_ten" placeholder="Alt Tag">
                                       </div>
                                    </div>
                                    <div class="col-md-8">
                                       <input type="file" class="form-control" name="images_ten[]" id="images_ten" multiple />
                                    </div>
                                    <div class="col-md-4">
                                       <button type="button" class="btn btn-info" onclick="upload_image('images_ten','student_life_photos',<?= $universityId ?>,'images_ten_data')"> Submit </button>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <h2>Uploaded Document List</h2>
                                    <table class="table table-bordered">
                                       <thead>
                                          <th style="width: 20%;">ID</th>
                                          <th style="width: 20%;">Alt Tag</th>
                                          <th style="width: 40%;">URL</th>
                                          <th style="width: 40%;">Details</th>
                                          <th>View</th>
                                          <th>Edit</th>
                                          <th>Delete</th>
                                       </thead>
                                       <tbody id="images_ten_data">
                                          <?php
                                          if ($universityAdditionalData['student_life_photos']) {
                                             foreach ($universityAdditionalData['student_life_photos'] as $itwokey => $itwovalue) {
                                          ?>
                                                <tr>
                                                   <td><?php echo $itwovalue['image_id']; ?></td>
                                                   <!-- <td><?php echo $itwovalue['alt_tag']; ?></td> -->
                                                   <td><?php echo isset($itwovalue['alt_tag']) ? $itwovalue['alt_tag'] : 'N/A'; ?></td>
                                                   <td><?php echo $itwovalue['image_url']; ?></td>
                                                   <td><?php if (isset($itwovalue['image_details'])) {
                                                            echo $itwovalue['image_details'];
                                                         } else {
                                                            echo "N.A";
                                                         } ?></td>
                                                   <td><a href="<?php echo $itwovalue['image_url']; ?>" target="_blank" class="btn btn-sm"><i class="fas fa-eye"></i></a></td>

                                                   <td>
                                                      <button class="btn btn-sm" type="button" onclick="showUniversityImageModal(<?php echo $itwovalue['image_id']; ?>, 'student_life_photos', <?= $universityId ?>)">
                                                         <i class="fas fa-edit"></i>
                                                      </button>
                                                   </td>

                                                   <td><button class="btn btn-sm" type="button" onclick="deleteUniversityImage(<?php echo $itwovalue['image_id']; ?>,'student_life_photos','images_ten_data',<?= $universityId ?>)"> <i class="fas fa-trash"></i></button></td>
                                                </tr>
                                          <?php }
                                          } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>

                           <button class="accordion">Facilities / Labs / Research Center Photos (Preferable if 5-10+) </button>
                           <div class="panel">
                              <div class="form-group">
                                 <div class="col-md-12">
                                    <h3>Upload Document (Max file size is 10mb/per file. Format recommended are PDF / DOCX / PNG / JPG)</h3>
                                    <br>
                                    <div class="col-md-8">
                                       <label for="name" class="col-md-4 control-label">Image Details</label>
                                       <div class="col-md-8">
                                          <input class="form-control" id="images_one_details" name="images_one_details" placeholder="Image Details" value="">
                                       </div>
                                    </div>

                                    <div class="col-md-8">
                                       <label for="name" class="col-md-4 control-label">Alt Tag</label>
                                       <div class="col-md-8">
                                          <input type="text" class="form-control" name="alt_tag" id="alt_tag_one" placeholder="Alt Tag">
                                       </div>
                                    </div>

                                    <div class="col-md-8">
                                       <input type="file" class="form-control" name="images_one[]" id="images_one" multiple />
                                    </div>
                                    <div class="col-md-4">
                                       <button type="button" class="btn btn-info" onclick="upload_image('images_one','facilities_labs_research_center',<?= $universityId ?>,'images_one_data')"> Submit </button>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <h2>Uploaded Document List</h2>
                                    <table class="table table-bordered">
                                       <thead>
                                          <th style="width: 20%;">ID</th>
                                          <th style="width: 20%;">Alt Tag</th>
                                          <th style="width: 40%;">URL</th>
                                          <th style="width: 40%;">Details</th>
                                          <th>View</th>
                                          <th>Edit</th>
                                          <th>Delete</th>
                                       </thead>
                                       <tbody id="images_one_data">
                                          <?php
                                          if ($universityAdditionalData['facilities_labs_research_center']) {
                                             foreach ($universityAdditionalData['facilities_labs_research_center'] as $itwokey => $itwovalue) {
                                          ?>
                                                <tr>
                                                   <td><?php echo $itwovalue['image_id']; ?></td>
                                                   <!-- <td><?php echo $itwovalue['alt_tag']; ?></td> -->
                                                   <td><?php echo isset($itwovalue['alt_tag']) ? $itwovalue['alt_tag'] : 'N/A'; ?></td>
                                                   <td><?php echo $itwovalue['image_url']; ?></td>
                                                   <td><?php if (isset($itwovalue['image_details'])) {
                                                            echo $itwovalue['image_details'];
                                                         } else {
                                                            echo "N.A";
                                                         } ?></td>
                                                   <td><a href="<?php echo $itwovalue['image_url']; ?>" target="_blank" class="btn btn-sm"><i class="fas fa-eye"></i></a></td>

                                                   <td>
                                                      <button class="btn btn-sm" type="button" onclick="showUniversityImageModal(<?php echo $itwovalue['image_id']; ?>, 'facilities_labs_research_center', <?= $universityId ?>)">
                                                         <i class="fas fa-edit"></i>
                                                      </button>
                                                   </td>

                                                   <td><button class="btn btn-sm" type="button" onclick="deleteUniversityImage(<?php echo $itwovalue['image_id']; ?>,'facilities_labs_research_center','images_one_data',<?= $universityId ?>)"> <i class="fas fa-trash"></i></button></td>
                                                </tr>
                                          <?php }
                                          } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>

                           <button class="accordion">Convocation Photos (Preferable if 5-10+) </button>
                           <div class="panel">
                              <div class="form-group">
                                 <div class="col-md-12">
                                    <h3>Upload Document (Max file size is 10mb/per file. Format recommended are PDF / DOCX / PNG / JPG)</h3>
                                    <br>
                                    <div class="col-md-8">
                                       <label for="name" class="col-md-4 control-label">Image Details</label>
                                       <div class="col-md-8">
                                          <input class="form-control" id="images_fourteen_details" name="images_fourteen_details" placeholder="Image Details" value="">
                                       </div>
                                    </div>

                                    <div class="col-md-8">
                                       <label for="name" class="col-md-4 control-label">Alt Tag</label>
                                       <div class="col-md-8">
                                          <input type="text" class="form-control" name="alt_tag" id="alt_tag_fourteen" placeholder="Alt Tag">
                                       </div>
                                    </div>

                                    <div class="col-md-8">
                                       <input type="file" class="form-control" name="images_fourteen[]" id="images_fourteen" multiple />
                                    </div>
                                    <div class="col-md-4">
                                       <button type="button" class="btn btn-info" onclick="upload_image('images_fourteen','convocation_photos',<?= $universityId ?>,'images_fourteen_data')"> Submit </button>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <h2>Uploaded Document List</h2>
                                    <table class="table table-bordered">
                                       <thead>
                                          <th style="width: 20%;">ID</th>
                                          <th style="width: 20%;">Alt Tag</th>
                                          <th style="width: 40%;">URL</th>
                                          <th style="width: 40%;">Details</th>
                                          <th>View</th>
                                          <th>Edit</th>
                                          <th>Delete</th>
                                       </thead>
                                       <tbody id="images_fourteen_data">
                                          <?php
                                          if ($universityAdditionalData['convocation_photos']) {
                                             foreach ($universityAdditionalData['convocation_photos'] as $itwokey => $itwovalue) {
                                          ?>
                                                <tr>
                                                   <td><?php echo $itwovalue['image_id']; ?></td>
                                                   <td><?php echo isset($itwovalue['alt_tag']) ? $itwovalue['alt_tag'] : 'N/A'; ?></td>
                                                   <td><?php echo $itwovalue['image_url']; ?></td>
                                                   <td><?php if (isset($itwovalue['image_details'])) {
                                                            echo $itwovalue['image_details'];
                                                         } else {
                                                            echo "N.A";
                                                         } ?></td>
                                                   <td><a href="<?php echo $itwovalue['image_url']; ?>" target="_blank" class="btn btn-sm"><i class="fas fa-eye"></i></a></td>

                                                   <td>
                                                      <button class="btn btn-sm" type="button" onclick="showUniversityImageModal(<?php echo $itwovalue['image_id']; ?>, 'convocation_photos', <?= $universityId ?>)">
                                                         <i class="fas fa-edit"></i>
                                                      </button>
                                                   </td>

                                                   <td><button class="btn btn-sm" type="button" onclick="deleteUniversityImage(<?php echo $itwovalue['image_id']; ?>,'convocation_photos','images_fourteen_data',<?= $universityId ?>)"><i class="fas fa-trash"></i></button></td>
                                                </tr>
                                          <?php }
                                          } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>

                           <button class="accordion">Event Photos (Preferable if 5-10+) </button>
                           <div class="panel">
                              <div class="form-group">
                                 <div class="col-md-12">
                                    <h3>Upload Document (Max file size is 10mb/per file. Format recommended are PDF / DOCX / PNG / JPG)</h3>
                                    <br>
                                    <div class="col-md-8">
                                       <label for="name" class="col-md-4 control-label">Image Details</label>
                                       <div class="col-md-8">
                                          <input class="form-control" id="images_fifteen_details" name="images_fifteen_details" placeholder="Image Details" value="">
                                       </div>
                                    </div>

                                    <div class="col-md-8">
                                       <label for="name" class="col-md-4 control-label">Alt Tag</label>
                                       <div class="col-md-8">
                                          <input type="text" class="form-control" name="alt_tag" id="alt_tag_fifteen" placeholder="Alt Tag">
                                       </div>
                                    </div>

                                    <div class="col-md-8">
                                       <input type="file" class="form-control" name="images_fifteen[]" id="images_fifteen" multiple />
                                    </div>
                                    <div class="col-md-4">
                                       <button type="button" class="btn btn-info" onclick="upload_image('images_fifteen','event_photos',<?= $universityId ?>,'images_fifteen_data')"> Submit </button>
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <h2>Uploaded Document List</h2>
                                    <table class="table table-bordered">
                                       <thead>
                                          <th style="width: 20%;">ID</th>
                                          <th style="width: 20%;">Alt Tag</th>
                                          <th style="width: 40%;">URL</th>
                                          <th style="width: 40%;">Details</th>
                                          <th>View</th>
                                          <th>Edit</th>
                                          <th>Delete</th>
                                       </thead>
                                       <tbody id="images_fifteen_data">
                                          <?php
                                          if ($universityAdditionalData['event_photos']) {
                                             foreach ($universityAdditionalData['event_photos'] as $itwokey => $itwovalue) {
                                          ?>
                                                <tr>
                                                   <td><?php echo $itwovalue['image_id']; ?></td>
                                                   <td><?php echo isset($itwovalue['alt_tag']) ? $itwovalue['alt_tag'] : 'N/A'; ?></td>
                                                   <td><?php echo $itwovalue['image_url']; ?></td>
                                                   <td><?php if (isset($itwovalue['image_details'])) {
                                                            echo $itwovalue['image_details'];
                                                         } else {
                                                            echo "N.A";
                                                         } ?></td>
                                                   <td><a href="<?php echo $itwovalue['image_url']; ?>" target="_blank" class="btn btn-sm"><i class="fas fa-eye"></i></a></td>

                                                   <td>
                                                      <button class="btn btn-sm" type="button" onclick="showUniversityImageModal(<?php echo $itwovalue['image_id']; ?>, 'event_photos', <?= $universityId ?>)">
                                                         <i class="fas fa-edit"></i>
                                                      </button>
                                                   </td>

                                                   <td><button class="btn btn-sm" type="button" onclick="deleteUniversityImage(<?php echo $itwovalue['image_id']; ?>,'event_photos','images_fifteen_data',<?= $universityId ?>)"><i class="fas fa-trash"></i></button></td>
                                                </tr>
                                          <?php }
                                          } ?>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>

                           <!-- <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">  </label>
                              <div class="col-sm-6">
                                 <input type="file" class="form-control" name="images_three[]" id="images_three" multiple />
                              </div>
                              <div class="col-sm-2">
                                 <button type="button" class="btn btn-info" onclick="upload_image('images_three','pg_brochures',<?= $universityId ?>)" > Submit </button>
                              </div>
                              </div>
                              <div class="form-group">
                                 <label for="inputEmail3" class="col-sm-4 control-label"> UG Brochures with Courses, Eligibility & Fees </label>
                                 <div class="col-sm-6">
                                    <input type="file" class="form-control" name="images_four[]" id="images_four" multiple />
                                 </div>
                                 <div class="col-sm-2">
                                    <button type="button" class="btn btn-info" onclick="upload_image('images_four','ug_brochures',<?= $universityId ?>)" > Submit </button>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="inputEmail3" class="col-sm-4 control-label"> Course Catalog </label>
                                 <div class="col-sm-6">
                                    <input type="file" class="form-control" name="images_five[]" id="images_five" multiple />
                                 </div>
                                 <div class="col-sm-2">
                                    <button type="button" class="btn btn-info" onclick="upload_image('images_five','course_catlog',<?= $universityId ?>)" > Submit </button>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="inputEmail3" class="col-sm-4 control-label"> Course Matrix (if prepared and can be shared) </label>
                                 <div class="col-sm-6">
                                    <input type="file" class="form-control" name="images_six[]" id="images_six" multiple />
                                 </div>
                                 <div class="col-sm-2">
                                    <button type="button" class="btn btn-info" onclick="upload_image('images_six','course_matrix',<?= $universityId ?>)" > Submit </button>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="inputEmail3" class="col-sm-4 control-label"> General Videos for International Students (File Upload / Youtube Link) </label>
                                 <div class="col-sm-6">
                                    <input type="file" class="form-control" name="images_seven[]" id="images_seven" multiple />
                                 </div>
                                 <div class="col-sm-2">
                                    <button type="button" class="btn btn-info" onclick="upload_image('images_seven','general_videos',<?= $universityId ?>)" > Submit </button>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="inputEmail3" class="col-sm-4 control-label"> Testimonial Videos of International Students (File Upload  / Youtube Link) </label>
                                 <div class="col-sm-6">
                                    <input type="file" class="form-control" name="images_eight[]" id="images_eight" multiple />
                                 </div>
                                 <div class="col-sm-2">
                                    <button type="button" class="btn btn-info" onclick="upload_image('images_eight','testimonial_videos',<?= $universityId ?>)" > Submit </button>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="inputEmail3" class="col-sm-4 control-label"> Campus Photos (Preferable if 5-10+) </label>
                                 <div class="col-sm-6">
                                    <input type="file" class="form-control" name="images_nine[]"  id="images_nine" multiple />
                                 </div>
                                 <div class="col-sm-2">
                                    <button type="button" class="btn btn-info" onclick="upload_image('images_nine','campus_photos',<?= $universityId ?>)" > Submit </button>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="inputEmail3" class="col-sm-4 control-label">  Student Life Photos  (Preferable if 5-10+) </label>
                                 <div class="col-sm-6">
                                    <input type="file" class="form-control" name="images_ten[]" id="images_ten" multiple />
                                 </div>
                                 <div class="col-sm-2">
                                    <button type="button" class="btn btn-info" onclick="upload_image('images_ten','student_life_photos',<?= $universityId ?>)" > Submit </button>
                                 </div>
                              </div>
                              <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">  Facilities / Labs / Research Center Photos (Preferable if 5-10+) </label>
                              <div class="col-sm-6">
                                 <input type="file" class="form-control" name="images_one[]" id="images_one" multiple />
                              </div>
                              <div class="col-sm-2">
                                 <button type="button" class="btn btn-info" onclick="upload_image('images_one','facilities_labs_research_center',<?= $universityId ?>)" > Submit </button>
                              </div>
                           </div> -->
                     </form>
                  </div>
               </div>

               <div id="Data" class="tabcontent" style="background-color: white;color: black;">
                  <form class="form-horizontal" name="data">
                     <h2>Text Data</h2>
                     <div class="col-md-12">
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-2 control-label"> Application Fee waiver for Students coming from Imperail? </label>
                           <div class="col-sm-10">
                              <textarea name="application_fee_waiver_imperial" id="application_fee_waiver_imperial" class="form-control" style="font-size:12px;" rows="4" cols="50"><?= $universityAdditionalData['application_fee_waiver_imperial'] ?></textarea>
                              <input type="hidden" value="<?= $universityId ?>" name="university_id" id="university_id" />
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-2 control-label"> University Long Overview (200 - 300 words if possible) </label>
                           <div class="col-sm-10">
                              <textarea name="university_long_overview" id="university_long_overview" class="form-control" style="font-size:12px;" rows="4" cols="50"><?= $universityAdditionalData['university_long_overview'] ?></textarea>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-2 control-label"> University Very Short Overview (1-2 Lines if possible) </label>
                           <div class="col-sm-10">
                              <textarea name="university_short_overview" id="university_short_overview" class="form-control" style="font-size:12px;" rows="4" cols="50"><?= $universityAdditionalData['university_short_overview'] ?></textarea>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-2 control-label">
                              List of Rankings you would like displayed on the Page?</label>
                           <div class="col-sm-10">
                              <textarea name="list_of_ranking" id="list_of_ranking" class="form-control" style="font-size:12px;" rows="4" cols="50"><?= $universityAdditionalData['list_of_ranking'] ?></textarea>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-2 control-label"> Total Number of International Students during any Typical Year </label>
                           <div class="col-sm-10">
                              <textarea name="total_international_student_year" id="total_international_student_year" class="form-control" style="font-size:12px;" rows="4" cols="50"><?= $universityAdditionalData['total_international_student_year'] ?></textarea>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-2 control-label"> Total Number of Indian Students during any Typical Year </label>
                           <div class="col-sm-10">
                              <textarea name="total_indian_student_year" id="total_indian_student_year" class="form-control" style="font-size:12px;" rows="4" cols="50"><?= $universityAdditionalData['total_indian_student_year'] ?></textarea>
                           </div>
                        </div>
                        <div class="form-group">
                           <?php

                           $pgEntryCount = 0;

                           if ($universityAdditionalData['college_ug_list']) {

                              $pgCount = 1;
                              $pgEntryCount = count($universityAdditionalData['college_pg_list']);

                              foreach ($universityAdditionalData['college_pg_list'] as $pgkey => $pgvalue) {


                           ?>
                                 <?php if ($pgCount == 1) { ?>
                                    <div class="col-md-12" id="<?php echo "college_pg_$pgCount"; ?>">

                                       <label for="inputEmail3" class="col-sm-2 control-label"> College Wise - Average Eligibility & Cost (PG) (Total Tuition for entire Course)? </label>

                                    <?php } else { ?>
                                       <div class="col-md-12" style="margin-bottom: 8px;" id="<?php echo "college_pg_$pgCount"; ?>">

                                          <label for="inputEmail3" class="col-sm-2 control-label"> </label>
                                       <?php } ?>
                                       <div class="col-sm-3">
                                          <input type="text" id="college_name_pg" name="college_name_pg[]" value="<?php echo $pgvalue['college_name']; ?>" placeholder="College Name" class="form-control" />
                                       </div>
                                       <div class="col-sm-1">
                                          <input type="text" id="gpa_pg" name="gpa_score_pg[]" value="<?php echo $pgvalue['gpa']; ?>" placeholder="GPA" class="form-control" />
                                       </div>
                                       <div class="col-sm-1">
                                          <input type="text" id="gre_pg" name="gre_score_pg[]" value="<?php echo $pgvalue['gre']; ?>" placeholder="GRE" class="form-control" />
                                       </div>
                                       <div class="col-sm-1">
                                          <input type="text" id="tofel_pg" name="tofel_score_pg[]" value="<?php echo $pgvalue['tofel']; ?>" placeholder="TOFEL" class="form-control" />
                                       </div>
                                       <div class="col-sm-2">
                                          <input type="text" id="cost_pg" name="cost_pg[]" value="<?php echo $pgvalue['cost']; ?>" placeholder="COST" class="form-control" />
                                       </div>
                                       <?php if ($pgCount == 1) { ?>
                                          <div class="col-sm-1">
                                             <input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addCollegePG()">
                                          </div>

                                       </div>

                                    <?php } else { ?>
                                       <div class="col-sm-1">
                                          <input type="button" name="delete" value="Delete" onclick="deletePg(<?php echo $pgCount; ?>)">
                                       </div>

                                    </div>

                                 <?php } ?>

                              <?php
                                 $pgCount = $pgCount + 1;
                              }
                           } else {
                              ?>

                              <div class="col-md-12" id="<?php echo "college_pg_1"; ?>">

                                 <label for="inputEmail3" class="col-sm-2 control-label"> College Wise - Average Eligibility & Cost (UG) (Total Tuition for entire Course)? </label>
                                 <div class="col-sm-3">
                                    <input type="text" id="college_name_pg" name="college_name_pg[]" placeholder="College Name" class="form-control" />
                                 </div>
                                 <div class="col-sm-1">
                                    <input type="text" id="gpa_pg" name="gpa_score_pg[]" placeholder="GPA" class="form-control" />
                                 </div>
                                 <div class="col-sm-1">
                                    <input type="text" id="gre_pg" name="gre_score_pg[]" placeholder="GRE" class="form-control" />
                                 </div>
                                 <div class="col-sm-1">
                                    <input type="text" id="tofel_pg" name="tofel_score_pg[]" placeholder="TOFEL" class="form-control" />
                                 </div>
                                 <div class="col-sm-2">
                                    <input type="text" id="cost_pg" name="cost_pg[]" placeholder="COST" class="form-control" />
                                 </div>
                                 <div class="col-sm-1">
                                    <input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addCollegePG()">
                                 </div>

                              </div>

                           <?php } ?>
                           <div id="pgdiv"></div>
                        </div>
                        <div class="form-group">
                           <?php

                           $ugEntryCount = 0;

                           if ($universityAdditionalData['college_ug_list']) {

                              $ugCount = 1;
                              $ugEntryCount = count($universityAdditionalData['college_ug_list']);

                              foreach ($universityAdditionalData['college_ug_list'] as $ugkey => $ugvalue) {


                           ?>
                                 <?php if ($ugCount == 1) { ?>
                                    <div class="col-md-12" id="<?php echo "college_ug_$ugCount"; ?>">

                                       <label for="inputEmail3" class="col-sm-2 control-label"> College Wise - Average Eligibility & Cost (UG) - (GPA / TOEFL / GRE) </label>

                                    <?php } else { ?>
                                       <div class="col-md-12" style="margin-bottom: 8px;" id="<?php echo "college_ug_$ugCount"; ?>">

                                          <label for="inputEmail3" class="col-sm-2 control-label"> </label>
                                       <?php } ?>
                                       <div class="col-sm-3">
                                          <input type="text" id="college_name_pg" name="college_name_ug[]" value="<?php echo $ugvalue['college_name']; ?>" placeholder="College Name" class="form-control" />
                                       </div>
                                       <div class="col-sm-1">
                                          <input type="text" id="gpa_pg" name="gpa_score_ug[]" value="<?php echo $ugvalue['gpa']; ?>" placeholder="GPA" class="form-control" />
                                       </div>
                                       <div class="col-sm-1">
                                          <input type="text" id="gre_pg" name="gre_score_ug[]" value="<?php echo $ugvalue['gre']; ?>" placeholder="GRE" class="form-control" />
                                       </div>
                                       <div class="col-sm-1">
                                          <input type="text" id="tofel_pg" name="tofel_score_ug[]" value="<?php echo $ugvalue['tofel']; ?>" placeholder="TOFEL" class="form-control" />
                                       </div>
                                       <div class="col-sm-2">
                                          <input type="text" id="cost_pg" name="cost_ug[]" value="<?php echo $ugvalue['cost']; ?>" placeholder="COST" class="form-control" />
                                       </div>
                                       <?php if ($ugCount == 1) { ?>
                                          <div class="col-sm-1">
                                             <input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addCollegeUG()">
                                          </div>

                                       </div>

                                    <?php } else { ?>
                                       <div class="col-sm-1">
                                          <input type="button" name="delete" value="Delete" onclick="deleteUg(<?php echo $ugCount; ?>)">
                                       </div>

                                    </div>

                                 <?php } ?>

                              <?php
                                 $ugCount = $ugCount + 1;
                              }
                           } else {
                              ?>

                              <div class="col-md-12" id="college_ug_1">

                                 <label for="inputEmail3" class="col-sm-2 control-label"> College Wise - Average Eligibility & Cost (UG) - (GPA / TOEFL / GRE) </label>

                                 <div class="col-sm-3">
                                    <input type="text" id="college_name_pg" name="college_name_ug[]" placeholder="College Name" class="form-control" />
                                 </div>
                                 <div class="col-sm-1">
                                    <input type="text" id="gpa_pg" name="gpa_score_ug[]" placeholder="GPA" class="form-control" />
                                 </div>
                                 <div class="col-sm-1">
                                    <input type="text" id="gre_pg" name="gre_score_ug[]" placeholder="GRE" class="form-control" />
                                 </div>
                                 <div class="col-sm-1">
                                    <input type="text" id="tofel_pg" name="tofel_score_ug[]" placeholder="TOFEL" class="form-control" />
                                 </div>
                                 <div class="col-sm-2">
                                    <input type="text" id="cost_pg" name="cost_ug[]" placeholder="COST" class="form-control" />
                                 </div>
                                 <div class="col-sm-1">
                                    <input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addCollegeUG()">
                                 </div>

                              </div>

                           <?php } ?>
                           <div id="ugdiv"></div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-2 control-label"> Scholarships for International Students (Link to relevant University page) </label>
                           <div class="col-sm-10">
                              <textarea name="scholarship_international" id="scholarship_international" class="form-control" style="font-size:12px;" rows="4" cols="50"><?= $universityAdditionalData['scholarship_international'] ?></textarea>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-2 control-label"> Jobs and Placement Overview (3-5 lines) </label>
                           <div class="col-sm-10">
                              <textarea name="job_placement_overview" id="job_placement_overview" class="form-control" style="font-size:12px;" rows="4" cols="50"><?= $universityAdditionalData['job_placement_overview'] ?></textarea>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-2 control-label"> Top 5 Reasons to Study at the University? </label>
                           <div class="col-sm-10">
                              <textarea name="top_reason_to_study" id="top_reason_to_study" class="form-control" style="font-size:12px;" rows="4" cols="50"><?= $universityAdditionalData['top_reason_to_study'] ?></textarea>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-2 control-label"> Top 5 Courses for the Indian Market (PG) </label>
                           <div class="col-sm-10">
                              <textarea name="top_course_pg" id="top_course_pg" class="form-control" style="font-size:12px;" rows="4" cols="50"><?= $universityAdditionalData['top_course_pg'] ?></textarea>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-2 control-label"> Top 5 Courses for the Indian Market (UG) </label>
                           <div class="col-sm-10">
                              <textarea name="top_course_ug" id="top_course_ug" class="form-control" style="font-size:12px;" rows="4" cols="50"><?= $universityAdditionalData['top_course_ug'] ?></textarea>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-2 control-label"> Research Spending & Endowment </label>
                           <div class="col-sm-10">
                              <textarea name="research_spending_endowment" id="research_spending_endowment" class="form-control" style="font-size:12px;" rows="4" cols="50"><?= $universityAdditionalData['research_spending_endowment'] ?></textarea>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-2 control-label"> Active Research Areas (short overview 5-7 lines) </label>
                           <div class="col-sm-10">
                              <textarea name="active_research_areas" id="active_research_areas" class="form-control" style="font-size:12px;" rows="4" cols="50"><?= $universityAdditionalData['active_research_areas'] ?></textarea>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-2 control-label"> Short Overview - Teaching quality / Pedagogy, Faculty and Academic Strengths </label>
                           <div class="col-sm-10">
                              <textarea name="short_overview_study" id="short_overview_study" class="form-control" style="font-size:12px;" rows="4" cols="50"><?= $universityAdditionalData['short_overview_study'] ?></textarea>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-2 control-label"> SMM Links </label>
                           <div class="col-sm-2">
                              <input type="text" name="smm_facebook" id="smm_facebook" class="form-control" placeholder="Facebook Profile Url" value="<?= $universityAdditionalData['smm_facebook'] ?>">
                           </div>
                           <div class="col-sm-2">
                              <input type="text" name="smm_twitter" id="smm_twitter" class="form-control" placeholder="Twitter Profile Url" value="<?= $universityAdditionalData['smm_twitter'] ?>">
                           </div>
                           <div class="col-sm-2">
                              <input type="text" name="smm_instagram" id="smm_instagram" class="form-control" placeholder="Instagram Profile Url" value="<?= $universityAdditionalData['smm_instagram'] ?>">
                           </div>
                           <div class="col-sm-2">
                              <input type="text" name="smm_youtube" id="smm_youtube" class="form-control" placeholder="Youtube Profile Url" value="<?= $universityAdditionalData['smm_youtube'] ?>">
                           </div>
                           <div class="col-sm-2">
                              <input type="text" name="smm_linkedin" id="smm_linkedin" class="form-control" placeholder="Linkedin Profile Url" value="<?= $universityAdditionalData['smm_linkedin'] ?>">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-2 control-label"> Location Note, short overview of the city / town / locality in which the main campus is situated? </label>
                           <div class="col-sm-10">
                              <textarea name="short_overview_location" id="short_overview_location" class="form-control" style="font-size:12px;" rows="4" cols="50"><?= $universityAdditionalData['short_overview_location'] ?></textarea>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-2 control-label"> New / Upcoming Courses, Facilities, Department, Research Centers to Highlight? (150 - 200 words) </label>
                           <div class="col-sm-10">
                              <textarea name="new_upcoming_courses" id="new_upcoming_courses" class="form-control" style="font-size:12px;" rows="4" cols="50"><?= $universityAdditionalData['new_upcoming_courses'] ?></textarea>
                           </div>
                        </div>

                        <div class="box-footer">
                           <button type="button" class="btn btn-default pull-center" onclick="window.history.back();">Back</button>
                           <button type="button" class="btn btn-warning pull-center" id="reset" onclick="window.location.reload();">Reset</button>
                           <button type="button" class="btn btn-info pull-right" onclick="submitUniversityData()"> Submit Data </button>
                        </div><!-- /.box-footer -->
                     </div>

               </div>

               </form>

            </div>
         </div>

         <!-- Modal -->
         <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
               <div class="modal-content">
                  <div class="modal-header">
                     <h4 class="modal-title" id="exampleModalLongTitle">Edit Documents Details</h4>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                     </button>
                  </div>
                  <div class="modal-body">
                     <!-- Hidden fields -->
                     <input type="hidden" id="image_id" name="image_id">
                     <input type="hidden" id="column_name" name="column_name">
                     <!-- Optionally, if you need university_id -->
                     <!-- <input type="hidden" id="university_id" name="university_id" value="..."> -->

                     <div class="form-group">
                        <label for="images_details">Image Details</label>
                        <input type="text" class="form-control" id="images_details" name="images_details">
                     </div>
                     <div class="form-group">
                        <label for="alt_tag">Alt Tag</label>
                        <input type="text" class="form-control" id="alt_tag" name="alt_tag">
                     </div>
                     <div class="form-group">
                        <label for="images_url">Upload New Image (Optional)</label>
                        <input type="file" class="form-control" name="images" id="images_url">
                     </div>
                     <div class="form-group">
                        <label>Current Image Preview</label>
                        <img id="current_image_preview" src="" alt="Current Image" class="img-fluid" style="max-height: 150px;">
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                     <button type="button" class="btn btn-primary" id="saveChanges">Save Changes</button>
                  </div>
               </div>
            </div>
         </div>


      </div>
      <!-- /.box -->
      <!-- general form elements disabled -->
</div>
<!--/.col (right) -->
</div>
<!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
   function openPage(pageName, elmnt, color) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
         tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablink");
      for (i = 0; i < tablinks.length; i++) {
         tablinks[i].style.backgroundColor = "";
      }
      document.getElementById(pageName).style.display = "block";
      elmnt.style.backgroundColor = color;
   }

   // Get the element with id="defaultOpen" and click on it
   document.getElementById("defaultOpen").click();

   function upload_image(file_name, file_type, university_id, replaceDiv, e) {
      var fd = new FormData();
      var files = $('#' + file_name)[0].files;
      var altTagInputId = 'alt_tag_' + file_name.replace('images_', '');
      var altTag;

      if ($('#' + altTagInputId).length) {
         altTag = $('#' + altTagInputId).val();
      } else {
         altTag = $('#alt_tag').val();
      }

      fd.append('alt_tag', altTag);

      var genBrouchDetails = '';
      if (file_type == 'general_brochures') {
         genBrouchDetails = $('#images_two_details').val();
      } else if (file_type == 'pg_brochures') {
         genBrouchDetails = $('#images_three_details').val();
      } else if (file_type == 'ug_brochures') {
         genBrouchDetails = $('#images_four_details').val();
      } else if (file_type == 'banner') {
         genBrouchDetails = $('#images_eleven_details').val();
      } else if (file_type == 'logo') {
         genBrouchDetails = $('#images_twelve_details').val();
      } else if (file_type == 'map_preview') {
         genBrouchDetails = $('#images_thirteen_details').val();
      } else if (file_type == 'course_catlog') {
         genBrouchDetails = $('#images_five_details').val();
      } else if (file_type == 'course_matrix') {
         genBrouchDetails = $('#images_six_details').val();
      } else if (file_type == 'campus_photos') {
         genBrouchDetails = $('#images_nine_details').val();
      } else if (file_type == 'student_life_photos') {
         genBrouchDetails = $('#images_ten_details').val();
      } else if (file_type == 'facilities_labs_research_center') {
         genBrouchDetails = $('#images_one_details').val();
      } else if (file_type == 'convocation_photos') {
         genBrouchDetails = $('#images_fourteen_details').val();
      } else if (file_type == 'event_photos') {
         genBrouchDetails = $('#images_fifteen_details').val();
      }

      fd.append('brochures_details', genBrouchDetails);

      for (var i = 0; i < files.length; i++) {
         fd.append('documents[]', files[i]);
      }

      if (files.length > 0) {
         fd.append('document_type', file_type);
         fd.append('university_id', university_id);

         event.preventDefault();

         $.ajax({
            url: '/admin/university/additionaldata/uploadDoc',
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
               console.log(response);
               document.getElementById(file_name).value = "";

               if (response['status'] == 'SUCCESS') {
                  var divData = response['data'];
                  if (divData.length) {
                     var imageDiv = "";

                     // divData.forEach(function(value, index) {
                     //    imageDiv += '<tr>' +
                     //       '<td>' + value.image_id + '</td>' +
                     //       '<td>' + (value.alt_tag ? value.alt_tag : 'N/A') + '</td>' +
                     //       '<td>' + value.image_url + '</td>' +
                     //       '<td>' + value.image_details + '</td>' +
                     //       '<td> <a target="_blank" href="' + value.image_url + '" class="btn btn-sm"><i class="fas fa-eye"></i> </a> </td>' +
                     //       '<td> <button class="btn btn-sm" type="button" onclick="showUniversityImageModal(' + value.image_id + ',\'' + file_type + '\', \'' + replaceDiv + '\',' + university_id + ')">' +
                     //       '<i class="fas fa-edit"></i> </button> </td>' +

                     //       '<td> <button class="btn btn-sm" type="button" onclick="deleteUniversityImage(' + value.image_id + ',\'' + file_type + '\', \'' + replaceDiv + '\',' + university_id + ')" ><i class="fas fa-trash"></i></button> </td>' +
                     //       '</tr>';
                     // });

                     divData.forEach(function(value, index) {
                        if (value.alt_tag && value.alt_tag.trim() !== "") {
                           // Condition 1: alt_tag is not empty
                           imageDiv += '<tr>' +
                              '<td>' + value.image_id + '</td>' +
                              '<td>' + value.alt_tag + '</td>' +
                              '<td>' + value.image_url + '</td>' +
                              '<td>' + value.image_details + '</td>' +
                              '<td> <a target="_blank" href="' + value.image_url + '" class="btn btn-sm"><i class="fas fa-eye"></i> </a> </td>' +
                              '<td> <button class="btn btn-sm" type="button" onclick="showUniversityImageModal(' + value.image_id + ',\'' + file_type + '\', \'' + replaceDiv + '\',' + university_id + ')">' +
                              '<i class="fas fa-edit"></i> </button> </td>' +
                              '<td> <button class="btn btn-sm" type="button" onclick="deleteUniversityImage(' + value.image_id + ',\'' + file_type + '\', \'' + replaceDiv + '\',' + university_id + ')" ><i class="fas fa-trash"></i></button> </td>' +
                              '</tr>';
                        } else {
                           // Condition 2: alt_tag is empty or null
                           imageDiv += '<tr>' +
                              '<td>' + value.image_id + '</td>' +
                              '<td>' + value.image_url + '</td>' +
                              '<td>' + value.image_details + '</td>' +
                              '<td> <a target="_blank" href="' + value.image_url + '" class="btn btn-sm"><i class="fas fa-eye"></i> </a> </td>' +
                              '<td> <button class="btn btn-sm" type="button" onclick="deleteUniversityImage(' + value.image_id + ',\'' + file_type + '\', \'' + replaceDiv + '\',' + university_id + ')" ><i class="fas fa-trash"></i></button> </td>' +
                              '</tr>';
                        }
                     });

                     document.getElementById(replaceDiv).innerHTML = imageDiv;
                  }
                  alert('Document Uploaded Successfully!');
                  location.reload();
                  e.preventDefault();
               } else {
                  var errorMsg = response['data'][0];
                  var errorFileName = response['file_name'];
                  alert(errorMsg + ' File Name - ' + errorFileName);
               }
            },
            error: function(xhr, status, error) {
               if (xhr.status == 413) {
                  alert("The file size is too large. Please upload a smaller file.");
               }
            }
         });
      } else {
         alert("Please select a file.");
      }
   }

   function showUniversityImageModal(imageId, columnName, university_id) {
      var dataString = {
         'university_id': university_id,
         'image_id': imageId,
         'column_name': columnName
      };

      event.preventDefault();

      $.ajax({
         url: '/admin/university/additionaldata/universityDataImageGet',
         type: 'post',
         data: dataString,
         cache: false,
         dataType: 'json',
         success: function(response) {
            console.log(response);
            if (response['status'] === 'SUCCESS') {
               var imageData = response['data'];

               $('#image_id').val(imageData.image_id);
               $('#images_details').val(imageData.image_details);
               $('#alt_tag').val(imageData.alt_tag);
               $('#column_name').val(columnName);

               if (imageData.image_url) {
                  $('#current_image_preview').attr('src', imageData.image_url);
               } else {
                  $('#current_image_preview').attr('src', '');
               }

               $('#exampleModalCenter').modal('show');
            } else {
               alert('Error: ' + response['message']);
            }
         },
         error: function() {
            alert('Error fetching data.');
         }
      });
   }


   $('#saveChanges').on('click', function(e) {
      e.preventDefault();

      var imageId = $('#image_id').val();
      var imageDetails = $('#images_details').val();
      var altTag = $('#alt_tag').val();

      var fd = new FormData();
      fd.append('image_id', imageId);
      fd.append('image_details', imageDetails);
      fd.append('alt_tag', altTag);

      var fileInput = $('#images_url')[0];
      if (fileInput.files.length > 0) {
         fd.append('images', fileInput.files[0]);
      }

      fd.append('university_id', $('#university_id').val());
      fd.append('column_name', $('#column_name').val());

      $.ajax({
         url: '/admin/university/additionaldata/updateImageData',
         type: 'post',
         data: fd,
         contentType: false,
         processData: false,
         dataType: 'json',
         success: function(response) {
            console.log(response);
            if (response.status === 'SUCCESS') {
               alert('Data updated successfully!');
               $('#exampleModalCenter').modal('hide');
               location.reload();
            } else {
               alert('Update failed: ' + response.message);
            }
         },
         error: function(xhr, status, error) {
            alert('Error updating data: ' + error);
         }
      });
   });


   function upload_video(file_name, file_type, university_id, replaceDiv, e) {
      var fd = new FormData();
      var files = $('#' + file_name)[0].files;
      //var files = $('#images_two').files;

      if (file_type == 'general_videos') {
         var genVideoDetails = $('#images_seven_details').val();
         fd.append('videos_details', genVideoDetails);
      } else if (file_type == 'testimonial_videos') {
         var genVideoDetails = $('#images_eight_details').val();
         fd.append('videos_details', genVideoDetails);
      }

      for (var i = 0; i < files.length; i++) {
         fd.append('documents[]', files[i]);
      }

      // Check file selected or not
      if (files.length > 0) {

         fd.append('document_type', file_type);
         fd.append('university_id', university_id);

         event.preventDefault();

         $.ajax({
            url: '/admin/university/additionaldata/uploadVideo',
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
               console.log(response);

               document.getElementById(file_name).value = "";

               if (response['status'] == 'SUCCESS') {



                  var divData = response['data'];
                  if (divData.length) {

                     var imageDiv = "";

                     divData.forEach(function(value, index) {

                        imageDiv += '<tr>' +
                           '<td>' + value.image_id + '</td>' +
                           '<td>' + value.image_url + '</td>' +
                           '<td>' + value.videos_details + '</td>' +
                           '<td> <a target="_blank" href="' + value.image_url + '" target="_blank" class="btn btn-sm btn-success"> View </a> </td>' +
                           '<td> <button class="btn btn-danger" type="button"  onclick="deleteUniversityImage(' + value.image_id + ',\'' + file_type + '\', \'' + replaceDiv + '\',' + university_id + ')" > Delete</button> </td>' +
                           '</tr>';
                     });

                     document.getElementById(replaceDiv).innerHTML = imageDiv;
                     // let active_acc = document.getElementsByClassName("accordion active");
                     // active_acc[0].click();

                  }
                  alert('Video Uploaded Successfully!');
                  e.preventDefault();
               } else {
                  var errorMsg = response['data'][0];
                  alert(errorMsg);
               }
            },
            error: function(xhr, status, error) {
               if (xhr.status == 413) {
                  alert("The file size is too larged. Please upload with less size");
               }
            }
         });
      } else {
         alert("Please select a file.");
      }

   }

   function deleteUniversityImage(imageId, columnName, replaceDiv, university_id, e) {
      var dataString = {
         'university_id': university_id,
         'image_id': imageId,
         'column_name': columnName
      };
      event.preventDefault();
      $.ajax({
         url: '/admin/university/additionaldata/universityDataImageDelete',
         type: 'post',
         data: dataString,
         cache: false,
         success: function(response) {
            console.log(response);
            if (response['status'] == 'SUCCESS') {


               var divData = response['data'];
               if (divData.length) {

                  var imageDiv = "";

                  divData.forEach(function(value, index) {

                     imageDiv += '<tr>' +
                        '<td>' + value.image_id + '</td>' +
                        '<td>' + value.image_url + '</td>' +
                        '<td>' + value.image_details + '</td>' +
                        '<td> <a target="_blank" href="' + value.image_url + '" target="_blank" class="btn btn-sm btn-success"> View </a> </td>' +
                        '<td> <button class="btn btn-danger" type="button"  onclick="deleteUniversityImage(' + value.image_id + ',\'' + columnName + '\', \'' + replaceDiv + '\',' + university_id + ')" > Delete</button> </td>' +
                        '</tr>';
                  });

                  document.getElementById(replaceDiv).innerHTML = imageDiv;

                  // let active_acc = document.getElementsByClassName("accordion active");
                  // active_acc[0].click();

               } else {
                  document.getElementById(replaceDiv).innerHTML = '';
               }
               alert('Document deleted successfully!');
               e.preventDefault();
            } else {
               alert('Error Occured');
            }
         },
      });

   }

   function submitUniversityData() {

      var university_id = <?= $universityId; ?>;
      var application_fee_waiver_imperial = $('#application_fee_waiver_imperial').val();
      var university_long_overview = $('#university_long_overview').val();
      var university_short_overview = $('#university_short_overview').val();
      var list_of_ranking = $('#list_of_ranking').val();
      var total_international_student_year = $('#total_international_student_year').val();
      var total_indian_student_year = $('#total_indian_student_year').val();
      var scholarship_international = $('#scholarship_international').val();
      var job_placement_overview = $('#job_placement_overview').val();
      var top_reason_to_study = $('#top_reason_to_study').val();
      var top_course_pg = $('#top_course_pg').val();
      var top_course_ug = $('#top_course_ug').val();
      var desired_levelofcourse = $('#desired_levelofcourse').val();
      var research_spending_endowment = $('#research_spending_endowment').val();
      var active_research_areas = $('#active_research_areas').val();
      var short_overview_study = $('#short_overview_study').val();

      //
      var smm_facebook = $('#smm_facebook').val();
      var smm_twitter = $('#smm_twitter').val();
      var smm_instagram = $('#smm_instagram').val();
      var smm_youtube = $('#smm_youtube').val();
      var smm_linkedin = $('#smm_linkedin').val();

      //

      var college_name_pg = new Array();
      $("input[name='college_name_pg[]']").each(function() {
         college_name_pg.push($(this).val());
      });

      var gpa_score_pg = new Array();
      $("input[name='gpa_score_pg[]']").each(function() {
         gpa_score_pg.push($(this).val());
      });

      var gre_score_pg = new Array();
      $("input[name='gre_score_pg[]']").each(function() {
         gre_score_pg.push($(this).val());
      });

      var tofel_score_pg = new Array();
      $("input[name='tofel_score_pg[]']").each(function() {
         tofel_score_pg.push($(this).val());
      });

      var cost_pg = new Array();
      $("input[name='cost_pg[]']").each(function() {
         cost_pg.push($(this).val());
      });

      //
      var college_name_ug = new Array();
      $("input[name='college_name_ug[]']").each(function() {
         college_name_ug.push($(this).val());
      });

      var gpa_score_ug = new Array();
      $("input[name='gpa_score_ug[]']").each(function() {
         gpa_score_ug.push($(this).val());
      });

      var gre_score_ug = new Array();
      $("input[name='gre_score_ug[]']").each(function() {
         gre_score_ug.push($(this).val());
      });

      var tofel_score_ug = new Array();
      $("input[name='tofel_score_ug[]']").each(function() {
         tofel_score_ug.push($(this).val());
      });

      var cost_ug = new Array();
      $("input[name='cost_ug[]']").each(function() {
         cost_ug.push($(this).val());
      });


      //
      var short_overview_location = $('#short_overview_location').val();
      var new_upcoming_courses = $('#new_upcoming_courses').val();

      var dataString = {
         'university_id': university_id,
         'application_fee_waiver_imperial': application_fee_waiver_imperial != '' ? application_fee_waiver_imperial : null,
         'university_long_overview': university_long_overview,
         'university_short_overview': university_short_overview,
         'list_of_ranking': list_of_ranking,
         'total_international_student_year': total_international_student_year,
         'total_indian_student_year': total_indian_student_year,
         'scholarship_international': scholarship_international,
         'job_placement_overview': job_placement_overview,
         'top_reason_to_study': top_reason_to_study,
         'top_course_pg': top_course_pg,
         'top_course_ug': top_course_ug,
         'research_spending_endowment': research_spending_endowment,
         'active_research_areas': active_research_areas,
         'short_overview_study': short_overview_study,
         'smm_facebook': smm_facebook,
         'smm_twitter': smm_twitter,
         'smm_instagram': smm_instagram,
         'smm_youtube': smm_youtube,
         'smm_linkedin': smm_linkedin,
         'short_overview_location': short_overview_location,
         'new_upcoming_courses': new_upcoming_courses,
         'college_name_pg': college_name_pg,
         'gpa_score_pg': gpa_score_pg,
         'gre_score_pg': gre_score_pg,
         'tofel_score_pg': gpa_score_pg,
         'cost_pg': cost_pg,
         'college_name_ug': college_name_ug,
         'gpa_score_ug': gpa_score_ug,
         'gre_score_ug': gre_score_ug,
         'tofel_score_ug': tofel_score_ug,
         'cost_ug': cost_ug

      };

      event.preventDefault();

      $.ajax({
         url: '/admin/university/additionaldata/universityDataSubmit',
         type: 'post',
         data: dataString,
         cache: false,
         success: function(response) {
            //alert(response);
            console.log(response);
            if (response == 'SUCCESS') {
               alert('Data Submitted Successfully');
            } else {
               alert('Error Occured');
            }
         },
         error: function(xhr, status, error) {

         }
      });

   }

   var totalcollegePG = <?php echo $ugEntryCount; ?>;

   function addCollegePG() {

      var htmlpg = '<div class="col-md-12" style="margin-bottom: 8px;" id="college_pg_' + (totalcollegePG + 1) + '" ><label for="inputEmail3" class="col-sm-2 control-label"></label><div  class="col-sm-3"><input type="text" id="college_name_pg' + (totalcollegePG + 1) + '" name="college_name_pg[]" placeholder="College Name" class="form-control" /></div><div  class="col-sm-1"> <input type="text" id="gpa_pg' + (totalcollegePG + 1) + '" name="gpa_score_pg[]" placeholder="GPA" class="form-control" /></div> <div  class="col-sm-1"> <input type="text" id="gre_pg' + (totalcollegePG + 1) + '" name="gre_score_pg[]" placeholder="GRE" class="form-control" /></div> <div  class="col-sm-1"> <input type="text" id="tofel_score_pg' + (totalcollegeUG + 1) + '" name="tofel_score_pg[]" placeholder="TOFEL" class="form-control" /></div> <div  class="col-sm-2"> <input type="text" id="cost_pg' + (totalcollegeUG + 1) + '" name="cost_pg[]" placeholder="COST" class="form-control" /></div> <div class="col-sm-1" > <input type="button" name="delete" value="Delete" onclick="deletePg(' + (totalcollegePG + 1) + ')"></div></div>';

      $("#pgdiv").before(htmlpg);

      totalcollegePG++;
   }

   function deletePg(pgId) {
      $("#college_pg_" + pgId).remove();
      totalcollegePG--;
   }

   var totalcollegeUG = <?php echo $ugEntryCount; ?>;

   function addCollegeUG() {

      var htmlug = '<div class="col-md-12" style="margin-bottom: 8px;"  id="college_ug_' + (totalcollegeUG + 1) + '" ><label for="inputEmail3" class="col-sm-2 control-label"></label><div  class="col-sm-3"><input type="text" id="college_name_ug' + (totalcollegeUG + 1) + '" name="college_name_ug[]" placeholder="College Name" class="form-control" /></div><div  class="col-sm-1"> <input type="text" id="gpa_ug' + (totalcollegeUG + 1) + '" name="gpa_score_ug[]" placeholder="GPA" class="form-control" /></div> <div  class="col-sm-1"> <input type="text" id="gre_ug' + (totalcollegeUG + 1) + '" name="gre_score_ug[]" placeholder="GRE" class="form-control" /></div> <div  class="col-sm-1"> <input type="text" id="tofel_score_ug' + (totalcollegeUG + 1) + '" name="tofel_score_ug[]" placeholder="TOFEL" class="form-control" /></div> <div  class="col-sm-2"> <input type="text" id="cost_ug' + (totalcollegeUG + 1) + '" name="cost_ug[]" placeholder="COST" class="form-control" /></div> <div class="col-sm-1" > <input type="button" name="delete" value="Delete" onclick="deleteUg(' + (totalcollegeUG + 1) + ')"></div></div>';

      $("#ugdiv").before(htmlug);

      totalcollegeUG++;
   }

   function deleteUg(ugId) {
      $("#college_ug_" + ugId).remove();
      totalcollegeUG--;
   }

   // var acc = document.getElementsByClassName("accordion");
   // var i;
   // var last;
   // var alwaysOneOpen = true;

   // for (i = 0; i < acc.length; i++) {
   //   acc[i].onclick = function(e) {
   //     e.preventDefault();
   //     // If the same accordion was clicked, toggle it
   //     if (last === this && !alwaysOneOpen) {
   //       toggleAccordion(last);
   //     } else {
   //       // If another accordion was open, close it
   //       if (last) {
   //         closeAccordion(last);
   //       }
   //       // Open clicked accordion
   //       last = this;
   //       openAccordion(last);
   //     }
   //   };
   // }

   // var closeAccordion = function(acc) {
   //   acc.classList.remove("active");
   //   var panel = acc.nextElementSibling;
   //     panel.classList.remove("active");
   //   panel.style.maxHeight = null;
   // }

   // var openAccordion = function(acc) {
   //   acc.classList.add("active");
   //   var panel = acc.nextElementSibling;
   //     panel.classList.add("active");
   //   panel.style.maxHeight = panel.scrollHeight + "px";
   // }

   // var toggleAccordion = function(acc) {
   //   last.classList.toggle("active");
   //   var panel = last.nextElementSibling;
   //   panel.classList.toggle("active");
   //   if (panel.style.maxHeight) {
   //     panel.style.maxHeight = null;
   //   } else {
   //     panel.style.maxHeight = panel.scrollHeight + "px";
   //   }
   // };

   // last = acc[0];
   // toggleAccordion(last);

   $(document).ready(function() {
      $("#accordion").accordion({
         active: false,
         collapsable: true
      });
   });

   var acc = document.getElementsByClassName("accordion");
   var i;

   for (i = 0; i < acc.length; i++) {
      acc[i].onclick = function(e) {
         e.preventDefault();
         this.classList.toggle("active");
         var panel = this.nextElementSibling;
         if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
         } else {
            panel.style.maxHeight = panel.scrollHeight + "10px";
         }
      }
   }
</script>