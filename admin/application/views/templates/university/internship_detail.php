<style type="text/css">
    .chkbx{
        float: left;
        margin: 8px 23px 1px 0px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Student<small>Manage Internships</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Student Management</a></li>
            <li class="active">Internship Form</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-10">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Internship</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="" method="post" name="adduni" autocomplete="off">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="university" class="col-sm-2 control-label">University *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="university" value="<?php echo $university_name;?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="college" class="col-sm-2 control-label">College *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="college" value="<?php echo $college_name;?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="department" class="col-sm-2 control-label">Department *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="department" value="<?php echo $department_name;?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="university" class="col-sm-2 control-label">Student Name *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="student_name" value="<?php echo $student_name;?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="college" class="col-sm-2 control-label">Current University *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="current_university" value="<?php echo $student_other_details->current_university;?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="department" class="col-sm-2 control-label">Current Course *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="current_course" value="<?php echo $student_other_details->current_course;?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                              <label for="application_deadline" class="col-sm-2 control-label">Expected Graduation Year</label>
                              <div class="col-sm-10">
                                <input type="text" class="mws-datepicker small" id="datepicker_3" name="expected_graduation_year" value="<?php echo $student_other_details->expected_graduation_year;?>" disabled>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="application_deadline" class="col-sm-2 control-label">Application Deadline</label>
                              <div class="col-sm-10">
                                <input type="text" class="mws-datepicker small" id="datepicker" name="application_deadline" value="<?php echo $application_deadline;?>" disabled>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="internship_start_date" class="col-sm-2 control-label">Internship From</label>
                              <div class="col-sm-10">
                                <input type="text" class="mws-datepicker small" id="datepicker_1" name="internship_start_date" value="<?php echo $internship_start_date;?>" disabled>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="internship_end_date" class="col-sm-2 control-label">Internship To</label>
                              <div class="col-sm-10">
                                <input type="text" class="mws-datepicker small" id="datepicker_2" name="internship_end_date" value="<?php echo $internship_end_date;?>" disabled>
                              </div>
                            </div>
                            <div class="form-group">
                                <label for="professors" class="col-sm-2 control-label">Professors</label>
                                <div class="col-sm-10">
                                    <textarea rows="4" class="form-control" id="professors" name="professors" placeholder="Professors" disabled><?php echo $professors; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="charges" class="col-sm-2 control-label">Charges</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="charges" name="charges" placeholder="Charges" value="<?php echo $charges; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="description" class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-10">
                                    <textarea rows="4" class="form-control" id="description" name="description" placeholder="Description"><?php echo $description; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="other_fees" class="col-sm-2 control-label">Other Fees</label>
                                <div class="col-sm-10">
                                    <textarea rows="4" class="form-control" id="other_fees" name="other_fees" placeholder="Other Fees" disabled><?php echo $other_fees; ?></textarea>
                                </div>
                            </div>
                            <?php
                            if($other_details)
                            {
                                foreach($other_details as $index => $question)
                                {
                                    ?>
                                    <div class="form-group">
                                        <label for="charges" class="col-sm-2 control-label">Question <?=($index+1)?></label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="questions[]" value="<?php echo $question; ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="charges" class="col-sm-2 control-label">Answer <?=($index+1)?></label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="answers[]" value="<?php echo $answers[$index]; ?>" disabled>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div><!-- /.box-body -->
                        <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                    </form>
                </div><!-- /.box -->
                <!-- general form elements disabled -->
            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
