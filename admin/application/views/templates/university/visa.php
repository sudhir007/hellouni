<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>University<small>Applied Universities</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">University Management</a></li>
            <li class="active">Applied Universities</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php
                if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>
                <div class="box">
                    <form action="<?php echo base_url();?>user/university/multidelete" method="post">
                        <?php
                        if($applications)
                        {
                            ?>
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>University Name</th>
                                            <th>College Name</th>
                                            <th>Student Name</th>
                                            <th>Student Email</th>
                                            <th>Student Phone</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($applications as $application)
                                    {
                                        ?>
                                        <tr>
                                            <td><input type="checkbox" class="multichk" name="chk[]" value="<?php echo $application['id'];?>"/></td>
                                            <td><?php echo $application['name'];?></td>
                                            <td><?php echo $application['college_name'];?></td>
                                            <td><?php echo $application['student_name'];?></td>
                                            <td><?php echo $application['student_email'];?></td>
                                            <td><?php echo $application['student_phone'];?></td>
                                            <td><?php echo $application['status'];?></td>
                                            <td>
                                                <a href="<?php echo base_url();?>university/visa/detail?uid=<?=$application['university_id']?>&aid=<?=$application['id']?>" title="View Application" target="_blank">View Application</a>
                                            </td>
                                            </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th>University Name</th>
                                            <th>College Name</th>
                                            <th>Student Name</th>
                                            <th>Student Email</th>
                                            <th>Student Phone</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div><!-- /.box-body -->
                            <?php
                        }
                        ?>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
