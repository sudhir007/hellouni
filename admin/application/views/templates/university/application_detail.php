<style type="text/css">
    .chkbx{
        float: left;
        margin: 8px 23px 1px 0px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>University<small>Application Detail</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">University Management</a></li>
            <li class="active">Application Detail</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-10">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <!-- form start -->
                    <form class="form-horizontal" action="" method="post" name="adduni" autocomplete="off">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="university" class="col-sm-2 control-label">University</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="university_name" value="<?php echo $university_name;?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="college" class="col-sm-2 control-label">College</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="college_name" value="<?php echo $college_name;?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="university" class="col-sm-2 control-label">Student Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="student_name" value="<?php echo $student_name;?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="student_email" class="col-sm-2 control-label">Student Email</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="student_email" value="<?php echo $student_email;?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                              <label for="student_phone" class="col-sm-2 control-label">Student Phone</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control" id="student_phone" name="student_phone" value="<?php echo $student_phone;?>" disabled>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="login_url" class="col-sm-2 control-label">Login URL</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control" id="login_url" name="login_url" value="<?php echo $login_url;?>" disabled>
                              </div>
                            </div>
                            <div class="form-group">
                                <label for="username" class="col-sm-2 control-label">Username</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="username" value="<?php echo $username;?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="password" value="<?php echo $password;?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                              <label for="application_deadline" class="col-sm-2 control-label">Application Deadline</label>
                              <div class="col-sm-10">
                                <input type="text" class="mws-datepicker small" id="application_deadline" name="application_deadline" value="<?php echo $application_deadline;?>" disabled>
                              </div>
                            </div>
                            <div class="form-group">
                                <label for="gre_code" class="col-sm-2 control-label">GRE(Code)</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="gre_code" value="<?php echo $gre_code;?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="toefl_code" class="col-sm-2 control-label">TOEFL(Code)</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="toefl_code" value="<?php echo $toefl_code;?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                              <label for="sop_needed" class="col-sm-2 control-label">SOP (Statement Of Purpose)</label>
                              <div class="col-sm-10">
                                  <label>Yes &nbsp;</label><input type="radio" name="sop_needed" value="1" <?=$sop_needed == 1 ? 'checked' : ''?> disabled>
                                  <label>No &nbsp;</label><input type="radio" name="sop_needed" value="0" <?=$sop_needed == 0 ? 'checked' : ''?> disabled>
                              </div>
                            </div>
                            <div class="form-group" style="display:<?=$sop_no_of_words?>">
                                <label class="col-sm-2 control-label">SOP - Number Of Words</label>
                                <div class="col-sm-10">
                                    <input type="text" name="sop_no_of_words" class="form-control" value="<?=$sop_no_of_words?>">
                                </div>
                            </div>
                            <div class="form-group">
                              <label for="lor_needed" class="col-sm-2 control-label">LOR (Letter Of Recommendation)</label>
                              <div class="col-sm-10">
                                  <label>Yes &nbsp;</label><input type="radio" name="lor_needed" value="1" <?=$lor_needed == 1 ? 'checked' : ''?> disabled>
                                  <label>No &nbsp;</label><input type="radio" name="lor_needed" value="0" <?=$lor_needed == 0 ? 'checked' : ''?> disabled>
                              </div>
                            </div>
                            <div class="form-group" style="display:<?=$lor_required?>">
                                <label for="lor_required" class="col-sm-2 control-label">LOR Required</label>
                                <div class="col-sm-10">
                                    <input type="text" name="lor_required" class="form-control" value="<?=$lor_required?>">
                                </div>
                            </div>
                            <div class="form-group" style="display:<?=$lor_type?>">
                              <label for="lor_type" class="col-sm-2 control-label">LOR Type</label>
                              <div class="col-sm-10">
                                  <label>Offline &nbsp;</label><input type="radio" name="lor_type" value="offline" <?=$lor_type == 'offline' ? 'checked' : ''?> disabled>
                                  <label>Online &nbsp;</label><input type="radio" name="lor_type" value="online" <?=$lor_type == 'online' ? 'checked' : ''?> disabled>
                                  <label>Online (After Application) &nbsp;</label><input type="radio" name="lor_type" value="online_after_application" <?=$lor_type == 'online_after_application' ? 'checked' : ''?> disabled>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="transcripts" class="col-sm-2 control-label">Transcripts</label>
                              <div class="col-sm-10">
                                  <label>After Admit &nbsp;</label><input type="radio" name="transcript" value="after_admit" <?=$transcript == 'after_admit' ? 'checked' : ''?> disabled>
                                  <label>Before Admit &nbsp;</label><input type="radio" name="transcript" value="before_admit" <?=$transcript == 'before_admit' ? 'checked' : ''?> disabled>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="transcript_submitted" class="col-sm-2 control-label">Transcripts Submitted?</label>
                              <div class="col-sm-10">
                                  <label>Yes &nbsp;</label><input type="radio" name="transcript_submitted" value="1" <?=$transcript_submitted == 1 ? 'checked' : ''?> disabled>
                                  <label>No &nbsp;</label><input type="radio" name="transcript_submitted" value="0" <?=$transcript_submitted == 0 ? 'checked' : ''?> disabled>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="application_fee" class="col-sm-2 control-label">Application Fee</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control" id="application_fee" name="application_fee" value="<?php echo $application_fee;?>" disabled>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="application_fee_paid" class="col-sm-2 control-label">Application Fee Paid?</label>
                              <div class="col-sm-10">
                                  <label>Yes &nbsp;</label><input type="radio" name="application_fee_paid" value="1" <?=$application_fee_paid == 1 ? 'checked' : ''?> disabled>
                                  <label>No &nbsp;</label><input type="radio" name="application_fee_paid" value="0" <?=$application_fee_paid == 0 ? 'checked' : ''?> disabled>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="request_url" class="col-sm-2 control-label">Request URL</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control" id="request_url" name="request_url" value="<?php echo $request_url;?>" disabled>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="status" class="col-sm-2 control-label">Status</label>
                              <div class="col-sm-10">
                                <input type="text" class="form-control" id="status" name="status" value="<?php echo $status;?>" disabled>
                              </div>
                            </div>
                        </div><!-- /.box-body -->
                    </form>
                </div><!-- /.box -->
                <!-- general form elements disabled -->
            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
