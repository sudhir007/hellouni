<script src='<?= base_url() ?>resources/tinymce/tinymce.min.js'></script>
<style type="text/css">
  @import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");

  /* Style the tab */

  .tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
  }


  /* Style the buttons inside the tab */

  .tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
  }


  /* Change background color of buttons on hover */

  .tab button:hover {
    background-color: #ddd;
  }


  /* Create an active/current tablink class */

  .tab button.active {
    background-color: #ccc;
  }


  /* Style the tab content */

  .tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
  }

  table {
    border-collapse: collapse;
  }

  tr {
    border-bottom: 1px solid #ccc;
  }

  th,
  td {
    text-align: left;
    padding: 4px;
  }

  .textPara {
    min-height: 100px;
    width: 100%;
  }

  .control-label {
    margin-bottom: 6px !important;
    font-weight: 700;
  }

  .accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
  }

  .accordion1 {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
  }

  .active,
  .accordion:hover {
    background-color: #ccc;
  }

  .active,
  .accordion1:hover {
    background-color: #ccc;
  }

  .panel {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
    box-shadow: 0px 2px 2px 0px #00000061;
  }

  .panel1 {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
    box-shadow: 0px 2px 2px 0px #00000061;
  }

  .panelFAQ {
    margin-bottom: 20px;
    background-color: #fff;
    border: 1px solid transparent;
    border-radius: 4px;
    -webkit-box-shadow: 0 1px 1px rgb(0 0 0 / 5%);
    box-shadow: 0 3px 2px rgb(0 0 0 / 13%);
    max-height: inherit;
    padding: 0px 0px;
  }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>University <small>University Info</small></h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">University</a></li>
      <li class="active">University Info</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Add / Update University Info (<b><?php echo $university[0]['name']; ?></b>)</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <?php
          if ($this->session->flashdata('flash_message')) {
          ?>
            <div class="alert alert-error alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
              <h4><i class="icon fa fa-circle-o"></i> Error</h4>
              <?php
              echo $this->session->flashdata('flash_message');
              ?>
            </div>
          <?php
          }
          ?>
          <div class="box-body">
            <div class="tab">
              <button class="tablinks" onclick="tabChange(event, 'div_tab_one')" id="defaultOpen">Overview</button>
              <button class="tablinks" onclick="tabChange(event, 'div_tab_two')">Courses </button>
              <button class="tablinks" onclick="tabChange(event, 'div_tab_three')">Ranking</button>
              <button class="tablinks" onclick="tabChange(event, 'div_tab_four')">Admission</button>
              <button class="tablinks" onclick="tabChange(event, 'div_tab_five')">Scholarships</button>
              <button class="tablinks" onclick="tabChange(event, 'div_tab_six')">FAQs </button>
              <button class="tablinks" onclick="tabChange(event, 'div_tab_eleven')">Latest Update </button>
              <button class="tablinks" onclick="tabChange(event, 'div_tab_seven')">SEO </button>
              <!-- <button class="tablinks" onclick="tabChange(event, 'div_tab_eight')">University Images </button>
                  <button class="tablinks" onclick="tabChange(event, 'div_tab_nine')">Gallary </button> -->
              <button class="tablinks" onclick="tabChange(event, 'div_tab_ten')">Social Media </button>

            </div>
            <div id="div_tab_one" class="tabcontent">
              <form class="form-horizontal" method="post" onsubmit="updateOverview(<?php echo $university[0]['id']; ?>)" name="tab_one_update" autocomplete="off">
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">University Name *</label>
                  <div class="col-sm-10">
                    <label for="name" class="col-sm-6 control-label">
                      <?php echo $university[0]['name']; ?>
                    </label>
                  </div>
                </div>
                <!-- <div class="col-md-12 form-group">
                      <label class="control-label">Upload Logo(Image) For <?php echo $university[0]['name']; ?> </label>
                      <div class="col-sm-12">
                        <textarea class='imageEditor'>

                        </textarea>
                      </div>
                    </div> -->
                <!-- <div class="form-group">
                          <label for="email" class="col-sm-2 control-label">Content Tab One *</label>
                          <div class="col-sm-10">
                            <textarea class='editor' name="tab_one" id="tab_one"><?php if (isset($university_info[0])) {
                                                                                    echo $university_info[0]['tab_one'];
                                                                                  } else {
                                                                                    echo "";
                                                                                  } ?></textarea>
                          </div>
                        <div id="div_tab_one" class="tabcontent">
                          <form class="form-horizontal" method="post" onsubmit="updateTabInfoOne(<?php echo $university[0]['id']; ?>)" name="tab_one_update" autocomplete="off">
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">University Name *</label>
                                <div class="col-sm-10">
                                    <label for="name" class="col-sm-6 control-label"><?php echo $university[0]['name']; ?></label>
                                </div>
                            </div>

                             <div class="form-group">
                                <label for="email" class="col-sm-2 control-label">Content Tab One *</label>
                                <div class="col-sm-10">
                                  <textarea class='editor' name="tab_one" id="tab_one"><?php if (isset($university_info[0])) {
                                                                                          echo $university_info[0]['tab_one'];
                                                                                        } else {
                                                                                          echo "";
                                                                                        } ?></textarea>
                                </div>
                            </div> -->

                <div class="col-md-12">
                  <button type='button' class="accordion">Section 1</button>
                  <div class="panel">
                    <div class="col-md-12">
                      <h4>Section 1 * (Numbers and Counters)</h4>
                      <div class="col-md-6 form-group">
                        <label for="name" class="col-md-4 control-label">Total Students</label>
                        <div class="col-md-8">
                          <input class="form-control" id="total_students" name="total_students" placeholder="Enter Number Value" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                          echo $university_overview_info[0]['total_students'];
                                                                                                                                        } else {
                                                                                                                                          echo "";
                                                                                                                                        } ?>">
                        </div>
                      </div>
                      <div class="col-md-6 form-group">
                        <label for="name" class="col-md-4 control-label">International Students</label>
                        <div class="col-md-8">
                          <input class="form-control" id="international_students" name="international_students" placeholder="Enter Number Value" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                                          echo $university_overview_info[0]['international_students'];
                                                                                                                                                        } else {
                                                                                                                                                          echo "";
                                                                                                                                                        } ?>">
                        </div>
                      </div>

                      <!-- <div class="col-md-6 form-group">
                                         <label for="type" class="col-md-4 control-label">Type</label>
                                          <div class="col-md-8">
                                          <select class="form-control" name="type">
                                            <option value="">Select Type</option>
                                            <option value="University Private" <?php if (isset($university_overview_info[0]) && $university_overview_info[0]['type'] == 'University Private') {
                                                                                  echo 'selected="selected"';
                                                                                } ?>>University Private</option>
                                            <option value="University Public" <?php if (isset($university_overview_info[0]) && $university_overview_info[0]['type'] == 'University Public') {
                                                                                echo 'selected="selected"';
                                                                              } ?>>University Public</option>
                                            <option value="University Private Not for Profit" <?php if (isset($university_overview_info[0]) && $university_overview_info[0]['type'] == 'University Private Not for Profit') {
                                                                                                echo 'selected="selected"';
                                                                                              } ?>>University Private Not for Profit</option>
                                            <option value="College Private" <?php if (isset($university_overview_info[0]) && $university_overview_info[0]['type'] == 'College Private') {
                                                                              echo 'selected="selected"';
                                                                            } ?>>College Private</option>
                                            <option value="College Public" <?php if (isset($university_overview_info[0]) && $university_overview_info[0]['type'] == 'College Public') {
                                                                              echo 'selected="selected"';
                                                                            } ?>>College Public</option>
                                            <option value="College Private not for Profit" <?php if (isset($university_overview_info[0]) && $university_overview_info[0]['type'] == 'College Private not for Profit') {
                                                                                              echo 'selected="selected"';
                                                                                            } ?>>College Private not for Profit</option>
                                            <option value="Non Degree Colleges" <?php if (isset($university_overview_info[0]) && $university_overview_info[0]['type'] == 'Non Degree Colleges') {
                                                                                  echo 'selected="selected"';
                                                                                } ?>>Non Degree Colleges</option>
                                          </select>
                                          </div>
                                    </div> -->
                      <div class="col-md-6 form-group">
                        <label for="type" class="col-md-4 control-label">Type</label>
                        <div class="col-md-8">
                          <select class="form-control" name="type" id="type">
                            <option value="">Select Type</option>
                            <option value="University Private"
                              <?php if (isset($university_overview_info[0]) && $university_overview_info[0]['type'] == 'University Private') {
                                echo 'selected="selected"';
                              } ?>>University Private
                            </option>
                            <option value="University Public"
                              <?php if (isset($university_overview_info[0]) && $university_overview_info[0]['type'] == 'University Public') {
                                echo 'selected="selected"';
                              } ?>>University Public
                            </option>
                            <option value="University Private Not for Profit"
                              <?php if (isset($university_overview_info[0]) && $university_overview_info[0]['type'] == 'University Private Not for Profit') {
                                echo 'selected="selected"';
                              } ?>>University Private Not for Profit
                            </option>
                            <option value="College Private"
                              <?php if (isset($university_overview_info[0]) && $university_overview_info[0]['type'] == 'College Private') {
                                echo 'selected="selected"';
                              } ?>>College Private
                            </option>
                            <option value="College Public"
                              <?php if (isset($university_overview_info[0]) && $university_overview_info[0]['type'] == 'College Public') {
                                echo 'selected="selected"';
                              } ?>>College Public
                            </option>
                            <option value="College Private not for Profit"
                              <?php if (isset($university_overview_info[0]) && $university_overview_info[0]['type'] == 'College Private not for Profit') {
                                echo 'selected="selected"';
                              } ?>>College Private not for Profit
                            </option>
                            <option value="Non Degree Colleges"
                              <?php if (isset($university_overview_info[0]) && $university_overview_info[0]['type'] == 'Non Degree Colleges') {
                                echo 'selected="selected"';
                              } ?>>Non Degree Colleges
                            </option>
                          </select>
                        </div>
                      </div>

                      <div class="col-md-6 form-group">
                        <label for="name" class="col-md-4 control-label">Application Fee (UG / PG)</label>
                        <div class="col-md-8">
                          <input class="form-control" id="application_fee" name="application_fee" placeholder="Enter Number Value" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                            echo $university_overview_info[0]['application_fee'];
                                                                                                                                          } else {
                                                                                                                                            echo "";
                                                                                                                                          } ?>">
                        </div>
                      </div>


                      <div class="col-md-6 form-group">
                        <label for="name" class="col-md-4 control-label">Total Degree Programs</label>
                        <div class="col-md-8">
                          <input class="form-control" id="total_degree_programs" name="total_degree_programs" placeholder="Enter Number Value" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                                        echo $university_overview_info[0]['total_degree_programs'];
                                                                                                                                                      } else {
                                                                                                                                                        echo "";
                                                                                                                                                      } ?>">
                        </div>
                      </div>

                      <div class="col-md-6 form-group">
                        <label for="name" class="col-md-4 control-label">Establishment Year</label>
                        <div class="col-md-8">
                          <input class="form-control" id="establishment_year" name="establishment_year" placeholder="Enter Number Value" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                                  echo $university_overview_info[0]['establishment_year'];
                                                                                                                                                } else {
                                                                                                                                                  echo "";
                                                                                                                                                } ?>">
                        </div>
                      </div>



                      <div class="col-md-6 form-group">
                        <label for="name" class="col-md-4 control-label">Location</label>
                        <div class="col-md-8">
                          <input class="form-control" id="location" name="location" placeholder="Enter Number Value" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                              echo $university_overview_info[0]['location'];
                                                                                                                            } else {
                                                                                                                              echo "";
                                                                                                                            } ?>">
                        </div>
                      </div>

                      <div class="col-md-6 form-group">
                        <label for="name" class="col-md-4 control-label">Acceptance Rate</label>
                        <div class="col-md-8">
                          <input class="form-control" id="acceptance_rate" name="acceptance_rate" placeholder="Enter Number Value" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                            echo $university_overview_info[0]['acceptance_rate'];
                                                                                                                                          } else {
                                                                                                                                            echo "";
                                                                                                                                          } ?>">
                        </div>
                      </div>
                      <div class="col-md-6 form-group">
                        <label for="name" class="col-md-4 control-label">Co-Op / CPT / Internships</label>
                        <div class="col-md-8">
                          <input class="form-control" id="internships" name="internships" placeholder="Enter Number Value" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                    echo $university_overview_info[0]['internships'];
                                                                                                                                  } else {
                                                                                                                                    echo "";
                                                                                                                                  } ?>">
                        </div>
                      </div>

                      <div class="col-md-6 form-group">
                        <label for="name" class="col-md-4 control-label">Assistantships </label>
                        <div class="col-md-8">
                          <input class="form-control" id="assistantantships" name="assistantantships" placeholder="Enter Number Value" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                                echo $university_overview_info[0]['assistantantships'];
                                                                                                                                              } else {
                                                                                                                                                echo "";
                                                                                                                                              } ?>">
                        </div>
                      </div>


                      <!-- <div class="col-md-6 form-group">
                                      <label for="name" class="col-md-4 control-label">Average Tuition Fee (Int'l)</label>
                                      <div class="col-md-8">
                                        <input class="form-control" id="average_tuition_fee" name="average_tuition_fee" placeholder="Enter Number Value" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                                                  echo $university_overview_info[0]['average_tuition_fee'];
                                                                                                                                                                } else {
                                                                                                                                                                  echo "";
                                                                                                                                                                } ?>">
                                      </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                      <label for="name" class="col-md-4 control-label">Average Cost of Living</label>
                                      <div class="col-md-8">
                                        <input class="form-control" id="average_cost_living" name="average_cost_living" placeholder="Enter Number Value" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                                                  echo $university_overview_info[0]['average_cost_living'];
                                                                                                                                                                } else {
                                                                                                                                                                  echo "";
                                                                                                                                                                } ?>">
                                      </div>
                                    </div> -->




                      <!-- <div class="col-md-12 form-group">
                                      <h5><b>Average Eligibility</b></h5>
                                      <div class="col-md-6 form-group">
                                        <label for="name" class="col-md-4 control-label">PG</label>
                                        <div class="col-md-4">
                                          <input class="form-control" id="average_eligibility_pg_gpa" name="average_eligibility_pg_gpa" placeholder="GPA" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                                                    echo $university_overview_info[0]['average_eligibility']['pg_gpa'];
                                                                                                                                                                  } else {
                                                                                                                                                                    echo "";
                                                                                                                                                                  } ?>" >
                                        </div>
                                        <div class="col-md-4">
                                          <input class="form-control" id="average_eligibility_pg_gre" name="average_eligibility_pg_gre" placeholder="GRE" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                                                    echo $university_overview_info[0]['average_eligibility']['pg_gre'];
                                                                                                                                                                  } else {
                                                                                                                                                                    echo "";
                                                                                                                                                                  } ?>" >
                                        </div>
                                      </div>
                                      <div class="col-md-6 form-group">
                                        <label for="name" class="col-md-4 control-label">UG</label>
                                        <div class="col-md-4">
                                          <input class="form-control" id="average_eligibility_ug_gpa" name="average_eligibility_ug_gpa" placeholder="GPA" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                                                    echo $university_overview_info[0]['average_eligibility']['ug_gpa'];
                                                                                                                                                                  } else {
                                                                                                                                                                    echo "";
                                                                                                                                                                  } ?>" >
                                        </div>
                                        <div class="col-md-4">
                                          <input class="form-control" id="average_eligibility_ug_gre" name="average_eligibility_ug_gre" placeholder="GRE" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                                                    echo $university_overview_info[0]['average_eligibility']['ug_gre'];
                                                                                                                                                                  } else {
                                                                                                                                                                    echo "";
                                                                                                                                                                  } ?>" >
                                        </div>
                                      </div>
                                    </div> -->
                    </div>
                  </div>

                  <button type='button' class="accordion"> General Admission Requirement (Undergraduate) </button>
                  <div class="panel">
                    <div class="col-md-12 form-group">
                      <h4>General Admission Requirement (Undergraduate)</h4>
                      <div class="col-md-12">
                        <label class="control-label"></label>
                        <textarea class="textPara form-control editor" name="general_admission_req_ug" id="general_admission_req_ug"><?php if (isset($university_overview_info[0])) {
                                                                                                                                        echo $university_overview_info[0]['general_admission_req_ug'];
                                                                                                                                      } else {
                                                                                                                                        echo "";
                                                                                                                                      } ?></textarea>
                      </div>


                    </div>
                  </div>

                  <button type='button' class="accordion"> General Admission Requirement (Postgraduate) </button>
                  <div class="panel">
                    <div class="col-md-12 form-group">
                      <h4>General Admission Requirement (Postgraduate)</h4>
                      <div class="col-md-12">
                        <label class="control-label"></label>
                        <textarea class="textPara form-control editor" name="general_admission_req_pg" id="general_admission_req_pg"><?php if (isset($university_overview_info[0])) {
                                                                                                                                        echo $university_overview_info[0]['general_admission_req_pg'];
                                                                                                                                      } else {
                                                                                                                                        echo "";
                                                                                                                                      } ?></textarea>
                      </div>


                    </div>
                  </div>

                  <button type='button' class="accordion">Overview</button>
                  <div class="panel">
                    <div class="col-md-12 form-group">
                      <h4>Overview of <?php echo $university[0]['name']; ?> * (Display Default - Visible All the time)</h4>
                      <div class="col-md-12">
                        <label class="control-label">Overview</label>
                        <textarea class="textPara form-control editor" name="overview_paragraph_one" id="overview_paragraph_one"><?php if (isset($university_overview_info[0])) {
                                                                                                                                    echo $university_overview_info[0]['overview_paragraph_one'];
                                                                                                                                  } else {
                                                                                                                                    echo "";
                                                                                                                                  } ?></textarea>
                      </div>
                      <!-- <div class="col-md-12">
                                      <label class="control-label">Overview Paragraph 2</label>
                                      <textarea class="textPara form-control editor" name="overview_paragraph_two" id="overview_paragraph_two" ><?php if (isset($university_overview_info[0])) {
                                                                                                                                                  echo $university_overview_info[0]['overview_paragraph_two'];
                                                                                                                                                } else {
                                                                                                                                                  echo "";
                                                                                                                                                } ?></textarea>
                                    </div>
                                    <div class="col-md-12">
                                      <label class="control-label">Overview Paragraph 3</label>
                                      <textarea class="textPara form-control editor" name="overview_paragraph_three" id="overview_paragraph_three" ><?php if (isset($university_overview_info[0])) {
                                                                                                                                                      echo $university_overview_info[0]['overview_paragraph_three'];
                                                                                                                                                    } else {
                                                                                                                                                      echo "";
                                                                                                                                                    } ?></textarea>
                                    </div> -->

                    </div>
                  </div>

                  <button type='button' class="accordion">Read More Section</button>
                  <div class="panel">
                    <div class="col-md-12 form-group">
                      <h4>Read More Section * (Visible only on <b>Read More</b> button click)</h4>
                      <div class="col-md-12">
                        <label class="control-label">International Students at <?php echo $university[0]['name']; ?></label>
                        <textarea class="textPara form-control editor" name="read_more_international_students" id="read_more_international_students"> <?php if (isset($university_overview_info[0])) {
                                                                                                                                                        echo $university_overview_info[0]['read_more_international_students'];
                                                                                                                                                      } else {
                                                                                                                                                        echo "";
                                                                                                                                                      } ?></textarea>
                      </div>
                      <!-- <div class="col-md-12">
                                      <label class="control-label">Student Life at <?php echo $university[0]['name']; ?></label>
                                      <textarea class="textPara form-control editor" name="read_more_student_life" id="read_more_student_life" ><?php if (isset($university_overview_info[0])) {
                                                                                                                                                  echo $university_overview_info[0]['read_more_student_life'];
                                                                                                                                                } else {
                                                                                                                                                  echo "";
                                                                                                                                                } ?></textarea>
                                    </div>
                                    <div class="col-md-12">
                                      <label class="control-label">Employment Figures at <?php echo $university[0]['name']; ?></label>
                                      <textarea class="textPara form-control editor" name="read_more_employment_figures" id="read_more_employment_figures" ><?php if (isset($university_overview_info[0])) {
                                                                                                                                                              echo $university_overview_info[0]['read_more_employment_figures'];
                                                                                                                                                            } else {
                                                                                                                                                              echo "";
                                                                                                                                                            } ?></textarea>
                                    </div>
                                    <div class="col-md-12">
                                      <label class="control-label">Colleges/Schools/Popular Programs at <?php echo $university[0]['name']; ?></label>
                                      <textarea class="textPara form-control editor" name="read_more_programs" id="read_more_programs" ><?php if (isset($university_overview_info[0])) {
                                                                                                                                          echo $university_overview_info[0]['read_more_programs'];
                                                                                                                                        } else {
                                                                                                                                          echo "";
                                                                                                                                        } ?></textarea>
                                    </div>
                                    <div class="col-md-12">
                                      <label class="control-label">Research at <?php echo $university[0]['name']; ?></label>
                                      <textarea class="textPara form-control editor" name="read_more_research" id="read_more_research" ><?php if (isset($university_overview_info[0])) {
                                                                                                                                          echo $university_overview_info[0]['read_more_research'];
                                                                                                                                        } else {
                                                                                                                                          echo "";
                                                                                                                                        } ?></textarea>
                                    </div>
                                    <div class="col-md-12">
                                      <label class="control-label">Alumni at <?php echo $university[0]['name']; ?></label>
                                      <textarea class="textPara form-control editor" name="read_more_alumni" id="read_more_alumni" ><?php if (isset($university_overview_info[0])) {
                                                                                                                                      echo $university_overview_info[0]['read_more_alumni'];
                                                                                                                                    } else {
                                                                                                                                      echo "";
                                                                                                                                    } ?></textarea>
                                    </div> -->

                    </div>
                  </div>
                </div>

                <div class="box-footer">
                  <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                  <button type="submit" class="btn btn-info pull-right"> Update </button>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
            <div id="div_tab_two" class="tabcontent">
              <form class="form-horizontal" method="post" onsubmit="updateValue('tab_two', <?php echo $university[0]['id']; ?>)" name="tab_two_update" autocomplete="off">

                <div class="col-md-12">
                  <div class="col-md-12 form-group">
                    <h4><?php echo $university[0]['name']; ?> Programs </h4>
                    <label class="control-label">Paragraph or Programs points </label>
                    <div class="col-sm-12">
                      <textarea class='editor' name="tab_two" id="tab_two">
                          <?php if (isset($university_info[0])) {
                            echo $university_info[0]['tab_two'];
                          } else {
                            echo "";
                          } ?>
                        </textarea>
                    </div>
                  </div>
                </div>

                <!-- <div class="col-md-12 form-group">
                        <h4>Master’s & Bachelor’s Degree Programs offered <?php echo $university[0]['name']; ?></h4>
                        <div class="col-md-12">
                          <label class="control-label">Paragraph 1</label>
                          <textarea class="textPara form-control" name="" id=""></textarea>
                        </div>
                    </div> -->
                <div class="box-footer">
                  <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                  <button type="submit" class="btn btn-info pull-right"> Update </button>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
            <div id="div_tab_three" class="tabcontent">
              <form class="form-horizontal" method="post" onsubmit="updateRanking(<?php echo $university[0]['id']; ?>)" name="tab_three_update" autocomplete="off">
                <!--
                    <div class="form-group">
                      <label for="email" class="col-sm-2 control-label">Content Tab Three *</label>
                      <div class="col-sm-10">
                        <textarea class='editor' name="tab_three" id="tab_three">
                          <?php if (isset($university_info[0])) {
                            echo $university_info[0]['tab_three'];
                          } else {
                            echo "";
                          } ?>
                        </textarea>
                      </div>
                    </div> -->
                <div class="col-md-12">
                  <div class="col-md-12 form-group">
                    <h4><?php echo $university[0]['name']; ?> Ranking</h4>
                    <div class="col-md-12">
                      <label class="control-label">Paragraph or list points</label>
                      <textarea class="editor form-control" name="tab_three" id="tab_three">
                            <?php if (isset($university_info[0])) {
                              echo $university_info[0]['tab_three'];
                            } else {
                              echo "";
                            } ?>
                          </textarea>
                    </div>

                    <div class="col-md-12">
                      <button type='button' class="accordion">Section 1</button>
                      <div class="panel">
                        <div class="col-md-12">
                          <!-- <h4>Section 1 * (Numbers and Counters)</h4> -->
                          <div class="col-md-12 form-group">
                            <label for="name" class="col-md-4 control-label">QS Global</label>
                            <div class="col-md-8">
                              <input class="form-control" id="qs_global_value" name="qs_global_value" placeholder="Enter QS Global Value" value="<?php if (isset($university_ranking)) {
                                                                                                                                                    echo $university_ranking->qs_global_value;
                                                                                                                                                  } else {
                                                                                                                                                    echo "";
                                                                                                                                                  } ?>">
                            </div>
                          </div>

                          <div class="col-md-12 form-group">
                            <label for="name" class="col-md-4 control-label">USNews Global</label>
                            <div class="col-md-8">
                              <input class="form-control" id="usNews_global_value" name="usNews_global_value" placeholder="Enter USNews Global Value" value="<?php if (isset($university_ranking)) {
                                                                                                                                                                echo $university_ranking->usNews_global_value;
                                                                                                                                                              } else {
                                                                                                                                                                echo "";
                                                                                                                                                              } ?>">
                            </div>
                          </div>

                          <div class="col-md-12 form-group">
                            <label for="name" class="col-md-4 control-label">USNews National</label>
                            <div class="col-md-8">
                              <input class="form-control" id="usNews_national_value" name="usNews_national_value" placeholder="Enter USNews National Value" value="<?php if (isset($university_ranking)) {
                                                                                                                                                                      echo $university_ranking->usNews_national_value;
                                                                                                                                                                    } else {
                                                                                                                                                                      echo "";
                                                                                                                                                                    } ?>">
                            </div>
                          </div>

                          <div class="col-md-12 form-group">
                            <label for="name" class="col-md-4 control-label">THE (Times Higher Education)</label>
                            <div class="col-md-8">
                              <input class="form-control" id="the_value" name="the_value" placeholder="Enter THE (Times Higher Education) Value" value="<?php if (isset($university_ranking)) {
                                                                                                                                                          echo $university_ranking->the_value;
                                                                                                                                                        } else {
                                                                                                                                                          echo "";
                                                                                                                                                        } ?>">
                            </div>
                          </div>

                          <div class="col-md-12 form-group">
                            <label for="name" class="col-md-4 control-label">ARWU (Academic Ranking of World Universities)</label>
                            <div class="col-md-8">
                              <input class="form-control" id="arwu_value" name="arwu_value" placeholder="Enter ARWU (Academic Ranking of World Universities) Value" value="<?php if (isset($university_ranking)) {
                                                                                                                                                                              echo $university_ranking->arwu_value;
                                                                                                                                                                            } else {
                                                                                                                                                                              echo "";
                                                                                                                                                                            } ?>">
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>
                    <!-- <div class="col-md-12">
                          <div class="col-md-12">
                            <div class="col-md-12">
                              <h4> University Ranking</h4>
                              <div class="col-md-6 form-group">
                                <label for="name" class="col-md-4 control-label">Ranking 1</label>
                                <div class="col-md-8">
                                  <input class="form-control" id="total_students" name="total_students" placeholder="Enter Number Value" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                                  echo $university_overview_info[0]['total_students'];
                                                                                                                                                } else {
                                                                                                                                                  echo "";
                                                                                                                                                } ?>">
                                </div>
                              </div>
                              <div class="col-md-6 form-group">
                                <label for="name" class="col-md-4 control-label">University Ranking : THE (Time Higher Education)2021</label>
                                <div class="col-md-8">
                                  <input class="form-control" id="international_students" name="international_students" placeholder="Enter Number Value" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                                                  echo $university_overview_info[0]['international_students'];
                                                                                                                                                                } else {
                                                                                                                                                                  echo "";
                                                                                                                                                                } ?>">
                                </div>
                              </div>
                              <div class="col-md-6 form-group">
                                <label for="name" class="col-md-4 control-label">National University Ranking : US News & World Report 2021</label>
                                <div class="col-md-8">
                                  <input class="form-control" id="total_degree_programs" name="total_degree_programs" placeholder="Enter Number Value" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                                                echo $university_overview_info[0]['total_degree_programs'];
                                                                                                                                                              } else {
                                                                                                                                                                echo "";
                                                                                                                                                              } ?>">
                                </div>
                              </div>
                              <div class="col-md-6 form-group">
                                <label for="name" class="col-md-4 control-label">Global Universities : US News & World Report 2021</label>
                                <div class="col-md-8">
                                  <input class="form-control" id="establishment_year" name="establishment_year" placeholder="Enter Number Value" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                                          echo $university_overview_info[0]['establishment_year'];
                                                                                                                                                        } else {
                                                                                                                                                          echo "";
                                                                                                                                                        } ?>">
                                </div>
                              </div>

                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="col-md-12">
                              <h4> Course Ranking</h4>
                              <div class="col-md-6 form-group">
                                <label for="name" class="col-md-4 control-label">University Ranking : ARWU (Shanghai Ranking)2020</label>
                                <div class="col-md-8">
                                  <input class="form-control" id="total_students" name="total_students" placeholder="Enter Number Value" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                                  echo $university_overview_info[0]['total_students'];
                                                                                                                                                } else {
                                                                                                                                                  echo "";
                                                                                                                                                } ?>">
                                </div>
                              </div>
                              <div class="col-md-6 form-group">
                                <label for="name" class="col-md-4 control-label">University Ranking : THE (Time Higher Education)2021</label>
                                <div class="col-md-8">
                                  <input class="form-control" id="international_students" name="international_students" placeholder="Enter Number Value" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                                                  echo $university_overview_info[0]['international_students'];
                                                                                                                                                                } else {
                                                                                                                                                                  echo "";
                                                                                                                                                                } ?>">
                                </div>
                              </div>
                              <div class="col-md-6 form-group">
                                <label for="name" class="col-md-4 control-label">National University Ranking : US News & World Report 2021</label>
                                <div class="col-md-8">
                                  <input class="form-control" id="total_degree_programs" name="total_degree_programs" placeholder="Enter Number Value" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                                                echo $university_overview_info[0]['total_degree_programs'];
                                                                                                                                                              } else {
                                                                                                                                                                echo "";
                                                                                                                                                              } ?>">
                                </div>
                              </div>
                              <div class="col-md-6 form-group">
                                <label for="name" class="col-md-4 control-label">Global Universities : US News & World Report 2021</label>
                                <div class="col-md-8">
                                  <input class="form-control" id="establishment_year" name="establishment_year" placeholder="Enter Number Value" value="<?php if (isset($university_overview_info[0])) {
                                                                                                                                                          echo $university_overview_info[0]['establishment_year'];
                                                                                                                                                        } else {
                                                                                                                                                          echo "";
                                                                                                                                                        } ?>">
                                </div>
                              </div>

                            </div>
                          </div>
                        </div> -->

                  </div>

                </div>
                <div class="box-footer">
                  <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                  <button type="submit" class="btn btn-info pull-right"> Update </button>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
            <div id="div_tab_four" class="tabcontent">
              <form class="form-horizontal" method="post" onsubmit="updateAdmissionInfo(<?php echo $university[0]['id']; ?>)" name="tab_four_update" autocomplete="off">
                <!-- <div class="form-group">
                      <label for="name" class="col-sm-2 control-label">University Name *</label>
                      <div class="col-sm-10">
                        <label for="name" class="col-sm-6 control-label">
                          <?php echo $university[0]['name']; ?>
                        </label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="email" class="col-sm-2 control-label">Content Tab Four *</label>
                      <div class="col-sm-10">
                        <textarea class='editor' name="tab_four" id="tab_four">
                          <?php if (isset($university_info[0])) {
                            echo $university_info[0]['tab_four'];
                          } else {
                            echo "";
                          } ?>
                        </textarea>
                      </div>
                    </div> -->
                <div class="panel panel-primary panelFAQ">
                  <div class="panel-heading">Question One</div>
                  <div class="panel-body">
                    <div class="col-md-12">
                      <label class="control-label">Admissions at <?php echo $university[0]['name']; ?> ?
                      </label>
                      <textarea class="textPara form-control editor" name="admisson_one" id="admisson_one"><?php if (isset($university_admission_info[0])) {
                                                                                                              echo $university_admission_info[0]['a_one'];
                                                                                                            } else {
                                                                                                              echo "";
                                                                                                            } ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="panel panel-primary panelFAQ">
                  <div class="panel-heading">Question Two</div>
                  <div class="panel-body">
                    <div class="col-md-12">
                      <label class="control-label">Application Fees at <?php echo $university[0]['name']; ?> ?
                      </label>
                      <textarea class="textPara form-control editor" name="admisson_two" id="admisson_two"><?php if (isset($university_admission_info[0])) {
                                                                                                              echo $university_admission_info[0]['a_two'];
                                                                                                            } else {
                                                                                                              echo "";
                                                                                                            } ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="panel panel-primary panelFAQ">
                  <div class="panel-heading">Question Three</div>
                  <div class="panel-body">
                    <div class="col-md-12">
                      <label class="control-label">SAT/ACT for admission to <?php echo $university[0]['name']; ?> ?
                      </label>
                      <textarea class="textPara form-control editor" name="admisson_three" id="admisson_three"><?php if (isset($university_admission_info[0])) {
                                                                                                                  echo $university_admission_info[0]['a_three'];
                                                                                                                } else {
                                                                                                                  echo "";
                                                                                                                } ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="panel panel-primary panelFAQ">
                  <div class="panel-heading">Question Four</div>
                  <div class="panel-body">
                    <div class="col-md-12">
                      <label class="control-label">GRE/GMAT for admission to <?php echo $university[0]['name']; ?> ?
                      </label>
                      <textarea class="textPara form-control editor" name="admisson_four" id="admisson_four"><?php if (isset($university_admission_info[0])) {
                                                                                                                echo $university_admission_info[0]['a_four'];
                                                                                                              } else {
                                                                                                                echo "";
                                                                                                              } ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="panel panel-primary panelFAQ">
                  <div class="panel-heading">Question Five</div>
                  <div class="panel-body">
                    <div class="col-md-12">
                      <label class="control-label">Proof of English Language Proficiency for Admission to <?php echo $university[0]['name']; ?> ?
                      </label>
                      <textarea class="textPara form-control editor" name="admisson_five" id="admisson_five"><?php if (isset($university_admission_info[0])) {
                                                                                                                echo $university_admission_info[0]['a_five'];
                                                                                                              } else {
                                                                                                                echo "";
                                                                                                              } ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="panel panel-primary panelFAQ">
                  <div class="panel-heading">Question Five-a</div>
                  <div class="panel-body">
                    <div class="col-md-12">
                      <label class="control-label">TOEFL (TEST OF ENGLISH AS A FOREIGN LANGUAGE) Score required at <?php echo $university[0]['name']; ?> ?
                      </label>
                      <textarea class="textPara form-control editor" name="admisson_five_a" id="admisson_five_a"><?php if (isset($university_admission_info[0])) {
                                                                                                                    echo $university_admission_info[0]['a_five_a'];
                                                                                                                  } else {
                                                                                                                    echo "";
                                                                                                                  } ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="panel panel-primary panelFAQ">
                  <div class="panel-heading">Question Five-b</div>
                  <div class="panel-body">
                    <div class="col-md-12">
                      <label class="control-label">IELTS (INTERNATIONAL ENGLISH LANGUAGE TESTING SERVICE) Score required at <?php echo $university[0]['name']; ?> ?
                      </label>
                      <textarea class="textPara form-control editor" name="admisson_five_b" id="admisson_five_b"><?php if (isset($university_admission_info[0])) {
                                                                                                                    echo $university_admission_info[0]['a_five_b'];
                                                                                                                  } else {
                                                                                                                    echo "";
                                                                                                                  } ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="panel panel-primary panelFAQ">
                  <div class="panel-heading">Question Five-c</div>
                  <div class="panel-body">
                    <div class="col-md-12">
                      <label class="control-label"> DUOLINGO ENGLISH TEST Score required at <?php echo $university[0]['name']; ?> ?
                      </label>
                      <textarea class="textPara form-control editor" name="admisson_five_c" id="admisson_five_c"><?php if (isset($university_admission_info[0])) {
                                                                                                                    echo $university_admission_info[0]['a_five_c'];
                                                                                                                  } else {
                                                                                                                    echo "";
                                                                                                                  } ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="panel panel-primary panelFAQ">
                  <div class="panel-heading">Question Five-d</div>
                  <div class="panel-body">
                    <div class="col-md-12">
                      <label class="control-label">Pearson PTE Score required at <?php echo $university[0]['name']; ?> ?
                      </label>

                      <textarea class="textPara form-control editor" name="admisson_five_d" id="admisson_five_d"><?php if (isset($university_admission_info[0])) {
                                                                                                                    echo $university_admission_info[0]['a_five_d'];
                                                                                                                  } else {
                                                                                                                    echo "";
                                                                                                                  } ?></textarea>

                    </div>
                  </div>
                </div>
                <div class="panel panel-primary panelFAQ">
                  <div class="panel-heading">Question Six</div>
                  <div class="panel-body">
                    <div class="col-md-12">
                      <label class="control-label">Application Requirements at <?php echo $university[0]['name']; ?> ?
                      </label>
                      <textarea class="textPara form-control editor" name="admisson_six" id="admisson_six"><?php if (isset($university_admission_info[0])) {
                                                                                                              echo $university_admission_info[0]['a_six'];
                                                                                                            } else {
                                                                                                              echo "";
                                                                                                            } ?></textarea>
                    </div>
                  </div>
                </div>

                <div class="panel panel-primary panelFAQ">
                  <div class="panel-heading">Question Six-b</div>
                  <div class="panel-body">
                    <div class="col-md-12">
                      <label class="control-label">What does a typical application to <?php echo $university[0]['name']; ?> contain?
                      </label>
                      <textarea class="textPara form-control editor" name="admisson_six_a" id="admisson_six_a"><?php if (isset($university_admission_info[0])) {
                                                                                                                  echo $university_admission_info[0]['a_six_a'];
                                                                                                                } else {
                                                                                                                  echo "";
                                                                                                                } ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="panel panel-primary panelFAQ">
                  <div class="panel-heading">Question Seven</div>
                  <div class="panel-body">
                    <div class="col-md-12">
                      <label class="control-label">Admission Timeline and Deadlines for <?php echo $university[0]['name']; ?> ?
                      </label>
                      <textarea class="textPara form-control editor" name="admisson_seven" id="admisson_seven"><?php if (isset($university_admission_info[0])) {
                                                                                                                  echo $university_admission_info[0]['a_seven'];
                                                                                                                } else {
                                                                                                                  echo "";
                                                                                                                } ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="panel panel-primary panelFAQ">
                  <div class="panel-heading">Question Eight</div>
                  <div class="panel-body">
                    <div class="col-md-12">
                      <label class="control-label">How to apply, i.e. the Application Process and Financial Assistance for <?php echo $university[0]['name']; ?> ?
                      </label>
                      <textarea class="textPara form-control editor" name="admisson_eight" id="admisson_eight"><?php if (isset($university_admission_info[0])) {
                                                                                                                  echo $university_admission_info[0]['a_eight'];
                                                                                                                } else {
                                                                                                                  echo "";
                                                                                                                } ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="panel panel-primary panelFAQ">
                  <div class="panel-heading">Question Nine</div>
                  <div class="panel-body">
                    <div class="col-md-12">
                      <label class="control-label">I-20 for International Students for admission to <?php echo $university[0]['name']; ?> ?
                      </label>
                      <textarea class="textPara form-control editor" name="admisson_nine" id="admisson_nine"><?php if (isset($university_admission_info[0])) {
                                                                                                                echo $university_admission_info[0]['a_nine'];
                                                                                                              } else {
                                                                                                                echo "";
                                                                                                              } ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="panel panel-primary panelFAQ">
                  <div class="panel-heading">Question Ten</div>
                  <div class="panel-body">
                    <div class="col-md-12">
                      <label class="control-label"><?php echo $university[0]['name']; ?> Tuition Fees for International Students ?
                      </label>
                      <textarea class="textPara form-control editor" name="admisson_ten" id="admisson_ten"><?php if (isset($university_admission_info[0])) {
                                                                                                              echo $university_admission_info[0]['a_ten'];
                                                                                                            } else {
                                                                                                              echo "";
                                                                                                            } ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="box-footer">
                  <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                  <button type="submit" class="btn btn-info pull-right"> Update </button>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
            <div id="div_tab_five" class="tabcontent">
              <form class="form-horizontal" method="post" onsubmit="updateScholarshipsFinancial(<?php echo $university[0]['id']; ?>)" name="tab_five_update" autocomplete="off">
                <!-- <div class="form-group">
                      <label for="name" class="col-sm-2 control-label">University Name *</label>
                      <div class="col-sm-10">
                        <label for="name" class="col-sm-6 control-label">
                          <?php echo $university[0]['name']; ?>
                        </label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="email" class="col-sm-2 control-label">Content Tab Five *</label>
                      <div class="col-sm-10">
                        <textarea class='editor' name="tab_five" id="tab_five">
                          <?php if (isset($university_info[0])) {
                            echo $university_info[0]['tab_five'];
                          } else {
                            echo "";
                          } ?>
                        </textarea>
                      </div>
                    </div> -->
                <div class="col-md-12 form-group">
                  <h4>Scholarships & Financial Aid Tab</h4>
                  <div class="col-md-12">
                    <label class="control-label"><?php echo $university[0]['name']; ?> Financial Assistance to International Students
                    </label>
                    <textarea class="textPara form-control editor" name="financial_assistance" id="financial_assistance"><?php if (isset($university_scholarships_financial_info[0])) {
                                                                                                                            echo $university_scholarships_financial_info[0]['financial_assistance'];
                                                                                                                          } else {
                                                                                                                            echo "";
                                                                                                                          } ?></textarea>
                  </div>
                  <div class="col-md-12">
                    <label class="control-label">Financial Aid for <?php echo $university[0]['name']; ?>
                    </label>
                    <textarea class="textPara form-control editor" name="financial_aid" id="financial_aid"><?php if (isset($university_scholarships_financial_info[0])) {
                                                                                                              echo $university_scholarships_financial_info[0]['financial_aid'];
                                                                                                            } else {
                                                                                                              echo "";
                                                                                                            } ?></textarea>
                  </div>
                  <div class="col-md-12">
                    <label class="control-label">Accommodations offered for International Students at <?php echo $university[0]['name']; ?>
                    </label>
                    <textarea class="textPara form-control editor" name="accommodations_offered" id="accommodations_offered"><?php if (isset($university_scholarships_financial_info[0])) {
                                                                                                                                echo $university_scholarships_financial_info[0]['accommodations_offered'];
                                                                                                                              } else {
                                                                                                                                echo "";
                                                                                                                              } ?></textarea>
                  </div>
                  <div class="col-md-12">
                    <label class="control-label">On-Campus Accommodation at <?php echo $university[0]['name']; ?>
                    </label>
                    <textarea class="textPara form-control editor" name="on_campus" id="on_campus"><?php if (isset($university_scholarships_financial_info[0])) {
                                                                                                      echo $university_scholarships_financial_info[0]['on_campus'];
                                                                                                    } else {
                                                                                                      echo "";
                                                                                                    } ?></textarea>
                  </div>
                  <div class="col-md-12">
                    <label class="control-label">Off-Campus Accommodation <?php echo $university[0]['name']; ?>
                    </label>
                    <textarea class="textPara form-control editor" name="off_campus" id="off_campus"><?php if (isset($university_scholarships_financial_info[0])) {
                                                                                                        echo $university_scholarships_financial_info[0]['off_campus'];
                                                                                                      } else {
                                                                                                        echo "";
                                                                                                      } ?></textarea>
                  </div>

                </div>
                <div class="box-footer">
                  <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                  <button type="submit" class="btn btn-info pull-right"> Update </button>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
            <div id="div_tab_six" class="tabcontent">
              <form class="form-horizontal" method="post" onsubmit="updateFaqInfo(<?php echo $university[0]['id']; ?>)" name="tab_six_update" autocomplete="off">
                <div class="col-md-12 form-group">
                  <h4>FAQ Tab</h4>

                  <div class="col-md-12">


                    <div class="form-group">

                      <div class="col-md-12">
                        <?php
                        if (isset($faqQusAns)) {
                          if ($faqQusAns) {

                            foreach ($faqQusAns as $index => $faqQusAnsData) {
                        ?>

                              <div class="col-md-12" id="no_of_faq_<?= ($index + 1) ?>">
                                <div class="col-md-5 form-group">
                                  <label for="name" class="col-md-4 control-label">Questions <?= ($index + 1) ?></label>
                                  <div class="col-md-8">
                                    <textarea class="form-control" name="question[]" id="question_<?= ($index + 1) ?>" placeholder="Enter Question Here" rows="4"><?= $faqQusAnsData['question'] ?></textarea>
                                  </div>
                                </div>
                                <div class="col-md-5 form-group">
                                  <div class="col-md-8">
                                    <textarea class="form-control" name="answer[]" id="answer_<?= ($index + 1) ?>" placeholder="Enter Answer Here" rows="4"><?= $faqQusAnsData['answer'] ?></textarea>
                                  </div>
                                </div>

                                <?php
                                if (!$index) {
                                ?>
                                  <div class="col-md-2 form-group">
                                    <input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addFaq()">
                                  </div>
                                <?php
                                } else {
                                ?>
                                  <div class="col-md-2 form-group">
                                    <input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteFaq(<?= ($index + 1) ?>)">
                                  </div>
                                <?php
                                }
                                ?>

                              </div>
                            <?php
                            }
                          } else { ?>
                            <div class="col-md-12" id="no_of_faq_1">
                              <div class="col-md-5 form-group">
                                <label for="name" class="col-md-4 control-label">Questions <?= ($index + 1) ?></label>
                                <div class="col-md-8">
                                  <textarea class="form-control" name="question[]" id="question_<?= ($index + 1) ?>" placeholder="Enter Question Here" rows="4"></textarea>
                                </div>
                              </div>
                              <div class="col-md-5 form-group">
                                <div class="col-md-8">
                                  <textarea class="form-control" name="answer[]" id="answer_<?= ($index + 1) ?>" placeholder="Enter Answer Here" rows="4"></textarea>
                                </div>
                              </div>
                              <div class="col-md-2 form-group">
                                <input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addFaq()">
                              </div>
                            </div>
                          <?php     }
                        } else {
                          ?>
                          <div class="col-md-12" id="no_of_faq_1">
                            <div class="col-md-5 form-group">
                              <label for="name" class="col-md-4 control-label">Questions <?= ($index + 1) ?></label>
                              <div class="col-md-8">
                                <textarea class="form-control" name="question[]" id="question_<?= ($index + 1) ?>" placeholder="Enter Question Here" rows="4"></textarea>
                              </div>
                            </div>
                            <div class="col-md-5 form-group">
                              <div class="col-md-8">
                                <textarea class="form-control" name="answer[]" id="answer_<?= ($index + 1) ?>" placeholder="Enter Answer Here" rows="4"></textarea>
                              </div>
                            </div>
                            <div class="col-md-2 form-group">
                              <input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addFaq()">
                            </div>
                          </div>
                        <?php
                        }
                        ?>
                      </div>
                    </div>



                  </div>

                  <!-- <div class="panel panel-primary panelFAQ">
                        <div class="panel-heading">Write FAQ</div>
                        <div class="panel-body">

                          <div class="col-md-12">
                            <label class="control-label">
                            </label>
                            <textarea class="textPara form-control editor" name="a_one" id="a_one"><?php if (isset($university_faq_info[0])) {
                                                                                                      echo $university_faq_info[0]['a_one'];
                                                                                                    } else {
                                                                                                      echo "";
                                                                                                    } ?></textarea>
                          </div>
                        </div>
                      </div> -->
                  <!--
                      <div class="panel panel-primary panelFAQ">
                            <div class="panel-heading">Question Three</div>
                            <div class="panel-body">
                          <div class="col-md-12">
                            <label class="control-label">What is the application deadline for <?php echo $university[0]['name']; ?> ?
                            </label>
                            <textarea class="textPara form-control editor" name="a_two" id="a_two"><?php if (isset($university_faq_info[0])) {
                                                                                                      echo $university_faq_info[0]['a_two'];
                                                                                                    } else {
                                                                                                      echo "";
                                                                                                    } ?></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-primary panelFAQ">
                        <div class="panel-heading">Question Three</div>
                        <div class="panel-body">

                          <div class="col-md-12">
                            <label class="control-label">What are the entry or eligibility criteria at <?php echo $university[0]['name']; ?> ?
                            </label>
                            <textarea class="textPara form-control editor" name="a_three" id="a_three"><?php if (isset($university_faq_info[0])) {
                                                                                                          echo $university_faq_info[0]['a_three'];
                                                                                                        } else {
                                                                                                          echo "";
                                                                                                        } ?></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-primary panelFAQ">
                        <div class="panel-heading">Question Four</div>
                        <div class="panel-body">

                          <div class="col-md-12">
                            <label class="control-label">What documentation is required for applying to <?php echo $university[0]['name']; ?> ?
                            </label>
                            <textarea class="textPara form-control editor" name="a_four" id="a_four"><?php if (isset($university_faq_info[0])) {
                                                                                                        echo $university_faq_info[0]['a_four'];
                                                                                                      } else {
                                                                                                        echo "";
                                                                                                      } ?></textarea>
                          </div>
                        </div>
                      </div>

                      <div class="panel panel-primary panelFAQ">
                        <div class="panel-heading">Question Five</div>
                        <div class="panel-body">

                          <div class="col-md-12">
                            <label class="control-label">How diverse is the student body at <?php echo $university[0]['name']; ?> ?
                            </label>
                            <textarea class="textPara form-control editor" name="a_five" id="a_five"><?php if (isset($university_faq_info[0])) {
                                                                                                        echo $university_faq_info[0]['a_five'];
                                                                                                      } else {
                                                                                                        echo "";
                                                                                                      } ?></textarea>
                          </div>
                        </div>
                      </div>

                      <div class="panel panel-primary panelFAQ">
                        <div class="panel-heading">Question Six</div>
                        <div class="panel-body">


                          <div class="col-md-12">
                            <label class="control-label">What is the acceptance rate at <?php echo $university[0]['name']; ?> ?
                            </label>
                            <textarea class="textPara form-control editor" name="a_six" id="a_six"><?php if (isset($university_faq_info[0])) {
                                                                                                      echo $university_faq_info[0]['a_six'];
                                                                                                    } else {
                                                                                                      echo "";
                                                                                                    } ?></textarea>
                          </div>
                        </div>
                      </div>

                      <div class="panel panel-primary panelFAQ">
                        <div class="panel-heading">Question Seven</div>
                        <div class="panel-body">


                          <div class="col-md-12">
                            <label class="control-label">What is the Ranking for <?php echo $university[0]['name']; ?> ?
                            </label>
                            <textarea class="textPara form-control editor" name="a_seven" id="a_seven"><?php if (isset($university_faq_info[0])) {
                                                                                                          echo $university_faq_info[0]['a_seven'];
                                                                                                        } else {
                                                                                                          echo "";
                                                                                                        } ?></textarea>
                          </div>
                        </div>
                      </div>

                      <div class="panel panel-primary panelFAQ">
                        <div class="panel-heading">Question Eight</div>
                        <div class="panel-body">
                          <div class="col-md-12">
                            <label class="control-label">Is <?php echo $university[0]['name']; ?> a good University ?
                            </label>
                            <textarea class="textPara form-control editor" name="a_eight" id="a_eight"><?php if (isset($university_faq_info[0])) {
                                                                                                          echo $university_faq_info[0]['a_eight'];
                                                                                                        } else {
                                                                                                          echo "";
                                                                                                        } ?></textarea>
                          </div>
                        </div>
                    </div>

                      <div class="panel panel-primary panelFAQ">
                        <div class="panel-heading">Question Eight</div>
                        <div class="panel-body">


                          <div class="col-md-12">
                            <label class="control-label">What is the application fee at <?php echo $university[0]['name']; ?> ?
                            </label>
                            <textarea class="textPara form-control editor" name="a_eight" id="a_eight"><?php if (isset($university_faq_info[0])) {
                                                                                                          echo $university_faq_info[0]['a_eight'];
                                                                                                        } else {
                                                                                                          echo "";
                                                                                                        } ?></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-primary panelFAQ">
                        <div class="panel-heading">Question Nine</div>
                        <div class="panel-body">


                          <div class="col-md-12">
                            <label class="control-label">What is the Average Salary and Job Placement Rate for <?php echo $university[0]['name']; ?> Graduates ?
                            </label>
                            <textarea class="textPara form-control editor" name="a_nine" id="a_nine"><?php if (isset($university_faq_info[0])) {
                                                                                                        echo $university_faq_info[0]['a_nine'];
                                                                                                      } else {
                                                                                                        echo "";
                                                                                                      } ?></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-primary panelFAQ">
                        <div class="panel-heading">Question Ten</div>
                        <div class="panel-body">

                          <div class="col-md-12">
                            <label class="control-label">How can I finance my <?php echo $university[0]['name']; ?> education ?
                            </label>
                            <textarea class="textPara form-control editor" name="a_ten" id="a_ten"><?php if (isset($university_faq_info[0])) {
                                                                                                      echo $university_faq_info[0]['a_ten'];
                                                                                                    } else {
                                                                                                      echo "";
                                                                                                    } ?></textarea>
                          </div>
                        </div>
                      </div>-->

                </div>
                <div class="box-footer">
                  <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                  <button type="submit" class="btn btn-info pull-right"> Update </button>
                  <input type="hidden" name="university_id" value="<?php echo $university[0]['id']; ?>">
                </div>
                <!-- /.box-footer -->
              </form>
            </div>



          </div>
          <!-- /.box -->
          <!-- general form elements disabled -->

          <div id="div_tab_eleven" class="tabcontent">
            <form class="form-horizontal" method="post" onsubmit="updateLatestInfo(<?php echo $university[0]['id']; ?>)" name="tab_eleven_update" autocomplete="off">
              <div class="col-md-12 form-group">
                <h4>Latest News Tab</h4>

                <div class="col-md-12">


                  <div class="form-group">

                    <div class="col-md-12">
                      <?php
                      if (isset($latestNews)) {
                        if ($latestNews) {

                          foreach ($latestNews as $index => $latestNewsData) {
                      ?>

                            <div class="col-md-12" id="no_of_news_<?= ($index + 1) ?>">
                              <div class="col-md-3 form-group">
                                <label for="name" class="col-md-4 control-label">Questions <?= ($index + 1) ?></label>
                                <div class="col-md-8">
                                  <textarea class="form-control" name="questionlet[]" id="questionlet_<?= ($index + 1) ?>" placeholder="Enter Question Here" rows="4"><?= $latestNewsData['question'] ?></textarea>
                                </div>
                              </div>
                              <div class="col-md-4 form-group">
                                <div class="col-md-12">
                                  <textarea class="editor" name="answerlet[]" id="answerlet_<?= ($index + 1) ?>" placeholder="Enter Answer Here" rows="4"><?= $latestNewsData['answer'] ?></textarea>
                                </div>
                              </div>
                              <div class="col-md-2 form-group">
                                <div class="col-md-12">
                                  <textarea class="form-control" name="origineDate[]" id="origineDate_<?= ($index + 1) ?>" placeholder="Enter Answer Here" rows="4"><?= $latestNewsData['origineDate'] ?></textarea>
                                </div>
                              </div>
                              <div class="col-md-2 form-group">
                                <div class="col-md-12">
                                  <textarea class="form-control" name="archive[]" id="archive_<?= ($index + 1) ?>" placeholder="Enter Archive Here" rows="4"><?= $latestNewsData['archive'] ?></textarea>
                                </div>
                              </div>

                              <?php
                              if (!$index) {
                              ?>
                                <div class="col-md-1 form-group">
                                  <input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addLatestNews()">
                                </div>
                              <?php
                              } else {
                              ?>
                                <div class="col-md-1 form-group">
                                  <input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteLatestNews(<?= ($index + 1) ?>)">
                                </div>
                              <?php
                              }
                              ?>

                            </div>
                          <?php
                          }
                        } else { ?>
                          <div class="col-md-12" id="no_of_news_1">
                            <div class="col-md-3 form-group">
                              <label for="name" class="col-md-4 control-label">Questions <?= ($index + 1) ?></label>
                              <div class="col-md-12">
                                <textarea class="form-control" name="questionlet[]" id="questionlet_<?= ($index + 1) ?>" placeholder="Enter Question Here" rows="4"></textarea>
                              </div>
                            </div>
                            <div class="col-md-4 form-group">
                              <div class="col-md-12">
                                <textarea class="editor" name="answerlet[]" id="answerlet_<?= ($index + 1) ?>" placeholder="Enter Answer Here" rows="4"></textarea>
                              </div>
                            </div>
                            <div class="col-md-2 form-group">
                              <div class="col-md-12">
                                <textarea class="form-control" name="origineDate[]" id="origineDate_<?= ($index + 1) ?>" placeholder="Enter Answer Here" rows="4"></textarea>
                              </div>
                            </div>
                            <div class="col-md-2 form-group">
                              <div class="col-md-12">
                                <textarea class="form-control" name="archive[]" id="archive_<?= ($index + 1) ?>" placeholder="Enter Archive Here" rows="4"></textarea>
                              </div>
                            </div>
                            <div class="col-md-1 form-group">
                              <input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addLatestNews()">
                            </div>
                          </div>
                        <?php     }
                      } else {
                        ?>
                        <div class="col-md-12" id="no_of_news_1">
                          <div class="col-md-3 form-group">
                            <label for="name" class="col-md-4 control-label">Questions <?= ($index + 1) ?></label>
                            <div class="col-md-8">
                              <textarea class="form-control" name="questionlet[]" id="questionlet_<?= ($index + 1) ?>" placeholder="Enter Question Here" rows="4"></textarea>
                            </div>
                          </div>
                          <div class="col-md-4 form-group">
                            <div class="col-md-12">
                              <textarea class="editor" name="answerlet[]" id="answerlet_<?= ($index + 1) ?>" placeholder="Enter Answer Here" rows="4"></textarea>
                            </div>
                          </div>
                          <div class="col-md-2 form-group">
                            <div class="col-md-12">
                              <textarea class="form-control" name="origineDate[]" id="origineDate_<?= ($index + 1) ?>" placeholder="Enter Origine Date Here" rows="4"></textarea>
                            </div>
                          </div>
                          <div class="col-md-2 form-group">
                            <div class="col-md-12">
                              <textarea class="form-control" name="archive[]" id="archive_<?= ($index + 1) ?>" placeholder="Enter Archive Here" rows="4"></textarea>
                            </div>
                          </div>
                          <div class="col-md-1 form-group">
                            <input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addLatestNews()">
                          </div>
                        </div>
                      <?php
                      }
                      ?>
                    </div>
                  </div>



                </div>

              </div>
              <div class="box-footer">
                <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                <button type="submit" class="btn btn-info pull-right"> Update </button>
                <input type="hidden" name="university_id" value="<?php echo $university[0]['id']; ?>">
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

          <div id="div_tab_seven" class="tabcontent">
            <form class="form-horizontal" method="post" onsubmit="updateSeoInfo(<?php echo $university[0]['id']; ?>)" name="tab_seven_update" autocomplete="off">

              <div class="col-md-12">

                <div class="col-md-6 form-group">
                  <label for="name" class="col-md-4 control-label">SEO Title</label>
                  <div class="col-md-8">
                    <textarea class="form-control" id="seo_title" name="seo_title" placeholder="Enter Seo Title" rows="4"><?php if (isset($university_seo_info[0])) {
                                                                                                                            echo $university_seo_info[0]['seo_title'];
                                                                                                                          } else {
                                                                                                                            echo "";
                                                                                                                          } ?></textarea>
                  </div>
                </div>

                <div class="col-md-6 form-group">
                  <label for="name" class="col-md-4 control-label">SEO Description</label>
                  <div class="col-md-8">
                    <textarea class="form-control" id="seo_description" name="seo_description" placeholder="Enter Seo Description" rows="4"><?php if (isset($university_seo_info[0])) {
                                                                                                                                              echo $university_seo_info[0]['seo_description'];
                                                                                                                                            } else {
                                                                                                                                              echo "";
                                                                                                                                            } ?></textarea>
                  </div>
                </div>

                <div class="col-md-6 form-group">
                  <label for="name" class="col-md-4 control-label">SEO Canonical</label>
                  <div class="col-md-8">
                    <textarea class="form-control" id="seo_canonical" name="seo_canonical" placeholder="Enter Seo canonical" rows="4"><?php if (isset($university_seo_info[0])) {
                                                                                                                                        echo $university_seo_info[0]['seo_canonical'];
                                                                                                                                      } else {
                                                                                                                                        echo "";
                                                                                                                                      } ?></textarea>
                  </div>
                </div>

                <div class="col-md-6 form-group">
                  <label for="name" class="col-md-4 control-label">SEO Index Option</label>
                  <div class="col-md-8">
                    <textarea class="form-control" id="seo_index_option" name="seo_index_option" placeholder="Enter Seo Index Option" rows="4"><?php if (isset($university_seo_info[0])) {
                                                                                                                                                  echo $university_seo_info[0]['seo_index_option'];
                                                                                                                                                } else {
                                                                                                                                                  echo "";
                                                                                                                                                } ?></textarea>
                  </div>
                </div>

                <div class="col-md-6 form-group">
                  <label for="name" class="col-md-4 control-label">SEO Schema</label>
                  <div class="col-md-8">
                    <textarea class="form-control" id="seo_schema" name="seo_schema" placeholder="Enter Seo Schema" rows="4"><?php if (isset($university_seo_info[0])) {
                                                                                                                                echo $university_seo_info[0]['seo_schema'];
                                                                                                                              } else {
                                                                                                                                echo "";
                                                                                                                              } ?></textarea>
                  </div>
                </div>


              </div>

              <div class="box-footer">
                <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                <button type="submit" class="btn btn-info pull-right"> Update </button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>


          <div id="div_tab_eight" class="tabcontent">
            <form class="form-horizontal" method="post" onsubmit="updateUniversityImagesInfo(<?php echo $university[0]['id']; ?>)" name="tab_eight_update" autocomplete="off">

              <div class="col-md-12">

                <div class="col-md-6 form-group">
                  <label for="name" class="col-md-4 control-label">Banner Image</label>
                  <div class="col-md-8">
                    <input type="file" name="banner" id="banner">
                  </div>
                </div>

                <div class="col-md-6 form-group">
                  <label for="name" class="col-md-4 control-label">Logo Image</label>
                  <div class="col-md-8">
                    <input type="file" name="logo" id="logo">
                  </div>
                </div>
              </div>

              <div class="box-footer">
                <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                <button type="submit" class="btn btn-info pull-right"> Update </button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>


          <div id="div_tab_nine" class="tabcontent">
            <form class="form-horizontal" method="post" action="<?php echo base_url(); ?>university/info/update/gallaryinfo" name="tab_nine_update">

              <div class="col-md-12">

                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Gallery Images</label>
                  <div class="col-sm-10">
                    <?php
                    if (isset($enrollment)) {
                      foreach ($enrollment as $index => $enrollmentData) {
                    ?>
                        <div id="no_of_gallery_<?= ($index + 1) ?>" <?= $index ? 'style="margin-top:10px;"' : '' ?>>
                          <div style="float:left; padding-right: 500px;" class="col-sm-3">
                            <label for="enrollment_year" class="col-sm-2" style="padding-right: 350px;">Image</label>
                            <input type="file" name="gallery[]" id="gallery_<?= ($index + 1) ?>">

                          </div>
                          <?php
                          if (!$index) {
                          ?>
                            <div><input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addEnrollment()"></div>
                          <?php
                          } else {
                          ?>
                            <div><input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteEnrollment(<?= ($index + 1) ?>)"></div>
                          <?php
                          }
                          ?>

                        </div>
                      <?php
                      }
                    } else {
                      ?>
                      <div id="no_of_gallery_1">
                        <div style="float:left; padding-right: 500px;" class="col-sm-3">
                          <label for="enrollment_year" class="col-sm-2" style="padding-left: 0px;">Image</label>
                          <input type="file" name="gallery[]" id="gallery_<?= ($index + 1) ?>">

                        </div>
                        <div><input type="button" name="add" value="Add" style="margin-right: 350px;" onclick="addEnrollment()"></div>
                      </div>
                    <?php
                    }
                    ?>
                  </div>
                </div>



              </div>

              <div class="box-footer">
                <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                <button type="submit" class="btn btn-info pull-right"> Update </button>
                <input type="hidden" name="university_id" value="<?php echo $university[0]['id']; ?>">
              </div>
              <!-- /.box-footer -->
            </form>
          </div>



          <div id="div_tab_ten" class="tabcontent">
            <form class="form-horizontal" method="post" onsubmit="updateSocialMediaInfo(<?php echo $university[0]['id']; ?>)" name="tab_ten_update" autocomplete="off">

              <div class="col-md-12">

                <div class="col-md-6 form-group">
                  <label for="name" class="col-md-4 control-label">Campus Video Link</label>
                  <div class="col-md-8">
                    <input class="form-control" id="campus_youtube" name="campus_youtube" placeholder="Enter Campus Video Link" value="<?php if (isset($university_social_media_info[0])) {
                                                                                                                                          echo $university_social_media_info[0]['campus_youtube'];
                                                                                                                                        } else {
                                                                                                                                          echo "";
                                                                                                                                        } ?>" />
                  </div>
                </div>

                <div class="col-md-6 form-group">
                  <label for="name" class="col-md-4 control-label">Facebook Link</label>
                  <div class="col-md-8">
                    <input class="form-control" id="smm_facebook" name="smm_facebook" placeholder="Enter Facebook Link" value="<?php if (isset($university_social_media_info[0])) {
                                                                                                                                  echo $university_social_media_info[0]['smm_facebook'];
                                                                                                                                } else {
                                                                                                                                  echo "";
                                                                                                                                } ?>" />
                  </div>
                </div>

                <div class="col-md-6 form-group">
                  <label for="name" class="col-md-4 control-label">Instagram Link</label>
                  <div class="col-md-8">
                    <input class="form-control" id="smm_instagram" name="smm_instagram" placeholder="Enter Instagram Link" value="<?php if (isset($university_social_media_info[0])) {
                                                                                                                                    echo $university_social_media_info[0]['smm_instagram'];
                                                                                                                                  } else {
                                                                                                                                    echo "";
                                                                                                                                  } ?>" />
                  </div>
                </div>

                <div class="col-md-6 form-group">
                  <label for="name" class="col-md-4 control-label">Youtube Link</label>
                  <div class="col-md-8">
                    <input class="form-control" id="smm_youtube" name="smm_youtube" placeholder="Enter Youtube Link" value="<?php if (isset($university_social_media_info[0])) {
                                                                                                                              echo $university_social_media_info[0]['smm_youtube'];
                                                                                                                            } else {
                                                                                                                              echo "";
                                                                                                                            } ?>" />
                  </div>
                </div>

                <div class="col-md-6 form-group">
                  <label for="name" class="col-md-4 control-label">Whatsapp Link</label>
                  <div class="col-md-8">
                    <input class="form-control" id="smm_whats" name="smm_whats" placeholder="Enter Whatsapp Link" value="<?php if (isset($university_social_media_info[0])) {
                                                                                                                            echo $university_social_media_info[0]['smm_whats'];
                                                                                                                          } else {
                                                                                                                            echo "";
                                                                                                                          } ?>" />
                  </div>
                </div>

                <div class="col-md-6 form-group">
                  <label for="name" class="col-md-4 control-label">Twitter Link</label>
                  <div class="col-md-8">
                    <input class="form-control" id="smm_twitter" name="smm_twitter" placeholder="Enter Twitter Link" value="<?php if (isset($university_social_media_info[0])) {
                                                                                                                              echo $university_social_media_info[0]['smm_twitter'];
                                                                                                                            } else {
                                                                                                                              echo "";
                                                                                                                            } ?>" />
                  </div>
                </div>

                <div class="col-md-6 form-group">
                  <label for="name" class="col-md-4 control-label">Linkedin Link</label>
                  <div class="col-md-8">
                    <input class="form-control" id="smm_linkedin" name="smm_linkedin" placeholder="Enter Linkedin Link" value="<?php if (isset($university_social_media_info[0])) {
                                                                                                                                  echo $university_social_media_info[0]['smm_linkedin'];
                                                                                                                                } else {
                                                                                                                                  echo "";
                                                                                                                                } ?>" />
                  </div>
                </div>


              </div>

              <div class="box-footer">
                <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                <button type="submit" class="btn btn-info pull-right"> Update </button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
  tinymce.init({
    selector: '.editor',
    setup: function(editor) {
      editor.on('change', function() {
        tinymce.triggerSave();
      });
    },
    theme: 'silver',
    height: 400,
    plugins: [
      ' advlist ',
      ' autolink ',
      ' charmap ',
      ' preview ',
      ' anchor ',
      ' link',
      ' image',
      ' lists ',
      ' code ',
      ' visualblocks ',
      ' table',
      ' searchreplace '
    ],
    toolbar: 'undo redo | blocks fontsize  | bold italic underline forecolor backcolor | alignleft aligncenter alignright alignjustify outdent indent | table tabledelete | numlist bullist | link image | removeformat | help',
    content_style: 'p { margin: 15px; color: #4a4949; font-size: 12pt; }' + ' ul { color: #4a4949; font-size: 12pt; }' + ' ol { color: #4a4949; font-size: 12pt; } ',
    fontsize_formats: "8pt 9pt 10pt 11pt 12pt 14pt 18pt 24pt 30pt 36pt 48pt 60pt 72pt 96pt"
  });

  tinymce.init({
    selector: '.imageEditor',
    setup: function(imageEditor) {
      imageEditor.on('change', function() {
        tinymce.triggerSave();
      });
    },
    theme: 'silver',
    height: 400,
    plugins: [
      ' advlist ',
      ' autolink ',
      ' charmap ',
      ' preview ',
      ' anchor ',
      ' link',
      ' image',
      ' code ',
      ' media '
    ],
    toolbar: 'undo redo | alignleft aligncenter alignright alignjustify outdent indent | link image media | help',
    media_alt_source: false
  });
</script>
<script>
  function tabChange(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
  }
  // Get the element with id="defaultOpen" and click on it
  document.getElementById("defaultOpen").click();

  function updateValue(tabId, universityId) {
    event.preventDefault();
    var tabValue = $('#' + tabId).val();
    var dataString = 'tab_name=' + tabId + '&tab_value=' + tabValue + '&u_id=' + universityId;
    var dataJson = {
      'tab_name': tabId,
      'tab_value': tabValue,
      'u_id': universityId,
    }
    $.ajax({
      type: "POST",
      url: "/admin/university/info/update",
      dataType: "json",
      data: dataJson,
      cache: false,
      success: function(result) {
        if (result.status == "SUCCESS") {
          alert("Data Uploaded Successfully!!!");
          window.location.reload();
        } else if (result.status == 'NOTVALIDUSER') {
          window.location.href = '/user/account';
        } else {
          alert(result.status);
          location.reload();
        }
      }
    });
  }

  var acc = document.getElementsByClassName("accordion");
  var i;

  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var panel = this.nextElementSibling;
      if (panel.style.maxHeight) {
        panel.style.maxHeight = null;
      } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
      }
    });
  }
  acc[0].click();

  function updateOverview(universityId) {

    event.preventDefault();

    var university_id = universityId;
    var total_students = $('#total_students').val();
    var international_students = $('#international_students').val();
    var total_degree_programs = $('#total_degree_programs').val();
    var establishment_year = $('#establishment_year').val();
    var type = $('#type').val();
    var location = $('#location').val();
    var acceptance_rate = $('#acceptance_rate').val();
    var internships = $('#internships').val();
    var application_fee = $('#application_fee').val();
    var average_tuition_fee = $('#average_tuition_fee').val();
    var average_cost_living = $('#average_cost_living').val();

    var general_admission_req_ug = $('#general_admission_req_ug').val();
    var general_admission_req_pg = $('#general_admission_req_pg').val();

    var overview_paragraph_one = $('#overview_paragraph_one').val();
    var overview_paragraph_two = $('#overview_paragraph_two').val();
    var overview_paragraph_three = $('#overview_paragraph_three').val();
    var read_more_international_students = $('#read_more_international_students').val();
    var read_more_employment_figures = $('#read_more_employment_figures').val();
    var read_more_student_life = $('#read_more_student_life').val();
    var read_more_programs = $('#read_more_programs').val();
    var read_more_research = $('#read_more_research').val();
    var read_more_alumni = $('#read_more_alumni').val();
    var assistantantships = $('#assistantantships').val();

    //eligibility_

    var average_eligibility_pg_gpa = $('#average_eligibility_pg_gpa').val();
    var average_eligibility_pg_gre = $('#average_eligibility_pg_gre').val();
    var average_eligibility_ug_gpa = $('#average_eligibility_ug_gpa').val();
    var average_eligibility_ug_gre = $('#average_eligibility_ug_gre').val();

    var dataJson = {
      'university_id': university_id,
      'total_students': total_students,
      'international_students': international_students,
      'total_degree_programs': total_degree_programs,
      'establishment_year': establishment_year,
      'type': type,
      'location': location,
      'acceptance_rate': acceptance_rate,
      'internships': internships,
      'application_fee': application_fee,
      'average_tuition_fee': average_tuition_fee,
      'average_cost_living': average_cost_living,

      'general_admission_req_ug': general_admission_req_ug,
      'general_admission_req_pg': general_admission_req_pg,

      'overview_paragraph_one': overview_paragraph_one,
      'overview_paragraph_two': overview_paragraph_two,
      'overview_paragraph_three': overview_paragraph_three,
      'read_more_international_students': read_more_international_students,
      'read_more_student_life': read_more_student_life,
      'read_more_employment_figures': read_more_employment_figures,
      'read_more_programs': read_more_programs,
      'read_more_research': read_more_research,
      'read_more_alumni': read_more_alumni,
      'assistantantships': assistantantships,
      'average_eligibility_pg_gpa': average_eligibility_pg_gpa,
      'average_eligibility_pg_gre': average_eligibility_pg_gre,
      'average_eligibility_ug_gpa': average_eligibility_ug_gpa,
      'average_eligibility_ug_gre': average_eligibility_ug_gre

    };


    $.ajax({

      type: "POST",

      url: "/admin/university/info/update/overview",

      dataType: "json",

      data: dataJson,

      cache: false,

      success: function(result) {

        if (result.status == "SUCCESS") {

          alert("Data Uploaded Successfully!!!");
          window.location.reload();


        } else if (result.status == 'NOTVALIDUSER') {


          window.location.href = '/user/account';

        } else {

          alert(result.status);
          location.reload();

        }

      }

    });

  }


  function updateRanking(universityId) {
    event.preventDefault();

    var university_id = universityId;

    var tab_three = $('#tab_three').val();
    var qs_global_value = $('#qs_global_value').val();
    var usNews_global_value = $('#usNews_global_value').val();
    var usNews_national_value = $('#usNews_national_value').val();
    var the_value = $('#the_value').val();
    var arwu_value = $('#arwu_value').val();

    var dataJson = {
      'university_id': university_id,
      'tab_three': tab_three,
      'qs_global_value': qs_global_value,
      'usNews_global_value': usNews_global_value,
      'usNews_national_value': usNews_national_value,
      'the_value': the_value,
      'arwu_value': arwu_value

    };


    $.ajax({

      type: "POST",

      url: "/admin/university/info/update/updateRanking",

      dataType: "json",

      data: dataJson,

      cache: false,

      success: function(result) {

        if (result.status == "SUCCESS") {

          alert("Data Uploaded Successfully!!!");
          window.location.reload();


        } else if (result.status == 'NOTVALIDUSER') {


          window.location.href = '/user/account';

        } else {

          alert(result.status);
          location.reload();

        }

      }

    });
  }


  function updateScholarshipsFinancial(universityId) {

    event.preventDefault();

    var university_id = universityId;

    var financial_assistance = $('#financial_assistance').val();
    var financial_aid = $('#financial_aid').val();
    var accommodations_offered = $('#accommodations_offered').val();
    var on_campus = $('#on_campus').val();
    var off_campus = $('#off_campus').val();

    var dataJson = {
      'university_id': university_id,
      'financial_assistance': financial_assistance,
      'financial_aid': financial_aid,
      'accommodations_offered': accommodations_offered,
      'on_campus': on_campus,
      'off_campus': off_campus

    };


    $.ajax({

      type: "POST",

      url: "/admin/university/info/update/scholarshipsfinancial",

      dataType: "json",

      data: dataJson,

      cache: false,

      success: function(result) {

        if (result.status == "SUCCESS") {

          alert("Data Uploaded Successfully!!!");
          window.location.reload();


        } else if (result.status == 'NOTVALIDUSER') {


          window.location.href = '/user/account';

        } else {

          alert(result.status);
          location.reload();

        }

      }

    });

  }

  function updateAdmissionInfo(universityId) {

    event.preventDefault();

    var university_id = universityId;

    var q_one = $('#q_one').val();

    var a_one = $('#admisson_one').val();
    var a_two = $('#admisson_two').val();
    var a_three = $('#admisson_three').val();
    var a_four = $('#admisson_four').val();

    var a_five = $('#admisson_five').val();
    var a_five_a = $('#admisson_five_a').val();
    var a_five_b = $('#admisson_five_b').val();
    var a_five_c = $('#admisson_five_c').val();
    var a_five_d = $('#admisson_five_d').val();

    var a_six = $('#admisson_six').val();
    var a_six_a = $('#admisson_six_a').val();

    var a_seven = $('#admisson_seven').val();
    var a_eight = $('#admisson_eight').val();
    var a_nine = $('#admisson_nine').val();
    var a_ten = $('#admisson_ten').val();

    var dataJson = {
      'university_id': university_id,
      'a_one': a_one,
      'a_two': a_two,
      'a_three': a_three,
      'a_four': a_four,
      'a_five': a_five,
      'a_five_a': a_five_a,
      'a_five_b': a_five_b,
      'a_five_c': a_five_c,
      'a_five_d': a_five_d,
      'a_six': a_six,
      'a_six_a': a_six_a,
      'a_seven': a_seven,
      'a_eight': a_eight,
      'a_nine': a_nine,
      'a_ten': a_ten

    };


    $.ajax({

      type: "POST",

      url: "/admin/university/info/update/admissioninfo",

      dataType: "json",

      data: dataJson,

      cache: false,

      success: function(result) {

        if (result.status == "SUCCESS") {

          alert("Data Uploaded Successfully!!!");
          window.location.reload();


        } else if (result.status == 'NOTVALIDUSER') {


          window.location.href = '/user/account';

        } else {

          alert(result.status);
          location.reload();

        }

      }

    });

  }



  function updateSeoInfo(universityId) {

    event.preventDefault();

    var university_id = universityId;

    var seo_title = $('#seo_title').val();
    var seo_description = $('#seo_description').val();
    var seo_canonical = $('#seo_canonical').val();
    var seo_index_option = $('#seo_index_option').val();
    var seo_schema = $('#seo_schema').val();

    var dataJson = {
      'university_id': university_id,
      'seo_title': seo_title,
      'seo_description': seo_description,
      'seo_canonical': seo_canonical,
      'seo_index_option': seo_index_option,
      'seo_schema': seo_schema

    };

    $.ajax({

      type: "POST",
      url: "/admin/university/info/update/seoinfo",
      dataType: "json",
      data: dataJson,
      cache: false,

      success: function(result) {

        if (result.status == "SUCCESS") {
          alert("Data Uploaded Successfully!!!");
          window.location.reload();
        } else if (result.status == 'NOTVALIDUSER') {
          window.location.href = '/user/account';
        } else {
          alert(result.status);
          location.reload();
        }

      }

    });
  }


  function updateUniversityImagesInfo(universityId) {

    var fd = new FormData();
    //var files = $('#banner')[0].files;
    var banner = $('#banner')[0].files;
    var logo = $('#logo')[0].files;
    var university_id = universityId;

    fd.append('banner', banner[0]);
    fd.append('logo', logo[0]);
    fd.append('university_id', university_id);

    event.preventDefault();

    $.ajax({

      url: "/admin/university/info/update/universityimageinfo",
      type: 'post',
      data: fd,
      contentType: false,
      processData: false,

      success: function(result) {
        //alert(result);
        if (result.status == "SUCCESS") {
          alert("Data Uploaded Successfully!!!");
          window.location.reload();
        } else if (result.status == 'NOTVALIDUSER') {
          window.location.href = '/user/account';
        } else {
          alert(result.status);
          location.reload();
        }

      }

    });
  }

  function updateSocialMediaInfo(universityId) {

    event.preventDefault();

    var university_id = universityId;

    var campus_youtube = $('#campus_youtube').val();
    var smm_facebook = $('#smm_facebook').val();
    var smm_twitter = $('#smm_twitter').val();
    var smm_instagram = $('#smm_instagram').val();
    var smm_youtube = $('#smm_youtube').val();
    var smm_linkedin = $('#smm_linkedin').val();
    var smm_whats = $('#smm_whats').val();

    var dataJson = {
      'university_id': university_id,
      'campus_youtube': campus_youtube,
      'smm_facebook': smm_facebook,
      'smm_twitter': smm_twitter,
      'smm_instagram': smm_instagram,
      'smm_youtube': smm_youtube,
      'smm_linkedin': smm_linkedin,
      'smm_whats': smm_whats

    };

    $.ajax({

      type: "POST",
      url: "/admin/university/info/update/socialmediainfo",
      dataType: "json",
      data: dataJson,
      cache: false,

      success: function(result) {

        if (result.status == "SUCCESS") {
          alert("Data Uploaded Successfully!!!");
          window.location.reload();
        } else if (result.status == 'NOTVALIDUSER') {
          window.location.href = '/user/account';
        } else {
          alert(result.status);
          location.reload();
        }

      }

    });
  }
</script>

<script type="text/javascript">
  var totalEnrollment = <?= $enrollment_count ?>;

  function addEnrollment() {
    var html = '<div id="no_of_gallery_' + (totalEnrollment + 1) + '" style="margin-top:10px;"><div style="float:left; padding-right: 500px;" class="col-sm-3"><label for="enrollment_year" class="col-sm-2" style="padding-left: 0px;">Image</label><input type="file" name="gallery[]" id="gallery_' + (totalEnrollment + 1) + '" ></div><div ><input type="button" name="add" value="Delete" style="margin-right: 350px;" onclick="deleteEnrollment(' + (totalEnrollment + 1) + ')"></div></div>';

    $("#no_of_gallery_" + totalEnrollment).after(html);
    totalEnrollment++;
  }

  function deleteEnrollment(enrollmentId) {
    $("#no_of_gallery_" + enrollmentId).remove();
    totalEnrollment--;
  }
</script>

<script type="text/javascript">
  var totalEnrollment = <?= $enrollment_count ?>;

  function addFaq() {

    var html = '<div class="col-md-12" id="no_of_faq_' + (totalEnrollment + 1) + '"><div class="col-md-5 form-group"><label for="name" class="col-md-4 control-label">Questions ' + (totalEnrollment + 1) + '</label><div class="col-md-8"><textarea class="form-control" name="question[]" id="question_' + (totalEnrollment + 1) + '" placeholder="Enter Question Here" rows="4"></textarea></div></div><div class="col-md-5 form-group"><div class="col-md-8"><textarea class="form-control" name="answer[]" id="answer_' + (totalEnrollment + 1) + '" placeholder="Enter Answer Here" rows="4"></textarea></div></div><div class="col-md-2 form-group"><input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteFaq(' + (totalEnrollment + 1) + ')"></div></div>';

    $("#no_of_faq_" + totalEnrollment).after(html);
    totalEnrollment++;
  }

  function deleteFaq(enrollmentId) {
    $("#no_of_faq_" + enrollmentId).remove();
    totalEnrollment--;
  }

  function updateFaqInfo(universityId) {

    event.preventDefault();

    var university_id = universityId;

    var question = new Array();
    $("textarea[name='question[]']").each(function() {
      question.push($(this).val());
    });

    var answer = new Array();
    $("textarea[name='answer[]']").each(function() {
      answer.push($(this).val());
    });

    var q_one = $('#q_one').val();
    var a_one = $('#a_one').val();

    var dataJson = {
      'university_id': university_id,
      'q_one': q_one,
      'a_one': a_one,
      'question': question,
      'answer': answer

    };


    $.ajax({

      type: "POST",

      url: "/admin/university/info/update/faqinfo",

      dataType: "json",

      data: dataJson,

      cache: false,

      success: function(result) {

        if (result.status == "SUCCESS") {

          alert("Data Uploaded Successfully!!!");
          window.location.reload();


        } else if (result.status == 'NOTVALIDUSER') {


          window.location.href = '/user/account';

        } else {

          alert(result.status);
          location.reload();

        }

      }

    });

  }
</script>


<script type="text/javascript">
  var totalNews = <?= $latest_news_count ?>;

  function addLatestNews() {

    var html = '<div class="col-md-12" id="no_of_news_' + (totalNews + 1) + '"><div class="col-md-3 form-group"><label for="name" class="col-md-4 control-label">Questions ' + (totalNews + 1) + '</label><div class="col-md-8"><textarea class="form-control" name="questionlet[]" id="questionlet_' + (totalNews + 1) + '" placeholder="Enter Question Here" rows="4"></textarea></div></div><div class="col-md-4 form-group"><div class="col-md-12"><textarea class="editor" name="answerlet[]" id="answerlet_' + (totalNews + 1) + '" placeholder="Enter Answer Here" rows="4"></textarea></div></div><div class="col-md-2 form-group"><div class="col-md-12"><textarea class="form-control" name="origineDate[]" id="origineDate_' + (totalNews + 1) + '" placeholder="Enter Origine Date Here" rows="4"></textarea></div></div><div class="col-md-2 form-group"><div class="col-md-12"><textarea class="form-control" name="archive[]" id="archive_' + (totalNews + 1) + '" placeholder="Enter Archive Here" rows="4"></textarea></div></div><div class="col-md-1 form-group"><input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteLatestNews(' + (totalNews + 1) + ')"></div></div>';

    $("#no_of_news_" + totalNews).after(html);
    totalNews++;
  }

  function deleteLatestNews(newsId) {
    $("#no_of_news_" + newsId).remove();
    totalNews--;
  }

  function updateLatestInfo(universityId) {

    event.preventDefault();

    var university_id = universityId;

    var question = new Array();
    $("textarea[name='questionlet[]']").each(function() {
      question.push($(this).val());
    });

    var answer = new Array();
    $("textarea[name='answerlet[]']").each(function() {
      answer.push($(this).val());
    });

    var origineDate = new Array();
    $("textarea[name='origineDate[]']").each(function() {
      origineDate.push($(this).val());
    });

    var archive = new Array();
    $("textarea[name='archive[]']").each(function() {
      archive.push($(this).val());
    });


    var dataJson = {
      'university_id': university_id,
      'origineDate': origineDate,
      'archive': archive,
      'question': question,
      'answer': answer

    };


    $.ajax({

      type: "POST",

      url: "/admin/university/info/update/latestinfo",

      dataType: "json",

      data: dataJson,

      cache: false,

      success: function(result) {
        //alert(result);
        if (result.status == "SUCCESS") {

          alert("Data Uploaded Successfully!!!");
          window.location.reload();


        } else if (result.status == 'NOTVALIDUSER') {


          window.location.href = '/user/account';

        } else {

          alert(result.status);
          location.reload();

        }

      }

    });

  }
</script>