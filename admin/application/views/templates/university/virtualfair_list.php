<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Virtualfair<small>Manage Virtualfair</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="#">Virtualfair Management</a></li>
            <li class="active">Virtualfair</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>

                <div class="box">
                    <form action="<?php echo base_url();?>university/virtualfair/multidelete" method="post">
                        <!--div class="box-header">
                            <a href="<?php echo base_url();?>university/virtualfair/form">
                                <button type="button" style="width:15%; float:left; margin-right:5px;" class="btn btn-block btn-primary"><i class="fa fa-plus"></i> Add Virtualfair</button>
                            </a>
                            <button type="submit" class="btn btn-block btn-danger multidel" style="width:15%; display:none;"><i class="fa fa-minus"></i> Delete Counselor</button>
                        </div--><!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Sl No</th>
                                        <th>Name</th>
                                        <th>Date</th>
                                        <th>URL</th>
                                        <!--th>Time</th>
                                        <th>Status</th>
                                        <th>Option</th-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1;
                                    if($vfairs)
                                    {
                                        foreach($vfairs as $vfair)
                                        {
                                            ?>
                                            <tr>
                                                <td><input type="checkbox" class="multichk" name="chk[]" value="<?php echo $vfair->id;?>"/>&nbsp;<?php echo $i++;?></td>
                                                <td><?php echo $vfair->name;?></td>
                                                <td><?php echo $vfair->fair_date;?></td>
                                                <td><?php echo '<a href="' . $vfair->url . '" target="_blank">' . $vfair->url . '</a>'?></td>
                                                <!--td><?php echo $vfair->fair_time;?></td>
                                                <td><?php echo $vfair->status;?></td>
                                                <td>
                                                    <a href="<?php echo base_url();?>university/virtualfair/form/<?php echo $vfair->id;?>" title="Modify"><i class="fa fa-edit"></i></a> &nbsp;
                                                    <a href="javascript:void();" title="Delete" class="del" id="<?php echo $vfair->id;?>"><i class="fa fa-trash-o"></i></a>
                                                </td-->
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Sl No</th>
                                        <th>Name</th>
                                        <th>Date</th>
                                        <th>URL</th>
                                        <!--th>Time</th>
                                        <th>Status</th>
                                        <th>Option</th-->
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
