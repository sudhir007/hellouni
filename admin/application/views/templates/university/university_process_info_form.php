<script src='<?= base_url() ?>resources/tinymce/tinymce.min.js'></script>
<style type="text/css">
@import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");

/* Style the tab */

.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}


/* Style the buttons inside the tab */

.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}


/* Change background color of buttons on hover */

.tab button:hover {
  background-color: #ddd;
}


/* Create an active/current tablink class */

.tab button.active {
  background-color: #ccc;
}


/* Style the tab content */

.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}

table {
  border-collapse: collapse;
}

tr {
  border-bottom: 1px solid #ccc;
}

th,
td {
  text-align: left;
  padding: 4px;
}

.textPara {
  min-height: 100px;
  width: 100%;
}

.control-label {
  margin-bottom: 6px !important;
  font-weight: 700;
}

.accordion {
  background-color: #eee;
  color: #444;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
  transition: 0.4s;
}

.accordion1 {
  background-color: #eee;
  color: #444;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
  transition: 0.4s;
}

.active,
.accordion:hover {
  background-color: #ccc;
}

.active,
.accordion1:hover {
  background-color: #ccc;
}

.panel {
  padding: 0 18px;
  background-color: white;
  max-height: 0;
  overflow: hidden;
  transition: max-height 0.2s ease-out;
  box-shadow: 0px 2px 2px 0px #00000061;
}

.panel1 {
  padding: 0 18px;
  background-color: white;
  max-height: 0;
  overflow: hidden;
  transition: max-height 0.2s ease-out;
  box-shadow: 0px 2px 2px 0px #00000061;
}
.panelFAQ{
  margin-bottom: 20px;
    background-color: #fff;
    border: 1px solid transparent;
    border-radius: 4px;
    -webkit-box-shadow: 0 1px 1px rgb(0 0 0 / 5%);
    box-shadow: 0 3px 2px rgb(0 0 0 / 13%);
    max-height: inherit;
    padding: 0px 0px;
}

.divUpload{
  margin-bottom: 20px;
box-shadow: 0px 2px 2px 0px #0000008c;
padding: 13px 15px;
border-radius: 5px;
}


</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>University <small>University Application Process Info</small></h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">University</a></li>
      <li class="active">University Application Process Info</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title"> Add Application Process Info (<b><?php echo $university[0]['name']; ?></b>) </h3> </div>
          <!-- /.box-header -->
          <!-- form start -->
          <?php
                    if($this->session->flashdata('flash_message'))
                    {
                        ?>
            <div class="alert alert-error alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
              <h4><i class="icon fa fa-circle-o"></i> Error</h4>
              <?php
                            echo $this->session->flashdata('flash_message');
                            ?>
            </div>
            <?php
                    }
                    ?>
              <div class="box-body">
                <div class="tab">

                  <button class="tablinks" onclick="tabChange(event, 'div_tab_one')" id="defaultOpen"> Application Process Info</button>
                  <button class="tablinks" onclick="tabChange(event, 'div_tab_two')"> Document List </button>
                  <button class="tablinks" onclick="tabChange(event, 'div_tab_three')"> Consent Form </button>
                  <button class="tablinks" onclick="tabChange(event, 'div_tab_four')"> Application Fill Option </button>

                </div>

                <div id="div_tab_one" class="tabcontent">
                  <form class="form-horizontal"  onsubmit="addApplicationList('process_description', <?php echo $university[0]['id']; ?>)" name="tab_one_update" autocomplete="off">

                    <div class="col-md-12">
                      <div class="col-md-12 form-group">
                      <h4> Application Process List For (<?php echo $university[0]['name']; ?>) </h4>
                      <label class="control-label"> Application Process List points </label>
                      <div class="col-sm-12">
                        <textarea class='editor' name="process_description" id="process_description"><?php if(isset($university_application_process_info[0])){ echo $university_application_process_info[0]['process_description']; }else{ echo ""; } ?></textarea>
                      </div>
                    </div>
                  </div>


                    <div class="box-footer">
                      <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                      <button type="submit" class="btn btn-info pull-right"> Update </button>
                    </div>
                    <!-- /.box-footer -->
                  </form>
                </div>

                <div id="div_tab_two" class="tabcontent">
                  <form class="form-horizontal" method="post" onsubmit="uploadAppProcessDoc('checklistdoc', 'checklist_url', <?php echo $university[0]['id']; ?>)" name="tab_two_update" autocomplete="off">
                          <div class="form-group">
                            <label for="name" class="col-sm-6 control-label">University Name (<?php echo $university[0]['name']; ?>)</label>
                            <div class="col-md-12">
                              <div class="col-md-12 divUpload">
                                  <div class="form-group col-md-12">
                                    <label for="name" class="col-md-3">Document Name</label>
                                    <input type="text" class="col-md-6" name="checklist_url" id="checklist_url"></input>
                                  </div>
                                  <div class="form-group col-md-12">
                                    <label for="name" class="col-md-3">Upload Document</label>
                                      <input type="file" id="checklistdoc" name="checklistdoc" class="col-md-8">

                                  </div>
                                  <div class="col-md-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-warning"> Upload </button>

                                    <?php
                                    if(isset($university_application_process_info[0]) && isset($university_application_process_info[0]['checklist_url']))
                                    {
                                    ?>
                                    <a class="btn btn-info" target="_blank" href="<?php echo $university_application_process_info[0]['checklist_url']; ?>"> View Url </a>
                                    <?php
                                      }
                                    ?>

                                  </div>
                              </div>

                            </div>

                          </div>


                    <!-- /.box-footer -->
                  </form>
                </div>


                <div id="div_tab_three" class="tabcontent">
                  <form class="form-horizontal" method="post" onsubmit="uploadAppProcessDoc('consentdoc', 'consentform_url', <?php echo $university[0]['id']; ?>)" name="tab_three_update" autocomplete="off">

                    <div class="form-group">
                      <label for="name" class="col-sm-6 control-label">University Name (<?php echo $university[0]['name']; ?>)</label>

                    <div class="col-md-12">
                      <div class="col-md-12 form-group">

                        <div class="col-md-12 divUpload">

                            <div class="form-group col-md-12">
                              <label for="name" class="col-md-3">Document Name</label>
                              <input type="text" class="col-md-6" name="consentform_url" id="consentform_url"></input>
                            </div>
                            <div class="form-group col-md-12">
                              <label for="name" class="col-md-3">Upload Document</label>
                                <input type="file" id="consentdoc" name="consentdoc" class="col-md-8">
                            </div>
                            <div class="col-md-12 col-md-offset-3">
                              <button type="submit" class="btn btn-warning"> Upload </button>

                              <?php
                              if(isset($university_application_process_info[0]) && isset($university_application_process_info[0]['consentform_url']))
                              {
                              ?>
                              <a class="btn btn-info" target="_blank" href="<?php echo $university_application_process_info[0]['consentform_url']; ?>"> View Url </a>
                              <?php
                                }
                              ?>

                            </div>
                        </div>


                      </div>

                    </div>

                  </form>

        </div>
        <!--/.col (right) -->
      </div>

      <div id="div_tab_four" class="tabcontent">
        <form class="form-horizontal" method="post" onsubmit="addApplicationList('application_fill_option', <?php echo $university[0]['id']; ?>)" name="tab_four_update" autocomplete="off">
                <div class="form-group">
                  <label for="name" class="col-sm-6 control-label">University Name (<?php echo $university[0]['name']; ?>)</label>
                  <div class="col-md-12">
                    <div class="col-md-12 divUpload">
                      <div class="form-group col-md-12">

                        <?php

                        $applicationProcessArray = [];

                        if(isset($university_application_process_info[0])) {

                          $applicationProcessArray = $university_application_process_info[0]['application_fill_option'];

                        }
                        ?>

                            <label class="container">Imperial will fill the application
                              <input type="checkbox" name="application_fill_option[]"  value="1" <?php if(in_array("1",$applicationProcessArray)){echo "checked";} ?> >
                              <span class="checkmark"></span>
                            </label>
                            <label class="container">Imperial will check the application
                              <input type="checkbox" name="application_fill_option[]" value="2" <?php if(in_array("2",$applicationProcessArray)){echo "checked";} ?> >
                              <span class="checkmark"></span>
                            </label>

                          </div>

                    </div>

                  </div>

                </div>

          <div class="box-footer">
            <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
            <button type="submit" class="btn btn-info pull-right"> Update </button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
      <!-- /.row -->
  </section>
  <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
  tinymce.init({
    selector: '.editor',
    setup: function(editor) {
      editor.on('change', function() {
        tinymce.triggerSave();
      });
    },
    theme: 'silver',
    height: 400,
    plugins: [
    ' advlist ',
    ' autolink ',
    ' charmap ',
    ' preview ',
    ' anchor ',
    ' link',
    ' image',
    ' lists ',
    ' code ',
    ' visualblocks ',
    ' table',
    ' searchreplace '
    ],
    toolbar: 'undo redo | formatselect | bold italic forecolor backcolor | fontsizeselect | alignleft aligncenter alignright alignjustify outdent indent | table tabledelete | numlist bullist | link image | removeformat | help',
    content_style: 'p { margin: 15px; color: #4a4949; font-size: 12pt; }' + ' ul { color: #4a4949; font-size: 12pt; }' + ' ol { color: #4a4949; font-size: 12pt; } ',
    fontsize_formats: "8pt 9pt 10pt 11pt 12pt 14pt 18pt 24pt 30pt 36pt 48pt 60pt 72pt 96pt"
  });
  </script>

<script type="text/javascript">

  function tabChange(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for(i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for(i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
  }
  // Get the element with id="defaultOpen" and click on it
  document.getElementById("defaultOpen").click();

function addApplicationList(tabId, universityId) {

          //alert("hello");
          event.preventDefault();
          if(tabId === 'application_fill_option'){

            var tabValue = [];

            $.each($("input[name='application_fill_option[]']:checked"), function(){
              tabValue.push($(this).val());
            });

          } else {
            var tabValue = $('#' + tabId).val();
          }

          var dataJson = {
            'f_name': tabId,
            'f_value': tabValue,
            'u_id': universityId,
          };

          console.log(dataJson);

          $.ajax({
            type: "POST",
            url: "/admin/university/universityinfo/universityProcessInfoAdd",
            dataType: "json",
            data: dataJson,
            cache: false,
            success: function(result) {
              if(result.status == "SUCCESS") {
                alert("Data Uploaded Successfully!!!");
                window.location.reload();
              } else if(result.status == 'NOTVALIDUSER') {
                window.location.href = '/user/account';
              } else {
                alert(result.status);
                location.reload();
              }
            }
          });
      }

      function uploadAppProcessDoc(docId, documentName, universityId){

        var fd = new FormData();
              var files = $('#'+docId)[0].files;
              var doc_name = $('#' + documentName).val();

              // Check file selected or not
              if(files.length > 0 ){

                 fd.append('document',files[0]);
                 fd.append('f_name',documentName);
                 fd.append('doc_name',doc_name);
                 fd.append('university_id',universityId);

                 event.preventDefault();

                 $.ajax({
                    url: '/admin/university/process/doc/update',
                    type: 'post',
                    data: fd,
                    contentType: false,
                    processData: false,
                    success: function(result){
                      if(result.status == "SUCCESS") {
                        alert("Data Uploaded Successfully!!!");
                        window.location.reload();
                      } else if(result.status == 'NOTVALIDUSER') {
                        window.location.href = '/user/account';
                      } else {
                        alert(result.status);
                        location.reload();
                      }
                    },
                 });
              }else{
                 alert("Please select a file.");
              }

      }

</script>
