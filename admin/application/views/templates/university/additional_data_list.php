<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>University<small> Universities Additional Info List </small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">University Management</a></li>
            <li class="active">Applied Universities</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php
                if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>
                <div class="box">
                    <form action="<?php echo base_url();?>user/university/multidelete" method="post">
                        <?php
                        if($university_aditional_data_list)
                        {
                            ?>
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>University Id</th>
                                            <th>University Name</th>
                                            <th>Edit Link</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($university_aditional_data_list as $application)
                                    {
                                        ?>
                                        <tr>
                                            <td><?php echo $application['university_id'];?></td>
                                            <td><?php echo $application['university_name'];?></td>

                                            <td>
                                                <a href="<?php echo base_url();?>university/additionaldata/additionaldataform/<?=$application['university_id']?>" title="Check Applications">View / Edit Data</a>
                                            </td>
                                            </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                          <th>University Id</th>
                                          <th>University Name</th>
                                          <th>Edit Link</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div><!-- /.box-body -->
                            <?php
                        }
                        ?>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
