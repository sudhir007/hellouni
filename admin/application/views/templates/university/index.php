<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Student<small>Manage Internships</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Student Management</a></li>
            <li class="active">Internship</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php
                if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>
                <div class="box">
                    <form action="<?php echo base_url();?>user/university/multidelete" method="post">
                        <div class="box-header">
                            <a href="<?php echo base_url();?>university/internship/form">
                                <button type="button" style="width:12%; float:left; margin-right:5px;" class="btn btn-block btn-primary"><i class="fa fa-plus"></i> Add Internship</button>
                            </a>
                        </div><!-- /.box-header -->
                        <?php
                        if($internships)
                        {
                            ?>
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>University Name</th>
                                            <th>College Name</th>
                                            <th>Department Name</th>
                                            <th>Application Deadline</th>
                                            <th>Internship From</th>
                                            <th>Internship To</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($internships as $internship)
                                    {
                                        ?>
                                        <tr>
                                            <td><input type="checkbox" class="multichk" name="chk[]" value="<?php echo $internship->id;?>"/></td>
                                            <td><?php echo $internship->university_name;?></td>
                                            <td><?php echo $internship->college_name;?></td>
                                            <td><?php echo $internship->department_name;?></td>
                                            <td><?php echo $internship->application_deadline;?></td>
                                            <td><?php echo $internship->internship_start_date;?></td>
                                            <td><?php echo $internship->internship_end_date;?></td>
                                            <td>
                                                <a href="<?php echo base_url();?>university/internship/form/<?php echo $internship->id;?>" title="Modify"><i class="fa fa-edit"></i></a> &nbsp;
                                                <a href="javascript:void();" title="Delete" class="del" id="<?php echo $internship->id;?>"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                            </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th>University Name</th>
                                            <th>College Name</th>
                                            <th>Department Name</th>
                                            <th>Application Deadline</th>
                                            <th>Internship From</th>
                                            <th>Internship To</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div><!-- /.box-body -->
                            <?php
                        }
                        ?>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
