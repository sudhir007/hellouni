<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Student<small>Internships Requests</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Student Management</a></li>
            <li class="active">Internships Requests</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php
                if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>
                <div class="box">
                    <form action="<?php echo base_url();?>user/university/multidelete" method="post">
                        <?php
                        if($internships)
                        {
                            ?>
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>University Name</th>
                                            <th>College Name</th>
                                            <th>Department Name</th>
                                            <th>Student Name</th>
                                            <th>Current University</th>
                                            <th>Current Course</th>
                                            <th>Expected Graduation Year</th>
                                            <th>Application Deadline</th>
                                            <th>Internship From</th>
                                            <th>Internship To</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($internships as $internship)
                                    {
                                        ?>
                                        <tr>
                                            <td><?php echo $internship->university_name;?></td>
                                            <td><?php echo $internship->college_name;?></td>
                                            <td><?php echo $internship->department_name;?></td>
                                            <td><?php echo $internship->student_name;?></td>
                                            <td><?php echo $internship->student_other_details && $internship->student_other_details->current_university ? $internship->student_other_details->current_university : '';?></td>
                                            <td><?php echo $internship->student_other_details && $internship->student_other_details->current_course ? $internship->student_other_details->current_course : '';?></td>
                                            <td><?php echo $internship->student_other_details && $internship->student_other_details->expected_graduation_year ? $internship->student_other_details->expected_graduation_year : '';?></td>
                                            <td><?php echo $internship->application_deadline;?></td>
                                            <td><?php echo $internship->internship_start_date;?></td>
                                            <td><?php echo $internship->internship_end_date;?></td>
                                            <td>
                                                <a href="<?php echo base_url();?>university/internship/detail/<?php echo $internship->id;?>" title="Modify"><i class="fa fa-eye"></i></a> &nbsp;
                                            </td>
                                            </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>University Name</th>
                                            <th>College Name</th>
                                            <th>Department Name</th>
                                            <th>Student Name</th>
                                            <th>Current University</th>
                                            <th>Current Course</th>
                                            <th>Expected Graduation Year</th>
                                            <th>Application Deadline</th>
                                            <th>Internship From</th>
                                            <th>Internship To</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div><!-- /.box-body -->
                            <?php
                        }
                        ?>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
