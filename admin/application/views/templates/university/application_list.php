<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>University<small>Applied Universities</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">University Management</a></li>
            <li class="active">Applied Universities</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php
                if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>
                <div class="box">
                    <form action="<?php echo base_url();?>user/university/multidelete" method="post">
                        <?php
                        if($applications)
                        {
                            ?>
                            <div class="box-body">
                                <table id="newAppExample" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>University Name</th>
                                            <th>Student Name</th>
                                            <th>Student Email</th>
                                            <th>Student Phone</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <!-- <?php
                                    foreach($applications as $application)
                                    {
                                        ?>
                                        <tr>
                                            <td><input type="checkbox" class="multichk" name="chk[]" value="<?php echo $application['university_id'];?>"/></td>
                                            <td><?php echo $application['name'];?></td>
                                            <td><?php echo $application['student_name'];?></td>
                                            <td><?php echo $application['student_email'];?></td>
                                            <td><?php echo $application['student_phone'];?></td>
                                            <td>
                                                <a href="<?php echo base_url();?>university/application/?uid=<?=$application['university_id']?>" title="Check Applications" target="_blank">Check Applications</a>
                                            </td>
                                            </tr>
                                        <?php
                                    }
                                    ?> -->
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th>University Name</th>
                                            <th>Student Name</th>
                                            <th>Student Email</th>
                                            <th>Student Phone</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div><!-- /.box-body -->
                            <?php
                        }
                        ?>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
var table;
  $(document).ready(function() {

    //datatables
    table = $('#newAppExample').DataTable({ 
//alert('hi');
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url();?>university/application/ajax_manage_page",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
                { "data": "id" },
                { "data": "name" },
                { "data": "student_name" },
                { "data": "student_email" },
                { "data": "student_phone" },
                { "data": "action" }
            ],
        

    });

  });
  
</script>
