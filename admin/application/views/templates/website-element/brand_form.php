<!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        <?php if($this->uri->segment('5') && $this->uri->segment('5')=='view'){

$view='disabled'; ?>

<script type="text/javascript">
$( document ).ready(function() {
$("#cleditor").cleditor()[0].disable("true");
//$(".btn .btn-primary .pull-right").attr('disabled' , true);
$('.pull-right').prop('disabled', true);

});

</script>
<?php } else { $view=''; }


 ?>
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- Statistics Button Container -->
            	
                
                <!-- Panels Start -->
				
				 <?php if($msg = $this->session->flashdata('flash_message')){?> 
				<div class="mws-form-message success"><?php echo $msg;?></div>
				 <?php } ?>
				 <?php if(validation_errors()){?> 
    <div class="mws-form-message error"><?php echo validation_errors(); ?></div>
     <?php } ?>

            	<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-magic"></i> Brand</span>
                    </div>
                    <div class="mws-panel-body no-padding">
					
					
                        <?php if($this->uri->segment('4')){?>
						<form class="mws-form wzd-default" action="<?php echo base_url();?>website-element/brand/edit_brand/<?php if(isset($id)) echo $id;?>" method="post">
						<?php }else{?>
						<form class="mws-form wzd-default" action="<?php echo base_url();?>website-element/brand/create_brand" method="post" enctype="multipart/form-data">
                        <?php }?>    
                            <fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> General</legend>
                                <div id class="mws-form-row">
                                    <label class="mws-form-label">Brand Name <span class="required">*</span></label>
                                    <div class="mws-form-item">
                       <input type="text" name="brand_name" class="required large" <?php echo $view; ?> value="<?php if(isset($name)) echo $name; ?>" required>
                                    </div>
                                </div>
                              
                               <?php /*?> <div class="mws-form-row">
                                    <label class="mws-form-label">Brand Description</label>
                                    <div class="mws-form-item">
                                        <textarea name="brand_description" rows="" cols="" class="required large"><?php if(isset($description)) echo $description; ?></textarea>
                                    </div>
                                </div><?php */?>
								
								
								
								
							<div class="mws-form-row">
                            	 <label class="mws-form-label">Brand Description</label>
                                <div class="mws-form-item">
                                	<textarea name="brand_description" id="cleditor" class="large"><?php if(isset($description)) echo $description; ?></textarea>
                                </div>
                            </div>	
								
								
								
								
								
								
                                 <div class="mws-form-row">
                                    <label class="mws-form-label">Sort Order</label>
                                    <div class="mws-form-item">
                                        <input type="text" name="brand_sort_order" <?php echo $view; ?> class="required email large" value="<?php if(isset($sort_order)) echo $sort_order; ?>">
                                    </div>
                                </div>
								
								
								
                            </fieldset>
                            
                            <fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> Links</legend>
                                 <div class="mws-form-row">
                                        <label class="mws-form-label">Property</label>
                                        <div class="mws-form-item">
                                        <select name="mult_properties[]" multiple="multiple" size="10" class="large" <?php echo $view; ?>>    
										<?php if($this->uri->segment('4')){ 
										    foreach($properties as $prop){
											?>
										<option value="<?php echo $prop->id;?>" <?php foreach($prop_brand as $pr){ if($prop->id==$pr->property_id){ echo 'selected="selected"' ; break; } } ?>><?php echo $prop->name; ?></option>
											<?php }
										     } else{ 
											 foreach($properties as $prop){?>
									    <option value="<?php echo $prop->id;?>"><?php echo $prop->name; ?></option>
											<?php } }?>
										</select>	 
                                        </div>
                                    </div>
                              
                               
                            </fieldset>
                            
                            <fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> Image</legend>
                                <div id class="mws-form-row">
                                    <label class="mws-form-label">Brand Logo <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                       <div class="image">
									  <?php if(isset($fileid)||isset($fileid)!=''){?>
									   <img src="<?php echo $path;?>" id="elfileimg-1" width="100" height="100" alt="" class="elfileimg">
									   <input type="hidden" name="file_id" value="<?php echo $fileid;?>"/>
									   <input type="hidden" name="brand_logo" value="<?php echo $path;?>" id="elfileurl-1" class="elfileurl" ><br>

									   <?php }else{?>
									   <img src="<?php echo base_url();?>images/no_image.jpg" width="100" height="100" alt="" id="elfileimg-1" class="elfileimg">
									   	<input type="hidden" name="brand_logo" value="" id="elfileurl-1" class="elfileurl" ><br>

									   <?php }?>
									   <input type="button" id="browse-1" class="btn btn-danger elselect-button" value="Browse" <?php echo $view; ?>>
                                       <div class="mws-panel-body no-padding no-border">
                                       <div id="elfinder"></div>
                                       </div>
                            
                           
                                    </div>
                                </div>
                              
                               
                            </fieldset>
							
							
							
							<fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> SEO</legend>
                            
							<div class="mws-form-row">
                            	 <label class="mws-form-label">Meta Tag</label>
                                <div class="mws-form-item">
                                	<textarea name="brand_meta_tag" <?php echo $view; ?> class="large"><?php if(isset($meta_tag)) echo $meta_tag; ?></textarea>
                                </div>
                            </div>	
								
							<div class="mws-form-row">
                            	 <label class="mws-form-label">Meta Description</label>
                                <div class="mws-form-item">
                                	<textarea name="brand_meta_description" <?php echo $view; ?> class="large"><?php if(isset($meta_description)) echo $meta_description; ?></textarea>
                                </div>
                            </div>	
								
								
								
                            </fieldset>
                      
					   
					    </form>
                    </div>
                </div>

              
                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->
			
