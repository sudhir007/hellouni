
    
        <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- Statistics Button Container -->
            	
                
                <!-- Panels Start -->
                 
            	 <?php if($msg = $this->session->flashdata('flash_message')){?> 
    <div class="mws-form-message success"><?php echo $msg;?></div>
     <?php } ?>
	 
	 
	  <?php if($ermsg = $this->session->flashdata('error_message')){?> 
    <div class="mws-form-message error"><?php echo $ermsg;?></div>
     <?php } ?>
				<div class="mws-panel grid_8">
				<form action="<?php echo base_url();?>website-element/event/delete_mult" method="post">
                	<div class="mws-panel-header">
					
                    	<span><i class="icon-table"></i> Event
						<a href="<?php echo base_url();?>website-element/event/form"><button type="button" class="btn btn-small">Insert</button></a>
						<button type="submit" name="delete-Brand" id="del" class="btn btn-small">Delete</button>
						
						</span>
						
					
						
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-datatable-fn mws-table">
                            <thead>
                                <tr>
                                    <th style="width: 1px;">Select</th>
									<th>Logo</th>
									<th>Name</th>
                                  
                                    <th>Status</th>
                                    <th>Sort Order</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($events as $event):?>
								<tr>
                                    <td><input type="checkbox" name="chk[]" class="brand_chk" id="<?php echo $event->id;?>" value="<?php echo $event->id;?>"/></td>
									
									<td><?php if(isset($event->path)||isset($event->path)!=''){?>
									<img src="<?php echo $event->path;?>" alt="" id="thumb0" width="50px" height="50px">
                                    <?php }else{?>
									<img src="<?php echo base_url();?>images/no_image.jpg" width="100" height="100" alt="" class="elfileimg">
									<?php }?></td>
                                    <td><?php echo $event->name;?></td>
                                    
                                    <td><?php echo $event->status_name;?></td>
									<td><?php echo $event->sort_order;?></td>
									<td class=" ">
                                        <span class="btn-group">
                                            
											 <a class="btn btn-small" href="<?php echo base_url();?>website-element/event/form/<?php echo $event->id;?>/view"><i class="icon-search"></i></a>
											
											
                                            <a class="btn btn-small" href="<?php echo base_url();?>website-element/event/form/<?php echo $event->id;?>"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small delete-row" id="<?php echo base_url(); ?>website-element/event/delete/<?php echo $event->id;?>" href="javascript:void(o);"><i class="icon-trash"></i></a></span>
                                    </td>
                                </tr>
                               <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
					
					</form>
                </div>

                                 

                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->
			
