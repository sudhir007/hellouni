<style>
   /* Style the tab */
   .tab {
   overflow: hidden;
   border: 1px solid #ccc;
   background-color: #f1f1f1;
   }
   /* Style the buttons inside the tab */
   .tab button {
   background-color: inherit;
   float: left;
   border: none;
   outline: none;
   cursor: pointer;
   padding: 14px 16px;
   transition: 0.3s;
   font-size: 18px;
   font-weight: 700;
   color:#F6881F;
   }
   /* Change background color of buttons on hover */
   .tab button:hover {
   background-color: #ddd;
   }
   /* Create an active/current tablink class */
   .tab button.active {
   background-color: #ccc;
   }
   /* Style the tab content */
   .tabcontent {
   display: none;
   padding: 6px 12px;
   border: 1px solid #ccc;
   border-top: none;
   }
   .showcontent{
   display: block;
   }
   .update_button{
   text-align: center;
   }
   #exam_stage, #exam_date, #result_date, #exam_score, #english_exam_stage, #english_exam_date, #english_result_date, #english_exam_score, #exam_score_gre, #exam_score_gmat, #exam_score_sat, #masters_id{
   display: none;
   }
   #personal_details .form-inline .form-group, #application_details .form-inline .form-group, #visa_details .form-inline .form-group{
   margin-bottom: 15px;
   }
   #competitive_exam label, #english_proficiency_exam label, #exam_stage label, #exam_date label, #result_date label, #english_exam_stage label , #afterschool label{
   margin-left: 5px;
   margin-right: 10px;
   }
   #exam_score label, #english_exam_score label{
   margin-left: 25px;
   }
   .col-lg-12, .col-lg-6{
   padding-left: 0px;
   }
   .blogShort{ border-bottom:1px solid #ddd;}
   .add{background: #333; padding: 10%; height: 300px;}
   .nav-sidebar {
   width: 100%;
   padding: 30px 0;
   border-right: 1px solid #ddd;
   }
   .nav-sidebar a {
   color: #333;
   -webkit-transition: all 0.08s linear;
   -moz-transition: all 0.08s linear;
   -o-transition: all 0.08s linear;
   transition: all 0.08s linear;
   }
   .nav-sidebar .active a {
   cursor: default;
   background-color: #0b56a8;
   color: #fff;
   }
   .nav-sidebar .active a:hover {
   background-color: #E50000;
   }
   .nav-sidebar .text-overflow a,
   .nav-sidebar .text-overflow .media-body {
   white-space: nowrap;
   overflow: hidden;
   -o-text-overflow: ellipsis;
   text-overflow: ellipsis;
   }
   .btn-blog {
   color: #ffffff;
   background-color: #E50000;
   border-color: #E50000;
   border-radius:0;
   margin-bottom:10px
   }
   .btn-blog:hover,
   .btn-blog:focus,
   .btn-blog:active,
   .btn-blog.active,
   .open .dropdown-toggle.btn-blog {
   color: white;
   background-color:#0b56a8;
   border-color: #0b56a8;
   }
   article h2{color:#333333;}
   h2{color:#0b56a8;}
   .margin10{margin-bottom:10px; margin-right:10px;}
   .container .text-style
   {
   text-align: justify;
   line-height: 23px;
   margin: 0 13px 0 0;
   font-size: 19px;
   }
   .noPadding{
   padding-left: 0px !important;
   padding-right: 0px !important;
   }
   .bRadius{
   border-radius: 6px;
   }
   table {
   font-family: arial, sans-serif;
   border-collapse: collapse;
   width: 100%;
   }
   td, th {
   border: 1px solid #dddddd;
   text-align: left;
   padding: 8px;
   color: black;
   }
   /*tr:nth-child(even) {
   background-color: #dddddd;
   }*/
   .selected-application{
   background-color: #dddddd !important;
   }
   .accordion {
   background-color: #fff;
   color: #444;
   cursor: pointer;
   padding: 1%;
   /*width: 100%;*/
   border: 0.5px solid;
   text-align: left;
   outline: none;
   font-size: 13px;
   transition: 0.4s;
   /*height: 20px;*/
   }
   .active, .accordion:hover {
   background-color: #eee;
   }
   .panel {
   padding: 0 18px;
   display: none;
   background-color: white;
   overflow: hidden;
   }
   .my-custom-scrollbar {
   position: relative;
   height: 200px;
   overflow: auto;
   }
   .table-wrapper-scroll-y {
   display: block;
   }
   .mytabs{
   margin: 2% 0px;
   }
   .mytabs .nav-tabs {
   background-color: #815dd5;
   }
   .mytabs .nav-tabs>li>a : hover {
   color: black;
   }
   .prfileBox3{
   background-color: #815dd5;
   margin: 2% 0px;
   padding: 1%;
   border-radius: 7px;
   }
   .margin2{
   margin: 2px 0 2px !important;
   }
   .ptag{
   margin: 0px;
   font-size: 13px;
   line-height: 1.8;
   letter-spacing: 1px;
   }
   .h3Heading{
   background-color: #f6882c;
   padding: 8px 2px;
   text-align: center;
   color: white;
   }
   .boxAppList{
   box-shadow: 0px 0px 12px #d8d1d1;
   border-radius: 7px;
   }
   .listtableMain thead tr {
   color: #815dd5;
   font-size: 13px;
   /* font-weight: 600; */
   letter-spacing: 0.8px;
   }
   .tablink {
   background-color: #f7f7f7;
   color: #f39d3f;
   float: left;
   border: none;
   outline: none;
   cursor: pointer;
   padding: 10px 10px;
   font-size: 18px;
   font-weight: 600;
   letter-spacing: 1.3px;
   width: 25%;
   }
   .mytabs .tabcontent {
   color: white;
   display: none;
   padding: 70px 20px;
   height: 100%;
   }
   .tabName{
   letter-spacing: 0.6px;
   font-size: 14px;
   }

   .form-control {
    height: 30px !important;
}

.form-group label{
  color: black !important;
}

.form-group h5{
  margin: 10px 0 6px;
    text-transform: uppercase;
    font-weight: 700;
    letter-spacing: 1px;
    color: #000;
}
.prplButton{
   color: #fff;
   background: #8703c5;
   border-color: #8703c5;
   border-radius: 12px;
   }
   .resetButton{
   color: #8c57c6;
   background: #f1f1f1;
   border-color: #8703c5;
   border-radius: 12px;
   }

   .insideUl a{
    color: white;
   }

   .tabordion {
    color: #333;
    display: block;
    font-family: arial, sans-serif;
    margin: auto;
    position: relative;
    width: 100%;
    min-height: 600px;
    height: auto;
    overflow: auto;
   }
   .tabordion input[name="sections"] {
   left: -9999px;
   position: absolute;
   top: -9999px;
   }
   .tabordion section {
   display: block;
   }
   .tabordion section label {
   background: #ffffff;
   border-right: 3px solid #815dd5;
   cursor: pointer;
   display: block;
   font-size: 1.2em;
   font-weight: bold;
   padding: 1px 15px;
   position: relative;
   width: 26%;
   z-index:100;
   }
   .tabordion section article {
   background: white;
   display: none;
   left: 27%;
   min-width: 300px;
   /* padding: 0 0 0 21px;*/
   position: absolute;
   top: 0;
   }
   .tabordion section article:after {
   /*background-color: #ccc;*/
   bottom: 0;
   content: "";
   display: block;
   left:-229px;
   position: absolute;
   top: 0;
   width: 220px;
   z-index:1;
   }
   .tabordion input[name="sections"]:checked + label {
   background: white;
   color: black;
   border-left: 3px solid #f39d3f;
   }
   .tabordion input[name="sections"]:checked + label i {
   color: #f6882c;
   }
   .tabordion input[name="sections"]:checked ~ article {
   display: block;
   }
   @media (max-width: 533px) {
   h1 {
   width: 100%;
   }
   .tabordion {
   width: 100%;
   }
   .tabordion section label {
   font-size: 1em;
   width: 160px;
   }
   .tabordion section article {
   left: 200px;
   min-width: 270px;
   }
   .tabordion section article:after {
   background-color: #ccc;
   bottom: 0;
   content: "";
   display: block;
   left:-199px;
   position: absolute;
   top: 0;
   width: 200px;
   }
   }
   .tabordion section label h6{
   text-transform: initial;
   font-size: 13px;
   }
   .tabordion section label h6 i{
   color: #815dd5;
   }
   @media (max-width: 768px) {
   h1 {
   width: 96%;
   }
   .tabordion {
   width: 100%;
   }
   }
   @media (min-width: 1366px) {
   h1 {
   width: 70%;
   }
   .tabordion {
   width: 100%;
   }
   }

   /* Style the tab */
   .newTab .tab {
   overflow: hidden;
   border: 1px solid #815dd5;
   /*background-color: #f1f1f1;*/
   }
   /* Style the buttons inside the tab */
   .newTab .tab button {
   background-color: inherit;
   float: left;
   border: none;
   outline: none;
   cursor: pointer;
   padding: 14px 16px;
   transition: 0.3s;
   font-size: 17px;
   }
   /* Change background color of buttons on hover */
   .newTab .tab button:hover {
   background-color: #815dd533;
   }
   /* Create an active/current tablink class */
   .newTab .tab button.active {
   background-color: #815dd5;
   color: white;
   }
   /* Style the tab content */
   .newTab .tabcontent1 {
   /*display: none;*/
   padding: 6px 12px;
   border: 1px solid #ccc;
   border-top: none;
   max-height: 520px;
   overflow: overlay;
   }
   .textVertical{
   writing-mode: tb-rl;

    transform: rotate(-180deg);
    color: white;
    margin-bottom: 14px;
   }
   .verticalDiv{
   padding: 29px 0px;
    background-color: #5d82d5;
    color: white;
    margin-top: 0px;
    text-align: -webkit-center;
   }
   .verticalDiv .col-md-11{
   padding-left: 0px;
   }
   .requiredDoc{
   background-color: #5d82d5;
   }
   .optDoc{
   background-color: #5dd5c4;
   }
   .docUplodedName{
   background-color: ghostwhite;
   }
   .docUploadNameLink {
   color: #815dd5 !important;
   }
   .docUploadBox{
   box-shadow: 1px 1px 1px 1px #efe7e7;
   padding-left: 0px !important;
   margin-bottom: 2%;
   }
   .docName h5{
   text-transform: capitalize;
   font-family: inherit;
   margin: 10px 0px;
   color: #000000;
   font-weight: inherit;
   letter-spacing: 1.5px;
   }
   .docName p{
   margin: 8px;
   }
   .statusBox{
   padding: 2% 1%;
   text-align: center;
   }
   .fa-times{
   font-size: 30px;
   color: red;
   }
   .fa-check{
   font-size: 30px;
   color: green;
   }
   .comment-box {
   margin-top: 30px !important;
   }
   /* CSS Test end */
   .comment-box img {
   width: 50px;
   height: 50px;
   }
   .comment-box .media-left {
   padding-right: 10px;
   width: 9%;
   }
   .comment-box .media-body p {
   border: 1px solid #ddd;
   padding: 10px;
   }
   .comment-box .media-body .media p {
   margin-bottom: 0;
   }
   .comment-box .media-heading {
   background-color: #815dd5;
   border: 1px solid #815dd5;
   padding: 7px 10px;
   position: relative;
   margin-bottom: -1px;
   color: white;
   }
   .comment-box .media-heading:before {
   content: "";
   width: 12px;
   height: 12px;
   background-color: #f5f5f5;
   border: 1px solid #ddd;
   border-width: 1px 0 0 1px;
   -webkit-transform: rotate(-45deg);
   transform: rotate(-45deg);
   position: absolute;
   top: 10px;
   left: -6px;
   }

   .sep{
       margin-top: 20px;
       margin-bottom: 20px;
       background-color: #e6c3c3;
   }

   .requiredInfoForm label{
          border-right: 3px solid #ffffff !important;
    cursor: pointer !important;
    display: inline-block !important;
   }

   /*document modal */
      #modalDocument .modal-title {
      text-align:center;
      text-transform:uppercase;
      color:#ffffff;
      }
      #modalDocument .modal-content {
      background-color: white;
      color: #000;
      padding: 20px 50px;
      }
      #modalDocument .modal-header {
      border-bottom: none;
      background: #00c0ef;
      color:#ffffff;
      text-align:center;
      }
      #modalDocument .modal-dialog {
      z-index: 10000;
      }
      #modalDocument .modal-header .close {
      color:#ffffff;
      }
      #modalDocument .modal-body {
      font-size: 16px;
      font-weight: 300;
      }
      #modalDocument .modal-body p {
      font-size: 12px;
      font-weight: 300;
      }
</style>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>Student<small>Manage Students</small></h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="#">Student Management</a></li>
         <li class="active">Student Profile</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-md-3">
            <div class="box box-default">
               <div class="box-body box-profile">
                  <!-- <img class="profile-user-img img-responsive img-circle" src="dist/img/avatar.png" alt="User profile picture"> -->
                  <h3 class="profile-username text-center"><b><span><?php if($name) echo $name;?></span></b></h3>
                  <ul class="list-group list-group-unbordered">
                     <li class="list-group-item txtBlack paddingLR">
                        <span class="profile-heading" style="color: black;">Email:</span> <a class="pull-right" ><?php if($email) echo $email;?></a>
                     </li>
                     <li class="list-group-item txtBlack paddingLR">
                        <span class="profile-heading" style="color: black;">Phone Number:</span> <a class="pull-right" ><?php if($phone) echo $phone;?></a>
                     </li>
                     <li class="list-group-item txtBlack paddingLR">
                        <span class="profile-heading" style="color: black;">Registration Type:</span> <a class="pull-right" ><?php if($registration_type) echo $registration_type;?></a>
                     </li>
                     <li class="list-group-item txtBlack paddingLR">
                        <span class="profile-heading" style="color: black;"> Parent Name: </span><a class="pull-right" > <?php if($parent_name) echo $parent_name;?> </a>
                     </li>
                     <li class="list-group-item txtBlack paddingLR">
                        <span class="profile-heading" style="color: black;"> Status: </span><a class="pull-right" ><?php if($status) echo $status ? 'Active' : 'Inactive' ;?></a>
                     </li>
                     <li class="list-group-item txtBlack paddingLR">
                        <span class="profile-heading" style="color: black;"> <a href="/admin/student/comments?id=<?=$id?>" target="_blank">Comment History</a> </span>
                        <a class="pull-right" href="/admin/student/applicationList?user_id=<?=$id?>&student_id=<?=$student_id?>" >Shortlist Universities / Courses</a>
                     </li>
                     <!-- <li class="list-group-item txtBlack paddingLR">
                        <a class="ist-group-item txtBlack" target="_blank" href="/comments/lead/{{leadId}}/0">Comment History</a>
                        <a class="ist-group-item txtBlack" style="float:right" href="/offers/{{leadId}}">Get Offers</a>
                        </li> -->
                  </ul>
               </div>
               <!-- /.box-body -->
            </div>
         </div>
         <div class="col-xs-9">
            <!-- /.box -->
            <?php
               if($this->session->flashdata('flash_message'))
               {
                   ?>
            <div class="alert alert-success alert-dismissible">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
               <h4><i class="icon fa fa-check"></i> Success</h4>
               <?php echo $this->session->flashdata('flash_message'); ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-xs-9 accordion"> Application List + </div>
         <div class="col-xs-9 panel">
            <table class="table table-bordered table-striped">
               <thead>
                  <th>Course</th>
                  <th>College</th>
                  <th>Degree</th>
                  <th>Duration</th>
                  <th>Total Fees</th>
               </thead>
               <tbody>
                  <?php
                     $i=1;
                     foreach($applied_universities as $university)
                     {
                         ?>
                  <tr class="<?=($selected_application == $university->id) ? 'selected-application' : ''?>">
                     <td><input type="radio" class="multichk" name="application" value="<?php echo $university->id;?>" <?=($selected_application == $university->id) ? 'checked' : ''?>></td>
                     <td><?php echo $university->name;?></td>
                     <td><?php echo $university->college_name;?></td>
                     <td><?php echo $university->course_name;?></td>
                     <td><?php echo $university->status;?></td>
                  </tr>
                  <?php
                     }
                     ?>
               </tbody>
            </table>
         </div>
         <div class="col-xs-9 accordion"> Session List + </div>
         <div class="col-xs-9 panel">
            <table  class="table table-bordered table-striped">
               <thead>
                  <th>University Name</th>
                  <th>slot 1</th>
                  <th>slot 2</th>
                  <th>slot 3</th>
                  <th>status</th>
               </thead>
               <tbody>
                  <?php
                     foreach($sessions_list as $sessions)
                     {
                         ?>
                  <tr >
                     <td><?php echo $sessions['name'];?></td>
                     <td><?php echo $sessions['slot_1'];?></td>
                     <td><?php echo $sessions['slot_1'];?></td>
                     <td><?php echo $sessions['slot_1'];?></td>
                     <td><?php echo $sessions['status'];?></td>
                  </tr>
                  <?php
                     }
                     ?>
               </tbody>
            </table>
         </div>
         <div class="col-xs-9 accordion"> Webinar List + </div>
         <div class="col-xs-9 panel">
            <table class="table table-bordered table-striped">
               <thead>
                  <th> Name</th>
                  <th> By </th>
                  <th> Date Time</th>
                  <th> Attended </th>
               </thead>
               <tbody>
                  <?php
                     foreach($webinar_list as $webinars)
                     {
                         ?>
                  <tr >
                     <td><?php echo $webinars['webinar_name'];?></td>
                     <td><?php echo $webinars['webinar_host_name'];?></td>
                     <td><?php echo $webinars['webinar_date']." - ".$webinars['webinar_time'];?></td>
                     <td><?php echo $webinars['webinar_attented'] ? "YES" : "NO" ; ?></td>
                  </tr>
                  <?php
                     }
                     ?>
               </tbody>
            </table>
         </div>
         <div class="col-xs-9 accordion"> Favourites List + </div>
         <div class="col-xs-9 panel">
            <table class="table table-bordered table-striped">
               <thead>
                  <th> Name </th>
                  <th> Type </th>
                  <th> Date </th>
               </thead>
               <tbody>
                  <?php
                     foreach($favourites as $favourite)
                     {
                         ?>
                  <tr >
                     <td><?php echo $favourite['name'];?></td>
                     <td><?php echo $favourite['entity_type'];?></td>
                     <td><?php echo $favourite['date_created'];?></td>
                  </tr>
                  <?php
                     }
                     ?>
               </tbody>
            </table>
         </div>
         <div class="col-xs-9 accordion"> Likes List + </div>
         <div class="col-xs-9 panel">
            <table id="example2" class="table table-bordered table-striped">
               <thead>
                  <th> Name </th>
                  <th> Type </th>
                  <th> Date </th>
               </thead>
               <tbody>
                  <?php
                     foreach($likes as $like)
                     {
                         ?>
                  <tr >
                     <td><?php echo $like['name'];?></td>
                     <td><?php echo $like['entity_type'];?></td>
                     <td><?php echo $like['date_created'];?></td>
                  </tr>
                  <?php
                     }
                     ?>
               </tbody>
            </table>
         </div>
         <div class="col-xs-9 accordion"> Views List + </div>
         <div class="col-xs-9 panel">
            <table id="example1" class="table table-bordered table-striped">
               <thead>
                  <th> Name </th>
                  <th> Type </th>
                  <th> Date </th>
               </thead>
               <tbody>
                  <?php
                     foreach($views as $view)
                     {
                         ?>
                  <tr >
                     <td><?php echo $view['name'];?></td>
                     <td><?php echo $view['entity_type'];?></td>
                     <td><?php echo $view['date_created'];?></td>
                  </tr>
                  <?php
                     }
                     ?>
               </tbody>
            </table>
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- <div class="row">
         <div class="col-md-3">
             <div class="box box-default">
                <div class="box-body box-profile">
                </div>
             </div>
         </div> -->
   </section>
   <!-- /.content -->
   <section class="mytabs container-fluid">
      <button class="tablink" onclick="openPage('Profile', this, '#f7f7f7')" id="defaultOpen">Profile</button>
      <button class="tablink" onclick="openPage('Documents', this, '#f7f7f7')">Documents</button>
      <button class="tablink" onclick="openPage('Applications', this, '#f7f7f7')" >Applications ( <?php if($applied_universities) { echo count($applied_universities); } else { echo "0"; } ?>) </button>
      <button class="tablink" onclick="openPage('Payments', this, '#f7f7f7')">Payments</button>
      <div id="Profile" class="tabcontent container-fluid">
         <div class="col-md-12 boxAppList" style="padding: 2% 2%;    background-color: white;">
            <ul class="nav nav-tabs insideUl" style="padding-top: 9px;padding-left: 9px;">
               <li class="active"><a data-toggle="tab" href="#home"><b class="tabName">
                  Personal Detail
                  </b></a>
               </li>
               <li><a data-toggle="tab" href="#qualification_detail"><b class="tabName">
                  Qualification Detail
                  </b></a>
               </li>
               <li><a data-toggle="tab" href="#desired_courses"><b class="tabName">
                  Desired Courses
                  </b></a>
               </li>
               <li><a data-toggle="tab" href="#parent_detail"><b class="tabName">
                  Parent Detail
                  </b></a>
               </li>

               <li><a data-toggle="tab" href="#work_exp"><b class="tabName">
                  Work Exp
                  </b></a>
               </li>

               <li><a data-toggle="tab" href="#emergency_contact_detail"><b class="tabName">
                  Emergency Contact Detail
                  </b></a>
               </li>
            </ul>
            <div class="tab-content">
               <div class="tab-pane active" id="home">
                  <form action="<?php echo base_url()?>student/index/save" class="form"  method="post"  style="background-color: white;">
                      <div class="form-group">
                        <div class="col-xs-6">
                           <label for="first_name">
                              <h5>Name</h5>
                           </label>
                           <?php
                              if($fid)
                              {
                                  ?>
                           <span class="form-control form_font_color input_field bRadius"><?php if($name) echo $name;?></span>
                           <?php
                              }
                              else
                              {
                                  ?>
                           <input type="text" name="name" placeholder=""  class="form-control" value="<?php if($name) echo $name;?>" />
                           <?php
                              }
                              ?>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="phone">
                              <h5>Mobile</h5>
                           </label>
                           <input type="text" class="form-control" name="phone" id="student_phone" placeholder="Mobile" value="<?=$phone?>">
                           <input type="hidden" class="form-control" name="old_phone" placeholder="Mobile" value="<?=$phone?>">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="email">
                              <h5>Email</h5>
                           </label>
                           <input type="email" class="form-control" name="email" id="student_email" placeholder="Email" value="<?=$email?>">
                           <input type="hidden" class="form-control" name="old_email" value="<?=$email?>">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6" style="margin: 10px 0px;">
                           <label for="dob">
                              <h5>Marital Status</h5>
                           </label>
                           <div class="col-md-12">
                              <label class="radio-inline">
                              <input type="radio" name="marital_status" value="1" <?=$marital_status == 1 ? 'checked' : ''?>>Yes
                              </label>
                              <label class="radio-inline">
                              <input type="radio" name="marital_status" value="0" <?=$marital_status == 0 ? 'checked' : ''?>>No
                              </label>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="address">
                              <h5>Place of Birth</h5>
                           </label>
                           <input type="text" class="form-control" name="place_of_birth" id="place_of_birth" placeholder="Address" value="<?=$place_of_birth?>">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="dob">
                              <h5>DOB</h5>
                           </label>
                           <input type="date" name="dob"  class="form-control" id="student_dob" value="<?=$dob?>"/>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="address">
                              <h5>Address</h5>
                           </label>
                           <input type="text" class="form-control" name="address" id="student_address" placeholder="Address" value="<?=$address?>">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="address">
                              <h5>Correspondence Address</h5>
                           </label>
                           <input type="text" class="form-control" name="correspondence_address" id="correspondence_address" placeholder="Address" value="<?=$correspondence_address?>">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="country">
                              <h5>Country</h5>
                           </label>
                           <select class="form-control" id="locationcountries" name="locationcountries">
                              <?php
                                 foreach($locationcountries as $location)
                                 {
                                     ?>
                              <option value="<?php echo $location->id;?>" <?php  if($location->id==$selected_country) echo 'selected="selected"'; ?> ><?php echo $location->name;?></option>
                              <?php
                                 }
                                 ?>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="state">
                              <h5>State</h5>
                           </label>
                           <?php
                              if (!$selected_state)
                              {
                                  ?>
                           <select class="form-control" id="state" name="state"></select>
                           <?php
                              }
                              else
                              {
                                  ?>
                           <select class="form-control" id="state" name="state">
                              <?php
                                 foreach($related_state_list as $location)
                                 {
                                     ?>
                              <option value="<?php echo $location->zone_id;?>" <?php  if($location->zone_id==$selected_state) echo 'selected="selected"'; ?> ><?php echo $location->name;?></option>
                              <?php
                                 }
                                 ?>
                           </select>
                           <?php
                              }
                              ?>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="city">
                              <h5>City</h5>
                           </label>
                           <?php
                              if (!$city)
                              {
                                  ?>
                           <select class="form-control" id="city" name="city"></select>
                           <?php
                              }
                              else
                              {
                                  ?>
                           <select class="form-control" id="city" name="city">
                              <?php
                                 foreach($related_city_list as $location)
                                 {
                                     ?>
                              <option value="<?php echo $location->id;?>" <?php  if($location->id==$city) echo 'selected="selected"'; ?> ><?php echo $location->city_name;?></option>
                              <?php
                                 }
                                 ?>
                           </select>
                           <?php
                              }
                              ?>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="zipcode">
                              <h5>Zipcode</h5>
                           </label>
                           <input type="text" class="form-control" name="zip" id="student_zip" placeholder="Zipcode" value="<?=$zip?>">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="hear_about_us">
                              <h5>Where Did You Hear About Us?</h5>
                           </label>
                           <select name="hear_info" id="student_hear_info" class="form-control">
                              <option value="0">Select Option</option>
                              <option value="Friends / Family" <?php if(isset($hear_about_us) && $hear_about_us == 'Friends / Family') echo 'selected="selected"';?> >Friends / Family</option>
                              <option value="Google / Facebook" <?php if(isset($hear_about_us) && $hear_about_us == 'Google / Facebook') echo 'selected="selected"';?>>Google / Facebook</option>
                              <option value="Stupidsid Seminar" <?php if(isset($hear_about_us) && $hear_about_us == 'Stupidsid Seminar') echo 'selected="selected"';?>>Stupidsid Seminar</option>
                              <option value="Stupidsid Website" <?php if(isset($hear_about_us) && $hear_about_us == 'Stupidsid Website') echo 'selected="selected"';?>>Stupidsid Website</option>
                              <option value="College Seminar" <?php if(isset($hear_about_us) && $hear_about_us == 'College Seminar') echo 'selected="selected"';?>>College Seminar</option>
                              <option value="News Paper" <?php if(isset($hear_about_us) && $hear_about_us == 'News Paper') echo 'selected="selected"';?>>News Paper</option>
                              <option value="Calling from Imperial" <?php if(isset($hear_about_us) && $hear_about_us == 'Calling from Imperial') echo 'selected="selected"';?>>Calling from Imperial</option>
                              <option value="Shiksha" <?php if(isset($hear_about_us) && $hear_about_us == 'Shiksha') echo 'selected="selected"';?>>Shiksha</option>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6" style="    margin: 10px 0px;">
                           <label for="gender">
                              <h5>Gender</h5>
                           </label>
                           <div class="col-md-12">
                              <label class="radio-inline">
                              <input type="radio" name="gender" value="M" <?=$gender == 'M' ? 'checked' : ''?>>Male
                              </label>
                              <label class="radio-inline">
                              <input type="radio" name="gender" value="F" <?=$gender == 'F' ? 'checked' : ''?>>Female
                              </label>
                              <label class="radio-inline">
                              <input type="radio" name="gender" value="O" <?=$gender == 'O' ? 'checked' : ''?>>Other
                              </label>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6" style="margin: 10px 0px;" >
                           <label for="passport_availalibility">
                              <h5>Do you have passport?</h5>
                           </label>
                           <div class="col-md-12">
                              <label class="radio-inline">
                              <input type="radio" name="passport_availalibility" value="1" id="chk-Yes" <?=$passport == '1' ? 'checked' : ''?> />Yes
                              </label>
                              <label class="radio-inline">
                              <input type="radio" name="passport_availalibility" value="0" id="chk-No" <?=$passport == '0' ? 'checked' : ''?> />No
                              </label>
                           </div>
                        </div>
                     </div>
                     <div id="passport_group" style="display: <?=$passport == '0' ? 'none' : 'block'?>">
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="passport_availalibility">
                              <h5>Passport Number</h5>
                           </label>
                              <input type="text" class="form-control" name="passport_number" id="passport_number" placeholder="Passport Number" value="<?=$passport_number?>">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="passport_availalibility">
                              <h5>Passport Issue Date</h5>
                           </label>
                           <input type="date" name="passport_issue_date"  class="form-control" id="passport_issue_date" value="<?=$passport_issue_date?>"/>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="passport_availalibility">
                              <h5>Passport Expiry Date</h5>
                           </label>
                           <input type="date" name="passport_expiry_date"  class="form-control" id="passport_expiry_date" value="<?=$passport_expiry_date?>"/>
                        </div>
                     </div>
                   </div>
                     <div class="form-group">
                        <div class="col-xs-12">
                           <br>
                           <input type="hidden" name="form_type" value="personal_details" />
                           <input type="hidden" name="student_id" value="<?=$student_id?>" />
                           <input type="hidden" name="user_id" value="<?=$id?>" />
                           <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                           <button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                        </div>
                     </div>


                     <!-- <div class="update_button">
                        <input type="hidden" name="form_type" value="personal_details" />
                        <input type="hidden" name="student_id" value="<?=$student_id?>" />
                        <input type="hidden" name="user_id" value="<?=$id?>" />
                        <button type="submit" class="button_sign_in btn btn-md btn-primary">UPDATE</button>
                     </div> -->
                  </form>
               </div>
               <div class="tab-pane" id="qualification_detail">
                  <form class="form" action="<?php echo base_url()?>student/index/save" method="post" id="qualification-detail-form">
                     <div class="form-group" id="competitive_exam">
                        <div class="col-xs-12">
                           <label for="competitive_exam">
                              <h5>Have you appeared any of the following exam?</h5>
                           </label>
                           <div class="col-md-12">
                              <label class="radio-inline">
                              <input type="radio" name="competitive_exam" <?php if($qualified_exams && $qualified_exams->competitive_exam_type == 'GRE') echo 'checked="checked"';?> id="gre" value="GRE">GRE
                              </label>
                              <label class="radio-inline">
                              <input type="radio" name="competitive_exam" <?php if($qualified_exams && $qualified_exams->competitive_exam_type == 'GMAT') echo 'checked="checked"';?> id="gmat" value="GMAT">GMAT
                              </label>
                              <label class="radio-inline">
                              <input type="radio" name="competitive_exam" <?php if($qualified_exams && $qualified_exams->competitive_exam_type == 'SAT') echo 'checked="checked"';?> id="sat" value="SAT">SAT
                              </label>
                              <label class="radio-inline">
                              <input type="radio" name="competitive_exam" <?php if($qualified_exams && $qualified_exams->competitive_exam_type == 'NO') echo 'checked="checked"';?> id="no_competitive_exam" value="NO">NO
                              </label>
                              <label class="radio-inline">
                              <input type="radio" name="competitive_exam" <?php if($qualified_exams && $qualified_exams->competitive_exam_type == 'NOT_REQUIRED') echo 'checked="checked"';?> id="not_required_competitive_exam" value="NOT_REQUIRED">NOT_REQUIRED
                              </label>
                           </div>
                        </div>
                        <span class="sep col-lg-12"></span>
                     </div>
                     <div class="form-group" id="exam_stage" <?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name']) echo 'style=display:block;'?>>
                        <div class="col-xs-12">
                           <label for="exam_stage">
                              <h5>Which stage of the exam have you reached?</h5>
                           </label>
                           <div class="col-md-12">
                              <label class="radio-inline">
                              <input type="radio" name="exam_stage" <?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'REGISTERED') echo 'checked="checked"';?> id="registered" value="REGISTERED">Registered
                              </label>
                              <label class="radio-inline">
                              <input type="radio" name="exam_stage" <?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_WAIT') echo 'checked="checked"';?> id="result_wait" value="RESULT_WAIT">Waiting For Result
                              </label>
                              <label class="radio-inline">
                              <input type="radio" name="exam_stage" <?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT') echo 'checked="checked"';?> id="result_out" value="RESULT_OUT">Have Result
                              </label>
                           </div>
                        </div>
                        <span class="sep col-lg-12"></span>
                     </div>
                     <div class="form-group" id="exam_date" <?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'REGISTERED') echo 'style=display:block;'?>>
                        <div class="col-xs-12">
                           <label for="exam_date">
                              <h5>Exam Date</h5>
                           </label>
                           <input type="date" name="exam_date" class="form-control" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'REGISTERED') echo $qualified_exams->competitive_exam_stage['value']?>"/>
                        </div>
                        <span class="sep col-lg-12"></span>
                     </div>
                     <div class="form-group" id="result_date" <?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_WAIT') echo 'style=display:block;'?>>
                        <div class="col-xs-12">
                           <label for="result_date">
                              <h5>Expected Result Date</h5>
                           </label>
                           <input type="date" name="result_date" class="form-control" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_WAIT') echo $qualified_exams->competitive_exam_stage['value']?>"/>
                        </div>
                        <span class="sep col-lg-12"></span>
                     </div>
                     <div class="form-group" id="exam_score" <?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT') echo 'style=display:block;' ?>>
                        <div class="col-xs-12" id="exam_score_gre" <?php if($qualified_exams && $qualified_exams->competitive_exam_type == 'GRE' && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT') echo 'style=display:block;' ?>>
                           <div class="col-xs-3">
                              <label for="exam_score_quant_gre">
                                 <h5>Quant</h5>
                              </label>
                              <input name="exam_score_quant_gre" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_quant_gre'])) echo $qualified_exams->competitive_exam_stage['exam_score_quant_gre'] ?>">
                           </div>
                           <div class="col-xs-3">
                              <label for="exam_score_verbal_gre">
                                 <h5>Verbal</h5>
                              </label>
                              <input name="exam_score_verbal_gre" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_verbal_gre'])) echo $qualified_exams->competitive_exam_stage['exam_score_verbal_gre'] ?>">
                           </div>
                           <div class="col-xs-3">
                              <label for="exam_score_awa_gre">
                                 <h5>AWA</h5>
                              </label>
                              <input name="exam_score_awa_gre" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_awa_gre'])) echo $qualified_exams->competitive_exam_stage['exam_score_awa_gre'] ?>">
                           </div>
                           <div class="col-xs-3">
                              <label for="exam_score_total_gre">
                                 <h5>Total</h5>
                              </label>
                              <input name="exam_score_total_gre" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_total_gre'])) echo $qualified_exams->competitive_exam_stage['exam_score_total_gre'] ?>">
                           </div>
                        </div>
                        <div class="col-xs-12" id="exam_score_gmat" <?php if($qualified_exams && $qualified_exams->competitive_exam_type == 'GMAT' && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT') echo 'style=display:block;' ?>>
                           <div class="col-xs-2">
                              <label for="exam_score_quant_gmat">
                                 <h5>Quant</h5>
                              </label>
                              <input name="exam_score_quant_gmat" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_quant_gmat'])) echo $qualified_exams->competitive_exam_stage['exam_score_quant_gmat'] ?>">
                           </div>
                           <div class="col-xs-2">
                              <label for="exam_score_verbal_gmat">
                                 <h5>Verbal</h5>
                              </label>
                              <input name="exam_score_verbal_gmat" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_verbal_gmat'])) echo $qualified_exams->competitive_exam_stage['exam_score_verbal_gmat'] ?>">
                           </div>
                           <div class="col-xs-2">
                              <label for="exam_score_ir">
                                 <h5>IR</h5>
                              </label>
                              <input name="exam_score_ir" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_ir'])) echo $qualified_exams->competitive_exam_stage['exam_score_ir'] ?>">
                           </div>
                           <div class="col-xs-3">
                              <label for="exam_score_awa_gmat">
                                 <h5>AWA</h5>
                              </label>
                              <input name="exam_score_awa_gmat" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_awa_gmat'])) echo $qualified_exams->competitive_exam_stage['exam_score_awa_gmat'] ?>">
                           </div>
                           <div class="col-xs-3">
                              <label for="exam_score_total_gmat">
                                 <h5>Total</h5>
                              </label>
                              <input name="exam_score_total_gmat" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_total_gmat'])) echo $qualified_exams->competitive_exam_stage['exam_score_total_gmat'] ?>">
                           </div>
                        </div>
                        <div class="col-xs-12" id="exam_score_sat" <?php if($qualified_exams && $qualified_exams->competitive_exam_type == 'SAT' && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT') echo 'style=display:block;' ?>>
                           <div class="col-xs-3">
                              <label for="exam_score_cirital_reading">
                                 <h5>Critical Reading</h5>
                              </label>
                              <input name="exam_score_cirital_reading" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_cirital_reading'])) echo $qualified_exams->competitive_exam_stage['exam_score_cirital_reading'] ?>">
                           </div>
                           <div class="col-xs-3">
                              <label for="exam_score_writing">
                                 <h5>Writing</h5>
                              </label>
                              <input name="exam_score_writing" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_writing'])) echo $qualified_exams->competitive_exam_stage['exam_score_writing'] ?>">
                           </div>
                           <div class="col-xs-3">
                              <label for="exam_score_math">
                                 <h5>Math</h5>
                              </label>
                              <input name="exam_score_math" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_math'])) echo $qualified_exams->competitive_exam_stage['exam_score_math'] ?>">
                           </div>
                           <div class="col-xs-3">
                              <label for="exam_score_total_sat">
                                 <h5>Total</h5>
                              </label>
                              <input name="exam_score_total_sat" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_total_sat'])) echo $qualified_exams->competitive_exam_stage['exam_score_total_sat'] ?>">
                           </div>
                        </div>
                        <span class="sep col-lg-12"></span>
                     </div>
                     <div class="form-group" id="english_proficiency_exam">
                        <div class="col-xs-12">
                           <label for="english_proficiency_exam">
                              <h5>Have you appeared any of the following exam?</h5>
                           </label>
                           <div class="col-md-12">
                              <label class="radio-inline">
                              <input type="radio" name="english_proficiency_exam" <?php if($qualified_exams && $qualified_exams->english_exam_type == 'TOEFL') echo 'checked="checked"';?> id="toefl" value="TOEFL">TOEFL
                              </label>
                              <label class="radio-inline">
                              <input type="radio" name="english_proficiency_exam" <?php if($qualified_exams && $qualified_exams->english_exam_type == 'IELTS') echo 'checked="checked"';?> id="ielts" value="IELTS">IELTS
                              </label>
                              <label class="radio-inline">
                              <input type="radio" name="english_proficiency_exam" <?php if($qualified_exams && $qualified_exams->english_exam_type == 'PTE') echo 'checked="checked"';?> id="pte" value="PTE">PTE
                              </label>
                              <label class="radio-inline">
                              <input type="radio" name="english_proficiency_exam" <?php if($qualified_exams && $qualified_exams->english_exam_type == 'NO') echo 'checked="checked"';?> id="no_english_proficiency_exam" value="NO">NO
                              </label>
                              <label class="radio-inline">
                              <input type="radio" name="english_proficiency_exam" <?php if($qualified_exams && $qualified_exams->english_exam_type == 'NOT_REQUIRED') echo 'checked="checked"';?> id="not_required_english_proficiency_exam" value="NOT_REQUIRED">NOT_REQUIRED
                              </label>
                           </div>
                        </div>
                        <span class="sep col-lg-12"></span>
                     </div>
                     <div class="form-group" id="english_exam_stage" <?php if($qualified_exams && $qualified_exams->english_exam_stage['name']) echo 'style=display:block;'?>>
                        <div class="col-xs-12">
                           <label for="english_exam_stage">
                              <h5>Which stage of the exam have you reached?</h5>
                           </label>
                           <div class="col-md-12">
                              <label class="radio-inline">
                              <input type="radio" name="english_exam_stage" <?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'REGISTERED') echo 'checked="checked"';?> id="english_registered" value="REGISTERED">Registered
                              </label>
                              <label class="radio-inline">
                              <input type="radio" name="english_exam_stage" <?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_WAIT') echo 'checked="checked"';?> id="english_result_wait" value="RESULT_WAIT">Waiting For Result
                              </label>
                              <label class="radio-inline">
                              <input type="radio" name="english_exam_stage" <?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT') echo 'checked="checked"';?> id="english_result_out" value="RESULT_OUT">Have Result
                              </label>
                           </div>
                        </div>
                        <span class="sep col-lg-12"></span>
                     </div>
                     <div class="form-group" id="english_exam_date" <?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'REGISTERED') echo 'style=display:block;'?>>
                        <div class="col-xs-12">
                           <label for="english_exam_date">
                              <h5>Exam Date</h5>
                           </label>
                           <input type="date" name="english_exam_date" class="form-control" value="<?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'REGISTERED') echo $qualified_exams->english_exam_stage['value']?>"/>
                        </div>
                        <span class="sep col-lg-12"></span>
                     </div>
                     <div class="form-group" id="english_result_date" <?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_WAIT') echo 'style=display:block;'?>>
                        <div class="col-xs-12">
                           <label for="english_result_date">
                              <h5>Expected Result Date</h5>
                           </label>
                           <input type="date" name="english_result_date" class="form-control" value="<?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_WAIT') echo $qualified_exams->english_exam_stage['value']?>"/>
                        </div>
                        <span class="sep col-lg-12"></span>
                     </div>
                     <div class="form-group" id="english_exam_score" <?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT') echo 'style=display:block;' ?>>
                        <div class="col-xs-12" id="english_exam_score_all">
                           <div class="col-xs-2">
                              <label for="english_exam_score_reading">
                                 <h5>Reading</h5>
                              </label>
                              <input name="english_exam_score_reading" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_reading']) echo $qualified_exams->english_exam_stage['english_exam_score_reading'] ?>">
                           </div>
                           <div class="col-xs-2">
                              <label for="english_exam_score_listening">
                                 <h5>Listening</h5>
                              </label>
                              <input name="english_exam_score_listening" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_listening']) echo $qualified_exams->english_exam_stage['english_exam_score_listening'] ?>">
                           </div>
                           <div class="col-xs-2">
                              <label for="english_exam_score_writing">
                                 <h5>Writing</h5>
                              </label>
                              <input name="english_exam_score_writing" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_writing']) echo $qualified_exams->english_exam_stage['english_exam_score_writing'] ?>">
                           </div>
                           <div class="col-xs-3">
                              <label for="english_exam_score_speaking">
                                 <h5>Speaking</h5>
                              </label>
                              <input name="english_exam_score_speaking" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_speaking']) echo $qualified_exams->english_exam_stage['english_exam_score_speaking'] ?>">
                           </div>
                           <div class="col-xs-3">
                              <label for="english_exam_score_total">
                                 <h5>Total</h5>
                              </label>
                              <input name="english_exam_score_total" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_total']) echo $qualified_exams->english_exam_stage['english_exam_score_total'] ?>">
                           </div>
                        </div>
                        <span class="sep col-lg-12"></span>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-12">
                           <label for="secondary_qualification">
                              <h5>Secondary Qualification</h5>
                           </label>
                           <table style="width: 100%;">
                              <th><label style="font-weight: 600;">Qualification</label></th>
                              <th><label style="font-weight: 600;">Institution Name</label></th>
                              <th><label style="font-weight: 600;">Board</label></th>
                              <th><label style="font-weight: 600;">Year Started</label></th>
                              <th><label style="font-weight: 600;">Year Finished</label></th>
                              <th><label style="font-weight: 600;">Grade/ %</label></th>
                              <th><label style="font-weight: 600;">Out Of Total</label></th>
                              <tbody>
                                 <tr>
                                    <td><label>Grade X</label></</td>
                                    <td><input type="text" name="secondary_institution_name" id="secondary_institution_name" class="form-control" value="<?php if($secondaryqualification_details !='') echo $secondaryqualification_details->institution_name; ?>"/></td>
                                    <td><input type="text" name="board" id="board" class="form-control" value="<?php if($secondaryqualification_details !='') echo $secondaryqualification_details->board; ?>"/></td>
                                    <td>
                                       <select class="form-control" id="secondary_year_starrted" name="secondary_year_starrted">
                                          <?php
                                             for($i=1980;$i<=2030;$i++)
                                             {
                                                 ?>
                                          <option value="<?php echo $i;?>" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->year_started == $i) echo 'selected="selected"';?> ><?php echo $i;?></option>
                                          <?php
                                             }?>
                                       </select>
                                    </td>
                                    <td>
                                       <select class="form-control" id="secondary_year_finished" name="secondary_year_finished">
                                          <?php
                                             for($i=1980;$i<=2030;$i++)
                                             {
                                                 ?>
                                          <option value="<?php echo $i;?>" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->year_finished == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
                                          <?php
                                             }
                                             ?>
                                       </select>
                                    </td>
                                    <td><input type="text" name="secondary_grade" id="secondary_grade" class="form-control" value="<?php if($secondaryqualification_details !='') echo $secondaryqualification_details->grade; ?>"/></td>
                                    <td>
                                       <select class="form-control" id="secondary_total" name="secondary_total">
                                          <option value="100" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 100) echo 'selected="selected"';?>>100</option>
                                          <option value="10"  <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 10) echo 'selected="selected"';?>>10</option>
                                          <option value="7"  <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 7) echo 'selected="selected"';?>>7</option>
                                          <option value="4"  <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 4) echo 'selected="selected"';?>>4</option>
                                       </select>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                        <span class="sep col-lg-12"></span>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-12">
                           <label for="diploma_12">
                              <h5>Have you done Diploma or 12th After School?</h5>
                           </label>
                           <div class="col-md-12">
                              <label class="radio-inline">
                              <input type="radio" name="diploma_12" <?php if($diplomaqualification_details !='') echo 'checked="checked"';?> id="diploma" value="1">Diploma
                              </label>
                              <label class="radio-inline">
                              <input type="radio" name="diploma_12" <?php if($highersecondaryqualification_details !='') echo 'checked="checked"';?> id="12th" value="2">12TH
                              </label>
                           </div>
                        </div>
                        <span class="sep col-lg-12"></span>
                     </div>
                     <div class="form-group diploma_hs" id="12thidd" <?php if($highersecondaryqualification_details !=''){?> style="display:block;"<?php }else{?>style="display:none;"<?php }?>>
                        <div class="col-xs-12">
                           <label for="higher_secondary_qualification">
                              <h5>Higher Secondary Qualification</h5>
                           </label>
                           <table style="width: 100%;">
                              <th><label style="font-weight: 600;">Qualification</label></th>
                              <th><label style="font-weight: 600;">Institution Name</label></th>
                              <th><label style="font-weight: 600;">Board</label></th>
                              <th><label style="font-weight: 600;">Year Started</label></th>
                              <th><label style="font-weight: 600;">Year Finished</label></th>
                              <th><label style="font-weight: 600;">Grade/ %</label></th>
                              <th><label style="font-weight: 600;">Out Of Total</label></th>
                              <tbody>
                                 <tr>
                                    <td><label>Grade XII</label></</td>
                                    <td><input type="text" name="hs_institution_name" id="hs_institution_name" class="form-control" value="<?php if($highersecondaryqualification_details !='') echo $highersecondaryqualification_details->institution_name; ?>"/></td>
                                    <td><input type="text" name="hs_board" id="hs_board" class="form-control" value="<?php if($highersecondaryqualification_details !='') echo $highersecondaryqualification_details->board; ?>"/></td>
                                    <td>
                                       <select class="form-control" id="hs_year_starrted" name="hs_year_starrted">
                                          <?php
                                             for($i=1980;$i<=2030;$i++)
                                             {
                                                 ?>
                                          <option value="<?php echo $i;?>" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->year_started == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
                                          <?php
                                             }
                                             ?>
                                       </select>
                                    </td>
                                    <td>
                                       <select class="form-control" id="hs_year_finished" name="hs_year_finished">
                                          <?php
                                             for($i=1980;$i<=2030;$i++)
                                             {
                                                 ?>
                                          <option value="<?php echo $i;?>" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->year_finished == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
                                          <?php
                                             }
                                             ?>
                                       </select>
                                    </td>
                                    <td><input type="text" name="hs_grade" id="hs_grade" class="form-control" value="<?php if($highersecondaryqualification_details !='') echo $highersecondaryqualification_details->grade; ?>"/></td>
                                    <td>
                                       <select class="form-control" id="hs_total" name="hs_total">
                                          <option value="100" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 100) echo 'selected="selected"';?>>100</option>
                                          <option value="10" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 10) echo 'selected="selected"';?>>10</option>
                                          <option value="7" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 7) echo 'selected="selected"';?>>7</option>
                                          <option value="4" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 4) echo 'selected="selected"';?>>4</option>
                                       </select>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                        <span class="sep col-lg-12"></span>
                     </div>
                     <div class="form-group diploma_hs" id="diploma_id" <?php if($diplomaqualification_details !=''){?> style="display:block;"<?php }else{?>style="display:none;"<?php }?>>
                        <div class="col-xs-12">
                           <label for="diploma">
                              <h5>Diploma</h5>
                           </label>
                           <table style="width: 100%;">
                              <th><label style="font-weight: 600;">Name of Course</label></th>
                              <th><label style="font-weight: 600;">College  Name</label></th>
                              <th><label style="font-weight: 600;">Year Started</label></th>
                              <th><label style="font-weight: 600;">Year Finished</label></th>
                              <th><label style="font-weight: 600;">Aggregate Percentage (1 to 6 Sem)</label></th>
                              <th><label style="font-weight: 600;">Out Of Total</label></th>
                              <tbody>
                                 <tr>
                                    <td><input type="text" name="diploma_course_name" id="diploma_course_name" class="form-control" value="<?php if($diplomaqualification_details !='') echo $diplomaqualification_details->course; ?>"/></</td>
                                    <td><input type="text" name="diploma_institution_name" id="diploma_institution_name" class="form-control" value="<?php if($diplomaqualification_details !='') echo $diplomaqualification_details->college; ?>"/></td>
                                    <td>
                                       <select class="form-control" id="diploma_year_starrted" name="diploma_year_starrted">
                                          <?php
                                             for($i=1980;$i<=2030;$i++)
                                             {
                                                 ?>
                                          <option value="<?php echo $i;?>" <?php if($diplomaqualification_details !='' && $diplomaqualification_details->year_started == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
                                          <?php
                                             }
                                             ?>
                                       </select>
                                    </td>
                                    <td>
                                       <select class="form-control" id="diploma_year_finished" name="diploma_year_finished">
                                          <?php
                                             for($i=1980;$i<=2030;$i++)
                                             {
                                                 ?>
                                          <option value="<?php echo $i;?>" <?php if($diplomaqualification_details !='' && $diplomaqualification_details->year_finished == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
                                          <?php
                                             }
                                             ?>
                                       </select>
                                    </td>
                                    <td><input type="text" name="diploma_grade" id="diploma_grade" class="form-control" value="<?php if($diplomaqualification_details !='') echo $diplomaqualification_details->aggregate; ?>"/></td>
                                    <td>
                                       <select class="form-control" id="diploma_total" name="diploma_total">
                                          <option value="100" <?php if($diplomaqualification_details !='' && $diplomaqualification_details->total == 100) echo 'selected="selected"';?>>100</option>
                                          <option value="10" <?php if($diplomaqualification_details !='' && $diplomaqualification_details->total == 10) echo 'selected="selected"';?>>10</option>
                                          <option value="7" <?php if($diplomaqualification_details !='' && $diplomaqualification_details->total == 7) echo 'selected="selected"';?>>7</option>
                                          <option value="4" <?php if($diplomaqualification_details !='' && $diplomaqualification_details->total == 10) echo 'selected="selected"';?>>4</option>
                                       </select>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                        <span class="sep col-lg-12"></span>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-12">
                           <label for="bachelor_graduation">
                              <h5>Bachelor's/Graduation</h5>
                           </label>
                           <table style="width: 100%;">
                              <th><label style="font-weight: 600;">Name of Course</label></th>
                              <th><label style="font-weight: 600;">University</label></th>
                              <th><label style="font-weight: 600;">College  Name</label></th>
                              <th><label style="font-weight: 600;">Major</label></th>
                              <th><label style="font-weight: 600;">Year Started</label></th>
                              <th><label style="font-weight: 600;">Year Finished</label></th>
                              <th><label style="font-weight: 600;">Grade / %</label></th>
                              <th><label style="font-weight: 600;">Out Of Total</label></th>
                              <tbody>
                                 <tr>
                                    <td width="15%"><input type="text" name="bachelor_course" id="bachelor_course" class="form-control" value="<?php if($bachelorqualification_details !='') echo $bachelorqualification_details->course; ?>"/></td>
                                    <td width="15%"><input type="text" name="bachelor_university" id="bachelor_university" class="form-control" value="<?php if($bachelorqualification_details !='') echo $bachelorqualification_details->university; ?>"/></td>
                                    <td width="15%"><input type="text" name="bachelor_college" id="bachelor_college" class="form-control" value="<?php if($bachelorqualification_details !='') echo $bachelorqualification_details->college; ?>"/></td>
                                    <td width="15%"><input type="text" name="bachelor_major" id="bachelor_major" class="form-control" value="<?php if($bachelorqualification_details !='') echo $bachelorqualification_details->major; ?>"/></td>
                                    <td width="10%">
                                       <select class="form-control" id="bachelor_year_starrted" name="bachelor_year_starrted">
                                          <?php
                                             for($i=1980;$i<=2030;$i++)
                                             {
                                                 ?>
                                          <option value="<?php echo $i;?>" <?php if($bachelorqualification_details !='' && $bachelorqualification_details->year_started == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
                                          <?php
                                             }
                                             ?>
                                       </select>
                                    </td>
                                    <td width="10%">
                                       <select class="form-control" id="bachelor_year_finished" name="bachelor_year_finished">
                                          <?php
                                             for($i=1980;$i<=2030;$i++)
                                             {
                                                 ?>
                                          <option value="<?php echo $i;?>" <?php if($bachelorqualification_details !='' && $bachelorqualification_details->year_finished == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
                                          <?php
                                             }
                                             ?>
                                       </select>
                                    </td>
                                    <td width="10%"><input type="text" name="bachelor_grade" id="bachelor_grade" class="form-control" value="<?php if($bachelorqualification_details !='') echo $bachelorqualification_details->grade;?>"/></td>
                                    <td width="10%">
                                       <select class="form-control" id="bachelor_total" name="bachelor_total">
                                          <option value="100" <?php if($bachelorqualification_details !='' && $bachelorqualification_details->total == 100) echo 'selected="selected"';?>>100</option>
                                          <option value="10" <?php if($bachelorqualification_details !='' && $bachelorqualification_details->total == 10) echo 'selected="selected"';?>>10</option>
                                          <option value="7" <?php if($bachelorqualification_details !='' && $bachelorqualification_details->total == 7) echo 'selected="selected"';?>>7</option>
                                          <option value="4" <?php if($bachelorqualification_details !='' && $bachelorqualification_details->total == 4) echo 'selected="selected"';?>>4</option>
                                       </select>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                        <span class="sep col-lg-12"></span>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-12">
                           <label for="masters">
                              <h5>Have you done Master?</h5>
                           </label>
                           <div class="col-md-12">
                              <label class="radio-inline">
                              <input type="radio" name="masters" <?php if($mastersqualification_details !='') echo 'checked="checked"';?> id="master_yes" value="1">Yes
                              </label>
                              <label class="radio-inline">
                              <input type="radio" name="masters" <?php if($mastersqualification_details =='') echo 'checked="checked"';?> id="master_no" value="2">No
                              </label>
                           </div>
                        </div>
                        <span class="sep col-lg-12"></span>
                     </div>
                     <div class="form-group" id="masters_id" <?php if($mastersqualification_details !=''){?> style="display:block;"<?php }else{?>style="display:none;"<?php }?>>
                        <div class="col-xs-12">
                           <label for="master_degree">
                              <h5>Master's</h5>
                           </label>
                           <table style="width: 100%;">
                              <th><label style="font-weight: 600;">Name of Course</label></th>
                              <th><label style="font-weight: 600;">University</label></th>
                              <th><label style="font-weight: 600;">College  Name</label></th>
                              <th><label style="font-weight: 600;">Major</label></th>
                              <th><label style="font-weight: 600;">Year Started</label></th>
                              <th><label style="font-weight: 600;">Year Finished</label></th>
                              <th><label style="font-weight: 600;">Grade / %</label></th>
                              <th><label style="font-weight: 600;">Out Of Total</label></th>
                              <tbody>
                                 <tr>
                                    <td width="15%"><input type="text" name="master_course" id="master_course" class="form-control" style="width:85%;" value="<?php if($mastersqualification_details !='') echo $mastersqualification_details->course; ?>"/></td>
                                    <td width="15%"><input type="text" name="master_university" id="master_university" class="form-control" style="width:85%;" value="<?php if($mastersqualification_details !='') echo $mastersqualification_details->university; ?>"/></td>
                                    <td width="15%"><input type="text" name="master_college" id="master_college" class="form-control" style="width:85%;" value="<?php if($mastersqualification_details !='') echo $mastersqualification_details->college; ?>"/></td>
                                    <td width="15%"><input type="text" name="master_major" id="master_major" class="form-control" style="width:85%;" value="<?php if($mastersqualification_details !='') echo $mastersqualification_details->major; ?>"/></td>
                                    <td width="10%">
                                       <select class="form-control" id="master_year_started" name="master_year_started">
                                          <?php
                                             for($i=1980;$i<=2030;$i++)
                                             {
                                                 ?>
                                          <option value="<?php echo $i;?>" <?php if($mastersqualification_details !='' && $mastersqualification_details->year_started == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
                                          <?php
                                             }
                                             ?>
                                       </select>
                                    </td>
                                    <td width="10%">
                                       <select class="form-control" id="master_year_finished" name="master_year_finished">
                                          <?php
                                             for($i=1980;$i<=2030;$i++)
                                             {
                                                 ?>
                                          <option value="<?php echo $i;?>" <?php if($mastersqualification_details !='' && $mastersqualification_details->year_finished == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
                                          <?php
                                             }
                                             ?>
                                       </select>
                                    </td>
                                    <td width="10%"><input type="text" name="master_grade" id="master_grade" class="form-control" style="width:85%;" value="<?php if($mastersqualification_details !='') echo $mastersqualification_details->grade; ?>" /></td>
                                    <td width="10%">
                                       <select class="form-control" id="master_total" name="master_total">
                                          <option value="100" <?php if($mastersqualification_details !='' && $mastersqualification_details->total == 100) echo 'selected="selected"';?>>100</option>
                                          <option value="10" <?php if($mastersqualification_details !='' && $mastersqualification_details->total == 10) echo 'selected="selected"';?>>10</option>
                                          <option value="7" <?php if($mastersqualification_details !='' && $mastersqualification_details->total == 7) echo 'selected="selected"';?>>7</option>
                                          <option value="4" <?php if($mastersqualification_details !='' && $mastersqualification_details->total == 4) echo 'selected="selected"';?>>4</option>
                                       </select>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                        <span class="sep col-lg-12"></span>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-12">
                           <br>
                           <input type="hidden" name="form_type" value="qualification_details" />
                           <input type="hidden" name="student_id" value="<?=$student_id?>" />
                           <input type="hidden" name="user_id" value="<?=$id?>" />

                           <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                           <button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                        </div>
                     </div>

                  </form>
               </div>
               <div class="tab-pane" id="desired_courses">
                 <?php
                    $destination = array();
                    $destination = explode(",",$desired_destination);
                    ?>

                  <form class="form" action="<?php echo base_url()?>student/index/save" method="post" id="desired-courses-form">
                     <div class="form-group">
                        <div class="col-md-12">
                           <label for="desired_destination">
                              <h5>Desired Destination</h5>
                           </label>
                        </div>
                        <div class="col-md-12">
                           <?php
                              foreach($countries as $val)
                              {
                                  ?>
                           <div class="col-md-4">
                              <div class="row" style="display: flex;">
                                 <div class="col-md-8">
                                    <label>
                                       <h6><?php echo $val->name;?></h6>
                                    </label>
                                 </div>
                                 <div class="col-md-4">
                                    <input type="checkbox" class="checkBoxDesign" name="deisred_destinations[]" value="<?php echo $val->id;?>" <?php if(isset($destination) && in_array($val->id, $destination)) echo 'checked="checked"';?>/>
                                 </div>
                                 <!-- <label><?php echo $val->name;?></label>&nbsp;&nbsp;&nbsp;&nbsp; -->
                              </div>
                           </div>
                           <?php
                              }
                              ?>
                        </div>
                        <span class="sep col-lg-12"></span>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-12">
                           <label for="desired_courses">
                              <h5>Desired Course</h5>
                           </label>
                           <div class="col-md-12 form-group">

                             <div class="col-md-6">
                                 <label for="inputEmail3" class="text-uppercase"> Desired Mainstream &nbsp;&nbsp; </label>

                                <select name="desired_course_name" id="desired_course_name" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>

                                  <option value="0">Select Mainstream</option>

                                  <?php
                                  $selectedCource = "";
                                  foreach($mainstreams as $course){
                                    if($course->id == $desiredcourse_details->desired_course_name){ $selectedCource = "selected" ; } else { $selectedCource="";}
                                    ?>

                                  <option value="<?php echo $course->id;?>" <?php echo $selectedCource; ?> ><?php echo strtoupper($course->name);?></option>

                                  <?php } ?>

                                </select>
                             </div>

                             <div class="col-md-6">
                                <label for="inputEmail3" class="text-uppercase"> Desired Substream &nbsp;&nbsp; </label>

                                 <select name="desired_sub_course_name" id="desired_sub_course_name" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>

                                   <option value="0">Select Substream </option>
                                   <?php
                                   $selectedSubCource = "";
                                   foreach($submainstreams as $subcourse){
                                     if($subcourse->id == $desiredcourse_details->desired_sub_course_name) { $selectedSubCource = "selected" ; } else { $selectedSubCource="";}
                                     ?>

                                   <option value="<?php echo $subcourse->id;?>" <?php echo $selectedSubCource; ?> ><?php echo strtoupper($subcourse->name);?></option>

                                   <?php } ?>
                                 </select>
                             </div>

                           </div>


                            <div class="col-md-12 form-group">

                             <div class="col-md-6">
                                <label for="inputEmail3" class="text-uppercase"> Desired Level Of Course &nbsp;&nbsp; </label>

                                      <select name="desired_course_level" id="desired_course_level" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>

                                        <option value="0">Select Level Of Course</option>

                                                      <?php
                                                      $selecteddegree = "";
                                                      foreach($degrees as $degree){
                                                        if($degree->id == $desiredcourse_details->desired_course_level) { $selecteddegree = "selected" ; } else { $selecteddegree="";}
                                                        ?>

                                      <option value="<?php echo $degree->id;?>" <?php echo $selecteddegree; ?> ><?php echo strtoupper($degree->name);?></option>

                                      <?php } ?>

                                      </select>
                             </div>

                             <div class="col-md-6">
                                <label for="inputEmail3" class="text-uppercase"> Desired Intake Month &nbsp;&nbsp; </label>

                                 <select name="desired_course_intake" id="desired_course_intake" class="form-control" style="width:100%; height:30px; font-size:12px;" required>

                                   <option value="0">Select Intake Month</option>

                                   <?php
                                   $slectedMonth = "";
                                   foreach($intake_month as $month){
                                     if($month->id == $desiredcourse_details->desired_course_intake) { $slectedMonth = "selected" ; } else { $slectedMonth="";}
                                     ?>

                                 <option value="<?php echo $month->id;?>" <?php echo $slectedMonth; ?> ><?php echo strtoupper($month->name);?></option>

                                 <?php } ?>

                                 </select>
                             </div>

                             </div>

                             <div class="col-md-12 form-group">

                              <div class="col-md-6">
                                 <label for="inputEmail3" class="text-uppercase"> Desired Intake Year &nbsp;&nbsp; </label>

                                       <select name="desired_course_year" id="desired_course_year" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>

                                         <option value="0">Select Intake Year</option>

                                    <?php
                                    $slectedYear = "";
                                    foreach($intake_year as $year){
                                      if($year->id == $desiredcourse_details->desired_course_year) { $slectedYear = "selected" ; } else { $slectedYear="";}
                                      ?>

                                       <option value="<?php echo $year->id;?>" <?php echo $slectedYear; ?> ><?php echo strtoupper($year->name);?></option>

                                       <?php } ?>

                                       </select>
                              </div>

                              </div>

                        </div>
                        <span class="sep col-lg-12"></span>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-12">
                           <br>
                           <input type="hidden" name="form_type" value="desired_courses" />
                           <input type="hidden" name="student_id" value="<?=$student_id?>" />
                           <input type="hidden" name="user_id" value="<?=$id?>" />
                           <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                           <button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                        </div>
                     </div>

                  </form>
               </div>
               <div class="tab-pane" id="parent_detail">
                  <form class="form" action="<?php echo base_url()?>student/index/save" method="post" id="parent-detail-form">
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="parent_name">
                              <h5>Parent Name</h5>
                           </label>
                           <input type="text" class="form-control" name="parent_name" id="parent_name" placeholder="Parent Name" value="<?php if($parent_name) echo $parent_name;?>">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="parent_occupation">
                              <h5>Parent Occupation</h5>
                           </label>
                           <input type="text" class="form-control" name="parent_occupation" id="parent_occupation" placeholder="Parent Occupation" value="<?php if($parent_occupation) echo $parent_occupation;?>">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="parent_mob">
                              <h5>Parent Mobile</h5>
                           </label>
                           <input type="text" class="form-control" name="parent_mob" id="parent_mob" placeholder="Parent Mobile" value="<?php if($parent_mob) echo $parent_mob;?>">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-12">
                           <br>
                           <input type="hidden" name="form_type" value="parent_details" />
                           <input type="hidden" name="user_id" value="<?=$id?>" />
                           <input type="hidden" name="student_id" value="<?=$student_id?>" />
                           <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                           <button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                        </div>
                     </div>

                  </form>
               </div>

               <div class="tab-pane" id="work_exp">
                  <hr>
                  <form class="form" action="<?php echo base_url()?>student/index/save" method="post" id="work-detail-form">
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="parent_name">
                              <h5>Company Name</h5>
                           </label>
                           <input type="text" class="form-control" name="company_name" id="company_name" placeholder="Company Name" value="<?php if($company_name) echo $company_name;?>">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="parent_occupation">
                              <h5>Designation</h5>
                           </label>
                           <input type="text" class="form-control" name="designation" id="designation" placeholder="Designation" value="<?php if($designation) echo $designation;?>">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="parent_mob">
                              <h5>Start Date</h5>
                           </label>
                           <input type="date" name="work_start_date"  class="form-control" id="work_start_date" value="<?=$work_start_date?>"/>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="parent_mob">
                              <h5>End Date</h5>
                           </label>
                           <input type="date" name="work_end_date"  class="form-control" id="work_end_date" value="<?=$work_end_date?>"/>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-12">
                           <br>
                           <input type="hidden" name="form_type" value="work_exp" />
                           <input type="hidden" name="user_id" value="<?=$id?>" />
                           <input type="hidden" name="student_id" value="<?=$student_id?>" />
                           <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                           <button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                        </div>
                     </div>

                  </form>
               </div>

               <div class="tab-pane" id="emergency_contact_detail">
                  <hr>
                  <form class="form" action="<?php echo base_url()?>student/index/save" method="post" id="Emergency-Contact-Details">
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="parent_name">
                              <h5> Name</h5>
                           </label>
                           <input type="text" class="form-control" name="e_name" id="e_name" placeholder="Name" value="<?=$e_name?>">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="parent_occupation">
                              <h5>Mobile Number</h5>
                           </label>
                           <input type="text" class="form-control" name="e_mobile_number" id="e_mobile_number" placeholder="Mobile Number" value="<?=$e_mobile_number?>">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="parent_mob">
                              <h5>Email-ID</h5>
                           </label>
                           <input type="text" class="form-control" name="e_email" id="e_email" placeholder="Email" value="<?=$e_email?>">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-6">
                           <label for="parent_mob">
                              <h5>Relation With The Applicant </h5>
                           </label>
                           <input type="text" class="form-control" name="e_relation" id="e_relation" placeholder="Relation ex.- Father, Brother etc" value="<?=$e_relation?>">
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-12">
                           <br>
                           <input type="hidden" name="form_type" value="emergency_contact_detail" />
                           <input type="hidden" name="user_id" value="<?=$id?>" />
                           <input type="hidden" name="student_id" value="<?=$student_id?>" />
                           <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                           <button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                        </div>
                     </div>

                  </form>
               </div>

            </div>
         </div>
      </div>
      <div id="Applications" class="tabcontent container-fluid">
         <div class="col-md-12 tabordion">
            <?php
                if($applied_universities)
               {
                     $i=1;
                     foreach($applied_universities as $university)
                     {
                         ?>

                        <section id="section<?= $i ?>">
                           <input type="radio" name="sections" id="option<?= $i ?>" onclick="handleClick(this);">
                           <label for="option<?= $i ?>">
                              <h6><i class="fas fa-book">&nbsp;</i><?php echo $university->course_name;?></h6>
                              <h6><i class="fas fa-university">&nbsp;</i><?php echo $university->name;?></h6>
                              <h6><i class="fas fa-bookmark">&nbsp;&nbsp;</i><?php echo $university->status;?></h6>
                           </label>
                           <article>
                             <div class="newTab" id="option<?= $i ?>">
                                 <div class="tab">
                                    <button class="tablinks1 active" onclick="openApplicationTab(event, 'DocUp<?php echo $university->name;?>')" >Required Documents</button>
                                    <button class="tablinks1" onclick="openApplicationTab(event, 'Comments<?php echo $university->name;?>')">Comments</button>
                                    <button class="tablinks1" onclick="openApplicationTab(event, 'RequiredInfo<?php echo $university->name;?>')">Required Info</button>
                                 </div>
                                 <div id="DocUp<?php echo $university->name;?>" class="tabcontent1 col-md-12">
                                   <h3>Doc Upload <?php echo $university->name;?></h3>
                                   <?php
                                   foreach ($myApplicationDocList as $keyd => $valued) {
                                   ?>
                                <div class="col-md-12 docUploadBox noPaddingCol">
                                  <?php if($valued['required'] == 1){  ?>
                                   <div class="col-md-1 verticalDiv requiredDoc">
                                     <h6 class="textVertical">
                                       Required
                                     </h6>

                                   </div>
                                 <?php } else { ?>
                                   <div class="col-md-1 verticalDiv optDoc">
                                     <h6 class="textVertical">
                                       Optional
                                     </h6>

                                   </div>
                                 <?php } ?>
                                   <div class="col-md-11 noPaddingCol" >
                                      <div class="col-md-12">
                                         <div class="col-md-2 statusBox">
                                            <h5>
                                               <?php if($valued['document_url']){ echo " <i class='fas fa-check'></i><br> Completed"; } else { echo " <i class='fas fa-times'></i><br> Pending"; } ?>
                                            </h5>
                                         </div>
                                         <div class="col-md-9 noPaddingCol" >
                                            <div class="col-md-12 noPaddingCol">
                                               <div class="col-md-8 docName">
                                                  <h5><?=$valued['name']?></h5>
                                                  <p>Default Certificate Education History Prototype</p>
                                               </div>
                                               <div class="col-md-4">
                                                 <form method="post" action="" enctype="multipart/form-data" id="myform<?php echo $university->id;?><?=$valued["id"]?>">
                                                 <input class="form-control" type="file" name="document<?php echo $university->id;?><?=$valued["id"]?>" id="document<?php echo $university->id;?><?=$valued["id"]?>">
                                                  <button class="btn btn-md prplButton" style="margin-top: 50%;" onclick="uploadAppDoc(<?php echo $university->id;?>,<?=$valued["id"]?>,<?php echo $university->user_id;?>)"><i class="fas fa-upload">&nbsp;</i>Upload</button>
                                                </form>
                                               </div>
                                            </div>
                                         </div>
                                      </div>
                                      <div class="col-md-12 docUplodedName">
                                        <?php if($valued['document_url']){ ?>
                                          <h6><a href="<?=$valued['document_url']?>" target="_blank" class="docUploadNameLink"> <?=$valued['name']?> View </a></h6>
                                        <?php } else { ?>
                                          <h6>No Document</h6>
                                         <?php } ?>
                                      </div>
                                   </div>
                                </div>
                              <?php } ?>

                                 </div>
                                 <div id="Comments<?php echo $university->name;?>" class="tabcontent1 col-md-12">
                                 <h3>Comments For <?php echo $university->name;?></h3>
                                    <button type="button" class="btn btn-md prplButton btnComment" data-toggle="collapse" data-target="#<?php echo $university->id;?>">New Comment</button>
                                    <div id="<?php echo $university->id;?>" class="collapse">
                                       <form>
                                          <div class="form-group">
                                             <label for="comment" style="border-right: 3px solid #ffffff;">Comment:</label>
                                             <textarea class="form-control" rows="5" id="notes<?php echo $university->id;?>" style="height: auto !important;"></textarea>
                                             <button class="btn btn-md prplButton" type="button" onclick="addNotes(<?php echo $university->id;?>,<?php echo $university->user_id;?>)" ><i class="glyphicon glyphicon-ok-sign"></i>Add Comment</button>
                                          </div>
                                       </form>
                                    </div>

                                    <?php if($university->noteslist) {

                                      foreach ($university->noteslist as $keyn => $valuen) {

                                      ?>
                                      <div class="media comment-box">
                                         <div class="media-left">
                                            <a href="#">
                                            <img class="img-responsive user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
                                            </a>
                                         </div>
                                         <div class="media-body">
                                            <h5 class="media-heading"><?php echo $valuen['created_by_name']; ?> <span style="float: right;"><?php $webinarDate = date('D, d F Y H:m:s', strtotime($valuen['date_created'])); echo "$webinarDate"; ?></span></h5>
                                            <p><?=$valuen['notes']?></p>
                                         </div>
                                      </div>

                                  <?php } } ?>

                                   <!--  <div class="media comment-box">
                                     <div class="media-left">
                                        <a href="#">
                                        <img class="img-responsive user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
                                        </a>
                                     </div>

                                 </div> -->

                              </div>
                              <div id="RequiredInfo<?php echo $university->name;?>" class="tabcontent1 col-md-12">
                                 <h3 style="display: contents;">Required Information For <?php echo $university->name;?></h3>
                                    <div class="col-md-12">
                                      <div class="col-md-12">
                                          <form class="form requiredInfoForm" action="##" method="post" id="">
                                             <div class="form-group">
                                                <div class="col-xs-6">
                                                   <label for="first_name">
                                                      <h5>Name</h5>
                                                   </label>
                                                   <input type="text" class="form-control" name="name" id="student_name" placeholder="Name" value="">
                                                </div>
                                             </div>
                                             <div class="form-group">
                                                <div class="col-xs-6">
                                                   <label for="phone">
                                                      <h5>Mobile</h5>
                                                   </label>
                                                   <input type="text" class="form-control" name="phone" id="student_phone" placeholder="Mobile" value="">
                                                </div>
                                             </div>
                                             <div class="form-group">
                                                <div class="col-xs-6">
                                                   <label for="email">
                                                      <h5>Email</h5>
                                                   </label>
                                                   <input type="email" class="form-control" name="email" id="student_email" placeholder="Email" value="">
                                                </div>
                                             </div>
                                             <div class="form-group">
                                                <div class="col-xs-6">
                                                   <label for="dob">
                                                      <h5>DOB</h5>
                                                   </label>
                                                   <input type="date" name="dob"  class="form-control" id="student_dob" value=""/>
                                                </div>
                                             </div>
                                             <div class="form-group">
                                                <div class="col-xs-6">
                                                   <label for="address">
                                                      <h5>Address</h5>
                                                   </label>
                                                   <input type="text" class="form-control" name="address" id="student_address" placeholder="Address" value="">
                                                </div>
                                             </div>
                                             <div class="form-group">
                                                <div class="col-xs-6">
                                                   <label for="country">
                                                      <h5>Country</h5>
                                                   </label>
                                                   <select name="locationcountries" id="student_locationcountries" class="form-control">
                                                      <option value="0">Select Country</option>
                                                      <?php
                                                         foreach($locationcountries as $location)
                                                         {
                                                             ?>
                                                      <option value="<?=$location->id?>" <?php if($location->id==$selected_country) echo 'selected="selected"';?>><?=$location->name?></option>
                                                      <?php
                                                         }
                                                         ?>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="form-group">
                                                <div class="col-xs-6">
                                                   <label for="state">
                                                      <h5>State</h5>
                                                   </label>
                                                   <select name="state" id="student_state" class="form-control">
                                                      <option value="0">Select State</option>
                                                      <?php
                                                         foreach($related_state_list as $location)
                                                         {
                                                             ?>
                                                      <option value="<?=$location->zone_id?>" <?php if($location->zone_id==$selected_state) echo 'selected="selected"';?>><?=$location->name?></option>
                                                      <?php
                                                         }
                                                         ?>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="form-group">
                                                <div class="col-xs-6">
                                                   <label for="city">
                                                      <h5>City</h5>
                                                   </label>
                                                   <select name="city" id="student_city" class="form-control">
                                                      <option value="0">Select City</option>
                                                      <?php
                                                         foreach($related_city_list as $location)
                                                         {
                                                             ?>
                                                      <option value="<?=$location->id?>" <?php if($location->id==$city) echo 'selected="selected"';?>><?=$location->city_name?></option>
                                                      <?php
                                                         }
                                                         ?>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="form-group">
                                                <div class="col-xs-6">
                                                   <label for="zipcode">
                                                      <h5>Zipcode</h5>
                                                   </label>
                                                   <input type="text" class="form-control" name="zip" id="student_zip" placeholder="Zipcode" value="">
                                                </div>
                                             </div>
                                             <div class="form-group">
                                                <div class="col-xs-6">
                                                   <label for="type">
                                                      <h5>Type</h5>
                                                   </label>
                                                   </div>
                                                <div class="col-xs-6" style="display: inline-flex;">
                                                   <input type="checkbox" id="Type1" name="Type1" value="Bike">
                                                  <label for="Type1"> Type1</label><br>
                                                  <input type="checkbox" id="Type2" name="Type2" value="Car">
                                                  <label for="Type2"> Type2</label><br>
                                                  <input type="checkbox" id="Type3" name="Type3" value="Boat">
                                                  <label for="Type3"> Type3</label><br><br>
                                                </div>

                                             </div>
                                             <div class="form-group">
                                                <div class="col-xs-6">
                                                   <label for="gender">
                                                      <h5>Gender</h5>
                                                   </label>
                                                   <div class="col-md-12">
                                                      <label class="radio-inline">
                                                      <input type="radio" name="gender" <?=$gender == 'M' ? 'checked' : ''?>>Male
                                                      </label>
                                                      <label class="radio-inline">
                                                      <input type="radio" name="gender" <?=$gender == 'F' ? 'checked' : ''?>>Female
                                                      </label>
                                                      <label class="radio-inline">
                                                      <input type="radio" name="gender" <?=$gender == 'O' ? 'checked' : ''?>>Other
                                                      </label>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="form-group">
                                                <div class="col-xs-12">
                                                   <br>
                                                   <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                                                   <button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                                                </div>
                                             </div>
                                          </form>
                                      </div>
                                    </div>
                                 </div>
                           </article>
                        </section>
            <?php
               $i++;
               }
             } else {
               echo "No Application Selected ...!!! <br/> Please Select University From List  <br/>";
               echo "<a class='btn btn-large btn-danger' href='/admin/student/applicationList?user_id=".$id."&student_id=".$student_id."' style='color: #f6882c'> <b>Shortlist Universities / Courses</b> </a>";
             }
                     ?>

         </div>
      </div>
      <div id="Documents" class="tabcontent container-fluid">
         <div  class="col-md-12 boxAppList docTab" style="padding: 2% 2%;    background-color: white;">
            <h3 style="color:black;">Documents</h3>
            <hr>
            <div class="tab">
              <button class="tablinks2" onclick="openDocType(event, 'BASIC')" id="defaultOpenTab">Basic</button>
              <button class="tablinks2" onclick="openDocType(event, 'ACADEMIC')">Academic </button>
              <button class="tablinks2" onclick="openDocType(event, 'TESTS')">Entrance Exam</button>
              <button class="tablinks2" onclick="openDocType(event, 'APPLICATION')">Other Applications</button>
            </div>
            <?php
            foreach ($new_doc_list as $keydl => $valuedl) {
              // code...

            ?>
            <div id="<?php echo "$keydl"; ?>" class="tabcontent2">
              <h3><?php echo "$keydl"; ?></h3>
              <table class="table table-bordered">
                  <thead>
                     <th style="width: 20%;">Name</th>
                     <th style="width: 40%;">Description</th>
                     <th>Upload</th>
                     <th style="width: 10%;">Status</th>
                  </thead>
                  <tbody>
                    <?php
                    foreach ($valuedl as $keyl => $valuel) {

                    ?>
                     <tr>
                        <td><?php echo $valuel['display_name']; ?></td>
                        <td><?php echo $valuel['description']; ?></td>
                        <td>
                           <div class="col-md-12">
                              <a class="btn btn-info" onclick='popup(<?php echo json_encode($valuel); ?>);return false;'> Upload Doc </a>
                           </div>
                        </td>
                        <td><?php $status =  $valuel['user_doc_list'] ? 'UPLOADED' : 'PENDING' ; echo $status;?></td>
                        <!-- <td ng-if="listvalue.document_count"><a class="btn btn-info" data-toggle="modal" data-target="#modalDocument" ng-click="viewDocumentkById(listvalue)" >{{listvalue.document_count}} Document Uploaded</a></td>
                        <td ng-if="!listvalue.document_count"> Pending </a></td> -->
                     </tr>
                   <?php } ?>
                  </tbody>
               </table>
            </div>
          <?php } ?>

            <div class="modal fade" id="modalDocument" role="dialog">
             <div class="modal-dialog">

               <!-- Modal content-->
               <div class="modal-content">
                 <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                   <h4 class="modal-title">Upload Document</h4>
                 </div>
                 <div class="modal-body">
                  <form name="" class="col-md-12 ">
                     <div class="col-md-12">
                        <label id="title_name">
                        Doc Name
                        </label>
                        <br/>
                        <br/>
                        <select class="form-control" name="doc_id" id="doc_id"  required>
                           <option value=""> Select Uploading Document</option>
                           </optgroup>
                        </select>
                     </div>
                     <br/><br/>
                     <div class="col-md-12">
                        <input type="file" name="userfile" id="userfile"  required >
                     </div>
                     <br/><br/>
                     <div class="col-md-12">
                        <label>
                        Document Name
                        </label>
                        <input type="text" class="form-group form-control" placeholder="Input Document Name" name="userdocname" id="userdocname"  style="height: auto;" ></input>
                     </div>
                     <div class="col-md-12" style="text-align: center;">
                        <button class="btn btn-primary btn-sm" type="button" onclick="uploadDoc()" > Upload </button>
                     </div>
                  </form>
                  <br>
                  <hr>
                  <div>
                     <div class="row">
                        <div class="col-md-12" >
                           <h3>Uploaded Document List</h3>
                           <table class="table table-bordered" >
                              <thead>
                                 <th>Document Name</th>
                                 <th>File Name</th>
                                 <th>View</th>
                                 <th>Delete</th>
                              </thead>
                              <tbody id="table_content">
                                 <tr>
                                    <td>display_name</td>
                                    <td style="max-width: 250px;overflow: auto;">url</td>
                                    <td><a href="" target="_blank" class="btn btn-sm btn-success">View</a></td>
                                    <td><button class="btn btn-danger" type="button" ng-click=""> Delete</button></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                 </div>
                 <div class="modal-footer">
                   <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                 </div>
               </div>

             </div>
           </div>

         </div>
      </div>
      <div id="Payments" class="tabcontent container-fluid">

      </div>
   </section>

</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
   var acc = document.getElementsByClassName("accordion");
   var i;

   for (i = 0; i < acc.length; i++) {
     acc[i].addEventListener("click", function() {
       this.classList.toggle("active");
       var panel = this.nextElementSibling;
       if (panel.style.display === "block") {
         panel.style.display = "none";
       } else {
         panel.style.display = "block";
     $("footer").animate({marginTop: "0px"}, 'fast');
       }
     });
   }

      function openCity(evt, cityName) {
          var i, tabcontent, tablinks;
          tabcontent = document.getElementsByClassName("tabcontent");
          for (i = 0; i < tabcontent.length; i++) {
              tabcontent[i].style.display = "none";
          }
          tablinks = document.getElementsByClassName("tablinks");
          for (i = 0; i < tablinks.length; i++) {
              tablinks[i].className = tablinks[i].className.replace(" active", "");
          }
          document.getElementById(cityName).style.display = "block";
          evt.currentTarget.className += " active";
      }

      $('#locationcountries').on('change', function() {
          var id = $(this).find(":selected").val();
          var dataSet = 'id=' + id;
          $.ajax({
              type         : 'POST', // define the type of HTTP verb we want to use (POST for our form)
              url      : '/user/account/getStates', // the url where we want to POST
              data         : dataSet, // our data object
              success: function (data1) {
                  $('#state').html(data1);
              }
          });
      });

      $('#state').on('change', function() {
          var id = $(this).find(":selected").val();
          var dataSet2 = 'id=' + id;
          $.ajax({
              type         : 'POST', // define the type of HTTP verb we want to use (POST for our form)
              url      : '/user/account/getCities', // the url where we want to POST
              data         : dataSet2, // our data object
              success: function (data2) {
              $('#city').html(data2);
              }
          });
      });

      $('input[type=radio][name=diploma_12]').change(function() {
          if (this.value == '1') {
              $('.diploma_hs').hide();
              $('#diploma_id').show();
          }
          else if (this.value == '2') {
              $('.diploma_hs').hide();
              $('#12thidd').show();
          }
      });

      $('input[type=radio][name=competitive_exam]').change(function() {
          if(this.value == 'NO' || this.value == 'NOT_REQUIRED'){
              $('#exam_stage').hide();
              $('#result_date').hide();
              $('#exam_date').hide();
              $('#exam_score').hide();
          }
          else{
              $('#exam_stage').show();
              if($('input[type=radio][name=exam_stage]:checked').val() == 'REGISTERED'){
                  $('#result_date').hide();
                  $('#exam_score').hide();
                  $('#exam_date').show();
              }
              else if($('input[type=radio][name=exam_stage]:checked').val() == 'RESULT_WAIT'){
                  $('#exam_date').hide();
                  $('#exam_score').hide();
                  $('#result_date').show();
              }
              else if($('input[type=radio][name=exam_stage]:checked').val() == 'RESULT_OUT'){
                  $('#result_date').hide();
                  $('#exam_date').hide();
                  $('#exam_score').show();
                  if($('input[type=radio][name=competitive_exam]:checked').val() == 'GRE'){
                      $('#exam_score_gmat').hide();
                      $('#exam_score_sat').hide();
                      $('#exam_score_gre').show();
                  }
                  else if($('input[type=radio][name=competitive_exam]:checked').val() == 'GMAT'){
                      $('#exam_score_sat').hide();
                      $('#exam_score_gre').hide();
                      $('#exam_score_gmat').show();
                  }
                  else if($('input[type=radio][name=competitive_exam]:checked').val() == 'SAT'){
                      $('#exam_score_gmat').hide();
                      $('#exam_score_gre').hide();
                      $('#exam_score_sat').show();
                  }
              }
          }
      });

      $('input[type=radio][name=exam_stage]').change(function() {

          if (this.value == 'REGISTERED') {
              $('#result_date').hide();
              $('#exam_score').hide();
              $('#exam_date').show();
          }

          else if (this.value == 'RESULT_WAIT') {
              $('#exam_date').hide();
              $('#exam_score').hide();
              $('#result_date').show();
          }
          else if (this.value == 'RESULT_OUT') {
              $('#result_date').hide();
              $('#exam_date').hide();
              $('#exam_score').show();
              if($('input[type=radio][name=competitive_exam]:checked').val() == 'GRE'){
                  $('#exam_score_gmat').hide();
                  $('#exam_score_sat').hide();
                  $('#exam_score_gre').show();
              }
              else if($('input[type=radio][name=competitive_exam]:checked').val() == 'GMAT'){
                  $('#exam_score_sat').hide();
                  $('#exam_score_gre').hide();
                  $('#exam_score_gmat').show();
              }
              else if($('input[type=radio][name=competitive_exam]:checked').val() == 'SAT'){
                  $('#exam_score_gmat').hide();
                  $('#exam_score_gre').hide();
                  $('#exam_score_sat').show();
              }

          }

      });

      $('input[type=radio][name=english_proficiency_exam]').change(function() {
          if(this.value == 'NO' || this.value == 'NOT_REQUIRED'){
              $('#english_exam_stage').hide();
              $('#english_result_date').hide();
              $('#english_exam_date').hide();
              $('#english_exam_score').hide();
          }
          else{
              $('#english_exam_stage').show();
              if($('input[type=radio][name=english_exam_stage]:checked').val() == 'REGISTERED'){
                  $('#english_result_date').hide();
                  $('#english_exam_score').hide();
                  $('#english_exam_date').show();
              }
              else if($('input[type=radio][name=english_exam_stage]:checked').val() == 'RESULT_WAIT'){
                  $('#english_exam_date').hide();
                  $('#english_exam_score').hide();
                  $('#english_result_date').show();
              }
              else if($('input[type=radio][name=english_exam_stage]:checked').val() == 'RESULT_OUT'){
                  $('#english_result_date').hide();
                  $('#english_exam_date').hide();
                  $('#english_exam_score').show();
              }
          }
      });

      $('input[type=radio][name=english_exam_stage]').change(function() {

          if (this.value == 'REGISTERED') {
              $('#english_result_date').hide();
              $('#english_exam_score').hide();
              $('#english_exam_date').show();
          }

          else if (this.value == 'RESULT_WAIT') {
              $('#english_exam_date').hide();
              $('#english_exam_score').hide();
              $('#english_result_date').show();
          }
          else if (this.value == 'RESULT_OUT') {
              $('#english_result_date').hide();
              $('#english_exam_date').hide();
              $('#english_exam_score').show();
          }

      });

      function validate(){

        //english test
        var engTestType = $('input[type=radio][name=english_proficiency_exam][checked=checked]').val();
        var engTestStatus = $('input[type=radio][name=english_exam_stage][checked=checked]').val();
        var totalMarks = $('#english_exam_score_total').val() ? $('#english_exam_score_total').val() : 0;

        //competitive_exam
        var comTestType = $('input[type=radio][name=competitive_exam][checked=checked]').val();
        var comTestStatus = $('input[type=radio][name=exam_stage][checked=checked]').val();
        var quantMarks = $('#exam_score_quant_gre').val() ? $('#exam_score_quant_gre').val() : 0;
        var verbalMarks = $('#exam_score_verbal_gre').val() ? $('#exam_score_verbal_gre').val() : 0;
        var awaMarks = $('#exam_score_awa_gre').val() ? $('#exam_score_awa_gre').val() : 0;

        if(comTestStatus == "RESULT_OUT"){

          if(quantMarks > 129 && quantMarks < 171){

            return true;

          } else {
            alert(" Quant marks should be between 130 to 170");
            document.getElementById('exam_score_quant_gre').style.borderColor = "red";
          }

          if(verbalMarks > 129 && verbalMarks < 171){

            return true;

          } else {
            alert(" Verbal marks should be between 130 to 170");
            document.getElementById('exam_score_verbal_gre').style.borderColor = "red";
          }

          if(awaMarks > 0 && awaMarks < 6){

            return true;

          } else {
            alert(" AWA marks should be between 0 to 5");
            document.getElementById('exam_score_awa_gre').style.borderColor = "red";
            return false;
          }

        }


        if(engTestType == "TOEFL" && engTestStatus == "RESULT_OUT"){

          if(totalMarks > 0 && totalMarks < 31){

            return true;

          } else {
            alert(engTestType + " total marks should be between 1 to 30");
            document.getElementById('english_exam_score_total').style.borderColor = "red";
            return false;
          }

        }

        if(engTestType == "TOEFL" && engTestStatus == "RESULT_OUT"){

          if(totalMarks > 0 && totalMarks < 31){

            return true;

          } else {
            alert(engTestType + " total marks should be between 1 to 30");
            document.getElementById('english_exam_score_total').style.borderColor = "red";
            return false;
          }

        }

        if(engTestType == "IELTS" && engTestStatus == "RESULT_OUT"){

          if(totalMarks > 0 && totalMarks < 10){

            return true;

          } else {
            alert(engTestType + " total marks should be between 1 to 30");
            document.getElementById('english_exam_score_total').style.borderColor = "red";
            return false;
          }

        }

        if(engTestType == "PTE" && engTestStatus == "RESULT_OUT"){

          if(totalMarks > 9 && totalMarks < 91){

            return true;

          } else {
            alert(engTestType + " total marks should be between 1 to 30");
            document.getElementById('english_exam_score_total').style.borderColor = "red";
            return false;
          }

        }

        if(engTestType == "DUOLINGO" && engTestStatus == "RESULT_OUT"){

          if(totalMarks > 9 && totalMarks < 161){

            return true;

          } else {
            alert(engTestType + " total marks should be between 1 to 30");
            document.getElementById('english_exam_score_total').style.borderColor = "red";
            return false;
          }

        }

      }

   function openPage(pageName,elmnt,color) {
   var i, tabcontent, tablinks;
   tabcontent = document.getElementsByClassName("tabcontent");
   for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
   }
   tablinks = document.getElementsByClassName("tablink");
   for (i = 0; i < tablinks.length; i++) {
    tablinks[i].style.backgroundColor = "";
    tablinks[i].style.color = "";
    tablinks[i].style.fontWeight = "";
    tablinks[i].style.borderBottom = "";
   }
   document.getElementById(pageName).style.display = "block";
   elmnt.style.backgroundColor = color;
   elmnt.style.color = "#815dd5";
   elmnt.style.fontWeight = "600";
   elmnt.style.borderBottom = "3px solid #815dd5";
   }

   // Get the element with id="defaultOpen" and click on it
   document.getElementById("defaultOpen").click();

   $('input[name="sections"]').first().prop('checked', true);

   function openApplicationTab(evt, cityName) {
   var i, tabcontent, tablinks;
   tabcontent = document.getElementsByClassName("tabcontent1");
   for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
   }
   tablinks = document.getElementsByClassName("tablinks1");
   for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
   }
   document.getElementById(cityName).style.display = "block";
   evt.currentTarget.className += " active";
   }

   // Get the element with id="defaultOpen" and click on it
   //document.getElementById("defaultOpenthis").click();

   $(".btnComment").click(function () {
            $(this).text(function(i, v){
               return v === 'New Comment' ? 'Hide' : 'New Comment'
            })
   });
</script>
<script type="text/javascript">

function addNotes(appId,studentId){

  var notes = $('#notes'+appId).val();

  //alert(appId);

  var dataString = 'app_id=' + appId + '&student_id=' + studentId + '&notes=' + notes ;

  event.preventDefault();

  $.ajax({

  type: "POST",

  url: "/student/profile/notesupdate",

  data: dataString,

  cache: false,

  success: function(result){

   if(result.status == "SUCCESS"){

     alert("Notes Update Successfully!!!");


   } else if(result.status == 'NOTVALIDUSER'){

     alert(result);
     window.location.href='/user/account';

   } else {

     alert(result);
    window.location.reload();

   }

  }

      });

}

function uploadAppDoc(appId,docId,studentId){

  var fd = new FormData();
        var files = $('#document'+appId+docId)[0].files;

        // Check file selected or not
        if(files.length > 0 ){

           fd.append('document',files[0]);
           fd.append('app_id',appId);
           fd.append('document_type',docId);
           fd.append('student_id',studentId);

           event.preventDefault();

           $.ajax({
              url: '/student/profile/appdocupdate',
              type: 'post',
              data: fd,
              contentType: false,
              processData: false,
              success: function(response){
                console.log(response);
                 if(response == 'SUCCESS'){
                    alert('file uploaded');
                 }else{
                    alert('file not uploaded');
                 }
              },
           });
        }else{
           alert("Please select a file.");
        }

}

$('input[type=radio][name=masters]').change(function() {
    if (this.value == '1') {
        $('#masters_id').show();
    }
    else if (this.value == '2') {
        $('#masters_id').hide();
    }
});



$(function() {
 $("input[name='passport_availalibility']").click(function() {
   if ($("#chk-Yes").is(":checked")) {
     $("#passport_group").show();
   } else {
     $("#passport_group").hide();
   }
 });
});


// $(function() {
//     $('input[name="sections"]').change(function(e) {
//         if (this.id == '1') {
//             $('#tabUni').text("First radio checked");
//         } else {
//             $('#event').text("First radio unchecked");
//         }
//       });
// });name="sections" id="option<?= $i ?>" id="tabUni<?= $i ?>"

var currentValue = 0;
function handleClick(sections) {
    // alert('Old value: ' + currentValue);
    // alert('New value: ' + sections.id);
    currentValue = sections.id;
    var articleId = $(".newTab").attr("id");
   // alert(articleId +" "+ currentValue);
    if (currentValue !== null) {
      //alert("inside" + currentValue);
      if (articleId !== null) {
            //alert("inside articleId " + articleId);
            if (currentValue !== articleId){
               //alert("inside not eq ");
               ///document.getElementById(currentValue).click();
               var newID = currentValue;
               //alert("new" + newID);
               //$newID.find('.tablinks1').setAttribute("class", "active");
               // document.getElementById(newID).child('.tablinks1').click();
               //e.preventDefault();
               // $(newID).find('.tablinks1').addClass('active');
               document.getElementById("defaultOpenthis").click();


            }
         }
      }

}



    // if (document.getElementsByClassName(".newTab")) {
    //     document.getElementByClassName(".newTab").setAttribute("id", currentValue);
    //     document.getElementById(".newTab .tab .tablinks1").click();
    // }$(this).siblings(".newTab .tab").setAttribute("id", currentValue);
    // else {
    // }


    function openDocType(evt, cityName) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent2");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablinks2");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }
      document.getElementById(cityName).style.display = "block";
      evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpenTab").click();

        function popup(modalcontent) {
          $('[id*="modalDocument"]').modal('show');

          var userId = <?=$id?>;
          console.log(modalcontent);

          if(modalcontent.doc_list.length){

                 //var courseHTMLL = '<form id="compare-course-form" name="compare-course-form" action="/course/compare" method="POST">';
                 var selectHTML = '<option value=""> Select Uploading Document </option>';

                modalcontent.doc_list.forEach(function(value, index){

                selectHTML += '<option value="'+value.document_master_id+'">'
                                  +value.display_name
                                  +'</option>';
                                });
                }

                document.getElementById("doc_id").innerHTML = selectHTML;
                document.getElementById("title_name").innerHTML = modalcontent.display_name;


                if(modalcontent.user_doc_list.length){

                       var tableHTML = '';

                      modalcontent.user_doc_list.forEach(function(valueu, indexu){

                      tableHTML += '<tr>'
                                   +'<td> '+valueu.display_name+' </td>'
                                   +'<td> '+valueu.document_display_name+' </td>'
                                   +'<td> <a href="'+valueu.document_url+'" target="_blank" class="btn btn-sm btn-success">View</a> </td>'
                                   +'<td> <button class="btn btn-danger" type="button" onclick="deleteDoc('+valueu.document_master_id+','+userId+')"> Delete</button> </td>'
                                   +'</tr>';
                                      });
                                    }


                document.getElementById("table_content").innerHTML = tableHTML;

        }

        function deleteDoc(docMasterId,userId){

          var dataString = 'doc_master_id=' + docMasterId + '&user_id=' + userId ;

          event.preventDefault();

          $.ajax({

          type: "POST",

          url: "/student/profile/deletedoc",

          data: dataString,

          cache: false,

          success: function(result){

           if(result.status == "SUCCESS"){

             alert("Document Deleted Successfully!!!");
             window.location.reload();


           } else if(result.status == 'NOTVALIDUSER'){

             //alert(result);
             window.location.href='/user/account';

           } else {

             alert(result);
             location.reload();

           }

          }

              });

        }

        function uploadDoc(){

                var fd = new FormData();
                var files = $('#userfile')[0].files;
                var docId = $('#doc_id').val();
                var appId = 1 ;
                var studentId = <?=$id?>;
                var userDoc = $('#userdocname').val();

                // Check file selected or not
                if(files.length > 0 ){

                   fd.append('document',files[0]);
                   fd.append('app_id',appId);
                   fd.append('document_type',docId);
                   fd.append('student_id',studentId);
                   fd.append('doc_name',userDoc);

                   event.preventDefault();

                   $.ajax({
                      url: '/student/profile/appdocupdate',
                      type: 'post',
                      data: fd,
                      contentType: false,
                      processData: false,
                      success: function(response){
                        console.log(response);
                         if(response == 'SUCCESS'){
                            alert('file uploaded');
                            window.location.reload();
                         }else{
                            alert('file not uploaded');
                         }
                      },
                   });
                }else{
                   alert("Please select a file.");
                }

        }

</script>
