<style type="text/css">
   .styleCss{
   font-size: 14px;
   background-color: #f6882c;
   color: white;
   font-weight: 600;
   letter-spacing: 0.8px;
   }
   .formControl{
   color: #fff !important;
   background-color: #8c57c5 !important;
   /* border: 0px; */
   font-size: 12px;
   border: 0px !important;
   box-shadow: inset 0 0 0 rgba(0,0,0,.075) !important;
   margin-top: 15px;
   }

   #dt-example td.details-control {
     cursor: pointer;
}
 #dt-example tr.shown td {
     background-color: #815dd5;
     color: #fff !important;
     border: none;
     border-right: 1px solid #815dd5;
}
 #dt-example tr.shown td:last-child {
     border-right: none;
}
 #dt-example tr.details-row .details-table tr:first-child td {
     color: #fff;
     background: #f6882c;
     border: none;
}
 #dt-example tr.details-row .details-table > td {
     padding: 0;
}
 #dt-example tr.details-row .details-table .fchild td:first-child {
     cursor: pointer;
}
 #dt-example tr.details-row .details-table .fchild td:first-child:hover {
     background: #fff;
}
 #dt-example .form-group.agdb-dt-lst {
     padding: 2px;
     height: 23px;
     margin-bottom: 0;
}
 #dt-example .form-group.agdb-dt-lst .form-control {
     height: 23px;
     padding: 2px;
}
 #dt-example .adb-dtb-gchild {
     padding-left: 2px;
}
 #dt-example .adb-dtb-gchild td {
     background: #f5fafc;
     padding-left: 15px;
}
 #dt-example .adb-dtb-gchild td:first-child {
     cursor: default;
}
 #dt-example .fchild ~ .adb-dtb-gchild {
    /* display: none;
     */
}
 .dataTables_wrapper{
    overflow: auto;
 }

.listTableView thead tr{
 background-color: #f6882c;
    color: white;
}

/* The Modal (background) */
.modal {
display: none; /* Hidden by default */
position: fixed; /* Stay in place */
z-index: 99999; /* Sit on top */
padding-top: 100px; /* Location of the box */
left: 0;
top: 0;
width: 100%; /* Full width */
height: 100%; /* Full height */
overflow: auto; /* Enable scroll if needed */
background-color: rgb(0,0,0); /* Fallback color */
background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}
/* Modal Content */
.modal-content {
background-color: #fefefe;
margin: auto;
padding: 20px;
border: 1px solid #888;
    width: 40%;
 z-index: 10000;
}

#fairModal .modal-content {
background-color: #fefefe;
margin: auto;
padding: 20px;
border: 1px solid #888;
    width: auto;
 z-index: 10000;
}

.fairModal a.pre-order-btn {
  color: #000;
  background-color: gold;
  border-radius: 1em;
  padding: 1em;
  display: block;
  margin: 1.5em auto;
  width: 50%;
  font-size: 1.25em;
  font-weight: 6600;
}
.fairModal a.pre-order-btn:hover {
  background-color: #3c1e4c;
  text-decoration: none;
  color: gold;
}
/* The Close Button */
.close {
color: #8e0f0f;
float: right;
font-size: 28px;
font-weight: bold;
}
.close:hover,
.close:focus {
color: #000;
text-decoration: none;
cursor: pointer;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Student<small>Manage Students</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Student Management</a></li>
            <li class="active">Student</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php
                if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>

                <div class="box">
                    <form action="<?php echo base_url();?>student/session/book" method="post" id="book-session-form">
                        <div class="box-header">
                          <a  style="text-align: center; border-radius:5px;" class="btn btn-md btn-warning pull-right" href="/admin/student/index/profile?id=<?=$student_id?>"> SKIP </a>

                           <a   style="text-align: center; border-radius:5px;" class="btn btn-md btn-info pull-right" href="/admin/student/index/profile?id=<?=$student_id?>"> FINISH </a>
                            <input type="hidden" value="<?=$university_id?>" name="university_id" id="university_id">
                            <input type="hidden" value="<?=$user_id?>" name="user_id" id="user_id">
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Country</th>
                                        <th>Placement Percentage</th>
                                        <th>Establishment Year</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    //var_dump($application_list);
                                    foreach($application_list as $applications)
                                    {
                                        ?>
                                        <tr>

                                            <td><?php echo $applications['name'];?></td>
                                            <td><?php echo $applications['country_name'];?></td>
                                            <td><?php echo $applications['placement_percentage'];?></td>
                                            <td><?php echo $applications['establishment_year'];?></td>
                                            <td><input type="button" class="btn btn-mg btn-info" onclick="courseModal('<?php echo $applications['university_id'];?>','<?php echo $applications['name'];?>')" value="Apply" /> </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                  <tr>
                                    <th>Name</th>
                                    <th>Country</th>
                                    <th>Placement Percentage</th>
                                    <th>Establishment Year</th>
                                    <th>Action</th>
                                  </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="courseDataModal" class="modal">
<!-- Modal content style="display: block;"-->
   <div class="modal-content">
      <span class="close closeFillFairData">&times;</span>
      <div id="fair-data-modal" style="text-align: center;">

        <div class="row">
            <div class="col-md-12 login-sec">
                <h2 class="text-center" style="color: #8c57c5;">Select Course</h2>
                <form class="login-form" class="form-horizontal" onsubmit="applyUniversity()" method="post" autocomplete="off">

        <div class="col-md-12 form-group">
          <div class="col-md-12">
            <label for="inputEmail3" class="text-uppercase"> University Name </label>
             <input type="text" class="form-control" style="width:100%; height:30px; font-size:12px;" name="university_name" id="university_name" >
          </div>
        </div>

        <div class="col-md-12 form-group">
          <div class="col-md-12">
            <label for="inputEmail3" class="text-uppercase"> Select Course </label>
             <input type="text" name="searchtextvalue" id="searchtextvalue" class="form-control search-slt typeahead" data-provide="typeahead" placeholder="Type Course Name & Select From Dropdown" style="width:100%; height:30px; font-size:12px;" >
          </div>
        </div>

        <div class="form-check" style="text-align: center;">

            <input type="submit"  style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Apply"></input>

        </div>


      </form>
  </div>
</div>

      </div>
   </div>


</div>

<script src="<?php echo base_url();?>../js/bootstrap-typeahead-min.js"></script>

<script type="text/javascript">

 var searchUniversityId = 0;
 var courseId = 0;

function courseModal(university_id, university_name){

  $("#courseDataModal").modal('show');

  document.getElementById("university_name").value = university_name;

  searchUniversityId = university_id;

  console.log(searchUniversityId);

}

var $input = $(".typeahead");
$input.typeahead({

source: function (query, process) {
   return $.get('/offer/course/textsuggestions/'+searchUniversityId, { query: query }, function (data) {
       return process(data.options);
   });
}

});

$input.change(function() {
  var current = $input.typeahead("getActive");
  if (current) {
    // Some item from your model is active!
    if (current.name == $input.val()) {
      courseId = current.id;
      // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
    } else {
      // This means it is only a partial match, you can either add a new item
      // or take the active if you don't want new items
    }
  } else {
    // Nothing is active so it is a new value (or maybe empty value)
  }
});
</script>
<script type="text/javascript">

var modalFillFairData = document.getElementById("courseDataModal");
var spanFillFair = document.getElementsByClassName("closeFillFairData")[0];

spanFillFair.onclick = function() {
  //modalFillFairData.style.display = "none";
  $("#courseDataModal").modal('hide');
}



function applyUniversity() {

  var university_id = searchUniversityId;
  var course_id = courseId;
  var user_id = $('#user_id').val();

  console.log(course_id);

  event.preventDefault();

    jQuery.ajax({
         type: "GET",
         url: "/admin/student/applicationList/apply/?uid=" + university_id + "&course_id="+ course_id +"&user_id=" + user_id,
     success: function(response) {
        alert("You have applied successfully");
        $("#courseDataModal").modal('hide');
        //window.location.href = '/search/university';
         }
     });
}

</script>
