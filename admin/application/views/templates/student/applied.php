<style>
    .counselors-list
    {
        display: none;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Student<small>Manage Students</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Student Management</a></li>
            <li class="active">Student</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php
                if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>

                <div class="box">
                    <form action="<?php echo base_url();?>student/index/applied" method="get">
                        <div class="box-header">
                            <select name="uid" onclick="showStudentList(this.value)">
                                <option value="">Please Select An University</option>
                                <?php
                                foreach($universities as $university)
                                {
                                    ?>
                                    <option value="<?=$university->id?>" <?=$university->id == $selected_university ? 'selected' : ''?>><?=$university->name?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div><!-- /.box-header -->
                        <?php
                        if(isset($students))
                        {
                            ?>
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Student Name</th>
                                            <th>Student Email</th>
                                            <th>Student Phone</th>
                                            <th>Originator Name</th>
                                            <th>Counselor Name</th>
                                            <th class="counselors-list"></th>
                                            <th>Desired Destinations</th>
                                            <th>Option</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i=1;
                                        foreach($students as $student)
                                        {
                                            ?>
                                            <tr>
                                                <td><input type="checkbox" class="multichk" name="chk[]" value="<?php echo $student->student_id;?>"/></td>
                                                <td><a href="/admin/student/index/profile?id=<?php echo $student->student_id;?>" target="_blank"><?php echo $student->student_name;?></a></td>
                                                <td><?php echo $student->student_email;?></td>
                                                <td><?php echo $student->student_phone;?></td>
                                                <td><?php echo $student->originator_name;?></td>
                                                <td id="assigned-counselor-<?=$student->student_id?>" class="assigned-counselor"><?php echo $student->facilitator_name;?></td>
                                                <td id="counselors-list-<?=$student->student_id?>" class="counselors-list">
                                                    <select name="counselor_id" onchange="updateCounselor('<?=$student->student_id?>', this.value)">
                                                        <?php
                                                        foreach($counselors as $counselor)
                                                        {
                                                            ?>
                                                            <option value="<?=$counselor->id . '|' . $counselor->name?>" <?=$counselor->name == $student->facilitator_name ? 'selected' : ''?>><?=$counselor->name?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </td>
                                                <td><?php echo $student->desired_destination;?></td>
                                                <td>
                                                    <a href="javascript::void();" title="Modify" class="edit" id="edit-<?php echo $student->student_id;?>" onclick="showCounselors('<?=$student->student_id?>')"><i class="fa fa-edit"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th>Student Name</th>
                                            <th>Student Email</th>
                                            <th>Student Phone</th>
                                            <th>Originator Name</th>
                                            <th>Counselor Name</th>
                                            <th class="counselors-list"></th>
                                            <th>Desired Destinations</th>
                                            <th>Option</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div><!-- /.box-body -->
                            <?php
                        }
                        ?>
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
    function showCounselors(studentId)
    {
        document.getElementById("assigned-counselor-" + studentId).style.display = "none";
        document.getElementById("counselors-list-" + studentId).style.display = "block";
    }

    function updateCounselor(studentId, counselor)
    {
        var splitCounselor = counselor.split('|');
        var counselorId = splitCounselor[0];
        var counselorName = splitCounselor[1];
        $.ajax({
            type: "POST",
            url: "/admin/student/index/update",
            data: {"student_id": studentId, "facilitator_id": counselorId},
            cache: false,
            success: function(result){
                if(result == 1){
                    alert('Success');
                    document.getElementById("assigned-counselor-" + studentId).innerHTML = counselorName;
                }
                else{
                    alert(result);
                }
                document.getElementById("assigned-counselor-" + studentId).style.display = "block";
                document.getElementById("counselors-list-" + studentId).style.display = "none";
            }
        });
    }

    function showStudentList(universityId)
    {
        window.location.href = "/admin/student/index/applied?uid=" + universityId;
    }
</script>
