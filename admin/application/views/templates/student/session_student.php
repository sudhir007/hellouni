<style>
    .counselors-list
    {
        display: none;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Student<small>Manage Students</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Student Management</a></li>
            <li class="active">Student</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php
                if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>

                <div class="box">
                    <form action="<?php echo base_url();?>student/session/book" method="post" id="book-session-form">
                        <div class="box-header">
                            <input type="submit" style="width:12%; float:right; margin-right:5px;" class="btn btn-block btn-primary" value="Book Session" id="student-book-session" disabled>
                            <input type="hidden" value="<?=$university_id?>" name="university_id">
                            <input type="hidden" value="<?=$university_login_id?>" name="university_login_id">
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Student Name</th>
                                        <th>Student Email</th>
                                        <th>Student Phone</th>
                                        <th>Originator Name</th>
                                        <th>Facilitator Name</th>
                                        <th>Desired Destinations</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    foreach($students as $student)
                                    {
                                        ?>
                                        <tr>
                                            <td><input type="checkbox" class="multichk" name="chk[]" value="<?php echo $student->student_login_id;?>" onclick="enableButton()"/></td>
                                            <td><a href="/admin/student/index/profile?id=<?php echo $student->student_id;?>" target="_blank"><?php echo $student->student_name;?></a></td>
                                            <td><?php echo $student->student_email;?></td>
                                            <td><?php echo $student->student_phone;?></td>
                                            <td><?php echo $student->originator_name;?></td>
                                            <td><?php echo $student->facilitator_name;?></td>
                                            <td><?php echo $student->desired_destination;?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th>Student Name</th>
                                        <th>Student Email</th>
                                        <th>Student Phone</th>
                                        <th>Originator Name</th>
                                        <th>Facilitator Name</th>
                                        <th>Desired Destinations</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
    function enableButton()
    {
        if ($("#book-session-form input:checkbox:checked").length > 0)
        {
            document.getElementById("student-book-session").disabled = false;
        }
        else
        {
           document.getElementById("student-book-session").disabled = true;
        }
    }
</script>
