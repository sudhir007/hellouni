<style>
    .add_comment_button{
        text-align: right;
        margin-top: 10px;
    }
</style>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>Student<small>Comments</small></h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="#">Student Management</a></li>
         <li class="active">Comments</li>
      </ol>
   </section>
   <section class="content">
       <div>
          <form action="<?php echo base_url()?>student/comments/add" class="form-group"  method="post"  style="background-color: white;padding: 30px;">

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                      <h4> <b>Add Your Comments : </b></h4>
                  <div class="form-group col-md-6" style="margin: 2px 0px;">
                      <label class="col-md-5">Comment Type : </label>
                      <select class="form-control bRadius col-md-3" id="entity_type" name="entity_type" required>
                        <option value=""> Select Type </option>
                         <?php
                            foreach($comment_types as $key => $value)
                            {
                                ?>
                                 <option value="<?php echo $key;?>"><?php echo $value;?></option>
                                 <?php
                            }
                            ?>
                      </select>
                  </div>

                  <div class="form-group col-md-6" >
                      <label class="col-md-5">Lead State : </label>
                      <select class="form-control bRadius col-md-3" id="lead_state" name="lead_state" required>
                        <option value=""> Select Type </option>
                         <?php
                            foreach($lead_state_list as $keyl => $valuel)
                            {
                                ?>
                                 <option value="<?php echo $keyl;?>"><?php echo $valuel;?></option>
                                 <?php
                            }
                            ?>
                      </select>
                  </div>

                  <div class="form-group col-md-6">
                      <label class="col-md-5">Application Name : </label>
                      <select class="form-control bRadius col-md-3" id="entity_id" name="entity_id">
                        <option value=""> Select Application </option>
                         <?php
                            foreach($applied_universities as $university)
                            {
                                ?>
                                 <option value="<?php echo $university->id;?>"><?php echo $university->name." ".$university->college_name." ".$university->course_name ;?></option>
                                 <?php
                            }
                            ?>
                      </select>
                  </div>
                  <div class="form-group col-md-6" >
                      <label class="col-md-7">Time Spent (In Minutes)  : </label>
                      <input type="text" name="time_spent" class="bRadius form-control input_field"/ required>
                  </div>

                  <div class="form-group col-md-6" >
                      <label class="col-md-7">Next Followup Date : </label>
                      <input type="date" name="followup_date" class="form-control input_field"/ required>
                  </div>

                  <div class="form-group col-md-6" >
                      <label class="col-md-5">Comment : </label>
                      <textarea name="comment" cols="80" rows="5" required></textarea>
                  </div>
              </div>
            </div>
          </div>

              <div class="add_comment_button">
                  <input type="hidden" name="student_login_id" value="<?=$student_login_id?>">
                  <button type="submit" class="button_sign_in btn btn-md btn-primary">Add Comment</button>
              </div>
          </form>
       </div>
   </section>
   <section class="content">
       <div class="row">
           <div class="col-xs-12">
               <div class="box">
                   <form action="<?php echo base_url();?>student/edit" method="post">
                       <div class="box-body">
                           <table id="example1" class="table table-bordered table-striped">
                               <thead>
                                   <tr>
                                       <th>Student Name</th>
                                       <th>Student Email</th>
                                       <th>Student Phone</th>
                                       <th>University Name</th>
                                       <th>Commented By</th>
                                       <th>Comment</th>
                                       <th>Comment Type</th>
                                       <th>Time Spent (In Minutes)</th>
                                       <th>Comment Date</th>
                                   </tr>
                               </thead>
                               <tbody>
                                   <?php
                                   $i=1;
                                   foreach($comments as $comment)
                                   {
                                       ?>
                                       <tr>
                                           <td><?php echo $comment->student_name;?></a></td>
                                           <td><?php echo $comment->student_email;?></td>
                                           <td><?php echo $comment->student_phone;?></td>
                                           <td><?php echo $comment->university_name;?></td>
                                           <td><?php echo $comment->commented_by;?></td>
                                           <td><?php echo $comment->comment;?></td>
                                           <td><?php echo $comment->entity_type;?></td>
                                           <td><?php echo $comment->time_spent;?></td>
                                           <td><?php echo $comment->added_time;?></td>
                                       </tr>
                                       <?php
                                   }
                                   ?>
                               </tbody>
                               <tfoot>
                                   <tr>
                                       <th>Student Name</th>
                                       <th>Student Email</th>
                                       <th>Student Phone</th>
                                       <th>University Name</th>
                                       <th>Commented By</th>
                                       <th>Comment</th>
                                       <th>Comment Type</th>
                                       <th>Time Spent (In Minutes)</th>
                                       <th>Comment Date</th>
                                   </tr>
                               </tfoot>
                           </table>
                       </div><!-- /.box-body -->
                   </form>
               </div><!-- /.box -->
           </div><!-- /.col -->
       </div><!-- /.row -->
   </section><!-- /.content -->
</div>
