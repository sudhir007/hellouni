<style>
    .counselors-list
    {
        display: none;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Student<small>Manage Students</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Student Management</a></li>
            <li class="active">Student List My</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php
                if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>

                <div class="box">
                    <form action="<?php echo base_url();?>student/edit" method="post">
                        <div class="box-header">
                            <a href="<?php echo base_url();?>student/index/add"><button type="button" style="width:12%; float:left; margin-right:5px;" class="btn btn-block btn-primary"><i class="fa fa-plus"></i> Add Student</button></a>
                            <a href="/admin/counsellor/create/application"><button type="button" style="width:12%; float:left; margin-right:5px;" class="btn btn-block btn-secondary"><i class="fa fa-plus"></i> New Student Application</button></a>
                            <button type="submit" class="btn btn-block btn-danger multidel" style="width:11%; display:none;"><i class="fa fa-minus"></i> Delete University</button>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>User Id</th>
                                        <th>Student Name</th>
                                        <th>Student Email</th>
                                        <th>Student Phone</th>
                                        <th>Create Date</th>
                                        <th>Originator Name</th>
                                        <th>Facilitator Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    foreach($student_list as $student)
                                    {
                                        ?>
                                        <tr>
                                            <td><?php echo $student->user_id;?></td>
                                            <td><a href="/admin/student/profile/<?php echo $student->user_id;?>/0" target="_blank"><?php echo $student->name;?></a></td>
                                            <td><?php echo $student->email;?></td>
                                            <td><?php echo $student->phone;?></td>
                                            <td><?php echo $student->createdate;?></td>
                                            <td><?php echo $student->originator_name;?></td>
                                            <td><?php echo $student->facilitator_name;?></td>

                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                      <th>User Id</th>
                                      <th>Student Name</th>
                                      <th>Student Email</th>
                                      <th>Student Phone</th>
                                      <th>Create Date</th>
                                      <th>Originator Name</th>
                                      <th>Facilitator Name</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
