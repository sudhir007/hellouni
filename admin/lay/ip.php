<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hellouni</title>

    <!-- Bootstrap Core CSS -->
  	<?php include 'head.php'; ?>
<style>
	.intro{
		background:#fff !important;
	}
	
	.process img{
		margin-left:-70px;
	}

.navbar{
	border-bottom: solid 4px #f6881f;
	background:#000;
}

#htrans{
	 border-bottom:solid 2px #b1b1b1;
}
</style>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- Navigation -->
    <?php include 'header.php'; ?>

    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
        	<div class="row" style=" padding-bottom:30px;box-shadow: 0 12px 6px -6px #f3f3f3;">
            	<div class="container">
                	<div class="col-lg-12 process">
                    	<img src="img/process.png" align="middle" style="margin-left:-70px;" />
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Footer -->
    <?php include 'footer.php'; ?>

    <!-- jQuery -->
   <?php  include 'js.php'?>

</body>

</html>
