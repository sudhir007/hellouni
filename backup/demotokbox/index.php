
<?php

include 'opentok.phar';

//OpenTok = require('opentok')

use OpenTok\OpenTok;
$apiKey = '45715252';
$apiSecret = '7a5801d197f3f32827e8dc4e666940355105619e';

$opentok = new OpenTok($apiKey, $apiSecret);


use OpenTok\MediaMode;
use OpenTok\ArchiveMode;

// Create a session that attempts to use peer-to-peer streaming:
$session = $opentok->createSession();

// A session that uses the OpenTok Media Router, which is required for archiving:
$session = $opentok->createSession(array( 'mediaMode' => MediaMode::ROUTED ));

// A session with a location hint:
$session = $opentok->createSession(array( 'location' => '12.34.56.78' ));

// An automatically archived session:
$sessionOptions = array(
    'archiveMode' => ArchiveMode::ALWAYS,
    'mediaMode' => MediaMode::ROUTED
);
$session = $opentok->createSession($sessionOptions);


// Store this sessionId in the database for later use
$sessionId = $session->getSessionId();



use OpenTok\Session;
use OpenTok\Role;

// Generate a Token from just a sessionId (fetched from a database)
$token = $opentok->generateToken($sessionId);
// Generate a Token by calling the method on the Session (returned from createSession)
//$token = $session->generateToken();

// Set some options in a token
$token = $session->generateToken(array(
    'role'       => Role::MODERATOR,
    'expireTime' => time()+(7 * 24 * 60 * 60), // in one week
    'data'       => 'name=Sumanta'
));

//print_r($token);



?>
<!--<script type="text/javascript">
var session;
var connectionCount = 0;



function connect() {
  // Replace apiKey and sessionId with your own values:
/*  var apiKey = <?php echo $apiKey; ?>;
  var sessionId = <?php echo $sessionId; ?>;*/
  session = OT.initSession("<?php echo $apiKey; ?>","<?php echo $sessionId; ?>");
  session.on({
    connectionCreated: function (event) {
      connectionCount++;
      console.log(connectionCount + ' connections.');
    },
    connectionDestroyed: function (event) {
      connectionCount--;
      console.log(connectionCount + ' connections.');
    },
    sessionDisconnected: function sessionDisconnectHandler(event) {
      // The event is defined by the SessionDisconnectEvent class
      console.log('Disconnected from the session.');
      document.getElementById('disconnectBtn').style.display = 'none';
      if (event.reason == 'networkDisconnected') {
        alert('Your network connection terminated.')
      }
    }
  });
  // Replace token with your own value:
  session.connect("<?php echo $token;?>", function(error) {
    if (error) {
      console.log('Unable to connect: ', error.message);
    } else {
      document.getElementById('disconnectBtn').style.display = 'block';
      console.log('Connected to the session.');
      connectionCount = 1;
    }
  });
}

function disconnect() {
  session.disconnect();
}

connect();







// PUBLISHER
var publisher;
var targetElement = 'publisherContainer';

/*publisher = OT.initPublisher(targetElement, null, function(error) {
  if (error) {
console.log(error);
    // The client cannot publish.
    // You may want to notify the user.
  } else {
    alert('Publisher initialized.');
	
	
  }
});*/

var pubOptions = {publishAudio:true, publishVideo:true,width: 800,height: 600};
	var replacementElementId = "localVideo";

// Replace replacementElementId with the ID of the DOM element to replace:
publisher = OT.initPublisher(replacementElementId, pubOptions);

//session.publish(publisher);

/*var replacementElementId = "localVideo";
var publisherOptions = {
  insertMode: 'append',
  width: '100%',
  height: '100%'
};
var publisher = OT.initPublisher(replacementElementId, publisherOptions);
session.publish(publisher);*/


//subscriber
var replacementElementId2 = "localVideo2";
var options = {subscribeToAudio:true, subscribeToVideo:true};

// Replace stream and replacementElementId with your own values:
subscriber = session.subscribe(stream,
                             replacementElementId2,
                             options);
							 
							 


function startVideoGuest() {
    if (OT.checkSystemRequirements() == 1) {
    var session = OT.initSession("<?php echo $apiKey; ?>","<?php echo $sessionId; ?>");
    session.on("streamCreated", function(event) {
    session.subscribe(event.stream, remoteVideos);
  });
  session.connect("<?php echo $token;?>", function(error) {
    if (error) {
      console.log("Error connecting: ", error.code, error.message);
    } else {
        $('.videoWindow').show();
        var replacementElementId = "localVideo2";
        publisher = OT.initPublisher(replacementElementId2);
        publisher.on({
            streamCreated: function (event) {
            console.log("Publisher started streaming.");
        },
        streamDestroyed: function (event) {
            console.log("Publisher stopped streaming. Reason: "
               + event.reason);
        }
    });
        console.log("Connected to the session.");
		session.publish(publisher)
    }
  });
 }

}  
startVideoGuest();

</script>-->

<div id="container">
  <div id="replaceElementId"></div>
  <div id="replaceElementId2"></div>
  
  
</div>


<!DOCTYPE html>
<html>

<head>
    <title> OpenTok Getting Started </title>
    <link href="http://inventifweb.net/UNI/tokboxcss/app.css" rel="stylesheet" type="text/css">
    <script src="https://static.opentok.com/v2/js/opentok.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</head>

<body>

    <div id="videos">
        <div id="subscriber"></div>
        <div id="publisher"></div>
    </div>

    <script type="text/javascript" src="http://inventifweb.net/UNI/tokboxjs/sampleconfig.js"></script>
    <script type="text/javascript" src="http://inventifweb.net/UNI/tokboxjs/app.js"></script>
	
	<script>
	
	
var apiKey = <?php echo $apiKey; ?>;

var sessionId = "<?php echo $sessionId; ?>";

var uniqueToken = "<?php echo $token;?>";
</script>
</body>

</html>

