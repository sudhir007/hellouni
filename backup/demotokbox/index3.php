
<?php

include 'opentok.phar';

//OpenTok = require('opentok')

use OpenTok\OpenTok;
$apiKey = '45715252';
$apiSecret = '7a5801d197f3f32827e8dc4e666940355105619e';

$opentok = new OpenTok($apiKey, $apiSecret);


use OpenTok\MediaMode;
use OpenTok\ArchiveMode;

// Create a session that attempts to use peer-to-peer streaming:
$session = $opentok->createSession();

// A session that uses the OpenTok Media Router, which is required for archiving:
$session = $opentok->createSession(array( 'mediaMode' => MediaMode::ROUTED ));

// A session with a location hint:
$session = $opentok->createSession(array( 'location' => '12.34.56.78' ));

// An automatically archived session:
$sessionOptions = array(
    'archiveMode' => ArchiveMode::ALWAYS,
    'mediaMode' => MediaMode::ROUTED
);
$session = $opentok->createSession($sessionOptions);


// Store this sessionId in the database for later use
$sessionId = $session->getSessionId();



use OpenTok\Session;
use OpenTok\Role;

// Generate a Token from just a sessionId (fetched from a database)
$token = $opentok->generateToken($sessionId);
// Generate a Token by calling the method on the Session (returned from createSession)
//$token = $session->generateToken();

// Set some options in a token
$token = $session->generateToken(array(
    'role'       => Role::MODERATOR,
    'expireTime' => time()+(7 * 24 * 60 * 60), // in one week
    'data'       => 'name=Sumanta'
));

//print_r($token);



?>




<!DOCTYPE html>
<html>

<head>
    <title> OpenTok Getting Started </title>
    <link href="http://inventifweb.net/demotokbox/app.css" rel="stylesheet" type="text/css">

    <script src="https://static.opentok.com/v2/js/opentok.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</head>

<body>

    <div id="videos">
        <div id="subscriber"></div>
        <div id="publisher"></div>
    </div>



<script type="text/javascript">

$(document).ready(function() {
  // Make an Ajax request to get the OpenTok API key, session ID, and token from the server
 
  
  initializeSession();
  
  
});

function initializeSession() {
  var session = OT.initSession("<?php echo $apiKey; ?>","<?php echo $sessionId; ?>");



  // Connect to the session
  session.connect("<?php echo $token;?>", function(error) {
    // If the connection is successful, initialize a publisher and publish to the session
    if (!error) {
      var publisher = OT.initPublisher('publisher', {
        insertMode: 'append',
        width: '100%',
        height: '100%',
		publishAudio:true, 
		publishVideo:true
      }, function(error) {
      if (error) console.log("initPublisher error "+error.code+" : "+error.message);
});

      session.publish(publisher, function(error) {
      if (error) console.log("session.publish error "+error.code+" : "+error.message);
});
	  
	  session.subscribe(session.stream, 'subscriber', {
      insertMode: 'append',
      width: '100%',
      height: '100%',
	  subscribeToAudio:true, 
	  subscribeToVideo:true
    });
	
    } else {
      console.log('There was an error connecting to the session: ', error.code, error.message);
    }
  });
  
  
  var options = {subscribeToAudio:true, subscribeToVideo:true};
  // Subscribe to a newly created stream
  session.on('streamCreated', function(event) {
    session.subscribe(event.stream, 'subscriber', {
      insertMode: 'append',
      width: '100%',
      height: '100%',
	  subscribeToAudio:true, 
	  subscribeToVideo:true
    });
  });

  session.on('sessionDisconnected', function(event) {
    console.log('You were disconnected from the session.', event.reason);
  });
}




</script>


