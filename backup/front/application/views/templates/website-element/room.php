
    
        <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- Statistics Button Container -->
            	
                
                <!-- Panels Start -->
                 
            	 <?php  if($msg = $this->session->flashdata('flash_message')){?> 
    <div class="mws-form-message success"><?php  echo $msg;?></div>
     <?php  } ?>
	 
	 
	  <?php if($ermsg = $this->session->flashdata('error_message')){?> 
    <div class="mws-form-message error"><?php  echo $ermsg;?></div>
     <?php } ?>
				<div class="mws-panel grid_8">
				<form action="<?php echo base_url();?>website-element/room/delete_mult_room" method="post">
                	<div class="mws-panel-header">
					
                    	<span><i class="icon-table"></i> Rooms
						<a href="<?php echo base_url();?>website-element/room/form"><button type="button" class="btn btn-small">Insert</button></a>
						<button type="submit" name="delete-Room" class="btn btn-small delbtn" id="del">Delete</button>
						
						</span>
						
					
						
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-datatable-fn mws-table">
                            <thead>
                                <tr>
                                    <th style="width: 1px;">Select</th>
									
									<th>Name</th>
                                  
                                    
                                    <th>Sort Order</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($rooms as $room):?>
								<tr>
                                    <td align="center"><input type="checkbox" name="chk[]" class="amenity_chk" id="<?php echo $room->id;?>" value="<?php echo $room->id;?>"/></td>
									<td><?php echo $room->name;?></td>
                                    
									<td><?php echo $room->sort_order;?></td>
									<td class=" ">
                                        <span class="btn-group">
                                              <a class="btn btn-small" href="<?php echo base_url();?>website-element/room/form/<?php echo $room->id;?>/view"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="<?php echo base_url();?>website-element/room/form/<?php echo $room->id;?>"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small delete-row" id="<?php echo base_url(); ?>website-element/room/delete_room/<?php echo $room->id;?>" href="javascript:void(o);"><i class="icon-trash"></i></a></span>
                                    </td>
                                </tr>
                               <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
					
					</form>
                </div>

                                 

                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->
			
