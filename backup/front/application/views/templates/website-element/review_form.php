<?php if($this->uri->segment('5') && $this->uri->segment('5')=='view'){

$view='disabled'; ?>

<script type="text/javascript">
$( document ).ready(function() {
$("#cleditor").cleditor()[0].disable("true");
//$(".btn .btn-primary .pull-right").attr('disabled' , true);
$('.pull-right').prop('disabled', true);

});

</script>
<?php } else { $view=''; }


 ?>
        
        <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- Statistics Button Container -->
            	
                
                <!-- Panels Start -->
				
				 <?php if($msg = $this->session->flashdata('flash_message')){?> 
				<div class="mws-form-message success"><?php echo $msg;?></div>
				 <?php } ?>
				 <?php if(validation_errors()){?> 
   					 <div class="mws-form-message error"><?php echo validation_errors(); ?></div>
     			 <?php } ?>

            	<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-magic"></i> Review</span>
                    </div>
                    <div class="mws-panel-body no-padding">
					
					
                        <?php if($this->uri->segment('4')){?>
						<form class="mws-form wzd-default" action="<?php echo base_url();?>website-element/review/edit/<?php if(isset($id)) echo $id;?>" method="post">
						<?php }else{?>
						<form class="mws-form wzd-default" action="<?php echo base_url();?>website-element/review/create" method="post" enctype="multipart/form-data">
                        <?php }?>    
                            <fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> General</legend>
                                <div id class="mws-form-row">
                                    <label class="mws-form-label">Author <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" <?php echo $view; ?> name="author" class="required large" value="<?php if(isset($author)) echo $author; ?>" required>
                                    </div>
                                </div>
                              
                            <div class="mws-form-row">
                            	 <label class="mws-form-label">Room</label>
                                <div class="mws-form-item">
                                	<select name="room" class="large" <?php echo $view; ?>>  
									<option value="0">Please Select..</option>  
										<?php if($this->uri->segment('4')){  
										    foreach($rooms as $room){
											?>
										<option value="<?php echo $room->id;?>" <?=$room->id==$selectedroom?'selected="selected"':'';?>><?php echo $room->name; ?></option>
											<?php }
										     } else{ 
											 foreach($rooms as $room){?>
									    <option value="<?php echo $room->id;?>"><?php echo $room->name; ?></option>
											<?php } }?>
										</select>
                                </div>
                            </div>	
								
								
								<div class="mws-form-row">
                            	 <label class="mws-form-label">Review Description</label>
                                <div class="mws-form-item">
                                	<textarea name="review_text" <?php echo $view; ?> class="large"><?php if(isset($description)) echo $description; ?></textarea>
                                </div>
                            </div>	
								
								
								
                                 <div class="mws-form-row">
                                    <label class="mws-form-label">Rating</label>
                                    <div class="mws-form-item">
									
									
									<span>Bad</span>&nbsp;&nbsp;
                                   
									 
									<input type="radio" <?php echo $view; ?> name="rating" value="1" <?php if(isset($selectedrating) && $selectedrating== '1') { echo "checked"; } ?>/>&nbsp;&nbsp;
									<input type="radio" <?php echo $view; ?> name="rating" value="2" <?php if(isset($selectedrating) && $selectedrating== '2') { echo "checked"; } ?>/>&nbsp;&nbsp;
									<input type="radio" <?php echo $view; ?> name="rating" value="3" <?php if(isset($selectedrating) && $selectedrating== '3') { echo "checked"; } ?>/>&nbsp;&nbsp;
									<input type="radio" <?php echo $view; ?> name="rating" value="4" <?php if(isset($selectedrating) && $selectedrating== '4') { echo "checked"; } ?>/>&nbsp;&nbsp;
									<input type="radio" <?php echo $view; ?> name="rating" value="5" <?php if(isset($selectedrating) && $selectedrating== '5') { echo "checked"; } ?>/>&nbsp;&nbsp;
									
									
									 
									 
									&nbsp;&nbsp;<span>Good</span>
                                    </div>
                                </div>
								
								<div class="mws-form-row">
                                    <label class="mws-form-label">Status</label>
                                    <div class="mws-form-item">
										<select name="review_status" <?php echo $view; ?>>
										<option value="1" <?php if(isset($status) && $status=='Active'){ echo 'selected="selected"'; } ?>>Active</option>
										<option value="2" <?php if(isset($status) && $status=='Inactive'){ echo 'selected="selected"'; } ?>>Inactive</option>										   
										</select>
                                    </div>
                                </div>
								
								
								
								
                            </fieldset>
                            
                         </form>
                    </div>
                </div>

              
                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->
			
