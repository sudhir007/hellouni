<?php if($this->uri->segment('6') && $this->uri->segment('6')=='view'){

$view='disabled'; ?>

<script type="text/javascript">
$( document ).ready(function() {
$("#cleditor").cleditor()[0].disable("true");
//$(".btn .btn-primary .pull-right").attr('disabled' , true);
$('.pull-right').prop('disabled', true);

});

</script>
<?php } else { $view=''; }


 ?>
        
        <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- Statistics Button Container -->
            	
                
                <!-- Panels Start -->
				
				 <?php if($msg = $this->session->flashdata('flash_message')){?> 
				<div class="mws-form-message success"><?php echo $msg;?></div>
				 <?php } ?>
				 
				<?php if(validation_errors()){?> 
                <div class="mws-form-message error"><?php echo validation_errors(); ?></div>
                <?php } ?>

            	<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-magic"></i> Photo Gallery</span>
                    </div>
                    <div class="mws-panel-body no-padding">
					
					  
                        <?php if($this->uri->segment('5')){?>
						<form class="mws-form wzd-default" action="<?php echo base_url();?>plugins/gallery/edit/photo/<?php echo $this->uri->segment('5'); ?>" method="post">
						<?php }else{?>
						<form class="mws-form wzd-default" action="<?php echo base_url();?>plugins/gallery/create/photo" method="post">
                        <?php }?> 
                            <fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> General</legend>
                                <div id class="mws-form-row">
                                    <label class="mws-form-label">Name <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" <?php echo $view; ?> name="gallery_name" class="required large" value="<?php if(isset($gallery_info)) echo $gallery_info->name; ?>" required>
                                    </div>
                                </div>
                              
                              
                                 <div class="mws-form-row">
                                    <label class="mws-form-label">Sort Order</label>
                                    <div class="mws-form-item">
                                        <input type="text" <?php echo $view; ?> name="gallery_sort_order" class="required email large" value="<?php if(isset($gallery_info)) echo $gallery_info->sort_order; ?>">
                                    </div>
                                </div>
								
								
									<div class="mws-form-row">
                    				<label class="mws-form-label">Status</label>
                    				<div class="mws-form-item">
                    					<select class="large" name="gallery_status" <?php echo $view; ?>>
                    						<?php if(isset($gallery_info) && $gallery_info->stname=='Active'){ ?>
											<option value="1" selected="selected">Active</option>
                    						<option value="2">Inactive</option>
											<?php } elseif(isset($gallery_info) && $gallery_info->stname=='Inactive'){?>
											<option value="1">Active</option>
                    						<option value="2" selected="selected">Inactive</option>
											<?php } else {  ?>
											<option value="1">Active</option>
                    						<option value="2">Inactive</option>
											<?php } ?>
                    					</select>
                    				</div>
                    			</div>
								
								
							</fieldset>
							
							<fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i>Links</legend>
								
								
								<table class="mws-table" id="plugins">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Title</th>
									<th>Alter</th>
                                    <th>Link</th>
                                    <th>Sort Order</th>
									 <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
							
							<?php if($this->uri->segment('5')){ $i=1; foreach($gallery_images as $img){ ?>
							
							<tr id="gallery_row_<?php echo $i; ?>">
                                    <td>
									<?php if($img['path']=='') { ?>
									<img src="<?php echo base_url();?>images/no_image.jpg" width="100" height="100" id="elfileimg-<?php echo $i; ?>" alt="" class="elfileimg">
									<?php } else { ?>
									<img src="<?php echo $img['path'] ; ?>" width="100" height="100" id="elfileimg-<?php echo $i; ?>" alt="" class="elfileimg">
									<?php } ?>
									   <input type="hidden" name="gallery[<?php echo $i; ?>][img]" id="elfileurl-<?php echo $i; ?>" value="<?php echo $img['path'] ; ?>" class="elfileurl" ><br>
									   <input type="button" id="browse-<?php echo $i; ?>" class="btn btn-danger elselect-button" value="Browse">
									
									</td>
                                    <td><input type="text" <?php echo $view; ?> name="gallery[<?php echo $i; ?>][title]" value="<?php echo $img['title'] ; ?>" ></td>
                                    <td><input type="text" <?php echo $view; ?> name="gallery[<?php echo $i; ?>][alter]" value="<?php echo $img['alter'] ; ?>" ></td>
                                    <td><input type="text" <?php echo $view; ?> name="gallery[<?php echo $i; ?>][link]" value="<?php echo $img['link'] ; ?>" ></td>
                                    <td><input type="text" <?php echo $view; ?> name="gallery[<?php echo $i; ?>][sort]" value="<?php echo $img['sort_order'] ; ?>" ></td>
									
									 <td><input type="button" <?php echo $view; ?> class="btn btn-danger" value="Remove" onclick="$('#gallery_row_<?php echo $i; ?>').remove();" /></td>
                                    
                                </tr>
							
							
							
							<?php $i++; } } else { ?>
                                <tr id="gallery_row_<?php echo $row; ?>">
                                    <td>
									<img src="<?php echo base_url();?>images/no_image.jpg" width="100" height="100" id="elfileimg-<?php echo $row; ?>" alt="" class="elfileimg">
									   <input type="hidden" name="gallery[<?php echo $row; ?>][img]" id="elfileurl-<?php echo $row; ?>" value="" class="elfileurl" ><br>
									   <input type="button" id="browse-<?php echo $row; ?>" class="btn btn-danger elselect-button" value="Browse">
									
									</td>
                                    <td><input type="text" name="gallery[<?php echo $row; ?>][title]" value="" ></td>
                                    <td><input type="text" name="gallery[<?php echo $row; ?>][alter]" value="" ></td>
                                    <td><input type="text" name="gallery[<?php echo $row; ?>][link]" value="" ></td>
                                    <td><input type="text" name="gallery[<?php echo $row; ?>][sort]" value="" ></td>
									
									 <td><input type="button" class="btn btn-danger" value="Remove" onclick="$('#gallery_row_<?php echo $row; ?>').remove();" /></td>
                                    
                                </tr>
								
							<?php } ?>	
								
								
                             
                                <tr id="gal-img-foot">
                                    <td colspan="4">&nbsp;</td>
                                    <td ><input type="button" id="add-image" class="btn btn-danger" value="Add Image" /></td>
                                </tr>
                            </tbody>
                        </table>
							
								
								
                             
								
                            </fieldset>
                        </form>
                    </div>
                </div>

              

                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->
			
<script>
$(document).ready(function()
{
var row = <?php echo $row+1; ?>;

$("#add-image").live('click',function()
{

var	html  = '';
	html += '<tr id="gallery_row_'+row+'">';
	html += '<td><img src="<?php echo base_url();?>images/no_image.jpg" width="100" height="100" alt="" id="elfileimg-'+row+'" class="elfileimg"><input type="hidden" name="gallery['+row+'][img]" id="elfileurl-'+row+'" value="" class="elfileurl"><br><input type="button" id="browse-'+row+'" class="btn btn-danger elselect-button" value="Browse"></td>';
	
	html += '<td><input type="text" name="gallery['+row+'][title]" value="" ></td>';
	html += '<td><input type="text" name="gallery['+row+'][alter]" value="" ></td>';
	html += '<td><input type="text" name="gallery['+row+'][link]" value="" ></td>';
	html += '<td><input type="text" name="gallery['+row+'][sort]" value="" ></td>';
	html += '<td><input type="button" class="btn btn-danger" value="Remove" onclick="$(\'#gallery_row_' + row + '\').remove();" /></td>';
	html += '</tr>';


	$('#gal-img-foot').before(html);
 row++;
});

});


</script>