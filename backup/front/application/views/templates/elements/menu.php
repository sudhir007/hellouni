
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Menu
            <small>Manage Menu</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Menu Management</a></li>
            <li class="active">Menu</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              
			  <!-- /.box -->
			  
			  <?php if($this->session->flashdata('flash_message')){ ?>
				<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                <?php echo $this->session->flashdata('flash_message'); ?>
              </div>
			  <?php } ?>

              <div class="box">
			   <form action="<?php echo base_url();?>elements/menu/multidelete" method="post">
                <div class="box-header">
                 <!--<a href="<?php echo base_url();?>location/country/form"><button type="button" style="width:10%; float:left; margin-right:5px;" class="btn btn-primary"><i class="fa fa-plus"></i> Add Country</button></a>-->
				 <button type="submit" class="btn btn-danger multidel" style="width:11%; display:none;"><i class="fa fa-minus"></i> Delete Country</button> 
                </div><!-- /.box-header -->
				
			  
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th align="center">Select</th>
                        <th>Name</th>
                      	<th>Type</th>
                        <th>Status</th>
					  </tr>
                    </thead>
                    <tbody>
					<?php 
					
						foreach($menus as $menu){ ?>
                      <tr>
                        <td align="center" width="10%"><input type="checkbox" class="multichk" name="chk[]" value="<?php echo $menu->id;?>"/>&nbsp;<?php //echo $count++; ?></td>
                        <td><?php echo $menu->name;?></td>
                       
						<td><?php echo $menu->type_name;?></td>
                       
                        <td><?php echo $menu->status_name;?></td> 
						 
					<?php  }
					?>
                      </tr>
                      
                    </tbody>
                    <tfoot>
                      <tr>
                       <th>Select</th>
                        <th>Name</th>
                        <th>Code</th>
                        <th>Status</th>
					  </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     