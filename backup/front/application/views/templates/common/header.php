<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hellouni</title>

    <!-- Bootstrap Core CSS -->
  	<?php //include 'head.php'; ?>
	
	  <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/grayscale.css" rel="stylesheet">

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- Navigation -->
    <?php  //include 'header.php'; ?>
	<style>
.header_icons{
}
.header_icons ul li{
	display:inline-block;
	list-style-type:none;
	margin-bottom:-10px;
	
}
</style>


<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    	<div  class="row" style="background:url(img/header_trans.png) repeat-x; padding: 2px 0px;">
        	<div class="container">
            	<div class="pull-left header_icons"><ul style="padding-left:0px;"><li><img src="img/1.png">INFO@IMPERIAL-OVERSEAS.COM</li>
                 <li><img src="img/2.png"> +91 9167991339 / +91 9819900666</li></ul></div>
                <div class="pull-right hidden-xs header_icons"><ul>
                <li><img src="img/f.png"></li>
                <li><img src="img/t.png"></li>
                <li><img src="img/g.png"></li>
                <li><img src="img/in.png"></li>
                <li><img src="img/login.png"></li>
                <li> | <a href="#"  style="font-size:11px; padding:0; margin:0;">LOGIN</a></li>
                <li> <img src="img/fav.png"><span style="font-size:11px; padding:0; margin:0;">FAVOURITES</span></li>
                </ul></div>
            </div>
         </div>
		 
		 <header class="intro">
        <div class="intro-body">
            <!--  <div class="container">
               <!-- <div class="row">-->
                    <!--<div class="col-md-8 col-md-offset-2">-->
      <!-- <iframe width="100%" height="100%" src="https://www.youtube.com/embed/OhQMUphxPkA?autoplay=1&rel=0&loop=1&playlist=OhQMUphxPkA?modestbranding=1&autohide=1&showinfo=0&controls=0" frameborder="0" allowfullscreen style="z-index:-1" ></iframe>-->
                        <!--<h1 class="brand-heading">Grayscale</h1>
                        <p class="intro-text">A free, responsive, one page Bootstrap theme.<br>Created by Start Bootstrap.</p>
                        <a href="#about" class="btn btn-circle page-scroll">
                            <i class="fa fa-angle-double-down animated"></i>
                        </a>-->
                   <!-- </div>-->
               <!-- </div>-->
            <!-- </div>-->
        </div>
    </header>
	
	
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">
                   <img src="img/logo.png" width="200" />
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse" style="margin-top: 10px;">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">HOME</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#download">ABOUT US</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">STUDENTS</a>
                    </li>
                     <li>
                        <a class="page-scroll" href="#contact">UNIVERSITY</a>
                    </li>
                     <li>
                        <a class="page-scroll" href="#contact">FLY ABROARD</a>
                    </li>
                     <li>
                        <a class="page-scroll" href="#contact">CONTACT US</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
 <!--header end-->