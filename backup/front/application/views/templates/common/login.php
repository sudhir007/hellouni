<!DOCTYPE html>

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Administrator | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/css/bootstrap.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/iCheck/square/blue.css">
	<style type="text/css">
	.error{color:red;}
	
	</style>
	
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>images/favicon-96x96.png">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
	  <img src="<?php echo base_url();?>images/HelloUni-Logo.png" style="width: 75%;">
        <a href="javascript:void();"><b>Administrator</b></a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        		
		
		<?php if($this->session->flashdata('flash_message')){ ?>
		<p class="login-box-msg" style="font-size: 20px; color: red;"><?php echo $this->session->flashdata('flash_message');?></p>
		<?php } else{?>
		<p class="login-box-msg">Please Sign In</p>
		<?php } ?>
		</p>
		
		
        <form action="<?php echo base_url(); ?>common/login/log_in" method="post">
          <div class="form-group has-feedback">
            <input type="email" name="email" id="email" class="form-control" placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
			<div id="emailerror" class="error"></div>
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="password" id="password" class="form-control" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
			<div id="passworderror" class="error"></div>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> Remember Me
                </label>
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" id="signin" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
        </form>

        

        <a href="#">I forgot my password</a><br>
       

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url(); ?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url(); ?>plugins/iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
	
	<script type="text/javascript">
	$( document ).ready(function() {
     
	 $( "#signin" ).click(function(e) {
       var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	   var email 	= $( "#email" ).val();
	   var password = $( "#password" ).val();
	   
	   if(email==''){
	  
	   $( "#emailerror" ).text("Email is Required"); return false; e.stoppropagation();
	  
	   }else if(!emailReg.test(email)){
	  
	   $( "#emailerror" ).text("Email is Invalid"); return false; e.stoppropagation();
	   }else if(password==''){
	   
	    $( "#emailerror" ).text("");
		$( "#passworderror" ).text("Password is Empty"); return false; e.stoppropagation();
	   }
	   
	   
     });
	 
      });
	</script>
  </body>

