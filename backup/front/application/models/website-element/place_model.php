<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model 
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 
  <Reverse bidding system> 
    Copyright (C) <2009>  <FreeLance PHP Script>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
    If you want more information, please email me at support@freelancephpscript.com or 
    contact us from http://www.freelancephpscript.com/contact 
 
 
 */
	 class Place_model extends CI_Model {
	 
 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
	
	 
	 
	 
	 
	 function getPlaces()
     { 
	 $this->db->from('nearby_place_master');
	 $this->db->join('infra_description_master', 'infra_description_master.id = nearby_place_master.description_id','left');
	 $this->db->join('infra_status_master', 'infra_status_master.id = nearby_place_master.status','left');
	
	 $this->db->select('nearby_place_master.id,nearby_place_master.description_id,nearby_place_master.photo_gallery_id,nearby_place_master.video_gallery_id,nearby_place_master.distance,infra_description_master.name,infra_description_master.description, infra_status_master.name as status');
	 
	 $result=$this->db->get()->result();
	 return $result;
	 
    }
	
	
	 function getPlaceByid($conditions)
     { 
	 $this->db->where($conditions);
	 $this->db->from('nearby_place_master');
	 $this->db->join('infra_description_master', 'infra_description_master.id = nearby_place_master.description_id','left');
	 $this->db->join('infra_status_master', 'infra_status_master.id = nearby_place_master.status','left');
	
	 $this->db->select('nearby_place_master.id,nearby_place_master.description_id,nearby_place_master.photo_gallery_id,nearby_place_master.video_gallery_id,nearby_place_master.distance,infra_description_master.name,infra_description_master.description, infra_status_master.name as status');
	 
	 $result=$this->db->get()->result();
	 return $result;
	 
    }
	
	function getPlaces_By_Property($conditions){
	
	$this->db->where($conditions);
	$this->db->from('property_to_nearplace');
	$this->db->join('nearby_place_master', 'nearby_place_master.id = property_to_nearplace.nearby_id','left');
	$this->db->join('infra_description_master', 'infra_description_master.id = nearby_place_master.description_id','left');
	$this->db->join('infra_status_master', 'infra_status_master.id = nearby_place_master.status','left');
	
	$this->db->select('nearby_place_master.id,nearby_place_master.description_id,nearby_place_master.photo_gallery_id,nearby_place_master.video_gallery_id,nearby_place_master.distance,infra_description_master.name,infra_description_master.description, infra_status_master.name as status');
	 
	 $result=$this->db->get()->result();
	return $result;
	 /*echo '<pre>';
	 print_r($result);
	 echo '</pre>';
	 exit();*/
	
	}
	
	
	
	function insertPlace($insertData=array())
	 {
	  		
	 $description_tbl_data = array(
     'name' => $insertData['name'],
     'description' => $insertData['description']
     );
	 
	 $this->db->insert('infra_description_master', $description_tbl_data);
	 $description_id = $this->db->insert_id();
	 
	 $place_tbl_data = array(
     'description_id' =>  $description_id,
     'photo_gallery_id' => $insertData['photo_gallery_id'],
	 'photo_gallery_id' => $insertData['photo_gallery_id'],
	 'distance' => $insertData['distance'],
	 'status' => $insertData['status'],
	 );
	 
	 $this->db->insert('nearby_place_master', $place_tbl_data);
	
	 return 'success';
	 }
	 
	 
	 
	 
	 
	  function editPlace($insertData=array())
	 {
	   $description_tbl_data = array(
       'name' => $insertData['name'],
       'description' => $insertData['description'],
	   
       );
	 
	 $place_details = $this->db->get_where('nearby_place_master', array('nearby_place_master.id' => $insertData['place_id']))->row();
	
	 
	 
	 // update infra_description_master
	 $this->db->where('id',$place_details->description_id);
	 $this->db->update('infra_description_master',$description_tbl_data);
	 
	$place_tbl_data = array(
    
     'photo_gallery_id' => $insertData['photo_gallery_id'],
	 'photo_gallery_id' => $insertData['photo_gallery_id'],
	 'distance' => $insertData['distance'],
	 'status' => $insertData['status'],
	 );
	 
	 // update amenities_master
	 $this->db->where('id',$insertData['place_id']);
	 $this->db->update('nearby_place_master',$place_tbl_data);
	 
	 return 'success';
	 }
	 
	 
	 
	  function deletePlace($id)
	 {
	   
	 $place_details = $this->db->get_where('nearby_place_master', array('nearby_place_master.id' => $id))->row();
	
	 
	 
	 
	 $this->db->delete('infra_description_master', array('id' => $place_details->description_id)); 
	 $this->db->delete('nearby_place_master', array('id' => $id)); 
	 return 'success';
	 }
	 
	 
	 
	 
	 
}
	
	
	
	
	
	
	
	
	
	
	
	
	
?>