

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Course
            <small>Manage Course</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Course Management</a></li>
			<?php if(!$this->uri->segment('4')){?>
            <li class="active">Add Course</li>
			<?php }else{?>
			 <li class="active">Modify Course</li>
			<?php }?>
			
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
           
            <!-- right column -->
            <div class="col-md-10">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">
				  <?php if(!$this->uri->segment('4')){?>
				   Add Course
				  <?php }else{?>
				  Modify Course
				  <?php }?>
				  </h3>
                </div><!-- /.box-header -->
                <!-- form start -->
				
				 <?php if($this->uri->segment('4')){?>
				<form class="form-horizontal" action="<?php echo base_url();?>course/course/edit/<?php if(isset($id)) echo $id;?>" method="post" name="addadmin">
				<?php }else{?>
                <form class="form-horizontal" action="<?php echo base_url();?>course/course/create" method="post" name="addadmin">
                <?php } ?>
				
				
                
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                      <div class="col-sm-10">
                        <input type="name" class="form-control" id="coursename" name="coursename" placeholder="Course Name" value="<?php if(isset($name)) echo $name; ?>">
                      </div>
                    </div>
					
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Details</label>
                      <div class="col-sm-10">
                       
						<textarea id="course_details" name="course_details"><?php if(isset($details)) echo $details; ?></textarea>
						<script type="text/javascript">CKEDITOR.replace( 'course_details' );</script>
                      </div>
                    </div>
					
                    <div class="form-group">
                  		 <label for="status" class="col-sm-2 control-label">Type</label>
						  <div class="col-sm-3">
						  <select class="form-control" name="coursetype" id="coursetype">
						  <?php foreach($course_types as $types){ ?>
							<option value="<?php echo $types->id; ?>"<?php if(isset($type) && $type==$types->id){ echo 'selected="selected"'; } ?>>
							<?php echo $types->type; ?>
							</option>
							<?php } ?>
						  </select>
						  </div>
						  
						  <?php if (isset($parent) && $parent!=''){?>
						  <div id="parent_level" class="level">
						  <?php }else{?>
						  <div id="parent_level" class="level" style="display:none;">
						  <?php }?>
						 
						 <label for="status" class="col-sm-3 control-label">Parent Course</label> 
						  <div class="col-sm-3">
						  <select class="form-control" name="parent_course" id="parent_course">
							
						  </select>
						  </div>
						  </div>
               		</div>
					
					<div class="form-group">
                  		 <label for="status" class="col-sm-2 control-label">Status</label>
						  <div class="col-sm-10">
						  <select class="form-control" name="status">
							<option value="1" <?php if(isset($status) && $status=='1'){ echo 'selected="selected"'; } ?>>Active</option>
							<option value="2" <?php if(isset($status) && $status=='2'){ echo 'selected="selected"'; } ?>>Inactive</option>	
						  </select>
						  </div>
               		</div>
					
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
					<button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                    <button type="submit" class="btn btn-info pull-right">
					 <?php if(!$this->uri->segment('4')){?>Create<?php }else{?> Update<?php }?></button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              <!-- general form elements disabled -->
             
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     