<?php

class MY_Controller extends CI_Controller {

  protected $data = array();

  function __construct() {
    parent::__construct();
  }

  function render_page($view) {
   
   
    $userdata=$this->session->userdata('user');
	
	if($userdata){ 
	
	
	$this->outputData['user'] = $userdata;
	
	
    $this->load->view('templates/common/header', $this->outputData);
    $this->load->view('templates/common/sidebar', $this->data);
    $this->load->view($view, $this->data);
    $this->load->view('templates/common/footer', $this->data);
   }else{
   
   redirect('common/login');
   }
  }

}  // end-class MY_Controller
//...


?>