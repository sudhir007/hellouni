<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Attribute extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

	
	function index()
	{   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('website-element/attribute_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }	
		 
		$cond_attr1 = array('attribute_master.status' => '1'); 
		$cond_attr2 = array('attribute_master.status' => '2'); 	
        $this->outputData['attributes'] = $this->attribute_model->getAttributes($cond_attr1,$cond_attr2);
        $this->render_page('templates/website-element/attribute',$this->outputData);
	
    } 
	
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('website-element/attribute_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }		
		 
		 if($this->uri->segment('4')){
		 $conditions = array('attribute_master.id' => $this->uri->segment('4'));
		 $attribute_info = $this->attribute_model->getAttributeByid($conditions);
		
		
		$this->outputData['id'] 				= $attribute_info->id;
		$this->outputData['name'] 				= $attribute_info->name;
		$this->outputData['sort_order'] 		= $attribute_info->sort_order;
		$this->outputData['status'] 			= $attribute_info->status_name;
		}
		 
		
        $this->render_page('templates/website-element/attribute_form',$this->outputData);
	
     } 
	 
	 
	 
	 function create()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
       $this->load->model('website-element/attribute_model');
	   
	   $this->form_validation->set_rules('attr_name', 'Attribute Name', 'required|trim|xss_clean');
	   if ($this->form_validation->run() == FALSE)
		{
		
			  $this->render_page('templates/website-element/attribute_form');
		}
		else
		{
		
		 $data = array(
        'name' 			=> $this->input->post('attr_name'),
        'sort_order' 	=>	$this->input->post('attr_sort_order'),
		'status' 		=>	$this->input->post('attr_status')
		
	     );
	    
		 if($this->attribute_model->insertAttribute($data)=='success'){
	 
	     $this->session->set_flashdata('flash_message', "Success: You have saved Attribute!");
	     redirect('website-element/attribute/index');
	     }
	 
	 
	   }
	}
	 
	 
	 function edit()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/attribute_model');
	   
	   $this->form_validation->set_rules('attr_name', 'Attribute Name', 'required|trim|xss_clean');
	   if ($this->form_validation->run() == FALSE)
		{
		
			  $this->render_page('templates/website-element/attribute_form');
		}
		else
		{
			 $data = array(
            'attr_id' 		=> 	$this->uri->segment('4'),
			'name' 			=> $this->input->post('attr_name'),
        	'sort_order' 	=>	$this->input->post('attr_sort_order'),
			'status' 		=>	$this->input->post('attr_status')
	        );
			
	         if($this->attribute_model->editAttribute($data)=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have modified Attribute!");
	         redirect('website-element/attribute/index');
	         }
	    }
	}
    
	
	function delete()
	 {   
	   
	   $this->load->helper(array('form', 'url'));
	   $this->load->model('website-element/attribute_model');
		
		if($this->attribute_model->deleteAttribute($this->uri->segment('4'))=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have deleted Attribute!");
	         redirect('website-element/attribute/index');
	    }
	}
	
	
	function delete_mult(){
	
	    $this->load->helper(array('form', 'url'));

		$this->load->model('website-element/attribute_model');
		
		$array = $this->input->post('chk');
		foreach($array as $id){
		$this->attribute_model->deleteAttribute($id);
		}
		$this->session->set_flashdata('flash_message', "Success: You have deleted Attribute!");
		redirect('website-element/attribute/index');
	
	
	}
	 

}//End  Home Class

?>