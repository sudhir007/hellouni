<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model 
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 
  <Reverse bidding system> 
    Copyright (C) <2009>  <FreeLance PHP Script>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
    If you want more information, please email me at support@freelancephpscript.com or 
    contact us from http://www.freelancephpscript.com/contact 
 
 
 */
	 class User_model extends CI_Model {
	 
 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
	function getUser($condition1,$condition2)
	 {
	 	if(count($condition1)>0 || count($condition2)>0  || isset($condition1) || isset($condition2))
		{
	     $this->db->where($condition1);
	     $this->db->where($condition2);
	  }
		
		
		$this->db->from('user_master');
		$this->db->join('type_master', 'type_master.type_id = user_master.type','left');
		$this->db->join('status_master', 'status_master.id = user_master.status','left');
		
		
		
		$this->db->select('user_master.id,user_master.name,user_master.email,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,status_master.status as status_name');
		
		
		
		$result=$this->db->get()->result();
		return $result;
			
	 }
	
	
	
	
	function checkUser($conditions=array())
	 {
	 	
		//echo 'aaaaaa'; die('----');
		$this->db->where($conditions);
		$this->db->from('user_master');
		$this->db->join('type_master', 'type_master.type_id = user_master.type','left');
		$this->db->join('status_master', 'status_master.id = user_master.status','left');
		
		
		
		$this->db->select('user_master.id,user_master.name,user_master.email,user_master.image,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,status_master.status as status_name');
		
		
		
		$result=$this->db->get()->row();
		return $result;
			
	 }
	 
	 function getStudentDetails($conditions=array())
	 {
	 	
		//echo 'aaaaaa'; die('----');
		$this->db->where($conditions);
		$this->db->from('user_master');
		$this->db->join('type_master', 'type_master.type_id = user_master.type','left');
		$this->db->join('status_master', 'status_master.id = user_master.status','left');
		$this->db->join('student_details', 'student_details.user_id = user_master.id','left');
		
		
		
		$this->db->select('user_master.id,user_master.name,user_master.email,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,user_master.fid,status_master.status as status_name,student_details.address,student_details.country,student_details.city,student_details.zip,student_details.dob,student_details.phone,student_details.parent_name,student_details.parent_mob,student_details.parent_occupation,student_details.hear_about_us,student_details.passport,student_details.desired_destination');
		
		
		
		$result=$this->db->get()->row();
		return $result;
			
	 }
	 
	 function getLead($conditions=array())
	 {
	 	
		
		$this->db->where($conditions);
		$this->db->from('lead_master');
		
		$this->db->select('lead_master.id,lead_master.name,lead_master.email,lead_master.phone,lead_master.mainstream,lead_master.courses,lead_master.degree,lead_master.countries');
		$this->db->order_by("id", "desc");
		$result=$this->db->get()->row();
		return $result;
			
	 }
	 
	 
	 function checkUserBy_Email($conditions=array())
	 {
	 	
		
		$this->db->where($conditions);
		$this->db->from('infra_email_master');
		$this->db->join('infra_user_master', 'infra_email_master.id = infra_user_master.email','left');
		$this->db->join('infra_role_master', 'infra_role_master.id = infra_user_master.role_id','left');
		$this->db->join('infra_status_master', 'infra_status_master.id = infra_user_master.status_id','left');
		
		$this->db->select('infra_user_master.id,infra_user_master.name,infra_user_master.user_name,infra_role_master.name as role,infra_email_master.email,infra_user_master.status_id,infra_status_master.name as statusname');
		
		
		
		$result=$this->db->get()->row();
		return $result;
			
	 }
	 
	 
	 function getUniversityDetailsByid ($conditions){
	
	 $this->db->where($conditions);
	
	 $this->db->from('university_details'); 
	 /*$this->db->join('user_master', 'user_master.id = university_details.university_id','left');
	 $this->db->join('status_master', 'status_master.id = user_master.status','left');*/
	
		
	 $this->db->select('university_details.university_id,university_details.about,university_details.website,university_details.banner,university_details.student,university_details.internationalstudent,university_details.facilities,university_details.address,university_details.country,university_details.state,university_details.city');
		
	 $result=$this->db->get()->row();

	 return $result;
	}
	
	
	function getuniversity_to_degree_to_course ($conditions){
	
	 $this->db->where($conditions);
	 
	 $this->db->from('university_to_degree_to_course');
	
	 $this->db->select('university_to_degree_to_course.id,university_to_degree_to_course.degree_id,university_to_degree_to_course.course_id,university_to_degree_to_course.university_id,university_to_degree_to_course.language,university_to_degree_to_course.admissionsemester,university_to_degree_to_course.beginning,university_to_degree_to_course.duration,university_to_degree_to_course.application_deadline,university_to_degree_to_course.details,university_to_degree_to_course.tutionfee,university_to_degree_to_course.enrolmentfee,university_to_degree_to_course.livingcost,university_to_degree_to_course.jobopportunities,university_to_degree_to_course.required_language,university_to_degree_to_course.academicrequirement,university_to_degree_to_course.academicrequirement');
		
	 $result=$this->db->get()->row();

	 return $result;
	}
	
	
	function checkFavourites($conditions=array())
	 { 	
		//echo 'aaaaaa'; die('----');
		$this->db->where($conditions);
		$this->db->from('favourites');
		/*$this->db->join('user_master', 'user_master.id = favourites.user_id','left');
		$this->db->join('status_master', 'status_master.id = user_master.status','left');*/
				
		$this->db->select('favourites.id,favourites.user_id,favourites.university_id,favourites.course');
				
		$result=$this->db->get()->row();
		return $result;
			
	 }
	 
	 function getFavourites($conditions=array())
	 { 	
		//echo 'aaaaaa'; die('----');
		$this->db->where($conditions);
		$this->db->from('favourites');
		$this->db->join('user_master', 'user_master.id = favourites.university_id','left');
		$this->db->join('university_details', 'university_details.university_id = user_master.id','left');
		$this->db->join('country_master', 'country_master.id = university_details.country','left');
		$this->db->join('course_master', 'course_master.id = favourites.course','left');
		$this->db->join('degree_master', 'degree_master.id = favourites.degree','left');
		
				
		$this->db->select('favourites.id,favourites.user_id,favourites.university_id as uni_id,favourites.course,user_master.name,user_master.image,country_master.name as countryname,course_master.name as coursename,degree_master.name as degreename');
				
		$result=$this->db->get()->result();
		return $result;
			
	 }
	
	 function insertFavourites($data=array())
	 { 
	   $this->db->insert('favourites', $data);
	   return $favouritesid = $this->db->insert_id();
	 }
	 
	 function deleteFavourites($condition)
	 {
	 $this->db->where($condition);
	 if($this->db->delete('favourites'))
	   return true;
	 }
	
	
	function addRemerberme($insertData=array(),$expire)
	{
	
		 $this->auth_model->setUserCookie('uname',$insertData['username'], $expire);
		 $this->auth_model->setUserCookie('pwd',$insertData['password'], $expire);
		 
		// echo $val=get_cookie('uname',TRUE); exit;
		 echo $this->input->cookie('uname', TRUE); exit;
		 //die();
		 
	}
		
	function removeRemeberme()
	{
	
	  $this->auth_model->clearUserCookie(array('uname','pwd'));
	  
	}
	
	
	function insertUser($insertData=array())
	 {
	  		
	 $user_master_data = array(
     'name' 		=> $insertData['name'],
     'email' 		=> $insertData['email'],
	 'password' 	=> $insertData['password'],
	 'type' 		=> $insertData['type'] ,
	 'createdate' 	=> $insertData['createdate'],
	 'status' 		=> $insertData['status'],
	 'notification' => $insertData['notification']
     );
	 
	 $this->db->insert('user_master', $user_master_data);
	 $user_id = $this->db->insert_id();
	 
	 $user_permission_course_data = array(
     'user_id' 	=> $user_id,
     'view' 	=> $insertData['view'],
	 'create' 	=> $insertData['create'],
	 'edit' 	=> $insertData['edit'],
	 'del' 		=> $insertData['del']
	  );
	 
	 $this->db->insert('user_permission_course', $user_permission_course_data);
	 return 'success';
	 
	 }
	 
	 
	 function insertStudent($insertData=array())
	 {
	  		
	 $user_master_data = array(
     'name' 		=> $insertData['name'],
     'email' 		=> $insertData['email'],
	 'password' 	=> $insertData['password'],
	 'image' 		=> $insertData['image'],
	 'type' 		=> $insertData['type'] ,
	 'createdate' 	=> $insertData['createdate'],
	 'status' 		=> $insertData['status'],
	 'notification' => '0',
	 'flag' 		=> '0',
	 'activation' 	=> $insertData['activation'],
	 'fid' 	=> $insertData['activation']
     );
	 
	 $this->db->insert('user_master', $user_master_data);
	 $user_id = $this->db->insert_id();
	 
	 $studentDetails_data = array(
     'user_id' 	=> $user_id,
     'address' 	=> $insertData['address'],
	 'country' 	=> $insertData['country'],
	 'city' 	=> $insertData['city'],
	 'zip' 		=> $insertData['zip'],
	 'dob' 		=> $insertData['dob'],
	 'phone' 	=> $insertData['phone']
	  );
	 
	 $this->db->insert('student_details', $studentDetails_data);
	 return $user_id;
	 
	 }
	 
	 function insertUserMaster($insertData=array())
	 {	
		 $this->db->insert('user_master', $insertData);
		 $user_id = $this->db->insert_id();	 
		 return $user_id;	 
	 }
	 	 
	 function insertStudentDetails($insertData=array())
	 {	
		 $this->db->insert('student_details', $insertData);		 
		 return $this->db->insert_id(); 
	 }
	 
	 
	 
	 
	 function insertLead($insertData=array())
	 {
	  		
	
	 $this->db->insert('lead_master', $insertData);
	 $lead_id = $this->db->insert_id();
	 
	 return $lead_id;
	 
	 }
	 
	 function updateUserInfo($insertData=array(),$id)
	 {	 
	 
	 $this->db->where('id',$id);
	 $this->db->update('user_master',$insertData); 
	 
	  return true;
	 }
	 
	 function updateStudentDetails($insertData=array(),$id)
	 {	 
	 
	 $this->db->where('user_id',$id);
	 $this->db->update('student_details',$insertData); 
	 
	  return true;
	 }
	 
	 
	 function updateLeadInfo($insertData=array(),$id)
	 {	 
	 
	 $this->db->where('id',$id);
	 $this->db->update('lead_master',$insertData); 
	 
	  return true;
	 }
	
	
	function getUserByid ($conditions){
	
	 $this->db->where($conditions);
	
	 
	 $this->db->from('user_master');
	 $this->db->join('type_master', 'type_master.type_id = user_master.type','left');
	 $this->db->join('status_master', 'status_master.id = user_master.status','left');
	 $this->db->join('user_permission_course', 'user_permission_course.user_id = user_master.id','left');
		
	 $this->db->select('user_master.id,user_master.name,user_master.email,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,status_master.status as status_name,user_master.notification,user_permission_course.view,user_permission_course.create,user_permission_course.edit,user_permission_course.del');
		
	 $result=$this->db->get()->row();

	 return $result;
	}
	
	
	function getUniversityByid ($conditions){
	
	 $this->db->where($conditions);
	
	 
	 $this->db->from('user_master');
	 $this->db->join('university_details', 'university_details.university_id = user_master.id','left');
	 $this->db->join('country_master', 'country_master.id = university_details.country','left');
	 $this->db->join('status_master', 'status_master.id = user_master.status','left');
	
		
	 $this->db->select('user_master.id,user_master.name,user_master.email,user_master.type,university_details.about,university_details.country,country_master.code');
		
	 $result=$this->db->get()->row();

	 return $result;
	}
	
	
	function editUser($insertData=array())
	 {
	 
	 // Update User table
		$user_master_data = array(
		 'name' 		=> $insertData['name'],
		 'email' 		=> $insertData['email'],
		 'password' 	=> $insertData['password'],
		 'type' 		=> $insertData['type'] ,
		 'status' 		=> $insertData['status'],
		 'notification' => $insertData['notification']
		 );
	 
	 $this->db->where('id',$insertData['id']);
	 $this->db->update('user_master',$user_master_data);
	 
	 $this->db->from('user_permission_course');
	 $this->db->where('user_id',$insertData['id']);		
	 $this->db->select('*');
		
	 $is_user_exists=$this->db->get()->row();
	 //print_r($is_user_exists); exit;

	 // $is_user_exists = $this->db->get_where('user_permission_course', array('user_permission_course.user_id' => $insertData['id']))->row();
	   if($is_user_exists){	 
		 
		 $user_permission_course_data = array(
		 'view' 	=> $insertData['view'],
		 'create' 	=> $insertData['create'],
		 'edit' 	=> $insertData['edit'],
		 'del' 		=> $insertData['del']
		  );
	 	$this->db->where('user_id',$insertData['id']);
	 	$this->db->update('user_permission_course',$user_permission_course_data);
	   }else{
	    
		$user_permission_course_data = array(
		 'user_id'	=> $insertData['id'],
		 'view' 	=> $insertData['view'],
		 'create' 	=> $insertData['create'],
		 'edit' 	=> $insertData['edit'],
		 'del' 		=> $insertData['del']
		  );
	 	$this->db->insert('user_permission_course', $user_permission_course_data);
	 
	   }
	  return 'success';
	 }
	 
	 function deleteUser($id)
	 {
	  $data = array('status' => '5');  
	   
	 $this->db->where('id',$id);
	 $this->db->update('user_master',$data);
	 return 'success';
	 }
	
	function deleteTrash($id)
	 {
	  	   
	 $this->db->where('id', $id);
	 $this->db->delete('user_master');
	 return 'success';
	 }
	 
	 
	 function checkDesiredCourse($conditions=array())	 {
	 	
		//echo 'aaaaaa'; die('----');
		$this->db->where($conditions);
		$this->db->from('desired_courses');
		$this->db->join('user_master', 'user_master.id = desired_courses.student_id','left');	
		
		/*$this->db->select('desired_courses.id,desired_courses.student_id,desired_courses.desired_course_name,desired_courses.	desired_course_level,desired_courses.desired_course_intake,desired_courses.desired_course_year');*/
		$this->db->select('*');				
		$result=$this->db->get()->row();
		return $result;
			
	 }
	 
	 function updateDesiredCourse($insertData=array(),$id)
	 {	 
	 $this->db->where('student_id',$id);
	 $this->db->update('desired_courses',$insertData);	 
	  return true;
	 }
	 
	  function insertDesiredCourse($data=array())
	 { 
	   $this->db->insert('desired_courses', $data);
	   return $desired_courses_id = $this->db->insert_id();
	 }
	 
	 function deleteDesiredCourse($student_id)
	 {
		 $this->db->where('student_id',$student_id);
		 if($this->db->delete('desired_courses'))
		    return true;
	 }
	 
	 function checkSecondaryQualification($conditions=array())	 {
		$this->db->where($conditions);
		$this->db->from('secondary_qualification');
		$this->db->join('user_master', 'user_master.id = secondary_qualification.student_id','left');		
		$this->db->select('*');				
		$result=$this->db->get()->row();
		return $result;			
	 }
	 
	 function updateSecondaryQualification($insertData=array(),$id)
	 {	 
	 $this->db->where('student_id',$id);
	 $this->db->update('secondary_qualification',$insertData);	 
	  return true;
	 }
	 
	  function insertSecondaryQualification($data=array())
	 { 
	   $this->db->insert('secondary_qualification', $data);
	   return $secondary_qualification_id = $this->db->insert_id();
	 }
	 
	 
	 function deleteSecondaryQualification($student_id)
	 {
		 $this->db->where('student_id',$student_id);
		 if($this->db->delete('secondary_qualification'))
		    return true;
	 }
	 
	 
	 function checkDiplomaQualification($conditions=array())	 {
		$this->db->where($conditions);
		$this->db->from('diploma_qualification');
		$this->db->join('user_master', 'user_master.id = diploma_qualification.student_id','left');		
		$this->db->select('*');				
		$result=$this->db->get()->row();
		return $result;			
	 }
	 
	 function updateDiplomaQualification($insertData=array(),$id)
	 {	 
	 $this->db->where('student_id',$id);
	 $this->db->update('diploma_qualification',$insertData);	 
	  return true;
	 }
	 
	  function insertDiplomaQualification($data=array())
	 { 
	   $this->db->insert('diploma_qualification', $data);
	   return $diploma_qualification_id = $this->db->insert_id();
	 }
	 
	 
	 function deleteDiplomaQualification($student_id)
	 {
		 $this->db->where('student_id',$student_id);
		 if($this->db->delete('diploma_qualification'))
		    return true;
	 }
	 
	 function checkHigher_secondary_qualification($conditions=array())	 {
		$this->db->where($conditions);
		$this->db->from('higher_secondary_qualification');
		$this->db->join('user_master', 'user_master.id = higher_secondary_qualification.student_id','left');		
		$this->db->select('*');				
		$result=$this->db->get()->row();
		return $result;			
	 }
	 
	 function updateHigher_secondary_qualification($insertData=array(),$id)
	 {	 
	 $this->db->where('student_id',$id);
	 $this->db->update('higher_secondary_qualification',$insertData);	 
	  return true;
	 }
	 
	  function insertHigher_secondary_qualification($data=array())
	 { 
	   $this->db->insert('higher_secondary_qualification', $data);
	   return $higher_secondary_qualification_id = $this->db->insert_id();
	 }
	 
	 
	 function deleteHigher_secondary_qualification($student_id)
	 {
		 $this->db->where('student_id',$student_id);
		 if($this->db->delete('higher_secondary_qualification'))
		    return true;
	 }
	 
	 
	 // Bachelor
	 
	 function checkBachelorQualification($conditions=array())	 {
		$this->db->where($conditions);
		$this->db->from('bachelor_qualification');
		$this->db->join('user_master', 'user_master.id = bachelor_qualification.student_id','left');		
		$this->db->select('*');				
		$result=$this->db->get()->row();
		return $result;			
	 }
	 
	 function updateBachelorQualification($insertData=array(),$id)
	 {	 
	 $this->db->where('student_id',$id);
	 $this->db->update('bachelor_qualification',$insertData);	 
	  return true;
	 }
	 
	  function insertBachelorQualification($data=array())
	 { 
	   $this->db->insert('bachelor_qualification', $data);
	   return $secondary_qualification_id = $this->db->insert_id();
	 }
	 
	 
	 function deleteBachelorQualification($student_id)
	 {
		 $this->db->where('student_id',$student_id);
		 if($this->db->delete('bachelor_qualification'))
		    return true;
	 }
	 
	  // Master
	 
	 function checkMasterQualification($conditions=array())	 {
		$this->db->where($conditions);
		$this->db->from('master_qualification');
		$this->db->join('user_master', 'user_master.id = master_qualification.student_id','left');		
		$this->db->select('*');				
		$result=$this->db->get()->row();
		return $result;			
	 }
	 
	 function updateMasterQualification($insertData=array(),$id)
	 {	 
	 $this->db->where('student_id',$id);
	 $this->db->update('master_qualification',$insertData);	 
	  return true;
	 }
	 
	  function insertMasterQualification($data=array())
	 { 
	   $this->db->insert('master_qualification', $data);
	   return $secondary_qualification_id = $this->db->insert_id();
	 }
	 
	 
	 function deleteMasterQualification($student_id)
	 {
		 $this->db->where('student_id',$student_id);
		 if($this->db->delete('master_qualification'))
		    return true;
	 }
	
}
?>