<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model 
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 
  <Reverse bidding system> 
    Copyright (C) <2009>  <FreeLance PHP Script>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
    If you want more information, please email me at support@freelancephpscript.com or 
    contact us from http://www.freelancephpscript.com/contact 
 
 
 */
	 class Attribute_model extends CI_Model {
	 
 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
	function insertAttribute($insertData=array())
	 {
	  $this->db->insert('attribute_master', $insertData);
	  return 'success';
	 }
	 
	 
	 
	 
	 function getAttributes($condition1,$condition2)
     { 
	 
	 if(count($condition1)>0 || count($condition2)>0  || isset($condition1) || isset($condition2)){
	  $this->db->where($condition1);
	  $this->db->or_where($condition2);
	  }
	  
	 $this->db->from('attribute_master');
	 $this->db->join('infra_status_master', 'infra_status_master.id = attribute_master.status','left');
	 $this->db->select('attribute_master.id,attribute_master.name,attribute_master.sort_order,attribute_master.status,infra_status_master.name as status_name');
	 
	 $result=$this->db->get()->result();
	 
	
	 return $result;
	 
    }
	
	function getAttributeByid($conditions)
     { 
	 $this->db->where($conditions);
	 $this->db->from('attribute_master');
	  $this->db->join('infra_status_master', 'infra_status_master.id = attribute_master.status','left');
	 $this->db->select('attribute_master.id,attribute_master.name,attribute_master.sort_order,attribute_master.status,infra_status_master.name as status_name');
	 
	 $result=$this->db->get()->row();

	 return $result;
	 
    }
	
	function getPropertiesBy_Room($conditions)
     { 
	 $this->db->where($conditions);
	 $this->db->from('room_to_property');
	
	 $this->db->select('room_to_property.id,room_to_property.property_id');
	 
	 $result=$this->db->get()->result();

	 return $result;
	 
    }
	
	function getAmenitiesBy_Room($conditions)
     { 
	 $this->db->where($conditions);
	 $this->db->from('amenities_to_rooms');
	
	 $this->db->select('amenities_to_rooms.id,amenities_to_rooms.room_type_id,amenities_to_rooms.amenities_id');
	 
	 $result=$this->db->get()->result();

	 return $result;
	 
    }
	
	
	 
	 function getContactmaster_info($conditions)
     { 
	    $query = $this->db->get_where('infra_contact_master', $conditions)->result();
		return $query; 
		 
	 }
	 
	 
	 
	 
	 function editAttribute($insertData=array())
	 {
	   $attribute_tbl_data = array(
       'name' 			=>  $insertData['name'],
       'sort_order' 	=>	$insertData['sort_order'],
	   'status' 		=>	$insertData['status'],
	   );
	 $this->db->where('id',$insertData['attr_id']);
	 $this->db->update('attribute_master',$attribute_tbl_data);
	 
	  return 'success';
	 }
	 
	 
	 
	 function deleteAttribute($id)
	 {
	  $data = array('status' => '4');  
	   
	 $this->db->where('id',$id);
	 $this->db->update('attribute_master',$data);
	 return 'success';
	 }
	
	 
}
	
	
	
	
	
	
	
	
	
	
	
	
	
?>