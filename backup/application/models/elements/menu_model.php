<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model 
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 
  <Reverse bidding system> 
    Copyright (C) <2009>  <FreeLance PHP Script>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
    If you want more information, please email me at support@freelancephpscript.com or 
    contact us from http://www.freelancephpscript.com/contact 
 
 
 */
	 class Menu_model extends CI_Model {
	 
 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
	

	
	function getMenus($conditions1)
     { 
	   
	   $this->db->where($conditions1);
	   $this->db->from('menu_master');
	   $this->db->join('status_master', 'status_master.id = menu_master.status','left');
	   $this->db->join('menu_type_master', 'menu_type_master.id = menu_master.type','left');
	   $this->db->select('menu_master.id,menu_master.name,menu_master.alias,menu_master.type,menu_master.details,menu_master.sort_order,menu_master.status,status_master.status as status_name,menu_type_master.type as type_name');
       $result = $this->db->get()->result();
	   
       return $result; 
    }
	
	
	function getMenuByAlias($conditions)
     { 
	   
	   $this->db->where($conditions);
	   $this->db->from('menu_master');
	   $this->db->join('status_master', 'status_master.id = menu_master.status','left');
	   $this->db->join('menu_type_master', 'menu_type_master.id = menu_master.type','left');
	   $this->db->select('menu_master.id,menu_master.name,menu_master.alias,menu_master.type,menu_master.details,menu_master.sort_order,menu_master.status,status_master.status as status_name,menu_type_master.type as type_name');
       $result = $this->db->get()->row();
	   
       return $result; 
    }
	
	
	
	
	function getActiveCountries()
     { $conditions = array('status'=> '1');
	   $this->db->where($conditions);
	   $this->db->select('*');
	   $this->db->from('infra_country_master');
       $result = $this->db->get()->result();
       return $result; 
    }
	
	 
	  function getCountry($id)
	 {
	 
	 	$conditions = array('id'=> $id);
	 	$this->db->where($conditions);
	 	$this->db->select('*');
		$query = $this->db->get('infra_country_master');		
		$row   = $query->row();
		return $row;
	 }
	 
	 
	 function getCountryByid($conditions)
	 {
	 
	 	
	 	$this->db->where($conditions);
	 	$this->db->from('country_master');
	   $this->db->join('status_master', 'status_master.id = country_master.status','left');
	   $this->db->select('country_master.id,country_master.name,country_master.code,country_master.status,status_master.status as status_name');
	   
	   $result = $this->db->get()->row();
       return $result; 
	 }
	 
	 function insertCountry($insertData=array())
	 {
	 $this->db->insert('country_master', $insertData);
	 return 'success';
	 }
	 
	 function editCountry($insertData=array())
	 {
	 $this->db->where('id',$insertData['id']);
	 $this->db->update('country_master',$insertData);
	 
	 
	 
	 return 'success';
	 }
	 
	 function deleteCountry($id)
	 {
	 $this->db->delete('country_master', array('id' => $id)); 
	 	  
	 return 'success';
	 }
	 
	 
	 
}
	
	
	
	
	
	
	
	
	
	
	
	
	
?>