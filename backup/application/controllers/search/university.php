<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class University extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
		$this->load->model('location/country_model');
		$this->load->model('course/degree_model');
		
    }

	
	function index()
	{   //die('++++');
	$this->load->model('user/auth_model');
  /*$this->auth_model->clearUserSession();
 
   $this->session->unset_userdata('leaduser');*/
   
  
   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('course/course_model');
		 $this->load->model('user/user_model');
		 $usrdata=$this->session->userdata('user');
		 //print_r($usrdata); exit;
	    // if(empty($usrdata)){  redirect('user/account'); }
		 $leaddata=$this->session->userdata('leaduser');
		 //Set searched key
		 $this->outputData['selected_country'] = '';
		 $this->outputData['selected_mainstream'] = '';
		 $this->outputData['selected_subcourse'] = '';
		 $this->outputData['related_subcourses'] ='';
		 $this->outputData['selected_degrees'] = '';
	    // if(empty($leaddata)){  redirect('user/account'); }
		 // echo $leaddata['countries']; exit;
		 
		 //echo $countryList = $leaddata['countries']; exit;
		 if($this->input->post('search')){
		 $courses = array('0'=>$this->input->post('subcourse'));
		 $degree = $this->input->post('degree'); 
		 $country_array = array_map("strtoupper", array('0'=>$this->input->post('country')));  //array('0'=>$this->input->post('country'));
		 
		 
		 
		 
		 
		 $sessiondata = array(
		 'mainstream'   => $this->input->post('mainstream'),
		 'subcourse'	=> $this->input->post('subcourse'),
		 'degree'		=> $this->input->post('degree'),
		 'country' 		=> $this->input->post('country')
		 
		 );
		 		 
		 $this->session->set_userdata('uni_search_criteria', $sessiondata);
		 
		 $sessiondata2=$this->session->userdata('leaduser');
		 $sessiondata2 = array(
		 'mainstream'   => $this->input->post('mainstream'),
		 'courses' 		=> $this->input->post('subcourse'),
		 'degree' 		=> $this->input->post('degree'),
		 'countries'    => $this->input->post('country')
		 );
	 
	 
     	$this->session->set_userdata('leaduser', $sessiondata2);
		
		
		//Set searched key
		 $this->outputData['selected_country'] = $this->input->post('country');
		 $this->outputData['selected_mainstream'] = $this->input->post('mainstream');
		 $this->outputData['selected_subcourse'] = $this->input->post('subcourse');
		 $this->outputData['related_subcourses'] = $this->course_model->getSubCourses(array('course_master.status' => '1','course_master.type' => '2','course_to_subCourse.course_id' => $this->input->post('mainstream')));
		 
		 $this->outputData['selected_degrees'] = $this->input->post('degree');
		 
		
		/* $leaddata225=$this->session->userdata('leaduser');
		 echo '<pre>'; 
		print_r($leaddata225);
		echo '</pre>'; exit;*/
		 
		 }else{
		 
		 $course_string = $leaddata['courses'];  
		 $degree = $leaddata['degree']; 
		 $courses = explode(",",$course_string);
		 $country_array = array_map("strtoupper", explode(",",$leaddata['countries']));		 
		 
		 }
		 
		 
		 
		  /*echo '<pre>';
		 print_r($courses); 
		  echo '</pre>';*/
		  
		 $result[] = array();
		 /*$cond1 = array('university_to_degree_to_course.course_id' => '9', 'university_to_degree_to_course.degree_id' => '1');
		 $result2 = $this->course_model->getuniversity_to_degree_to_course($cond1);
		  echo '<pre>';
		 print_r($result2); 
		  echo '</pre>';*/
		  
		 //array_push($result,$this->course_model->getuniversity_to_degree_to_course($cond1));
		  /*foreach($courses as $substream){
		  echo $substream;
		  }*/
		if($this->input->post('search')){
		 $count = 0;
		 
		 foreach($courses as $substream){
		 
		 $cond = array('university_to_degree_to_course.course_id' => $substream, 'university_to_degree_to_course.degree_id' => $degree);
		 $details = $this->course_model->getuniversity_to_degree_to_course($cond);
		//print_r($details); 
			//exit;
		  
		  if($details){
			 foreach($details as $detail){
			 
			$uniinfo = $this->user_model->getUniversityByid(array('user_master.id'=>$detail->university_id));
			
			//print_r($uniinfo); 
			//exit;
			
			
			//$sample_array = array("mark","steve","adam");

            
			
			//print_r($country_array);
			//exit;
			
			if (in_array($uniinfo->code, $country_array)){
			
			 $result[$count]['id'] = $detail->university_id;
			 $result[$count]['university'] = $detail->name;
			 
			 $comparesession = $this->session->userdata('university');
			 if($this->session->userdata('university')){
			 foreach($comparesession as $compare){
			    if($compare['university_id']==$detail->university_id){
			    $result[$count]['details_link'] = '';
			    }else{
			    $result[$count]['details_link'] = base_url().'university/details/index/'.$detail->university_id;
			   
			    }
			  }
			 }else{
			 $result[$count]['details_link'] = base_url().'university/details/index/'.$detail->university_id;
			 
			 }
			 
			  $query = $this->user_model->checkFavourites(array('favourites.user_id'=>$usrdata['id'],'favourites.university_id'=>$detail->university_id,'favourites.degree'=>$leaddata['degree'],'favourites.course'=>$leaddata['mainstream']));
			  
			 if(count($query)<1){
			 $result[$count]['favourites_link'] = base_url().'user/favourites/add/'.$detail->university_id;
			 }else{			 
			 $result[$count]['favourites_link'] = '#';
			 }
			 $result[$count]['details_link'] = base_url().'university/details/index/'.$detail->university_id;
			 $result[$count]['compare_link'] = base_url().'university/compare/index/'.$detail->university_id;
			 
			 $cond2 = array('university_to_degree_to_course.university_id' => $detail->university_id, 'university_to_degree_to_course.degree_id' => $degree);
		     $universityCourses = $this->course_model->getCoursesRelatedToUniversityDegree($cond2);
			 $courseinfo = array();
			 $j = 0;
			 foreach($courses as $crs){
			 
				 foreach($universityCourses as $unicourse){
				 if($unicourse->id==$crs){
				 
				 $courseinfo[$j]['id'] = $unicourse->id;
				 $courseinfo[$j]['course_name'] = $unicourse->name;
				 $j++;
				 }
				 }
			 
			 
			 }
			 $result[$count]['course']= $courseinfo;
			 unset($courseinfo);
			 
			 
			 /* echo '<pre>';
		 print_r($universityCourses); 
		  echo '</pre>'; exit;*/
			 
			// $result[$count]['course'] = $substream;
			 
			 $count++;
			 
		      } //if country
			
			}
			
			} 
			 
		 
		 
		 }
		}
		/* echo '<pre>';
		 print_r($result); 
		  echo '</pre>'; 
		  
		  echo count($result);*/
		  
		 if($result[0]){
		  
		  $duplicate=array();

			foreach ($result as $index=>$t) {
				if (isset($duplicate[$t["university"]])) {
					unset($result[$index]);
					continue;
				}
				$duplicate[$t["university"]]=true;
			}
			
		}	//print_r($result);
		  
		   /*echo '<pre>';
		 print_r($result); 
		  echo '</pre>'; exit;*/
		 
		 //$k = 0;
		 
		 $this->outputData['uni'] = array();
		 
		 $this->outputData['uni'] = $result;
		 
		 /*
		  header("Content-type:application/json"); 
         echo json_encode($this->outputData['uni']); exit;*/
		 
		 
		// foreach(array_unique($result) as $key){
		 
		// $cond = array('user_master.id' => $key, 'user_master.status' => 1,'user_master.type'=>3);
		// $unidetails = $this->user_model->getUniversityByid($cond);
		 
		 //print_r($unidetails); exit;
		// $this->outputData['uni'][$k]['name'] = $unidetails->name;
		// $this->outputData['uni'][$k]['name'] = $unidetails->;
		 
		// }
		 
		 
		 
		 /*echo '<pre>';
		 print_r(array_unique($result)); 
		  echo '</pre>';
		  exit;*/
		
		/*$cond_course1 = array('course_master.status !=' => '5', 'course_master.type' => '1'); 
		//$cond_user2 = array('course_master.type !=' => '1'); 			
        $this->outputData['courses'] = $this->course_model->getCourse($cond_course1);
		$this->outputData['course_types'] = $this->course_model->getAllCourseTypes();
		//$this->outputData['view'] = 'list';
		
		
		   
	     $this->outputData['business_management'] = $this->course_model->getSubCourses(array('course_to_subCourse.course_id' => 1 ,'course_master.status' => 1));
		 $this->outputData['art'] = $this->course_model->getSubCourses(array('course_id' => 2 ,'course_master.status' => 1));
		 $this->outputData['achitecture'] = $this->course_model->getSubCourses(array('course_id' => 3,'course_master.status' => 1));
		 $this->outputData['engineering'] = $this->course_model->getSubCourses(array('course_id' => 4,'course_master.status' => 1));
		 $this->outputData['medicnie'] = $this->course_model->getSubCourses(array('course_id' => 5,'course_master.status' => 1));
		 $this->outputData['humanities'] = $this->course_model->getSubCourses(array('course_id' => 6,'course_master.status' => 1)); 
		 $this->outputData['hospitality'] = $this->course_model->getSubCourses(array('course_id' => 7,'course_master.status' => 1));
		 $this->outputData['science'] = $this->course_model->getSubCourses(array('course_id' => 8,'course_master.status' => 1));
		 
	 
		//exit;
		*/
			
		//$this->load->view('templates/user/body',$this->outputData);
		
		$this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));
		$this->outputData['mainstreams'] = $this->course_model->getCourse(array('course_master.status'=>1,'course_master.type'=>1));
		$this->outputData['degrees'] = $this->degree_model->getDegree(array('degree_master.status'=>1));
        $this->render_page('templates/search/universitylist',$this->outputData);
	
    } 
	
	
	function search()
	{   
	
	//echo $this->input->get('course'); die('++++');
	$this->load->model('user/auth_model');
	
  /*$this->auth_model->clearUserSession();
 
   $this->session->unset_userdata('leaduser');*/
   
  
   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('course/course_model');
		 $this->load->model('user/user_model');
		// $leaddata=$this->session->userdata('leaduser');
	     //if(empty($userdata)){  redirect('common/login'); }
		 // echo $leaddata['countries']; exit;
		 
		 //echo $countryList = $leaddata['countries']; exit;
		// if($this->input->get('search')){
		/* $courses = array('0'=>$this->input->get('subcourse'));
		 $degree = $this->input->get('degree'); 
		 $country_array = array_map("strtoupper", array('0'=>$this->input->get('country')));  //array('0'=>$this->input->post('country'));*/
		 
		 $course_string = $this->input->get('subcourse');  
		 $degree = $this->input->get('degree'); 
		 $courses = explode(",",$course_string);
		 $country_array = array_map("strtoupper", explode(",",$this->input->get('country')));
		 
		 /*}else{
		 
		 $course_string = $leaddata['courses'];  
		 $degree = $leaddata['degree']; 
		 $courses = explode(",",$course_string);
		 $country_array = array_map("strtoupper", explode(",",$leaddata['countries']));
		 }*/
		 
		 
		 
		  /*echo '<pre>';
		 print_r($courses); 
		  echo '</pre>';*/
		  
		 $result[] = array();
		 /*$cond1 = array('university_to_degree_to_course.course_id' => '9', 'university_to_degree_to_course.degree_id' => '1');
		 $result2 = $this->course_model->getuniversity_to_degree_to_course($cond1);
		  echo '<pre>';
		 print_r($result2); 
		  echo '</pre>';*/
		  
		 //array_push($result,$this->course_model->getuniversity_to_degree_to_course($cond1));
		  /*foreach($courses as $substream){
		  echo $substream;
		  }*/
		 $count = 0;
		 foreach($courses as $substream){
		 
		 $cond = array('university_to_degree_to_course.course_id' => $substream, 'university_to_degree_to_course.degree_id' => $degree);
		 $details = $this->course_model->getuniversity_to_degree_to_course($cond);
		//print_r($details);
		  
		  if($details){
			 foreach($details as $detail){
			 
			$uniinfo = $this->user_model->getUniversityByid(array('user_master.id'=>$detail->university_id));
			
			//print_r($uniinfo); 
			
			
			//$sample_array = array("mark","steve","adam");

            
			
			//print_r($country_array);
			//exit;
			
			if (in_array($uniinfo->code, $country_array)){
			
			 $result[$count]['id'] = $detail->university_id;
			 $result[$count]['university'] = $detail->name;
			 
			 $cond2 = array('university_to_degree_to_course.university_id' => $detail->university_id, 'university_to_degree_to_course.degree_id' => $degree);
		     $universityCourses = $this->course_model->getCoursesRelatedToUniversityDegree($cond2);
			 $courseinfo = array();
			 $j = 0;
			 foreach($courses as $crs){
			 
				 foreach($universityCourses as $unicourse){
				 if($unicourse->id==$crs){
				 
				 $courseinfo[$j]['id'] = $unicourse->id;
				 $courseinfo[$j]['course_name'] = $unicourse->name;
				 $j++;
				 }
				 }
			 
			 
			 }
			 $result[$count]['course']= $courseinfo;
			 unset($courseinfo);
			 
			 
			 /* echo '<pre>';
		 print_r($universityCourses); 
		  echo '</pre>'; exit;*/
			 
			// $result[$count]['course'] = $substream;
			 
			 $count++;
			 
		      } //if country
			
			}
			
			} 
			 
		 
		 
		 }
		/* echo '<pre>';
		 print_r($result); 
		  echo '</pre>'; 
		  
		  echo count($result);*/
		  
		 if($result[0]){
		  
		  $duplicate=array();

			foreach ($result as $index=>$t) {
				if (isset($duplicate[$t["university"]])) {
					unset($result[$index]);
					continue;
				}
				$duplicate[$t["university"]]=true;
			}
			
		}	//print_r($result);
		  
		  /* echo '<pre>';
		 print_r($result); 
		  echo '</pre>';*/ //exit;
		 
		 //$k = 0;
		 $this->outputData['uni'] = array();
		 
		 $this->outputData['uni'] = $result;
		 $data = array(        	
			'name' 			=>  $this->input->get('name'),
			'email' 		=>  $this->input->get('email'),
			'phone' 		=>  $this->input->get('phone')        				
		);
	 
	
	 if($data)
	 {$leadId = $this->user_model->insertLead($data); 
	 }
		 header("Content-type:application/json"); 
         echo json_encode($this->outputData['uni']);
		 
		 
		// foreach(array_unique($result) as $key){
		 
		// $cond = array('user_master.id' => $key, 'user_master.status' => 1,'user_master.type'=>3);
		// $unidetails = $this->user_model->getUniversityByid($cond);
		 
		 //print_r($unidetails); exit;
		// $this->outputData['uni'][$k]['name'] = $unidetails->name;
		// $this->outputData['uni'][$k]['name'] = $unidetails->;
		 
		// }
		 
		 
		 
		 /*echo '<pre>';
		 print_r(array_unique($result)); 
		  echo '</pre>';
		  exit;*/
		
		/*$cond_course1 = array('course_master.status !=' => '5', 'course_master.type' => '1'); 
		//$cond_user2 = array('course_master.type !=' => '1'); 			
        $this->outputData['courses'] = $this->course_model->getCourse($cond_course1);
		$this->outputData['course_types'] = $this->course_model->getAllCourseTypes();
		//$this->outputData['view'] = 'list';
		
		
		   
	     $this->outputData['business_management'] = $this->course_model->getSubCourses(array('course_to_subCourse.course_id' => 1 ,'course_master.status' => 1));
		 $this->outputData['art'] = $this->course_model->getSubCourses(array('course_id' => 2 ,'course_master.status' => 1));
		 $this->outputData['achitecture'] = $this->course_model->getSubCourses(array('course_id' => 3,'course_master.status' => 1));
		 $this->outputData['engineering'] = $this->course_model->getSubCourses(array('course_id' => 4,'course_master.status' => 1));
		 $this->outputData['medicnie'] = $this->course_model->getSubCourses(array('course_id' => 5,'course_master.status' => 1));
		 $this->outputData['humanities'] = $this->course_model->getSubCourses(array('course_id' => 6,'course_master.status' => 1)); 
		 $this->outputData['hospitality'] = $this->course_model->getSubCourses(array('course_id' => 7,'course_master.status' => 1));
		 $this->outputData['science'] = $this->course_model->getSubCourses(array('course_id' => 8,'course_master.status' => 1));
		 
	 
		//exit;
		*/
			
		//$this->load->view('templates/user/body',$this->outputData);
		
		//$this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));
		//$this->outputData['mainstreams'] = $this->course_model->getCourse(array('course_master.status'=>1,'course_master.type'=>1));
		//$this->outputData['degrees'] = $this->degree_model->getDegree(array('degree_master.status'=>1));
       // $this->render_page('templates/search/universitylist',$this->outputData);
	
    } 
	
	function test()
	{   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('course/course_model');
		// $userdata=$this->session->userdata('user');
	     //if(empty($userdata)){  redirect('common/login'); }
		
		$cond_user1 = array('course_master.status !=' => '5'); 
		//$cond_user2 = array('course_master.type !=' => '1'); 			
        $this->outputData['courses'] = $this->course_model->getCourse($cond_user1);
		$this->outputData['course_types'] = $this->course_model->getAllCourseTypes();
		//$this->outputData['view'] = 'list';
		
			
		$this->load->view('templates/course/dynamic_pop',$this->outputData);
       // $this->render_page('templates/course/course',$this->outputData);
	
    } 
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('course/course_model');

		 $userdata=$this->session->userdata('user');
		 //print_r($userdata); exit;
	     if(empty($userdata)){  redirect('common/login'); }		
		 
		 $this->outputData['course_types'] = $this->course_model->getAllCourseTypes();
				 
		 if($this->uri->segment('4')){
		 $conditions = array('course_master.id' => $this->uri->segment('4'));
		 $course_details = $this->course_model->getCourseByid($conditions);
		 //print_r($course_details); exit;
					
		$this->outputData['id'] 			= $course_details->id;
		$this->outputData['name'] 			= $course_details->name;
		$this->outputData['details'] 		= $course_details->details;
		$this->outputData['type'] 			= $course_details->type;
		$this->outputData['parent'] 		= $course_details->course_id;
		$this->outputData['status'] 		= $course_details->status;
		
		if($this->outputData['parent']){
		
		$cond1 = array('course_master.status' => '1','course_master.type' => '1'); 
		$this->outputData['parent_courses'] = $this->course_model->getCourse($cond1);	
        // print_r($this->outputData['parent_courses']); exit;
		}
			
		}
		  
				 
		//$this->outputData['cities']	= $this->city_model->getCities();
        $this->render_page('templates/course/course_form',$this->outputData);
	
     } 
	 
	 function create()
	 {   
	   //die('+++++');
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('user/user_model');
	   $this->load->model('course/course_model');
	   
	   $userdata=$this->session->userdata('user');
	   
      	$data = array(
        	'name' 			=>  $this->input->post('coursename'),
        	'details' 		=>  $this->input->post('course_details'),
        	'type' 			=>  $this->input->post('coursetype'),
			'parent' 		=>  $this->input->post('parent_course'),
			'createdate' 	=>  date('Y-m-d'),
			'createdby' 	=>	$userdata['id'],
			'status' 		=>	$this->input->post('status')
			
		);
		//print_r($data); exit;	 
	     if($this->course_model->insertCourse($data)=='success')
		 {
		 	$this->session->set_flashdata('flash_message', "Success: You have saved Course!");
	     	redirect('course/course');
	     }
	}
	 
	 
	 function edit()
	 {   
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('user/user_model');
	   $this->load->model('course/course_model');
       
	  $userdata=$this->session->userdata('user');
	   
      	$data = array(
        	'id'            =>  $this->uri->segment('4'),
			'name' 			=>  $this->input->post('coursename'),
        	'details' 		=>  $this->input->post('course_details'),
        	'type' 			=>  $this->input->post('coursetype'),
			'parent' 		=>  $this->input->post('parent_course'),
			'modifiedby' 	=>	$userdata['id'],
			'status' 		=>	$this->input->post('status')
			
		);
		 
		 if($this->course_model->editCourse($data)=='success'){
		 		 	 
	     $this->session->set_flashdata('flash_message', "You have modified Course!");
	      redirect('course/course');
	     }
	 
	   
	}
	
	
	function delete()
	 {   
	   
	   $this->load->helper(array('form', 'url'));
	   $this->load->model('course/course_model');
		
		if($this->course_model->deleteCourse($this->uri->segment('4'))=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have deleted Course!");
	         redirect('course/course');
	    }
	}
	
	
	function multidelete(){
	
	    $this->load->helper(array('form', 'url'));

		$this->load->model('course/course_model');
		
		$array = $this->input->post('chk');
		foreach($array as $id):
		
		$this->course_model->deleteCourse($id);
		
		endforeach;
		$this->session->set_flashdata('flash_message', "You have deleted Course!");
		 redirect('course/course');
	
	
	}
	
	function getAllParentCourses()
	 {   
	  
	   $this->load->helper(array('form', 'url'));
	   $this->load->model('course/course_model');
		
	   if($this->input->post('typeid')==2){
		$cond1 = array('course_master.status' => '1','course_master.type' => '1','course_master.id !=' => $this->input->post('id') ); 
			
       // print_r($this->course_model->getCourse($cond1)); //exit;
		foreach($this->course_model->getCourse($cond1) as $course){
		echo '<option value="'.$course->id.'">'.$course->name.'</option>';
	    }
	 
	 }
	// print_r($this->course_model->getCourse($cond1));
	         
	   
	}
	
	
	function getCourseInfo()
	 {   
	  
	   $this->load->helper(array('form', 'url'));
	   $this->load->model('course/course_model');
		$coursedata = array();
	  $coursedata = json_decode(stripslashes($this->input->post('data')));
	  
	  //print_r($coursedata);
	   
	$this->load->view('templates/user/body',$this->outputData);
	// print_r($this->course_model->getCourse($cond1));
	         
	   
	}
	
	function getSubcoursesList()
	{ 
	  $this->load->model('course/course_model');
	  //$this->outputData['test'] = json_decode(stripslashes($this->input->post('email'))); 
	  $condition = array('course_master.status' => '1','course_master.type' => '2','course_to_subCourse.course_id' => $this->input->post('id'));
	  $subcourses = $this->course_model->getSubCourses($condition);
	  
	  // print_r($subcourses); 
	   
	  foreach($subcourses as $course){
	  echo "<option value='".$course->idd."'> ".strtoupper($course->name)."</option>";
	  }
	 
	  //$this->load->view('templates/course/dynamic_pop',$this->outputData);
} 
	 
}//End  Home Class

?>