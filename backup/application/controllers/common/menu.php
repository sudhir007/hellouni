<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Menu extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
		$this->load->model('location/country_model');
		$this->load->model('course/degree_model');
		$this->load->model('elements/menu_model');
    }

	
	function index()
	{   //die($this->uri->segment('1'));
	
	
	    if($this->menu_model->getMenuByAlias(array('menu_master.alias'=>$this->uri->segment('1')))){
	      $details = $this->menu_model->getMenuByAlias(array('menu_master.alias'=>$this->uri->segment('1')));
		  
		  $this->outputData['name'] 	= $details->name;
		  $this->outputData['detail'] 	= $details->details;
		  
          $this->render_page('templates/common/menu_related_page',$this->outputData);
		  
	    }else{
		
		
		
		}
	
	// $this->load->model('user/auth_model');
		
		
	
    } 
	
	function test()
	{   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('course/course_model');
		// $userdata=$this->session->userdata('user');
	     //if(empty($userdata)){  redirect('common/login'); }
		
		$cond_user1 = array('course_master.status !=' => '5'); 
		//$cond_user2 = array('course_master.type !=' => '1'); 			
        $this->outputData['courses'] = $this->course_model->getCourse($cond_user1);
		$this->outputData['course_types'] = $this->course_model->getAllCourseTypes();
		//$this->outputData['view'] = 'list';
		
			
		$this->load->view('templates/course/dynamic_pop',$this->outputData);
       // $this->render_page('templates/course/course',$this->outputData);
	
    } 
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('course/course_model');

		 $userdata=$this->session->userdata('user');
		 //print_r($userdata); exit;
	     if(empty($userdata)){  redirect('common/login'); }		
		 
		 $this->outputData['course_types'] = $this->course_model->getAllCourseTypes();
				 
		 if($this->uri->segment('4')){
		 $conditions = array('course_master.id' => $this->uri->segment('4'));
		 $course_details = $this->course_model->getCourseByid($conditions);
		 //print_r($course_details); exit;
					
		$this->outputData['id'] 			= $course_details->id;
		$this->outputData['name'] 			= $course_details->name;
		$this->outputData['details'] 		= $course_details->details;
		$this->outputData['type'] 			= $course_details->type;
		$this->outputData['parent'] 		= $course_details->course_id;
		$this->outputData['status'] 		= $course_details->status;
		
		if($this->outputData['parent']){
		
		$cond1 = array('course_master.status' => '1','course_master.type' => '1'); 
		$this->outputData['parent_courses'] = $this->course_model->getCourse($cond1);	
        // print_r($this->outputData['parent_courses']); exit;
		}
			
		}
		  
				 
		//$this->outputData['cities']	= $this->city_model->getCities();
        $this->render_page('templates/course/course_form',$this->outputData);
	
     } 
	 
	 function create()
	 {   
	   //die('+++++');
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('user/user_model');
	   $this->load->model('course/course_model');
	   
	   $userdata=$this->session->userdata('user');
	   
      	$data = array(
        	'name' 			=>  $this->input->post('coursename'),
        	'details' 		=>  $this->input->post('course_details'),
        	'type' 			=>  $this->input->post('coursetype'),
			'parent' 		=>  $this->input->post('parent_course'),
			'createdate' 	=>  date('Y-m-d'),
			'createdby' 	=>	$userdata['id'],
			'status' 		=>	$this->input->post('status')
			
		);
		//print_r($data); exit;	 
	     if($this->course_model->insertCourse($data)=='success')
		 {
		 	$this->session->set_flashdata('flash_message', "Success: You have saved Course!");
	     	redirect('course/course');
	     }
	}
	 
	 
	 function edit()
	 {   
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('user/user_model');
	   $this->load->model('course/course_model');
       
	  $userdata=$this->session->userdata('user');
	   
      	$data = array(
        	'id'            =>  $this->uri->segment('4'),
			'name' 			=>  $this->input->post('coursename'),
        	'details' 		=>  $this->input->post('course_details'),
        	'type' 			=>  $this->input->post('coursetype'),
			'parent' 		=>  $this->input->post('parent_course'),
			'modifiedby' 	=>	$userdata['id'],
			'status' 		=>	$this->input->post('status')
			
		);
		 
		 if($this->course_model->editCourse($data)=='success'){
		 		 	 
	     $this->session->set_flashdata('flash_message', "You have modified Course!");
	      redirect('course/course');
	     }
	 
	   
	}
	
	
	function delete()
	 {   
	   
	   $this->load->helper(array('form', 'url'));
	   $this->load->model('course/course_model');
		
		if($this->course_model->deleteCourse($this->uri->segment('4'))=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have deleted Course!");
	         redirect('course/course');
	    }
	}
	
	
	function multidelete(){
	
	    $this->load->helper(array('form', 'url'));

		$this->load->model('course/course_model');
		
		$array = $this->input->post('chk');
		foreach($array as $id):
		
		$this->course_model->deleteCourse($id);
		
		endforeach;
		$this->session->set_flashdata('flash_message', "You have deleted Course!");
		 redirect('course/course');
	
	
	}
	
	function getAllParentCourses()
	 {   
	  
	   $this->load->helper(array('form', 'url'));
	   $this->load->model('course/course_model');
		
	   if($this->input->post('typeid')==2){
		$cond1 = array('course_master.status' => '1','course_master.type' => '1','course_master.id !=' => $this->input->post('id') ); 
			
       // print_r($this->course_model->getCourse($cond1)); //exit;
		foreach($this->course_model->getCourse($cond1) as $course){
		echo '<option value="'.$course->id.'">'.$course->name.'</option>';
	    }
	 
	 }
	// print_r($this->course_model->getCourse($cond1));
	         
	   
	}
	
	
	function getCourseInfo()
	 {   
	  
	   $this->load->helper(array('form', 'url'));
	   $this->load->model('course/course_model');
		$coursedata = array();
	  $coursedata = json_decode(stripslashes($this->input->post('data')));
	  
	  //print_r($coursedata);
	   
	$this->load->view('templates/user/body',$this->outputData);
	// print_r($this->course_model->getCourse($cond1));
	         
	   
	}
	
	function getSubcoursesList()
	{ 
	  $this->load->model('course/course_model');
	  //$this->outputData['test'] = json_decode(stripslashes($this->input->post('email'))); 
	  $condition = array('course_master.status' => '1','course_master.type' => '2','course_to_subCourse.course_id' => $this->input->post('id'));
	  $subcourses = $this->course_model->getSubCourses($condition);
	  
	  // print_r($subcourses); 
	   
	  foreach($subcourses as $course){
	  echo "<option value='".$course->idd."'> ".strtoupper($course->name)."</option>";
	  }
	 
	  //$this->load->view('templates/course/dynamic_pop',$this->outputData);
} 
	 
}//End  Home Class

?>