<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Amenity extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

	
	function index()
	{   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('website-element/amenity_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }	
		 
		$cond_amenity1 = array('amenities_master.status' => '1'); 
		$cond_amenity2 = array('amenities_master.status' => '2'); 		
        $this->outputData['amenities'] = $this->amenity_model->getAmenities($cond_amenity1,$cond_amenity2);
        $this->render_page('templates/website-element/amenity',$this->outputData);
	
} 
	
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('website-element/amenity_model');
		 
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }		
		 
		 if($this->uri->segment('4')){
		 $conditions = array(
	 				  'amenities_master.id' => $this->uri->segment('4')
	 					);
		 $amenity_info = $this->amenity_model->getAmenityByid($conditions);
		
		foreach($amenity_info as $amenity_in){
		$this->outputData['id'] 			= $amenity_in->id;
		$this->outputData['name'] 			= $amenity_in->name;
		$this->outputData['description'] 	= $amenity_in->description;
		$this->outputData['sort_order'] 	= $amenity_in->sort_order;
		$this->outputData['type_id'] 	= $amenity_in->type_id;
		$this->outputData['for'] 	= $amenity_in->for;
		}
		  
		  }
		 $this->outputData['amenities']	= $this->amenity_model->getAmenitiesType();   
        $this->render_page('templates/website-element/amenity_form',$this->outputData);
	
     } 
	 
	 
	 
	 function create()
	 {   
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
       $this->load->model('website-element/amenity_model');
	   
	   $this->form_validation->set_rules('amenity_name', 'Amenity Name', 'required|trim|xss_clean');
	   
	   if ($this->form_validation->run() == FALSE)
		{
		
			  $this->render_page('templates/website-element/property_form');
		}
		else
		{
			
		 $data = array(
        	'name' => $this->input->post('amenity_name'),
        	'description' => $this->input->post('amenity_description'),
       		'sort_order' =>$this->input->post('amenity_sort_order'),
			'type_id' =>$this->input->post('amenity_type'),
			'for' =>$this->input->post('amenity_for')
	     	);
	 
	     if($this->amenity_model->insertAmenity($data)=='success'){
	 
	     $this->session->set_flashdata('flash_message', "Success: You have saved Amenity!");
	     redirect('website-element/amenity/index');
	     }
	 
	 
	   }
	}
	 
	 
	 function edit_amenity()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/amenity_model');
		$this->form_validation->set_rules('amenity_name', 'Amenityname', 'required|trim|xss_clean');
		$this->form_validation->set_rules('amenity_description', 'Amenity_Description', 'required|trim|xss_clean');
	

		if ($this->form_validation->run() == FALSE)
		{
			
			  $this->render_page('templates/website-element/amenity_form');
		}
		else
		{
			 
			 $data = array(
            	'amenity_id' => $this->uri->segment('4'),
				'name' => $this->input->post('amenity_name'),
            	'description' => $this->input->post('amenity_description'),
            	'sort_order' =>$this->input->post('amenity_sort_order'),
				'type_id' =>$this->input->post('amenity_type'),
		   		'for' =>$this->input->post('amenity_for')
	        	);
			
	         if($this->amenity_model->editAmenity($data)=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have modified Amenity!");
	         redirect('website-element/amenity/index');
	         }
	 
	 
	    }
	}
    
	
	function delete_amenity()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
       $this->load->model('website-element/amenity_model');
		
		
		if($this->amenity_model->deleteAmenity($this->uri->segment('4'))=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have deleted Amenity!");
	         redirect('website-element/amenity/index');
	     }
	}
	
	
	function delete_mult_amenity(){
	
	    $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/amenity_model');
		
		$array = $this->input->post('chk');
		foreach($array as $id):
		
		$this->amenity_model->deleteAmenity($id);
		
		endforeach;
		$this->session->set_flashdata('flash_message', "Success: You have deleted Amenities!");
		redirect('website-element/amenity/index');
	
	
	}
	 

}//End  Home Class


?>