<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Package extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

	
	function index()
	{   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('website-element/package_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }
		
		$cond_package1 = array('package_master.status' => '1'); 
		$cond_package2 = array('package_master.status' => '2'); 			
        $this->outputData['packages'] = $this->package_model->getPackages($cond_package1,$cond_package2);
        $this->render_page('templates/website-element/package',$this->outputData);
	
    } 
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('website-element/package_model');
		 $this->load->model('plugins/gallery_model');
		 $this->load->model('website-element/options_model'); 
		 $this->load->model('website-element/attribute_model');
		 $this->load->model('website-element/seo_model');
		 

		 $this->load->model('administrator/common_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }		
		 
		 if($this->uri->segment('4')){
		 $conditions = array('package_master.id' => $this->uri->segment('4'));
		 $package = $this->package_model->getPackageByid($conditions);
		
		
		$this->outputData['id'] 			= $package->id;
		$this->outputData['title'] 			= $package->name;
		$this->outputData['start'] 			= $this->common_model->changeDate("m/d/Y",$package->start_date);
		$this->outputData['end'] 			= $this->common_model->changeDate("m/d/Y",$package->end_date);
		$this->outputData['description'] 	= $package->description;
		$this->outputData['status'] 		= $package->status;
		$this->outputData['virtual_tour'] 	= $package->virtual_tour;
		
		
		// Seo details
		$seomaster_details = $this->seo_model->getSeoById($package->seo_id);
		
		
		$this->outputData['meta_tag'] 			=  $seomaster_details->meta_tag;
		$this->outputData['meta_description'] 	=  $seomaster_details->meta_description;
		
		// Photo gallery data
		$photo_gallery = array(
	 				 'package_id' => $package->id,
					 'type' 		=>'photo'
	 				);
		$this->outputData['selected_photo']			=	$this->gallery_model->getGallery_By_Package($photo_gallery);
		
		// Video gallery data
		$video_gallery = array(
	 				 'package_id' => $package->id,
					 'type' 		=>'video'
	 				);
		$this->outputData['selected_video']			=	$this->gallery_model->getGallery_By_Package($video_gallery);
		
		$this->outputData['package_attributes'] = $this->package_model->getAttributes_BY_Package($this->uri->segment('4'));
		
		//echo $this->uri->segment('4');
		//echo '<pre>';
		//print_r($this->outputData['package_attributes']);
		//echo '</pre>'; 
		$this->outputData['row']=count($this->outputData['package_attributes']);
		//exit();
		
		
		
		
		 }else{
		 
		 
		 $this->outputData['row']=0;
		 
		 
		 
		 
		 }
		  
		$cond_gallery = array('gallery_master.status' => '1'); 
		$this->outputData['photos']		=	$this->gallery_model->getPhotoGallaries($cond_gallery,$array=array());
		$this->outputData['videos']		=	$this->gallery_model->getVideoGallaries($cond_gallery,$array=array());
		$this->outputData['options'] = $this->options_model->getOptions();
		
		$cond_attr1 = array('attribute_master.status' => '1'); 
		$this->outputData['attributes'] = $this->attribute_model->getAttributes($cond_attr1,$array=array());
        $this->render_page('templates/website-element/package_form',$this->outputData);
	
     } 
	 
	 function create()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
       $this->load->model('website-element/package_model');
       $this->load->model('administrator/common_model');
	   $this->form_validation->set_rules('package_title', 'Title', 'required|trim|xss_clean');
	   $this->form_validation->set_rules('package_start', 'Start Date', 'required|trim|xss_clean');
	   $this->form_validation->set_rules('package_end', 'End Date', 'required|trim|xss_clean');
	   $this->form_validation->set_rules('package_description', 'Description', 'required|trim|xss_clean');
       
	  if ($this->form_validation->run() == FALSE)
		{
			$this->render_page('templates/website-element/package_form');
		}
		else
		{  
		
		
		 	$data = array(
        	'title' 		=>  $this->input->post('package_title'),
        	'start_date' 	=>  $this->common_model->changeDate("Y-m-d",$this->input->post('package_start')),
        	'end_date' 		=>  $this->common_model->changeDate("Y-m-d",$this->input->post('package_end')),
			'description' 	=>  $this->input->post('package_description'),
			'status' 		=>  $this->input->post('package_status'),
			'photo' 		=>	$this->input->post('photo_gallery'),
			'video' 		=>	$this->input->post('video_gallery'),
			'virtual_tour' 	=>	(string)$this->input->post('virtual_tour'),
			'attr' 			=>	$this->input->post('attribute'),
			'meta_tag'			=>  $this->input->post('package_meta_tag'),
			'meta_description'	=>  $this->input->post('package_meta_description') 
			);
			
			/*echo '<pre>';
			print_r($data['attr']);
			echo '</pre>'; 
			exit(); */
	 
	     if($this->package_model->insertPackage($data)=='success'){
		 $this->session->set_flashdata('flash_message', "Success: You have saved Package!");
	     redirect('website-element/package/index');
	     }
	   }
	}
	 
	 
	 function edit()
	 {   
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
       $this->load->model('website-element/package_model');
       $this->load->model('administrator/common_model');
	   $this->form_validation->set_rules('package_title', 'Title', 'required|trim|xss_clean');
	   $this->form_validation->set_rules('package_start', 'Start Date', 'required|trim|xss_clean');
	   $this->form_validation->set_rules('package_end', 'End Date', 'required|trim|xss_clean');
	   $this->form_validation->set_rules('package_description', 'Description', 'required|trim|xss_clean');

	   if ($this->form_validation->run() == FALSE)
		{
		
			  $this->render_page('templates/website-element/package_form');
		}
		else
		{
			 $data = array(
        		'title' 		=>	$this->input->post('package_title'),
        		'start_date' 	=>	$this->common_model->changeDate("Y-m-d",$this->input->post('package_start')),
        		'end_date' 		=>  $this->common_model->changeDate("Y-m-d",$this->input->post('package_end')),
				'description' 	=>	$this->input->post('package_description'),
				'status' 		=>	$this->input->post('package_status'),
				'photo' 		=>	$this->input->post('photo_gallery'),
				'video' 		=>	$this->input->post('video_gallery'),
				'virtual_tour' 	=>	(string)$this->input->post('virtual_tour'),
				'attr' 			=>	$this->input->post('attribute'),
				'meta_tag'			=>  $this->input->post('package_meta_tag'),
			    'meta_description'	=>  $this->input->post('package_meta_description') 
	     	 	);
		 
		 if($this->package_model->editPackage($this->uri->segment('4'),$data)=='success'){
	 
	     $this->session->set_flashdata('flash_message', "Success: You have modified Package!");
	     redirect('website-element/package/index');
	     }
	 
	   }
	}
	
	
	function delete()
	 {   
	   
	   $this->load->helper(array('form', 'url'));
	   $this->load->model('website-element/package_model');
		
		if($this->package_model->deletePackage($this->uri->segment('4'))=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have deleted Package!");
	         redirect('website-element/package/index');
	    }
	}
	
	
	function multidelete(){
	
	    $this->load->helper(array('form', 'url'));

		$this->load->model('website-element/package_model');
		
		$array = $this->input->post('chk');
		foreach($array as $id):
		
		$this->package_model->deletePackage($id);
		
		endforeach;
		$this->session->set_flashdata('flash_message', "Success: You have deleted Package!");
		redirect('website-element/package/index');
	
	
	}
	 
}//End  Home Class

?>