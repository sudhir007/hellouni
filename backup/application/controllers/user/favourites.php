<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Favourites extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()

    {

        parent::__construct();
        
		$this->load->model('user/user_model');
		$this->load->model('user/auth_model');

        

    }

	
	function index()

	{    $this->load->helper('cookie_helper'); 
         $this->load->model('user/auth_model');
		 
		 
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('user/account'); }
		
		 $this->outputData['favourites'] = $this->user_model->getFavourites(array('favourites.user_id'=>$userdata['id']));
		// print_r($this->outputData['favourites']); exit;
		 $this->render_page('templates/user/favourites',$this->outputData);

						//$this->render_page('templates/common/index');
    } 

  
	function add()
	 {   
	   //die('+++++');
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   
	   $this->load->model('course/course_model');
	   $this->load->model('user/auth_model');
       
	   $leaddata = $this->session->userdata('leaduser'); 
	   
	  // print_r($leaddata); exit;
	   
	   $userdata=$this->session->userdata('user');
	   
	   if(empty($userdata)){  redirect('user/account'); }
			
			
	   $query = $this->user_model->checkFavourites(array('favourites.user_id'=>$userdata['id'],'favourites.university_id'=>$this->uri->segment('4'),'favourites.degree'=>$leaddata['degree'],'favourites.course'=>$leaddata['mainstream']));
	  // print_r($query); exit;	 
	   if(count($query)<1){
			$data = array(
				'user_id' 		=>  $userdata['id'],
				'university_id' =>  $this->uri->segment('4'),
				'course' 		=>  $leaddata['mainstream'],
				'degree'		=>  $leaddata['degree'],			
				'createdate' 	=>  date('Y-m-d')
			);
			//print_r($data); exit;	 
			$this->user_model->insertFavourites($data);
		 
		  redirect('user/favourites');
		 }
	}
	
	
	function delete()
	
		{    $this->load->helper('cookie_helper'); 
			 $this->load->model('user/auth_model');
			 $userdata=$this->session->userdata('user');
	         if(empty($userdata)){  redirect('user/account'); }
			 
			 if($this->uri->segment('4')){
			 
			 if($this->user_model->deleteFavourites(array('favourites.id'=>$this->uri->segment('4'))))
			   $this->session->set_flashdata('flash_message', "Success: Favourites has been deleted!");
			 
			 }
			 
			 redirect('user/favourites');
			 
	
			
	
							//$this->render_page('templates/common/index');
	   } 
	   
	
	function update()
	
		{    
		$this->load->helper('cookie_helper'); 
		$this->load->model('user/auth_model');
		$userdata=$this->session->userdata('user');
	    if(!$userdata){  redirect('user/acount'); }
		//print_r($userdata); exit;
		$condition = array('user_master.id'=>$userdata['id'],'user_master.status'=>1,'user_master.type'=>5);
		$userDetails = $this->user_model->getStudentDetails($condition);
			
		$this->outputData['id']= $userDetails->id; 
	 	$this->outputData['name']= $userDetails->name;
		$this->outputData['email']= $userDetails->email;
		$this->outputData['typename']= $userDetails->typename;
		$this->outputData['address']= $userDetails->address;
		$this->outputData['country']= $userDetails->country;
		$this->outputData['city']= $userDetails->city;
		$this->outputData['zip']= $userDetails->zip;
		$this->outputData['dob']= $userDetails->dob;
		$this->outputData['phone']= $userDetails->phone;
		$this->outputData['fid']= $userDetails->fid;
		//print_r($userDetails); exit;
			
		$this->render_page('templates/user/update_profile',$this->outputData);
							//$this->render_page('templates/common/index');
	   } 
	   
	 function save()
	 {   
	   //die('+++++');
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   
	   $this->load->model('course/course_model');
	   $this->load->model('user/auth_model');
       
	   //$leaddata = $this->session->userdata('leaduser'); 
	   $userdata=$this->session->userdata('user');
	   
	   if(!$userdata['fid']){
	   
	   $details = array(
        	'name' 			=>  $this->input->post('name'),
        	'email' 		=>  $this->input->post('email'),
        	'password' 		=>  md5($this->input->post('password')),
					
		);	
		$this->user_model->updateUserInfo($details,$userdata['id'])	;
		
		}
		
		
		$data = array(
        	'address'		=>	$this->input->post('address'),
			'country'		=>	$this->input->post('country'),
			'city'			=>	$this->input->post('city'),
			'zip'			=>	$this->input->post('zip'),
			'dob'			=>	$this->input->post('dob'),
			'phone'			=>	$this->input->post('phone')
		);
		$this->user_model->updateStudentDetails($data,$userdata['id']);
		
		$from = 'sumanta@inventifweb.com';
		
			 if(!$userdata['fid']){
			  $email = $this->input->post('email');
			  $name = $this->input->post('name');
			 }else{
			  $email = $userdata['email'];
			  $name = $userdata['name'];
			 }
			//$to = $this->input->post('email');
			$subject = "Your Account has been Updated";
			$to = $userdata['email'];
			$message = "
			<head>
			<title>Please verify your email</title>
			</head>
			<body>
			<p>Hi ".$userdata['name'].",</p>
			<p>Your Account has been sucessfully created under HelloUni.</p>
			
			
			<p>URL - ".base_url()."</p>
			</body>
			";
			
			// Always set content-type when sending HTML email
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			
			// More headers
			$headers .= 'From:'.$from. "\r\n";
			mail($to,$subject,$message,$headers);		
			//if(mail($to,$subject,$message,$headers)){die('++');}else{die('--');}
		 	$this->session->set_flashdata('flash_message', "Success: Please verify!");
			
				
		if(!$userdata['fid']){
		redirect('user/account/logout');
		}else{
		//redirect('user/account');
		}
		//print_r($data); exit;	 
	    
			// Mail to user for verification
			
			 $this->session->set_flashdata('flash_message', "Your profile has been updated.");
	     	redirect('user/account/update');
	     
	}
	
	function login()

	{
     //Load Models - for this function
	 $this->load->model('user/user_model');
	 $this->load->model('user/auth_model');
	 $this->load->helper('cookie'); 
	
	 
     $email = $this->input->post('email');
	 $password = md5($this->input->post('password')); //exit;
	 $conditions 		=  array('user_master.email'=>$email,'user_master.password' => $password,'user_master.status' => 1);
	 $query				= $this->user_model->checkUser($conditions); 
		 
	 if(count($query)> 0){ 
	  
	  $data = array(
     'id' => $query->id,
	 'name'=> $query->name,
     'email' => $query->email,
     'type' => $query->type,
	 'typename' => $query->typename,
     'is_logged_in' => true
     );
     $this->session->set_userdata('user', $data);
	 
	/* if($this->input->post('remember'))
						{ 
						    $insertData=array();
						    $insertData['username']=$this->input->post('username');
						    $insertData['password']=$this->input->post('pwd');
						    $expire=60*60*24*100;
							if( $this->auth_model->getUserCookie('uname')=='')
							{ 
							$this->user_model->addRemerberme($insertData,$expire); 
							
							}		
						}
						else
						{
						   $this->user_model->removeRemeberme(); 
							
						}*/
							
							
	 redirect('search/university');
	 }else{
	 $this->session->set_flashdata('flash_message', "Sorry! Invalid Login.");
	
	 redirect('user/account');
	 }
} 



function fblogin()

	{
     //Load Models - for this function
	 $this->load->model('user/user_model');
	 $this->load->model('user/auth_model');
	 $this->load->helper('cookie'); 
	 $leaddata = $this->session->userdata('leaduser'); 
	 
	 $fid = $this->input->post('id'); //exit;
	 $name = $this->input->post('name');
	 $email = $this->input->post('email');
	 $image = $this->input->post('image');
	// $password = md5($this->input->post('password')); //exit;
	 $conditions 		=  array('user_master.email'=>$email,'user_master.status' => 1);
	 $query				= $this->user_model->checkUser($conditions); 
		 
	 if(count($query)> 0){ 
	  
	  $data = array(
        	'name' 			=>  $this->input->post('name'),
        	'email' 		=>  $this->input->post('email'),
        	'password' 		=>  '',
			'image'			=>  $this->input->post('image'),
			'type' 			=>  '5',
			'createdate' 	=>  date('Y-m-d'),
			'activation' 	=>	'',
			'fid' 			=>	$this->input->post('id'),
			'status' 		=>	'1'
			/*'address'		=>	'',
			'country'		=>	'',
			'city'			=>	'',
			'zip'			=>	'',
			'dob'			=>	'',
			'phone'			=>	''*/
		);
	  
	  if($this->user_model->updateUserInfo($data,$query->id)){
	  
	  $usrdata = array(
					 'id' => $query->id,
					 'name'=> $this->input->post('name'),
					 'email' => $this->input->post('email'),
					 'fid' => $this->input->post('id'),
					 'type' => 5,
					 'is_logged_in' => true
			      );
	   $this->session->set_userdata('user', $usrdata);
	  echo 1;
	  
	  }
	  
	  
	 
	/* if($this->input->post('remember'))
						{ 
						    $insertData=array();
						    $insertData['username']=$this->input->post('username');
						    $insertData['password']=$this->input->post('pwd');
						    $expire=60*60*24*100;
							if( $this->auth_model->getUserCookie('uname')=='')
							{ 
							$this->user_model->addRemerberme($insertData,$expire); 
							
							}		
						}
						else
						{
						   $this->user_model->removeRemeberme(); 
							
						}*/
							
							
	// redirect('search/university');
	 }else{
	 
	 
	 $data = array(
        	'name' 			=>  $this->input->post('name'),
        	'email' 		=>  $this->input->post('email'),
        	'password' 		=>  '',
			'image'			=>  $this->input->post('image'),
			'type' 			=>  '5',
			'createdate' 	=>  date('Y-m-d'),
			'activation' 	=>	'',
			'fid' 			=>	$this->input->post('id'),
			'status' 		=>	'1',
			'address'		=>	'',
			'country'		=>	'',
			'city'			=>	'',
			'zip'			=>	'',
			'dob'			=>	'',
			'phone'			=>	''
		);
		$userid = $this->user_model->insertStudent($data);
		if($userid)
		 {  
		     
			 if($leaddata['id']){
			 $leadMasterData =array(
			' name'			=> $this->input->post('name'),
			 'email'		=> $this->input->post('email'),
			 'phone' 		=> $this->input->post('phone'),
			 );
			 $this->user_model->updateLeadInfo($leadMasterData,$leaddata['id']);
			 
			 $sessiondata = array(
			 'id' 			=> $leaddata['id'],
			 'name'			=> $this->input->post('name'),
			 'email'		=> $this->input->post('email'),
			 'phone' 		=> $this->input->post('phone'),
			 'mainstream'   => $leaddata['mainstream'],
			 'courses' 		=> $leaddata['courses'],
			 'degree' 		=> $leaddata['degree'],
			 'countries'    => $leaddata['countries']
			 );
	        $this->auth_model->clearUserSession();
	        $this->session->unset_userdata('leaduser');	
			$this->session->set_userdata('leaduser', $sessiondata);
			}
			$usrdata = array(
					 'id' => $userid,
					 'name'=> $this->input->post('name'),
					 'email' => $this->input->post('email'),
					 'fid' => $this->input->post('id'),
					 'type' => 5,
					 'is_logged_in' => true
			);
			$this->session->set_userdata('user', $usrdata);
     		
			
			
			// Mail to user for verification
			
			$from = 'sumanta@inventifweb.com';
			$to = $this->input->post('email');
			$subject = "Your Account has been sucessfully created under HelloUni.";
			
			$message = "
			<head>
			<title>Your Account has been sucessfully created under HelloUni.</title>
			</head>
			<body>
			<p>Hi ".$this->input->post('name').",</p>
			<p>Your Account has been sucessfully created under HelloUni.</p>
			<br/><br/>
			<p>Your registered email credential is as follows -</p>
			<p>Email - ".$this->input->post('email')."</p>
			<p>URL - ".base_url()."</p>
			</body>
			";
			
			// Always set content-type when sending HTML email
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			
			// More headers
			$headers .= 'From:'.$from. "\r\n";
			mail($to,$subject,$message,$headers);		
			//if(mail($to,$subject,$message,$headers)){die('++');}else{die('--');}
		 	$this->session->set_flashdata('flash_message', "Success: Please verify!");
	     	//redirect('user/account/');
			
			echo 1;
	     }
		
		
     //$this->session->set_flashdata('flash_message', "Sorry! Invalid Login.");
	// redirect('user/account');
	  }
	 } 


function logout()
 { 
  $this->load->model('user/auth_model');
  $this->auth_model->clearUserSession();
  //$this->session->set_flashdata('flash_message', $this->common_model->flash_message('success',$this->lang->line('logout_success')));
  //$this->auth_model->clearUserCookie(array('username','password'));
   //$userdata=$this->session->userdata('user');
   $this->session->unset_userdata('user');
   $this->session->unset_userdata('leaduser');
   $this->session->unset_userdata('university');
  //unset($userdata);
  //$userdata=$this->session->userdata('user');
  //$leaduser=$this->session->userdata('leaduser');
  //print_r($userdata); print_r($leaduser); exit();
  //$this->auth_model->clearUserCookie(array('user_name','user_password'));
  redirect('user/account');
  //$this->load->view('youraccount/index',$userdata);
    
 } //Function logout End



function forgot()

	{ 
     //Load Models - for this function
	 $this->load->helper(array('form', 'url'));
	 $this->load->library('form_validation');
	 $this->load->model('user/user_model');
	 $this->load->model('user/auth_model');
	 $this->load->helper('cookie'); 
	 
	 $this->form_validation->set_rules('credencial', 'Credencial', 'required|trim|xss_clean');
	 $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean');
	 
	 if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('templates/common/login');
			//$this->session->set_flashdata('flash_message', $this->form_validation->validation_errors());
			//redirect('common/login'); 
			//die('sssss');
		}
		else
		{  
	 
	
	 $credencial 	= $this->input->post('credencial');
	 $email 		= $this->input->post('email');
	 
	 $conditions 	=  array('infra_email_master.email'=>$email,'infra_email_master.contact_type'=>'1');
	 $query			= $this->user_model->checkUserBy_Email($conditions);
	 
	 $setting_details		= $this->settings_model->getSettings($conditions);
	 
	 foreach($setting_details as $setting){
	 if($setting->key=='config_email'){
	 $admin_email = $this->settings_model->getEmail($setting->value);
	  }
	 }
	 
	 if(count($query)> 0){
	 
	 if($query->status_id==1){
	 
	 if($credencial=='username'){
	
	 $to = $query->email;
	 $from = $admin_email;
	 $subject = 'Your User Name';
	 $username = $query->name;
	 
	 $link = '#'; $linktext = $username; 
	 $text = 'Your User Name is following -';
	 $body = $this->user_model->TempBody($username,$link,$linktext,$text);
	 
	 $this->user_model->SendMail($to,$from,$subject,$body) ;
	 $this->session->set_flashdata('flash_message', "Please check your mail to get User Name.");
	 
	 }elseif($credencial=='password'){
	 
	 // send the mail
	 $to = $query->email;
	 $from = $admin_email;
	 $subject = 'Please check you mail';
	 $username = $query->name;
	
	// $user_tbl_data['activation_key']     = md5(time());
	 
	 $user_tbl_data = array('reset_key' => md5(time()));
	 
	 $this->user_model->Update_User_activation($user_tbl_data,$query->id);
	 
	 
	 $link = base_url().'common/login/confirm/'.$query->id.'/'.$user_tbl_data['reset_key'];
	 $linktext = $link; 
	 $text = 'Please click the link below or copy and fire it in the browser to reset password';
	 
	 $body = $this->user_model->TempBody($username,$link,$linktext, $text);
	 
	 $this->user_model->SendMail($to,$from,$subject,$body) ;
	 $this->session->set_flashdata('flash_message', "Please check your mail to reset password.");
	 
	  }
	 }elseif($query->status_id==2){
	 
	 $this->session->set_flashdata('flash_message', "Sorry!Your account is Inactive. Please activate the account first.");
	
	 }elseif($query->status_id==3){
	 
	 $this->session->set_flashdata('flash_message', "Sorry!Your account is Blocked. Please contact to supaer admin.");
	
	 }elseif($query->status_id==4){
	 
	 $this->session->set_flashdata('flash_message', "Sorry!Your account is Deleted.");
	
	 }else{
	 
	  $this->session->set_flashdata('flash_message', "Sorry!Invalid email. Please sign up first.");
	 }
	 
	 }else{
	
	$this->session->set_flashdata('flash_message', "Sorry!Invalid email. Please sign up first.");
	}
	
	
	}
	
	 redirect('common/login');
	
} 


function confirm()

	{ 
	  if($this->uri->segment('5')){
	  
	  
	  $check_reset_data = array('infra_user_master.id' =>$this->uri->segment('4'),'infra_user_master.reset_key' => $this->uri->segment('5'),'infra_user_master.status_id' =>'1');
	  $query = $this->user_model->CheckReset($check_reset_data);
	  
	  if(count($query)> 0){
	  
	  $check_security_data = array('user_security_question.user_id' =>$this->uri->segment('4'));
	  $security_query = $this->user_model->getSecurity_Credential($check_security_data);
	 
	  $this->outputData['security_question1']= $security_query->question1; 
	  $this->outputData['security_question2']= $security_query->question2;
	  
	  $this->load->view('templates/common/security_verify',$this->outputData);
	  
	  }else{
	  
	     
	  
	  }
	  /*echo '<pre>';
	  print_r($query);
	  echo '</pre>';*/
	  }
	}

	
	function verify(){
	
	//echo $this->input->post('currenturl'); exit;
	if($this->uri->segment('4')){
	 $check_security_data = array('user_security_question.user_id' =>$this->uri->segment('4'));
	 $query = $this->user_model->checkUser(array('activation'=>$this->uri->segment('4')));
	 if($query){	 
	 $leadData = $this->user_model->getLead(array('email'=>$query->email));
	// print_r($leadData); exit;
	 
	 $sessiondata = array(
			 'id' 			=> $leadData->id,
			 'name'			=> $leadData->name,
			 'email'		=> $leadData->email,
			 'phone' 		=> $leadData->phone,
			 'mainstream'   => $leadData->mainstream,
			 'courses' 		=> $leadData->courses,
			 'degree' 		=> $leadData->degree,
			 'countries'    => $leadData->countries
	 );
	 $this->auth_model->clearUserSession();
	 $this->session->unset_userdata('leaduser');
     $this->session->set_userdata('leaduser', $sessiondata);
	 
	 $allData = $this->session->userdata('leaduser'); 
	 
	 $data = array(
     'id' => $query->id,
	 'name'=> $query->name,
     'email' => $query->email,
     'type' => $query->type,
	 'typename' => $query->typename,
     'is_logged_in' => true
     );
     $this->session->set_userdata('user', $data);
	 
	 $this->user_model->updateUserInfo(array('status'=>'1','activation'=>''),$query->id);
	 
	 redirect('search/university');
	  }else{
	 
	  $this->session->set_flashdata('flash_message', "Sorry!Invalid link.");
	  redirect('user/account');
	  	  
	   } 
	 }else{
	 
	 $this->session->set_flashdata('flash_message', "Sorry!Invalid link. Please sign up first.");
	 redirect('user/account');
	 }
	
	
	}
	
	
	function resetpassword(){
  
     if($this->input->post('submit')){
	
	 $user_tbl_data = array('infra_user_master.password' => md5($this->input->post('newpassword')));
	 
	 $this->user_model->Update_User_activation($user_tbl_data,$this->uri->segment('4'));
	 
	 
	 
	 
	 
	  $this->session->set_flashdata('flash_message', "Congrats! Pasword has been reset. Please Log In");
	  redirect('common/login');
	  
	 }
    }
	
	
	
	public function web_login()
	{//$this->load->library('facebook');
		//$this->load->helper('url');
//die('jjjjj');

		echo $this->facebook->logged_in();
	}



	

}//End  Home Class





?>