<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Account extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()

    {

        parent::__construct();
		

         $this->load->model('user/user_model');
		 $this->load->model('user/auth_model');
		 $this->load->model('location/country_model');
		 $this->load->model('location/state_model');
		 $this->load->model('location/city_model');
		 $this->load->model('additional/additional_model');

        

    }

	
	function index()

	{    $this->load->helper('cookie_helper'); 
         $this->load->model('user/auth_model');
		 
		 
		 $userdata=$this->session->userdata('user');
	     if(!empty($userdata)){  redirect('search/university'); }
		 $this->render_page('templates/user/login',$this->outputData);

		

						//$this->render_page('templates/common/index');
    } 



	function signup()
	
		{    $this->load->helper('cookie_helper'); 
			 $this->load->model('user/auth_model');
			$userdata=$this->session->userdata('user');
	        if(!empty($userdata)){  redirect('location/country'); }
			$this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));
			$this->outputData['locationcountries'] = $this->country_model->getLocationCountries(array('location_country_master.status'=>1));
			$this->outputData['intakes'] = $this->additional_model->getIntakes(array('intake_master.status' => '1'));	
			
			
			
		   $this->render_page('templates/user/sign_up',$this->outputData);
	
			
	
							//$this->render_page('templates/common/index');
	   } 
	   
	function create()
	 {   
	   //die('+++++');
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   
	   $this->load->model('course/course_model');
	   $this->load->model('user/auth_model');
       
	   $leaddata = $this->session->userdata('leaduser'); 
	   //$userdata=$this->session->userdata('user');
	   
      	$data = array(
        	'name' 			=>  $this->input->post('name'),
        	'email' 		=>  $this->input->post('email'),
        	'password' 		=>  md5($this->input->post('password')),
			'image'			=>  '',
			'type' 			=>  '5',
			'createdate' 	=>  date('Y-m-d'),
			'activation' 	=>	md5(time()),
			'fid'			=>  '',
			'status' 		=>	'3'
			
		);
		
		$user_id = $this->user_model->insertUserMaster($data);
		
		$student_data = array(        	
			'user_id' 				=>	$user_id,
			'address'				=>	$this->input->post('address'),			
			'city'					=>	$this->input->post('city'),
			'zip'					=>	$this->input->post('zip'),
			'dob'					=>	$this->input->post('dob'),
			'phone'					=>	$this->input->post('phone'),
			'parent_name'			=> 	$this->input->post('phone'),
			'parent_mob'			=> 	$this->input->post('parent_mob'),
			'parent_occupation'		=> 	$this->input->post('parent_occupation'),
			'hear_about_us'			=> 	$this->input->post('hear_info'),
			'passport'				=> 	$this->input->post('passport_availalibility'),
			'desired_destination'	=> 	implode(",",$this->input->post('deisred_destinations'))
		);
		//print_r($data); exit;	 
	     if($this->user_model->insertStudentDetails($student_data))
		 {  
		     
		///*********************************************			 
		if($this->input->post('desired_course_name')){
		
			$desired_course = array(
					'student_id'			=>	$user_id,
					'desired_course_name'	=>	$this->input->post('desired_course_name'),
					'desired_course_level'	=>	$this->input->post('desired_coures_level'),
					'desired_course_intake'	=>	$this->input->post('desired_course_intake'),
					'desired_course_year'	=>	$this->input->post('desired_coures_year')
				);
							
			if(!$this->user_model->checkDesiredCourse(array('student_id'=>$user_id))){			
			    $this->user_model->insertDesiredCourse($desired_course);		
			}	
		}			 
			 
		// Secondary Qualification 
		
		if($this->input->post('secondary_institution_name')){
		
			$secondary_qualification = array(
					'student_id'		=>	$user_id,
					'institution_name'	=>	$this->input->post('secondary_institution_name'),
					'board'				=>	$this->input->post('board'),
					'year_started'		=>	$this->input->post('secondary_year_starrted'),
					'year_finished'		=>	$this->input->post('secondary_year_finished'),
					'grade'				=>	$this->input->post('secondary_grade'),
					'total'				=>	$this->input->post('secondary_total')
				);			
			
			if(!$this->user_model->checkSecondaryQualification(array('student_id'=>$user_id))){			
			    $this->user_model->insertSecondaryQualification($secondary_qualification);
			}			 
		}
		
		// Diploma Or 12th
		
		 if($this->input->post('diploma_12')==1){
		 
		 	if($this->input->post('diploma_course_name')){
				$diploma_qualification = array(
					'student_id'		=>	$user_id,
					'course'			=>	$this->input->post('diploma_course_name'),
					'college'			=>	$this->input->post('diploma_institution_name'),
					'year_started'		=>	$this->input->post('diploma_year_starrted'),
					'year_finished'		=>	$this->input->post('diploma_year_finished'),
					'aggregate'			=>	$this->input->post('diploma_grade'),
					'total'				=>	$this->input->post('diploma_total')
				);			
			
				if(!$this->user_model->checkDiplomaQualification(array('student_id'=>$user_id))){			
					$this->user_model->insertDiplomaQualification($diploma_qualification);
				}
			}
		 
			
		 
		 }else if($this->input->post('diploma_12')==2){
		 
			 if($this->input->post('hs_institution_name')){
					$hs_qualification = array(
						'student_id'		=>	$user_id,
						'institution_name'	=>	$this->input->post('hs_institution_name'),
						'board'				=>	$this->input->post('hs_board'),
						'year_started'		=>	$this->input->post('hs_year_starrted'),
						'year_finished'		=>	$this->input->post('hs_year_finished'),
						'grade'				=>	$this->input->post('hs_grade'),
						'total'				=>	$this->input->post('hs_total')
					);				
				
					if(!$this->user_model->checkHigher_secondary_qualification(array('student_id'=>$user_id))){			
						$this->user_model->insertHigher_secondary_qualification($hs_qualification);	
					}
				}	 
				
		 }
		 
		 //Bachelor		
		if($this->input->post('bachelor_course')){
		
			$bachelor_qualification = array(
					'student_id'		=>	$user_id,
					'course'			=>	$this->input->post('bachelor_course'),
					'university'		=>	$this->input->post('bachelor_university'),
					'college'			=>	$this->input->post('bachelor_college'),
					'major'				=>	$this->input->post('bachelor_major'),
					'year_started'		=>	$this->input->post('bachelor_year_starrted'),
					'year_finished'		=>	$this->input->post('bachelor_year_finished'),
					'grade'				=>	$this->input->post('bachelor_grade'),
					'total'				=>	$this->input->post('bachelor_total')
				);	  	
			
				if(!$this->user_model->checkBachelorQualification(array('student_id'=>$user_id))){			   
					$this->user_model->insertBachelorQualification($bachelor_qualification);				
				}		 
			} 
			
		 // Masters
			if($this->input->post('masters')==1){
		 
		 	if($this->input->post('master_course')){
				$master_qualification = array(
					'student_id'		=>	$user_id,
					'course'			=>	$this->input->post('master_course'),
					'university'		=>	$this->input->post('master_university'),
					'college'			=>	$this->input->post('master_college'),
					'major'				=>	$this->input->post('master_major'),
					'year_started'		=>	$this->input->post('master_year_started'),
					'year_finished'		=>	$this->input->post('master_year_finished'),
					'grade'				=>	$this->input->post('master_grade'),
					'total'				=>	$this->input->post('master_total')
				);			
			
				if(!$this->user_model->checkMasterQualification(array('student_id'=>$user_id))){			   
					$this->user_model->insertMasterQualification($master_qualification);					
				}
			  }		 
		   }			
			 
			//************************************************* 
			 
			 
			 $leadMasterData =array(
			' name'			=> $this->input->post('name'),
			 'email'		=> $this->input->post('email'),
			 'phone' 		=> $this->input->post('phone'),
			 );
			 $this->user_model->updateLeadInfo($leadMasterData,$leaddata['id']);
			 
			 $sessiondata = array(
			 'id' 			=> $leaddata['id'],
			 'name'			=> $this->input->post('name'),
			 'email'		=> $this->input->post('email'),
			 'phone' 		=> $this->input->post('phone'),
			 'mainstream'   => $leaddata['mainstream'],
			 'courses' 		=> $leaddata['courses'],
			 'degree' 		=> $leaddata['degree'],
			 'countries'    => $leaddata['countries']
			 );
	        $this->auth_model->clearUserSession();
	        $this->session->unset_userdata('leaduser');
     		$this->session->set_userdata('leaduser', $sessiondata);
			
			
			// Mail to user for verification
			
			$from = 'sumanta@inventifweb.com';
			$to = $this->input->post('email');
			$subject = "Please verify your email";
			
			$message = "
			<head>
			<title>Please verify your email</title>
			</head>
			<body>
			<p>Hi ".$this->input->post('name').",</p>
			<p>Your Account has been sucessfully created under HelloUni.</p>
			<p>Please activate your account by clicking the link below or copy the link and fire it in the browser -</p>
			<p>Activation link - ".base_url()."user/account/verify/".$data['activation']."</p><br/><br/>
			<p>Your registered login credential is as follows -</p>
			<p>Email - ".$this->input->post('email')."</p>
			<p>Password -".$this->input->post('password')."</p>
			<p>URL - ".base_url()."</p>
			</body>
			";
			
			// Always set content-type when sending HTML email
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			
			// More headers
			$headers .= 'From:'.$from. "\r\n";
			mail($to,$subject,$message,$headers);		
			//if(mail($to,$subject,$message,$headers)){die('++');}else{die('--');}
		 	$this->session->set_flashdata('flash_message', "Success: Please verify!");
	     	redirect('user/account/');
	     }
	}
	
	function update()
	
		{    
		$this->load->helper('cookie_helper'); 
		$this->load->model('user/auth_model');
		$userdata=$this->session->userdata('user');
	    if(!$userdata){  redirect('user/acount'); }
		//print_r($userdata); exit;
		$condition = array('user_master.id'=>$userdata['id'],'user_master.status'=>1,'user_master.type'=>5);
		$userDetails = $this->user_model->getStudentDetails($condition);
			
		$this->outputData['id']= $userDetails->id; 
	 	$this->outputData['name']= $userDetails->name;
		$this->outputData['email']= $userDetails->email;
		$this->outputData['typename']= $userDetails->typename;
		$this->outputData['address']= $userDetails->address;
		$this->outputData['country']= $userDetails->country;
		$this->outputData['city']= $userDetails->city;		
		
		//print_r($this->state_model->getParticularCity(array('city_master.id' =>$userDetails->city,'city_master.status'=>1))); 
		$this->outputData['selected_state'] = $this->state_model->getParticularCity(array('city_master.id' =>$userDetails->city,'city_master.status'=>1))->state_id;		
		$this->outputData['selected_country']= $this->state_model->getParticularCity(array('city_master.id' =>$userDetails->city,'city_master.status'=>1))->country_id;
		
		$this->outputData['related_state_list'] = $this->state_model->getLocationStates(array('state_master.country_id' =>$this->outputData['selected_country'],'state_master.status'=>1));
		
		$this->outputData['related_city_list'] = $this->state_model->getCities(array('city_master.state_id' =>$this->outputData['selected_state'],'city_master.status'=>1));
		
		//print_r($this->outputData['related_state_list']); exit; 
		
		
		
		
		$this->outputData['zip']= $userDetails->zip;
		$this->outputData['dob']= $userDetails->dob;
		$this->outputData['phone']= $userDetails->phone;
		$this->outputData['parent_name']= $userDetails->parent_name;
		$this->outputData['parent_mob']= $userDetails->parent_mob;
		$this->outputData['parent_occupation']= $userDetails->parent_occupation;
		$this->outputData['hear_about_us']= $userDetails->hear_about_us;
		$this->outputData['passport']= $userDetails->passport;
		$this->outputData['desired_destination']= $userDetails->desired_destination;
		$this->outputData['fid']= $userDetails->fid;
		//print_r($userDetails); exit;
		
		
		
		
		
		
		//desired courses
		if($this->user_model->checkDesiredCourse(array('student_id'=>$userdata['id']))){			
			    $this->outputData['desiredcourse_details'] = $this->user_model->checkDesiredCourse(array('student_id'=>$userdata['id']));			
		}else{
				$this->outputData['desiredcourse_details'] = '';
		}
		
		//Secondary Qualification
		if($this->user_model->checkSecondaryQualification(array('student_id'=>$userdata['id']))){			
			    $this->outputData['secondaryqualification_details'] = $this->user_model->checkSecondaryQualification(array('student_id'=>$userdata['id']));			
		}else{
				$this->outputData['secondaryqualification_details'] = '';
		}
		
		
		//Diploma
		if($this->user_model->checkDiplomaQualification(array('student_id'=>$userdata['id']))){			
				$this->outputData['diplomaqualification_details'] = $this->user_model->checkDiplomaQualification(array('student_id'=>$userdata['id']));			
		}else{
				$this->outputData['diplomaqualification_details'] = '';
		}
		
		// 12th
		if($this->user_model->checkHigher_secondary_qualification(array('student_id'=>$userdata['id']))){
				$this->outputData['highersecondaryqualification_details'] = $this->user_model->checkHigher_secondary_qualification(array('student_id'=>$userdata['id']));			
		}else{
				$this->outputData['highersecondaryqualification_details'] = '';			
		}
		
		//Bachelor
		if($this->user_model->checkBachelorQualification(array('student_id'=>$userdata['id']))){
				$this->outputData['bachelorqualification_details'] = $this->user_model->checkBachelorQualification(array('student_id'=>$userdata['id']));		
		}else{
				$this->outputData['bachelorqualification_details'] = '';
		}
		
		//Masters
		if($this->user_model->checkMasterQualification(array('student_id'=>$userdata['id']))){	
				$this->outputData['mastersqualification_details'] = $this->user_model->checkMasterQualification(array('student_id'=>$userdata['id']));	
		}else{
				$this->outputData['mastersqualification_details'] = '';	
		}
		
		
		$this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));
		$this->outputData['locationcountries'] = $this->country_model->getLocationCountries(array('location_country_master.status'=>1));
		$this->outputData['intakes'] = $this->additional_model->getIntakes(array('intake_master.status' => '1'));	
			
		$this->render_page('templates/user/update_profile',$this->outputData);
							//$this->render_page('templates/common/index');
	   } 
	   
	 function save()
	 {   
	   
	   $this->load->helper(array('form', 'url'));
	   $this->load->library('form_validation');	   
	   $this->load->model('course/course_model');
	   $this->load->model('user/auth_model');
       
	   //$leaddata = $this->session->userdata('leaduser'); 
	   $userdata=$this->session->userdata('user');
	   
	   if(!$userdata['fid']){
	   
	   $details = array(
        	'name' 			=>  $this->input->post('name')
        	//'email' 		=>  $this->input->post('email')
        	//'password' 		=>  md5($this->input->post('password')),
					
		);	
		$this->user_model->updateUserInfo($details,$userdata['id'])	;
		
		}
		
		
		$data = array(
        	'address'				=>	$this->input->post('address'),			
			'city'					=>	$this->input->post('city'),
			'zip'					=>	$this->input->post('zip'),
			'dob'					=>	$this->input->post('dob'),
			'phone'					=>	$this->input->post('phone'),
			'parent_name'			=>	$this->input->post('parent_name'),
			'parent_mob'			=>	$this->input->post('parent_mob'),
			'parent_occupation'		=>	$this->input->post('parent_occupation'),
			'hear_about_us'			=>	$this->input->post('hear_info'),
			'passport'				=>	$this->input->post('passport_availalibility'), 
			'desired_destination' 	=>	implode(",",$this->input->post('deisred_destinations'))
		);
		$this->user_model->updateStudentDetails($data,$userdata['id']);
		
		//desired course details
		
		if($this->input->post('desired_course_name')){
		
			$desired_course = array(
					'student_id'			=>	$userdata['id'],
					'desired_course_name'	=>	$this->input->post('desired_course_name'),
					'desired_course_level'	=>	$this->input->post('desired_coures_level'),
					'desired_course_intake'	=>	$this->input->post('desired_course_intake'),
					'desired_course_year'	=>	$this->input->post('desired_coures_year')
				);
			
			
			if($this->user_model->checkDesiredCourse(array('student_id'=>$userdata['id']))){			
			    $this->user_model->updateDesiredCourse($desired_course,$userdata['id']);			
			}else{
				$this->user_model->insertDesiredCourse($desired_course);
			}		
		}else{ 
		
			$this->user_model->deleteDesiredCourse($userdata['id']);		
		
		}
		
		//secondary qualification details
		//echo $this->input->post('secondary_institution_name'); exit;
		if($this->input->post('secondary_institution_name')){
		
			$secondary_qualification = array(
					'student_id'		=>	$userdata['id'],
					'institution_name'	=>	$this->input->post('secondary_institution_name'),
					'board'				=>	$this->input->post('board'),
					'year_started'		=>	$this->input->post('secondary_year_starrted'),
					'year_finished'		=>	$this->input->post('secondary_year_finished'),
					'grade'				=>	$this->input->post('secondary_grade'),
					'total'				=>	$this->input->post('secondary_total')
				);
		}	
			
			if($this->user_model->checkSecondaryQualification(array('student_id'=>$userdata['id']))){			
			    if($this->input->post('secondary_institution_name')){
					$this->user_model->updateSecondaryQualification($secondary_qualification,$userdata['id']);	
				}else{				
					$this->user_model->deleteSecondaryQualification($userdata['id']);	
				}		
			}else{
				 if($this->input->post('secondary_institution_name')){
					$this->user_model->insertSecondaryQualification($secondary_qualification);
				 }
			}		
		
		
		// Diploma Or 12th
		
		 if($this->input->post('diploma_12')==1){
		 
		 	if($this->input->post('diploma_course_name')){
				$diploma_qualification = array(
					'student_id'		=>	$userdata['id'],
					'course'			=>	$this->input->post('diploma_course_name'),
					'college'			=>	$this->input->post('diploma_institution_name'),
					'year_started'		=>	$this->input->post('diploma_year_starrted'),
					'year_finished'		=>	$this->input->post('diploma_year_finished'),
					'aggregate'			=>	$this->input->post('diploma_grade'),
					'total'				=>	$this->input->post('diploma_total')
				);
			}
			
			if($this->user_model->checkDiplomaQualification(array('student_id'=>$userdata['id']))){			
			    if($this->input->post('diploma_course_name')){
					$this->user_model->updateDiplomaQualification($diploma_qualification,$userdata['id']);	
				}else{				
					$this->user_model->deleteDiplomaQualification($userdata['id']);	
				}		
			}else{
				 if($this->input->post('diploma_course_name')){
					$this->user_model->insertDiplomaQualification($diploma_qualification);
				 }
			}
			
		 
			 //delete else here
			 if($this->user_model->checkHigher_secondary_qualification(array('student_id'=>$userdata['id']))){
				$this->user_model->deleteHigher_secondary_qualification($userdata['id']);
			 }
		 
		 
		 }else if($this->input->post('diploma_12')==2){
		 
			 if($this->input->post('hs_institution_name')){
					$hs_qualification = array(
						'student_id'		=>	$userdata['id'],
						'institution_name'	=>	$this->input->post('hs_institution_name'),
						'board'				=>	$this->input->post('hs_board'),
						'year_started'		=>	$this->input->post('hs_year_starrted'),
						'year_finished'		=>	$this->input->post('hs_year_finished'),
						'grade'				=>	$this->input->post('hs_grade'),
						'total'				=>	$this->input->post('hs_total')
					);
				}
				
				if($this->user_model->checkHigher_secondary_qualification(array('student_id'=>$userdata['id']))){			
					if($this->input->post('hs_institution_name')){
						$this->user_model->updateHigher_secondary_qualification($hs_qualification,$userdata['id']);	
					}else{				
						$this->user_model->deleteHigher_secondary_qualification($userdata['id']);	
					}		
				}else{
					 if($this->input->post('hs_institution_name')){
						$this->user_model->insertHigher_secondary_qualification($hs_qualification);
					 }
				}
		 
				//delete else here
				 if($this->user_model->checkDiplomaQualification(array('student_id'=>$userdata['id']))){
					$this->user_model->deleteDiplomaQualification($userdata['id']);
				 }
		 }else{
		 
		 	$this->user_model->deleteDiplomaQualification($userdata['id']);	
			$this->user_model->deleteHigher_secondary_qualification($userdata['id']);	
		 
		 }
		
		//Bachelor
		
		if($this->input->post('bachelor_course')){
		
			$bachelor_qualification = array(
					'student_id'		=>	$userdata['id'],
					'course'			=>	$this->input->post('bachelor_course'),
					'university'		=>	$this->input->post('bachelor_university'),
					'college'			=>	$this->input->post('bachelor_college'),
					'major'				=>	$this->input->post('bachelor_major'),
					'year_started'		=>	$this->input->post('bachelor_year_starrted'),
					'year_finished'		=>	$this->input->post('bachelor_year_finished'),
					'grade'				=>	$this->input->post('bachelor_grade'),
					'total'				=>	$this->input->post('bachelor_total')
				);
		   }	
			
			if($this->user_model->checkBachelorQualification(array('student_id'=>$userdata['id']))){			
			    if($this->input->post('bachelor_course')){
					$this->user_model->updateBachelorQualification($bachelor_qualification,$userdata['id']);	
				}else{				
					$this->user_model->deleteBachelorQualification($userdata['id']);	
				}		
			}else{
				 if($this->input->post('bachelor_course')){
					$this->user_model->insertBachelorQualification($bachelor_qualification);
				 }
			}
			
			
			
			if($this->input->post('masters')==1){
		 
		 	if($this->input->post('master_course')){
				$master_qualification = array(
					'student_id'		=>	$userdata['id'],
					'course'			=>	$this->input->post('master_course'),
					'university'		=>	$this->input->post('master_university'),
					'college'			=>	$this->input->post('master_college'),
					'major'				=>	$this->input->post('master_major'),
					'year_started'		=>	$this->input->post('master_year_started'),
					'year_finished'		=>	$this->input->post('master_year_finished'),
					'grade'				=>	$this->input->post('master_grade'),
					'total'				=>	$this->input->post('master_total')
				);
			}
			
			if($this->user_model->checkMasterQualification(array('student_id'=>$userdata['id']))){			
			    if($this->input->post('master_course')){
					$this->user_model->updateMasterQualification($master_qualification,$userdata['id']);	
				}else{				
					$this->user_model->deleteMasterQualification($userdata['id']);	
				}		
			}else{
				 if($this->input->post('master_course')){
					$this->user_model->insertMasterQualification($master_qualification);
				 }
			}		 
		 
		 }else{
		 
		 		$this->user_model->deleteMasterQualification($userdata['id']);		 
		 
		 }
		
		
		//print_r($bachelor_qualification); exit;
		
		
		
		
		
		
		
		$from = 'sumanta@inventifweb.com';
		
			 if(!$userdata['fid']){
			  $email = $this->input->post('email');
			  $name = $this->input->post('name');
			 }else{
			  $email = $userdata['email'];
			  $name = $userdata['name'];
			 }
			//$to = $this->input->post('email');
			$subject = "Your Account has been Updated";
			$to = $userdata['email'];
			$message = "
			<head>
			<title>Your Account has been Updated</title>
			</head>
			<body>
			<p>Hi ".$userdata['name'].",</p>
			<p>Your Account has been Updated.</p>
			
			
			<p>URL - ".base_url()."</p>
			</body>
			";
			
			// Always set content-type when sending HTML email
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			
			// More headers
			$headers .= 'From:'.$from. "\r\n";
			mail($to,$subject,$message,$headers);		
			//if(mail($to,$subject,$message,$headers)){die('++');}else{die('--');}
		 	$this->session->set_flashdata('flash_message', "Success: Please verify!");
			
				
		/*if(!$userdata['fid']){
		redirect('user/account/logout');
		}else{
		//redirect('user/account');
		}*/
		
		
		//print_r($data); exit;	 
	    
			// Mail to user for verification
			
			 $this->session->set_flashdata('flash_message', "Your profile has been updated.");
	     	redirect('user/account/update');
	     
	}
	
	function login()

	{
     //Load Models - for this function
	 $this->load->model('user/user_model');
	 $this->load->model('user/auth_model');
	 $this->load->helper('cookie'); 
	
	 
     $email = $this->input->post('email');
	 $password = md5($this->input->post('password')); //exit;
	 $conditions 		=  array('user_master.email'=>$email,'user_master.password' => $password,'user_master.status' => 1);
	 $query				= $this->user_model->checkUser($conditions); 
		 
	 if(count($query)> 0){ 
	  
	  $data = array(
     'id' => $query->id,
	 'name'=> $query->name,
     'email' => $query->email,
     'type' => $query->type,
	 'typename' => $query->typename,
	 'fid' =>'',
     'is_logged_in' => true
     );
     $this->session->set_userdata('user', $data);
	 
	/* if($this->input->post('remember'))
						{ 
						    $insertData=array();
						    $insertData['username']=$this->input->post('username');
						    $insertData['password']=$this->input->post('pwd');
						    $expire=60*60*24*100;
							if( $this->auth_model->getUserCookie('uname')=='')
							{ 
							$this->user_model->addRemerberme($insertData,$expire); 
							
							}		
						}
						else
						{
						   $this->user_model->removeRemeberme(); 
							
						}*/
							
							
	 redirect('search/university');
	 }else{
	 $this->session->set_flashdata('flash_message', "Sorry! Invalid Login.");
	
	 redirect('user/account');
	 }
} 



function fblogin()

	{
     //Load Models - for this function
	 $this->load->model('user/user_model');
	 $this->load->model('user/auth_model');
	 $this->load->helper('cookie'); 
	 $leaddata = $this->session->userdata('leaduser'); 
	 
	 $fid = $this->input->post('id'); //exit;
	 $name = $this->input->post('name');
	 $email = $this->input->post('email');
	 $image = $this->input->post('image');
	// $password = md5($this->input->post('password')); //exit;
	 $conditions 		=  array('user_master.email'=>$email,'user_master.status' => 1);
	 $query				= $this->user_model->checkUser($conditions); 
		 
	 if(count($query)> 0){ 
	  
	  $data = array(
        	'name' 			=>  $this->input->post('name'),
        	'email' 		=>  $this->input->post('email'),
        	'password' 		=>  '',
			'image'			=>  $this->input->post('image'),
			'type' 			=>  '5',
			'createdate' 	=>  date('Y-m-d'),
			'activation' 	=>	'',
			'fid' 			=>	$this->input->post('id'),
			'status' 		=>	'1'
			/*'address'		=>	'',
			'country'		=>	'',
			'city'			=>	'',
			'zip'			=>	'',
			'dob'			=>	'',
			'phone'			=>	''*/
		);
	  
	  if($this->user_model->updateUserInfo($data,$query->id)){
	  
	  $usrdata = array(
					 'id' => $query->id,
					 'name'=> $this->input->post('name'),
					 'email' => $this->input->post('email'),
					 'fid' => $this->input->post('id'),
					 'type' => 5,
					 'is_logged_in' => true
			      );
	   $this->session->set_userdata('user', $usrdata);
	  echo 1;
	  
	  }
	  
	  
	 
	/* if($this->input->post('remember'))
						{ 
						    $insertData=array();
						    $insertData['username']=$this->input->post('username');
						    $insertData['password']=$this->input->post('pwd');
						    $expire=60*60*24*100;
							if( $this->auth_model->getUserCookie('uname')=='')
							{ 
							$this->user_model->addRemerberme($insertData,$expire); 
							
							}		
						}
						else
						{
						   $this->user_model->removeRemeberme(); 
							
						}*/
							
							
	// redirect('search/university');
	 }else{
	 
	 
	 $data = array(
        	'name' 			=>  $this->input->post('name'),
        	'email' 		=>  $this->input->post('email'),
        	'password' 		=>  '',
			'image'			=>  $this->input->post('image'),
			'type' 			=>  '5',
			'createdate' 	=>  date('Y-m-d'),
			'activation' 	=>	'',
			'fid' 			=>	$this->input->post('id'),
			'status' 		=>	'1',
			'address'		=>	'',
			'country'		=>	'',
			'city'			=>	'',
			'zip'			=>	'',
			'dob'			=>	'',
			'phone'			=>	''
		);
		$userid = $this->user_model->insertStudent($data);
		if($userid)
		 {  
		     
			 if($leaddata['id']){
			 $leadMasterData =array(
			' name'			=> $this->input->post('name'),
			 'email'		=> $this->input->post('email'),
			 'phone' 		=> $this->input->post('phone'),
			 );
			 $this->user_model->updateLeadInfo($leadMasterData,$leaddata['id']);
			 
			 $sessiondata = array(
			 'id' 			=> $leaddata['id'],
			 'name'			=> $this->input->post('name'),
			 'email'		=> $this->input->post('email'),
			 'phone' 		=> $this->input->post('phone'),
			 'mainstream'   => $leaddata['mainstream'],
			 'courses' 		=> $leaddata['courses'],
			 'degree' 		=> $leaddata['degree'],
			 'countries'    => $leaddata['countries']
			 );
	        $this->auth_model->clearUserSession();
	        $this->session->unset_userdata('leaduser');	
			$this->session->set_userdata('leaduser', $sessiondata);
			}
			$usrdata = array(
					 'id' => $userid,
					 'name'=> $this->input->post('name'),
					 'email' => $this->input->post('email'),
					 'fid' => $this->input->post('id'),
					 'type' => 5,
					 'is_logged_in' => true
			);
			$this->session->set_userdata('user', $usrdata);
     		
			
			
			// Mail to user for verification
			
			$from = 'sumanta@inventifweb.com';
			$to = $this->input->post('email');
			$subject = "Your Account has been sucessfully created under HelloUni.";
			
			$message = "
			<head>
			<title>Your Account has been sucessfully created under HelloUni.</title>
			</head>
			<body>
			<p>Hi ".$this->input->post('name').",</p>
			<p>Your Account has been sucessfully created under HelloUni.</p>
			<br/><br/>
			<p>Your registered email credential is as follows -</p>
			<p>Email - ".$this->input->post('email')."</p>
			<p>URL - ".base_url()."</p>
			</body>
			";
			
			// Always set content-type when sending HTML email
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			
			// More headers
			$headers .= 'From:'.$from. "\r\n";
			mail($to,$subject,$message,$headers);		
			//if(mail($to,$subject,$message,$headers)){die('++');}else{die('--');}
		 	$this->session->set_flashdata('flash_message', "Success: Please verify!");
	     	//redirect('user/account/');
			
			echo 1;
	     }
		
		
     //$this->session->set_flashdata('flash_message', "Sorry! Invalid Login.");
	// redirect('user/account');
	  }
	 } 


function logout()
 { 
  $this->load->model('user/auth_model');
  $this->auth_model->clearUserSession();
  //$this->session->set_flashdata('flash_message', $this->common_model->flash_message('success',$this->lang->line('logout_success')));
  //$this->auth_model->clearUserCookie(array('username','password'));
   //$userdata=$this->session->userdata('user');
   $this->session->unset_userdata('user');
   $this->session->unset_userdata('leaduser');
   $this->session->unset_userdata('university');
  //unset($userdata);
  //$userdata=$this->session->userdata('user');
  //$leaduser=$this->session->userdata('leaduser');
  //print_r($userdata); print_r($leaduser); exit();
  //$this->auth_model->clearUserCookie(array('user_name','user_password'));
  redirect('user/account');
  //$this->load->view('youraccount/index',$userdata);
    
 } //Function logout End
 
 
 
 function getStates(){  
   
 	//echo 1;	
		if($this->input->post('id')){
			
			$locationcountries = $this->state_model->getLocationStates(array('state_master.country_id' =>$this->input->post('id'),'state_master.status'=>1));
			foreach($locationcountries as $location){
						echo "<option value='".$location->zone_id."'>".$location->name."</option>";
			}
		}		  
 } 
 
 function getCities(){  
   
 	//echo 1;	
		if($this->input->post('id')){
			
			$locationcities = $this->state_model->getCities(array('city_master.state_id' =>$this->input->post('id'),'city_master.status'=>1));
			foreach($locationcities as $location){
						echo "<option value='".$location->id."'>".$location->city_name."</option>";
			}
		}		  
 }



function forgot()

	{ 
     //Load Models - for this function
	 $this->load->helper(array('form', 'url'));
	 $this->load->library('form_validation');
	 $this->load->model('user/user_model');
	 $this->load->model('user/auth_model');
	 $this->load->helper('cookie'); 
	 
	 $this->form_validation->set_rules('credencial', 'Credencial', 'required|trim|xss_clean');
	 $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean');
	 
	 if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('templates/common/login');
			//$this->session->set_flashdata('flash_message', $this->form_validation->validation_errors());
			//redirect('common/login'); 
			//die('sssss');
		}
		else
		{  
	 
	
	 $credencial 	= $this->input->post('credencial');
	 $email 		= $this->input->post('email');
	 
	 $conditions 	=  array('infra_email_master.email'=>$email,'infra_email_master.contact_type'=>'1');
	 $query			= $this->user_model->checkUserBy_Email($conditions);
	 
	 $setting_details		= $this->settings_model->getSettings($conditions);
	 
	 foreach($setting_details as $setting){
	 if($setting->key=='config_email'){
	 $admin_email = $this->settings_model->getEmail($setting->value);
	  }
	 }
	 
	 if(count($query)> 0){
	 
	 if($query->status_id==1){
	 
	 if($credencial=='username'){
	
	 $to = $query->email;
	 $from = $admin_email;
	 $subject = 'Your User Name';
	 $username = $query->name;
	 
	 $link = '#'; $linktext = $username; 
	 $text = 'Your User Name is following -';
	 $body = $this->user_model->TempBody($username,$link,$linktext,$text);
	 
	 $this->user_model->SendMail($to,$from,$subject,$body) ;
	 $this->session->set_flashdata('flash_message', "Please check your mail to get User Name.");
	 
	 }elseif($credencial=='password'){
	 
	 // send the mail
	 $to = $query->email;
	 $from = $admin_email;
	 $subject = 'Please check you mail';
	 $username = $query->name;
	
	// $user_tbl_data['activation_key']     = md5(time());
	 
	 $user_tbl_data = array('reset_key' => md5(time()));
	 
	 $this->user_model->Update_User_activation($user_tbl_data,$query->id);
	 
	 
	 $link = base_url().'common/login/confirm/'.$query->id.'/'.$user_tbl_data['reset_key'];
	 $linktext = $link; 
	 $text = 'Please click the link below or copy and fire it in the browser to reset password';
	 
	 $body = $this->user_model->TempBody($username,$link,$linktext, $text);
	 
	 $this->user_model->SendMail($to,$from,$subject,$body) ;
	 $this->session->set_flashdata('flash_message', "Please check your mail to reset password.");
	 
	  }
	 }elseif($query->status_id==2){
	 
	 $this->session->set_flashdata('flash_message', "Sorry!Your account is Inactive. Please activate the account first.");
	
	 }elseif($query->status_id==3){
	 
	 $this->session->set_flashdata('flash_message', "Sorry!Your account is Blocked. Please contact to supaer admin.");
	
	 }elseif($query->status_id==4){
	 
	 $this->session->set_flashdata('flash_message', "Sorry!Your account is Deleted.");
	
	 }else{
	 
	  $this->session->set_flashdata('flash_message', "Sorry!Invalid email. Please sign up first.");
	 }
	 
	 }else{
	
	$this->session->set_flashdata('flash_message', "Sorry!Invalid email. Please sign up first.");
	}
	
	
	}
	
	 redirect('common/login');
	
} 


function confirm()

	{ 
	  if($this->uri->segment('5')){
	  
	  
	  $check_reset_data = array('infra_user_master.id' =>$this->uri->segment('4'),'infra_user_master.reset_key' => $this->uri->segment('5'),'infra_user_master.status_id' =>'1');
	  $query = $this->user_model->CheckReset($check_reset_data);
	  
	  if(count($query)> 0){
	  
	  $check_security_data = array('user_security_question.user_id' =>$this->uri->segment('4'));
	  $security_query = $this->user_model->getSecurity_Credential($check_security_data);
	 
	  $this->outputData['security_question1']= $security_query->question1; 
	  $this->outputData['security_question2']= $security_query->question2;
	  
	  $this->load->view('templates/common/security_verify',$this->outputData);
	  
	  }else{
	  
	     
	  
	  }
	  /*echo '<pre>';
	  print_r($query);
	  echo '</pre>';*/
	  }
	}

	
	function verify(){
	
	//echo $this->input->post('currenturl'); exit;
	if($this->uri->segment('4')){
	 $check_security_data = array('user_security_question.user_id' =>$this->uri->segment('4'));
	 $query = $this->user_model->checkUser(array('activation'=>$this->uri->segment('4')));
	 if($query){	 
	 $leadData = $this->user_model->getLead(array('email'=>$query->email));
	// print_r($leadData); exit;
	 
	 $sessiondata = array(
			 'id' 			=> $leadData->id,
			 'name'			=> $leadData->name,
			 'email'		=> $leadData->email,
			 'phone' 		=> $leadData->phone,
			 'mainstream'   => $leadData->mainstream,
			 'courses' 		=> $leadData->courses,
			 'degree' 		=> $leadData->degree,
			 'countries'    => $leadData->countries
	 );
	 $this->auth_model->clearUserSession();
	 $this->session->unset_userdata('leaduser');
     $this->session->set_userdata('leaduser', $sessiondata);
	 
	 $allData = $this->session->userdata('leaduser'); 
	 
	 $data = array(
     'id' => $query->id,
	 'name'=> $query->name,
     'email' => $query->email,
     'type' => $query->type,
	 'typename' => $query->typename,
     'is_logged_in' => true
     );
     $this->session->set_userdata('user', $data);
	 
	 $this->user_model->updateUserInfo(array('status'=>'1','activation'=>''),$query->id);
	 
	 redirect('search/university');
	  }else{
	 
	  $this->session->set_flashdata('flash_message', "Sorry!Invalid link.");
	  redirect('user/account');
	  	  
	   } 
	 }else{
	 
	 $this->session->set_flashdata('flash_message', "Sorry!Invalid link. Please sign up first.");
	 redirect('user/account');
	 }
	
	
	}
	
	function checkemail(){
  
     if($this->input->is_ajax_request()) {	
		 $email = $this->input->post('email');		 
		 $conditions 		=  array('user_master.email'=>$email);
		 $query				= $this->user_model->checkUser($conditions);
			 
		 if(count($query)> 0){ 		 
		 	echo 1; 
		 }else{
		 	echo 0; 
		 }
	  
	 }
    }
	
	
	function resetpassword(){
  
     if($this->input->post('submit')){
	
	 $user_tbl_data = array('infra_user_master.password' => md5($this->input->post('newpassword')));
	 
	 $this->user_model->Update_User_activation($user_tbl_data,$this->uri->segment('4'));
	 
	 
	 
	 
	 
	  $this->session->set_flashdata('flash_message', "Congrats! Pasword has been reset. Please Log In");
	  redirect('common/login');
	  
	 }
    }
	
	
	
	public function web_login()
	{//$this->load->library('facebook');
		//$this->load->helper('url');
//die('jjjjj');

		echo $this->facebook->logged_in();
	}
	
	
function tokboxdemo()

	{    $this->load->helper('cookie_helper'); 
         $this->load->model('user/auth_model');
		 
		 		
		 //$this->render_page('templates/user/tokboxdemo',$this->outputData);
		 $this->load->view('templates/user/tokboxdemo', $this->outputData);

		

						//$this->render_page('templates/common/index');
    } 


	

}//End  Home Class





?>