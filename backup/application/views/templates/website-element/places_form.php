<?php if($this->uri->segment('5') && $this->uri->segment('5')=='view'){

$view='disabled'; ?>

<script type="text/javascript">
$( document ).ready(function() {
$("#cleditor").cleditor()[0].disable("true");
//$(".btn .btn-primary .pull-right").attr('disabled' , true);
$('.pull-right').prop('disabled', true);

});

</script>
<?php } else { $view=''; }


 ?>
        
        <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- Statistics Button Container -->
            	
                
                <!-- Panels Start -->
				
				 <?php if($msg = $this->session->flashdata('flash_message')){?> 
				<div class="mws-form-message success"><?php echo $msg;?></div>
				 <?php } ?>
				 
				<?php if(validation_errors()){?> 
                <div class="mws-form-message error"><?php echo validation_errors(); ?></div>
                <?php } ?>

            	<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-magic"></i> Places</span>
                    </div>
                    <div class="mws-panel-body no-padding">
					
					  <?php if($this->uri->segment('4')){?>
					   <form class="mws-form wzd-default"  action="<?php echo base_url();?>website-element/places/edit/<?php if(isset($id)) echo $id;?>" method="post">
					  <?php }else{?>
                        <form class="mws-form wzd-default" action="<?php echo base_url();?>website-element/places/create" method="post">
                      <?php } ?>
                            <fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> General</legend>
                                <div id class="mws-form-row">
                                    <label class="mws-form-label">Name <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" <?php echo $view; ?> name="place_name" class="required large" value="<?php if(isset($name)) echo $name; ?>" required>
                                    </div>
                                </div>
                              
                                <div class="mws-form-row">
                                    <label class="mws-form-label">Description</label>
                                    <div class="mws-form-item">
                                        <textarea name="place_description" id="cleditor" rows="" cols="" class="required large"><?php if(isset($description)) echo $description; ?></textarea>
                                    </div>
                                </div>
                                 <div class="mws-form-row">
                                    <label class="mws-form-label">Distance</label>
                                    <div class="mws-form-item">
                                        <input type="text" <?php echo $view; ?> name="place_distance" class="required email large" value="<?php if(isset($distance)) echo $distance; ?>">
                                    </div>
                                </div>
								
								
								
								
								
								
								
							</fieldset>
							
							
							  <fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> Links</legend>
								
								 <div class="mws-form-row">
                                    <label class="mws-form-label">Photo Gallery</label>
                                    <div class="mws-form-item">
										<select name="place_photo" <?php echo $view; ?>>
										<?php foreach($photoes as $photo) {?>
											
											<?php if(isset($photo_id) && $photo_id== $photo->id){?>
											<option value="<?php echo $photo->id;?>" selected="selected"><?php echo $photo->name;?></option>
                                           <?php }else {?>
										   <option value="<?php echo $photo->id;?>"><?php echo $photo->name;?></option>
										   <?php }
										     }?>									   
										</select>
                                    </div>
                                </div>
								
								
								    <div class="mws-form-row">
                                    <label class="mws-form-label">Video Gallery</label>
                                    <div class="mws-form-item">
										<select name="place_video" <?php echo $view; ?>>
										<?php foreach($videos as $video) {?>
											
											<?php if(isset($video_id) && $video_id== $video->id){?>
											<option value="<?php echo $video->id;?>" selected="selected"><?php echo $video->name;?></option>
                                           <?php }else {?>
										   <option value="<?php echo $video->id;?>"><?php echo $video->name;?></option>
										   <?php }
										     }?>									   
										</select>
                                    </div>
                                </div> 
								</fieldset>
							
							
							
							
							
                        </form>
                    </div>
                </div>

              

                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->
			
