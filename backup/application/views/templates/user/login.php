<?php
########## app ID and app SECRET (Replace with yours) #############
$appId = '1703402549928493'; //Facebook App ID
$appSecret = 'f6513c6aa28d37e2f827b3d1e51541a5'; // Facebook App Secret
$return_url = 'http://inventifweb.net/UNI/search/university';  //path to script folder
$fbPermissions = 'publish_actions,public_profile,email,user_location'; // more permissions : https://developers.facebook.com/docs/authentication/permissions/

########## MySql details (Replace with yours) #############
/*$db_username = "root"; //Database Username
$db_password = ""; //Database Password
$hostname = "localhost"; //Mysql Hostname
$db_name = 'demo'; //Database Name*/
###################################################################

?>
    
   
 <!--<script src="js/jquery-1.11.0.js"></script>
       
  <script type="text/javascript" src="src/DateTimePicker.js"></script>
   <script src="tabs/assets/js/responsive-tabs.min.js"></script>-->
   
    <style>
   	 .shade_border{
		background:url(img/shade1.png) repeat-x; height:11px;
	}
	.search_by{
	background-color:#f3f4f4; box-shadow:5px 5px 5px #eae8e8; height:auto;
	    padding-top: 10px;
    padding-left: 30px;
	 padding-right: 30px;
	}
	.search_by h4{
		color:#3c3c3c;
		font-family:lato_regular;
		font-weight:500;
	}
	.table_heading{
		padding:5px;
		font-weight:bold;
	}
	.more_detail{
	background-color:#F6881F;
	text-transform:uppercase;
	height:28px;	
	border:0px;
	  color:#000; 
		border-radius:2px;
		width:25px;
}
.right_side_form{
background:#3c3c3c;
box-shadow:5px 5px 2px #CCCCCC; 
height:auto;

}
label{
	color:#fff;
	
}
.more_detail{
	background-color:#F6881F;
	text-transform:uppercase;
	height:28px;	
	border:0px;
	  color:#fff; 
		border-radius:2px;
		
}
label{
font-size:11px;
font-weight:300;	
}
#input_container { position:relative; padding:0; margin:0;     padding-top: 25px;
    }
.input { margin:0; padding-left: 62px; }

#input_img { position:absolute; bottom:150px; left:15px; width:50px; height:27px; margin-left:-5px;}

#input2 { margin:0; padding-left: 62px; }

#input_img2 { position:absolute; bottom:100px; left:15px; width:50px; height:27px; }
#input3 { margin:0; padding-left: 62px; }

#input_img3 { position:absolute; bottom:163px; left:15px; width:50px; height:27px; }
.login_background{
	background-color:#f3f4f6; height:auto;
}
.form_font_color{
	color:#818182;
}

.invalid_color{
color:red;
}
.form_font_color a{
	color:#818182;
}

.button_sign_in{
	background-color:#F6881F; border:none;  width: 100px;height: 28px;
}
.sign_line{
background:url(img/line2.png) repeat-y; height:100px;
}
.login_head{
color:#F6881F;padding-left:0px;
}
.login_head2{
text-transform:capitalize; color:#3C3C3C;"
}

##errormsg{color:red; }
    </style>

</head>

<body>

    <!-- Navigation -->
    <style>
.header_icons{
}
.header_icons ul li{
	display:inline-block;
	list-style-type:none;
	margin-bottom:-10px;
	
}
</style>

  
      <div id="page_loader">  <div class="container hidden-xs" style="padding: 17px; " > 
            <img src="<?php echo base_url();?>img/page1.png" width="100%"  />
          
       </div>
        <div class="container visible-xs " style="text-align:center;">
           
           <img src="<?php echo base_url();?>img/page1-1.jpg" />
       </div>
  
  	 <div class="shade_border"></div>
 	
   
        <div class="container login_background" >
		  
        	<div class="col-lg-12 col-md-12"> 
			<?php if($this->session->flashdata('flash_message')) { ?>
			<h4 class="invalid_color"><?php echo $this->session->flashdata('flash_message');?></h4>
			<?php }else{ ?>
				<h4 class="form_font_color">Welcome, Please Sign In</h4>
			<?php }?>
			
			</div>
			
           		<div class="col-lg-4 col-md-4">
                	 <div class="form-group"><h4 class="login_head">LOGIN</h4></div>
                    <div class="form-group"> <h5 class="login_head2"><strong>Returning student</strong></h5></div>
                   <span class="form_font_color">I already have an account</span>
                    <form action="<?php echo base_url();?>user/account/login" class="form-group" id="input_container" method="post">
                    	<div class="form-group " >
						<div id="errormsg"></div>
                        	<input type="text" name="email" placeholder="Email" id="email" class="form-control form_font_color input" style="height:34px; width:240px" required/>
                            <img src="<?php echo base_url();?>img/email.png" id="input_img" class="hidden-xs">
                        </div>
                    	
                        <div class="form-group ">
                      <input type="password" name="password" placeholder="Password"  id="input3" class="form-control form_font_color" style="height:34px; width:240px" required/>
                            <img src="<?php echo base_url();?>img/pwd.png" id="input_img2"  class="hidden-xs">
                        </div>
                        <div class="form-group"><p class="form_font_color"><a href="#">Forgot Password ?</a></p></div>
                        <div class="clearfix"></div>
                       <div class="form-group">
            <button type="submit" class="button_sign_in" id="signin">SIGN IN</button>&nbsp;
                        	
                       </div>
                    </form>
                    
                </div>
                <div class="col-lg-4 col-md-4">
                	<h4 class="login_head">New Student</h4>
                   <div class="form-group form_font_color"> <h5 class="form_font_color"><strong>I would like to Sign up</strong></h5></div>
                   <p class="form_font_color">By creating an account at HELLOUNI you will be able to shop faster, be up to date on an orders status, and keep track of the orders you have previously made.</p>
                     <a href="<?php echo base_url();?>user/account/signup"><button type="button" class="button_sign_in">SIGN UP</button></a>
                </div>
                <div class="col-lg-1 hidden-xs hidden-sm hidden-md">
<div style="background:url(img/line2.png) repeat-y; height:100px;"></div><img src="<?php echo base_url();?>img/or.png" width="37"><div class="sign_line"></div>
                </div>
                <div class="col-lg-3 col-md-4">
                <div class="content-section" style="padding-top:50px;">
				<div id="results">
				
				</div>
               <div id="LoginButton"><a href="#" rel="nofollow" onClick="javascript:CallAfterLogin();return false;"><img src="<?php echo base_url();?>img/facebook-signup.png" /></a></div><div><img src="<?php echo base_url();?>img/google-signup.png" />
			   <link href="<?php echo base_url();?>css/buttonstyle.css" rel="stylesheet" type="text/css">
			   <?php /*
if(!$this->session->userdata('user'))
{
    echo '<div id="results">';
    echo '<!-- results will be placed here -->';
    echo '</div>';
    echo '<div id="LoginButton">';
	echo '<a href="#" rel="nofollow" class="fblogin-button" onClick="javascript:CallAfterLogin();return false;">Login with Facebook</a>';
    echo '</div>';

}
else
{
	//echo 'Hi '. $_SESSION['user_name'].'! You are Logged in to facebook, <a href="?logout=1">Log Out</a>.';
}*/
?>

			   <div id="fb-root"></div>
<script type="text/javascript">
window.fbAsyncInit = function() {
	FB.init({
		appId: '<?php echo $appId; ?>',
		cookie: true,xfbml: true,
		channelUrl: '<?php echo base_url();?>fbtest2/channel.php',
		oauth: true
		});
	};
(function() {
	var e = document.createElement('script');
	e.async = true;e.src = document.location.protocol +'//connect.facebook.net/en_US/all.js';
	document.getElementById('fb-root').appendChild(e);}());

function CallAfterLogin(){
	FB.login(function(response) {	
	
	//(JSON.stringify(response));
		
		if (response.status === "connected") 
		{  
		
		
			LodingAnimate(); //Animate login
			FB.api('/me?fields=name,email,first_name', function(data) {
			
			//alert(data.hometown);
			
			//alert(JSON.stringify(data));
				
			  if(data.email == null)
			  {
					//Facbeook user email is empty, you can check something like this.
					alert("You must allow us to access your email id!"); 
					ResetAnimate();

			  }else{
					AjaxResponse();
			  }
			  
		  });
		 }
	},
	{scope:'<?php echo $fbPermissions; ?>',return_scopes: true});
}


function test(){

FB.getLoginStatus(function(response) {
      if (response.status == 'connected') {
        getCurrentUserInfo(response)
      } else {
        FB.login(function(response) {
          if (response.authResponse){
            getCurrentUserInfo(response)
          } else {
            console.log('Auth cancelled.')
          }
        }, { scope: 'email' });
      }
    });

    function getCurrentUserInfo() {
      FB.api('/me', function(userInfo) {
        console.log(userInfo.name + ': ' + userInfo.email);
      });
    }
}


function getCurrentUserInfo() {
      FB.api('/me', function(userInfo) {
        console.log(userInfo.name + ': ' + userInfo.email);
      });
    }


//functions
function AjaxResponse()
{
	 //Load data from the server and place the returned HTML into the matched element using jQuery Load().
	 //$( "#results" ).load( "fbtest2/process_facebook.php" );
	 
	 //window.location.href="http://inventifweb.net/UNI/fbtest2/process.php";
	 
	 
		 
	 $.ajax({

			type: "POST",
			
			url: "http://inventifweb.net/UNI/fbtest2/process.php",

			//data: jsonString, 

			cache: false,

			success: function(str){
			
			
			
			var obj = jQuery.parseJSON(str);
            //alert(obj.image);
			
			
			var dataString = 'name=' + obj.name + '&email=' + obj.email + '&image=' +obj.image + '&id=' +obj.id  ;
			
			// alert(dataString);
			 
			      $.ajax({

						type: "POST",
						
						url: "http://inventifweb.net/UNI/user/account/fblogin",
			
						data: dataString, 
			
						cache: false,
			
						success: function(re){
						
						 //$('#myModal').html(result);
						 // $('#myModal').show("AA");
						 //alert(re);
						 if(re==1){
						 
						 window.location.reload();
						 }
							
							
					   // $('#newserrormessage').text(result);
							   
						
						}
			
					  });	
				
				
          
			       
			
			}

          });	
	
	 
	 
	 
	 
}

//Show loading Image
function LodingAnimate() 
{
	$("#LoginButton").hide(); //hide login button once user authorize the application
	$("#results").html('<img src="<?php echo base_url();?>img/ajax-loader.gif" style="width: 30%; margin-left: 74px;" /> <br><span style="color:red;>"Please Wait Connecting...</span>'); //show loading image while we process user
}

//Reset User button
function ResetAnimate() 
{
	$("#LoginButton").show(); //Show login button 
	$("#results").html(''); //reset element html
}




$( document ).ready(function() {
   $("#signin").click(function(){
    var email=$("#email").val();
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	
	if(!emailReg.test(email)){
            
			$('#errormsg').text('Please enter a valid email address!');
			//$('#email').val('');
			 $('#email').focus();
			event.stopPropagation();
			return false;
        }
});
});
</script>
			   
			   
			   
			   </div>
                </div>
                </div>
           
         
     </div>	
   </div>
   <br/>

