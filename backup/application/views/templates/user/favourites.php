 <style type="text/css">
   	 .shade_border{
		background:url(<?php echo base_url();?>img/shade1.png) repeat-x; height:11px;
	}
	.search_by{
	background-color:#f3f4f4; box-shadow:5px 5px 5px #eae8e8; height:auto;
	    padding-top: 10px;
    padding-left: 30px;
	 padding-right: 30px;
	}
	.search_by h4{
		color:#3c3c3c;
		font-family:lato_regular;
		font-weight:500;
	}
	.first{
		border-left:1px solid #eaeaeb;
		border-top:1px solid #eaeaeb;
		border-bottom:1px solid #eaeaeb;
		text-align:center;
		background:#fbfbfb;
		/*padding: 30px 15px !important;*/
		
	}
	.last{
		border-right:1px solid #eaeaeb;
		border-top:1px solid #eaeaeb;
		border-bottom:1px solid #eaeaeb;
		text-align:center;
		background:#fbfbfb;
		padding: 30px 15px !important;
		
	}
	.element{
		border-top:1px solid #eaeaeb;
		border-bottom:1px solid #eaeaeb;
		text-align:center;
		background:#fbfbfb;
		padding: 30px 15px !important;
	}
	#table_heading{
		font-weight:bold;
		padding: 12px 9px 10px 9px;
		text-align: center;
		border-bottom: 1px solid #eaeaeb;
		border-top:1px solid #eaeaeb;
	}
	#table_heading-first{
		 width:169px;
		 font-weight:bold;
		 border-left:1px solid #eaeaeb;
		 border-bottom:1px solid #eaeaeb;
		 border-top:1px solid #eaeaeb; 
		 text-align:center; 
		 background:#fbfbfb;
		 padding: 12px 15px 10px 15px;
	}
	#table_heading-last{
		border-right:1px solid #eaeaeb;
		 border-bottom:1px solid #eaeaeb;
		 border-top:1px solid #eaeaeb; 
		 text-align:center; 
		 background:#fbfbfb;
		 padding: 12px 15px 10px 15px;
		 width:126px;
	}
	.more_detail{
	background-color:#F6881F;
	text-transform:uppercase;
	height:100%;	
	border:0px;
	  color:#fff; 
		border-radius:2px;
		
}
 .menutables {
 border-collapse: separate;
 }

    
  tr .menutables {
	  margin-top:10px;  
  }
  
  .delimg{
  cursor:pointer;
  }
  
  .bkbtn{ background-color: red; width: 9%; height: 35px; border-color: red;} 
  .bkbtnDiv{text-align: right; height: 50px;}
  
   </style>
	
<div class="clearfix"></div>
<div id="page_loader">  
<div class="shade_border"></div>
<div class="container hidden-xs " style="padding: 17px; " > 
            <img src="<?php echo base_url();?>img/page1.png" width="100%"  />
          
       </div>
        <div class="container visible-xs" style="text-align:center;">
           
           <img src="<?php echo base_url();?>img/page1-1.jpg" />
       </div>
  	 <div class="shade_border"></div>
 	
   
        <div class="container">
        	
             <div class="clearfix visible-xs visible-sm "></div>
             <br class="visible-xs visible-sm "/>
             <div class="col-md-12 col-sm-12">
             	<div class="table-responsive">  
                <form name="myform" action="checkboxes.asp" method="post">        
  <table class="table menutables" width="100%" cellspacing="10">
    <thead>
      <tr>
        <td style="border:1px solid #eaeaeb; text-align:center; padding: 10px 15px 10px 15px;" valign="middle" width="48">
       <input type="checkbox" name="Check_ctr" value="yes" onClick="Check(this)"></td>
        <td class="table_heading" id="table_heading-first">University</td>
        <td class="table_heading" id="table_heading">University Name</td>
        <td class="table_heading" id="table_heading">Country</td>
        <td class="table_heading" id="table_heading">Area Of Interest</td>
        <td  class="table_heading" id="table_heading">Level Of Course</td>
        <td class="table_heading" id="table_heading"></td>
        <td class="table_heading" id="table_heading-last"></td>
      </tr>
    </thead>
    <tr>
    <td colspan="8">&nbsp;</td>
    </tr>
	
	 <?php if($favourites){ 
	 foreach($favourites as $fav){?>
     <tr>
	   <td class="table_heading" style="border:1px solid #eaeaeb; text-align:center; padding:40px 15px; "valign="middle" width="48">
       <input type="checkbox" name="check_list" value="<?php echo $fav->id;?>"></td>
        <td class="table_heading first" valign="middle"><img src="<?php echo base_url();?>img/university_logo.png" width="115"/></td>
        <td class="table_heading element" width="129"><?php echo $fav->name;?></td>
        <td class="table_heading element" width="88"><?php echo $fav->countryname;?></td>
        <td class="table_heading element" width="137"><?php echo $fav->coursename;?></td>
        <td  class="table_heading element" width="131"><?php echo $fav->degreename;?></td>
        <td class="table_heading element" width="137"><img onclick="deletefav(<?php echo $fav->id;?>)" class="img-responsive delimg" src="<?php echo base_url();?>img/delete.png"></td>
		
        <td class="table_heading last"><a href="#"><img class="img-responsive" src="<?php echo base_url();?>img/book.png"></a></td>
      </tr>
    
	
	<?php } }else{?>
	
	<tr>
	   <td class="table_heading" style="border:1px solid #eaeaeb; text-align:center; padding:40px 15px; "valign="middle" width="48" colspan="8">
	   There is no university in the favourites listing
       </td>
      </tr>
   <?php } ?>
   
   <tr><td colspan="8">&nbsp;</td></tr>
   
  </table></form>
  <div class="clearfix"></div>
  <div class="bkbtnDiv"><a href="<?php echo base_url();?>search/university"><input type="button" class="bkbtn" value="Back To Listing"/></a></div>
  <div class="clearfix"></div>
  <!-- <table class="table" width="100%">
   
      <tr>
        <td class="table_heading" style="border:1px solid #eaeaeb; text-align:center; padding:40px 15px; "valign="middle" width="48">
        <input type="checkbox" name="chk_box" /></td>
        <td class="table_heading" style="border-left:1px solid #eaeaeb; border-top:1px solid #eaeaeb; border-bottom:1px solid #eaeaeb; text-align:center; background:#fbfbfb; padding: 30px 15px;" valign="middle"><img src="img/university_logo.png" width="115"/></td>
        <td class="table_heading" style="border-top:1px solid #eaeaeb; border-bottom:1px solid #eaeaeb; text-align:center; background:#fbfbfb;    padding:30px 9px;" width="129">Nanyan Acadmey of Fine Arts</td>
        <td class="table_heading" style="border-top:1px solid #eaeaeb; border-bottom:1px solid #eaeaeb; text-align:center; background:#fbfbfb;     padding:30px 15px;" width="88">Singapore</td>
        <td class="table_heading" style="border-top:1px solid #eaeaeb; border-bottom:1px solid #eaeaeb; text-align:center; background:#fbfbfb;   padding:30px 15px;" width="137">Engineering</td>
        <td  class="table_heading" style=" border-bottom:1px solid #eaeaeb;border-top:1px solid #eaeaeb; text-align:center; background:#fbfbfb;   padding:30px 15px;" width="131">Diploma</td>
        <td class="table_heading" style="border-right:1px solid #eaeaeb; border-bottom:1px solid #eaeaeb;border-top:1px solid #eaeaeb; text-align:center; background:#fbfbfb;  padding:30px 15px;"><button type="button" class="more_detail">More >></button></td>
      </tr>
  
   
  </table>
 <table class="table" width="100%">
   
      <tr>
        <td class="table_heading" style="border:1px solid #eaeaeb; text-align:center; padding:40px 20px; " width="126" valign="middle">
        <input type="checkbox" name="chk_box" /></td>
        <td class="table_heading" style="border-left:1px solid #eaeaeb; border-top:1px solid #eaeaeb; border-bottom:1px solid #eaeaeb; text-align:center; background:#fbfbfb; padding: 30px 15px;" valign="middle" width="142"><img src="img/university_logo.png"/></td>
        <td class="table_heading" style="border-top:1px solid #eaeaeb; border-bottom:1px solid #eaeaeb; text-align:center; background:#fbfbfb;    padding:30px 9px;" width="129">Nanyan Acadmey of Fine Arts</td>
        <td class="table_heading" style="border-top:1px solid #eaeaeb; border-bottom:1px solid #eaeaeb; text-align:center; background:#fbfbfb;     padding:30px 15px;" width="88">Singapore</td>
        <td class="table_heading" style="border-top:1px solid #eaeaeb; border-bottom:1px solid #eaeaeb; text-align:center; background:#fbfbfb;   padding:30px 15px;" width="137">Engineering</td>
        <td  class="table_heading" style=" border-bottom:1px solid #eaeaeb;border-top:1px solid #eaeaeb; text-align:center; background:#fbfbfb;   padding:30px 15px;" width="134">Diploma</td>
        <td class="table_heading" style="border-right:1px solid #eaeaeb; border-bottom:1px solid #eaeaeb;border-top:1px solid #eaeaeb; text-align:center; background:#fbfbfb;  padding:30px 15px;" width="36"><button type="button" class="more_detail">>></button></td>
      </tr>
  
   
  </table>-->
  </div>
             
             
             </div>
         </div>
    
   </div>
<!--<?php echo base_url();?>user/favourites/delete/<?php echo $fav->id;?>-->   
<script type="text/javascript">
function deletefav(id){
if(confirm("Do you really want to delete this favourite")==true){
   window.location.href='<?php echo base_url();?>user/favourites/delete/'+id;
  }
}
</script> 
   
   
   