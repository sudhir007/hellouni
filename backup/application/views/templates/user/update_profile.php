<?php $leaddata=$this->session->userdata('leaduser'); ?>
   
    <style>
   	 .shade_border{
		background:url(img/shade1.png) repeat-x; height:11px;
	}
	.search_by{
	background-color:#f3f4f4; box-shadow:5px 5px 5px #eae8e8; height:auto;
	    padding-top: 10px;
    padding-left: 30px;
	 padding-right: 30px;
	}
	.search_by h4{
		color:#3c3c3c;
		font-family:lato_regular;
		font-weight:500;
	}
	.table_heading{
		padding:5px;
		font-weight:bold;
	}
	.more_detail{
	background-color:#F6881F;
	text-transform:uppercase;
	height:28px;	
	border:0px;
	  color:#000; 
		border-radius:2px;
		width:25px;
}
.right_side_form{
background:#3c3c3c;
box-shadow:5px 5px 2px #CCCCCC; 
height:auto;

}
label{
	color:#818182;
	font-size:11px;
font-weight:300;	
margin-top:5px;
}
.input_field{
	
width:215px;
height:25px;	
}
.more_detail{
	background-color:#F6881F;
	text-transform:uppercase;
	height:28px;	
	border:0px;
	  color:#fff; 
		border-radius:2px;
		
}

#input_container { position:relative; padding:0; margin:0;     padding-top: 25px;
    }
#input { margin:0; padding-left: 62px; }

#input_img { position:absolute; bottom:150px; left:15px; width:50px; height:27px; margin-left:-5px;}

#input2 { margin:0; padding-left: 62px; }

#input_img2 { position:absolute; bottom:100px; left:15px; width:50px; height:27px; }
#input3 { margin:0; padding-left: 62px; }

#input_img3 { position:absolute; bottom:163px; left:15px; width:50px; height:27px; }
.login_background{
	background-color:#f3f4f6; height:auto;
}
.form_font_color{
	color:#818182;
}
.form_font_color a{
	color:#818182;
}

.button_sign_in{
	background-color:#F6881F; border:none;  width: 100px;height: 28px;
	margin-bottom:20px;
}
.sign_line{
background:url(img/line2.png) repeat-y; height:100px;
}
.login_head{
color:#F6881F;padding-left:0px;
}
.login_head2{
text-transform:capitalize; color:#3C3C3C;"
}
    </style>

</head>

<body>

    <!-- Navigation -->
    <style>
.header_icons{
}
.header_icons ul li{
	display:inline-block;
	list-style-type:none;
	margin-bottom:-10px;
	
}
.sep{
    margin-top: 20px;
    margin-bottom: 20px;
   
    
}
.invalid_color{
color:red;
}
</style>

<style type="text/css">
#12thidd{display:none;}
#diploma_id{display:none;}
#masters_id{display:none;}
#locationcountries{widows:50%;}
#state{width:50%;}
#city{width:50%;}
</style>

  
      <div id="page_loader">  <div class="container hidden-xs" style="padding: 17px; " > 
            <img src="<?php echo base_url();?>img/page1.png" width="100%"  />
          
       </div>
        <div class="container visible-xs " style="text-align:center;">
           
           <img src="img/page1-1.jpg" />
       </div>
  
  	 <div class="shade_border"></div>
 	
   
        <div class="container login_background" >
        	<div class="col-lg-12 col-md-12"> <h4 class="form_font_color">Update Your Profile </h4></div>
           		<div class="col-lg-12 col-md-12">
				<?php if($this->session->flashdata('flash_message')) { ?>
				<h6 class="invalid_color"><?php echo $this->session->flashdata('flash_message');?></h6>
				<?php }?>
				
                	 <h4 class="login_head">Personal Details</h4>
                    
                  
                    <form action="<?php echo base_url()?>user/account/save" class="form-group form-inline" id="input_container" method="post"  >
                    	<div class="form-group col-md-6 " >
                        	<label class="col-md-3">Name</label>
							<?php if($fid){?>
                        	<span class="form-control form_font_color input_field"><?php if($name) echo $name;?></span>
                           <?php }else{?>
						   <input type="text" name="name" placeholder=""  class="form-control form_font_color input_field" value="<?php if($name) echo $name;?>" />
						   <?php } ?>
                        </div>
                    	
                        <div class="form-group col-md-6">
                        <label class="col-md-3">DOB</label>
                      <input type="text" name="dob"  class="form-control form_font_color input_field" value="<?php if($dob) echo $dob;?>"/>
                      </div>
                      <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span>  
                      <div class="form-group col-md-6 " >
                        	<label class="col-md-3">Email</label>
							<span class="form-control form_font_color"><?php if($email) echo $email;?></span> 
                        </div>
                    	
                        <div class="form-group col-md-6">
                        <label class="col-md-3">Phone No.</label>
                      <input type="text" name="phone"    class="form-control form_font_color input_field" value="<?php if($phone) echo $phone;?>"/>
                      </div>
                       <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span>  
                      <div class="form-group col-md-12 " >
                        	<label class="col-md-3">Address</label>
                   <textarea type="text" name="address"   class="form-control form_font_color input_field " style="width:60%; height:100px;"  ><?php if($address) echo $address;?></textarea>
                           
                        </div>
                    	 
						 <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span>
                        
					<div class="form-group col-md-6">                      
					   <label class="col-md-3" >Country</label>
                    
					  <select class="form-control" id="locationcountries" name="locationcountries">
								<?php foreach($locationcountries as $location){?>
									<option value="<?php echo $location->id;?>" <?php  if($location->id==$selected_country) echo 'selected="selected"'; ?> ><?php echo $location->name;?></option>
								<?php }?>
                  			</select>	
					 
                      </div>
                         
					   <div class="form-group col-lg-6 ">
							<label class="col-md-3">State</label>
							
							<?php if (!$selected_state){?>
							
							<select class="form-control" id="state" name="state"></select>	
							
							<?php }else{?>
							
							<select class="form-control" id="state" name="state">
								<?php foreach($related_state_list as $location){?>
									<option value="<?php echo $location->zone_id;?>" <?php  if($location->zone_id==$selected_state) echo 'selected="selected"'; ?> ><?php echo $location->name;?></option>
								<?php }?>
                  			</select>	
							
							<?php } ?>
							
							
							 
                       </div>                      
                     <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span> 				 
					 
					 
					  <div class="form-group col-md-6 " >
                        	<label class="col-md-3">City</label>
							<?php if (!$city){?>
                        	 <select class="form-control" id="city" name="city"></select>
							<?php }else{?>
							
							<select class="form-control" id="city" name="city">
								<?php foreach($related_city_list as $location){?>
							<option value="<?php echo $location->id;?>" <?php  if($location->id==$city) echo 'selected="selected"'; ?> ><?php echo $location->city_name;?></option>
								<?php }?>
                  			</select>
								
							<?php }?>
							 
                     </div>
					 
					  <div class="form-group col-md-6">
                        <label class="col-md-3">Zipcode</label>
                      <input type="text" name="zip"    class="form-control form_font_color input_field" value="<?php if($zip) echo $zip;?>"/>
                      </div>
					 
					 
                         <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span> 
						
					  <div class="form-group col-lg-6" >
                        	<label class="col-md-3">Parent Name</label>
                        	<input type="text" name="parent_name" id="parent_name" class="form-control form_font_color input_field" value="<?php if($parent_name) echo $parent_name;?>" />                           
                      </div>
					  <div class="form-group col-lg-6" >
                        	<label class="col-md-3">Parent Occupation </label>
                        	<input type="text" name="parent_occupation" id="parent_occupation" class="form-control form_font_color input_field" value="<?php if($parent_occupation) echo $parent_occupation;?>" />                           
                      </div>
					  
					  <div class="form-group col-lg-6" >
                        	<label class="col-md-3">Parent Mobile</label>
                        	<input type="text" name="parent_mob" id="parent_mob" class="form-control form_font_color input_field" value="<?php if($parent_mob) echo $parent_mob;?>" />                           
                      </div>
					  <div class="form-group col-lg-6" >
                        	<label class="col-md-3">Where did you hear about us from </label>
                        	<select class="form-control" name="hear_info" id="hear_info" required="">
								<option value="0">select option</option>
								<option value="Friends / Family" <?php if(isset($hear_about_us) && $hear_about_us == 'Friends / Family') echo 'selected="selected"';?> >Friends / Family</option>
								<option value="Google / Facebook" <?php if(isset($hear_about_us) && $hear_about_us == 'Google / Facebook') echo 'selected="selected"';?>>Google / Facebook</option>
								<option value="Stupidsid Seminar" <?php if(isset($hear_about_us) && $hear_about_us == 'Stupidsid Seminar') echo 'selected="selected"';?>>Stupidsid Seminar</option>
								<option value="Stupidsid Website" <?php if(isset($hear_about_us) && $hear_about_us == 'Stupidsid Website') echo 'selected="selected"';?>>Stupidsid Website</option>
								<option value="College Seminar" <?php if(isset($hear_about_us) && $hear_about_us == 'College Seminar') echo 'selected="selected"';?>>College Seminar</option>
								<option value="News Paper" <?php if(isset($hear_about_us) && $hear_about_us == 'News Paper') echo 'selected="selected"';?>>News Paper</option>
								<option value="Calling from Imperial" <?php if(isset($hear_about_us) && $hear_about_us == 'Calling from Imperial') echo 'selected="selected"';?>>Calling from Imperial</option>
							</select>                           
                      </div>
					  
					  <div class="form-group col-lg-12" >
                        	<label class="col-md-4">Is your passport available? </label>                        	
								<label>Yes</label> <input name="passport_availalibility" id="passport_availalibility_yes" value="1" type="radio" <?php if(isset($passport) && $passport == 1) echo 'checked="checked"';?> >
								<label>No</label> <input name="passport_availalibility" id="passport_availalibility_no" value="0" type="radio" <?php if(isset($passport) && $passport == 0) echo 'checked="checked"';?>>					                         
                      </div>
					  <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span> 
					  
					  
					  <div class="form-group col-lg-12" >
                        	<label class="col-md-3">Desired Destination </label>  
							<?php 
								$destination = array();
								$destination = explode(",",$desired_destination);
							?>                      	
							<?php foreach($countries as $val){?>
							<input type="checkbox" class="deisred_destinations" name="deisred_destinations[]" value="<?php echo $val->id;?>" <?php if(isset($destination) && in_array($val->id, $destination)) echo 'checked="checked"';?>/>&nbsp;
							<label><?php echo $val->name;?></label>&nbsp;&nbsp;&nbsp;&nbsp;
                            <?php }?>					                         
                      </div>
					  
					  <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span> 
					  
					  <div class="clearfix"></div>					  
					   <div class="form-group col-lg-12" >
					    <h4 class="login_head">Qualification Details</h4>
                        	<label class="col-md-3">Desired Courses </label>                        	
							<table style="width: 100%;">
							
							<th><label style="font-weight: 600;">Desired Course</label></th>
							<th><label style="font-weight: 600;">Level</label></th>
							<th><label style="font-weight: 600;">Desired intake in Months</label></th>
							<th><label style="font-weight: 600;">Year of Admission</label></th>
							
							<tbody>
							<tr>
							<td><input type="text" name="desired_course_name" id="desired_course_name" class="form-control form_font_color input_field" value="<?php if($desiredcourse_details !='') echo $desiredcourse_details->desired_course_name;?>" /></td>
							<td>
							<select class="form-control" id="desired_coures_level" name="desired_coures_level">
								<option value="1" <?php if($desiredcourse_details !='' && $desiredcourse_details->desired_course_level == 1) echo 'selected="selected"';?>>Graduate/Diploma</option>
								<option value="2" <?php if($desiredcourse_details !='' && $desiredcourse_details->desired_course_level == 2) echo 'selected="selected"';?>>Post Graduation/Masters</option>
								<option value="3" <?php if($desiredcourse_details !='' && $desiredcourse_details->desired_course_level == 3) echo 'selected="selected"';?>>Professional Program</option>
								<option value="4" <?php if($desiredcourse_details !='' && $desiredcourse_details->desired_course_level == 4) echo 'selected="selected"';?>>Doctorate</option>
                  			</select>							
							</td>

							<td>
							<select class="form-control" id="desired_course_intake" name="desired_course_intake">
							<?php foreach($intakes as $intk){?>
									<option value="<?php echo $intk->id;?>" <?php if($desiredcourse_details !='' && $desiredcourse_details->desired_course_intake == $intk->id) echo 'selected="selected"';?>><?php echo $intk->name;?></option>
								<?php }?>
                  			</select>							
							</td>
							<td>
							<select class="form-control" id="desired_coures_year" name="desired_coures_year">
								<option value="2016" <?php if($desiredcourse_details !='' && $desiredcourse_details->desired_course_year == 2016) echo 'selected="selected"';?>>2016</option>
								<option value="2017" <?php if($desiredcourse_details !='' && $desiredcourse_details->desired_course_year == 2017) echo 'selected="selected"';?>>2017</option>
								<option value="2018" <?php if($desiredcourse_details !='' && $desiredcourse_details->desired_course_year == 2018) echo 'selected="selected"';?>>2018</option>
								<option value="2019" <?php if($desiredcourse_details !='' && $desiredcourse_details->desired_course_year == 2019) echo 'selected="selected"';?>>2019</option>
                  			</select>							
							</td>
							</tr>
							
							</tbody>
							
							</table>				                         
                      </div>
					  
					  <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span> 					  
					  <div class="clearfix"></div>					  
					  
					  <div class="form-group col-lg-12" >
                        	<label class="col-md-3">Secondary Qualification </label>                        	
							<table style="width: 100%;">
							
							<th><label style="font-weight: 600;">Qualification</label></th>
							<th><label style="font-weight: 600;">Institution Name</label></th>
							<th><label style="font-weight: 600;">Board</label></th>
							<th><label style="font-weight: 600;">Year Started</label></th>
							<th><label style="font-weight: 600;">Year Finished</label></th>
							<th><label style="font-weight: 600;">Grade/ %</label></th>
							<th><label style="font-weight: 600;">Out Of Total</label></th>
							
							<tbody>
							<tr>
							<td><label>Grade X</label></</td>
							<td><input type="text" name="secondary_institution_name" id="secondary_institution_name" class="form-control form_font_color input_field" value="<?php if($secondaryqualification_details !='') echo $secondaryqualification_details->institution_name; ?>"/></td>
							<td><input type="text" name="board" id="board" class="form-control form_font_color input_field" value="<?php if($secondaryqualification_details !='') echo $secondaryqualification_details->board; ?>"/></td>
							<td>
							<select class="form-control" id="secondary_year_starrted" name="secondary_year_starrted">
								<?php for($i=1980;$i<=2030;$i++){?>
									<option value="<?php echo $i;?>" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->year_started == $i) echo 'selected="selected"';?> ><?php echo $i;?></option>
								<?php }?>
                  			</select>							
							</td>
							<td>
							<select class="form-control" id="secondary_year_finished" name="secondary_year_finished">
								<?php for($i=1980;$i<=2030;$i++){?>
									<option value="<?php echo $i;?>" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->year_finished == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
								<?php }?>
                  			</select>							
							</td>
							<td><input type="text" name="secondary_grade" id="secondary_grade" class="form-control form_font_color input_field" value="<?php if($secondaryqualification_details !='') echo $secondaryqualification_details->grade; ?>"/></td>
							<td>
							<select class="form-control" id="secondary_total" name="secondary_total">
								<option value="100" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 100) echo 'selected="selected"';?>>100</option>
								<option value="10"  <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 10) echo 'selected="selected"';?>>10</option>
								<option value="7"  <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 7) echo 'selected="selected"';?>>7</option>
								<option value="4"  <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 4) echo 'selected="selected"';?>>4</option>
                  			</select>							
							</td>
							</tr>
							
							</tbody>
							
							</table>				                         
                      </div>
					  
					  <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span> 
					  
					  <div class="form-group col-lg-12" >
                        	<label class="col-md-4">Have you done Diploma or 12th After School? </label>                        	
								<label>Diploma</label> <input name="diploma_12" class="diploma_12" id="diploma" value="1" type="radio" <?php if($diplomaqualification_details !='') echo 'checked="checked"';?> >
								<label>12TH</label> <input name="diploma_12" class="diploma_12" id="12th" value="2" type="radio" <?php if($highersecondaryqualification_details !='') echo 'checked="checked"';?>>					                         
                      </div>
					  <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span> 
					  
					  <div class="form-group col-lg-12 diploma_hs" id="12thidd" <?php if($highersecondaryqualification_details !=''){?> style="display:block;"<?php }else{?>style="display:none;"<?php }?>>
                        	<label class="col-md-3">Higher Secondary Qualification </label>                        	
							<table style="width: 100%;">
							
							<th><label style="font-weight: 600;">Qualification</label></th>
							<th><label style="font-weight: 600;">Institution Name</label></th>
							<th><label style="font-weight: 600;">Board</label></th>
							<th><label style="font-weight: 600;">Year Started</label></th>
							<th><label style="font-weight: 600;">Year Finished</label></th>
							<th><label style="font-weight: 600;">Grade/ %</label></th>
							<th><label style="font-weight: 600;">Out Of Total</label></th>
							
							<tbody>
							<tr>
							<td><label>Grade XII</label></</td>
							<td><input type="text" name="hs_institution_name" id="hs_institution_name" class="form-control form_font_color input_field" value="<?php if($highersecondaryqualification_details !='') echo $highersecondaryqualification_details->institution_name; ?>"/></td>
							<td><input type="text" name="hs_board" id="hs_board" class="form-control form_font_color input_field" value="<?php if($highersecondaryqualification_details !='') echo $highersecondaryqualification_details->board; ?>"/></td>
							<td>
							<select class="form-control" id="hs_year_starrted" name="hs_year_starrted">
								<?php for($i=1980;$i<=2030;$i++){?>
									<option value="<?php echo $i;?>" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->year_started == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
								<?php }?>
                  			</select>							
							</td>
							<td>
							<select class="form-control" id="hs_year_finished" name="hs_year_finished">
								<?php for($i=1980;$i<=2030;$i++){?>
									<option value="<?php echo $i;?>" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->year_finished == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
								<?php }?>
                  			</select>							
							</td>
							<td><input type="text" name="hs_grade" id="hs_grade" class="form-control form_font_color input_field" value="<?php if($highersecondaryqualification_details !='') echo $highersecondaryqualification_details->grade; ?>"/></td>
							<td>
							<select class="form-control" id="hs_total" name="hs_total">
								<option value="100" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 100) echo 'selected="selected"';?>>100</option>
								<option value="10" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 10) echo 'selected="selected"';?>>10</option>
								<option value="7" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 7) echo 'selected="selected"';?>>7</option>
								<option value="4" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 4) echo 'selected="selected"';?>>4</option>
                  			</select>							
							</td>
							</tr>
							
							</tbody>
							
							</table>				                         
                      </div>
					  
					   <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span> 
					  
					  
					  <div class="form-group col-lg-12 diploma_hs" id="diploma_id" <?php if($diplomaqualification_details !=''){?> style="display:block;"<?php }else{?>style="display:none;"<?php }?> >
                        	<label class="col-md-3">Diploma </label>                        	
							<table style="width: 100%;">
							
							<th><label style="font-weight: 600;">Name of Course</label></th>
							<th><label style="font-weight: 600;">College  Name</label></th>							
							<th><label style="font-weight: 600;">Year Started</label></th>
							<th><label style="font-weight: 600;">Year Finished</label></th>
							<th><label style="font-weight: 600;">Aggregate Percentage (1 to 6 Sem)</label></th>
							<th><label style="font-weight: 600;">Out Of Total</label></th>
							
							<tbody>
							<tr>
							<td><input type="text" name="diploma_course_name" id="diploma_course_name" class="form-control form_font_color input_field" value="<?php if($diplomaqualification_details !='') echo $diplomaqualification_details->course; ?>"/></</td>
							<td><input type="text" name="diploma_institution_name" id="diploma_institution_name" class="form-control form_font_color input_field" value="<?php if($diplomaqualification_details !='') echo $diplomaqualification_details->college; ?>"/></td>							
							<td>
							<select class="form-control" id="diploma_year_starrted" name="diploma_year_starrted">
								<?php for($i=1980;$i<=2030;$i++){?>
									<option value="<?php echo $i;?>" <?php if($diplomaqualification_details !='' && $diplomaqualification_details->year_started == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
								<?php }?>
                  			</select>							
							</td>
							<td>
							<select class="form-control" id="diploma_year_finished" name="diploma_year_finished">
								<?php for($i=1980;$i<=2030;$i++){?>
									<option value="<?php echo $i;?>" <?php if($diplomaqualification_details !='' && $diplomaqualification_details->year_finished == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
								<?php }?>
                  			</select>							
							</td>
							<td><input type="text" name="diploma_grade" id="diploma_grade" class="form-control form_font_color input_field" value="<?php if($diplomaqualification_details !='') echo $diplomaqualification_details->aggregate; ?>"/></td>
							<td>
							<select class="form-control" id="diploma_total" name="diploma_total">
								<option value="100" <?php if($diplomaqualification_details !='' && $diplomaqualification_details->total == 100) echo 'selected="selected"';?>>100</option>
								<option value="10" <?php if($diplomaqualification_details !='' && $diplomaqualification_details->total == 10) echo 'selected="selected"';?>>10</option>
								<option value="7" <?php if($diplomaqualification_details !='' && $diplomaqualification_details->total == 7) echo 'selected="selected"';?>>7</option>
								<option value="4" <?php if($diplomaqualification_details !='' && $diplomaqualification_details->total == 10) echo 'selected="selected"';?>>4</option>
                  			</select>							
							</td>
							</tr>
							
							</tbody>							
							</table>				                         
                      </div>
					  
					  <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span> 
					  
					  
					  <div class="form-group col-lg-12" id="bachelor_id" >
                        	<label class="col-md-3">Bachelor's/Graduation </label>                        	
							<table style="width: 100%;">
							
							<th><label style="font-weight: 600;">Name of Course</label></th>
							<th><label style="font-weight: 600;">University</label></th>	
							<th><label style="font-weight: 600;">College  Name</label></th>	
							<th><label style="font-weight: 600;">Major</label></th>														
							<th><label style="font-weight: 600;">Year Started</label></th>
							<th><label style="font-weight: 600;">Year Finished</label></th>
							<th><label style="font-weight: 600;">Grade / %</label></th>
							<th><label style="font-weight: 600;">Out Of Total</label></th>
							
							<tbody>
							<tr>
							
							<td><input type="text" name="bachelor_course" id="bachelor_course" class="form-control form_font_color input_field" value="<?php if($bachelorqualification_details !='') echo $bachelorqualification_details->course; ?>"/></td>
							<td><input type="text" name="bachelor_university" id="bachelor_university" class="form-control form_font_color input_field" value="<?php if($bachelorqualification_details !='') echo $bachelorqualification_details->university; ?>"/></td>
							<td><input type="text" name="bachelor_college" id="bachelor_college" class="form-control form_font_color input_field" value="<?php if($bachelorqualification_details !='') echo $bachelorqualification_details->college; ?>"/></td>
							<td><input type="text" name="bachelor_major" id="bachelor_major" class="form-control form_font_color input_field" value="<?php if($bachelorqualification_details !='') echo $bachelorqualification_details->major; ?>"/></td>
							<td>
							<select class="form-control" id="bachelor_year_starrted" name="bachelor_year_starrted">
								<?php for($i=1980;$i<=2030;$i++){?>
									<option value="<?php echo $i;?>" <?php if($bachelorqualification_details !='' && $bachelorqualification_details->year_started == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
								<?php }?>
                  			</select>							
							</td>
							<td>
							<select class="form-control" id="bachelor_year_finished" name="bachelor_year_finished">
								<?php for($i=1980;$i<=2030;$i++){?>
									<option value="<?php echo $i;?>" <?php if($bachelorqualification_details !='' && $bachelorqualification_details->year_finished == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
								<?php }?>
                  			</select>							
							</td>
							<td><input type="text" name="bachelor_grade" id="bachelor_grade" class="form-control form_font_color input_field" value="<?php if($bachelorqualification_details !='') echo $bachelorqualification_details->grade;?>"/></td>
							<td>
							<select class="form-control" id="bachelor_total" name="bachelor_total">
								<option value="100" <?php if($bachelorqualification_details !='' && $bachelorqualification_details->total == 100) echo 'selected="selected"';?>>100</option>
								<option value="10" <?php if($bachelorqualification_details !='' && $bachelorqualification_details->total == 10) echo 'selected="selected"';?>>10</option>
								<option value="7" <?php if($bachelorqualification_details !='' && $bachelorqualification_details->total == 7) echo 'selected="selected"';?>>7</option>
								<option value="4" <?php if($bachelorqualification_details !='' && $bachelorqualification_details->total == 4) echo 'selected="selected"';?>>4</option>
                  			</select>							
							</td>
							</tr>
							
							</tbody>
							
							</table>				                         
                      </div>				  
					  
					  
					  <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span> 
					  
					  
					   <div class="form-group col-lg-12" >
                        	<label class="col-md-4">Have you done Master? </label>                        	
								<label>Yes</label> <input name="masters" id="master_yes" value="1" type="radio" <?php if($mastersqualification_details !='') echo 'checked="checked"';?>>
								<label>No</label> <input name="masters" id="master_no" value="2" type="radio" <?php if($mastersqualification_details =='') echo 'checked="checked"';?>>					                         
                      </div>
					  <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span> 
					  
					   <div class="form-group col-lg-12" id="masters_id" <?php if($mastersqualification_details !=''){?> style="display:block;"<?php }else{?>style="display:none;"<?php }?>>
                        	<label class="col-md-3">Master's</label>                        	
							<table style="width: 100%;">
							
							<th><label style="font-weight: 600;">Name of Course</label></th>
							<th><label style="font-weight: 600;">University</label></th>	
							<th><label style="font-weight: 600;">College  Name</label></th>	
							<th><label style="font-weight: 600;">Major</label></th>														
							<th><label style="font-weight: 600;">Year Started</label></th>
							<th><label style="font-weight: 600;">Year Finished</label></th>
							<th><label style="font-weight: 600;">Grade / %</label></th>
							<th><label style="font-weight: 600;">Out Of Total</label></th>
							
							<tbody>
							<tr>
							
							<td><input type="text" name="master_course" id="master_course" class="form-control form_font_color input_field" value="<?php if($mastersqualification_details !='') echo $mastersqualification_details->course; ?>"/></td>
							<td><input type="text" name="master_university" id="master_university" class="form-control form_font_color input_field" value="<?php if($mastersqualification_details !='') echo $mastersqualification_details->university; ?>"/></td>
							<td><input type="text" name="master_college" id="master_college" class="form-control form_font_color input_field" value="<?php if($mastersqualification_details !='') echo $mastersqualification_details->college; ?>"/></td>
							<td><input type="text" name="master_major" id="master_major" class="form-control form_font_color input_field" value="<?php if($mastersqualification_details !='') echo $mastersqualification_details->major; ?>"/></td>
							<td>
							<select class="form-control" id="master_year_started" name="master_year_started">
								<?php for($i=1980;$i<=2030;$i++){?>
									<option value="<?php echo $i;?>" <?php if($mastersqualification_details !='' && $mastersqualification_details->year_started == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
								<?php }?>
                  			</select>							
							</td>
							<td>
							<select class="form-control" id="master_year_finished" name="master_year_finished">
								<?php for($i=1980;$i<=2030;$i++){?>
									<option value="<?php echo $i;?>" <?php if($mastersqualification_details !='' && $mastersqualification_details->year_finished == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
								<?php }?>
                  			</select>							
							</td>
							<td><input type="text" name="master_grade" id="master_grade" class="form-control form_font_color input_field" /></td>
							<td>
							<select class="form-control" id="master_total" name="master_total">
								<option value="100" <?php if($mastersqualification_details !='' && $mastersqualification_details->total == 100) echo 'selected="selected"';?>>100</option>
								<option value="10" <?php if($mastersqualification_details !='' && $mastersqualification_details->total == 10) echo 'selected="selected"';?>>10</option>
								<option value="7" <?php if($mastersqualification_details !='' && $mastersqualification_details->total == 7) echo 'selected="selected"';?>>7</option>
								<option value="4" <?php if($mastersqualification_details !='' && $mastersqualification_details->total == 4) echo 'selected="selected"';?>>4</option>
                  			</select>							
							</td>
							</tr>
							
							</tbody>
							
							</table>				                         
                      </div>				  
					  
					  
					  <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span> 
					  
					  
                         
                      <div class="form-group col-lg-6" >
                        	<label class="col-md-3">Password</label>
                        	<input type="password" name="password"  class="form-control form_font_color input_field" />
                           
                        </div>
                    	<div class="clearfix"></div>
                        <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span> 
                        <div class="form-group col-lg-6 ">
                        <label class="col-md-3">Retype Password</label>
                      <input type="password" name="password2"    class="form-control form_font_color input_field"/>
                      </div>
                       
                        <div class="clearfix"></div>
                        <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span> 
                       <div class="form-group">
            <button type="submit" class="button_sign_in">UPDATE</button>&nbsp;
                        	
                       </div>
                    </form>
                    
                </div>
                
                
                
           
         
     </div>	
   </div>
   <br/>
   
   
   
   <script type="text/javascript">   
   $(document).ready(function() {
   
   // diploma/hs
    $('input[type=radio][name=diploma_12]').change(function() {
        if (this.value == '1') {
			$('.diploma_hs').hide();		
            $('#diploma_id').show();						
        }
        else if (this.value == '2') {
            $('.diploma_hs').hide();		
            $('#12thidd').show();
			
        }
    });
	
	// Masters
	$('input[type=radio][name=masters]').change(function() {
        if (this.value == '1') {			
            $('#masters_id').show();						
        }
        else if (this.value == '2') {
            $('#masters_id').hide();		
			
        }
    });
	
		// getStates
	$('#locationcountries').on('change', function() {
		var id = $(this).find(":selected").val();
		var dataSet = 'id=' + id;					

							$.ajax({
									type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
									url 		: 'http://inventifweb.net/UNI/user/account/getStates', // the url where we want to POST
									data 		: dataSet, // our data object
									success: function (data1) {
									 $('#state').html(data1);									
									}
						    });

    });
	
	// getCities
	$('#state').on('change', function() {
		var id = $(this).find(":selected").val();
		var dataSet2 = 'id=' + id;					

							$.ajax({
									type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
									url 		: 'http://inventifweb.net/UNI/user/account/getCities', // the url where we want to POST
									data 		: dataSet2, // our data object
									success: function (data2) {
									 $('#city').html(data2);									
									}
						    });

    });
	
});
   </script>

   