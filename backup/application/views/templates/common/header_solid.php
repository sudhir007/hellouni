<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $config_meta_desc;?>">
    <meta name="author" content="<?php echo $config_meta_title;?>">

    <title><?php echo $config_title;?></title>

    <!-- Bootstrap Core CSS -->
  	<?php //include 'head.php'; ?>
	  <link rel="stylesheet" href="<?php echo base_url();?>css/reset.css">
	  <link href="<?php echo base_url();?>tabs/assets/css/responsive-tabs.css" rel="stylesheet">
		<link href="<?php echo base_url();?>tabs/assets/css/responsive-tabs2.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>src/DateTimePicker.css" />
	  <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>css/grayscale.css" rel="stylesheet">
	<link href="<?php echo base_url();?>jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/reveal.css">	
	  	
		<!-- Attach necessary scripts -->
		<!-- <script type="text/javascript" src="jquery-1.4.4.min.js"></script> -->
	    <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
	  <?php if($this->uri->segment('1')=='university'){?>
	    <!--<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.min.js"></script>-->
		
	  <?php }else{?>
		<!--<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>-->
		<script type="text/javascript" src="<?php echo base_url();?>js/jquery.reveal.js"></script>
	  <?php }?>
		
		<script type="text/javascript" src="<?php echo base_url();?>js/bootstrap.min.js"></script>
		
		
		
		
		<style type="text/css">
			
			.big-link { display:block; margin-top: 100px; text-align: center; font-size: 70px; color: #06f; }
		</style>
    
    <style>
   	 .shade_border{
		background:url(img/shade1.png) repeat-x; height:11px;
	}
    </style>
	
	<style>
.header_icons{
}
.header_icons ul li{
	display:inline-block;
	list-style-type:none;
	margin-bottom:-10px;
	
}

.my-account{
	position:absolute;
	background-color:#EBEBEB;
	right:110px;
	top:33px;
	border-radius:7px;
	padding:10px;
	z-index:9999;
}
#my_account{
	display:none;
}
.image_round{
	border-radius:40px;
}
.border_line{
	border:1px solid #fff;
	width:129px;
	
	
}
.edit_profile{
width:88px;background-color:#F6881F; border-radius:15px; margin-top: 5px;
    margin-bottom: 10px; text-transform:capitalize;;
	margin-left: 10px;
	font-size:12px;
}
.session{
width:88px;background-color:#3C3C3C; border-radius:15px; margin-top:10px; text-transform:capitalize; 	
margin-left: 10px;
font-size:12px;
}
.profile_img{
margin-top:5px;
}
</style>
</head>

<body>

    <!-- Navigation -->
    <?php  //include 'header.php'; ?>
	<style>
.header_icons{
}
.header_icons ul li{
	display:inline-block;
	list-style-type:none;
	margin-bottom:-10px;
	
}
.logout_padding{
padding-top:3px;
}
.notloginFav{
cursor:pointer;
}
</style>


<nav class="navbar navbar-custom  top-nav-collapse" role="navigation">
    	<div class="" style="background-color:#3C3C3C; padding: 2px 0px; border-bottom: 1px solid #A4A4A4; ">
        	<div class="container" style="background-color:#3C3C3C;">
            	<div class="pull-left header_icons"><ul style="padding-left:0px;"><li><img src="<?php echo base_url();?>img/1.png"><?php echo $config_email; ?></li>
                 <li><img src="<?php echo base_url();?>img/2.png"> <?php echo $config_phone; ?></li></ul></div>
                <div class="pull-right hidden-xs header_icons"><ul>
                <li><a href="<?php echo $config_facebook; ?>"><img src="<?php echo base_url();?>img/f.png"></a></li>
                <li><a href="<?php echo $config_twitter; ?>"><img src="<?php echo base_url();?>img/t.png"></a></li>
                <li><a href="<?php echo $config_gplus; ?>"><img src="<?php echo base_url();?>img/g.png"></a></li>
                <li><a href="<?php echo $config_linkedin; ?>"><img src="<?php echo base_url();?>img/in.png"></a></li>
                <li class="logout_padding" id="<?php if($this->session->userdata('user')){ echo 'my_accountinfo';}?>">  
				<?php if(!$this->session->userdata('user')){?>
				<a href="<?php echo base_url();?>user/account/"><img src="<?php echo base_url();?>img/login.png"><span style="font-size:11px; padding:0; margin:0;">LOGIN</span></a>
				<?php }else{?>
				Hi <?php $usr = explode(" ",$this->session->userdata('user')['name']); echo $usr[0];?>,&nbsp;<a href="<?php echo base_url();?>user/account/logout"><img src="<?php echo base_url();?>img/login.png"><span style="font-size:11px; padding:0; margin:0;">LOGOUT</span></a>
				<?php }?>
				
				</li>
                <li> <?php if(!$this->session->userdata('user')){?>
				
				<a href="<?php echo base_url();?>user/account" class="notloginFav" title="Please Login First"><img src="<?php echo base_url();?>img/fav.png"><span style="font-size:11px; padding:0; margin:0;">FAVOURITES</span></a>
				<?php }else{?>
				
				<a href="<?php echo base_url();?>user/favourites" class="notloginFav"><img src="<?php echo base_url();?>img/fav.png"><span style="font-size:11px; padding:0; margin:0;">FAVOURITES</span></a>				
				<?php }?>
				</li>
                </ul></div>
            </div>
         </div>
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">
                   <img src="<?php echo base_url();?>img/logo.png" width="200">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse" style="margin-top: 10px; padding-right:0px;">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden active">
                        <a href="#page-top"></a>
                    </li>
                     <li>
                        <a class="page-scroll" href="<?php echo base_url();?>">HOME</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="<?php echo base_url();?>about">ABOUT US</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="<?php echo base_url();?>students">STUDENTS</a>
                    </li>
                     <li>
                        <a class="page-scroll" href="<?php echo base_url();?>university">UNIVERSITY</a>
                    </li>
                     <li>
                        <a class="page-scroll" href="<?php echo base_url();?>abroard">FLY ABROARD</a>
                    </li>
                     <li>
                        <a class="page-scroll" href="<?php echo base_url();?>contact">CONTACT US</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	<!--document.getElementById('my_account').style.display = 'block';-->
	<div id="my_account"  class="col-md-2 my-account additional">
<div class="col-lg-4 profile_img">
<?php if($this->session->userdata('user')['fid']){?>
<img src="http://graph.facebook.com/<?php echo $this->session->userdata('user')['fid'];?>/picture?type=normal" width="65" class="image_round" />
<?php }else{ ?>
<img src="<?php echo base_url();?>images/no_image.jpg" width="65" class="image_round" />
<?php } ?>
</div>
<div class="col-lg-7">
<div><a href="<?php echo base_url();?>user/account/update"><button class="btn edit_profile" >Edit Profile</button></a>
</div>
<div class="border_line"></div>
<div><button class="btn session" >Session</button></div>
</div>

</div>