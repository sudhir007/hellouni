

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="description" content="<?php echo $config_meta_desc;?>">
    <meta name="author" content="<?php echo $config_meta_title;?>">

     <title><?php echo $config_title;?></title>

    <!-- Bootstrap Core CSS -->
  	<?php //include 'head.php'; ?>
	
	  <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>css/grayscale.css" rel="stylesheet">
	
	<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style.css" />-->
<style>
.header_icons{
}
.header_icons ul li{
	display:inline-block;
	list-style-type:none;
	margin-bottom:-10px;
	
}

.my-account{
	position:absolute;
	background-color:#EBEBEB;
	right:110px;
	top:41px;
	border-radius:7px;
	padding:10px;
	z-index:9999;
}
#my_account{
	display:none;
}
.image_round{
	border-radius:40px;
}
.border_line{
	border:1px solid #fff;
	width:129px;
	
	
}
.edit_profile{
width:88px;background-color:#F6881F; border-radius:15px; margin-top: 5px;
    margin-bottom: 10px; text-transform:capitalize;;
	margin-left: 10px;
	font-size:12px;
}
.session{
width:88px;background-color:#3C3C3C; border-radius:15px; margin-top:10px; text-transform:capitalize; 	
margin-left: 10px;
font-size:12px;
}
.profile_img{
margin-top:5px;
}
</style>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- Navigation -->
    <?php  //include 'header.php'; ?>
	<style>
.header_icons{
}
.header_icons ul li{
	display:inline-block;
	list-style-type:none;
	margin-bottom:-10px;
	
}
</style>


<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    	<div  class="row" style="background:url(<?php echo base_url();?>img/header_trans.png) repeat-x; padding: 2px 0px;">
        	<div class="container">
            	<div class="pull-left header_icons"><ul style="padding-left:0px;"><li><img src="img/1.png">INFO@IMPERIAL-OVERSEAS.COM</li>
                 <li><img src="<?php echo base_url();?>img/2.png"> +91 9167991339 / +91 9819900666</li></ul></div>
                <div class="pull-right hidden-xs header_icons"><ul>
                <li><a href="<?php echo $config_facebook; ?>"><img src="<?php echo base_url();?>img/f.png"></a></li>
                <li><a href="<?php echo $config_twitter; ?>"><img src="<?php echo base_url();?>img/t.png"></a></li>
                <li><a href="<?php echo $config_gplus; ?>"><img src="<?php echo base_url();?>img/g.png"></a></li>
                <li><a href="<?php echo $config_linkedin; ?>"><img src="<?php echo base_url();?>img/in.png"></a></li>
                
				<li class="logout_padding" id="<?php if($this->session->userdata('user')){ echo 'my_accountinfo';}?>">  
				<?php if(!$this->session->userdata('user')){?>
				<a href="<?php echo base_url();?>user/account/"><img src="<?php echo base_url();?>img/login.png"><span style="font-size:11px; padding:0; margin:0;">LOGIN</span></a>
				<?php }else{?>
				Hi <?php $usr = explode(" ",$this->session->userdata('user')['name']); echo $usr[0];?>,&nbsp;<a href="<?php echo base_url();?>user/account/logout"><img src="<?php echo base_url();?>img/login.png"><span style="font-size:11px; padding:0; margin:0;">LOGOUT</span></a>
				<?php }?>
				
				</li>
                <li> <img src="<?php echo base_url();?>img/fav.png"><span style="font-size:11px; padding:0; margin:0;">FAVOURITES</span></li>
                </ul></div>
            </div>
         </div>
		 
		 
	
	
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">
                   <img src="<?php echo base_url();?>img/logo.png" width="200" />
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse" style="margin-top: 10px;">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="<?php echo base_url();?>">HOME</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="<?php echo base_url();?>about">ABOUT US</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="<?php echo base_url();?>students">STUDENTS</a>
                    </li>
                     <li>
                        <a class="page-scroll" href="<?php echo base_url();?>university">UNIVERSITY</a>
                    </li>
                     <li>
                        <a class="page-scroll" href="<?php echo base_url();?>abroard">FLY ABROARD</a>
                    </li>
                     <li>
                        <a class="page-scroll" href="<?php echo base_url();?>contact">CONTACT US</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	
	<div id="my_account"  class="col-md-2 my-account additional">
<div class="col-lg-4 profile_img">
<?php if($this->session->userdata('user')['fid']){?>
<img src="http://graph.facebook.com/<?php echo $this->session->userdata('user')['fid'];?>/picture?type=normal" width="65" class="image_round" />
<?php }else{ ?>
<img src="<?php echo base_url();?>images/no_image.jpg" width="65" class="image_round" />
<?php } ?>
</div>
<div class="col-lg-7">
<div><a href="<?php echo base_url();?>user/account/update"><button class="btn edit_profile" >Edit Profile</button></a>
</div>
<div class="border_line"></div>
<div><button class="btn session" >Session</button></div>
</div>

</div>
 <!--header end-->
 <header class="intro">
        <div class="intro-body">
            <!--  <div class="container">
               <!-- <div class="row">-->
                    <!--<div class="col-md-8 col-md-offset-2">-->
					
					<iframe width="100%" height="720" src="https://www.youtube-nocookie.com/embed/Yemt0aXyLyc" frameborder="0" allowfullscreen></iframe>
       <!--<iframe width="100%" height="100%" src="https://www.youtube.com/embed/Yemt0aXyLyc?autoplay=1&rel=0&loop=1&playlist=OhQMUphxPkA?modestbranding=1&autohide=1&showinfo=0&controls=0" frameborder="0" allowfullscreen style="z-index:-1" ></iframe>-->
	   <!--<iframe width="560" height="315" src="https://www.youtube.com/embed/Yemt0aXyLyc" frameborder="0" allowfullscreen></iframe>-->
                        <!--<h1 class="brand-heading">Grayscale</h1>
                        <p class="intro-text">A free, responsive, one page Bootstrap theme.<br>Created by Start Bootstrap.</p>
                        <a href="#about" class="btn btn-circle page-scroll">
                            <i class="fa fa-angle-double-down animated"></i>
                        </a>-->
                   <!-- </div>-->
               <!-- </div>-->
            <!-- </div>-->
        </div>
    </header>
<div style="text-align: center;"><a style="margin: -48px auto 0px; display: inline-block;" href="http://inventifweb.net/UNI/location/country"><input style="margin-top: -50px;
padding: 10px;width: 100px;background-color: #000;border: medium none;border-radius: 100px;font-size: 14px;color: #FFF;opacity: 0.5;" name="skip" id="skip" value="Skip" type="button"></a></div>