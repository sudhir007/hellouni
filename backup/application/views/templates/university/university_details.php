<script type="text/javascript" src="<?php echo base_url();?>source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>source/jquery.fancybox.css?v=2.1.5" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style.css" media="screen" />

<script src="<?php echo base_url();?>tabs/assets/js/responsive-tabs.min.js"></script>
<script type="text/javascript">
		$(document).ready(function() {
		
		//alert('test');
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();

			/*
			 *  Different effects
			 */

			// Change title type, overlay closing speed
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});

			// Disable opening and closing animations, change title type
			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});

			// Set custom style, close if clicked, change title type and overlay color
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});

			// Remove padding, set opening and closing animations, close if clicked and disable overlay
			$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});

			/*
			 *  Button helper. Disable animations, hide close button, change title type and content
			 */

			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});


			/*
			 *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
			 */

			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,
				arrows    : false,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});

			/*
			 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
			*/
			$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});

			/*
			 *  Open manually
			 */

			$("#fancybox-manual-a").click(function() {
				$.fancybox.open('1_b.jpg');
			});

			$("#fancybox-manual-b").click(function() {
				$.fancybox.open({
					href : 'iframe.html',
					type : 'iframe',
					padding : 5
				});
			});

			$("#fancybox-manual-c").click(function() {
				$.fancybox.open([
					{
						href : '1_b.jpg',
						title : 'My title'
					}, {
						href : '2_b.jpg',
						title : '2nd title'
					}, {
						href : '3_b.jpg'
					}
				], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50
						}
					}
				});
			});


		});
	</script>
 <style>
   	 .shade_border{
		background:url(img/shade1.png) repeat-x; height:11px;
	}
	.search_by{
	background-color:#f3f4f4; box-shadow:5px 5px 5px #eae8e8; height:auto;
	    padding-top: 10px;
    padding-left: 30px;
	 padding-right: 30px;
	}
	.search_by h4{
		color:#3c3c3c;
		font-family:lato_regular;
		font-weight:500;
	}
	.table_heading{
		padding:5px;
		font-weight:bold;
	}
	.more_detail{
	background-color:#F6881F;
	text-transform:uppercase;
	height:28px;	
	border:0px;
	  color:#000; 
		border-radius:2px;
		width:25px;
}
.right_side_form{
background:#3c3c3c;
box-shadow:5px 5px 2px #CCCCCC; 
height:auto;
margin-top:40px;
margin-left:10px;

}
label{
	color:#fff;
	
}
.more_detail{
	background-color:#F6881F;
	text-transform:uppercase;
	height:28px;	
	border:0px;
	  color:#fff; 
		border-radius:2px;
		
}
label{
font-size:11px;
font-weight:300;	
}
.total{
background-image:url(images/tab.png);
width:94px;
height:41px;
float:left;
}
.total_text{
color:#7e7e7e;
padding-top:2px;
padding-left:10px;

font-size:12px;
}
.total_text2{
color:#5b8abe;
font-size:12px;
}
.people{
float:left;
padding:10px;
height:30px

}
.font_color{
margin-left:-13px;
}
.font_color h4{
color:#374552;
}
.fancy_box{
border: 1px solid #64717d ;
margin-top:5px;
margin-left:5px;
margin-right:40px;
padding:5px;
}
.cd-tabs h4{
margin: 16px 0 16px;
text-transform: uppercase;
font-family: lato_regular;
font-weight: 500;
letter-spacing: 1px;
color: #374552;
}
.font_class{
font-family: lato_regular;
width:100%;
height:40px;
}
.uni_logo{
top:-155px;position:absolute; margin-left:-15px;
}
.uni_logo img{
padding:7px;background:#fff;
}
.header_icons{
}
.header_icons ul li{
	display:inline-block;
	list-style-type:none;
	margin-bottom:-10px;
	
}
    </style>

<div class="clearfix"></div>

<div id="page_loader">  <div> 
<?php if($banner){?>
            <img src="<?php echo base_url().'admin/'.$banner;?>" width="100%"  />
<?php }else{?>
           <img src="<?php echo base_url();?>images/ranepa_postgraduate_cover_0_0.jpg" width="100%"  />
<?php }?>
          <div class="cd-tabs ">
		 <div><div class="col-lg-3 visible-lg uni_logo ">
		 
		 <?php if($image){?>
		 
		 <img src="<?php echo base_url().'admin/'.$image;?>" />
		
		 <?php }else{?>
		 
		 <img src="<?php echo base_url();?>images/uni-logo.png" />
		 <?php }?>
		 
		 </div>	
		 <div class="col-sm-3 visible-sm visible-xs" style="top:-66px;position:absolute;"><img src="images/uni-logo.png" width="70" /></div>
	<div class="font_class" style="background-color:#374552;"><nav class="col-lg-9 col-lg-offset-3 col-md-3">
		<ul class="cd-tabs-navigation" style="margin-bottom:0px;">
					
			<li><a data-content="about" class="selected" href="#0">ABOUT US</a></li>
			<li><a data-content="overview" href="#0">OVERVIEW</a></li>
			<li><a data-content="course_content" href="#0">COURSE CONTENT</a></li>
			<li><a data-content="cost" href="#0">COST / FUNDING</a></li>
			<li><a data-content="entry" href="#0">ENTRY REQUIREMENT</a></li>
			
		</ul> <!-- cd-tabs-navigation -->
	</nav></div><div class="clearfix"></div></div>
	<div class="col-lg-8 col-md-9 col-sm-12">
	<ul class="cd-tabs-content font_color ">
		<li data-content="about" class="selected">
		<h4><?php echo $name;?></h4>
			
			
			<?php echo $about;?>
	
	<div class="col-lg-6 font_color">
	   <h4>Total No. Students</h4>
	  <div class="total col-lg-6">
	  <p class="total_text">In Total<br/>
	  <span class="total_text2"><?php if(isset($student)) echo $student;?></span></p>
	  </div>
	  <div class="people"><img src="<?php echo base_url();?>images/1.png"/></div>
	  <div class="clearfix"></div>
	    <h4>Total No. International Students</h4>
	  <div class="total col-lg-6">
	  <p class="total_text">In Total<br/>
	  <span class="total_text2"><?php if(isset($internationalstudent)) echo $internationalstudent;?></span></p>
	  </div>
	  <div class="people"><img src="<?php echo base_url();?>images/1.png"/></div>
	 </div>
	 <div class="clearfix visible-xs visible-sm "></div>
	 <div class="col-lg-6">
	  <h4>Facility</h4>
	   <p style="color:#374552;"> <!--obcaecati fuga assumenda nihil aliquid sed vero, modi, 
	   voluptatem? Vitae voluptas aperiam nostrum quo harum numquam earum facilis sequi.
	   iure commodi ducimus similique doloremque! Odio quaerat dolorum, alias nihil quam iure del
	   ectus repellendus modi cupiditate dolore atque quasi--> 
	   
	   <?php if(isset($facilities)) echo $facilities;?>
	   
	   
	   
	   </p>
	 </div><div class="clearfix "></div>
		<!--<div  style="white-space:nowrap;">-->
		 <h4>Images / Videos</h4>
		<div class="col-lg-5 col-md-3 col-sm-3 fancy_box"><a class="fancybox" href="<?php echo base_url();?>img/portfolio/11.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><img src="<?php echo base_url();?>img/portfolio/thumb/11.jpg" width="100%"  alt="" /></a></div>

		<div class="col-lg-5  col-md-3 col-sm-3  fancy_box "><a class="fancybox" href="<?php echo base_url();?>img/portfolio/22.jpg" data-fancybox-group="gallery" title="Etiam quis mi eu elit temp"><img src="<?php echo base_url();?>img/portfolio/thumb/22.jpg" width="100%"   /></a></div>

		
		
		
	
		
		</li>

		<li data-content="overview">
		<h4>Degree</h4>
			<p><?php if(isset($degree_name)) echo $degree_name;?></p>
		<h4>Course Languages</h4>
			<p> <?php if(isset($language)) echo $language;?></p>
			<h4>Admission semester</h4>
			<p><?php if(isset($admissionsemester)) echo $admissionsemester;?></p>
		
		<h4>Beginning</h4>
			<p><?php if(isset($beginning)) echo $beginning;?></p>
		<h4>Programe Duration</h4>
			<p><?php if(isset($duration)) echo $duration;?></p>
		
		<h4>Application Deadline</h4>
			<p><?php if(isset($application_deadline)) echo $application_deadline;?></p>
		</li>
<li data-content="course_content">
		<h4><?php echo $name;?></h4>
			<?php if(isset($details)) echo $details;?>
		</li>
		<li data-content="cost">
		<h4>Tuition Fees</h4>
			<p><?php if(isset($tutionfee)) echo $tutionfee;?></p>
		<h4>Enrolment Fees</h4>
			<p><?php if(isset($enrolmentfee)) echo $enrolmentfee;?></p>
			<h4>Cost Of Living</h4>
			<p><?php if(isset($livingcost)) echo $livingcost;?></p>
		
		<h4>Job Opportunity</h4>
			<p><?php if(isset($jobopportunities)) echo $jobopportunities;?></p>
		</li>

		

		<li data-content="entry">
		<h4>Language requirements</h4>
			<p><?php if(isset($required_language)) echo $required_language;?></p>
		<h4>Academic requirements</h4>
			
			<?php if(isset($academicrequirement)) echo $academicrequirement;?>
			
		</li>

		<li data-content="trash">
		<h4>Russian Presidential Academy of  National Economy and Public Ad</h4>
			<p>Trash Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio itaque a iure nostrum animi praesentium, numquam quidem, nemo, voluptatem, aspernatur incidunt. Fugiat aspernatur excepturi fugit aut, dicta reprehenderit temporibus, nobis harum consequuntur quo sed, illum.</p>

			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima doloremque optio tenetur, natus voluptatum error vel dolorem atque perspiciatis aliquam nemo id libero dicta est saepe laudantium provident tempore ipsa, accusamus similique laborum, consequatur quia, aut non maiores. Consectetur minus ipsum aliquam pariatur dolorem rerum laudantium minima perferendis in vero voluptatem suscipit cum labore nemo explicabo, itaque nobis debitis molestias officiis? Impedit corporis voluptates reiciendis deleniti, magnam, fuga eveniet! Velit ipsa quo labore molestias mollitia, quidem, alias nisi architecto dolor aliquid qui commodi tempore deleniti animi repellat delectus hic. Alias obcaecati fuga assumenda nihil aliquid sed vero, modi, voluptatem? Vitae voluptas aperiam nostrum quo harum numquam earum facilis sequi. Labore maxime laboriosam omnis delectus odit harum recusandae sint incidunt, totam iure commodi ducimus similique doloremque! Odio quaerat dolorum, alias nihil quam iure delectus repellendus modi cupiditate dolore atque quasi obcaecati quis magni excepturi vel, non nemo consequatur, mollitia rerum amet in. Nesciunt placeat magni, provident tempora possimus ut doloribus ullam!</p>
		</li>
		
	</ul></div></div><div class="col-lg-3 right_side_form col-sm-12 col-md-3 col-xs-10" >
        
         <h5 style="color:#fff;">Book one to one sessions :</h5> 
         <form class="form-horizontal" method="post">
         <div class="form-group">
         	<label class="control-label col-sm-4" style="text-align:left;">Name :</label>
           <div class="col-sm-8"  style="float:right;"> <input type="text" name="name" class="form-control"></div>
         </div>
          <div class="form-group">
         	<label class="control-label col-sm-4" style="text-align:left;">Email :</label>
            <div class="col-sm-8" style="float:right;"><input type="text" name="email" class="form-control"></div>
         </div>
          <div class="form-group">
         	<label class="control-label col-sm-4" style="text-align:left; padding-right: 0px;">Mobile No. :</label>
            <div class="col-sm-8"  style="float:right;"><input type="text" name="mobile_no" class="form-control"></div>
         </div>
         
         <div class="form-group">
         <label class="control-label col-sm-4" style="text-align:left;">Date :</label>
		<div class="col-sm-8"  style="float:right;"><input type="text" data-field="date" class="form-control" name="date" readonly> </div>
		</div>
		<!------------------------ Time Picker ------------------------>
         <div class="form-group">
		 <label class="control-label col-sm-4" style="text-align:left;">Time : </label>
		<div class="col-sm-8"  style="float:right;"><input type="text" data-field="time" class="form-control" readonly name="time"></div>
        </div>
	
		<!---------------------- DateTime Picker ---------------------->
		
	
	
		<div id="dtBox"></div>
      
         <div class="form-group">
         	<label class="control-label col-sm-4" style="text-align:left;">State :</label>
            <div class="col-sm-8"  style="float:right;"> <select class="form-control" name="state" id="state">
                      	<option value="">--Select State--</option> 
              
										                  </select></div>
         </div>
          <div class="form-group">
         	<label class="control-label col-sm-4" style="text-align:left;">City :</label>
            <div class="col-sm-8"  style="float:right;"><select class="form-control" name="city"  id="city">
                      	<option value="">--Select City--</option> 
              
										                  </select></div>
         </div>
         <div class="form-group">
         	<label class="control-label col-sm-4" style="text-align:left;">Query :</label>
           <div class="col-sm-8"  style="float:right;"><textarea class="form-control" name="query"></textarea></div>
         </div>
          <div class="form-group">  
          <div class="col-sm-10">   
         <button type="button" class="btn btn-default" name="submit" style="font-size:11px;">Submit >></button></div>
         </div>
         </form>
         
         
        
         </div> <div class="clearfix visible-md visible-sm "></div>
		 
		  <!-- cd-tabs-content -->
		 <div class="clearfix"></div>
		 
	</p>

</div>
       </div>
       
  
  	
 	
   
        <div class="container">
        	<!-- <h4 style="color:#000;">Choose Country</h4>-->
            <br/>
          
           
           
           
        <!--<div  style="background:url(img/line.png) repeat-y; height:120px; width:4px; float:left;" ></div>-->
        
          <!--<div class="clearfix"></div> -->  
      
         
     </div>	
   </div>
   
   
   
   
    <!-- Bootstrap Core JavaScript -->
    <!--<script src="js/bootstrap.min.js"></script>-->

    <!-- Plugin JavaScript -->
  <!--  <script src="js/jquery.easing.min.js"></script>-->

    <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
  <!--  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>-->

    <!-- Custom Theme JavaScript -->
   

<!--<script src="js/main.js"></script>--> <!-- Resource jQuery -->