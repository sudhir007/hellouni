<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model 
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 
  <Reverse bidding system> 
    Copyright (C) <2009>  <FreeLance PHP Script>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
    If you want more information, please email me at support@freelancephpscript.com or 
    contact us from http://www.freelancephpscript.com/contact 
 
 
 */
	 class User_model extends CI_Model {
	 
 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
	function getUser($condition1,$condition2)
	 {
	 	if(count($condition1)>0 || count($condition2)>0  || isset($condition1) || isset($condition2))
		{
	     $this->db->where($condition1);
	     $this->db->where($condition2);
	  }
		
		
		$this->db->from('user_master');
		$this->db->join('type_master', 'type_master.type_id = user_master.type','left');
		$this->db->join('status_master', 'status_master.id = user_master.status','left');
		
		
		
		$this->db->select('user_master.id,user_master.name,user_master.email,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,status_master.status as status_name');
		
		
		
		$result=$this->db->get()->result();
		return $result;
			
	 }
	
	
	
	
	function checkUser($conditions=array())
	 {
	 	
		//echo 'aaaaaa'; die('----');
		$this->db->where($conditions);
		$this->db->from('user_master');
		$this->db->join('type_master', 'type_master.type_id = user_master.type','left');
		$this->db->join('status_master', 'status_master.id = user_master.status','left');
		
		
		
		$this->db->select('user_master.id,user_master.name,user_master.email,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,status_master.status as status_name');
		
		
		
		$result=$this->db->get()->row();
		return $result;
			
	 }
	 
	 function getLead($conditions=array())
	 {
	 	
		
		$this->db->where($conditions);
		$this->db->from('lead_master');
		
		$this->db->select('lead_master.id,lead_master.name,lead_master.email,lead_master.phone,lead_master.mainstream,lead_master.courses,lead_master.degree,lead_master.countries');
		$this->db->order_by("id", "desc");
		$result=$this->db->get()->row();
		return $result;
			
	 }
	 
	 
	 function checkUserBy_Email($conditions=array())
	 {
	 	
		
		$this->db->where($conditions);
		$this->db->from('infra_email_master');
		$this->db->join('infra_user_master', 'infra_email_master.id = infra_user_master.email','left');
		$this->db->join('infra_role_master', 'infra_role_master.id = infra_user_master.role_id','left');
		$this->db->join('infra_status_master', 'infra_status_master.id = infra_user_master.status_id','left');
		
		$this->db->select('infra_user_master.id,infra_user_master.name,infra_user_master.user_name,infra_role_master.name as role,infra_email_master.email,infra_user_master.status_id,infra_status_master.name as statusname');
		
		
		
		$result=$this->db->get()->row();
		return $result;
			
	 }
	 
	 
	 function getUniversityDetailsByid ($conditions){
	
	 $this->db->where($conditions);
	
	 $this->db->from('university_details'); 
	 /*$this->db->join('user_master', 'user_master.id = university_details.university_id','left');
	 $this->db->join('status_master', 'status_master.id = user_master.status','left');*/
	
		
	 $this->db->select('university_details.university_id,university_details.about,university_details.website,university_details.student,university_details.internationalstudent,university_details.facilities,university_details.address,university_details.country,university_details.state,university_details.city');
		
	 $result=$this->db->get()->row();

	 return $result;
	}
	
	
	function getuniversity_to_degree_to_course ($conditions){
	
	 $this->db->where($conditions);
	 
	 $this->db->from('university_to_degree_to_course');
	
	 $this->db->select('university_to_degree_to_course.id,university_to_degree_to_course.degree_id,university_to_degree_to_course.course_id,university_to_degree_to_course.university_id,university_to_degree_to_course.language,university_to_degree_to_course.admissionsemester,university_to_degree_to_course.beginning,university_to_degree_to_course.duration,university_to_degree_to_course.application_deadline,university_to_degree_to_course.details,university_to_degree_to_course.tutionfee,university_to_degree_to_course.enrolmentfee,university_to_degree_to_course.livingcost,university_to_degree_to_course.jobopportunities,university_to_degree_to_course.required_language,university_to_degree_to_course.academicrequirement,university_to_degree_to_course.academicrequirement');
		
	 $result=$this->db->get()->row();

	 return $result;
	}
	
	
	function addRemerberme($insertData=array(),$expire)
	{
	
		 $this->auth_model->setUserCookie('uname',$insertData['username'], $expire);
		 $this->auth_model->setUserCookie('pwd',$insertData['password'], $expire);
		 
		// echo $val=get_cookie('uname',TRUE); exit;
		 echo $this->input->cookie('uname', TRUE); exit;
		 //die();
		 
	}
		
	function removeRemeberme()
	{
	
	  $this->auth_model->clearUserCookie(array('uname','pwd'));
	  
	}
	
	
	function insertUser($insertData=array())
	 {
	  		
	 $user_master_data = array(
     'name' 		=> $insertData['name'],
     'email' 		=> $insertData['email'],
	 'password' 	=> $insertData['password'],
	 'type' 		=> $insertData['type'] ,
	 'createdate' 	=> $insertData['createdate'],
	 'status' 		=> $insertData['status'],
	 'notification' => $insertData['notification']
     );
	 
	 $this->db->insert('user_master', $user_master_data);
	 $user_id = $this->db->insert_id();
	 
	 $user_permission_course_data = array(
     'user_id' 	=> $user_id,
     'view' 	=> $insertData['view'],
	 'create' 	=> $insertData['create'],
	 'edit' 	=> $insertData['edit'],
	 'del' 		=> $insertData['del']
	  );
	 
	 $this->db->insert('user_permission_course', $user_permission_course_data);
	 return 'success';
	 
	 }
	 
	 
	 function insertStudent($insertData=array())
	 {
	  		
	 $user_master_data = array(
     'name' 		=> $insertData['name'],
     'email' 		=> $insertData['email'],
	 'password' 	=> $insertData['password'],
	 'type' 		=> $insertData['type'] ,
	 'createdate' 	=> $insertData['createdate'],
	 'status' 		=> $insertData['status'],
	 'notification' => '0',
	 'flag' 		=> '0',
	 'activation' 	=> $insertData['activation']
     );
	 
	 $this->db->insert('user_master', $user_master_data);
	 $user_id = $this->db->insert_id();
	 
	 $studentDetails_data = array(
     'user_id' 	=> $user_id,
     'address' 	=> $insertData['address'],
	 'country' 	=> $insertData['country'],
	 'city' 	=> $insertData['city'],
	 'zip' 		=> $insertData['zip'],
	 'dob' 		=> $insertData['dob'],
	 'phone' 	=> $insertData['phone']
	  );
	 
	 $this->db->insert('student_details', $studentDetails_data);
	 return true;
	 
	 }
	 
	 function insertLead($insertData=array())
	 {
	  		
	
	 $this->db->insert('lead_master', $insertData);
	 $lead_id = $this->db->insert_id();
	 
	 return $lead_id;
	 
	 }
	 
	 function updateUserInfo($insertData=array(),$id)
	 {	 
	 
	 $this->db->where('id',$id);
	 $this->db->update('user_master',$insertData); 
	 
	  return true;
	 }
	 
	 
	 function updateLeadInfo($insertData=array(),$id)
	 {	 
	 
	 $this->db->where('id',$id);
	 $this->db->update('lead_master',$insertData); 
	 
	  return true;
	 }
	
	
	function getUserByid ($conditions){
	
	 $this->db->where($conditions);
	
	 
	 $this->db->from('user_master');
	 $this->db->join('type_master', 'type_master.type_id = user_master.type','left');
	 $this->db->join('status_master', 'status_master.id = user_master.status','left');
	 $this->db->join('user_permission_course', 'user_permission_course.user_id = user_master.id','left');
		
	 $this->db->select('user_master.id,user_master.name,user_master.email,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,status_master.status as status_name,user_master.notification,user_permission_course.view,user_permission_course.create,user_permission_course.edit,user_permission_course.del');
		
	 $result=$this->db->get()->row();

	 return $result;
	}
	
	
	function getUniversityByid ($conditions){
	
	 $this->db->where($conditions);
	
	 
	 $this->db->from('user_master');
	 $this->db->join('university_details', 'university_details.university_id = user_master.id','left');
	 $this->db->join('country_master', 'country_master.id = university_details.country','left');
	 $this->db->join('status_master', 'status_master.id = user_master.status','left');
	
		
	 $this->db->select('user_master.id,user_master.name,user_master.email,user_master.type,university_details.about,university_details.country,country_master.code');
		
	 $result=$this->db->get()->row();

	 return $result;
	}
	
	
	function editUser($insertData=array())
	 {
	 
	 // Update User table
		$user_master_data = array(
		 'name' 		=> $insertData['name'],
		 'email' 		=> $insertData['email'],
		 'password' 	=> $insertData['password'],
		 'type' 		=> $insertData['type'] ,
		 'status' 		=> $insertData['status'],
		 'notification' => $insertData['notification']
		 );
	 
	 $this->db->where('id',$insertData['id']);
	 $this->db->update('user_master',$user_master_data);
	 
	 $this->db->from('user_permission_course');
	 $this->db->where('user_id',$insertData['id']);		
	 $this->db->select('*');
		
	 $is_user_exists=$this->db->get()->row();
	 //print_r($is_user_exists); exit;

	 // $is_user_exists = $this->db->get_where('user_permission_course', array('user_permission_course.user_id' => $insertData['id']))->row();
	   if($is_user_exists){	 
		 
		 $user_permission_course_data = array(
		 'view' 	=> $insertData['view'],
		 'create' 	=> $insertData['create'],
		 'edit' 	=> $insertData['edit'],
		 'del' 		=> $insertData['del']
		  );
	 	$this->db->where('user_id',$insertData['id']);
	 	$this->db->update('user_permission_course',$user_permission_course_data);
	   }else{
	    
		$user_permission_course_data = array(
		 'user_id'	=> $insertData['id'],
		 'view' 	=> $insertData['view'],
		 'create' 	=> $insertData['create'],
		 'edit' 	=> $insertData['edit'],
		 'del' 		=> $insertData['del']
		  );
	 	$this->db->insert('user_permission_course', $user_permission_course_data);
	 
	   }
	  return 'success';
	 }
	 
	 function deleteUser($id)
	 {
	  $data = array('status' => '5');  
	   
	 $this->db->where('id',$id);
	 $this->db->update('user_master',$data);
	 return 'success';
	 }
	
	function deleteTrash($id)
	 {
	  	   
	 $this->db->where('id', $id);
	 $this->db->delete('user_master');
	 return 'success';
	 }
	
	
					   
	
	 function getSendToEmail($type)
	{
		
		$query="SELECT emailid_master.`Email` FROM `mailtemplate`,`emailid_master` WHERE 
emailid_master.`ID`=mailtemplate.`To` and mailtemplate.`MailType`='".$type."'";
		
		$result=$this->db->query($query);
		return($result->row());
	}
	
	
	function TempTop() {
	
 $top='<table width="963" border="2" cellspacing="0" cellpadding="0" align="center"       style="border-collapse:collapse; border-color:#5cd5f0">
		<tr>
		<td align="left" valign="top" style="padding:10px;">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="left" valign="top" style="background:#333;"><img src="'.base_url().'images/logo-small1.png" width="300" height="72" alt="" /></td></tr>
	</table>';
			
 return $top;
	
	             }
				 
	function TempBody($username,$link,$linktext,$text) {
	
	
 $top='<table>
		<tr>
		<td align="left" valign="top" style="padding:10px;">
		
		<p>Hi '.$username.',</p>
		
		<p>'.$text.'</p>
		
		<p style="color:#0000CC"><a href="'.$link.'">"'.$linktext.'"</a></p>
		</td>
		</tr>
	</table>';
			
 return $top;
	
	             }
	
	
function TempBottom() {
	
 $bottom='<p>Thanks and Regards,</p>
              <p>Super Admin</p>
						<strong><p><a href="'.base_url().'">TripAdapt</a></p></strong></td>
						</tr>
						</table>
						</td>
						</tr>
						</table>';
			
                return $bottom;
	
	                  }
	
	
	
	
	
	
	
	
function SendCustomEmail($P_Type,$P_To,$id)
{


    if($P_Type=="CustomerMessageToAdmin")
	{
	
   $result2=$this->BrandInfo($id);
$result1=$this->EmailInfo($P_Type);
 
 

 
$from=$result1->Email;
$to=$P_To;
$subject=$result1->Subject;
$content=$result1->Message;
$body='<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						<td align="left" valign="top" style="font:normal 14px/20px Verdana, Geneva, sans-serif; color:#333; padding:20px 0 20px 0px;">
<p>Dear Administrator,</p>
<p>You have a new message from <font style="color:#0000ff">'.$result2->username.'</font></p>
						'.$content.'
                      
';		


	}	
	
	
	
	
	
	 if($P_Type=="ChangePasswordToCustomer")
	{
	
   $result2=$this->BrandInfo($id);
$result1=$this->EmailInfo($P_Type);
 
 

 
$from=$result1->Email;
$to="sas.somnath@gmail.com";
$subject=$result1->Subject;
$content=$result1->Message;

$body='<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						<td align="left" valign="top" style="font:normal 14px/20px Verdana, Geneva, sans-serif; color:#333; padding:20px 0 20px 0px;">

						'.$content.'
						<p> Login Id:<font color="#0000ff">'.$result2->email.'</font> <br ></p>
						

				       <p> Password:<font color="#0000ff">'.$result2->passwrd.'</font></p>
					<strong>  </strong>
                      
';			


	}	
	
	
	
	
if($P_Type=="NewQueryToAdmin"){

	$result5=$this->MsgInfoTo($id); 
 $result4=$this->MsgInfo($id);       
  
$result1=$this->EmailInfo($P_Type);
 
$from=$result1->Email;
$to=$P_To;
$subject=$result1->Subject;
$content=$result1->Message;
$body='<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						<td align="left" valign="top" style="font:normal 14px/20px Verdana, 
<p>There is a new query from <font style="color:#0000ff">'.$result4['BrandName'].'</font> to <font style="color:#0000ff">'.$result5['BrandName'].'</font></p>
						'.$content.'';		


	                                             }		
	
	
	if($P_Type=="NewAdvertToAdmin"){
	

$result1=$this->EmailInfo($P_Type);
 
$from=$result1->Email;
$to=$P_To;
$subject=$result1->Subject;
$content=$result1->Message;
$body='<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						<td align="left" valign="top" style="font:normal 14px/20px Verdana, Geneva, sans-serif; color:#333; padding:20px 0 20px 0px;">
<p>Dear Administrator,</p>
<p>There is a new Advert in RangyBrands.</p>
						'.$content.'
                      
';		


	                                             }
	
	
	
$toid=$to;
$sub=$subject;
$fromid=$from;
$message=$this->TempTop().$body.$this->TempBottom();

				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= 'From: '.$fromid . "\r\n";

				$mail=mail($toid, $sub, $message, $headers);

return $mail;	
	
	
	
	
	
												 
}



	function SendMail($to,$from,$subject,$body) {
	
                //$to = $query->email;
				//$subject = "Plese Register First";
				//******body
				$message = "<html><head></head><body>";
				$message .= $this->TempTop();
				$message .= $body;
				$message .= $this->TempBottom();
				
				//$from = $this->config->item('site_admin_mail');
				
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; 
				$headers .= "From:" . $from;
				mail($to,$subject,$message,$headers);
	
           }


    function CheckReset($conditions){
	
	$this->db->where($conditions);
	$this->db->from('infra_user_master');
	$this->db->select('*');
	
	 $result=$this->db->get()->row();

	 return $result;
	
	}

 function getSecurity_Credential($conditions){
	
	$this->db->where($conditions);
	$this->db->from('user_security_question');
	$this->db->select('*');
	
	 $result=$this->db->get()->row();

	 return $result;
	
	}
	
}
	
	
	
	
	
	
	
	
	
	
	
	
	
?>