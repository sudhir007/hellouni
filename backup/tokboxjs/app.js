var apiKey,
    sessionId,
    token;

$(document).ready(function() {
  // Make an Ajax request to get the OpenTok API key, session ID, and token from the server
  $.get('http://inventifweb.net/UNI/user/account/tokboxdemo' + '/session', function(res) {
    apiKey = 45715252;
    sessionId = "1_MX40NTcxNTI1Mn5-MTQ3ODYwNDI2MzU3NX5nMjYwa3g1dWdTeTc0MW1DQ2JnQjZKeFB-fg";
    token = "T1==cGFydG5lcl9pZD00NTcxNTI1MiZzaWc9MWQxMGQyNDc4YTYxNDE2ZTEzZTUwNGIzMjEyYjhhMWMyMGJkZTEwODpzZXNzaW9uX2lkPTFfTVg0ME5UY3hOVEkxTW41LU1UUTNPRFl3TkRJMk16VTNOWDVuTWpZd2EzZzFkV2RUZVRjME1XMURRMkpuUWpaS2VGQi1mZyZjcmVhdGVfdGltZT0xNDc4NjA0MjcxJm5vbmNlPTAuNTM5NTI3MTg2OTcyMDYxNyZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNDc4NjA3ODcx";

    initializeSession();
  });
});

function initializeSession() {
  var session = OT.initSession(apiKey, sessionId);

  // Subscribe to a newly created stream
  session.on('streamCreated', function(event) {
    session.subscribe(event.stream, 'subscriber', {
      insertMode: 'append',
      width: '100%',
      height: '100%'
    });
  });

  session.on('sessionDisconnected', function(event) {
    console.log('You were disconnected from the session.', event.reason);
  });

  // Connect to the session
  session.connect(token, function(error) {
    // If the connection is successful, initialize a publisher and publish to the session
    if (!error) {
      var publisher = OT.initPublisher('publisher', {
        insertMode: 'append',
        width: '100%',
        height: '100%'
      });

      session.publish(publisher);
    } else {
      console.log('There was an error connecting to the session: ', error.code, error.message);
    }
  });
}

