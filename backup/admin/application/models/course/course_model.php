<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model 
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 
  <Reverse bidding system> 
    Copyright (C) <2009>  <FreeLance PHP Script>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
    If you want more information, please email me at support@freelancephpscript.com or 
    contact us from http://www.freelancephpscript.com/contact 
 
 
 */
	 class Course_model extends CI_Model {
	 
 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
	function getCourse($condition1)
	 {
	 	
		$this->db->where($condition1);
		$this->db->from('course_master');
		$this->db->join('course_type', 'course_type.id = course_master.type','left');
		$this->db->join('status_master', 'status_master.id = course_master.status','left');
		
		
		
		$this->db->select('course_master.id,course_master.name,course_master.details,course_master.type,course_type.type as typename,course_master.createdate,course_master.modifydate,course_master.status,status_master.status as status_name');
		
		//,course_master.type,course_type.type as typename,course_master.createdate,course_master.modifydate,course_master.status,status_master.status as status_name
		$result=$this->db->get()->result(); 
		//print_r($result); exit;
		return $result;
			
	 }
	 
	 function getCourses($condition1)
	 {
	 	
		$this->db->where($condition1);
		$this->db->from('course_master');
		$this->db->join('course_type', 'course_type.id = course_master.type','left');
		$this->db->join('status_master', 'status_master.id = course_master.status','left');
		$this->db->join('course_to_subCourse', 'course_to_subCourse.course_id = course_master.id','left');
		
		
		
		$this->db->select('course_master.id,course_master.name,course_master.details,course_master.type,course_type.type as typename,course_master.createdate,course_master.modifydate,course_master.status,status_master.status as status_name,course_to_subCourse.subcourse_id');
		
		//,course_master.type,course_type.type as typename,course_master.createdate,course_master.modifydate,course_master.status,status_master.status as status_name
		$result=$this->db->get()->result(); 
		//print_r($result); exit;
		return $result;
			
	 }
	 
	 function getStreams($condition1,$uniId)
	 { //die('++');
	 	
		/*$this->db->where(array('course_to_university.course_type'=>'1','course_to_university.university_id'=>$uniId));
		
		$this->db->from('course_to_university');
		$this->db->select('*');
		$result=$this->db->get()->result(); 
		//print_r($result); exit;
		$data = array();
		foreach($result as $rslt){
		array_push($data,$rslt->course_id);
		}*/
		//print_r($data); exit;
		$this->db->where($condition1);
		//$this->db->where($condition2);
		$this->db->where(array('course_to_university.course_id'=>null, 'course_to_university.university_id'=>'8'));
		//$this->db->where(array('course_to_university.university_id'=>'8'));
		//$this->db->where_not_in('course_to_university.course_id', $data);
		$this->db->from('course_master');
		$this->db->join('course_type', 'course_type.id = course_master.type','left');
		$this->db->join('status_master', 'status_master.id = course_master.status','left'); 
		$this->db->join('course_to_university', 'course_to_university.course_id = course_master.id','left');
		
		
		
		$this->db->select('course_master.id,course_master.name,course_master.details,course_master.type,course_type.type as typename,course_master.createdate,course_master.modifydate,course_master.status,status_master.status as status_name');
		
		//,course_master.type,course_type.type as typename,course_master.createdate,course_master.modifydate,course_master.status,status_master.status as status_name
		$result=$this->db->get()->result(); 
		//print_r($result); exit;
		return $result;
		
			
	 }
	 
	 function getsubCourses($condition1)
	 {
	 	
		$this->db->where($condition1);
		$this->db->from('course_to_subCourse');
		$this->db->join('course_master', 'course_master.id = course_to_subCourse.subcourse_id','left');
		$this->db->join('course_type', 'course_type.id = course_master.type','left');
		$this->db->join('status_master', 'status_master.id = course_master.status','left');
		
		
		
		$this->db->select('course_master.id,course_master.name,course_master.details,course_master.type,course_type.type as typename,course_master.createdate,course_master.modifydate,course_master.status,status_master.status as status_name');
		
		//,course_master.type,course_type.type as typename,course_master.createdate,course_master.modifydate,course_master.status,status_master.status as status_name
		
		
		
		$result=$this->db->get()->result(); 
		
		//print_r($result); exit;
		
		return $result;
			
	 }
	 
	 
	 function getUniversityCourses($condition1,$condition2)
	 {
	 	
		$this->db->where($condition1);
		$this->db->where($condition2);
		$this->db->from('course_to_university');
		$this->db->join('course_master', 'course_master.id = course_to_university.course_id','left');
		$this->db->join('course_type', 'course_type.id = course_master.type','left');
		$this->db->join('status_master', 'status_master.id = course_to_university.status','left');
		$this->db->join('course_to_subCourse', 'course_to_subCourse.subcourse_id = course_to_university.course_id','left');
		
		
		
		$this->db->select('course_to_university.id, course_master.id as idd,course_master.name,course_type.type as typename,course_master.createdate,course_master.modifydate,course_master.status,status_master.status as status_name');
		
		//,course_master.type,course_type.type as typename,course_master.createdate,course_master.modifydate,course_master.status,status_master.status as status_name
		
		
		
		$result=$this->db->get()->result(); 
		
		//print_r($result); exit;
		
		return $result;
			
	 }
	 
	 
	 function getUniversityCoursesByRepresentative($condition1,$condition2)
	 {
	 	
		$this->db->where($condition1);
		$this->db->where($condition2);
		$this->db->from('course_to_representative');
		$this->db->join('course_master', 'course_master.id = course_to_representative.course_id','left');
		$this->db->join('course_type', 'course_type.id = course_master.type','left');
		$this->db->join('status_master', 'status_master.id = course_master.status','left');
		$this->db->join('course_to_subCourse', 'course_to_subCourse.subcourse_id = course_to_representative.course_id','left');
		
		
		
		$this->db->select('course_to_representative.course_id, course_master.id as idd,course_master.name,course_type.type as typename,course_master.createdate,course_master.modifydate,course_master.status,status_master.status as status_name');
		
		//,course_master.type,course_type.type as typename,course_master.createdate,course_master.modifydate,course_master.status,status_master.status as status_name
		
		
		
		$result=$this->db->get()->result(); 
		
		//print_r($result); exit;
		
		return $result;
			
	 }
	 
	 
	 function getCoursesRelatedToDegreeByid ($conditions){
	
	 $this->db->where($conditions);
	 
	 $this->db->from('degree_to_course');
	 $this->db->join('course_master', 'course_master.id = degree_to_course.course_id','left');
	 $this->db->join('status_master', 'status_master.id = course_master.status','left');
	 $this->db->select('course_master.id,course_master.name,course_master.status,status_master.status as status_name');
		
	 $result=$this->db->get()->result();

	 return $result;
	}
	
	
	function getCoursesRelatedToUniversityDegree ($conditions){
	
	 $this->db->where($conditions);
	 
	 $this->db->from('university_to_degree_to_course');
	 $this->db->join('course_master', 'course_master.id = university_to_degree_to_course.course_id','left');
	 $this->db->join('status_master', 'status_master.id = course_master.status','left');
	 $this->db->select('course_master.id,course_master.name,course_master.status,status_master.status as status_name');
		
	 $result=$this->db->get()->result();

	 return $result;
	}
	 
	 function getAllCourseTypes()
	 {		
		$this->db->from('course_type');
		$this->db->select('*');
		$result=$this->db->get()->result(); 
		return $result;
			
	 }
	
	function updateUniversityCourses($condition,$data)
	 {
	 	$this->db->where(array('course_id'=>$condition['course_id']));
		$this->db->from('course_to_subCourse');
		$this->db->select('*');
		$result=$this->db->get()->result(); 
		
		foreach($result as $row){
		
		
		$this->db->delete('course_to_university', array('course_id' => $row->subcourse_id,'course_type' => $condition['course_type'],'university_id' => $condition['university_id'])); 
		$this->db->delete('course_to_representative', array('course_id' => $row->subcourse_id,'university_id' => $condition['university_id'])); 
		$this->db->delete('university_to_degree_to_course', array('course_id' => $row->subcourse_id,'university_id' => $condition['university_id'])); 
		}
		
		foreach($data['subcourse'] as $subcourse){	
		
		$subcoursedata = array(
        	'course_id' 	=>  $subcourse,
        	'course_type' 	=>  '2',
			'university_id'	=>  $condition['university_id'],
        	'status' 		=>	'1'
		);
		
		$this->db->insert('course_to_university',$subcoursedata); 
		
		
		//$this->course_model->insertUniversityCourse($subcoursedata);
		
		}
		
		return $this->db->insert_id();
			
	 }
	
	
	function checkUser($conditions=array())
	 {
	 	
		//echo 'aaaaaa'; die('----');
		$this->db->where($conditions);
		$this->db->from('user_master');
		$this->db->join('type_master', 'type_master.type_id = user_master.type','left');
		$this->db->join('status_master', 'status_master.id = user_master.status','left');
		
		
		
		$this->db->select('user_master.id,user_master.name,user_master.email,user_master.type,type_master.type as typename,user_master.createdate,user_master.modifydate,user_master.status,status_master.status as status_name');
		
		
		
		$result=$this->db->get()->row();
		return $result;
			
	 }
	 
	 
	 function checkUserBy_Email($conditions=array())
	 {
	 	
		
		$this->db->where($conditions);
		$this->db->from('infra_email_master');
		$this->db->join('infra_user_master', 'infra_email_master.id = infra_user_master.email','left');
		$this->db->join('infra_role_master', 'infra_role_master.id = infra_user_master.role_id','left');
		$this->db->join('infra_status_master', 'infra_status_master.id = infra_user_master.status_id','left');
		
		$this->db->select('infra_user_master.id,infra_user_master.name,infra_user_master.user_name,infra_role_master.name as role,infra_email_master.email,infra_user_master.status_id,infra_status_master.name as statusname');
		
		
		
		$result=$this->db->get()->row();
		return $result;
			
	 }
	
	
	function addRemerberme($insertData=array(),$expire)
	{
	
		 $this->auth_model->setUserCookie('uname',$insertData['username'], $expire);
		 $this->auth_model->setUserCookie('pwd',$insertData['password'], $expire);
		 
		// echo $val=get_cookie('uname',TRUE); exit;
		 echo $this->input->cookie('uname', TRUE); exit;
		 //die();
		 
	}
		
	function removeRemeberme()
	{
	
	  $this->auth_model->clearUserCookie(array('uname','pwd'));
	  
	}
	
	
	function insertCourse($insertData=array())
	 {
	  		
	 $course_master_data = array(
     'name' 		=> $insertData['name'],
     'details' 		=> $insertData['details'],
	 'type' 		=> $insertData['type'],
	 
	 'createdate' 	=> $insertData['createdate'],
	 'createdby' 	=> $insertData['createdby'],
	 'status' 		=> $insertData['status']
     );
	$this->db->insert('course_master', $course_master_data); 
	$course_id = $this->db->insert_id(); 
	
	if($insertData['parent']){
	
	$course_to_subcourse_data = array(
        	'course_id' 	=>  $insertData['parent'],
        	'subcourse_id' 	=>  $course_id
        	);
	$this->db->insert('course_to_subCourse', $course_to_subcourse_data);
	} 
	
	 return 'success';
	 
	 }
	 
	 function insertUniversityCourse($insertData=array())
	 {
	 
	 
	 $this->db->insert('course_to_university', array('course_id'=>$insertData['course_id'],'course_type'=>$insertData['course_type'],'university_id'=>$insertData['university_id'],'status' => $insertData['status'])); 
	 
	 /*foreach($insertData['subcourse'] as $subcourse){		
	 $course_to_university_data = array(
     'course_id'	=> $subcourse,    
	 'course_type' 	=> $insertData['type'],	
	 'university_id'=>$insertData['uni_id'],
	 'status' 		=> $insertData['status']
     );
	 
	 $this->db->insert('course_to_university', $course_to_university_data); 
	 
	 }*/
		//echo $this->db->insert_id(); exit;
	 return $this->db->insert_id();
	 
	 }
	
	
	function getCourseByid ($conditions){
	
	 $this->db->where($conditions);
	
	 
	 $this->db->from('course_master');
	 $this->db->join('course_type', 'course_type.id = course_master.type','left');
	 $this->db->join('status_master', 'status_master.id = course_master.status','left');
	 $this->db->join('course_to_subCourse', 'course_to_subCourse.subcourse_id = course_master.id','left');
	 
	 $this->db->select('course_master.id,course_master.name, course_master.details, course_master.type, course_type.type as typename, course_master.status, status_master.status as statusname,course_to_subCourse.course_id');
		
	 $result=$this->db->get()->row();

	 return $result;
	}
	
	
	function getParentCourse ($conditions){
	
	 $this->db->where($conditions);
	
	 
	 $this->db->from('course_master');
	 $this->db->join('course_type', 'course_type.id = course_master.type','left');
	 $this->db->join('status_master', 'status_master.id = course_master.status','left');
	 $this->db->join('course_to_subCourse', 'course_to_subCourse.course_id = course_master.id','left');
	 
	 $this->db->select('course_master.id,course_master.name, course_master.details, course_master.type, course_type.type as typename, course_master.status, status_master.status as statusname,course_to_subCourse.course_id');
		
	 $result=$this->db->get()->row();

	 return $result;
	}
	
	
	
	function editCourse($insertData=array())
	 {
	// print_r($insertData); exit;
	 // Update User table
		$course_master_data = array(
		 'name' 		=> $insertData['name'],
		 'details' 		=> $insertData['details'],
		 'type' 		=> $insertData['type'],
		 
		 
		 'modifiedby' 	=> $insertData['modifiedby'],
		 'status' 		=> $insertData['status']
		 );
	 
	 $this->db->where('id',$insertData['id']);
	 $this->db->update('course_master',$course_master_data);
	 
	 $course_id = $this->db->insert_id(); 
	
	 if($insertData['parent']){
	 	/*$this->db->delete('course_to_subCourse', array('subcourse_id' => $insertData['id'])); 
	  
		 $course_to_subcourse_data = array(
				'course_id' 	=>  $insertData['parent'],
				'subcourse_id' 	=>  $insertData['id']
				);
		 $this->db->insert('course_to_subCourse', $course_to_subcourse_data);*/
		 
		 $query = $this->db->get_where('course_to_subCourse', array('subcourse_id' => $insertData['id']))->row();
		 
				 if($query){
								
				 $this->db->where('subcourse_id',$insertData['id']);
				 $this->db->update('course_to_subCourse',array(	'course_id' =>  $insertData['parent']));
				
				 }else{
				 
				 $course_to_subcourse_data = array(
						'course_id' 	=>  $insertData['parent'],
						'subcourse_id' 	=>  $insertData['id']
						);
				 $this->db->insert('course_to_subCourse', $course_to_subcourse_data);
				 
				 }
		 }else{
	  
		  $this->db->delete('course_to_subCourse', array('subcourse_id' => $insertData['id'])); 
		  
		  } 
	  return 'success';
	 }
	 
	 function deleteCourse($id)
	 {
	  $data = array('status' => '5');  
	   
	 $this->db->where('id',$id);
	 $this->db->update('course_master',$data);
	 return 'success';
	 }
	 
	 function deleteStream($condition)
	 {
	    
		$this->db->delete('course_to_university', array('course_id' => $condition['course_id'],'course_type' => '1','university_id' => $condition['university_id'])); 
		
		$this->db->where(array('course_id'=>$condition['course_id']));
		$this->db->from('course_to_subCourse');
		$this->db->select('*');
		$result=$this->db->get()->result(); 
		
		if($result){
		foreach($result as $row){
				
		$this->db->delete('course_to_university', array('course_id' => $row->subcourse_id,'course_type' => $condition['course_type'],'university_id' => $condition['university_id'])); 
		}
		
		}
		
		return true;
		
		
	 }
	 
	 function deleteSubStream($condition)
	 {
	    
		/*$this->db->delete('course_to_university', array('course_id' => $condition['course_id'],'course_type' => '1','university_id' => $condition['university_id'])); 
		
		$this->db->where(array('course_id'=>$condition['course_id']));
		$this->db->from('course_to_subCourse');
		$this->db->select('*');
		$result=$this->db->get()->result(); 
		
		if($result){
		foreach($result as $row){
				
		$this->db->delete('course_to_university', array('course_id' => $row->subcourse_id,'course_type' => $condition['course_type'],'university_id' => $condition['university_id'])); 
		}
		
		}*/
		//$this->db->where($condition);
		
		//$this->db->delete('course_to_university');
		
		$this->db->delete('course_to_university', $condition); 
		$this->db->delete('course_to_representative', $condition); 
		$this->db->delete('university_to_degree_to_course', $condition); 
		return true;
		
		//die('+++++++++++++++++');
	 }
	
	
	function Update_User_activation($insertData=array(),$id)
	 {
	   $this->db->where('id',$id);
	   $this->db->update('infra_user_master',$insertData);
	 
	}
					   
	
	 function getSendToEmail($type)
	{
		
		$query="SELECT emailid_master.`Email` FROM `mailtemplate`,`emailid_master` WHERE 
emailid_master.`ID`=mailtemplate.`To` and mailtemplate.`MailType`='".$type."'";
		
		$result=$this->db->query($query);
		return($result->row());
	}
	
	
	function TempTop() {
	
 $top='<table width="963" border="2" cellspacing="0" cellpadding="0" align="center"       style="border-collapse:collapse; border-color:#5cd5f0">
		<tr>
		<td align="left" valign="top" style="padding:10px;">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="left" valign="top" style="background:#333;"><img src="'.base_url().'images/logo-small1.png" width="300" height="72" alt="" /></td></tr>
	</table>';
			
 return $top;
	
	             }
				 
	function TempBody($username,$link,$linktext,$text) {
	
	
 $top='<table>
		<tr>
		<td align="left" valign="top" style="padding:10px;">
		
		<p>Hi '.$username.',</p>
		
		<p>'.$text.'</p>
		
		<p style="color:#0000CC"><a href="'.$link.'">"'.$linktext.'"</a></p>
		</td>
		</tr>
	</table>';
			
 return $top;
	
	             }
	
	
function TempBottom() {
	
 $bottom='<p>Thanks and Regards,</p>
              <p>Super Admin</p>
						<strong><p><a href="'.base_url().'">TripAdapt</a></p></strong></td>
						</tr>
						</table>
						</td>
						</tr>
						</table>';
			
                return $bottom;
	
	                  }
	
	
	
	
	
	
	
	
function SendCustomEmail($P_Type,$P_To,$id)
{


    if($P_Type=="CustomerMessageToAdmin")
	{
	
   $result2=$this->BrandInfo($id);
$result1=$this->EmailInfo($P_Type);
 
 

 
$from=$result1->Email;
$to=$P_To;
$subject=$result1->Subject;
$content=$result1->Message;
$body='<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						<td align="left" valign="top" style="font:normal 14px/20px Verdana, Geneva, sans-serif; color:#333; padding:20px 0 20px 0px;">
<p>Dear Administrator,</p>
<p>You have a new message from <font style="color:#0000ff">'.$result2->username.'</font></p>
						'.$content.'
                      
';		


	}	
	
	
	
	
	
	 if($P_Type=="ChangePasswordToCustomer")
	{
	
   $result2=$this->BrandInfo($id);
$result1=$this->EmailInfo($P_Type);
 
 

 
$from=$result1->Email;
$to="sas.somnath@gmail.com";
$subject=$result1->Subject;
$content=$result1->Message;

$body='<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						<td align="left" valign="top" style="font:normal 14px/20px Verdana, Geneva, sans-serif; color:#333; padding:20px 0 20px 0px;">

						'.$content.'
						<p> Login Id:<font color="#0000ff">'.$result2->email.'</font> <br ></p>
						

				       <p> Password:<font color="#0000ff">'.$result2->passwrd.'</font></p>
					<strong>  </strong>
                      
';			


	}	
	
	
	
	
if($P_Type=="NewQueryToAdmin"){

	$result5=$this->MsgInfoTo($id); 
 $result4=$this->MsgInfo($id);       
  
$result1=$this->EmailInfo($P_Type);
 
$from=$result1->Email;
$to=$P_To;
$subject=$result1->Subject;
$content=$result1->Message;
$body='<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						<td align="left" valign="top" style="font:normal 14px/20px Verdana, 
<p>There is a new query from <font style="color:#0000ff">'.$result4['BrandName'].'</font> to <font style="color:#0000ff">'.$result5['BrandName'].'</font></p>
						'.$content.'';		


	                                             }		
	
	
	if($P_Type=="NewAdvertToAdmin"){
	

$result1=$this->EmailInfo($P_Type);
 
$from=$result1->Email;
$to=$P_To;
$subject=$result1->Subject;
$content=$result1->Message;
$body='<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						<td align="left" valign="top" style="font:normal 14px/20px Verdana, Geneva, sans-serif; color:#333; padding:20px 0 20px 0px;">
<p>Dear Administrator,</p>
<p>There is a new Advert in RangyBrands.</p>
						'.$content.'
                      
';		


	                                             }
	
	
	
$toid=$to;
$sub=$subject;
$fromid=$from;
$message=$this->TempTop().$body.$this->TempBottom();

				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= 'From: '.$fromid . "\r\n";

				$mail=mail($toid, $sub, $message, $headers);

return $mail;	
	
	
	
	
	
												 
}



	function SendMail($to,$from,$subject,$body) {
	
                //$to = $query->email;
				//$subject = "Plese Register First";
				//******body
				$message = "<html><head></head><body>";
				$message .= $this->TempTop();
				$message .= $body;
				$message .= $this->TempBottom();
				
				//$from = $this->config->item('site_admin_mail');
				
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n"; 
				$headers .= "From:" . $from;
				mail($to,$subject,$message,$headers);
	
           }


    function CheckReset($conditions){
	
	$this->db->where($conditions);
	$this->db->from('infra_user_master');
	$this->db->select('*');
	
	 $result=$this->db->get()->row();

	 return $result;
	
	}

 function getSecurity_Credential($conditions){
	
	$this->db->where($conditions);
	$this->db->from('user_security_question');
	$this->db->select('*');
	
	 $result=$this->db->get()->row();

	 return $result;
	
	}
	
}
	
	
	
	
	
	
	
	
	
	
	
	
	
?>