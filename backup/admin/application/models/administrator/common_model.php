<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model 
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 
  <Reverse bidding system> 
    Copyright (C) <2009>  <FreeLance PHP Script>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
    If you want more information, please email me at support@freelancephpscript.com or 
    contact us from http://www.freelancephpscript.com/contact 
 
 
 */
	 class Common_model extends CI_Model {
	 
 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
	

	function getFileinfo($logo)
	{
	 	$ch = curl_init($logo);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, TRUE);
		curl_setopt($ch, CURLOPT_NOBODY, TRUE);

		$data = curl_exec($ch);
		$size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
		$contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
		curl_close($ch);
		
		$logo_details = array('logo_name' => basename($logo), 'logo_type' => $contentType, 'logo_size' => $size);
	    return $logo_details;
	
	}
	
	function getFileinfo_Byid($id)
	{
	 	return $result = $this->db->get_where('infra_file_master', array('infra_file_master.id' => $id))->row();
	    /*echo '<pre>';
		print_r($result);
		echo '</pre>';
		exit();*/
	}
	
	function insertFile($insertData=array())
	{
	 	 $this->db->insert('infra_file_master', $insertData);
		 return $this->db->insert_id();
	
	}
	
	function updateFile($insertData=array(),$id)
	{
	 $this->db->where('id',$id);
	 $this->db->update('infra_file_master',$insertData);
	
	}
	
	
	function getCurrencies()
     { $this->db->from('currency_master'); //$this->table is a field with the table of categoris
       $result = $this->db->get()->result(); //fetching the result
       
	  // print_r($result); die('++++++++++');
	   return $result; 
    }
	
	
	
	function changeDate($format,$date)
     { 
	 
	  $newdate=date($format, strtotime($date));
	   return $newdate; 
    }
	 
	 
	 
}
	
	
	
	
	
	
	
	
	
	
	
	
	
?>