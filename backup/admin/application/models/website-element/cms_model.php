<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model 
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 
  <Reverse bidding system> 
    Copyright (C) <2009>  <FreeLance PHP Script>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
    If you want more information, please email me at support@freelancephpscript.com or 
    contact us from http://www.freelancephpscript.com/contact 
 
 
 */
	 class Cms_model extends CI_Model {
	 
 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
	function insertCms($insertData=array())
	 {
	  
	  // insert into infra_description_master
	 $description_tbl_data = array(
     'name' => $insertData['name'],
     'description' => $insertData['description'],
	 
     );
	 
	 $this->db->insert('infra_description_master', $description_tbl_data);
	 $description_id = $this->db->insert_id();
	
	 // insert into property_master	
	 $cms_tbl_data = array(
     'description_id' => $description_id,
     'sort_order' => $insertData['sort_order'],
	 'status' => $insertData['status']
     );
	 $this->db->insert('cmspage_master', $cms_tbl_data);
	 $cmspage_id = $this->db->insert_id();
	 
	 // insert into gallery_to_cmspage	(video)
	  foreach ($insertData['photo'] as $key1){
	 $gallery_to_cmspage_tbl_data1 = array(
	 'gallery_id' => $key1,
     'cmspage_id' => $cmspage_id
      );
	 $this->db->insert('gallery_to_cmspage', $gallery_to_cmspage_tbl_data1);
	 }
	 
	 
	// print_r($insertData['video']); exit;
	 
	 foreach ($insertData['video'] as $key2){
	 $gallery_to_cmspage_tbl_data2 = array(
	 'gallery_id' => $key2,
     'cmspage_id' => $cmspage_id
      );
	 $this->db->insert('gallery_to_cmspage', $gallery_to_cmspage_tbl_data2);
	 }
	 
	 
	 return 'success';
	 }
	 
	 
	 
	 
	 function getCms($conditions)
     { 
	 $this->db->where($conditions);
	 $this->db->from('cmspage_master');
	 $this->db->join('infra_description_master', 'cmspage_master.description_id = infra_description_master.id','left');
	 $this->db->join('infra_status_master', 'infra_status_master.id = cmspage_master.status','left');
	
	 
	 $this->db->select('cmspage_master.id,cmspage_master.status,cmspage_master.sort_order,infra_description_master.name,infra_description_master.description, infra_status_master.name as status_name');
	 
	 $result=$this->db->get()->result();
	 return $result;
	 
    }
	
	function getCmsByid($conditions)
     { 
	 $this->db->where($conditions);
	 $this->db->from('cmspage_master');
	 $this->db->join('infra_description_master', 'cmspage_master.description_id = infra_description_master.id','left');
	 $this->db->join('infra_status_master', 'infra_status_master.id = cmspage_master.status','left');
	 
	 
	 $this->db->select('cmspage_master.id,cmspage_master.status,cmspage_master.sort_order,infra_description_master.name,infra_description_master.description, infra_status_master.name as status_name');
	 
	 $result=$this->db->get()->row();
	 
	 /* echo '<pre>';
	 print_r($result);
	 echo '</pre>';	
	 exit(); */
	 return $result;
	 
    }
	
	
	
	function getPropertiesBy_Event($conditions)
     { 
	 $this->db->where($conditions);
	 $this->db->from('event_to_property');
	
	 $this->db->select('event_to_property.id,event_to_property.property_id,event_to_property.event_id');
	 
	 $result=$this->db->get()->result();

	 return $result;
	 
    }
	
	
	 
	 function getContactmaster_info($conditions)
     { 
	    $query = $this->db->get_where('infra_contact_master', $conditions)->result();
		return $query; 
		 /*echo '<pre>';
	 print_r($query);
	 echo '</pre>';	
	 exit();*/
	 
	    /*$this->db->where($conditions);
	    $this->db->from('infra_contact_master');
	    $this->db->select();
		$result=$this->db->get()->result();
		return $result; 
		
		 echo '<pre>';
	 print_r($result);
	 echo '</pre>';	
	 exit();*/
	 }
	 
	 
	 
	 
	 function editCms($insertData=array())
	 {
	 
	  $description_tbl_data = array(
     'name' => $insertData['name'],
     'description' => $insertData['description']
	 );
	 $cms_details = $this->db->get_where('cmspage_master', array('cmspage_master.id' => $insertData['id']))->row();
	
	 $description_id = $cms_details->description_id;
	 
	 // update infra_description_master
	 $this->db->where('id',$description_id);
	 $this->db->update('infra_description_master',$description_tbl_data);
	 
	 
	 $cmspage_master_data = array(
	 'status' 		=> $insertData['status'],
     'sort_order' 	=> $insertData['sort_order'],
	 );
	 
	 
	 $this->db->where('id',$insertData['id']);
	 $this->db->update('cmspage_master',$cmspage_master_data);
	 
      
	  $this->db->delete('gallery_to_cmspage', array('cmspage_id' => $insertData['id'])); 
	 
	 
	 if($insertData['photo']){
	  foreach ($insertData['photo'] as $key1){
	  $gallery_to_cmspage_data1 = array(
	 'gallery_id' => $key1,
     'cmspage_id' => $insertData['id']
      );
	  $this->db->insert('gallery_to_cmspage', $gallery_to_cmspage_data1);
	   }
	  
	  }
	  
	  
	 if($insertData['video']){
	 	 
	 foreach ($insertData['video'] as $key2){ 
	  $gallery_to_cmspage_data2 = array(
	 'gallery_id' => $key2,
     'cmspage_id' => $insertData['id']
      );
	  $this->db->insert('gallery_to_cmspage', $gallery_to_cmspage_data2);
	  }
	  }

	 return 'success';
	 }
	 
	 
	  function deleteCmspage($id)
	 {
	  $data = array('status' => '4');  
	   
	 $this->db->where('id',$id);
	 $this->db->update('cmspage_master',$data);
	 return 'success';
	 }
	
	 
}
	
	
	
	
	
	
	
	
	
	
	
	
	
?>