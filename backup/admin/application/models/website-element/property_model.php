<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model 
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 
  <Reverse bidding system> 
    Copyright (C) <2009>  <FreeLance PHP Script>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
    If you want more information, please email me at support@freelancephpscript.com or 
    contact us from http://www.freelancephpscript.com/contact 
 
 
 */
	 class Property_model extends CI_Model {
	 
 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
	function insertProperty($insertData=array())
	 {
	  
	  // insert into infra_description_master
	 $description_tbl_data = array(
     'name' => $insertData['name'],
     'description' => $insertData['description'],
	 
     );
	 
	 $this->db->insert('infra_description_master', $description_tbl_data);
	 $description_id = $this->db->insert_id();
	 
	   // insert into infra_address_master
	 $address_tbl_data = array(
     'addressline1' => $insertData['address'],
     'city_id' => $insertData['city'],
	 'postcode' => $insertData['postcode'],
	 'latitude' => $insertData['latitude'],
	 'longitude' => $insertData['longitude']
     );
	 $this->db->insert('infra_address_master', $address_tbl_data);
	 $address_id = $this->db->insert_id();
	 
	  
	  // insert into infra_email_master
	  $email_tbl_data = array(
     'email' => $insertData['email'],
     'contact_type' => '1'
     );
	 $this->db->insert('infra_email_master', $email_tbl_data);
	 $email_id = $this->db->insert_id();
	 
	 
	 // insert into infra_contact_master
	 $telephone_data = array(
     'value' => $insertData['telephone'],
     'contact_type' => '3'
     );
	 $this->db->insert('infra_contact_master', $telephone_data);
	 $telephone_id = $this->db->insert_id();
	 
	 
	 // insert into infra_contact_master
	 $fax_data = array(
     'value' => $insertData['fax'],
     'contact_type' => '2'
     );
	 
	 $this->db->insert('infra_contact_master', $fax_data);
	 $fax_id = $this->db->insert_id();
	 
	 // insert into infra_contact_master
	 $tollfree_data = array(
     'value' => $insertData['tollfree'],
     'contact_type' => '4'
     );
	 
	 $this->db->insert('infra_contact_master', $tollfree_data);
	 $tollfree_id = $this->db->insert_id();
	 
	
      // insert into infra_file_master	
	 $filemaster_tbl_data = array(
	 'file_name' 	=> $insertData['logo_name'],
     'file_type' 	=> $insertData['logo_type'],
     'path' 		=> $insertData['logo_path'],
	 'file_size'	=> $insertData['logo_size']
     );
	 $this->db->insert('infra_file_master', $filemaster_tbl_data);
	 $filemaster_id = $this->db->insert_id();
	 
	 // insert into seo_master
	 $seomaster_tbl_data = array(
	 'meta_tag' 			=> $insertData['meta_tag'],
     'meta_description' 	=> $insertData['meta_description']
     
     );
	 $this->db->insert('seo_master', $seomaster_tbl_data);
	 $seomaster_id = $this->db->insert_id();
	 
	 
	 // insert into property_master	
	 $property_tbl_data = array(
	 'title'			=> $insertData['title'],
     'description_id' 	=> $description_id,
	 'seo_id' 			=> $seomaster_id,
     'address_id' 		=> $address_id,
	 'email_id' 		=> $email_id,
	 'telephone' 		=> $telephone_id,
	 'fax' 				=> $fax_id,
	 'tollfree' 		=> $tollfree_id,
	 'logo' 			=> $filemaster_id,
	 'status_id' 		=> $insertData['status'],
	 'sort_order' 		=> $insertData['sort_order'],
	 'virtual_tour' 	=> $insertData['virtual_tour']
     );
	 $this->db->insert('property_master', $property_tbl_data);
	 $property_id = $this->db->insert_id();
	 // insert into gallery_to_property	(photo)
	 if($insertData['photo']){
	 foreach ($insertData['photo'] as $key1){
	 $gallery_to_property_tbl_data1 = array(
	 'gallery_id' => $key1,
     'property_id' => $property_id
      );
	 $this->db->insert('gallery_to_property', $gallery_to_property_tbl_data1);
	 }
	 
	 }
	 // insert into gallery_to_property	(video)
	 if($insertData['video']){
	 foreach ($insertData['video'] as $key2){
	 $gallery_to_property_tbl_data2 = array(
	 'gallery_id' => $key2,
     'property_id' => $property_id
      );
	 $this->db->insert('gallery_to_property', $gallery_to_property_tbl_data2);
	 }
	 }
	 
	 
	 // insert into property_to_nearplace
	 if($insertData['near_place']){
	 foreach ($insertData['near_place'] as $key3){
	 $property_to_nearbyplace_data3 = array(
	 'nearby_id' => $key3,
     'property_id' => $property_id
      );
	 $this->db->insert('property_to_nearplace', $property_to_nearbyplace_data3);
	 }
	 }
	 
	 // insert into amenity_to_property
	 if($insertData['amenity']){
	 foreach ($insertData['amenity'] as $key4){
	 $amenity_to_property_data = array(
	 'amenity_id' => $key4,
     'property_id' => $property_id
      );
	 $this->db->insert('amenity_to_property', $amenity_to_property_data);
	 }
	 }
	 
	 return 'success';
	 
	 }
	 
	 
	 
	 
	 function getProperties($conditions1,$conditions2)
     { 
	  if(count($conditions1)>0 || count($conditions2)>0  || isset($conditions1) || isset($conditions2)){
	  $this->db->where($conditions1);
	  $this->db->or_where($conditions2);
	  }
	 $this->db->from('property_master');
	 $this->db->join('infra_description_master', 'property_master.description_id = infra_description_master.id','left');
	 $this->db->join('infra_address_master', 'property_master.address_id = infra_address_master.id','left');
	 $this->db->join('infra_city_master', 'infra_address_master.city_id = infra_city_master.id','left');
	 $this->db->join('infra_state_master', 'infra_state_master.zone_id = infra_city_master.state_id','left');
	 $this->db->join('infra_country_master', 'infra_state_master.country_id = infra_country_master.id','left');
	 $this->db->join('infra_email_master', 'property_master.email_id = infra_email_master.id','left');
	 
	 $this->db->join('infra_status_master', 'infra_status_master.id = property_master.status_id','left');
	  $this->db->join('infra_file_master', 'infra_file_master.id = property_master.logo','left');
	 
	 $this->db->select('property_master.id,infra_description_master.name,infra_description_master.description, infra_address_master.id as addr_id,infra_address_master.addressline1,
	 infra_address_master.postcode,infra_country_master.name as country,infra_city_master.city_name as city,infra_state_master.zone_id as stateid,infra_state_master.name as statename,infra_state_master.code as statecode,infra_email_master.email, infra_status_master.name as status,property_master.telephone,property_master.fax,property_master.sort_order,infra_file_master.id as fileid, infra_file_master.file_name, infra_file_master.file_type, infra_file_master.path, infra_file_master.file_size');
	 
	 $result=$this->db->get()->result();
	 
	
	 return $result;
	 
    }
	
	function getPropertyByid($conditions)
     { 
	 $this->db->where($conditions);
	 $this->db->from('property_master');
	 $this->db->join('infra_description_master', 'property_master.description_id = infra_description_master.id','left');
	 $this->db->join('infra_address_master', 'infra_address_master.id = property_master.address_id','left');
	 $this->db->join('infra_city_master', 'infra_city_master.id = infra_address_master.city_id','left');
	$this->db->join('infra_state_master', 'infra_state_master.zone_id = infra_city_master.state_id','left');
	 $this->db->join('infra_country_master', 'infra_state_master.country_id = infra_country_master.id','left');
	 
	 $this->db->join('infra_email_master', 'infra_email_master.id = property_master.email_id','left');
	 $this->db->join('infra_status_master', 'infra_status_master.id = property_master.status_id','left');
	 $this->db->join('infra_file_master', 'infra_file_master.id = property_master.logo','left');
	 

	 $this->db->select('property_master.id,property_master.title,property_master.seo_id,infra_description_master.name,infra_description_master.description, infra_address_master.id as addr_id,infra_address_master.addressline1,
	 infra_address_master.postcode,infra_address_master.latitude,infra_address_master.longitude,infra_country_master.name as country,infra_country_master.id as country_id,infra_city_master.city_name as city,infra_city_master.id as city_id,infra_state_master.zone_id as stateid,infra_state_master.name as statename,infra_state_master.code as statecode,infra_email_master.email, infra_status_master.name as status,property_master.telephone,property_master.fax,property_master.tollfree,property_master.sort_order,property_master.virtual_tour,infra_file_master.id as fileid, infra_file_master.file_name, infra_file_master.file_type, infra_file_master.path, infra_file_master.file_size');
	 
	 $result=$this->db->get()->row();
	 
	 /* echo '<pre>';
	 print_r($result);
	 echo '</pre>';	
	 exit(); */
	 return $result;
	 
    }
	 
	 function getContactmaster_info($conditions)
     { 
	    $query = $this->db->get_where('infra_contact_master', $conditions)->result();
		return $query; 
		 
	 }
	 
	 
	 
	 
	 function editProperty($insertData=array())
	 {
	 
	 
	
	   $description_tbl_data = array(
       'name' => $insertData['property_name'],
       'description' => $insertData['property_description']
	   
       );
	 
	 $property_details = $this->db->get_where('property_master', array('property_master.id' => $insertData['property_id']))->row();
	
	 //foreach($property_details as $key):
	 $description_id 	= $property_details->description_id;
	 $seomaster_id 		= $property_details->seo_id;
	 $address_id 		= $property_details->address_id;
	 $email_id 			= $property_details->email_id;
	 $telephone 		= $property_details->telephone;
	 $tollfree 			= $property_details->tollfree;
	 $fax 				= $property_details->fax;
	 $file_id 			= $property_details->logo;
	 
	 //endforeach;
	 
	 // update infra_description_master
	 $this->db->where('id',$description_id);
	 $this->db->update('infra_description_master',$description_tbl_data);
	 
	  $filemaster_tbl_data = array(
	 'file_name' => $insertData['logo_name'],
     'file_type' => $insertData['logo_type'],
     'path' => $insertData['logo_path'],
	 'file_size' => $insertData['logo_size']
     );
	 
	 $this->db->where('id',$insertData['property_logo']);
	 $this->db->update('infra_file_master',$filemaster_tbl_data);
	 
	  // update seo_master
	  $seomaster_tbl_data = array(
	 'meta_tag' 			=> $insertData['meta_tag'],
     'meta_description' 	=> $insertData['meta_description']
     
     );
	 $this->db->where('id',$seomaster_id);
	 $this->db->update('seo_master',$seomaster_tbl_data);
	 
	 
	 $property_master_tbl_data = array(
	 'title'			=> $insertData['title'],
	 'status_id' 		=> $insertData['property_status'],
     'sort_order' 		=> $insertData['property_sort_order'],
	 'virtual_tour' 	=> $insertData['virtual_tour']
	 
     );
	 
	 // update property_master
	 $this->db->where('id',$insertData['property_id']);
	 $this->db->update('property_master',$property_master_tbl_data);
	 
	 $address_master_tbl_data = array(
	 'addressline1' => $insertData['property_address'],
	 'city_id' => $insertData['property_city'],
	 'postcode' => $insertData['property_postcode'],
	 'latitude' => $insertData['latitude'],
	 'longitude' => $insertData['longitude']
	 
	 );
	 
	 $this->db->where('id',$address_id);
	 $this->db->update('infra_address_master',$address_master_tbl_data);
	 
	 
	 // update email
	 $email_master_tbl_data = array(
	 'email' => $insertData['property_email'],
	 );
	 
	 $this->db->where('id',$email_id);
	 $this->db->update('infra_email_master',$email_master_tbl_data);
	 
	 // update telephone
	 $contact_ms_phone_tbl_data = array(
	 'value' => $insertData['property_telephone'],
	 );
	     
	 
	 $this->db->where('id',$telephone);
	 $this->db->where('contact_type','3');
	 $this->db->update('infra_contact_master',$contact_ms_phone_tbl_data);
	 
	 // update fax
	 $contact_ms_fax_tbl_data = array(
	 'value' => $insertData['property_fax'],
	 );
	 
	 $this->db->where('id',$fax);
	 $this->db->where('contact_type','2');
	 $this->db->update('infra_contact_master',$contact_ms_fax_tbl_data);
	 
	 // update tollfree
	 $contact_ms_tollfree_tbl_data = array(
	 'value' => $insertData['property_tollfree'],
	 );
	     
	 $this->db->where('id',$tollfree);
	 $this->db->where('contact_type','4');
	 $this->db->update('infra_contact_master',$contact_ms_tollfree_tbl_data);
	 
	 $islive_gallery = $this->db->get_where('gallery_to_property', array('property_id' => $insertData['property_id']))->row();
	  if($islive_gallery){
	 $this->db->delete('gallery_to_property', array('property_id' => $insertData['property_id'])); 
	 }
	 if($insertData['photo']){
	  foreach ($insertData['photo'] as $key1){
	 $gallery_to_property_tbl_data1 = array(
	 'gallery_id' 	=> $key1,
     'property_id' 	=> $insertData['property_id']
      );
	  $this->db->insert('gallery_to_property', $gallery_to_property_tbl_data1);
	  }
	  }
	  
	  
	 if($insertData['video']){ 
	 foreach ($insertData['video'] as $key2){ 
	  $gallery_to_property_tbl_data2 = array(
	 'gallery_id' 	=> $key2,
     'property_id' 	=> $insertData['property_id']
      );
	  $this->db->insert('gallery_to_property', $gallery_to_property_tbl_data2);
	  }
	  
	  }
	  
	  $islive = $this->db->get_where('property_to_nearplace', array('property_id' => $insertData['property_id']))->row();
	  if($islive){
	  $this->db->delete('property_to_nearplace', array('property_id' => $insertData['property_id'])); 
	  }
	  if($insertData['near_place']){ 
	  foreach ($insertData['near_place'] as $key3){ 
	  $property_to_nearplace_data = array(
	 'nearby_id' 	=> $key3,
     'property_id' 	=> $insertData['property_id']
      );
	  $this->db->insert('property_to_nearplace', $property_to_nearplace_data);
	  }
	  }
	 return 'success';
	 }
	 
	 
	 /*function deleteProperty($id)
	 {
	   
	 //$property_details = $this->db->get_where('property_master', array('property_master.id' => $id))->result();
//	
//	 foreach($property_details as $property):
//	 $description_id = $property->description_id;
//	 $address_id = $property->address_id;
//	 $email_id = $property->email_id;
//	 $telephone = $property->telephone;
//	 $fax = $property->fax;
//	 endforeach;
//	 
//	 
//	 
//	
//	 $this->db->delete('infra_description_master', array('id' => $description_id)); 
//	 $this->db->delete('infra_address_master', array('id' => $address_id)); 
//	 $this->db->delete('infra_email_master', array('id' => $email_id));
//	 $this->db->delete('infra_contact_master', array('id' => $telephone));
//	 $this->db->delete('infra_contact_master', array('id' => $fax));
//	 $this->db->delete('property_master', array('id' => $id)); 
//	 return 'success';
	 
	
	 
	 $property_details = $this->db->get_where('property_master', array('property_master.id' => $id))->result();
 
	foreach($property_details as $property):
	 
	$this->db->delete('property_master', array('id' => $id)); 
	 $this->db->delete('infra_description_master', array('id' => $property->description_id)); 
	  $this->db->delete('infra_address_master', array('id' => $property->address_id)); 
	 $this->db->delete('infra_email_master', array('id' => $property->email_id));
	 $this->db->delete('infra_contact_master', array('id' => $property->telephone));
	 $this->db->delete('infra_contact_master', array('id' => $property->fax));
	 $this->db->delete('infra_file_master', array('id' => $property->logo));
	 
	endforeach;
	 return 'success';
	 
	 }*/
	 
	 
	 function deleteProperty($id)
	 {
	  $data = array('status_id' => '4');  
	   
	 $this->db->where('id',$id);
	 $this->db->update('property_master',$data);
	 return 'success';
	 }
	
	 
}
	
	
	
	
	
	
	
	
	
	
	
	
	
?>