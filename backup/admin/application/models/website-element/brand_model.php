<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model 
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 
  <Reverse bidding system> 
    Copyright (C) <2009>  <FreeLance PHP Script>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
    If you want more information, please email me at support@freelancephpscript.com or 
    contact us from http://www.freelancephpscript.com/contact 
 
 
 */
	 class Brand_model extends CI_Model {
	 
 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
	function createBrand($insertData=array())
	 {
	   
		
	 $description_tbl_data = array(
     'name' => $insertData['brand_name'],
     'description' => $insertData['brand_description']
     );
	 
	 $this->db->insert('infra_description_master', $description_tbl_data);
	 $description_id = $this->db->insert_id();
	 
	  $seo_tbl_data = array(
       'meta_tag' => $insertData['brand_meta_tag'],
       'meta_description' => $insertData['brand_meta_description'],
	   
       );
	 
	 $this->db->insert('seo_master', $seo_tbl_data);
	 $seo_id = $this->db->insert_id();
	  
	 
	 
	 $filemaster_tbl_data = array(
	 'file_name' => $insertData['logo_name'],
     'file_type' => $insertData['logo_type'],
     'path' => $insertData['logo_path'],
	 'file_size' => $insertData['logo_size']
     );
	 
	 $this->db->insert('infra_file_master', $filemaster_tbl_data);
	 $filemaster_id = $this->db->insert_id();
	 
	 
	 $brandmaster_tbl_data = array(
	 'status' => '1',
     'description_id' => $description_id,
	 'seo_id' => $seo_id,
     'sort_order' => $insertData['brand_sort_order'],
	 'logo' => $filemaster_id
     );
	 $this->db->insert('brand_master', $brandmaster_tbl_data);
	  
	  $brand_id = $this->db->insert_id();
	 
	 if($insertData['properties']){
	 foreach($insertData['properties'] as $key):
	  $brandToproperty_tbl_data = array(
      'property_id' =>  $key,
      'brand_id' => $brand_id
	  );
	  $this->db->insert('brand_to_property', $brandToproperty_tbl_data);
	 endforeach;
	 }
	 
	 return 'success';
	 }
	
		
		
	function editBrand($insertData=array())
	 {
	   $description_tbl_data = array(
       'name' => $insertData['brand_name'],
       'description' => $insertData['brand_description'],
	   
       );
	 
	 $brand_details = $this->db->get_where('brand_master', array('brand_master.id' => $insertData['brand_id']))->row();
	
	 $description_id = $brand_details->description_id;
	 $file_id = $brand_details->logo;

	 
	 // update infra_description_master
	 $this->db->where('id',$description_id);
	 $this->db->update('infra_description_master',$description_tbl_data);
	 
	 $seo_tbl_data = array(
       'meta_tag' => $insertData['brand_meta_tag'],
       'meta_description' => $insertData['brand_meta_description'],
	   
       );
	  $this->db->where('id',$brand_details->seo_id);
	 $this->db->update('seo_master',$seo_tbl_data);
	 
	 $brandmaster_tbl_data = array(
	 'status' => '1',
     'sort_order' => $insertData['brand_sort_order'],
	 'logo' => $insertData['brand_logo']
     );
	 
	 // update brand_master
	 $this->db->where('id',$insertData['brand_id']);
	 $this->db->update('brand_master',$brandmaster_tbl_data);
	 
	 // update brand_to_property
	 $this->db->delete('brand_to_property', array('brand_id' => $insertData['brand_id'])); 
	 
	 if($insertData['properties']){
	 foreach($insertData['properties'] as $key):
	  $brandToproperty_tbl_data = array(
      'property_id' =>  $key,
      'brand_id' => $insertData['brand_id']
	  );
	  $this->db->insert('brand_to_property', $brandToproperty_tbl_data);
	 
	 endforeach;
	 }
	 // update infra_file_master
	 $filemaster_tbl_data = array(
	 'file_name' => $insertData['logo_name'],
     'file_type' => $insertData['logo_type'],
     'path' => $insertData['logo_path'],
	 'file_size' => $insertData['logo_size']
     );
	 
	 $this->db->where('id',$insertData['brand_logo']);
	 $this->db->update('infra_file_master',$filemaster_tbl_data);
	 
	 return 'success';
	 }
	
	
	/*function deleteBrand($id)
	 {
	   
	 $brand_details = $this->db->get_where('brand_master', array('brand_master.id' => $id))->result();
	
	 foreach($brand_details as $brnd):
	 $description_id = $brnd->description_id;
	 endforeach;
	 
	 $this->db->delete('brand_master', array('id' => $id)); 
	 $this->db->delete('infra_description_master', array('id' => $description_id)); 
	 return 'success';
	 }*/
	 
	 function deleteBrand($id)
	 {
	  $data = array('status' => '4');  
	   
	 $this->db->where('id',$id);
	 $this->db->update('brand_master',$data);
	 return 'success';
	 }
	
	
	
	
	
	function getBrands($condition1,$condition2)
     { 
	 
	 if(count($condition1)>0 || count($condition2)>0  || isset($condition1) || isset($condition2)){
	  $this->db->where($condition1);
	  $this->db->or_where($condition2);
	  }
	  
	 $this->db->from('brand_master');
	 $this->db->join('infra_description_master', 'brand_master.description_id = infra_description_master.id','left');
	 $this->db->join('infra_status_master', 'infra_status_master.id = brand_master.status','left');
	 $this->db->join('infra_file_master', 'infra_file_master.id = brand_master.logo','left');
	 
	 $this->db->select('brand_master.id,infra_description_master.name,infra_description_master.description,infra_status_master.name as stname,brand_master.sort_order,infra_file_master.id as fileid,infra_file_master.file_name,infra_file_master.file_type,infra_file_master.path,infra_file_master.file_size');
	 
	 $result=$this->db->get()->result();
	 return $result;
	 
    }
	
	function getBrandByid($conditions)
     { 
	 $this->db->where($conditions);
	 $this->db->from('brand_master');
	 $this->db->join('infra_description_master', 'brand_master.description_id = infra_description_master.id','left');
	 $this->db->join('seo_master', 'brand_master.seo_id = seo_master.id','left');

	 $this->db->join('infra_status_master', 'infra_status_master.id = brand_master.status','left');
	 $this->db->join('infra_file_master', 'infra_file_master.id = brand_master.logo','left');
	 
	 $this->db->select('brand_master.id,infra_description_master.name,infra_description_master.description, seo_master.meta_tag,seo_master.meta_description,infra_status_master.name as stname,brand_master.sort_order,infra_file_master.id as fileid,infra_file_master.file_name,infra_file_master.file_type,infra_file_master.path,infra_file_master.file_size');
	 
	 $result=$this->db->get()->result();
	 return $result;
	 
    }
	
	
	function getPropertiesBy_brand($conditions)
     { 
	 $this->db->where($conditions);
	 $this->db->from('brand_to_property');
	
	 $this->db->select('brand_to_property.id, brand_to_property.property_id, brand_to_property.brand_id');
	 
	 $result=$this->db->get()->result();

	 return $result;
	 
    }
	
	function getCountries()
     { $this->db->from('infra_country_master');
       $result = $this->db->get()->result();
       return $result; 
    }
	
	
	function getCities()
     { $this->db->from('infra_city_master');
       $result = $this->db->get()->result();
       return $result; 
    }
	
	function getCitiesByCountry($country_id)
     { 
	  $this->db->from('infra_city_master'); 
	  $this->db->where('country_id', $country_id);
      $result = $this->db->get()->result();
	  return $result; 
    }
	
	
	function updateSettings($data)
	 {
	     
		  foreach(array_keys($data) as $dt){
		  
		  $updateData = array("value"=>$data["".$dt.""]);
		  //print_r($updateData);
		   $this->db->where('key',$dt);
		   $this->db->update('site_settings',$updateData);
		 }
		
		//$this->db->update('site_settings',$updateData);
	 }
	 
	 
	 
	 function updateEmail($email,$id)
	 {
		   $updateData = array("email"=>$email);
		   $this->db->where('id',$id);
		   $this->db->update('infra_email_master',$updateData);
	 }
	 
	  function updateContact($value,$id)
	 {
		   $updateData = array("value"=>$value);
		   $this->db->where('id',$id);
		   $this->db->update('infra_contact_master',$updateData);
	 }
	 
	 
	  function getEmail($id)
	 {
	 
	 	$conditions = array('id'=> $id);
	 	$this->db->where($conditions);
	 	$this->db->select('email');
		$query = $this->db->get('infra_email_master');		
		$row   = $query->row();
		return $row->email;
	 }
	 
	  function getContact($id)
	 {
	 
	 	$conditions = array('id'=> $id);
	 	$this->db->where($conditions);
	 	$this->db->select('value');
		$query = $this->db->get('infra_contact_master');		
		$row   = $query->row();
		return $row->value;
	 }
	 
	 
	 
}
	
	
	
	
	
	
	
	
	
	
	
	
	
?>