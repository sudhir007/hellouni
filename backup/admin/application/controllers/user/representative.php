<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Representative extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
		$this->load->model('course/course_model');
    }

	
	function index()
	{   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('user/user_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }
		if($userdata['type']==1){
		$cond_user1 = array('user_master.status !=' => '5'); 
		$cond_user2 = array('user_master.type' => '4'); 			
        $this->outputData['users'] = $this->user_model->getUser($cond_user1,$cond_user2);
		}else if($userdata['type']==3){
		$cond_user1 = array('user_master.status !=' => '5'); 
		$cond_user2 = array('university_to_representative.university_id' => $userdata['id']); 			
        $this->outputData['users'] = $this->user_model->getRepresentativeByUniversity($cond_user1,$cond_user2);
		}
		$this->outputData['view'] = 'list';
		
		/*echo '<pre>';
		print_r($this->outputData['users']);
		echo '</pre>';
		exit();*/
		
		
		
		//$this->load->view('templates/user/body',$this->outputData);
        $this->render_page('templates/user/representatives',$this->outputData);
	
    } 
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('user/user_model');
		 //$this->load->model('location/country_model');
		 //$this->load->model('location/city_model'); 
		 //$this->load->model('location/state_model');
		 

		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }		
		 
		 if($this->uri->segment('4')){
		 $conditions = array('user_master.id' => $this->uri->segment('4'));
		 $user_details = $this->user_model->getUserByid($conditions);
		 
		// echo '<pre>'; print_r($user_details); echo '</pre>'; exit;
			
		$this->outputData['id'] 				= $user_details->id;
		$this->outputData['name'] 				= $user_details->name;
		$this->outputData['email'] 				= $user_details->email;
		$this->outputData['status'] 			= $user_details->status;
		$this->outputData['notification'] 		= $user_details->notification;
		$this->outputData['create'] 			= $user_details->create;
		$this->outputData['view'] 				= $user_details->view;
		$this->outputData['edit'] 				= $user_details->edit;
		$this->outputData['del'] 				= $user_details->del;
		
		
		$this->outputData['courses'] = $this->user_model->getCourseByRepresentative(array('course_to_representative.university_id'=>$userdata['id'],'course_to_representative.representative_id '=>$this->uri->segment('4')));
		
		
		
			
		}
		
		$cond_user1 = array('user_master.status' => '1'); 
		$cond_user2 = array('user_master.type' => '3'); 			
        $this->outputData['universities'] = $this->user_model->getUser($cond_user1,$cond_user2);
		
		
		$cond2 = array('course_master.status' => '1', 'course_master.type' => '1'); 
		$cond1 = array('course_to_university.university_id' => $userdata['id']); 			
        $parent_courses = $this->course_model->getUniversityCourses($cond1,$cond2);
		
		
		
		$fullCourseList = array();
		$fullCourseList[]['subcourses'] 	= array();
		$subCourses = array();
		$i = 0;
		
		foreach($parent_courses as $parent){
		
		
		 $fullCourseList[$i]['id'] 			= $parent->idd;
		 $fullCourseList[$i]['name'] 		= $parent->name;
		 $fullCourseList[$i]['status'] 		= $parent->status;
		 
		 $con2 = array('course_master.status' => '1', 'course_master.type' => '2'); 
		 $con1 = array('course_to_university.university_id' => $userdata['id'], 'course_to_subCourse.course_id' => $parent->idd); 
		 
		 
		 $subcourses = $this->course_model->getUniversityCourses($con1,$con2);
		 
		 
		 // Get courses which are already allocated to representatives of the university
		 if($this->uri->segment('4')){		 
		 
		 	$course_condition = array('course_to_representative.university_id'=>$userdata['id'],'course_to_representative.representative_id !='=>$this->uri->segment('4'));
		 
		 }else{
		 
		 	$course_condition = array('course_to_representative.university_id'=>$userdata['id']);
		 
		 }
		 
		 $allocated_courses = $this->user_model->getCourseByRepresentative($course_condition);
		 
		 // make list of courses except allready allocated by others	 
		 if($allocated_courses){		 
		 
		 foreach($allocated_courses as $allocated){
		    foreach($subcourses as $key => $value){
		   
		 
			 if ($allocated->id == $value->idd){
			 
			 unset($subcourses[$key]); //die('+++++');
			 
			  }
			 
			 }
		 
		   }
		 }
		 
		 $fullCourseList[$i]['subcourses'] 	= $subcourses;
		 
		
		
		$i++;
		
		
		}
		
				
		// Unset Main stream if there is no sub courses assigned
		foreach($fullCourseList as $key => $value){
		   
		 
			 if (!$value['subcourses']){
			 
			 unset($fullCourseList[$key]); //die('+++++');
			 
			  }
			 
			 }
			 
			 
			 
		
		
		$this->outputData['allList'] 		= $fullCourseList;
		
		/*echo '<pre>';
		print_r($allocated_courses);
		echo '</pre>'; exit;*/
		
        $this->render_page('templates/user/representative_form',$this->outputData);
	
     } 
	 
	 function create()
	 {   
	   //die('+++++');
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('user/user_model');
	   
	    $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }		
       /*$this->load->model('website-element/package_model');
       $this->load->model('administrator/common_model');
	   $this->form_validation->set_rules('name', 'Name', 'required|trim|xss_clean');
	   $this->form_validation->set_rules('user_name', 'User Name', 'required|trim|xss_clean');
	   $this->form_validation->set_rules('user_role', 'User Role', 'required|trim|xss_clean');
	   $this->form_validation->set_rules('country', 'Country', 'required|trim|xss_clean');
	   
	   $this->form_validation->set_rules('city', 'City', 'required|trim|xss_clean');
	   $this->form_validation->set_rules('address', 'Address', 'required|trim|xss_clean');
	   $this->form_validation->set_rules('postcode', 'Postcode', 'required|trim|xss_clean');
	   $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean');*/
       
	 /* if ($this->form_validation->run() == FALSE)
		{
			$this->render_page('templates/user/user_form');
		}
		else
		{ */ 
		
		
		 	$data = array(
        	'name' 			=>  $this->input->post('representativename'),
        	'email' 		=>  $this->input->post('email'),
        	'password' 		=>  md5($this->input->post('password')),
			'type' 			=>  '4',
			'createdate' 	=>  date('Y-m-d'),
			'university'	=>	$this->input->post('university'),
			'status' 		=>	$this->input->post('status'),
			'view' 			=>	$this->input->post('view'),
			'create' 		=>	$this->input->post('creation'),
			'edit' 			=>	$this->input->post('edit'),
			'del' 			=>	$this->input->post('deletion'),
			'notification' 	=>	$this->input->post('notification')
			
			
			);
			
			
			//print_r($this->input->post('courses')); exit;
			
			
			$representative_id = $this->user_model->insert_Representative($data);
			
			if($representative_id){
			
				foreach($this->input->post('courses') as $course){
			
				$courseData = array(
				'course_id' 			=>  $course,
				'representative_id' 	=>  $representative_id,
				'university_id' 		=>  $userdata['id']
				);
				
			
				$this->user_model->insertCourseToRepresentative($courseData);
			
			  }
			
			}
			
			
	 
	     if(representative_id){
		 
		 if($this->input->post('notification')==1){
			
			$from = 'superadmin@hellouni.com';
			$to = $this->input->post('email');
			$subject = "Notification on creation of your Representative Account Under HelloUni";
			
			$message = "
			<head>
			<title>Notification on creation of your Representative Account Under HelloUni</title>
			</head>
			<body>
			<p>Hi ".$this->input->post('representativename').",</p>
			<p>Your Representative Account has been sucessfully created by Super Admin under HelloUni.</p>
			<p>Your login credential is as follows -</p>
			<p>Email - ".$this->input->post('email')."</p>
			<p>Password -".$this->input->post('password')."</p>
			<p>URL - ".base_url()."</p>
			</body>
			";
			
			// Always set content-type when sending HTML email
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			
			// More headers
			$headers .= 'From:'.$from. "\r\n";
					
			mail($to,$subject,$message,$headers);
			
			}
		 $this->session->set_flashdata('flash_message', "Success: You have saved Representative!");
	     redirect('user/representative/');
	     }
	  // }
	}
	 
	 
	 function edit()
	 {   
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('user/user_model');
	   
	   $userdata=$this->session->userdata('user');
	   if(empty($userdata)){  redirect('common/login'); }		
       
	  /* $this->form_validation->set_rules('name', 'Name', 'required|trim|xss_clean');
	   
	   $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean');

	   if ($this->form_validation->run() == FALSE)
		{
		
			 $this->render_page('templates/user/admin_form');
		}
		else
		{*/
			$data = array(
        	'id'            =>  $this->uri->segment('4'),
			'name' 			=>  $this->input->post('representativename'),
        	'email' 		=>  $this->input->post('email'),
        	/*'password' 		=>  md5($this->input->post('password')),*/
			'type' 			=>  '4',
			'createdate' 	=>  date('Y-m-d'),
			'university'	=>	$this->input->post('university'),
			'status' 		=>	$this->input->post('status'),
			'view' 			=>	$this->input->post('view'),
			'create' 		=>	$this->input->post('creation'),
			'edit' 			=>	$this->input->post('edit'),
			'del' 			=>	$this->input->post('deletion'),
			'notification' 	=>	$this->input->post('notification')
			
			);
			
			if($this->input->post('password')){
			$data['password'] = md5($this->input->post('password'));
			}
			
			
		 	
			
			/*echo '<pre>';
			print_r($data);
			echo '</pre>'; 
			exit(); */
		 
		 
		 if($this->user_model->editRepresentative($data)=='success'){
		 	 
		 if($this->user_model->deleteCourseToRepresentative(array('course_to_representative.university_id'=>$userdata['id'],'course_to_representative.representative_id'=>$this->uri->segment('4')))){
		 
			 foreach($this->input->post('courses') as $course){
				
					$courseData = array(
					'course_id' 			=>  $course,
					'representative_id' 	=>  $this->uri->segment('4'),
					'university_id' 		=>  $userdata['id']
					);
					
				$this->user_model->insertCourseToRepresentative($courseData);
				
			}
		
		}
		 
		 
		 
		 
		  
		  if($this->input->post('notification')==1){
			
			$from = 'superadmin@hellouni.com';
			$to = $this->input->post('email');
			$subject = "Notification on modification of your Representative Account Under HelloUni";
			
			$message = "
			<head>
			<title>Notification on modification of your Representative Account Under HelloUni</title>
			</head>
			<body>
			<p>Hi ".$this->input->post('representativename').",</p>
			<p>Your Representative account has been sucessfully modified by Super Admin under HelloUni.</p>
			<p>Your login credential is as follows -</p>
			<p>Email - ".$this->input->post('email')."</p>
			<p>Password -".$this->input->post('password')."</p>
			<p>URL - ".base_url()."</p>
			</body>
			";
			
			// Always set content-type when sending HTML email
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			
			// More headers
			$headers .= 'From:'.$from. "\r\n";
					
			mail($to,$subject,$message,$headers);
			
			}
	 
	     $this->session->set_flashdata('flash_message', "Success: You have modified Representative!");
	      redirect('user/representative');
	     }
	 
	   //}
	}
	
	
	function delete()
	 {   
	   
	   $this->load->helper(array('form', 'url'));
	   $this->load->model('user/user_model');
		
		if($this->user_model->deleteUser($this->uri->segment('4'))=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have deleted University!");
	         redirect('user/admin');
	    }
	}
	
	
	function multidelete(){
	
	    $this->load->helper(array('form', 'url'));

		$this->load->model('user/user_model');
		
		$array = $this->input->post('chk');
		foreach($array as $id):
		
		$this->user_model->deleteUser($id);
		
		endforeach;
		$this->session->set_flashdata('flash_message', "You have deleted User!");
		 redirect('user/admin');
	
	
	}
	
	function trash()
	{   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('user/user_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }
		
		$cond_user1 = array('user_master.status ' => '5'); 
		$cond_user2 = array(); 			
        $this->outputData['users'] = $this->user_model->getUser($cond_user1,$cond_user2);
		$this->outputData['view'] = 'list';
		
        $this->render_page('templates/user/trash_admins',$this->outputData);
	
    } 
	
	function deletetrash()
	 {   
	   
	   $this->load->helper(array('form', 'url'));
	   $this->load->model('user/user_model');
		
		if($this->user_model->deleteTrash($this->uri->segment('4'))=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have deleted User!");
	         redirect('user/admin/trash');
	    }
	}
	
	function multidelete_trash(){
	
	    $this->load->helper(array('form', 'url'));

		$this->load->model('user/user_model');
		
		$array = $this->input->post('chk');
		foreach($array as $id):
		
		$this->user_model->deleteTrash($id);
		
		endforeach;
		$this->session->set_flashdata('flash_message', "You have deleted User!");
		 redirect('user/admin/trash');
	
	
	}
	
	

	
	
	 
}//End  Home Class

?>