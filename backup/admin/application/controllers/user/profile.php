<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Profile extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()

    {

         parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
        //Models
		$this->load->model('user/user_model');
		$this->load->model('location/country_model');
   }

	

	

	function index()

	{
       //$userdata=$this->session->userdata('user');
		 
		 if(empty($userdata)){  redirect('common/login'); }
		 
		 $cond_user1 = array('country_master.status ' => '1'); 
		 $conditions = array('user_master.id' => $userdata['id']);
		 
		 if($userdata['type']==3){
		die('test1');
		 $user_details = $this->user_model->getUserByid($conditions);
		// print_r($userdata); exit;
		 echo '<pre>'; print_r($user_details); echo '</pre>'; exit;
			
		$this->outputData['id'] 				= $user_details->id;
		$this->outputData['name'] 				= $user_details->name;
		$this->outputData['email'] 				= $user_details->email;
		 
		 $this->outputData['countries'] = $this->country_model->getCountries($cond_user1);
		 
		 $this->render_page('templates/user/university_profile',$this->outputData);
		 
         }else if($userdata['type']==2){
		 
		 $this->render_page('templates/user/admin_profile');
		 
		 }
	

	} 
	
	function save()

	{
       
		 
		 if(empty($userdata)){  redirect('common/login'); }
		 //print_r($userdata);
		 if($userdata['type']==3){
		 
		 
		 $conditions = array('user_master.id' => $userdata['id']);
		 $user_details = $this->user_model->getUserByid($conditions);
		 
		// echo '<pre>'; print_r($user_details); echo '</pre>'; exit;
			
		$this->outputData['id'] 				= $user_details->id;
		$this->outputData['name'] 				= $user_details->name;
		$this->outputData['email'] 				= $user_details->email;
		
		$this->render_page('templates/user/university_profile',$this->outputData);
		 
         }else if($userdata['type']==2){
		 
		 $this->render_page('templates/user/admin_profile');
		 
		 }
	

	} 


}//End  Home Class
?>

