<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class City extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

	
	function index()
	{   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('location/country_model');
		 $this->load->model('location/city_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }	
		 
		$cond_city1 = array('infra_city_master.status' => '1'); 
		$cond_city2 = array('infra_city_master.status' => '2'); 		
        $this->outputData['cities'] = $this->city_model->getCities($cond_city1,$cond_city2);
        
		
		$this->render_page('templates/location/city');
	
} 
	
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
         
		  $this->load->model('location/country_model');
		 $this->load->model('location/city_model');
		 $this->load->model('location/state_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }		
		 
		 if($this->uri->segment('4')){
		 $conditions = array(
	 				  'infra_city_master.id' => $this->uri->segment('4')
	 					);
		 $city_info = $this->city_model->getCityDetailsBy_Id($conditions);
		 
		 foreach($city_info as $city_in):
		 $this->outputData['id'] 			= $city_in->id;
		 $this->outputData['city_name'] 	= $city_in->city_name;
		 $this->outputData['state_id'] 	= $city_in->state_id;
		 $this->outputData['status'] 		= $city_in->statusname;
		 endforeach;
		}
		 
		$this->outputData['active_countries']	= $this->country_model->getActiveCountries();  
		$cond_city1 = array('infra_city_master.status' => '1'); 
		$cond_city2 = array('infra_city_master.status' => '2'); 		
        $this->outputData['cities'] = $this->city_model->getCities($cond_city1,$cond_city2); 
		
		
		$cond_state1 = array('infra_state_master.status' => '1'); 
				
        $this->outputData['states'] = $this->state_model->getStates($cond_state1,$array=array());
		
		
		//$this->outputData['states']	= $this->city_model->getCities();
		 /*echo '<pre>';
		 print_r($this->outputData['countries']);
		 echo '</pre>';
		 exit();*/
		 
		

        $this->render_page('templates/location/city_form',$this->outputData);
	
     } 
	 
	 
	 
	 function create()
	 {   
	  // die('+++++++++++++++++');
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
      
		$this->load->model('location/country_model');
		$this->load->model('location/city_model');
		$this->outputData['countries']	= $this->country_model->getCountries();   
		//$this->outputData['cities']	= $this->city_model->getCities();
		
		$this->form_validation->set_rules('city_name', 'City_Name', 'required|trim|xss_clean');
		$this->form_validation->set_rules('state', 'State', 'required|greater_than[0]');
		

		if ($this->form_validation->run() == FALSE)
		{
			// redirect('website-element/brand/form');
			  $this->render_page('templates/location/city_form');
		}
		else
		{  
		   $data = array(
     		'city_name' => $this->input->post('city_name'),
     		'state_id' => $this->input->post('state'),
     		'status' =>$this->input->post('status')
     		);
			
			//print_r($data); exit();
	 
	        if($this->city_model->insertCity($data)=='success'){
	 
	        $this->session->set_flashdata('flash_message', "Success: You have saved City!");
	        redirect('location/city/index');
	    }
	 
	   }
	}
	 
	 
	 function edit_city()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('location/country_model');
		$this->load->model('location/city_model');
		$this->load->model('administrator/common_model');
		$this->form_validation->set_rules('city_name', 'City_Name', 'required|trim|xss_clean');
		$this->form_validation->set_rules('state', 'State', 'required|greater_than[0]');
	

		if ($this->form_validation->run() == FALSE)
		{
			
			  $this->render_page('templates/location/city_form');
		}
		else
		{
			  //$logo_info = $this->common_model->getFileinfo($this->input->post('property_logo'));
			 $data = array(
			'id' => $this->uri->segment('4'),
     		'city_name' => $this->input->post('city_name'),
     		'state_id' => $this->input->post('state'),
     		'status' =>$this->input->post('status')
     		);
			
			
	         if($this->city_model->editCity($data)=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have modified City!");
	         redirect('location/city/index');
	         }
	 
	 
	    }
	}
    
	
	function delete_city()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('location/city_model');
		
		
		if($this->city_model->deleteCity($this->uri->segment('4'))=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have deleted City!");
	        redirect('location/city/index');
	        
	 
	 
	    }
	}
	
	
	function delete_mult_city(){
	
	    $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('location/city_model');
		
		$array = $this->input->post('chk');
		foreach($array as $id):
		
		$this->city_model->deleteCity($id);
		
		endforeach;
		$this->session->set_flashdata('flash_message', "Success: You have deleted City!");
		redirect('location/city/index');
	
	
	}
	 
	 
	 
	 

	

}//End  Home Class





?>