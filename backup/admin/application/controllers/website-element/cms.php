<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Cms extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

	
	function index()
	{   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('website-element/property_model');
		 $this->load->model('website-element/cms_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }	
		
		$cond = array('cmspage_master.status' => '1');
		$this->outputData['cmspages'] = $this->cms_model->getCms($cond);
        $this->render_page('templates/website-element/cms',$this->outputData);
	
} 
	
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('website-element/property_model');
		 $this->load->model('website-element/cms_model');
		 $this->load->model('plugins/gallery_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }		
		 
		 if($this->uri->segment('4')){
		 $conditions = array(
	 				  'cmspage_master.id' => $this->uri->segment('4')
	 					);
		 $cms_info = $this->cms_model->getCmsByid($conditions);
		 
		 //foreach($property_info as $prop_in):
		$this->outputData['id'] 	= $cms_info->id;
		$this->outputData['name'] 	= $cms_info->name;
		$this->outputData['description'] 	= $cms_info->description;
		
	    $this->outputData['status'] 	= $cms_info->status;
		
		// Sort order data
		$this->outputData['sort_order'] 	= $cms_info->sort_order;
		
		// Photo gallery data
		$photo_gallery = array(
	 				 'cmspage_id' => $cms_info->id,
					 'type' 		=>'photo'
	 				);
		$this->outputData['selected_photo']			=	$this->gallery_model->getGallery_By_Cmspage($photo_gallery);
		
		// Video gallery data
		$video_gallery = array(
	 				 'cmspage_id' => $cms_info->id,
					 'type' 		=>'video'
	 				);
		$this->outputData['selected_video']			=	$this->gallery_model->getGallery_By_Cmspage($video_gallery);
		//print_r($this->outputData['selected_video']);
		//exit();
		
		 }
		$cond_gallery = array('gallery_master.status' => '1'); 
		$this->outputData['photos']		=	$this->gallery_model->getPhotoGallaries($cond_gallery,$array=array());
		$this->outputData['videos']		=	$this->gallery_model->getVideoGallaries($cond_gallery,$array=array());
		$this->render_page('templates/website-element/cms_form',$this->outputData);
	
     } 
	 
	 
	 
	 function create()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/property_model'); 
		$this->load->model('website-element/cms_model');
		$this->load->model('administrator/common_model');
		
		
		$this->form_validation->set_rules('cmspage_title', 'Cms Name', 'required|trim|xss_clean');
		$this->form_validation->set_rules('cmspage_sort_order', 'Cms_sort_order', 'required|greater_than[0]');
		
		if ($this->form_validation->run() == FALSE)
		{
			// redirect('website-element/brand/form');
			  $this->render_page('templates/website-element/cms_form');
		}
		else
		{
			//$this->load->view('formsuccess');
		 
	 
	  $data = array(
     'name' 		=>	$this->input->post('cmspage_title'),
     'description'	=>	$this->input->post('cmspage_content'),
     'sort_order' 	=>	$this->input->post('cmspage_sort_order'),
	 'status' 		=>	$this->input->post('cmspage_status'),
	 'properties'	=>	$this->input->post('mult_properties'),
	 'photo' 		=>	$this->input->post('photo_gallery'),
	 'video' 		=>	$this->input->post('video_gallery')
	 );
	 
	 
	 
	 
	 if($this->cms_model->insertCms($data)=='success'){
	 
	  $this->session->set_flashdata('flash_message', "Success: You have saved CMS Page!");
	  redirect('website-element/cms/index');
	  }
	 
	 
	 }
	}
	 
	 
	 function edit()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/property_model'); 
		$this->load->model('website-element/cms_model');
		$this->load->model('administrator/common_model');
		
		$this->form_validation->set_rules('cmspage_title', 'Cms Name', 'required|trim|xss_clean');
		$this->form_validation->set_rules('cmspage_sort_order', 'Cms_sort_order', 'required|greater_than[0]');
		
		if ($this->form_validation->run() == FALSE)
		{
			
			$this->render_page('templates/website-element/cms_form');
		}
		else
		{     
			  
			  
			 $data = array(
			 	'id'			=>  $this->uri->segment('4'),
     			'name' 			=>	$this->input->post('cmspage_title'),
     			'description'	=>	$this->input->post('cmspage_content'),
     			'sort_order' 	=>	$this->input->post('cmspage_sort_order'),
	 			'status' 		=>	$this->input->post('cmspage_status'),
	 			'properties'	=>	$this->input->post('mult_properties'),
	 			'photo' 		=>	$this->input->post('photo_gallery'),
	 			'video' 		=>	$this->input->post('video_gallery')
	 			);
	 
		
	         if($this->cms_model->editCms($data)=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have modified CMS Page!");
	         redirect('website-element/cms/index');
	         }
	 
	 
	    }
	}
    
	
	function delete()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/cms_model');
		
		
		
		if($this->cms_model->deleteCmspage($this->uri->segment('4'))=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have deleted CMS Page!");
	         redirect('website-element/cms/index');
	        
	 
	 
	    }
	}
	
	
	
	function delete_mult_properties(){
	
	    $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
       $this->load->model('website-element/cms_model');
		
		$array = $this->input->post('chk');
		foreach($array as $id):
		
		$this->cms_model->deleteCmspage($id);
		
		endforeach;
		$this->session->set_flashdata('flash_message', "Success: You have deleted CMS Page!");
		redirect('website-element/cms/index');
	
	
	}
	 
	 
	 
	 

	

}//End  Home Class





?>