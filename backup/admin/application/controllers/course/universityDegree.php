<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class UniversityDegree extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

	
	function index()
	{   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('course/course_model');
		 $this->load->model('course/degree_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }
		
		$cond1 = array(); 
		$cond2 = array('degree_to_university.university_id' => $userdata['id']); 			
        			
        $this->outputData['degrees'] = $this->degree_model->getUniversitySelectedDegree($cond1,$cond2);
		
		$this->outputData['view'] = 'list';
		
		//die('++++');	
		//$this->load->view('templates/user/body',$this->outputData);
        $this->render_page('templates/course/universitydegree',$this->outputData);
	
    } 
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('course/course_model');
		 $this->load->model('course/degree_model');

		 $userdata=$this->session->userdata('user');
		 //print_r($userdata); exit;
	     if(empty($userdata)){  redirect('common/login'); }		
		 
		 $cond1 = array('degree_master.status' => '1'); 
		 $cond2 = array(); 			
        			
         $this->outputData['degrees'] = $this->degree_model->getUniversityDegree($cond1,$cond2);
		$temp_uids=array(); 
		$unique_results = array(); 
		foreach($this->outputData['degrees'] as $result) 
		{  if(!in_array($result->id,$temp_uids)) 
		   { $temp_uids[]=$result->id; 
			  $unique_results[]=$result; 
		   } 
		} 
		$this->outputData['degrees'] = $unique_results; 
		unset($temp_uids, $unique_results); 
		 
		 
		 $condition1 = array('degree_master.status' => '1'); 
		 $condition2 = array('degree_to_university.university_id' => $userdata['id']); 			
         $existDegree = $this->degree_model->getUniversityDegree($condition1,$condition2);	
		 // print_r($existDegree); exit;
		 
		 
		 if($this->uri->segment('4')){
		 
		  $found = false;
		 foreach($existDegree as $key => $var){
		  if($this->uri->segment('4') == $var->id){
		   $found = true;
           $info = 1;
		   break;
		   }		   
		}
		
		if ($found) unset($existDegree[$key]);
		 
		 $conditions = array('degree_master.id' => $this->uri->segment('4'));
		 $degree_details = $this->degree_model->getUniversityDegreeByid($conditions);
		// print_r($degree_details); exit;
					
		$this->outputData['id'] 			= $degree_details->id;
		$this->outputData['name'] 			= $degree_details->name;
		$this->outputData['status'] 		= $degree_details->status;
		////////////////////////////////////////////////////////////////////
		
		$cond = array('degree_to_course.degree_id' => $this->uri->segment('4'));
		
		//print_r($this->course_model->getCoursesRelatedToDegreeByid($cond));	exit;
		$i=0;
		$j=0;
		$id='';
		foreach($this->course_model->getCoursesRelatedToDegreeByid($cond) as $course){
		
		$universityActiveCourse = $this->course_model->getUniversityCourses(array('course_to_university.course_id'=>$course->id, 'course_to_university.university_id'=>$userdata['id'],'course_to_university.status'=>1),array());
		
		if($universityActiveCourse){
		
		$parent = $this->course_model->getParentCourse(array('course_to_subCourse.subcourse_id'=>$course->id,'course_master.status'=>1));
		
		if($parent!=null && $id!=$parent->id){
			//echo '<option style="background-color: #ffb3e5;" value="'.$parent->id.'" disabled>'.$parent->name.'</option>';
			$id = $parent->id;
			
			 $fullCourseList[$i]['id'] 			= $parent->id;
		     $fullCourseList[$i]['name'] 		= $parent->name;
			 //$fullCourseList[$i]['subcourses'] 	= array('id'=>$course->id,'name'=>$course->name);
			 $fullCourseList[$i]['subcourses'][$j] = array('id'=>$course->id,'name'=>$course->name);
			 $i++; 
		}else{
		$j++;
		$i--;
		$fullCourseList[$i]['subcourses'][$j] 	= array('id'=>$course->id,'name'=>$course->name);
		$i++;
		
		}
		//$fullCourseList[$i]['subcourses'] 	= array('id'=>$course->id,'name'=>$course->name);
		//echo '<option value="'.$course->id.'">&nbsp;&nbsp;&nbsp;----'.$course->name.'</option>';
		}
		}
		 $this->outputData['allList'] 		= $fullCourseList;
		/* echo '<pre>'; 
		 print_r($fullCourseList);
		 echo '</pre>';
		 
		 exit;*/
		
		
		////////////////////////////////////////////////////////////////////
		
		
		 
		 //$this->outputData['allList'] 		= $fullCourseList;
		
		$cond = array('university_to_degree_to_course.degree_id' => $this->uri->segment('4'),'university_to_degree_to_course.university_id'=>$userdata['id'],'course_master.status'=>1);		 
		$this->outputData['courses'] 		= $this->course_model->getCoursesRelatedToUniversityDegree($cond);
		
		//print_r($this->outputData['courses']); exit;
		}
		  
		$found = false;
		foreach($this->outputData['degrees'] as $key => $var){
		
		  foreach($existDegree as $ed){
		  if($ed->id == $var->id){
		   $found = true;
           $info = 1;
		   break;
		   }
		   
		  }
		  
		  if ($found){
		  unset($this->outputData['degrees'][$key]);
		  $found = false;
		  
		  }
		
		}
		
		
		 



		/*echo '<pre>';
		print_r($this->outputData['degrees']); echo '</pre>'; exit;*/
		//$this->outputData['cities']	= $this->city_model->getCities();
        $this->render_page('templates/course/universitydegree_form',$this->outputData);
	
     } 
	 
	 function create()
	 {   
	   //die('+++++');
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('user/user_model');
	   $this->load->model('course/course_model');
	   $this->load->model('course/degree_model');
	   
	   $userdata=$this->session->userdata('user');
	   
      	$data = array(
        	'name' 			=>  $this->input->post('name'),
        	'createdate' 	=>  date('Y-m-d'),
			'createdby' 	=>	$userdata['id'],
			'status' 		=>	$this->input->post('status'),
			'course_id' 	=> $this->input->post('stream')
			
		);
		
		//foreach($this->input->post('degree') as $degree){	
		
		$degreedata = array(
        	'degree_id' 	=>  $this->input->post('degree'),
        	'university_id'	=>  $userdata['id'],
        	'status' 		=>	$this->input->post('status'),
			
			'createdate' 	=>  date('Y-m-d'),
			'createdby' 	=>	$userdata['id'],
			
			'course_id' 	=> $this->input->post('stream')
		);
		
		$this->degree_model->insertUniversityDegree($degreedata);
		
		//}
		$this->session->set_flashdata('flash_message', "You have created Degree!");
		redirect('course/universityDegree');
	}
	 
	 
	 function edit()
	 {   
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('user/user_model');
	   $this->load->model('course/course_model');
	   $this->load->model('course/degree_model');
       
	   $userdata=$this->session->userdata('user');
	   $condition = array('degree_id'=>$this->uri->segment('4'),'university_id'=>$userdata['id']);
	   $degreedata = array(
	   					'degree_id' => $this->input->post('degree'),
						'status' =>	$this->input->post('status'),
						'university_id'=>$userdata['id'],
						'course_id' => $this->input->post('stream'),
						'modifyby' 	=> $userdata['id']
					    );
	   
	   
	   
	  
		 
		 if($this->degree_model->editUniversityDegree($condition, $degreedata)){
		 		 	 
	     $this->session->set_flashdata('flash_message', "You have modified Degree!");
	      redirect('course/universityDegree');
	     }
	 
	   
	}
	
	
	function delete()
	 {   
	   
	   $this->load->helper(array('form', 'url'));
	   $this->load->model('course/degree_model');
	   $userdata=$this->session->userdata('user');
	   
	   $condition = array('degree_id'=>$this->uri->segment('4'), 'university_id'=>$userdata['id']);
		if($this->degree_model->deleteDegree($condition)=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have deleted Degree!");
	         redirect('course/universityDegree');
	    }
	}
	
	
	function multidelete(){
	
	    $this->load->helper(array('form', 'url'));

		$this->load->model('course/degree_model');
		$userdata=$this->session->userdata('user');
		 
		$array = $this->input->post('chk');
		foreach($array as $id):
		
		$condition = array('degree_id'=>$id, 'university_id'=>$userdata['id']);
		$this->degree_model->deleteDegree($condition);
		
		endforeach;
		$this->session->set_flashdata('flash_message', "You have deleted Degrees!");
		redirect('course/universityDegree');
	
	
	}
	
	function getAllParentCourses()
	 {   
	  
	   $this->load->helper(array('form', 'url'));
	   $this->load->model('course/course_model');
		
	   if($this->input->post('typeid')==2){
		$cond1 = array('course_master.status' => '1','course_master.type' => '1','course_master.id !=' => $this->input->post('id') ); 
			
       // print_r($this->course_model->getCourse($cond1)); //exit;
		foreach($this->course_model->getCourse($cond1) as $course){
		echo '<option value="'.$course->id.'">'.$course->name.'</option>';
	    }
	 
	 }
	// print_r($this->course_model->getCourse($cond1));
	         
	   
	}
	
	function getStreamTree()
	 {   
	  
	   $this->load->helper(array('form', 'url'));
	   $this->load->model('course/course_model');
	    $userdata=$this->session->userdata('user');
		
		/*$universityActiveCourse = $this->course_model->getUniversityCourses(array('course_to_university.course_id'=>21, 'course_to_university.university_id'=>$userdata['id'],'course_to_university.status'=>1),array());
		
		print_r($universityActiveCourse); exit;
		
		$parent = $this->course_model->getParentCourse(array('course_to_subCourse.subcourse_id'=>21,'course_master.status'=>1));
		
		print_r($parent); exit;*/
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
	   //if($this->input->post('typeid')==2){
		$cond = array('degree_to_course.degree_id' => $this->input->post('degreeid'));
		//$cond = array('degree_to_course.degree_id' => $this->uri->segment('4'));
		//$this->course_model->getCoursesRelatedToDegreeByid($cond);	
		/*echo '<pre>';
        print_r($this->course_model->getCoursesRelatedToDegreeByid($cond)); 
		echo '</pre>';
		exit;*/
		foreach($this->course_model->getCoursesRelatedToDegreeByid($cond) as $course){
		
		
		
		$universityActiveCourse = $this->course_model->getUniversityCourses(array('course_to_university.course_id'=>$course->id, 'course_to_university.university_id'=>$userdata['id'],'course_to_university.status'=>1),array());
		
		//print_r($universityCourse); exit;
		
		if($universityActiveCourse){
		
		$parent = $this->course_model->getParentCourse(array('course_to_subCourse.subcourse_id'=>$course->id,'course_master.status'=>1));
		
		//print_r($parent); exit;
		if($parent!=null && $id!=$parent->id){
			echo '<option style="background-color: #ffb3e5;" value="'.$parent->id.'" disabled>'.$parent->name.'</option>';
			$id = $parent->id;
		}
		
		echo '<option value="'.$course->id.'">&nbsp;&nbsp;&nbsp;----'.$course->name.'</option>';
		
		 }
	    }
	 
	 //}
	// print_r($this->course_model->getCourse($cond1));
	         
	   
	}
	
	
	
	 
}//End  Home Class

?>