<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Intake extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
		$this->load->model('additional/additional_model');				
		$this->load->model('user/user_model');
    }

	
	function index()
	{   
	     
		 $this->load->helper('cookie_helper'); 
         //die('++++++++++++++++++++++++++++++');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }
		
		$cond = array('intake_master.status !=' => '4'); 
			
        $this->outputData['intakes'] = $this->additional_model->getIntakes($cond);
		$this->outputData['view'] = 'list';
        $this->render_page('templates/course/intake',$this->outputData);
	
    } 
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('user/user_model');
		

		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }		
		 
		 if($this->uri->segment('4')){
		 $conditions = array('intake_master.id' => $this->uri->segment('4'));
		 $intake_details = $this->additional_model->getParticularIntake($conditions);
		 
			
		$this->outputData['id'] 				= $intake_details->id;
		$this->outputData['name'] 				= $intake_details->name;		
		$this->outputData['status'] 			= $intake_details->status;
		
		}		
        
		$this->render_page('templates/course/intake_form',$this->outputData);
	
     } 
	 
	 function create()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('user/user_model');
       		
		 	$data = array(
        	'name' 			=>  $this->input->post('name'),
			'status' 		=>	$this->input->post('status')			
			);
			
			
	   $intake_id = $this->additional_model->insertIntake($data);
	     		 
		 if($intake_id){
			
		
		 $this->session->set_flashdata('flash_message', "Success: You have created Intake!");
		 
		 }
	     redirect('course/intake');
	     
	  
	}
	 
	 
	 function edit()
	 {   
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   
       if($this->uri->segment('4')){
	  
			$data = array(
        	'name' 			=>  $this->input->post('name'),
        	'status' 		=>	$this->input->post('status')			
			);
		 
		 
		 if($this->additional_model->updateIntake($data,array('intake_master.id'=>$this->uri->segment('4')))){		 
	 
	      $this->session->set_flashdata('flash_message', "Success: You have modified Intake!");
	      redirect('course/intake');
	     }
	 
	  }
	}
	
	
	function delete()
	 {   
	   
	   $this->load->helper(array('form', 'url'));
	   
	   	//echo $this->uri->segment('4'); exit;	
		$data = array('status' => 4);
		if($this->additional_model->updateIntake($data,array('intake_master.id'=>$this->uri->segment('4')))){	
	 
	         $this->session->set_flashdata('flash_message', "Success: You have deleted Intake!");
	          redirect('course/intake');
	    }
	}
	
	
	function multidelete(){
	
	    $this->load->helper(array('form', 'url'));

		
		$array = $this->input->post('chk');
		foreach($array as $id){
		
		$this->additional_model->updateIntake(array('status' => 4),array('intake_master.id'=>$id));
		
		}
		$this->session->set_flashdata('flash_message', "You have deleted Intakes!");
		 redirect('course/intake');
	
	
	}
	
	
	
	 
}//End  Home Class

?>