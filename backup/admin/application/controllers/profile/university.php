<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class University extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()

    {

         parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
        //Models
		$this->load->model('location/country_model');
		$this->load->model('user/user_model');
   }

	

	

	function index()

	{
       $userdata=$this->session->userdata('user');
	   if(!empty($usrdata)){  redirect(''); }
		$conditions = array('user_master.id' => $userdata['id']);
		  $cond_user1 = array('country_master.status ' => '1'); 
		 if($userdata['type']==3){
		//die('test1');
		 $user_details = $this->user_model->getUserByid($conditions);
		 $university_details = $this->user_model->getUniversityDetailsByid(array('university_details.university_id'=>$userdata['id']));
		// print_r($userdata); exit;
		// echo '<pre>'; print_r($university_details); echo '</pre>'; exit;
			
		$this->outputData['id'] 					= $user_details->id;
		$this->outputData['name'] 					= $user_details->name;
		$this->outputData['email'] 					= $user_details->email;
		$this->outputData['image'] 					= $user_details->image;
		$this->outputData['banner'] 					= $university_details->banner;
		if($university_details->about){
		$this->outputData['about'] 					= $university_details->about;
		}
		if($university_details->website){
		$this->outputData['website'] 				= $university_details->website;
		}
		if($university_details->student){
		$this->outputData['student'] 				= $university_details->student;
		}
		if($university_details->internationalstudent){
		$this->outputData['internationalstudent'] 	= $university_details->internationalstudent;
		}
		if($university_details->facilities){
		$this->outputData['facilities'] 			= $university_details->facilities;
		}
		if($university_details->address){
		$this->outputData['address'] 				= $university_details->address;
		}
		if($university_details->country){
		$this->outputData['countryid'] 				= $university_details->country;
		}
		if($university_details->state){
		$this->outputData['state'] 					= $university_details->state;
		}
		if($university_details->city){
		$this->outputData['city'] 					= $university_details->city;
		}
		
		$this->outputData['phone'] 					= $user_details->phone;
		
		 $this->outputData['countries'] = $this->country_model->getCountries($cond_user1);
		$this->outputData['address_details'] = $this->user_model->getUniAddress(array('address_master.user_id' => $userdata['id'])); 
		 
		// echo '<pre>'; print_r($this->outputData['address_details']); echo '</pre>'; exit;
		 $this->render_page('templates/profile/university_profile',$this->outputData);
         }
		 /*else if($userdata['type']==2){
		 
		 $this->render_page('templates/user/admin_profile');
		 
		 }*/
	

	} 
	
	function save()

	{  $this->load->helper(array('form', 'url'));
       $userdata=$this->session->userdata('user');
		 
		 if(empty($userdata)){  redirect('common/login'); }
		 //print_r($userdata);
		 if($userdata['type']==3){
		 //echo $_FILES["fileToUpload"]["tmp_name"]; 
		 //$target_dir = "upload/";
		 if($_FILES["fileToUpload"]["name"]){
		 $random_digit=round(microtime(true));
		 $temp = explode(".", $_FILES["fileToUpload"]["name"]);
		 // if(end($temp)=='gif') echo 'hi'; exit;
		 //if(end($temp)!='jpg' || end($temp)!='JPG' || end($temp)!='jpeg' || end($temp)!='JPEG' || )
		 
		 if(end($temp)!='gif' && end($temp)!='jpg' && end($temp)!='JPG' && end($temp)!='jpeg' && end($temp)!='JPEG')
		 {
		  $this->session->set_flashdata('flash_message', 'File should be in jpg/png/gif formate !');
		  redirect('profile/university');
		 }
		 
		 if ($_FILES["fileToUpload"]["size"] > 500000) {
		 $this->session->set_flashdata('flash_message', 'File should be in less than 5 MB in size!');
		  redirect('profile/university');
		 }
		 
		 $target_file = "upload/" .$userdata['name'].$random_digit.'.'.end($temp);
		 
		 copy($_FILES["fileToUpload"]["tmp_name"], $target_file);
		 } else if(!$this->input->post('profileimage')){
		 
		  $target_file = '';
		 }else{
		  $target_file = $this->input->post('profileimage');
		 }
		 
		 
   // FOR BANNR IMAGE 
   
   if($_FILES["bannerToUpload"]["name"]){
		 $banner_random_digit=round(microtime(true));
		 $banner_temp = explode(".", $_FILES["bannerToUpload"]["name"]);
		 // if(end($temp)=='gif') echo 'hi'; exit;
		 //if(end($temp)!='jpg' || end($temp)!='JPG' || end($temp)!='jpeg' || end($temp)!='JPEG' || )
		 
		 if(end($banner_temp)!='gif' && end($banner_temp)!='jpg' && end($banner_temp)!='JPG' && end($banner_temp)!='jpeg' && end($banner_temp)!='JPEG')
		 {
		  $this->session->set_flashdata('flash_message', 'Banner should be in jpg/png/gif formate !');
		  redirect('profile/university');
		 }
		 
		 if ($_FILES["bannerToUpload"]["size"] > 500000) {
		 $this->session->set_flashdata('flash_message', 'Banner should be in less than 5 MB in size!');
		  redirect('profile/university');
		 }
		 
		 $banner_target_file = "upload/" .$userdata['name'].'_banner_'.$banner_random_digit.'.'.end($banner_temp);
		 
		 copy($_FILES["bannerToUpload"]["tmp_name"], $banner_target_file);
		 } else if(!$this->input->post('banner')){
		 
		 $banner_target_file = '';
		 }else{
		 $banner_target_file = $this->input->post('banner');
		 }
		 
		 
		 
		 
		  $data = array(
        	'id'            		=>  $userdata['id'],
			'name' 					=>  $this->input->post('uniname'),
        	'email' 				=>  $this->input->post('email'),
			/*'password'		=>  md5($this->input->post('password')),*/
			/*'image' 		=>	base_url().$target_file,*/
        	'about' 				=>	$this->input->post('about'),
			
			'website' 				=>	$this->input->post('website'),
			'student' 				=>	$this->input->post('student'),
			'internationalstudent' 	=>	$this->input->post('internationalstudent'),
			'address'  				=>	$this->input->post('address'),
			'country'  				=>	$this->input->post('country'),
			'state'  				=>	$this->input->post('state'),
			'city'  				=>	$this->input->post('city'),
			
			'phone' 				=>	$this->input->post('phone'),
			'facilities' 			=>	$this->input->post('facilities')
			
			);
			
			if($target_file){
			
			$data['image'] = $target_file;
			
			}else{
			$data['image'] = '';
			} 
			
			
			if($banner_target_file){
			
			$data['banner'] = $banner_target_file;
			
			}else{
			$data['banner'] = '';
			}
			
			//print_r($data); exit;
			
			/*if($this->input->post('password')!=''){
			$data['password'] = md5($this->input->post('password'));
			}*/
			
			//print_r($this->input->post('country')); exit;
			
			/*$data['fulladdress'][] = array();
			$i = 0;
		 	foreach($this->input->post('address') as $add){
		 
		 	$data['fulladdress'][$i] = array('address'=>$add, 'country'=>$this->input->post('country')[$i]);
		 	$i++;
		 	}*/
		 	//echo '<pre>';
		  	//print_r($data['fulladdress']); echo '</pre>'; exit;
		 if($this->user_model->editUniUser($data)=='success'){
		 $this->session->set_flashdata('flash_message', 'Profile has been updated');
		 redirect('profile/university');
		  
		 }
		 
         
	}

	} 
	
	function changepassword(){
	
	$this->render_page('templates/common/changepassword');
	
	}
	
	function updatepassword(){
	
	
	 
	 $data = array('id' => $this->uri->segment(4), 'password' =>  md5($this->input->post('newpassword1')));
	 
	 if($this->user_model->changepassword($data)=='success'){
		  
		  $this->session->unset_userdata('user');
		  $this->session->set_flashdata('flash_message', 'You have successfully changed password. Please log in to continue');
		  redirect('common/login');
		  
		 }
	 
	
	
	}


}//End  Home Class
?>

