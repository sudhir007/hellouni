     <style type="text/css">.tt{ height:0px !important;}</style>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Menu
            <small>Manage Menu</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Settings</a></li>
			<li><a href="#">Website Element</a></li>
            <li class="active">Main Menu</li>
			
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
           
            <!-- right column -->
            <div class="col-md-10">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title"><?php echo $name; ?></h3>
                </div><!-- /.box-header -->
                <!-- form start -->
				<form class="form-horizontal" action="<?php echo base_url();?>elements/menu/save/<?php echo $this->uri->segment('4'); ?>" method="post" name="updatemenu" enctype="multipart/form-data">
        
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                      <div class="col-sm-10">
                        <input type="name" class="form-control" id="name" name="name" placeholder="Name" value="<?php if(isset($name)) echo $name; ?>">
                      </div>
                    </div>
					
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Alias</label>
                      <div class="col-sm-10">
                        <input type="name" class="form-control" id="alias" name="alias" placeholder="Alias" value="<?php if(isset($alias)) echo $alias; ?>" disabled="disabled">
                      </div>
                    </div>
					
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Details</label>
                      <div class="col-sm-10">
                      
						<textarea class="form-control" id="details" name="details" cols="" rows="5"><?php if(isset($details)){ echo $details; }?></textarea>
						<script type="text/javascript">CKEDITOR.replace( 'details' );</script>
                      </div>
                    </div>
					
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
					<button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                    <button type="submit" class="btn btn-info pull-right">Update</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              <!-- general form elements disabled -->
             
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     