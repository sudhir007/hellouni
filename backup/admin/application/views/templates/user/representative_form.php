

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Representative
            <small>Manage Representative Users</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Representative Management</a></li>
            <li class="active"><?php if($this->uri->segment(4)){echo "Edit";} else{echo "Add";}?> Representative</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
           
            <!-- right column -->
            <div class="col-md-10">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title"><?php if($this->uri->segment(4)){echo "Edit";} else{echo "Add";}?> Representative User</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
				
				 <?php if($this->uri->segment('4')){?>
				<form class="form-horizontal" action="<?php echo base_url();?>user/representative/edit/<?php if(isset($id)) echo $id;?>" method="post" name="editrepresentative">
				<?php }else{?>
                <form class="form-horizontal" action="<?php echo base_url();?>user/representative/create" method="post" name="addrepresentative">
                <?php } ?>
				
				
                
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                      <div class="col-sm-10">
                        <input type="name" class="form-control" id="representativename" name="representativename" placeholder="Representative Name" value="<?php if(isset($name)) echo $name; ?>">
                      </div>
                    </div>
					
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Representative Email" value="<?php if(isset($email)) echo $email; ?>">
                      </div>
                    </div>
					
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Representative Password">
                      </div>
					  </div>
                    </div>
					<?php if($this->session->userdata('user')['type']==1 || $this->session->userdata('user')['type']==2){?>
					<div class="form-group">
                  		 <label for="status" class="col-sm-2 control-label">University</label>
						  <div class="col-sm-10">
						  <select class="form-control" name="university">
													
							<?php foreach($universities as $university){ ?>
							
							<option value="<?php echo $university->id;?>"><?php echo $university->name;?></option>
														
							<?php }?>
						  </select>
						 
						  </div>
               		</div>
					
					<?php }else{ ?>
					
					 <input type="hidden" name="university" id="university" value="<?php echo $this->session->userdata('user')['id'];?>"/>
					 
					<?php } ?>
					
					<div class="form-group">
                  		 <label for="status" class="col-sm-2 control-label">Status</label>
						  <div class="col-sm-10">
						  <select class="form-control" name="status">
							<option value="1" <?php if(isset($status) && $status=='1'){ echo 'selected="selected"'; } ?>>Active</option>
							<option value="2" <?php if(isset($status) && $status=='2'){ echo 'selected="selected"'; } ?>>Inactive</option>	
						  </select>
						  </div>
               		</div>
					
					 
					<div class="form-group">
                  		 <label for="status" class="col-sm-2 control-label">Streams</label>
						  <div class="col-sm-10">
						  <select class="form-control" name="courses[]" multiple="multiple" size="10">
						 
							<?php foreach($allList as $alLst){?>
							<option style="background-color: #ffb3e5;" value="<?php echo $alLst['id']; ?>" disabled>
							
							<?php echo $alLst['name']; ?>
							
							</option>
							
							<?php foreach($alLst['subcourses'] as $subcourses){?>
							<option value="<?php echo $subcourses->idd; ?>"<?php 
							if(isset($courses)){foreach($courses as $course){ if($course->id==$subcourses->idd){echo 'selected="selected"'; break; }}}
							
							
							?>>
							&nbsp;&nbsp;&nbsp;----<?php echo $subcourses->name; ?>
							
							</option>
							<?php } ?>
							<?php } ?>
						  </select>
						  </div>
               		</div>
					    
	                <style type="text/css">.chkbx{float: left; margin: 8px 23px 1px 0px;}</style>
					<div class="form-group">
					<label for="permission" class="col-sm-2 control-label">Permission</label>
					<?php //echo $create.$view.$edit.$del; ?>
					 <div class="col-sm-10">
					  <div class="chkbx"><label>Course</label></div>
					  <div class="chkbx"><input type="checkbox" name="creation" value="1" <?php if(isset($create) && $create==true){?> checked="checked"<?php } ?>> Create</div>
					  <div class="chkbx"><input type="checkbox" name="view" value="1" <?php if(isset($view) && $view==true){?> checked="checked"<?php } ?>> View</div>
					  <div class="chkbx"><input type="checkbox" name="edit" value="1" <?php if(isset($edit) && $edit==true){?> checked="checked"<?php } ?>> Edit</div>
					 <div class="chkbx"> <input type="checkbox" name="deletion" value="1" <?php if(isset($del) && $del==true){?> checked="checked"<?php } ?>> Delete</div>
					 
					 </div>
					 </div>
					 
					 
					
					<div class="form-group">
                  		 <label for="status" class="col-sm-2 control-label">Notification</label>
						  <div class="col-sm-10">
						  <div class="radio">
							<label>
							 <input type="radio" name="notification" id="notify_yes" value="1" checked="<?php if(isset($notification)==1){echo 'checked';}?>">Yes (Send email on <?php if(!$this->uri->segment('4')){?>creation<?php }else{?>modification <?php } ?>)
							 
							</label>
						  </div>
						  <div class="radio">
							<label>
							 
							  <input type="radio" name="notification" id="notify_no" value="0" checked="<?php if(isset($notification)==0){echo 'checked';}?>">No (Don't send email on<?php if(!$this->uri->segment('4')){?> creation<?php }else{?> modification <?php } ?>)
							</label>
						  </div>
						  </div>
               		</div>
                    
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
					<button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                    <button type="submit" class="btn btn-info pull-right"> <?php if($this->uri->segment('4')) {echo 'Update';}else{ echo 'Create';} ?></button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              <!-- general form elements disabled -->
             
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     