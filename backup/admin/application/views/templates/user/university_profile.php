     <style type="text/css">.tt{ height:0px !important;}</style>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            University
            <small>Manage University Profile</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">University Management</a></li>
            <li class="active">University Profile</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
           
            <!-- right column -->
            <div class="col-md-10">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">University Profile</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
				
				<?php //$userdata = $this->session->userdata;?>
                <form class="form-horizontal" action="<?php echo base_url();?>common/profile/save/<?php if(isset($id)) echo $id; ?>" method="post" name="adduniversity" enctype="multipart/form-data">
         
				
				
                
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                      <div class="col-sm-10">
                        <input type="name" class="form-control" id="uniname" name="uniname" placeholder="Name" value="<?php if(isset($name)) echo $name; ?>">
                      </div>
                    </div>
					
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php if(isset($email)) echo $email; ?>">
                      </div>
                    </div>
					
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                      </div>
                    </div>
					
					<div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Confirm Password</label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" id="confirmpassword" name="confirmpassword" placeholder="Confirm Password">
                      </div>
                    </div>
				
					<div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Profile Image</label>
                      <style type="text/css">.customdiv{width:36%; height:36%;}</style>
					  <div class="col-sm-10 customdiv">
                  <!-- <input type="file" name="logo"/>-->
						<div id="cropContainerModal" class="">
						<?php if(isset($image)){?>
						<!--<div class="cropControls cropControlsUpload"> <i id="delimg" class="cropControlRemoveCroppedImage" style="padding-top: 55px; background-color: black;"></i> </div>-->
					 <img id="profileimg" src="<?php echo $image;?>" style="width:300px;">
						<?php } ?>
						
						</div>
			         <input type="hidden" name="logopath" id="logopath" value="<?php if(isset($image)){ echo $image; }?>"/>
                      </div>
                    </div>
					
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">About</label>
                      <div class="col-sm-10">
                      
						<textarea class="form-control" id="about" name="about" cols="" rows="5"><?php if(isset($about)){ echo $about; }?></textarea>
                      </div>
                    </div>
					
					
					
					<?php if($address_details){
					$i=0;
					 foreach($address_details as $adrs){
					
					?>
					<div>
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Address</label>
                      <div class="col-sm-10">
                        <input type="name" class="form-control" id="address" name="address[]" placeholder="Address" value="<?php echo $adrs->address;?>">
                      </div>
                    </div>
					
					
					
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Country</label>
                      <div class="col-sm-10">
                        <select class="form-control" name="country[]" id="country">
						  <?php foreach($countries as $country){?>
							<option value="<?php echo $country->id; ?>" <?php if($country->id== $adrs->country){?> selected="selected"<?php } ?>><?php echo $country->name; ?></option>
							<?php } ?>
						
						</select>
                      </div>
					  
                    </div>
					
					<?php if($i!=0){?>
					<div class="removeVar col-sm-10" style="margin-left: 87%;margin-bottom: 2%;"><button class="btn btn-block btn-danger" title="Remove Address" style="width: 9%;"><span class="glyphicon glyphicon-minus"></span></button></div></div>
					
					<?php } $i++; }  
					
					}else{?>
					
					
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Address</label>
                      <div class="col-sm-10">
                        <input type="name" class="form-control" id="address" name="address[]" placeholder="Address">
                      </div>
                    </div>
					
					
					
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Country</label>
                      <div class="col-sm-10">
                        <select class="form-control" name="country[]" id="country">
						  <?php foreach($countries as $country){?>
							<option value="<?php echo $country->id; ?>"><?php echo $country->name; ?></option>
							<?php } ?>
						
						</select>
                      </div>
                    </div>
					
					
					<?php } ?>
					<!--<p>
					<span id="addVar">Add Variable</span>
					</p>-->
					<div>
					   
					   <!--<input type="button" id="addVar" class="btn btn-block btn-info" style="width: 10%;margin-left: 45%;height: 20%;" value="Add"/>-->
					   <span id="addVar" class="glyphicon glyphicon-plus" style="margin-left: 50%;margin-bottom: 2%;font-size: 21px;cursor: pointer;background-color: aqua;width: 10%;line-height: 3; padding-left: 4%;" title="Add Address"></span>
					  
                     </div>					
					
                            
                 <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Phone</label>
                      <div class="col-sm-10">
                        <input type="name" class="form-control" id="phone" name="phone" placeholder="Phone" value="<?php if(isset($phone)){ echo $phone; }?>">
                      </div>
                    </div>
					
                    
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
					<button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                    <button type="submit" class="btn btn-info pull-right">Create</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              <!-- general form elements disabled -->
             
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     