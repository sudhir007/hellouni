<?php if($this->uri->segment('5') && $this->uri->segment('5')=='view'){

$view='disabled'; ?>

<script type="text/javascript">
$( document ).ready(function() {
$("#cleditor").cleditor()[0].disable("true");
//$(".btn .btn-primary .pull-right").attr('disabled' , true);
$('.pull-right').prop('disabled', true);

});

</script>
<?php } else { $view=''; }


 ?>
        
        <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- Statistics Button Container -->
            	
                
                <!-- Panels Start -->
				
				 <?php if($msg = $this->session->flashdata('flash_message')){?> 
				<div class="mws-form-message success"><?php echo $msg;?></div>
				 <?php } ?>
				 
				<?php if(validation_errors()){?> 
                <div class="mws-form-message error"><?php echo validation_errors(); ?></div>
                <?php } ?>

            	<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-magic"></i> User</span>
                    </div>
                    <div class="mws-panel-body no-padding">
					
					  <?php if($this->uri->segment('4')){?>
					   <form class="mws-form wzd-default" action="<?php echo base_url();?>user/user/edit/<?php if(isset($id)) echo $id;?>" method="post">
					  <?php }else{?>
                        <form class="mws-form wzd-default" action="<?php echo base_url();?>user/user/create" method="post">
                      <?php } ?>
                            <fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> General</legend>
                                <div id class="mws-form-row">
                                    <label class="mws-form-label">Name <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" <?php echo $view; ?> name="name" class="required large" value="<?php if(isset($name)) echo $name; ?>" required>
                                    </div>
                                </div>
                              
                                <div id class="mws-form-row">
                                    <label class="mws-form-label">User Name <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" <?php echo $view; ?> name="user_name" class="required large" value="<?php if(isset($user_name)) echo $user_name; ?>" required>
                                    </div>
                                </div>
								
								<div id class="mws-form-row">
                                    <label class="mws-form-label">Password <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" <?php echo $view; ?> name="password" class="required large" value="<?php //if(isset($name)) echo $name; ?>" required>
                                    </div>
                                </div>
								
								<div id class="mws-form-row">
                                    <label class="mws-form-label">Confirm Password <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" <?php echo $view; ?> name="confirm_password" class="required large" value="<?php //if(isset($name)) echo $name; ?>" required>
                                    </div>
                                </div>
								
								
								<div class="mws-form-row">
                                    <label class="mws-form-label">User Role<span class="required">*</span></label>
                                    <div class="mws-form-item">
                                     <span>Administrator</span>  
									<input type="radio" <?php echo $view; ?> name="user_role" value="1" <?php if(isset($user_role) && $user_role== '1') { echo "checked"; } ?>/>&nbsp;&nbsp;
									<span>User</span>
									<input type="radio" <?php echo $view; ?> name="user_role" value="2" <?php if(isset($user_role) && $user_role== '2') { echo "checked"; } ?>/>&nbsp;&nbsp;
                                    </div>
                                </div>
								
                                <div class="mws-form-row">
									<label class="mws-form-label">Status</label>
									 <div class="mws-form-item">
										<select name="user_status" <?php echo $view; ?>>
										<option value="1" <?php if(isset($user_status) && $user_status=='1'){ echo 'selected="selected"'; } ?>>Active</option>
										<option value="2" <?php if(isset($user_status) && $user_status=='2'){ echo 'selected="selected"'; } ?>>Inactive</option>										   										</select>
									</div>
								 </div>
							</fieldset>
							
							
							<fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-delivery"></i> Local</legend>
                                <div class="mws-form-row">
                                        <label class="mws-form-label">Country</label>
                                        <div class="mws-form-item">
                                       <select class="required large ajax_country" name="country" id="config_country" required <?php echo $view; ?>>
										<?php if($this->uri->segment('4')){ 
										    foreach($countries as $country){
											?>
										<option value="<?php echo $country->id;?>" <?php if($country->id==$selected_country){ echo 'selected="selected"' ;}  ?>><?php echo $country->name; ?></option>
											<?php }
										     } else{ 
											 foreach($countries as $country){?>
									    <option value="<?php echo $country->id;?>"><?php echo $country->name; ?></option>
											<?php } }?>
										</select>	 
                                        </div>
                                    </div>
								
								<div id class="mws-form-row">
                                    <label class="mws-form-label">State <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        
										<select class="required large ajax_state" <?php //echo $view; ?> name="user_state" id="user_state" required>
                                            <option>Select State</option>
											
											<?php 
											if($this->uri->segment('4')){
											foreach($states as $st) {
											$id = $st->id; //$id = $st->id; ?>
											<option value="<?php echo $id;?>" <?php if(isset($state_id) && $state_id==$id){echo 'selected="selected"';}?>><?php echo $st->name; ?></option>
											<?php } }?>
											
                                           
                                        </select>
                                    </div>
                                </div>
								
								<div id class="mws-form-row">
                                    <label class="mws-form-label">City <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <select class="required large ajax_city" name="city" id="config_city" required <?php echo $view; ?>>
                                            <option>Select City</option>
											<?php if($this->uri->segment('4')){?>
											<?php foreach($cities as $city) {?>
											<option value="<?php echo $city->id;?>" <?php if(isset($selected_city) && $selected_city==$city->id){echo 'selected="selected"';}?>><?php echo $city->city_name;?></option>
                                           <?php }?>
										   <?php }?>
                                           
                                        </select>
                                    </div>
                                </div>
                                <div class="mws-form-row">
                                    <label class="mws-form-label">Address<span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <textarea name="address" <?php echo $view; ?> rows="" cols="" class="required large" required><?php if(isset($address)) echo $address;?></textarea>
                                    </div>
                                </div>
								 <div class="mws-form-row">
                                    <label class="mws-form-label">Postcode <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" <?php echo $view; ?> name="postcode" class="required email large" value="<?php if(isset($postcode)) echo $postcode; ?>" required>
                                    </div>
                                </div>
								
								 <div class="mws-form-row">
                                    <label class="mws-form-label">Email<span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" <?php echo $view; ?> name="email" class="required email large" value="<?php if(isset($email)) echo $email; ?>" required>
										

                                    </div>
                                </div>
								
							 </fieldset>
							
							
							
							
							
                        </form>
                    </div>
                </div>

              

                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->
			
