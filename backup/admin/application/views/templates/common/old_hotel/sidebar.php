  <!-- Start Main Wrapper -->

    <div id="mws-wrapper">

    

    	<!-- Necessary markup, do not remove -->

		<div id="mws-sidebar-stitch"></div>

		<div id="mws-sidebar-bg"></div>

        

        <!-- Sidebar Wrapper -->

        <div id="mws-sidebar">

        

            <!-- Hidden Nav Collapse Button -->

            <div id="mws-nav-collapse">

                <span></span>

                <span></span>

                <span></span>

            </div>

            

        	<!-- Searchbox -->

        	<div id="mws-searchbox" class="mws-inset">

            	<form action="typography.html">

                	<input type="text" class="mws-search-input" placeholder="Search...">

                    <button type="submit" class="mws-search-submit"><i class="icon-search"></i></button>

                </form>

            </div>

            

            <!-- Main Navigation -->

            <div id="mws-navigation">

                <ul>

                    <li class="active"><a href="<?php echo base_url();?>common/dashboard/index"><i class="icon-home"></i> Dashboard</a></li>

                    <li><a href=""><i class="icon-official"></i> Administrator</a>
					
					 <ul><li><a href="<?php echo base_url();?>administrator/settings/index">Settings</a></li></ul>
					
					</li>
					
					<li><a href=""><i class="icon-add-contact"></i> User</a>
					
					 <ul><li><a href="<?php echo base_url();?>user/user/index">User</a></li></ul>
					
					</li>
					

                    <li><a href=""><i class="icon-graph"></i> Website Element</a>
					
<ul class="closed" style="display: none;">
					 <li><a href="<?php echo base_url();?>website-element/brand/index">Brand</a></li>
					 <li><a href="<?php echo base_url();?>website-element/property/index">Property</a></li>
					 <li><a href="<?php echo base_url();?>website-element/room/index">Room</a></li>
					 <li><a href="<?php echo base_url();?>website-element/amenity/index">Amenity</a></li>
					 <li><a href="<?php echo base_url();?>website-element/places/index">Near By Place</a></li>
					 <li><a href="<?php echo base_url();?>website-element/options/index">Option</a></li> 
					 <li><a href="<?php echo base_url();?>website-element/package/index">Package</a></li> 
					 <li><a href="<?php echo base_url();?>website-element/loyality/index">Loyality</a></li>
					 <li><a href="<?php echo base_url();?>website-element/event/index">Event</a></li>  
					 <li><a href="<?php echo base_url();?>website-element/cms/index">CMS Page</a></li>
					 <li><a href="<?php echo base_url();?>website-element/review/index">Review</a></li>
					 <li><a href="<?php echo base_url();?>website-element/attribute/index">Attribute</a></li>   
					 </ul>
					</li>

                   
                      <li><a href=""><i class="icon-plus"></i> Plugins</a>
					
					<ul><li><a href="<?php echo base_url();?>plugins/gallery/index/photo">Photo Gallery</a></li>
					<li><a href="<?php echo base_url();?>plugins/gallery/index/video">Video Gallery</a></li>
					</ul>
					
					</li>
					
					<li><a href=""><i class="icon-map-marker"></i> Location</a>
					
					 <ul>
					     <li><a href="<?php echo base_url();?>location/city/index">City</a></li>
						 <li><a href="<?php echo base_url();?>location/state/index">State</a></li>
						 <li><a href="<?php echo base_url();?>location/country/index">Country</a></li>
						 
					</ul>
					
					
					
					</li>
                 
                  

                </ul>

            </div>         

        </div>