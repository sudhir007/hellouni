     <style type="text/css">.tt{ height:0px !important;}</style>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $this->session->userdata('user')['typename'];?>
            <small>Manage <?php echo $this->session->userdata('user')['typename'];?> Password</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#"><?php echo $this->session->userdata('user')['typename'];?> Management</a></li>
            <li class="active">Change Password</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
           
            <!-- right column -->
            <div class="col-md-10">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Change Password</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
				
				<?php //$userdata = $this->session->userdata;?>
                <form class="form-horizontal" action="<?php echo base_url();?>common/profile/updatepassword/<?php //echo $this->uri->segment(4); ?>" method="post" name="changepassword" enctype="multipart/form-data">
         
				
				
                
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">New Password</label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" id="newpassword" name="newpassword" placeholder="New Password">
                      </div>
                    </div>
					
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Confirm Password</label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" id="newpassword1" name="newpassword1" placeholder="Confirm Password">
                      </div>
                    </div>
					
					</div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
					<button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                    <button type="submit" class="btn btn-info pull-right">Update</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              <!-- general form elements disabled -->
             
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     