

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Degree
            <small>Manage Degree</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Degree Management</a></li>
			<?php if(!$this->uri->segment('4')){?>
            <li class="active">Add Degree</li>
			<?php }else{?>
			 <li class="active">Modify Degree</li>
			<?php }?>
			
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
           
            <!-- right column -->
            <div class="col-md-10">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">
				  <?php if(!$this->uri->segment('4')){?>
				   Add Degree
				  <?php }else{?>
				  Modify Degree
				  <?php }?>
				  </h3>
                </div><!-- /.box-header -->
                <!-- form start -->
				
				 <?php if($this->uri->segment('4')){?>
				<form class="form-horizontal" action="<?php echo base_url();?>course/degree/edit/<?php if(isset($id)) echo $id;?>" method="post" name="addadmin">
				<?php }else{?>
                <form class="form-horizontal" action="<?php echo base_url();?>course/degree/create" method="post" name="adddegree">
                <?php } ?>
				
				
                
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                      <div class="col-sm-10">
                        <input type="name" class="form-control" id="name" name="name" placeholder="Degree Name" value="<?php if(isset($name)) echo $name; ?>">
                      </div>
                    </div>
					
					
					
                    
					
					<div class="form-group">
                  		 <label for="status" class="col-sm-2 control-label">Status</label>
						  <div class="col-sm-10">
						  <select class="form-control" name="status">
							<option value="1" <?php if(isset($status) && $status=='1'){ echo 'selected="selected"'; } ?>>Active</option>
							<option value="2" <?php if(isset($status) && $status=='2'){ echo 'selected="selected"'; } ?>>Inactive</option>	
						  </select>
						  </div>
               		</div>
					
					<?php //echo '<pre>'; print_r($allList); echo '</pre>';?>
					
					<div class="form-group">
                  		 <label for="status" class="col-sm-2 control-label">Streams</label>
						  <div class="col-sm-10">
						  <select class="form-control" name="courses[]" multiple="multiple" size="10">
						 
							<?php foreach($allList as $alLst){?>
							<option style="background-color: #ffb3e5;" value="<?php echo $alLst['id']; ?>" disabled>
							
							<?php echo $alLst['name']; ?>
							
							</option>
							
							<?php foreach($alLst['subcourses'] as $subcourses){?>
							<option value="<?php echo $subcourses->id; ?>"<?php 
							if(isset($courses)){foreach($courses as $course){ if($course->id==$subcourses->id){echo 'selected="selected"'; break; }}}
							
							
							?>>
							&nbsp;&nbsp;&nbsp;----<?php echo $subcourses->name; ?>
							
							</option>
							<?php } ?>
							<?php } ?>
						  </select>
						  </div>
               		</div>
					
               
				  </div>
				  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
					<button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                    <button type="submit" class="btn btn-info pull-right">
					 <?php if(!$this->uri->segment('4')){?>Create<?php }else{?> Update<?php }?></button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              <!-- general form elements disabled -->
             
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	  
	  	<!-- elFinder JS (REQUIRED) -->
		<script src="<?php echo base_url();?>elfinder/js/elfinder.min.js"></script>

		<!-- elFinder translation (OPTIONAL) -->
		<script src="<?php echo base_url();?>elfinder/js/i18n/elfinder.ru.js"></script>
	  <!-- elFinder initialization (REQUIRED) -->
		<script type="text/javascript" charset="utf-8">
			// Documentation for client options:
			// https://github.com/Studio-42/elFinder/wiki/Client-configuration-options
			$(document).ready(function() {
			//alert('+++');
				$('#aa').elfinder({
					url : 'elfinder/php/connector.minimal.php'  // connector URL (REQUIRED)
					// , lang: 'ru'                    // language (OPTIONAL)
				});
			});
		</script>
     