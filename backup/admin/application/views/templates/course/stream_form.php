

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Stream
            <small>Manage Stream</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Stream Management</a></li>
			<?php if(!$this->uri->segment('4')){?>
            <li class="active">Add Stream</li>
			<?php }else{?>
			 <li class="active">Modify Stream</li>
			<?php }?>
			
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
           
            <!-- right column -->
            <div class="col-md-10">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">
				  <?php if(!$this->uri->segment('4')){?>
				   Add Stream
				  <?php }else{?>
				  Modify Stream
				  <?php }?>
				  </h3>
                </div><!-- /.box-header -->
                <!-- form start -->
				
				 <?php if($this->uri->segment('4')){?>
				<form class="form-horizontal" action="<?php echo base_url();?>course/streams/edit/<?php if(isset($id)) echo $id;?>" method="post" name="addadmin">
				<?php }else{?>
                <form class="form-horizontal" action="<?php echo base_url();?>course/streams/create" method="post" name="addcourse">
                <?php } ?>
				
				
                
                  <div class="box-body"> 
                   
					
                    <div class="form-group">
                  		 <label for="status" class="col-sm-2 control-label">Main Stream</label>
						  <div class="col-sm-10">
						  <select class="form-control" name="maincourse" id="maincourse">
						  <option value="" <?php if(!isset($id)){ echo 'selected="selected"'; } ?>>Select Main Stream</option>
						  <?php foreach($courses as $course){ ?>
							<option value="<?php echo $course->id; ?>"<?php if(isset($id) && $id==$course->id){ echo 'selected="selected"'; } ?>>
							<?php echo $course->name; ?>
							</option>
							<?php } ?>
						  </select>
						  </div>
						  
						
               		</div>
					<?php if(!$this->uri->segment('4')){?>
					<div class="form-group" id="subCourses" style="display:none;">
                  		 <label for="status" class="col-sm-2 control-label">Course</label>
						  <div class="col-sm-10">
						  <select class="form-control" name="subcourse[]" id="subcourse" multiple="multiple">
						  </select>
						  </div>
						  
						
               		</div>
					<?php }else{?>
					
					<div class="form-group" id="subCourses">
                  		 <label for="status" class="col-sm-2 control-label">Course</label>
						  <div class="col-sm-10">
						  <select class="form-control" name="subcourse[]" id="subcourse" multiple="multiple">
						  <?php foreach($subcourses as $subcourse){?>
						 <option value="<?php echo $subcourse->id;?>" 
						 <?php foreach($selected_university_courses as $slc){if($slc->idd==$subcourse->id){ echo 'selected="selected"'; }} ?>>
						 <?php echo $subcourse->name;?>
						 </option> 
						  <?php }?>
						  </select>
						  </div>
						  
						
               		</div>
					
					<?php }?>
					
					<div class="form-group">
                  		 <label for="status" class="col-sm-2 control-label">Status</label>
						  <div class="col-sm-10">
						  <select class="form-control" name="status">
							<option value="1" <?php if(isset($status) && $status=='1'){ echo 'selected="selected"'; } ?>>Active</option>
							<option value="2" <?php if(isset($status) && $status=='2'){ echo 'selected="selected"'; } ?>>Inactive</option>	
						  </select>
						  </div>
               		</div>
					
                  
				  
				  
					
					
				  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
					<button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                    <button type="submit" class="btn btn-info pull-right">
					 <?php if(!$this->uri->segment('4')){?>Create<?php }else{?> Update<?php }?></button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              <!-- general form elements disabled -->
             
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	  
	  
		<script type="text/javascript" charset="utf-8">
			// Documentation for client options:
			// https://github.com/Studio-42/elFinder/wiki/Client-configuration-options
			$(document).ready(function() {
			
				$("#maincourse").change(function(){
					var maincourse_id = this.value;
					if(maincourse_id){
					var dataString = 'id='+maincourse_id;	
					
					$.ajax({
		
					type: "POST",
		
					url: "http://inventifweb.net/UNI/admin/course/streams/getSubCourses",
		
					data: dataString,
		
					cache: false,
		
					success: function(result){
					 			 
					 $('#subCourses').show();
					 $('#subcourse').html(result);
					}
		
				  });	
				  
				  }
				});
			});
		</script>
     