
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Courses
            <small>Manage Courses</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Courses Management</a></li>
            <li class="active">Courses</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              
			  <!-- /.box -->
			  
			  <?php if($this->session->flashdata('flash_message')){ ?>
				<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                <?php echo $this->session->flashdata('flash_message'); ?>
              </div>
			  <?php } ?>

              <div class="box">
			   <form action="<?php echo base_url();?>course/streams/multidelete" method="post">
                <div class="box-header">
                 <!--<a href="<?php echo base_url();?>course/streams/form"><button type="button" style="width:10%; float:left; margin-right:5px;" class="btn btn-primary"><i class="fa fa-plus"></i> Add Stream</button></a>
				 <button type="submit" class="btn btn-danger multidel" style="width:11%; display:none;"><i class="fa fa-minus"></i> Delete Stream</button> -->
                </div><!-- /.box-header -->
				
			  
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th style="width:1%;">Select</th>
                        <th style="width:40%;">Name</th>
                       
						<th>Level</th>
                        
                        <th>Status</th>
						<?php if($hasEditingPermission==1 || $hasDeletePermission==1){?>
						<th>Option</th>
						<?php } ?>
                      </tr>
                    </thead>
                    <tbody>
					<?php $count = 0; 
					foreach($university_courses as $course){ ?>
                      <tr>
                        <td align="center"><input type="checkbox" class="multichk" name="chk[]" value="<?php echo $course->idd;?>"/>&nbsp;<?php //echo $count++; ?></td>
                        <td><?php echo $course->name;?></td>
                       
						<td><?php echo $course->typename;?></td>
                        
                        <td><?php echo $course->status_name;?></td> 
						<?php if($hasEditingPermission==1 || $hasDeletePermission==1){?>
						<td>
						<?php if($hasEditingPermission==1){?>
						<a href="<?php echo base_url();?>course/substream/form/<?php echo $course->idd;?>" title="Modify"><i class="fa fa-edit"></i></a>&nbsp;
						<?php } if($hasDeletePermission==1){?> 
						<a href="javascript:void();" title="Delete" class="del" id="<?php echo $course->idd;?>"><i class="fa fa-trash-o"></i></a>
						<?php } ?> 
						</td>
						<?php } ?> 
					<?php  } ?>
                      </tr>
                      
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Sl No</th>
                        <th>Name</th>
                       
						<th>Level</th>
                      
                        <th>Status</th>
						<?php if($hasEditingPermission==1 || $hasDeletePermission==1){?>
						<th>Option</th>
						<?php } ?>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     