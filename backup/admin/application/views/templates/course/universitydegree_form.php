

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Degree
            <small>Manage Degree</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Degree Management</a></li>
			<?php if(!$this->uri->segment('4')){?>
            <li class="active">Add Degree</li>
			<?php }else{?>
			 <li class="active">Modify Degree</li>
			<?php }?>
			
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
           
            <!-- right column -->
            <div class="col-md-10">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">
				  <?php if(!$this->uri->segment('4')){?>
				   Add Degree
				  <?php }else{?>
				  Modify Degree
				  <?php }?>
				  </h3>
                </div><!-- /.box-header -->
                <!-- form start -->
				
				 <?php if($this->uri->segment('4')){?>
				<form class="form-horizontal" action="<?php echo base_url();?>course/universityDegree/edit/<?php if(isset($id)) echo $id;?>" method="post" name="addadmin">
				<?php }else{?>
                <form class="form-horizontal" action="<?php echo base_url();?>course/universityDegree/create" method="post" name="adddegree">
                <?php } ?>
				
				
                
                  <div class="box-body">
                   
				    <div class="form-group">
                  		 <label for="status" class="col-sm-2 control-label">Degree</label>
						  <div class="col-sm-10">
						  <select class="form-control" name="degree" id="degree">
						  <option>Select</option>
							<?php foreach($degrees as $degree){ ?>
							<option value="<?php echo $degree->id; ?>" <?php if(isset($id) && $id==$degree->id){ echo 'selected="selected"'; } ?>>
							
							<?php echo $degree->name; ?>
							</option>
							<?php } ?>
						  </select>
						  </div>
               		</div>
					
					<?php if(!isset($allList)){?>                  
					<div class="form-group" id="streamDiv" style="display:none;">
                  		 <label for="stream" class="col-sm-2 control-label">Stream</label>
						  <div class="col-sm-10">
						  <select class="form-control" name="stream[]" id="stream" multiple="multiple" size="10">
							
						  </select>
						  </div>
               		</div>
					
					<?php }else{ ?>
					
					<div class="form-group" id="streamDiv">
                  		 <label for="stream" class="col-sm-2 control-label">Stream</label>
						  <div class="col-sm-10">
						  <select class="form-control" name="stream[]" id="stream" multiple="multiple" size="10">
							<?php foreach($allList as $alst){?>
							
							<option style="background-color: #ffb3e5;" value="<?php echo $alst['id']; ?>" <?php //if(isset($id) && $id==$degree->id){ echo 'selected="selected"'; } ?>disabled>
							
							<?php echo $alst['name']; ?>
							</option>
							
							<?php foreach($alst['subcourses'] as $subcourses){?>
							<option value="<?php echo $subcourses['id']; ?>"<?php 
							if(isset($courses)){foreach($courses as $course){ if($course->id==$subcourses['id']){echo 'selected="selected"'; break; }}}
							
							
							?>>
							&nbsp;&nbsp;&nbsp;----<?php echo $subcourses['name']; ?>
							
							</option>
							<?php } ?>
							
							
							<?php } ?>
						  </select>
						  </div>
               		</div>
					
					
					<?php }?>
					
					<div class="form-group">
                  		 <label for="status" class="col-sm-2 control-label">Status</label>
						  <div class="col-sm-10">
						  <select class="form-control" name="status">
							<option value="1" <?php if(isset($status) && $status=='1'){ echo 'selected="selected"'; } ?>>Active</option>
							<option value="2" <?php if(isset($status) && $status=='2'){ echo 'selected="selected"'; } ?>>Inactive</option>	
						  </select>
						  </div>
               		</div>
					
               
				  </div>
				  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
					<button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                    <button type="submit" class="btn btn-info pull-right">
					 <?php if(!$this->uri->segment('4')){?>Create<?php }else{?> Update<?php }?></button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              <!-- general form elements disabled -->
             
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	  
	  	<!-- elFinder JS (REQUIRED) -->
		<script src="<?php echo base_url();?>elfinder/js/elfinder.min.js"></script>

		<!-- elFinder translation (OPTIONAL) -->
		<script src="<?php echo base_url();?>elfinder/js/i18n/elfinder.ru.js"></script>
	  <!-- elFinder initialization (REQUIRED) -->
		<script type="text/javascript" charset="utf-8">
			// Documentation for client options:
			// https://github.com/Studio-42/elFinder/wiki/Client-configuration-options
			$(document).ready(function() {
			//alert('+++');
				
			$('#degree').on('change', function() {
	
			 
			   var dataString = 'degreeid='+this.value;	
							
					$.ajax({
		
					type: "POST",
		
					url: "http://inventifweb.net/UNI/admin/course/universityDegree/getStreamTree",
		
					data: dataString,
		
					cache: false,
		
					success: function(result){
					 $('#streamDiv').fadeIn('slow');
					 $('#stream').html(result);
					}
		
				  });	
			  
	        });
			});
		</script>
     