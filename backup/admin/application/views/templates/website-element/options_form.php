<?php if($this->uri->segment('5') && $this->uri->segment('5')=='view'){

$view='disabled'; ?>

<script type="text/javascript">
$( document ).ready(function() {
$("#cleditor").cleditor()[0].disable("true");
//$(".btn .btn-primary .pull-right").attr('disabled' , true);
$('.pull-right').prop('disabled', true);

});

</script>
<?php } else { $view=''; }


 ?>
 <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- Statistics Button Container -->
            	
                
                <!-- Panels Start -->
				
				 <?php if($msg = $this->session->flashdata('flash_message')){?> 
				<div class="mws-form-message success"><?php echo $msg;?></div>
				 <?php } ?>
				 
				<?php if(validation_errors()){?> 
                <div class="mws-form-message error"><?php echo validation_errors(); ?></div>
                <?php } ?>

            	<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-magic"></i> Options</span>
                    </div>
                    <div class="mws-panel-body no-padding">
					
					  <?php if($this->uri->segment('4')){?>
					   <form class="mws-form wzd-default" action="<?php echo base_url();?>website-element/options/edit/<?php if(isset($option->id)) echo $option->id;?>" method="post">
					  <?php }else{?>
                        <form class="mws-form wzd-default" action="<?php echo base_url();?>website-element/options/create" method="post">
                      <?php } ?>
                            <fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> General</legend>
                                <div id class="mws-form-row">
                                    <label class="mws-form-label">Name <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" <?php echo $view; ?> name="option_name" class="required small" value="<?php if(isset($option->name)) echo $option->name; ?>" required>
                                    </div>
                                </div>
                              
								 <div class="mws-form-row">
                                    <label class="mws-form-label">Type</label>
                                    <div class="mws-form-item">
										<select name="option_type" <?php echo $view; ?>>
										<option value="text" <?php if(isset($option->type) && $option->type=='text'){ echo 'selected="selected"'; } ?>>Text</option>
										<option value="textarea" <?php if(isset($option->type) && $option->type=='textarea'){ echo 'selected="selected"'; } ?>>Textarea</option>
										<option value="radio" <?php if(isset($option->type) && $option->type=='radio'){ echo 'selected="selected"'; } ?>>Radio</option>										                                        <option value="dropdown" <?php if(isset($option->type) && $option->type=='dropdown'){ echo 'selected="selected"'; } ?>>Dropdown</option>
										<option value="checkbox" <?php if(isset($option->type) && $option->type=='checkbox'){ echo 'selected="selected"'; } ?>>Checkbox</option>
										<option value="date" <?php if(isset($option->type) && $option->type=='date'){ echo 'selected="selected"'; } ?>>Date</option>
										</select>
                                    </div>
                                </div>
								
								
								
								
								    
								
								
								
							</fieldset>
                        </form>
                    </div>
                </div>

              

                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->
			
