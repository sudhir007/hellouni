<?php if($this->uri->segment('5') && $this->uri->segment('5')=='view'){

$view='disabled'; ?>

<script type="text/javascript">
$( document ).ready(function() {
$("#cleditor").cleditor()[0].disable("true");
//$(".btn .btn-primary .pull-right").attr('disabled' , true);
$('.pull-right').prop('disabled', true);

});

</script>
<?php } else { $view=''; }


 ?>
        
        <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- Statistics Button Container -->
            	
                
                <!-- Panels Start -->
				
				 <?php if($msg = $this->session->flashdata('flash_message')){?> 
				<div class="mws-form-message success"><?php echo $msg;?></div>
				 <?php } ?>
				 <?php if(validation_errors()){?> 
    <div class="mws-form-message error"><?php echo validation_errors(); ?></div>
     <?php } ?>

            	<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-magic"></i> CMS Page</span>
                    </div>
                    <div class="mws-panel-body no-padding">
					
					
                        <?php if($this->uri->segment('4')){?>
						<form class="mws-form wzd-default" action="<?php echo base_url();?>website-element/cms/edit/<?php if(isset($id)) echo $id;?>" method="post">
						<?php }else{?>
						<form class="mws-form wzd-default" action="<?php echo base_url();?>website-element/cms/create" method="post" enctype="multipart/form-data">
                        <?php }?>    
                            <fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> General</legend>
                                <div id class="mws-form-row">
                                    <label class="mws-form-label">Tilte <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" <?php echo $view; ?> name="cmspage_title" class="required large" value="<?php if(isset($name)) echo $name; ?>" required>
                                    </div>
                                </div>
                              
							<div class="mws-form-row">
                            	 <label class="mws-form-label">Page Content</label>
                                <div class="mws-form-item">
                                	<textarea name="cmspage_content" id="cleditor" class="large"><?php if(isset($description)) echo $description; ?></textarea>
                                </div>
                            </div>	
								
								
								
								
								
								
                                 <div class="mws-form-row">
                                    <label class="mws-form-label">Sort Order</label>
                                    <div class="mws-form-item">
                                        <input type="text" <?php echo $view; ?> name="cmspage_sort_order" class="required email large" value="<?php if(isset($sort_order)) echo $sort_order; ?>">
                                    </div>
                                </div>
								
								<div class="mws-form-row">
                                    <label class="mws-form-label">Status</label>
                                    <div class="mws-form-item">
										<select name="cmspage_status" <?php echo $view; ?>>
										<option value="1" <?php if(isset($status) && $status=='Active'){ echo 'selected="selected"'; } ?>>Active</option>
										<option value="2" <?php if(isset($status) && $status=='Inactive'){ echo 'selected="selected"'; } ?>>Inactive</option>										   
										</select>
                                    </div>
                                </div>
								
                            </fieldset>
                            
                            <fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> Links</legend>
                                 
                              
                                 <div class="mws-form-row">
                                    <label class="mws-form-label">Photo Gallery</label>
                                    <div class="mws-form-item">
										<select name="photo_gallery[]" multiple="multiple" size="10" class="large" <?php echo $view; ?>>    
										<?php if($this->uri->segment('4')){ 
										    foreach($photos as $photo){
											?>
										<option value="<?php echo $photo->id;?>" <?php foreach($selected_photo as $sp){ if($photo->id==$sp->id){ echo 'selected="selected"' ; break; } } ?>><?php echo $photo->name; ?></option>
											<?php }
										     } else{  foreach($photos as $photo) {?>
											<option value="<?php echo $photo->id;?>" ><?php echo $photo->name;?></option>
                                        <?php }}?>									   
										</select>
                                    </div>
                                </div>
								
								 <div class="mws-form-row">
                                    <label class="mws-form-label">Video Gallery</label>
                                    <div class="mws-form-item">
										<select name="video_gallery[]" multiple="multiple" size="10" class="large" <?php echo $view; ?>>    
										<?php if($this->uri->segment('4')){ 
										    foreach($videos as $video){
											?>
										<option value="<?php echo $video->id;?>" <?php foreach($selected_video as $sv){ if($video->id==$sv->id){ echo 'selected="selected"' ; break; } } ?>><?php echo $video->name; ?></option>
											<?php }
										     } else{ 
											      foreach($videos as $video) {?>
											<option value="<?php echo $video->id;?>"><?php echo $video->name;?></option>
                                        <?php  }}?>									   
										</select>
                                    </div>
                                </div>
								
								
                            </fieldset>
                            
                            
							
							
							
							
                      
					   
					    </form>
                    </div>
                </div>

              
                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->
			
