
        
        <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- Statistics Button Container -->
            	
                
                <!-- Panels Start -->
				
				 <?php if($msg = $this->session->flashdata('flash_message')){?> 
				<div class="mws-form-message success"><?php echo $msg;?></div>
				 <?php } ?>
				 <?php if(validation_errors()){?> 
    <div class="mws-form-message error"><?php echo validation_errors(); ?></div>
     <?php } ?>

            	<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-magic"></i> Brand</span>
                    </div>
                    <div class="mws-panel-body no-padding">
					
					
                        <?php if($this->uri->segment('4')){?>
						<form class="mws-form wzd-default" action="<?php echo base_url();?>website-element/brand/edit_brand/<?php if(isset($id)) echo $id;?>" method="post">
						<?php }else{?>
						<form class="mws-form wzd-default" action="<?php echo base_url();?>website-element/brand/create_brand" method="post">
                        <?php }?>    
                            <fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> General</legend>
                                <div id class="mws-form-row">
                                    <label class="mws-form-label">Brand Name <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" name="brand_name" class="required large" value="<?php if(isset($name)) echo $name; ?>" required>
                                    </div>
                                </div>
                              
                                <div class="mws-form-row">
                                    <label class="mws-form-label">Brand Description</label>
                                    <div class="mws-form-item">
                                        <textarea name="brand_description" rows="" cols="" class="required large"><?php if(isset($description)) echo $description; ?></textarea>
                                    </div>
                                </div>
                                 <div class="mws-form-row">
                                    <label class="mws-form-label">Sort Order</label>
                                    <div class="mws-form-item">
                                        <input type="text" name="brand_sort_order" class="required email large" value="<?php if(isset($sort_order)) echo $sort_order; ?>">
                                    </div>
                                </div>
								
                            </fieldset>
                            
                            
                            
                            <fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> Image</legend>
                                <div id class="mws-form-row">
                                    <label class="mws-form-label">Brand Logo <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                       <div class="image">
									   <img src="<?php echo base_url();?>images/no_image.jpg" alt="" id="thumb0">
									   <input type="hidden" name="product_image[0][image]" value="" id="image0"><br>
									   <input type="button" id="mws-jui-dialog-btn" class="btn btn-danger" value="Browse">
                        
                            
                            <div id="mws-jui-dialog">
                        		<div class="mws-dialog-inner">
                            		
                                </div>
                            </div>
                                    </div>
                                </div>
                              
                               <div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-archive"></i> File Manager</span>
                    </div>
                    <div class="mws-panel-body no-padding no-border">
                        <div style="display: none;"></div><div id="elfinder" class="el-finder"><div class="el-finder-toolbar"><ul></ul></div><div class="el-finder-workzone"><div class="el-finder-nav ui-resizable ui-resizable-autohide" style="height: 300px;"><ul class="el-finder-places" style="display: none;"><li><a href="#" class="el-finder-places-root"><div></div>Places</a><ul></ul></li></ul><div class="ui-resizable-handle ui-resizable-e" style="z-index: 1000; display: none;"></div><ul class="el-finder-tree"></ul></div><div class="el-finder-cwd" unselectable="on" style="height: 300px;"></div><div class="el-finder-spinner" style="display: none;"></div><p class="el-finder-err" style="display: none;"><strong>Unable to connect to backend!</strong></p><div style="clear:both"></div></div><div class="el-finder-statusbar"><div class="el-finder-path"></div><div class="el-finder-stat"></div><div class="el-finder-sel"></div></div></div>
                    </div>
				</div>
                            </fieldset>
							
							
							
							
                      
					   
					    </form>
                    </div>
                </div>

              

                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->
			
