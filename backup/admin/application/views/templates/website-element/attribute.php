
    
        <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- Statistics Button Container -->
            	
                
                <!-- Panels Start -->
                 
            	 <?php  if($msg = $this->session->flashdata('flash_message')){?> 
    <div class="mws-form-message success"><?php  echo $msg;?></div>
     <?php  } ?>
	 
	 
	  <?php if($ermsg = $this->session->flashdata('error_message')){?> 
    <div class="mws-form-message error"><?php  echo $ermsg;?></div>
     <?php } ?>
				<div class="mws-panel grid_8">
				<form action="<?php echo base_url();?>website-element/attribute/delete_mult" method="post">
                	<div class="mws-panel-header">
					
                    	<span><i class="icon-table"></i> Attribute
						<a href="<?php echo base_url();?>website-element/attribute/form"><button type="button" class="btn btn-small">Insert</button></a>
						<button type="submit" name="delete-Attribute" class="btn btn-small delbtn" id="del">Delete</button>
						
						</span>
						
					
						
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-datatable-fn mws-table">
                            <thead>
                                <tr>
                                    <th style="width: 1px;">Select</th>
									
									<th>Name</th>
                                  
                                    
                                    <th>Sort Order</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($attributes as $attr):?>
								<tr>
                                    <td align="center"><input type="checkbox" name="chk[]" class="amenity_chk" id="<?php echo $attr->id;?>" value="<?php echo $attr->id;?>"/></td>
									<td><?php echo $attr->name;?></td>
                                    
									<td><?php echo $attr->sort_order;?></td>
									<td class=" ">
                                        <span class="btn-group">
                                           
										   <a class="btn btn-small" href="<?php echo base_url();?>website-element/attribute/form/<?php echo $attr->id;?>/view"><i class="icon-search"></i></a>
										   
                                            <a class="btn btn-small" href="<?php echo base_url();?>website-element/attribute/form/<?php echo $attr->id;?>"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small delete-row" id="<?php echo base_url(); ?>website-element/attribute/delete/<?php echo $attr->id;?>" href="javascript:void(o);"><i class="icon-trash"></i></a></span>
                                    </td>
                                </tr>
                               <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
					
					</form>
                </div>

                                 

                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->
			
