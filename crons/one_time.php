<?php
require dirname(__FILE__) . '/../application/mongo/autoload.php';
date_default_timezone_set('Asia/Calcutta');

// Mongo Settings
//$mongo_server = 'localhost';
$mongo_server = '13.233.46.52';
$mongo_port = '27017';
$mongo_client = 'mongodb://' . $mongo_server . ':' . $mongo_port;
$client = new MongoDB\Client($mongo_client);
$mongo_db = $client->HelloUni;

// MySQL Settings
$mysql_host = 'edu.cjnafqapdygx.ap-south-1.rds.amazonaws.com';
$mysql_user = 'eduloans';
$mysql_pass = 'eduloans123';
$mysql_db = 'hellouni_live';

/*
$mysql_host = 'localhost';
$mysql_user = 'root';
$mysql_pass = 'root123';
$mysql_db = 'hellouni';
*/

$conn = mysqli_connect($mysql_host, $mysql_user, $mysql_pass, $mysql_db);
if(!$conn)
{
    die(mysqli_connect_error());
}

// User Secion Starts (One Time)

$user_collection = $mongo_db->selectCollection('users');
//$user_count = $user_collection->count();
$user_count = 0;
$user_collection->drop();
$user_collection = $mongo_db->selectCollection('users');

$event_name = 'VIRTUAL-FAIR-29-FEB-2024';

/*$query_select_users = "SELECT * FROM user_master WHERE registration_type = '$event_name'";
$query_select_users_obj = mysqli_query($conn, $query_select_users);
if(!$query_select_users_obj)
{
    echo $query_select_users;
    die(mysqli_error($conn));
}

while($row = mysqli_fetch_assoc($query_select_users_obj))
{
    foreach($row as $field => $value)
    {
        if(in_array($field, array('type', 'id', 'status', 'link_allowed')))
        {
            $row[$field] = (int)$value;
        }
        else
        {
            $row[$field] = iconv('UTF-8', 'UTF-8//IGNORE', $value);
        }
    }
    $insertOneResult = $user_collection->insertOne($row);
}*/

// User Section Ends

// Seminar Students Section Starts (One Time)

$seminar_students_collection = $mongo_db->selectCollection('seminar_students');
$seminar_students_collection->drop();

$demo_collection = $mongo_db->selectCollection('demo');
$demo_collection->drop();

$query_select_seminar_students = "SELECT * FROM seminar_students WHERE current_use = '$event_name'";
$query_select_seminar_students_obj = mysqli_query($conn, $query_select_seminar_students);
if(!$query_select_seminar_students_obj)
{
    echo $query_select_seminar_students;
    die(mysqli_error($conn));
}

while($row = mysqli_fetch_assoc($query_select_seminar_students_obj))
{
    foreach($row as $field => $value)
    {
        if(in_array($field, array('user_id', 'id', 'tokbox_id')))
        {
            $row[$field] = (int)$value;
        }
        else
        {
            $row[$field] = iconv('UTF-8', 'UTF-8//IGNORE', $value);
        }
    }

    $insertOneResult = $seminar_students_collection->insertOne($row);
    //$row['id'] = $row['user_id'];
    $insertOneResult = $user_collection->insertOne($row);

    $demo['username'] = $row['username'];
    $demo_collection->insertOne($row);
}

// Seminar Students Section Ends


// Tokens Section Starts (One Time)
$connection_tokens = $mongo_db->selectCollection('tokbox_token');
$connection_tokens->drop();

$query_select_tokens = "SELECT * FROM tokbox_token WHERE type = '$event_name'";
$query_select_tokens_obj = mysqli_query($conn, $query_select_tokens);
if(!$query_select_tokens_obj)
{
    echo $query_select_tokens;
    die(mysqli_error($conn));
}

while($row = mysqli_fetch_assoc($query_select_tokens_obj))
{
    foreach($row as $field => $value)
    {
        $row[$field] = iconv('UTF-8', 'UTF-8//IGNORE', $value);

        switch($field)
        {
            case 'id':
            case 'force_start':
                $row[$field] = (int)$value;
                break;
        }
    }
    $insertOneResult = $connection_tokens->insertOne($row);
}
// Tokens Section Ends

/*
// Fair University Section Starts (One Time)
$fair_universities = $mongo_db->selectCollection('fair_universities');
$fair_universities->drop();

$query_select_fair_universities = "SELECT * FROM fair_universities";
$query_select_fair_universities_obj = mysqli_query($conn, $query_select_fair_universities);
if(!$query_select_fair_universities_obj)
{
    echo $query_select_fair_universities;
    die(mysqli_error($conn));
}

while($row = mysqli_fetch_assoc($query_select_fair_universities_obj))
{
    foreach($row as $field => $value)
    {
        $row[$field] = iconv('UTF-8', 'UTF-8//IGNORE', $value);
        if($field == 'id' || $field == 'order')
        {
            $row[$field] = (int)$value;
        }
    }
    $insertOneResult = $fair_universities->insertOne($row);
}
// Fair University Section Ends
 */

// Fair Delegates Section Starts (One Time)
$fair_delegates = $mongo_db->selectCollection('fair_delegates');
$fair_delegates->drop();

$query_select_fair_delegates = "SELECT fd.*, fu.name AS university_name FROM fair_delegates fd LEFT JOIN fair_universities fu ON fu.id = fd.fair_university_id WHERE fd.event = '$event_name' AND fd.active = 1";
$query_select_fair_delegates_obj = mysqli_query($conn, $query_select_fair_delegates);
if(!$query_select_fair_delegates_obj)
{
    echo $query_select_fair_delegates;
    die(mysqli_error($conn));
}

while($row = mysqli_fetch_assoc($query_select_fair_delegates_obj))
{
    foreach($row as $field => $value)
    {
        $row[$field] = iconv('UTF-8', 'UTF-8//IGNORE', $value);

        switch($field)
        {
            case 'id':
            case 'fair_university_id':
            case 'tokbox_token_id':
            case 'mapping_id':
                $row[$field] = (int)$value;
                break;
        }
    }
    $insertOneResult = $fair_delegates->insertOne($row);
}
// Fair Delegates Section Ends

/*
// College Categories Section Starts (One Time)
$college_categories = $mongo_db->selectCollection('colleges_categories');
$college_categories->drop();

$query_select_college_categories = "SELECT * FROM colleges_categories";
$query_select_college_categories_obj = mysqli_query($conn, $query_select_college_categories);
if(!$query_select_college_categories_obj)
{
    echo $query_select_college_categories;
    die(mysqli_error($conn));
}

while($row = mysqli_fetch_assoc($query_select_college_categories_obj))
{
    foreach($row as $field => $value)
    {
        $row[$field] = iconv('UTF-8', 'UTF-8//IGNORE', $value);

        switch($field)
        {
            case 'id':
                $row[$field] = (int)$value;
                break;
        }
    }
    $insertOneResult = $college_categories->insertOne($row);
}
// College Categories Section Ends
 */

/*
// Fair Events Section Starts (One Time)
$fair_events = $mongo_db->selectCollection('fair_events');
$fair_events->drop();

$query_select_fair_events = "SELECT * FROM fair_events";
$query_select_fair_events_obj = mysqli_query($conn, $query_select_fair_events);
if(!$query_select_fair_events_obj)
{
    echo $query_select_fair_events;
    die(mysqli_error($conn));
}

while($row = mysqli_fetch_assoc($query_select_fair_events_obj))
{
    foreach($row as $field => $value)
    {
        $row[$field] = iconv('UTF-8', 'UTF-8//IGNORE', $value);

        switch($field)
        {
            case 'id':
            case 'tokbox_id':
                $row[$field] = (int)$value;
                break;
        }
    }
    $insertOneResult = $fair_events->insertOne($row);
}
// Fair Events Section Ends
 */
mysqli_close($conn);
?>
