<?php
require dirname(__FILE__) . '/../application/mongo/autoload.php';
date_default_timezone_set('Asia/Calcutta');

$mongo_server = '13.233.46.52';
$mongo_port = '27017';
$mongo_client = 'mongodb://' . $mongo_server . ':' . $mongo_port;
$client = new MongoDB\Client($mongo_client);
$mongo_db = $client->HelloUni;

// MySQL Settings
$mysql_host = 'edu.cjnafqapdygx.ap-south-1.rds.amazonaws.com';
$mysql_user = 'eduloans';
$mysql_pass = 'eduloans123';
$mysql_db = 'hellouni_live';

$conn = mysqli_connect($mysql_host, $mysql_user, $mysql_pass, $mysql_db);
if(!$conn)
{
    die(mysqli_connect_error());
}

// Chat Section Starts
/*$sessionId = '2_MX40NjIyMDk1Mn5-MTU5NjEwOTIzNDc0M352eU9QRU9GbGJiTnltTFFWTHllRjg0bFZ-fg-1';
$query_select_chats = "SELECT * FROM tokbox_chat WHERE session_id = '" . $sessionId . "' LIMIT 18446744073709551610 OFFSET " . $chat_messages_count;*/

/*$chat_collection = $mongo_db->selectCollection('chats');
//$chat_messages_count = $chat_collection->count();
$chat_collection->drop();
$chat_collection = $mongo_db->selectCollection('chats');

$query_select_chats = "SELECT * FROM seminar_chat WHERE 1=1";
$query_select_chats_obj = mysqli_query($conn, $query_select_chats);
if(!$query_select_chats_obj)
{
    echo $query_select_chats;
    die(mysqli_error($conn));
}

while($row = mysqli_fetch_assoc($query_select_chats_obj))
{
    foreach($row as $field => $value)
    {
        $row[$field] = iconv('UTF-8', 'UTF-8//IGNORE', $value);
    }
    $insertOneResult = $chat_collection->insertOne($row);
}
*/
// Chat Section Ends

// Seminar Students Section Ends

// Seminar Slot Section Starts
/*
$seminar_slot_collection = $mongo_db->selectCollection('seminar_slots');
$seminar_slot_collection->drop();
$seminar_slot_collection = $mongo_db->selectCollection('seminar_slots');

$query_select_seminar_slots = "SELECT * FROM seminar_slots WHERE 1=1";
$query_select_seminar_slots_obj = mysqli_query($conn, $query_select_seminar_slots);
if(!$query_select_seminar_slots_obj)
{
    echo $query_select_seminar_slots;
    die(mysqli_error($conn));
}

while($row = mysqli_fetch_assoc($query_select_seminar_slots_obj))
{
    foreach($row as $field => $value)
    {
        if(in_array($field, array('user_id', 'tokbox_id', 'joined')))
        {
            $row[$field] = (int)$value;
        }
        else
        {
            $row[$field] = iconv('UTF-8', 'UTF-8//IGNORE', $value);
        }
    }
    $insertOneResult = $seminar_slot_collection->insertOne($row);
}
*/
// Seminar Slot Section Ends

// User Link Allow Update Starts
/*
$user_collection = $mongo_db->selectCollection('users');

$query_select_users_link = "SELECT id, link_allowed FROM user_master WHERE link_allowed > 1 ";
$query_select_users_link_obj = mysqli_query($conn, $query_select_users_link);
if(!$query_select_users_link_obj)
{
    echo $query_select_users_link;
    die(mysqli_error($conn));
}

while($row = mysqli_fetch_assoc($query_select_users_link_obj))
{
    foreach($row as $field => $value)
    {
        $row[$field] = iconv('UTF-8', 'UTF-8//IGNORE', $value);
    }
    $updateQuery['id'] = $row['id'];
    $updateData = ['$set' => ['link_allowed' => $row['link_allowed']]];
    $updateOneResult = $user_collection->updateOne($updateQuery, $updateData);
}
*/
// User Link Allow Update Ends

// Room Section Starts
/*
$room_collection = $mongo_db->selectCollection('rooms');
$room_collection->drop();
$room_collection = $mongo_db->selectCollection('rooms');

$query_select_seminar_rooms = "SELECT * FROM seminar_rooms";
$query_select_seminar_rooms_obj = mysqli_query($conn, $query_select_seminar_rooms);
if(!$query_select_seminar_rooms_obj)
{
    echo $query_select_seminar_rooms;
    die(mysqli_error($conn));
}

while($row = mysqli_fetch_assoc($query_select_seminar_rooms_obj))
{
    foreach($row as $field => $value)
    {
        if(in_array($field, array('status', 'tokbox_id')))
        {
            $row[$field] = (int)$value;
        }
        else
        {
            $row[$field] = iconv('UTF-8', 'UTF-8//IGNORE', $value);
        }
    }
    $insertOneResult = $room_collection->insertOne($row);
}
*/
// Room Section Ends

// Seminar Logs Section Starts
/*
$seminar_logs_collection = $mongo_db->selectCollection('seminar_logs');
$seminar_log_detail = $seminar_logs_collection->findOne();
if($seminar_log_detail['current_session'] != 'ONE_ON_ONE')
{
    $seminar_logs_collection->drop();
    $seminar_logs_collection = $mongo_db->selectCollection('seminar_logs');

    $query_select_seminar_logs = "SELECT * FROM seminar_logs";
    $query_select_seminar_logs_obj = mysqli_query($conn, $query_select_seminar_logs);
    if(!$query_select_seminar_logs_obj)
    {
        echo $query_select_seminar_logs;
        die(mysqli_error($conn));
    }

    while($row = mysqli_fetch_assoc($query_select_seminar_logs_obj))
    {
        foreach($row as $field => $value)
        {
            $row[$field] = iconv('UTF-8', 'UTF-8//IGNORE', $value);
        }
        $insertOneResult = $seminar_logs_collection->insertOne($row);
    }
}
*/
// Seminar Logs Section Ends

// Queue Section Starts
/*
$queue_collection = $mongo_db->selectCollection('queue');
//$queue_collection->drop();
$queue_collection = $mongo_db->selectCollection('queue');

$query_select_seminar_queue = "SELECT * FROM seminar_queue ORDER BY id ASC";
$query_select_seminar_queue_obj = mysqli_query($conn, $query_select_seminar_queue);
if(!$query_select_seminar_queue_obj)
{
    echo $query_select_seminar_queue;
    die(mysqli_error($conn));
}

while($row = mysqli_fetch_assoc($query_select_seminar_queue_obj))
{
    foreach($row as $field => $value)
    {
        if(in_array($field, array('tokbox_id', 'id')))
        {
            $row[$field] = (int)$value;
        }
        else
        {
            $row[$field] = iconv('UTF-8', 'UTF-8//IGNORE', $value);
        }
    }

    $mongoQueueQuery['username'] = $row['username'];
    $queueData = $queue_collection->findOne($mongoQueueQuery);

    if(!$queueData)
    {
        $insertOneResult = $queue_collection->insertOne($row);
    }
}
*/
// Queue Section Ends

// Connection Section Starts
/*
$connection_collection = $mongo_db->selectCollection('connections');
$connection_collection->drop();
$connection_collection = $mongo_db->selectCollection('connections');

$query_select_connections = "SELECT * FROM seminar_connections";
$query_select_connections_obj = mysqli_query($conn, $query_select_connections);
if(!$query_select_connections_obj)
{
    echo $query_select_connections;
    die(mysqli_error($conn));
}

while($row = mysqli_fetch_assoc($query_select_connections_obj))
{
    foreach($row as $field => $value)
    {
        if(in_array($field, array('user_id', 'tokbox_id')))
        {
            $row[$field] = (int)$value;
        }
        else
        {
            $row[$field] = iconv('UTF-8', 'UTF-8//IGNORE', $value);
        }
    }
    $insertOneResult = $connection_collection->insertOne($row);
}
*/
// Connection Section Ends

// IP Section Starts
/*
$ip_collection = $mongo_db->selectCollection('users_ips');
$ip_collection->drop();
$ip_collection = $mongo_db->selectCollection('users_ips');

$query_select_ips = "SELECT * FROM seminar_ips";
$query_select_ips_obj = mysqli_query($conn, $query_select_ips);
if(!$query_select_ips_obj)
{
    echo $query_select_ips;
    die(mysqli_error($conn));
}

while($row = mysqli_fetch_assoc($query_select_ips_obj))
{
    foreach($row as $field => $value)
    {
        $row[$field] = iconv('UTF-8', 'UTF-8//IGNORE', $value);
    }
    $insertOneResult = $ip_collection->insertOne($row);
}
*/
// IP Section Ends

// Document Section Starts
/*
$document_collection = $mongo_db->selectCollection('documents');
$document_collection->drop();
$document_collection = $mongo_db->selectCollection('documents');

$query_select_documents = "SELECT d.*, u.username, dm.name AS document_name, ss.student_id AS student_id FROM document d JOIN user_master u ON u.id = d.user_id JOIN document_meta dm ON dm.id = d.document_meta_id JOIN seminar_students ss ON ss.user_id = u.id WHERE dm.university_id = 2";
$query_select_documents_obj = mysqli_query($conn, $query_select_documents);
if(!$query_select_documents_obj)
{
    echo $query_select_documents;
    die(mysqli_error($conn));
}

while($row = mysqli_fetch_assoc($query_select_documents_obj))
{
    foreach($row as $field => $value)
    {
        $row[$field] = iconv('UTF-8', 'UTF-8//IGNORE', $value);
    }
    $insertOneResult = $document_collection->insertOne($row);
}
*/
// Document Section Ends

// Seminar Attended Rooms Section Starts
/*
$room_student_count_collection = $mongo_db->selectCollection('room_student_count');
$room_student_count_collection->drop();

$query_select_attended_rooms = "SELECT * FROM seminar_attended_rooms";
$query_select_attended_rooms_obj = mysqli_query($conn, $query_select_attended_rooms);
if(!$query_select_attended_rooms_obj)
{
    echo $query_select_attended_rooms;
    die(mysqli_error($conn));
}

while($row = mysqli_fetch_assoc($query_select_attended_rooms_obj))
{
    foreach($row as $field => $value)
    {
        $row[$field] = iconv('UTF-8', 'UTF-8//IGNORE', $value);

        switch($field)
        {
            case 'id':
            case 'tokbox_id':
            case 'attended':
                $row[$field] = (int)$value;
                break;
        }
    }
    $insertOneResult = $room_student_count_collection->insertOne($row);
}
*/
// Seminar Attended Rooms Section Ends

// Insert Attenders To Table Starts
/*
$attender_collection = $mongo_db->selectCollection('attenders');
$fair_delegates_collection = $mongo_db->selectCollection('fair_delegates');
//$fair_universities_collection = $mongo_db->selectCollection('fair_universities');

$attender_cursor = $attender_collection->find();
$event_name = 'VIRTUAL-FAIR-29-FEB-2024';
foreach($attender_cursor as $attender)
{
    //$user_id = $attender['user_id'];
    $tokbox_id = $attender['tokbox_id'];
    $username = $attender['username'];

    if($attender['sessionType'] == 'WEBINAR')
    {
        $university_name = 'WEBINAR';
        $delegate_name = 'Hiren';
    }
    else
    {
        $mongo_fair_delegate_query['tokbox_token_id'] = (int)$tokbox_id;
        $fair_delegate_detail = $fair_delegates_collection->findOne($mongo_fair_delegate_query);

        $university_name = '';
        $delegate_name = '';

        if($fair_delegate_detail)
        {
            $fair_university_id = $fair_delegate_detail['fair_university_id'];
            $delegate_name = $fair_delegate_detail['name'];

            $mongo_fair_university_query['id'] = (int)$fair_university_id;
            //$fair_university_detail = $fair_universities_collection->findOne($mongo_fair_university_query);

            //$university_name = $fair_university_detail ? $fair_university_detail['name'] : $fair_delegate_detail['designation'];
            $university_name = $fair_delegate_detail['designation'];
        }
    }

    //$query_select_user = "SELECT ss.username as username, ss.name as student_name, ss.phone as student_mobile, ss.email as student_email, ss.country_looking_for as desired_country, ss.desired_mainstream as desired_course, ss.desired_subcourse as desired_subcourse, ss.intake_year as intake_year, ss.intake_month as intake_month, ss.competitive_exam as competitive_exam, ss.competitive_exam_score as gre_gmat_score, ss.gpa_exam as aggregate_gpa, ss.gpa_exam_score as gpa_score, ss.work_experience as has_work_experience, ss.work_experience_month as total_work_experience, cus.display_name as desired_speciliazation, dm.name as level_of_course, ss.user_school as student_current_school, ss.current_city as current_city, ss.interested_fi, ss.study_year, ss.current_stream_department, ss.enrolled, ss.counselling_branch, ss.remark FROM seminar_students ss left join colleges_categories cc on cc.id = ss.desired_mainstream join user_master um on um.id = ss.user_id left join courses_categories cus ON cus.id = ss.desired_subcourse left join degree_master dm on dm.id = ss.desired_levelofcourse where ss.user_id = " . $user_id;

    $query_select_user = "SELECT ss.username as username, ss.name as student_name, ss.phone as student_mobile, ss.email as student_email, ss.country_looking_for as desired_country, ss.desired_mainstream as desired_course, ss.desired_subcourse as desired_subcourse, ss.intake_year as intake_year, ss.intake_month as intake_month, ss.competitive_exam as competitive_exam, ss.competitive_exam_score as gre_gmat_score, ss.gpa_exam as aggregate_gpa, ss.gpa_exam_score as gpa_score, ss.work_experience as has_work_experience, ss.work_experience_month as total_work_experience, cus.display_name as desired_speciliazation, dm.name as level_of_course, ss.user_school as student_current_school, ss.current_city as current_city, ss.interested_fi, ss.study_year, ss.current_stream_department, ss.enrolled, ss.counselling_branch, ss.remark FROM seminar_students ss left join colleges_categories cc on cc.id = ss.desired_mainstream left join courses_categories cus ON cus.id = ss.desired_subcourse left join degree_master dm on dm.id = ss.desired_levelofcourse where ss.username = '" . $username . "'";


    $query_select_user_obj = mysqli_query($conn, $query_select_user);
    if(!$query_select_user_obj)
    {
        echo $query_select_user;
        die(mysqli_error($conn));
    }
    $user_row = mysqli_fetch_assoc($query_select_user_obj);
    if($user_row)
    {
        $seminar_attender = [];
        $seminar_attender['user_id'] = $attender['user_id'];
        $seminar_attender['username'] = $user_row['username'];
        $seminar_attender['tokbox_id'] = $attender['tokbox_id'];
        $seminar_attender['type'] = 'Student';
        $seminar_attender['room'] = $university_name;
        $seminar_attender['email'] = $user_row['student_email'];
        $seminar_attender['phone'] = $user_row['student_mobile'];
        $seminar_attender['event'] = $event_name;
        $seminar_attender['name'] = $user_row['student_name'];
        $seminar_attender['country_looking_for'] = $user_row['desired_country'];
        $seminar_attender['current_city'] = $user_row['current_city'];
        $seminar_attender['course'] = $user_row['desired_course'];
        $seminar_attender['subcourse'] = $user_row['desired_subcourse'];
        $seminar_attender['remark'] = $user_row['remark'];
        $seminar_attender['level_of_course'] = $user_row['level_of_course'];
        $seminar_attender['school'] = $user_row['student_current_school'];
        $seminar_attender['intake_year'] = $user_row['intake_year'];
        $seminar_attender['intake_month'] = $user_row['intake_month'];
        $seminar_attender['competitive_exam'] = $user_row['competitive_exam'];
        $seminar_attender['exam_score'] = $user_row['gre_gmat_score'];
        $seminar_attender['gpa_exam_base'] = $user_row['aggregate_gpa'];
        $seminar_attender['gpa_score'] = $user_row['gpa_score'];
        $seminar_attender['work_expereince'] = $user_row['has_work_experience'];
        $seminar_attender['delegate_name'] = $delegate_name;
        $seminar_attender['interested_fi'] = $user_row['interested_fi'];
        $seminar_attender['study_year'] = $user_row['study_year'];
        $seminar_attender['current_stream_department'] = $user_row['current_stream_department'];
        $seminar_attender['enrolled'] = $user_row['enrolled'];
        $seminar_attender['counselling_branch'] = $user_row['counselling_branch'];

        $query_insert_attender = 'INSERT INTO seminar_attenders(';
        $all_fields = [];
        $all_values = [];

        foreach($seminar_attender as $key => $value)
        {
            $all_fields[] = $key;
            $all_values[] = $value;
        }
        $implode_fields = implode(', ', $all_fields);
        $implode_values = implode("', '", $all_values);
        $query_insert_attender .= $implode_fields . ')';
        $query_insert_attender .= " VALUES(";
        foreach($all_values as $value)
        {
            $query_insert_attender .= "'" . mysqli_escape_string($conn, $value) . "',";
        }
        $query_insert_attender = substr($query_insert_attender, 0, -1);
        $query_insert_attender .= ")";
        //echo $query_insert_attender . "\n";

        $query_insert_attender_obj = mysqli_query($conn, $query_insert_attender);
        if(!$query_insert_attender_obj)
        {
            echo $query_insert_attender . "\n";
            die(mysqli_error($conn));
        }
    }
}
*/ 
// Insert Attenders To Table Ends

// Universities Attenders To Table Starts
/*
$universities_tracker_collection = $mongo_db->selectCollection('university_tracker');
$universities_cursor = $universities_tracker_collection->find();
foreach($universities_cursor as $attender)
{
    $username = $attender['username'];
    $booth_id = $attender['booth_id'];

    $mongo_fair_university_query['id'] = (int)$booth_id;
    $fair_university_detail = $fair_universities_collection->findOne($mongo_fair_university_query);

    if($fair_university_detail)
    {
        $university_name = $fair_university_detail['name'];

        $query_select_user = "SELECT ss.user_id as user_id, ss.username as username, ss.name as student_name, ss.phone as student_mobile, ss.email as student_email, ss.country_looking_for as desired_country, cc.display_name as desired_course, ss.intake_year as intake_year, ss.intake_month as intake_month, ss.competitive_exam as competitive_exam, ss.competitive_exam_score as gre_gmat_score, ss.gpa_exam as aggregate_gpa, ss.gpa_exam_score as gpa_score, ss.work_experience as has_work_experience, ss.work_experience_month as total_work_experience, cus.display_name as desired_speciliazation, dm.name as level_of_course, ss.user_school as student_current_school, ss.current_city as current_city FROM seminar_students ss left join colleges_categories cc on cc.id = ss.desired_mainstream join user_master um on um.id = ss.user_id left join courses_categories cus ON cus.id = ss.desired_subcourse left join degree_master dm on dm.id = ss.desired_levelofcourse where ss.username = " . $username;

        $query_select_user_obj = mysqli_query($conn, $query_select_user);
        if(!$query_select_user_obj)
        {
            echo $query_select_user;
            die(mysqli_error($conn));
        }
        $user_row = mysqli_fetch_assoc($query_select_user_obj);
        if($user_row)
        {
            $seminar_attender = [];
            $seminar_attender['user_id'] = $user_row['user_id'];
            $seminar_attender['username'] = $user_row['username'];
            $seminar_attender['tokbox_id'] = $attender['tokbox_id'];
            $seminar_attender['type'] = 'Student';
            $seminar_attender['room'] = $university_name;
            $seminar_attender['email'] = $user_row['student_email'];
            $seminar_attender['phone'] = $user_row['student_mobile'];
            $seminar_attender['event'] = 'FAIR2021';
            $seminar_attender['name'] = $user_row['student_name'];
            $seminar_attender['country_looking_for'] = $user_row['desired_country'];
            $seminar_attender['current_city'] = $user_row['current_city'];
            $seminar_attender['course'] = $user_row['desired_course'];
            $seminar_attender['level_of_course'] = $user_row['level_of_course'];
            $seminar_attender['school'] = $user_row['student_current_school'];
            $seminar_attender['intake_year'] = $user_row['intake_year'];
            $seminar_attender['intake_month'] = $user_row['intake_month'];
            $seminar_attender['competitive_exam'] = $user_row['competitive_exam'];
            $seminar_attender['exam_score'] = $user_row['gre_gmat_score'];
            $seminar_attender['gpa_exam_base'] = $user_row['aggregate_gpa'];
            $seminar_attender['gpa_score'] = $user_row['gpa_score'];
            $seminar_attender['work_expereince'] = $user_row['has_work_experience'];

            $query_insert_attender = 'INSERT INTO universities_attenders(';
            $all_fields = [];
            $all_values = [];

            foreach($seminar_attender as $key => $value)
            {
                $all_fields[] = $key;
                $all_values[] = $value;
            }
            $implode_fields = implode(', ', $all_fields);
            $implode_values = implode("', '", $all_values);
            $query_insert_attender .= $implode_fields . ')';
            $query_insert_attender .= " VALUES(";
            foreach($all_values as $value)
            {
                $query_insert_attender .= "'" . mysqli_escape_string($conn, $value) . "',";
            }
            $query_insert_attender = substr($query_insert_attender, 0, -1);
            $query_insert_attender .= ")";
            //echo $query_insert_attender . "\n";

            $query_insert_attender_obj = mysqli_query($conn, $query_insert_attender);
            if(!$query_insert_attender_obj)
            {
                echo $query_insert_attender . "\n";
                die(mysqli_error($conn));
            }
        }
    }
}
*/
// Universities Attenders To Table Ends


// Webinar Section Starts
/*
$connection_webinars = $mongo_db->selectCollection('webinars');
$connection_webinars->drop();
$connection_webinars = $mongo_db->selectCollection('webinars');

$query_select_webinars = "SELECT * FROM webinar_master";
$query_select_webinars_obj = mysqli_query($conn, $query_select_webinars);
if(!$query_select_webinars_obj)
{
    echo $query_select_webinars;
    die(mysqli_error($conn));
}

while($row = mysqli_fetch_assoc($query_select_webinars_obj))
{
    foreach($row as $field => $value)
    {
        $row[$field] = iconv('UTF-8', 'UTF-8//IGNORE', $value);
    }
    $insertOneResult = $connection_webinars->insertOne($row);
}
*/
// Webinar Section Ends

// Online Users Section Starts

$online_users_collection = $mongo_db->selectCollection('online_users');
$room_student_count_collection = $mongo_db->selectCollection('room_student_count');
$queue_collection = $mongo_db->selectCollection('queue');
$connection_collection = $mongo_db->selectCollection('connections');
$webinar_connection_collection = $mongo_db->selectCollection('webinar_connections');

$online_users_cursor = $online_users_collection->find();

$current_time = date('Y-m-d H:i:s');

foreach($online_users_cursor as $online_user)
{
    $last_update = $online_user['time'];
    $last_update_string = strtotime($last_update);
    $current_time_string = strtotime($current_time);

    $diff = $current_time_string - $last_update_string;
    //echo "lu - " . $last_update . "\tlus - " . $last_update_string . "\tct - " . $current_time . "\tcts - " . $current_time_string . "\tdiff - " . $diff . "\n";
    if($diff > 90)
    {
        $username = $online_user['username'];
        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);

        $mongoRoomStudentCountQuery['username'] = $username;
        $room_student_count_collection->deleteMany($mongoRoomStudentCountQuery);

        $mongoOnlineUserQuery['username'] = $username;
        $online_users_collection->deleteMany($mongoOnlineUserQuery);

        $mongoConnectionQuery['username'] = $username;
	$connection_collection->deleteMany($mongoConnectionQuery);
	$webinar_connection_collection->deleteMany($mongoConnectionQuery);
    }
}

// Online Users Section Ends

mysqli_close($conn);
?>
