<?php
require dirname(__FILE__) . "/../vendor/autoload.php";
use OpenTok\OpenTok;
use OpenTok\MediaMode;

// LOCAL
/*
$host = 'localhost';
$user = 'root';
$pass = 'root123';
$db = 'hellouni';
*/
// TEST
/*
$host = '13.233.46.52';
$user = 'eduloan';
$pass = 'eduloans@123';
$db = 'hellouni_test';
*/
// LIVE

$host = 'edu.cjnafqapdygx.ap-south-1.rds.amazonaws.com';
$user = 'eduloans';
$pass = 'eduloans123';
$db = 'hellouni_live';

$conn = mysqli_connect($host, $user, $pass, $db);

if(!$conn)
{
    die(mysqli_connect_error());
}

$opentok = new OpenTok('46220952', '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2');

/*
$query_string_fair_delegates = "SELECT * FROM fair_delegates WHERE tokbox_token_id IS NULL";
$query_string_fair_delegates_obj = mysqli_query($conn, $query_string_fair_delegates);
if(!$query_string_fair_delegates_obj)
{
    echo $query_string_fair_delegates;
    die(mysqli_error($conn));
}

while($row = mysqli_fetch_assoc($query_string_fair_delegates_obj))
{
    $session = $opentok->createSession(array(
        'mediaMode' => MediaMode::ROUTED
    ));
    $session_id = $session->getSessionId();

    $query_insert = "INSERT INTO tokbox_token(session_id, created_at, type) VALUES('" . $session_id . "', now(), 'FAIR-2021')";
    $query_insert_obj = mysqli_query($conn, $query_insert);
    if(!$query_insert_obj)
    {
        echo $query_insert . "\n";
        die(mysqli_error($conn));
    }

    $last_inserted_id = mysqli_insert_id($conn);
    $delegate_id = $row['id'];
    $username = $row['username'];

    $usernameEncode = base64_encode($username);
    $usernameEncode = str_replace("=", "", $usernameEncode);

    $tokboxIdEncode = base64_encode($last_inserted_id);
    $tokboxIdEncode = str_replace("=", "", $tokboxIdEncode);

    $room_url = 'https://hellouni.org/event/panelist/' . $tokboxIdEncode . '/' . $usernameEncode;

    $query_update = "UPDATE fair_delegates set tokbox_token_id = '" . $last_inserted_id . "', room_url = '" . $room_url . "' where id = " . $delegate_id;
    $query_update_obj = mysqli_query($conn, $query_update);
    if(!$query_update_obj)
    {
        echo $query_update . "\n";
        die(mysqli_error($conn));
    }
}
*/

for($i = 0; $i < 1; $i++)
{
    $session = $opentok->createSession(array(
        'mediaMode' => MediaMode::ROUTED
    ));
    $session_id = $session->getSessionId();

    $query_insert = "INSERT INTO tokbox_token(session_id, created_at, type) VALUES('" . $session_id . "', now(), 'FAIR-JULY-2023')";
    $query_insert_obj = mysqli_query($conn, $query_insert);
    if(!$query_insert_obj)
    {
        echo $query_insert . "\n";
        die(mysqli_error($conn));
    }
}

mysqli_close($conn);
echo "Done\n";

/*
SELECT * FROM hellouni_live.user_master where registration_type = 'FAIR' order by id desc;select u.name as `Full Name`, u.email AS `Email`, u.phone AS `Mobile`,
u.username AS `Login Id`, s.intake_month AS `Intake Month`, s.intake_year AS `Intake Year`, s.comp_exam AS `Competitive Exam`, s.comp_exam_score
AS `Competitive Exam Score`, s.gpa_exam_score AS `GPA Score`, s.work_exp_month AS `Work Experience`, l.countries AS `Desired Country`, c.name AS `Desired Course`,
m.display_name AS `Speciliaztion`, d.name AS `Desired Level`, s.current_school AS `Current School`, s.current_city AS `Current City` FROM hellouni_live.user_master u JOIN
hellouni_live.student_details s ON s.user_id = u.id JOIN hellouni_live.lead_master l ON l.email = u.email JOIN hellouni_live.courses c ON c.id = l.courses JOIN
hellouni_live.colleges_categories m ON m.id = l.mainstream JOIN hellouni_live.degree_master d ON d.id = l.degree WHERE u.registration_type = 'FAIR' GROUP BY u.id

hellouni@123
Here is the list of usernames
Pre-Counselor
sonali_pre_counselor
rahul_pre_counselor
akshay_pre_counselor
vaidehi_pre_counselor
priyanka_pre_counselor
ranjana_pre_counselor

Post-Counselor
vaidya_post_counselor
neelanjana_post_counselor
saloni_post_counselor
yamnesh_post_counselor
anuradha_post_counselor
komal_post_counselor
hiral_post_counselor

Eduloan Counselor
edu_counselor_1
edu_counselor_2
edu_counselor_3
edu_counselor_4

Password is same for all
hellouni@123

Steps to follow
Login to backend panel
https://www.hellouni.org/admin/

On left hand side there will be a menu
Virtual Fair

Click on it, it will expand show
All Fair

After click on it, you will get the link to join the panel.

nik_edu_counselor

sonali_pre_counselor - 5331
rahul_pre_counselor - 5332
akshay_pre_counselor - 5333
sanjana_pre_counselor - 5334
vaidehi_pre_counselor - 5335
priyanka_pre_counselor - 5336
ranjana_pre_counselor - 5337

vaidya_post_counselor - 5338
neelanjana_post_counselor - 5339
saloni_post_counselor - 5340
yamnesh_post_counselor - 5341
anuradha_post_counselor - 5342
komal_post_counselor - 5343
hiral_post_counselor - 5344

nik_edu_counselor - 5345

hellouni@123

*/
?>
