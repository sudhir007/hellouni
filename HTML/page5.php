<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hellouni</title>

    <!-- Bootstrap Core CSS -->
  	<?php include 'head.php'; ?>
	<link href="jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
    
    <script src="js/jquery.js"></script>
    <script src="jqvmap/jquery.vmap.js" type="text/javascript"></script>
    <script src="jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
	<script src="jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
    
	<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('#vmap').vectorMap({
		    map: 'world_en',
		    backgroundColor: '#fff',
		    color: '#ffffff',
			  hoverColor: '#F6881F',
     		 selectedColor: '#F6881F',
		    hoverOpacity: 0.7,
		    
		    enableZoom: true,
		    showTooltip: true,
		    values: sample_data,
		    scaleColors: ['#cccccc', '#d5d5d5'],
		    normalizeFunction: 'polynomial'
		});
		
	});
	function page_loader(){
		alert($('page_loader.php').html())
		$('#page_loader').load('page_loader.php');
	}
	
    
	</script>
    <style>
   	 .shade_border{
		background:url(img/shade1.png) repeat-x; height:11px;
	}
	.search_by{
	background-color:#f3f4f4; box-shadow:5px 5px 5px #eae8e8; height:auto;
	    padding-top: 10px;
    padding-left: 30px;
	 padding-right: 30px;
	}
	.search_by h4{
		color:#3c3c3c;
		font-family:lato_regular;
		font-weight:500;
	}
	.first{
		border-left:1px solid #eaeaeb;
		border-top:1px solid #eaeaeb;
		border-bottom:1px solid #eaeaeb;
		text-align:center;
		background:#fbfbfb;
		padding: 30px 15px !important;
		
	}
	.last{
		border-right:1px solid #eaeaeb;
		border-top:1px solid #eaeaeb;
		border-bottom:1px solid #eaeaeb;
		text-align:center;
		background:#fbfbfb;
		padding: 30px 15px !important;
		
	}
	.element{
		border-top:1px solid #eaeaeb;
		border-bottom:1px solid #eaeaeb;
		text-align:center;
		background:#fbfbfb;
		padding: 30px 15px !important;
	}
	#table_heading{
		font-weight:bold;
		padding: 12px 9px 10px 9px;
		text-align: center;
		border-bottom: 1px solid #eaeaeb;
		border-top:1px solid #eaeaeb;
	}
	#table_heading-first{
		 width:169px;
		 font-weight:bold;
		 border-left:1px solid #eaeaeb;
		 border-bottom:1px solid #eaeaeb;
		 border-top:1px solid #eaeaeb; 
		 text-align:center; 
		 background:#fbfbfb;
		 padding: 12px 15px 10px 15px;
	}
	#table_heading-last{
		border-right:1px solid #eaeaeb;
		 border-bottom:1px solid #eaeaeb;
		 border-top:1px solid #eaeaeb; 
		 text-align:center; 
		 background:#fbfbfb;
		 padding: 12px 15px 10px 15px;
		 width:126px;
	}
	.more_detail{
	background-color:#F6881F;
	text-transform:uppercase;
	height:100%;	
	border:0px;
	  color:#fff; 
		border-radius:2px;
		
}
 .menutables { border-collapse: separate; }

    
  tr .menutables {
	  margin-top:10px;  
  }
    </style>
    
  <SCRIPT LANGUAGE="JavaScript">
<!-- 

<!-- Begin
function Check(e)
{
	var chk=jQuery("input[name=check_list]");
if(e.checked==true){
	
for (i = 0; i < chk.length; i++)
chk[i].checked = true ;
}else{

for (i = 0; i < chk.length; i++)
chk[i].checked = false ;
}
}

// End -->
</script>
</head>

<body>

    <!-- Navigation -->
    <?php include 'header_solid.php'; ?>

    <div class="clearfix"></div><!-- Intro Header -->
  
      <div id="page_loader">  <div class="container hidden-xs " style="padding: 17px; " > 
            <img src="img/page1.jpg" width="100%"  />
          
       </div>
        <div class="container visible-xs" style="text-align:center;">
           
           <img src="img/page1-1.jpg" />
       </div>
  	 <div class="shade_border"></div>
 	
   
        <div class="container">
        	<!-- <h4 style="color:#000;">Choose Country</h4>-->
            <br/>
             <!--<div class="col-md-3 col-sm-12">
             	<div class="search_by col-md-12 col-sm-6 col-lg-12 ">
                	<h4>Search By</h4>
                    <form action="page5_action.php" method="post"/>
                     <div class="form-group">
                    	<label>Country</label>
                        <div><select class="form-control" size="3" multiple="multiple" name="country[]">
                        	
                            <?php
							include 'connection.php';
							$sel="select * from country";
							$exe=mysqli_query($conn,$sel);
							while($fetch=mysqli_fetch_array($exe))
							{
							 ?>
                             <option value="<?php echo $fetch['country_id'] ?>"><?php echo $fetch['short_name'] ?></option>
                             <?php }
							 ?>
                        </select>.</div>
                    
                    </div>
                    <div class="form-group">
                    	<label>Area Of Interest</label>
                        <div><select class="form-control">
                        	<option value="0">-Select Interest-</option>
                            <?php
							include 'connection.php';
							$sel="select * from area_of_interest where aoe_status='yes'";
							$exe=mysqli_query($conn,$sel);
							while($fetch=mysqli_fetch_array($exe))
							{
							 ?>
                             <option value="<?php echo $fetch['id'] ?>"><?php echo $fetch['aoe_name'] ?></option>
                             <?php }
							 ?>
                        </select>.</div>
                    
                    </div>
                    <div class="form-group">
                    	<label>Level Of Course</label>
                        <div><select class="form-control">
                        	<option value="0">-Select Course-</option>
                            <?php
							include 'connection.php';
							$sel="select * from level_of_course where loc_status='yes'";
							$exe=mysqli_query($conn,$sel);
							while($fetch=mysqli_fetch_array($exe))
							{
							 ?>
                             <option value="<?php echo $fetch['id'] ?>"><?php echo $fetch['loc_name'] ?></option>
                             <?php }
							 ?>
                        </select></div>
                       <div class="form-group" style="padding:18px 0px;"><button type="submit" class="more_detail" style="width:100px;">Search</button></div>
                    
                    </div>
                </div>
                <br/><br class="visible-xs visible-sm visible-md visible-lg "/>
             	<div class="col-sm-6 col-lg-12 col-md-12" style="box-shadow:5px 5px 5px #eae8e8; text-align:center;margin-top: 10px;"><img src="img/ha.png" width="100%" /></div>
             
              <div class="clearfix"></div>
             </div>-->
             <div class="clearfix visible-xs visible-sm "></div>
             <br class="visible-xs visible-sm "/>
             <div class="col-md-12 col-sm-12">
             	<div class="table-responsive">  
                <form name="myform" action="checkboxes.asp" method="post">        
  <table class="table menutables" width="100%" cellspacing="10">
    <thead>
      <tr>
        <td style="border:1px solid #eaeaeb; text-align:center; padding: 10px 15px 10px 15px;" valign="middle" width="48">
       <input type="checkbox" name="Check_ctr" value="yes" onClick="Check(this)"></td>
        <td class="table_heading" id="table_heading-first">University</td>
        <td class="table_heading" id="table_heading">University Name</td>
        <td class="table_heading" id="table_heading">Country</td>
        <td class="table_heading" id="table_heading">Area Of Interest</td>
        <td  class="table_heading" id="table_heading">Level Of Course</td>
        <td class="table_heading" id="table_heading"></td>
        <td class="table_heading" id="table_heading-last"></td>
      </tr>
    </thead>
    <tr>
    <td colspan="8">&nbsp;</td>
    </tr>
     <tr>
        <td class="table_heading" style="border:1px solid #eaeaeb; text-align:center; padding:40px 15px; "valign="middle" width="48">
       <input type="checkbox" name="check_list" value="1"></td>
        <td class="table_heading first" valign="middle"><img src="img/university_logo.png" width="115"/></td>
        <td class="table_heading element" width="129">Nanyan Acadmey of Fine Arts</td>
        <td class="table_heading element" width="88">Singapore</td>
        <td class="table_heading element" width="137">Engineering</td>
        <td  class="table_heading element" width="131">Diploma</td>
        <td class="table_heading element" width="137"><a href="#"><img class="img-responsive" src="images/delete.png"></a></td>
        <td class="table_heading last"><a href="#"><img class="img-responsive" src="images/book.png"></a></td>
      </tr>
    <tr><td colspan="8">&nbsp;</td></tr>
     <tr>
        <td class="table_heading" style="border:1px solid #eaeaeb; text-align:center; padding:40px 15px; "valign="middle" width="48">
       <input type="checkbox" name="check_list" value="1"></td>
        <td class="table_heading first" valign="middle"><img src="img/university_logo.png" width="115"/></td>
        <td class="table_heading element" width="129">Nanyan Acadmey of Fine Arts</td>
        <td class="table_heading element" width="88">Singapore</td>
        <td class="table_heading element" width="137">Engineering</td>
        <td  class="table_heading element" width="131">Diploma</td>
        <td class="table_heading element" width="137"><a href="#"><img class="img-responsive" src="images/delete.png"></a></td>
        <td class="table_heading last"><a href="#"><img class="img-responsive" src="images/book.png"></a></td>
      </tr>
       <tr>
    <td colspan="8">&nbsp;</td>
    </tr>
  </table></form>
  <div class="clearfix"></div>
  <!-- <table class="table" width="100%">
   
      <tr>
        <td class="table_heading" style="border:1px solid #eaeaeb; text-align:center; padding:40px 15px; "valign="middle" width="48">
        <input type="checkbox" name="chk_box" /></td>
        <td class="table_heading" style="border-left:1px solid #eaeaeb; border-top:1px solid #eaeaeb; border-bottom:1px solid #eaeaeb; text-align:center; background:#fbfbfb; padding: 30px 15px;" valign="middle"><img src="img/university_logo.png" width="115"/></td>
        <td class="table_heading" style="border-top:1px solid #eaeaeb; border-bottom:1px solid #eaeaeb; text-align:center; background:#fbfbfb;    padding:30px 9px;" width="129">Nanyan Acadmey of Fine Arts</td>
        <td class="table_heading" style="border-top:1px solid #eaeaeb; border-bottom:1px solid #eaeaeb; text-align:center; background:#fbfbfb;     padding:30px 15px;" width="88">Singapore</td>
        <td class="table_heading" style="border-top:1px solid #eaeaeb; border-bottom:1px solid #eaeaeb; text-align:center; background:#fbfbfb;   padding:30px 15px;" width="137">Engineering</td>
        <td  class="table_heading" style=" border-bottom:1px solid #eaeaeb;border-top:1px solid #eaeaeb; text-align:center; background:#fbfbfb;   padding:30px 15px;" width="131">Diploma</td>
        <td class="table_heading" style="border-right:1px solid #eaeaeb; border-bottom:1px solid #eaeaeb;border-top:1px solid #eaeaeb; text-align:center; background:#fbfbfb;  padding:30px 15px;"><button type="button" class="more_detail">More >></button></td>
      </tr>
  
   
  </table>
 <table class="table" width="100%">
   
      <tr>
        <td class="table_heading" style="border:1px solid #eaeaeb; text-align:center; padding:40px 20px; " width="126" valign="middle">
        <input type="checkbox" name="chk_box" /></td>
        <td class="table_heading" style="border-left:1px solid #eaeaeb; border-top:1px solid #eaeaeb; border-bottom:1px solid #eaeaeb; text-align:center; background:#fbfbfb; padding: 30px 15px;" valign="middle" width="142"><img src="img/university_logo.png"/></td>
        <td class="table_heading" style="border-top:1px solid #eaeaeb; border-bottom:1px solid #eaeaeb; text-align:center; background:#fbfbfb;    padding:30px 9px;" width="129">Nanyan Acadmey of Fine Arts</td>
        <td class="table_heading" style="border-top:1px solid #eaeaeb; border-bottom:1px solid #eaeaeb; text-align:center; background:#fbfbfb;     padding:30px 15px;" width="88">Singapore</td>
        <td class="table_heading" style="border-top:1px solid #eaeaeb; border-bottom:1px solid #eaeaeb; text-align:center; background:#fbfbfb;   padding:30px 15px;" width="137">Engineering</td>
        <td  class="table_heading" style=" border-bottom:1px solid #eaeaeb;border-top:1px solid #eaeaeb; text-align:center; background:#fbfbfb;   padding:30px 15px;" width="134">Diploma</td>
        <td class="table_heading" style="border-right:1px solid #eaeaeb; border-bottom:1px solid #eaeaeb;border-top:1px solid #eaeaeb; text-align:center; background:#fbfbfb;  padding:30px 15px;" width="36"><button type="button" class="more_detail">>></button></td>
      </tr>
  
   
  </table>-->
  </div>
             
             
             </div>
         </div>
     	
   </div>
   <br/>

    <!-- About Section 
    <section id="about" class="container content-section text-center">
        <div class="row">
            <img src="img/home1.jpg"/>
        </div>
    </section>

    <!-- Download Section -->
    

    <!-- Footer -->
    <?php include 'footer.php'; ?>

    <!-- jQuery -->
   <?php  include 'js.php'?>

</body>

</html>
