<style>
.header_icons{
}
.header_icons ul li{
	display:inline-block;
	list-style-type:none;
	margin-bottom:-10px;
	
}
</style>

<nav class="navbar navbar-custom  top-nav-collapse" role="navigation">
    	<div class="" style="background-color:#3C3C3C; padding: 2px 0px; border-bottom: 1px solid #A4A4A4; ">
        	<div class="container" style="background-color:#3C3C3C;">
            	<div class="pull-left header_icons"><ul style="padding-left:0px;"><li><img src="img/1.png">INFO@IMPERIAL-OVERSEAS.COM</li>
                 <li><img src="img/2.png"> +91 9167991339 / +91 9819900666</li></ul></div>
                <div class="pull-right hidden-xs header_icons"><ul>
                <li><img src="img/f.png"></li>
                <li><img src="img/t.png"></li>
                <li><img src="img/g.png"></li>
                <li><img src="img/in.png"></li>
                <li> | <img src="img/login.png"><span style="font-size:11px; padding:0; margin:0;">LOGIN</span></li>
                <li> <img src="img/fav.png"><span style="font-size:11px; padding:0; margin:0;">FAVOURITES</span></li>
                </ul></div>
            </div>
         </div>
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse" style="margin-top: 10px; padding-right:0px;">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden active">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">HOME</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#download">ABOUT US</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">STUDENTS</a>
                    </li>
                     <li>
                        <a class="page-scroll" href="#contact">UNIVERSITY</a>
                    </li>
                     <li>
                        <a class="page-scroll" href="#contact">FLY ABROARD</a>
                    </li>
                     <li>
                        <a class="page-scroll" href="#contact">CONTACT US</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>