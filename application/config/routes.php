<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*

| -------------------------------------------------------------------------

| URI ROUTING

| -------------------------------------------------------------------------

| This file lets you re-map URI requests to specific controller functions.

|

| Typically there is a one-to-one relationship between a URL string

| and its corresponding controller class/method. The segments in a

| URL normally follow this pattern:

|

|	example.com/class/method/id/

|

| In some instances, however, you may want to remap this relationship

| so that a different class/function is called than the one

| corresponding to the URL.

|

| Please see the user guide for complete details:

|

|	http://codeigniter.com/user_guide/general/routing.html

|

| -------------------------------------------------------------------------

| RESERVED ROUTES

| -------------------------------------------------------------------------

|

| There area two reserved routes:

|

|	$route['default_controller'] = 'welcome';

|

| This route indicates which controller class should be loaded if the

| URI contains no data. In the above example, the "welcome" class

| would be loaded.

|

|	$route['404_override'] = 'errors/page_missing';

|

| This route will tell the Router what URI segments to use if those provided

| in the URL cannot be matched to a valid route.

|

*/

$route['wpi-intro/(:any)'] = 'event/webinar/NTE5/$1';

$route['default_controller'] = "common/home";
$route['homepage/api'] = "common/home/homePageApi";
$route['tokbox'] = "tokbox";
$route['session'] = "session";
$route['test'] = "test";
$route['university_info/(:any)'] = "university/details/universityInfo";
$route['college'] = "college";
$route['department'] = "department";
$route['about'] = "page/about";
$route['launchPage'] = "page/launch";
$route['university'] = "page/university";
$route['guidance'] = "page/faq";
$route['listtable'] = "page/listtable";
$route['studentProfilePage'] = "page/studentProfilePage";
$route['friends-n-alumns'] = "page/alumns";
$route['alumnu-profile'] = "page/alumnu_profile";
$route['additional-services-offers'] = "page/additional_services";
$route['join-a-group'] = "page/join_a_group";
$route['researches'] = "page/researches";
$route['research_Details'] = "page/researchDetails";
$route['research_Details/(:num)'] = "page/researchDetails";
$route['internships'] = "page/internships";
$route['internships/my'] = "page/internships_my";
$route['internship/(:num)'] = "page/internship/$1";
$route['user/account/signup'] = "location/country";
$route['user/signup'] = "user/account/signup";
$route['privacy'] = "privacy/index";
$route['mypage'] = "privacy/mypage";
$route['university/register'] = "webinar/registration/universityForm";
$route['university/register/submit'] = "webinar/registration/universitySubmit";
$route['university/textsearch'] = "Croncontroller/freeTextSearchNew";
$route['university/textsuggestions'] = "Croncontroller/searchKeyNew";
$route['university/virtualfair'] = 'webinar/registration/eduFairrRegistrationForm';
$route['university/virtualfair/(:num)'] = 'webinar/registration/eduFairrRegistrationForm';
$route['university/virtualfair/list/(:num)'] = 'webinar/webinarmaster/virtualFairParticipantsListNew';
$route['megaedufair2021'] = 'event/virtualfair';
$route['[a-zA-Z0-9-]+'] = 'university/details/index';             //'cart/show/$1';
$route['UTARegistration2021'] = "counsellor/registration/utaRegistration";
$route['LoanMela2022_Registration'] = "webinar/registration/LoanMelaRegistrationForm";
$route['EarlybirdRegistration'] = "webinar/registration/EarlybirdRegistration";
$route['EarlybirdRegistration/register'] = "webinar/registration/earlybirdRegistrationFormData";
$route['UK_Ireland'] = "webinar/registration/UkIerlandRegistrationForm";
//$route['utaday'] = 'event/virtualfair';
$route['utaday'] = 'counsellor/registration/utaRegistration';
$route['thankyou'] = 'counsellor/registration/thankYou';

//counsellor_registration_form
$route['counsellor/register'] = "counsellor/registration/counsellorForm";
$route['counsellor/team/register'] = "counsellor/registration/counsellorTeamForm";

$route['counsellor/dashboard'] = "counsellor/counsellormaster/dashboard";
$route['counsellor/dashboard/filter'] = "counsellor/counsellormaster/dashboardFilter";
$route['counsellor/dashboard/graph'] = "counsellor/counsellormaster/graphDashboard";


$route['counsellor/team'] = "counsellor/counsellormaster/counsellorTeamView";
$route['counsellor/team/list'] = "counsellor/counsellormaster/counsellorTeam";
$route['counsellor/profile/(:num)'] = "counsellor/counsellormaster/profileUpdateForm";
$route['counsellor/profile/update'] = "counsellor/counsellormaster/profileUpdate";
$route['counsellor/profile/updatebankinfo'] = "counsellor/counsellormaster/updateBankInfo";
$route['counsellor/profile/updatedoc'] = "counsellor/counsellormaster/updateDoc";

$route['counsellor/notification/list'] = "notification/notification/notificationList";
$route['counsellor/notification'] = "notification/notification/notificationListPage";
$route['counsellor/notification/seen'] = "notification/notification/notificationSeen";

$route['counsellor/student'] = "counsellor/counsellormaster/studentView";
$route['counsellor/student/list'] = "counsellor/counsellormaster/studentList";
$route['counsellor/student/filter/list'] = "counsellor/counsellormaster/studentFilterList";

$route['counsellor/application'] = "counsellor/counsellormaster/applicationView";
$route['counsellor/application/list'] = "counsellor/counsellormaster/applicationList";
$route['counsellor/application/filter/list'] = "counsellor/counsellormaster/applicationFilterList";

$route['counsellor/add/student'] = "counsellor/registration/studentsignupform";
$route['counsellor/submit/student'] = "counsellor/registration/studentsignup";
$route['counsellor/student/offerlist/view/(:num)'] = "counsellor/registration/studentofferlistview";
$route['counsellor/university/offerlist/(:num)'] = "counsellor/registration/studentofferlist";
$route['counsellor/student/comments/view/(:num)'] = "counsellor/registration/studentCommentListview";
$route['counsellor/student/comments/(:num)'] = "counsellor/registration/studentCommentlist";
$route['counsellor/student/comments/add'] = "counsellor/registration/addComment";
$route['counsellor/student/comments/submit'] = "counsellor/registration/add";
$route['counsellor/earnings'] = "counsellor/counsellormaster/counsellorsEarnings";


//comment application levels
$route['counsellor/application/comments/view/(:num)/(:num)'] = "counsellor/registration/studentCommentListviewApplication";
$route['counsellor/application/comments/(:num)/(:num)'] = "counsellor/registration/studentCommentlistApplication";
$route['counsellor/application/comments/add'] = "counsellor/registration/addCommentApplication";
$route['counsellor/application/comments/submit'] = "counsellor/registration/addApplication";


//application forms
$route['counsellor/create/application'] = "counsellor/studentapplication/studentApplicationForm";
$route['counsellor/add/application'] = "counsellor/studentapplication/submitStudentApplication";

$route['counsellor/application/updatetemp'] = "counsellor/studentapplication/updateStudentApplication";
$route['counsellor/application/addStudentApplication'] = "counsellor/studentapplication/addStudentApplication";
$route['counsellor/student/registration'] = "counsellor/studentapplication/studentRegistration";
$route['counsellor/student/application/submit'] = "counsellor/studentapplication/studentApplicationSubmit";
$route['counsellor/university/process/info'] = "counsellor/studentapplication/universityProcessInfo";
$route['counsellor/university/process/submit'] = "counsellor/studentapplication/universityProcessSubmit";

$route['counsellor/application/status/submit'] = "counsellor/studentapplication/applicationStatusSubmit";


$route['offer/course/textsuggestions/(:num)'] = "counsellor/counsellormaster/courseListByUniversityId";

$route['student/profile/(:num)'] = "user/account/update";
$route['student/profile/(:num)/(:num)'] = "user/account/update";
$route['student/profile/save'] = "user/account/save";
$route['student/profile/notesupdate'] = "user/account/notesInsert";
$route['student/profile/appdocupdate'] = "user/account/appDocUpdate";
$route['student/profile/deletedoc'] = "user/account/deleteDoc";
$route['student/profile/commentsupdate'] = "user/account/commentsInsert";

$route['student/visa/seminarlink'] = "meeting/visaseminar/visaSeminarUrl";
$route['student/visa/seminarparentlink'] = "meeting/visaseminar/visaSeminarUrlParent";
$route['student/visaurl/adminaccess'] = "webinar/registration/adminAccess";

$route['listtable'] = "page/listtable";
$route['dashboard'] = "page/dashboard";

//visa Seminar
$route['visa/seminar/form/(:num)'] = "webinar/registration/visaSeminarForm";

//imperial Event Data
$route['imperial/event/Data'] = "webinar/registration/imperialEventData";


$route['404_override'] = '';

$route['quotations/sortby_([a-z]+)_([a-z]+)'] = "quotations/visitorLeads/$1/$2";

$route['quotations/sortby_([a-z]+)_([a-z]+)/(:num)'] = "quotations/visitorLeads/$1/$2/$3";

//$route['cities/fhgh/(:num)'] = "cities/cities/$1";



/* End of file routes.php */

/* Location: ./application/config/routes.php */
$route['translate_uri_dashes'] = TRUE;
