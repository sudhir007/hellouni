<?php
require_once APPPATH . 'libraries/Mail/sMail.php';
class Page extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
        track_user();
    }

    public function about()
    {
        $this->render_page('templates/pages/about');
    }

    public function launch()
    {
        // $this->render_page('templates/pages/launchPage');
        $this->render_page('templates/meeting/launch');
    }

    public function university()
    {
      $this->load->model('croncontroller_model');
      $this->load->model('user/user_model');
      $this->load->model('redi_model');
      $this->load->model('location/country_model');
  		$this->load->model('course/degree_model');
      $this->load->model('course/course_model');


        $sortBy = $this->input->get('sort') ? $this->input->get('sort') : 'ranking_world';
        $this->outputData['sort_by'] = $sortBy;
        $criteria = 'DESC';
        if($sortBy == 'ranking_world')
        {
            $sortBy = "ISNULL(" . $sortBy . "), " . $sortBy . " ASC";
        }
        else
        {
            $sortBy = $sortBy . " " . $criteria;
        }

        $uarrayIds = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
        $usrdata = array();

        $allUniversities = $this->user_model->getAllUniversities($sortBy);
        $courseListObj = $this->user_model->getCourseDataByMultipleId($uarrayIds,$sortBy);
        $finalList = array();
        foreach($allUniversities as $index => $university)
            {
                $courseCategoryId = '';
                $uni_details = $this->user_model->getUniversityDataByid($university['id'], $courseCategoryId=0);
                if($uni_details)
                {
                    $finalList[] = $university;
                }
            }

        /*$this->outputData['universities'] = $finalList;
        $this->outputData['userdata'] = $usrdata;
        $this->render_page('templates/pages/university', $this->outputData);*/

        $this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));
        $this->outputData['mainstreams'] = $this->course_model->getAllCollegeCategories();
        $this->outputData['degrees'] = $this->degree_model->getDegree(array('degree_master.status'=>1));

        $this->outputData['uni'] = $finalList;
        $this->outputData['cou'] = $courseListObj;
        $this->outputData['userdata'] = $usrdata;
        $this->outputData['userLoggedIn'] = false;
        //$this->outputData['cou'] = $courseListObj;
        //$this->outputData['freetextsearch'] = $searchText;

        $this->render_page('templates/search/universitylist',$this->outputData);


    }
    public function faq()
    {
        $this->render_page('templates/pages/faq');
    }
    public function alumns()
    {
        $this->render_page('templates/pages/alumns');
    }
    public function additional_services()
    {
        $this->render_page('templates/pages/additional-services');
    }
	public function join_a_group()
    {
        $this->load->model('user/user_model');
        $allGroups = $this->user_model->getAllGroups();
        $this->outputData['groups'] = $allGroups;
        $this->render_page('templates/pages/join-a-group', $this->outputData);
    }

    public function researches()
    {
        $this->load->model('course/course_model');
        $this->load->model('user/user_model');
        $getParameters = $this->input->get();
        if($getParameters && isset($getParameters['str']))
        {
            $allResearches = $this->course_model->getResearchesBySearch($getParameters['str']);
            echo json_encode($allResearches);
        }
        else
        {
          $this->load->helper(array('form', 'url'));
          $userdata=$this->session->userdata('user');

          $allResearches = $this->course_model->getAllResearches();

          $uId = $userdata['id'] ? $userdata['id'] : 0;

            foreach ($allResearches as $ldkey => $ldvalue) {
              // code...
              $conditionArrayc = [
                'user_id' => $uId,
                'entity_id' => $ldvalue['id'],
                'entity_type' => 'researches'
              ];

              $favQueryc = $this->user_model->commonCheck($conditionArrayc,'favourites_detail');
              if(count($favQueryc)<1){
                  $allResearches[$ldkey]['favourites_link'] = '';
              }else{
                  $allResearches[$ldkey]['fav_id'] = $favQueryc->id;
                  $allResearches[$ldkey]['favourites_link'] = 1;
              }

              $likesQueryc = $this->user_model->commonCheck($conditionArrayc,'likes_detail');

              if(count($likesQueryc)<1){
                $allResearches[$ldkey]['likes_link'] = 0;
              } else {
                $allResearches[$ldkey]['likes_link'] = 1;
              }
            }

            $this->outputData['researches'] = $allResearches;

            $this->render_page('templates/pages/researches', $this->outputData);
        }
    }

    public function researchDetails()
    {
      $this->load->helper(array('form', 'url'));
      $userdata=$this->session->userdata('user');
      if(empty($userdata)){
        redirect('/user/account');
      }

      $this->load->model('course/course_model');
      $this->load->model('webinar/common_model');
      $this->load->model('user/user_model');

      $researchId = $this->uri->segment('2');

      $researchObj = $this->course_model->getResearchByIdNew($researchId);

      if(!$researchObj)
      {
          redirect('/researches');
      }

      $uId = $userdata['id'] ? $userdata['id'] : 0;

      $dataObj = [
        "user_id" => $uId,
        "entity_id" => $researchId,
        "entity_type" => "researches"
      ];

      //$insertObj = $this->common_model->post($dataObj,"unique_count");
      $insertObj = $this->common_model->post($dataObj,"views_detail");

      $researchDetail = $this->course_model->getDetailResearches($researchId);
      //$relatedCourse = $this->course_model->relatedCourseDetail($courseDetail[0]['id'],$courseDetail[0]['department_id']);
      //var_dump($researchDetail);die();

      $this->outputData['research_detail'] = $researchDetail[0];
      //$this->outputData['related_courses'] = $relatedCourse;
      $this->outputData['consent'] = $researchDetail[0]['uni_consent_data'];

      $this->outputData['userLoggedIn'] = false;
     //$userdata=$this->session->userdata('user');
     //var_dump($researchDetail);die();
     if($userdata){
         $this->outputData['userLoggedIn'] = true;
         $alreadyConnected = $this->user_model->getSlotByUserAndUniversity($userdata['id'], $courseDetail[0]['uni_id']);
         if($alreadyConnected){
             $alreadyConnected->slot_1_show = $alreadyConnected->slot_1 ? date('d-M-Y h:i A', strtotime($alreadyConnected->slot_1)) : NULL;
             $alreadyConnected->slot_2_show = $alreadyConnected->slot_2 ? date('d-M-Y h:i A', strtotime($alreadyConnected->slot_2)) : NULL;
             $alreadyConnected->slot_3_show = $alreadyConnected->slot_3 ? date('d-M-Y h:i A', strtotime($alreadyConnected->slot_3)) : NULL;
             $this->outputData['alreadyConnected'] = $alreadyConnected;
             $this->outputData['show_url'] = false;

             if(!$this->outputData['applied'])
             {
                 $this->load->model('tokbox_model');
                 $participants = array("user_id" => $uId, "slot_id" => $alreadyConnected->id);
                 $json_participant = json_encode($participants);
                 $token = $this->tokbox_model->getTokenByParticipant($json_participant);
                 if($token && $token->token)
                 {
                     $encodedTokboxId = base64_encode($token->id);
                     $this->outputData['tokbox_url'] = '/tokbox?id=' . $encodedTokboxId;

                     if($token->expiry_time)
                     {
                         $expiryTime = $token->expiry_time;
                         $currentTime = date('Y-m-d H:i:s');

                         $time1 = strtotime($expiryTime);
                         $time2 = strtotime($currentTime);

                         if($expiryTime && $time2 < $time1){
                             $this->outputData['show_url'] = true;
                         }
                     }
                 }
             }
         }

         $condition = array('user_master.id'=>$userdata['id'],'user_master.status'=>1,'user_master.type'=>5);

         $userDetails = $this->user_model->getStudentDetails($condition);
         //$qualifiedExams = $this->user_model->checkQualifiedExam(array('student_id'=>$userdata['id']));
         $leadCondition = array("email" => $userdata['email']);
         $leadMaster = $this->user_model->getLead($leadCondition);

         $mandatoryFields = $courseDetail[0]['uni_mandatory_fields'];
         if($mandatoryFields)
         {
             $this->load->model('location/country_model');
             $this->load->model('location/city_model');

             $this->outputData['locationcountries'] = $this->country_model->getLocationCountries(array('location_country_master.status'=>1));
             $this->outputData['cities'] = $this->city_model->getAllCities();
             $this->outputData['mainstreams'] = $this->course_model->getAllCollegeCategories();
             $this->outputData['levels'] = $this->course_model->getAllDegrees();
             if($leadMaster->mainstream)
             {
                 $this->outputData['subCourses'] = $this->course_model->getCourseCategoriesByCollegeCategory($leadMaster->mainstream);
             }

             $mandatoryFields = json_decode($mandatoryFields, true);
             foreach($mandatoryFields as $fieldList)
             {
                 foreach($fieldList as $tableName => $fieldArray)
                 {
                     foreach($fieldArray as $field)
                     {
                         if($tableName == 'user_master' || $tableName == 'student_details')
                         {
                             $this->outputData[$tableName][$field] = $userDetails->$field;
                         }
                         else if($tableName == 'lead_master')
                         {
                             $this->outputData[$tableName][$field] = $leadMaster->$field;
                         }
                     }
                 }
             }
         }

         $studentName = $userdata['name'];
         $date = date('Y-m-d');
          $universityName = $this->outputData['name'];
          $countryName = $this->outputData['country'];
          $sessionType = 'One On One Session';

          $this->outputData['consent'] = str_replace('$todayDate', $date, $this->outputData['consent']);
          $this->outputData['consent'] = str_replace('$universityName', $universityName, $this->outputData['consent']);
          $this->outputData['consent'] = str_replace('$sessionType', $sessionType, $this->outputData['consent']);
          $this->outputData['consent'] = str_replace('$studentName', $studentName, $this->outputData['consent']);
          $this->outputData['consent'] = str_replace('$countryName', $countryName, $this->outputData['consent']);
     }
        $this->render_page('templates/pages/research_details',$this->outputData);
    }

    public function alumnu_profile()
    {
        $this->render_page('templates/pages/alumns-details');
    }

    public function internships()
    {
        $this->load->model('university/university_model');
        $getParameters = $this->input->get();
        if($getParameters && isset($getParameters['str']))
        {
            $allInternships = $this->university_model->getInternshipsBySearch($getParameters['str']);
            echo json_encode($allResearches);
        }
        else
        {
            $allInternships = $this->university_model->getAllInternships();
            $this->outputData['internships'] = $allInternships;
            $this->render_page('templates/pages/internships', $this->outputData);
        }
    }

    public function internship($internshipId)
    {
        $this->load->model(array('university/university_model', 'user/user_model', 'location/country_model'));
        $usrdata=$this->session->userdata('user');

        $allCountries = $this->country_model->getLocationCountries(array('location_country_master.status'=>1));

        if($usrdata)
        {
            $condition = array('user_master.id'=>$usrdata['id'],'user_master.status'=>1,'user_master.type'=>5);
    		$userDetails = $this->user_model->getStudentDetails($condition);
            $userDetails->student_other_details = $userDetails->student_other_details ? json_decode($userDetails->student_other_details) : '';
            $this->outputData['userDetails'] = $userDetails;
            $studentId = $userDetails->student_id;
            $isExist = $this->university_model->checkInternship($internshipId, $studentId);
            if($isExist)
            {
                $isExist->other_details = $isExist->other_details ? json_decode($isExist->other_details) : '';
            }
            $userId = $usrdata['id'];
            $documentType = 3;
        }

        if($this->input->post())
        {
            $postVariables = $this->input->post();
            if($_FILES['document']['name'])
            {
        		if (!file_exists('./uploads/student_' . $userId))
                {
            		mkdir('./uploads/student_' . $userId, 0777, true);
        		}

                $uploadFilePath     = './uploads/student_' . $userId;
                $uploadPath         = '/uploads/student_' . $userId;
                $pathInfo           = pathinfo($_FILES['document']['name']);
                $tempfilename       = preg_replace('/\s+/', '_', $pathInfo['filename']);
                $tempfilename       = str_replace('.', '_', $tempfilename);
                $tempfilename       = str_replace('&', '_', $tempfilename);
                $tempfilename       = str_replace('-', '_', $tempfilename);
                $tempfilename       = str_replace('+', '_', $tempfilename);

                $fileExt                    = $pathInfo['extension'];
                $fileName                   = $userId . '_' . $documentType . '_' . $tempfilename . '.' . $fileExt;
                $config['upload_path']      = $uploadFilePath;
                $config['file_name']        = $fileName;
                $config['allowed_types']    = 'gif|jpg|png|JPEG|JPG|jpeg|pdf|PDF';
                $config['max_size']         = 2048;
                $config['max_width']        = 10240;
                $config['max_height']       = 7680;

                $this->load->library('upload', $config);

                $studentDocument = $this->user_model->getDocumentByStudentType($userId, $documentType);
                if($studentDocument)
                {
                    $oldFile = $_SERVER['DOCUMENT_ROOT'] . $uploadPath . '/' . $studentDocument->name;
                    if(file_exists($oldFile))
                    {
                        unlink($oldFile);
                    }
                }

                if(!$this->upload->do_upload('document'))
                {
                    $this->outputData['document_error'] = $this->upload->display_errors();
                    /*echo '<pre>';
                    print_r($this->upload->display_errors());
                    exit;*/
                }
                else
                {
                    if($studentDocument)
                    {
                        $updatedData = [
                            "name" => $fileName,
                            "url" => base_url() . $uploadPath . '/' . $fileName
                        ];
                        $documentUpdate = $this->user_model->updateDocumentByStudentType($userId, $documentType, $updatedData);
                    }
                    else
                    {
                        $insertData = [
                            "user_id"           => $userId,
                            "name"              => $fileName,
                            "url"               => base_url() . $uploadPath . '/' . $fileName,
                            "document_meta_id"  => $documentType
                        ];

                        $documentInsert = $this->user_model->addDocument($insertData);
                    }
                }
            }

            $studentOtherDetail['current_university'] = $postVariables['current_university'];
            $studentOtherDetail['current_course'] = $postVariables['current_course'];
            $studentOtherDetail['expected_graduation_year'] = $postVariables['expected_graduation_year'];
            $studentUpdatedData['country'] = $postVariables['nationality'];
            $studentUpdatedData['other_details'] = json_encode($studentOtherDetail);

            $this->user_model->updateStudentDetails($studentUpdatedData, $userId);
            $internshipData['student_id'] = $studentId;
            $internshipData['internship_id'] = $internshipId;
            $internshipData['other_details'] = json_encode($postVariables['answers']);
            if($isExist)
            {
                $id = $isExist->id;
                $this->university_model->updateInternship($id, $internshipData);
            }
            else
            {
                $this->university_model->addInternship($internshipData);
            }
            $this->outputData['success'] = 1;

            /*$data['user_id'] = $usrdata['id'];
            $data['university_id'] = $internshipDetail->university_id;
            $data['university_login_id'] = $internshipDetail->university_login_id;
            $data['slot_type'] = 'INTERNSHIP';
            $data['date_created'] = date('Y-m-d H:i:s');

            $alreadyConnected = $this->user_model->getSlotByUserAndUniversity($data['user_id'], $data['university_id'], $data['slot_type']);
            if($alreadyConnected)
            {
                $this->session->set_flashdata('flash_message', '<p style="font-size:11px;">Your request is in under process.<br/>Please check your mail for any further information.</p>');
            }

            else
            {
                if($this->user_model->addSlot($data))
                {
                    $condition_1 = array("user_master.id" => $data['university_login_id']);
                    $condition_2 = array();
                    $loginDetail = $this->user_model->getUser($condition_1, $condition_2);
                    if($loginDetail)
                    {
                        $universityEmail = $loginDetail[0]->email;
                        $universityName = $loginDetail[0]->name;
                        $studentName = $usrdata['name'];
                        $studentEmail = $usrdata['email'];

                        $ccMailList = '';
                        $mailAttachments = '';

                        $mailSubject = "One To One Request For $universityName Successful";
                        $mailTemplate = "Hi $studentName

                                            It looks like $universityName has found a place in your wishlist!

                                            A representative from the university will be glad to answer all your queries.

                                            We will intimate the representative about your interest in knowing more about the university and request them to share their availability.

                                            <b>Thanks and Regards,
                                            HelloUni Coordinator
                                            +91 81049 09690</b>

                                            <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

                        //$sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

                        $mailSubject = "Student's Request For One To One Session";
                        $mailTemplate = "Hi $universityName,

                                        The following student has sent a request for one to one session.

                                        Name = $studentName
                                        Email = $studentEmail

                                        <b>Thanks and Regards,
                                        HelloUni Coordinator
                                        +91 81049 09690</b>

                                        <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

                        //$sendMail = sMail($universityEmail, $universityName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);
                    }
                    //$this->session->set_flashdata('flash_message', '<p style="font-size:11px;">Congrats! Your request has been sent.<br/>You will get a notification shortly.</p>');

                }
                else
                {
                    //$this->session->set_flashdata('flash_message', "Some issue occured while sending your request.");
                    $this->outputData['success'] = 0;
                }
            }
            //redirect('/internships/my');*/
        }
        if($usrdata)
        {
            $isExist = $this->university_model->checkInternship($internshipId, $studentId);
            if($isExist)
            {
                $isExist->other_details = $isExist->other_details ? json_decode($isExist->other_details) : '';
                $studentDocument = $this->user_model->getDocumentByStudentType($userId, $documentType);
                $this->outputData['document'] = $studentDocument;
            }
            $this->outputData['student_internship'] = $isExist;
        }

        $internshipDetail = $this->university_model->getInternshipById($internshipId);
        $internshipDetail->other_details = $internshipDetail->other_details ? json_decode($internshipDetail->other_details) : '';
        $internshipDetail->professors_guide = $internshipDetail->professors_guide ? json_decode($internshipDetail->professors_guide) : '';
        $internshipDetail->other_fees = $internshipDetail->other_fees ? json_decode($internshipDetail->other_fees) : '';

        $this->outputData['internshipDetail'] = $internshipDetail;
        $this->outputData['user_data'] = $usrdata;
        $this->outputData['countries'] = $allCountries;
        $this->render_page('templates/pages/internship_form', $this->outputData);
    }

    function internships_my()
    {
        $this->load->model(array('user/user_model'));
        $userdata = $this->session->userdata('user');
        if(!$userdata)
        {
            redirect('/user/account');
        }
        $userId = $userdata['id'];
        $mySessions = $this->user_model->getMySessions($userId, 'INTERNSHIP');
        if($mySessions)
        {
            $traversedUniversities = array();
            foreach($mySessions as $index => $sessionArray)
            {
                if(!in_array($sessionArray['university_id'], $traversedUniversities))
                {
                    $traversedUniversities[] = $sessionArray['university_id'];
                    $mySessions[$index]['action'] = '';
                    $mySessions[$index]['slot_form'] = '';

                    if($sessionArray['confirmed_slot'])
                    {
                        $date1 = strtotime($sessionArray['confirmed_slot']);
                        $date2 = strtotime(date('Y-m-d H:i:s'));

                        $diff = $date2 - $date1;
                        if($diff > 0)
                        {
                            $mySessions[$index]['action'] = '<form method="post" action="/university/details/connect">
                   				                                 <div style="text-align:center;">
                   		         	                                  <input type="submit" class="btn btn-default" name="submit" style="margin:10px auto;" value="Connect Again">
                   		                                         </div>
                   				                                 <input type="hidden" name="university_id" value="' . $sessionArray['university_id'] . '">
                   				                                 <input type="hidden" name="university_login_id" value="' . $sessionArray['university_login_id'] . '">
                                                                 <input type="hidden" name="slug" value="' . $sessionArray['slug'] . '">
               	                                             </form>';
                        }
                    }
                    else if($sessionArray['slot_1'])
                    {
                        $alreadyConnected = $this->user_model->getSlotByUserAndUniversity($userId, $sessionArray['university_id'], 'INTERNSHIP');
                        $alreadyConnected->slot_1_show = $alreadyConnected->slot_1 ? date('d-M-Y h:i A', strtotime($alreadyConnected->slot_1)) : NULL;
                        $alreadyConnected->slot_2_show = $alreadyConnected->slot_2 ? date('d-M-Y h:i A', strtotime($alreadyConnected->slot_2)) : NULL;
                        $alreadyConnected->slot_3_show = $alreadyConnected->slot_3 ? date('d-M-Y h:i A', strtotime($alreadyConnected->slot_3)) : NULL;

                        $mySessions[$index]['action'] = "<a href='#' onclick=\"showModal('internship-$index')\" style='color:#337ab7;'>Pick A Slot</a>";
                        $mySessions[$index]['slot_form'] = "<form class='form-horizontal' method='post' action='/university/details/slotconfirm'><h5 style='color:#fff; text-align:center;'>Please select any one slot</h5>";
                        if($alreadyConnected->slot_1)
                        {
                            $mySessions[$index]['slot_form'] .= "<div style='padding:5px;'><input type='radio' name='confirmed_slot' value=" . $alreadyConnected->slot_1 . " " . ($alreadyConnected->confirmed_slot == $alreadyConnected->slot_1 ? 'checked' : '') . ">&nbsp;&nbsp;<b>Slot 1 :</b> &nbsp;&nbsp;" . $alreadyConnected->slot_1_show . "</div>";
                        }
                        if($alreadyConnected->slot_2)
    					{
                            $mySessions[$index]['slot_form'] .= "<div style='padding:5px;'><input type='radio' name='confirmed_slot' value='" . $alreadyConnected->slot_2 . "' " . ($alreadyConnected->confirmed_slot == $alreadyConnected->slot_2 ? 'checked' : '') . ">&nbsp;&nbsp;<b>Slot 2 :</b> &nbsp;&nbsp;" . $alreadyConnected->slot_2_show . "</div>";
                        }
                        if($alreadyConnected->slot_3)
    					{
                            $mySessions[$index]['slot_form'] .= "<div style='padding:5px;'><input type='radio' name='confirmed_slot' value='" . $alreadyConnected->slot_3 . "' " . ($alreadyConnected->confirmed_slot == $alreadyConnected->slot_3 ? 'checked' : '') . ">&nbsp;&nbsp;<b>Slot 3 :</b> &nbsp;&nbsp;" . $alreadyConnected->slot_3_show . "</div>";
                        }
                        $mySessions[$index]['slot_form'] .= "<input type='hidden' name='slot_id' value=" . $alreadyConnected->id . "><input type='hidden' name='university_id' value=" . $sessionArray['university_id'] . "><input type='hidden' name='university_login_id' value=" . $sessionArray['university_login_id'] . "><input type='hidden' name='university_website' value=" . $sessionArray['website'] . "><input type='hidden' name='university_email' value=" . $sessionArray['email'] . "><input type='hidden' name='university_name' value=" . $sessionArray['name'] . "><input type='hidden' name='slug' value=" . $sessionArray['slug'] . "><input type='hidden' name='form_type' value='INTERNSHIP'><div style='text-align:center;'><input type='submit' class='btn btn-default' name='submit' style='margin:10px auto;' value='Submit'></div></form>";
                    }
                    else
                    {
                        $mySessions[$index]['action'] = "Wait For Slots";
                    }
                }
                else
                {
                    unset($mySessions[$index]);
                }
            }
        }
        $this->outputData['my_sessions'] = $mySessions;
        $this->render_page('templates/pages/internships_my');
    }

    public function dashboard()
    {
        $this->render_page('templates/Student/dashboard');
    }

    public function listtable()
    {
        $this->render_page('templates/Student/listpage');
    }

    public function studentProfilePage()
    {
        $this->render_page('templates/Student/studentProfile');
    }
}
?>
