<?php
class College extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
        $this->load->model('university/college_model');
        track_user();
    }

    function index()
    {
        $collegeId = $this->uri->segment(3);
        if(!$collegeId)
        {
            redirect('/');
        }

        $collegeDetail = $this->college_model->getCollegeById($collegeId);
        $finalData = array();
        if($collegeDetail)
        {
            $finalData['College Name'] = $collegeDetail[0]['name'];
            $finalData['University Name'] = $collegeDetail[0]['university_name'];
            $finalData['Campus Name'] = $collegeDetail[0]['campuse_name'];
            $finalData['Mainstream'] = $collegeDetail[0]['mainstream'];
            $finalData['Enrollment'] = $collegeDetail[0]['enrollment'] ? json_decode($collegeDetail[0]['enrollment'], true) : '';
            $finalData['Ranking'] = $collegeDetail[0]['ranking'] ? json_decode($collegeDetail[0]['ranking'], true) : '';
            $finalData['College Research Expenditure'] = $collegeDetail[0]['college_research_expenditure'];
            $finalData['Total Enrollment'] = $collegeDetail[0]['total_enrollment'];
            $finalData['International Enrollment'] = $collegeDetail[0]['international_enrollment'];
            $finalData['Application Deadline (USA)'] = $collegeDetail[0]['application_deadline_usa'] ? json_decode($collegeDetail[0]['application_deadline_usa'], true) : '';
            $finalData['Application Deadline (International)'] = $collegeDetail[0]['application_deadline_international'] ? json_decode($collegeDetail[0]['application_deadline_international'], true) : '';
            $finalData['Application Fees (USA)'] = $collegeDetail[0]['application_fees_usa'] ? json_decode($collegeDetail[0]['application_fees_usa'], true) : '';
            $finalData['Application Fees (International)'] = $collegeDetail[0]['application_fees_international'] ? json_decode($collegeDetail[0]['application_fees_international'], true) : '';
            $finalData['Admission Director Name'] = $collegeDetail[0]['admission_director_name'];
            $finalData['Admission Director Phone'] = $collegeDetail[0]['admission_director_phone'];
            $finalData['Admission Director Email'] = $collegeDetail[0]['admission_director_email'];
            $finalData['Admission URL'] = $collegeDetail[0]['admission_url'];
            $finalData['Average GRE'] = $collegeDetail[0]['average_gre'] ? json_decode($collegeDetail[0]['average_gre'], true) : '';
            $finalData['Research Expenditure Per Faculty'] = $collegeDetail[0]['research_expenditure_per_faculty'];
            $finalData['Established Date'] = $collegeDetail[0]['establishment_date'];
            $finalData['Acceptance Rate'] = $collegeDetail[0]['acceptance_rate'];
        }

        $this->outputData['collegeDetail'] = $finalData;
        $this->render_page('templates/college/index', $this->outputData);
    }
}
?>
