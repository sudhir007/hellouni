<?php

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

require_once APPPATH . 'libraries/Mail/sMail.php';

class Notification extends MY_Controller {

  public $outputData;		//Holds the output data for each view
  public $loggedInUser;


      function __construct() {

          parent::__construct();

          $this->load->library('template');

          $this->lang->load('enduser/home', $this->config->item('language_code'));

          $this->load->library('form_validation');

          $this->load->model('location/country_model');
          $this->load->model('webinar/webinar_model');
          $this->load->model('webinar/common_model');
          $this->load->model('course/course_model');
          $this->load->model('course/degree_model');
          $this->load->model('user/user_model');
          $this->load->model('counsellor/counsellor_model');
          $this->load->model('redi_model');

      }

      public function notificationListPage(){

        $this->load->helper('cookie_helper');
        $usrdata=$this->session->userdata('user');

        if(empty($usrdata)){
          redirect('user/account');
          exit();
        }

        $this->render_page('templates/counsellor/notification_list',$this->outputData);

      }

      public function notificationList(){

        $usrdata=$this->session->userdata('user');

        if(empty($usrdata)){
          redirect('user/account');
          exit();
        }


        $response['notification_list'] = [];

        if($usrdata['type'] == '5'){

          $whereStudentId = $usrdata['id'];

        } else {

          $counsellorId = $usrdata['id'] ? $usrdata['id'] : 5826;
          $key = "COUNSELLOR-CHILD-".$counsellorId;

          $keyExist = $this->redi_model->getKey($key);

          if($keyExist){

             $allChild = $keyExist;

          } else{

            $getChild = $this->counsellor_model->getChild($counsellorId);

            $allChild = $getChild && $getChild[0]['child_ids'] ? $getChild[0]['child_ids'].", $counsellorId" : $counsellorId;

            $setKey = $this->redi_model->setKey($key, $allChild);
          }

          $studentList = $this->counsellor_model->studentList($allChild);

          $whereStudentId = "";
          foreach ($studentList as $keyrm => $valuerm) {
  									// code...
  									$whereStudentId .= $valuerm['user_id'].", " ;
  								}
  				$whereStudentId = rtrim($whereStudentId,", ");

        }

        $notificationList = $this->user_model->getNotificationList($whereStudentId);

        $studentNotificationCount = 0;
        $agentNotificationCount = 0;

        foreach ($notificationList['data'] as $keysa => $valuesa) {

          if($valuesa['seen_by_student'] == 0){
            $studentNotificationCount = $studentNotificationCount + 1;
          }

          if($valuesa['seen_by_agent'] == 0){
            $agentNotificationCount = $agentNotificationCount + 1;
          }
        }

        $response['notification_list'] = $notificationList['data'];
        $response['student_notification_count'] = $studentNotificationCount;
        $response['agent_notification_count'] = $agentNotificationCount;
        $response['notification_count'] = $usrdata['type'] == '5' ? $studentNotificationCount : $agentNotificationCount;

        $response['status'] = "SUCCESS";

        header('Content-Type: application/json');
        echo (json_encode($response));

      }

      public function notificationSeen(){

        $usrdata=$this->session->userdata('user');

        if(empty($usrdata)){
          redirect('user/account');
          exit();
        }

        $response['notification_list'] = [];

        if($usrdata['type'] == '5'){

          $whereStudentId = $usrdata['id'];

        } else {

          $counsellorId = $usrdata['id'] ? $usrdata['id'] : 5826;
          $key = "COUNSELLOR-CHILD-".$counsellorId;

          $keyExist = $this->redi_model->getKey($key);

          if($keyExist){

             $allChild = $keyExist;

          } else{

            $getChild = $this->counsellor_model->getChild($counsellorId);

            $allChild = $getChild && $getChild[0]['child_ids'] ? $getChild[0]['child_ids'].", $counsellorId" : $counsellorId;

            $setKey = $this->redi_model->setKey($key, $allChild);
          }

          $studentList = $this->counsellor_model->studentList($allChild);

          $whereStudentId = "";
          foreach ($studentList as $keyrm => $valuerm) {
  									// code...
  									$whereStudentId .= $valuerm['user_id'].", " ;
  								}
  				$whereStudentId = rtrim($whereStudentId,", ");

        }

        $notificationList = $this->user_model->updateNotificationList($whereStudentId, $usrdata['type']);

        $response['status'] = "SUCCESS";

        header('Content-Type: application/json');
        echo (json_encode($response));

      }

}
