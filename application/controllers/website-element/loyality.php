<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Loyality extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

	
	function index()
	{   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('website-element/property_model');
		 $this->load->model('website-element/loyality_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }	
		
		$cond = array('loyality_master.status' => '1');
		$this->outputData['loyalities'] = $this->loyality_model->getLoyalities($cond);
        $this->render_page('templates/website-element/loyality',$this->outputData);
	
} 
	
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('website-element/property_model');
		 $this->load->model('website-element/loyality_model');
		 $this->load->model('website-element/place_model');
		 $this->load->model('website-element/amenity_model');
		 $this->load->model('location/country_model');
		 $this->load->model('location/city_model');
		 $this->load->model('plugins/gallery_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }		
		 
		 if($this->uri->segment('4')){
		 $conditions = array(
	 				  'loyality_master.id' => $this->uri->segment('4')
	 					);
		 $loyality_info = $this->loyality_model->getLoyalityByid($conditions);
		
		$this->outputData['id'] 	= $loyality_info->id;
		$this->outputData['name'] 	= $loyality_info->name;
		$this->outputData['description'] 	= $loyality_info->description;
		
	    $cond = array('loyality_to_property.loyality_id' => $this->outputData['id']);
		
		$this->outputData['prop_loyality'] = $this->loyality_model->getPropertiesBy_Loyality($cond);
	
		$this->outputData['fileid'] 	= $loyality_info->fileid;
		$this->outputData['file_name'] 	= $loyality_info->file_name;
		$this->outputData['file_type'] 	= $loyality_info->file_type;
		$this->outputData['path'] 	= $loyality_info->path;
		$this->outputData['file_size'] 	= $loyality_info->file_size;
		
		$this->outputData['status'] 	= $loyality_info->status;
		
		// Sort order data
		$this->outputData['sort_order'] 	= $loyality_info->sort_order;
		
		// Photo gallery data
		$photo_gallery = array(
	 				 'loyality_id' => $loyality_info->id,
					 'type' 		=>'photo'
	 				);
		$this->outputData['selected_photo']			=	$this->gallery_model->getGallery_By_Loyality($photo_gallery);
		
		// Video gallery data
		$video_gallery = array(
	 				 'loyality_id' => $loyality_info->id,
					 'type' 		=>'video'
	 				);
		$this->outputData['selected_video']			=	$this->gallery_model->getGallery_By_Loyality($video_gallery);
		
		 }
		$cond = array('property_master.status_id' => '1'); 
		$this->outputData['properties']	=	$this->property_model->getProperties($cond,$array=array());
		$cond_gallery = array('gallery_master.status' => '1'); 
		$this->outputData['photos']		=	$this->gallery_model->getPhotoGallaries($cond_gallery,$array=array());
		$this->outputData['videos']		=	$this->gallery_model->getVideoGallaries($cond_gallery,$array=array());
		$this->render_page('templates/website-element/loyality_form',$this->outputData);
	
     } 
	 
	 
	 
	 function create()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/property_model'); 
		$this->load->model('website-element/loyality_model');
		$this->load->model('administrator/common_model');
		$this->load->model('location/country_model');
		$this->load->model('location/city_model');
		$this->outputData['countries']	= $this->country_model->getCountries();   
		$this->outputData['cities']	= $this->city_model->getCities();
		
		$this->form_validation->set_rules('loyality_name', 'Loyality Name', 'required|trim|xss_clean');
		$this->form_validation->set_rules('loyality_sort_order', 'Loyality_sort_order', 'required|greater_than[0]');
		
		$this->form_validation->set_rules('loyality_description', 'Loyality_Description', 'required|trim|xss_clean');
		


		if ($this->form_validation->run() == FALSE)
		{
			// redirect('website-element/brand/form');
			  $this->render_page('templates/website-element/loyality_form');
		}
		else
		{
			
		 $logo_info = $this->common_model->getFileinfo($this->input->post('loyality_logo'));
	 
	  	$data = array(
     	'name' 			=>	$this->input->post('loyality_name'),
     	'description'	=>	$this->input->post('loyality_description'),
     	'sort_order' 	=>	$this->input->post('loyality_sort_order'),
	 	'status' 		=>	$this->input->post('loyality_status'),
	 	'properties'	=>	$this->input->post('mult_properties'),
	 	'photo' 		=>	$this->input->post('photo_gallery'),
	 	'video' 		=>	$this->input->post('video_gallery'),
	 	'logo_path' 	=>	$this->input->post('loyality_logo'),
	 	'logo_name' 	=>	$logo_info['logo_name'],
	 	'logo_type' 	=>	$logo_info['logo_type'],
	 	'logo_size' 	=>	$logo_info['logo_size']
		);
	 
	 	if($this->loyality_model->insertLoyality($data)=='success'){
	 
	  	$this->session->set_flashdata('flash_message', "Success: You have saved Loyality!");
	  	redirect('website-element/loyality/index');
	  	}
	  }
	}
	 
	 
	 function edit()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/property_model');
		$this->load->model('website-element/loyality_model');
		$this->load->model('administrator/common_model');
		
		$this->form_validation->set_rules('loyality_name', 'Loyality Name', 'required|trim|xss_clean');
		//$this->form_validation->set_rules('loyality_description', 'Loyality_Description', 'required|trim|xss_clean');
		$this->form_validation->set_rules('loyality_sort_order', 'Loyality Sort Order', 'integer');
	

		if ($this->form_validation->run() == FALSE)
		{
			
			  $this->render_page('templates/website-element/loyality_form');
		}
		else
		{     //echo $this->input->post('file_id'); die('+++++');
		      $logo_info = $this->common_model->getFileinfo($this->input->post('loyality_logo'));
			  
			  
			  $data = array(
			  
              'id'				=>  $this->uri->segment('4'),
			  'name' 			=>	$this->input->post('loyality_name'),
              'description'		=>	$this->input->post('loyality_description'),
              'sort_order' 		=>	$this->input->post('loyality_sort_order'),
	          'status' 			=>	$this->input->post('loyality_status'),
	          'properties'		=>	$this->input->post('mult_properties'),
	          'photo' 			=>	$this->input->post('photo_gallery'),
	          'video' 			=>	$this->input->post('video_gallery'),
			  'loyality_logo' 	=>  $this->input->post('file_id'),
			  'logo_path' 		=>	$this->input->post('loyality_logo'),
	 		  'logo_name' 		=>	$logo_info['logo_name'],
	 	      'logo_type' 		=>	$logo_info['logo_type'],
	          'logo_size' 		=>	$logo_info['logo_size']
	
	 );
				
				
	         if($this->loyality_model->editLoyality($data)=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have modified Loyality!");
	         redirect('website-element/loyality/index');
	         }
	 
	 
	    }
	}
    
	
	function delete()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/loyality_model');
		
		
		
		if($this->loyality_model->deleteLoyality($this->uri->segment('4'))=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have deleted Property!");
	         redirect('website-element/loyality/index');
	        
	 
	 
	    }
	}
	
	
	
	function delete_mult(){
	
	    $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
       $this->load->model('website-element/loyality_model');
		
		$array = $this->input->post('chk');
		foreach($array as $id):
		
		$this->loyality_model->deleteLoyality($id);
		
		endforeach;
		$this->session->set_flashdata('flash_message', "Success: You have deleted Loyality!");
		redirect('website-element/loyality/index');
	
	
	}
	 
	 
	 
	 

	

}//End  Home Class





?>