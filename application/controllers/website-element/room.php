<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Room extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

	
	function index()
	{   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('website-element/room_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }	
		 
		$cond_room1 = array('room_type_master.status' => '1'); 
		$cond_room2 = array('room_type_master.status' => '2'); 	
        $this->outputData['rooms'] = $this->room_model->getRooms($cond_room1,$cond_room2);
        $this->render_page('templates/website-element/room',$this->outputData);
	
    } 
	
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('website-element/room_model');
		 $this->load->model('website-element/property_model');
		 $this->load->model('website-element/amenity_model');    
		  $this->load->model('plugins/gallery_model');
		  
		  
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }		
		 
		 if($this->uri->segment('4')){
		 $conditions = array(
	 				  'room_type_master.id' => $this->uri->segment('4')
	 					);
		 $room_info = $this->room_model->getRoomByid($conditions);
		
		foreach($room_info as $room_in){
		$this->outputData['id'] 				= $room_in->id;
		$this->outputData['name'] 				= $room_in->name;
		$this->outputData['description'] 		= $room_in->description;
		$this->outputData['sort_order'] 		= $room_in->sort_order;
		$this->outputData['room_rate'] 			= $room_in->rate;
		$this->outputData['selected_enquery'] 	= $room_in->enquery;
		$this->outputData['selected_booked'] 	= $room_in->booking;
		$this->outputData['virtual_tour'] 		= $room_in->virtual_tour;
		}
		
		$cond = array(
	 				  'room_to_property.room_type_id' => $this->outputData['id']
	 					);
		
		$this->outputData['prop_room'] = $this->room_model->getPropertiesBy_Room($cond);
		
		$cond2 = array(
	 				  'amenities_to_rooms.room_type_id' => $this->outputData['id']
	 					);
		
		$this->outputData['amenty_room'] = $this->room_model->getAmenitiesBy_Room($cond2);
        
		$cond3 = array(
	 				  'gallery_to_room.room_type_id' => $this->outputData['id'],
					  'gallery_master.type' => 'photo',
					  'gallery_master.status' => '1'
	 					);
		
		$this->outputData['selected_photos'] = $this->gallery_model->getGalleryBy_Room($cond3);
		
	    $cond4 = array(
	 				  'gallery_to_room.room_type_id' => $this->outputData['id'],
					  'gallery_master.type' => 'video',
					  'gallery_master.status' => '1'
	 					);
		
		$this->outputData['selected_videos'] = $this->gallery_model->getGalleryBy_Room($cond4);
		  }
		 
		$cond = array('property_master.status_id' => '1'); 
		$this->outputData['properties']	=	$this->property_model->getProperties($cond, $array=array());
		$amenity_conditions = array('amenities_master.for' =>'property','amenities_master.status' =>'1');
		$this->outputData['amenities']	= 	$this->amenity_model->getAmenities_by_Attribute($amenity_conditions);
		
		$cond2 = array('gallery_master.status' => '1'); 
		$this->outputData['photos']		=	$this->gallery_model->getPhotoGallaries($cond2,$array=array());
		$this->outputData['videos']		=	$this->gallery_model->getVideoGallaries($cond2,$array=array());
        $this->render_page('templates/website-element/room_form',$this->outputData);
	
     } 
	 
	 
	 
	 function create()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
       $this->load->model('website-element/room_model');
	   
	   $this->form_validation->set_rules('room_name', 'Room Name', 'required|trim|xss_clean');
	   $this->form_validation->set_rules('room_rate', 'Room Rate', 'required|trim|xss_clean|numeric');
	   
	   if ($this->form_validation->run() == FALSE)
		{
		
			  $this->render_page('templates/website-element/room_form');
		}
		else
		{
		
		 $data = array(
        'name' 			=> $this->input->post('room_name'),
        'description' 	=> 	$this->input->post('room_description'),
        'room_meta_tag' 	=> 	$this->input->post('room_meta_tag'),
        'room_meta_description' 	=> 	$this->input->post('room_meta_description'),
        'sort_order' 	=>	$this->input->post('room_sort_order'),
		'properties' 	=> 	$this->input->post('mult_properties'),
		'amenities' 	=> 	$this->input->post('mult_amenities'),
		'room_rate'		=> 	$this->input->post('room_rate'),
		'enquery' 		=>  $this->input->post('room_enquery'),
		'booking' 		=>  $this->input->post('room_booked'),
		'photo' 		=>	$this->input->post('photo_gallery'),
	 	'video' 		=>	$this->input->post('video_gallery'),
		'virtual_tour' 	=>	(string)$this->input->post('room_virtual_tour')
	     );
	    
		 if($this->room_model->insertRoom($data)=='success'){
	 
	     $this->session->set_flashdata('flash_message', "Success: You have saved Room!");
	     redirect('website-element/room/index');
	     }
	 
	 
	   }
	}
	 
	 
	 function edit_room()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/room_model');
		$this->form_validation->set_rules('room_name', 'Room Name', 'required|trim|xss_clean');
		$this->form_validation->set_rules('room_rate', 'Room Rate', 'required|trim|xss_clean|numeric');
	

		if ($this->form_validation->run() == FALSE)
		{
			
			  $this->render_page('templates/website-element/room_form');
		}
		else
		{
			 $data = array(
            'room_id' 			=> 	$this->uri->segment('4'),
			'name' 				=> 	$this->input->post('room_name'),
            'description' 		=> 	$this->input->post('room_description'),
			'room_meta_tag' 	=> 	$this->input->post('room_meta_tag'),
            'room_meta_description' 	=> 	$this->input->post('room_meta_description'),
            'sort_order' 		=>	$this->input->post('room_sort_order'),
			'properties' 		=> 	$this->input->post('mult_properties'),
			'amenities' 		=> 	$this->input->post('mult_amenities'),
			'room_rate'			=> 	$this->input->post('room_rate'),
			'enquery' 			=>  $this->input->post('room_enquery'),
			'booking' 			=>  $this->input->post('room_booked'),
			'photo' 			=>	$this->input->post('photo_gallery'),
	 		'video' 			=>	$this->input->post('video_gallery'),
			'virtual_tour' 		=>	(string)$this->input->post('room_virtual_tour')
	        );
			
	         if($this->room_model->editRoom($data)=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have modified Room!");
	         redirect('website-element/room/index');
	         }
	    }
	}
    
	
	function delete_room()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
       $this->load->model('website-element/room_model');
		
		if($this->room_model->deleteRoom($this->uri->segment('4'))=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have deleted Room!");
	         redirect('website-element/room/index');
	    }
	}
	
	
	function delete_mult_room(){
	
	    $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/room_model');
		
		$array = $this->input->post('chk');
		foreach($array as $id):
		
		$this->room_model->deleteRoom($id);
		
		endforeach;
		$this->session->set_flashdata('flash_message', "Success: You have deleted Rooms!");
		redirect('website-element/room/index');
	
	
	}
	 

}//End  Home Class

?>