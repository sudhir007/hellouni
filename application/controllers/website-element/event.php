<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Event extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

	
	function index()
	{   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('website-element/property_model');
		 $this->load->model('website-element/event_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }	
		
		$cond = array('event_master.status' => '1');
		$this->outputData['events'] = $this->event_model->getEvents($cond);
        $this->render_page('templates/website-element/event',$this->outputData);
	
} 
	
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('website-element/property_model');
		 $this->load->model('website-element/event_model');
		 $this->load->model('plugins/gallery_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }		
		 
		 if($this->uri->segment('4')){
		 $conditions = array(
	 				  'event_master.id' => $this->uri->segment('4')
	 					);
		 $event_info = $this->event_model->getEventByid($conditions);
		 /*echo '<pre>';
		 print_r($loyality_info);
		 echo '</pre>';
		 exit();*/
		 //foreach($property_info as $prop_in):
		$this->outputData['id'] 	= $event_info->id;
		$this->outputData['name'] 	= $event_info->name;
		$this->outputData['description'] 	= $event_info->description;
		
	    $cond = array('event_to_property.event_id' => $this->outputData['id']);
		
		$this->outputData['prop_event'] = $this->event_model->getPropertiesBy_event($cond,$array=array());
	
	/*echo '<pre>';
		 print_r($this->outputData['prop_loyality']);
		 echo '</pre>';
		 exit();*/
	    
		$this->outputData['fileid'] 	= $event_info->fileid;
		$this->outputData['file_name'] 	= $event_info->file_name;
		$this->outputData['file_type'] 	= $event_info->file_type;
		$this->outputData['path'] 	= $event_info->path;
		$this->outputData['file_size'] 	= $event_info->file_size;
		
		$this->outputData['status'] 	= $event_info->status;
		
		// Sort order data
		$this->outputData['sort_order'] 	= $event_info->sort_order;
		
		// Photo gallery data
		$photo_gallery = array(
	 				 'event_id' => $event_info->id,
					 'type' 		=>'photo'
	 				);
		$this->outputData['selected_photo']			=	$this->gallery_model->getGallery_By_Event($photo_gallery);
		
		// Video gallery data
		$video_gallery = array(
	 				 'event_id' => $event_info->id,
					 'type' 		=>'video'
	 				);
		$this->outputData['selected_video']			=	$this->gallery_model->getGallery_By_Event($video_gallery);
		//print_r($this->outputData['selected_video']);
		//exit();
		
		 }
		 $cond = array('property_master.status_id' => '1'); 
		$this->outputData['properties']	=	$this->property_model->getProperties($cond,$array=array());
		$cond_gallery = array('gallery_master.status' => '1'); 
		$this->outputData['photos']		=	$this->gallery_model->getPhotoGallaries($cond_gallery,$array=array());
		$this->outputData['videos']		=	$this->gallery_model->getVideoGallaries($cond_gallery,$array=array());
		$this->render_page('templates/website-element/event_form',$this->outputData);
	
     } 
	 
	 
	 
	 function create()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/property_model'); 
		$this->load->model('website-element/event_model');
		$this->load->model('administrator/common_model');
		
		
		$this->form_validation->set_rules('event_name', 'Event Name', 'required|trim|xss_clean');
		$this->form_validation->set_rules('event_sort_order', 'Event_sort_order', 'required|greater_than[0]');
		
		$this->form_validation->set_rules('event_description', 'Event_Description', 'required|trim|xss_clean');
		


		if ($this->form_validation->run() == FALSE)
		{
			// redirect('website-element/brand/form');
			  $this->render_page('templates/website-element/event_form');
		}
		else
		{
			//$this->load->view('formsuccess');
		 $logo_info = $this->common_model->getFileinfo($this->input->post('event_logo'));
	 
	  $data = array(
     'name' 		=>	$this->input->post('event_name'),
     'description'	=>	$this->input->post('event_description'),
     'sort_order' 	=>	$this->input->post('event_sort_order'),
	 'status' 		=>	$this->input->post('event_status'),
	 'properties'	=>	$this->input->post('mult_properties'),
	 'photo' 		=>	$this->input->post('photo_gallery'),
	 'video' 		=>	$this->input->post('video_gallery'),
	 'logo_path' 	=>	$this->input->post('event_logo'),
	 'logo_name' 	=>	$logo_info['logo_name'],
	 'logo_type' 	=>	$logo_info['logo_type'],
	 'logo_size' 	=>	$logo_info['logo_size']
	
	 );
	 
	 
	 
	 
	 if($this->event_model->insertEvent($data)=='success'){
	 
	  $this->session->set_flashdata('flash_message', "Success: You have saved event!");
	  redirect('website-element/event/index');
	  }
	 
	 
	 }
	}
	 
	 
	 function edit()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/property_model'); 
		$this->load->model('website-element/event_model');
		$this->load->model('administrator/common_model');
		
		$this->form_validation->set_rules('event_name', 'Event Name', 'required|trim|xss_clean');
		$this->form_validation->set_rules('event_sort_order', 'Event_sort_order', 'required|greater_than[0]');
		
		$this->form_validation->set_rules('event_description', 'Event_Description', 'required|trim|xss_clean');
	

		if ($this->form_validation->run() == FALSE)
		{
			
			  $this->render_page('templates/website-element/event_form');
		}
		else
		{     //echo $this->input->post('file_id'); die('+++++');
		      $logo_info = $this->common_model->getFileinfo($this->input->post('event_logo'));
			  
			  
			 $data = array(
			 	'id'			=>  $this->uri->segment('4'),
     			'name' 		=>	$this->input->post('event_name'),
     			'description'	=>	$this->input->post('event_description'),
     			'sort_order' 	=>	$this->input->post('event_sort_order'),
	 			'status' 		=>	$this->input->post('event_status'),
	 			'properties'	=>	$this->input->post('mult_properties'),
	 			'photo' 		=>	$this->input->post('photo_gallery'),
	 			'video' 		=>	$this->input->post('video_gallery'),
				'event_logo' =>  $this->input->post('file_id'),
	 			'logo_path' 	=>	$this->input->post('event_logo'),
	 			'logo_name' 	=>	$logo_info['logo_name'],
	 			'logo_type' 	=>	$logo_info['logo_type'],
	 			'logo_size' 	=>	$logo_info['logo_size']
	
	
	 );
				
				
	         if($this->event_model->editEvent($data)=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have modified Event!");
	         redirect('website-element/event/index');
	         }
	 
	 
	    }
	}
    
	
	function delete()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/loyality_model');
		
		
		
		if($this->loyality_model->deleteLoyality($this->uri->segment('4'))=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have deleted Property!");
	         redirect('website-element/loyality/index');
	        
	 
	 
	    }
	}
	
	
	
	function delete_mult_properties(){
	
	    $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
       $this->load->model('website-element/loyality_model');
		
		$array = $this->input->post('chk');
		foreach($array as $id):
		
		$this->loyality_model->deleteLoyality($id);
		
		endforeach;
		$this->session->set_flashdata('flash_message', "Success: You have deleted Loyality!");
		redirect('website-element/loyality/index');
	
	
	}
	 
	 
	 
	 

	

}//End  Home Class





?>