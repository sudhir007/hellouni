<?php
require APPPATH . "vendor/autoload.php";

use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;

class Tokbox extends CI_Controller
{
    public function index()
    {
        $tokenId = $this->input->get('id');
        if(!$tokenId)
        {
            redirect('/');
        }
        $usrdata = $this->session->userdata('user');
        $uid = $this->input->get('uid');
        if(empty($usrdata) && !$uid){
            $data['error'] = "Please login first to join conference";
        }
        else{
            $this->load->model('tokbox_model');
            $tokenId = base64_decode($tokenId);
            $tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
            if(!$tokboxDetail){
                redirect('/');
            }

            $data['student'] = false;

            if($usrdata['typename'] == 'Student'){
                $this->load->model('user/user_model');
                $participants = json_decode($tokboxDetail->participants, true);
                $slotId = $participants['slot_id'];
                $updatedData['status'] = 'Attained';
                $this->user_model->editSlot($slotId, $updatedData);
                $data['student'] = true;
            }
            $opentok = new OpenTok('46220952', '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2');
            // Create a session that attempts to use peer-to-peer streaming:
            //$session = $opentok->createSession();
            // Store this sessionId in the database for later use
            //$sessionId = $session->getSessionId();
            //$sessionId = '2_MX40NjIyMDk1Mn5-MTU0MjI2NjMxMzE1M35HQ2FQV2JQNFBKSXVTa0RMelJLdVdtczJ-fg';
            // Generate a Token from just a sessionId (fetched from a database)
            //$token = $opentok->generateToken($sessionId);
            $token = $tokboxDetail->token;
            $sessionId = $tokboxDetail->session_id;
            //echo "no isse - " . $sessionId;
            $data['sessionId'] = $sessionId;
            $data['token'] = $token;
            $data['tokboxId'] = $tokenId;
        }
        $this->load->view('tokbox', $data);
    }

    public function token()
    {
        $this->load->model('tokbox_model');
        $opentok = new OpenTok('46220952', '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2');
        $sessionId = '2_MX40NjIyMDk1Mn5-MTU0MjI2NjMxMzE1M35HQ2FQV2JQNFBKSXVTa0RMelJLdVdtczJ-fg';
        $token = $opentok->generateToken($sessionId);

        $data['token'] = $token;
        $data['created_at'] = date('Y-m-d H:i:s');
        $tokenId = $this->tokbox_model->addToken($data);
        echo 'Token Id - ' . $tokenId;
    }

	public function startArchive()
	{
		$jsonBody = file_get_contents('php://input');
		$arrayBody = json_decode($jsonBody, true);
		$sessionId = $arrayBody['sessionId'];
        $tokboxId = $arrayBody['tokboxId'];

		$opentok = new OpenTok('46220952', '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2');
		// Create a simple archive of a session
		//$archive = $opentok->startArchive($sessionId);

		// Create an archive using custom options
		$archiveOptions = array(
    			'name' => 'Important Presentation',     // default: null
    			'hasAudio' => true,                     // default: true
    			'hasVideo' => true,                     // default: true
    			//'outputMode' => OutputMode::COMPOSED,   // default: OutputMode::COMPOSED
    			'resolution' => '1280x720'              // default: '640x480'
		);
		$archive = $opentok->startArchive($sessionId, $archiveOptions);

		// Store this archiveId in the database for later use
		$archiveId = $archive->id;

        $this->load->model('tokbox_model');
        $updatedData['archive_id'] = $archiveId;
        $this->tokbox_model->editToken($updatedData, $tokboxId);
	}

	public function stopArchive($archiveId)
	{
        $opentok = new OpenTok('46220952', '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2');
        $opentok->stopArchive($archiveId);
	}

    public function viewArchive($archiveId)
    {
        $opentok = new OpenTok('46220952', '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2');
        $archive = $opentok->getArchive($archiveId);
        if ($archive->status=='available') {
           redirect($archive->url);
        }
        else {
           redirect('/');
        }
    }

    public function webinar($type=NULL)
    {
        if($type)
        {
            $tokenId = $this->input->get('id');
            $mobile = $this->input->get('m');
            if(!$tokenId)
            {
                redirect('/');
            }

            $usrdata = $this->session->userdata('usrdata');
            if(!$usrdata)
            {
                redirect('/tokbox/form/' . $type . '?id=' . $tokenId . '&m=' . $mobile);
            }

            $this->load->model('tokbox_model');
            $this->load->model('webinar/common_model');

            $tokenId = base64_decode($tokenId);
            $tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
            if(!$tokboxDetail){
                redirect('/');
            }

            $whereCondition = array("tokbox_token_id" => $tokenId);
            $webinarData = $this->common_model->get($whereCondition, 'webinar_master');
            if(!$webinarData){
                redirect('/');
            }

            $currentRole = $type;

            switch($currentRole)
            {
                case 'organizer':
                    $currentRole = 'moderator';
                    break;
                case 'attendee':
                    $currentRole = 'subscriber';
                    break;
                case 'presenter':
                    $currentRole = 'publisher';
                    break;
            }

            $guestName = $usrdata['name'];
            $guestId = $usrdata['id'];
            $connectionMetaData = $guestName;
            $sessionId = $tokboxDetail->session_id;

            $polling_questions = $this->tokbox_model->getAllActivePolls();

            if($polling_questions)
            {
                foreach($polling_questions as $index => $poll_question)
                {
                    $polling_questions[$index]['question_string'] = $poll_question['question'];
                    $choices = [];
                    $options = json_decode($poll_question['options'], true);
                    foreach($options as $option)
                    {
                        if($option == $poll_question['answer'])
                        {
                            $choices['correct'] = $option;
                        }
                        else
                        {
                            $choices['wrong'][] = $option;
                        }
                    }
                    $polling_questions[$index]['choices'] = $choices;
                }
            }
            $polling = (isset($_GET['polling']) || $currentRole == 'moderator') ? 1 : 0;

            $sentMessages = $this->tokbox_model->getChatFromUserSession($guestId, 'GUEST', $sessionId);
            $receivedMessages = $this->tokbox_model->getChatToUserSession($guestId, 'GUEST', $sessionId);
            $sentUsers = array();
            $sentGuests = array();
            $sentUserNames = array();
            $sentGuestNames = array();
            if($receivedMessages)
            {
                foreach($receivedMessages as $receivedMessage)
                {
                    if($receivedMessage['user_type'] == 'USER' && !in_array($receivedMessage['from'], $sentUsers))
                    {
                        $sentUsers[] = $receivedMessage['from'];
                    }
                    else if($receivedMessage['user_type'] == 'GUEST' && !in_array($receivedMessage['from'], $sentGuests))
                    {
                        $sentGuests[] = $receivedMessage['from'];
                    }
                }
                if($sentUsers)
                {
                    $sentUserNames = $this->tokbox_model->getUserName($sentUsers);
                }
                if($sentGuests)
                {
                    $sentGuestNames = $this->tokbox_model->getGuestName($sentGuests);
                }
            }

            $allMessages = [];
            if($sentMessages)
            {
                foreach($sentMessages as $sentMessage)
                {
                    $allMessages[$sentMessage['id']] = $sentMessage;
                }
            }
            if($receivedMessages)
            {
                foreach($receivedMessages as $receivedMessage)
                {
                    $allMessages[$receivedMessage['id']] = $receivedMessage;
                }
            }

            if($allMessages)
            {
                ksort($allMessages);
                foreach($allMessages as $index => $message)
                {
                    if($message['chat_of'] == 'theirs')
                    {
                        if(in_array($message['from'], $sentUsers))
                        {
                            foreach($sentUserNames as $sentUserName)
                            {
                                if($sentUserName['id'] == $message['from'])
                                {
                                    $message['name'] = $sentUserName['name'];
                                    $allMessages[$index] = $message;
                                }
                            }
                        }
                        else if(in_array($message['from'], $sentGuests))
                        {
                            foreach($sentGuestNames as $sentGuestName)
                            {
                                if($sentGuestName['id'] == $message['from'])
                                {
                                    $message['name'] = $sentGuestName['name'];
                                    $allMessages[$index] = $message;
                                }
                            }
                        }
                    }
                    else
                    {
                        $message['name'] = $guestName;
                        $allMessages[$index] = $message;
                    }
                }
            }

            $commonChat = array();
            $privateChat = array();
            if($allMessages)
            {
                foreach($allMessages as $chatMessage)
                {
                    if($chatMessage['to'] == -1)
                    {
                        $commonChat[] = $chatMessage;
                    }
                    else
                    {
                        $privateChat[] = $chatMessage;
                    }
                }
            }

            $opentok = new OpenTok('46220952', '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2');
            $data['token'] = $opentok->generateToken($sessionId, array('role' => $currentRole, 'expireTime' => time()+(7 * 24 * 60 * 60), 'data' =>  $connectionMetaData));
            $data['sessionId'] = $sessionId;
            $data['username'] = $guestName;
            $data['tokboxId'] = $tokenId;
            $data['role'] = $currentRole;
            $data['polling'] = $polling;
            $data['polling_questions'] = json_encode($polling_questions);
            $data['common_chat_history'] = $commonChat;
            $data['private_chat_history'] = $privateChat;
            $data['email'] = $usrdata['email'];

            if($this->input->get('m') || $currentRole == 'subscriber')
            {
                $this->load->view('test', $data);
            }
            else
            {
                $this->load->view('webinar', $data);
            }
        }
        else
        {
            $tokenId = $this->input->get('id');
            $mobile = $this->input->get('m');
            if(!$tokenId)
            {
                redirect('/');
            }
            $usrdata = $this->session->userdata('user');

            $uid = $this->input->get('uid');
            if(empty($usrdata) && !$uid)
            {
                redirect('/tokbox/webinar/attendee?id=' . $tokenId . '&m=' . $mobile);
            }
            else
            {
                $this->load->model('tokbox_model');
                $this->load->model('webinar/common_model');

                $tokenId = base64_decode($tokenId);
                $tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
                if(!$tokboxDetail){
                    redirect('/');
                }

                $whereCondition = array("tokbox_token_id" => $tokenId);
                $webinarData = $this->common_model->get($whereCondition, 'webinar_master');
                if(!$webinarData){
                    redirect('/');
                }

                $participants = json_decode($tokboxDetail->participants, true);
                $currentRole = 'subscriber';
                foreach($participants as $role => $ids)
                {
                    $explodeIds = explode(',', $ids);
                    {
                        if(in_array($usrdata['id'], $explodeIds))
                        {
                            $currentRole = $role;
                        }
                    }
                }
		$currentRole = 'presenter';

                switch($currentRole)
                {
                    case 'organizer':
                        $currentRole = 'moderator';
                        break;
                    case 'attendee':
                        $currentRole = 'subscriber';
                        break;
                    case 'presenter':
                        $currentRole = 'publisher';
                        break;
                }

                $username = $usrdata['username'];
                $userId = $usrdata['id'];
                //$connectionMetaData = "username=$username,userLevel=4";
                $connectionMetaData = $username;
                $sessionId = $tokboxDetail->session_id;

                $polling_questions = $this->tokbox_model->getAllActivePolls();

                if($polling_questions)
                {
                    foreach($polling_questions as $index => $poll_question)
                    {
                        $polling_questions[$index]['question_string'] = $poll_question['question'];
                        $choices = [];
                        $options = json_decode($poll_question['options'], true);
                        foreach($options as $option)
                        {
                            if($option == $poll_question['answer'])
                            {
                                $choices['correct'] = $option;
                            }
                            else
                            {
                                $choices['wrong'][] = $option;
                            }
                        }
                        $polling_questions[$index]['choices'] = $choices;
                    }
                }
                $polling = (isset($_GET['polling']) || $currentRole == 'moderator') ? 1 : 0;

                $sentMessages = $this->tokbox_model->getChatFromUserSession($userId, 'USER', $sessionId);
                $receivedMessages = $this->tokbox_model->getChatToUserSession($userId, 'USER', $sessionId);
                $sentUsers = array();
                $sentGuests = array();
                $sentUserNames = array();
                $sentGuestNames = array();
                if($receivedMessages)
                {
                    foreach($receivedMessages as $receivedMessage)
                    {
                        if($receivedMessage['user_type'] == 'USER' && !in_array($receivedMessage['from'], $sentUsers))
                        {
                            $sentUsers[] = $receivedMessage['from'];
                        }
                        else if($receivedMessage['user_type'] == 'GUEST' && !in_array($receivedMessage['from'], $sentGuests))
                        {
                            $sentGuests[] = $receivedMessage['from'];
                        }
                    }

                    if($sentUsers)
                    {
                        $sentUserNames = $this->tokbox_model->getUserName($sentUsers);
                    }
                    if($sentGuests)
                    {
                        $sentGuestNames = $this->tokbox_model->getGuestName($sentGuests);
                    }
                }

                $allMessages = [];
                if($sentMessages)
                {
                    foreach($sentMessages as $sentMessage)
                    {
                        $allMessages[$sentMessage['id']] = $sentMessage;
                    }
                }
                if($receivedMessages)
                {
                    foreach($receivedMessages as $receivedMessage)
                    {
                        $allMessages[$receivedMessage['id']] = $receivedMessage;
                    }
                }

                if($allMessages)
                {
                    ksort($allMessages);
                    foreach($allMessages as $index => $message)
                    {
                        if($message['chat_of'] == 'theirs')
                        {
                            if(in_array($message['from'], $sentUsers))
                            {
                                foreach($sentUserNames as $sentUserName)
                                {
                                    if($sentUserName['id'] == $message['from'])
                                    {
                                        $message['name'] = $sentUserName['name'];
                                        $allMessages[$index] = $message;
                                    }
                                }
                            }
                            else if(in_array($message['from'], $sentGuests))
                            {
                                foreach($sentGuestNames as $sentGuestName)
                                {
                                    if($sentGuestName['id'] == $message['from'])
                                    {
                                        $message['name'] = $sentGuestName['name'];
                                        $allMessages[$index] = $message;
                                    }
                                }
                            }
                        }
                        else
                        {
                            $message['name'] = $username;
                            $allMessages[$index] = $message;
                        }
                    }
                }

                $commonChat = array();
                $privateChat = array();
                if($allMessages)
                {
                    foreach($allMessages as $chatMessage)
                    {
                        if($chatMessage['to'] == -1)
                        {
                            $commonChat[] = $chatMessage;
                        }
                        else
                        {
                            $privateChat[] = $chatMessage;
                        }
                    }
                }

                $opentok = new OpenTok('46220952', '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2');
                $data['token'] = $opentok->generateToken($sessionId, array('role' => $currentRole, 'expireTime' => time()+(7 * 24 * 60 * 60), 'data' =>  $connectionMetaData));
                $data['sessionId'] = $sessionId;
                $data['username'] = $username;
                $data['tokboxId'] = $tokenId;
                $data['role'] = $currentRole;
                $data['polling'] = $polling;
                $data['polling_questions'] = json_encode($polling_questions);
                $data['common_chat_history'] = $commonChat;
                $data['private_chat_history'] = $privateChat;

                if($this->input->get('m'))
                {
                    $this->load->view('test', $data);
                }
                else
                {
                    $this->load->view('webinar', $data);
                }
            }
        }
    }

    function form($type, $tokenId = NULL, $mobile_option = NULL)
    {
        $this->load->model('tokbox_model');

        if($this->input->post())
        {
            $post_data = $this->input->post();
            $post_data['have_i20'] = $post_data['have_i20'] ? $post_data['have_i20'] : NULL;
            $post_data['filling_visa'] = $post_data['filling_visa'] ? $post_data['filling_visa'] : NULL;
            $post_data['name'] = $post_data['first_name'] . ' ' . $post_data['last_name'];

            /*$checkGuest = $this->tokbox_model->checkGuest($post_data['email'], $post_data['session_id']);
            if($checkGuest)
            {
                $post_data['tokenId'] = $tokenId;
                $post_data['mobile_option'] = $mobile_option;
                $post_data['sessionId'] = $post_data['session_id'];
                $post_data['error'] = "The email is already registered";
                if($type == 'attendee')
                {
                    $this->load->view('tokbox_form', $post_data);
                }
                else
                {
                    $this->load->view('organizer_form', $post_data);
                }
            }
            else
            {
                $addGuest = $this->tokbox_model->addGuest($post_data);
                $addGuest['typename'] = 'Guest';
                $this->session->set_userdata('usrdata', $addGuest);
                redirect('/tokbox/webinar/' . $type . '?id=' . base64_encode($tokenId) . '&m=' . $mobile_option);
            }*/
            if(!trim($post_data['first_name']) || !trim($post_data['email']))
            {
                $post_data['tokenId'] = $tokenId;
                $post_data['mobile_option'] = $mobile_option;
                $post_data['sessionId'] = $post_data['session_id'];
                $post_data['error'] = "Name & Email are mandatory";
                if($type == 'attendee')
                {
                    $this->load->view('tokbox_form', $post_data);
                }
                else
                {
                    $this->load->view('organizer_form', $post_data);
                }
            }
            else
            {
                $addGuest = $this->tokbox_model->addGuest($post_data);
                $addGuest['typename'] = 'Guest';
                $this->session->set_userdata('usrdata', $addGuest);
                redirect('/tokbox/webinar/' . $type . '?id=' . base64_encode($tokenId) . '&m=' . $mobile_option);
            }
        }
        else
        {
            $tokenId = $this->input->get('id');
            $mobile_option = $this->input->get('m');
            if(!$tokenId)
            {
                redirect('/');
            }

            $tokenId = base64_decode($tokenId);
            $tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
            if(!$tokboxDetail)
            {
                redirect('/');
            }
            $sessionId = $tokboxDetail->session_id;

            $data['type'] = $type;
            $data['sessionId'] = $sessionId;
            $data['tokenId'] = $tokenId;
            $data['mobile_option'] = $mobile_option;
            if($type == 'attendee')
            {
                $this->load->view('tokbox_form', $data);
            }
            else
            {
                $this->load->view('organizer_form', $data);
            }
        }
    }

    function chat()
    {
        $usrdata = $this->session->userdata('usrdata') ? $this->session->userdata('usrdata') : $this->session->userdata('user');
        $post_data = json_decode(file_get_contents("php://input"), true);

        $this->load->model('tokbox_model');
        $to = ($post_data['to'] == 'All') ? '-1' : $post_data['to'];
        $toUserType = NULL;
        if($to != -1)
        {
            $connectionDetail = $this->tokbox_model->checkConnection($to, $post_data['sessionId']);
            $to = $connectionDetail['user_id'];
            $toUserType = $connectionDetail['user_type'];
        }

        $data['from'] = $usrdata['id'];
        $data['to'] = $to;
        $data['to_user_type'] = $toUserType;
        $data['session_id'] = $post_data['sessionId'];
        $data['tokbox_id'] = $post_data['tokboxId'];
        $data['user_type'] = ($usrdata['typename'] == 'Guest') ? 'GUEST' : 'USER';
        $data['message'] = $post_data['msg'];
        $data['from_role'] = isset($post_data['from_role']) ? $post_data['from_role'] : NULL;
        $data['chat_id'] = isset($post_data['chat_id']) ? $post_data['chat_id'] : NULL;


        $this->tokbox_model->storeChat($data);
    }

    function delchat()
    {
        $this->load->model('tokbox_model');
        $post_data = json_decode(file_get_contents("php://input"), true);
        $chatId = isset($post_data['chat_id']) && $post_data['chat_id'] ? $post_data['chat_id'] : '';
        $this->tokbox_model->deleteChat($chatId);
    }

    function connection()
    {
        $usrdata = $this->session->userdata('usrdata') ? $this->session->userdata('usrdata') : $this->session->userdata('user');
        $post_data = json_decode(file_get_contents("php://input"), true);

        $this->load->model('tokbox_model');

        $data['connection_id'] = $post_data['connectionId'];
        $data['session_id'] = $post_data['sessionId'];
        $data['tokbox_id'] = $post_data['tokboxId'];
        $data['user_type'] = ($usrdata['typename'] == 'Guest') ? 'GUEST' : 'USER';
        $data['user_id'] = $usrdata['id'];
        $data['username'] = $post_data['username'];
        $data['role'] = isset($post_data['role']) ? $post_data['role'] : '';

        $allConnections = $this->tokbox_model->addConnection($data, $post_data['myConnectionId']);
        $attendees = [];
        $organizers = [];
        $panelists = [];

        if($allConnections)
        {
            foreach($allConnections as $connection)
            {
                switch($connection['role'])
                {
                    case 'Attendee':
                        $attendees[] = $connection;
                        break;
                    case 'Panelist':
                        $panelists[] = $connection;
                        break;
                    case 'Organizer':
                        $organizers[] = $connection;
                        break;
                }
            }
        }

        $finalResult = [
            'attendees' => $attendees,
            'panelists' => $panelists,
            'organizers' => $organizers,
            'attendee_count' => count($attendees),
            'panelist_count' => count($panelists),
            'organizer_count' => count($organizers)
        ];

        echo json_encode($finalResult);
    }

    function delconnection()
    {
        $usrdata = $this->session->userdata('usrdata') ? $this->session->userdata('usrdata') : $this->session->userdata('user');
        $post_data = json_decode(file_get_contents("php://input"), true);

        $this->load->model('tokbox_model');

        $data['connection_id'] = $post_data['connectionId'];
        $data['session_id'] = $post_data['sessionId'];
        $data['connected'] = 0;

        $this->tokbox_model->updateConnection($data, $data['connection_id'], $data['session_id']);
        $allConnections = $this->tokbox_model->getAllConnections($post_data['sessionId'], $post_data['myConnectionId']);
        $attendees = [];
        $organizers = [];
        $panelists = [];

        if($allConnections)
        {
            foreach($allConnections as $connection)
            {
                switch($connection['role'])
                {
                    case 'Attendee':
                        $attendees[] = $connection;
                        break;
                    case 'Panelist':
                        $panelists[] = $connection;
                        break;
                    case 'Organizer':
                        $organizers[] = $connection;
                        break;
                }
            }
        }

        $finalResult = [
            'attendees' => $attendees,
            'panelists' => $panelists,
            'organizers' => $organizers,
            'attendee_count' => count($attendees),
            'panelist_count' => count($panelists),
            'organizer_count' => count($organizers)
        ];

        echo json_encode($finalResult);
    }

    function poll()
    {
        $this->load->model('tokbox_model');
        $post_values = json_decode(file_get_contents("php://input"), true);

        $usrdata = $this->session->userdata('usrdata') ? $this->session->userdata('usrdata') : $this->session->userdata('user');
        $data['user_id'] = $usrdata['id'];
        $data['user_type'] = ($usrdata['typename'] == 'Guest') ? 'GUEST' : 'USER';
        //foreach($post_data as $post_values)
        //{
            $data['question_id'] = $post_values['question_id'];
            $data['answer'] = $post_values['user_ans'];
            $data['session_id'] = $post_values['session_id'];
            $data['other_detail'] = json_encode($post_values);

            $this->tokbox_model->addPollResult($data);
        //}
    }

    function pollResult($sessionId, $questionId = NULL)
    {
        $this->load->model('tokbox_model');
        if($questionId)
        {
            $pollResults = $this->tokbox_model->getPollResultByQuestion($sessionId, $questionId);
        }
        else
        {
            $pollResults = $this->tokbox_model->getPollResult($sessionId);
        }
        $questionsList = [];
        foreach($pollResults as $index => $pollResult)
        {
            if($pollResult['other_detail'])
            {
                $pollResult['other_detail'] = json_decode($pollResult['other_detail'], true);
                if(!isset($questionsList[$pollResult['other_detail']['question_string']]))
                {
                    foreach($pollResult['other_detail']['choices'] as $choice)
                    {
                        $questionsList[$pollResult['other_detail']['question_string']][$choice] = 0;
                    }
                    $questionsList[$pollResult['other_detail']['question_string']]['total_count'] = 0;
                }
                $questionsList[$pollResult['other_detail']['question_string']][$pollResult['answer']] = $pollResult['count'];
                $questionsList[$pollResult['other_detail']['question_string']]['total_count'] += $pollResult['count'];
            }
            $pollResults[$index] = $pollResult;
        }

        $finalString = '<div id="question">';
        if($questionsList)
        {
            foreach($questionsList as $index => $answerList)
            {
                $finalString .= '<h2>' . $index . '</h2>';
                $totalCount = $answerList['total_count'];
                unset($answerList['total_count']);
                foreach($answerList as $answer => $count)
                {
                    if($totalCount)
                    {
                        $answerList[$answer] = round($count * 100 / $totalCount, 2);
                    }
                    $finalString .= '<label>' . $answer . ' ' . $answerList[$answer] . '%</label>';
                }
                $questionsList[$index] = $answerList;
            }
        }
        $finalString .= '</div>';
        echo $finalString;

        //echo json_encode($questionsList);
    }

    function checkSession()
    {
        $usrdata = $this->session->userdata('usrdata');
        echo "Start";
        if(!$usrdata)
        {
            $this->load->model('tokbox_model');
            $checkGuest = $this->tokbox_model->checkGuest($post_data['email'], $post_data['session_id']);
            $checkGuest['typename'] = 'Guest';
            $this->session->set_userdata('usrdata', $addGuest);
            echo "-Created";
        }
    }

    function deleteSession()
    {
        $this->session->unset_userdata('usrdata');
        $this->session->sess_destroy();
        echo "Deleted";
    }

}
?>
