<?php

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

class Croncontroller extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('university/college_model');
        $this->load->model('webinar/common_model');
    }

    function counterHompage(){

      $this->load->model('croncontroller_model');
      $this->load->model('redi_model');

      $homePageCount = $this->croncontroller_model->HomePageCounter();

      $key = "HELLOUNI-HomePageCounter";

      $value = $this->redi_model->setKey($key,json_encode($homePageCount));
      //$userObj = json_decode($value, true);

    }

    function featuredUniversity(){

      $this->load->model('croncontroller_model');
      $this->load->model('redi_model');

      $featuredUniversityObj = $this->croncontroller_model->featuredUniversity();

      $featuredUniversityArray = [];

      foreach ($featuredUniversityObj as $key => $value) {
        // code...
        $featuredUniversityArray[$value['university_id']][] = $featuredUniversityObj[$key];
      }

      $key = "HELLOUNI-FeaturedUniversity";

      $value = $this->redi_model->setKey($key,json_encode($featuredUniversityArray));
      //$userObj = json_decode($value, true);

    }

    function searchKeyCron(){

      $this->load->model('croncontroller_model');
      $this->load->model('redi_model');

      $searchKeyObj = $this->croncontroller_model->searchTextKey();

      $searchKeyArray = [];
      $fsearchKeyArray = [];

      foreach ($searchKeyObj['five'] as $keyf => $valuef) {
        // code...
        $ukeyv = strtolower($valuef['searchkey']);
          $searchKeyArray[$ukeyv][] = $valuef['university_ids'];

      }

      foreach ($searchKeyObj['one'] as $keyo => $valueo) {
        // code...
        foreach ($searchKeyObj['two'] as $keyt => $valuet) {
          // code...
          $searchKeyArray[strtolower($valueo['searchkey']).' '.strtolower($valuet['searchkey'])][] = $valueo['university_ids'].','.$valuet['university_ids'];
        }

      }

      foreach ($searchKeyArray as $keyf => $valuef) {
        // code...
        $fsearchKeyArray[$keyf] = implode(array_unique($valuef),',');
      }

      foreach ($fsearchKeyArray as $keyi => $valuei) {
        // code...
        $insertData = [
          "search_key" => $keyi,
          "ids" => $valuei
        ];

        $insertData = $this->common_model->post($insertData,"search_keys");
      }

      foreach ($searchKeyObj['courses'] as $keyic => $valueic) {
        // code...
        $insertData = [
          "search_key" => $valueic['searchkey'],
          "course_ids" => $valueic['id'],
          "entity_type" => 'COURSES'
        ];

        $insertDataObj = $this->common_model->post($insertData,"search_keys");
      }



      $key = "HELLOUNI-SearchText";
      $value = $this->redi_model->setKey($key,json_encode($fsearchKeyArray));
      //$userObj = json_decode($value, true);

    }

    function eventCron(){

      $this->load->model('croncontroller_model');
      $this->load->model('redi_model');

      $eventObj = $this->croncontroller_model->upcommingEvent();

      $eventArray = [];

      foreach ($eventObj as $key => $value) {
        // code...
        $eventArray[$value['id']][] = $eventObj[$key];
      }

      $key = "HELLOUNI-EventText";
      $value = $this->redi_model->setKey($key,json_encode($eventArray));
      //$userObj = json_decode($value, true);

    }

    function researchCron(){

      $this->load->model('croncontroller_model');
      $this->load->model('redi_model');

      $researchObj = $this->croncontroller_model->researchHome();

      $researchArray = [];

      foreach ($researchObj as $key => $value) {
        // code...
        $researchArray[$value['id']][] = $researchObj[$key];
      }

      $key = "HELLOUNI-researchText";
      $value = $this->redi_model->setKey($key,json_encode($researchArray));
      //$userObj = json_decode($value, true);

    }

    function searchKey (){

      $this->load->model('croncontroller_model');
      $this->load->model('redi_model');

      $key = "HELLOUNI-SearchText";

      $fsearchKeyArray = $this->redi_model->getKey($key);
      $fsearchKeyArray = json_decode($fsearchKeyArray, true);

      //$searchText = "arizona";
      $searchText = $_GET['query'];
      $percent_old = 0;
      $percent_old1 = 0;
      $counta = [];

        foreach ($fsearchKeyArray as  $key => $value ) # loop the arrays
              {
                  //$text = implode(", ", $value); # implode the array to get a string similar to $bio
                  similar_text(strtolower($searchText), $key, $percent); # get a percentage of similar text

                  //$percent = levenshtein(strtolower($searchText), $key);


                  if(count($counta) < 5)
                  {
                    $counta[$key] = intval(round($percent));
                  }

                  if(count($counta) == 5){

                    $counta[$key] = intval(round($percent));

                    arsort($counta);

                    $counta = array_slice($counta,0,5);

                  }

                  /*if ($percent > $percent_old) # check if the current value of $percent is > to the old one
                  {
                      $percent_old = $percent; # assign $percent to $percent_old
                      $final_result = $fsearchKeyArray[$key]; # assign $key to $final_result
                  }*/
              }

              //var_dump($counta);

              $universityIds = "";
              $skeys = [];

              foreach ($counta as $keyv => $valuev) {
                // code...

                $universityIds .= $fsearchKeyArray[$keyv].",";
                $skeys [] = ['id' => $keyv,'name' => $keyv];
                //array_push($skeys,$keyv);

              }
              $uarrayIds = [];
              $uarrayIds = explode(',',$universityIds);
              $uarrayIds = array_unique($uarrayIds);
              $list = implode($uarrayIds,',');
              $list = rtrim($list,",");

          //echo $universityIds."<br>";
          //echo $list."<br>";
          //var_dump($uarrayIds);
          //var_dump($counta);

          //echo "<pre>";
          //print_r($fsearchKeyArray);
          $response['options'] = $skeys;
          header('Content-Type: application/json');
          echo (json_encode($response));

    }

    function searchKeyNew_sudhir (){

      $this->load->model('croncontroller_model');

      $searchText = $_GET['query'];

      $searchTextArray = explode(' ',$searchText);
      $fsearchText = "";

      foreach ($searchTextArray as $keytext) {
        // code...
        $fsearchText .= $keytext."* ";
      }

      $fsearchText = rtrim($fsearchText," ");

        $searchKeysResult = $this->croncontroller_model->searchkey($fsearchText, 'UNIVERSITIES');

        usort($searchKeysResult, function ($a, $b) {
              return $a["id"] - $b["id"];
            });

        $skeys = [];

              foreach ($searchKeysResult as $keyv => $valuev) {

                $skeys [] = ['id' => $valuev['search_key'],'name' => $valuev['search_key']];

              }

          //echo "<pre>";
          //print_r($fsearchKeyArray);



          $response['options'] = $skeys;
          header('Content-Type: application/json');
          echo (json_encode( $response));

    }

    function freeTextSearchNew_sudhir(){
      $this->load->model('croncontroller_model');
      $this->load->model('user/user_model');
      $this->load->model('redi_model');
      $this->load->model('location/country_model');
      $this->load->model('course/degree_model');
      $this->load->model('course/course_model');

      $usrdata=$this->session->userdata('user');

      $userId = $usrdata['id'] ? $usrdata['id'] : 0;

      $sortBy = $this->input->get('sort') ? $this->input->get('sort') : 'ranking_world';
      $this->outputData['sort_by'] = $sortBy;

      $searchText = $_GET['searchtextvalue'] ? $_GET['searchtextvalue'] : NULL;

      $searchTextArray = explode(' ',$searchText);
      $fsearchText = "";

      foreach ($searchTextArray as $keytext) {
        // code...
        $fsearchText .= $keytext."* ";
      }

      $fsearchText = rtrim($fsearchText," ");

        $searchKeys = $this->croncontroller_model->searchkey($fsearchText, 'UNIVERSITIES');

        $universityIds = "";

        foreach ($searchKeys as $keyv => $valuev) {

          $universityIds .= $valuev['ids'].",";

        }

        $uarrayIds = [];
        $uarrayIds = explode(',',$universityIds);
        $uarrayIds = array_unique($uarrayIds);

        $universityListObj = $this->user_model->getUniversityDataByMultipleId($uarrayIds,$sortBy);



        $finalList = array();
        foreach($universityListObj as $index => $university)
            {
              if($usrdata){
              $favCondition = [
                "user_id" => $userId,
                "university_id" => $university['id']
              ];
              $query = $this->user_model->checkFavourites($favCondition);
              if(count($query)<1){
                  $university['favourites_link'] = '';
              }else{
                  $university['fav_id'] = $query->id;
                  $university['favourites_link'] = 1;
              }

                $courseCategoryId = '';
                $conditionArrayc = [
                  'user_id' => $userId,
                  'entity_id' => $university['id'],
                  'entity_type' => 'universities'
                ];
                $likesQuery = $this->user_model->commonCheck($conditionArrayc,'likes_detail');

                if(count($likesQuery)<1){
                  $university['likes_link'] = 0;
                } else {
                  $university['likes_link'] = 1;
                }
              }

                $uni_details = $this->user_model->getUniversityDataByid($university['id'], $courseCategoryId=0);
                if($uni_details)
                {
                    $finalList[] = $university;
                }
            }

          $searchKeys = $this->croncontroller_model->searchkey($fsearchText, 'COURSES');

            $coursesId = "";

            foreach ($searchKeys as $keyv => $valuev) {

              $coursesId .= $valuev['course_ids'].",";

            }

            $carrayIds = [];
            $carrayIds = explode(',',$coursesId);
            $carrayIds = array_unique($carrayIds);

            $carrayIds = empty($carrayIds) ?  $uarrayIds : $carrayIds ;

        $courseListObj = $this->user_model->getCourseDataByMultipleId($carrayIds,$sortBy);

        $this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));

        $this->outputData['mainstreams'] = $this->course_model->getAllCollegeCategories();

        $this->outputData['degrees'] = $this->degree_model->getDegree(array('degree_master.status'=>1));

        $this->outputData['userLoggedIn'] = false;
        $finalListCourse = [];

        foreach ($courseListObj as $clokey => $clovalue) {

          if($usrdata){
            $this->outputData['userLoggedIn'] = true;
          // code...
          $conditionArrayc = [
            'user_id' => $userId,
            'entity_id' => $clovalue['id'],
            'entity_type' => 'courses'
          ];
          $favQueryc = $this->user_model->commonCheck($conditionArrayc,'favourites_detail');
          if(count($favQueryc)<1){
              $clovalue['favourites_link'] = '';
          }else{
              $clovalue['fav_id'] = $favQueryc->id;
              $clovalue['favourites_link'] = 1;
          }

          $likesQueryc = $this->user_model->commonCheck($conditionArrayc,'likes_detail');

          if(count($likesQueryc)<1){
            $clovalue['likes_link'] = 0;
          } else {
            $clovalue['likes_link'] = 1;
          }
        }

        $finalListCourse[] = $clovalue;
      }

        $this->outputData['uni'] = array();
        $this->outputData['uni'] = $finalList;
        $this->outputData['cou'] = $finalListCourse;
        $this->outputData['freetextsearch'] = $searchText;

        /*echo "<pre>";
        print_r($this->outputData);
        die();*/

        $this->render_page('templates/search/universitylist',$this->outputData);

    }


    function searchKeyArray (){

      $this->load->model('croncontroller_model');
      $this->load->model('redi_model');

      $key = "HELLOUNI-SearchText";

      $fsearchKeyArray = $this->redi_model->getKey($key);
      $fsearchKeyArray = json_decode($fsearchKeyArray, true);

      $searchText = "nursing";
      $searchText = $_GET['query'];

      $percent_old = 0;
      $percent_old1 = 0;
      $counta = [];
      $fsearchKeyArrayNew = [];

      foreach ($fsearchKeyArray as $key => $value) {
        // code...
        $fsearchKeyArrayNew[] = ['id' => $key,'name' => $key] ;
        //break;
      }

          //echo "<pre>";
          print_r(json_encode($fsearchKeyArrayNew));
          die();

    }

    function freeTextSearch(){

      $this->load->model('croncontroller_model');
      $this->load->model('user/user_model');
      $this->load->model('redi_model');
      $this->load->model('location/country_model');
      $this->load->model('course/degree_model');
      $this->load->model('course/course_model');

      $usrdata=$this->session->userdata('user');

      $userId = $usrdata['id'] ? $usrdata['id'] : 0;

      $sortBy = $this->input->get('sort') ? $this->input->get('sort') : 'ranking_world';
      $this->outputData['sort_by'] = $sortBy;

      $key = "HELLOUNI-SearchText";

      $fsearchKeyArray = $this->redi_model->getKey($key);
      $fsearchKeyArray = json_decode($fsearchKeyArray, true);

      //$searchText = "arizona";
      $searchText = $_GET['searchtextvalue'] ? $_GET['searchtextvalue'] : NULL;
      $percent_old = 0;
      $percent_old1 = 0;
      $counta = [];

        foreach ($fsearchKeyArray as  $key => $value ) # loop the arrays
              {
                  //$text = implode(", ", $value); # implode the array to get a string similar to $bio
                  similar_text(strtolower($searchText), $key, $percent); # get a percentage of similar text

                  //$percent = levenshtein(strtolower($searchText), $key);


                  if(count($counta) < 5)
                  {
                    $counta[$key] = intval(round($percent));
                  }

                  if(count($counta) == 5){

                    $counta[$key] = intval(round($percent));

                    arsort($counta);

                    $counta = array_slice($counta,0,5);

                  }

              }

              //var_dump($counta);

              $universityIds = "";
              $skeys = [];

              foreach ($counta as $keyv => $valuev) {
                // code...

                $universityIds .= $fsearchKeyArray[$keyv].",";
                $skeys [] = ['id' => $keyv,'name' => $keyv];
                //array_push($skeys,$keyv);

              }
              $uarrayIds = [];
              $uarrayIds = explode(',',$universityIds);
              $uarrayIds = array_unique($uarrayIds);


              $universityListObj = $this->user_model->getUniversityDataByMultipleId($uarrayIds,$sortBy);

              $finalList = array();
              foreach($universityListObj as $index => $university)
                  {
                    $university['favourites_link'] = '';
                    $university['likes_link'] = 0;

                      $uni_details = $this->user_model->getUniversityDataByid($university['id'], $courseCategoryId=0);
                      if($uni_details)
                      {
                          $finalList[] = $university;
                      }
                  }

                  if($usrdata){

                    $allQuery = $this->user_model->commonCheckAll($userId,'universities');

                    foreach ($allQuery as $keyaq => $valueaq) {

                      $keyr = array_search($valueaq['entity_id'], array_column($finalList, 'id'));
                      if(is_numeric($keyr)){

                        if($valueaq['s_type'] == 'likes'){
                          $finalList[$keyr]['likes_link'] = 1;
                          //echo "1<br>";
                        }

                        if($valueaq['s_type'] == 'favourites'){
                          $finalList[$keyr]['fav_id'] = $valueaq['id'];
                          $finalList[$keyr]['favourites_link'] = 1;
                        }

                      }

                    }

                  }

              $courseListObj = $this->user_model->getCourseDataByMultipleId($uarrayIds,$sortBy);

              $courseListData = $courseListObj;

              $this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));

              $this->outputData['mainstreams'] = $this->course_model->getAllCollegeCategories();

              $this->outputData['degrees'] = $this->degree_model->getDegree(array('degree_master.status'=>1));

              $this->outputData['userLoggedIn'] = false;
              /*if($usrdata){
                $this->outputData['userLoggedIn'] = true;
                //$userdata=$this->session->userdata('user');
              foreach ($courseListObj as $clokey => $clovalue) {
                // code...
                $conditionArrayc = [
                  'user_id' => $userId,
                  'entity_id' => $clovalue['id'],
                  'entity_type' => 'courses'
                ];
                $favQueryc = $this->user_model->commonCheck($conditionArrayc,'favourites_detail');
                if(count($favQueryc)<1){
                    $courseListObj[$clokey]['favourites_link'] = '';
                }else{
                    $courseListObj[$clokey]['fav_id'] = $favQueryc->id;
                    $courseListObj[$clokey]['favourites_link'] = 1;
                }

                $likesQueryc = $this->user_model->commonCheck($conditionArrayc,'likes_detail');

                if(count($likesQueryc)<1){
                  $courseListObj[$clokey]['likes_link'] = 0;
                } else {
                  $courseListObj[$clokey]['likes_link'] = 1;
                }
              }


              foreach ($courseListObj as $clokey => $clovalue) {
                // code...
                $conditionArrayc = [
                  'user_id' => $userId,
                  'entity_id' => $clovalue['id'],
                  'entity_type' => 'courses'
                ];
                $favQueryc = $this->user_model->commonCheck($conditionArrayc,'favourites_detail');
                if(count($favQueryc)<1){
                    $courseListObj[$clokey]['favourites_link'] = '';
                }else{
                    $courseListObj[$clokey]['fav_id'] = $favQueryc->id;
                    $courseListObj[$clokey]['favourites_link'] = 1;
                }

                $likesQueryc = $this->user_model->commonCheck($conditionArrayc,'likes_detail');

                if(count($likesQueryc)<1){
                  $courseListObj[$clokey]['likes_link'] = 0;
                } else {
                  $courseListObj[$clokey]['likes_link'] = 1;
                }
              }
            }*/


            //

            if($usrdata){

            $this->outputData['userLoggedIn'] = true;
            foreach ($courseListData as $ldkey => $ldvalue) {

               $courseListData[$ldkey]['favourites_link'] = '';
               $courseListData[$ldkey]['likes_link'] = 0;
            }

            $allQueryc = $this->user_model->commonCheckAll($userId,'courses');

            foreach ($allQueryc as $keyaqc => $valueaqc) {

              $keyrc = array_search($valueaqc['entity_id'], array_column($courseListData, 'id'));
              if(is_numeric($keyrc)){

                if($valueaqc['s_type'] == 'likes'){
                  $courseListData[$keyrc]['likes_link'] = 1;

                }

                if($valueaqc['s_type'] == 'favourites'){
                  $courseListData[$keyrc]['fav_id'] = $valueaqc['id'];
                  $courseListData[$keyrc]['favourites_link'] = 1;
                }

              }

            }
          }

              //$this->outputData['university_list'] = $universityListObj;

              $this->outputData['uni'] = array();
              $this->outputData['uni'] = $finalList;
              $this->outputData['cou'] = $courseListObj;
              $this->outputData['freetextsearch'] = $searchText;

              $this->render_page('templates/search/universitylist',$this->outputData);


    }

    function searchFilters(){

      $this->load->model('croncontroller_model');
      $this->load->model('user/user_model');
      $this->load->model('redi_model');
      $this->load->model('location/country_model');
      $this->load->model('course/degree_model');
      $this->load->model('course/course_model');

      $this->load->helper(array('form', 'url'));
      $usrdata=$this->session->userdata('user');

      if(empty($userdata)){
        //redirect('user/account');
      }

      $userId = $usrdata['id'] ? $usrdata['id'] : 0;

      $countryIds     = $this->input->post('selected_country') ? $this->input->post('selected_country') : NULL;
      $workPermit     = $this->input->post('selected_work_permit') ? $this->input->post('selected_work_permit') : NULL;
      $universityGrade = $this->input->post('selected_grade_type') ? $this->input->post('selected_grade_type') : NULL;
      $courseDuration  = $this->input->post('selected_duration') ? $this->input->post('selected_duration') : NULL;
      $categoryId  = $this->input->post('selected_category_id') ? $this->input->post('selected_category_id') : NULL;
      $subCategoryId  = $this->input->post('selected_sub_category_id') ? $this->input->post('selected_sub_category_id') : NULL;
      $intake   = $this->input->post('selected_intake') ? $this->input->post('selected_intake') : NULL;
      $NumberOfIntake = $this->input->post('selected_no_of_intake') ? $this->input->post('selected_no_of_intake') : NULL;
      $tuitionFees    = $this->input->post('selected_tuition_fee') ? $this->input->post('selected_tuition_fee') : NULL;
      $applicationFees    = $this->input->post('selected_application_fee') ? $this->input->post('selected_application_fee') : NULL;
      $backlogs = $this->input->post('selected_backlogs') ? $this->input->post('selected_backlogs') : NULL;
      $dropYears = $this->input->post('selected_drop_year') ? $this->input->post('selected_drop_year') : NULL;
      $degreeIds    = $this->input->post('selected_degrees') ? $this->input->post('selected_degrees') : NULL;
      $GPA = $this->input->post('selected_gpa') ? $this->input->post('selected_gpa') : NULL;
      $outofGPA = $this->input->post('selected_outof_gpa') ? $this->input->post('selected_outof_gpa') : NULL;

      $yearsEducation     = $this->input->post('selected_years_education') ? $this->input->post('selected_years_education') : NULL;
      $scholarship    = $this->input->post('selected_scholarship_available') ? $this->input->post('selected_scholarship_available') : NULL;
      $wesrequirement   = $this->input->post('selected_wes') ? $this->input->post('selected_wes') : NULL;
      $conditionalAdmit   = $this->input->post('selected_conditional_admit') ? $this->input->post('selected_conditional_admit') : NULL;

      $requirementIELTS = $this->input->post('selected_rquirement_IELTS') ? $this->input->post('selected_rquirement_IELTS') : NULL;
      $requirementGMAT  = $this->input->post('selected_rquirement_GMAT') ? $this->input->post('selected_rquirement_GMAT') : NULL;
      $requirementSAT   = $this->input->post('selected_rquirement_SAT') ? $this->input->post('selected_rquirement_SAT') : NULL;
      $requirementTOEFL = $this->input->post('selected_rquirement_TOEFL') ? $this->input->post('selected_rquirement_TOEFL') : NULL;
      $requirementPTE   = $this->input->post('selected_rquirement_PTE') ? $this->input->post('selected_rquirement_PTE') : NULL;
      $requirementGRE   = $this->input->post('selected_rquirement_GRE') ? $this->input->post('selected_rquirement_GRE') : NULL;
      $requirementDUOLINGO   = $this->input->post('selected_rquirement_duolingo') ? $this->input->post('selected_rquirement_duolingo') : NULL;
      $sortingOrder   = $this->input->post('selected_sorting_order') ? $this->input->post('selected_sorting_order') : NULL;


      $filterCondition = "";

      if(!empty($countryIds)){

        $scountryIds = implode(',', $countryIds);
        $scountryIds = rtrim($scountryIds,",");

          $filterCondition .= "universities.country_id IN ($scountryIds) AND ";

      }

      if(!empty($workPermit)){
        $sworkPermit = implode(',', $workPermit);
        $sworkPermit = rtrim($sworkPermit,",");

        $filterCondition .= "JSON_UNQUOTE( JSON_EXTRACT (courses.work_permit, '$.work_permit_needed')) in ( $sworkPermit ) AND ";

      }

      if( !empty($universityGrade)){
        $suniversityGrade = "";
        foreach ($universityGrade as $key) {
          $suniversityGrade .= "'$key', ";
        }
        $suniversityGrade = rtrim($suniversityGrade,", ");

        $filterCondition .= "universities.type IN ($suniversityGrade) AND ";
      }

      if(!empty($courseDuration)){

        $scourseDuration = implode(',', $courseDuration);
        $scourseDuration = rtrim($scourseDuration,",");

        if($scourseDuration == 60 ){
          $filterCondition .= "";
        } else {
          $filterCondition .= "courses.duration BETWEEN 0 AND $scourseDuration AND ";
        }

      }

      if(!empty($categoryId)){
        $scategoryId = implode(',', $categoryId);
        $scategoryId = rtrim($scategoryId,",");

        $filterCondition .= "colleges.category_id IN ($scategoryId) AND ";
      }

      if(!empty($subCategoryId)){
        $ssubCategoryId = implode(',', $subCategoryId);
        $ssubCategoryId = rtrim($ssubCategoryId,",");

        $filterCondition .= "courses.category_id IN ($ssubCategoryId) AND ";
      }

      if(!empty($NumberOfIntake)){
        $sNumberOfIntake = implode(',', $NumberOfIntake);
        $sNumberOfIntake = rtrim($sNumberOfIntake,",");
        if($sNumberOfIntake != 6){
          $filterCondition .= "courses.no_of_intake IN ($sNumberOfIntake) AND ";
        }

      }

      if(!empty($intake)){
        $sintake = "( ";
        foreach ($intake as $ikey) {
          $sintake .= "JSON_CONTAINS(courses.intake_months, JSON_ARRAY('".$ikey."') OR " ;
        }
        $sintake = rtrim($sintake,"OR ");
        $sintake = $sintake." ) ";

        $filterCondition .= $sintake ." AND ";
      }

      if(!empty($tuitionFees)){

        $stuitionFees = implode(',', $tuitionFees);
        $stuitionFees = rtrim($stuitionFees,",");

        if($stuitionFees == 100000 ){
          $filterCondition .= "";
        } else {
          $filterCondition .= "courses.total_fees BETWEEN 0 AND $stuitionFees AND ";
        }

      }

      if(!empty($applicationFees)){

        $sapplicationFees = "";
        foreach ($applicationFees as $akey) {
          $sapplicationFees .= "'$akey', ";
        }
        $sapplicationFees = rtrim($sapplicationFees,", ");

        $filterCondition .= "courses.application_fee IN ($sapplicationFees) AND ";
      }

      if(!empty($backlogs)){

        $sbacklogs = implode(',', $backlogs);
        $sbacklogs = rtrim($sbacklogs,",");

        $filterCondition .= "JSON_UNQUOTE( JSON_EXTRACT (courses.backlogs, '$.backlog_accepted')) in ( $sbacklogs ) AND ";

      }

      if(!empty($dropYears)){

        $sdropYears = "";
        foreach ($dropYears as $dkey) {
          $sdropYears .= "'$dkey', ";
        }
        $sdropYears = rtrim($sdropYears,", ");

        $filterCondition .= "courses.drop_years IN ($sdropYears) AND ";
      }

      if(!empty($degreeIds)){

        $sdegreeIds = implode(',', $degreeIds);
        $sdegreeIds = rtrim($sdegreeIds,",");

        $filterCondition .= "courses.degree_id IN ($sdegreeIds) AND ";
      }

      if(!empty($GPA)){

        $sGPA = implode(',', $GPA);
        $sGPA = rtrim($sGPA,",");
        if($sGPA != 100){
        $filterCondition .= "courses.eligibility_gpa BETWEEN 0 AND $sGPA AND ";
      }
      }

      if(!empty($yearsEducation)){

        $syearsEducation = "";
        foreach ($yearsEducation as $ykey) {
          $syearsEducation .= "'$ykey', ";
        }
        $syearsEducation = rtrim($syearsEducation,", ");

        $filterCondition .= "courses.with_15_years_education IN ($syearsEducation) AND ";
      }

      if(!empty($scholarship)){

        $sscholarship = "";
        foreach ($scholarship as $ykey) {
          $sscholarship .= "'$ykey', ";
        }
        $sscholarship = rtrim($sscholarship,", ");

        $filterCondition .= "courses.scholarship_available IN ($sscholarship) AND ";
      }

      if(!empty($wesrequirement)){

        $swesrequirement = "";
        foreach ($wesrequirement as $wkey) {
          $swesrequirement .= "'$wkey', ";
        }
        $swesrequirement = rtrim($swesrequirement,", ");

        $filterCondition .= "courses.wes_requirement IN ($swesrequirement) AND ";
      }

      if(!empty($conditionalAdmit)){

        $sconditionalAdmit = "";
        foreach ($conditionalAdmit as $cakey) {
          $sconditionalAdmit .= "'$cakey', ";
        }
        $sconditionalAdmit = rtrim($sconditionalAdmit,", ");

        $filterCondition .= "courses.conditional_admit IN ($sconditionalAdmit) AND ";
      }

      if(!empty($outofGPA)){

        $soutofGPA = implode(',', $outofGPA);
        $soutofGPA = rtrim($soutofGPA,",");
        if($soutofGPA != 1000){
        $filterCondition .= "courses.gpa_scale = $soutofGPA AND ";
      }

      }

      if(!empty($requirementIELTS)){

        $srequirementIELTS = implode(',', $requirementIELTS);
        $srequirementIELTS = rtrim($srequirementIELTS,",");
        if($srequirementIELTS != 9){
        $filterCondition .= "courses.eligibility_ielts BETWEEN 0 AND $srequirementIELTS AND ";
      }
      }

        if(!empty($requirementGMAT)){

          $srequirementGMAT = implode(',', $requirementGMAT);
          $srequirementGMAT = rtrim($srequirementGMAT,",");
          if($srequirementGMAT != 800){
          $filterCondition .= "courses.eligibility_gmat BETWEEN 0 AND $srequirementGMAT AND ";
        }

      }

          if(!empty($requirementSAT)){

            $srequirementSAT = implode(',', $requirementSAT);
            $srequirementSAT = rtrim($srequirementSAT,",");
            if($srequirementSAT != 1600){
            $filterCondition .= "courses.eligibility_sat BETWEEN 0 AND $srequirementSAT AND ";
          }
        }

        if(!empty($requirementTOEFL)){

          $srequirementTOEFL = implode(',', $requirementTOEFL);
          $srequirementTOEFL = rtrim($srequirementTOEFL,",");
          if($srequirementTOEFL != 120){
          $filterCondition .= "courses.eligibility_toefl BETWEEN 0 AND $srequirementTOEFL AND ";
        }
      }

        if(!empty($requirementPTE)){

          $srequirementPTE = implode(',', $requirementPTE);
          $srequirementPTE = rtrim($srequirementPTE,",");
          if($srequirementPTE != 90){
          $filterCondition .= "courses.eligibility_pte BETWEEN 0 AND $srequirementPTE AND ";
        }
      }

      if(!empty($requirementGRE)){

        $srequirementGRE = implode(',', $requirementGRE);
        $srequirementGRE = rtrim($srequirementGRE,",");
        if($srequirementGRE != 340){
        $filterCondition .= "courses.eligibility_gre BETWEEN 0 AND $srequirementGRE AND ";
      }
    }

    if(!empty($requirementDUOLINGO)){

      $srequirementDUOLINGO = implode(',', $requirementDUOLINGO);
      $srequirementDUOLINGO = rtrim($srequirementDUOLINGO,",");

      if($srequirementDUOLINGO != 160){
        $filterCondition .= "courses.eligibility_duolingo BETWEEN 0 AND $srequirementDUOLINGO AND ";
      }

    }

    //echo $filterCondition;
    //die();

    $usortBy = "universities.ranking_world";
    $csortBy = "universities.ranking_world";

    if( !empty($sortingOrder)){

      foreach ($sortingOrder as $sokey) {
        if($sokey == "ranking_world"){
          $usortBy = "universities.ranking_world";
          $csortBy = "universities.ranking_world";
        }

        if($sokey == "application_fee_amount"){
          $usortBy = "courses.application_fee_amount";
          $csortBy = "courses.application_fee_amount";
        }
        if($sokey == "total_fees"){
          $usortBy = "courses.total_fees";
          $csortBy = "courses.total_fees";
        }
        if($sokey == "likes"){
          $usortBy = "universities.university_likes_count";
          $csortBy = "courses.course_likes_count";
        }
        if($sokey == "fav"){
          $usortBy = "universities.university_fav_count";
          $csortBy = "courses.course_fav_count";
        }
        if($sokey == "view"){
          $usortBy = "universities.university_view_count";
          $csortBy = "courses.course_view_count";
        }
      }
    }

    $universityListObj = $this->user_model->getSearchDataByMultipleField($filterCondition,$usortBy);

    $courseListObj = $this->user_model->getSearchDataByMultipleFieldCourse($filterCondition,$csortBy);

    $response['userLoggedIn'] = false;
    //$userdata=$this->session->userdata('user');
    /*if($usrdata){

      $response['userLoggedIn'] = true;

    foreach ($universityListObj as $ulokey => $ulovalue) {
      // code...
      $favCondition = [
        "user_id" => $userId,
        "university_id" => $ulovalue['id']
      ];
      $query = $this->user_model->checkFavourites($favCondition);
      if(count($query)<1){
          $universityListObj[$ulokey]['favourites_link'] = '';
      }else{
          $universityListObj[$ulokey]['fav_id'] = $query->id;
          $universityListObj[$ulokey]['favourites_link'] = 1;
      }

      $conditionArray = [
        'user_id' => $userId,
        'entity_id' => $ulovalue['id'],
        'entity_type' => 'universities'
      ];
      $likesQuery = $this->user_model->commonCheck($conditionArray,'likes_detail');

      if(count($likesQuery)<1){
        $universityListObj[$ulokey]['likes_link'] = 0;
      } else {
        $universityListObj[$ulokey]['likes_link'] = 1;
      }
    }

    foreach ($courseListObj as $clokey => $clovalue) {
      // code...
      $conditionArrayc = [
        'user_id' => $userId,
        'entity_id' => $clovalue['id'],
        'entity_type' => 'courses'
      ];

      $favQueryc = $this->user_model->commonCheck($conditionArrayc,'favourites_detail');
      if(count($favQueryc)<1){
          $courseListObj[$clokey]['favourites_link'] = '';
      }else{
          $courseListObj[$clokey]['fav_id'] = $favQueryc->id;
          $courseListObj[$clokey]['favourites_link'] = 1;
      }

      $likesQueryc = $this->user_model->commonCheck($conditionArrayc,'likes_detail');

      if(count($likesQueryc)<1){
        $courseListObj[$clokey]['likes_link'] = 0;
      } else {
        $courseListObj[$clokey]['likes_link'] = 1;
      }
    }
  }*/



    foreach($universityListObj as $index => $data)
    {
        $data['banner'] = $data['banner'] ? $data['banner'] : '/uploads/banner.png';
        $data['name'] = $data['name'] ? $data['name'] : 'NA';
        $data['slug'] = $data['slug'] ? $data['slug'] : 'NA';
        $data['id'] = $data['id'] ? $data['id'] : 'NA';
        $data['country_name'] = $data['country_name'] ? $data['country_name'] : 'NA';
        $data['total_students'] = $data['total_students'] ? $data['total_students'] : 'NA';
        $data['total_students_international'] = $data['total_students_international'] ? $data['total_students_international'] : 'NA';
        $data['establishment_year'] = $data['establishment_year'] ? $data['establishment_year'] : 'NA';
        $data['ranking_world'] = $data['ranking_world'] ? $data['ranking_world'] : 'NA';
        $data['ranking_usa'] = $data['ranking_usa'] ? $data['ranking_usa'] : 'NA';
        $data['placement_percentage'] = $data['placement_percentage'] ? $data['placement_percentage'] : 'NA';
        $data['admission_success_rate'] = $data['admission_success_rate'] ? $data['admission_success_rate'] : 'NA';
        $data['university_view_count'] = $data['university_view_count'] ? $data['university_view_count'] : 'NA';
        $data['university_likes_count'] = $data['university_likes_count'] ? $data['university_likes_count'] : 'NA';
        $data['university_fav_count'] = $data['university_fav_count'] ? $data['university_fav_count'] : 'NA';
        $data['likes_link'] = $data['likes_link'] ? $data['likes_link'] : 'NA';
        $data['favourites_link'] = $data['favourites_link'] ? $data['favourites_link'] : 'NA';

        $universityListObj[$index] = $data;
    }

    foreach($courseListObj as $index => $data)
    {
        $data['university_name'] = $data['university_name'] ? $data['university_name'] : 'NA';
        $data['university_banner'] = $data['university_banner'] ? $data['university_banner'] : 'NA';
        $data['name'] = $data['name'] ? $data['name'] : 'NA';
        $data['degree_name'] = $data['degree_name'] ? $data['degree_name'] : 'NA';
        $data['department_name'] = $data['department_name'] ? $data['department_name'] : 'NA';
        $data['duration'] = $data['duration'] ? $data['duration'] : 'NA';
        $data['start_months'] = $data['start_months'] ? $data['start_months'] : 'NA';
        $data['application_fee'] = $data['application_fee'] ? $data['application_fee'] : 'NA';
        $data['expected_salary'] = $data['expected_salary'] ? $data['expected_salary'] : 'NA';
        $data['course_likes_count'] = $data['course_likes_count'] ? $data['course_likes_count'] : 'NA';
        $data['course_fav_count'] = $data['course_fav_count'] ? $data['course_fav_count'] : 'NA';
        $data['course_view_count'] = $data['course_view_count'] ? $data['course_view_count'] : 'NA';
        $data['likes_link'] = $data['likes_link'] ? $data['likes_link'] : 'NA';
        $data['favourites_link'] = $data['favourites_link'] ? $data['favourites_link'] : 'NA';

        $courseListObj[$index] = $data;
    }

    if($usrdata){

      $response['userLoggedIn'] = true;

      $allQuery = $this->user_model->commonCheckAll($userId,'universities');

      foreach ($allQuery as $keyaq => $valueaq) {

        $keyr = array_search($valueaq['entity_id'], array_column($universityListObj, 'id'));
        if(is_numeric($keyr)){

          if($valueaq['s_type'] == 'likes'){
            $universityListObj[$keyr]['likes_link'] = 1;
            //echo "1<br>";
          }

          if($valueaq['s_type'] == 'favourites'){
            $universityListObj[$keyr]['fav_id'] = $valueaq['id'];
            $universityListObj[$keyr]['favourites_link'] = 1;
          }

        }

      }

      // courses

      $allQueryc = $this->user_model->commonCheckAll($userId,'courses');

      foreach ($allQueryc as $keyaqc => $valueaqc) {

        $keyrc = array_search($valueaqc['entity_id'], array_column($courseListObj, 'id'));
        if(is_numeric($keyrc)){

          if($valueaqc['s_type'] == 'likes'){
            $courseListObj[$keyrc]['likes_link'] = 1;

          }

          if($valueaqc['s_type'] == 'favourites'){
            $courseListObj[$keyrc]['fav_id'] = $valueaqc['id'];
            $courseListObj[$keyrc]['favourites_link'] = 1;
          }

        }

      }

    }

    $response['universities'] = $universityListObj;
    $response['courses'] = $courseListObj;
    $response['filter_condition'] = $filterCondition;
    header('Content-Type: application/json');
    echo (json_encode($response));

    }


// search by nikhil logic
    function searchKeyNew (){

      $this->load->model('croncontroller_model');

      $searchText = $_GET['query'];

      $searchTextArray = explode(' ',$searchText);

          $universities_score = [];
          $courses_score = [];
          $universities_score_name = [];
          $courses_score_name = [];

          foreach($searchTextArray as $keyword){

              $query_string_select_obj = $this->croncontroller_model->searchKeyNew($keyword);

              foreach($query_string_select_obj as $keya => $valuea){

                  if(!isset($universities_score[$valuea['university_id']]))
                  {
                      $universities_score[$valuea['university_id']] = $valuea['score'];
                  }
                  else
                  {
                      $universities_score[$valuea['university_id']] += $valuea['score'];
                  }

                  if(!isset($courses_score[$valuea['course_id']]))
                  {
                      $courses_score[$valuea['course_id']] = $valuea['score'];
                  }
                  else
                  {
                      $courses_score[$valuea['course_id']] += $valuea['score'];
                  }
              }
          }

          arsort($universities_score);
          arsort($courses_score);

          $top_ten_universities_id_array = [];
          $top_ten_courses_id_array = [];

          $universities = [];
          $courses = [];

          foreach($universities_score as $university_id => $score)
          {
              if(count($top_ten_universities_id_array) > 10)
              {
                  break;
              }

              $top_ten_universities_id_array[] = $university_id;

              $query_string_select_university_obj = $this->common_model->get(["id" => $university_id], "universities");

              foreach($query_string_select_university_obj['data'] as $keyu => $valueu){

                  $universities[] = $valueu['name'];
                  $skeys [] = ['id' => $valueu['name'],'name' => $valueu['name']];

              }
          }

          foreach($courses_score as $course_id => $score)
          {
              if(count($top_ten_courses_id_array) > 10)
              {
                  break;
              }

              $top_ten_courses_id_array[] = $course_id;

              $query_string_select_course_obj = $this->common_model->get(["id" => $course_id], "courses");

              foreach($query_string_select_course_obj['data'] as $keyc => $valuec){

                  $courses[] = $valuec['name'];
                  $skeys [] = ['id' => $valuec['name'],'name' => $valuec['name']];

              }
          }

          $response['options'] = $skeys;
          header('Content-Type: application/json');
          echo (json_encode( $response));
      }

      function freeTextSearchNew(){

        $this->load->model('croncontroller_model');
        $this->load->model('user/user_model');
        $this->load->model('redi_model');
        $this->load->model('location/country_model');
        $this->load->model('course/degree_model');
        $this->load->model('course/course_model');

        $usrdata=$this->session->userdata('user');

        $userId = $usrdata['id'] ? $usrdata['id'] : 0;

        $sortBy = $this->input->get('sort') ? $this->input->get('sort') : 'ranking_world';
        $this->outputData['sort_by'] = $sortBy;

        $searchText = $_GET['searchtextvalue'] ? $_GET['searchtextvalue'] : NULL;
        $afterLogin = isset($_GET['afl']) ? "YES" : "NO";

        $searchTextArray = explode(' ',$searchText);

            $universities_score = [];
            $courses_score = [];

            foreach($searchTextArray as $keyword){

                $query_string_select_obj = $this->croncontroller_model->searchKeyNew($keyword);

                foreach($query_string_select_obj as $keya => $valuea){

                    if(!isset($universities_score[$valuea['university_id']]))
                    {
                        $universities_score[$valuea['university_id']] = $valuea['score'];
                    }
                    else
                    {
                        $universities_score[$valuea['university_id']] += $valuea['score'];
                    }

                    if(!isset($courses_score[$valuea['course_id']]))
                    {
                        $courses_score[$valuea['course_id']] = $valuea['score'];
                    }
                    else
                    {
                        $courses_score[$valuea['course_id']] += $valuea['score'];
                    }
                }
            }

            arsort($universities_score);
            arsort($courses_score);

            $top_ten_universities_id_array = [];
            $top_ten_courses_id_array = [];

            $universities = [];
            $courses = [];

            foreach($universities_score as $university_id => $score)
            {
                if(count($top_ten_universities_id_array) > 15)
                {
                    break;
                }

                $top_ten_universities_id_array[] = $university_id;
            }

            foreach($courses_score as $course_id => $score)
            {
                if(count($top_ten_courses_id_array) > 30)
                {
                    break;
                }

                $top_ten_courses_id_array[] = $course_id;
            }

            $top_ten_universities_id_array = empty($top_ten_universities_id_array) ? [1000] : array_unique($top_ten_universities_id_array) ;
            $top_ten_courses_id_array = empty($top_ten_courses_id_array) ? [1000] : array_unique($top_ten_courses_id_array) ;


          $universityListObj = $this->user_model->getUniversityDataByMultipleId($top_ten_universities_id_array,$sortBy);

          $finalList = array();
          foreach($universityListObj as $index => $university)
              {
                if($usrdata){
                $favCondition = [
                  "user_id" => $userId,
                  "university_id" => $university['id']
                ];
                $query = $this->user_model->checkFavourites($favCondition);
                if(count($query)<1){
                    $university['favourites_link'] = '';
                }else{
                    $university['fav_id'] = $query->id;
                    $university['favourites_link'] = 1;
                }

                  $courseCategoryId = '';
                  $conditionArrayc = [
                    'user_id' => $userId,
                    'entity_id' => $university['id'],
                    'entity_type' => 'universities'
                  ];
                  $likesQuery = $this->user_model->commonCheck($conditionArrayc,'likes_detail');

                  if(count($likesQuery)<1){
                    $university['likes_link'] = 0;
                  } else {
                    $university['likes_link'] = 1;
                  }
                }

                  $finalList[] = $university;
              }

          $courseListObj = $this->user_model->getCourseDataByMultipleId($top_ten_courses_id_array,$sortBy);

          $this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));

          $this->outputData['mainstreams'] = $this->course_model->getAllCollegeCategories();

          $this->outputData['degrees'] = $this->degree_model->getDegree(array('degree_master.status'=>1));

          if($usrdata){
            $this->outputData['userLoggedIn'] = true;
          } else {
            $this->outputData['userLoggedIn'] = false;
          }

          $finalListCourse = [];

          foreach ($courseListObj as $clokey => $clovalue) {

            if($usrdata){
            // code...
            $conditionArrayc = [
              'user_id' => $userId,
              'entity_id' => $clovalue['id'],
              'entity_type' => 'courses'
            ];
            $favQueryc = $this->user_model->commonCheck($conditionArrayc,'favourites_detail');
            if(count($favQueryc)<1){
                $clovalue['favourites_link'] = '';
            }else{
                $clovalue['fav_id'] = $favQueryc->id;
                $clovalue['favourites_link'] = 1;
            }

            $likesQueryc = $this->user_model->commonCheck($conditionArrayc,'likes_detail');

            if(count($likesQueryc)<1){
              $clovalue['likes_link'] = 0;
            } else {
              $clovalue['likes_link'] = 1;
            }
          }

          $finalListCourse[] = $clovalue;
        }

        $this->outputData['freetextsearch'] = $searchText;

        if($afterLogin == "YES"){

        unset($this->outputData['freetextsearch']);

        $mySessions = $this->user_model->getMySessions($userId);
        $myFavourites = $this->user_model->getFavourites(array('favourites.user_id' => $userId));
        $myAppliedUniversities = $this->user_model->getAppliedUniversitiesByUser($userId);
        $appliedUniversities = array();
        if($myAppliedUniversities)
        {
            foreach($myAppliedUniversities as $index => $appliedUniversity)
            {
                if(!in_array($appliedUniversity['university_id'], $appliedUniversities))
                {
                    $appliedUniversities[] = $appliedUniversity['university_id'];
                    switch($appliedUniversity['status'])
                    {
                        case 'In Process':
                            $myAppliedUniversities[$index]['action'] = "Submit Application Form";
                            break;
                        case 'Submitted':
                            $myAppliedUniversities[$index]['action'] = "Awaiting Decision";
                            break;
                        case 'Admit Received':
                            $myAppliedUniversities[$index]['action'] = "Wait For I20";
                            break;
                        case 'I 20 Received':
                            $myAppliedUniversities[$index]['action'] = "Start Filling VISA Form";
                            break;
                        case 'VISA In Process':
                            $myAppliedUniversities[$index]['action'] = "Wait For VISA Date Booking";
                            break;
                        case 'VISA Date Booked':
                            $myAppliedUniversities[$index]['action'] = "Wait For VISA Approval";
                            break;
                        case 'VISA Approved':
                            $myAppliedUniversities[$index]['action'] = "Congratulation!! Pack Your Bags Now!!";
                            break;
                    }
                    $myAppliedUniversities[$index]['check_application'] = '<form method="get" action="/user/sessions/applications">
                                                                     <div>
                                                                          <input type="submit" class="btn btn-default" name="submit" value="Check All Colleges Application">
                                                                     </div>
                                                                     <input type="hidden" name="uid" value="' . $appliedUniversity['university_id'] . '">
                                                                     <input type="hidden" name="tab" value="application_form">
                                                                 </form>';
                }
                else
                {
                    unset($myAppliedUniversities[$index]);
                }
            }
        }
        if($myFavourites)
        {
            foreach($myFavourites as $index => $myFavourite)
            {
                if(!in_array($myFavourite['uni_id'], $appliedUniversities))
                {
                    $myFavourite['university_id'] = $myFavourite['uni_id'];
                    $myFavourite['status'] = 'Shortlisted';
                    //$myFavourite['confirmed_slot'] = '';
                    $myFavourite['action'] = '<form method="post" action="/university/details/connect">
                                                         <div>
                                                              <input type="submit" class="btn btn-default" name="submit" value="Book Session">
                                                              <input type="button" class="btn btn-default" name="apply_button" value="Apply" onclick="applyUniversity(' . $myFavourite['university_id'] . ', \'' . $myFavourite['name'] . '\', \'' . $myFavourite['email'] . '\')">
                                                         </div>
                                                         <input type="hidden" name="university_id" value="' . $myFavourite['university_id'] . '">
                                                         <input type="hidden" name="university_login_id" value="' . $myFavourite['university_login_id'] . '">
                                                         <input type="hidden" name="slug" value="' . $myFavourite['slug'] . '">
                                                     </form>';
                    $myFavourites[$index] = $myFavourite;
                }
                else
                {
                    unset($myFavourites[$index]);
                }
            }
        }
        /*echo '<pre>';
        echo $userId;
        print_r($mySessions);
        print_r($myFavourites);
        exit;*/
        if($mySessions)
        {
            $this->load->model(array('tokbox_model'));
            $traversedUniversities = array();
            foreach($mySessions as $index => $sessionArray)
            {
                if(!in_array($sessionArray['university_id'], $traversedUniversities))
                {
                    $key = array_search($sessionArray['university_id'], array_column($myFavourites, 'university_id'));
                    if($key !== FALSE)
                    {
                        array_splice($myFavourites, $key, 1);
                    }
                    $traversedUniversities[] = $sessionArray['university_id'];
                    $mySessions[$index]['action'] = '';

                    switch($sessionArray['status'])
                    {
                        case 'Attained':
                            if(!in_array($sessionArray['university_id'], $appliedUniversities))
                            {
                                $mySessions[$index]['action'] = '<form method="get" action="/university/details/application">
                                                                 <div>
                                                                      <input type="submit" class="btn btn-default" name="submit" value="Start Documentation">
                                                                 </div>
                                                                 <input type="hidden" name="uid" value="' . $sessionArray['university_id'] . '">
                                                             </form>
                                                             <form method="post" action="/university/details/connect">
                                                                 <div>
                                                                      <input type="submit" class="btn btn-default" name="submit" value="Connect Again">
                                                                 </div>
                                                                 <input type="hidden" name="university_id" value="' . $sessionArray['university_id'] . '">
                                                                 <input type="hidden" name="university_login_id" value="' . $sessionArray['university_login_id'] . '">
                                                                 <input type="hidden" name="slug" value="' . $sessionArray['slug'] . '">
                                                             </form>';
                            }
                            else
                            {
                                unset($mySessions[$index]);
                            }
                            break;
                        case 'Rejected':
                            $mySessions[$index]['action'] = '<form method="post" action="/university/details/connect">
                                                                 <div>
                                                                      <input type="submit" class="btn btn-default" name="submit" value="Connect Again">
                                                                 </div>
                                                                 <input type="hidden" name="university_id" value="' . $sessionArray['university_id'] . '">
                                                                 <input type="hidden" name="university_login_id" value="' . $sessionArray['university_login_id'] . '">
                                                                 <input type="hidden" name="slug" value="' . $sessionArray['slug'] . '">
                                                             </form>';
                            break;
                        case 'Missed':
                            $mySessions[$index]['action'] = '<form method="post" action="/university/details/connect">
                                                                 <div>
                                                                      <input type="submit" class="btn btn-default" name="submit" value="Connect Again">
                                                                 </div>
                                                                 <input type="hidden" name="university_id" value="' . $sessionArray['university_id'] . '">
                                                                 <input type="hidden" name="university_login_id" value="' . $sessionArray['university_login_id'] . '">
                                                                 <input type="hidden" name="slug" value="' . $sessionArray['slug'] . '">
                                                             </form>';
                            break;
                        case 'Requested':
                            $mySessions[$index]['action'] = "Wait For Slots";
                            $mySessions[$index]['confirmed_slot'] = $mySessions[$index]['date_created'];
                            break;
                        case 'Scheduled':
                            if($sessionArray['confirmed_slot'])
                            {
                                $mySessions[$index]['action'] = "Wait For Session";

                                $participants = array(
                                    'slot_id' => $sessionArray['id'],
                                    'user_id' => $sessionArray['user_id'],
                                    'university_id' => $sessionArray['university_id']
                                );
                                $json_participant = json_encode($participants);

                                $token = $this->tokbox_model->getTokenByParticipant($json_participant);
                                if($token && $token->token)
                                {
                                    $confirmed_slot = $sessionArray['confirmed_slot'];

                                    $meetingStartTime = strtotime($confirmed_slot);
                                    $currentTime = strtotime(date('Y-m-d H:i:s'));

                                    $meetingEndTime = $meetingStartTime + 1200000;
                                    if($meetingEndTime < $currentTime)
                                    {
                                        $expired = true;
                                    }
                                    else
                                    {
                                        $expired = false;
                                        $encodedTokboxId = base64_encode($token->id);
                                        $tokboxUrl = "<a href='/meeting/oneonone?id=" . $encodedTokboxId . "' target='_blank' style='color:#337ab7;'>Join Conference</a>";
                                    }

                                    /*if($token->expiry_time)
                                    {
                                        $expiryTime = $token->expiry_time;
                                        $currentTime = date('Y-m-d H:i:s');

                                        $time1 = strtotime($expiryTime);
                                        $time2 = strtotime($currentTime);

                                        if($expiryTime && $time2 < $time1)
                                        {
                                            $expired = false;
                                        }
                                    }*/
                                    if($expired)
                                    {
                                        $mySessions[$index]['action'] = "Session Expired";
                                    }
                                    else
                                    {
                                        $mySessions[$index]['action'] = $tokboxUrl;
                                    }
                                }
                            }
                            else
                            {
                                $mySessions[$index]['action'] = "<a href='/" . $sessionArray['slug'] . "' target='_blank' style='color:#337ab7;'>Pick A Slot</a>";
                                $mySessions[$index]['confirmed_slot'] = $mySessions[$index]['date_created'];
                            }
                            break;
                    }
                }
                else
                {
                    unset($mySessions[$index]);
                }
            }
        }
        $mySessions = array_merge($myFavourites, $mySessions);
        /*echo '<pre>';
        print_r($mySessions);
        exit;*/
        $this->outputData['my_sessions'] = $mySessions;
        $this->outputData['my_applied_universities'] = $myAppliedUniversities;

      }


          $this->outputData['uni'] = array();
          $this->outputData['uni'] = $finalList;
          $this->outputData['cou'] = $finalListCourse;



          $this->render_page('templates/search/universitylist',$this->outputData);

      }

}

?>
