<?php
class Department extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
        $this->load->model('university/department_model');
        track_user();
    }

    function index()
    {
        $departmentId = $this->uri->segment(3);
        if(!$departmentId)
        {
            redirect('/');
        }

        $departmentDetail = $this->department_model->getDepartmentById($departmentId);
        $finalData = array();
        if($departmentDetail)
        {
            $finalData['Department Name'] = $departmentDetail[0]['name'];
            $finalData['College Name'] = $departmentDetail[0]['college_name'];
            $finalData['University Name'] = $departmentDetail[0]['university_name'];
            $finalData['Campus Name'] = $departmentDetail[0]['campuse_name'];
            $finalData['Mainstream'] = $departmentDetail[0]['mainstream'];
            $finalData['Contact Title'] = $departmentDetail[0]['contact_title'];
            $finalData['Contact Name'] = $departmentDetail[0]['contact_name'];
            $finalData['Contact Email'] = $departmentDetail[0]['contact_email'];
            $finalData['Enrollment'] = $departmentDetail[0]['enrollment'] ? json_decode($departmentDetail[0]['enrollment'], true) : '';
            $finalData['Ranking'] = $departmentDetail[0]['ranking'] ? json_decode($departmentDetail[0]['ranking'], true) : '';
            $finalData['College Research Expenditure'] = $departmentDetail[0]['college_research_expenditure'];
            $finalData['Total Enrollment'] = $departmentDetail[0]['total_enrollment'];
            $finalData['International Enrollment'] = $departmentDetail[0]['international_enrollment'];
            $finalData['Application Deadline (USA)'] = $departmentDetail[0]['application_deadline_usa'] ? json_decode($departmentDetail[0]['application_deadline_usa'], true) : '';
            $finalData['Application Deadline (International)'] = $departmentDetail[0]['application_deadline_international'] ? json_decode($departmentDetail[0]['application_deadline_international'], true) : '';
            $finalData['Application Fees (USA)'] = $departmentDetail[0]['application_fees_usa'] ? json_decode($departmentDetail[0]['application_fees_usa'], true) : '';
            $finalData['Application Fees (International)'] = $departmentDetail[0]['application_fees_international'] ? json_decode($departmentDetail[0]['application_fees_international'], true) : '';
            $finalData['Admission Department Name'] = $departmentDetail[0]['admissions_department_name'];
            $finalData['Admission Department URL'] = $departmentDetail[0]['admissions_department_url'];
            $finalData['Department Phone'] = $departmentDetail[0]['department_phone'];
            $finalData['Department Email'] = $departmentDetail[0]['department_email'];
            $finalData['Number Of Faculty'] = $departmentDetail[0]['noof_faculty'];
            $finalData['Average GRE'] = $departmentDetail[0]['average_gre'] ? json_decode($departmentDetail[0]['average_gre'], true) : '';
            $finalData['Research Expenditure Per Faculty'] = $departmentDetail[0]['research_expenditure_per_faculty'];
        }

        $this->outputData['departmentDetail'] = $finalData;
        $this->render_page('templates/department/index', $this->outputData);
    }
}
?>
