<?php
require APPPATH . "vendor/autoload.php";
include_once APPPATH . 'mongo/autoload.php';

require_once APPPATH . 'libraries/Mail/sMail.php';

use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;

date_default_timezone_set("Asia/Calcutta");

class Event extends MY_Controller
{
    private $_apiKey = '46220952';
    private $_apiSecret = '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2';
    //private $_currentRoomId = 211;

    private $_eventName = 'VIRTUAL-FAIR-30-SEP-2024';
    private $_master_otp = '001122';
    private $_room_limit = 10;
    private $_counseling_room_limit = 10;
    private $_queue_alert_count = 100;
    private $_webinar_tokbox_id = 519;

    /*private $_pre_counseling_rooms = [656];
    private $_room_tokbox_ids = [536, 537, 538, 539, 540, 541, 542, 543, 544, 545];
    private $_eduloans_rooms = [660, 661, 662, 663, 664, 665, 666, 667, 668, 669];
    private $_post_counseling_rooms = [670, 671, 672, 673, 674, 675, 676, 677, 678, 679];*/

    private $_room_tokbox_ids = [];
    private $_pre_counseling_rooms = [];
    private $_eduloans_rooms = [];
    private $_post_counseling_rooms = [];
    private $_computer_rooms = [493, 494];

    private $_user_collection;
    private $_connection_collection;
    private $_webinar_connection_collection;
    private $_seminar_slot_collection;
    private $_chat_collection;
    private $_attender_collection;
    private $_queue_collection;
    //private $_post_counselling_collection;
    private $_webinar_status_collection;
    private $_seminar_students_collection;
    private $_fair_universities_collection;
    private $_fair_delegates_collection;
    private $_room_student_count_collection;
    private $_online_user_collection;
    private $_fair_events_collection;
    private $_free_consultation_collection;
    private $_university_tracker_collection;
    private $_tokbox_token_collection;
    private $_help_collection;
    private $_college_category_collection;
    private $_demo_collection;
    private $_ip_collection;
    private $_event_time_collection;
    private $_tokbox_error_collection;

    function __construct()
    {
        parent::__construct();
        $this->load->model(['seminar_model']);

        $mongo_server = '13.233.46.52';
        //$mongo_server = 'localhost';
        $mongo_port = '27017';
        $mongo_client = 'mongodb://' . $mongo_server . ':' . $mongo_port;
        $client = new MongoDB\Client($mongo_client);
        $mongo_db = $client->HelloUni;

        $this->_user_collection = $mongo_db->selectCollection('users');
        $this->_connection_collection = $mongo_db->selectCollection('connections');
        $this->_webinar_connection_collection = $mongo_db->selectCollection('webinar_connections');
        //$this->_seminar_slot_collection = $mongo_db->selectCollection('seminar_slots');
        $this->_chat_collection = $mongo_db->selectCollection('chats');
        $this->_attender_collection = $mongo_db->selectCollection('attenders');
        $this->_queue_collection = $mongo_db->selectCollection('queue');
        //$this->_post_counselling_collection = $mongo_db->selectCollection('post_counselling');
        $this->_webinar_status_collection = $mongo_db->selectCollection('webinar_status');
        $this->_seminar_students_collection = $mongo_db->selectCollection('seminar_students');
        $this->_fair_universities_collection = $mongo_db->selectCollection('fair_universities');
        $this->_fair_delegates_collection = $mongo_db->selectCollection('fair_delegates');
        $this->_room_student_count_collection = $mongo_db->selectCollection('room_student_count');
        $this->_online_user_collection = $mongo_db->selectCollection('online_users');
        $this->_fair_events_collection = $mongo_db->selectCollection('fair_events');
        $this->_free_consultation_collection = $mongo_db->selectCollection('free_consultation');
        $this->_university_tracker_collection = $mongo_db->selectCollection('university_tracker');
        $this->_tokbox_token_collection = $mongo_db->selectCollection('tokbox_token');
        $this->_help_collection = $mongo_db->selectCollection('help_tech');
        $this->_college_category_collection = $mongo_db->selectCollection('colleges_categories');
        $this->_demo_collection = $mongo_db->selectCollection('demo');
        $this->_ip_collection = $mongo_db->selectCollection('users_ips');
        $this->_event_time_collection = $mongo_db->selectCollection('event_time');
        $this->_tokbox_error_collection = $mongo_db->selectCollection('tokbox_error');

        $tokbox_token_query['type'] = $this->_eventName;
        $tokbox_token_cursor = $this->_tokbox_token_collection->find($tokbox_token_query);
        foreach($tokbox_token_cursor as $token_data)
        {
            $this->_background_images[$token_data['id']] = $token_data['background_url'];
            $this->_tokboxSessionIds[$token_data['id']] = $token_data['session_id'];

            if($token_data['room_type'] == 'UNIVERSITY')
            {
                $this->_room_tokbox_ids[] = $token_data['id'];
            }
            if($token_data['room_type'] == 'PRE-COUNSELING')
            {
                $this->_pre_counseling_rooms[] = $token_data['id'];
            }
            if($token_data['room_type'] == 'EDULOAN')
            {
                $this->_eduloans_rooms[] = $token_data['id'];
            }
            if($token_data['room_type'] == 'POST-COUNSELING')
            {
                $this->_post_counseling_rooms[] = $token_data['id'];
            }
        }
    }

    function login()
    {
        if($this->input->post())
        {
		//$email = strtolower($this->input->post('email'));
		$email = $this->input->post('email');

            if($email != 'nikhil.gupta@issc.in' && $email != 'hr@issc.in')
            {
                $event_timing = $this->_event_time_collection->findOne();
                if(!$event_timing['start'])
                {
                    echo '<script type="text/javascript">alert("' . $event_timing['message'] . '"); window.location.href="/event/login";</script>';
                    exit;
                }
                /*echo '<script type="text/javascript">alert("Loan Mela Will Start At 2:00PM. Please join at that time."); window.location.href="/event/login";</script>';
                exit;*/
            }

            $email = iconv('UTF-8', 'UTF-8//IGNORE', $email);
            $mongoUserQuery['email'] = ['$eq' => $email];
            $userDetail = $this->_user_collection->findOne($mongoUserQuery);
            if(!$userDetail)
            {
                echo '<script type="text/javascript">alert("Sorry! You are not registered for webinar. Please do register first. !!!"); window.location.href="/event/login";</script>';
                //echo '<script type="text/javascript">alert("The faculty Connect Session will begin at 9:30 PM, Fri 16th Feb. Please wait until the session opens."); window.location.href="/event/login";</script>';
                exit;
            }

            $username = $userDetail['username'];
            $userId = $userDetail['id'];

            $usernameEncode = base64_encode($username);
            $usernameEncode = str_replace("=", "", $usernameEncode);

            $webinar_status = $this->_webinar_status_collection->findOne();
            // Comment it for webinar sessions
            $webinar_status = 1;
            if($webinar_status)
            {
                /*$roomTokboxId = base64_encode($this->_room_tokbox_ids[0]);
                $roomTokboxId = str_replace("=", "", $roomTokboxId);
                redirect("https://hellouni.org/event/student/" . $roomTokboxId . "/" . $usernameEncode);*/

                $userIpAddress = $this->get_client_ip();

                $mongoIpQuery['user_id'] = ['$eq' => (int)$userId];
                $userIpDetail = $this->_ip_collection->findOne($mongoIpQuery);

                $isLoggedIn = false;
                if($userIpDetail)
                {
                    $lastIp = $userIpDetail['ip_address'];
                    $linkAllowed = $userIpDetail['link_allowed'];

                    if($lastIp != $userIpAddress && $linkAllowed == 1)
                    {
                        $isLoggedIn = true;
                    }
                }

                if($isLoggedIn)
                {
                    echo '<script type="text/javascript">var text = "You are logged in to other machine. If you continue here then it will logout you from other machine and you will have to start from beginning again. Press OK to continue or Cancel."; if (confirm(text) == true) {window.location.href="/event/rooms/' . $usernameEncode . '";} </script>';
                }
                else
                {
                    redirect("/event/rooms/" . $usernameEncode);
                }
            }
            else
	        {
		        //echo '<script type="text/javascript">alert("Sorry! Webinar is rescheduled at 10:30 PM. Please come back at 10:30 PM. Sorry for incovenience. !!!"); window.location.href="/event/login";</script>';
                $webinarTokboxId = base64_encode($this->_webinar_tokbox_id);
                $webinarTokboxId = str_replace("=", "", $webinarTokboxId);
                //redirect("https://hellouni.org/event/webinar/" . $webinarTokboxId . "/" . $usernameEncode);
		        redirect("/wpi-intro/" . $usernameEncode);
            }

            //redirect("/event/rooms/" . $usernameEncode);
            //redirect("/event/reception/" . $usernameEncode);
        }
        $this->load->view('templates/meeting/event_login');
    }

    /*
    function register()
    {
        if($this->input->post())
        {
            if(!$this->input->post('user_email') || !$this->input->post('user_mobile') || !$this->input->post('user_name'))
            {
                echo "All fields are mandatory";
                exit;
            }
            $userEmailId = $this->input->post('user_email');
            $userMobileNumber = $this->input->post('user_mobile');

            $userEmailId = iconv('UTF-8', 'UTF-8//IGNORE', $userEmailId);
            $mongoUserQuery['email'] = ['$eq' => $userEmailId];
            $userDetail = $this->_user_collection->findOne($mongoUserQuery);
            if(!$userDetail)
            {
                $userData['name'] = $this->input->post('user_name');
                $userData['email'] = $this->input->post('user_email');
                $userData['phone'] = $this->input->post('user_mobile');

                $userData['type'] = 5;
                $userData['username'] = strtolower(str_replace(" ", "_", $userData['name']));
                $username = $userData['username'];
                $userData['password'] = md5($username . "@123");

                $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
                $mongoUserQuery['username'] = ['$eq' => $username];
                $userDetail = $this->_user_collection->findOne($mongoUserQuery);

                if(!$userDetail)
                {
                    $userId = $this->seminar_model->addUser($userData);
                    $userData['id'] = (int)$userId;
                    $this->_user_collection->insertOne($userData);
                }
            }
            else
            {
                $username = $userDetail['username'];
            }

            $usernameEncode = base64_encode($username);
            $usernameEncode = str_replace("=", "", $usernameEncode);

            $webinar_status = $this->_webinar_status_collection->findOne();
            if($webinar_status)
            {
                $roomTokboxId = base64_encode($this->_room_tokbox_ids[0]);
                $roomTokboxId = str_replace("=", "", $roomTokboxId);

                echo "https://hellouni.org/event/student/" . $roomTokboxId . "/" . $usernameEncode;
            }
            else
            {
                $webinarTokboxId = base64_encode($this->_webinar_tokbox_id);
                $webinarTokboxId = str_replace("=", "", $webinarTokboxId);

                echo "https://hellouni.org/event/webinar/" . $webinarTokboxId . "/" . $usernameEncode;
            }
            exit();
        }

        $this->render_page('templates/meeting/event_register');
    }
    */

    /*
    function check_slot($username, $tokboxId = NULL)
    {
        $final_result = [];
        if(!$username)
        {
            $final_result['error'] = 1;
            $final_result['msg'] = 'Username is missing';
            echo json_encode($final_result);
            exit;
        }

        $blockedStudents = [];

        $decodedUsername = base64_decode($username);

        if(in_array($decodedUsername, $blockedStudents))
        {
            $final_result['error'] = 1;
            $final_result['msg'] = 'Sorry! You are not allowed to join this event.';
            echo json_encode($final_result);
            exit;
        }

        $decodedUsername = iconv('UTF-8', 'UTF-8//IGNORE', $decodedUsername);
        $mongoUserQuery['username'] = ['$eq' => $decodedUsername];
        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        if(!$userDetail)
        {
            $final_result['error'] = 1;
            $final_result['msg'] = 'Invalid username';
            echo json_encode($final_result);
            exit;
        }

        $userId = $userDetail['id'];
        $mongoSlotQuery['user_id'] = (int)$userId;
        $slotDetail = $this->_seminar_slot_collection->findOne($mongoSlotQuery);
        if(!$slotDetail || !$slotDetail['start_time'])
        {
            $final_result['error'] = 1;
            $final_result['msg'] = 'Sorry! No slot has been assigned to you.';
            echo json_encode($final_result);
            exit;
        }
        $currentTime = date('Y-m-d H:i:s');

        $currentTimeString = strtotime($currentTime);
        $slotStartTimeString = strtotime($slotDetail['start_time']);
        $slotExtendedTimeString = strtotime($slotDetail['extended_time']);
        $slotEndTimeString = strtotime($slotDetail['end_time']);

        if($this->input->get('p'))
        {
            $tokboxId = base64_decode($tokboxId);
            if($tokboxId == $this->_currentRoomId)
            {
                if($slotExtendedTimeString < $currentTimeString)
                {
                    echo 0;
                }
                else if($slotEndTimeString < $currentTimeString)
                {
                    echo 1;
                }
                else
                {
                    echo 2;
                }
            }
            else
            {
                echo 2;
            }
            exit;
        }

        if($slotStartTimeString > $currentTimeString)
        {
            $final_result['error'] = 1;
            $final_result['msg'] = 'Your slot has not been started yet. Please remain on this page or come back on given slot time. You will get notification. If you missed the slot, then you can join only at the end of session.';
            echo json_encode($final_result);
            exit;
        }

        if($slotEndTimeString < $currentTimeString)
        {
            $didJoin = $slotDetail['joined'];
            if(!$didJoin)
            {
                $final_result['error'] = 2;
                $final_result['msg'] = 'Sorry! You missed the slot. You can join only at the end of session. If you want to join, please click on join queue and come back on the same page. You will get notification on the same page.';
                echo json_encode($final_result);
                exit;
            }
            else
            {
                $final_result['error'] = 2;
                $final_result['msg'] = 'Sorry! You have already attended the event. If you want to join again then you can join only at the end of session. Please click on join queue and come back on the same page. You will get notification on the same page.';
                echo json_encode($final_result);
                exit;
            }
        }

        $final_result['error'] = 0;
        echo json_encode($final_result);
        exit;
    }
    */

    function rooms($username, $linkCount = 1)
    {
        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/event/login";</script>';
            exit;
        }

        // TODO take it from mongodb
        $blockedStudents = [];

        $decodedUsername = base64_decode($username);

        if(in_array($decodedUsername, $blockedStudents))
        {
            echo '<script type="text/javascript">alert("Sorry! You are not authorized."); window.location.href="/event/login";</script>';
            exit;
        }

        $decodedUsername = iconv('UTF-8', 'UTF-8//IGNORE', $decodedUsername);
        $mongoUserQuery['username'] = ['$eq' => $decodedUsername];
        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        if(!$userDetail)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/event/register";</script>';
            exit;
        }

        $userId = $userDetail['id'];
        $mongoConnectionQuery['user_id'] = (int)$userId;
        $this->_connection_collection->deleteMany($mongoConnectionQuery);

        $userIpAddress = $this->get_client_ip();
        $mongoIpQuery['user_id'] = ['$eq' => (int)$userId];
        $userIpDetail = $this->_ip_collection->findOne($mongoIpQuery);
        $this->_ip_collection->deleteMany($mongoIpQuery);

        $linkAllowed = $this->input->get('p') ? 10 : 1;
        $insertData['user_id'] = (int)$userId;
        $insertData['ip_address'] = $userIpAddress;
        $insertData['link_allowed'] = (int)$linkAllowed;

        $this->_ip_collection->insertOne($insertData);

        $mongoQueueDeleteQuery['username'] = $decodedUsername;
        //$this->_queue_collection->deleteMany($mongoQueueDeleteQuery);
        $this->_room_student_count_collection->deleteMany($mongoQueueDeleteQuery);

        // For UTA to join pre counselor

        /*$attenderDetail = $this->_attender_collection->findOne($mongoQueueDeleteQuery);
        if(!$attenderDetail)
        {
            $preCounselorTokboxId = $this->check_available_room($username, 'PRE', true);
            if($preCounselorTokboxId)
            {
                redirect("/event/student/" . $preCounselorTokboxId . "/" . $username);
            }
        }*/

        $showJoin = 1;
        $errorMessage = '';
        $didJoin = 0;
        $joinQueue = 1;
        $runScript = 0;

        $inQueue = $this->_queue_collection->findOne($mongoQueueDeleteQuery);
        if($inQueue)
        {
            $runScript = 1;

            $queueTokboxId = $inQueue['tokbox_id'];

            $mongoQueueTokboxQuery['tokbox_id'] = (int)$queueTokboxId;
            $queueCursor = $this->_queue_collection->find($mongoQueueTokboxQuery);

            foreach($queueCursor as $index => $queue)
            {
                if($queue['username'] == $decodedUsername)
                {
                    $encodedToxboxId = base64_encode($queueTokboxId);
                    $encodedToxboxId = str_replace("=", "", $encodedToxboxId);

                    if(!$index)
                    {
                        $mongoRoomStudentCountQuery['tokbox_id'] = (int)$queueTokboxId;
                        $roomStudentsCount = $this->_room_student_count_collection->count($mongoRoomStudentCountQuery);
                        if($roomStudentsCount < $this->_room_limit)
                        {
                            $errorMessage = "Your turn is here. You will be redirected in room once its available. Please remain on same page.";
                        }
                        else
                        {
                            $errorMessage = "Your are Next in Queue. The average wait time is about 2 - 5 minutes. You will be redirected in room once its available. Please remain on same page.";
                        }
                    }
                    elseif(isset($queue['priority']) && $queue['priority'])
                    {
                        if($roomStudentsCount < $this->_room_limit)
                        {
                            $errorMessage = "Your turn is here. You will be redirected in room once its available. Please remain on same page.";
                        }
                        else
                        {
                            $errorMessage = "Your are Next in Queue. The average wait time is about 2 - 5 minutes. You will be redirected in room once its available. Please remain on same page.";
                        }
                    }
                    else
                    {
                        $errorMessage = "Your Queue Position is #" . ($index + 1) . ". The average wait time is 5-10 minutes depending on your number.. You will be redirected in room once its available. Please remain on same page.";
                    }
                }
            }
        }

        // TODO the redirection link will be decided through config
        /*if(isset($this->_panelistTokboxIds[$decodedUsername]))
        {
            $tokboxId = $this->_panelistTokboxIds[$decodedUsername];
            $tokboxId = base64_encode($tokboxId);
            $tokboxId = str_replace("=", "", $tokboxId);
            redirect('/event/panelist/' . $tokboxId . '/' . $username);
        }
        if(in_array($decodedUsername, $this->_superAdmins))
        {
            redirect('/admin/event/index/' . $username);
        }

        $mongoSlotQuery['user_id'] = (int)$userId;
        $slotDetail = $this->_seminar_slot_collection->findOne($mongoSlotQuery);
        if(!$slotDetail || !$slotDetail['start_time'])
        {
            echo '<script type="text/javascript">alert("Sorry! No slot has been assigned to you."); window.location.href="/event/login";</script>';
            exit;
        }
        $showJoin = 1;
        $errorMessage = '';
        $didJoin = 0;
        $joinQueue = 1;
        $runScript = 1;

        $currentTime = date('Y-m-d H:i:s');

        $currentTimeString = strtotime($currentTime);
        $slotStartTimeString = strtotime($slotDetail['start_time']);

        if($slotStartTimeString > $currentTimeString)
        {
            $showJoin = 0;
            $joinQueue = 0;

            $mongoQueueQuery['user_id'] = (int)$userId;
            $queueCount = $this->_queue_collection->count($mongoQueueQuery);
            if($queueCount)
            {
                $didJoin = 1;

                $errorMessage = 'Your have booked another slot which has not been started yet. Please remain on this page or come back on given slot time. You will get notification to join room OR you can talk to our counselor on joining post counselor room.';
            }
            else
            {
                $errorMessage = 'Your slot has not been started yet. Please remain on this page or come back on given slot time. You will get notification. If you missed the slot, then you can join only at the end of session.';
            }
        }
        if($showJoin)
        {
            $slotEndTimeString = strtotime($slotDetail['end_time']);
            if($slotEndTimeString < $currentTimeString)
            {
                $showJoin = 0;
                $didJoin = $slotDetail['joined'];
                if(!$didJoin)
                {
                    $runScript = 0;
                    $errorMessage = 'Sorry! You missed the slot. You can join only at the end of session. If you want to join, please click on join queue and come back on the same page. You will get notification on the same page OR you can talk to our counselor on joining post counselor room.';
                }
                else
                {
                    $errorMessage = 'Sorry! You have already attended the event. Now you can join our post counselling room. If you want to join faculty room again then you can join only at the end of session. Please click on join queue and come back on the same page. You will get notification on the same page.';
                }
                $didJoin = 1;
            }
        }
        if($showJoin)
        {
            $tokboxId = $slotDetail['tokbox_id'];
            $tokboxId = base64_encode($tokboxId);
            $tokboxId = str_replace("=", "", $tokboxId);

            redirect('/event/student/' . $tokboxId . '/' . $username);
        }*/

        $roomsTokboxIds = $this->_room_tokbox_ids;
        $allRooms = [];
        $roomStatus = [];

        foreach($roomsTokboxIds as $roomTokboxId)
        {
            $mongoRoomStudentCountQuery['tokbox_id'] = (int)$roomTokboxId;
            $roomStudentsCount = $this->_room_student_count_collection->count($mongoRoomStudentCountQuery);

            $mongoFairDelegatesQuery['tokbox_token_id'] = (int)$roomTokboxId;
            $roomDetail = $this->_fair_delegates_collection->findOne($mongoFairDelegatesQuery);
            $allRooms[] = $roomDetail;

            $roomTokboxId = $roomDetail['mapping_id'];
            $roomTokboxId = base64_encode($roomTokboxId);
            $roomTokboxId = str_replace("=", "", $roomTokboxId);

            // Change Status logic after UTA
            $status = 1;

            if($roomStudentsCount >= $this->_room_limit)
            {
                $status = 1;
            }
            else
            {
                $queueCount = $this->_queue_collection->count($mongoRoomStudentCountQuery);
                $totalCount = $roomStudentsCount + $queueCount;
                if($totalCount >= $this->_room_limit)
                {
                    $status = 1;
                }
            }

            $roomStatus[] = ['tokboxId' => $roomTokboxId, 'status' => $status];
        }

        $this->outputData['username'] = $username;
        $this->outputData['userId'] = $userId;
        $this->outputData['linkCount'] = $linkCount;
        $this->outputData['joined'] = $didJoin;
        $this->outputData['joinQueue'] = $joinQueue;
        $this->outputData['msg'] = $errorMessage;
        $this->outputData['runScript'] = $runScript;
        $this->outputData['roomStatus'] = $roomStatus;
        $this->outputData['allRooms'] = $allRooms;
        //$this->outputData['startTime'] = $slotDetail['start_time'];
        //$this->outputData['endTime'] = $slotDetail['end_time'];
        $this->outputData['startTime'] = '';
        $this->outputData['endTime'] = '';
        $this->render_page('templates/meeting/event_rooms', $this->outputData);
    }


    function join_queue($encodedRoomId, $encodedUsername, $confirm = 0)
    {
        $tokboxId = base64_decode($encodedRoomId);
        $username = base64_decode($encodedUsername);

        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);

        /*
        $mongoFairDelegatesQuery['tokbox_token_id'] = (int)$tokboxId;
        $fairDelegateDetail = $this->_fair_delegates_collection->findOne($mongoFairDelegatesQuery);
        $boothId = $fairDelegateDetail['fair_university_id'];
        */

        $mongoQueueQuery['username'] = $username;
        $queueData = $this->_queue_collection->findOne($mongoQueueQuery);
        if($queueData)
        {
            $queueTokboxId = $queueData['tokbox_id'];
            if($tokboxId != $queueTokboxId && !$confirm)
            {
                $arr = [
                    "success" => false,
                    "msg" => "You are already in queue for a different room. Would you like to remove from other room and join this room queue?"
                ];
                echo json_encode($arr);
                exit;
            }
            if($tokboxId != $queueTokboxId)
            {
                $this->_queue_collection->deleteMany($mongoQueueQuery);

                $newQueueData['tokbox_id'] = (int)$tokboxId;
                //$newQueueData['booth_id'] = (int)$boothId;
                $newQueueData['username'] = $username;
                $newQueueData['priority'] = 0;

                $this->_queue_collection->insertOne($newQueueData);
            }
        }
        else
        {
            $newQueueData['tokbox_id'] = (int)$tokboxId;
            //$newQueueData['booth_id'] = (int)$boothId;
            $newQueueData['username'] = $username;
            $newQueueData['priority'] = 0;

            $this->_queue_collection->insertOne($newQueueData);
        }

        $arr = [
            "success" => true,
            "msg" => "You've been added in the queue. Will be notified when your turn comes. Meanwhile you can explore other rooms."
        ];
        echo json_encode($arr);
        exit;
    }

    // Old Function
    /*
    function join_queue($userId)
    {
        $final_result = [];
        if(!$userId)
        {
            $final_result['error'] = 1;
            echo json_encode($final_result);
            exit;
        }
        //$queueCount = $this->_queue_collection->count();
        //$quotient = (int)($queueCount / 10);
        $quotient = 0;
        $sessionEndTime = '2021-03-10 20:15:00';
        $nextSlotStartTime = date('Y-m-d H:i:s', strtotime($sessionEndTime . ' + ' . ($quotient * 15) . ' minute'));
        $nextSlotEndTime = date('Y-m-d H:i:s', strtotime($nextSlotStartTime . ' + 15 minute'));
        $nextSlotExtendedTime = date('Y-m-d H:i:s', strtotime($nextSlotEndTime . ' + 10 minute'));
        $tokboxId = $this->_currentRoomId;

        $mongoSlotQuery['user_id'] = (int)$userId;
        $slotDetail = $this->_seminar_slot_collection->findOne($mongoSlotQuery);
        if($slotDetail)
        {
            $updateSlotData['start_time'] = $nextSlotStartTime;
            $updateSlotData['end_time'] = $nextSlotEndTime;
            $updateSlotData['extended_time'] = $nextSlotExtendedTime;
            $updateSlotData['joined'] = 0;
            $this->seminar_model->updateSlot($userId, $tokboxId, $updateSlotData);
        }
        else
        {
            $addSlotData['user_id'] = (int)$userId;
            $addSlotData['tokbox_id'] = $tokboxId;
            $addSlotData['start_time'] = $nextSlotStartTime;
            $addSlotData['end_time'] = $nextSlotEndTime;
            $addSlotData['extended_time'] = $nextSlotExtendedTime;
            $addSlotData['joined'] = 0;
            $this->seminar_model->addSlot($addSlotData);
        }

        $mongoSlotData['user_id'] = (int)$userId;
        $mongoSlotData['tokbox_id'] = $tokboxId;
        $mongoSlotData['start_time'] = $nextSlotStartTime;
        $mongoSlotData['end_time'] = $nextSlotEndTime;
        $mongoSlotData['extended_time'] = $nextSlotExtendedTime;
        $mongoSlotData['joined'] = 0;
        $this->_seminar_slot_collection->deleteMany($mongoSlotQuery);
        $this->_seminar_slot_collection->insertOne($mongoSlotData);

        $mongoQueueQuery['user_id'] = $mongoQueueData['user_id'] = (int)$userId;
        $this->_queue_collection->deleteMany($mongoQueueQuery);
        $this->_queue_collection->insertOne($mongoQueueData);

        $final_result['error'] = 0;
        $final_result['msg'] = "Congratulation!! New slot has been booked for you. Slot: " . $nextSlotStartTime . " - " . $nextSlotEndTime;
        $final_result['startTime'] = $nextSlotStartTime;
        $final_result['endTime'] = $nextSlotEndTime;
        echo json_encode($final_result);
    }
    */

    function get_post_cid()
    {
        $mongoPostCounsellingQuery['active'] = 1;
        $postCounsellingDetail = $this->_post_counselling_collection->findOne($mongoPostCounsellingQuery);
        $newTokboxId = 212;
        if($postCounsellingDetail)
        {
            $currentTokboxId = $postCounsellingDetail['tokbox_id'];
            if($currentTokboxId == 212)
            {
                $newTokboxId = 213;
            }
        }
        $this->_post_counselling_collection->deleteMany($mongoPostCounsellingQuery);
        $postCounsellingData['tokbox_id'] = (int)$newTokboxId;
        $postCounsellingData['active'] = 1;
        $this->_post_counselling_collection->insertOne($postCounsellingData);

        $newTokboxId = base64_encode($newTokboxId);
        $newTokboxId = str_replace("=", "", $newTokboxId);
        echo $newTokboxId;
    }

    function get_connections($tokboxId)
    {
        $connections = [];
        $mongoConnectionQuery['tokbox_id'] = (int)$tokboxId;
        $mongoConnectionQuery['type'] = 'Student';
        //$connectionCursor = $this->_connection_collection->find($mongoConnectionQuery);
        $connectionCursor = $this->_webinar_connection_collection->find($mongoConnectionQuery);
        foreach($connectionCursor as $connection)
        {
            $mongoSeminarStudentQuery['user_id'] = $connection['user_id'];
            $mongoSeminarStudentQuery['current_use'] = $this->_eventName;
            $seminarStudentDetail = $this->_seminar_students_collection->findOne($mongoSeminarStudentQuery);
            $connection['name'] = 'NA';
            $connection['desired_course'] = 'NA';
            $connection['intake'] = 'NA';
            $connection['gre_score'] = 'NA';
            $connection['cgpa'] = 'NA';
            $connection['current_city'] = 'NA';
            $connection['agency_name'] = 'NA';
            $connection['phone'] = 'NA';
            $connection['email'] = 'NA';
            $connection['country'] = 'NA';
            $connection['loan_type'] = 'NA';
            if($seminarStudentDetail)
            {
                $connection['name'] = $seminarStudentDetail['name'];
                $connection['current_city'] = $seminarStudentDetail['current_city'];
                //$connection['intake'] = $seminarStudentDetail['intake_month'] ? $seminarStudentDetail['intake_month'] . ' - ' . $seminarStudentDetail['intake_year'] : $seminarStudentDetail['intake_year'];
                $connection['intake'] = $seminarStudentDetail['intake_month'] . ' ' . $seminarStudentDetail['intake_year'];
                $connection['gre_score'] = $seminarStudentDetail['competitive_exam_score'] ? $seminarStudentDetail['competitive_exam_score'] : 'NA';
                $connection['cgpa'] = ($seminarStudentDetail['gpa_exam'] && $seminarStudentDetail['gpa_exam_score']) ? $seminarStudentDetail['gpa_exam_score'] . ' / ' . $seminarStudentDetail['gpa_exam'] : '';
                $connection['country'] = $seminarStudentDetail['country_looking_for'] ? $seminarStudentDetail['country_looking_for'] : 'NA';
                $connection['loan_type'] = $seminarStudentDetail['education_loan_type'] ? $seminarStudentDetail['education_loan_type'] : 'NA';
                $connection['agency_name'] = $seminarStudentDetail['agency_name'] ? $seminarStudentDetail['agency_name'] : 'NA';
                $connection['email'] = $seminarStudentDetail['email'] ? $seminarStudentDetail['email'] : 'NA';
                $connection['phone'] = $seminarStudentDetail['phone'] ? $seminarStudentDetail['phone'] : 'NA';

                if($seminarStudentDetail['desired_mainstream'])
                {
                    /*$mongoCollegeCategoryQuery['id'] = (int)$seminarStudentDetail['desired_mainstream'];
                    $desired_course = $this->_college_category_collection->findOne($mongoCollegeCategoryQuery);
                    $connection['desired_course'] = $desired_course['display_name'];*/

                    $connection['desired_course'] = $seminarStudentDetail['desired_mainstream'];
                }

                $connection['desired_course'] = $seminarStudentDetail['desired_levelofcourse'];
            }
            $connections[] = $connection;
        }

        echo json_encode($connections);
    }

    function chat()
    {
        $post_data = json_decode(file_get_contents("php://input"), true);

        $data['from'] = $post_data['user_id'];
        $data['to'] = -1;
        $data['to_user_type'] = NULL;
        $data['session_id'] = $post_data['sessionId'];
        $data['tokbox_id'] = $post_data['tokboxId'];
        $data['user_type'] = 'USER';
        $data['message'] = $post_data['msg'];
        $data['from_role'] = isset($post_data['from_role']) ? $post_data['from_role'] : NULL;
        $data['chat_id'] = isset($post_data['chat_id']) ? $post_data['chat_id'] : NULL;

        $this->seminar_model->storeChat($data);

        $insertOneResult = $this->_chat_collection->insertOne($data);
    }

    function thankyou($username)
    {
        $decodedUsername = base64_decode($username);
        $decodedUsername = iconv('UTF-8', 'UTF-8//IGNORE', $decodedUsername);
        $mongoUserQuery['username'] = ['$eq' => $decodedUsername];
        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        if($userDetail)
        {
            $userId = $userDetail['id'];
            $mongoConnectionQuery['user_id'] = (int)$userId;
            $this->_connection_collection->deleteMany($mongoConnectionQuery);
        }

        $mongoRoomStudentCountQuery['username'] = $decodedUsername; $this->_room_student_count_collection->deleteMany($mongoRoomStudentCountQuery);

        // UTA Flow To Join Rooms In A Row
        /*$attenderCursor = $this->_attender_collection->find($mongoRoomStudentCountQuery);
        $preAttended = false;
        $roomAttended = false;
        $postAttended = false;
        if($attenderCursor)
        {
            foreach($attenderCursor as $attender)
            {
                if(in_array($attender['tokbox_id'], $this->_pre_counseling_rooms))
                {
                    $preAttended = true;
                }
                if(in_array($attender['tokbox_id'], $this->_room_tokbox_ids))
                {
                    $roomAttended = true;
                }
                if(in_array($attender['tokbox_id'], $this->_post_counseling_rooms))
                {
                    $postAttended = true;
                }
            }
        }

        if(!$preAttended)
        {
            $preCounselorTokboxId = $this->check_available_room($username, 'PRE', true);
            if($preCounselorTokboxId)
            {
                echo '<script type="text/javascript">alert("You have not joined any pre couseling room."); window.location.href="/event/student/' . $preCounselorTokboxId . '/' . $username . '";</script>';
                exit;
            }
        }
        if(!$roomAttended)
        {
            echo '<script type="text/javascript">alert("You have not joined any UTA room."); window.location.href="/event/rooms/' . $username . '";</script>';
            exit;
        }
        if(!$postAttended)
        {
            $postCounselorTokboxId = $this->check_available_room($username, 'POST', true);
            if($postCounselorTokboxId)
            {
                echo '<script type="text/javascript">alert("You have not joined any post couseling room."); window.location.href="/event/student/' . $postCounselorTokboxId . '/' . $username . '";</script>';
                exit;
            }
        }*/
        $this->outputData['username'] = $decodedUsername;

        $this->load->view('templates/meeting/event_thankyou', $this->outputData);
    }

    function logout($username)
    {
        $data['errorMessage'] = 'You are logged in from other system. If you want to continue, go to CRM and click on landing url. It will logout you from other system.';
        $this->load->view('templates/meeting/uta_error', $data);
    }

    function get_redirect_url($username)
    {
        $decodedUsername = base64_decode($username);
        $decodedUsername = iconv('UTF-8', 'UTF-8//IGNORE', $decodedUsername);
        $mongoUserQuery['username'] = ['$eq' => $decodedUsername];
        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        if($userDetail)
        {
            $userId = $userDetail['id'];
            $userType = $userDetail['type'];
            $linkAllowed = $userDetail['link_allowed'];

            //$roomTokboxId = base64_encode($this->_room_tokbox_ids[0]);
            //$roomTokboxId = str_replace("=", "", $roomTokboxId);

            if($userType == 5 && $linkAllowed < 50)
            {
                //echo '/event/student/' . $roomTokboxId . '/' . $username;
                echo '/event/rooms/' . $username;
            }
            /*else if($userType == 1 || $userType == 2)
            {
                echo '/admin/event/index/' . $username;
            }*/
            else
            {
                //echo '/event/student/' . $roomTokboxId . '/' . $username;
                //echo '/event/panelist/' . $roomTokboxId . '/' . $username;

                $delegateDetail = $this->_fair_delegates_collection->findOne($mongoUserQuery);
                echo $delegateDetail['room_url'];
            }
        }
    }

    function end_session()
    {
        $row['status'] = 1;
        $this->_webinar_status_collection->insertOne($row);
    }

    function webinar($tokboxId, $username, $linkCount = 1)
    {
        $blockedStudents = [];

        if(!$tokboxId)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/event/login";</script>';
            exit;
        }

        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/event/login";</script>';
            exit;
        }

        $data['base64_token_id'] = $tokboxId;
        $data['base64_username'] = $username;

        $decodedTokboxId = base64_decode($tokboxId);
        $decodedUsername = base64_decode($username);

        if(!isset($this->_tokboxSessionIds[$decodedTokboxId]))
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/event/login";</script>';
            exit;
        }

        if(in_array($decodedUsername, $blockedStudents))
        {
            echo '<script type="text/javascript">alert("Sorry! You are not authorized."); window.location.href="/";</script>';
            exit;
        }

        $decodedUsername = iconv('UTF-8', 'UTF-8//IGNORE', $decodedUsername);
        $mongoUserQuery['username'] = ['$eq' => $decodedUsername];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/";</script>';
            exit;
        }
        else
        {
            $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        }

        $userId = $userDetail['id'];
        $entityName = $userDetail['name'];
        $userType = $this->input->get('r') ? $this->input->get('r') : $userDetail['type'];
        $linkAllowed = $userDetail['link_allowed'];

        /*$webinar_status = $this->_webinar_status_collection->findOne();
        if($webinar_status)
        {
            $roomTokboxId = base64_encode($this->_room_tokbox_ids[0]);
            $roomTokboxId = str_replace("=", "", $roomTokboxId);

            switch($userType)
            {
                case 1:
                    redirect('/admin/event/index/' . $username);
                    break;
                case 3:
                    redirect("/event/panelist/" . $roomTokboxId . "/" . $username);
                    break;
                default:
                    redirect("/event/rooms/" . $username);
                    break;
            }
        }*/

        $connectionMetaData = $decodedUsername;
        $sessionId = $this->_tokboxSessionIds[$decodedTokboxId];

        $mongoConnectionQuery['user_id'] = (int)$userId;
        $this->_webinar_connection_collection->deleteMany($mongoConnectionQuery);

        $attenderRow['user_id'] = (int)$userId;
        $attenderRow['username'] = $decodedUsername;
        $attenderRow['tokbox_id'] = (int)$decodedTokboxId;
        $attenderRow['session_id'] = $sessionId;
        $attenderRow['sessionType'] = 'WEBINAR';
        $insertOneResult = $this->_attender_collection->insertOne($attenderRow);

        $commonChat = array();

        // New Chat Starts
        $mongoChatQuery['session_id'] = $sessionId;
        $chatCount = $this->_chat_collection->count($mongoChatQuery);
        $chats = [];
        if($chatCount)
        {
            $chatCursor = $this->_chat_collection->find($mongoChatQuery);
            switch($userType)
            {
                case 1:
                    foreach($chatCursor as $chat)
                    {
                        $chat['chat_of'] = 'mine';
                        $chat['name'] = $username;
                        if($chat['from'] != $userId)
                        {
                            $chat['chat_of'] = 'theirs';

                            $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                            $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                            $chat['name'] = $chatUserDetail['username'];
                        }
                        if($chat['name'])
                        {
                            $chats[] = $chat;
                        }
                    }
                    break;
                case 3:
                    foreach($chatCursor as $chat)
                    {
                        if($chat['from'] == $userId)
                        {
                            $chat['chat_of'] = 'mine';
                            $chat['name'] = $username;
                        }
                        else if($chat['from_role'] == 'PUBLISHER')
                        {
                            $chat['chat_of'] = 'theirs';

                            $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                            $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                            $chat['name'] = $chatUserDetail['username'];
                        }
                        if($chat['name'])
                        {
                            $chats[] = $chat;
                        }
                    }
                    break;
                default:
                    foreach($chatCursor as $chat)
                    {
                        if($chat['from'] == $userId)
                        {
                            $chat['chat_of'] = 'mine';
                            $chat['name'] = $username;
                        }
                        else if($chat['from_role'] == 'PUBLISHER')
                        {
                            $chat['chat_of'] = 'theirs';

                            $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                            $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                            $chat['name'] = $chatUserDetail['username'];
                        }
                        if($chat['name'])
                        {
                            $chats[] = $chat;
                        }
                    }
                    break;
            }
        }
        // New Chat Ends

        $connectioRow['user_id'] = (int)$userId;
        $connectioRow['username'] = $decodedUsername;
        $connectioRow['tokbox_id'] = (int)$decodedTokboxId;
        $connectioRow['session_id'] = $sessionId;

        switch($userType)
        {
            case 1:
            case 3:
                $role = 'moderator';
                $viewFile = 'event_organizer_webinar';
                $connectioRow['type'] = 'Organizer';

                $mongoPresenterQuery['link_allowed'] = ['$eq' => 100];
                $presenterCursor = $this->_user_collection->find($mongoPresenterQuery);
                $allPresenters = [];
                foreach($presenterCursor as $presenter)
                {
                    $presenterUsername = base64_encode($presenter['username']);
                    $presenterUsername = str_replace("=", "", $presenterUsername);
                    $allPresenters[$presenter['name']] = $presenterUsername;
                }

                $data['presenters'] = $allPresenters;
                break;
            /*case 3:
                $role = 'moderator';
                //$viewFile = 'event_panelist_webinar';
                $viewFile = 'event_organizer_webinar';
                $connectioRow['type'] = 'Panelist';
                break;*/
            default:
                $userIpAddress = $this->get_client_ip();
                $mongoIpQuery['user_id'] = ['$eq' => (int)$userId];
                $userIpDetail = $this->_ip_collection->findOne($mongoIpQuery);
                $this->_ip_collection->deleteMany($mongoIpQuery);

                $insertData['user_id'] = (int)$userId;
                $insertData['ip_address'] = $userIpAddress;
                $insertData['link_allowed'] = (int)$linkAllowed;

                $this->_ip_collection->insertOne($insertData);
                $role = 'subscriber';
                $viewFile = 'event_student_webinar';
                $connectioRow['type'] = 'Student';
                break;
        }

        $insertOneResult = $this->_webinar_connection_collection->insertOne($connectioRow);

        $mongoUserOnlineQuery['username'] = $decodedUsername;
        $this->_online_user_collection->deleteMany($mongoUserOnlineQuery);

        $mongoUserOnlineQuery['time'] = date('Y-m-d H:i:s');
        $this->_online_user_collection->insertOne($mongoUserOnlineQuery);

        $opentok = new OpenTok($this->_apiKey, $this->_apiSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => $role,
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $background_image['url'] = $this->_background_images[$decodedTokboxId];

        $data['sessionId'] = $sessionId;
        $data['username'] = $decodedUsername;
        $data['tokboxId'] = $decodedTokboxId;
        $data['userId'] = $userId;
        $data['common_chat_history'] = $chats;
        $data['apiKey'] = $this->_apiKey;
        $data['backgroundImage'] = $background_image;
        $data['entityName'] = $entityName;

        $this->load->view('templates/meeting/' . $viewFile, $data);
    }

    function student($tokboxId, $username)
    {
        $blockedStudents = [];

        if(!$tokboxId)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/event/login";</script>';
            exit;
        }

        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/event/login";</script>';
            exit;
        }

        $data['base64_tokbox_id'] = $tokboxId;
        $data['base64_username'] = $username;

        $tokboxId = base64_decode($tokboxId);
        $username = base64_decode($username);

        if(in_array($username, $blockedStudents))
        {
            echo '<script type="text/javascript">alert("Sorry! You are not authorized."); window.location.href="/event/login";</script>';
            exit;
        }

        if(!isset($this->_tokboxSessionIds[$tokboxId]))
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/event/login";</script>';
            exit;
        }

        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/";</script>';
            exit;
        }
        else
        {
            $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        }

        /*$webinar_status = $this->_webinar_status_collection->findOne();
        if(!$webinar_status)
        {
            $webinarTokboxId = base64_encode($this->_webinar_tokbox_id);
            $webinarTokboxId = str_replace("=", "", $webinarTokboxId);
            redirect('/event/webinar/' . $webinarTokboxId . "/" . $data['base64_username']);
        }*/

        $userId = $userDetail['id'];
        $studentName = $userDetail['name'];

        $mongoRoomStudentCountQuery['tokbox_id'] = (int)$tokboxId;
        $roomStudentsCount = $this->_room_student_count_collection->count($mongoRoomStudentCountQuery);
        $roomLimit = $this->_room_limit;
        if(!in_array($tokboxId, $this->_room_tokbox_ids))
        {
            $roomLimit = $this->_counseling_room_limit;
        }

        if($roomStudentsCount >= $roomLimit)
        {
            $mongoQueueQuery['username'] = $username;
            $queueData = $this->_queue_collection->findOne($mongoQueueQuery);
            if(!$queueData)
            {
                $newQueueData['tokbox_id'] = (int)$tokboxId;
                $newQueueData['username'] = $username;
                $newQueueData['priority'] = 0;

                $this->_queue_collection->insertOne($newQueueData);
            }

            //echo '<script type="text/javascript">alert("The room is full. Please join the queue.");window.location.href="/event/rooms/' . $data['base64_username'] . '";</script>';
            echo '<script type="text/javascript">alert("The room is full. We have added you in queue. You will be notified once room is available.");window.location.href="/event/rooms/' . $data['base64_username'] . '";</script>';
            exit;
        }
        else
        {
            $mongoQueueQuery['tokbox_id'] = (int)$tokboxId;
            $queueCursor = $this->_queue_collection->find($mongoQueueQuery);
            $queueCount = 0;
            $priority = 0;
            foreach($queueCursor as $queue)
            {
                if($queue['username'] == $username)
                {
                    if(isset($queue['priority']) && $queue['priority'])
                    {
                        $priority = 1;
                    }
                    break;
                }
                $queueCount++;
            }
            $totalCount = $roomStudentsCount + $queueCount;
            if($totalCount >= $roomLimit && !$priority)
            {
                echo '<script type="text/javascript">alert("The room is full. Please join the queue.");window.location.href="/event/rooms/' . $data['base64_username'] . '";</script>';
                exit;
            }

            $mongoQueueDeleteQuery['username'] = $username;
            $queueCursor = $this->_queue_collection->deleteMany($mongoQueueDeleteQuery);
        }


        // When use slots
        /*
        if($tokboxId == $this->_currentRoomId)
        {
            $mongoSlotQuery['user_id'] = (int)$userId;
            $slotDetail = $this->_seminar_slot_collection->findOne($mongoSlotQuery);
            if(!$slotDetail)
            {
                echo '<script type="text/javascript">alert("Sorry! No slot has been assigned to you."); window.location.href="/event/login";</script>';
                exit;
            }
            $currentTime = date('Y-m-d H:i:s');

            $currentTimeString = strtotime($currentTime);
            $slotStartTimeString = strtotime($slotDetail['start_time']);
            if($slotStartTimeString > $currentTimeString)
            {
                echo '<script type="text/javascript">alert("Your slot has not been started yet. Please come back when your slot starts. To check your slot time, please click OK"); window.location.href="/event/rooms/ ' . $data['base64_username'] . '";</script>';
                exit;
            }

            $slotEndTimeString = strtotime($slotDetail['end_time']);
            if($slotEndTimeString < $currentTimeString)
            {
                $didJoin = $slotDetail['joined'];
                if(!$didJoin)
                {
                    echo '<script type="text/javascript">alert("Sorry! You missed the slot. You can join only at the end of session. If you want to join, please go to event room page and click on Join Queue."); window.location.href="/event/rooms/ ' . $data['base64_username'] . '";</script>';
                    exit;
                }
                else
                {
                    echo '<script type="text/javascript">alert("Sorry! You have already attended the event. If you want to join again then you can join only at the end of session. Please go to event room page and click on Join Queue."); window.location.href="/event/rooms/ ' . $data['base64_username'] . '";"); </script>';
                    exit;
                }
            }
        }*/

        $mongoConnectionQuery['user_id'] = (int)$userId;
        $this->_connection_collection->deleteMany($mongoConnectionQuery);

        $connectionMetaData = $username;
        $sessionId = $this->_tokboxSessionIds[$tokboxId];

        $connectioRow['user_id'] = (int)$userId;
        $connectioRow['username'] = $username;
        $connectioRow['tokbox_id'] = (int)$tokboxId;
        $connectioRow['session_id'] = $sessionId;
        $connectioRow['type'] = 'Student';
        $insertOneResult = $this->_connection_collection->insertOne($connectioRow);

        $attenderRow['user_id'] = (int)$userId;
        $attenderRow['username'] = $username;
        $attenderRow['tokbox_id'] = (int)$tokboxId;
        $attenderRow['session_id'] = $sessionId;
        $attenderRow['sessionType'] = 'ONE_ON_ONE';
        $insertOneResult = $this->_attender_collection->insertOne($attenderRow);

        $mongoChatQuery['session_id'] = $sessionId;
        $chatCount = $this->_chat_collection->count($mongoChatQuery);
        $chats = [];
        if($chatCount)
        {
            $chatCursor = $this->_chat_collection->find($mongoChatQuery);
            foreach($chatCursor as $chat)
            {
                if($chat['from'] == $userId)
                {
                    $chat['chat_of'] = 'mine';
                    $chat['name'] = $username;
                }
                else if($chat['from_role'] != 'STUDENT')
                {
                    $chat['chat_of'] = 'theirs';

                    $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                    $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                    $chat['name'] = $chatUserDetail['username'];
                }
                if($chat['name'])
                {
                    $chats[] = $chat;
                }
            }
        }

        // When use slots
        /*
        $slotUpdatedData['joined'] = 1;
        $this->seminar_model->updateSlot($userId, $tokboxId, $slotUpdatedData);

        $mongoSlotQuery['user_id'] = (int)$userId;
        //$mongoSlotQuery['tokbox_id'] = (int)$tokboxId;
        $slotDetail = $this->_seminar_slot_collection->findOne($mongoSlotQuery);
        $this->_seminar_slot_collection->deleteMany($mongoSlotQuery);

        $mongoSlotData['user_id'] = (int)$userId;
        $mongoSlotData['tokbox_id'] = (int)$tokboxId;
        $mongoSlotData['start_time'] = $slotDetail['start_time'];
        $mongoSlotData['end_time'] = $slotDetail['end_time'];
        $mongoSlotData['extended_time'] = $slotDetail['extended_time'];
        $mongoSlotData['joined'] = 1;
        $this->_seminar_slot_collection->insertOne($mongoSlotData);
        */

        /*
        $allFairDelegatesCursor = $this->_fair_delegates_collection->find();
        foreach($allFairDelegatesCursor as $fairDelegate)
        {
            $fairTokboxId = $fairDelegate['tokbox_token_id'];

            $mongoRoomStudentCountQuery['tokbox_id'] = (int)$fairTokboxId;
            $studentCounts = $this->_room_student_count_collection->findOne($mongoRoomStudentCountQuery);
            $count = 0;
            $presentStudents = [];
            if($studentCounts)
            {
                $count = $studentCounts['count'];
                $students = $studentCounts['students'];
                foreach($students as $student)
                {
                    $presentStudents[] = $student;
                }

                if (($key = array_search($username, $presentStudents)) !== false)
                {
                    unset($presentStudents[$key]);
                    if($count)
                    {
                        $count--;

                        $this->_room_student_count_collection->deleteMany($mongoRoomStudentCountQuery);
                        $roomLimitInsert['tokbox_id'] = (int)$fairTokboxId;
                        $roomLimitInsert['count'] = (int)$count;
                        $roomLimitInsert['students'] = $presentStudents;
                        $this->_room_student_count_collection->insertOne($roomLimitInsert);
                    }
                }
            }
        }
        */

        $data['leave_fair'] = 0;
        if(in_array($tokboxId, $this->_post_counseling_rooms))
        {
            $data['leave_fair'] = 1;
        }

        $roomLimitInsert['tokbox_id'] = (int)$tokboxId;
        $roomLimitInsert['username'] = $username;
        $this->_room_student_count_collection->insertOne($roomLimitInsert);

        $backgroundUrlPath = APPPATH . '..' . $this->_background_images[$tokboxId];

        $background_image['url'] = file_exists($backgroundUrlPath) ? $this->_background_images[$tokboxId] : '';

        $opentok = new OpenTok($this->_apiKey, $this->_apiSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'publisher',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['sessionId'] = $sessionId;
        $data['userId'] = $userId;
        $data['username'] = $username;
        $data['tokboxId'] = $tokboxId;
        $data['apiKey'] = $this->_apiKey;
        $data['linkCount'] = $linkCount;
        $data['common_chat_history'] = $chats;
        $data['backgroundImage'] = $background_image;
        $data['studentName'] = $studentName;

        $this->load->view('templates/meeting/event_student_oneonone', $data);
    }

    function organizer($tokboxId, $username)
    {
        if(!$tokboxId)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $data['base64_token_id'] = $tokboxId;
        $data['base64_username'] = $username;

        $tokboxId = base64_decode($tokboxId);
        $username = base64_decode($username);

        $background_image['url'] = $this->_background_images[$tokboxId];

        if(!isset($this->_tokboxSessionIds[$tokboxId]))
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        //$userDetail = $this->_user_collection->findOne($mongoUserQuery);
        $userDetail = $this->_fair_delegates_collection->findOne($mongoUserQuery);
        if(!$userDetail)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/";</script>';
            exit;
        }

        /*$webinar_status = $this->_webinar_status_collection->findOne();
        if(!$webinar_status)
        {
            $webinarTokboxId = base64_encode($this->_webinar_tokbox_id);
            $webinarTokboxId = str_replace("=", "", $webinarTokboxId);
            redirect('/event/webinar/' . $webinarTokboxId . "/" . $data['base64_username']);
        }*/

        $userId = $userDetail['id'];
        $organizerName = $userDetail['name'];

        $connectionMetaData = $username;
        $sessionId = $this->_tokboxSessionIds[$tokboxId];

        $mongoChatQuery['session_id'] = $sessionId;
        $chatCount = $this->_chat_collection->count($mongoChatQuery);
        $chats = [];
        if($chatCount)
        {
            $chatCursor = $this->_chat_collection->find($mongoChatQuery);
            foreach($chatCursor as $chat)
            {
                $chat['chat_of'] = 'mine';
                $chat['name'] = $username;
                if($chat['from'] != $userId)
                {
                    $chat['chat_of'] = 'theirs';

                    $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                    $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                    $chat['name'] = $chatUserDetail['username'];
                }
                if($chat['name'])
                {
                    $chats[] = $chat;
                }
            }
        }

        $mongoConnectionQuery['user_id'] = (int)$userId;
        $this->_connection_collection->deleteMany($mongoConnectionQuery);

        $row['user_id'] = (int)$userId;
        $row['username'] = $username;
        $row['tokbox_id'] = (int)$tokboxId;
        $row['session_id'] = $sessionId;
        $row['type'] = 'Organizer';
        $insertOneResult = $this->_connection_collection->insertOne($row);

        $mongoHelpQuery['tokbox_id'] = (int)$tokboxId;
        $this->_help_collection->deleteMany($mongoHelpQuery);

        $opentok = new OpenTok($this->_apiKey, $this->_apiSecret);

        $role = $this->input->get('s') ? 'subscriber' : 'moderator';

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => $role,
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['sessionId'] = $sessionId;
        $data['username'] = $username;
        $data['userId'] = $userId;
        $data['tokboxId'] = $tokboxId;
        $data['apiKey'] = $this->_apiKey;
        $data['common_chat_history'] = $chats;
        $data['backgroundImage'] = $background_image;
        $data['organizerName'] = $organizerName;
        $data['role'] = $role;

        $this->load->view('templates/meeting/event_organizer', $data);
    }

    function panelist($tokboxId, $username)
    {
        if(!$tokboxId)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $data['base64_token_id'] = $tokboxId;
        $data['base64_username'] = $username;

        $tokboxId = base64_decode($tokboxId);
        $username = base64_decode($username);

        $background_image['url'] = $this->_background_images[$tokboxId];

        if(!isset($this->_tokboxSessionIds[$tokboxId]))
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        //$userDetail = $this->_user_collection->findOne($mongoUserQuery);
        $userDetail = $this->_fair_delegates_collection->findOne($mongoUserQuery);
        if(!$userDetail)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/";</script>';
            exit;
        }

        /*$webinar_status = $this->_webinar_status_collection->findOne();
        if(!$webinar_status)
        {
            $webinarTokboxId = base64_encode($this->_webinar_tokbox_id);
            $webinarTokboxId = str_replace("=", "", $webinarTokboxId);
            redirect('/event/webinar/' . $webinarTokboxId . "/" . $data['base64_username']);
        }*/

        $userId = $userDetail['id'];
        $panelistName = $userDetail['name'];
        /*$boothId = $userDetail['fair_university_id'];
        $encodedBoothId = base64_encode($boothId);
        $encodedBoothId = str_replace("=", "", $encodedBoothId);*/

        $connectionMetaData = $username;
        $sessionId = $this->_tokboxSessionIds[$tokboxId];

        $mongoChatQuery['session_id'] = $sessionId;
        $chatCount = $this->_chat_collection->count($mongoChatQuery);
        $chats = [];
        if($chatCount)
        {
            $chatCursor = $this->_chat_collection->find($mongoChatQuery);
            foreach($chatCursor as $chat)
            {
                if($chat['from'] == $userId)
                {
                    $chat['chat_of'] = 'mine';
                    $chat['name'] = $username;
                }
                else if($chat['from_role'] == 'STUDENT')
                {
                    $chat['chat_of'] = 'theirs';

                    $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                    $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                    $chat['name'] = $chatUserDetail['username'];
                }
                if($chat['name'])
                {
                    $chats[] = $chat;
                }
            }
        }

        $mongoConnectionQuery['user_id'] = (int)$userId;
        $this->_connection_collection->deleteMany($mongoConnectionQuery);

        $row['user_id'] = (int)$userId;
        $row['username'] = $username;
        $row['tokbox_id'] = (int)$tokboxId;
        $row['session_id'] = $sessionId;
        $row['type'] = 'Panelist';
        $insertOneResult = $this->_connection_collection->insertOne($row);

        $opentok = new OpenTok($this->_apiKey, $this->_apiSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'moderator',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['student_detail'] = true;
        /*if(in_array($tokboxId, $this->_pre_counseling_rooms) || in_array($tokboxId, $this->_post_counseling_rooms))
        {
            $data['student_detail'] = true;
        }*/

        $data['sessionId'] = $sessionId;
        $data['username'] = $username;
        $data['userId'] = $userId;
        $data['tokboxId'] = $tokboxId;
        $data['apiKey'] = $this->_apiKey;
        $data['common_chat_history'] = $chats;
        $data['backgroundImage'] = $background_image;
        $data['panelistName'] = $panelistName;
        $data['boothId'] = $encodedBoothId;

        $this->load->view('templates/meeting/event_panelist', $data);
    }

    function delete_connection($username)
    {
        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        $this->_connection_collection->deleteMany($mongoConnectionQuery);
    }

    function virtualfair()
    {
        $this->load->model(['course/course_model', 'user/user_model', 'location/country_model', 'course/degree_model']);

        $otherCollege[0]['id'] = '';
        $otherCollege[0]['city'] = '';
        $otherCollege[0]['college'] = 'Other';
        $otherCollege[0]['university'] = 'Other';
        $indianColleges = $this->user_model->getIndianColleges();
        $allIndianColleges = array_merge($otherCollege, $indianColleges);
        $this->outputData['colleges'] = $allIndianColleges;

        $this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));

        $this->outputData['mainstreams'] = $this->course_model->getAllCollegeCategories();

        $this->outputData['degrees'] = $this->degree_model->getDegree(array('degree_master.status'=>1));

        //$this->render_page('templates/meeting/virtualfair_registration_form',$this->outputData);
        $this->render_page('templates/meeting/uta_registration_form',$this->outputData);
    }

    function virtualfair_registration()
    {
        $eventName = $this->_eventName;
        //$eventName = 'EDULOANS-MELA-MARCH-2022';

        $firstName          = $this->input->post('first_name');
        $lastName           = $this->input->post('last_name');
        $userEmail          = $this->input->post('user_email');
        $userMobile         = $this->input->post('user_mobile');
        //$userMessage        = $this->input->post('user_message');
        $userPassword       = '';

        // $userPassword       = $this->input->post('user_password');
        // $userSchool         = $this->input->post('cschool');
        // $userOtherSchool    = $this->input->post('other_cschool');
        // $intakeMonth        = $this->input->post('intake_month');
         $intakeYear         = $this->input->post('intake_year');
         //$otp                = $this->input->post('otp');
        // $studyYear          = $this->input->post('study_year');
        // $interested_fi      = $this->input->post('uta_department');
        // $currentStream      = $this->input->post('current_stream');
         //$enrolled           = $this->input->post('user_enrolled');
        // $imperialBranchConvenient  = $this->input->post('user_convenient_branch');
         //$referFriend  = $this->input->post('refer_friend');
         //$oneToOneBooking  = $this->input->post('one_to_one_booking');

         $userCountry        = $this->input->post('user_desired_country');
         //$userCourse     = $this->input->post('user_desired_course');
         //$userUniversity     = $this->input->post('user_desired_university');
         //$counsellingBranch  = $this->input->post('counselling_branch');
         //$userMainstream     = $this->input->post('desired_mainstream');
         $loanType         = $this->input->post('loan_type');



        //$userSubcourse      = $this->input->post('desired_subcourse');
        // $userLevelofcourse  = $this->input->post('desired_levelofcourse');
         $userCity           = $this->input->post('ccity');
        // $competitiveExam    = $this->input->post('comp_exam');
        // $examScore          = $this->input->post('comp_exam_score');
        // $gpaExam            = $this->input->post('gpa_exam');
        // $gpaExamScore       = $this->input->post('gpa_exam_score');
        // $workExperience     = $this->input->post('work_exp');
        // $experienceMonth    = $this->input->post('work_exp_month');


        /*if($userSchool == 'Other' && !$userOtherSchool)
        {
            $arr = [
                "success" => false,
                "message" => 'Current school is missing'
            ];
            echo json_encode($arr);
            exit();
        }*/

        /*
        if(!$userCountry)
        {
            $arr = [
                "success" => false,
                "message" => 'Desired country is missing'
            ];
            echo json_encode($arr);
            exit();
        }*/

        //$otpMasterData = $this->seminar_model->checkMasterOtp($userMobile);
        /*$checkMobileVerified = 0;
        if($otpMasterData)
        {
            $checkMobileVerified = $otpMasterData['status'];
        }
        else if($otp == $this->_master_otp)
        {
            $checkMobileVerified = 1;
        }*/

        $checkMobileVerified = 1;

        if(!$checkMobileVerified)
        {
            $arr = [
                "success" => false,
                "message" => 'Mobile Number Not Verified ..!!!'
            ];
            echo json_encode($arr);
            exit();
        }

        $checkUserExist = $this->seminar_model->checkUser($userMobile);

        if($checkUserExist)
        {
            $user_id = $checkUserExist['id'];
            $username = $checkUserExist['username'];
            $mobileVerified = $checkUserExist['mobile_verified'];
            $data = [
                'registration_type' => $eventName,
                'mobile_verified' => $mobileVerified + 1,
                //'password' => md5($userPassword),
                'modifydate' => date('Y-m-d H:i:s'),
                'registration_id'   => 'WEB'
            ];

            $this->seminar_model->updateUser($user_id, $data);
        }
        else
        {
            $username = substr($userEmail, 0, 3) . '' . substr($userMobile, 0, 3);
            $userPassword = $userPassword ? $userPassword : $username;
            $data = array(
                'name'              => $firstName . ' ' . $lastName,
                'username'          => $username,
                'email'             => $userEmail,
                'password'          => md5($userPassword),
                'image'             => '',
                'type'              => '5',
                'createdate'        => date('Y-m-d H:i:s'),
                'modifydate'        => date('Y-m-d H:i:s'),
                'activation'        => md5(time()),
                'fid'               => '',
                'registration_type' => $eventName,
                'status'            => '1',
                'phone'             => $userMobile,
                'mobile_verified'   => 1,
                'registration_id'   => 'WEB'
            );

            $user_id = $this->seminar_model->insertUserMaster($data);

            /*
            $data = array(
                'name'          => $firstName . ' ' . $lastName,
                'email'         => $userEmail,
                'phone'         => $userMobile,
                'degree'        => $userLevelofcourse,
                'mainstream'    => $userMainstream,
                'courses'       => $userSubcourse,
                'countries'     => $userCountry
            );

            $leadId = $this->seminar_model->insertLead($data);
            */

            $student_data = array(
                'user_id'           => $user_id,
                'phone'             => $userMobile,
                'facilitator_id'    => 1,
                'current_city'      => $userCity,
                //'current_school'    => $userSchool != 'Other' ? $userSchool : $userOtherSchool,
                //'intake_month'      => $intakeMonth,
                'intake_year'       => $intakeYear,
                //'comp_exam'         => $competitiveExam,
                //'comp_exam_score'   => $examScore,
                //'gpa_exam'          => $gpaExam,
                //'gpa_exam_score'    => $gpaExamScore,
                //'work_exp'          => $workExperience,
                //'work_exp_month'    => $experienceMonth,
                'originator_id'     => $user_id
            );

            $this->seminar_model->insertStudentDetails($student_data);
        }

        $seminar_entry = $this->seminar_model->checkSeminarStudent($user_id, $eventName);
        if(!$seminar_entry)
        {
            $seminar_data['name'] = $firstName . ' ' . $lastName;
            $seminar_data['username'] = $username;
            $seminar_data['email'] = $userEmail;
            $seminar_data['phone'] = $userMobile;
            $seminar_data['current_use'] = $eventName;
            $seminar_data['user_id'] = $user_id;
            $seminar_data['education_loan_type'] = $loanType;
            $seminar_data['current_city'] = $userCity;
            $seminar_data['country_looking_for'] = $userCountry;
            //$seminar_data['desired_mainstream'] = $userCourse;
            //$seminar_data['applied_university'] = $userUniversity;
            //$seminar_data['desired_subcourse'] = $userSubcourse;
            //$seminar_data['desired_levelofcourse'] = $userLevelofcourse;
            //$seminar_data['user_school'] = $userSchool != 'Other' ? $userSchool : $userOtherSchool;
            //$seminar_data['intake_month'] = $intakeMonth;
            $seminar_data['intake_year'] = $intakeYear;
            //$seminar_data['competitive_exam'] = $competitiveExam;
            //$seminar_data['competitive_exam_score'] = $examScore;
            //$seminar_data['gpa_exam'] = $gpaExam;
            //$seminar_data['gpa_exam_score'] = $gpaExamScore;
            //$seminar_data['work_experience'] = $workExperience;
            //$seminar_data['work_experience_month'] = $experienceMonth;
            //$seminar_data['study_year'] = $studyYear;
            //$seminar_data['interested_fi'] = $interested_fi;
            //$seminar_data['current_stream_department'] = $currentStream;
            //$seminar_data['enrolled'] = $enrolled;
            //$seminar_data['imperial_branch_convenient'] = $imperialBranchConvenient;
            //$seminar_data['one_to_one_booking'] = $oneToOneBooking;
            //$seminar_data['refer_friend'] = $referFriend;
            //$seminar_data['counselling_branch'] = $counsellingBranch;
            //$seminar_data['remark'] = $userMessage;

            $seminar_id = $this->seminar_model->insertSeminarStudent($seminar_data);
        }
        else
        {
            $seminar_id = $seminar_entry['id'];
        }

        $mongoUserQuery['id'] = ['$eq' => (int)$user_id];
        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        if(!$userDetail)
        {
            $userData = $this->seminar_model->getUserByid($user_id);
            $userData['id'] = (int)$user_id;
            $this->_user_collection->insertOne($userData);
        }

        $mongoSeminarStudentQuery['id'] = ['$eq' => (int)$seminar_id];
        $seminarStudentDetail = $this->_seminar_students_collection->findOne($mongoUserQuery);
        if(!$seminarStudentDetail)
        {
            $seminarStudentData = $this->seminar_model->getSeminarStudentById($seminar_id);
            $seminarStudentData['id'] = (int)$seminar_id;
            $this->_seminar_students_collection->insertOne($seminarStudentData);
        }

        $encodedUsername = base64_encode($username);
        $encodedUsername = str_replace("=", "", $encodedUsername);

        $arr = [
            "success" => true,
            "encodedUsername" => $encodedUsername
        ];
        echo json_encode($arr);
    }

    function reception($encodedUsername)
    {
        $username = base64_decode($encodedUsername);
        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);

        $counselor_tokbox_ids = [332, 334, 336, 338, 340, 342, 360, 362, 364, 366, 368, 370, 346, 348, 350, 352, 354];

        foreach($counselor_tokbox_ids as $tokboxId)
        {
            $mongoRoomStudentCountQuery['tokbox_id'] = (int)$tokboxId;
            $roomStudentsCount = $this->_room_student_count_collection->findOne($mongoRoomStudentCountQuery);
            if($roomStudentsCount)
            {
                $presentStudents = [];
                $count = $roomStudentsCount['count'];
                $students = $roomStudentsCount['students'];
                foreach($students as $student)
                {
                    $presentStudents[] = $student;
                }

                if (($key = array_search($username, $presentStudents)) !== false)
                {
                    unset($presentStudents[$key]);
                    if($count)
                    {
                        $count--;

                        $this->_room_student_count_collection->deleteMany($mongoRoomStudentCountQuery);
                        $roomLimitInsert['tokbox_id'] = (int)$tokboxId;
                        $roomLimitInsert['count'] = (int)$count;
                        $roomLimitInsert['students'] = $presentStudents;
                        $this->_room_student_count_collection->insertOne($roomLimitInsert);
                    }
                }
            }
        }

        $this->outputData['encodedUsername'] = $encodedUsername;
        $this->load->view('templates/meeting/event_reception', $this->outputData);
    }

    function booths($encodedUsername)
    {
        $fairUniversities = [];
        $universitiesList = [];
        $universityNames = [];
        $fairUniversitiesCursor = $this->_fair_universities_collection->find([], ['sort' => ['order' => 1]]);
        foreach($fairUniversitiesCursor as $fairUniversity)
        {
            $encodedBoothId = base64_encode($fairUniversity['id']);
            $encodedBoothId = str_replace("=", "", $encodedBoothId);
            $fairUniversity['encodedId'] = $encodedBoothId;
            $fairUniversities[] = $fairUniversity;

            $universityNames[] = $fairUniversity['name'];
            $universitiesList[$fairUniversity['id']] = $fairUniversity['name'];
        }

        $username = base64_decode($encodedUsername);
        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);

        $counselor_tokbox_ids = [332, 334, 336, 338, 340, 342, 360, 362, 364, 366, 368, 370, 346, 348, 350, 352, 354];

        foreach($counselor_tokbox_ids as $tokboxId)
        {
            $mongoRoomStudentCountQuery['tokbox_id'] = (int)$tokboxId;
            $roomStudentsCount = $this->_room_student_count_collection->findOne($mongoRoomStudentCountQuery);
            if($roomStudentsCount)
            {
                $presentStudents = [];
                $count = $roomStudentsCount['count'];
                $students = $roomStudentsCount['students'];
                foreach($students as $student)
                {
                    $presentStudents[] = $student;
                }

                if (($key = array_search($username, $presentStudents)) !== false)
                {
                    unset($presentStudents[$key]);
                    if($count)
                    {
                        $count--;

                        $this->_room_student_count_collection->deleteMany($mongoRoomStudentCountQuery);
                        $roomLimitInsert['tokbox_id'] = (int)$tokboxId;
                        $roomLimitInsert['count'] = (int)$count;
                        $roomLimitInsert['students'] = $presentStudents;
                        $this->_room_student_count_collection->insertOne($roomLimitInsert);
                    }
                }
            }
        }

        $this->outputData['fairUniversities'] = $fairUniversities;
        $this->outputData['encodedUsername'] = $encodedUsername;
        $this->outputData['universityNames'] = json_encode($universityNames);
        $this->outputData['allUniversities'] = json_encode($universitiesList);
        $this->load->view('templates/meeting/event_booths', $this->outputData);
    }

    function booth($encodedBoothId, $encodedUsername)
    {
        // Get all room ids from booth id
        // Get counts from all room ids
        $boothId = base64_decode($encodedBoothId);
        $username = base64_decode($encodedUsername);

        $universityTracker['username'] = $username;
        $universityTracker['booth_id'] = (int)$boothId;
        $this->_university_tracker_collection->insertOne($universityTracker);

        $mongoFairUniversityQuery['id'] = (int)$boothId;
        $fairUniversity = $this->_fair_universities_collection->findOne($mongoFairUniversityQuery);

        $mongoFairDelegatesQuery['fair_university_id'] = (int)$boothId;
        $fairDelegatesCursor = $this->_fair_delegates_collection->find($mongoFairDelegatesQuery);
        $fairDelegates = [];
        $encodedRoomIds = [];
        $panelits = [];
        $tracked_room_ids = [];
        foreach($fairDelegatesCursor as $delegate)
        {
            $tokboxId = $delegate['tokbox_token_id'];
            $delegate['profile_pic'] = '/application/images/fair2021/delegates/' . $delegate['username'] . '.jpg';
            $fairDelegates[] = $delegate;
            if (in_array($tokboxId, $tracked_room_ids))
            {
                continue;
            }

            $encodedRoomId = base64_encode($delegate['tokbox_token_id']);
            $encodedRoomId = str_replace("=", "", $encodedRoomId);

            $tracked_room_ids[] = $tokboxId;

            $mongoRoomStudentCountQuery['tokbox_id'] = (int)$delegate['tokbox_token_id'];
            $studentCounts = $this->_room_student_count_collection->findOne($mongoRoomStudentCountQuery);
            $count = 0;
            $presentStudents = [];
            if($studentCounts)
            {
                $count = $studentCounts['count'];
                $students = $studentCounts['students'];
                foreach($students as $student)
                {
                    $presentStudents[] = $student;
                }

                if (($key = array_search($username, $presentStudents)) !== false)
                {
                    unset($presentStudents[$key]);
                    if($count)
                    {
                        $count--;

                        $this->_room_student_count_collection->deleteMany($mongoRoomStudentCountQuery);
                        $roomLimitInsert['tokbox_id'] = (int)$tokboxId;
                        $roomLimitInsert['count'] = (int)$count;
                        $roomLimitInsert['students'] = $presentStudents;
                        $this->_room_student_count_collection->insertOne($roomLimitInsert);

                        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
                        $mongoUserQuery['username'] = ['$eq' => $username];
                        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
                        if($userDetail)
                        {
                            $userId = $userDetail['id'];
                            $mongoConnectionQuery['user_id'] = (int)$userId;
                            $this->_connection_collection->deleteMany($mongoConnectionQuery);
                        }
                    }
                }
            }

            $mongoQueueQuery['tokbox_id'] = (int)$tokboxId;
            $queueCursor = $this->_queue_collection->find($mongoQueueQuery);
            $queueCount = 0;
            foreach($queueCursor as $queue)
            {
                $queueCount++;
            }
            $count = $count + $queueCount;
            $count = $count > $this->_room_limit ? $this->_room_limit : $count;
            $count = 0;

            $tempArray['room_id'] = $encodedRoomId;
            $tempArray['student_count'] = $count;

            $encodedRoomIds[] = $tempArray;
            $panelits[$encodedRoomId] = $delegate['name'];
        }
        if(count($encodedRoomIds) < 4)
        {
            $encodedRoomIdsCount = count($encodedRoomIds);
            for($i = $encodedRoomIdsCount; $i < 4; $i++)
            {
                $tempArray['room_id'] = -1;
                $tempArray['student_count'] = 0;

                $encodedRoomIds[] = $tempArray;
            }
        }
        $this->outputData['encodedUsername'] = $encodedUsername;
        $this->outputData['encodedRoomIds'] = $encodedRoomIds;
        $this->outputData['encodedBoothId'] = $encodedBoothId;
        $this->outputData['panelist'] = $panelits;
        $this->outputData['roomLimit'] = $this->_room_limit;
        $this->outputData['fairUniversity'] = $fairUniversity;
        $this->outputData['fairDelegates'] = $fairDelegates;
        $this->load->view('templates/meeting/event_booth', $this->outputData);
    }

    function send_otp()
    {
        $mobileNumber = $this->input->post('mobile_number') ? $this->input->post('mobile_number') : 0;
        $userEmail = $this->input->post('user_email') ? $this->input->post('user_email') : '';

        if(!($mobileNumber) || strlen($mobileNumber) != 10 || !$userEmail)
        {
            $arr = [
                "success" => false,
                "message" => 'Please provide valid mobile number & email id !!!'
            ];
            echo json_encode($arr);
            exit();
        }

        $mobileAlreadyAvailable = $this->seminar_model->checkUserMobile($mobileNumber, $this->_eventName);

        if($mobileAlreadyAvailable)
        {
            $arr = [
                "success" => false,
                "message" => 'Mobile Number Already Registered'
            ];
            echo json_encode($arr);
            exit();
        }

        $mobileAlreadyVerified = $this->seminar_model->checkMasterOtp($mobileNumber);

        if($mobileAlreadyVerified)
        {
            $otp = $mobileAlreadyVerified['otp'];
        }
        else
        {
            $otp = mt_rand(100000, 999999);

            $insertData =["mobile_number" => $mobileNumber, "otp" => $otp];
            $this->seminar_model->insertOtp($insertData);
        }

        if(filter_var($userEmail, FILTER_VALIDATE_EMAIL))
        {
            $mailSubject = "HelloUni - OTP Verification Code !";
            $mailTemplate = "Hi User,

                     Please use this OTP to validate your login on Hellouni - $otp

                     Regards,
                     Team HelloUni
                     Tel: +91 81049 09690
                     Email: info@hellouni.org


                     <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

            $ccMailList = '';
            $mailAttachments = '';

            //$studentEmail = 'nikhil.gupta@issc.in';

            $sendMail = sMail($userEmail, 'User', $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);
        }

        $arr = [
            "success" => true,
            "message" => 'OTP Sent ON Given MObile Number AND Email-ID'
        ];

        echo json_encode($arr);
    }

    function verify_otp()
    {
        $mobileNumber = $this->input->post('mobile_number') ?  $this->input->post('mobile_number') : 0;
        $otp = $this->input->post('otp') ?  $this->input->post('otp') : 0;

        if( !($otp) || strlen($otp) != 6)
        {
            $arr = [
                "success" => true,
                "message" => 'OTP Is Missing OR Invalid OTP'
            ];
            echo json_encode($arr);
            exit();
        }

        if(!($mobileNumber))
        {
            $arr = [
                "success" => true,
                "message" => 'Mobile Number Is Missing'
            ];
            echo json_encode($arr);
            exit();
        }

        $isexists = $this->seminar_model->checkMobileOTP($mobileNumber, $otp);

        if(!$isexists)
        {
            $arr = [
                "success" => true,
                "message" => 'Mobile Number Or OTP is Invalid'
            ];
            echo json_encode($arr);
            exit();
        }

        $otpMasterUpdate['status'] = 1;

        $updateMobileOTP = $this->seminar_model->updateOtp($mobileNumber, $otpMasterUpdate);

        $arr = [
            "success" => true,
            "message" => 'Successfully Verified ...!!!'
        ];
        echo json_encode($arr);
    }

    function virtualfair_login()
    {
        $userMobile         = $this->input->post('user_mobile');
        $userPassword       = $this->input->post('user_password');

        $userMobile = iconv('UTF-8', 'UTF-8//IGNORE', $userMobile);
        $mongoUserQuery['phone'] = $userMobile;

        if($userPassword != $this->_master_otp)
        {
            $mongoUserQuery['password'] = md5($userPassword);
        }

        $userDetail = $this->_user_collection->findOne($mongoUserQuery);

        if($userDetail)
        {
            $username = $userDetail['username'];
        }
        else
        {
            $arr = [
                "success" => false,
                "message" => 'Mobile number is not registered'
            ];
            echo json_encode($arr);
            exit();
        }

        $encodedUsername = base64_encode($username);
        $encodedUsername = str_replace("=", "", $encodedUsername);

        $arr = [
            "success" => true,
            "encodedUsername" => $encodedUsername
        ];
        echo json_encode($arr);
    }

    function leave_meeting($encodedRoomId, $encodedUsername)
    {
        $tokboxId = base64_decode($encodedRoomId);
        $username = base64_decode($encodedUsername);
        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);

        $mongoRoomStudentCountQuery['username'] = $username; $this->_room_student_count_collection->deleteMany($mongoRoomStudentCountQuery);

        $mongoUserQuery['username'] = ['$eq' => $username];
        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        if($userDetail)
        {
            $userId = $userDetail['id'];
            $mongoConnectionQuery['user_id'] = (int)$userId;
            $this->_connection_collection->deleteMany($mongoConnectionQuery);
        }
    }

    function check_queue($encodedUsername)
    {
        $username = base64_decode($encodedUsername);
        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);

        $mongoQueueQuery['username'] = $username;
        $queueData = $this->_queue_collection->findOne($mongoQueueQuery);
        if($queueData)
        {
            $queueTokboxId = $queueData['tokbox_id'];

            $mongoQueueTokboxQuery['tokbox_id'] = (int)$queueTokboxId;
            $queueCursor = $this->_queue_collection->find($mongoQueueTokboxQuery);

            $priority = 0;
            $allQueue = [];
            foreach($queueCursor as $index => $queue)
            {
                $allQueue[] = $queue;
                if(isset($queue['priority']) && $queue['priority'])
                {
                    $priority = 1;
                }
            }

            foreach($allQueue as $index => $queue)
            {
                if($index > $this->_queue_alert_count)
                {
                    break;
                }

                if($queue['username'] == $username)
                {
                    //$encodedBoothId = base64_encode($queue['booth_id']);
                    //$encodedBoothId = str_replace("=", "", $encodedBoothId);

                    $encodedToxboxId = base64_encode($queueTokboxId);
                    $encodedToxboxId = str_replace("=", "", $encodedToxboxId);

                    $mongoRoomStudentCountQuery['tokbox_id'] = (int)$queueTokboxId;
                    $roomStudentsCount = $this->_room_student_count_collection->count($mongoRoomStudentCountQuery);

                    if(!$index)
                    {
                        if($roomStudentsCount < $this->_room_limit)
                        {
                            /*if(!$priority)
                            {
                                $arr = [
                                    "success" => true,
                                    //"booth_id" => $encodedBoothId,
                                    "room_id" => $encodedToxboxId,
                                    "join" => 1,
                                    "msg" => "Your turn is here. Join the room with in 15 sec else you will loose your turn and has to join queue again."
                                ];
                            }
                            else
                            {
                                $arr = [
                                    "success" => true,
                                    //"booth_id" => $encodedBoothId,
                                    "room_id" => $encodedToxboxId,
                                    "join" => 0,
                                    "msg" => "Your turn is here. Join the room with in 15 sec else you will loose your turn and has to join queue again."
                                ];
                            }*/

                            $arr = [
                                "success" => true,
                                "room_id" => $encodedToxboxId,
                                "join" => 1,
                                "msg" => "Your turn is here. Join the room with in 15 sec else you will loose your turn and has to join queue again."
                            ];
                        }
                        else
                        {
                            $arr = [
                                "success" => true,
                                //"booth_id" => $encodedBoothId,
                                "room_id" => $encodedToxboxId,
                                "join" => 0,
                                //"msg" => "Your are Next in Queue. The average wait time is about 2 - 5 minutes. You will receive a message to join the room when a seat becomes free and will have 15 seconds to join."
                                "msg" => "Your are Next in Queue. The average wait time is about 2 - 5 minutes. You will be redirected in room once its available. Please remain on same page."
                            ];
                        }
                    }
                    else if(isset($queue['priority']) && $queue['priority'])
                    {
                        if($roomStudentsCount < $this->_room_limit)
                        {
                            $arr = [
                                "success" => true,
                                //"booth_id" => $encodedBoothId,
                                "room_id" => $encodedToxboxId,
                                "join" => 1,
                                "msg" => "Your turn is here. Join the room with in 15 sec else you will loose your turn and has to join queue again."
                            ];
                        }
                        else
                        {
                            $arr = [
                                "success" => true,
                                //"booth_id" => $encodedBoothId,
                                "room_id" => $encodedToxboxId,
                                "join" => 0,
                                //"msg" => "Your are Next in Queue. The average wait time is about 2 - 5 minutes. You will receive a message to join the room when a seat becomes free and will have 15 seconds to join."
                                "msg" => "Your are Next in Queue. The average wait time is about 2 - 5 minutes. You will be redirected in room once its available. Please remain on same page."
                            ];
                        }
                    }
                    else
                    {
                        $arr = [
                            "success" => true,
                            //"booth_id" => $encodedBoothId,
                            "room_id" => $encodedToxboxId,
                            "join" => 0,
                            //"msg" => "Your Queue Position is #" . ($index + 1) . ". The average wait time is 5-10 minutes depending on your number.</p><p> You can either visit other rooms and return when a seat becomes free or wait in the room until a joining message pops up."
                            "msg" => "Your Queue Position is #" . ($index + 1) . ". The average wait time is 5-10 minutes depending on your number.. You will be redirected in room once its available. Please remain on same page."
                        ];
                    }

                    echo json_encode($arr);
                    exit;
                }
            }
        }
        $arr = [
            "success" => false
        ];
        echo json_encode($arr);
        exit;
    }

    function check_queue_count($tokboxId)
    {
        $mongoQuery['tokbox_id'] = (int)$tokboxId;
        $queueCount = $this->_queue_collection->count($mongoQuery);

        $queueCount = $queueCount ? $queueCount : 0;
        echo $queueCount;
    }

    function delete_queue($encodedUsername)
    {
        $username = base64_decode($encodedUsername);
        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);

        $mongoQueueQuery['username'] = $username;
        $queueData = $this->_queue_collection->deleteMany($mongoQueueQuery);
    }

    function user_online($encodedUsername, $encodedRoomId = NULL)
    {
        $username = base64_decode($encodedUsername);
        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);

        if($encodedRoomId)
        {
            $roomId = base64_decode($encodedRoomId);
            $roomId = iconv('UTF-8', 'UTF-8//IGNORE', $roomId);
            $mongoUserOnlineQuery['room_id'] = (int)$roomId;
        }

        $mongoUserOnlineQuery['username'] = $username;
        $this->_online_user_collection->deleteMany($mongoUserOnlineQuery);

        $mongoUserOnlineQuery['time'] = date('Y-m-d H:i:s');
        $this->_online_user_collection->insertOne($mongoUserOnlineQuery);

        $mongoConnectionQuery['username'] = $username;
        $connection = $this->_webinar_connection_collection->findOne($mongoConnectionQuery);

        $data['base64_tokbox_id'] = $encodedRoomId;
        $data['base64_username'] = $encodedUsername;
        $data['token'] = '';

        if($connection && $connection['allow'])
        {
            if(!$connection['token'])
            {
                $tokboxId = base64_decode($encodedRoomId);
                $username = base64_decode($encodedUsername);

                $connectionMetaData = $username;
                $sessionId = $this->_tokboxSessionIds[$tokboxId];

                $opentok = new OpenTok($this->_apiKey, $this->_apiSecret);

                $data['token'] = $opentok->generateToken($sessionId, array(
                    'role' => 'publisher',
                    'expireTime' => time()+(7 * 24 * 60 * 60),
                    'data' =>  $connectionMetaData
                ));

                $connectionData['token'] = $data['token'];

                $this->_webinar_connection_collection->updateOne(
                    $mongoConnectionQuery,
                    ['$set' => $connectionData]
                );
            }
            else
            {
                $data['token'] = $connection['token'];
            }
        }
        else if($connection && !$connection['allow'])
        {
            if($connection['token'])
            {
                $tokboxId = base64_decode($encodedRoomId);
                $username = base64_decode($encodedUsername);

                $connectionMetaData = $username;
                $sessionId = $this->_tokboxSessionIds[$tokboxId];

                $opentok = new OpenTok($this->_apiKey, $this->_apiSecret);

                $data['token'] = $opentok->generateToken($sessionId, array(
                    'role' => 'subscriber',
                    'expireTime' => time()+(7 * 24 * 60 * 60),
                    'data' =>  $connectionMetaData
                ));

                $connectionData['token'] = '';

                $this->_webinar_connection_collection->updateOne(
                    $mongoConnectionQuery,
                    ['$set' => $connectionData]
                );
            }
            else
            {
                $data['token'] = $connection['token'];
            }
        }

        echo json_encode($data);
    }

    function download_brochure($encodedBoothId = NULL)
    {
        if($encodedBoothId)
        {
            $tokboxId = $encodedBoothId;

            $mongoQuery['tokbox_token_id'] = (int)$tokboxId;
            $fairDelegateDetail = $this->_fair_delegates_collection->findOne($mongoQuery);

            //echo $fairDelegateDetail['brochure_url'];
            redirect($fairDelegateDetail['brochure_url']);

            /*$boothId = base64_decode($encodedBoothId);

            $mongoFairUniversityQuery['id'] = (int)$boothId;
            $fairUniversity = $this->_fair_universities_collection->findOne($mongoFairUniversityQuery);

            $filename = $fairUniversity['brochures'];
            $downloadFileName = $fairUniversity['name'];

            $filepath = APPPATH . '..' . $filename;

            header("Content-type: application/zip");
            header('Content-Disposition: attachment; filename="' . $downloadFileName . '".zip;');
            header("Pragma: no-cache");
            header("Expires: 0");
            readfile($filepath);*/
        }
        else
        {
            $downloadFileName = 'Falmouth_student';
            $filepath = APPPATH . '/images/Falmouth_student.zip';

            header("Content-type: application/zip");
            header('Content-Disposition: attachment; filename="' . $downloadFileName . '".zip;');
            header("Pragma: no-cache");
            header("Expires: 0");
            readfile($filepath);
        }
        exit;
    }

    function coming_fair_event()
    {
        $current_time = date('Y-m-d H:i:s');
        $current_time_string = strtotime($current_time);

        $fair_events_cursor = $this->_fair_events_collection->find();
        foreach($fair_events_cursor as $event)
        {
            $event_start_time = $event['start_time'];
            $event_end_time = $event['end_time'];

            $event_start_time_string = strtotime($event_start_time);
            $event_end_time_string = strtotime($event_end_time);

            $start_diff = $event_start_time_string - $current_time_string;
            $end_diff = $event_end_time_string - $current_time_string;

            //echo $current_time . "<br>" . $event_start_time . "<br>" . $event_end_time . "<br>" . $current_time_string . "<br>" . $event_start_time_string . "<br>" . $event_end_time_string;

            if($start_diff > 0 && $start_diff < 300)
            {
                $arr = [
                    "success" => true,
                    "msg" => 'Event will start shortly',
                    "join" => false
                ];
                echo json_encode($arr);
                exit;
            }
            else if($start_diff < 0 && $end_diff > 0)
            {
                $arr = [
                    "success" => true,
                    "msg" => 'Event is running',
                    "join" => true,
                    "room_id" => 'MjI5'
                ];
                echo json_encode($arr);
                exit;
            }
        }
        $arr = [
            "success" => false
        ];
        echo json_encode($arr);
        exit;
    }

    function university($id)
    {
        $mongoFairUniversityQuery['id'] = (int)$id;
        $fairUniversity = $this->_fair_universities_collection->findOne($mongoFairUniversityQuery);
        $encodedBoothId = base64_encode($fairUniversity['id']);
        $encodedBoothId = str_replace("=", "", $encodedBoothId);
        $fairUniversity['encodedId'] = $encodedBoothId;
        echo json_encode($fairUniversity);
    }

    function schedule($encodedUsername)
    {
        $data['encodedUsername'] = $encodedUsername;
        $this->load->view('templates/meeting/event_schedule', $data);
    }

    function check_available_room($encodedUsername, $roomType, $internalCall = false)
    {
        $username = base64_decode($encodedUsername);
        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $roomLimit = $this->_room_limit;

        switch($roomType)
        {
            case 'PRE':
                $tokboxIds = $this->_pre_counseling_rooms;
                $roomLimit = $this->_counseling_room_limit;
                break;
            case 'FINANCE':
                $tokboxIds = [360, 362, 364, 366, 368, 370];
                break;
            case 'POST':
                $tokboxIds = $this->_post_counseling_rooms;
                $roomLimit = $this->_counseling_room_limit;
                break;
            case 'IMPERIAL':
                $tokboxIds = [434, 435, 436, 437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453];
                break;
            case 'EDULOAN':
                //$tokboxIds = [454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465, 466];
                $tokboxIds = $this->_eduloans_rooms;
                $roomLimit = $this->_counseling_room_limit;
                break;
            case 'HDFC':
                $tokboxIds = [467];
                break;
            case 'FOREX':
                $tokboxIds = [468];
                break;
            case 'COMPUTER':
                $tokboxIds = $this->_computer_rooms;
                $tokboxId = $tokboxIds[mt_rand(0, count($tokboxIds) - 1)];
                $encodedRoomId = base64_encode($tokboxId);
                $encodedRoomId = str_replace("=", "", $encodedRoomId);
                echo $encodedRoomId;
                exit;
                break;
        }
        shuffle($tokboxIds);

        $finalTokboxId = '';
        $studentCount = 0;

        foreach($tokboxIds as $tokboxId)
        {
            $mongoRoomStudentCountQuery['tokbox_id'] = (int)$tokboxId;
            $roomStudentsCount = $this->_room_student_count_collection->count($mongoRoomStudentCountQuery);
            if(!$roomStudentsCount)
            {
                $finalTokboxId = $tokboxId;
                break;
            }
        }

        if(!$finalTokboxId)
        {
            $previousCount = $roomLimit;
            foreach($tokboxIds as $index => $tokboxId)
            {
                $mongoRoomStudentCountQuery['tokbox_id'] = (int)$tokboxId;
                $roomStudentsCount = $this->_room_student_count_collection->count($mongoRoomStudentCountQuery);

                // May be not needed cause of shuffling

                if($roomStudentsCount < $roomLimit && $roomStudentsCount < $previousCount)
                {
                    $finalTokboxId = $tokboxId;
                    $previousCount = $roomStudentsCount;
                }

                /*if($roomStudentsCount < $roomLimit)
                {
                    $finalTokboxId = $tokboxId;
                    break;
                }*/
            }
        }
        if($finalTokboxId)
        {
            $encodedRoomId = base64_encode($finalTokboxId);
            $encodedRoomId = str_replace("=", "", $encodedRoomId);

            if($internalCall)
            {
                return $encodedRoomId;
            }
            else
            {
                echo $encodedRoomId;
                exit;
            }
        }
        if($internalCall)
        {
            return 0;
        }
        else
        {
            echo 0;
        }
    }

    function check_empty_room()
    {
        $allTokboxIds = $this->_room_tokbox_ids;

        $imperialTokboxIds = [434, 435, 436, 437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453];

        $eduloanTokboxIds = [454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465, 466];

        $hdfcTokboxIds = [467];

        $forexTokboxIds = [468];

        $availableRooms = [];

        foreach($allTokboxIds as $tokboxId)
        {
            $mongoRoomStudentCountQuery['tokbox_id'] = (int)$tokboxId;
            $roomStudentsCount = $this->_room_student_count_collection->count($mongoRoomStudentCountQuery);
            if($roomStudentsCount < $this->_room_limit)
            {
                if(in_array($tokboxId, $imperialTokboxIds) && !in_array('Imperial', $availableRooms))
                {
                    $availableRooms[] = 'Imperial';
                }
                else if(in_array($tokboxId, $eduloanTokboxIds) && !in_array('Eduloan', $availableRooms))
                {
                    $availableRooms[] = 'Eduloan';
                }
                else if(in_array($tokboxId, $forexTokboxIds) && !in_array('Forex', $availableRooms))
                {
                    $availableRooms[] = 'Forex';
                }
                else if(in_array($tokboxId, $hdfcTokboxIds) && !in_array('HDFC', $availableRooms))
                {
                    $availableRooms[] = 'HDFC';
                }
            }
        }

        if($availableRooms)
        {
            $roomString = implode(', ', $availableRooms);
            echo $roomString . ' rooms are available';
            exit;
        }
        echo 0;
    }

    function book_consultation($encodedUsername)
    {
        $username = base64_decode($encodedUsername);
        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);

        $mongoConsultationQuery['username'] = $username;
        $this->_free_consultation_collection->deleteMany($mongoConsultationQuery);
        $this->_free_consultation_collection->insertOne($mongoConsultationQuery);
    }

    function get_help($encodedToxboxId)
    {
        $tokboxId = base64_decode($encodedToxboxId);
        $tokboxId = iconv('UTF-8', 'UTF-8//IGNORE', $tokboxId);

        $help['tokbox_id'] = (int)$tokboxId;
        $this->_help_collection->insertOne($help);
    }

    function check_time()
    {
        $post_data = json_decode(file_get_contents("php://input"), true);
        //date_default_timezone_set($post_data['timezone']);
        $launchDate = '2022-01-30 17:00:00';
     	$date = new DateTime($launchDate, new DateTimeZone('Asia/Calcutta'));

     	$launchTime = strtotime($date->format('Y-m-d H:i:s')) * 1000;

        echo $launchTime;
    }

    function launch($encodedToxboxId, $encodedUsername)
    {
        $data['tokboxId'] = $encodedToxboxId;
        $data['username'] = $encodedUsername;

        $this->load->view('templates/meeting/launch', $data);
    }

    function check_user($userId)
    {
        $userIpAddress = $this->get_client_ip();

        $mongoIpQuery['user_id'] = ['$eq' => (int)$userId];
        $userIpDetail = $this->_ip_collection->findOne($mongoIpQuery);
        if($userIpDetail)
        {
            $lastIp = $userIpDetail['ip_address'];
            $linkAllowed = $userIpDetail['link_allowed'];

            if($lastIp != $userIpAddress && $linkAllowed == 1)
            {
                echo 0;
                exit;
            }
        }
        echo 1;
    }

    function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        return $ipaddress;
    }

    function demo()
    {
        exit;
        $demoUsersCursor = $this->_demo_collection->find();
        foreach($demoUsersCursor as $demoUser)
        {
            $username = $demoUser['username'];

            $usernameEncode = base64_encode($username);
            $usernameEncode = str_replace("=", "", $usernameEncode);

            $mongoQueryDemo['username'] = $username;
            $this->_demo_collection->deleteMany($mongoQueryDemo);

            redirect("/event/reception/" . $usernameEncode);
        }

    }

    function temp()
    {
        //$post_data = json_decode(file_get_contents("php://input"), true);
        $post_data = $this->input->post();
        $name = $post_data['name'] ? $post_data['name'] : '';
        if($name)
        {
            $email = $post_data['email'] ? $post_data['email'] : str_replace(' ', '', $name) . '@imperial.com';
            $phone = $post_data['phone'] ? $post_data['phone'] : '';
            $link_allowed = $post_data['link'] ? $post_data['link'] : 1;
            $username = str_replace(' ', '', $name) . mt_rand(111111, 999999);

            $email = iconv('UTF-8', 'UTF-8//IGNORE', $email);
            $mongoUserQuery['email'] = ['$eq' => $email];
            $userDetail = $this->_user_collection->findOne($mongoUserQuery);
            if(!$userDetail)
            {
                $userMaster['username'] = $username;
                $userMaster['password'] = md5($username);
                $userMaster['email'] = $email;
                $userMaster['name'] = $name;
                $userMaster['link_allowed'] = $link_allowed;
                $userMaster['phone'] = $phone;
                $userMaster['registration_type'] = $this->_eventName;

                $userId = $this->seminar_model->insertUserMaster($userMaster);

                $seminarData['user_id'] = $userId;
                $seminarData['username'] = $username;
                $seminarData['email'] = $email;
                $seminarData['name'] = $name;
                $seminarData['phone'] = $phone;
                $seminarData['current_use'] = $this->_eventName;

                $this->seminar_model->insertSeminarStudent($seminarData);

                $newUserId = $userId;

                $userData['id'] = (int)$newUserId;
                $userData['email'] = $email;
                $userData['name'] = $name;
                $userData['phone'] = $phone;
                $userData['username'] = $username;
                $userData['registration_type'] = $this->_eventName;
                $userData['link_allowed'] = $link_allowed;

                $this->_user_collection->insertOne($userData);
            }
            else
            {
                $username = $userDetail['username'];
            }
            $usernameEncode = base64_encode($username);
            $usernameEncode = str_replace("=", "", $usernameEncode);
            redirect("/wpi-intro/" . $usernameEncode);
        }
        else
        {
            $this->load->view('templates/meeting/temp_register');
        }
    }

    function runtime_register()
    {
        $post_data = json_decode(file_get_contents("php://input"), true);
        $email = $post_data['email'];
        $name = $post_data['name'] ? $post_data['name'] : $post_data['email'];
        $phone = $post_data['phone'] ? $post_data['phone'] : '';
        $link_allowed = $post_data['link'] ? $post_data['link'] : 1;
        $username = 'user_sep_' . mt_rand(111111, 999999);

        $email = iconv('UTF-8', 'UTF-8//IGNORE', $email);
        $mongoUserQuery['email'] = ['$eq' => $email];
        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        if(!$userDetail)
        {
            $userMaster['username'] = $username;
            $userMaster['password'] = md5($username);
            $userMaster['email'] = $email;
            $userMaster['name'] = $name;
            $userMaster['link_allowed'] = $link_allowed;
            $userMaster['phone'] = $phone;
            $userMaster['registration_type'] = $this->_eventName;

            $userId = $this->seminar_model->insertUserMaster($userMaster);

            $seminarData['user_id'] = $userId;
            $seminarData['username'] = $username;
            $seminarData['email'] = $email;
            $seminarData['name'] = $name;
            $seminarData['phone'] = $phone;
            $seminarData['current_use'] = $this->_eventName;

            $this->seminar_model->insertSeminarStudent($seminarData);

            /*$options = [
                'sort' => [
                    'id' => -1
                ],
                'limit' => 1
            ];
            $maxIdUserDetail = $this->_user_collection->findOne([], $options);
            $maxId = $maxIdUserDetail['id'];

            $newUserId = $maxId + 1;*/

            $newUserId = $userId;

            $userData['id'] = (int)$newUserId;
            $userData['email'] = $email;
            $userData['name'] = $name;
            $userData['phone'] = $phone;
            $userData['username'] = $username;
            $userData['registration_type'] = $this->_eventName;
            $userData['link_allowed'] = $link_allowed;

            $this->_user_collection->insertOne($userData);
        }

        echo "Done";
    }

    function queue_priority()
    {
        $post_data = json_decode(file_get_contents("php://input"), true);
        $email = $post_data['email'];

        $email = iconv('UTF-8', 'UTF-8//IGNORE', $email);
        $mongoUserQuery['email'] = ['$eq' => $email];
        $userDetail = $this->_user_collection->findOne($mongoUserQuery);

        if($userDetail)
        {
            $username = $userDetail['username'];

            $mongoQueueQuery['username'] = $username;
            $queueData = $this->_queue_collection->findOne($mongoQueueQuery);

            $queueTokboxId = 0;

            if($queueData)
            {
                $queueTokboxId = $queueData['tokbox_id'];

                $this->_queue_collection->deleteMany($mongoQueueQuery);
            }

            if($queueTokboxId)
            {
                $newQueueData['tokbox_id'] = (int)$queueTokboxId;
                $newQueueData['username'] = $username;
                $newQueueData['priority'] = 1;

                $this->_queue_collection->insertOne($newQueueData);
            }
        }

        echo "Done";
    }

    function delegate()
    {
        $post_data = json_decode(file_get_contents("php://input"), true);
        $designation = $post_data['designation'];
        $name = $post_data['name'] ? $post_data['name'] : $post_data['designation'];
        $token_id = $post_data['token_id'];
        $mapped_token_id = $post_data['mapped_token_id'];
        $username = $post_data['username'];
        $roomType = $post_data['type'];
        //$username = $username . '_' . $token_id;

        $usernameEncode = base64_encode($username);
        $usernameEncode = str_replace("=", "", $usernameEncode);

        $tokenIdEncode = base64_encode($mapped_token_id);
        $tokenIdEncode = str_replace("=", "", $tokenIdEncode);

        $roomUrl = 'https://hellouni.org/event/panelist/' . $tokenIdEncode . '/' . $usernameEncode;

        $delegateData['designation'] = $designation;
        $delegateData['name'] = $name;
        $delegateData['tokbox_token_id'] = (int)$token_id;
        $delegateData['username'] = $username;
        $delegateData['room_url'] = $roomUrl;
        $delegateData['event'] = $this->_eventName;
        $delegateData['mapping_id'] = (int)$mapped_token_id;

        $delegateId = $this->seminar_model->insertFairDelegate($delegateData);

        $delegateData['id'] = (int)$delegateId;

        $this->_fair_delegates_collection->insertOne($delegateData);

        $tokenData['room_type'] = $roomType;
        $this->seminar_model->editToken($tokenData, $delegateData['tokbox_token_id']);

        $tokbox_token_query['id'] = $delegateData['tokbox_token_id'];
        $this->_tokbox_token_collection->updateOne(
            $tokbox_token_query,
            ['$set' => $tokenData]
        );

        echo "Done";
    }

    function keysValueTokbox(){

      $opentok = new OpenTok('46220952', '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2');

      $tokbox_data['api_key'] = '46220952';
      $tokbox_data['session_key'] = '2_MX40NjIyMDk1Mn5-MTY5Nzg5Mjk4NjQ3OX5zdWJpL2pUMjhXczEyS2FwNXRRb29KTFJ-fn4';


      $sessionId = '2_MX40NjIyMDk1Mn5-MTY5Nzg5Mjk4NjQ3OX5zdWJpL2pUMjhXczEyS2FwNXRRb29KTFJ-fn4';

      $token = $opentok->generateToken($sessionId);
      $tokbox_data['token_key'] = $token;
      $currentTime = date('Y-m-d H:i:s');
      $expiry = strtotime($currentTime) + 3600;
      $expiryTime = date('Y-m-d H:i:s', $expiry);
      $tokbox_data['expiry_time'] = $expiryTime;

      echo"<pre>";
      var_dump($tokbox_data);
      die();
      //header('Content-Type: application/json');
      //echo (json_encode($tokbox_data));

    }

    /*function get_publisher_token($tokboxId, $username)
    {
        $data['base64_tokbox_id'] = $tokboxId;
        $data['base64_username'] = $username;

        $tokboxId = base64_decode($tokboxId);
        $username = base64_decode($username);

        $connectionMetaData = $username;
        $sessionId = $this->_tokboxSessionIds[$tokboxId];

        $opentok = new OpenTok($this->_apiKey, $this->_apiSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'publisher',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        echo json_encode($data);
    }*/

    function allow_student($userId)
    {
        $mongoConnectionQuery['user_id'] = (int)$userId;
        $connection = $this->_webinar_connection_collection->findOne($mongoConnectionQuery);
        if($connection)
        {
            $connectionData['allow'] = true;

            $this->_webinar_connection_collection->updateOne(
                $mongoConnectionQuery,
                ['$set' => $connectionData]
            );
        }
    }

    function disallow_student($userId)
    {
        $mongoConnectionQuery['user_id'] = (int)$userId;
        $connection = $this->_webinar_connection_collection->findOne($mongoConnectionQuery);
        if($connection)
        {
            $connectionData['allow'] = false;

            $this->_webinar_connection_collection->updateOne(
                $mongoConnectionQuery,
                ['$set' => $connectionData]
            );
        }
    }

    function log_error()
    {
        $post_data = json_decode(file_get_contents("php://input"), true);
        $this->_tokbox_error_collection->insertOne($post_data);
    }
}
