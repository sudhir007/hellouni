<?php
require APPPATH . "vendor/autoload.php";
require_once APPPATH . 'libraries/Mail/sMail.php';

use OpenTok\OpenTok;
class Session extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library(array('form_validation', 'uri'));
        $this->load->model(array('session/sessionmanagement_model', 'tokbox_model'));

        $this->outputData['time_array'] = array('00:00', '00:15', '00:30', '00:45', '01:00', '01:15', '01:30', '01:45', '02:00', '02:15', '02:30', '02:45', '03:00', '03:15', '03:30', '03:45', '04:00', '04:15', '04:30', '04:45', '05:00', '05:15', '05:30', '05:45', '06:00', '06:15', '06:30', '06:45', '07:00', '07:15', '07:30', '07:45', '08:00', '08:15', '08:30', '08:45', '09:00', '09:15', '09:30', '09:45', '10:00', '10:15', '10:30', '10:45', '11:00', '11:15', '11:30', '11:45', '12:00', '12:15', '12:30', '12:45', '13:00', '13:15', '13:30', '13:45', '14:00', '14:15', '14:30', '14:45', '15:00', '15:15', '15:30', '15:45', '16:00', '16:15', '16:30', '16:45', '17:00', '17:15', '17:30', '17:45', '18:00', '18:15', '18:30', '18:45', '19:00', '19:15', '19:30', '19:45', '20:00', '20:15', '20:30', '20:45', '21:00', '21:15', '21:30', '21:45', '22:00', '22:15', '22:30', '22:45', '23:00', '23:15', '23:30', '23:45');
    }

    function index(){
        $this->load->helper('cookie_helper');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('user/account');
            exit();
        }
        $this->render_page('templates/user/sessionall',$this->outputData);
    }

    function allsession()
    {
        $userdata=$this->session->userdata('user');

        if(empty($userdata)){  redirect('common/login'); }
        $universityId = 0;
        if(isset($userdata['id']))
        {
            $universityId = $userdata['id'];
            $universityDetail = $this->sessionmanagement_model->getUniversityById($userdata['university_id']);
            $timezone = isset($universityDetail[0]['timezone']) ? $universityDetail[0]['timezone'] : '';
        }
        else
        {
            $timezone = 5.5;
        }

        $allSessions = $this->sessionmanagement_model->getSlotByUniversity($universityId);
        if($allSessions)
        {
            foreach($allSessions as $index => $session)
            {
                $localTimezone = $session->timezone ? 5.5 - (float)$session->timezone : 5.5 - (float)$timezone;
                $session->slot_1 = $session->slot_1 ? date('Y-m-d H:i:s', strtotime($session->slot_1) - $localTimezone * 60 * 60) : '';
                $session->slot_2 = $session->slot_2 ? date('Y-m-d H:i:s', strtotime($session->slot_2) - $localTimezone * 60 * 60) : '';
                $session->slot_3 = $session->slot_3 ? date('Y-m-d H:i:s', strtotime($session->slot_3) - $localTimezone * 60 * 60) : '';
                $session->confirmed_slot = $session->confirmed_slot ? date('Y-m-d H:i:s', strtotime($session->confirmed_slot) - $localTimezone * 60 * 60) : '';
                $allSessions[$index] = $session;
            }
        }
        $allSession['session_page'] = 'All Sessions';
        $allSession['sessions'] = $allSessions;
        $allSession['redirect_url'] = '/user/session';
        $allSession['user_detail'] = $userdata;

        header('Content-Type: application/json');
        echo json_encode($allSession);
        //$this->outputData['timezone'] = $localTimezone;
        //$this->render_page('templates/user/session', $this->outputData);
    }

    function coming(){
        $this->load->helper('cookie_helper');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('user/account');
            exit();
        }
        $this->render_page('templates/user/sessioncoming',$this->outputData);
    }

    function comingfilter()
    {
        $userdata=$this->session->userdata('user');

        if(empty($userdata)){  redirect('common/login'); }
        $universityId = 0;
        if(isset($userdata['id']))
        {
            $universityId = $userdata['id'];
            $universityDetail = $this->sessionmanagement_model->getUniversityById($userdata['id']);
            $timezone = isset($universityDetail[0]['timezone']) ? $universityDetail[0]['timezone'] : '';
        }
        else
        {
            $timezone = 5.5;
        }
        //$localTimezone = 5 - (int)$timezone;

        $allSessions = $this->sessionmanagement_model->getComingSlotByUniversity($universityId);
        if($allSessions)
        {
            foreach($allSessions as $index => $session)
            {
                $user_id = $session->user_id;
                $universityId = $session->university_id;
                $participants = array("user_id" => $user_id,"university_id" => $universityId);
                $json_participant = json_encode($participants);
                $token = $this->tokbox_model->getTokenByParticipant($json_participant);
                $allSessions[$index]->tokbox_url = "";
                if($token && $token->token)
                {
                    $encodedTokboxId = base64_encode($token->id);
                    $encodedUniversityId = base64_encode($userdata['id']);
                    $allSessions[$index]->tokbox_url = str_replace('/admin', '', base_url()) . "/meeting/oneonone/panelist?id=" . $encodedTokboxId;
                }
                $localTimezone = $session->timezone ? 5.5 - (float)$session->timezone : 5.5 - (float)$timezone;
                $session->slot_1 = $session->slot_1 ? date('Y-m-d H:i:s', strtotime($session->slot_1) - $localTimezone * 60 * 60) : '';
                $session->slot_2 = $session->slot_2 ? date('Y-m-d H:i:s', strtotime($session->slot_2) - $localTimezone * 60 * 60) : '';
                $session->slot_3 = $session->slot_3 ? date('Y-m-d H:i:s', strtotime($session->slot_3) - $localTimezone * 60 * 60) : '';
                $session->confirmed_slot = $session->confirmed_slot ? date('Y-m-d H:i:s', strtotime($session->confirmed_slot) - $localTimezone * 60 * 60) : '';
                $allSessions[$index] = $session;
            }
        }

        $comingSession['sessions'] = $allSessions;
        $comingSession['session_page'] = 'Coming Sessions';
        $comingSession['redirect_url'] = '/user/session/coming';
        $comingSession['user_detail'] = $userdata;

        header('Content-Type: application/json');
        echo json_encode($comingSession);

        //$this->render_page('templates/user/session', $this->outputData);
    }

    function completed(){
        $this->load->helper('cookie_helper');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('user/account');
            exit();
        }
        $this->render_page('templates/user/sessioncompleted',$this->outputData);
    }

    function completedfilter()
    {
        $userdata=$this->session->userdata('user');

        if(empty($userdata)){  redirect('common/login'); }
        $universityId = 0 ;
        if(isset($userdata['id']))
        {
            $universityId = $userdata['id'];
            $universityDetail = $this->sessionmanagement_model->getUniversityById($userdata['id']);
            $timezone = isset($universityDetail[0]['timezone']) ? $universityDetail[0]['timezone'] : '';
        }
        else
        {
            $timezone = 5.5;
        }
        //$localTimezone = 5 - (int)$timezone;

        $allSessions = $this->sessionmanagement_model->getCompletedSlotByUniversity($universityId);
        if($allSessions)
        {
            foreach($allSessions as $index => $session)
            {
                $user_id = $session->user_id;
                $universityId = $session->university_id;
                $participants = array("user_id" => $user_id,"university_id" => $universityId);
                $json_participant = json_encode($participants);
                $token = $this->tokbox_model->getTokenByParticipant($json_participant);
                $session->recording = 0;
                if($token && $token->archive_id)
                {
                    $session->recording = 1;
                    $session->archive_id = $token->archive_id;
                }
                $localTimezone = $session->timezone ? 5.5 - (float)$session->timezone : 5.5 - (float)$timezone;
                $session->slot_1 = $session->slot_1 ? date('Y-m-d H:i:s', strtotime($session->slot_1) - $localTimezone * 60 * 60) : '';
                $session->slot_2 = $session->slot_2 ? date('Y-m-d H:i:s', strtotime($session->slot_2) - $localTimezone * 60 * 60) : '';
                $session->slot_3 = $session->slot_3 ? date('Y-m-d H:i:s', strtotime($session->slot_3) - $localTimezone * 60 * 60) : '';
                $session->confirmed_slot = $session->confirmed_slot ? date('Y-m-d H:i:s', strtotime($session->confirmed_slot) - $localTimezone * 60 * 60) : '';
                $allSessions[$index] = $session;
            }
        }

        $completedSession['sessions'] = $allSessions;
        $completedSession['session_page'] = 'Completed Sessions';
        $completedSession['redirect_url'] = '/user/session/completed';
        $completedSession['user_detail'] = $userdata;

        header('Content-Type: application/json');
        echo json_encode($completedSession);

        //$this->outputData['timezone'] = $localTimezone;
        //$this->render_page('templates/user/session', $this->outputData);
    }

    function unassigned(){
        $this->load->helper('cookie_helper');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('user/account');
            exit();
        }
        $this->render_page('templates/user/sessionunassigned',$this->outputData);
    }

    function unassignedfilter()
    {
        $userdata=$this->session->userdata('user');

        if(empty($userdata)){  redirect('common/login'); }
        $universityId = 0;
        if(isset($userdata['id']))
        {
            $universityId = $userdata['id'];
            $universityDetail = $this->sessionmanagement_model->getUniversityById($userdata['university_id']);
            $timezone = isset($universityDetail[0]['timezone']) ? $universityDetail[0]['timezone'] : '';
        }
        else
        {
            $timezone = 5.5;
        }
        //$localTimezone = 5 - (int)$timezone;

        $allSessions = $this->sessionmanagement_model->getUnassignedSlotByUniversity($universityId);
        if($allSessions)
        {
            foreach($allSessions as $index => $session)
            {
                $localTimezone = $session->timezone ? 5.5 - (float)$session->timezone : 5.5 - (float)$timezone;
                $session->slot_1 = $session->slot_1 ? date('Y-m-d H:i:s', strtotime($session->slot_1) - $localTimezone * 60 * 60) : '';
                $session->slot_2 = $session->slot_2 ? date('Y-m-d H:i:s', strtotime($session->slot_2) - $localTimezone * 60 * 60) : '';
                $session->slot_3 = $session->slot_3 ? date('Y-m-d H:i:s', strtotime($session->slot_3) - $localTimezone * 60 * 60) : '';
                $allSessions[$index] = $session;
            }
        }
        $unassignedSession['sessions'] = $allSessions;
        $unassignedSession['user_detail'] = $userdata;
        $unassignedSession['session_page'] = 'Unassigned Sessions';
        $unassignedSession['redirect_url'] = '/user/session/unassigned';

        header('Content-Type: application/json');
        echo json_encode($unassignedSession);

        //$this->outputData['timezone'] = $localTimezone;
        //$this->render_page('templates/user/session', $this->outputData);
    }

    function unconfirmed(){
        $this->load->helper('cookie_helper');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('user/account');
            exit();
        }
        $this->render_page('templates/user/sessionunconfirmed',$this->outputData);
    }

    function unconfirmedfilter()
    {
        $userdata=$this->session->userdata('user');

        if(empty($userdata)){  redirect('common/login'); }
        $universityId = 0;
        if(isset($userdata['id']))
        {
            $universityId = $userdata['id'];
            $universityDetail = $this->sessionmanagement_model->getUniversityById($userdata['university_id']);
            $timezone = isset($universityDetail[0]['timezone']) ? $universityDetail[0]['timezone'] : '';
        }
        else
        {
            $timezone = 5.5;
        }
        //$localTimezone = 5 - (int)$timezone;

        $allSessions = $this->sessionmanagement_model->getUnconfirmedSlotByUniversity($universityId);
        if($allSessions)
        {
            foreach($allSessions as $index => $session)
            {
                $localTimezone = $session->timezone ? 5.5 - (float)$session->timezone : 5.5 - (float)$timezone;
                $session->slot_1 = $session->slot_1 ? date('Y-m-d H:i:s', strtotime($session->slot_1) - $localTimezone * 60 * 60) : '';
                $session->slot_2 = $session->slot_2 ? date('Y-m-d H:i:s', strtotime($session->slot_2) - $localTimezone * 60 * 60) : '';
                $session->slot_3 = $session->slot_3 ? date('Y-m-d H:i:s', strtotime($session->slot_3) - $localTimezone * 60 * 60) : '';
                $allSessions[$index] = $session;
            }
        }

        $unconfirmedSession['sessions'] = $allSessions;
        $unconfirmedSession['session_page'] = 'Unconfirmed Sessions';
        $unconfirmedSession['redirect_url'] = '/user/session/unconfirmed';
        $unconfirmedSession['user_detail'] = $userdata;

        header('Content-Type: application/json');
        echo json_encode($unconfirmedSession);

        //$this->outputData['timezone'] = $localTimezone;
        //$this->render_page('templates/user/session', $this->outputData);
    }

    function predefineslots(){
        $this->load->helper('cookie_helper');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('user/account');
            exit();
        }
        $this->render_page('templates/user/predefineslots',$this->outputData);
    }

    function predefineslotsfilter(){

      $userdata=$this->session->userdata('user');

      if(empty($userdata)){  redirect('common/login'); }

      $universityId = 0;
      if(isset($userdata['university_id']))
      {
          $universityId = $userdata['university_id'];
      }

      $preDefineListObj = $this->sessionmanagement_model->preDefineList($universityId);

      $predefineSlots['predefined_slots'] = $preDefineListObj;
      $predefineSlots['user_detail'] = $userdata;

      header('Content-Type: application/json');
      echo json_encode($predefineSlots);

      //var_dump($preDefineListObj);
      //die();

      //$this->render_page('templates/user/predefine_slots', $this->outputData);

    }



    function form()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/user_model');
        //$this->load->model('user/university_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata)){  redirect('common/login'); }

        //$universityDetail = $this->university_model->getUniversityById($userdata['university_id']);
        //$timezone = $universityDetail[0]['timezone'];

        if(isset($userdata['university_id']))
        {
            $universityDetail = $this->sessionmanagement_model->getUniversityById($userdata['university_id']);
            $timezone = isset($universityDetail[0]['timezone']) ? $universityDetail[0]['timezone'] : '';
        }
        else
        {
            $timezone = 5.5;
        }

        $this->outputData['time_array'] = array('12 AM', '12:15 AM', '12:30 AM', '12:45 AM', '01:00 AM', '01:15 AM', '01:30 AM', '01:45 AM', '02:00 AM', '02:15 AM', '02:30 AM', '02:45 AM', '03:00 AM', '03:15 AM', '03:30 AM', '03:45 AM', '04:00 AM', '04:15 AM', '04:30 AM', '04:45 AM', '05:00 AM', '05:15 AM', '05:30 AM', '05:45 AM', '06:00 AM', '06:15 AM', '06:30 AM', '06:45 AM', '07:00 AM', '07:15 AM', '07:30 AM', '07:45 AM', '08:00 AM', '08:15 AM', '08:30 AM', '08:45 AM', '09:00 AM', '09:15 AM', '09:30 AM', '09:45 AM', '10:00 AM', '10:15 AM', '10:30 AM', '10:45 AM', '11:00 AM', '11:15 AM', '11:30 AM', '11:45 AM', '12:00 PM', '12:15 PM', '12:30 PM', '12:45 PM', '01:00 PM', '01:15 PM', '01:30 PM', '01:45 PM', '02:00 PM', '02:15 PM', '02:30 PM', '02:45 PM', '03:00 PM', '03:15 PM', '03:30 PM', '03:45 PM', '04:00 PM', '04:15 PM', '04:30 PM', '04:45 PM', '05:00 PM', '05:15 PM', '05:30 PM', '05:45 PM', '06:00 PM', '06:15 PM', '06:30 PM', '06:45 PM', '07:00 PM', '07:15 PM', '07:30 PM', '07:45 PM', '08:00 PM', '08:15 PM', '08:30 PM', '08:45 PM', '09:00 PM', '09:15 PM', '09:30 PM', '09:45 PM', '10:00 PM', '10:15 PM', '10:30 PM', '10:45 PM', '11:00 PM', '11:15 PM', '11:30 PM', '11:45 PM');

        $time_array = array('00:00:00' => '12 AM', '00:15:00' => '12:15 AM', '00:30:00' => '12:30 AM', '00:45:00' => '12:45 AM', '01:00:00' => '01:00 AM', '01:15:00' => '01:15 AM', '01:30:00' => '01:30 AM', '01:45:00' => '01:45 AM', '02:00:00' => '02:00 AM', '02:15:00' => '02:15 AM', '02:30:00' => '02:30 AM', '02:45:00' => '02:45 AM', '03:00:00' => '03:00 AM', '03:15:00' => '03:15 AM', '03:30:00' => '03:30 AM', '03:45:00' => '03:45 AM', '04:00:00' => '04:00 AM', '04:15:00' => '04:15 AM', '04:30:00' => '04:30 AM', '04:45:00' => '04:45 AM', '05:00:00' => '05:00 AM', '05:15:00' => '05:15 AM', '05:30:00' => '05:30 AM', '05:45:00' => '05:45 AM', '06:00:00' => '06:00 AM', '06:15:00' => '06:15 AM', '06:30:00' => '06:30 AM', '06:45:00' => '06:45 AM', '07:00:00' => '07:00 AM', '07:15:00' => '07:15 AM', '07:30:00' => '07:30 AM', '07:45:00' => '07:45 AM', '08:00:00' => '08:00 AM', '08:15:00' => '08:15 AM', '08:30:00' => '08:30 AM', '08:45:00' => '08:45 AM', '09:00:00' => '09:00 AM', '09:15:00' => '09:15 AM', '09:30:00' => '09:30 AM', '09:45:00' => '09:45 AM', '10:00:00' => '10:00 AM', '10:15:00' => '10:15 AM', '10:30:00' => '10:30 AM', '10:45:00' => '10:45 AM', '11:00:00' => '11:00 AM', '11:15:00' => '11:15 AM', '11:30:00' => '11:30 AM', '11:45:00' => '11:45 AM', '12:00:00' => '12:00 PM', '12:15:00' => '12:15 PM', '12:30:00' => '12:30 PM', '12:45:00' => '12:45 PM', '13:00:00' => '01:00 PM', '13:15:00' => '01:15 PM', '13:30:00' => '01:30 PM', '13:45:00' => '01:45 PM', '14:00:00' => '02:00 PM', '14:15:00' => '02:15 PM', '14:30:00' => '02:30 PM', '14:45:00' => '02:45 PM', '15:00:00' => '03:00 PM', '15:15:00' => '03:15 PM', '15:30:00' => '03:30 PM', '15:45:00' => '03:45 PM', '16:00:00' => '04:00 PM', '16:15:00' => '04:15 PM', '16:30:00' => '04:30 PM', '16:45:00' => '04:45 PM', '17:00:00' => '05:00 PM', '17:15:00' => '05:15 PM', '17:30:00' => '05:30 PM', '17:45:00' => '05:45 PM', '18:00:00' => '06:00 PM', '18:15:00' => '06:15 PM', '18:30:00' => '06:30 PM', '18:45:00' => '06:45 PM', '19:00:00' => '07:00 PM', '19:15:00' => '07:15 PM', '19:30:00' => '07:30 PM', '19:45:00' => '07:45 PM', '20:00:00' => '08:00 PM', '20:15:00' => '08:15 PM', '20:30:00' => '08:30 PM', '20:45:00' => '08:45 PM', '21:00:00' => '09:00 PM', '21:15:00' => '09:15 PM', '21:30:00' => '09:30 PM', '21:45:00' => '09:45 PM', '22:00:00' => '10:00 PM', '22:15:00' => '10:15 PM', '22:30:00' => '10:30 PM', '22:45:00' => '10:45 PM', '23:00:00' => '11:00 PM', '23:15:00' => '11:15 PM', '23:30:00' => '11:30 PM', '23:45:00' => '11:45 PM');

        if($this->uri->segment('4') == 'multiple'){

          $slotIdStr = $this->input->get('ids');
          $slotIds = explode(",", $slotIdStr);

          $this->outputData['slot_id'] = $slotIdStr;

        } else {
        if($this->uri->segment('4'))
        {
            $slotDetail = $this->user_model->getSlotById($this->uri->segment('4'));
            $this->outputData['slot_1_date'] = '';
            $this->outputData['slot_2_date'] = '';
            $this->outputData['slot_3_date'] = '';
            $this->outputData['slot_1_time'] = '';
            $this->outputData['slot_2_time'] = '';
            $this->outputData['slot_3_time'] = '';
            $this->outputData['timezone'] = isset($slotDetail[0]->timezone) ? $slotDetail[0]->timezone : $timezone;
            $localTimezone = 5.5 - (float)$this->outputData['timezone'];

            if($slotDetail[0]->slot_1)
            {
                $slotDetail[0]->slot_1 = date('Y-m-d H:i:s', strtotime($slotDetail[0]->slot_1) - $localTimezone * 60 * 60);

                $slot1 = explode(" ", $slotDetail[0]->slot_1);
                $this->outputData['slot_1_date'] = $slot1[0];
                $this->outputData['slot_1_time'] = $time_array[$slot1[1]];
            }
            if($slotDetail[0]->slot_2)
            {
                $slotDetail[0]->slot_2 = date('Y-m-d H:i:s', strtotime($slotDetail[0]->slot_2) - $localTimezone * 60 * 60);
                $slot2 = explode(" ", $slotDetail[0]->slot_2);
                $this->outputData['slot_2_date'] = $slot2[0];
                $this->outputData['slot_2_time'] = $time_array[$slot2[1]];
            }
            if($slotDetail[0]->slot_3)
            {
                $slotDetail[0]->slot_3 = date('Y-m-d H:i:s', strtotime($slotDetail[0]->slot_3) - $localTimezone * 60 * 60);
                $slot3 = explode(" ", $slotDetail[0]->slot_3);
                $this->outputData['slot_3_date'] = $slot3[0];
                $this->outputData['slot_3_time'] = $time_array[$slot3[1]];
            }
            $this->outputData['slot_id'] = $this->uri->segment('4');
        }
        else
        {
            $slotIds = $this->input->post('slot_ids');
            if(!$slotIds)
            {
                $this->session->set_flashdata('flash_message', "Please select one of the session to create slot");
                redirect('user/session/unassigned');
            }

            $slotIdsStr = '';
            foreach($slotIds as $slotId)
            {
                $slotIdsStr .= $slotId . ",";
            }
            $slotIdsStr = substr($slotIdsStr, 0, -1);
            $this->outputData['slot_id'] = $slotIdsStr;
        }
      }
        $this->render_page('templates/user/student_form', $this->outputData);
    }

    function edit()
    {
        $time_array = array('00:00:00', '00:15:00', '00:30:00', '00:45:00', '01:00:00', '01:15:00', '01:30:00', '01:45:00', '02:00:00', '02:15:00', '02:30:00', '02:45:00', '03:00:00', '03:15:00', '03:30:00', '03:45:00', '04:00:00', '04:15:00', '04:30:00', '04:45:00', '05:00:00', '05:15:00', '05:30:00', '05:45:00', '06:00:00', '06:15:00', '06:30:00', '06:45:00', '07:00:00', '07:15:00', '07:30:00', '07:45:00', '08:00:00', '08:15:00', '08:30:00', '08:45:00', '09:00:00', '09:15:00', '09:30:00', '09:45:00', '10:00:00', '10:15:00', '10:30:00', '10:45:00', '11:00:00', '11:15:00', '11:30:00', '11:45:00', '12:00:00', '12:15:00', '12:30:00', '12:45:00', '13:00:00', '13:15:00', '13:30:00', '13:45:00', '14:00:00', '14:15:00', '14:30:00', '14:45:00', '15:00:00', '15:15:00', '15:30:00', '15:45:00', '16:00:00', '16:15:00', '16:30:00', '16:45:00', '17:00:00', '17:15:00', '17:30:00', '17:45:00', '18:00:00', '18:15:00', '18:30:00', '18:45:00', '19:00:00', '19:15:00', '19:30:00', '19:45:00', '20:00:00', '20:15:00', '20:30:00', '20:45:00', '21:00:00', '21:15:00', '21:30:00', '21:45:00', '22:00:00', '22:15:00', '22:30:00', '22:45:00', '23:00:00', '23:15:00', '23:30:00', '23:45:00');

        $this->load->helper('cookie_helper');
        $this->load->model('user/user_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata)){  redirect('common/login'); }

        $slotIdStr = $this->input->post('slot_id');
        $slotIds = explode(",", $slotIdStr);
        $slug = $userdata['slug'];
        /*$data['slot_1_date'] = $this->input->post('slot_1_date');
        $data['slot_1_time'] = $this->input->post('slot_1_time');
        $data['slot_2_date'] = $this->input->post('slot_2_date');
        $data['slot_2_time'] = $this->input->post('slot_2_time');
        $data['slot_3_date'] = $this->input->post('slot_3_date');
        $data['slot_3_time'] = $this->input->post('slot_3_time');*/

        $istTimezone = 5.5 - (float)$this->input->post('timezone');
        $data['timezone'] = $this->input->post('timezone');

        foreach($slotIds as $slotId)
        {
          if($slotId == 0){
            continue;
          }
            $slot1 = '';
            $slot2 = '';
            $slot3 = '';

            if($this->input->post('slot_1_date'))
            {
                $data['slot_1'] = $this->input->post('slot_1_date') . " " . $time_array[$this->input->post('slot_1_time')];
                $data['slot_1'] = date('Y-m-d H:i:s', strtotime($data['slot_1']) + $istTimezone*60*60);
                $slot1 = $data['slot_1'];
            }
            if($this->input->post('slot_2_date'))
            {
                $data['slot_2'] = $this->input->post('slot_2_date') . " " . $time_array[$this->input->post('slot_2_time')];
                $data['slot_2'] = date('Y-m-d H:i:s', strtotime($data['slot_2']) + $istTimezone*60*60);
                $slot2 = $data['slot_2'];
            }
            if($this->input->post('slot_3_date'))
            {
                $data['slot_3'] = $this->input->post('slot_3_date') . " " . $time_array[$this->input->post('slot_3_time')];
                $data['slot_3'] = date('Y-m-d H:i:s', strtotime($data['slot_3']) + $istTimezone*60*60);
                $slot3 = $data['slot_3'];
            }

            $data['status'] = 'Scheduled';

            $this->user_model->editSlot($slotId, $data);

            $slotData = $this->user_model->getSlotById($slotId);
            $userEmail = $slotData[0]->email;
            $userName = $slotData[0]->name;
            $universityEmail = $userdata['email'];
            $universityName = $userdata['name'];
            $universityUrl = 'https://www.hellouni.org/' . $slug;

            $mailSubject = "Slots For $universityName";
            $mailTemplate = "Hi $userName,

                                $universityName has shared the following slots for the chat session with you.

                                &bull; $slot1 IST
                                &bull; $slot2 IST
                                &bull; $slot3 IST

                                Select one as per your availability <a href='$universityUrl'>$universityUrl</a>

                                <b>Thanks and Regards,
                                HelloUni Coordinator
                                +91 81049 09690</b>

                                <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

            $ccMailList = '';
            $mailAttachments = '';

            $sendMail = sMail($userEmail, $userName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);
        }

        redirect('/user/session/unconfirmed');
    }

    function delete()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/user_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $slotIdStr = $this->input->get('slot_id');
        $slotIds = explode(",", $slotIdStr);

        foreach($slotIds as $slotId)
        {
            $data['status'] = 'Deleted';
            $this->user_model->editSlot($slotId, $data);
        }

        $redirectUrl = $this->input->get('redirectUrl');
        redirect($redirectUrl);
    }

    function slotform()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/user_model');
        $this->load->model('user/university_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata)){  redirect('common/login'); }

        //$universityDetail = $this->university_model->getUniversityById($userdata['university_id']);
        //$timezone = $universityDetail[0]['timezone'];

        if(isset($userdata['university_id']))
        {
            $universityDetail = $this->university_model->getUniversityById($userdata['university_id']);
            $timezone = isset($universityDetail[0]['timezone']) ? $universityDetail[0]['timezone'] : '';
            $universityList[] = [ "name"=>$userdata['name'], "id"=>$userdata['university_id'] ];
        }
        else
        {
            $timezone = 5.5;
            $universityList = $this->university_model->getAllUniversitiesActive();
        }

        $this->outputData['time_array'] = array('12 AM', '12:15 AM', '12:30 AM', '12:45 AM', '01:00 AM', '01:15 AM', '01:30 AM', '01:45 AM', '02:00 AM', '02:15 AM', '02:30 AM', '02:45 AM', '03:00 AM', '03:15 AM', '03:30 AM', '03:45 AM', '04:00 AM', '04:15 AM', '04:30 AM', '04:45 AM', '05:00 AM', '05:15 AM', '05:30 AM', '05:45 AM', '06:00 AM', '06:15 AM', '06:30 AM', '06:45 AM', '07:00 AM', '07:15 AM', '07:30 AM', '07:45 AM', '08:00 AM', '08:15 AM', '08:30 AM', '08:45 AM', '09:00 AM', '09:15 AM', '09:30 AM', '09:45 AM', '10:00 AM', '10:15 AM', '10:30 AM', '10:45 AM', '11:00 AM', '11:15 AM', '11:30 AM', '11:45 AM', '12:00 PM', '12:15 PM', '12:30 PM', '12:45 PM', '01:00 PM', '01:15 PM', '01:30 PM', '01:45 PM', '02:00 PM', '02:15 PM', '02:30 PM', '02:45 PM', '03:00 PM', '03:15 PM', '03:30 PM', '03:45 PM', '04:00 PM', '04:15 PM', '04:30 PM', '04:45 PM', '05:00 PM', '05:15 PM', '05:30 PM', '05:45 PM', '06:00 PM', '06:15 PM', '06:30 PM', '06:45 PM', '07:00 PM', '07:15 PM', '07:30 PM', '07:45 PM', '08:00 PM', '08:15 PM', '08:30 PM', '08:45 PM', '09:00 PM', '09:15 PM', '09:30 PM', '09:45 PM', '10:00 PM', '10:15 PM', '10:30 PM', '10:45 PM', '11:00 PM', '11:15 PM', '11:30 PM', '11:45 PM');

        $time_array = array('00:00:00' => '12 AM', '00:15:00' => '12:15 AM', '00:30:00' => '12:30 AM', '00:45:00' => '12:45 AM', '01:00:00' => '01:00 AM', '01:15:00' => '01:15 AM', '01:30:00' => '01:30 AM', '01:45:00' => '01:45 AM', '02:00:00' => '02:00 AM', '02:15:00' => '02:15 AM', '02:30:00' => '02:30 AM', '02:45:00' => '02:45 AM', '03:00:00' => '03:00 AM', '03:15:00' => '03:15 AM', '03:30:00' => '03:30 AM', '03:45:00' => '03:45 AM', '04:00:00' => '04:00 AM', '04:15:00' => '04:15 AM', '04:30:00' => '04:30 AM', '04:45:00' => '04:45 AM', '05:00:00' => '05:00 AM', '05:15:00' => '05:15 AM', '05:30:00' => '05:30 AM', '05:45:00' => '05:45 AM', '06:00:00' => '06:00 AM', '06:15:00' => '06:15 AM', '06:30:00' => '06:30 AM', '06:45:00' => '06:45 AM', '07:00:00' => '07:00 AM', '07:15:00' => '07:15 AM', '07:30:00' => '07:30 AM', '07:45:00' => '07:45 AM', '08:00:00' => '08:00 AM', '08:15:00' => '08:15 AM', '08:30:00' => '08:30 AM', '08:45:00' => '08:45 AM', '09:00:00' => '09:00 AM', '09:15:00' => '09:15 AM', '09:30:00' => '09:30 AM', '09:45:00' => '09:45 AM', '10:00:00' => '10:00 AM', '10:15:00' => '10:15 AM', '10:30:00' => '10:30 AM', '10:45:00' => '10:45 AM', '11:00:00' => '11:00 AM', '11:15:00' => '11:15 AM', '11:30:00' => '11:30 AM', '11:45:00' => '11:45 AM', '12:00:00' => '12:00 PM', '12:15:00' => '12:15 PM', '12:30:00' => '12:30 PM', '12:45:00' => '12:45 PM', '13:00:00' => '01:00 PM', '13:15:00' => '01:15 PM', '13:30:00' => '01:30 PM', '13:45:00' => '01:45 PM', '14:00:00' => '02:00 PM', '14:15:00' => '02:15 PM', '14:30:00' => '02:30 PM', '14:45:00' => '02:45 PM', '15:00:00' => '03:00 PM', '15:15:00' => '03:15 PM', '15:30:00' => '03:30 PM', '15:45:00' => '03:45 PM', '16:00:00' => '04:00 PM', '16:15:00' => '04:15 PM', '16:30:00' => '04:30 PM', '16:45:00' => '04:45 PM', '17:00:00' => '05:00 PM', '17:15:00' => '05:15 PM', '17:30:00' => '05:30 PM', '17:45:00' => '05:45 PM', '18:00:00' => '06:00 PM', '18:15:00' => '06:15 PM', '18:30:00' => '06:30 PM', '18:45:00' => '06:45 PM', '19:00:00' => '07:00 PM', '19:15:00' => '07:15 PM', '19:30:00' => '07:30 PM', '19:45:00' => '07:45 PM', '20:00:00' => '08:00 PM', '20:15:00' => '08:15 PM', '20:30:00' => '08:30 PM', '20:45:00' => '08:45 PM', '21:00:00' => '09:00 PM', '21:15:00' => '09:15 PM', '21:30:00' => '09:30 PM', '21:45:00' => '09:45 PM', '22:00:00' => '10:00 PM', '22:15:00' => '10:15 PM', '22:30:00' => '10:30 PM', '22:45:00' => '10:45 PM', '23:00:00' => '11:00 PM', '23:15:00' => '11:15 PM', '23:30:00' => '11:30 PM', '23:45:00' => '11:45 PM');

        $this->outputData['university_list'] = $universityList;
        $this->render_page('templates/user/predefined_slot_form', $this->outputData);
    }

    function predefineslotcreate(){

      $this->load->helper('cookie_helper');
      $this->load->model('user/user_model');
      $this->load->model('webinar/webinar_model');
      $userdata=$this->session->userdata('user');
      if(empty($userdata)){  redirect('common/login'); }

      $time_array = array('00:00:00', '00:15:00', '00:30:00', '00:45:00', '01:00:00', '01:15:00', '01:30:00', '01:45:00', '02:00:00', '02:15:00', '02:30:00', '02:45:00', '03:00:00', '03:15:00', '03:30:00', '03:45:00', '04:00:00', '04:15:00', '04:30:00', '04:45:00', '05:00:00', '05:15:00', '05:30:00', '05:45:00', '06:00:00', '06:15:00', '06:30:00', '06:45:00', '07:00:00', '07:15:00', '07:30:00', '07:45:00', '08:00:00', '08:15:00', '08:30:00', '08:45:00', '09:00:00', '09:15:00', '09:30:00', '09:45:00', '10:00:00', '10:15:00', '10:30:00', '10:45:00', '11:00:00', '11:15:00', '11:30:00', '11:45:00', '12:00:00', '12:15:00', '12:30:00', '12:45:00', '13:00:00', '13:15:00', '13:30:00', '13:45:00', '14:00:00', '14:15:00', '14:30:00', '14:45:00', '15:00:00', '15:15:00', '15:30:00', '15:45:00', '16:00:00', '16:15:00', '16:30:00', '16:45:00', '17:00:00', '17:15:00', '17:30:00', '17:45:00', '18:00:00', '18:15:00', '18:30:00', '18:45:00', '19:00:00', '19:15:00', '19:30:00', '19:45:00', '20:00:00', '20:15:00', '20:30:00', '20:45:00', '21:00:00', '21:15:00', '21:30:00', '21:45:00', '22:00:00', '22:15:00', '22:30:00', '22:45:00', '23:00:00', '23:15:00', '23:30:00', '23:45:00');

      $data['university_id'] = $this->input->post('university_id');

      $istTimezone = 5.5 - (float)$this->input->post('timezone');
      $data['timezone'] = $this->input->post('timezone');

      if($this->input->post('slot_1_date'))
      {
          $data['slot_1'] = $this->input->post('slot_1_date') . " " . $time_array[$this->input->post('slot_1_time')];
          $data['slot_1'] = date('Y-m-d H:i:s', strtotime($data['slot_1']) + $istTimezone*60*60);
      }
      if($this->input->post('slot_2_date'))
      {
          $data['slot_2'] = $this->input->post('slot_2_date') . " " . $time_array[$this->input->post('slot_2_time')];
          $data['slot_2'] = date('Y-m-d H:i:s', strtotime($data['slot_2']) + $istTimezone*60*60);
      }
      if($this->input->post('slot_3_date'))
      {
          $data['slot_3'] = $this->input->post('slot_3_date') . " " . $time_array[$this->input->post('slot_3_time')];
          $data['slot_3'] = date('Y-m-d H:i:s', strtotime($data['slot_3']) + $istTimezone*60*60);
      }

      //$data['status'] = '1';
      $data['slots_created_by'] = $userdata['id'];

      $slotAdd = $this->webinar_model->post($data,'predefine_slots');

      redirect('/user/session/predefineslots');

    }

    function predefineslotdelete(){

      $this->load->helper('cookie_helper');
      $this->load->model('user/user_model');
      $this->load->model('webinar/webinar_model');
      $userdata=$this->session->userdata('user');
      if(empty($userdata)){  redirect('common/login'); }

      $predefineSlotId = $this->uri->segment('4');

      $whereCondition['id'] = $predefineSlotId;
      $webinarData['status'] = '0';

      $deletePreDefineSlotsId = $this->webinar_model->put($whereCondition, $webinarData, 'predefine_slots');

      redirect('/user/session/predefineslots');

    }

    function generate_url()
    {
        $this->load->model('user/user_model');

        $participants = array(
            'slot_id' => $_GET['id'],
            'user_id' => $_GET['user_id'],
            'university_id' => $_GET['university_id']
        );
        $json_participant = json_encode($participants);

        $tokenDetail = $this->tokbox_model->getTokenByParticipant($json_participant);

        if($tokenDetail)
        {
            if($tokenDetail->session_id)
            {
                $opentok = new OpenTok('46220952', '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2');
                $sessionId = $tokenDetail->session_id;
                $tokenId = $tokenDetail->id;
                $token = $opentok->generateToken($sessionId);
                $tokbox_data['token'] = $token;
                $currentTime = date('Y-m-d H:i:s');
                $expiry = strtotime($currentTime) + 3600;
                $expiryTime = date('Y-m-d H:i:s', $expiry);
                $tokbox_data['expiry_time'] = $expiryTime;
                $this->tokbox_model->editToken($tokbox_data, $tokenId);

                $studentId = $_GET['user_id'];
                $universityLoginId = $_GET['university_login_id'];

                $condition = array('user_master.id' => $studentId);
                $studentDetail = $this->user_model->getUserByid($condition);

                $condition = array('user_master.id' => $universityLoginId);
                $universityDetail = $this->user_model->getUserByid($condition);

                $universityEmail = $universityDetail->email;
                $universityName = $universityDetail->name;

                $studentName = $studentDetail->name;
                $studentEmail = $studentDetail->email;

                /*echo "Student Mail - " . $studentEmail . "\n";
                echo "University Mail - " . $universityEmail . "\n";

                $studentEmail = 'nikhil.gupta@issc.in';
                $universityEmail = 'nikhil.gupta@blocmatrix.com';*/

                $ccMailList = '';
                $mailAttachments = '';

                $encodedTokboxId = base64_encode($tokenId);
                $encodedUniversityId = base64_encode($universityLoginId);

                $mailSubject = "Conference URL For One To One With $universityName";
                $mailTemplate = "Hi $studentName

                                    Please click on the following URL to join the conference with $universityName

                                    <a href='" . str_replace('/admin', '', base_url()) . "/meeting/oneonone?id=" . $encodedTokboxId . "'>" . str_replace('/admin', '', base_url()) . "/meeting/oneonone?id=" . $encodedTokboxId . "</a>

                                    <b>Thanks and Regards,
                                    HelloUni Coordinator
                                    +91 81049 09690</b>

                                    <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

                $sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

                $mailSubject = "Conference URL For One To One With $studentName";
                $mailTemplate = "Hi $universityName,

                                Please click on the following URL to join the conference with $studentName

                                <a href='" . str_replace('/admin', '', base_url()) . "/meeting/oneonone/panelist?id=" . $encodedTokboxId . "'>" . str_replace('/admin', '', base_url()) . "/meeting/oneonone/panelist?id=" . $encodedTokboxId . "</a>

                                <b>Thanks and Regards,
                                HelloUni Coordinator
                                +91 81049 09690</b>

                                <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

                $sendMail = sMail($universityEmail, $universityName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

                echo '<script type="text/javascript">alert("Conference url has been generated successfully."); window.location.href="/user/session/coming"</script>';
            }
        }
    }

    function pdf()
    {
        $slot_id = $this->input->get('sid');
        $slot_detail = $this->user_model->getSlotById($slot_id);
        if(!$slot_detail)
        {
            redirect('/');
        }
        $universityId = $slot_detail[0]->university_id;
        $consentData = $this->user_model->getUniversityConsentByid($universityId);
        $consent = $consentData->consent_data;

        $studentName = $slot_detail[0]->name;
        $date = date('Y-m-d', strtotime($slot_detail[0]->date_created));
        $universityName = $consentData->name;
        $countryName = $consentData->country_name;
        $sessionType = 'One On One Session';
        $studentDOB = "1985-09-09";
        $passport = "P1234B";
        $resident = "Hasmathpet";
        $program = "Compuer Science";
        $intake = "Jan-Jul";
        $studentEmail = "nik@abc.com";
        $studentMobile = "923123123";

        for($i = strlen($studentName); $i < 80; $i++){
            $studentName .= '&nbsp;';
        }
        for($i = strlen($studentDOB); $i < 40; $i++){
            $studentDOB .= '&nbsp;';
        }
        for($i = strlen($passport); $i < 65; $i++){
            $passport .= '&nbsp;';
        }
        for($i = strlen($resident); $i < 50; $i++){
            $resident .= '&nbsp;';
        }
        for($i = strlen($program); $i < 70; $i++){
            $program .= '&nbsp;';
        }
        for($i = strlen($intake); $i < 50; $i++){
            $intake .= '&nbsp;';
        }
        for($i = strlen($universityName); $i < 70; $i++){
            $universityName .= '&nbsp;';
        }
        for($i = strlen($studentEmail); $i < 70; $i++){
            $studentEmail .= '&nbsp;';
        }
        for($i = strlen($studentMobile); $i < 70; $i++){
            $studentMobile .= '&nbsp;';
        }
        for($i = strlen($date); $i < 40; $i++){
            $date .= '&nbsp;';
        }

        $path = APPPATH . '/../b46e9c18-c34b-11ea-8b25-0cc47a792c0a_id_b46e9c18-c34b-11ea-8b25-0cc47a792c0a_files/background2.jpg';
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $imageData = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($imageData);
        $backgroundImage = '<img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $base64) . '">';

        $img = '<img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $slot_detail[0]->student_signature) . '">';

        $consent = str_replace('<img src="b46e9c18-c34b-11ea-8b25-0cc47a792c0a_id_b46e9c18-c34b-11ea-8b25-0cc47a792c0a_files/background2.jpg" style="width:110%">', $backgroundImage, $consent);
        $consent = str_replace('<span id="student_name_1"></span>', '<span id="student_name_1"><u>' . $studentName . '</u></span>', $consent);
        $consent = str_replace('<span id="student_name_2"></span>', '<span id="student_name_2"><u>' . $studentName . '</u></span>', $consent);
        $consent = str_replace('<span id="dob"></span>', '<span id="dob"><u>' . $studentDOB . '</u></span>', $consent);
        $consent = str_replace('<span id="passport"></span>', '<span id="passport"><u>' . $passport . '</u></span>', $consent);
        $consent = str_replace('<span id="resident"></span>', '<span id="resident"><u>' . $resident . '</u></span>', $consent);
        $consent = str_replace('<span id="program"></span>', '<span id="program"><u>' . $program . '</u></span>', $consent);
        $consent = str_replace('<span id="intake"></span>', '<span id="intake"><u>' . $intake . '</u></span>', $consent);
        $consent = str_replace('<span id="university"></span>', '<span id="university"><u>' . $universityName . '</u></span>', $consent);
        $consent = str_replace('<span id="email"></span>', '<span id="email"><u>' . $studentEmail . '</u></span>', $consent);
        $consent = str_replace('<span id="mobile"></span>', '<span id="mobile"><u>' . $studentMobile . '</u></span>', $consent);
        $consent = str_replace('<span id="date"></span>', '<span id="date"><u>' . $date . '</u></span>', $consent);

        $consent = str_replace('<div style="position:absolute;left:11.80px;top:645.66px" class="cls_005"><span class="cls_005"><div id="signature-box" class="wrapper"><textarea class="form-control" rows="4" cols="50" name="comment" disabled>Your signature here...</textarea><canvas id="signature-pad" class="signature-pad" width=325 height=75></canvas></div></span></div>', '<div style="position:absolute;left:11.80px;top:645.66px" class="cls_005"><span class="cls_005">' . $img . '</span></div>', $consent);
        $consent = str_replace('<div style="position:absolute;left:415.80px;top:645.66px" class="cls_005"><span class="cls_005"><span class="cls_005"><b> OR </b></span></span></div>', '', $consent);
        $consent = str_replace('<div style="position:absolute;left:600px;top:645.66px" class="cls_005"><span class="cls_005"><div id="upload-signature-box" class="wrapper"><form name="upload-signature" enctype="multipart/form-data" method="post"><input type="file" name="signature" id="upload-signature"><input type="submit" value="Upload Signature" name="submit" style="margin-top: 10px;"></form></div></span></div>', '', $consent);
        $this->load->helper('pdf_helper');

        $data['studentName'] = $studentName;
        $data['dob'] = $studentDOB;
        $data['passport'] = $passport;
        $data['resident'] = $resident;
        $data['program'] = $program;
        $data['intake'] = $intake;
        $data['email'] = $studentEmail;
        $data['mobile'] = $studentMobile;
        $data['universityName'] = $universityName;
        $data['date'] = $date;

        $data['signature'] = $slot_detail[0]->student_signature;
        $data['content'] = $consent;

        $this->load->view('templates/user/consent', $data);
    }

    function recording()
    {
        $archieveId = $this->input->get('aid');
        $bucketUrl = "https://hellouni.s3.ap-south-1.amazonaws.com";
        $tokboxPartnerId = "46220952";
        $file = "archive.mp4";
        $this->outputData['source_url'] = $bucketUrl . "/" . $tokboxPartnerId . "/" . $archieveId . "/" . $file;
        $this->render_page('templates/user/recording', $this->outputData);
    }
}
