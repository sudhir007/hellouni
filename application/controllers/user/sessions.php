<?php
class Sessions extends MY_Controller
{
    public $outputData;

	public $loggedInUser;

    function __construct()
    {
        parent::__construct();

		$this->load->model(array('user/user_model'));
        $this->load->helper(array('cookie_helper'));
        track_user();
    }

	function index()
    {
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('user/account');
        }

        $userId = $userdata['id'];

        $mySessions = $this->user_model->getMySessions($userId);
        $myFavourites = $this->user_model->getFavourites(array('favourites.user_id' => $userId));
        $myAppliedUniversities = $this->user_model->getAppliedUniversitiesByUser($userId);
        $appliedUniversities = array();
        if($myAppliedUniversities)
        {
            foreach($myAppliedUniversities as $index => $appliedUniversity)
            {
                $appliedUniversities[] = $appliedUniversity['university_id'];
                switch($appliedUniversity['status'])
                {
                    case 'In Process':
                        $myAppliedUniversities[$index]['action'] = "Submit Application Form";
                        break;
                    case 'Submitted':
                        $myAppliedUniversities[$index]['action'] = "Awaiting Decision";
                        break;
                    case 'Admit Received':
                        $myAppliedUniversities[$index]['action'] = "Submit Finance Documentation";
                        break;
                }
                $myAppliedUniversities[$index]['view_application'] = '<form method="get" action="/user/sessions/applications">
                                                                 <div>
                                                                      <input type="submit" class="btn btn-default" name="submit" value="Check Application">
                                                                 </div>
                                                                 <input type="hidden" name="uid" value="' . $appliedUniversity['university_id'] . '">
                                                                 <input type="hidden" name="status" value="' . $appliedUniversity['status'] . '">
                                                                 <input type="hidden" name="tab" value="application_form">
                                                             </form>';
            }
        }
        if($myFavourites)
        {
            foreach($myFavourites as $index => $myFavourite)
            {
                $myFavourite['university_id'] = $myFavourite['uni_id'];
                $myFavourite['status'] = 'Shortlisted';
                $myFavourite['confirmed_slot'] = '';
                $myFavourite['action'] = '<form method="post" action="/university/details/connect">
                                                     <div style="text-align:center;">
                                                          <input type="submit" class="btn btn-default" name="submit" style="margin:10px auto;" value="Book Session">
                                                     </div>
                                                     <input type="hidden" name="university_id" value="' . $myFavourite['university_id'] . '">
                                                     <input type="hidden" name="university_login_id" value="' . $myFavourite['university_login_id'] . '">
                                                     <input type="hidden" name="slug" value="' . $myFavourite['slug'] . '">
                                                 </form>';
                $myFavourites[$index] = $myFavourite;
            }
        }
        /*echo '<pre>';
        echo $userId;
        print_r($mySessions);
        print_r($myFavourites);
        exit;*/
        if($mySessions)
        {
            $traversedUniversities = array();
            foreach($mySessions as $index => $sessionArray)
            {
                if(!in_array($sessionArray['university_id'], $traversedUniversities))
                {
                    $key = array_search($sessionArray['university_id'], array_column($myFavourites, 'university_id'));
                    if($key !== FALSE)
                    {
                        array_splice($myFavourites, $key, 1);
                    }
                    $traversedUniversities[] = $sessionArray['university_id'];
                    $mySessions[$index]['action'] = '';

                    switch($sessionArray['status'])
                    {
                        case 'Attained':
                            if(!in_array($sessionArray['university_id'], $appliedUniversities))
                            {
                                $mySessions[$index]['action'] = '<form method="get" action="/university/details/application">
                   				                                 <div style="text-align:center;">
                   		         	                                  <input type="submit" class="btn btn-default" name="submit" style="margin:10px auto;" value="Start Documentation">
                   		                                         </div>
                   				                                 <input type="hidden" name="uid" value="' . $sessionArray['university_id'] . '">
               	                                             </form>
                                                             <form method="post" action="/university/details/connect">
                				                                 <div style="text-align:center;">
                		         	                                  <input type="submit" class="btn btn-default" name="submit" style="margin:10px auto;" value="Connect Again">
                		                                         </div>
                				                                 <input type="hidden" name="university_id" value="' . $sessionArray['university_id'] . '">
                				                                 <input type="hidden" name="university_login_id" value="' . $sessionArray['university_login_id'] . '">
                                                                 <input type="hidden" name="slug" value="' . $sessionArray['slug'] . '">
            	                                             </form>';
                            }
                            else
                            {
                                unset($mySessions[$index]);
                            }
                            break;
                        case 'Rejected':
                            $mySessions[$index]['action'] = '<form method="post" action="/university/details/connect">
                   				                                 <div style="text-align:center;">
                   		         	                                  <input type="submit" class="btn btn-default" name="submit" style="margin:10px auto;" value="Connect Again">
                   		                                         </div>
                   				                                 <input type="hidden" name="university_id" value="' . $sessionArray['university_id'] . '">
                   				                                 <input type="hidden" name="university_login_id" value="' . $sessionArray['university_login_id'] . '">
                                                                 <input type="hidden" name="slug" value="' . $sessionArray['slug'] . '">
               	                                             </form>';
                            break;
                        case 'Missed':
                            $mySessions[$index]['action'] = '<form method="post" action="/university/details/connect">
                   				                                 <div style="text-align:center;">
                   		         	                                  <input type="submit" class="btn btn-default" name="submit" style="margin:10px auto;" value="Connect Again">
                   		                                         </div>
                   				                                 <input type="hidden" name="university_id" value="' . $sessionArray['university_id'] . '">
                   				                                 <input type="hidden" name="university_login_id" value="' . $sessionArray['university_login_id'] . '">
                                                                 <input type="hidden" name="slug" value="' . $sessionArray['slug'] . '">
               	                                             </form>';
                            break;
                        case 'Requested':
                            $mySessions[$index]['action'] = "Wait For Slots";
                            break;
                        case 'Scheduled':
                            $mySessions[$index]['action'] = "<a href='/" . $sessionArray['slug'] . "' target='_blank' style='color:#337ab7;'>Pick A Slot</a>";
                            break;
                    }
                }
                else
                {
                    unset($mySessions[$index]);
                }
            }
        }
        $mySessions = array_merge($myFavourites, $mySessions);
        $this->outputData['my_sessions'] = $mySessions;
        $this->outputData['my_applied_universities'] = $myAppliedUniversities;
        $this->render_page('templates/user/sessions',$this->outputData);
    }

    function applications()
    {
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('user/account');
        }

        $userId = $userdata['id'];
        if(!isset($_GET['uid']))
        {
            redirect('search/university');
        }
        $universityId = $_GET['uid'];
        $appliedColleges = $this->user_model->getAppliedColleges($userId, $universityId);
        if($appliedColleges)
        {
            foreach($appliedColleges as $index => $appliedCollege)
            {
                switch($appliedCollege['status'])
                {
                    case 'In Process':
                        $appliedColleges[$index]['action'] = "Submit Application Form";
                        break;
                    case 'Submitted':
                        $appliedColleges[$index]['action'] = "Awaiting Decision";
                        break;
                    case 'Admit Received':
                        $appliedColleges[$index]['action'] = "Wait For I20";
                        break;
                    case 'I 20 Received':
                        $appliedColleges[$index]['action'] = '<input type="button" class="btn btn-default" name="submit" value="Start Filling VISA Form" onClick="showModal(' . $appliedCollege['id'] . ', ' . $appliedCollege['university_id'] . ', \'' . $appliedCollege['status'] . '\')">';
                        break;
                    case 'VISA In Process':
                        $appliedColleges[$index]['action'] = "Wait For VISA Date Booking";
                        break;
                    case 'VISA Date Booked':
                        $appliedColleges[$index]['action'] = "Wait For VISA Approval";
                        break;
                    case 'VISA Approved':
                        $appliedColleges[$index]['action'] = "Congratulation!! Pack Your Bags Now!!";
                        break;
                }

                $appliedColleges[$index]['view_application'] = '<form method="get" action="/user/account/update">
                                                                 <div>
                                                                      <input type="submit" class="btn btn-default" name="submit" value="View Application">
                                                                 </div>
                                                                 <input type="hidden" name="aid" value="' . $appliedCollege['id'] . '">
                                                                 <input type="hidden" name="uid" value="' . $appliedCollege['university_id'] . '">
                                                                 <input type="hidden" name="status" value="' . $appliedCollege['status'] . '">
                                                                 <input type="hidden" name="tab" value="application_form">
                                                             </form>';
            }
        }
        $this->outputData['applied_colleges'] = $appliedColleges;
        $this->outputData['university_id'] = $universityId;
        $this->render_page('templates/user/applications',$this->outputData);
    }
}
?>
