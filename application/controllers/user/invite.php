<?php
require_once APPPATH . 'libraries/Mail/sMail.php';
class Invite extends MY_Controller
{
    public $outputData;

    function __construct()
    {
        parent::__construct();
        $this->load->model('user/user_model');
        track_user();
    }

    function index()
    {
        $this->load->helper('cookie_helper');
        $encryptedUsername = $this->input->get('u');
        if(!$encryptedUsername)
        {
            redirect('search/university');
        }
        $username = base64_decode($encryptedUsername);
        $userDetail = $this->user_model->getUserMaster($username);
        if(!$userDetail)
        {
            redirect('search/university');
        }
        $this->outputData['encryptedUsername'] = $encryptedUsername;
        $this->outputData['invited'] = 0;

        $this->render_page('templates/user/invite', $this->outputData);
    }

    function send()
    {
        $encryptedUsername = $this->input->post('euname');
        $username = base64_decode($encryptedUsername);
        $userDetail = $this->user_model->getUserMaster($username);
        if(!$userDetail)
        {
            redirect('search/university');
        }
        $userId = $userDetail[0]['id'];
        $allEmails = $this->input->post('friends_email');
        $allEmails = explode(',', $allEmails);
        foreach($allEmails as $email)
        {
            $email = trim($email);
            if(filter_var($email, FILTER_VALIDATE_EMAIL))
            {
                $insertData['user_id'] = $userId;
                $insertData['friend_email'] = $email;
                $this->user_model->addInvitations($insertData);

                $currentYear = date("Y");
                $mailSubject = "You are invited to Join the Imperial Virtual Education Fair $currentYear!";
                $mailTemplate = "Hello,

                                I would like to invite you to join the Imperial Virtual Education Fair $currentYear!

                                Interact with Universities from Across the Globe! Have all your doubts, queries and concerns resolved, your profile evaluated and arrive at an informed decision regarding your dreams to study abroad!

                                To join, please register using the link below:

                                <a href='https://www.hellouni.org/university/virtualfair/'>https://www.hellouni.org/university/virtualfair/</a>

                                <b><u>Virtual Fair Schedule</u>:</b>

                                <b><u>Date</u>:</b> Friday, 25 September 2020
                                <b><u>Time</u>:</b> 5:00 PM - 11:00 PM (IST)

                                Regards,
                                Team HelloUni
                                Tel: +91 81049 09690
                                Email: info@hellouni.org

                                <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

                $ccMailList = '';
                $mailAttachments = '';

                $sendMail = sMail($email, $email, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);
            }
        }

        $this->outputData['invited'] = 1;
        $this->render_page('templates/user/invite', $this->outputData);
    }
}
?>
