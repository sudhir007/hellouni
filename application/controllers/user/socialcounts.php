<?php

class Socialcounts extends MY_Controller {

	//Global variable

    public $outputData;		//Holds the output data for each view
	  public $loggedInUser;


	 function __construct(){

        parent::__construct();

		$this->load->model('user/user_model');
		$this->load->model('user/auth_model');

        track_user();

    }


	function likesAdd(){

	   $this->load->helper(array('form', 'url'));
	   $this->load->library('form_validation');

	   $this->load->model('course/course_model');
	   $this->load->model('user/auth_model');

	   $leaddata = $this->session->userdata('leaduser');
	   $userdata=$this->session->userdata('user');
	   if(empty($userdata)){
       echo "login";
       //redirect('user/account', 'refresh');

     } else {

       $entity_id = $this->uri->segment('4');
       $entity_type = $this->uri->segment('5');
       $user_id = $userdata['id'] ? $userdata['id'] : 0;

       $conditionArray = [
         'user_id' => $user_id,
         'entity_id' => $entity_id,
         'entity_type' => $entity_type
       ];

	   $query = $this->user_model->commonCheck($conditionArray,'likes_detail');

     if(count($query)<1){
			$data = array(
				'user_id' 		=>  $user_id,
        'entity_id' => $entity_id,
        'entity_type' => $entity_type
			);

			$lastInsertId = $this->user_model->insertCommon($data,'likes_detail');
      echo $lastInsertId;
		 }
   }
}

  function favAdd(){

	   $this->load->helper(array('form', 'url'));
	   $this->load->library('form_validation');

	   $this->load->model('course/course_model');
	   $this->load->model('user/auth_model');

	   //$leaddata = $this->session->userdata('leaduser');
	   $userdata=$this->session->userdata('user');
	   if(empty($userdata)){
       echo "login";
       //redirect('user/account', 'refresh');

     } else {

       $entity_id = $this->uri->segment('4');
       $entity_type = $this->uri->segment('5');
       $user_id = $userdata['id'];

       $conditionArray = [
         'user_id' => $user_id,
         'entity_id' => $entity_id,
         'entity_type' => $entity_type
       ];


	   $query = $this->user_model->commonCheck($conditionArray,'favourites_detail');

     if(count($query)<1){
			$data = array(
				'user_id' 		=>  $user_id,
        'entity_id' => $entity_id,
        'entity_type' => $entity_type
			);

			$lastInsertId = $this->user_model->insertCommon($data,'favourites_detail');
      echo $lastInsertId;
		 }
   }
	}


	function favDelete() {

       $this->load->helper('cookie_helper');
			 $this->load->model('user/auth_model');
			 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){
         echo "login";
       } else {

         if($this->uri->segment('4')){

           $condition = [
             "id" => $this->uri->segment('4')
           ];

  			 if($this->user_model->deleteCommon($condition,'favourites_detail'))
            echo "success";
  			 }

       }
   }

}

?>
