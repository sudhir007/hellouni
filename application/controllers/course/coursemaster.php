<?php

require APPPATH . "vendor/autoload.php";

use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;

require_once APPPATH . 'libraries/Mail/sMail.php';

class Coursemaster extends MY_Controller {

      public $outputData;   //Holds the output data for each view
      public $loggedInUser;


   function __construct()

    {

        parent::__construct();

    $this->load->library('template');

    $this->lang->load('enduser/home', $this->config->item('language_code'));

    $this->load->library('form_validation');

    $this->load->model('location/country_model');

    $this->load->model('webinar/webinar_model');

    $this->load->model('webinar/common_model');

    $this->load->model('course/course_model');

    $this->load->model('course/degree_model');

    $this->load->model('user/user_model');

      //  track_user();


    }

    function courselist(){

      $this->load->helper(array('form', 'url'));
      $userdata=$this->session->userdata('user');
      if(empty($userdata)){
        redirect('user/account');
      }

      $courseId = $this->uri->segment('4');

      $courseObj = $this->course_model->getCourseByIdNew($courseId);

      if(!$courseObj)
      {
          redirect('/');
      }

      $uId = $userdata['id'] ? $userdata['id'] : 0;

      $dataObj = [
        "user_id" => $uId,
        "entity_id" => $courseId,
        "entity_type" => "courses"
      ];

      //$insertObj = $this->common_model->post($dataObj,"unique_count");
      $insertObj = $this->common_model->post($dataObj,"views_detail");

      $courseDetail = $this->course_model->courseDetail($courseId);
      $relatedCourse = $this->course_model->relatedCourseDetail($courseDetail[0]['id'],$courseDetail[0]['department_id']);
      //var_dump($relatedCourse);die();
      if($courseDetail[0]['admission_requirements'])
      {
          $seperator = ['1.', '2.', '3.', '4.', '5.', '6.', '7.', '8.', '9.', '10.', '11.', '12.', '13.', '14.', '15.'];
          $result = str_replace($seperator, "---", $courseDetail[0]['admission_requirements']);
          $courseDetail[0]['admission_requirements'] = explode('---', $result);
      }

      $this->outputData['course_detail'] = $courseDetail[0];
      $this->outputData['related_courses'] = $relatedCourse;
      $this->outputData['consent'] = $courseDetail[0]['uni_consent_data'];

      $this->outputData['userLoggedIn'] = false;
     //$userdata=$this->session->userdata('user');
     if($userdata){
         $this->outputData['userLoggedIn'] = true;
         $alreadyConnected = $this->user_model->getSlotByUserAndUniversity($userdata['id'], $courseDetail[0]['uni_id']);
         if($alreadyConnected){
             $alreadyConnected->slot_1_show = $alreadyConnected->slot_1 ? date('d-M-Y h:i A', strtotime($alreadyConnected->slot_1)) : NULL;
             $alreadyConnected->slot_2_show = $alreadyConnected->slot_2 ? date('d-M-Y h:i A', strtotime($alreadyConnected->slot_2)) : NULL;
             $alreadyConnected->slot_3_show = $alreadyConnected->slot_3 ? date('d-M-Y h:i A', strtotime($alreadyConnected->slot_3)) : NULL;
             $this->outputData['alreadyConnected'] = $alreadyConnected;
             $this->outputData['show_url'] = false;

             if(!$this->outputData['applied'])
             {
                 $this->load->model('tokbox_model');
                 $participants = array("user_id" => $uId, "slot_id" => $alreadyConnected->id);
                 $json_participant = json_encode($participants);
                 $token = $this->tokbox_model->getTokenByParticipant($json_participant);
                 if($token && $token->token)
                 {
                     $encodedTokboxId = base64_encode($token->id);
                     $this->outputData['tokbox_url'] = '/tokbox?id=' . $encodedTokboxId;

                     if($token->expiry_time)
                     {
                         $expiryTime = $token->expiry_time;
                         $currentTime = date('Y-m-d H:i:s');

                         $time1 = strtotime($expiryTime);
                         $time2 = strtotime($currentTime);

                         if($expiryTime && $time2 < $time1){
                             $this->outputData['show_url'] = true;
                         }
                     }
                 }
             }
         }

         $condition = array('user_master.id'=>$userdata['id'],'user_master.status'=>1,'user_master.type'=>5);

         $userDetails = $this->user_model->getStudentDetails($condition);
         //$qualifiedExams = $this->user_model->checkQualifiedExam(array('student_id'=>$userdata['id']));
         $leadCondition = array("email" => $userdata['email']);
         $leadMaster = $this->user_model->getLead($leadCondition);

         $mandatoryFields = $courseDetail[0]['uni_mandatory_fields'];
         if($mandatoryFields)
         {
             $this->load->model('location/country_model');
             $this->load->model('location/city_model');

             $this->outputData['locationcountries'] = $this->country_model->getLocationCountries(array('location_country_master.status'=>1));
             $this->outputData['cities'] = $this->city_model->getAllCities();
             $this->outputData['mainstreams'] = $this->course_model->getAllCollegeCategories();
             $this->outputData['levels'] = $this->course_model->getAllDegrees();
             if($leadMaster->mainstream)
             {
                 $this->outputData['subCourses'] = $this->course_model->getCourseCategoriesByCollegeCategory($leadMaster->mainstream);
             }

             $mandatoryFields = json_decode($mandatoryFields, true);
             foreach($mandatoryFields as $fieldList)
             {
                 foreach($fieldList as $tableName => $fieldArray)
                 {
                     foreach($fieldArray as $field)
                     {
                         if($tableName == 'user_master' || $tableName == 'student_details')
                         {
                             $this->outputData[$tableName][$field] = $userDetails->$field;
                         }
                         else if($tableName == 'lead_master')
                         {
                             $this->outputData[$tableName][$field] = $leadMaster->$field;
                         }
                     }
                 }
             }
         }

         $studentName = $userdata['name'];
         $date = date('Y-m-d');
          $universityName = $this->outputData['name'];
          $countryName = $this->outputData['country'];
          $sessionType = 'One On One Session';

          $this->outputData['consent'] = str_replace('$todayDate', $date, $this->outputData['consent']);
          $this->outputData['consent'] = str_replace('$universityName', $universityName, $this->outputData['consent']);
          $this->outputData['consent'] = str_replace('$sessionType', $sessionType, $this->outputData['consent']);
          $this->outputData['consent'] = str_replace('$studentName', $studentName, $this->outputData['consent']);
          $this->outputData['consent'] = str_replace('$countryName', $countryName, $this->outputData['consent']);
     }

      $this->render_page('templates/course/course_details',$this->outputData);

    }



}
