<?php

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

require APPPATH . "vendor/autoload.php";
include_once APPPATH . 'mongo/autoload.php';

require_once APPPATH . 'libraries/Mail/sMail.php';

use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;

date_default_timezone_set("Asia/Calcutta");

class Event extends MY_Controller
{
    private $_apiKey = '46220952';
    private $_apiSecret = '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2';

    private $_eventName = 'IMPERIAL-COUNSELLING-MODEL';
    private $_master_otp = '001122';
    private $_room_limit = 8;
    private $_room_tokbox_id = 547;

    private $_user_collection;
    private $_connection_collection;
    private $_chat_collection;
    private $_attender_collection;
    private $_queue_collection;
    private $_imperial_counselling_collection;
    private $_imperial_counselling_attended_collection;
    private $_seminar_students_collection;
    private $_fair_delegates_collection;
    private $_room_student_count_collection;
    private $_online_user_collection;
    private $_tokbox_token_collection;
    private $_help_collection;

    function __construct()
    {
        parent::__construct();
        $this->load->model(['seminar_model']);

        $mongo_server = '13.233.46.52';
        //$mongo_server = 'localhost';
        $mongo_port = '27017';
        $mongo_client = 'mongodb://' . $mongo_server . ':' . $mongo_port;
        $client = new MongoDB\Client($mongo_client);
        $mongo_db = $client->HelloUni;

        $this->_user_collection = $mongo_db->selectCollection('users');
        $this->_connection_collection = $mongo_db->selectCollection('connections');
        $this->_chat_collection = $mongo_db->selectCollection('chats');
        $this->_attender_collection = $mongo_db->selectCollection('attenders');
        $this->_queue_collection = $mongo_db->selectCollection('queue');
        $this->_imperial_counselling_collection = $mongo_db->selectCollection('Imperial_Counselling_Data');
        $this->_imperial_counselling_attended_collection = $mongo_db->selectCollection('Imperial_Counselling_Attended_Data');
        $this->_seminar_students_collection = $mongo_db->selectCollection('seminar_students');
        $this->_fair_delegates_collection = $mongo_db->selectCollection('fair_delegates');
        $this->_room_student_count_collection = $mongo_db->selectCollection('room_student_count');
        $this->_online_user_collection = $mongo_db->selectCollection('online_users');
        $this->_tokbox_token_collection = $mongo_db->selectCollection('tokbox_token');
        $this->_help_collection = $mongo_db->selectCollection('help_tech');

        $tokbox_token_query['type'] = $this->_eventName;
        $tokbox_token_cursor = $this->_tokbox_token_collection->find($tokbox_token_query);
        foreach($tokbox_token_cursor as $token_data)
        {
            $this->_background_images[$token_data['id']] = $token_data['background_url'];
            $this->_tokboxSessionIds[$token_data['id']] = $token_data['session_id'];
        }
    }


    function rooms($username, $linkCount = 1)
    {
        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/event/login";</script>';
            exit;
        }

        // TODO take it from mongodb
        $blockedStudents = [];

        $decodedUsername = base64_decode($username);

        if(in_array($decodedUsername, $blockedStudents))
        {
            echo '<script type="text/javascript">alert("Sorry! You are not authorized."); window.location.href="/event/login";</script>';
            exit;
        }

        $decodedUsername = iconv('UTF-8', 'UTF-8//IGNORE', $decodedUsername);
        $mongoUserQuery['username'] = ['$eq' => $decodedUsername];
        $userDetail = $this->_imperial_counselling_collection->findOne($mongoUserQuery);
        if(!$userDetail)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/event/register";</script>';
            exit;
        }

        $this->outputData['username'] = $username;
        $this->outputData['userId'] = 1;
        $this->outputData['linkCount'] = 1;
        $this->outputData['joined'] = "YES";
        $this->outputData['joinQueue'] = "YES";
        $this->outputData['msg'] = '';
        $this->outputData['runScript'] = 1;
        $this->outputData['roomStatus'] = 1;
        $this->outputData['allRooms'] = 1;
        $this->outputData['startTime'] = '';
        $this->outputData['endTime'] = '';

        $this->render_page('templates/imperialCounsellingTokbox/event_rooms', $this->outputData);

      }



    function get_connections($tokboxId)
    {
        $connections = [];
        $mongoConnectionQuery['tokbox_id'] = $this->_room_tokbox_id;
        $mongoConnectionQuery['join'] = "NO";
        $mongoConnectionQuery['counselling_date'] = Date('Y-m-d');

        $connectionCursor = $this->_imperial_counselling_collection->find($mongoConnectionQuery);

        foreach($connectionCursor as $connection)
        {
            $connections[] = $connection;
        }

        echo json_encode($connections);
    }

    function chat()
    {
        $post_data = json_decode(file_get_contents("php://input"), true);

        $data['from'] = $post_data['user_id'];
        $data['to'] = -1;
        $data['to_user_type'] = NULL;
        $data['session_id'] = $post_data['sessionId'];
        $data['tokbox_id'] = $post_data['tokboxId'];
        $data['user_type'] = 'USER';
        $data['message'] = $post_data['msg'];
        $data['from_role'] = isset($post_data['from_role']) ? $post_data['from_role'] : NULL;
        $data['chat_id'] = isset($post_data['chat_id']) ? $post_data['chat_id'] : NULL;

        $this->seminar_model->storeChat($data);

        $insertOneResult = $this->_chat_collection->insertOne($data);
    }

    function thankyou($username)
    {
        $decodedUsername = base64_decode($username);
        $decodedUsername = iconv('UTF-8', 'UTF-8//IGNORE', $decodedUsername);
        /*$mongoUserQuery['username'] = ['$eq' => $decodedUsername];
        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        if($userDetail)
        {
            $userId = $userDetail['id'];
            $mongoConnectionQuery['user_id'] = (int)$userId;
            $this->_connection_collection->deleteMany($mongoConnectionQuery);
        }

        $mongoRoomStudentCountQuery['username'] = $decodedUsername; $this->_room_student_count_collection->deleteMany($mongoRoomStudentCountQuery);*/


        $this->load->view('templates/imperialCounsellingTokbox/event_thankyou');
    }

    function feedback_post(){
        //echo "<pre>"; print_r('hello m here'); exit;
        $this->response('hello m here', REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
    }

    function logout($username)
    {
        $data['errorMessage'] = 'You are logged in from other system. If you want to continue, go to CRM and click on landing url. It will logout you from other system.';
        $this->load->view('templates/imperialCounsellingTokbox/uta_error', $data);
    }


    function student($tokboxId, $username)
    {
        $blockedStudents = [];

        if(!$tokboxId)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/event/login";</script>';
            exit;
        }

        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/event/login";</script>';
            exit;
        }

        $data['base64_tokbox_id'] = $tokboxId;
        $data['base64_username'] = $username;

        $tokboxId = base64_decode($tokboxId);
        $username = base64_decode($username);

        if(in_array($username, $blockedStudents))
        {
            echo '<script type="text/javascript">alert("Sorry! You are not authorized."); window.location.href="/event/login";</script>';
            exit;
        }

        if(!isset($this->_tokboxSessionIds[$tokboxId]))
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/event/login";</script>';
            exit;
        }

        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/";</script>';
            exit;
        }
        else
        {
            $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        }


        $mongoQueueQuery['username'] = $username;

        $queueData = $this->_imperial_counselling_collection->findOne($mongoQueueQuery);

        $joinedValue = "NO";

        if($queueData){

          $joinedValue = $queueData['join'];
          $updateQuery['username'] = $username;
          $updateData = ['$set' => ['join' => "NO"]];
          $updateOneResult = $this->_imperial_counselling_collection->updateOne($updateQuery, $updateData);

          $insertData['username'] = $queueData['username'];
          $insertData['name'] = $queueData['name'];
          $insertData['email'] = $queueData['email'];
          $insertData['phone'] = $queueData['phone'];
          $insertData['city'] = $queueData['city'];
          $insertData['enrolled'] = $queueData['enrolled'];
          $insertData['current_use'] = $queueData['current_use'];
          $insertData['university_id'] = $queueData['university_id'];
          $insertData['counselling_date'] = $queueData['counselling_date'];

          $insertAttentedOne = $this->_imperial_counselling_attended_collection->insertOne($insertData);
        }

        if($joinedValue == "NO"){
          $encodeUsername = base64_encode($username);
          $encodeUsername = str_replace("=", "", $encodeUsername);
          redirect('/imperialCounsellingTokbox/event/rooms/'.$encodeUsername);
          exit;
        }

        $userId = $userDetail['id'];
        $studentName = $userDetail['name'];

        $mongoRoomStudentCountQuery['tokbox_id'] = (int)$tokboxId;
        $roomStudentsCount = $this->_room_student_count_collection->count($mongoRoomStudentCountQuery);
        $roomLimit = $this->_room_limit;

        if($roomStudentsCount >= $roomLimit)
        {
            $mongoQueueQuery['username'] = $username;
            $queueData = $this->_queue_collection->findOne($mongoQueueQuery);
            if(!$queueData)
            {
                $newQueueData['tokbox_id'] = (int)$tokboxId;
                $newQueueData['username'] = $username;
                $newQueueData['priority'] = 0;

                $this->_queue_collection->insertOne($newQueueData);
            }

            echo '<script type="text/javascript">alert("The room is full. We have added you in queue. You will be notified once room is available.");window.location.href="/event/rooms/' . $data['base64_username'] . '";</script>';
            exit;
        }
        else
        {
            $mongoQueueQuery['tokbox_id'] = (int)$tokboxId;
            $queueCursor = $this->_queue_collection->find($mongoQueueQuery);
            $queueCount = 0;
            $priority = 0;
            foreach($queueCursor as $queue)
            {
                if($queue['username'] == $username)
                {
                    if(isset($queue['priority']) && $queue['priority'])
                    {
                        $priority = 1;
                    }
                    break;
                }
                $queueCount++;
            }
            $totalCount = $roomStudentsCount + $queueCount;
            if($totalCount >= $roomLimit && !$priority)
            {
                echo '<script type="text/javascript">alert("The room is full. Please join the queue.");window.location.href="/event/rooms/' . $data['base64_username'] . '";</script>';
                exit;
            }

            $mongoQueueDeleteQuery['username'] = $username;
            $queueCursor = $this->_queue_collection->deleteMany($mongoQueueDeleteQuery);
        }



        $mongoConnectionQuery['user_id'] = (int)$userId;
        $this->_connection_collection->deleteMany($mongoConnectionQuery);

        $connectionMetaData = $username;
        $sessionId = $this->_tokboxSessionIds[$tokboxId];

        $connectioRow['user_id'] = (int)$userId;
        $connectioRow['username'] = $username;
        $connectioRow['tokbox_id'] = (int)$tokboxId;
        $connectioRow['session_id'] = $sessionId;
        $connectioRow['type'] = 'Student';
        $insertOneResult = $this->_connection_collection->insertOne($connectioRow);

        $attenderRow['user_id'] = (int)$userId;
        $attenderRow['username'] = $username;
        $attenderRow['tokbox_id'] = (int)$tokboxId;
        $attenderRow['session_id'] = $sessionId;
        $attenderRow['sessionType'] = 'ONE_ON_ONE';
        $insertOneResult = $this->_attender_collection->insertOne($attenderRow);

        $mongoChatQuery['session_id'] = $sessionId;
        $chatCount = $this->_chat_collection->count($mongoChatQuery);
        $chats = [];
        if($chatCount)
        {
            $chatCursor = $this->_chat_collection->find($mongoChatQuery);
            foreach($chatCursor as $chat)
            {
                if($chat['from'] == $userId)
                {
                    $chat['chat_of'] = 'mine';
                    $chat['name'] = $username;
                }
                else if($chat['from_role'] == 'PUBLISHER')
                {
                    $chat['chat_of'] = 'theirs';

                    $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                    $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                    $chat['name'] = $chatUserDetail['username'];
                }
                if($chat['name'])
                {
                    $chats[] = $chat;
                }
            }
        }


        $roomLimitInsert['tokbox_id'] = (int)$tokboxId;
        $roomLimitInsert['username'] = $username;
        $this->_room_student_count_collection->insertOne($roomLimitInsert);

        $background_image['url'] = $this->_background_images[$tokboxId];

        $opentok = new OpenTok($this->_apiKey, $this->_apiSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'publisher',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['sessionId'] = $sessionId;
        $data['userId'] = $userId;
        $data['username'] = $username;
        $data['tokboxId'] = $tokboxId;
        $data['apiKey'] = $this->_apiKey;
        $data['linkCount'] = $linkCount;
        $data['common_chat_history'] = $chats;
        $data['backgroundImage'] = $background_image;
        $data['studentName'] = $studentName;

        $this->load->view('templates/imperialCounsellingTokbox/event_student_oneonone', $data);
    }

    function organizer($tokboxId, $username)
    {
        if(!$tokboxId)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $data['base64_token_id'] = $tokboxId;
        $data['base64_username'] = $username;

        $tokboxId = base64_decode($tokboxId);
        $username = base64_decode($username);

        $background_image['url'] = $this->_background_images[$tokboxId];

        if(!isset($this->_tokboxSessionIds[$tokboxId]))
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        //$userDetail = $this->_user_collection->findOne($mongoUserQuery);
        $userDetail = $this->_fair_delegates_collection->findOne($mongoUserQuery);
        if(!$userDetail)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/";</script>';
            exit;
        }


        $userId = $userDetail['id'];
        $organizerName = $userDetail['name'];

        $connectionMetaData = $username;
        $sessionId = $this->_tokboxSessionIds[$tokboxId];

        $mongoChatQuery['session_id'] = $sessionId;
        $chatCount = $this->_chat_collection->count($mongoChatQuery);
        $chats = [];
        if($chatCount)
        {
            $chatCursor = $this->_chat_collection->find($mongoChatQuery);
            foreach($chatCursor as $chat)
            {
                $chat['chat_of'] = 'mine';
                $chat['name'] = $username;
                if($chat['from'] != $userId)
                {
                    $chat['chat_of'] = 'theirs';

                    $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                    $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                    $chat['name'] = $chatUserDetail['username'];
                }
                if($chat['name'])
                {
                    $chats[] = $chat;
                }
            }
        }

        $mongoConnectionQuery['user_id'] = (int)$userId;
        $this->_connection_collection->deleteMany($mongoConnectionQuery);

        $row['user_id'] = (int)$userId;
        $row['username'] = $username;
        $row['tokbox_id'] = (int)$tokboxId;
        $row['session_id'] = $sessionId;
        $row['type'] = 'Organizer';
        $insertOneResult = $this->_connection_collection->insertOne($row);

        $mongoHelpQuery['tokbox_id'] = (int)$tokboxId;
        $this->_help_collection->deleteMany($mongoHelpQuery);

        $opentok = new OpenTok($this->_apiKey, $this->_apiSecret);

        $role = $this->input->get('s') ? 'subscriber' : 'moderator';

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => $role,
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['sessionId'] = $sessionId;
        $data['username'] = $username;
        $data['userId'] = $userId;
        $data['tokboxId'] = $tokboxId;
        $data['apiKey'] = $this->_apiKey;
        $data['common_chat_history'] = $chats;
        $data['backgroundImage'] = $background_image;
        $data['organizerName'] = $organizerName;
        $data['role'] = $role;

        $this->load->view('templates/imperialCounsellingTokbox/event_organizer', $data);
    }

    function panelist($tokboxId, $username)
    {
        if(!$tokboxId)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $data['base64_token_id'] = $tokboxId;
        $data['base64_username'] = $username;

        $tokboxId = base64_decode($tokboxId);
        $username = base64_decode($username);

        $background_image['url'] = $this->_background_images[$tokboxId];

        if(!isset($this->_tokboxSessionIds[$tokboxId]))
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        //$userDetail = $this->_user_collection->findOne($mongoUserQuery);
        $userDetail = $this->_fair_delegates_collection->findOne($mongoUserQuery);
        if(!$userDetail)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/";</script>';
            exit;
        }

        $userId = $userDetail['id'];
        $panelistName = $userDetail['name'];
        /*$boothId = $userDetail['fair_university_id'];
        $encodedBoothId = base64_encode($boothId);
        $encodedBoothId = str_replace("=", "", $encodedBoothId);*/

        $connectionMetaData = $username;
        $sessionId = $this->_tokboxSessionIds[$tokboxId];

        $mongoChatQuery['session_id'] = $sessionId;
        $chatCount = $this->_chat_collection->count($mongoChatQuery);
        $chats = [];
        if($chatCount)
        {
            $chatCursor = $this->_chat_collection->find($mongoChatQuery);
            foreach($chatCursor as $chat)
            {
                if($chat['from'] == $userId)
                {
                    $chat['chat_of'] = 'mine';
                    $chat['name'] = $username;
                }
                else if($chat['from_role'] == 'PUBLISHER')
                {
                    $chat['chat_of'] = 'theirs';

                    $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                    $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                    $chat['name'] = $chatUserDetail['username'];
                }
                if($chat['name'])
                {
                    $chats[] = $chat;
                }
            }
        }

        $mongoConnectionQuery['user_id'] = (int)$userId;
        $this->_connection_collection->deleteMany($mongoConnectionQuery);

        $row['user_id'] = (int)$userId;
        $row['username'] = $username;
        $row['tokbox_id'] = (int)$tokboxId;
        $row['session_id'] = $sessionId;
        $row['type'] = 'Panelist';
        $insertOneResult = $this->_connection_collection->insertOne($row);

        $opentok = new OpenTok($this->_apiKey, $this->_apiSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'moderator',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['student_detail'] = true;

        $data['sessionId'] = $sessionId;
        $data['username'] = $username;
        $data['userId'] = $userId;
        $data['tokboxId'] = $tokboxId;
        $data['apiKey'] = $this->_apiKey;
        $data['common_chat_history'] = $chats;
        $data['backgroundImage'] = $background_image;
        $data['panelistName'] = $panelistName;
        $data['boothId'] = $encodedBoothId;
        $data['panelistInfoPopup'] = "YES";

        if(isset($_COOKIE['pname'])){

          $data['panelistName'] = $_COOKIE['pname'].' ( '.$_COOKIE['pdesignation'].' )';
          $data['panelistInfoPopup'] = "NO";
        }

        $this->load->view('templates/imperialCounsellingTokbox/event_panelist', $data);
    }


    function virtualfair()
    {
        $this->load->model(['course/course_model', 'user/user_model', 'location/country_model', 'course/degree_model']);

        $this->outputData['university_id'] = $this->uri->segment('4');
        $this->render_page('templates/imperialCounsellingTokbox/uta_registration_form',$this->outputData);
    }

    function virtualfair_registration()
    {
        $eventName = $this->_eventName;

        $firstName          = $this->input->post('first_name');
        $lastName           = $this->input->post('last_name');
        $userEmail          = $this->input->post('user_email');
        $userMobile         = $this->input->post('user_mobile');
        $userPassword       = $userMobile;
        $enrolled           = $this->input->post('user_enrolled');
        $userCity           = $this->input->post('user_city');

        $checkMobileVerified = 1;

        $checkUserExist = $this->seminar_model->checkUser($userMobile);

        if($checkUserExist)
        {
            $user_id = $checkUserExist['id'];
            $username = $checkUserExist['username'];
            $mobileVerified = $checkUserExist['mobile_verified'];
            $data = [
                'registration_type' => $eventName,
                'mobile_verified' => $mobileVerified + 1,
                'modifydate' => date('Y-m-d H:i:s'),
                'registration_id'   => 'WEB'
            ];

            $this->seminar_model->updateUser($user_id, $data);
        }
        else
        {
            $username = substr($userEmail, 0, 3) . '' . substr($userMobile, 0, 3);
            $userPassword = $userPassword ? $userPassword : $username;
            $data = array(
                'name'              => $firstName . ' ' . $lastName,
                'username'          => $username,
                'email'             => $userEmail,
                'password'          => md5($userPassword),
                'image'             => '',
                'type'              => '5',
                'createdate'        => date('Y-m-d H:i:s'),
                'modifydate'        => date('Y-m-d H:i:s'),
                'activation'        => md5(time()),
                'fid'               => '',
                'registration_type' => $eventName,
                'status'            => '1',
                'phone'             => $userMobile,
                'mobile_verified'   => 1,
                'registration_id'   => 'WEB'
            );

            $user_id = $this->seminar_model->insertUserMaster($data);

            $student_data = array(
                'user_id'           => $user_id,
                'phone'             => $userMobile,
                'facilitator_id'    => 1,
                'current_city'      => $userCity,
                'originator_id'     => $user_id
            );

            $this->seminar_model->insertStudentDetails($student_data);
        }

        $universityId = $this->input->post('university_id');

        $otherFields['applied_university'] = $universityId;
        $seminar_entry = $this->seminar_model->checkSeminarStudent($user_id, $eventName, $otherFields);
        if(!$seminar_entry)
        {
            $seminar_data['name'] = $firstName . ' ' . $lastName;
            $seminar_data['username'] = $username;
            $seminar_data['email'] = $userEmail;
            $seminar_data['phone'] = $userMobile;
            $seminar_data['current_use'] = $eventName;
            $seminar_data['user_id'] = $user_id;
            $seminar_data['education_loan_type'] = $loanType;
            $seminar_data['current_city'] = $userCity;
            $seminar_data['enrolled'] = $enrolled;
            $seminar_data['applied_university'] = $universityId;

            $seminar_id = $this->seminar_model->insertSeminarStudent($seminar_data);
        }
        else
        {
            $seminar_id = $seminar_entry['id'];
        }

        $mongoUserQuery['id'] = ['$eq' => (int)$user_id];
        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        if(!$userDetail)
        {
            $userData = $this->seminar_model->getUserByid($user_id);
            $userData['id'] = (int)$user_id;
            $this->_user_collection->insertOne($userData);
        }

        $mongoSeminarStudentQuery['id'] = ['$eq' => (int)$seminar_id];
        $seminarStudentDetail = $this->_seminar_students_collection->findOne($mongoUserQuery);
        if(!$seminarStudentDetail)
        {
            $seminarStudentData = $this->seminar_model->getSeminarStudentById($seminar_id);
            $seminarStudentData['id'] = (int)$seminar_id;
            $this->_seminar_students_collection->insertOne($seminarStudentData);
        }

        $newImperialCounsellingData['tokbox_id'] = $this->_room_tokbox_id;
        $newImperialCounsellingData['username'] = $username;
        $newImperialCounsellingData['name'] = $firstName . ' ' . $lastName;
        $newImperialCounsellingData['email'] = $userEmail;
        $newImperialCounsellingData['phone'] = $userMobile;
        $newImperialCounsellingData['city'] = $userCity;
        $newImperialCounsellingData['enrolled'] = $enrolled;
        $newImperialCounsellingData['current_use'] = $eventName;
        $newImperialCounsellingData['join'] = "NO";
        $newImperialCounsellingData['priority'] = 0;
        $newImperialCounsellingData['university_id'] = $universityId;
        $newImperialCounsellingData['counselling_date'] = Date('Y-m-d');

        $this->_imperial_counselling_collection->insertOne($newImperialCounsellingData);

        // Apply University
        $checkApplied = $this->seminar_model->getAppliedUniversityByUsre($user_id, $universityId);
        if(!$checkApplied)
        {
            $applyData['user_id'] = $user_id;
            $applyData['university_id'] = $universityId;
            $this->seminar_model->applyUniversity($applyData);
        }

        $encodedUsername = base64_encode($username);
        $encodedUsername = str_replace("=", "", $encodedUsername);

        $arr = [
            "success" => true,
            "encodedUsername" => $encodedUsername
        ];
        echo json_encode($arr);
    }


    function send_otp()
    {
        $mobileNumber = $this->input->post('mobile_number') ? $this->input->post('mobile_number') : 0;
        $userEmail = $this->input->post('user_email') ? $this->input->post('user_email') : '';

        if(!($mobileNumber) || strlen($mobileNumber) != 10 || !$userEmail)
        {
            $arr = [
                "success" => false,
                "message" => 'Please provide valid mobile number & email id !!!'
            ];
            echo json_encode($arr);
            exit();
        }

        $mobileAlreadyAvailable = $this->seminar_model->checkUserMobile($mobileNumber, $this->_eventName);

        if($mobileAlreadyAvailable)
        {
            $arr = [
                "success" => false,
                "message" => 'Mobile Number Already Registered'
            ];
            echo json_encode($arr);
            exit();
        }

        $mobileAlreadyVerified = $this->seminar_model->checkMasterOtp($mobileNumber);

        if($mobileAlreadyVerified)
        {
            $otp = $mobileAlreadyVerified['otp'];
        }
        else
        {
            $otp = mt_rand(100000, 999999);

            $insertData =["mobile_number" => $mobileNumber, "otp" => $otp];
            $this->seminar_model->insertOtp($insertData);
        }

        if(filter_var($userEmail, FILTER_VALIDATE_EMAIL))
        {
            $mailSubject = "HelloUni - OTP Verification Code !";
            $mailTemplate = "Hi User,

                     Please use this OTP to validate your login on Hellouni - $otp

                     Regards,
                     Team HelloUni
                     Tel: +91 81049 09690
                     Email: info@hellouni.org


                     <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

            $ccMailList = '';
            $mailAttachments = '';

            //$studentEmail = 'nikhil.gupta@issc.in';

            $sendMail = sMail($userEmail, 'User', $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);
        }

        $arr = [
            "success" => true,
            "message" => 'OTP Sent ON Given MObile Number AND Email-ID'
        ];

        echo json_encode($arr);
    }

    function verify_otp()
    {
        $mobileNumber = $this->input->post('mobile_number') ?  $this->input->post('mobile_number') : 0;
        $otp = $this->input->post('otp') ?  $this->input->post('otp') : 0;

        if( !($otp) || strlen($otp) != 6)
        {
            $arr = [
                "success" => true,
                "message" => 'OTP Is Missing OR Invalid OTP'
            ];
            echo json_encode($arr);
            exit();
        }

        if(!($mobileNumber))
        {
            $arr = [
                "success" => true,
                "message" => 'Mobile Number Is Missing'
            ];
            echo json_encode($arr);
            exit();
        }

        $isexists = $this->seminar_model->checkMobileOTP($mobileNumber, $otp);

        if(!$isexists)
        {
            $arr = [
                "success" => true,
                "message" => 'Mobile Number Or OTP is Invalid'
            ];
            echo json_encode($arr);
            exit();
        }

        $otpMasterUpdate['status'] = 1;

        $updateMobileOTP = $this->seminar_model->updateOtp($mobileNumber, $otpMasterUpdate);

        $arr = [
            "success" => true,
            "message" => 'Successfully Verified ...!!!'
        ];
        echo json_encode($arr);
    }

    function virtualfair_login()
    {
        $userMobile         = $this->input->post('user_mobile');
        $userPassword       = $this->input->post('user_password');

        $userMobile = iconv('UTF-8', 'UTF-8//IGNORE', $userMobile);
        $mongoUserQuery['phone'] = $userMobile;

        if($userPassword != $this->_master_otp)
        {
            $mongoUserQuery['password'] = md5($userPassword);
        }

        $userDetail = $this->_user_collection->findOne($mongoUserQuery);

        if($userDetail)
        {
            $username = $userDetail['username'];
        }
        else
        {
            $arr = [
                "success" => false,
                "message" => 'Mobile number is not registered'
            ];
            echo json_encode($arr);
            exit();
        }

        $encodedUsername = base64_encode($username);
        $encodedUsername = str_replace("=", "", $encodedUsername);

        $arr = [
            "success" => true,
            "encodedUsername" => $encodedUsername
        ];
        echo json_encode($arr);
    }

    function leave_meeting($encodedRoomId, $encodedUsername)
    {
        $tokboxId = base64_decode($encodedRoomId);
        $username = base64_decode($encodedUsername);
        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);

        $mongoRoomStudentCountQuery['username'] = $username; $this->_room_student_count_collection->deleteMany($mongoRoomStudentCountQuery);

        $mongoUserQuery['username'] = ['$eq' => $username];
        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        if($userDetail)
        {
            $userId = $userDetail['id'];
            $mongoConnectionQuery['user_id'] = (int)$userId;
            $this->_connection_collection->deleteMany($mongoConnectionQuery);
        }
    }

    function joinRoom($username){

      $updateQuery['username'] = $username;
      $updateData = ['$set' => ['join' => "YES"]];
      $updateOneResult = $this->_imperial_counselling_collection->updateOne($updateQuery, $updateData);

      $arr = [
          "success" => true,
          "room_join" => "YES"
      ];
      echo json_encode($arr);

    }

    function check_queue($encodedUsername)
    {
        $username = base64_decode($encodedUsername);
        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);

        $mongoQueueQuery['username'] = $username;

        $queueData = $this->_imperial_counselling_collection->findOne($mongoQueueQuery);

        if($queueData)
        {
            $queueTokboxId = $queueData['tokbox_id'];
            $joinRoom = $queueData['join'];

            $encodedToxboxId = base64_encode($queueTokboxId);
            $encodedToxboxId = str_replace("=", "", $encodedToxboxId);


            $arr = [
                "success" => true,
                "room_id" => $encodedToxboxId,
                "join" => $joinRoom,

            ];
            echo json_encode($arr);
            exit;
          }

        $arr = [
            "success" => false
        ];
        echo json_encode($arr);
        exit;
    }


    function user_online($encodedUsername, $encodedRoomId = NULL)
    {
        $username = base64_decode($encodedUsername);
        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);

        if($encodedRoomId)
        {
            $roomId = base64_decode($encodedRoomId);
            $roomId = iconv('UTF-8', 'UTF-8//IGNORE', $roomId);
            $mongoUserOnlineQuery['room_id'] = (int)$roomId;
        }

        $mongoUserOnlineQuery['username'] = $username;
        $this->_online_user_collection->deleteMany($mongoUserOnlineQuery);

        $mongoUserOnlineQuery['time'] = date('Y-m-d H:i:s');
        $this->_online_user_collection->insertOne($mongoUserOnlineQuery);
    }
}
