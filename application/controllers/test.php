<?php
require APPPATH . "vendor/autoload.php";

use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;

class Test extends CI_Controller
{
    public function index()
    {
        $this->load->view('test_form');
    }

    public function thankyou()
    {
        $this->load->view('test_thankyou');
    }

    public function load()
    {
        //$sessionId = $_GET['sid'];
        //$sessionId = '1_MX40NjIyMDk1Mn5-MTU4ODE0NDQxMzMxNH5oS1dmSXFhZHRrYzluWnRaRCtyMjFKMzl-QX4'; // Auto Archive
        $sessionId = '2_MX40NjIyMDk1Mn5-MTU4ODIzMTU3ODE2N344eWloTVBNazhnSkxERWtTWDZRUnJCMU9-fg'; // Wihtout Auto Archive
        $role = $_GET['role']; //publisher, subscriber, moderator
        $username = $_GET['uname'];
        $polling = isset($_GET['polling']) ? 1 : 0;
        //$connectionMetaData = "username=$username,userLevel=4";
        $connectionMetaData = $username;
        $polling_questions = [
            [
                "id" => 1,
                "question_string" => "What color is the sky?",
                "choices" => [
                    "correct" => "Blue",
                    "wrong" => ["Pink", "Orange", "Green"]
                ]
            ],
            [
                "id" => 2,
                "question_string" => "Who is the main character of Harry Potter?",
                "choices" => [
                    "correct" => "Harry Potter",
                    "wrong" => ["Hermione Granger", "Ron Weasley", "Voldemort"]
                ]
            ]
        ];

        $opentok = new OpenTok('46220952', '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2');
        $data['token'] = $opentok->generateToken($sessionId, array('role' => $role, 'expireTime' => time()+(7 * 24 * 60 * 60), 'data' =>  $connectionMetaData));
        $data['sessionId'] = $sessionId;
        $data['username'] = $username;
        $data['tokboxId'] = mt_rand(1000, 9999);
        $data['role'] = $role;
        $data['polling'] = $polling;
        $data['polling_questions'] = json_encode($polling_questions);

        $this->session->set_userdata('tokbox_username', $username);
        $user_id = mt_rand(1, 99999);
        $this->session->set_userdata('tokbox_userid', $user_id);

        $this->load->view('test', $data);
    }


    function poll()
    {
        //echo $this->session->userdata('tokbox_username');
        $this->load->model('tokbox_model');
        $user_id = $this->session->userdata('tokbox_userid');
        $post_data = json_decode(file_get_contents("php://input"), true);
        $data['user_id'] = $user_id;
        foreach($post_data as $post_values)
        {
            $data['question_id'] = $post_values['question_id'];
            $data['answer'] = $post_values['user_ans'];
            $post_values['user'] = $this->session->userdata('tokbox_username');
            $data['other_detail'] = json_encode($post_values);

            $this->tokbox_model->addPollResult($data);
        }
    }

}
?>
