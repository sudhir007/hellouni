<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com


 */

class Course extends MY_Controller {



	//Global variable

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;



    /**

	 * Constructor

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }


	function index()
	{
	     $this->load->helper('cookie_helper');
         $this->load->model('university/course_model');
         $this->load->model('filter_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }

         if($userdata['university_id'])
        {
            $this->outputData['courses'] = $this->course_model->getAllCoursesByUniversity($userdata['university_id']);
            $this->outputData['university_list'] = $this->filter_model->getAllUniversityById($userdata['university_id']);
        }
        else
        {
            $this->outputData['courses'] = $this->course_model->getAllCourses();
            $this->outputData['university_list'] = $this->filter_model->getAllUniversityById();
        }

		$this->outputData['view'] = 'list';

		/*echo '<pre>';
		print_r($this->outputData);
		echo '</pre>';
		exit();*/

        $this->render_page('templates/university/course',$this->outputData);

    }

    function coursebydepartmentid()
  	{
  	     $this->load->helper('cookie_helper');
         $this->load->model('university/course_model');

  		   $userdata=$this->session->userdata('user');
  	     if(empty($userdata)){  redirect('common/login'); }

          $departmentId = $_GET['department_id'];

          $this->outputData['courses'] = $this->course_model->getAllCoursesByDepartmentId($departmentId);
          $this->outputData['department_list'] = $this->course_model->getAllDepartmentByDepartmentId($departmentId);
          $this->outputData['selected_department_id'] = $departmentId;
      		$this->outputData['view'] = 'list';

          $this->render_page('templates/university/coursebydepartment',$this->outputData);

      }

    function filter() {

  	     $this->load->helper('cookie_helper');
         $this->load->model('university/course_model');
         $this->load->model('filter_model');

  		   $userdata=$this->session->userdata('user');
  	     if(empty($userdata)){
           redirect('common/login');
         }

          $selectedUniversityId = isset($_POST['university_id']) &&  $_POST['university_id'] != "" ? $_POST['university_id'] : 0;
          $selectedCampusId = isset($_POST['campus_id']) && $_POST['campus_id'] !="" ? $_POST['campus_id'] : 0;
          $selectedCollegeId = isset($_POST['college_id']) && $_POST['college_id'] !="" ? $_POST['college_id'] : 0;
          $selectedDepartmentId = isset($_POST['department_id']) && $_POST['department_id'] !="" ? $_POST['department_id'] : 0;
          $selectedCourseId = isset($_POST['course_id']) && $_POST['course_id'] !="" ? $_POST['course_id'] : 0;

          $allUniversityList = [];
          $allCampusList = [];
          $allCollegeList = [];
          $allDepartmentList = [];
          $allCourseList = [];

          if($userdata['university_id']) {
             $allUniversityList = $this->filter_model->getAllUniversityById($userdata['university_id']);
         } else {
             $allUniversityList = $this->filter_model->getAllUniversityById();
         }


          if($selectedUniversityId){
            $allCampusList = $this->filter_model->getAllCampusesByUniversityId($selectedUniversityId);
          }

          if($selectedCampusId){
            $allCollegeList = $this->filter_model->getAllCollegeByCampusId($selectedCampusId);
          }

          if($selectedCollegeId){
            $allDepartmentList = $this->filter_model->getAllDepartmentByCollegeId($selectedCollegeId);
          }

          if($selectedDepartmentId){
            $allCourseList = $this->filter_model->getAllCoursesByDepartmentId($selectedDepartmentId);
          }


          $this->outputData['courses'] = $this->filter_model->getFilterCourses($selectedUniversityId,$selectedCampusId,$selectedCollegeId,$selectedDepartmentId,$selectedCourseId);


          $this->outputData['selected_university_id'] = $selectedUniversityId;
          $this->outputData['university_list'] = $allUniversityList;
          $this->outputData['selected_campus_id'] = $selectedCampusId;
          $this->outputData['campus_list'] = $allCampusList;
          $this->outputData['selected_college_id'] = $selectedCollegeId;
          $this->outputData['college_list'] = $allCollegeList;
          $this->outputData['selected_department_id'] = $selectedDepartmentId;
          $this->outputData['department_list'] = $allDepartmentList;
          $this->outputData['selected_course_id'] = $selectedCourseId;
          $this->outputData['course_list'] = $allCourseList;

  		$this->outputData['view'] = 'list';

  		/*echo '<pre>';
  		print_r($this->outputData);
  		echo '</pre>';
  		exit();*/

          $this->render_page('templates/university/course',$this->outputData);

      }

	 function form()
	 {
	     $this->load->helper('cookie_helper');
         $this->load->model('university/department_model');
         $this->load->model('university/course_model');
         $this->load->model('filter_model');
		 //$this->load->model('location/country_model');
		 //$this->load->model('location/city_model');
		 //$this->load->model('location/state_model');


		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }
         $this->outputData['sop_no_of_words'] = "none";
         $this->outputData['lor_required'] = "none";
         $this->outputData['lor_type'] = "none";

         $this->outputData['workPermit'] = 'none';
         $this->outputData['backlog'] = 'none';
         $this->outputData['applicationFee'] = 'none';

		 if($this->uri->segment('4')){
         $courseData = $this->course_model->getCourseByIdNew($this->uri->segment('4'));
         $eligibility_gre = $courseData->eligibility_gre ? json_decode($courseData->eligibility_gre) : '';
         $backlogs = $courseData->backlogs ? json_decode($courseData->backlogs) : '';
         $workPermit = $courseData->work_permit ? json_decode($courseData->work_permit) : '';

	      $this->outputData['id'] 				= $courseData->id;
		$this->outputData['name'] 				= $courseData->name;
		$this->outputData['department'] 				= $courseData->department_id;
        $this->outputData['category'] 				= $courseData->category_id;
        $this->outputData['degree'] 				= $courseData->degree_id;
		$this->outputData['duration'] 				= $courseData->duration;
		$this->outputData['deadline_link'] 				= $courseData->deadline_link;
        /*$this->outputData['deadline_date_spring'] 				= $courseData->deadline_date_spring;
        $this->outputData['deadline_date_fall'] 				= $courseData->deadline_date_fall;
		$this->outputData['intake'] 				= $courseData->intake;*/
		$this->outputData['total_fees'] 				= $courseData->total_fees;
        $this->outputData['contact_title'] 				= $courseData->contact_title;
		$this->outputData['contact_name'] 				= $courseData->contact_name;
		$this->outputData['contact_email'] 				= $courseData->contact_email;
        $this->outputData['eligibility_gpa'] 				= $courseData->eligibility_gpa;
        $this->outputData['gpa_scale'] 				= $courseData->gpa_scale;
		$this->outputData['eligibility_gre_quant'] 				= isset($eligibility_gre->quant) ? $eligibility_gre->quant : '';
        $this->outputData['eligibility_gre_verbal'] 				= isset($eligibility_gre->verbal) ? $eligibility_gre->verbal : '';
		$this->outputData['eligibility_toefl'] 				= $courseData->eligibility_toefl;
        $this->outputData['eligibility_ielts'] 				= $courseData->eligibility_ielts;
		$this->outputData['expected_salary'] 				= $courseData->expected_salary;
		$this->outputData['eligibility_work_experience'] 				= $courseData->eligibility_work_experience;
        $this->outputData['sop'] 				= $courseData->sop ? json_decode($courseData->sop) : '';
        $this->outputData['lor'] 				= $courseData->lor ? json_decode($courseData->lor) : '';

        $this->outputData['degree_substream'] 				= $courseData->degree_substream;
        $this->outputData['no_of_intake'] 				= $courseData->no_of_intake;
		$this->outputData['intake_months'] 				= $courseData->intake_months ? implode(',', json_decode($courseData->intake_months, true)) : '';
        $this->outputData['application_fee'] 				= $courseData->application_fee;
        $this->outputData['application_fee_amount'] 				= $courseData->application_fee_amount;
		$this->outputData['application_fee_waiver_imperial'] 				= $courseData->application_fee_waiver_imperial;
        $this->outputData['cost_of_living_year'] 				= $courseData->cost_of_living_year;
        $this->outputData['test_required'] 				= $courseData->test_required;
		$this->outputData['eligibility_gmat'] 				= $courseData->eligibility_gmat;
        $this->outputData['eligibility_sat'] 				= $courseData->eligibility_sat;
        $this->outputData['eligibility_english_proficiency'] 				= $courseData->eligibility_english_proficiency;
		$this->outputData['eligibility_pte'] 				= $courseData->eligibility_pte;
        $this->outputData['eligibility_duolingo'] 				= $courseData->eligibility_duolingo;
        $this->outputData['resume_required'] 				= $courseData->resume_required;
		$this->outputData['admission_requirements'] 				= $courseData->admission_requirements;
        $this->outputData['backlog_accepted'] 				= isset($backlogs->backlog_accepted) ? $backlogs->backlog_accepted : '';
        $this->outputData['number_of_backlogs'] 				= isset($backlogs->number_of_backlogs) ? $backlogs->number_of_backlogs : '';
		$this->outputData['drop_years'] 				= $courseData->drop_years;
        $this->outputData['scholarship_available'] 				= $courseData->scholarship_available;
        $this->outputData['with_15_years_education'] 				= $courseData->with_15_years_education;
		$this->outputData['wes_requirement'] 				= $courseData->wes_requirement;
        $this->outputData['work_while_studying'] 				= $courseData->work_while_studying;
        $this->outputData['cooperate_internship_participation'] 				= $courseData->cooperate_internship_participation;
        $this->outputData['work_permit_needed'] 				= isset($workPermit->work_permit_needed) ? $workPermit->work_permit_needed : '';
        $this->outputData['no_of_months'] 				= isset($workPermit->no_of_months) ? $workPermit->no_of_months : '';
        $this->outputData['start_months'] 	= $courseData->start_months ? implode(',', json_decode($courseData->start_months, true)) : '';
        $this->outputData['decision_months'] 	= $courseData->decision_months ? implode(',', json_decode($courseData->decision_months, true)) : '';
        $this->outputData['conditional_admit'] 				= $courseData->conditional_admit;

        $this->outputData['sop_no_of_words'] = isset($this->outputData['sop']->sop_needed) && $this->outputData['sop']->sop_needed ? 'block' : 'none';
        $this->outputData['lor_required'] = isset($this->outputData['lor']->lor_needed) && $this->outputData['lor']->lor_needed ? 'block' : 'none';
        $this->outputData['lor_type'] = isset($this->outputData['lor']->lor_needed) && $this->outputData['lor']->lor_needed ? 'block' : 'none';
        $this->outputData['workPermit'] = $this->outputData['work_permit_needed'] == 'Yes' ? 'block' : 'none';
        $this->outputData['backlog'] = $this->outputData['backlog_accepted'] == 'Yes' ? 'block' : 'none';
        $this->outputData['applicationFee'] = $this->outputData['application_fee'] == 'Yes' ? 'block' : 'none';


        $this->outputData['selected_university_id'] = $courseData->university_id;
        $this->outputData['selected_campus_id'] = $courseData->campus_id;
        $this->outputData['selected_college_id'] = $courseData->college_id;
        $this->outputData['selected_department_id'] = $courseData->department_id;
        $this->outputData['description'] = $courseData->description;

        $selectedUniversityId = $courseData->university_id ? $courseData->university_id : 0;
        $selectedCampusId = $courseData->campus_id ? $courseData->campus_id : 0;
        $selectedCollegeId = $courseData->college_id ? $courseData->college_id : 0;

        if($selectedUniversityId){
          $allCampusList = $this->filter_model->getAllCampusesByUniversityId($selectedUniversityId);
        }

        if($selectedCampusId){
          $allCollegeList = $this->filter_model->getAllCollegeByCampusId($selectedCampusId);
        }

        if($selectedCollegeId){
          $allDepartmentList = $this->filter_model->getAllDepartmentByCollegeId($selectedCollegeId);
        }

       $this->outputData['campus_list'] = $allCampusList;
       $this->outputData['college_list'] = $allCollegeList;
       $this->outputData['department_list'] = $allDepartmentList;

		}

       if($userdata['university_id']) {
          $allUniversityList = $this->filter_model->getAllUniversityById($userdata['university_id']);
      } else {
          $allUniversityList = $this->filter_model->getAllUniversityById();
      }

        $allDegrees = $this->course_model->getAllDegrees();
        $allCategories = $this->course_model->getAllCategories();

        $this->outputData['degrees'] = $allDegrees;
        $this->outputData['categories'] = $allCategories;
        $this->outputData['university_list'] = $allUniversityList;
        $this->outputData['degree_substreams'] = ['Grade (1-8)', 'Grade (9-10)', 'High School (11th-12th)', 'UG Certificate (1 Year)', 'UG Diploma (2 Year)', 'UG Advanced Diploma (3 year)', 'UG Degree (3 Years)', 'UG Degree (4 years)', 'PG Diploma/Certificate', 'PG (Masters)', 'UG+PG (Accelerated) Degree', 'Doctoral Degree (PHd)', 'Foundation', 'Short Term Programs (Non Degree)', 'Pathway Programs', 'Twinning Programmes (UG)', 'Twinning Programmes (PG)'];

        $this->render_page('templates/university/course_form_new',$this->outputData);

     }

	 function create()
	 {
         if(!$this->input->post('name') || !$this->input->post('department') || !$this->input->post('category_id') || !$this->input->post('degree'))
         {
             $this->session->set_flashdata('flash_message', "Error: Please fill all mandatory fields!");
    	     redirect('university/course/form');
         }
	   //die('+++++');
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('university/course_model');

       $eligibility_gre = array(
           'quant' => $this->input->post('eligibility_gre_quant'),
           'verbal' => $this->input->post('eligibility_gre_verbal')
       );

       $sop = array();
       $lor = array();
       $backlogs = array();
       $workPermit = array();

       if($this->input->post('sop_needed') != ''){
           $sop = array(
               'sop_needed'        => $this->input->post('sop_needed'),
               'sop_no_of_words'   => $this->input->post('sop_no_of_words')
           );
       }

       if($this->input->post('lor_needed') != ''){
           $lor = array(
               'lor_needed'    => $this->input->post('lor_needed'),
               'lor_required'  => $this->input->post('lor_required'),
               'lor_type'      => $this->input->post('lor_type')
           );
       }

       if($this->input->post('backlog_accepted') != ''){
           $backlogs = array(
               'backlog_accepted'        => $this->input->post('backlog_accepted'),
               'number_of_backlogs'   => $this->input->post('number_of_backlogs')
           );
       }

       if($this->input->post('work_permit_needed') != ''){
           $workPermit = array(
               'work_permit_needed'        => $this->input->post('work_permit_needed'),
               'no_of_months'   => $this->input->post('no_of_months')
           );
       }

		 	$data = array(
        	'name' 			=>  $this->input->post('name'),
        	'department_id' 		=>  $this->input->post('department'),
            'category_id' 		=>  $this->input->post('category_id') ? $this->input->post('category_id') : NULL,
            'degree_id' 		=>  $this->input->post('degree'),
            'duration' 		=>  $this->input->post('duration') ? $this->input->post('duration') : NULL,
            'deadline_link' 		=>  $this->input->post('deadline_link'),
            /*'deadline_date_spring' 		=>  $this->input->post('deadline_date_spring') ? $this->input->post('deadline_date_spring') : NULL,
            'deadline_date_fall' 		=>  $this->input->post('deadline_date_fall') ? $this->input->post('deadline_date_fall') : NULL,
            'intake' 		=>  $this->input->post('intake') ? $this->input->post('intake') : NULL,*/
            'total_fees' 		=>  $this->input->post('total_fees') ? $this->input->post('total_fees') : NULL,
            'contact_title' 		=>  $this->input->post('contact_title'),
            'contact_name' 		=>  $this->input->post('contact_name'),
            'contact_email' 		=>  $this->input->post('contact_email'),
            'eligibility_gpa' 		=>  $this->input->post('eligibility_gpa') ? $this->input->post('eligibility_gpa') : NULL,
            'gpa_scale' 		=>  $this->input->post('gpa_scale') ? $this->input->post('gpa_scale') : NULL,
            'eligibility_gre' 		=>  json_encode($eligibility_gre),
            'eligibility_toefl' 		=>  $this->input->post('eligibility_toefl') ? $this->input->post('eligibility_toefl') : NULL,
            'eligibility_ielts' 		=>  $this->input->post('eligibility_ielts') ? $this->input->post('eligibility_ielts') : NULL,
            'expected_salary' 		=>  $this->input->post('expected_salary') ? $this->input->post('expected_salary') : NULL,
            'eligibility_work_experience' 		=>  $this->input->post('eligibility_work_experience') ? $this->input->post('eligibility_work_experience') : NULL,
            'date_created' 	=>	date('Y-m-d H:i:s'),
            'date_updated' 	=>	date('Y-m-d H:i:s'),
            'added_by' 	=>	1,
            'updated_by' 	=>	1,
            'sop' => json_encode($sop),
            'lor' => json_encode($lor),
            'degree_substream' => $this->input->post('degree_substream'),
            'no_of_intake' => $this->input->post('no_of_intake') ? $this->input->post('no_of_intake') : NULL,
            'intake_months' => $this->input->post('intake_months') ? json_encode(explode(',', $this->input->post('intake_months'))) : NULL,
            'application_fee' => $this->input->post('application_fee') ? $this->input->post('application_fee') : NULL,
            'application_fee_amount' => $this->input->post('application_fee_amount') ? $this->input->post('application_fee_amount') : NULL,
            'application_fee_waiver_imperial' => $this->input->post('application_fee_waiver_imperial') ? $this->input->post('application_fee_waiver_imperial') : NULL,
            'cost_of_living_year' => $this->input->post('cost_of_living_year') ? $this->input->post('cost_of_living_year') : NULL,
            'test_required' => $this->input->post('test_required') ? $this->input->post('test_required') : NULL,
            'eligibility_gmat' 		=>  $this->input->post('eligibility_gmat') ? $this->input->post('eligibility_gmat') : NULL,
            'eligibility_sat' 		=>  $this->input->post('eligibility_sat') ? $this->input->post('eligibility_sat') : NULL,
            'eligibility_english_proficiency' 		=>  $this->input->post('eligibility_english_proficiency') ? $this->input->post('eligibility_english_proficiency') : NULL,
            'eligibility_pte' 		=>  $this->input->post('eligibility_pte') ? $this->input->post('eligibility_pte') : NULL,
            'eligibility_duolingo' 		=>  $this->input->post('eligibility_duolingo') ? $this->input->post('eligibility_duolingo') : NULL,
            'resume_required' 		=>  $this->input->post('resume_required') ? $this->input->post('resume_required') : NULL,
            'admission_requirements' => $this->input->post('admission_requirements'),
            'backlogs' => json_encode($backlogs),
            'drop_years' 		=>  $this->input->post('drop_years') ? $this->input->post('drop_years') : NULL,
            'scholarship_available' 		=>  $this->input->post('scholarship_available') ? $this->input->post('scholarship_available') : NULL,
            'with_15_years_education' 		=>  $this->input->post('with_15_years_education') ? $this->input->post('with_15_years_education') : NULL,
            'wes_requirement' 		=>  $this->input->post('wes_requirement') ? $this->input->post('wes_requirement') : NULL,
            'work_while_studying' 		=>  $this->input->post('work_while_studying') ? $this->input->post('work_while_studying') : NULL,
            'cooperate_internship_participation' 		=>  $this->input->post('cooperate_internship_participation') ? $this->input->post('cooperate_internship_participation') : NULL,
            'work_permit' => json_encode($workPermit),
            'start_months' => $this->input->post('start_months') ? json_encode(explode(',', $this->input->post('start_months'))) : NULL,
            'decision_months' => $this->input->post('decision_months') ? json_encode(explode(',', $this->input->post('decision_months'))) : NULL,
            'conditional_admit' 		=>  $this->input->post('conditional_admit') ? $this->input->post('conditional_admit') : NULL,
            'description' => $this->input->post('description')
			);


			/*echo '<pre>';
			print_r($data);
			echo '</pre>';
			exit(); */

	     if($this->course_model->addCourse($data))
         {
                $this->session->set_flashdata('flash_message', "Success: You have successfully added course!");
	            redirect('university/course/');
	     }
         $this->session->set_flashdata('flash_message', "Failed: Some issue occured while adding course!");
         redirect('university/course/');
	}


	 function edit()
	 {
	   $this->load->model('university/course_model');
       $courseId = $this->uri->segment('4');
       $sop = array();
       $lor = array();
       $backlogs = array();
       $workPermit = array();

       if($this->input->post('sop_needed') != ''){
           $sop = array(
               'sop_needed'        => $this->input->post('sop_needed'),
               'sop_no_of_words'   => $this->input->post('sop_no_of_words')
           );
       }

       if($this->input->post('lor_needed') != ''){
           $lor = array(
               'lor_needed'    => $this->input->post('lor_needed'),
               'lor_required'  => $this->input->post('lor_required'),
               'lor_type'      => $this->input->post('lor_type')
           );
       }

       $eligibility_gre = array(
           'quant' => $this->input->post('eligibility_gre_quant'),
           'verbal' => $this->input->post('eligibility_gre_verbal')
       );

       if($this->input->post('backlog_accepted') != ''){
           $backlogs = array(
               'backlog_accepted'        => $this->input->post('backlog_accepted'),
               'number_of_backlogs'   => $this->input->post('number_of_backlogs')
           );
       }

       if($this->input->post('work_permit_needed') != ''){
           $workPermit = array(
               'work_permit_needed'        => $this->input->post('work_permit_needed'),
               'no_of_months'   => $this->input->post('no_of_months')
           );
       }

       $data = array(
       'name' 			=>  $this->input->post('name'),
       'department_id' 		=>  $this->input->post('department'),
       'category_id' 		=>  $this->input->post('category_id') ? $this->input->post('category_id') : NULL,
       'degree_id' 		=>  $this->input->post('degree'),
       'duration' 		=>  $this->input->post('duration') ? $this->input->post('duration') : NULL,
       'deadline_link' 		=>  $this->input->post('deadline_link'),
       /*'deadline_date_spring' 		=>  $this->input->post('deadline_date_spring') ? $this->input->post('deadline_date_spring') : NULL,
       'deadline_date_fall' 		=>  $this->input->post('deadline_date_fall') ? $this->input->post('deadline_date_fall') : NULL,
       'intake' 		=>  $this->input->post('intake') ? $this->input->post('intake') : NULL,
       'total_fees' 		=>  $this->input->post('total_fees') ? $this->input->post('total_fees') : NULL,*/
       'contact_title' 		=>  $this->input->post('contact_title'),
       'contact_name' 		=>  $this->input->post('contact_name'),
       'contact_email' 		=>  $this->input->post('contact_email'),
	'eligibility_gpa'           =>  $this->input->post('eligibility_gpa') ? $this->input->post('eligibility_gpa') : NULL,
    'gpa_scale' 		=>  $this->input->post('gpa_scale') ? $this->input->post('gpa_scale') : NULL,
            'eligibility_gre' 		=>  json_encode($eligibility_gre),
            'eligibility_toefl'                 =>  $this->input->post('eligibility_toefl') ? $this->input->post('eligibility_toefl') : NULL,
            'eligibility_ielts'                 =>  $this->input->post('eligibility_ielts') ? $this->input->post('eligibility_ielts') : NULL,
            'expected_salary'                =>  $this->input->post('expected_salary') ? $this->input->post('expected_salary') : NULL,
       'eligibility_work_experience' 		=>  $this->input->post('eligibility_work_experience') ? $this->input->post('eligibility_work_experience') : NULL,
       'date_created' 	=>	date('Y-m-d H:i:s'),
       'date_updated' 	=>	date('Y-m-d H:i:s'),
       'added_by' 	=>	1,
       'updated_by' 	=>	1,
       'sop' => json_encode($sop),
       'lor' => json_encode($lor),
       'degree_substream' => $this->input->post('degree_substream'),
       'no_of_intake' => $this->input->post('no_of_intake') ? $this->input->post('no_of_intake') : NULL,
       'intake_months' => $this->input->post('intake_months') ? json_encode(explode(',', $this->input->post('intake_months'))) : NULL,
       'application_fee' => $this->input->post('application_fee') ? $this->input->post('application_fee') : NULL,
       'application_fee_amount' => $this->input->post('application_fee_amount') ? $this->input->post('application_fee_amount') : NULL,
       'application_fee_waiver_imperial' => $this->input->post('application_fee_waiver_imperial') ? $this->input->post('application_fee_waiver_imperial') : NULL,
       'cost_of_living_year' => $this->input->post('cost_of_living_year') ? $this->input->post('cost_of_living_year') : NULL,
       'test_required' => $this->input->post('test_required') ? $this->input->post('test_required') : NULL,
       'eligibility_gmat' 		=>  $this->input->post('eligibility_gmat') ? $this->input->post('eligibility_gmat') : NULL,
       'eligibility_sat' 		=>  $this->input->post('eligibility_sat') ? $this->input->post('eligibility_sat') : NULL,
       'eligibility_english_proficiency' 		=>  $this->input->post('eligibility_english_proficiency') ? $this->input->post('eligibility_english_proficiency') : NULL,
       'eligibility_pte' 		=>  $this->input->post('eligibility_pte') ? $this->input->post('eligibility_pte') : NULL,
       'eligibility_duolingo' 		=>  $this->input->post('eligibility_duolingo') ? $this->input->post('eligibility_duolingo') : NULL,
       'resume_required' 		=>  $this->input->post('resume_required') ? $this->input->post('resume_required') : NULL,
       'admission_requirements' => $this->input->post('admission_requirements'),
       'backlogs' => json_encode($backlogs),
       'drop_years' 		=>  $this->input->post('drop_years') ? $this->input->post('drop_years') : NULL,
       'scholarship_available' 		=>  $this->input->post('scholarship_available') ? $this->input->post('scholarship_available') : NULL,
       'with_15_years_education' 		=>  $this->input->post('with_15_years_education') ? $this->input->post('with_15_years_education') : NULL,
       'wes_requirement' 		=>  $this->input->post('wes_requirement') ? $this->input->post('wes_requirement') : NULL,
       'work_while_studying' 		=>  $this->input->post('work_while_studying') ? $this->input->post('work_while_studying') : NULL,
       'cooperate_internship_participation' 		=>  $this->input->post('cooperate_internship_participation') ? $this->input->post('cooperate_internship_participation') : NULL,
       'work_permit' => json_encode($workPermit),
       'start_months' => $this->input->post('start_months') ? json_encode(explode(',', $this->input->post('start_months'))) : NULL,
       'decision_months' => $this->input->post('decision_months') ? json_encode(explode(',', $this->input->post('decision_months'))) : NULL,
       'conditional_admit' 		=>  $this->input->post('conditional_admit') ? $this->input->post('conditional_admit') : NULL,
       'description' => $this->input->post('description')
       );


        $this->course_model->editCourse($courseId, $data);
        $this->session->set_flashdata('flash_message', "Success: You have modified course!");
         redirect('university/course');

	   //}
	}


	function delete()
	 {

	   $this->load->helper(array('form', 'url'));
	   $this->load->model('university/user_model');

		if($this->user_model->deleteUser($this->uri->segment('4'))=='success'){

	         $this->session->set_flashdata('flash_message', "Success: You have deleted University!");
	         redirect('university/course');
	    }
	}


	function multidelete(){

	    $this->load->helper(array('form', 'url'));

		$this->load->model('university/user_model');

    $data = [ "status"=> 0 ];
		$array = $this->input->post('chk');
		foreach($array as $id):

		$this->course_model->editCourse($id, $data);

		endforeach;
		$this->session->set_flashdata('flash_message', "You have deleted User!");
		 redirect('university/course');


	}

  function selectedoptions(){

	    $this->load->helper(array('form', 'url'));

		$this->load->model('university/user_model');
    $this->load->model('university/course_model');
    $this->load->model('university/department_model');

    $userdata=$this->session->userdata('user');
    if(empty($userdata)){  redirect('common/login'); }

		$array = $this->input->post('chk');
    $optionsId = $this->input->post('options_id') ? $this->input->post('options_id') : 0;
    $assignId = $this->input->post('assign_id') !='' ? $this->input->post('assign_id') : 0;
    $selectedDepartmentId = $this->input->post('selected_department_id');

    if($optionsId == "ASSIGN" && $assignId == 0){
      $this->session->set_flashdata('flash_message', "For Assign You Have To Select Department !");
      redirect('university/course/coursebydepartmentid?department_id='.$selectedDepartmentId);
    }

    if($optionsId == "DELETEDEPARTMENT"){
      $data = ["status" => 0];
      $this->department_model->editDepartment($selectedDepartmentId, $data);
      $this->session->set_flashdata('flash_message', "Department Deleted!");

    } else if($optionsId == "ASSIGN"){
      $data = ["department_id" => $assignId];

      foreach($array as $id){
  		$this->course_model->editCourse($id, $data);
      }

      $this->session->set_flashdata('flash_message', "You have Assigned Course!");

    } else {
      $data = ["status" => 0];

      foreach($array as $id){
  		$this->course_model->editCourse($id, $data);
    }

      $this->session->set_flashdata('flash_message', "You have Deleted Course!");
    }

		 redirect('university/course/coursebydepartmentid?department_id='.$selectedDepartmentId);

	}

	function trash()
	{
	     $this->load->helper('cookie_helper');
         $this->load->model('university/user_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }

		$cond_user1 = array('user_master.status ' => '5');
		$cond_user2 = array();
        $this->outputData['users'] = $this->user_model->getUser($cond_user1,$cond_user2);
		$this->outputData['view'] = 'list';

        $this->render_page('templates/university/trash_admins',$this->outputData);

    }

	function deletetrash()
	 {

	   $this->load->helper(array('form', 'url'));
	   $this->load->model('university/user_model');

		if($this->user_model->deleteTrash($this->uri->segment('4'))=='success'){

	         $this->session->set_flashdata('flash_message', "Success: You have deleted User!");
	         redirect('university/university/trash');
	    }
	}

	function multidelete_trash(){

	    $this->load->helper(array('form', 'url'));

		$this->load->model('university/user_model');

		$array = $this->input->post('chk');
		foreach($array as $id):

		$this->user_model->deleteTrash($id);

		endforeach;
		$this->session->set_flashdata('flash_message', "You have deleted User!");
		 redirect('university/university/trash');


	}






}//End  Home Class

?>
