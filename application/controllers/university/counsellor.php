<?php

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

require_once APPPATH . 'libraries/Mail/sMail.php';

class Counsellor extends MY_Controller
{
    public $outputData;
    public $loggedInUser;

    function __construct()
    {
        parent::__construct();

        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		    $this->load->library('form_validation');

        $this->load->model('redi_model');
        $this->load->model('university/counsellor_model');
        $this->load->model('webinar/common_model');
    }

    function index()
    {
        $this->load->helper('cookie_helper');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('user/account');
            exit();
        }
        $this->render_page('templates/university/counsellor_list',$this->outputData);
    }

    function filter()
    {
        $this->load->helper('cookie_helper');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('user/account');
            exit();
        }

        $condition_1 = array('user_master.status !=' => '5');
        $condition_2 = array('user_master.type' => '6');

        $representatives = $this->counsellor_model->getCounsellorList($condition_1, $condition_2);

        header('Content-Type: application/json');
        echo json_encode($representatives);
    }


    function communicatiohistory()
    {
        $this->load->helper('cookie_helper');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('user/account');
            exit();
        }

        $this->outputData['entity_id'] =  $this->uri->segment('4');

        $this->render_page('templates/university/communication_list',$this->outputData);
    }

    function chfilter() {
        $this->load->helper('cookie_helper');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('user/account');
            exit();
        }

        $userId = $userdata['id'];
        $counsellorId = $this->uri->segment('4') ? $this->uri->segment('4') : 0;

      /*  $condition_1 = array('comments.added_by' => $userId);
        $condition_2 = array('comments.entity_id' => $counsellorId);
        $condition_2 = array('comments.added_by' => $counsellorId, 'comments.entity_id' => $userId);*/

        $communications = $this->counsellor_model->getCommunnicationAllList($userId, $counsellorId);

        header('Content-Type: application/json');
        echo json_encode($communications);
    }

    function communicationstudentlist() {
        $this->load->helper('cookie_helper');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('user/account');
            exit();
        }

        $userId = $userdata['id'];
        $counsellorId = $this->uri->segment('4') ? $this->uri->segment('4') : 0;
        $studentId = $this->uri->segment('5') ? $this->uri->segment('5') : 0;

        $communications = $this->counsellor_model->getCommunnicationStudentList($userId, $counsellorId, $studentId);

        header('Content-Type: application/json');
        echo json_encode($communications);
    }

    function insertComments(){

      $this->load->helper('cookie_helper');
      $userdata=$this->session->userdata('user');
      if(empty($userdata))
      {
          echo "INVALIDLOGIN";
          //redirect('user/account');
          exit();
      }

      $entity_id = $this->input->post('entity_id');
      $entity_type = $this->input->post('type') ? $this->input->post('type') : 'UNIVERSITY';
      $student_id = $this->input->post('student_login_id') ? $this->input->post('student_login_id') : NULL;
      $added_by = $userdata['id'];
      $comments = $this->input->post('comment');

      $postData = [
        "entity_id" => $entity_id,
        "entity_type" => $entity_type,
        "student_login_id" => $student_id,
        "added_by" => $added_by,
        "comment" => $comments
      ];

      $insertCommentObj = $this->common_model->post($postData,"comments");

      echo "SUCCESS";

    }

    function inserttComments(){

      $this->load->helper('cookie_helper');
      $userdata=$this->session->userdata('user');
      if(empty($userdata))
      {
          echo "INVALIDLOGIN";
          //redirect('user/account');
          exit();
      }

      $entity_id = $this->input->post('entity_id');
      $entity_type = $this->input->post('type') ? $this->input->post('type') : 'STUDENT';
      $student_id = $this->input->post('student_login_id') ? $this->input->post('student_login_id') : NULL;
      $added_by = $userdata['id'];
      $comments = $this->input->post('comment');

      $postData = [
        "entity_id" => $entity_id,
        "entity_type" => $entity_type,
        "student_login_id" => $student_id,
        "added_by" => $added_by,
        "comment" => $comments
      ];

      $insertCommentObj = $this->common_model->post($postData,"comments");

      echo "SUCCESS";

    }


}
?>
