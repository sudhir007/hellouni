<?php

// ini_set('display_errors', '1');
// ini_set('display_startup_errors', '1');
// error_reporting(E_ALL);

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com


 */

class Campus extends MY_Controller {



	//Global variable

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;



    /**

	 * Constructor

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

 //    function index()
	// {
	// 	$this->load->helper('cookie_helper');
	// 	$this->render_page('templates/university/campus');
	// }

	function index()
	{
	     $this->load->helper('cookie_helper');
         $this->load->model('university/campus_model');
         $this->load->model('filter_model');

		   $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }

         if($userdata['university_id'])
        {
            $this->outputData['campuses'] = $this->campus_model->getCampusesByUniversity($userdata['university_id']);
            $this->outputData['university_list'] = $this->filter_model->getAllUniversityById($userdata['university_id']);
        }
        else
        {
            $this->outputData['campuses'] = $this->campus_model->getAllCampuses();
            $this->outputData['university_list'] = $this->filter_model->getAllUniversityById();
        }
		$this->outputData['view'] = 'list';

		/*echo '<pre>';
		print_r($this->outputData);
		echo '</pre>';
		exit();*/

        $this->render_page('templates/university/campus',$this->outputData);

    }

    function filter() {

  	     $this->load->helper('cookie_helper');
         $this->load->model('university/course_model');
         $this->load->model('filter_model');

  		   $userdata=$this->session->userdata('user');
  	     if(empty($userdata)){
           redirect('common/login');
         }

          $selectedUniversityId = isset($_POST['university_id']) &&  $_POST['university_id'] != "" ? $_POST['university_id'] : 0;
          $selectedCampusId = isset($_POST['campus_id']) && $_POST['campus_id'] !="" ? $_POST['campus_id'] : 0;

          $allUniversityList = [];
          $allCampusList = [];

          if($userdata['university_id']) {
             $allUniversityList = $this->filter_model->getAllUniversityById($userdata['university_id']);
         } else {
             $allUniversityList = $this->filter_model->getAllUniversityById();
         }


          if($selectedUniversityId){
            $allCampusList = $this->filter_model->getAllCampusesByUniversityId($selectedUniversityId);
          }


          $this->outputData['campuses'] = $this->filter_model->getFilterCampus($selectedUniversityId,$selectedCampusId);


          $this->outputData['selected_university_id'] = $selectedUniversityId;
          $this->outputData['university_list'] = $allUniversityList;
          $this->outputData['selected_campus_id'] = $selectedCampusId;
          $this->outputData['campus_list'] = $allCampusList;

  		$this->outputData['view'] = 'list';

  		/*echo '<pre>';
  		print_r($this->outputData);
  		echo '</pre>';
  		exit();*/

          $this->render_page('templates/university/campus',$this->outputData);

      }

	 function form()
	 {
	     $this->load->helper('cookie_helper');
         $this->load->model('university/campus_model');
         $this->load->model('university/university_model');
         $this->load->model('filter_model');
		 //$this->load->model('location/country_model');
		 //$this->load->model('location/city_model');
		 //$this->load->model('location/state_model');


		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }

		 if($this->uri->segment('4')){
         $campusData = $this->campus_model->getCampusByIdNew($this->uri->segment('4'));

	    $this->outputData['id'] 				= $campusData->id;
		$this->outputData['name'] 				= $campusData->name;
		$this->outputData['university'] 				= $campusData->university_id;
        $this->outputData['size'] 			= $campusData->size;
        $this->outputData['size_unit'] 			= $campusData->size_unit;
        $this->outputData['state'] 			= $campusData->state;
		$this->outputData['city'] 			= $campusData->city;
		$this->outputData['timezone'] 		= $campusData->timezone;
		$this->outputData['locale'] 			= $campusData->locale;
        $this->outputData['accomodation_type'] 			= $campusData->accomodation_type;

        $this->outputData['selected_university_id'] = $campusData->university_id;

		}
    if($userdata['university_id']) {
       $allUniversityList = $this->filter_model->getAllUniversityById($userdata['university_id']);
   } else {
       $allUniversityList = $this->filter_model->getAllUniversityById();
   }

        $this->outputData['university_id'] = $userdata['university_id'];
        $this->outputData['university_list'] = $allUniversityList;
        $this->render_page('templates/university/campus_form',$this->outputData);

     }

	 function create()
	 {
         $userdata=$this->session->userdata('user');
         if(empty($userdata)){  redirect('common/login'); }

         if(!$this->input->post('name') || (!$this->input->post('university') && $userdata['type'] != 3))
         {
             $this->session->set_flashdata('flash_message', "Error: Please fill all mandatory fields!");
    	     redirect('university/campus/form');
         }
	   //die('+++++');
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('university/campus_model');


		 	$data = array(
        	'name' 			=>  $this->input->post('name'),
        	'university_id' 		=>  $this->input->post('university') ? $this->input->post('university') : $userdata['university_id'],
        	'state' 		=>  $this->input->post('state'),
			'city' 		=>	$this->input->post('city'),
			'size' 			=>	$this->input->post('size') ? $this->input->post('size') : NULL,
            'size_unit' 			=>	$this->input->post('size_unit') ? $this->input->post('size_unit') : NULL,
			'timezone' 		=>	$this->input->post('timezone'),
			'locale' 			=>	$this->input->post('locale'),
            'accomodation_type' 			=>	$this->input->post('accomodation_type'),
            'date_created' 	=>	date('Y-m-d H:i:s'),
            'date_updated' 	=>	date('Y-m-d H:i:s'),
            'added_by' 	=>	1,
            'updated_by' 	=>	1
			);


			/*echo '<pre>';
			print_r($data);
			echo '</pre>';
			exit(); */

	     if($this->campus_model->addCampus($data))
         {
                $this->session->set_flashdata('flash_message', "Success: You have successfully added campus!");
	            redirect('university/campus/');
	     }
         $this->session->set_flashdata('flash_message', "Failed: Some issue occured while adding campus!");
         redirect('university/campus/');
	}


	//  function edit()
	//  {
	//    $this->load->model('user/campus_model');
 //       $campusId = $this->uri->segment('4');
 //        $data = array(
 //        'name' 			=>  $this->input->post('name'),
 //        'university_id' 		=>  $this->input->post('university'),
 //        'state' 		=>  $this->input->post('state'),
 //        'city' 		=>	$this->input->post('city'),
 //        'size' 			=>	$this->input->post('size') ? $this->input->post('size') : NULL,
 //        'size_unit' 			=>	$this->input->post('size_unit') ? $this->input->post('size_unit') : NULL,
 //        'timezone' 		=>	$this->input->post('timezone'),
 //        'locale' 			=>	$this->input->post('locale'),
 //        'accomodation_type' 			=>	$this->input->post('accomodation_type'),
 //        'date_created' 	=>	date('Y-m-d H:i:s'),
 //        'date_updated' 	=>	date('Y-m-d H:i:s'),
 //        'added_by' 	=>	1,
 //        'updated_by' 	=>	1
 //        );

 //        $this->campus_model->editCampus($campusId, $data);
 //        $this->session->set_flashdata('flash_message', "Success: You have modified Campus!");
 //         redirect('user/campus');

	//    //}
	// }


	// function delete()
	//  {

	//    $this->load->helper(array('form', 'url'));
	//    $this->load->model('user/user_model');

	// 	if($this->user_model->deleteUser($this->uri->segment('4'))=='success'){

	//          $this->session->set_flashdata('flash_message', "Success: You have deleted University!");
	//          redirect('user/campus');
	//     }
	// }


	// function multidelete(){

	//     $this->load->helper(array('form', 'url'));

	// 	$this->load->model('user/user_model');

	// 	$array = $this->input->post('chk');
	// 	foreach($array as $id):

	// 	$this->user_model->deleteUser($id);

	// 	endforeach;
	// 	$this->session->set_flashdata('flash_message', "You have deleted User!");
	// 	 redirect('user/campus');


	// }

	// function trash()
	// {
	//      $this->load->helper('cookie_helper');
 //         $this->load->model('user/user_model');
	// 	 $userdata=$this->session->userdata('user');
	//      if(empty($userdata)){  redirect('common/login'); }

	// 	$cond_user1 = array('user_master.status ' => '5');
	// 	$cond_user2 = array();
 //        $this->outputData['users'] = $this->user_model->getUser($cond_user1,$cond_user2);
	// 	$this->outputData['view'] = 'list';

 //        $this->render_page('templates/user/trash_admins',$this->outputData);

 //    }

	// function deletetrash()
	//  {

	//    $this->load->helper(array('form', 'url'));
	//    $this->load->model('user/user_model');

	// 	if($this->user_model->deleteTrash($this->uri->segment('4'))=='success'){

	//          $this->session->set_flashdata('flash_message', "Success: You have deleted User!");
	//          redirect('user/university/trash');
	//     }
	// }

	// function multidelete_trash(){

	//     $this->load->helper(array('form', 'url'));

	// 	$this->load->model('user/user_model');

	// 	$array = $this->input->post('chk');
	// 	foreach($array as $id):

	// 	$this->user_model->deleteTrash($id);

	// 	endforeach;
	// 	$this->session->set_flashdata('flash_message', "You have deleted User!");
	// 	 redirect('user/university/trash');


	// }






}//End  Home Class

?>
