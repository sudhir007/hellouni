<?php
require_once APPPATH . 'libraries/Mail/sMail.php';

class Alumni extends MY_Controller
{
    public $outputData;
    public $loggedInUser;

    function __construct()
    {
        parent::__construct();

        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');

        $this->load->model('location/country_model');
        $this->load->model('webinar/webinar_model');
        $this->load->model('webinar/common_model');
        $this->load->model('counsellor/counsellor_model');
        $this->load->model('course/course_model');
		$this->load->model('course/degree_model');
        $this->load->model('user/user_model');
        $this->load->model('tokbox_model');
        $this->load->model('redi_model');
        $this->load->model('university/university_model');
    }

    function index()
    {
        $this->load->helper('cookie_helper');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('user/account');
            exit();
        }
        $this->render_page('templates/university/alumni_list',$this->outputData);
    }

    function filter()
    {
        $this->load->helper('cookie_helper');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('user/account');
            exit();
        }
        $universityId = $userdata['university_id'];
        $universityLoginId = $userdata['id'];

        $alumni = $this->university_model->getAlumniByUniversity($universityId);

        header('Content-Type: application/json');
        echo json_encode($alumni);
    }

    function add()
    {
        $this->load->helper('cookie_helper');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('user/account');
            exit();
        }
        $this->render_page('templates/university/alumni_form',$this->outputData);
    }

    function create()
    {
        $this->load->helper('cookie_helper');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('user/account');
            exit();
        }

        $data = array(
            'name' 			=>  $this->input->post('name'),
            'email' 		=>  $this->input->post('email'),
            'username' 		=>  $this->input->post('username'),
            'password' 		=>  md5($this->input->post('password')),
            'type' 			=>  '4',
            'createdate' 	=>  date('Y-m-d H:i:s'),
            'university'	=>	$userdata['university_id'],
            'status' 		=>	1,
            'view' 			=>	1,
            'create' 		=>	1,
            'edit' 			=>	1,
            'del' 			=>	1,
            'notification' 	=>	1
        );

        $userExist = $this->university_model->checkUserByUsername($this->input->post('username'));
        if($userExist)
        {
            echo 'The username already exists!';
            exit;
        }

        $alumnusId = $this->university_model->addAlumnus($data);

        if($alumnusId)
        {
            if($this->input->post('notification')==1)
            {
                $userEmail = $this->input->post('email');
                $name = $this->input->post('name');
                $username = $this->input->post('username');
                $password = $this->input->post('password');

                $mailSubject = "Notification on creation of your Alumnus Account Under HelloUni";
                $mailTemplate = "Hi $name,

                Your Alumnus Account has been sucessfully created by Super Admin under HelloUni. Your login credential is as follows

                Username: $username
                Password: $password

                Kindly login to your account and explore the options.

                <a href='https://www.hellouni.org/admin'>https://www.hellouni.org/admin</a>

                <b>Thanks and Regards,
                HelloUni Coordinator
                +91 81049 09690</b>

                <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

                $ccMailList = '';
                $mailAttachments = '';

                $sendMail = sMail($userEmail, $userName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

            }
            echo 'SUCCESS';
        }
    }

    function update()
    {
        $this->load->helper('cookie_helper');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('user/account');
            exit();
        }
        if(!$this->uri->segment('4'))
        {
            redirect('university/alumni');
            exit();
        }

        $alumnusId = $this->uri->segment('4');
        $condition = ['user_master.id' => $alumnusId];
        $alumnusDetail = $this->user_model->getUserByid($condition);
        $this->outputData['profile_detail'] = $alumnusDetail;

        $this->render_page('templates/university/alumni_form',$this->outputData);
    }

    function edit()
    {
        $this->load->helper('cookie_helper');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('user/account');
            exit();
        }

        $data = array(
            'name' 			=>  $this->input->post('name'),
            'email' 		=>  $this->input->post('email')
        );
        if($this->input->post('password'))
        {
            $data['password'] = md5($this->input->post('password'));
        }
        $alumnusId = $this->input->post('id');

        $this->university_model->updateAlumnus($alumnusId, $data);
        echo 'SUCCESS';

        // TODO check notification enabled or not then send email
        /*if($alumnusId)
        {
            if($this->input->post('notification')==1)
            {
                $userEmail = $this->input->post('email');
                $name = $this->input->post('name');
                $username = $this->input->post('username');
                $password = $this->input->post('password');

                $mailSubject = "Notification on creation of your Alumnus Account Under HelloUni";
                $mailTemplate = "Hi $name,

                Your Alumnus Account has been sucessfully created by Super Admin under HelloUni. Your login credential is as follows

                Username: $username
                Password: $password

                Kindly login to your account and explore the options.

                <a href='https://www.hellouni.org/admin'>https://www.hellouni.org/admin</a>

                <b>Thanks and Regards,
                HelloUni Coordinator
                +91 81049 09690</b>

                <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

                $ccMailList = '';
                $mailAttachments = '';

                $sendMail = sMail($userEmail, $userName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

            }
            echo 'SUCCESS';
        }*/
    }
}
?>
