<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com


 */

class Department extends MY_Controller {



	//Global variable

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;



    /**

	 * Constructor

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }


	function index()
	{
	     $this->load->helper('cookie_helper');
         $this->load->model('university/department_model');
         $this->load->model('filter_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }

         if($userdata['university_id'])
        {
            $this->outputData['departments'] = $this->department_model->getAllDepartmentsByUniversity($userdata['university_id']);
            $this->outputData['university_list'] = $this->filter_model->getAllUniversityById($userdata['university_id']);
        }
        else
        {
            $this->outputData['departments'] = $this->department_model->getAllDepartments();
            $this->outputData['university_list'] = $this->filter_model->getAllUniversityById();
        }


		$this->outputData['view'] = 'list';

		/*echo '<pre>';
		print_r($this->outputData);
		echo '</pre>';
		exit();*/

        $this->render_page('templates/university/department',$this->outputData);

    }

    function filter() {

  	     $this->load->helper('cookie_helper');
         $this->load->model('university/course_model');
         $this->load->model('filter_model');

  		   $userdata=$this->session->userdata('user');
  	     if(empty($userdata)){
           //redirect('common/login');
         }

          $selectedUniversityId = isset($_POST['university_id']) &&  $_POST['university_id'] != "" ? $_POST['university_id'] : 0;
          $selectedCampusId = isset($_POST['campus_id']) && $_POST['campus_id'] !="" ? $_POST['campus_id'] : 0;
          $selectedCollegeId = isset($_POST['college_id']) && $_POST['college_id'] !="" ? $_POST['college_id'] : 0;
          $selectedDepartmentId = isset($_POST['department_id']) && $_POST['department_id'] !="" ? $_POST['department_id'] : 0;

          $allUniversityList = [];
          $allCampusList = [];
          $allCollegeList = [];
          $allDepartmentList = [];

          if($userdata['university_id']) {
             $allUniversityList = $this->filter_model->getAllUniversityById($userdata['university_id']);
         } else {
             $allUniversityList = $this->filter_model->getAllUniversityById();
         }


          if($selectedUniversityId){
            $allCampusList = $this->filter_model->getAllCampusesByUniversityId($selectedUniversityId);
          }

          if($selectedCampusId){
            $allCollegeList = $this->filter_model->getAllCollegeByCampusId($selectedCampusId);
          }

          if($selectedCollegeId){
            $allDepartmentList = $this->filter_model->getAllDepartmentByCollegeId($selectedCollegeId);
          }


          $this->outputData['departments'] = $this->filter_model->getFilterDepartment($selectedUniversityId,$selectedCampusId,$selectedCollegeId,$selectedDepartmentId);


          $this->outputData['selected_university_id'] = $selectedUniversityId;
          $this->outputData['university_list'] = $allUniversityList;
          $this->outputData['selected_campus_id'] = $selectedCampusId;
          $this->outputData['campus_list'] = $allCampusList;
          $this->outputData['selected_college_id'] = $selectedCollegeId;
          $this->outputData['college_list'] = $allCollegeList;
          $this->outputData['selected_department_id'] = $selectedDepartmentId;
          $this->outputData['department_list'] = $allDepartmentList;


  		$this->outputData['view'] = 'list';

  		/*echo '<pre>';
  		print_r($this->outputData);
  		echo '</pre>';
  		exit();*/

          $this->render_page('templates/university/department',$this->outputData);

      }

	 function form()
	 {
	     $this->load->helper('cookie_helper');
         $this->load->model('university/department_model');
         $this->load->model('university/college_model');
         $this->load->model('filter_model');
		 //$this->load->model('location/country_model');
		 //$this->load->model('location/city_model');
		 //$this->load->model('location/state_model');


		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }
         $this->outputData['enrollment_count'] = 1;
         $this->outputData['ranking_count']  = 1;
         $this->outputData['application_deadline_usa_count'] = 1;
         $this->outputData['application_deadline_international_count']   = 1;
         $this->outputData['application_fees_usa_count'] = 1;
         $this->outputData['application_fees_international_count']   = 1;
         $this->outputData['average_gre_count']   = 1;

		 if($this->uri->segment('4')){
         $departmentData = $this->department_model->getDepartmentByIdNew($this->uri->segment('4'));

	    $this->outputData['id'] 				= $departmentData->id;
		$this->outputData['name'] 				= $departmentData->name;
        $this->outputData['contact_title'] 				= $departmentData->contact_title;
        $this->outputData['contact_name'] 				= $departmentData->contact_name;
        $this->outputData['contact_email'] 				= $departmentData->contact_email;
		$this->outputData['college'] 				= $departmentData->college_id;
        $this->outputData['enrollment'] 				= json_decode($departmentData->enrollment, true);
		$this->outputData['ranking'] 				= json_decode($departmentData->ranking, true);
		$this->outputData['college_research_expenditure'] 				= $departmentData->college_research_expenditure;
        $this->outputData['research_expenditure_per_faculty'] 				= $departmentData->research_expenditure_per_faculty;
        $this->outputData['total_enrollment'] 				= $departmentData->total_enrollment;
		$this->outputData['international_enrollment'] 				= $departmentData->international_enrollment;
        $this->outputData['admissions_department_name'] 				= $departmentData->admissions_department_name;
        $this->outputData['admissions_department_url'] 				= $departmentData->admissions_department_url;
        $this->outputData['department_phone'] 				= $departmentData->department_phone;
        $this->outputData['department_email'] 				= $departmentData->department_email;
        $this->outputData['noof_faculty'] 				= $departmentData->noof_faculty;
		$this->outputData['application_deadline_usa'] 				= json_decode($departmentData->application_deadline_usa, true);
        $this->outputData['application_deadline_international'] 				= json_decode($departmentData->application_deadline_international, true);
		$this->outputData['application_fees_usa'] 				= json_decode($departmentData->application_fees_usa, true);
		$this->outputData['application_fees_international'] 				= json_decode($departmentData->application_fees_international, true);
        $this->outputData['average_gre'] 				= json_decode($departmentData->average_gre, true);
        $this->outputData['enrollment_count'] = count($this->outputData['enrollment']);
        $this->outputData['ranking_count']  = count($this->outputData['ranking']);
        $this->outputData['application_deadline_usa_count'] = count($this->outputData['application_deadline_usa']);
        $this->outputData['application_deadline_international_count']   = count($this->outputData['application_deadline_international']);
        $this->outputData['application_fees_usa_count'] = count($this->outputData['application_fees_usa']);
        $this->outputData['application_fees_international_count']   = count($this->outputData['application_fees_international']);
        $this->outputData['average_gre_count']   = count($this->outputData['average_gre']);

        $this->outputData['selected_university_id'] = $departmentData->university_id;
        $this->outputData['selected_campus_id'] = $departmentData->campus_id;
        $this->outputData['selected_college_id'] = $departmentData->college_id;

        $selectedUniversityId = $departmentData->university_id ? $departmentData->university_id : 0;
        $selectedCampusId = $departmentData->campus_id ? $departmentData->campus_id : 0;
        $selectedCollegeId = $departmentData->college_id ? $departmentData->college_id : 0;

        if($selectedUniversityId){
          $allCampusList = $this->filter_model->getAllCampusesByUniversityId($selectedUniversityId);
        }

        if($selectedCampusId){
          $allCollegeList = $this->filter_model->getAllCollegeByCampusId($selectedCampusId);
        }

       $this->outputData['campus_list'] = $allCampusList;
       $this->outputData['college_list'] = $allCollegeList;

		}

    if($userdata['university_id']) {
       $allUniversityList = $this->filter_model->getAllUniversityById($userdata['university_id']);
   } else {
       $allUniversityList = $this->filter_model->getAllUniversityById();
   }

        //$this->outputData['colleges'] = $allColleges;
        $this->outputData['university_list'] = $allUniversityList;

        $this->render_page('templates/university/department_form',$this->outputData);

     }

	 function create()
	 {
         if(!$this->input->post('name') || !$this->input->post('college'))
         {
             $this->session->set_flashdata('flash_message', "Error: Please fill all mandatory fields!");
    	     redirect('university/department/form');
         }
	   //die('+++++');
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('university/department_model');

       $enrollment = array();
       $ranking = array();
       $applicationDeadlineUSA = array();
       $applicationDeadlineInternational = array();
       $applicationFeesUSA = array();
       $applicationFeesInternational = array();
       $averageGRE = array();

       foreach($this->input->post('enrollment_year') AS $index => $enrollmentYear)
       {
           $enrollment[$index]['year'] = $enrollmentYear;
           $enrollment[$index]['number'] = $this->input->post('enrollment_numbers')[$index];
       }

       foreach($this->input->post('ranking_year') AS $index => $rankingYear)
       {
           $ranking[$index]['year'] = $rankingYear;
           $ranking[$index]['by_whom'] = $this->input->post('ranking_by_whom')[$index];
           $ranking[$index]['ranking'] = $this->input->post('ranking')[$index];
       }

       foreach($this->input->post('deadline_usa_year') AS $index => $deadlineYear)
       {
           $applicationDeadlineUSA[$index]['year'] = $deadlineYear;
           $applicationDeadlineUSA[$index]['date'] = $this->input->post('deadline_usa_calendar')[$index];
       }

       foreach($this->input->post('deadline_international_year') AS $index => $deadlineYear)
       {
           $applicationDeadlineInternational[$index]['year'] = $deadlineYear;
           $applicationDeadlineInternational[$index]['date'] = $this->input->post('deadline_international_calendar')[$index];
       }

       foreach($this->input->post('application_fees_usa_currency') AS $index => $feesCurrency)
       {
           $applicationFeesUSA[$index]['currency'] = $feesCurrency;
           $applicationFeesUSA[$index]['amount'] = $this->input->post('application_fees_usa_amount')[$index];
       }

       foreach($this->input->post('application_fees_international_currency') AS $index => $feesCurrency)
       {
           $applicationFeesInternational[$index]['currency'] = $feesCurrency;
           $applicationFeesInternational[$index]['amount'] = $this->input->post('application_fees_international_amount')[$index];
       }

       foreach($this->input->post('average_gre_quant') AS $index => $quant)
       {
           $averageGRE[$index]['quant'] = $quant;
           $averageGRE[$index]['verbal'] = $this->input->post('average_gre_verbal')[$index];
           $averageGRE[$index]['awa'] = $this->input->post('average_gre_awa')[$index];
       }


		 	$data = array(
        	'name' 			=>  $this->input->post('name'),
            'contact_title' 			=>  $this->input->post('contact_title'),
            'contact_name' 			=>  $this->input->post('contact_name'),
            'contact_email' 			=>  $this->input->post('contact_email'),
        	'college_id' 		=>  $this->input->post('college'),
            'enrollment'    => json_encode($enrollment),
            'ranking'       => json_encode($ranking),
            'application_deadline_usa' => json_encode($applicationDeadlineUSA),
            'application_deadline_international' => json_encode($applicationDeadlineInternational),
            'application_fees_usa'  => json_encode($applicationFeesUSA),
            'application_fees_international' => json_encode($applicationFeesInternational),
            'college_research_expenditure'  => $this->input->post('college_research_expenditure') ? $this->input->post('college_research_expenditure') : NULL,
            'total_enrollment'  => $this->input->post('total_enrollment') ? $this->input->post('total_enrollment') : NULL,
            'international_enrollment'  => $this->input->post('international_enrollment') ? $this->input->post('international_enrollment') : NULL,
            'research_expenditure_per_faculty'  => $this->input->post('research_expenditure_per_faculty') ? $this->input->post('research_expenditure_per_faculty') : NULL,
            'admissions_department_name' => $this->input->post('admissions_department_name'),
            'department_phone' => $this->input->post('department_phone') ? $this->input->post('department_phone') : NULL,
            'department_email' => $this->input->post('department_email'),
            'admissions_department_url' => $this->input->post('admissions_department_url'),
            'noof_faculty' => $this->input->post('noof_faculty') ? $this->input->post('noof_faculty') : NULL,
            'average_gre'       => json_encode($averageGRE),
            'date_created' 	=>	date('Y-m-d H:i:s'),
            'date_updated' 	=>	date('Y-m-d H:i:s'),
            'added_by' 	=>	1,
            'updated_by' 	=>	1
			);


			/*echo '<pre>';
			print_r($data);
			echo '</pre>';
			exit();*/

	     if($this->department_model->addDepartment($data))
         {
                $this->session->set_flashdata('flash_message', "Success: You have successfully added department!");
	            redirect('university/department/');
	     }
         $this->session->set_flashdata('flash_message', "Failed: Some issue occured while adding department!");
         redirect('university/department/');
	}


	 function edit()
	 {
	   $this->load->model('university/department_model');

       $enrollment = array();
       $ranking = array();
       $applicationDeadlineUSA = array();
       $applicationDeadlineInternational = array();
       $applicationFeesUSA = array();
       $applicationFeesInternational = array();
       $averageGRE = array();

       foreach($this->input->post('enrollment_year') AS $index => $enrollmentYear)
       {
           $enrollment[$index]['year'] = $enrollmentYear;
           $enrollment[$index]['number'] = $this->input->post('enrollment_numbers')[$index];
       }

       foreach($this->input->post('ranking_year') AS $index => $rankingYear)
       {
           $ranking[$index]['year'] = $rankingYear;
           $ranking[$index]['by_whom'] = $this->input->post('ranking_by_whom')[$index];
           $ranking[$index]['ranking'] = $this->input->post('ranking')[$index];
       }

       foreach($this->input->post('deadline_usa_year') AS $index => $deadlineYear)
       {
           $applicationDeadlineUSA[$index]['year'] = $deadlineYear;
           $applicationDeadlineUSA[$index]['date'] = $this->input->post('deadline_usa_calendar')[$index];
       }

       foreach($this->input->post('deadline_international_year') AS $index => $deadlineYear)
       {
           $applicationDeadlineInternational[$index]['year'] = $deadlineYear;
           $applicationDeadlineInternational[$index]['date'] = $this->input->post('deadline_international_calendar')[$index];
       }

       foreach($this->input->post('application_fees_usa_currency') AS $index => $feesCurrency)
       {
           $applicationFeesUSA[$index]['currency'] = $feesCurrency;
           $applicationFeesUSA[$index]['amount'] = $this->input->post('application_fees_usa_amount')[$index];
       }

       foreach($this->input->post('application_fees_international_currency') AS $index => $feesCurrency)
       {
           $applicationFeesInternational[$index]['currency'] = $feesCurrency;
           $applicationFeesInternational[$index]['amount'] = $this->input->post('application_fees_international_amount')[$index];
       }

       foreach($this->input->post('average_gre_quant') AS $index => $quant)
       {
           $averageGRE[$index]['quant'] = $quant;
           $averageGRE[$index]['verbal'] = $this->input->post('average_gre_verbal')[$index];
           $averageGRE[$index]['awa'] = $this->input->post('average_gre_awa')[$index];
       }

       $departmentId = $this->uri->segment('4');
        $data = array(
        'name' 			=>  $this->input->post('name'),
        'contact_title' 			=>  $this->input->post('contact_title'),
        'contact_name' 			=>  $this->input->post('contact_name'),
        'contact_email' 			=>  $this->input->post('contact_email'),
        'college_id' 		=>  $this->input->post('college'),
        'enrollment'    => json_encode($enrollment),
        'ranking'       => json_encode($ranking),
        'application_deadline_usa' => json_encode($applicationDeadlineUSA),
        'application_deadline_international' => json_encode($applicationDeadlineInternational),
        'application_fees_usa'  => json_encode($applicationFeesUSA),
        'application_fees_international' => json_encode($applicationFeesInternational),
        'college_research_expenditure'  => $this->input->post('college_research_expenditure'),
        'total_enrollment'  => $this->input->post('total_enrollment'),
        'international_enrollment'  => $this->input->post('international_enrollment'),
        'research_expenditure_per_faculty'  => $this->input->post('research_expenditure_per_faculty'),
        'admissions_department_name' => $this->input->post('admissions_department_name'),
        'department_phone' => $this->input->post('department_phone'),
        'department_email' => $this->input->post('department_email'),
        'admissions_department_url' => $this->input->post('admissions_department_url'),
        'noof_faculty' => $this->input->post('noof_faculty'),
        'average_gre'       => json_encode($averageGRE),
        'date_created' 	=>	date('Y-m-d H:i:s'),
        'date_updated' 	=>	date('Y-m-d H:i:s'),
        'added_by' 	=>	1,
        'updated_by' 	=>	1
        );

        $this->department_model->editDepartment($departmentId, $data);
        $this->session->set_flashdata('flash_message', "Success: You have modified department!");
         redirect('university/department');

	   //}
	}


	function delete()
	 {

	   $this->load->helper(array('form', 'url'));
	   $this->load->model('user/user_model');

		if($this->user_model->deleteUser($this->uri->segment('4'))=='success'){

	         $this->session->set_flashdata('flash_message', "Success: You have deleted University!");
	         redirect('university/department');
	    }
	}


	function multidelete(){

	    $this->load->helper(array('form', 'url'));

		$this->load->model('user/user_model');

		$array = $this->input->post('chk');
		foreach($array as $id):

		$this->user_model->deleteUser($id);

		endforeach;
		$this->session->set_flashdata('flash_message', "You have deleted User!");
		 redirect('university/department');


	}

	function trash()
	{
	     $this->load->helper('cookie_helper');
         $this->load->model('user/user_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }

		$cond_user1 = array('user_master.status ' => '5');
		$cond_user2 = array();
        $this->outputData['users'] = $this->user_model->getUser($cond_user1,$cond_user2);
		$this->outputData['view'] = 'list';

        $this->render_page('templates/university/trash_admins',$this->outputData);

    }

	function deletetrash()
	 {

	   $this->load->helper(array('form', 'url'));
	   $this->load->model('university/user_model');

		if($this->user_model->deleteTrash($this->uri->segment('4'))=='success'){

	         $this->session->set_flashdata('flash_message', "Success: You have deleted User!");
	         redirect('university/university/trash');
	    }
	}

	function multidelete_trash(){

	    $this->load->helper(array('form', 'url'));

		$this->load->model('university/user_model');

		$array = $this->input->post('chk');
		foreach($array as $id):

		$this->user_model->deleteTrash($id);

		endforeach;
		$this->session->set_flashdata('flash_message', "You have deleted User!");
		 redirect('university/university/trash');


	}






}//End  Home Class

?>
