<?php

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

class Research extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

    function index()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('university/university_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata)){  redirect('common/login'); }

        if($userdata['university_id'])
        {
            $this->outputData['researches'] = $this->university_model->getAllResearchesByUniversity($userdata['university_id']);
        }
        else
        {
            $this->outputData['researches'] = $this->university_model->getAllResearches();
        }

        $user_id = $userdata['id'];
        $this->render_page('templates/university/researches',$this->outputData);
    }

    function form()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('university/university_model');
        $this->load->model('university/college_model');
        $this->load->model('university/department_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata)){  redirect('common/login'); }
        $allUniversities = $this->university_model->getAllUniversities();
        if($userdata['university_id'])
        {
            $allColleges = $this->college_model->getAllCollegesByUniversity($userdata['university_id']);
            $allDepartments = $this->department_model->getAllDepartmentsByUniversity($userdata['university_id']);
        }
        else
        {
            $allColleges = $this->college_model->getAllColleges();
            $allDepartments = $this->department_model->getAllDepartments();
        }

        if($this->uri->segment('4'))
        {
            $researchData = $this->university_model->getResearchById($this->uri->segment('4'));
            $this->outputData['university'] = $researchData->university_id;
            $this->outputData['college'] = $researchData->college_id;
            $this->outputData['department'] = $researchData->department_id;
            $this->outputData['topic'] = $researchData->topic;
            $this->outputData['description'] = $researchData->description;
            $this->outputData['research_url'] = $researchData->research_url;
            $this->outputData['faculty_url'] = $researchData->faculty_url;
            $this->outputData['funding'] = $researchData->funding;
            $this->outputData['id'] = $this->uri->segment('4');
        }

        $this->outputData['university_id'] = $userdata['university_id'];
        $this->outputData['universities'] = $allUniversities;
        $this->outputData['colleges'] = $allColleges;
        $this->outputData['departments'] = $allDepartments;
        $this->render_page('templates/university/research_form', $this->outputData);
    }

    function create()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/university_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata)){  redirect('common/login'); }

        $data['department_id'] = $this->input->post('department');
        $data['topic'] = $this->input->post('topic');
        $data['description'] = $this->input->post('description');
        $data['research_url'] = $this->input->post('research_url');
        $data['faculty_url'] = $this->input->post('faculty_url');
        $data['funding'] = $this->input->post('funding') ? $this->input->post('funding') : NULL;

        $this->university_model->addResearch($data);
        redirect('/university/research');
    }

    function edit()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('university/university_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata)){  redirect('common/login'); }

        $data['department_id'] = $this->input->post('department');
        $data['topic'] = $this->input->post('topic');
        $data['description'] = $this->input->post('description');
        $data['research_url'] = $this->input->post('research_url');
        $data['faculty_url'] = $this->input->post('faculty_url');
        $data['funding'] = $this->input->post('funding') ? $this->input->post('funding') : NULL;

        $research_id = $this->uri->segment('4');

        $this->university_model->editResearch($research_id, $data);
        redirect('/university/research');
    }
}
?>
