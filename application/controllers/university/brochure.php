<?php
class Brochure extends MY_Controller
{
    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
        $this->load->library('form_validation');
    }


    function index()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('university/university_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        if($userdata['university_id'])
        {
            $this->outputData['brochures'] = $this->university_model->getBrochuresByUniversity($userdata['university_id']);
        }
        else
        {
            $this->outputData['brochures'] = $this->university_model->getAllBrochures();
        }
        $this->outputData['view'] = 'list';
        $this->render_page('templates/university/brochure',$this->outputData);
    }

    function form()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('university/university_model');

        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        if($this->uri->segment('4'))
        {
            $brochureData                           = $this->university_model->getBrochuresById($this->uri->segment('4'));
            $this->outputData['id'] 				= $brochureData->id;
            $this->outputData['brochure_title'] 	= $brochureData->brochure_title;
            $this->outputData['brochure_file_name'] = $brochureData->brochure_file_name;

        }
        $this->render_page('templates/university/brochure_form',$this->outputData);
    }

    function create()
    {
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        if(!$this->input->post('brochure_title') || (!$this->input->post('university') && $userdata['type'] != 3))
        {
            $this->session->set_flashdata('flash_message', "Error: Please fill all mandatory fields!");
            redirect('university/brochure/form');
        }
        //die('+++++');
        $this->load->helper(array('form', 'url'));
        $this->load->model('university/university_model');

        // Copied from alumns.php
        $data = array(
            'brochure_title' 	=>  $this->input->post('brochure_title'),
            'university_id' 	=>  $this->input->post('university') ? $this->input->post('university') : $userdata['university_id']
        );
        if($_FILES["brochure_file_name"]["name"])
        {
            $random_digit = round(microtime(true));
            $temp = explode(".", $_FILES["brochure_file_name"]["name"]);

            $target_image = APPPATH . "../upload/" . $userdata['name'] . $random_digit . '_brochure.' . end($temp);

            $file_name =  base_url() . '/upload/' . $userdata['name'] . $random_digit . '_brochure.' . end($temp);
            $data['brochure_file_name'] = $file_name;

            copy($_FILES["brochure_file_name"]["tmp_name"], $target_image);
        }

        if($this->university_model->addBrochure($data))
        {
            $this->session->set_flashdata('flash_message', "Success: You have successfully added brochure!");
            redirect('university/brochure/');
        }
        $this->session->set_flashdata('flash_message', "Failed: Some issue occured while adding brochure!");
        redirect('university/brochure/');
	}


	 function edit()
	 {
	   $this->load->model('university/campus_model');
       $campusId = $this->uri->segment('4');
        $data = array(
        'name' 			=>  $this->input->post('name'),
        'university_id' 		=>  $this->input->post('university'),
        'state' 		=>  $this->input->post('state'),
        'city' 		=>	$this->input->post('city'),
        'size' 			=>	$this->input->post('size') ? $this->input->post('size') : NULL,
        'size_unit' 			=>	$this->input->post('size_unit') ? $this->input->post('size_unit') : NULL,
        'timezone' 		=>	$this->input->post('timezone'),
        'locale' 			=>	$this->input->post('locale'),
        'accomodation_type' 			=>	$this->input->post('accomodation_type'),
        'date_created' 	=>	date('Y-m-d H:i:s'),
        'date_updated' 	=>	date('Y-m-d H:i:s'),
        'added_by' 	=>	1,
        'updated_by' 	=>	1
        );

        $this->campus_model->editCampus($campusId, $data);
        $this->session->set_flashdata('flash_message', "Success: You have modified Campus!");
         redirect('university/campus');

	   //}
	}


	function delete()
	 {

	   $this->load->helper(array('form', 'url'));
	   $this->load->model('university/user_model');

		if($this->user_model->deleteUser($this->uri->segment('4'))=='success'){

	         $this->session->set_flashdata('flash_message', "Success: You have deleted University!");
	         redirect('university/university');
	    }
	}


	function multidelete(){

	    $this->load->helper(array('form', 'url'));

		$this->load->model('university/user_model');

		$array = $this->input->post('chk');
		foreach($array as $id):

		$this->user_model->deleteUser($id);

		endforeach;
		$this->session->set_flashdata('flash_message', "You have deleted User!");
		 redirect('university/university');


	}

	function trash()
	{
	     $this->load->helper('cookie_helper');
         $this->load->model('university/user_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }

		$cond_user1 = array('user_master.status ' => '5');
		$cond_user2 = array();
        $this->outputData['users'] = $this->user_model->getUser($cond_user1,$cond_user2);
		$this->outputData['view'] = 'list';

        $this->render_page('templates/university/trash_admins',$this->outputData);

    }

	function deletetrash()
	 {

	   $this->load->helper(array('form', 'url'));
	   $this->load->model('university/user_model');

		if($this->user_model->deleteTrash($this->uri->segment('4'))=='success'){

	         $this->session->set_flashdata('flash_message', "Success: You have deleted User!");
	         redirect('university/university/trash');
	    }
	}

	function multidelete_trash(){

	    $this->load->helper(array('form', 'url'));

		$this->load->model('university/user_model');

		$array = $this->input->post('chk');
		foreach($array as $id):

		$this->user_model->deleteTrash($id);

		endforeach;
		$this->session->set_flashdata('flash_message', "You have deleted User!");
		 redirect('university/university/trash');


	}






}//End  Home Class

?>
