<?php

// ini_set('display_errors', '1');
// ini_set('display_startup_errors', '1');
// error_reporting(E_ALL);

require_once APPPATH . 'libraries/Mail/sMail.php';
class Internship extends MY_Controller
{
    public $outputData;
    public $loggedInUser;

    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
        $this->load->helper(array('cookie_helper', 'form', 'url'));
        $this->load->model(array('university/user_model', 'university/university_model', 'university/college_model', 'university/department_model', 'university/course_model'));

        $this->outputData['time_array'] = array('00:00', '00:15', '00:30', '00:45', '01:00', '01:15', '01:30', '01:45', '02:00', '02:15', '02:30', '02:45', '03:00', '03:15', '03:30', '03:45', '04:00', '04:15', '04:30', '04:45', '05:00', '05:15', '05:30', '05:45', '06:00', '06:15', '06:30', '06:45', '07:00', '07:15', '07:30', '07:45', '08:00', '08:15', '08:30', '08:45', '09:00', '09:15', '09:30', '09:45', '10:00', '10:15', '10:30', '10:45', '11:00', '11:15', '11:30', '11:45', '12:00', '12:15', '12:30', '12:45', '13:00', '13:15', '13:30', '13:45', '14:00', '14:15', '14:30', '14:45', '15:00', '15:15', '15:30', '15:45', '16:00', '16:15', '16:30', '16:45', '17:00', '17:15', '17:30', '17:45', '18:00', '18:15', '18:30', '18:45', '19:00', '19:15', '19:30', '19:45', '20:00', '20:15', '20:30', '20:45', '21:00', '21:15', '21:30', '21:45', '22:00', '22:15', '22:30', '22:45', '23:00', '23:15', '23:30', '23:45');
    }

    function index()
    {
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }
        $allInternships = array();
        switch($userdata['type'])
        {
            case 1:
            case 2:
                $allInternships = $this->university_model->getAllInternships();
                break;
            case 3:
                $allInternships = $this->university_model->getAllInternshipsByUniversity($userdata['university_id']);
                break;
        }

        $this->outputData['internships'] = $allInternships;
        $this->render_page('templates/university/internship', $this->outputData);
    }

    function form()
    {
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $this->outputData['edit'] = false;
        $this->outputData['isUniversity'] = false;
        $questionCount = 0;
        $professorCount = 1;
        $otherFeeCount = 1;

        if($this->uri->segment('4'))
        {
            $internshipId = $this->uri->segment('4');
            $internship = $this->university_model->getInternshipById($internshipId);
            //echo '<pre>'; print_r($internship); echo '</pre>'; exit;

            $this->outputData['id'] 				    = $internship->id;
            $this->outputData['university_id'] 			= $internship->university_id;
            $this->outputData['college_id'] 			= $internship->college_id;
            $this->outputData['department_id'] 			= $internship->department_id;
            $this->outputData['application_deadline'] 	= $internship->application_deadline;
            $this->outputData['internship_start_date'] 	= $internship->internship_start_date;
            $this->outputData['internship_end_date'] 	= $internship->internship_end_date;
            $this->outputData['professors_guide'] 		= $internship->professors_guide ? json_decode($internship->professors_guide, true) : array();
            $this->outputData['charges']                = $internship->charges;
            $this->outputData['description'] 			= $internship->description;
            $this->outputData['title']                  = $internship->title;
            $this->outputData['other_fees'] 			= $internship->other_fees ? json_decode($internship->other_fees, true) : array();
            $this->outputData['other_details'] 			= $internship->other_details ? json_decode($internship->other_details, true) : array();
            $this->outputData['university_name'] 		= $internship->university_name;
            $this->outputData['college_name'] 		    = $internship->college_name;
            $this->outputData['department_name']        = $internship->department_name;
            $this->outputData['edit']                   = true;
            $professorCount                             = count($this->outputData['other_details']);
            $questionCount                              = count($this->outputData['professors_guide']);
        }
        elseif($userdata['type'] == 3)
        {
            $allColleges = $this->college_model->getAllCollegesByUniversity($userdata['university_id']);
            $this->outputData['country_id'] = $userdata['country_id'];
            $this->outputData['university_name'] = $userdata['name'];
            $this->outputData['colleges'] = $allColleges;
            $this->outputData['isUniversity'] = true;
        }

        $allCountries = $this->university_model->getAllCountries();
        $this->outputData['countries'] = $allCountries;
        $this->outputData['question_count'] = $questionCount;
        $this->outputData['professor_count'] = $professorCount;
        $this->outputData['other_fee_count'] = $otherFeeCount;
        $this->render_page('templates/university/internship_form',$this->outputData);
    }

    function create()
    {
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        if($userdata['type'] == 3)
        {
            $universityId = $userdata['university_id'];
        }
        else
        {
            $universityId = $this->input->post('university');
        }

        if(!$universityId || !$this->input->post('college') || !$this->input->post('department') || !$this->input->post('application_deadline') || !$this->input->post('internship_start_date') || !$this->input->post('internship_end_date') || !$this->input->post('title') || !$this->input->post('description'))
        {
            $this->session->set_flashdata('flash_message', "Error: Please fill all mandatory fields!");
            redirect('university/internship/form');
        }

        $professorsName = $this->input->post('professor_guide_name');
        $professorsLink = $this->input->post('professor_guide_link');
        $professorsArray = array();

        if($_FILES['photo']['name'])
        {
            $this->load->library('upload');
            $fileCount = count($professorsName);
            for($i = 0; $i < $fileCount; $i++)
            {
                $photoUrl = '';
                $professorName = $professorsName[$i];
                $professorLink = $professorsLink[$i];
                if($_FILES['photo']['name'][$i])
                {
                    $config = array();
                    $uploadFilePath     = './../uploads';
                    $uploadPath         = '/../uploads';
                    $pathInfo           = pathinfo($_FILES['photo']['name'][$i]);
                    $tempfilename       = preg_replace('/\s+/', '_', $pathInfo['filename']);
                    $tempfilename       = str_replace('.', '_', $tempfilename);
                    $tempfilename       = str_replace('&', '_', $tempfilename);
                    $tempfilename       = str_replace('-', '_', $tempfilename);
                    $tempfilename       = str_replace('+', '_', $tempfilename);

                    $fileExt                    = $pathInfo['extension'];
                    $fileName                   = 'professor_university_' . $universityId . '_' . str_replace(' ', '_', $professorsName[$i]) . '.' . $fileExt;
                    $config['upload_path']      = $uploadFilePath;
                    $config['file_name']        = $fileName;
                    $config['allowed_types']    = 'gif|jpg|png|JPEG|JPG|jpeg';
                    $config['max_size']         = 2048;
                    $config['max_width']        = 10240;
                    $config['max_height']       = 7680;

                    $this->upload->initialize($config);

                    $_FILES['images']['name']= $_FILES['photo']['name'][$i];
                    $_FILES['images']['type']= $_FILES['photo']['type'][$i];
                    $_FILES['images']['tmp_name']= $_FILES['photo']['tmp_name'][$i];
                    $_FILES['images']['error']= $_FILES['photo']['error'][$i];
                    $_FILES['images']['size']= $_FILES['photo']['size'][$i];

                    if(!$this->upload->do_upload('images'))
                    {
                        $this->outputData['document_error'] = $this->upload->display_errors();
                    }
                    else
                    {
                        $photoUrl = str_replace('admin/', '', base_url()) . 'uploads/professor_university_' . $universityId . '_' . str_replace(' ', '_', $professorsName[$i]) . '.' . $fileExt;
                    }
                }

                $professorsArray[] = array(
                    'name' => $professorName,
                    'link' => $professorLink,
                    'photo' => $photoUrl
                );
            }
        }

        $data = array(
            'university_id'             =>  $universityId,
            'college_id'                =>  $this->input->post('college'),
            'department_id'             =>  $this->input->post('department'),
            'title'                     =>	$this->input->post('title'),
            'description'               =>	$this->input->post('description'),
            'application_deadline' 		=>  $this->input->post('application_deadline'),
            'internship_start_date' 	=>  $this->input->post('internship_start_date'),
            'internship_end_date' 		=>	$this->input->post('internship_end_date'),
            'professors_guide' 			=>	json_encode($professorsArray),
            'charges' 		            =>	$this->input->post('charges'),
            'other_fees'                =>  $this->input->post('other_fees') ? json_encode($this->input->post('other_fees')) : NULL,
            'other_details' 			=>  $this->input->post('questions') ? json_encode($this->input->post('questions')) : NULL,
            'date_created' 	            =>	date('Y-m-d H:i:s'),
            'date_updated' 	            =>	date('Y-m-d H:i:s'),
            'last_updated_by'           =>	$userdata['id']
        );

        $insertInternship = $this->university_model->addInternship($data);

        $this->session->set_flashdata('flash_message', "Success: You have saved Internship!");
        redirect('university/internship');
    }

    function edit()
    {
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $internshipId = $this->uri->segment('4');
        if(!$internshipId)
        {
            redirect('university/internship');
        }

        if(!$this->input->post('application_deadline') || !$this->input->post('internship_start_date') || !$this->input->post('internship_end_date') || !$this->input->post('professors') || !$this->input->post('charges') || !$this->input->post('description'))
        {
            $this->session->set_flashdata('flash_message', "Error: Please fill all mandatory fields!");
            redirect('university/internship/form');
        }

        $data = array(
            'application_deadline' 		=>  $this->input->post('application_deadline'),
            'internship_start_date' 	=>  $this->input->post('internship_start_date'),
            'internship_end_date' 		=>	$this->input->post('internship_end_date'),
            'professors' 			    =>	$this->input->post('professors'),
            'charges' 		            =>	$this->input->post('charges'),
            'description'               =>	$this->input->post('description'),
            'other_fees'                =>  $this->input->post('other_fees') ? $this->input->post('other_fees') : NULL,
            'other_details' 			=>  $this->input->post('questions') ? json_encode($this->input->post('questions')) : NULL,
            'date_updated' 	            =>	date('Y-m-d H:i:s'),
            'last_updated_by'           =>	$userdata['id']
        );

        $this->university_model->updateInternship($internshipId, $data);

        $this->session->set_flashdata('flash_message', "Success: You have modified Internship!");
        redirect('university/internship');
    }

    function universities()
    {
        $countryId = $_GET['cid'];
        if($countryId)
        {
            $allUniversites = $this->university_model->getUniversitiesByCountry($countryId);
            $htmlString = '<option value="">Please Select</option>';
            if($allUniversites)
            {
                foreach($allUniversites as $university)
                {
                    $htmlString .= '<option value="' . $university->id . '">' . $university->name . '</option>';
                }
            }
            echo $htmlString;
        }
    }

    function colleges()
    {
        $universityId = $_GET['uid'];
        if($universityId)
        {
            $allColleges = $this->college_model->getAllCollegesByUniversity($universityId);
            $htmlString = '<option value="">Please Select</option>';
            if($allColleges)
            {
                foreach($allColleges as $college)
                {
                    $htmlString .= '<option value="' . $college->id . '">' . $college->name . '</option>';
                }
            }
            echo $htmlString;
        }
    }

    function departments()
    {
        $collegeId = $_GET['cid'];
        if($collegeId)
        {
            $allDepartments = $this->department_model->getAllDepartmentsByCollege($collegeId);
            $htmlString = '<option value="">Please Select</option>';
            if($allDepartments)
            {
                foreach($allDepartments as $department)
                {
                    $htmlString .= '<option value="' . $department->id . '">' . $department->name . '</option>';
                }
            }
            echo $htmlString;
        }
    }

    function courses()
    {
        $departmentId = $_GET['did'];
        if($departmentId)
        {
            $allCourses = $this->course_model->getAllCoursesByDepartment($departmentId);
            $htmlString = '<option value="">Please Select</option>';
            if($allCourses)
            {
                foreach($allCourses as $course)
                {
                    $htmlString .= '<option value="' . $course->id . '">' . $course->name . ' (' . $course->degree_name . ')</option>';
                }
            }
            echo $htmlString;
        }
    }

    function requests()
    {
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }
        //$universityDetail = $this->university_model->getUniversityById($userdata['university_id']);

        $allInternships = $this->university_model->getAllInternshipRequests();
        if($allInternships)
        {
            foreach($allInternships as $index => $value)
            {
                $value->student_other_details = $value->student_other_details ? json_decode($value->student_other_details) : NULL;
                $value->internship_other_details = $value->internship_other_details ? json_decode($value->internship_other_details) : NULL;
                $value->other_details = $value->other_details ? json_decode($value->other_details) : NULL;

                $allInternships[$index] = $value;
            }
        }
        $this->outputData['internships'] = $allInternships;
        $this->render_page('templates/university/internship_requests', $this->outputData);
    }

    function session($type)
    {
        switch($type)
        {
            case 'all':
                $this->all();
                break;
            case 'coming':
                $this->coming();
                break;
            case 'unassigned':
                $this->unassigned();
                break;
            case 'unconfirmed':
                $this->unconfirmed();
                break;
            case 'completed':
                $this->completed();
                break;
        }
    }

    function all()
    {
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }
        $universityDetail = $this->university_model->getUniversityById($userdata['university_id']);
        $timezone = isset($universityDetail[0]['timezone']) ? $universityDetail[0]['timezone'] : '';

        $allSessions = $this->user_model->getSlotByUniversity($userdata['id'], 'INTERNSHIP');
        if($allSessions)
        {
            foreach($allSessions as $index => $session)
            {
                $localTimezone = $session->timezone ? 5.5 - (float)$session->timezone : 5.5 - (float)$timezone;
                $session->slot_1 = $session->slot_1 ? date('Y-m-d H:i:s', strtotime($session->slot_1) - $localTimezone * 60 * 60) : '';
                $session->slot_2 = $session->slot_2 ? date('Y-m-d H:i:s', strtotime($session->slot_2) - $localTimezone * 60 * 60) : '';
                $session->slot_3 = $session->slot_3 ? date('Y-m-d H:i:s', strtotime($session->slot_3) - $localTimezone * 60 * 60) : '';
                $allSessions[$index] = $session;
            }
        }
        $this->outputData['session_page'] = 'All Sessions';
        $this->outputData['sessions'] = $allSessions;
        $this->outputData['redirect_url'] = '/user/session';
        $this->outputData['user_detail'] = $userdata;
        //$this->outputData['timezone'] = $localTimezone;
        $this->render_page('templates/user/session', $this->outputData);
    }

    function coming()
    {
        $userdata=$this->session->userdata('user');

        if(empty($userdata)){  redirect('common/login'); }
        $universityDetail = $this->university_model->getUniversityById($userdata['university_id']);
        $timezone = isset($universityDetail[0]['timezone']) ? $universityDetail[0]['timezone'] : '';
        //$localTimezone = 5 - (int)$timezone;

        $allSessions = $this->user_model->getComingSlotByUniversity($userdata['id'], 'INTERNSHIP');
        if($allSessions)
        {
            foreach($allSessions as $index => $session)
            {
                $user_id = $session->user_id;
                $universityId = $session->university_id;
                $participants = array("user_id" => $user_id,"university_id" => $universityId);
                $json_participant = json_encode($participants);
                $token = $this->tokbox_model->getTokenByParticipant($json_participant);
                $allSessions[$index]->tokbox_url = "";
                if($token && $token->token)
                {
                    $encodedTokboxId = base64_encode($token->id);
                    $encodedUniversityId = base64_encode($userdata['id']);
                    $allSessions[$index]->tokbox_url = "https://www.hellouni.org/tokbox?id=" . $encodedTokboxId . "&uid=" . $encodedUniversityId;
                }
                $localTimezone = $session->timezone ? 5.5 - (float)$session->timezone : 5.5 - (float)$timezone;
                $session->slot_1 = $session->slot_1 ? date('Y-m-d H:i:s', strtotime($session->slot_1) - $localTimezone * 60 * 60) : '';
                $session->slot_2 = $session->slot_2 ? date('Y-m-d H:i:s', strtotime($session->slot_2) - $localTimezone * 60 * 60) : '';
                $session->slot_3 = $session->slot_3 ? date('Y-m-d H:i:s', strtotime($session->slot_3) - $localTimezone * 60 * 60) : '';
                $allSessions[$index] = $session;
            }
        }
        $this->outputData['sessions'] = $allSessions;
        //$this->outputData['timezone'] = $localTimezone;
        $this->outputData['session_page'] = 'Coming Sessions';
        $this->outputData['redirect_url'] = '/user/session/coming';
        $this->outputData['user_detail'] = $userdata;
        $this->render_page('templates/user/session', $this->outputData);
    }

    function completed()
    {
        $userdata=$this->session->userdata('user');

        if(empty($userdata)){  redirect('common/login'); }
        $universityDetail = $this->university_model->getUniversityById($userdata['university_id']);
        $timezone = isset($universityDetail[0]['timezone']) ? $universityDetail[0]['timezone'] : '';
        //$localTimezone = 5 - (int)$timezone;

        $allSessions = $this->user_model->getCompletedSlotByUniversity($userdata['id'], 'INTERNSHIP');
        if($allSessions)
        {
            foreach($allSessions as $index => $session)
            {
                $localTimezone = $session->timezone ? 5.5 - (float)$session->timezone : 5.5 - (float)$timezone;
                $session->slot_1 = $session->slot_1 ? date('Y-m-d H:i:s', strtotime($session->slot_1) - $localTimezone * 60 * 60) : '';
                $session->slot_2 = $session->slot_2 ? date('Y-m-d H:i:s', strtotime($session->slot_2) - $localTimezone * 60 * 60) : '';
                $session->slot_3 = $session->slot_3 ? date('Y-m-d H:i:s', strtotime($session->slot_3) - $localTimezone * 60 * 60) : '';
                $allSessions[$index] = $session;
            }
        }
        $this->outputData['sessions'] = $allSessions;
        $this->outputData['session_page'] = 'Completed Sessions';
        $this->outputData['redirect_url'] = '/user/session/completed';
        $this->outputData['user_detail'] = $userdata;
        //$this->outputData['timezone'] = $localTimezone;
        $this->render_page('templates/user/session', $this->outputData);
    }

    function unassigned()
    {
        $userdata=$this->session->userdata('user');

        if(empty($userdata)){  redirect('common/login'); }
        $universityDetail = $this->university_model->getUniversityById($userdata['university_id']);
        $timezone = isset($universityDetail[0]['timezone']) ? $universityDetail[0]['timezone'] : '';
        //$localTimezone = 5 - (int)$timezone;

        $allSessions = $this->user_model->getUnassignedSlotByUniversity($userdata['id'], 'INTERNSHIP');
        if($allSessions)
        {
            foreach($allSessions as $index => $session)
            {
                $localTimezone = $session->timezone ? 5.5 - (float)$session->timezone : 5.5 - (float)$timezone;
                $session->slot_1 = $session->slot_1 ? date('Y-m-d H:i:s', strtotime($session->slot_1) - $localTimezone * 60 * 60) : '';
                $session->slot_2 = $session->slot_2 ? date('Y-m-d H:i:s', strtotime($session->slot_2) - $localTimezone * 60 * 60) : '';
                $session->slot_3 = $session->slot_3 ? date('Y-m-d H:i:s', strtotime($session->slot_3) - $localTimezone * 60 * 60) : '';
                $allSessions[$index] = $session;
            }
        }
        $this->outputData['sessions'] = $allSessions;
        $this->outputData['session_page'] = 'Unassigned Sessions';
        $this->outputData['redirect_url'] = '/user/session/unassigned';
        $this->outputData['user_detail'] = $userdata;
        //$this->outputData['timezone'] = $localTimezone;
        $this->render_page('templates/user/session', $this->outputData);
    }

    function unconfirmed()
    {
        $userdata=$this->session->userdata('user');

        if(empty($userdata)){  redirect('common/login'); }
        $universityDetail = $this->university_model->getUniversityById($userdata['university_id']);
        $timezone = isset($universityDetail[0]['timezone']) ? $universityDetail[0]['timezone'] : '';
        //$localTimezone = 5 - (int)$timezone;

        $allSessions = $this->user_model->getUnconfirmedSlotByUniversity($userdata['id'], 'INTERNSHIP');
        if($allSessions)
        {
            foreach($allSessions as $index => $session)
            {
                $localTimezone = $session->timezone ? 5.5 - (float)$session->timezone : 5.5 - (float)$timezone;
                $session->slot_1 = $session->slot_1 ? date('Y-m-d H:i:s', strtotime($session->slot_1) - $localTimezone * 60 * 60) : '';
                $session->slot_2 = $session->slot_2 ? date('Y-m-d H:i:s', strtotime($session->slot_2) - $localTimezone * 60 * 60) : '';
                $session->slot_3 = $session->slot_3 ? date('Y-m-d H:i:s', strtotime($session->slot_3) - $localTimezone * 60 * 60) : '';
                $allSessions[$index] = $session;
            }
        }
        $this->outputData['sessions'] = $allSessions;
        $this->outputData['session_page'] = 'Unconfirmed Sessions';
        $this->outputData['redirect_url'] = '/user/session/unconfirmed';
        $this->outputData['user_detail'] = $userdata;
        //$this->outputData['timezone'] = $localTimezone;
        $this->render_page('templates/user/session', $this->outputData);
    }

    function detail($internshipId)
    {
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $internshipId = $this->uri->segment('4');
        $internship = $this->university_model->getInternshipRequestById($internshipId);
        //echo '<pre>'; print_r($internship); echo '</pre>'; exit;

        $this->outputData['id'] 				    = $internship->id;
        $this->outputData['university_id'] 			= $internship->university_id;
        $this->outputData['college_id'] 			= $internship->college_id;
        $this->outputData['department_id'] 			= $internship->department_id;
        $this->outputData['application_deadline'] 	= $internship->application_deadline;
        $this->outputData['internship_start_date'] 	= $internship->internship_start_date;
        $this->outputData['internship_end_date'] 	= $internship->internship_end_date;
        $this->outputData['professors'] 			= $internship->professors;
        $this->outputData['charges']                = $internship->charges;
        $this->outputData['description'] 			= $internship->description;
        $this->outputData['other_fees'] 			= $internship->other_fees;
        $this->outputData['other_details'] 			= $internship->other_details ? json_decode($internship->other_details, true) : array();
        $this->outputData['university_name'] 		= $internship->university_name;
        $this->outputData['college_name'] 		    = $internship->college_name;
        $this->outputData['department_name']        = $internship->department_name;
        $questionCount                              = count($this->outputData['other_details']);
        $this->outputData['student_name']           = $internship->student_name;
        $this->outputData['student_other_details']  = $internship->student_other_details ? json_decode($internship->student_other_details) : NULL;
        $this->outputData['answers']                = $internship->internship_other_details ? json_decode($internship->internship_other_details) : NULL;

        $allCountries = $this->university_model->getAllCountries();
        $this->outputData['countries'] = $allCountries;
        $this->outputData['question_count'] = $questionCount;
        $this->render_page('templates/university/internship_detail',$this->outputData);
    }
}
?>
