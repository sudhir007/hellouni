<?php
require_once APPPATH . 'libraries/Mail/sMail.php';

class Student extends MY_Controller
{
    public $outputData;
    public $loggedInUser;

    function __construct()
    {
        parent::__construct();

        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		    $this->load->library('form_validation');

        $this->load->model('location/country_model');
        $this->load->model('webinar/webinar_model');
        $this->load->model('webinar/common_model');
        $this->load->model('counsellor/counsellor_model');
        $this->load->model('course/course_model');
		    $this->load->model('course/degree_model');
        $this->load->model('user/user_model');
        $this->load->model('tokbox_model');
        $this->load->model('redi_model');
        $this->load->model('university/university_model');
    }

    function index() {
        $this->load->helper('cookie_helper');
        $usrdata=$this->session->userdata('user');

        if(empty($usrdata))
        {
            redirect('user/account');
            exit();
        }

        $startDate = $this->input->get('startDate') ? $this->input->get('startDate') : '';
        $endDate = $this->input->get('endDate') ? $this->input->get('endDate') : '';
        $countryIds = $this->input->get('countryId') ? $this->input->get('countryId') : '';
        $intake_year = $this->input->get('intake_year') ? $this->input->get('intake_year') : '';
        $intake_month = $this->input->get('intake_month') ? $this->input->get('intake_month') : '';
        $lead_state_type = $this->input->get('leadstatetype') ? $this->input->get('leadstatetype') : '';

        $this->outputData['start_date'] = $startDate;
        $this->outputData['end_date'] = $endDate;
        $this->outputData['country_id'] = $countryIds;
        $this->outputData['intake_year'] = $intake_year;
        $this->outputData['intake_month'] = $intake_month;
        $this->outputData['lead_state_type'] = $lead_state_type;

        $lead_state_array = [
            ["id" => 'total_student', "value" => 'TOTAL STUDENT'],
            ["id" => 'interested_student', "value" => 'INTERESTED STUDENT'],
            ["id" => 'potential_student', "value" => 'POTENTIAL STUDENT']
        ];

        $this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));
        $this->outputData['lead_state_dropdown'] = $lead_state_array;

        $this->render_page('templates/university/student_list',$this->outputData);

    }

    function filter() {

        $this->load->helper('cookie_helper');
        $usrdata=$this->session->userdata('user');

        if(empty($usrdata))
        {
            redirect('user/account');
            exit();
        }

        $universityId = $usrdata['university_id'];
        $universityLoginId = $usrdata['id'];

        $startDate = $this->input->post('start_date');
        $endDate = $this->input->post('end_date');
        $countryIds = $this->input->post('country_id');
        $intake_year = $this->input->post('intake_year');
        $intake_month = $this->input->post('intake_month');
        $status = $this->input->post('application_status') ? $this->input->post('application_status') : 0;
        $statusFilter = "";

        if($status)
        {
            if($status == 'total_student')
            {
                $statusFilter = "'INTERESTED', 'POTENTIAL'";
            }
            else if($status == 'interested_student')
            {
                $statusFilter = "'INTERESTED'";
            }
            else if($status == 'potential_student')
            {
                $statusFilter = "'POTENTIAL'";
            }
        }

        $response['student_list'] = $this->university_model->getFilteredStudents($universityId, $startDate, $endDate, $countryIds, $intake_year, $intake_month, $statusFilter);

        header('Content-Type: application/json');
        echo (json_encode($response));
    }
}

?>
