<?php
require_once APPPATH . 'libraries/Mail/sMail.php';

class Dashboard extends MY_Controller
{
    public $outputData;
    public $loggedInUser;

    function __construct()
    {
        parent::__construct();

        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');

        $this->load->model('location/country_model');
        $this->load->model('webinar/webinar_model');
        $this->load->model('webinar/common_model');
        $this->load->model('counsellor/counsellor_model');
        $this->load->model('course/course_model');
		$this->load->model('course/degree_model');
        $this->load->model('user/user_model');
        $this->load->model('tokbox_model');
        $this->load->model('redi_model');
        $this->load->model('university/university_model');
    }

    function index()
    {
        $this->load->helper('cookie_helper');

        $usrdata=$this->session->userdata('user');

        if(empty($usrdata))
        {
            redirect('user/account');
            exit();
        }

        $universityId = $usrdata['university_id'];
        $universityLoginId = $usrdata['id'];

        $applicationCount = $this->university_model->getDashboardApplicationCount($universityId);
        $studentCount = $this->university_model->getDashboardStudentCount($universityId);

        /*$applicationDetail = $this->counsellor_model->applicationData($allChild);
        $visaApplicationDetail = $this->counsellor_model->visaApplicationData($allChild);
        $studentDetail = $this->counsellor_model->studentData($allChild);*/

        //application
        $this->outputData['totalApplicationCount'] = 0;
        $this->outputData['approvedApplicationCount'] = 0;
        $this->outputData['rejectedApplicationCount'] = 0;

        $approvedStatusArray = ['ADMIT_RECEIVED_CONDITIONAL', 'ADMIT_RECEIVED_UNCONDITIONAL', 'VISA_PROCESS_FINANCIAL_DOCUMENTS', 'VISA_APPLIED', 'VISA_APPROVED', 'VISA_REJECTED', 'VISA_APPROVED_OFFER'];
        $rejectedStatusArray = ['ADMIT_DENIED', 'ADMIT_WITHDRAW'];

        foreach ($applicationCount as $keyad => $valuead)
        {
            if(in_array($valuead['status'], $approvedStatusArray))
            {
                $this->outputData['approvedApplicationCount'] += $valuead['count'];
                $this->outputData['totalApplicationCount'] += $valuead['count'];
            }
            else if(in_array($valuead['status'], $rejectedStatusArray))
            {
                $this->outputData['rejectedApplicationCount'] += $valuead['count'];
                $this->outputData['totalApplicationCount'] += $valuead['count'];
            }
            else if($valuead['status'] == 'APPLICATION_PROCESS')
            {
                $this->outputData['totalApplicationCount'] += $valuead['count'];
            }
            else if($valuead['status'] == 'APPLICATION_SUBMITTED')
            {
                $this->outputData['totalApplicationCount'] += $valuead['count'];
            }
        }

        //offers
        $this->outputData['totalOfferCount'] = 0;
        $this->outputData['approvedOfferCount'] = 0;
        $this->outputData['rejectedOfferCount'] = 0;

        foreach ($applicationCount as $keyoad => $valueoad)
        {
            if($valueoad['status'] == 'ADMIT_RECEIVED_UNCONDITIONAL')
            {
                $this->outputData['approvedOfferCount'] = $valueoad['count'];
                $this->outputData['totalOfferCount'] = $this->outputData['totalOfferCount'] + $valueoad['count'];
            }
            else if($valueoad['status'] == 'ADMIT_DENIED')
            {
                $this->outputData['rejectedOfferCount'] = $valueoad['count'];
                $this->outputData['totalOfferCount'] = $this->outputData['totalOfferCount'] + $valueoad['count'];
            }
        }

        //student
        $this->outputData['totalStudentCount'] = 0;
        $this->outputData['interestedStudentCount'] = 0;
        $this->outputData['potentialStudentCount'] = 0;

        foreach ($studentCount as $keysd => $valuesd)
        {
            if($valuesd['status'] == 'INTERESTED')
            {
                $this->outputData['interestedStudentCount'] = $valuesd['count'];
                $this->outputData['totalStudentCount'] = $this->outputData['totalStudentCount'] + $valuesd['count'];
            }
            else if($valuesd['status'] == 'POTENTIAL')
            {
                $this->outputData['potentialStudentCount'] = $valuesd['count'];
                $this->outputData['totalStudentCount'] = $this->outputData['totalStudentCount'] + $valuesd['count'];
            }
        }

        $this->outputData['totalPaymentCount'] = $totalPaymentCount;
        $this->outputData['approvedPaymentCount'] = $approvedPaymentCount;
        $this->outputData['rejectedPaymentCount'] = $rejectedPaymentCount;
        $this->outputData['start_date'] = $usrdata['createdate'] ? date('Y-m-d', strtotime($usrdata['createdate'])) : date('Y-m-d') ;
        $this->outputData['end_date'] = date('Y-m-d');

        $this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));

        $this->render_page('templates/university/dashboard',$this->outputData);

    }

    function graph()
    {
        $this->load->helper('cookie_helper');
        $usrdata=$this->session->userdata('user');

        if(empty($usrdata))
        {
            redirect('user/account');
            exit();
        }

        $universityId = $usrdata['university_id'];
        $universityLoginId = $usrdata['id'];

        //Grpah Data
        $graphDetail = $this->university_model->getGrapData($universityId);

        $applicationGraph = [];
        $offerGraph = [];
        $studentGraph = [];

        $monthArray = [1,2,3,4,5,6,7,8,9,10,11,12];

        $applicationStatusArray = ['ADMIT_RECEIVED_CONDITIONAL','ADMIT_DENIED','APPLICATION_PROCESS','APPLICATION_SUBMITTED'];
        $offerStatusArray = ['ADMIT_RECEIVED_UNCONDITIONAL','ADMIT_DENIED'];
        $studentStatusArray = ['INTERESTED','POTENTIAL'];

        if($graphDetail)
        {
            foreach ($graphDetail['application_data'] as $keyadg => $valueadg)
            {
                if(in_array($valueadg['status'], $applicationStatusArray))
                {
                    $applicationGraph[$valueadg['graph_month']][] = $valueadg['count'];
                }
            }

            foreach ($graphDetail['application_data'] as $keyoadg => $valueoadg)
            {
                if(in_array($valueoadg['status'], $offerStatusArray))
                {
                    $offerGraph[$valueoadg['graph_month']][] = $valueoadg['count'];
                }
            }

            foreach ($graphDetail['student_data'] as $keysdg => $valuesdg)
            {
                if(in_array($valuesdg['status'], $studentStatusArray))
                {
                    $studentGraph[$valuesdg['graph_month']][] = $valuesdg['count'];
                }
            }
        }

        $applicationGraphValues = [];
        $offerGraphValues = [];
        $studentGraphValues = [];

        foreach ($monthArray as $keyma)
        {
            if(isset($applicationGraph[$keyma]))
            {
                $applicationGraphValues[] = array_sum($applicationGraph[$keyma]);
            }
            else
            {
                $applicationGraphValues[] = NULL;
            }

            if(isset($offerGraph[$keyma]))
            {
                $offerGraphValues[] = array_sum($offerGraph[$keyma]);
            }
            else
            {
                $offerGraphValues[] = NULL;
            }

            if(isset($studentGraph[$keyma]))
            {
                $studentGraphValues[] = array_sum($studentGraph[$keyma]);
            }
            else
            {
                $studentGraphValues[] = NULL;
            }
        }

        $response = [];

        $response['applicationGraphValues'] = $applicationGraphValues;
        $response['offerGraphValues'] = $offerGraphValues;
        $response['studentGraphValues'] = $studentGraphValues;

        header('Content-Type: application/json');
        echo (json_encode($response));
    }

    function filter()
    {
        $this->load->helper('cookie_helper');
        $usrdata=$this->session->userdata('user');

        if(empty($usrdata))
        {
            redirect('user/account');
            exit();
        }

        $universityId = $usrdata['university_id'];
        $universityLoginId = $usrdata['id'];

        $startDate = $this->input->post('start_date');
        $endDate = $this->input->post('end_date');
        $countryIds = $this->input->post('country_id');
        $intake_year = $this->input->post('intake_year');
        $intake_month = $this->input->post('intake_month');

        $dateConditons = "";
        $countryCondition = "";
        $intakeCondition = "";
        $appStatusCondition = "";
        $offerTYpeCondition = "";

        $applicationDetail = $this->university_model->getFilteredApplicationsDashboard($universityId, $startDate, $endDate, $countryIds, $intake_year, $intake_month);
        $studentDetail = $this->university_model->getFilteredStudentsDashboard($universityId, $startDate, $endDate, $countryIds, $intake_year, $intake_month);

        //Grpah Data
        $graphDetail = $this->university_model->getFilteredGraph($universityId, $startDate, $endDate, $countryIds, $intake_year, $intake_month);

        $applicationGraph = [];
        $offerGraph = [];
        $studentGraph = [];

        $monthArray = [1,2,3,4,5,6,7,8,9,10,11,12];
        $applicationStatusArray = ['ADMIT_RECEIVED_CONDITIONAL','ADMIT_DENIED','APPLICATION_PROCESS','APPLICATION_SUBMITTED'];
        $offerStatusArray = ['ADMIT_RECEIVED_UNCONDITIONAL','ADMIT_DENIED'];
        $studentStatusArray = ['INTERESTED','POTENTIAL'];

        if($graphDetail)
        {
            foreach ($graphDetail['application_data'] as $keyadg => $valueadg)
            {
                if(in_array($valueadg['status'], $applicationStatusArray))
                {
                    $applicationGraph[$valueadg['graph_month']][] = $valueadg['count'];
                }
            }

            foreach ($graphDetail['application_data'] as $keyoadg => $valueoadg)
            {
                if(in_array($valueoadg['status'], $offerStatusArray))
                {
                    $offerGraph[$valueoadg['graph_month']][] = $valueoadg['count'];
                }
            }

            foreach ($graphDetail['student_data'] as $keysdg => $valuesdg)
            {
                if(in_array($valuesdg['status'], $studentStatusArray))
                {
                    $studentGraph[$valuesdg['graph_month']][] = $valuesdg['count'];
                }
            }
        }

        $applicationGraphValues = [];
        $offerGraphValues = [];
        $studentGraphValues = [];

        foreach ($monthArray as $keyma)
        {
            if($applicationGraph[$keyma])
            {
                $applicationGraphValues[] = array_sum($applicationGraph[$keyma]);
            }
            else
            {
                $applicationGraphValues[] = NULL;
            }

            if($offerGraph[$keyma])
            {
                $offerGraphValues[] = array_sum($offerGraph[$keyma]);
            }
            else
            {
                $offerGraphValues[] = NULL;
            }

            if($studentGraph[$keyma])
            {
                $studentGraphValues[] = array_sum($studentGraph[$keyma]);
            }
            else
            {
                $studentGraphValues[] = NULL;
            }
        }

        $response = [];

        $response['applicationGraphValues'] = $applicationGraphValues;
        $response['offerGraphValues'] = $offerGraphValues;
        $response['studentGraphValues'] = $studentGraphValues;

        //application
        $response['totalApplicationCount'] = 0;
        $response['approvedApplicationCount'] = 0;
        $response['rejectedApplicationCount'] = 0;

        foreach ($applicationDetail as $keyad => $valuead)
        {
            if($valuead['status'] == 'ADMIT_RECEIVED_CONDITIONAL')
            {
                $response['approvedApplicationCount'] = $valuead['count'];
                $response['totalApplicationCount'] = $response['totalApplicationCount'] + $valuead['count'];
            }
            else if($valuead['status'] == 'ADMIT_DENIED')
            {
                $response['rejectedApplicationCount'] = $valuead['count'];
                $response['totalApplicationCount'] = $response['totalApplicationCount'] + $valuead['count'];
            }
            else if($valuead['status'] == 'APPLICATION_PROCESS')
            {
                $response['totalApplicationCount'] = $response['totalApplicationCount'] + $valuead['count'];
            }
            else if($valuead['status'] == 'APPLICATION_SUBMITTED')
            {
                $response['totalApplicationCount'] = $response['totalApplicationCount'] + $valuead['count'];
            }
        }


        //offers
        $response['totalOfferCount'] = 0;
        $response['approvedOfferCount'] = 0;
        $response['rejectedOfferCount'] = 0;

        foreach ($applicationDetail as $keyoad => $valueoad)
        {
            if($valueoad['status'] == 'ADMIT_RECEIVED_UNCONDITIONAL')
            {
                $response['approvedOfferCount'] = $valueoad['count'];
                $response['totalOfferCount'] = $response['totalOfferCount'] + $valueoad['count'];
            }
            else if($valueoad['status'] == 'ADMIT_WITHDRAW')
            {
                $response['rejectedOfferCount'] = $valueoad['count'];
                $response['totalOfferCount'] = $response['totalOfferCount'] + $valueoad['count'];
            }
        }

        //student
        $response['totalStudentCount'] = 0;
        $response['interestedStudentCount'] = 0;
        $response['potentialStudentCount'] = 0;

        foreach ($studentDetail as $keysd => $valuesd)
        {
            if($valuesd['status'] == 'INTERESTED')
            {
                $response['interestedStudentCount'] = $valuesd['count'];
                $response['totalStudentCount'] = $response['totalStudentCount'] + $valuesd['count'];
            }
            else if($valuesd['status'] == 'POTENTIAL')
            {
                $response['potentialStudentCount'] = $valuesd['count'];
                $response['totalStudentCount'] = $response['totalStudentCount'] + $valuesd['count'];
            }
        }

        $response['totalPaymentCount'] = $totalPaymentCount;
        $response['approvedPaymentCount'] = $approvedPaymentCount;
        $response['rejectedPaymentCount'] = $rejectedPaymentCount;


        header('Content-Type: application/json');
        echo (json_encode($response));

    }
}

?>
