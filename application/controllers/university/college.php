<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com


 */

class College extends MY_Controller {



	//Global variable

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;



    /**

	 * Constructor

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }


	function index()
	{
	       $this->load->helper('cookie_helper');
         $this->load->model('university/college_model');
         $this->load->model('filter_model');

		     $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }

         if($userdata['university_id'])
        {
            $this->outputData['colleges'] = $this->college_model->getAllCollegesByUniversity($userdata['university_id']);
            $this->outputData['university_list'] = $this->filter_model->getAllUniversityById($userdata['university_id']);
        }
        else
        {
            $this->outputData['colleges'] = $this->college_model->getAllColleges();
            $this->outputData['university_list'] = $this->filter_model->getAllUniversityById();
        }

		$this->outputData['view'] = 'list';

		/*echo '<pre>';
		print_r($this->outputData);
		echo '</pre>';
		exit();*/

        $this->render_page('templates/university/college',$this->outputData);

    }

    function filter() {

  	     $this->load->helper('cookie_helper');
         $this->load->model('university/course_model');
         $this->load->model('filter_model');

  		   $userdata=$this->session->userdata('user');
  	     if(empty($userdata)){
           redirect('common/login');
         }

          $selectedUniversityId = isset($_POST['university_id']) &&  $_POST['university_id'] != "" ? $_POST['university_id'] : 0;
          $selectedCampusId = isset($_POST['campus_id']) && $_POST['campus_id'] !="" ? $_POST['campus_id'] : 0;
          $selectedCollegeId = isset($_POST['college_id']) && $_POST['college_id'] !="" ? $_POST['college_id'] : 0;

          $allUniversityList = [];
          $allCampusList = [];
          $allCollegeList = [];

          if($userdata['university_id']) {
             $allUniversityList = $this->filter_model->getAllUniversityById($userdata['university_id']);
         } else {
             $allUniversityList = $this->filter_model->getAllUniversityById();
         }


          if($selectedUniversityId){
            $allCampusList = $this->filter_model->getAllCampusesByUniversityId($selectedUniversityId);
          }

          if($selectedCampusId){
            $allCollegeList = $this->filter_model->getAllCollegeByCampusId($selectedCampusId);
          }


          $this->outputData['colleges'] = $this->filter_model->getFilterCollege($selectedUniversityId,$selectedCampusId,$selectedCollegeId);


          $this->outputData['selected_university_id'] = $selectedUniversityId;
          $this->outputData['university_list'] = $allUniversityList;
          $this->outputData['selected_campus_id'] = $selectedCampusId;
          $this->outputData['campus_list'] = $allCampusList;
          $this->outputData['selected_college_id'] = $selectedCollegeId;
          $this->outputData['college_list'] = $allCollegeList;

  		$this->outputData['view'] = 'list';

  		/*echo '<pre>';
  		print_r($this->outputData);
  		echo '</pre>';
  		exit();*/

          $this->render_page('templates/university/college',$this->outputData);

      }

	 function form()
	 {
	     $this->load->helper('cookie_helper');
         $this->load->model('university/campus_model');
         $this->load->model('university/college_model');
         $this->load->model('filter_model');
		 //$this->load->model('location/country_model');
		 //$this->load->model('location/city_model');
		 //$this->load->model('location/state_model');


		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }
         $this->outputData['enrollment_count'] = 1;
         $this->outputData['ranking_count']  = 1;
         $this->outputData['application_deadline_usa_count'] = 1;
         $this->outputData['application_deadline_international_count']   = 1;
         $this->outputData['application_fees_usa_count'] = 1;
         $this->outputData['application_fees_international_count']   = 1;
         $this->outputData['average_gre_count']   = 1;

		 if($this->uri->segment('4')){
         $collegeData = $this->college_model->getCollegeByIdNew($this->uri->segment('4'));

	    $this->outputData['id'] 				= $collegeData->id;
		$this->outputData['name'] 				= $collegeData->name;
		$this->outputData['campus'] 				= $collegeData->campus_id;
        $this->outputData['category'] 				= $collegeData->category_id;
        $this->outputData['enrollment'] 				= json_decode($collegeData->enrollment, true);
		$this->outputData['ranking'] 				= json_decode($collegeData->ranking, true);
		$this->outputData['college_research_expenditure'] 				= $collegeData->college_research_expenditure;
        $this->outputData['total_enrollment'] 				= $collegeData->total_enrollment;
		$this->outputData['international_enrollment'] 				= $collegeData->international_enrollment;
		$this->outputData['application_deadline_usa'] 				= json_decode($collegeData->application_deadline_usa, true);
        $this->outputData['application_deadline_international'] 				= json_decode($collegeData->application_deadline_international, true);
		$this->outputData['application_fees_usa'] 				= json_decode($collegeData->application_fees_usa, true);
		$this->outputData['application_fees_international'] 				= json_decode($collegeData->application_fees_international, true);
        $this->outputData['admission_director_name'] 				= $collegeData->admission_director_name;
		$this->outputData['admission_director_phone'] 				= $collegeData->admission_director_phone;
		$this->outputData['admission_director_email'] 				= $collegeData->admission_director_email;
        $this->outputData['admission_url'] 				= $collegeData->admission_url;
		$this->outputData['average_gre'] 				= json_decode($collegeData->average_gre, true);
		$this->outputData['research_expenditure_per_faculty'] 				= $collegeData->research_expenditure_per_faculty;
        $this->outputData['enrollment_count'] = count($this->outputData['enrollment']);
        $this->outputData['ranking_count']  = count($this->outputData['ranking']);
        $this->outputData['application_deadline_usa_count'] = count($this->outputData['application_deadline_usa']);
        $this->outputData['application_deadline_international_count']   = count($this->outputData['application_deadline_international']);
        $this->outputData['application_fees_usa_count'] = count($this->outputData['application_fees_usa']);
        $this->outputData['application_fees_international_count']   = count($this->outputData['application_fees_international']);
        $this->outputData['average_gre_count']   = count($this->outputData['average_gre']);
        $this->outputData['established_date'] 				= $collegeData->establishment_date;
        $this->outputData['acceptance_rate'] 				= $collegeData->acceptance_rate;

        $this->outputData['selected_university_id'] = $collegeData->university_id;
        $this->outputData['selected_campus_id'] = $collegeData->campus_id;

        $selectedUniversityId = $collegeData->university_id ? $collegeData->university_id : 0;
        $selectedCampusId = $collegeData->campus_id ? $collegeData->campus_id : 0;

        if($selectedUniversityId){
          $allCampusList = $this->filter_model->getAllCampusesByUniversityId($selectedUniversityId);
        }

       $this->outputData['campus_list'] = $allCampusList;

		}

    if($userdata['university_id']) {
       $allUniversityList = $this->filter_model->getAllUniversityById($userdata['university_id']);
   } else {
       $allUniversityList = $this->filter_model->getAllUniversityById();
   }

       $this->outputData['categories'] = $this->college_model->getAllCategories();

        //$this->outputData['campuses'] = $allCampuses;
        $this->outputData['university_list'] = $allUniversityList;
        $this->render_page('templates/university/college_form',$this->outputData);

     }

	 function create()
	 {
         if(!$this->input->post('name') || !$this->input->post('campus') || !$this->input->post('category_id'))
         {
             $this->session->set_flashdata('flash_message', "Error: Please fill all mandatory fields!");
    	     redirect('university/college/form');
         }
	   //die('+++++');
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('university/college_model');
       $enrollment = array();
       $ranking = array();
       $applicationDeadlineUSA = array();
       $applicationDeadlineInternational = array();
       $applicationFeesUSA = array();
       $applicationFeesInternational = array();
       $averageGRE = array();

       foreach($this->input->post('enrollment_year') AS $index => $enrollmentYear)
       {
           $enrollment[$index]['year'] = $enrollmentYear;
           $enrollment[$index]['number'] = $this->input->post('enrollment_numbers')[$index];
       }

       foreach($this->input->post('ranking_year') AS $index => $rankingYear)
       {
           $ranking[$index]['year'] = $rankingYear;
           $ranking[$index]['by_whom'] = $this->input->post('ranking_by_whom')[$index];
           $ranking[$index]['ranking'] = $this->input->post('ranking')[$index];
       }

       foreach($this->input->post('deadline_usa_year') AS $index => $deadlineYear)
       {
           $applicationDeadlineUSA[$index]['year'] = $deadlineYear;
           $applicationDeadlineUSA[$index]['date'] = $this->input->post('deadline_usa_calendar')[$index];
       }

       foreach($this->input->post('deadline_international_year') AS $index => $deadlineYear)
       {
           $applicationDeadlineInternational[$index]['year'] = $deadlineYear;
           $applicationDeadlineInternational[$index]['date'] = $this->input->post('deadline_international_calendar')[$index];
       }

       foreach($this->input->post('application_fees_usa_currency') AS $index => $feesCurrency)
       {
           $applicationFeesUSA[$index]['currency'] = $feesCurrency;
           $applicationFeesUSA[$index]['amount'] = $this->input->post('application_fees_usa_amount')[$index];
       }

       foreach($this->input->post('application_fees_international_currency') AS $index => $feesCurrency)
       {
           $applicationFeesInternational[$index]['currency'] = $feesCurrency;
           $applicationFeesInternational[$index]['amount'] = $this->input->post('application_fees_international_amount')[$index];
       }

       foreach($this->input->post('average_gre_quant') AS $index => $quant)
       {
           $averageGRE[$index]['quant'] = $quant;
           $averageGRE[$index]['verbal'] = $this->input->post('average_gre_verbal')[$index];
           $averageGRE[$index]['awa'] = $this->input->post('average_gre_awa')[$index];
       }


		 	$data = array(
        	'name' 			=>  $this->input->post('name'),
        	'campus_id' 		=>  $this->input->post('campus'),
            'category_id' 		=>  $this->input->post('category_id') ? $this->input->post('category_id') : NULL,
            'enrollment'    => json_encode($enrollment),
            'ranking'       => json_encode($ranking),
            'application_deadline_usa' => json_encode($applicationDeadlineUSA),
            'application_deadline_international' => json_encode($applicationDeadlineInternational),
            'application_fees_usa'  => json_encode($applicationFeesUSA),
            'application_fees_international' => json_encode($applicationFeesInternational),
            'college_research_expenditure'  => $this->input->post('college_research_expenditure') ? $this->input->post('college_research_expenditure') : NULL,
            'total_enrollment'  => $this->input->post('total_enrollment') ? $this->input->post('total_enrollment') : NULL,
            'international_enrollment'  => $this->input->post('international_enrollment') ? $this->input->post('international_enrollment') : NULL,
            'admission_director_name'  => $this->input->post('admission_director_name'),
            'admission_director_phone'  => $this->input->post('admission_director_phone') ? $this->input->post('admission_director_phone') : NULL,
            'admission_director_email'  => $this->input->post('admission_director_email'),
            'admission_url'  => $this->input->post('admission_url'),
            'average_gre'       => json_encode($averageGRE),
            'research_expenditure_per_faculty'  => $this->input->post('research_expenditure_per_faculty') ? $this->input->post('research_expenditure_per_faculty') : NULL,
            'date_created' 	=>	date('Y-m-d H:i:s'),
            'date_updated' 	=>	date('Y-m-d H:i:s'),
            'establishment_date' => $this->input->post('establishment_date'),
            'acceptance_rate' => $this->input->post('acceptance_rate'),
            'added_by' 	=>	1,
            'updated_by' 	=>	1
			);


			/*echo '<pre>';
			print_r($data);
			echo '</pre>';
			exit(); */

	     if($this->college_model->addCollege($data))
         {
                $this->session->set_flashdata('flash_message', "Success: You have successfully added college!");
	            redirect('university/college/');
	     }
         $this->session->set_flashdata('flash_message', "Failed: Some issue occured while adding college!");
         redirect('university/college/');
	}


	 function edit()
	 {
	   $this->load->model('university/college_model');
       $enrollment = array();
       $ranking = array();
       $applicationDeadlineUSA = array();
       $applicationDeadlineInternational = array();
       $applicationFeesUSA = array();
       $applicationFeesInternational = array();
       $averageGRE = array();

       foreach($this->input->post('enrollment_year') AS $index => $enrollmentYear)
       {
           $enrollment[$index]['year'] = $enrollmentYear;
           $enrollment[$index]['number'] = $this->input->post('enrollment_numbers')[$index];
       }

       foreach($this->input->post('ranking_year') AS $index => $rankingYear)
       {
           $ranking[$index]['year'] = $rankingYear;
           $ranking[$index]['by_whom'] = $this->input->post('ranking_by_whom')[$index];
           $ranking[$index]['ranking'] = $this->input->post('ranking')[$index];
       }

       foreach($this->input->post('deadline_usa_year') AS $index => $deadlineYear)
       {
           $applicationDeadlineUSA[$index]['year'] = $deadlineYear;
           $applicationDeadlineUSA[$index]['date'] = $this->input->post('deadline_usa_calendar')[$index];
       }

       foreach($this->input->post('deadline_international_year') AS $index => $deadlineYear)
       {
           $applicationDeadlineInternational[$index]['year'] = $deadlineYear;
           $applicationDeadlineInternational[$index]['date'] = $this->input->post('deadline_international_calendar')[$index];
       }

       foreach($this->input->post('application_fees_usa_currency') AS $index => $feesCurrency)
       {
           $applicationFeesUSA[$index]['currency'] = $feesCurrency;
           $applicationFeesUSA[$index]['amount'] = $this->input->post('application_fees_usa_amount')[$index];
       }

       foreach($this->input->post('application_fees_international_currency') AS $index => $feesCurrency)
       {
           $applicationFeesInternational[$index]['currency'] = $feesCurrency;
           $applicationFeesInternational[$index]['amount'] = $this->input->post('application_fees_international_amount')[$index];
       }

       foreach($this->input->post('average_gre_quant') AS $index => $quant)
       {
           $averageGRE[$index]['quant'] = $quant;
           $averageGRE[$index]['verbal'] = $this->input->post('average_gre_verbal')[$index];
           $averageGRE[$index]['awa'] = $this->input->post('average_gre_awa')[$index];
       }

       $collegeId = $this->uri->segment('4');
        $data = array(
        'name' 			=>  $this->input->post('name'),
        'campus_id' 		=>  $this->input->post('campus'),
        'category_id' 		=>  $this->input->post('category_id') ? $this->input->post('category_id') : NULL,
        'enrollment'    => json_encode($enrollment),
        'ranking'       => json_encode($ranking),
        'application_deadline_usa' => json_encode($applicationDeadlineUSA),
        'application_deadline_international' => json_encode($applicationDeadlineInternational),
        'application_fees_usa'  => json_encode($applicationFeesUSA),
        'application_fees_international' => json_encode($applicationFeesInternational),
        'college_research_expenditure'  => $this->input->post('college_research_expenditure') ? $this->input->post('college_research_expenditure') : NULL,
        'total_enrollment'  => $this->input->post('total_enrollment') ? $this->input->post('total_enrollment') : NULL,
        'international_enrollment'  => $this->input->post('international_enrollment') ? $this->input->post('international_enrollment') : NULL,
        'admission_director_name'  => $this->input->post('admission_director_name'),
        'admission_director_phone'  => $this->input->post('admission_director_phone') ? $this->input->post('admission_director_phone') : NULL,
        'admission_director_email'  => $this->input->post('admission_director_email'),
        'admission_url'  => $this->input->post('admission_url'),
        'average_gre'       => json_encode($averageGRE),
        'research_expenditure_per_faculty'  => $this->input->post('research_expenditure_per_faculty') ? $this->input->post('research_expenditure_per_faculty') : NULL,
        'establishment_date' => $this->input->post('establishment_date'),
        'acceptance_rate' => $this->input->post('acceptance_rate'),
        'date_created' 	=>	date('Y-m-d H:i:s'),
        'date_updated' 	=>	date('Y-m-d H:i:s'),
        'added_by' 	=>	1,
        'updated_by' 	=>	1
        );

        $this->college_model->editCollege($collegeId, $data);
        $this->session->set_flashdata('flash_message', "Success: You have modified college!");
         redirect('university/college');

	   //}
	}


	// function delete()
	//  {

	//    $this->load->helper(array('form', 'url'));
	//    $this->load->model('user/user_model');

	// 	if($this->user_model->deleteUser($this->uri->segment('4'))=='success'){

	//          $this->session->set_flashdata('flash_message', "Success: You have deleted University!");
	//          redirect('user/college');
	//     }
	// }


	// function multidelete(){

	//     $this->load->helper(array('form', 'url'));

	// 	$this->load->model('user/user_model');

	// 	$array = $this->input->post('chk');
	// 	foreach($array as $id):

	// 	$this->user_model->deleteUser($id);

	// 	endforeach;
	// 	$this->session->set_flashdata('flash_message', "You have deleted User!");
	// 	 redirect('user/college');


	// }

	// function trash()
	// {
	//      $this->load->helper('cookie_helper');
 //         $this->load->model('user/user_model');
	// 	 $userdata=$this->session->userdata('user');
	//      if(empty($userdata)){  redirect('common/login'); }

	// 	$cond_user1 = array('user_master.status ' => '5');
	// 	$cond_user2 = array();
 //        $this->outputData['users'] = $this->user_model->getUser($cond_user1,$cond_user2);
	// 	$this->outputData['view'] = 'list';

 //        $this->render_page('templates/user/trash_admins',$this->outputData);

 //    }

	// function deletetrash()
	//  {

	//    $this->load->helper(array('form', 'url'));
	//    $this->load->model('user/user_model');

	// 	if($this->user_model->deleteTrash($this->uri->segment('4'))=='success'){

	//          $this->session->set_flashdata('flash_message', "Success: You have deleted User!");
	//          redirect('user/university/trash');
	//     }
	// }

	// function multidelete_trash(){

	//     $this->load->helper(array('form', 'url'));

	// 	$this->load->model('user/user_model');

	// 	$array = $this->input->post('chk');
	// 	foreach($array as $id):

	// 	$this->user_model->deleteTrash($id);

	// 	endforeach;
	// 	$this->session->set_flashdata('flash_message', "You have deleted User!");
	// 	 redirect('user/university/trash');


	// }






}//End  Home Class

?>
