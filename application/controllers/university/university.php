<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com


 */

require_once APPPATH . 'libraries/Mail/sMail.php';
class University extends MY_Controller {



	//Global variable

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;



    /**

	 * Constructor

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }


	function index()
	{
	     $this->load->helper('cookie_helper');
         $this->load->model('university/user_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }

		$cond_user1 = array('user_master.status !=' => '5');
		$cond_user2 = array('user_master.type' => '3');
        $this->outputData['users'] = $this->user_model->getUser($cond_user1,$cond_user2);
		$this->outputData['view'] = 'list';

		/*echo '<pre>';
		print_r($this->outputData['users']);
		echo '</pre>';
		exit();*/



		//$this->load->view('templates/user/body',$this->outputData);
        $this->render_page('templates/university/universities',$this->outputData);

    }

	 function form()
	 {
	     $this->load->helper('cookie_helper');
         $this->load->model('university/user_model');
         $this->load->model('university/university_model');
		 //$this->load->model('location/country_model');
		 //$this->load->model('location/city_model');
		 //$this->load->model('location/state_model');


		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }
         $total_unique_selling_points = 0;

		 if($this->uri->segment('4')){
		 $conditions = array('user_master.id' => $this->uri->segment('4'));
		 $user_details = $this->user_model->getUniversityByid($conditions);

		 //echo '<pre>'; print_r($user_details); echo '</pre>'; exit;

		$this->outputData['id'] 				= $user_details->id;
        $this->outputData['country'] 			= $user_details->country_id;
		$this->outputData['name'] 				= $user_details->name;
        $this->outputData['username'] 			= $user_details->username;
		$this->outputData['email'] 				= $user_details->email;
        $this->outputData['password'] 			= $user_details->password;
        $this->outputData['website'] 			= $user_details->website;
		$this->outputData['status'] 			= $user_details->status;
		$this->outputData['notification'] 		= $user_details->notification;
		$this->outputData['create'] 			= $user_details->create;
		$this->outputData['view'] 				= $user_details->view;
		$this->outputData['edit'] 				= $user_details->edit;
		$this->outputData['del'] 				= $user_details->del;
        $this->outputData['description'] 		= $user_details->description;
        $this->outputData['establishment_year'] = $user_details->establishment_year;
        $this->outputData['university_type'] 	= $user_details->university_type;
        $this->outputData['carnegie_accreditation'] = $user_details->carnegie_accreditation;
        $this->outputData['total_students'] 	= $user_details->total_students;
        $this->outputData['total_students_UG'] 	= $user_details->total_students_UG;
        $this->outputData['total_students_PG'] 	= $user_details->total_students_PG;
        $this->outputData['total_students_international'] = $user_details->total_students_international;
        $this->outputData['ranking_usa'] 		= $user_details->ranking_usa;
        $this->outputData['ranking_world'] 		= $user_details->ranking_world;
        $this->outputData['admission_success_rate'] = $user_details->admission_success_rate;
        $unique_selling_points = explode('--', $user_details->unique_selling_points);
        $all_unique_selling_points = [];
        if($unique_selling_points)
        {
            foreach($unique_selling_points as $unique_selling_point)
            {
                if($unique_selling_point)
                {
                    $all_unique_selling_points[] = $unique_selling_point;
                }
            }
        }
        $this->outputData['unique_selling_points'] 	= $all_unique_selling_points;
        $this->outputData['research_spending'] 	= $user_details->research_spending;
        $this->outputData['placement_percentage'] 	= $user_details->placement_percentage;
        $this->outputData['national_ranking_source'] 	= $user_details->national_ranking_source;
        $this->outputData['worldwide_ranking_source'] 	= $user_details->worldwide_ranking_source;
        $this->outputData['map_normal_view'] 	= $user_details->map_normal_view;
        $this->outputData['map_street_view'] 	= $user_details->map_street_view;
        $this->outputData['whatsapp_link'] 	= $user_details->whatsapp_link;
        $this->outputData['edit'] 	= true;
        $total_unique_selling_points = count($all_unique_selling_points);
		}

        $allCountries = $this->university_model->getAllCountries();
        $this->outputData['countries'] = $allCountries;
        $this->outputData['total_unique_selling_points'] = $total_unique_selling_points;
        $this->render_page('templates/user/university_form',$this->outputData);

     }

	 function create()
	 {
         if(!$this->input->post('uniname') || !$this->input->post('username') || !$this->input->post('email') || !$this->input->post('password') || !$this->input->post('country'))
         {
             $this->session->set_flashdata('flash_message', "Error: Please fill all mandatory fields!");
    	     redirect('university/university/form');
         }
	   //die('+++++');
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('university/user_model');

       $unique_selling_points = '';
       if($this->input->post('unique_selling_points'))
       {
           $unique_selling_points = implode('--', $this->input->post('unique_selling_points'));
           $unique_selling_points = '--' . $unique_selling_points;
       }

		 	$data = array(
        	'name' 			=>  $this->input->post('uniname'),
            'username' 		=>  $this->input->post('username'),
        	'email' 		=>  $this->input->post('email') ? $this->input->post('email') : NULL,
        	'password' 		=>  md5($this->input->post('password')),
			'type' 			=>  '3',
			'createdate' 	=>  date('Y-m-d'),
			'status' 		=>	$this->input->post('status'),
			'view' 			=>	$this->input->post('view'),
			'create' 		=>	$this->input->post('creation'),
			'edit' 			=>	$this->input->post('edit'),
			'del' 			=>	$this->input->post('deletion'),
			'notification' 	=>	$this->input->post('notification'),
            'country_id' => $this->input->post('country'),
            'website' 		=>  $this->input->post('website'),
            'about' 		=>  $this->input->post('about'),
            'establishment_year' 		=>  $this->input->post('establishment_year') ? $this->input->post('establishment_year') : NULL,
 			'university_type' 			=>  $this->input->post('type') ? $this->input->post('type') : NULL,
 			'carnegie_accreditation' 	=>  $this->input->post('carnegie_accreditation'),
 			'total_students' 		=>	$this->input->post('total_students') ? $this->input->post('total_students') : NULL,
 			'total_students_UG' 			=>	$this->input->post('total_students_UG') ? $this->input->post('total_students_UG') : NULL,
 			'total_students_PG' 		=>	$this->input->post('total_students_PG') ? $this->input->post('total_students_PG') : NULL,
 			'total_students_international' 			=>	$this->input->post('total_students_international') ? $this->input->post('total_students_international') : NULL,
            'ranking_usa' 			=>	$this->input->post('ranking_usa') ? $this->input->post('ranking_usa') : NULL,
            'ranking_world' 			=>	$this->input->post('ranking_world') ? $this->input->post('ranking_world') : NULL,
 			'admission_success_rate' 			=>	$this->input->post('admission_success_rate') ? $this->input->post('admission_success_rate') : NULL,
 			'unique_selling_points' 	=>	$unique_selling_points ? $unique_selling_points : NULL,
            'research_spending' 	=>	$this->input->post('research_spending') ? $this->input->post('research_spending') : NULL,
            'placement_percentage' 	=>	$this->input->post('placement_percentage') ? $this->input->post('placement_percentage') : NULL,
            'national_ranking_source' 	=>  $this->input->post('national_ranking_source'),
            'worldwide_ranking_source' 	=>  $this->input->post('worldwide_ranking_source'),
            'map_normal_view' 	=>	$this->input->post('map_normal_view') ? $this->input->post('map_normal_view') : NULL,
            'map_street_view' 	=>	$this->input->post('map_street_view') ? $this->input->post('map_street_view') : NULL,
            'whatsapp_link' 	=>	$this->input->post('whatsapp_link') ? $this->input->post('whatsapp_link') : NULL,
            'date_created' 	=>	date('Y-m-d H:i:s'),
            'date_updated' 	=>	date('Y-m-d H:i:s'),
            'added_by' 	=>	1,
            'updated_by' 	=>	1
			);


			/*echo '<pre>';
			print_r($data);
			echo '</pre>';
			exit(); */
            $insertUniversity = $this->user_model->insertUniDetails($data);

	     if($insertUniversity['success']){

		 if($this->input->post('notification')==1){

             $userEmail = $this->input->post('email');
             $name = $this->input->post('adminname');
             $username = $this->input->post('username');
             $password = $this->input->post('password');

             $mailSubject = "Notification on creation of your University Account Under HelloUni";
             $mailTemplate = "Hi $name,

                                 Your University Account has been sucessfully created by Super Admin under HelloUni. Your login credential is as follows

                                 Username: $username
                                 Password: $password

                                 Kindly login to your account and explore the options.

                                 <a href='https://www.hellouni.org/admin'>https://www.hellouni.org/admin</a>

                                 <b>Thanks and Regards,
                                 HelloUni Coordinator
                                 +91 81049 09690</b>

                                 <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

             $ccMailList = '';
             $mailAttachments = '';

             $sendMail = sMail($userEmail, $userName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

			}
		 $this->session->set_flashdata('flash_message', "Success: You have saved University!");
	     redirect('university/university/');
	     }
	  // }
	}


	 function edit()
	 {
         if(!$this->input->post('uniname') || !$this->input->post('email') || !$this->input->post('country'))
         {
             $this->session->set_flashdata('flash_message', "Error: Please fill all mandatory fields!");
    	     redirect('university/university/form');
         }
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('university/user_model');


	  /* $this->form_validation->set_rules('name', 'Name', 'required|trim|xss_clean');

	   $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean');

	   if ($this->form_validation->run() == FALSE)
		{

			 $this->render_page('templates/user/admin_form');
		}
		else
		{*/
        $unique_selling_points = '';
        if($this->input->post('unique_selling_points'))
        {
            $unique_selling_points = implode('--', $this->input->post('unique_selling_points'));
            $unique_selling_points = '--' . $unique_selling_points;
        }
			$data = array(
        	'id'            =>  $this->uri->segment('4'),
			'name' 			=>  $this->input->post('uniname'),
        	'email' 		=>  $this->input->post('email'),
			'type' 			=>  '3',
			'status' 		=>	$this->input->post('status'),
			'view' 			=>	$this->input->post('view'),
			'create' 		=>	$this->input->post('creation'),
			'edit' 			=>	$this->input->post('edit'),
			'del' 			=>	$this->input->post('deletion'),
			'notification' 	=>	$this->input->post('notification'),
            'website' 		=>  $this->input->post('website'),
            'about' 		=>  $this->input->post('about'),
            'country_id' => $this->input->post('country'),
         	'establishment_year' 		=>  $this->input->post('establishment_year') ? $this->input->post('establishment_year') : NULL,
 			'university_type' 			=>  $this->input->post('type') ? $this->input->post('type') : NULL,
 			'carnegie_accreditation' 	=>  $this->input->post('carnegie_accreditation'),
 			'total_students' 		=>	$this->input->post('total_students') ? $this->input->post('total_students') : NULL,
 			'total_students_UG' 			=>	$this->input->post('total_students_UG') ? $this->input->post('total_students_UG') : NULL,
 			'total_students_PG' 		=>	$this->input->post('total_students_PG') ? $this->input->post('total_students_PG') : NULL,
 			'total_students_international' 			=>	$this->input->post('total_students_international') ? $this->input->post('total_students_international') : NULL,
            'ranking_usa' 			=>	$this->input->post('ranking_usa') ? $this->input->post('ranking_usa') : NULL,
            'ranking_world' 			=>	$this->input->post('ranking_world') ? $this->input->post('ranking_world') : NULL,
 			'admission_success_rate' 			=>	$this->input->post('admission_success_rate') ? $this->input->post('admission_success_rate') : NULL,
 			'unique_selling_points' 	=>	$unique_selling_points ? $unique_selling_points : NULL,
            'research_spending' 	=>	$this->input->post('research_spending') ? $this->input->post('research_spending') : NULL,
            'placement_percentage' 	=>	$this->input->post('placement_percentage') ? $this->input->post('placement_percentage') : NULL,
            'national_ranking_source' 	=>  $this->input->post('national_ranking_source'),
            'worldwide_ranking_source' 	=>  $this->input->post('worldwide_ranking_source'),
            'map_normal_view' 	=>	$this->input->post('map_normal_view') ? $this->input->post('map_normal_view') : NULL,
            'map_street_view' 	=>	$this->input->post('map_street_view') ? $this->input->post('map_street_view') : NULL,
            'whatsapp_link' 	=>	$this->input->post('whatsapp_link') ? $this->input->post('whatsapp_link') : NULL,
            'date_updated' 	=>	date('Y-m-d H:i:s'),
            'added_by' 	=>	1,
            'updated_by' 	=>	1

			);
            if($this->input->post('password')){
                $data['password'] = md5($this->input->post('password'));
            }

		 if($this->user_model->editUser($data)=='success'){

		  if($this->input->post('notification')==1){
              $userEmail = $this->input->post('email');
              $name = $this->input->post('adminname');
              $username = $this->input->post('username');
              $password = $this->input->post('password');

              $mailSubject = "Notification on modification of your University Account Under HelloUni";
              $mailTemplate = "Hi $name,

                                  Your University Account has been sucessfully modified by Super Admin under HelloUni.

                                  Kindly login to your account and explore the options.

                                  <a href='https://www.hellouni.org/admin'>https://www.hellouni.org/admin</a>

                                  <b>Thanks and Regards,
                                  HelloUni Coordinator
                                  +91 81049 09690</b>

                                  <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

              $ccMailList = '';
              $mailAttachments = '';

              $sendMail = sMail($userEmail, $userName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

			}

	     $this->session->set_flashdata('flash_message', "Success: You have modified University!");
	      redirect('university/university');
	     }

	   //}
	}


	function delete()
	 {

	   $this->load->helper(array('form', 'url'));
	   $this->load->model('university/user_model');

		if($this->user_model->deleteUser($this->uri->segment('4'))=='success'){

	         $this->session->set_flashdata('flash_message', "Success: You have deleted University!");
	         redirect('university/university');
	    }
	}


	function multidelete(){

	    $this->load->helper(array('form', 'url'));

		$this->load->model('university/user_model');

		$array = $this->input->post('chk');
		foreach($array as $id):

		$this->user_model->deleteUser($id);

		endforeach;
		$this->session->set_flashdata('flash_message', "You have deleted User!");
		 redirect('university/university');


	}

	function trash()
	{
	     $this->load->helper('cookie_helper');
         $this->load->model('university/user_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }

		$cond_user1 = array('user_master.status ' => '5');
		$cond_user2 = array();
        $this->outputData['users'] = $this->user_model->getUser($cond_user1,$cond_user2);
		$this->outputData['view'] = 'list';

        $this->render_page('templates/university/trash_admins',$this->outputData);

    }

	function deletetrash()
	 {

	   $this->load->helper(array('form', 'url'));
	   $this->load->model('university/user_model');

		if($this->user_model->deleteTrash($this->uri->segment('4'))=='success'){

	         $this->session->set_flashdata('flash_message', "Success: You have deleted User!");
	         redirect('university/university/trash');
	    }
	}

	function multidelete_trash(){

	    $this->load->helper(array('form', 'url'));

		$this->load->model('university/user_model');

		$array = $this->input->post('chk');
		foreach($array as $id):

		$this->user_model->deleteTrash($id);

		endforeach;
		$this->session->set_flashdata('flash_message', "You have deleted User!");
		 redirect('university/university/trash');


	}

    function logo()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/university_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $allLogos = $this->university_model->getUnapprovedLogos();
        $this->outputData['all_logos'] = $allLogos;
        $this->outputData['view'] = 'list';
        $this->render_page('templates/user/university_logo', $this->outputData);
    }

    function slider()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/university_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $allSliders = $this->university_model->getUnapprovedSliders();
        $this->outputData['all_sliders'] = $allSliders;
        $this->outputData['view'] = 'list';
        $this->render_page('templates/user/university_slider', $this->outputData);
    }

    function sliderCss()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/university_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $postedData = $this->input->post();
        $universityId = $postedData['university_id'];
        $data['slider_css'] = $postedData['slider_css'];
        $this->university_model->updateUniversity($universityId, $data);
        echo 1;
    }

    function sliderApprove()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/university_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $postedData = $this->input->post();
        $universityId = $postedData['university_id'];
        $data['slider_approved'] = $postedData['slider_approved'];
        $this->university_model->updateUniversity($universityId, $data);
        echo 1;
    }

    function logoCss()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/university_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $postedData = $this->input->post();
        $universityId = $postedData['university_id'];
        $data['style_logo'] = $postedData['style_logo'];
        $this->university_model->updateUniversity($universityId, $data);
        echo 1;
    }

    function logoApprove()
    {
        $this->load->helper('cookie_helper');
        $this->load->model('user/university_model');
        $userdata=$this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $postedData = $this->input->post();
        $universityId = $postedData['university_id'];
        $data['logo_approved'] = $postedData['logo_approved'];
        $this->university_model->updateUniversity($universityId, $data);
        echo 1;
    }

}//End  Home Class

?>
