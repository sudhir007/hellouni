<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package   Reverse bidding system

 * @subpackage  Controllers

 * @category  Common Display

 * @author    FreeLance PHP Script Team

 * @version   Version 1.0

 * @created   December 30 2008

 * @link    http://www.freelancephpscript.com



 */

/*ini_set('display_errors', '1');
 ini_set('display_startup_errors', '1');
 error_reporting(E_ALL);*/

 require APPPATH . "vendor/autoload.php";

 use OpenTok\OpenTok;
 use OpenTok\MediaMode;
 use OpenTok\ArchiveMode;
 use OpenTok\Session;
 use OpenTok\Role;

 require_once APPPATH . 'libraries/Mail/sMail.php';

class Details extends MY_Controller {



  //Global variable

    public $outputData;   //Holds the output data for each view

  public $loggedInUser;



    /**

   * Constructor

   *

   * Loads language files and models needed for this controller

   */

   function __construct()

    {

        parent::__construct();

       // $this->load->library('template');
      //  $this->load->model('user/user_model');
    //$this->load->model('administrator/settings_model');


     // $this->lang->load('enduser/home', $this->config->item('language_code'));

    //$this->load->model('youraccount/global_model');

          //$this->load->model('youraccount/auth_model');

    //$this->load->library('form_validation');

        track_user();



    }

    function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}


function getCourseByCatIdUniId(){

  $this->load->helper(array('form', 'url'));
  $userdata=$this->session->userdata('user');
  if(empty($userdata)){  redirect('user/account'); }

  $this->load->model('course/course_model');
  $this->load->model('user/user_model');

  $catId = $this->input->post('cat_id');
  $uId = $this->input->post('uni_id');

  $disciplinesCourseObj = $this->user_model->getCourseByCatIdUID($catId,$uId);

  $response['courses'] = $disciplinesCourseObj;
  header('Content-Type: application/json');
  echo (json_encode($response));

}


  function index()
  {
    $this->load->helper(array('form', 'url'));
    $userdata=$this->session->userdata('user');
    if(empty($userdata)){
      redirect('user/account');
    }

        $slug = $this->uri->segment(1);
        if(!$slug)
        {
            redirect('/');
        }
     $this->load->helper(array('form', 'url'));
     $this->load->library('form_validation');
     $this->load->model('user/user_model');
     $this->load->model('course/course_model');
     $this->load->model('course/degree_model');
     $this->load->model('webinar/common_model');
     $this->load->model('tokbox_model');

       $slugUniversity = $this->user_model->getUniversityBySlug($slug);
       if(!$slugUniversity)
       {
           redirect('/');
       }

       //$userIpAddress = $this->get_client_ip();

       $uId = $userdata['id'] ? $userdata['id'] : 0;
       $uniId = $slugUniversity[0]['id'];
       $dataObj = [
         "user_id" => $uId,
         "entity_id" => $uniId,
         "entity_type" => "universities"
       ];

       //$insertObj = $this->common_model->post($dataObj,"unique_count");
       $insertObj = $this->common_model->post($dataObj,"views_detail");

       $DisciplinesObj = $this->user_model->getTrendingDisciplinesByUID($slugUniversity[0]['id']);

       $disciplineTList = [];
       $disciplineBList = [];
       $disciplinepercentage = [];
       $totalDiscipline = [];
       $dount = 0;

       foreach ($DisciplinesObj as $dkey => $dvalue) {

          array_push($disciplineBList,["id"=> $dvalue['id'], "university_id" => $dvalue['university_id'],"name" => $dvalue['name'], "img" => $dvalue['image_path'], "icon_class" => $dvalue['icon_img'] ]);
          array_push($totalDiscipline,$dvalue['count']);

          if($dount < 4){
            $disciplineTList[$dvalue['name']] = $dvalue['count'];
          } else {
            $disciplineTList['others'][] = $dvalue['count'];
          }
          $dount = $dount + 1;
       }

       $totalVal = array_sum($totalDiscipline);
       $disciplineTPList = [];

       foreach ($disciplineTList as $pkey => $pvalue) {
         // code...
         if($pkey == 'others'){
           $disciplineTPList[$pkey] = round(((array_sum($disciplineTList[$pkey]) * 100) / $totalVal));
         } else{
           $disciplineTPList[$pkey] = round((($disciplineTList[$pkey] * 100) / $totalVal));
         }

       }

       $this->outputData['trending_discipline'] = $disciplineTPList;
       $this->outputData['browse_discipline'] = $disciplineBList;

       $feesDetailObj = $this->user_model->getAvgFeesByUID($slugUniversity[0]['id']);

       $this->outputData['fees_detail'] = $feesDetailObj;

       $usrdata=$this->session->userdata('user');
       $courseCategoryId = '';
       $uni_search_criteria=$this->session->userdata('uni_search_criteria');
       $uni_details = array();
       $urlUniversityId = $slugUniversity[0]['id'];
       if($uni_search_criteria && $usrdata){
           $courseCategoryIds = explode(",", $uni_search_criteria['subcourse']);
           $uni_details = array();
           foreach($courseCategoryIds as $courseCategoryId){
               $uni_detail = $this->user_model->getUniversityDataByid($urlUniversityId, $courseCategoryId);
               if($uni_detail){
                   foreach($uni_detail as $detail){
                       $uni_details[] = $detail;
                   }
               }
           }
           if(!$uni_details){
               $courseCategoryId = '';
               $uni_details = $this->user_model->getUniversityDataByid($urlUniversityId, $courseCategoryId);
           }
       }
       else{
           $uni_details = $this->user_model->getUniversityDataByid($urlUniversityId, $courseCategoryId);
       }

       $user_id = $usrdata['id'];

       //$universityId = $this->uri->segment(4);
       $appliedUniversity = $this->user_model->checkUniversityApplied($user_id, $urlUniversityId);
       /*echo '<pre>';
       echo "University Id - " . $urlUniversityId . "\n";
       print_r($appliedUniversity);
       exit;*/

       /*echo '<pre>';
       print_r($uni_details);
       exit;*/
       $degrees = array();
       $colleges = array();
       $courses = array();
       $campuses = array();
       $departments = array();
       $courses_details = array();
       $colleges_details = array();
       $departments_details = array();

       $eligibility_gpa = 0;
       $eligibility_gre = 0;
       $eligibility_toefl = 0;
       $eligibility_ielts = 0;
       $eligibility_salary = 0;
       $gpa_scale = 0;

       foreach($uni_details as $uni_data)
       {
           if(!in_array($uni_data['degree'], $degrees))
           {
               $degrees[] = $uni_data['degree'];
           }

           if(!in_array($uni_data['college'], $colleges))
           {
               $colleges[] = $uni_data['college'];
               $temp = array();
               $temp['id'] = $uni_data['college_id'];
               $temp['name'] = $uni_data['college'];
               $temp['total_enrollment'] = $uni_data['college_total_enrollment'];
               $temp['application_deadline_usa'] = $uni_data['college_application_deadline_usa'] ? json_decode($uni_data['college_application_deadline_usa'], true) : '';
               $temp['application_deadline_international'] = $uni_data['college_application_deadline_international'] ? json_decode($uni_data['college_application_deadline_international'], true) : '';
               $temp['application_fees_usa'] = $uni_data['college_application_fees_usa'] ? json_decode($uni_data['college_application_fees_usa'], true) : '';
               $temp['application_fees_international'] = $uni_data['college_application_fees_international'] ? json_decode($uni_data['college_application_fees_international'], true) : '';
               $temp['average_gre'] = $uni_data['college_average_gre'] ? json_decode($uni_data['college_average_gre'], true) : '';
               $temp['admission_url'] = $uni_data['college_admission_url'];
               $temp['establishment_date'] = $uni_data['college_establishment_date'];
               $temp['ranking'] = $uni_data['college_ranking'];
               $temp['college_research_expenditure'] = $uni_data['college_research_expenditure'];
               $temp['research_expenditure_per_faculty'] = $uni_data['college_research_expenditure_per_faculty'];
               $temp['acceptance_rate'] = $uni_data['college_acceptance_rate'];
               $colleges_details[] = $temp;
           }

           if(!in_array($uni_data['department_name'], $departments))
           {
               $departments[] = $uni_data['department_name'];
               $temp = array();
               $temp['id'] = $uni_data['department_id'];
               $temp['name'] = $uni_data['department_name'];
               $temp['total_enrollment'] = $uni_data['department_total_enrollment'];
               $temp['application_deadline_usa'] = $uni_data['department_application_deadline_usa'] ? json_decode($uni_data['department_application_deadline_usa'], true) : '';
               $temp['application_deadline_international'] = $uni_data['department_application_deadline_international'] ? json_decode($uni_data['department_application_deadline_international'], true) : '';
               $temp['application_fees_usa'] = $uni_data['department_application_fees_usa'] ? json_decode($uni_data['department_application_fees_usa'], true) : '';
               $temp['application_fees_international'] = $uni_data['department_application_fees_international'] ? json_decode($uni_data['department_application_fees_international'], true) : '';
               $temp['admissions_department_url'] = $uni_data['department_admissions_department_url'];
               $departments_details[] = $temp;
           }

           $courseString = $uni_data['course'] . '|' . $uni_data['degree'] . '|' . $uni_data['department_name'] . '|' . $uni_data['college'];
           if(!in_array($courseString, $courses))
           {
               $courses[] = $courseString;
               $temp = array();
               $temp['course_id'] = $uni_data['course_id'];
               $temp['name'] = $uni_data['course'];
               $temp['college'] = $uni_data['college'];
               $temp['degree'] = $uni_data['degree'];
               $temp['department'] = $uni_data['department_name'];
               $temp['total_fees'] = $uni_data['total_fees'];
               $temp['deadline_link'] = $uni_data['deadline_link'];
               $temp['course_duration'] = $uni_data['course_duration'];
               $temp['gpa'] = $uni_data['eligibility_gpa'];
               $temp['gpa_scale'] = $uni_data['gpa_scale'];
               $temp['gre'] = $uni_data['eligibility_gre'];
               $temp['toefl'] = $uni_data['eligibility_toefl'];
               $temp['ielts'] = $uni_data['eligibility_ielts'];
               $temp['application_fee_course'] = $uni_data['application_fee_course'];
               $temp['start_months'] = $uni_data['start_months'];
               $temp['stay_back_option'] = $uni_data['stay_back_option'];
               $temp['eligibility_salary'] = $uni_data['eligibility_salary'];
               $temp['course_view_count'] = $uni_data['course_view_count'];
               $temp['course_likes_count'] = $uni_data['course_likes_count'];
               $temp['course_fav_count'] = $uni_data['course_fav_count'];
               $courses_details[] = $temp;
           }

           if(!in_array($uni_data['campus'], $campuses))
           {
               $campuses[] = $uni_data['campus'];
           }

           if($uni_data['eligibility_gpa'] && (!$eligibility_gpa || $eligibility_gpa > $uni_data['eligibility_gpa']))
           {
               $eligibility_gpa = $uni_data['eligibility_gpa'];
               $gpa_scale = $uni_data['gpa_scale'];
           }

           if($uni_data['eligibility_gre'] && (!$eligibility_gre || $eligibility_gre > $uni_data['eligibility_gre']))
           {
               $eligibility_gre = $uni_data['eligibility_gre'];
           }

           if($uni_data['eligibility_toefl'] && (!$eligibility_toefl || $eligibility_toefl > $uni_data['eligibility_toefl']))
           {
               $eligibility_toefl = $uni_data['eligibility_toefl'];
           }

           if($uni_data['eligibility_ielts'] && (!$eligibility_ielts || $eligibility_ielts > $uni_data['eligibility_ielts']))
           {
               $eligibility_ielts = $uni_data['eligibility_ielts'];
           }
       }

     //User details
       $this->outputData['slug']            = $slug;
     $this->outputData['id']            = $uni_details[0]['id'];
       $this->outputData['login_id']        = $uni_details[0]['login_id'];
     $this->outputData['name']          = $uni_details[0]['name'];
     $this->outputData['email']           = $uni_details[0]['email'];
      $this->outputData['images']          = $uni_details[0]['images'] ? json_decode($uni_details[0]['images'], true) : array();
       $this->outputData['logo']          = $uni_details[0]['logo'];
       $this->outputData['style_logo']          = $uni_details[0]['style_logo'];
       $this->outputData['ranking_usa']       = $uni_details[0]['ranking_usa'];
       $this->outputData['ranking']            = $uni_details[0]['ranking_world'];
       $this->outputData['placement_percentage']    = $uni_details[0]['placement_percentage'];
       $this->outputData['admission_success_rate']    = $uni_details[0]['admission_success_rate'];
       $this->outputData['logo_approved']    = $uni_details[0]['logo_approved'];
       $this->outputData['consent']             = $uni_details[0]['consent_data'];
       $this->outputData['map_street_view']        = $uni_details[0]['map_street_view'];
       $this->outputData['map_normal_view']        = $uni_details[0]['map_normal_view'];
       $this->outputData['university_gallery']     = json_decode($uni_details[0]['university_gallery']);
       $this->outputData['preview']    = $this->input->get('preview') ? $this->input->get('preview') : '';

     //University Details
     $this->outputData['about']           = $uni_details[0]['about'];
     $this->outputData['website']         = $uni_details[0]['website'];
     $this->outputData['type']           = $uni_details[0]['type'];
     $this->outputData['establishment_year']  = $uni_details[0]['establishment_year'];
     $this->outputData['carnegie_accreditation']  = $uni_details[0]['carnegie_accreditation'];
     $this->outputData['banner']            = $uni_details[0]['banner'];
     $this->outputData['student']         = $uni_details[0]['total_students'];
     $this->outputData['internationalstudent']  = $uni_details[0]['total_students_international'];
     $this->outputData['facilities']        = $uni_details[0]['facilities'];
     $this->outputData['usps']                     = $uni_details[0]['unique_selling_points'] ? explode("--", $uni_details[0]['unique_selling_points']) : array();
     $this->outputData['research_spending']          = $uni_details[0]['research_spending'];
     //$this->outputData['address']         = $uni_details->address;
     $this->outputData['country']         = $uni_details[0]['country_name'];
     $this->outputData['currency']         = $uni_details[0]['currency'];
       $this->outputData['group_url']         = $uni_details[0]['group_url'];
       $this->outputData['degrees']                 = $degrees;
       $this->outputData['colleges']                 = $colleges;
       $this->outputData['courses']                 = $courses;
       $this->outputData['campuses']                 = $campuses;
       $this->outputData['eligibility_gpa']            = $eligibility_gpa;
       $this->outputData['eligibility_gre']            = $eligibility_gre;
       $this->outputData['eligibility_toefl']            = $eligibility_toefl;
       $this->outputData['eligibility_ielts']            = $eligibility_ielts;
       $this->outputData['gpa_scale']            = $gpa_scale;
       $this->outputData['courses_details']            = $courses_details;
       $this->outputData['colleges_details']    = $colleges_details;
       $this->outputData['departments_details'] = $departments_details;
       $this->outputData['applied'] = 0;
       if($appliedUniversity)
       {
           $this->outputData['applied'] = 1;
       }
     //$this->outputData['state']           = $uni_details->state;
     //$this->outputData['city']          = $uni_details->city;


      /*$this->outputData['mainstream']     = $this->input->post('mainstream');
    $this->outputData['courses']    = $this->input->post('courses');
    $this->outputData['degree']     = $this->input->post('drg');
    $this->outputData['countries'] = $this->input->post('countries');*/

        /*if($uni_search_criteria)
    {

        $condition3 = array(
              'university_to_degree_to_course.degree_id'    => $uni_search_criteria['degree'],
              'university_to_degree_to_course.course_id'    => $uni_search_criteria['subcourse'],
              'university_to_degree_to_course.university_id'  => $this->uri->segment(4)
            );

    $university_to_degree_to_course = $this->user_model->getuniversity_to_degree_to_course($condition3);

     $this->outputData['id']          = $university_to_degree_to_course->id;
     $this->outputData['degree_id']       = $university_to_degree_to_course->degree_id;
     $this->outputData['degree_name']     = $this->degree_model->getDegreeByid(array('degree_master.id'=>$university_to_degree_to_course->degree_id))->name;
     $this->outputData['course_id']       = $university_to_degree_to_course->course_id;
     $this->outputData['university_id']     = $university_to_degree_to_course->university_id;
     $this->outputData['language']      = $university_to_degree_to_course->language;
     $this->outputData['admissionsemester']   = $university_to_degree_to_course->admissionsemester;
     $this->outputData['beginning']       = $university_to_degree_to_course->beginning;
     $this->outputData['duration']      = $university_to_degree_to_course->duration;
     $this->outputData['application_deadline']= $university_to_degree_to_course->application_deadline;
     $this->outputData['details']       = $university_to_degree_to_course->details;

     $this->outputData['tutionfee']       = $university_to_degree_to_course->tutionfee;
     $this->outputData['enrolmentfee']    = $university_to_degree_to_course->enrolmentfee;
     $this->outputData['livingcost']      = $university_to_degree_to_course->livingcost;
     $this->outputData['jobopportunities']  = $university_to_degree_to_course->jobopportunities;
     $this->outputData['required_language']   = $university_to_degree_to_course->required_language;
     $this->outputData['academicrequirement'] = $university_to_degree_to_course->academicrequirement;
   }*/

        $this->outputData['userLoggedIn'] = false;
       $userdata=$this->session->userdata('user');
       if($userdata){
           $this->outputData['userLoggedIn'] = true;
           $alreadyConnected = $this->user_model->getSlotByUserAndUniversity($userdata['id'], $this->outputData['id']);

           //$priCurrentTime = date("Y-m-d H:i:s");
          /* $timestamp = time(); // get the current timestamp
            $priCurrentTime = date('Y-m-d H:i:s', $timestamp); // format the timestamp as a string in the desired format
            echo $priCurrentTime;

           $datetime1 = $priCurrentTime;
            $datetime2 = new DateTime($alreadyConnected->confirmed_slot);
echo "<pre>"; print_r($datetime1);
echo "<pre>"; print_r($datetime2);
            if ($datetime1 > $datetime2) {
                echo 'datetime1 greater than datetime2';
            }

            if ($datetime1 < $datetime2) {
                echo 'datetime1 lesser than datetime2';
            }

            if ($datetime1 == $datetime2) {
                echo 'datetime2 is equal than datetime1';
            }

           if($priCurrentTime > $alreadyConnected->confirmed_slot){
            echo "<pre>";print_r('my slot expired');
           }else{
            echo "<pre>";print_r('my slot not expired');
           }
           echo "<pre>"; print_r(var_dump($priCurrentTime == $alreadyConnected->confirmed_slot)); 
           echo "<pre>"; print_r(var_dump($priCurrentTime <  $alreadyConnected->confirmed_slot)); 
           echo "<pre>"; print_r(var_dump($priCurrentTime >  $alreadyConnected->confirmed_slot)); 
           echo "<pre>"; print_r($priCurrentTime); 
           echo "<pre>"; print_r($alreadyConnected->confirmed_slot); 
           echo "<pre>"; print_r($alreadyConnected); exit;*/
           if($alreadyConnected){
               $alreadyConnected->slot_1_show = $alreadyConnected->slot_1 ? date('d-M-Y h:i A', strtotime($alreadyConnected->slot_1)) : NULL;
               $alreadyConnected->slot_2_show = $alreadyConnected->slot_2 ? date('d-M-Y h:i A', strtotime($alreadyConnected->slot_2)) : NULL;
               $alreadyConnected->slot_3_show = $alreadyConnected->slot_3 ? date('d-M-Y h:i A', strtotime($alreadyConnected->slot_3)) : NULL;
               $this->outputData['alreadyConnected'] = $alreadyConnected;
               $this->outputData['show_url'] = false;

               if(!$this->outputData['applied'])
               {
                   $participants = array("user_id" => $user_id, "slot_id" => $alreadyConnected->id);
                   $json_participant = json_encode($participants);
                   $token = $this->tokbox_model->getTokenByParticipant($json_participant);
                   if($token && $token->token)
                   {
                       $encodedTokboxId = base64_encode($token->id);
                       $this->outputData['tokbox_url'] = '/tokbox?id=' . $encodedTokboxId;

                       if($token->expiry_time)
                       {
                           $expiryTime = $token->expiry_time;
                           $currentTime = date('Y-m-d H:i:s');

                           $time1 = strtotime($expiryTime);
                           $time2 = strtotime($currentTime);

                           if($expiryTime && $time2 < $time1){
                               $this->outputData['show_url'] = true;
                           }
                       }
                   }
               }
           }

           $condition = array('user_master.id'=>$userdata['id'],'user_master.status'=>1,'user_master.type'=>5);

           $userDetails = $this->user_model->getStudentDetails($condition);
           //$qualifiedExams = $this->user_model->checkQualifiedExam(array('student_id'=>$userdata['id']));
           $leadCondition = array("email" => $userdata['email']);
           $leadMaster = $this->user_model->getLead($leadCondition);

           $mandatoryFields = $uni_details[0]['mandatory_fields'];
           if($mandatoryFields)
           {
               $this->load->model('location/country_model');
               $this->load->model('location/city_model');

               $this->outputData['locationcountries'] = $this->country_model->getLocationCountries(array('location_country_master.status'=>1));
               $this->outputData['cities'] = $this->city_model->getAllCities();
               $this->outputData['mainstreams'] = $this->course_model->getAllCollegeCategories();
               $this->outputData['levels'] = $this->course_model->getAllDegrees();
               if($leadMaster->mainstream)
               {
                   $this->outputData['subCourses'] = $this->course_model->getCourseCategoriesByCollegeCategory($leadMaster->mainstream);
               }

               $mandatoryFields = json_decode($mandatoryFields, true);
               foreach($mandatoryFields as $fieldList)
               {
                   foreach($fieldList as $tableName => $fieldArray)
                   {
                       foreach($fieldArray as $field)
                       {
                           if($tableName == 'user_master' || $tableName == 'student_details')
                           {
                               $this->outputData[$tableName][$field] = $userDetails->$field;
                           }
                           else if($tableName == 'lead_master')
                           {
                               $this->outputData[$tableName][$field] = $leadMaster->$field;
                           }
                       }
                   }
               }
           }

           $studentName = $userdata['name'];
           $date = date('Y-m-d');
            $universityName = $this->outputData['name'];
            $countryName = $this->outputData['country'];
            $sessionType = 'One On One Session';

            $this->outputData['consent'] = str_replace('$todayDate', $date, $this->outputData['consent']);
            $this->outputData['consent'] = str_replace('$universityName', $universityName, $this->outputData['consent']);
            $this->outputData['consent'] = str_replace('$sessionType', $sessionType, $this->outputData['consent']);
            $this->outputData['consent'] = str_replace('$studentName', $studentName, $this->outputData['consent']);
            $this->outputData['consent'] = str_replace('$countryName', $countryName, $this->outputData['consent']);
       }
       /*echo '<pre>';
       print_r($this->outputData);
       exit;*/
    //echo '<pre>'; print_r($university_to_degree_to_course); echo '</pre>'; exit;
    $this->render_page('templates/university/university_details',$this->outputData);
  }

  function universityInfo(){

    $this->load->helper(array('form', 'url'));
     $this->load->library('form_validation');
     $this->load->model('user/user_model');
     $this->load->model('course/course_model');
     $this->load->model('course/degree_model');
     $this->load->model('webinar/common_model');
     $this->load->model('tokbox_model');

     $userdata=$this->session->userdata('user');
    if(empty($userdata)){
      redirect('user/account');
    }

    $slugId = $this->uri->segment(2);
    if(!$slugId)
    {
        redirect('/');
    }

    $slugUniversity = $this->user_model->getUniversityBySlug($slugId);
       if(!$slugUniversity)
       {
           redirect('/');
       }

    $universityObj = $this->common_model->get(["slug" => $slugId], "universities");
    $universityId = $universityObj['data'][0]['id'];
    $universityInfoObj = $this->common_model->get(["university_id" => $universityId], "university_info");
    $universityOverviewInfoObj = $this->common_model->get(["university_id" => $universityId], "university_overview_info");
    $universityScholarshipsFinancialInfoObj = $this->common_model->get(["university_id" => $universityId], "university_scholarships_financial_info");
    $universityFaqInfoObj = $this->common_model->get(["university_id" => $universityId], "university_faq_info");
    $universityAdmissionInfoObj = $this->common_model->get(["university_id" => $universityId], "university_admission_info");

    if($universityOverviewInfoObj['data']){
      $universityOverviewInfoObj['data'][0]['average_eligibility'] = json_decode($universityOverviewInfoObj['data'][0]['average_eligibility'], true);
    }

    $this->outputData['university'] = $universityObj['data'];
    //var_dump($universityId);
    //var_dump($universityObj);
    //var_dump($universityOverviewInfoObj);die();
    $this->outputData['university_info'] = $universityInfoObj['data'];
    $this->outputData['university_overview_info'] = $universityOverviewInfoObj['data'];
    $this->outputData['university_scholarships_financial_info'] = $universityScholarshipsFinancialInfoObj['data'];
    $this->outputData['university_faq_info'] = $universityFaqInfoObj['data'];
    $this->outputData['university_admission_info'] = $universityAdmissionInfoObj['data'];
    //var_dump($universityAdmissionInfoObj);

    /* old index code start */

    $uId = $userdata['id'] ? $userdata['id'] : 0;
       $uniId = $slugUniversity[0]['id'];
       $dataObj = [
         "user_id" => $uId,
         "entity_id" => $uniId,
         "entity_type" => "universities"
       ];

       //$insertObj = $this->common_model->post($dataObj,"unique_count");
       $insertObj = $this->common_model->post($dataObj,"views_detail");

       $DisciplinesObj = $this->user_model->getTrendingDisciplinesByUID($slugUniversity[0]['id']);

       $disciplineTList = [];
       $disciplineBList = [];
       $disciplinepercentage = [];
       $totalDiscipline = [];
       $dount = 0;

       foreach ($DisciplinesObj as $dkey => $dvalue) {

          array_push($disciplineBList,["id"=> $dvalue['id'], "university_id" => $dvalue['university_id'],"name" => $dvalue['name'], "img" => $dvalue['image_path'], "icon_class" => $dvalue['icon_img'] ]);
          array_push($totalDiscipline,$dvalue['count']);

          if($dount < 4){
            $disciplineTList[$dvalue['name']] = $dvalue['count'];
          } else {
            $disciplineTList['others'][] = $dvalue['count'];
          }
          $dount = $dount + 1;
       }

       $totalVal = array_sum($totalDiscipline);
       $disciplineTPList = [];

       foreach ($disciplineTList as $pkey => $pvalue) {
         // code...
         if($pkey == 'others'){
           $disciplineTPList[$pkey] = round(((array_sum($disciplineTList[$pkey]) * 100) / $totalVal));
         } else{
           $disciplineTPList[$pkey] = round((($disciplineTList[$pkey] * 100) / $totalVal));
         }

       }

       $this->outputData['trending_discipline'] = $disciplineTPList;
       $this->outputData['browse_discipline'] = $disciplineBList;

       $feesDetailObj = $this->user_model->getAvgFeesByUID($slugUniversity[0]['id']);

       $this->outputData['fees_detail'] = $feesDetailObj;

       $usrdata=$this->session->userdata('user');
       $courseCategoryId = '';
       $uni_search_criteria=$this->session->userdata('uni_search_criteria');
       $uni_details = array();
       $urlUniversityId = $slugUniversity[0]['id'];
       if($uni_search_criteria && $usrdata){
           $courseCategoryIds = explode(",", $uni_search_criteria['subcourse']);
           $uni_details = array();
           foreach($courseCategoryIds as $courseCategoryId){
               $uni_detail = $this->user_model->getUniversityDataByid($urlUniversityId, $courseCategoryId);
               if($uni_detail){
                   foreach($uni_detail as $detail){
                       $uni_details[] = $detail;
                   }
               }
           }
           if(!$uni_details){
               $courseCategoryId = '';
               $uni_details = $this->user_model->getUniversityDataByid($urlUniversityId, $courseCategoryId);
           }
       }
       else{
           $uni_details = $this->user_model->getUniversityDataByid($urlUniversityId, $courseCategoryId);
       }

       $user_id = $usrdata['id'];

       //$universityId = $this->uri->segment(4);
       $appliedUniversity = $this->user_model->checkUniversityApplied($user_id, $urlUniversityId);
       /*echo '<pre>';
       echo "University Id - " . $urlUniversityId . "\n";
       print_r($appliedUniversity);
       exit;*/

       /*echo '<pre>';
       print_r($uni_details);
       exit;*/
       $degrees = array();
       $colleges = array();
       $courses = array();
       $campuses = array();
       $departments = array();
       $courses_details = array();
       $colleges_details = array();
       $departments_details = array();

       $eligibility_gpa = 0;
       $eligibility_gre = 0;
       $eligibility_toefl = 0;
       $eligibility_ielts = 0;
       $eligibility_salary = 0;
       $gpa_scale = 0;

       foreach($uni_details as $uni_data)
       {
           if(!in_array($uni_data['degree'], $degrees))
           {
               $degrees[] = $uni_data['degree'];
           }

           if(!in_array($uni_data['college'], $colleges))
           {
               $colleges[] = $uni_data['college'];
               $temp = array();
               $temp['id'] = $uni_data['college_id'];
               $temp['name'] = $uni_data['college'];
               $temp['total_enrollment'] = $uni_data['college_total_enrollment'];
               $temp['application_deadline_usa'] = $uni_data['college_application_deadline_usa'] ? json_decode($uni_data['college_application_deadline_usa'], true) : '';
               $temp['application_deadline_international'] = $uni_data['college_application_deadline_international'] ? json_decode($uni_data['college_application_deadline_international'], true) : '';
               $temp['application_fees_usa'] = $uni_data['college_application_fees_usa'] ? json_decode($uni_data['college_application_fees_usa'], true) : '';
               $temp['application_fees_international'] = $uni_data['college_application_fees_international'] ? json_decode($uni_data['college_application_fees_international'], true) : '';
               $temp['average_gre'] = $uni_data['college_average_gre'] ? json_decode($uni_data['college_average_gre'], true) : '';
               $temp['admission_url'] = $uni_data['college_admission_url'];
               $temp['establishment_date'] = $uni_data['college_establishment_date'];
               $temp['ranking'] = $uni_data['college_ranking'];
               $temp['college_research_expenditure'] = $uni_data['college_research_expenditure'];
               $temp['research_expenditure_per_faculty'] = $uni_data['college_research_expenditure_per_faculty'];
               $temp['acceptance_rate'] = $uni_data['college_acceptance_rate'];
               $colleges_details[] = $temp;
           }

           if(!in_array($uni_data['department_name'], $departments))
           {
               $departments[] = $uni_data['department_name'];
               $temp = array();
               $temp['id'] = $uni_data['department_id'];
               $temp['name'] = $uni_data['department_name'];
               $temp['total_enrollment'] = $uni_data['department_total_enrollment'];
               $temp['application_deadline_usa'] = $uni_data['department_application_deadline_usa'] ? json_decode($uni_data['department_application_deadline_usa'], true) : '';
               $temp['application_deadline_international'] = $uni_data['department_application_deadline_international'] ? json_decode($uni_data['department_application_deadline_international'], true) : '';
               $temp['application_fees_usa'] = $uni_data['department_application_fees_usa'] ? json_decode($uni_data['department_application_fees_usa'], true) : '';
               $temp['application_fees_international'] = $uni_data['department_application_fees_international'] ? json_decode($uni_data['department_application_fees_international'], true) : '';
               $temp['admissions_department_url'] = $uni_data['department_admissions_department_url'];
               $departments_details[] = $temp;
           }

           $courseString = $uni_data['course'] . '|' . $uni_data['degree'] . '|' . $uni_data['department_name'] . '|' . $uni_data['college'];
           if(!in_array($courseString, $courses))
           {
               $courses[] = $courseString;
               $temp = array();
               $temp['course_id'] = $uni_data['course_id'];
               $temp['name'] = $uni_data['course'];
               $temp['college'] = $uni_data['college'];
               $temp['degree'] = $uni_data['degree'];
               $temp['department'] = $uni_data['department_name'];
               $temp['total_fees'] = $uni_data['total_fees'];
               $temp['deadline_link'] = $uni_data['deadline_link'];
               $temp['course_duration'] = $uni_data['course_duration'];
               $temp['gpa'] = $uni_data['eligibility_gpa'];
               $temp['gpa_scale'] = $uni_data['gpa_scale'];
               $temp['gre'] = $uni_data['eligibility_gre'];
               $temp['toefl'] = $uni_data['eligibility_toefl'];
               $temp['ielts'] = $uni_data['eligibility_ielts'];
               $temp['application_fee_course'] = $uni_data['application_fee_course'];
               $temp['start_months'] = $uni_data['start_months'];
               $temp['stay_back_option'] = $uni_data['stay_back_option'];
               $temp['eligibility_salary'] = $uni_data['eligibility_salary'];
               $temp['course_view_count'] = $uni_data['course_view_count'];
               $temp['course_likes_count'] = $uni_data['course_likes_count'];
               $temp['course_fav_count'] = $uni_data['course_fav_count'];
               $courses_details[] = $temp;
           }

           if(!in_array($uni_data['campus'], $campuses))
           {
               $campuses[] = $uni_data['campus'];
           }

           if($uni_data['eligibility_gpa'] && (!$eligibility_gpa || $eligibility_gpa > $uni_data['eligibility_gpa']))
           {
               $eligibility_gpa = $uni_data['eligibility_gpa'];
               $gpa_scale = $uni_data['gpa_scale'];
           }

           if($uni_data['eligibility_gre'] && (!$eligibility_gre || $eligibility_gre > $uni_data['eligibility_gre']))
           {
               $eligibility_gre = $uni_data['eligibility_gre'];
           }

           if($uni_data['eligibility_toefl'] && (!$eligibility_toefl || $eligibility_toefl > $uni_data['eligibility_toefl']))
           {
               $eligibility_toefl = $uni_data['eligibility_toefl'];
           }

           if($uni_data['eligibility_ielts'] && (!$eligibility_ielts || $eligibility_ielts > $uni_data['eligibility_ielts']))
           {
               $eligibility_ielts = $uni_data['eligibility_ielts'];
           }
       }

     //User details
       $this->outputData['slug']            = $slugId;
     $this->outputData['id']            = $uni_details[0]['id'];
       $this->outputData['login_id']        = $uni_details[0]['login_id'];
     $this->outputData['name']          = $uni_details[0]['name'];
     $this->outputData['email']           = $uni_details[0]['email'];
      $this->outputData['images']          = $uni_details[0]['images'] ? json_decode($uni_details[0]['images'], true) : array();
       $this->outputData['logo']          = $uni_details[0]['logo'];
       $this->outputData['style_logo']          = $uni_details[0]['style_logo'];
       $this->outputData['ranking_usa']       = $uni_details[0]['ranking_usa'];
       $this->outputData['ranking']            = $uni_details[0]['ranking_world'];
       $this->outputData['placement_percentage']    = $uni_details[0]['placement_percentage'];
       $this->outputData['admission_success_rate']    = $uni_details[0]['admission_success_rate'];
       $this->outputData['logo_approved']    = $uni_details[0]['logo_approved'];
       $this->outputData['consent']             = $uni_details[0]['consent_data'];
       $this->outputData['map_street_view']        = $uni_details[0]['map_street_view'];
       $this->outputData['map_normal_view']        = $uni_details[0]['map_normal_view'];
       $this->outputData['university_gallery']     = json_decode($uni_details[0]['university_gallery']);
       $this->outputData['preview']    = $this->input->get('preview') ? $this->input->get('preview') : '';

     //University Details
     $this->outputData['about']           = $uni_details[0]['about'];
     $this->outputData['website']         = $uni_details[0]['website'];
     $this->outputData['type']           = $uni_details[0]['type'];
     $this->outputData['establishment_year']  = $uni_details[0]['establishment_year'];
     $this->outputData['carnegie_accreditation']  = $uni_details[0]['carnegie_accreditation'];
     $this->outputData['banner']            = $uni_details[0]['banner'];
     $this->outputData['student']         = $uni_details[0]['total_students'];
     $this->outputData['internationalstudent']  = $uni_details[0]['total_students_international'];
     $this->outputData['facilities']        = $uni_details[0]['facilities'];
     $this->outputData['usps']                     = $uni_details[0]['unique_selling_points'] ? explode("--", $uni_details[0]['unique_selling_points']) : array();
     $this->outputData['research_spending']          = $uni_details[0]['research_spending'];
     //$this->outputData['address']         = $uni_details->address;
     $this->outputData['country']         = $uni_details[0]['country_name'];
     $this->outputData['currency']         = $uni_details[0]['currency'];
       $this->outputData['group_url']         = $uni_details[0]['group_url'];
       $this->outputData['degrees']                 = $degrees;
       $this->outputData['colleges']                 = $colleges;
       $this->outputData['courses']                 = $courses;
       $this->outputData['campuses']                 = $campuses;
       $this->outputData['eligibility_gpa']            = $eligibility_gpa;
       $this->outputData['eligibility_gre']            = $eligibility_gre;
       $this->outputData['eligibility_toefl']            = $eligibility_toefl;
       $this->outputData['eligibility_ielts']            = $eligibility_ielts;
       $this->outputData['gpa_scale']            = $gpa_scale;
       $this->outputData['courses_details']            = $courses_details;
       $this->outputData['colleges_details']    = $colleges_details;
       $this->outputData['departments_details'] = $departments_details;
       $this->outputData['applied'] = 0;
       if($appliedUniversity)
       {
           $this->outputData['applied'] = 1;
       }
     

        $this->outputData['userLoggedIn'] = false;
       $userdata=$this->session->userdata('user');
       if($userdata){
           $this->outputData['userLoggedIn'] = true;
           $alreadyConnected = $this->user_model->getSlotByUserAndUniversity($userdata['id'], $this->outputData['id']);
           if($alreadyConnected){
               $alreadyConnected->slot_1_show = $alreadyConnected->slot_1 ? date('d-M-Y h:i A', strtotime($alreadyConnected->slot_1)) : NULL;
               $alreadyConnected->slot_2_show = $alreadyConnected->slot_2 ? date('d-M-Y h:i A', strtotime($alreadyConnected->slot_2)) : NULL;
               $alreadyConnected->slot_3_show = $alreadyConnected->slot_3 ? date('d-M-Y h:i A', strtotime($alreadyConnected->slot_3)) : NULL;
               $this->outputData['alreadyConnected'] = $alreadyConnected;
               $this->outputData['show_url'] = false;

               if(!$this->outputData['applied'])
               {
                   $participants = array("user_id" => $user_id, "slot_id" => $alreadyConnected->id);
                   $json_participant = json_encode($participants);
                   $token = $this->tokbox_model->getTokenByParticipant($json_participant);
                   if($token && $token->token)
                   {
                       $encodedTokboxId = base64_encode($token->id);
                       $this->outputData['tokbox_url'] = '/tokbox?id=' . $encodedTokboxId;

                       if($token->expiry_time)
                       {
                           $expiryTime = $token->expiry_time;
                           $currentTime = date('Y-m-d H:i:s');

                           $time1 = strtotime($expiryTime);
                           $time2 = strtotime($currentTime);

                           if($expiryTime && $time2 < $time1){
                               $this->outputData['show_url'] = true;
                           }
                       }
                   }
               }
           }

           $condition = array('user_master.id'=>$userdata['id'],'user_master.status'=>1,'user_master.type'=>5);

           $userDetails = $this->user_model->getStudentDetails($condition);
           //$qualifiedExams = $this->user_model->checkQualifiedExam(array('student_id'=>$userdata['id']));
           $leadCondition = array("email" => $userdata['email']);
           $leadMaster = $this->user_model->getLead($leadCondition);

           $mandatoryFields = $uni_details[0]['mandatory_fields'];
           if($mandatoryFields)
           {
               $this->load->model('location/country_model');
               $this->load->model('location/city_model');

               $this->outputData['locationcountries'] = $this->country_model->getLocationCountries(array('location_country_master.status'=>1));
               $this->outputData['cities'] = $this->city_model->getAllCities();
               $this->outputData['mainstreams'] = $this->course_model->getAllCollegeCategories();
               $this->outputData['levels'] = $this->course_model->getAllDegrees();
               if($leadMaster->mainstream)
               {
                   $this->outputData['subCourses'] = $this->course_model->getCourseCategoriesByCollegeCategory($leadMaster->mainstream);
               }

               $mandatoryFields = json_decode($mandatoryFields, true);
               foreach($mandatoryFields as $fieldList)
               {
                   foreach($fieldList as $tableName => $fieldArray)
                   {
                       foreach($fieldArray as $field)
                       {
                           if($tableName == 'user_master' || $tableName == 'student_details')
                           {
                               $this->outputData[$tableName][$field] = $userDetails->$field;
                           }
                           else if($tableName == 'lead_master')
                           {
                               $this->outputData[$tableName][$field] = $leadMaster->$field;
                           }
                       }
                   }
               }
           }

           $studentName = $userdata['name'];
           $date = date('Y-m-d');
            $universityName = $this->outputData['name'];
            $countryName = $this->outputData['country'];
            $sessionType = 'One On One Session';

            $this->outputData['consent'] = str_replace('$todayDate', $date, $this->outputData['consent']);
            $this->outputData['consent'] = str_replace('$universityName', $universityName, $this->outputData['consent']);
            $this->outputData['consent'] = str_replace('$sessionType', $sessionType, $this->outputData['consent']);
            $this->outputData['consent'] = str_replace('$studentName', $studentName, $this->outputData['consent']);
            $this->outputData['consent'] = str_replace('$countryName', $countryName, $this->outputData['consent']);
       }

    /* old index code end */
    $this->render_page('templates/university/testPageUniversity',$this->outputData);
  }

  function create()

  {
     //Load Models - for this function
   $this->load->model('user/user_model');
   $this->load->model('user/auth_model');
   $this->load->helper('cookie');


     $email = $this->input->post('email');

   $data = array(

      'name'      =>  $this->input->post('name'),
      'email'     =>  $this->input->post('email'),
      'phone'     =>  $this->input->post('phone')

    );


   if($data)
   {$leadId = $this->user_model->insertLead($data);
   }



    $sessiondata = array(
     'id'       => $leadId,
   'name'     => $this->input->post('name'),
     'email'    => $this->input->post('email'),
     'phone'    => $this->input->post('phone'),
   'mainstream'   => $this->input->post('mainstream'),
   'courses'    => $this->input->post('courses'),
     'degree'     => $this->input->post('degree'),
   'countries'    => $this->input->post('countries')
     );


     $this->session->set_userdata('leaduser', $sessiondata);

   redirect('search/university');

}


function logout()
 {
  $this->load->model('user/auth_model');
  $this->auth_model->clearUserSession();
  //$this->session->set_flashdata('flash_message', $this->common_model->flash_message('success',$this->lang->line('logout_success')));
  //$this->auth_model->clearUserCookie(array('username','password'));
   //$userdata=$this->session->userdata('user');
   $this->session->unset_userdata('user');
  //unset($userdata);
  //print_r($userdata); exit();
  //$this->auth_model->clearUserCookie(array('user_name','user_password'));
  redirect('common/login');
  //$this->load->view('youraccount/index',$userdata);

 } //Function logout End



function forgot()

  {
     //Load Models - for this function
   $this->load->helper(array('form', 'url'));
   $this->load->library('form_validation');
   $this->load->model('user/user_model');
   $this->load->model('user/auth_model');
   $this->load->helper('cookie');

   $this->form_validation->set_rules('credencial', 'Credencial', 'required|trim|xss_clean');
   $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean');

   if ($this->form_validation->run() == FALSE)
    {
      $this->load->view('templates/common/login');
      //$this->session->set_flashdata('flash_message', $this->form_validation->validation_errors());
      //redirect('common/login');
      //die('sssss');
    }
    else
    {


   $credencial  = $this->input->post('credencial');
   $email     = $this->input->post('email');

   $conditions  =  array('infra_email_master.email'=>$email,'infra_email_master.contact_type'=>'1');
   $query     = $this->user_model->checkUserBy_Email($conditions);

   $setting_details   = $this->settings_model->getSettings($conditions);

   foreach($setting_details as $setting){
   if($setting->key=='config_email'){
   $admin_email = $this->settings_model->getEmail($setting->value);
    }
   }

   if(count($query)> 0){

   if($query->status_id==1){

   if($credencial=='username'){

   $to = $query->email;
   $from = $admin_email;
   $subject = 'Your User Name';
   $username = $query->name;

   $link = '#'; $linktext = $username;
   $text = 'Your User Name is following -';
   $body = $this->user_model->TempBody($username,$link,$linktext,$text);

   $this->user_model->SendMail($to,$from,$subject,$body) ;
   $this->session->set_flashdata('flash_message', "Please check your mail to get User Name.");

   }elseif($credencial=='password'){

   // send the mail
   $to = $query->email;
   $from = $admin_email;
   $subject = 'Please check you mail';
   $username = $query->name;

  // $user_tbl_data['activation_key']     = md5(time());

   $user_tbl_data = array('reset_key' => md5(time()));

   $this->user_model->Update_User_activation($user_tbl_data,$query->id);


   $link = base_url().'common/login/confirm/'.$query->id.'/'.$user_tbl_data['reset_key'];
   $linktext = $link;
   $text = 'Please click the link below or copy and fire it in the browser to reset password';

   $body = $this->user_model->TempBody($username,$link,$linktext, $text);

   $this->user_model->SendMail($to,$from,$subject,$body) ;
   $this->session->set_flashdata('flash_message', "Please check your mail to reset password.");

    }
   }elseif($query->status_id==2){

   $this->session->set_flashdata('flash_message', "Sorry!Your account is Inactive. Please activate the account first.");

   }elseif($query->status_id==3){

   $this->session->set_flashdata('flash_message', "Sorry!Your account is Blocked. Please contact to supaer admin.");

   }elseif($query->status_id==4){

   $this->session->set_flashdata('flash_message', "Sorry!Your account is Deleted.");

   }else{

    $this->session->set_flashdata('flash_message', "Sorry!Invalid email. Please sign up first.");
   }

   }else{

  $this->session->set_flashdata('flash_message', "Sorry!Invalid email. Please sign up first.");
  }


  }

   redirect('common/login');

}


function confirm()

  {
    if($this->uri->segment('5')){


    $check_reset_data = array('infra_user_master.id' =>$this->uri->segment('4'),'infra_user_master.reset_key' => $this->uri->segment('5'),'infra_user_master.status_id' =>'1');
    $query = $this->user_model->CheckReset($check_reset_data);

    if(count($query)> 0){

    $check_security_data = array('user_security_question.user_id' =>$this->uri->segment('4'));
    $security_query = $this->user_model->getSecurity_Credential($check_security_data);

    $this->outputData['security_question1']= $security_query->question1;
    $this->outputData['security_question2']= $security_query->question2;

    $this->load->view('templates/common/security_verify',$this->outputData);

    }else{



    }
    /*echo '<pre>';
    print_r($query);
    echo '</pre>';*/
    }
  }


  function verification(){

  //echo $this->input->post('currenturl'); exit;

   $check_security_data = array('user_security_question.user_id' =>$this->uri->segment('4'));
   $query = $this->user_model->getSecurity_Credential($check_security_data);


   $security_ans1     = md5($this->input->post('security_ans1'));

   $security_ans2     = md5($this->input->post('security_ans2'));

   if($query->answer1==$security_ans1 && $query->answer2==$security_ans2){

   $this->outputData['userid']= $this->uri->segment('4');
    $this->load->view('templates/common/resetpassword',$this->outputData);

   }else{

    $this->session->set_flashdata('flash_message', "Sorry! Incorrect Security answeres. Please try again");
    redirect($this->input->post('currenturl'));
   }

  }


  function resetpassword(){

     if($this->input->post('submit')){

   $user_tbl_data = array('infra_user_master.password' => md5($this->input->post('newpassword')));

   $this->user_model->Update_User_activation($user_tbl_data,$this->uri->segment('4'));





    $this->session->set_flashdata('flash_message', "Congrats! Pasword has been reset. Please Log In");
    redirect('common/login');

   }
    }

    function connect()
    {
        $this->load->model('user/user_model');
        $userdata=$this->session->userdata('user');
        if(!$userdata){
            redirect('/user/account');
        }

        /*echo '<pre>';
        print_r($userdata);
        print_r($this->input->post());
        exit;*/

        $details = array(
            'name' 			=>  $this->input->post('student_name')
        );
        $this->user_model->updateUserInfo($details, $userdata['id']);
        $studentData = array(
            'dob'					=>	$this->input->post('student_dob'),
            'country'				=>	$this->input->post('student_country'),
            'city'				    =>	$this->input->post('student_city'),
            'passport_number'		=>	$this->input->post('student_passport'),
            'intake_month'			=>	$this->input->post('student_intake_month'),
            'intake_year'			=>	$this->input->post('student_intake_year'),
            'hear_about_us'			=>	$this->input->post('student_hear_about_us')
        );
        $this->user_model->updateStudentDetails($studentData,$userdata['id']);

        $leadMasterDetail = array(
            'mainstream' 		=>  $this->input->post('student_mainstream'),
            'courses' 			=>  $this->input->post('student_subcourse'),
            'degree' 			=>  $this->input->post('student_level')
        );
        $this->user_model->updateLeadInfoByEmail($leadMasterDetail, $userdata['email']);

        $data['user_id'] = $userdata['id'];
        $data['university_id'] = $this->input->post('university_id');
        $data['university_login_id'] = $this->input->post('university_login_id');
        $data['date_created'] = date('Y-m-d H:i:s');
        $data['slot_type'] = 'UNIVERSITY';
        $data['student_signature'] = $this->input->post('student_signature');

        $alreadyConnected = $this->user_model->getSlotByUserAndUniversity($data['user_id'], $data['university_id']);
        if($alreadyConnected)
        {
            $this->session->set_flashdata('flash_message', '<p style="font-size:11px;">Your request is in under process.<br/>Please check your mail for any further information.</p>');
        }

        else
        {
            if($this->user_model->addSlot($data))
            {
                $condition_1 = array("user_master.id" => $data['university_login_id']);
                $condition_2 = array();
                $loginDetail = $this->user_model->getUser($condition_1, $condition_2);
                if($loginDetail)
                {
                    $universityEmail = $loginDetail[0]->email;
                    $universityName = $loginDetail[0]->name;
                    $studentName = $userdata['name'];
                    $studentEmail = $userdata['email'];

                    $ccMailList = '';
                    $mailAttachments = '';

                    $mailSubject = "One To One Request For $universityName Successful";
                    $mailTemplate = "Hi $studentName

                                        It looks like $universityName has found a place in your wishlist!

                                        A representative from the university will be glad to answer all your queries.

                                        We will intimate the representative about your interest in knowing more about the university and request them to share their availability.

                                        <b>Thanks and Regards,
                                        HelloUni Coordinator
                                        +91 81049 09690</b>

                                        <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

                    $sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

                    $mailSubject = "Student's Request For One To One Session";
                    $mailTemplate = "Hi $universityName,

                                    The following student has sent a request for one to one session.

                                    Name = $studentName
                                    Email = $studentEmail

                                    <b>Thanks and Regards,
                                    HelloUni Coordinator
                                    +91 81049 09690</b>

                                    <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

                    $sendMail = sMail($universityEmail, $universityName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);
                }
                $this->session->set_flashdata('flash_message', '<p style="font-size:11px;">Congrats! Your request has been sent.<br/>You will get a notification shortly.</p>');
            }
            else
            {
                $this->session->set_flashdata('flash_message', "Some issue occured while sending your request.");
            }
        }
        redirect('university_info/'.$this->input->post('slug'));
    }

    function slotconfirm()
    {
        $this->load->model('user/user_model');
        $this->load->model('tokbox_model');
        $userdata=$this->session->userdata('user');
        if(!$userdata){
            redirect('/user/account');
        }

        $slot_id = $this->input->post('slot_id');
        $data['confirmed_slot'] = $this->input->post('confirmed_slot');
        $data['confirmed_date'] = date('Y-m-d H:i:s');
        $this->user_model->editSlot($slot_id, $data);

        $this->load->model('tokbox_model');
        $opentok = new OpenTok('46220952', '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2');
        $session = $opentok->createSession(array(
            'mediaMode' => MediaMode::ROUTED
        ));
        // Store this sessionId in the database for later use
        $sessionId = $session->getSessionId();
        //$sessionId = '2_MX40NjIyMDk1Mn5-MTU0MjI2NjMxMzE1M35HQ2FQV2JQNFBKSXVTa0RMelJLdVdtczJ-fg';
        //$token = $opentok->generateToken($sessionId);

        $condition = array('user_master.id'=>$userdata['id'],'user_master.status'=>1,'user_master.type'=>5);
        $userDetails = $this->user_model->getStudentDetails($condition);
        $counselorId = $userDetails->counselor_id;

        $participants = array(
            "user_id" => (string)$userdata['id'],
            "university_id" => (string)$this->input->post('university_id'),
            "counselor_id" => (string)$counselorId
        );
        $tokbox_data['participants'] = json_encode($participants);

        $token_detail = $this->tokbox_model->getTokenByParticipant($tokbox_data['participants']);
        $participants["slot_id"] = (string)$slot_id;
        $tokbox_data['participants'] = json_encode($participants);

        //$tokbox_data['token'] = $token;
        $tokbox_data['session_id'] = $sessionId;
        $tokbox_data['created_at'] = date('Y-m-d H:i:s');
        if($token_detail)
        {
            $tokenId = $token_detail->id;
            $this->tokbox_model->editToken($tokbox_data, $tokenId);
        }
        else
        {
            $tokenId = $this->tokbox_model->addToken($tokbox_data);
        }

        $universityId = $this->input->post('university_login_id');

        $universityEmail = $this->input->post('university_email');
        $universityName = $this->input->post('university_name');
        $universityWebsite = $this->input->post('university_website');
        $studentName = $userdata['name'];
        $studentEmail = $userdata['email'];
        $confirmedSlot = $data['confirmed_slot'];

        $ccMailList = '';
        $mailAttachments = '';

        $mailSubject = "One To One Request For $universityName Successful";
        $mailTemplate = "Hello there!

                            The following slot has been finalized for the chat session between $studentName and $universityName.

                            $confirmedSlot

                            You will receive a link to join the conference 15 minutes before the session begins.

                            Please check your inbox at the scheduled time to enter the meeting.

                            Please do the requisite research on the university and department of your choice. Links are provided below.

                            $universityWebsite

                            For a judicious use of time, please keep the scope of the conversation limited to “studying at the university.”

                            Good luck.

                            <b>Thanks and Regards,
                            HelloUni Coordinator
                            +91 81049 09690</b>

                            <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

        $sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

        $mailSubject = "One To One Request For $studentName Successful";
        $mailTemplate = "Hello there!

                            The following slot has been finalized by $studentName.

                            $confirmedSlot

                            You will receive a link to join the conference 15 minutes before the session begins.

                            Please check your inbox at the scheduled time to enter the meeting.

                            <b>Thanks and Regards,
                            HelloUni Coordinator
                            +91 81049 09690</b>

                            <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

        $sendMail = sMail($universityEmail, $universityName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

        if($this->input->post('form_type') && $this->input->post('form_type') == 'INTERNSHIP')
        {
            redirect('/internships/my');
        }
        redirect('university_info/'.$this->input->post('slug'));
    }

    function apply()
    {
        $this->load->model('user/user_model');
        $userdata=$this->session->userdata('user');
        if(!$userdata){
            redirect('/user/account');
        }

        $data['university_id'] = $_GET['uid'];
        $data['course_id'] = isset($_GET['course_id']) ? $_GET['course_id'] : NULL;
        $data['user_id'] = $_GET['user_id'] ? $_GET['user_id'] : $userdata['id'];

        $uni_details = $this->user_model->getUniversityDataByid($data['university_id'], '');
        $universityName = $uni_details[0]['name'];
        $universityEmail = $uni_details[0]['email'];

        $appliedUniversity = $this->user_model->checkUniversityApplied($data['user_id'], $data['university_id']);
        if(!$appliedUniversity)
        {
            $this->user_model->applyUniversity($data);
            $updateStudent['status'] = 'APPLICATION_PROCESS';
            $this->user_model->updateStudentDetails($updateStudent, $data['user_id']);

            $studentName = $userdata['name'];
            $studentEmail = $userdata['email'];
            //$universityName = $_GET['uname'];
            //$universityEmail = $_GET['uemail'];

            $ccMailList = '';
            $mailAttachments = '';

            $mailSubject = "$studentName Has Applied";
            $mailTemplate = "Hi $universityName,

                                We are happy to let you know that $studentName has finished the application. Request you to go through the same. Please feel free to get in touch with us should you need any more information.

                                <b>Thanks and Regards,
                                HelloUni Coordinator
                                +91 81049 09690</b>

                                <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

            $sendMail = sMail($universityEmail, $universityName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);
        }

        $this->outputData['university_name'] = $universityName;
        $this->outputData['university_email'] = $universityEmail;

        $this->render_page('templates/university/apply', $this->outputData);
    }

    function application()
    {
        $userdata=$this->session->userdata('user');
        if(!$userdata){
            redirect('/user/account');
        }

        $this->load->model(array('user/user_model'));

        $universityId = $_GET['uid'];
        $userId = $userdata['id'];

        $data['university_id'] = $universityId;
        $data['user_id'] = $userId;
        $applicationId = $this->user_model->applyUniversity($data);

        redirect('user/account/update?submit=Check+Application&uid=' . $universityId . '&tab=application_form&aid=' . $applicationId . '&status=In Process');
        /*$appliedUniversityDetail = $this->user_model->checkUniversityApplied($userId, $universityId);
        $appliedUniversityDetail[0]['sop'] = $appliedUniversityDetail[0]['sop'] ? json_decode($appliedUniversityDetail[0]['sop'], true) : $appliedUniversityDetail[0]['sop'];
        $appliedUniversityDetail[0]['lor'] = $appliedUniversityDetail[0]['lor'] ? json_decode($appliedUniversityDetail[0]['lor'], true) : $appliedUniversityDetail[0]['lor'];
        $appliedUniversityDetail[0]['transcripts'] = $appliedUniversityDetail[0]['transcripts'] ? json_decode($appliedUniversityDetail[0]['transcripts'], true) : $appliedUniversityDetail[0]['transcripts'];
        $appliedUniversityDetail[0]['application_fee'] = $appliedUniversityDetail[0]['application_fee'] ? json_decode($appliedUniversityDetail[0]['application_fee'], true) : $appliedUniversityDetail[0]['application_fee'];
        $this->outputData['applied_university_detail'] = $appliedUniversityDetail[0];

        $uni_detail = $this->user_model->getUniversityDataByid($universityId, '');
        $universityName = $uni_detail[0]['name'];
        $universityLoginId = $uni_detail[0]['user_id'];
        $colleges = array();
        foreach($uni_detail as $detail)
        {
            if(!in_array($detail['college'], $colleges))
            {
                $colleges[$detail['college_id']] = $detail['college'];
            }
        }
        $this->outputData['sop_no_of_words'] = $appliedUniversityDetail[0]['sop'] && isset($appliedUniversityDetail[0]['sop']['sop_needed']) && $appliedUniversityDetail[0]['sop']['sop_needed']? 'block' : 'none';
        $this->outputData['lor_required'] = $appliedUniversityDetail[0]['lor'] && isset($appliedUniversityDetail[0]['lor']['lor_needed']) && $appliedUniversityDetail[0]['lor']['lor_needed'] ? 'block' : 'none';
        $this->outputData['lor_type'] = $appliedUniversityDetail[0]['lor'] && isset($appliedUniversityDetail[0]['lor']['lor_needed']) && $appliedUniversityDetail[0]['lor']['lor_needed'] ? 'block' : 'none';

        $this->outputData['university_id'] = $universityId;
        $this->outputData['university_login_id'] = $universityLoginId;
        $this->outputData['university_name'] = $universityName;
        $this->outputData['colleges'] = $colleges;
        $this->render_page('templates/university/application_form', $this->outputData);*/
    }

    function application_submit()
    {
        $userdata=$this->session->userdata('user');
        if(!$userdata){
            redirect('/user/account');
        }

        $this->load->model(array('user/user_model'));

        $postVariables = $this->input->post();
        $universityId = $postVariables['university_id'];
        $applicationId = $postVariables['application_id'];
        //$userId = $userdata['id'];
        if($userdata['type'] == 5){
          $userId = $userdata['id'];
        } else {
          $userId = $this->input->post('user_id');
        }
        $sop = array();
        $lor = array();
        $transcripts = array();

        if(isset($postVariables['sop_needed'])){
            $sop = array(
                'sop_needed'        => $postVariables['sop_needed'],
                'sop_no_of_words'   => $postVariables['sop_no_of_words']
            );
        }

        if(isset($postVariables['lor_needed'])){
            $lor = array(
                'lor_needed'    => $postVariables['lor_needed'],
                'lor_required'  => $postVariables['lor_required'],
                'lor_type'      => isset($postVariables['lor_type']) ? $postVariables['lor_type'] : ''
            );
        }

        if(isset($postVariables['transcript'])){
            $transcripts = array(
                'transcript'            => $postVariables['transcript'],
                'transcript_submitted'  => isset($postVariables['transcript_submitted']) ? $postVariables['transcript_submitted'] : ''
            );
        }

        $application_fee = array(
            'application_fee'       => $postVariables['application_fee'],
            'application_fee_paid'  => isset($postVariables['application_fee_paid']) ? $postVariables['application_fee_paid'] : ''
        );

        $priority = 1;
        switch($postVariables['status'])
        {
            case 'APPLICATION_PROCESS':
                $priority = 1;
                break;
            case 'APPLICATION_SUBMITTED':
                $priority = 2;
                break;
            case 'ADMIT_RECEIVED_CONDITIONAL':
                $priority = 3;
                break;
            case 'ADMIT_RECEIVED_UNCONDITIONAL':
                $priority = 4;
                break;
        }

        $updatedData = array(
            'login_url'         => $postVariables['login_url'],
            'username'          => $postVariables['username'],
            'password'          => $postVariables['password'],
            'dead_line'         => $postVariables['application_deadline'] ? $postVariables['application_deadline'] : NULL,
            'gre_code'          => $postVariables['gre_code'],
            'toefl_code'        => $postVariables['toefl_code'],
            'sop'               => json_encode($sop),
            'lor'               => json_encode($lor),
            'transcripts'       => json_encode($transcripts),
            'application_fee'   => json_encode($application_fee),
            'requested_url'     => $postVariables['request_url'],
            'date_updated'      => date('Y-m-d H:i:s'),
            'status'            => $postVariables['status'],
            'priority'          => $priority
        );

        if(isset($postVariables['college']) && $postVariables['college'])
        {
            $updatedData['college_id'] = $postVariables['college'];
        }
        if(isset($postVariables['course']) && $postVariables['course'])
        {
            $updatedData['course_id'] = $postVariables['course'];
        }

        $this->user_model->updateAppliedUniversityById($applicationId, $updatedData);
        if($userdata['type'] != 5){
          redirect('/student/profile/' . $userId);
        } else {
          redirect('user/account/update?submit=Check+Application&uid=' . $universityId . '&tab=application_form&aid=' . $applicationId . '&status=' . $postVariables['status']);
        }
    }

    function visa_submit()
    {
        $userdata=$this->session->userdata('user');
        if(!$userdata){
            redirect('/user/account');
        }

        $this->load->model(array('user/user_model'));

        $postVariables = $this->input->post();
        $applicationId = $postVariables['application_id'];
        $universityId = $postVariables['university_id'];
        //$userId = $userdata['id'];
        if($userdata['type'] == 5){
          $userId = $userdata['id'];
        } else {
          $userId = $this->input->post('user_id');
        }

        $mockDates = array(
            'mock_date_1'  => $postVariables['visa_mock_date_1'],
            'mock_slot_1'  => $postVariables['visa_mock_slot_1'],
            'mock_date_2'  => $postVariables['visa_mock_date_2'],
            'mock_slot_2'  => $postVariables['visa_mock_slot_2'],
            'mock_date_3'  => $postVariables['visa_mock_date_3'],
            'mock_slot_3'  => $postVariables['visa_mock_slot_3']
        );

        $data = array(
            'application_id'    => $applicationId,
            'ofc_date'          => $postVariables['visa_ofc_date'],
            'interview_date'    => $postVariables['visa_interview_date'],
            'ds_160'            => $postVariables['visa_ds_160'],
            'cgi_federal'       => $postVariables['visa_cgi_federal'],
            'sevis'             => $postVariables['visa_sevis'],
            'mock_dates'        => json_encode($mockDates),
            'date_updated'      => date('Y-m-d H:i:s'),
            'status'            => $postVariables['visa_status']
        );

        $checkVisa = $this->user_model->checkVisaByApplication($applicationId);
        if($checkVisa)
        {
            $this->user_model->updateVisaByApplication($applicationId, $data);
        }
        else
        {
            $this->user_model->addVisa($data);
        }
        $priority = 4;
        switch($postVariables['status'])
        {
            case 'VISA_PROCESS_FINANCIAL_DOCUMENTS':
                $priority = 5;
                break;
            case 'VISA_APPLIED':
                $priority = 6;
                break;
            case 'VISA_APPROVED':
                $priority = 7;
                break;
        }

        $updatedData = array(
            'status'    => $postVariables['visa_status'],
            'priority'  => $priority
        );

        $this->user_model->updateAppliedUniversityById($applicationId, $updatedData);

        if($userdata['type'] != 5){
          redirect('/student/profile/' . $userId);
        } else {
          redirect('user/account/update?submit=Check+Application&uid=' . $universityId . '&tab=visa_form&aid=' . $applicationId);
        }
    }

    function getSubCourses()
    {
        $this->load->model('course/course_model');
        if($this->input->post('id'))
        {
            $collegeCategoryId = $this->input->post('id');
            $subCourses = $this->course_model->getCourseCategoriesByCollegeCategory($collegeCategoryId);
            foreach($subCourses as $subCourse)
            {
                echo "<option value='" . $subCourse->id . "'>" . $subCourse->display_name . "</option>";
            }
		}
    }

    function uploadSignature()
    {
        $userdata=$this->session->userdata('user');
        if(!$userdata){
            redirect('/user/account');
        }

        $this->load->model(array('user/user_model'));

        if($_FILES["file"]["name"])
        {
            $path = $_FILES["file"]["name"];
            $temp = explode(".", $path);
            $type = end($temp);
            $imageData = file_get_contents($_FILES["file"]["tmp_name"]);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($imageData);

            $data['signature'] = $base64;
            $this->user_model->updateStudentDetails($data, $userdata['id']);
            echo 1;
            exit;
        }
        echo 0;
    }
}

?>
