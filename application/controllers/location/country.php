<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com


 */

class Country extends MY_Controller {



	//Global variable

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;



    /**

	 * Constructor

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
        track_user();
    }


	function index()
	{   //die('++++++++++++++++');
	     $this->load->helper('cookie_helper');
         $this->load->model('location/country_model');
		 //$this->load->model('location/city_model');
		// $userdata=$this->session->userdata('user');
	     //if(empty($userdata)){  redirect('common/login'); }

		$cond_country1 = array('country_master.status !=' => '5');

		$this->outputData['countries'] = $this->country_model->getCountries($cond_country1);
       /* echo '<pre>';
		print_r($this->outputData['countries']);
		echo '</pre>'; die('++++++');*/
		$this->render_page('templates/location/country');

}


	 function form()
	 {
	     $this->load->helper('cookie_helper');

		  $this->load->model('location/country_model');

		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }

		 if($this->uri->segment('4')){
		 $conditions = array(
	 				  'country_master.id' => $this->uri->segment('4')
	 					);
		 $country_info = $this->country_model->getCountryByid($conditions);

		 /*echo '<pre>';
		 print_r($country_info);
		 echo '</pre>';
		 exit();*/

		 $this->outputData['id'] 			= $country_info->id;
		 $this->outputData['name'] 			= $country_info->name;
		 $this->outputData['code'] 			= $country_info->code;
		 $this->outputData['status'] 		= $country_info->status;


		}
		$this->render_page('templates/location/country_form',$this->outputData);

     }



	 function create()
	 {

	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');

		$this->load->model('location/country_model');
		//$this->load->model('location/city_model');
		//$this->outputData['countries']	= $this->country_model->getCountries();
		//$this->outputData['cities']	= $this->city_model->getCities();

		//$this->form_validation->set_rules('country_name', 'Country_Name', 'required|trim|xss_clean');


		//if ($this->form_validation->run() == FALSE)
		//{
			// redirect('website-element/brand/form');
			 // $this->render_page('templates/location/country_form');
		//}
		//else
		//{
		   $data = array(
			'name' => $this->input->post('name'),
     		'code' => $this->input->post('code'),
     		'status' =>$this->input->post('status')
     		);

			//print_r($data); exit();

	        if($this->country_model->insertCountry($data)=='success'){

	        $this->session->set_flashdata('flash_message', "You have saved Country!");
	        redirect('location/country');
	    }

	  // }
	}


	 function edit()
	 {

	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('location/country_model');

			  //$logo_info = $this->common_model->getFileinfo($this->input->post('property_logo'));
			 $data = array(
			'id' => $this->uri->segment('4'),
     		'name' => $this->input->post('name'),
     		'code' => $this->input->post('code'),
     		'status' =>$this->input->post('status')
     		);


	         if($this->country_model->editCountry($data)=='success'){

	         $this->session->set_flashdata('flash_message', "You have modified Country!");
	         redirect('location/country');
	         }



	}


	function delete_country()
	 {

	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('location/country_model');


		if($this->country_model->deleteCountry($this->uri->segment('4'))=='success'){

	         $this->session->set_flashdata('flash_message', "You have deleted Country!");
	        redirect('location/country');



	    }
	}


	function delete_mult_country(){

	    $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('location/country_model');

		$array = $this->input->post('chk');
		foreach($array as $id):

		$this->country_model->deleteCountry($id);

		endforeach;
		$this->session->set_flashdata('flash_message', "Success: You have deleted Country!");
		redirect('location/country/index');


	}







}//End  Home Class





?>
