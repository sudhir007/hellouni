<?php
require APPPATH . "vendor/autoload.php";

use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;

class Virtualfair extends MY_Controller
{
    private $_tokboxId = '46220952';
    private $_tokboxSecret = '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2';

    function __construct()
    {
        parent::__construct();
        $this->load->model('tokbox_model');
        $this->load->model('virtualfair_model');
        $this->load->model('redi_model');
    }

    function index()
    {
        echo "In webinar index";
    }

    function organizer()
    {
        $tokenId = $this->input->get('id');
        if(!$tokenId)
        {
            redirect('/');
        }

        $this->load->model('tokbox_model');
        $this->load->model('webinar/common_model');

        $tokenId = base64_decode($tokenId);
        $tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
        if(!$tokboxDetail)
        {
            redirect('/');
        }

        $whereCondition = array("tokbox_token_id" => $tokenId);
        $webinarData = $this->common_model->get($whereCondition, 'webinar_master');
        if(!$webinarData)
        {
            redirect('/');
        }

        $usrdata = $this->session->userdata('user') ? $this->session->userdata('user') : $this->session->userdata('usrdata');
        if(!$usrdata)
        {
            redirect('/meeting/webinar/oform?id=' . base64_encode($tokenId) . '&utype=organizer');
        }

        $username = $usrdata['username'];
        $userId = $usrdata['id'];
        $userType = $usrdata['typename'] == 'Guest' ? 'GUEST' : 'USER';

        $connectionMetaData = $username;
        $sessionId = $tokboxDetail->session_id;

        $polling_questions = $this->tokbox_model->getAllActivePolls();

        if($polling_questions)
        {
            foreach($polling_questions as $index => $poll_question)
            {
                $polling_questions[$index]['question_string'] = $poll_question['question'];
                $choices = [];
                $options = json_decode($poll_question['options'], true);
                foreach($options as $option)
                {
                    if($option == $poll_question['answer'])
                    {
                        $choices['correct'] = $option;
                    }
                    else
                    {
                        $choices['wrong'][] = $option;
                    }
                }
                $polling_questions[$index]['choices'] = $choices;
            }
        }

        $sentMessages = $this->tokbox_model->getChatFromUserSession($userId, $userType, $sessionId);
        $receivedMessages = $this->tokbox_model->getChatToUserSession($userId, $userType, $sessionId);

        $sentUsers = array();
        $sentGuests = array();
        $sentUserNames = array();
        $sentGuestNames = array();
        if($receivedMessages)
        {
            foreach($receivedMessages as $receivedMessage)
            {
                if($receivedMessage['user_type'] == 'USER' && !in_array($receivedMessage['from'], $sentUsers))
                {
                    $sentUsers[] = $receivedMessage['from'];
                }
                else if($receivedMessage['user_type'] == 'GUEST' && !in_array($receivedMessage['from'], $sentGuests))
                {
                    $sentGuests[] = $receivedMessage['from'];
                }
            }

            if($sentUsers)
            {
                $sentUserNames = $this->tokbox_model->getUserName($sentUsers);
            }
            if($sentGuests)
            {
                $sentGuestNames = $this->tokbox_model->getGuestName($sentGuests);
            }
        }

        $allMessages = [];
        if($sentMessages)
        {
            foreach($sentMessages as $sentMessage)
            {
                $allMessages[$sentMessage['id']] = $sentMessage;
            }
        }
        if($receivedMessages)
        {
            foreach($receivedMessages as $receivedMessage)
            {
                $allMessages[$receivedMessage['id']] = $receivedMessage;
            }
        }

        if($allMessages)
        {
            ksort($allMessages);
            foreach($allMessages as $index => $message)
            {
                if($message['chat_of'] == 'theirs')
                {
                    if(in_array($message['from'], $sentUsers))
                    {
                        foreach($sentUserNames as $sentUserName)
                        {
                            if($sentUserName['id'] == $message['from'])
                            {
                                $message['name'] = $sentUserName['name'];
                                $allMessages[$index] = $message;
                            }
                        }
                    }
                    else if(in_array($message['from'], $sentGuests))
                    {
                        foreach($sentGuestNames as $sentGuestName)
                        {
                            if($sentGuestName['id'] == $message['from'])
                            {
                                $message['name'] = $sentGuestName['name'];
                                $allMessages[$index] = $message;
                            }
                        }
                    }
                }
                else
                {
                    $message['name'] = $username;
                    $allMessages[$index] = $message;
                }
            }
        }

        $commonChat = array();
        $privateChat = array();
        if($allMessages)
        {
            foreach($allMessages as $chatMessage)
            {
                if($chatMessage['to'] == -1)
                {
                    $commonChat[] = $chatMessage;
                }
                else
                {
                    $privateChat[] = $chatMessage;
                }
            }
        }

        $running_poll = $this->tokbox_model->getRunningPollByToken($tokenId);

        $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'moderator',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['sessionId'] = $sessionId;
        $data['username'] = $username;
        $data['tokboxId'] = $tokenId;
        $data['polling'] = 1;
        $data['polling_questions'] = json_encode($polling_questions);
        $data['common_chat_history'] = $commonChat;
        $data['private_chat_history'] = $privateChat;
        $data['apiKey'] = $this->_tokboxId;
        $data['running_poll'] = $running_poll;

        $this->load->view('templates/meeting/organizer', $data);
    }

    function panelist()
    {
        $tokenId = $this->input->get('id');
        if(!$tokenId)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/admin";</script>';
            exit;
        }

        $tokenId = base64_decode($tokenId);
        $tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
        if(!$tokboxDetail)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/admin";</script>';
            exit;
        }

        $virtualFairDetail = $this->virtualfair_model->getByTokenId($tokenId);
        if(!$virtualFairDetail)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/admin";</script>';
            exit;
        }

        $participantId = $virtualFairDetail->id;
        /*$participantSlots = $this->virtualfair_model->getSlotsByParticipant($participantId);
        if(!$participantSlots)
        {
            echo '<script type="text/javascript">alert("You do not have any slot. Please contact to our help desk."); window.location.href="/admin";</script>';
            exit;
        }*/

        $usrdata = $this->session->userdata('user');
        if(!$usrdata)
        {
            echo '<script type="text/javascript">alert("Please login to join virtual fair."); window.location.href="/admin";</script>';
            exit;
        }

        if($usrdata['id'] != $virtualFairDetail->university_id)
        {
            echo '<script type="text/javascript">alert("You are not authorized.");</script>';
            exit;
        }

        /*foreach($participantSlots as $index => $participantSlot)
        {
            $participantSlot['students'] = [];
            $participantSlot['start_time_string'] = strtotime($participantSlot['start_time']);
            $participantSlot['end_time_string'] = strtotime($participantSlot['end_time']);
            $participantSlots[$index] = $participantSlot;
        }*/

        $key = "PARTICIPANT-" . $participantId;
        $ttl = 36000;
        $jsonKeyData = $this->redi_model->getKey($key);
        if($jsonKeyData)
        {
            $keyData = json_decode($jsonKeyData, true);
        }
        else
        {
            $keyData = [];
        }
        $json_data = json_encode($keyData);
        $this->redi_model->setKeyWithExpiry($key, $json_data, $ttl);

        $username = $virtualFairDetail->alias;
        $backgrounImage = $virtualFairDetail->background_image_url;
        $userId = $usrdata['id'];
        $userType = 'USER';

        $connectionMetaData = $username;
        $sessionId = $tokboxDetail->session_id;

        $sentMessages = $this->tokbox_model->getChatFromUserSession($userId, $userType, $sessionId);
        $receivedMessages = $this->tokbox_model->getChatToUserSession($userId, $userType, $sessionId);

        $sentUsers = array();
        $sentGuests = array();
        $sentUserNames = array();
        $sentGuestNames = array();
        if($receivedMessages)
        {
            foreach($receivedMessages as $receivedMessage)
            {
                if(!in_array($receivedMessage['from'], $sentUsers))
                {
                    $sentUsers[] = $receivedMessage['from'];
                }
            }

            if($sentUsers)
            {
                $sentUserNames = $this->tokbox_model->getUserName($sentUsers);
            }
        }

        $allMessages = [];
        if($sentMessages)
        {
            foreach($sentMessages as $sentMessage)
            {
                $allMessages[$sentMessage['id']] = $sentMessage;
            }
        }
        if($receivedMessages)
        {
            foreach($receivedMessages as $receivedMessage)
            {
                $allMessages[$receivedMessage['id']] = $receivedMessage;
            }
        }

        if($allMessages)
        {
            ksort($allMessages);
            foreach($allMessages as $index => $message)
            {
                if($message['chat_of'] == 'theirs')
                {
                    if(in_array($message['from'], $sentUsers))
                    {
                        foreach($sentUserNames as $sentUserName)
                        {
                            if($sentUserName['id'] == $message['from'])
                            {
                                $message['name'] = $sentUserName['name'];
                                $allMessages[$index] = $message;
                            }
                        }
                    }
                    else if(in_array($message['from'], $sentGuests))
                    {
                        foreach($sentGuestNames as $sentGuestName)
                        {
                            if($sentGuestName['id'] == $message['from'])
                            {
                                $message['name'] = $sentGuestName['name'];
                                $allMessages[$index] = $message;
                            }
                        }
                    }
                }
                else
                {
                    $message['name'] = $username;
                    $allMessages[$index] = $message;
                }
            }
        }

        $commonChat = array();
        $privateChat = array();
        if($allMessages)
        {
            foreach($allMessages as $chatMessage)
            {
                if($chatMessage['to'] == -1)
                {
                    $commonChat[] = $chatMessage;
                }
                else
                {
                    $privateChat[] = $chatMessage;
                }
            }
        }

        $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'moderator',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $brochures = $virtualFairDetail->brochures ? json_decode($virtualFairDetail->brochures, true) : [];

        $key = "PARTICIPANT-ATTENDED-" . $participantId;
        $ttl = 36000;
        $attended = $this->redi_model->getKey($key);
        if(!$attended)
        {
            $attended = 0;
        }
        $this->redi_model->setKeyWithExpiry($key, $attended, $ttl);

        $data['sessionId'] = $sessionId;
        $data['username'] = $username;
        $data['backgroundImage'] = $backgrounImage;
        $data['tokboxId'] = $tokenId;
        $data['common_chat_history'] = $commonChat;
        $data['private_chat_history'] = $privateChat;
        $data['apiKey'] = $this->_tokboxId;
        $data['participantId'] = $participantId;
        $data['maxStudents'] = $virtualFairDetail->limit_of_student;
        //$data['myStatus'] = $participantSlots[0]['status'];
        $data['backgroundImage'] = $virtualFairDetail->background_image_url;
        $data['brochures'] = $brochures;

        $this->load->view('templates/meeting/panelist_virtualfair', $data);
    }

    function attendee()
    {
        $tokenId = $this->input->get('id');
        if(!$tokenId)
        {
            redirect('/');
        }

        $tokenId = base64_decode($tokenId);
        $tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
        if(!$tokboxDetail)
        {
            redirect('/');
        }

        $virtualFairDetail = $this->virtualfair_model->getByTokenId($tokenId);
        if(!$virtualFairDetail)
        {
            redirect('/');
        }

        $usrdata = $this->session->userdata('user');
        if(!$usrdata)
        {
            echo '<script type="text/javascript">alert("Please login to join virtual fair."); window.location.href="/";</script>';
            exit;
        }
        $currentTime = date('Y-m-d H:i:s');
        $participantId = $virtualFairDetail->id;
        $userId = $usrdata['id'];
        $virtualFairId = 1;

        $isRegistered = $this->virtualfair_model->checkRegistration($userId, $virtualFairId, $participantId);
        if(!$isRegistered)
        {
            $vfairRegister['user_id'] = $userId;
            $vfairRegister['virtual_fair_id'] = $virtualFairId;
            $vfairRegister['participant_id'] = $participantId;
            $this->virtualfair_model->registerVirtualFair($vfairRegister);
        }

        $brochures = $virtualFairDetail->brochures ? json_decode($virtualFairDetail->brochures, true) : [];
        $comingSlots = $this->virtualfair_model->getSlotByParticipantTime($participantId, $currentTime);
        if(!$comingSlots)
        {
            echo '<script type="text/javascript">alert("Currently there is no slot for this university. Please come after some time."); window.location.href="/university/virtualfair/list/1";</script>';
            exit;
        }
        $runningSlot = $comingSlots[0];

        $loadSession = true;
        $currentSlotBooking = $runningSlot->student_booked ? json_decode($runningSlot->student_booked, true) : [];
        $currentSlotAttending = $runningSlot->student_attending ? json_decode($runningSlot->student_attending, true) : [];
        $currentSlotAttended = $runningSlot->student_attended ? json_decode($runningSlot->student_attended, true) : [];
        $maxStudents = $runningSlot->max_allowed_students;

        /*if($currentSlotBooking)
        {
            if(!in_array($userId, $currentSlotBooking))
            {
                echo '<script type="text/javascript">alert("The room is full. Please book next available slot."); window.location.href="/university/virtualfair/list/1";</script>';
                exit;
                //TODO: Here comes logic of one minute
                if($currentSlotAttending && !in_array($userId, $currentSlotAttending) && count($currentSlotAttending) >= $maxStudents)
                {
                    echo '<script type="text/javascript">alert("The room is full. Please book next available slot."); window.location.href="/university/virtualfair/list/1";</script>';
                    exit;
                }
            }
            $loadSession = true;
        }
        else
        {
            if($currentSlotAttending && !in_array($userId, $currentSlotAttending) && count($currentSlotAttending) >= $maxStudents)
            {
                echo '<script type="text/javascript">alert("The room is full. Please book next available slot."); window.location.href="/university/virtualfair/list/1";</script>';
                exit;
            }
            $loadSession = true;
        }*/
        if($loadSession)
        {
            $username = $usrdata['name'];
            $userId = $usrdata['id'];
            $userType = 'USER';

            $key = "PARTICIPANT-" . $participantId;
            $jsonKeyData = $this->redi_model->getKey($key);

            $studentDetail = $this->virtualfair_model->getStudentDetailByUserId($userId);

            $currentTimeString = strtotime(date('Y-m-d H:i:s'));
            $keyData = json_decode($jsonKeyData, true);
            $students = [];
            foreach($keyData as $index => $data)
            {
                if($currentTimeString >= $data['start_time_string'] && $currentTimeString <= $data['end_time_string'])
                {
                    $cachedStudents = $data['students'] ? json_decode($data['students'], true) : [];
                    $found = false;
                    if($cachedStudents)
                    {
                        foreach($cachedStudents as $cachedStudent)
                        {
                            if($cachedStudent['id'] == $studentDetail->id)
                            {
                                $found = true;
                            }
                        }
                    }
                    if(!$found)
                    {
                        $studentDetail->potential = 0;
                        $studentDetail->name = $username;
                        $data['students'][] = $studentDetail;
                    }
                    $keyData[$index] = $data;
                    $ttl = 36000;
                    $json_data = json_encode($keyData);
                    $this->redi_model->setKeyWithExpiry($key, $json_data, $ttl);
                    break;
                }
            }

            $slotId = $runningSlot->id;

            $updateParticipant['total_attended'] = $virtualFairDetail->total_attended + 1;
            $this->virtualfair_model->updateParticipant($participantId, $updateParticipant);

            if(!$currentSlotAttending)
            {
                $updateSlotData['student_attending'] = json_encode([(int)$userId]);
                $this->virtualfair_model->updateSlot($slotId, $updateSlotData);
            }
            else if(!in_array($userId, $currentSlotAttending))
            {
                $updateSlotData['student_attending'][] = (int)$userId;
                $updateSlotData['student_attending'] = json_encode($updateSlotData['student_attending']);
                $this->virtualfair_model->updateSlot($slotId, $updateSlotData);
            }

            $connectionMetaData = $username;
            $sessionId = $tokboxDetail->session_id;

            $sentMessages = $this->tokbox_model->getChatFromUserSession($userId, $userType, $sessionId);
            $receivedMessages = $this->tokbox_model->getChatToUserSession($userId, $userType, $sessionId);

            $sentUsers = array();
            $sentGuests = array();
            $sentUserNames = array();
            $sentGuestNames = array();
            if($receivedMessages)
            {
                foreach($receivedMessages as $receivedMessage)
                {
                    if($receivedMessage['user_type'] == 'USER' && !in_array($receivedMessage['from'], $sentUsers))
                    {
                        $sentUsers[] = $receivedMessage['from'];
                    }
                    else if($receivedMessage['user_type'] == 'GUEST' && !in_array($receivedMessage['from'], $sentGuests))
                    {
                        $sentGuests[] = $receivedMessage['from'];
                    }
                }

                if($sentUsers)
                {
                    $sentUserNames = $this->tokbox_model->getUserName($sentUsers);
                }
                if($sentGuests)
                {
                    $sentGuestNames = $this->tokbox_model->getGuestName($sentGuests);
                }
            }

            $allMessages = [];
            if($sentMessages)
            {
                foreach($sentMessages as $sentMessage)
                {
                    $allMessages[$sentMessage['id']] = $sentMessage;
                }
            }
            if($receivedMessages)
            {
                foreach($receivedMessages as $receivedMessage)
                {
                    $allMessages[$receivedMessage['id']] = $receivedMessage;
                }
            }

            if($allMessages)
            {
                ksort($allMessages);
                foreach($allMessages as $index => $message)
                {
                    if($message['chat_of'] == 'theirs')
                    {
                        if(in_array($message['from'], $sentUsers))
                        {
                            foreach($sentUserNames as $sentUserName)
                            {
                                if($sentUserName['id'] == $message['from'])
                                {
                                    $message['name'] = $sentUserName['name'];
                                    $allMessages[$index] = $message;
                                }
                            }
                        }
                        else if(in_array($message['from'], $sentGuests))
                        {
                            foreach($sentGuestNames as $sentGuestName)
                            {
                                if($sentGuestName['id'] == $message['from'])
                                {
                                    $message['name'] = $sentGuestName['name'];
                                    $allMessages[$index] = $message;
                                }
                            }
                        }
                    }
                    else
                    {
                        $message['name'] = $username;
                        $allMessages[$index] = $message;
                    }
                }
            }

            $commonChat = array();
            $privateChat = array();
            if($allMessages)
            {
                foreach($allMessages as $chatMessage)
                {
                    if($chatMessage['to'] == -1)
                    {
                        $commonChat[] = $chatMessage;
                    }
                    else
                    {
                        $privateChat[] = $chatMessage;
                    }
                }
            }

            $key = "PARTICIPANT-ATTENDED-" . $participantId;
            $ttl = 36000;
            $attended = $this->redi_model->getKey($key);
            if(!$attended)
            {
                $attended = 0;
            }
            else
            {
                $attended++;
            }
            $this->redi_model->setKeyWithExpiry($key, $attended, $ttl);

            $data['sessionId'] = $sessionId;
            $data['username'] = $username;
            $data['tokboxId'] = $tokenId;
            $data['common_chat_history'] = $commonChat;
            $data['private_chat_history'] = $privateChat;
            $data['apiKey'] = $this->_tokboxId;
            $data['slot_id'] = $slotId;
            $data['brochures'] = $brochures;
            $data['participantId'] = $participantId;
            $data['backgroundImage'] = $virtualFairDetail->background_image_url;

            $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

            $bulkJoinTime = strtotime(date('2020-09-29 10:30:00'));
            //if($currentTimeString < $bulkJoinTime)
            //{
                $data['token'] = $opentok->generateToken($sessionId, array(
                    'role' => 'moderator',
                    'expireTime' => time()+(7 * 24 * 60 * 60),
                    'data' =>  $connectionMetaData
                ));

                $this->load->view('templates/meeting/attendee_virtualfair', $data);
            //}
            /*else
            {
                $data['token'] = $opentok->generateToken($sessionId, array(
                    'role' => 'subscriber',
                    'expireTime' => time()+(7 * 24 * 60 * 60),
                    'data' =>  $connectionMetaData
                ));

                $this->load->view('templates/meeting/student_virtualfair', $data);
            /}*/
        }
    }

    function check_time()
    {
        $slotId = $this->input->get('sid');

        $virtualFairSlot = $this->virtualfair_model->getSlotById($slotId);
        if(!$virtualFairSlot)
        {
            echo -1;
            exit;
        }

        $currentTime = date('Y-m-d H:i:s');
        $slotEndTime = $virtualFairSlot->end_time;

        $cTimeString = strtotime($currentTime);
        $sTimeString = strtotime($slotEndTime);

        $diff = $sTimeString - $cTimeString;
        if($diff <= 0)
        {
            echo -1;
            exit;
        }
        $time = new DateTime(date('i:s', $diff));
        echo $time;
    }

    function startSession()
    {
        $this->load->model('tokbox_model');
        $post_data = json_decode(file_get_contents("php://input"), true);
        $tokenId = $post_data['tid'];
        $updatedData['force_start'] = 1;
        $this->tokbox_model->editToken($updatedData, $tokenId);
        echo 1;
    }

    function pre_counselling()
    {
        $tokenId = $this->input->get('id');
        if(!$tokenId)
        {
            redirect('/');
        }

        $tokenId = base64_decode($tokenId);
        $tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
        if(!$tokboxDetail)
        {
            redirect('/');
        }

        $virtualFairDetail = $this->virtualfair_model->getByTokenId($tokenId);
        if(!$virtualFairDetail)
        {
            redirect('/');
        }

        $usrdata = $this->session->userdata('user');
        if(!$usrdata)
        {
            echo '<script type="text/javascript">alert("Please login to join virtual fair."); window.location.href="/admin";</script>';
            exit;
        }

        if($usrdata['id'] != $virtualFairDetail->university_id)
        {
            echo '<script type="text/javascript">alert("You are not authorized.");</script>';
            exit;
        }

        $username = $virtualFairDetail->alias;
        $participantId = $virtualFairDetail->id;
        $userId = $usrdata['id'];
        $userType = $usrdata['typename'] == 'Guest' ? 'GUEST' : 'USER';

        $connectionMetaData = $username;
        $sessionId = $tokboxDetail->session_id;

        $sentMessages = $this->tokbox_model->getChatFromUserSession($userId, $userType, $sessionId);
        $receivedMessages = $this->tokbox_model->getChatToUserSession($userId, $userType, $sessionId);

        $sentUsers = array();
        $sentGuests = array();
        $sentUserNames = array();
        $sentGuestNames = array();
        if($receivedMessages)
        {
            foreach($receivedMessages as $receivedMessage)
            {
                if($receivedMessage['user_type'] == 'USER' && !in_array($receivedMessage['from'], $sentUsers))
                {
                    $sentUsers[] = $receivedMessage['from'];
                }
                else if($receivedMessage['user_type'] == 'GUEST' && !in_array($receivedMessage['from'], $sentGuests))
                {
                    $sentGuests[] = $receivedMessage['from'];
                }
            }

            if($sentUsers)
            {
                $sentUserNames = $this->tokbox_model->getUserName($sentUsers);
            }
            if($sentGuests)
            {
                $sentGuestNames = $this->tokbox_model->getGuestName($sentGuests);
            }
        }

        $allMessages = [];
        if($sentMessages)
        {
            foreach($sentMessages as $sentMessage)
            {
                $allMessages[$sentMessage['id']] = $sentMessage;
            }
        }
        if($receivedMessages)
        {
            foreach($receivedMessages as $receivedMessage)
            {
                $allMessages[$receivedMessage['id']] = $receivedMessage;
            }
        }

        if($allMessages)
        {
            ksort($allMessages);
            foreach($allMessages as $index => $message)
            {
                if($message['chat_of'] == 'theirs')
                {
                    if(in_array($message['from'], $sentUsers))
                    {
                        foreach($sentUserNames as $sentUserName)
                        {
                            if($sentUserName['id'] == $message['from'])
                            {
                                $message['name'] = $sentUserName['name'];
                                $allMessages[$index] = $message;
                            }
                        }
                    }
                    else if(in_array($message['from'], $sentGuests))
                    {
                        foreach($sentGuestNames as $sentGuestName)
                        {
                            if($sentGuestName['id'] == $message['from'])
                            {
                                $message['name'] = $sentGuestName['name'];
                                $allMessages[$index] = $message;
                            }
                        }
                    }
                }
                else
                {
                    $message['name'] = $username;
                    $allMessages[$index] = $message;
                }
            }
        }

        $commonChat = array();
        $privateChat = array();
        if($allMessages)
        {
            foreach($allMessages as $chatMessage)
            {
                if($chatMessage['to'] == -1)
                {
                    $commonChat[] = $chatMessage;
                }
                else
                {
                    $privateChat[] = $chatMessage;
                }
            }
        }

        $key = "PARTICIPANT-" . $virtualFairDetail->id;
        $ttl = 36000;
        $jsonKeyData = $this->redi_model->getKey($key);
        if($jsonKeyData)
        {
            $keyData = json_decode($jsonKeyData, true);
        }
        else
        {
            $keyData = [];
        }
        $json_data = json_encode($keyData);
        $this->redi_model->setKeyWithExpiry($key, $json_data, $ttl);

        $key = "PARTICIPANT-ATTENDED-" . $participantId;
        $ttl = 36000;
        $attended = $this->redi_model->getKey($key);
        if(!$attended)
        {
            $attended = 0;
        }
        $this->redi_model->setKeyWithExpiry($key, $attended, $ttl);

        $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'moderator',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['sessionId'] = $sessionId;
        $data['username'] = $username;
        $data['tokboxId'] = $tokenId;
        $data['common_chat_history'] = $commonChat;
        $data['private_chat_history'] = $privateChat;
        $data['apiKey'] = $this->_tokboxId;
        $data['participant_id'] = $virtualFairDetail->id;

        $this->load->view('templates/meeting/panelist_pre_counselor', $data);
    }

    function edu_counselling()
    {
        $tokenId = $this->input->get('id');
        if(!$tokenId)
        {
            redirect('/');
        }

        $tokenId = base64_decode($tokenId);
        $tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
        if(!$tokboxDetail)
        {
            redirect('/');
        }

        $virtualFairDetail = $this->virtualfair_model->getByTokenId($tokenId);
        if(!$virtualFairDetail)
        {
            redirect('/');
        }

        $usrdata = $this->session->userdata('user');
        if(!$usrdata)
        {
            echo '<script type="text/javascript">alert("Please login to join virtual fair."); window.location.href="/admin";</script>';
            exit;
        }

        if($usrdata['id'] != $virtualFairDetail->university_id)
        {
            echo '<script type="text/javascript">alert("You are not authorized.");</script>';
            exit;
        }

        $username = $virtualFairDetail->alias;
        $userId = $usrdata['id'];
        $userType = $usrdata['typename'] == 'Guest' ? 'GUEST' : 'USER';

        $connectionMetaData = $username;
        $sessionId = $tokboxDetail->session_id;

        $sentMessages = $this->tokbox_model->getChatFromUserSession($userId, $userType, $sessionId);
        $receivedMessages = $this->tokbox_model->getChatToUserSession($userId, $userType, $sessionId);

        $sentUsers = array();
        $sentGuests = array();
        $sentUserNames = array();
        $sentGuestNames = array();
        if($receivedMessages)
        {
            foreach($receivedMessages as $receivedMessage)
            {
                if($receivedMessage['user_type'] == 'USER' && !in_array($receivedMessage['from'], $sentUsers))
                {
                    $sentUsers[] = $receivedMessage['from'];
                }
                else if($receivedMessage['user_type'] == 'GUEST' && !in_array($receivedMessage['from'], $sentGuests))
                {
                    $sentGuests[] = $receivedMessage['from'];
                }
            }

            if($sentUsers)
            {
                $sentUserNames = $this->tokbox_model->getUserName($sentUsers);
            }
            if($sentGuests)
            {
                $sentGuestNames = $this->tokbox_model->getGuestName($sentGuests);
            }
        }

        $allMessages = [];
        if($sentMessages)
        {
            foreach($sentMessages as $sentMessage)
            {
                $allMessages[$sentMessage['id']] = $sentMessage;
            }
        }
        if($receivedMessages)
        {
            foreach($receivedMessages as $receivedMessage)
            {
                $allMessages[$receivedMessage['id']] = $receivedMessage;
            }
        }

        if($allMessages)
        {
            ksort($allMessages);
            foreach($allMessages as $index => $message)
            {
                if($message['chat_of'] == 'theirs')
                {
                    if(in_array($message['from'], $sentUsers))
                    {
                        foreach($sentUserNames as $sentUserName)
                        {
                            if($sentUserName['id'] == $message['from'])
                            {
                                $message['name'] = $sentUserName['name'];
                                $allMessages[$index] = $message;
                            }
                        }
                    }
                    else if(in_array($message['from'], $sentGuests))
                    {
                        foreach($sentGuestNames as $sentGuestName)
                        {
                            if($sentGuestName['id'] == $message['from'])
                            {
                                $message['name'] = $sentGuestName['name'];
                                $allMessages[$index] = $message;
                            }
                        }
                    }
                }
                else
                {
                    $message['name'] = $username;
                    $allMessages[$index] = $message;
                }
            }
        }

        $commonChat = array();
        $privateChat = array();
        if($allMessages)
        {
            foreach($allMessages as $chatMessage)
            {
                if($chatMessage['to'] == -1)
                {
                    $commonChat[] = $chatMessage;
                }
                else
                {
                    $privateChat[] = $chatMessage;
                }
            }
        }

        $key = "PARTICIPANT-" . $virtualFairDetail->id;
        $ttl = 36000;
        $jsonKeyData = $this->redi_model->getKey($key);
        if($jsonKeyData)
        {
            $keyData = json_decode($jsonKeyData, true);
        }
        else
        {
            $keyData = [];
        }
        $json_data = json_encode($keyData);
        $this->redi_model->setKeyWithExpiry($key, $json_data, $ttl);

        $participantId = $virtualFairDetail->id;
        $key = "PARTICIPANT-ATTENDED-" . $participantId;
        $ttl = 36000;
        $attended = $this->redi_model->getKey($key);
        if(!$attended)
        {
            $attended = 0;
        }
        $this->redi_model->setKeyWithExpiry($key, $attended, $ttl);

        $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'moderator',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['sessionId'] = $sessionId;
        $data['username'] = $username;
        $data['tokboxId'] = $tokenId;
        $data['common_chat_history'] = $commonChat;
        $data['private_chat_history'] = $privateChat;
        $data['apiKey'] = $this->_tokboxId;
        $data['participant_id'] = $virtualFairDetail->id;

        $this->load->view('templates/meeting/panelist_edu_counselor', $data);
    }

    function post_counselling()
    {
        $tokenId = $this->input->get('id');
        if(!$tokenId)
        {
            redirect('/');
        }

        $tokenId = base64_decode($tokenId);
        $tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
        if(!$tokboxDetail)
        {
            redirect('/');
        }

        $virtualFairDetail = $this->virtualfair_model->getByTokenId($tokenId);
        if(!$virtualFairDetail)
        {
            redirect('/');
        }

        $usrdata = $this->session->userdata('user');
        if(!$usrdata)
        {
            echo '<script type="text/javascript">alert("Please login to join virtual fair."); window.location.href="/admin";</script>';
            exit;
        }

        if($usrdata['id'] != $virtualFairDetail->university_id)
        {
            echo '<script type="text/javascript">alert("You are not authorized.");</script>';
            exit;
        }

        $username = $virtualFairDetail->alias;
        $userId = $usrdata['id'];
        $userType = $usrdata['typename'] == 'Guest' ? 'GUEST' : 'USER';

        $connectionMetaData = $username;
        $sessionId = $tokboxDetail->session_id;

        $sentMessages = $this->tokbox_model->getChatFromUserSession($userId, $userType, $sessionId);
        $receivedMessages = $this->tokbox_model->getChatToUserSession($userId, $userType, $sessionId);

        $sentUsers = array();
        $sentGuests = array();
        $sentUserNames = array();
        $sentGuestNames = array();
        if($receivedMessages)
        {
            foreach($receivedMessages as $receivedMessage)
            {
                if($receivedMessage['user_type'] == 'USER' && !in_array($receivedMessage['from'], $sentUsers))
                {
                    $sentUsers[] = $receivedMessage['from'];
                }
                else if($receivedMessage['user_type'] == 'GUEST' && !in_array($receivedMessage['from'], $sentGuests))
                {
                    $sentGuests[] = $receivedMessage['from'];
                }
            }

            if($sentUsers)
            {
                $sentUserNames = $this->tokbox_model->getUserName($sentUsers);
            }
            if($sentGuests)
            {
                $sentGuestNames = $this->tokbox_model->getGuestName($sentGuests);
            }
        }

        $allMessages = [];
        if($sentMessages)
        {
            foreach($sentMessages as $sentMessage)
            {
                $allMessages[$sentMessage['id']] = $sentMessage;
            }
        }
        if($receivedMessages)
        {
            foreach($receivedMessages as $receivedMessage)
            {
                $allMessages[$receivedMessage['id']] = $receivedMessage;
            }
        }

        if($allMessages)
        {
            ksort($allMessages);
            foreach($allMessages as $index => $message)
            {
                if($message['chat_of'] == 'theirs')
                {
                    if(in_array($message['from'], $sentUsers))
                    {
                        foreach($sentUserNames as $sentUserName)
                        {
                            if($sentUserName['id'] == $message['from'])
                            {
                                $message['name'] = $sentUserName['name'];
                                $allMessages[$index] = $message;
                            }
                        }
                    }
                    else if(in_array($message['from'], $sentGuests))
                    {
                        foreach($sentGuestNames as $sentGuestName)
                        {
                            if($sentGuestName['id'] == $message['from'])
                            {
                                $message['name'] = $sentGuestName['name'];
                                $allMessages[$index] = $message;
                            }
                        }
                    }
                }
                else
                {
                    $message['name'] = $username;
                    $allMessages[$index] = $message;
                }
            }
        }

        $commonChat = array();
        $privateChat = array();
        if($allMessages)
        {
            foreach($allMessages as $chatMessage)
            {
                if($chatMessage['to'] == -1)
                {
                    $commonChat[] = $chatMessage;
                }
                else
                {
                    $privateChat[] = $chatMessage;
                }
            }
        }

        $key = "PARTICIPANT-" . $virtualFairDetail->id;
        $ttl = 36000;
        $jsonKeyData = $this->redi_model->getKey($key);
        if($jsonKeyData)
        {
            $keyData = json_decode($jsonKeyData, true);
        }
        else
        {
            $keyData = [];
        }
        $json_data = json_encode($keyData);
        $this->redi_model->setKeyWithExpiry($key, $json_data, $ttl);

        $participantId = $virtualFairDetail->id;
        $key = "PARTICIPANT-ATTENDED-" . $participantId;
        $ttl = 36000;
        $attended = $this->redi_model->getKey($key);
        if(!$attended)
        {
            $attended = 0;
        }
        $this->redi_model->setKeyWithExpiry($key, $attended, $ttl);

        $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'moderator',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['sessionId'] = $sessionId;
        $data['username'] = $username;
        $data['tokboxId'] = $tokenId;
        $data['common_chat_history'] = $commonChat;
        $data['private_chat_history'] = $privateChat;
        $data['apiKey'] = $this->_tokboxId;
        $data['participant_id'] = $virtualFairDetail->id;

        $this->load->view('templates/meeting/panelist_post_counselor', $data);
    }

    function attendee_pre_counselling()
    {
        $tokenId = $this->input->get('id');
        if(!$tokenId)
        {
            redirect('/');
        }

        $tokenId = base64_decode($tokenId);
        $tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
        if(!$tokboxDetail)
        {
            redirect('/');
        }

        $virtualFairDetail = $this->virtualfair_model->getByTokenId($tokenId);
        if(!$virtualFairDetail)
        {
            redirect('/');
        }

        $usrdata = $this->session->userdata('user');
        if(!$usrdata)
        {
            echo '<script type="text/javascript">alert("Please login to join virtual fair."); window.location.href="/";</script>';
            exit;
        }

        if(!$virtualFairDetail->available)
        {
            echo '<script type="text/javascript">alert("Counselor is not available. Please wait");</script>';
            exit;
        }

        $username = $usrdata['name'];
        $userId = $usrdata['id'];
        $userType = 'USER';

        //$updateParticipant['available'] = 0;
        $updateParticipant['total_attended'] = $virtualFairDetail->total_attended + 1;
        //$updateParticipant['currently_handling'] = $virtualFairDetail->currently_handling + 1;
        $participantId = $virtualFairDetail->id;
        $this->virtualfair_model->updateParticipant($participantId, $updateParticipant);
        //$this->virtualfair_model->deleteCounselorBooking($userId);

        $connectionMetaData = (string)$userId;
        $sessionId = $tokboxDetail->session_id;

        $key = "PARTICIPANT-" . $participantId;
        $jsonKeyData = $this->redi_model->getKey($key);

        $studentDetail = $this->virtualfair_model->getStudentDetailByUserId($userId);
        $keyData = json_decode($jsonKeyData, true);
        $students = [];
        foreach($keyData as $index => $cachedStudent)
        {
            if($cachedStudent['id'] == $studentDetail->id)
            {
                //unset($keyData[$index]);
                continue;
            }
            $students[] = $cachedStudent;
        }
       	$studentDetail->name = $username;
       	$students[] = $studentDetail;
        $keyData = $students;
        $ttl = 36000;
        $json_data = json_encode($keyData);
        $this->redi_model->setKeyWithExpiry($key, $json_data, $ttl);

        $sentMessages = $this->tokbox_model->getChatFromUserSession($userId, $userType, $sessionId);
        $receivedMessages = $this->tokbox_model->getChatToUserSession($userId, $userType, $sessionId);

        $sentUsers = array();
        $sentGuests = array();
        $sentUserNames = array();
        $sentGuestNames = array();
        if($receivedMessages)
        {
            foreach($receivedMessages as $receivedMessage)
            {
                if($receivedMessage['user_type'] == 'USER' && !in_array($receivedMessage['from'], $sentUsers))
                {
                    $sentUsers[] = $receivedMessage['from'];
                }
                else if($receivedMessage['user_type'] == 'GUEST' && !in_array($receivedMessage['from'], $sentGuests))
                {
                    $sentGuests[] = $receivedMessage['from'];
                }
            }

            if($sentUsers)
            {
                $sentUserNames = $this->tokbox_model->getUserName($sentUsers);
            }
            if($sentGuests)
            {
                $sentGuestNames = $this->tokbox_model->getGuestName($sentGuests);
            }
        }

        $allMessages = [];
        if($sentMessages)
        {
            foreach($sentMessages as $sentMessage)
            {
                $allMessages[$sentMessage['id']] = $sentMessage;
            }
        }
        if($receivedMessages)
        {
            foreach($receivedMessages as $receivedMessage)
            {
                $allMessages[$receivedMessage['id']] = $receivedMessage;
            }
        }

        if($allMessages)
        {
            ksort($allMessages);
            foreach($allMessages as $index => $message)
            {
                if($message['chat_of'] == 'theirs')
                {
                    if(in_array($message['from'], $sentUsers))
                    {
                        foreach($sentUserNames as $sentUserName)
                        {
                            if($sentUserName['id'] == $message['from'])
                            {
                                $message['name'] = $sentUserName['name'];
                                $allMessages[$index] = $message;
                            }
                        }
                    }
                    else if(in_array($message['from'], $sentGuests))
                    {
                        foreach($sentGuestNames as $sentGuestName)
                        {
                            if($sentGuestName['id'] == $message['from'])
                            {
                                $message['name'] = $sentGuestName['name'];
                                $allMessages[$index] = $message;
                            }
                        }
                    }
                }
                else
                {
                    $message['name'] = $username;
                    $allMessages[$index] = $message;
                }
            }
        }

        $commonChat = array();
        $privateChat = array();
        if($allMessages)
        {
            foreach($allMessages as $chatMessage)
            {
                if($chatMessage['to'] == -1)
                {
                    $commonChat[] = $chatMessage;
                }
                else
                {
                    $privateChat[] = $chatMessage;
                }
            }
        }

        $key = "PARTICIPANT-ATTENDED-" . $participantId;
        $ttl = 36000;
        $attended = $this->redi_model->getKey($key);
        if(!$attended)
        {
            $attended = 0;
        }
        else
        {
            $attended++;
        }
        $this->redi_model->setKeyWithExpiry($key, $attended, $ttl);

        $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);
        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'moderator',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));
        $data['sessionId'] = $sessionId;
        $data['username'] = $username;
        $data['tokboxId'] = $tokenId;
        $data['common_chat_history'] = $commonChat;
        $data['private_chat_history'] = $privateChat;
        $data['apiKey'] = $this->_tokboxId;
        $data['participant_id'] = $participantId;

        $this->load->view('templates/meeting/attendee_pre_counselor', $data);
    }

    function attendee_edu_counselling()
    {
        $tokenId = $this->input->get('id');
        if(!$tokenId)
        {
            redirect('/');
        }

        $tokenId = base64_decode($tokenId);
        $tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
        if(!$tokboxDetail)
        {
            redirect('/');
        }

        $virtualFairDetail = $this->virtualfair_model->getByTokenId($tokenId);
        if(!$virtualFairDetail)
        {
            redirect('/');
        }

        $usrdata = $this->session->userdata('user');
        if(!$usrdata)
        {
            echo '<script type="text/javascript">alert("Please login to join virtual fair."); window.location.href="/";</script>';
            exit;
        }

        if(!$virtualFairDetail->available)
        {
            echo '<script type="text/javascript">alert("Counselor is not available. Please wait");</script>';
            exit;
        }

        $username = $usrdata['name'];
        $userId = $usrdata['id'];
        $userType = 'USER';

        //$updateParticipant['available'] = 0;
        $updateParticipant['total_attended'] = $virtualFairDetail->total_attended + 1;
        //$updateParticipant['currently_handling'] = $virtualFairDetail->currently_handling + 1;
        $participantId = $virtualFairDetail->id;
        $this->virtualfair_model->updateParticipant($participantId, $updateParticipant);

        //$this->virtualfair_model->deleteCounselorBooking($userId);

        $connectionMetaData = (string)$userId;
        $sessionId = $tokboxDetail->session_id;

        $key = "PARTICIPANT-" . $participantId;
        $jsonKeyData = $this->redi_model->getKey($key);

        $studentDetail = $this->virtualfair_model->getStudentDetailByUserId($userId);
        $keyData = json_decode($jsonKeyData, true);
        $students = [];
        foreach($keyData as $index => $cachedStudent)
        {
            if($cachedStudent['id'] == $studentDetail->id)
            {
		//unset($keyData[$index]);
		continue;
            }
	    $students[] = $cachedStudent;
        }
       	$studentDetail->name = $username;
       	$students[] = $studentDetail;
	$keyData = $students;
        $ttl = 36000;
        $json_data = json_encode($keyData);
        $this->redi_model->setKeyWithExpiry($key, $json_data, $ttl);

        $sentMessages = $this->tokbox_model->getChatFromUserSession($userId, $userType, $sessionId);
        $receivedMessages = $this->tokbox_model->getChatToUserSession($userId, $userType, $sessionId);

        $sentUsers = array();
        $sentGuests = array();
        $sentUserNames = array();
        $sentGuestNames = array();
        if($receivedMessages)
        {
            foreach($receivedMessages as $receivedMessage)
            {
                if($receivedMessage['user_type'] == 'USER' && !in_array($receivedMessage['from'], $sentUsers))
                {
                    $sentUsers[] = $receivedMessage['from'];
                }
                else if($receivedMessage['user_type'] == 'GUEST' && !in_array($receivedMessage['from'], $sentGuests))
                {
                    $sentGuests[] = $receivedMessage['from'];
                }
            }

            if($sentUsers)
            {
                $sentUserNames = $this->tokbox_model->getUserName($sentUsers);
            }
            if($sentGuests)
            {
                $sentGuestNames = $this->tokbox_model->getGuestName($sentGuests);
            }
        }

        $allMessages = [];
        if($sentMessages)
        {
            foreach($sentMessages as $sentMessage)
            {
                $allMessages[$sentMessage['id']] = $sentMessage;
            }
        }
        if($receivedMessages)
        {
            foreach($receivedMessages as $receivedMessage)
            {
                $allMessages[$receivedMessage['id']] = $receivedMessage;
            }
        }

        if($allMessages)
        {
            ksort($allMessages);
            foreach($allMessages as $index => $message)
            {
                if($message['chat_of'] == 'theirs')
                {
                    if(in_array($message['from'], $sentUsers))
                    {
                        foreach($sentUserNames as $sentUserName)
                        {
                            if($sentUserName['id'] == $message['from'])
                            {
                                $message['name'] = $sentUserName['name'];
                                $allMessages[$index] = $message;
                            }
                        }
                    }
                    else if(in_array($message['from'], $sentGuests))
                    {
                        foreach($sentGuestNames as $sentGuestName)
                        {
                            if($sentGuestName['id'] == $message['from'])
                            {
                                $message['name'] = $sentGuestName['name'];
                                $allMessages[$index] = $message;
                            }
                        }
                    }
                }
                else
                {
                    $message['name'] = $username;
                    $allMessages[$index] = $message;
                }
            }
        }

        $commonChat = array();
        $privateChat = array();
        if($allMessages)
        {
            foreach($allMessages as $chatMessage)
            {
                if($chatMessage['to'] == -1)
                {
                    $commonChat[] = $chatMessage;
                }
                else
                {
                    $privateChat[] = $chatMessage;
                }
            }
        }

        $key = "PARTICIPANT-ATTENDED-" . $participantId;
        $ttl = 36000;
        $attended = $this->redi_model->getKey($key);
        if(!$attended)
        {
            $attended = 0;
        }
        else
        {
            $attended++;
        }
        $this->redi_model->setKeyWithExpiry($key, $attended, $ttl);

        $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'moderator',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['sessionId'] = $sessionId;
        $data['username'] = $username;
        $data['tokboxId'] = $tokenId;
        $data['common_chat_history'] = $commonChat;
        $data['private_chat_history'] = $privateChat;
        $data['apiKey'] = $this->_tokboxId;
        $data['participant_id'] = $participantId;

        $this->load->view('templates/meeting/attendee_edu_counselor', $data);
    }

    function attendee_post_counselling()
    {
        $tokenId = $this->input->get('id');
        if(!$tokenId)
        {
            redirect('/');
        }

        $tokenId = base64_decode($tokenId);
        $tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
        if(!$tokboxDetail)
        {
            redirect('/');
        }

        $virtualFairDetail = $this->virtualfair_model->getByTokenId($tokenId);
        if(!$virtualFairDetail)
        {
            redirect('/');
        }

        $usrdata = $this->session->userdata('user');
        if(!$usrdata)
        {
            echo '<script type="text/javascript">alert("Please login to join virtual fair."); window.location.href="/";</script>';
            exit;
        }

        if(!$virtualFairDetail->available)
        {
            echo '<script type="text/javascript">alert("Counselor is not available. Please wait");</script>';
            exit;
        }

        $username = $usrdata['name'];
        $userId = $usrdata['id'];
        $userType = 'USER';

        //$updateParticipant['available'] = 0;
        $updateParticipant['total_attended'] = $virtualFairDetail->total_attended + 1;
        //$updateParticipant['currently_handling'] = $virtualFairDetail->currently_handling + 1;
        $participantId = $virtualFairDetail->id;
        $this->virtualfair_model->updateParticipant($participantId, $updateParticipant);

        //$this->virtualfair_model->deleteCounselorBooking($userId);

        $connectionMetaData = (string)$userId;
        $sessionId = $tokboxDetail->session_id;

        $key = "PARTICIPANT-" . $participantId;
        $jsonKeyData = $this->redi_model->getKey($key);

        $studentDetail = $this->virtualfair_model->getStudentDetailByUserId($userId);
        $keyData = json_decode($jsonKeyData, true);
        $students = [];
        foreach($keyData as $index => $cachedStudent)
        {
            if($cachedStudent['id'] == $studentDetail->id)
            {
		//unset($keyData[$index]);
		continue;
            }
	    $students[] = $cachedStudent;
        }
       	$studentDetail->name = $username;
       	$students[] = $studentDetail;
	$keyData = $students;
        $ttl = 36000;
        $json_data = json_encode($keyData);
        $this->redi_model->setKeyWithExpiry($key, $json_data, $ttl);

        $sentMessages = $this->tokbox_model->getChatFromUserSession($userId, $userType, $sessionId);
        $receivedMessages = $this->tokbox_model->getChatToUserSession($userId, $userType, $sessionId);

        $sentUsers = array();
        $sentGuests = array();
        $sentUserNames = array();
        $sentGuestNames = array();
        if($receivedMessages)
        {
            foreach($receivedMessages as $receivedMessage)
            {
                if($receivedMessage['user_type'] == 'USER' && !in_array($receivedMessage['from'], $sentUsers))
                {
                    $sentUsers[] = $receivedMessage['from'];
                }
                else if($receivedMessage['user_type'] == 'GUEST' && !in_array($receivedMessage['from'], $sentGuests))
                {
                    $sentGuests[] = $receivedMessage['from'];
                }
            }

            if($sentUsers)
            {
                $sentUserNames = $this->tokbox_model->getUserName($sentUsers);
            }
            if($sentGuests)
            {
                $sentGuestNames = $this->tokbox_model->getGuestName($sentGuests);
            }
        }

        $allMessages = [];
        if($sentMessages)
        {
            foreach($sentMessages as $sentMessage)
            {
                $allMessages[$sentMessage['id']] = $sentMessage;
            }
        }
        if($receivedMessages)
        {
            foreach($receivedMessages as $receivedMessage)
            {
                $allMessages[$receivedMessage['id']] = $receivedMessage;
            }
        }

        if($allMessages)
        {
            ksort($allMessages);
            foreach($allMessages as $index => $message)
            {
                if($message['chat_of'] == 'theirs')
                {
                    if(in_array($message['from'], $sentUsers))
                    {
                        foreach($sentUserNames as $sentUserName)
                        {
                            if($sentUserName['id'] == $message['from'])
                            {
                                $message['name'] = $sentUserName['name'];
                                $allMessages[$index] = $message;
                            }
                        }
                    }
                    else if(in_array($message['from'], $sentGuests))
                    {
                        foreach($sentGuestNames as $sentGuestName)
                        {
                            if($sentGuestName['id'] == $message['from'])
                            {
                                $message['name'] = $sentGuestName['name'];
                                $allMessages[$index] = $message;
                            }
                        }
                    }
                }
                else
                {
                    $message['name'] = $username;
                    $allMessages[$index] = $message;
                }
            }
        }

        $commonChat = array();
        $privateChat = array();
        if($allMessages)
        {
            foreach($allMessages as $chatMessage)
            {
                if($chatMessage['to'] == -1)
                {
                    $commonChat[] = $chatMessage;
                }
                else
                {
                    $privateChat[] = $chatMessage;
                }
            }
        }

        $key = "PARTICIPANT-ATTENDED-" . $participantId;
        $ttl = 36000;
        $attended = $this->redi_model->getKey($key);
        if(!$attended)
        {
            $attended = 0;
        }
        else
        {
            $attended++;
        }
        $this->redi_model->setKeyWithExpiry($key, $attended, $ttl);

        $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'moderator',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['sessionId'] = $sessionId;
        $data['username'] = $username;
        $data['tokboxId'] = $tokenId;
        $data['common_chat_history'] = $commonChat;
        $data['private_chat_history'] = $privateChat;
        $data['apiKey'] = $this->_tokboxId;
        $data['participant_id'] = $participantId;

        $this->load->view('templates/meeting/attendee_post_counselor', $data);
    }

    function update_pre_counselor()
    {
        $post_data = json_decode(file_get_contents("php://input"), true);
        $participantId = $post_data['id'];
        $this->virtualfair_model->updateParticipant($participantId, $post_data);
    }

    function student_detail()
    {
        $participantId = $this->input->get('pid');
        $key = "PARTICIPANT-" . $participantId;
        $jsonKeyData = $this->redi_model->getKey($key);

        $currentTimeString = strtotime(date('Y-m-d H:i:s'));
        $keyData = json_decode($jsonKeyData, true);
        $students = [];
        foreach($keyData as $data)
        {
            if($currentTimeString >= $data['start_time_string'] && $currentTimeString <= $data['end_time_string'])
            {
                $students = $data['students'];
                break;
            }
        }

        echo json_encode($students);
    }

    function pre_student_detail()
    {
        $participantId = $this->input->get('pid');
        $key = "PARTICIPANT-" . $participantId;
        $jsonKeyData = $this->redi_model->getKey($key);

        $keyData = json_decode($jsonKeyData, true);

        echo json_encode($keyData);
    }

    function download_brochure()
    {
        $participantId = $this->input->get('pid');
        $brochureId = $this->input->get('bid');

        $participantDetail = $this->virtualfair_model->getParticipantById($participantId);
        $brochures = $participantDetail->brochures ? json_decode($participantDetail->brochures, true) : [];
        if($brochures)
        {
            foreach($brochures as $brochure)
            {
                if($brochure['id'] == $brochureId)
                {
                    $filename = $brochure['file'];
                    $downloadFileName = $brochure['title'];

                    $filepath = APPPATH . '..' . $filename;

			header("Content-type: application/zip");
            		header('Content-Disposition: attachment; filename="' . $downloadFileName . '".zip;');
            		header("Pragma: no-cache");
            		header("Expires: 0");
            		readfile($filepath);
                    exit;
                }
            }
        }
    }

    function make_potential()
    {
        $participantId = $this->input->get('pid');
        $userId = $this->input->get('uid');
        $potential = $this->input->get('p');

        $updatedData['potential'] = $potential;
        $this->virtualfair_model->updateVirtualFairRegister($userId, $participantId, $updatedData);

        $key = "PARTICIPANT-" . $participantId;
        $jsonKeyData = $this->redi_model->getKey($key);

        $currentTimeString = strtotime(date('Y-m-d H:i:s'));
        $keyData = json_decode($jsonKeyData, true);
        $students = [];
        foreach($keyData as $index => $data)
        {
            if($currentTimeString >= $data['start_time_string'] && $currentTimeString <= $data['end_time_string'])
            {
                $cachedStudents = $data['students'] ? $data['students'] : [];
                if($cachedStudents)
                {
                    foreach($cachedStudents as $cachedKey => $cachedStudent)
                    {
                        if($cachedStudent['user_id'] == $userId)
                        {
                            $cachedStudent['potential'] = $potential;
                        }
                        $cachedStudents[$cachedKey] = $cachedStudent;
                    }
                }
                $data['students'] = $cachedStudents;
            }

            $keyData[$index] = $data;
            $ttl = 36000;
            $json_data = json_encode($keyData);
            $this->redi_model->setKeyWithExpiry($key, $json_data, $ttl);
        }
    }

    /*function attendee_join_precounselling()
    {
        $post_data = json_decode(file_get_contents("php://input"), true);
        if($post_data)
        {
            $usrdata = $this->session->userdata('user');
            $userId =
            $connectionId = $post_data['cid'];
        }
    }*/

    function delete_counselor_attendee()
    {
        $participantId = $this->input->get('pid');
        $userId = $this->input->get('uid');
        $participantDetail = $this->virtualfair_model->getParticipantById($participantId);
        $currentlyHandling = $participantDetail->currently_handling > 0 ? $participantDetail->currently_handling - 1 : 0;
        $updatedData['currently_handling'] = $currentlyHandling;
        $this->virtualfair_model->updateParticipant($participantId, $updatedData);

        $key = "PARTICIPANT-" . $participantId;
        $jsonKeyData = $this->redi_model->getKey($key);

        $keyData = json_decode($jsonKeyData, true);
        foreach($keyData as $index => $data)
        {
            if($data['user_id'] == $userId)
            {
                unset($keyData[$index]);
            }
        }
        $ttl = 36000;
        $json_data = json_encode($keyData);
        $this->redi_model->setKeyWithExpiry($key, $json_data, $ttl);

        echo json_encode($keyData);
    }

    function update_max_students_limit()
    {
        $participantId = $this->input->get('pid');
        $studentCount = $this->input->get('sc');
        $updatedData['limit_of_student'] = $studentCount;
        $this->virtualfair_model->updateParticipant($participantId, $updatedData);

        $updateSlotData['max_allowed_students'] = $studentCount;
        $this->virtualfair_model->updateSlotByParticipantId($participantId, $updateSlotData);
    }

    function update_my_status()
    {
        $participantId = $this->input->get('pid');
        $status = $this->input->get('s');

        $updateSlotData['status'] = $status;
        $this->virtualfair_model->updateSlotByParticipantId($participantId, $updateSlotData);
    }

    function waiting_students_pre_counselling()
    {
        $random = mt_rand(0, 50);
        echo $random;
    }

    function waiting_students_university()
    {
        $random = mt_rand(0, 50);
        echo $random;
    }

    function total_attended_university()
    {
        $random = mt_rand(0, 50);
        echo $random;
    }

    function help()
    {
        $userId = $this->input->get('id');
        $tokenId = $this->input->get('tid');
        $type = $this->input->get('type');

        $checkHelp = $this->virtualfair_model->checkHelp($userId, $tokenId, $type);
        if(!$checkHelp)
        {
            $data['user_id'] = $userId;
            $data['token_id'] = $tokenId;
            $data['type'] = $type;

            $this->virtualfair_model->addHelp($data);
        }
    }

   function queuecount(){

      $queuecountObj = $this->virtualfair_model->queuecountquery();

      $response['data'] = $queuecountObj;

      header('Content-Type: application/json');
      echo (json_encode($response));

    }

    function pre_counselor_waiting()
    {
        $waiting = $this->virtualfair_model->getPrecounselorWaiting();
        if($waiting)
        {
            echo $waiting->count;
            exit;
        }
        echo 0;
    }

    function post_counselor_waiting()
    {
        $waiting = $this->virtualfair_model->getPostcounselorWaiting();
        if($waiting)
        {
            echo $waiting->count;
            exit;
        }
        echo 0;
    }

    function edu_counselor_waiting()
    {
        $waiting = $this->virtualfair_model->getEducounselorWaiting();
        if($waiting)
        {
            echo $waiting->count;
            exit;
        }
        echo 0;
    }

    function total_registrations()
    {
        $totalRegistrationkey = "Total-Registration";
        $totalRegistrationCount = $this->redi_model->getKey($totalRegistrationkey);
        echo $totalRegistrationCount;
    }

    function total_attended_students()
    {
        $participantId = $this->input->get('pid');
        $key = "PARTICIPANT-ATTENDED-" . $participantId;
        $attended = $this->redi_model->getKey($key);
        echo $attended;
    }

    function eduFairAugHomePage()
    {
       $this->load->view('templates/htmlDesign/homePage');

    }
    function eduFairAugUniList()
    {
       $this->load->view('templates/htmlDesign/universityList');

    }
    function eduFairAugUniDetails()
    {
       $this->load->view('templates/htmlDesign/uniDetailsPage');

    }
    function eduFairSchedule()
    {
       $this->load->view('templates/htmlDesign/schedulePage');

    }
}
