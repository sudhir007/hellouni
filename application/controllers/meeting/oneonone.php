<?php
require APPPATH . "vendor/autoload.php";

use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;

class Oneonone extends MY_Controller
{
    private $_tokboxId = '46220952';
    private $_tokboxSecret = '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2';

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $tokenId = $this->input->get('id');
        if(!$tokenId)
        {
            redirect('/');
        }
        $usrdata = $this->session->userdata('user');
        if(empty($usrdata))
        {
            $data['error'] = "Please login first to join meeting";
        }
        else
        {
            $this->load->model('tokbox_model');
            $tokenId = base64_decode($tokenId);
            $tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
            if(!$tokboxDetail)
            {
                redirect('/');
            }

            $this->load->model('user/user_model');
            $participants = json_decode($tokboxDetail->participants, true);

            $slotId = $participants['slot_id'];
            $slotDetail = $this->user_model->getSlotById($slotId);
            $confirmed_slot = $slotDetail['confirmed_slot'];

            $meetingStartTime = strtotime($confirmed_slot);
            $currentTime = strtotime(date('Y-m-d H:i:s'));

            if($meetingStartTime > $currentTime)
            {
                $data['timer_error'] = "Please login first to join meeting";
                $data['countDownDate'] = $confirmed_slot;
            }
            else
            {
                $updatedData['status'] = 'Attained';
                $this->user_model->editSlot($slotId, $updatedData);

                $slotDetail = $this->user_model->getSlotById($slotId);
                $universityId = $slotDetail['university_id'];

                $brochures = $this->user_model->getBrochuresByUniversity($universityId);

                $sessionId = $tokboxDetail->session_id;
                $userId = $usrdata['id'];
                $username = $usrdata['username'];
                $connectionMetaData = $username;

                $opentok = new OpenTok('46220952', '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2');
                $data['token'] = $opentok->generateToken($sessionId, array(
                    'role' => 'publisher',
                    'expireTime' => time()+(7 * 24 * 60 * 60),
                    'data' =>  $connectionMetaData
                ));

                $polling_questions = $this->tokbox_model->getAllActivePolls();

                if($polling_questions)
                {
                    foreach($polling_questions as $index => $poll_question)
                    {
                        $polling_questions[$index]['question_string'] = $poll_question['question'];
                        $choices = [];
                        $options = json_decode($poll_question['options'], true);
                        foreach($options as $option)
                        {
                            if($option == $poll_question['answer'])
                            {
                                $choices['correct'] = $option;
                            }
                            else
                            {
                                $choices['wrong'][] = $option;
                            }
                        }
                        $polling_questions[$index]['choices'] = $choices;
                    }
                }

                $sentMessages = $this->tokbox_model->getChatFromUserSession($userId, 'USER', $sessionId);
                $receivedMessages = $this->tokbox_model->getChatToUserSession($userId, 'USER', $sessionId);

                $sentUsers = array();
                $sentUserNames = array();
                if($receivedMessages)
                {
                    foreach($receivedMessages as $receivedMessage)
                    {
                        if(!in_array($receivedMessage['from'], $sentUsers))
                        {
                            $sentUsers[] = $receivedMessage['from'];
                        }
                    }

                    if($sentUsers)
                    {
                        $sentUserNames = $this->tokbox_model->getUserName($sentUsers);
                    }
                }

                $allMessages = [];
                if($sentMessages)
                {
                    foreach($sentMessages as $sentMessage)
                    {
                        $allMessages[$sentMessage['id']] = $sentMessage;
                    }
                }
                if($receivedMessages)
                {
                    foreach($receivedMessages as $receivedMessage)
                    {
                        $allMessages[$receivedMessage['id']] = $receivedMessage;
                    }
                }

                if($allMessages)
                {
                    ksort($allMessages);
                    foreach($allMessages as $index => $message)
                    {
                        if($message['chat_of'] == 'theirs')
                        {
                            if(in_array($message['from'], $sentUsers))
                            {
                                foreach($sentUserNames as $sentUserName)
                                {
                                    if($sentUserName['id'] == $message['from'])
                                    {
                                        $message['name'] = $sentUserName['name'];
                                        $allMessages[$index] = $message;
                                    }
                                }
                            }
                            else if(in_array($message['from'], $sentGuests))
                            {
                                foreach($sentGuestNames as $sentGuestName)
                                {
                                    if($sentGuestName['id'] == $message['from'])
                                    {
                                        $message['name'] = $sentGuestName['name'];
                                        $allMessages[$index] = $message;
                                    }
                                }
                            }
                        }
                        else
                        {
                            $message['name'] = $username;
                            $allMessages[$index] = $message;
                        }
                    }
                }

                $commonChat = array();
                $privateChat = array();
                if($allMessages)
                {
                    foreach($allMessages as $chatMessage)
                    {
                        if($chatMessage['to'] == -1)
                        {
                            $commonChat[] = $chatMessage;
                        }
                        else
                        {
                            $privateChat[] = $chatMessage;
                        }
                    }
                }

                $meetingEndTime = $meetingStartTime + 60000;

                $data['timer'] = ($currentTime >= $meetingStartTime) ? ($meetingEndTime - $currentTime) * 1000 : 0;

                $data['sessionId'] = $sessionId;
                $data['username'] = $username;
                $data['tokboxId'] = $tokenId;
                $data['polling_questions'] = json_encode($polling_questions);
                $data['common_chat_history'] = $commonChat;
                $data['apiKey'] = $this->_tokboxId;
                $data['slotId'] = $slotId;
                $data['brochures'] = $brochures;
            }
        }
        $this->load->view('templates/meeting/student_oneonone', $data);
    }

    function organizer()
    {
        $tokenId = $this->input->get('id');
        if(!$tokenId)
        {
            redirect('/');
        }
        $usrdata = $this->session->userdata('user');
        if(empty($usrdata))
        {
            $data['error'] = "Please login first to join meeting";
        }
        else
        {
            $this->load->model('tokbox_model');
            $tokenId = base64_decode($tokenId);
            $tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
            if(!$tokboxDetail)
            {
                redirect('/');
            }

            $this->load->model('user/user_model');
            $participants = json_decode($tokboxDetail->participants, true);

            $sessionId = $tokboxDetail->session_id;
            $userId = $usrdata['id'];
            $username = $usrdata['username'];
            $connectionMetaData = $username;

            $opentok = new OpenTok('46220952', '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2');
            $data['token'] = $opentok->generateToken($sessionId, array(
                'role' => 'publisher',
                'expireTime' => time()+(7 * 24 * 60 * 60),
                'data' =>  $connectionMetaData
            ));

            $polling_questions = $this->tokbox_model->getAllActivePolls();

            if($polling_questions)
            {
                foreach($polling_questions as $index => $poll_question)
                {
                    $polling_questions[$index]['question_string'] = $poll_question['question'];
                    $choices = [];
                    $options = json_decode($poll_question['options'], true);
                    foreach($options as $option)
                    {
                        if($option == $poll_question['answer'])
                        {
                            $choices['correct'] = $option;
                        }
                        else
                        {
                            $choices['wrong'][] = $option;
                        }
                    }
                    $polling_questions[$index]['choices'] = $choices;
                }
            }

            $sentMessages = $this->tokbox_model->getChatFromUserSession($userId, 'USER', $sessionId);
            $receivedMessages = $this->tokbox_model->getChatToUserSession($userId, 'USER', $sessionId);

            $sentUsers = array();
            $sentUserNames = array();
            if($receivedMessages)
            {
                foreach($receivedMessages as $receivedMessage)
                {
                    if(!in_array($receivedMessage['from'], $sentUsers))
                    {
                        $sentUsers[] = $receivedMessage['from'];
                    }
                }

                if($sentUsers)
                {
                    $sentUserNames = $this->tokbox_model->getUserName($sentUsers);
                }
            }

            $allMessages = [];
            if($sentMessages)
            {
                foreach($sentMessages as $sentMessage)
                {
                    $allMessages[$sentMessage['id']] = $sentMessage;
                }
            }
            if($receivedMessages)
            {
                foreach($receivedMessages as $receivedMessage)
                {
                    $allMessages[$receivedMessage['id']] = $receivedMessage;
                }
            }

            if($allMessages)
            {
                ksort($allMessages);
                foreach($allMessages as $index => $message)
                {
                    if($message['chat_of'] == 'theirs')
                    {
                        if(in_array($message['from'], $sentUsers))
                        {
                            foreach($sentUserNames as $sentUserName)
                            {
                                if($sentUserName['id'] == $message['from'])
                                {
                                    $message['name'] = $sentUserName['name'];
                                    $allMessages[$index] = $message;
                                }
                            }
                        }
                        else if(in_array($message['from'], $sentGuests))
                        {
                            foreach($sentGuestNames as $sentGuestName)
                            {
                                if($sentGuestName['id'] == $message['from'])
                                {
                                    $message['name'] = $sentGuestName['name'];
                                    $allMessages[$index] = $message;
                                }
                            }
                        }
                    }
                    else
                    {
                        $message['name'] = $username;
                        $allMessages[$index] = $message;
                    }
                }
            }

            $commonChat = array();
            $privateChat = array();
            if($allMessages)
            {
                foreach($allMessages as $chatMessage)
                {
                    if($chatMessage['to'] == -1)
                    {
                        $commonChat[] = $chatMessage;
                    }
                    else
                    {
                        $privateChat[] = $chatMessage;
                    }
                }
            }

            $meetingStartTime = strtotime(date('2020-07-01 17:00:00'));
            $currentTime = strtotime(date('Y-m-d H:i:s'));
            $meetingEndTime = $meetingStartTime + 60;

            $data['timer'] = ($currentTime >= $meetingStartTime) ? ($meetingEndTime - $currentTime) * 1000 : 0;

            $data['sessionId'] = $sessionId;
            $data['username'] = $username;
            $data['tokboxId'] = $tokenId;
            $data['polling_questions'] = json_encode($polling_questions);
            $data['common_chat_history'] = $commonChat;
            $data['apiKey'] = $this->_tokboxId;
        }
        $this->load->view('templates/meeting/organizer_oneonone', $data);
    }

    function panelist()
    {
        $tokenId = $this->input->get('id');
        if(!$tokenId)
        {
            redirect('/');
        }
        $usrdata = $this->session->userdata('user');
        if(empty($usrdata))
        {
            $data['error'] = "Please login first to join meeting";
        }
        else
        {
            $this->load->model('tokbox_model');
            $tokenId = base64_decode($tokenId);
            $tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
            if(!$tokboxDetail)
            {
                redirect('/');
            }

            $this->load->model('user/user_model');
            $participants = json_decode($tokboxDetail->participants, true);

            $sessionId = $tokboxDetail->session_id;
            $userId = $usrdata['id'];
            $username = $usrdata['username'];
            $connectionMetaData = $username;

            $opentok = new OpenTok('46220952', '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2');
            $data['token'] = $opentok->generateToken($sessionId, array(
                'role' => 'publisher',
                'expireTime' => time()+(7 * 24 * 60 * 60),
                'data' =>  $connectionMetaData
            ));

            $polling_questions = $this->tokbox_model->getAllActivePolls();

            if($polling_questions)
            {
                foreach($polling_questions as $index => $poll_question)
                {
                    $polling_questions[$index]['question_string'] = $poll_question['question'];
                    $choices = [];
                    $options = json_decode($poll_question['options'], true);
                    foreach($options as $option)
                    {
                        if($option == $poll_question['answer'])
                        {
                            $choices['correct'] = $option;
                        }
                        else
                        {
                            $choices['wrong'][] = $option;
                        }
                    }
                    $polling_questions[$index]['choices'] = $choices;
                }
            }

            $sentMessages = $this->tokbox_model->getChatFromUserSession($userId, 'USER', $sessionId);
            $receivedMessages = $this->tokbox_model->getChatToUserSession($userId, 'USER', $sessionId);

            $sentUsers = array();
            $sentUserNames = array();
            if($receivedMessages)
            {
                foreach($receivedMessages as $receivedMessage)
                {
                    if(!in_array($receivedMessage['from'], $sentUsers))
                    {
                        $sentUsers[] = $receivedMessage['from'];
                    }
                }

                if($sentUsers)
                {
                    $sentUserNames = $this->tokbox_model->getUserName($sentUsers);
                }
            }

            $allMessages = [];
            if($sentMessages)
            {
                foreach($sentMessages as $sentMessage)
                {
                    $allMessages[$sentMessage['id']] = $sentMessage;
                }
            }
            if($receivedMessages)
            {
                foreach($receivedMessages as $receivedMessage)
                {
                    $allMessages[$receivedMessage['id']] = $receivedMessage;
                }
            }

            if($allMessages)
            {
                ksort($allMessages);
                foreach($allMessages as $index => $message)
                {
                    if($message['chat_of'] == 'theirs')
                    {
                        if(in_array($message['from'], $sentUsers))
                        {
                            foreach($sentUserNames as $sentUserName)
                            {
                                if($sentUserName['id'] == $message['from'])
                                {
                                    $message['name'] = $sentUserName['name'];
                                    $allMessages[$index] = $message;
                                }
                            }
                        }
                        else if(in_array($message['from'], $sentGuests))
                        {
                            foreach($sentGuestNames as $sentGuestName)
                            {
                                if($sentGuestName['id'] == $message['from'])
                                {
                                    $message['name'] = $sentGuestName['name'];
                                    $allMessages[$index] = $message;
                                }
                            }
                        }
                    }
                    else
                    {
                        $message['name'] = $username;
                        $allMessages[$index] = $message;
                    }
                }
            }

            $commonChat = array();
            $privateChat = array();
            if($allMessages)
            {
                foreach($allMessages as $chatMessage)
                {
                    if($chatMessage['to'] == -1)
                    {
                        $commonChat[] = $chatMessage;
                    }
                    else
                    {
                        $privateChat[] = $chatMessage;
                    }
                }
            }

            $meetingStartTime = strtotime(date('2020-07-01 17:00:00'));
            $currentTime = strtotime(date('Y-m-d H:i:s'));
            $meetingEndTime = $meetingStartTime + 60;

            $data['timer'] = ($currentTime >= $meetingStartTime) ? ($meetingEndTime - $currentTime) * 1000 : 0;

            $data['sessionId'] = $sessionId;
            $data['username'] = $username;
            $data['tokboxId'] = $tokenId;
            $data['polling_questions'] = json_encode($polling_questions);
            $data['common_chat_history'] = $commonChat;
            $data['apiKey'] = $this->_tokboxId;
        }
        $this->load->view('templates/meeting/panelist_oneonone', $data);
    }

    function connection()
    {
        $usrdata = $this->session->userdata('usrdata') ? $this->session->userdata('usrdata') : $this->session->userdata('user');
        $post_data = json_decode(file_get_contents("php://input"), true);

        $this->load->model('tokbox_model');

        $data['connection_id'] = $post_data['connectionId'];
        $data['session_id'] = $post_data['sessionId'];
        $data['tokbox_id'] = $post_data['tokboxId'];
        $data['user_type'] = ($usrdata['typename'] == 'Guest') ? 'GUEST' : 'USER';
        $data['user_id'] = $usrdata['id'];

        $this->tokbox_model->addConnection($data);
    }

    function chat()
    {
        $usrdata = $this->session->userdata('usrdata') ? $this->session->userdata('usrdata') : $this->session->userdata('user');
        $post_data = json_decode(file_get_contents("php://input"), true);

        $this->load->model('tokbox_model');
        $to = ($post_data['to'] == 'All') ? '-1' : $post_data['to'];
        $toUserType = NULL;
        if($to != -1)
        {
            $connectionDetail = $this->tokbox_model->checkConnection($to, $post_data['sessionId']);
            $to = $connectionDetail['user_id'];
            $toUserType = $connectionDetail['user_type'];
        }

        $data['from'] = $usrdata['id'];
        $data['to'] = $to;
        $data['to_user_type'] = $toUserType;
        $data['session_id'] = $post_data['sessionId'];
        $data['tokbox_id'] = $post_data['tokboxId'];
        $data['user_type'] = ($usrdata['typename'] == 'Guest') ? 'GUEST' : 'USER';
        $data['message'] = $post_data['msg'];
        $data['from_role'] = isset($post_data['from_role']) ? $post_data['from_role'] : NULL;

        $this->tokbox_model->storeChat($data);
    }

    function poll()
    {
        $this->load->model('tokbox_model');
        $post_values = json_decode(file_get_contents("php://input"), true);

        $usrdata = $this->session->userdata('usrdata') ? $this->session->userdata('usrdata') : $this->session->userdata('user');
        $data['user_id'] = $usrdata['id'];
        $data['user_type'] = ($usrdata['typename'] == 'Guest') ? 'GUEST' : 'USER';
        $data['question_id'] = $post_values['question_id'];
        $data['answer'] = $post_values['user_ans'];
        $data['session_id'] = $post_values['session_id'];
        $data['other_detail'] = json_encode($post_values);

        $this->tokbox_model->addPollResult($data);
    }

    function pollResult($sessionId)
    {
        $this->load->model('tokbox_model');
        $pollResults = $this->tokbox_model->getPollResult($sessionId);
        foreach($pollResults as $index => $pollResult)
        {
            if($pollResult['other_detail'])
            {
                $pollResult['other_detail'] = json_decode($pollResult['other_detail'], true);
            }
            $pollResults[$index] = $pollResult;
        }

        echo json_encode($pollResults);
    }

    public function startArchive()
	{
		$jsonBody = file_get_contents('php://input');
		$arrayBody = json_decode($jsonBody, true);
		$sessionId = $arrayBody['sessionId'];
        $tokboxId = $arrayBody['tokboxId'];

		$opentok = new OpenTok('46220952', '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2');
		// Create a simple archive of a session
		//$archive = $opentok->startArchive($sessionId);

		// Create an archive using custom options
		$archiveOptions = array(
    			'name' => 'Important Presentation',     // default: null
    			'hasAudio' => true,                     // default: true
    			'hasVideo' => true,                     // default: true
    			//'outputMode' => OutputMode::COMPOSED,   // default: OutputMode::COMPOSED
    			'resolution' => '1280x720'              // default: '640x480'
		);
		$archive = $opentok->startArchive($sessionId, $archiveOptions);

		// Store this archiveId in the database for later use
		$archiveId = $archive->id;

        $this->load->model('tokbox_model');
        $updatedData['archive_id'] = $archiveId;
        $this->tokbox_model->editToken($updatedData, $tokboxId);
	}

	public function stopArchive($archiveId)
	{
        $opentok = new OpenTok('46220952', '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2');
        $opentok->stopArchive($archiveId);
	}

    function download()
    {
        //$filename = $_SERVER['DOCUMENT_ROOT'] . '/uploads/logo.png';
		//$downloadFileName = "logo.png";

        $this->load->model('user/user_model');
        $brochureId = $this->input->get('t');
        $brochureDetail = $this->user_model->getBrochuresById($brochureId);
        $filename = $brochureDetail->brochure_file_name;
        $downloadFileName = $brochureDetail->brochure_title;

        $filepath = APPPATH . '../' . str_replace(base_url(), '', $filename);

		//header("Content-Type: text/csv; charset=UTF-8");
		header('Content-Disposition: attachment; filename="' . $downloadFileName . '";');
		header("Pragma: no-cache");
		header("Expires: 0");
		readfile($filepath);
    }

    public function studentFeedback()
    {
        $data['slotId'] = $this->input->get('sid');
        $this->load->view('templates/meeting/studentFeedback', $data);
    }

    function feedback()
    {
        $interested = $this->input->post('interested');
        $slotId = $this->input->post('slotId');
        if($interested == 'YES')
        {
            $this->load->model('user/user_model');
            $slotDetail = $this->user_model->getSlotById($slotId);
            if($slotDetail)
            {
                $universityId = $slotDetail['university_id'];
                redirect('/university/details/application?uid=' . $universityId);
            }
        }
        redirect('/test/thankyou');
    }
}
