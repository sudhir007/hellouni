<?php
require_once APPPATH . 'libraries/Mail/sMail.php';

require APPPATH . "vendor/autoload.php";
include_once APPPATH . 'mongo/autoload.php';

use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;

class Uta extends MY_Controller
{
    private $_tokboxId = '46220952';
    private $_tokboxSecret = '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2';

    private $_tokenSessionIds = [
        '188' => '2_MX40NjIyMDk1Mn5-MTYxMzI4NDcyMDQxOX50dnEzQWErajdhVDg5bndQMUdjd2R2Y0d-fg',
        '189' => '2_MX40NjIyMDk1Mn5-MTYxMzI4NDcyMDcwOX5wQlkrSGVXWlJNaEMxdTlCMWUxdXptcFN-fg',
        '190' => '2_MX40NjIyMDk1Mn5-MTYxMzI4NDcyMDk5NH5OMjNFYm5qZDJ3eUFKSXBxbzJYTmFTaEF-fg',
        '191' => '2_MX40NjIyMDk1Mn5-MTYxMzI4NDcyMTI4MX43S0thMGFLbUgwMi91Y1MxdzNJNW9QdHZ-fg',
        '192' => '1_MX40NjIyMDk1Mn5-MTYxMzI4NDcyMTU2OH5kTGJ0L3hqaVNJd1B1SE1jNFRSQTNxRGp-fg',
        '193' => '1_MX40NjIyMDk1Mn5-MTYxMzI4NDcyMTg1M35Xa0EwMUhZWHFmMEJKMEhMcGoveVlRVTh-fg',
        '194' => '2_MX40NjIyMDk1Mn5-MTYxMzI4NDcyMjE0MH5oSXFOZlZlK1VIWmV0SFBNK3JKQlVwK2N-fg',
        '195' => '1_MX40NjIyMDk1Mn5-MTYxMzI4NDcyMjQyN342Z2QxaHJzWi9OaFVmQkhPeXVQOVZ0V2t-fg',
        '196' => '1_MX40NjIyMDk1Mn5-MTYxMzI4NTQ0MzM0M35ZbGdITitvQjZZRUhsaUhYaUpwVVBjZ3Z-fg',
        '197' => '2_MX40NjIyMDk1Mn5-MTYxMzI4NTQ0MzYzNX4vSGxTbGloU2hsNGxzS3UrMHpYOXpxa1Z-fg',
        '198' => '1_MX40NjIyMDk1Mn5-MTYxMzI4NTQ0MzkxNn4xUjk3a1pycU1qQnFIYVF1aDZ2UlBqeHp-fg'
    ];

    private $_allRooms = [
        'computer_science' => [188, 190],
        'engineering_other' => 191,
        'information_system' => 192,
        'graduate_other' => 193,
        'civil_construction' => 194,
        'general_inqueries' => 195,
        'post_counselling' => [189, 196, 197, 198]
    ];

    private $_room_background_images = [
        '188' => 'https://www.hellouni.org/application/images/uta_seminar/computer_science.jpg',
        '189' => 'https://www.hellouni.org/application/images/uta_seminar/post_counselling.jpg',
        '190' => 'https://www.hellouni.org/application/images/uta_seminar/computer_science.jpg',
        '191' => 'https://www.hellouni.org/application/images/uta_seminar/engineering_other.jpg',
        '192' => 'https://www.hellouni.org/application/images/uta_seminar/information_system.jpg',
        '193' => 'https://www.hellouni.org/application/images/uta_seminar/graduate_other.jpg',
        '194' => 'https://www.hellouni.org/application/images/uta_seminar/civil_construction.jpg',
        '195' => 'https://www.hellouni.org/application/images/uta_seminar/general_inqueries.jpg',
        '196' => 'https://www.hellouni.org/application/images/uta_seminar/post_counselling.jpg',
        '197' => 'https://www.hellouni.org/application/images/uta_seminar/post_counselling.jpg',
        '198' => 'https://www.hellouni.org/application/images/uta_seminar/post_counselling.jpg'
    ];

    private $_superAdmins = [
        'deepika_admin',
        'banit_sawhney',
        'hiren_rathod',
        'harkiran_sawhney',
        'nik_admin',
        'tanmay_kudyadi',
        'sudhir_admin',
        'srivathsa_vaidya'
    ];

    private $_user_collection;
    private $_chat_collection;
    private $_ip_collection;
    private $_attender_collection;
    private $_connection_collection;
    private $_token_collection;
    private $_document_collection;
    private $_room_collection;
    private $_queue_collection;
    private $_tech_support_collection;
    private $_panelist_collection;
    private $_seminar_student_collection;

    function __construct()
    {
        /*echo '<script type="text/javascript">alert("The session has not been started yet. Please come back after some time."); window.location.href="/";</script>';
        exit;*/

        parent::__construct();
        $this->load->model(['uta_model']);

        $mongo_server = 'localhost';
        $mongo_port = '27017';
        $mongo_client = 'mongodb://' . $mongo_server . ':' . $mongo_port;
        $client = new MongoDB\Client($mongo_client);
        $mongo_db = $client->HelloUni;

        $this->_user_collection = $mongo_db->selectCollection('users');
        $this->_chat_collection = $mongo_db->selectCollection('chats');
        $this->_ip_collection = $mongo_db->selectCollection('users_ips');
        $this->_attender_collection = $mongo_db->selectCollection('attenders');
        $this->_connection_collection = $mongo_db->selectCollection('connections');
        $this->_temp_collection = $mongo_db->selectCollection('temporary');
        $this->_token_collection = $mongo_db->selectCollection('tokens');
        $this->_document_collection = $mongo_db->selectCollection('documents');
        $this->_room_collection = $mongo_db->selectCollection('rooms');
        $this->_queue_collection = $mongo_db->selectCollection('queue');
        $this->_tech_support_collection = $mongo_db->selectCollection('tech_support');
        $this->_panelist_collection = $mongo_db->selectCollection('panelists');
        $this->_seminar_students_collection = $mongo_db->selectCollection('seminar_students');
    }

    function organizer($tokenId, $username)
    {
        if(!$tokenId)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $data['base64_token_id'] = $tokenId;
        $data['base64_username'] = $username;

        $tokenId = base64_decode($tokenId);
        $username = base64_decode($username);

        if(!isset($this->_tokenSessionIds[$tokenId]))
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $this->_background_images['url'] = $this->_room_background_images[$tokenId];

        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/";</script>';
            exit;
        }

        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        $userId = $userDetail['id'];

        $connectionMetaData = $username;
        $sessionId = $this->_tokenSessionIds[$tokenId];

        $mongoChatQuery['session_id'] = $sessionId;
        $chatCount = $this->_chat_collection->count($mongoChatQuery);
        $chats = [];
        if($chatCount)
        {
            $chatCursor = $this->_chat_collection->find($mongoChatQuery);
            foreach($chatCursor as $chat)
            {
                $chat['chat_of'] = 'mine';
                $chat['name'] = $username;
                if($chat['from'] != $userId)
                {
                    $chat['chat_of'] = 'theirs';

                    $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                    $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                    $chat['name'] = $chatUserDetail['username'];
                }
                if($chat['name'])
                {
                    $chats[] = $chat;
                }
            }
        }

        $connectionType = 'ONE_ON_ONE';
        $this->uta_model->deleteConnection($userId, $connectionType);

        $insertConnectionData['user_id'] = $userId;
        $insertConnectionData['tokbox_id'] = $tokenId;
        $insertConnectionData['username'] = $username;
        $insertConnectionData['type'] = $connectionType;
        $this->uta_model->insertConnection($insertConnectionData);

        $row['user_id'] = $userId;
        $row['username'] = $username;
        $row['tokbox_id'] = $tokenId;
        $row['session_id'] = $sessionId;
        $row['type'] = $connectionType;
        $row['student_id'] = '';
        $insertOneResult = $this->_connection_collection->insertOne($row);

        $mongoTechQuery['tokbox_id'] = $tokenId;
        $this->_tech_support_collection->deleteMany($mongoTechQuery);

        $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'moderator',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['sessionId'] = $sessionId;
        $data['username'] = $username;
        $data['userId'] = $userId;
        $data['tokboxId'] = $tokenId;
        $data['apiKey'] = $this->_tokboxId;
        $data['common_chat_history'] = $chats;
        $data['backgroundImage'] = $this->_background_images;

        $this->load->view('templates/meeting/organizer_uta', $data);
    }

    function panelist($tokenId, $username = NULL)
    {
        if(!$tokenId)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$username)
        {
            $userdata = $this->session->userdata('user') ? $this->session->userdata('user') : $this->session->userdata('usrdata');
            if(!$userdata)
            {
                redirect('/meeting/uta/pform?id=' . $tokenId);
            }

            $username = $userdata['username'];
            $userId = $userdata['user_id'];
        }
        else
        {
            $username = base64_decode($username);

            $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
            $mongoUserQuery['username'] = ['$eq' => $username];
            $userCount = $this->_user_collection->count($mongoUserQuery);
            if(!$userCount)
            {
                echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
                exit;
            }

            $userDetail = $this->_user_collection->findOne($mongoUserQuery);
            $userId = $userDetail['id'];
        }

        $tokenId = base64_decode($tokenId);

        if(!isset($this->_tokenSessionIds[$tokenId]))
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $this->_background_images['url'] = $this->_room_background_images[$tokenId];

        $connectionMetaData = $username;
        $sessionId = $this->_tokenSessionIds[$tokenId];

        $mongoChatQuery['session_id'] = $sessionId;
        $chatCount = $this->_chat_collection->count($mongoChatQuery);
        $chats = [];
        if($chatCount)
        {
            $chatCursor = $this->_chat_collection->find($mongoChatQuery);
            foreach($chatCursor as $chat)
            {
                if($chat['from'] == $userId)
                {
                    $chat['chat_of'] = 'mine';
                    $chat['name'] = $username;
                }
                else if($chat['from_role'] == 'PUBLISHER')
                {
                    $chat['chat_of'] = 'theirs';

                    $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                    $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                    $chat['name'] = $chatUserDetail['username'];
                }
                if($chat['name'])
                {
                    $chats[] = $chat;
                }
            }
        }

        $connectionType = 'ONE_ON_ONE';
        $this->uta_model->deleteConnection($userId, $connectionType);

        $insertConnectionData['user_id'] = $userId;
        $insertConnectionData['tokbox_id'] = $tokenId;
        $insertConnectionData['username'] = $username;
        $insertConnectionData['type'] = $connectionType;
        $this->uta_model->insertConnection($insertConnectionData);

        $row['user_id'] = $userId;
        $row['username'] = $username;
        $row['tokbox_id'] = $tokenId;
        $row['session_id'] = $sessionId;
        $row['type'] = $connectionType;
        $row['student_id'] = '';
        $insertOneResult = $this->_connection_collection->insertOne($row);

        $mongoRoomQuery['tokbox_id'] = (int)$tokenId;
        $this->_room_collection->deleteMany($mongoRoomQuery);

        $roomRow['tokbox_id'] = (int)$tokenId;
        $roomRow['available'] = 1;
        $insertOneResult = $this->_room_collection->insertOne($roomRow);

        $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'moderator',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['sessionId'] = $sessionId;
        $data['username'] = $username;
        $data['userId'] = $userId;
        $data['tokboxId'] = $tokenId;
        $data['apiKey'] = $this->_tokboxId;
        $data['common_chat_history'] = $chats;
        $data['backgroundImage'] = $this->_background_images;

        $this->load->view('templates/meeting/panelist_uta', $data);
    }

    function student($tokenId, $username, $linkCount = 1)
    {
        $blockedStudents = [
            'miten_dalsaniya','harshkumar_patel','satyam_swami','himanshu_more','vaishnavi_sawant','maitreyee_gupte','durgeshbonde','suyog_bachhav',
            'atharva_karmarkar','apoorva_janorkar','kaustubh1321@gmail.com','pranav_gajarmal','juie_darwade','sudheendra_katikar','Ketaki',
            'deepti_tanpure','sidharth_sabadra','Aishwarya9850','snehallokesh','aditi_subhedar','rushikesh_pharate','palash_jhamb','vishakha_kasherwal',
            'vivek_m_shah','himani_bajaj','juhi_dalal','sachi_parekh','janhavipatil80@gmail.com','purva_mundlye','isha_mundlye','ayushj','sakshi_shinde'
        ];

        if(!$tokenId)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $data['base64_token_id'] = $tokenId;
        $data['base64_username'] = $username;

        $tokenId = base64_decode($tokenId);
        $username = base64_decode($username);

        if(in_array($username, $blockedStudents))
        {
            echo '<script type="text/javascript">alert("Sorry! You are not authorized."); window.location.href="/";</script>';
            exit;
        }

        if(!isset($this->_tokenSessionIds[$tokenId]))
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/";</script>';
            exit;
        }
        else
        {
            $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        }
        $userId = $userDetail['id'];
        $linkAllowed = $userDetail['link_allowed'];
        if($linkCount > $linkAllowed)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }
        $ipUserId = $userId;
        if($linkCount != 1)
        {
            $ipUserId = $userId . '-' . $linkCount;
        }

        $userIpAddress = $this->get_client_ip();

        $mongoIpQuery['user_id'] = ['$eq' => $ipUserId];
        $userIpDetail = $this->_ip_collection->findOne($mongoIpQuery);
        if($userIpDetail)
        {
            $free_allow = ['nik_uni_1', 'nik_uni_2', 'nikhil', 'test_nik', 'nikhil_g', 'nik_edu_counselor'];

            $lastIp = $userIpDetail['ip_address'];
            $logout = $this->input->get('l');
            if($lastIp != $userIpAddress && !$logout && !in_array($username, $free_allow))
            {
                $data['errorMessage'] = 'You are logged in from another system. Do you want to logout from other system and continue here?<br/><br/><br/><a href="/meeting/uta/student/' . $data['base64_token_id'] . '/' . $data['base64_username'] . '?l=1"><button type="button">Yes</button></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="/meeting/uta/student/' . $data['base64_token_id'] . '/' . $data['base64_username'] . '?l=0"><button type="button">No</button></a>';
                $this->load->view('templates/meeting/uta_error', $data);
                exit;
            }
            if($logout)
            {
                $this->uta_model->deleteIp($ipUserId);

                $insertData['user_id'] = $ipUserId;
                $insertData['ip_address'] = $userIpAddress;
                $this->uta_model->insertIp($insertData);

                $mongoIpQuery["user_id"] = $ipUserId;
                $deleteResult = $this->_ip_collection->deleteMany($mongoIpQuery);

                $this->_ip_collection->insertOne($insertData);
            }
        }
        else
        {
            $insertData['user_id'] = $ipUserId;
            $insertData['ip_address'] = $userIpAddress;
            $this->uta_model->insertIp($insertData);

            $this->_ip_collection->insertOne($insertData);
        }

        $connectionType = 'ONE_ON_ONE';
        $this->uta_model->deleteConnection($userId, $connectionType);

        $insertConnectionData['user_id'] = $userId;
        $insertConnectionData['tokbox_id'] = $tokenId;
        $insertConnectionData['username'] = $username;
        $insertConnectionData['type'] = $connectionType;
        $this->uta_model->insertConnection($insertConnectionData);

        $connectionMetaData = $username;
        $sessionId = $this->_tokenSessionIds[$tokenId];

        $mongoStudentQuery['username'] = ['$eq' => $username];
        $seminarDetail = $this->_seminar_students_collection->findOne($mongoStudentQuery);

        $row['user_id'] = $userId;
        $row['username'] = $username;
        $row['tokbox_id'] = $tokenId;
        $row['session_id'] = $sessionId;
        $row['type'] = $connectionType;
        $row['student_id'] = $seminarDetail['student_id'];
        $insertOneResult = $this->_connection_collection->insertOne($row);

        //$mongoAttenderQuery['user_id'] = $userId;
        //$deleteResult = $this->_attender_collection->deleteMany($mongoAttenderQuery);

        $row['user_id'] = $userId;
        $row['username'] = $username;
        $row['tokbox_id'] = $tokenId;
        $row['session_id'] = $sessionId;
        $row['type'] = $connectionType;
        $insertOneResult = $this->_attender_collection->insertOne($row);

        $mongoChatQuery['session_id'] = $sessionId;
        $chatCount = $this->_chat_collection->count($mongoChatQuery);
        $chats = [];
        if($chatCount)
        {
            $chatCursor = $this->_chat_collection->find($mongoChatQuery);
            foreach($chatCursor as $chat)
            {
                if($chat['from'] == $userId)
                {
                    $chat['chat_of'] = 'mine';
                    $chat['name'] = $username;
                }
                else if($chat['from_role'] == 'PUBLISHER')
                {
                    $chat['chat_of'] = 'theirs';

                    $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                    $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                    $chat['name'] = $chatUserDetail['username'];
                }
                if($chat['name'])
                {
                    $chats[] = $chat;
                }
            }
        }

        $mongoQueueQuery['username'] = $username;
        $this->_queue_collection->deleteMany($mongoQueueQuery);

        $mongoRoomQuery['tokbox_id'] = (int)$tokenId;
        $roomData = $this->_room_collection->findOne($mongoRoomQuery);
        if(!(int)$roomData['available'])
        {
            $mongoQueueCountQuery['tokbox_id'] = $tokenId;
            $inQueue = $this->_queue_collection->count($mongoQueueCountQuery);

            $queueRow['tokbox_id'] = $tokenId;
            $queueRow['username'] = $username;
            $insertOneResult = $this->_queue_collection->insertOne($queueRow);

            echo '<script type="text/javascript">alert("Sorry! The room is full. We have put you in queue. Your current position is ' . $inQueue . '"); window.location.href="/meeting/uta/rooms/' . $data['base64_username'] . '";</script>';
            exit;
        }

        $this->_background_images['url'] = $this->_room_background_images[$tokenId];

        $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'publisher',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['sessionId'] = $sessionId;
        $data['userId'] = $userId;
        $data['username'] = $username;
        $data['tokboxId'] = $tokenId;
        $data['apiKey'] = $this->_tokboxId;
        $data['linkCount'] = $linkCount;
        $data['common_chat_history'] = $chats;
        $data['backgroundImage'] = $this->_background_images;

        $this->load->view('templates/meeting/student_uta', $data);
    }

    function rooms($username, $linkCount = 1)
    {
        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $blockedStudents = [
            'miten_dalsaniya','harshkumar_patel','satyam_swami','himanshu_more','vaishnavi_sawant','maitreyee_gupte','durgeshbonde','suyog_bachhav',
            'atharva_karmarkar','apoorva_janorkar','kaustubh1321@gmail.com','pranav_gajarmal','juie_darwade','sudheendra_katikar','Ketaki',
            'deepti_tanpure','sidharth_sabadra','Aishwarya9850','snehallokesh','aditi_subhedar','rushikesh_pharate','palash_jhamb','vishakha_kasherwal',
            'vivek_m_shah','himani_bajaj','juhi_dalal','sachi_parekh','janhavipatil80@gmail.com','purva_mundlye','isha_mundlye','ayushj','sakshi_shinde'
        ];

        $decodedUsername = base64_decode($username);

        if(in_array($decodedUsername, $blockedStudents))
        {
            echo '<script type="text/javascript">alert("Sorry! You are not authorized."); window.location.href="/";</script>';
            exit;
        }

        $decodedUsername = iconv('UTF-8', 'UTF-8//IGNORE', $decodedUsername);
        $mongoUserQuery['username'] = ['$eq' => $decodedUsername];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/";</script>';
            exit;
        }

        $seminarDetail = $this->_seminar_students_collection->findOne($mongoUserQuery);
        $selectedRoom = $seminarDetail && $seminarDetail['tokbox_id'] ? $seminarDetail['tokbox_id'] : '';

        $launchDate = $seminarDetail['timeslot'];
        $date = new DateTime($launchDate);
        $launchTime = strtotime($date->format('Y-m-d H:i:s'));
        $currentTime = strtotime(date('Y-m-d H:i:s'));

        if($this->input->get('nik')){
                echo $date->format('Y-m-d H:i:s') . "<br>";
                echo $launchTime . "<br>";
                echo $currentTime . "<br>";
                echo $launchDate; exit;
        }

        $this->outputData['launch'] = 1;
        /*if($currentTime < $launchTime)
    	{
            $slotTime = date('h:i a', strtotime($launchDate));
            $this->outputData['slotTime'] = $slotTime;
            $this->outputData['launch'] = 0;

            echo '<script type="text/javascript">alert("Please come back on your given slot time ' . $slotTime . '"); window.location.href="/";</script>';
            exit;
        }*/

        $this->outputData['username'] = $username;
        $this->outputData['linkCount'] = $linkCount;
        $this->outputData['selectedRoom'] = (int)$selectedRoom;
        $base64_room = base64_encode($selectedRoom);
        $base64_room = str_replace("=", "", $base64_room);
        $this->outputData['base64_room'] = $base64_room;
        $this->render_page('templates/meeting/rooms_uta', $this->outputData);
    }

    function check_user($userId, $linkCount = 1)
    {
        $userIpAddress = $this->get_client_ip();
        if($linkCount != 1)
        {
            $userId = $userId  . '-' . $linkCount;
        }

        $mongoIpQuery['user_id'] = ['$eq' => $userId];
        $userIpDetail = $this->_ip_collection->findOne($mongoIpQuery);
        if($userIpDetail)
        {
            $lastIp = $userIpDetail['ip_address'];
            if($lastIp != $userIpAddress)
            {
                echo 0;
                exit;
            }
        }
        echo 1;
    }

    function error()
    {
        $data['errorMessage'] = "You just logged in from another system. You've been logged out from this system. Please contact to support.";
        $this->load->view('templates/meeting/visaseminar_error', $data);
    }

    function delete_connection($username)
    {
        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            return;
        }

        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        $userId = isset($userDetail['user_id']) ? $userDetail['user_id'] : $userDetail['id'];

        $this->uta_model->deleteConnection($userId);

        $mongoQuery["username"] = $username;
        $deleteResult = $this->_connection_collection->deleteMany($mongoQuery);
    }

    function add_connection($username, $tokenId)
    {
        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            return;
        }

        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        $userId = isset($userDetail['user_id']) ? $userDetail['user_id'] : $userDetail['id'];

        $this->uta_model->deleteConnection($userId);

        $mongoQuery["username"] = $username;
        $deleteResult = $this->_connection_collection->deleteMany($mongoQuery);

        $connectionType = 'ONE_ON_ONE';
        $sessionId = $this->_tokenSessionIds[$tokenId];

        $insertConnectionData['user_id'] = $userId;
        $insertConnectionData['tokbox_id'] = $tokenId;
        $insertConnectionData['username'] = $username;
        $insertConnectionData['type'] = $connectionType;
        $this->uta_model->insertConnection($insertConnectionData);

        $row['user_id'] = $userId;
        $row['username'] = $username;
        $row['tokbox_id'] = $tokenId;
        $row['session_id'] = $sessionId;
        $row['type'] = $connectionType;
        $insertOneResult = $this->_connection_collection->insertOne($row);
    }

    function get_connections($tokenId)
    {
        $connectionType = 'ONE_ON_ONE';
        $connections = [];
        $mongoConnectionQuery['tokbox_id'] = $tokenId;
        $mongoConnectionQuery['type'] = $connectionType;
        $connectionCursor = $this->_connection_collection->find($mongoConnectionQuery);
        foreach($connectionCursor as $connection)
        {
            $connections[] = $connection;
        }

        echo json_encode($connections);
    }

    function post_cid()
    {
        $postCounsellingRooms = $this->_allRooms['post_counselling'];
        $mongoConnectionQuery = [
            ['$group' => ['_id' => '$tokbox_id', 'count' => ['$sum' => 1]]],
            ['$sort' => ['count' => 1]]
        ];
        $tokboxId = '';
        $connectionCursor = $this->_connection_collection->aggregate($mongoConnectionQuery);
        foreach($connectionCursor as $connection)
        {
            if(in_array($connection['_id'], $postCounsellingRooms))
            {
                $tokboxId = $connection['_id'];
                break;
            }
        }
        if(!$tokboxId)
        {
            $random = mt_rand(0, count($postCounsellingRooms) - 1);
            $tokboxId = $postCounsellingRooms[$random];
        }

        $base64_token_id = base64_encode($tokboxId);
        $base64_token_id = str_replace("=", "", $base64_token_id);
        echo $base64_token_id;
    }

    function chat()
    {
        $post_data = json_decode(file_get_contents("php://input"), true);

        $data['from'] = $post_data['user_id'];
        $data['to'] = -1;
        $data['to_user_type'] = NULL;
        $data['session_id'] = $post_data['sessionId'];
        $data['tokbox_id'] = $post_data['tokboxId'];
        $data['user_type'] = 'USER';
        $data['message'] = $post_data['msg'];
        $data['from_role'] = isset($post_data['from_role']) ? $post_data['from_role'] : NULL;
        $data['chat_id'] = isset($post_data['chat_id']) ? $post_data['chat_id'] : NULL;


        $this->uta_model->storeChat($data);

        $insertOneResult = $this->_chat_collection->insertOne($data);
    }

    function get_stats()
    {
        $totalRegistrations = 1477;

        $connectionType = 'ONE_ON_ONE';
        $mongoConnectionQuery['type'] = $connectionType;
        $activeParticipants = $this->_connection_collection->count($mongoConnectionQuery);

        $sessionAttended = 0;

        $attendersCount = $this->_attender_collection->count($mongoConnectionQuery);
        if($attendersCount && $activeParticipants)
        {
            $attendersCursor = $this->_attender_collection->find($mongoConnectionQuery);
            $activeParticipantsCursor = $this->_connection_collection->find($mongoConnectionQuery);

            $allAttenders = [];
            $allActiveParticipants = [];
            foreach($attendersCursor as $attender)
            {
                $allAttenders[] = $attender;
            }
            foreach($activeParticipantsCursor as $activeParticipant)
            {
                $allActiveParticipants[] = $activeParticipant;
            }

            foreach($allAttenders as $attender)
            {
                $found = false;
                foreach($allActiveParticipants as $activeParticipant)
                {
                    if($attender['user_id'] == $activeParticipant['user_id'])
                    {
                        $found = true;
                        break;
                    }
                }
                if(!$found)
                {
                    $sessionAttended++;
                }
            }
        }

        //$activeRooms = mt_rand(10, 15);
        $activeRooms = 0;

        $allStats = [
            'total_registrations' => $totalRegistrations,
            'active_participants' => $activeParticipants,
            'session_attended' => $sessionAttended,
            'active_rooms' => $activeRooms
        ];

        echo json_encode($allStats);
    }

    function thankyou()
    {
        $this->load->view('templates/meeting/visaseminar_thankyou');
    }

    function get_documents($tokboxId, $username = NULL)
    {
        $panelists = ['hong', 'sholanda', 'khalili', 'ginger', 'morgan', 'lin', 'digant_dave', 'nomura', 'kumar', 'gautam', 'crosby', 'paul', 'subash', 'kimetha', 'sandra', 'sebastian', 'peter', 'harriet', 'polly', 'ashley', 'sanjana_sanzgiri', 'komal_mirkar', 'pavni_gaurangi', 'sonali_palkar', 'yamnesh_patel', 'saloni_magre', 'rahul_bhat', 'sanhita_bhattacharya', 'priyanka_mirkar', 'akshay_bhandari', 'sudhir_admin', 'tanmay_kudyadi', 'nik_admin', 'harkiran_sawhney', 'hiren_rathod', 'banit_sawhney', 'deepika_admin', 'srivathsa_vaidya'];
        if(!$username)
        {
            $mongoConnectionQuery['tokbox_id'] = $tokboxId;
            $connectionCursor = $this->_connection_collection->find($mongoConnectionQuery);
            $users = [];
            $trackedUsers = [];
            foreach($connectionCursor as $connection)
            {
                if(!in_array($connection['username'], $panelists) && !in_array($connection['username'], $trackedUsers))
                {
                    $temp['user_id'] = $connection['user_id'];
                    $temp['username'] = $connection['username'];
                    $temp['student_id'] = $connection['student_id'];
                    $users[] = $temp;
                    $trackedUsers[] = $connection['username'];
                }
            }
            echo json_encode($users);
        }
        else
        {
            $documents = [];
            $mongoDocumentQuery['username'] = $username;
            $documentCursor = $this->_document_collection->find($mongoDocumentQuery);
            foreach($documentCursor as $document)
            {
                $documents[] = $document;
            }

            echo json_encode($documents);
        }
    }

    function download($filename, $userId)
    {
        $filepath = APPPATH . '../uploads/student_' . $userId . '/' . $filename;
		header('Content-Disposition: attachment; filename="' . $filename . '";');
		header("Pragma: no-cache");
		header("Expires: 0");
		readfile($filepath);
    }

    function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        return $ipaddress;
    }

    function visaSeminarUrl()
    {

        $userEmailId = $this->input->post('user_email') ? $this->input->post('user_email') : 'NAAA';
        $userMobileNumber = $this->input->post('user_mobile') ? $this->input->post('user_mobile') : 'NAAA';

        $userDetail = $this->uta_model->validUserInfo($userEmailId,$userMobileNumber);

        if($userDetail)
        {

            $tokenId = 157; //$userDetail[0]['tokbox_id'];
            $userName = $userDetail[0]['username'];

            $tokenId = base64_encode($tokenId);
            $usernamecode = base64_encode($userName);

            $usernamecode = str_replace("=", "", $usernamecode);
            $tokenId = str_replace("=", "", $tokenId);

            echo "https://hellouni.org/meeting/uta/rooms/$usernamecode";
            exit();
        }
        else
        {
            echo "Invalid User";
            exit();
        }
    }

    function visaSeminarUrlParent()
    {

        $this->load->model('webinar/common_model');

        $userEmailId = $this->input->post('user_email') ? $this->input->post('user_email') : 'NAAA';
        $userMobileNumber = $this->input->post('user_mobile') ? $this->input->post('user_mobile') : 'NAAA';
        $linkNumber = $this->input->post('link_number') ? $this->input->post('link_number') : 2;

        $userDetail = $this->uta_model->validUserInfo($userEmailId,$userMobileNumber);

        if($userDetail)
        {
            $userName = $userDetail[0]['username'];
            $studentEmail = $userDetail[0]['email'];
            $condition = ["username" => $userName];
            $updateData = [
                "link_allowed" => $linkNumber
            ];

            $updateObj = $this->common_model->upsert($condition,$updateData,"user_master");

            $mongoUserQuery['username'] = $userName;
            $updateMongoUser = ['$set' => ['link_allowed' => $linkNumber]];
            $this->_user_collection->updateOne($mongoUserQuery, $updateMongoUser);

            $tokenId = 157; //$userDetail[0]['tokbox_id'];

            $tokenId = base64_encode($tokenId);
            $usernamecode = base64_encode($userName);

            $usernamecode = str_replace("=", "", $usernamecode);
            $tokenId = str_replace("=", "", $tokenId);

            $mailSubject = "HelloUni - Your Link to access the VISA Finance Webinar (Parent Link : $linkNumber)";
            $mailTemplate = "Hi ,

                                Hope this email finds you and your near & dear ones well... Hope all of you are staying positive & testing negative...

                                We have received a request to generate a URL for you to access the VISA Seminar.

                                Please open the link below on your Laptop (for best result) using Chrome Browser (for best result) to enter the Visa Webinar:

                                URL : https://hellouni.org/meeting/uta/rooms/$usernamecode/$linkNumber

                              Regards,
                              Team HelloUni
                              Tel: +91 81049 09690
                              Email: info@hellouni.org

                               <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

            $ccMailList = '';
            $mailAttachments = '';

            //$studentEmail = 'nikhil.gupta@issc.in';

            $sendMail = sMail($studentEmail, "Student", $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

            if($sendMail){

              echo "https://hellouni.org/meeting/uta/rooms/$usernamecode/$linkNumber";
              exit();

            } else {

              echo "https://hellouni.org/meeting/uta/rooms/$usernamecode/$linkNumber";
              exit();

            }

        }
        else
        {
            echo "Invalid User";
            exit();
        }
    }

    function pform()
    {
        if($this->input->post())
        {
            $post_data = $this->input->post();
            $post_data['name'] = $post_data['first_name'] . ' ' . $post_data['last_name'];
            $tokenId = $post_data['token_id'];

            if(!trim($post_data['first_name']) || !trim($post_data['email']))
            {
                $post_data['error'] = "Name & Email are mandatory";
                $this->load->view('uta_pform', $post_data);
            }
            else
            {
                unset($post_data['token_id']);
                $totalGuests = $this->uta_model->getGuestsCount();
                $post_data['user_id'] = 1000000 + $totalGuests['count'];
                $post_data['type'] = 'panelist';
                $post_data['ip'] = $this->get_client_ip();

                $addGuest = $this->uta_model->addGuest($post_data);
                $addGuest['username'] = $addGuest['name'];
                $this->session->set_userdata('usrdata', $addGuest);

                $mongoUserQuery['username'] = $addGuest['username'];
                $this->_user_collection->deleteMany($mongoUserQuery);

                $this->_user_collection->insertOne($addGuest);

                $panelistRow['tokbox_id'] = $tokenId;
                $panelistRow['name'] = $post_data['name'];
                $this->_panelist_collection->insertOne($panelistRow);

                redirect('/meeting/uta/panelist/' . base64_encode($tokenId));
            }
        }
        else
        {
            $tokenId = $this->input->get('id');
            if(!$tokenId)
            {
                redirect('/');
            }

            $tokenId = base64_decode($tokenId);

            // Token Valication New Code Starts
            $mongoTokenId = iconv('UTF-8', 'UTF-8//IGNORE', $tokenId);
            $mongoTokenQuery['id'] = ['$eq' => $mongoTokenId];
            $tokenCount = $this->_token_collection->count($mongoTokenQuery);
            if(!$tokenCount)
            {
                echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
                exit;
            }
            $tokboxDetail = $this->_token_collection->findOne($mongoTokenQuery);
            // Token Valication New Code Ends

            $sessionId = $tokboxDetail['session_id'];

            $data['session_id'] = $sessionId;
            $data['token_id'] = $tokenId;
            $this->load->view('templates/meeting/uta_pform', $data);
        }
    }

    function temp()
    {
        echo '<script type="text/javascript">alert("The link is not active anymore."); window.location.href="/";</script>';
        exit;

        if($this->input->post())
        {
            $name = $this->input->post('name');
            $username = strtolower(str_replace(" ", "_", $name));

            $checkUser = $this->uta_model->getUserByUsername($username);
            if(!$checkUser)
            {
                $user['username'] = $username;
                $user['name'] = $name;
                $user['type'] = 3;
                $user['status'] = 1;
                $user['password'] = md5($username);
                $user['link_allowed'] = 1;

                $lastInsertId = $this->uta_model->insertUser($user);
                $user['id'] = $lastInsertId;

                $mongoUserQuery['username'] = $username;
                $this->_user_collection->deleteMany($mongoUserQuery);
                $this->_user_collection->insertOne($user);
            }

            /*$tempTokenTypes = [158, 164, 165, 166, 167, 169, 171, 173, 175, 176, 177];
            $storedTokenTypes = [];
            $tokenId = '';

            $tempCursor = $this->_temp_collection->find();
            foreach($tempCursor as $storedTokenType)
            {
                $storedTokenTypes[] = $storedTokenType['token_id'];
            }

            foreach($tempTokenTypes as $tempTokenType)
            {
                if(!in_array($tempTokenType, $storedTokenTypes))
                {
                    $mongoTempQuery['username'] = $username;
                    $this->_temp_collection->deleteMany($mongoTempQuery);

                    $row['token_id'] = $tempTokenType;
                    $row['name'] = $name;
                    $row['username'] = $username;

                    $this->_temp_collection->insertOne($row);

                    $tokenId = $tempTokenType;
                    break;
                }
            }*/

            $base64Username = base64_encode($username);
            $base64Username = str_replace("=", "", $base64Username);

            /*if($tokenId)
            {
                redirect('/meeting/visaseminar/organizer/w/MTU3/' . $base64Username . '?t=1');
            }
            else
            {
                redirect('/meeting/visaseminar/student/w/MTU3/' . $base64Username . '?t=1');
            }*/
            redirect('/meeting/uta/student/w/MTU3/' . $base64Username);
        }
        $this->load->view('templates/meeting/temp_visaseminar');
    }

    function reset($username, $type = '')
    {
        delete_cookie('webinar_completed');
        if($type == 'c')
        {
            redirect('/meeting/uta/student/w/MTU3/' . $username);
        }
        else if($type == 'o')
        {
            redirect('/meeting/uta/organizer/w/MTU3/' . $username);
        }
        else
        {
            redirect('/meeting/uta/rooms/' . $username);
        }
    }

    function room_status($tokenId, $status)
    {
        $mongoRoomQuery['tokbox_id'] = (int)$tokenId;
        $this->_room_collection->deleteMany($mongoRoomQuery);

        if(in_array($tokenId, [189, 196, 197, 198]))
        {
            $status = 1;
        }

        $roomRow['tokbox_id'] = (int)$tokenId;
        $roomRow['available'] = (int)$status;
        $insertOneResult = $this->_room_collection->insertOne($roomRow);
    }

    function wait_count($tokenId)
    {
        $mongoQueueQuery['tokbox_id'] = $tokenId;
        $count = $this->_queue_collection->count($mongoQueueQuery);
        echo $count;
    }

    function get_rooms_status()
    {
        $rooms = [];
        $roomsCursor = $this->_room_collection->find();
        foreach($roomsCursor as $room)
        {
            $rooms[] = $room;
        }
        echo json_encode($rooms);
    }

    function join_queue($tokboxId, $username)
    {
        $username = base64_decode($username);
        $tokboxId = base64_decode($tokboxId);

        $mongoQueueQuery['username'] = $username;
        $this->_queue_collection->deleteMany($mongoQueueQuery);

        $queueRow['tokbox_id'] = $tokboxId;
        $queueRow['username'] = $username;
        $insertOneResult = $this->_queue_collection->insertOne($queueRow);
    }

    function get_queue_status($tokboxId, $username)
    {
        $username = base64_decode($username);
        $tokboxId = base64_decode($tokboxId);

        $mongoRoomQuery['tokbox_id'] = (int)$tokboxId;
        $roomData = $this->_room_collection->findOne($mongoRoomQuery);
        if(!(int)$roomData['available'])
        {
                $mongoQueueQuery['tokbox_id'] = $tokboxId;
                $queueCursor = $this->_queue_collection->find($mongoQueueQuery);

                $inqueue = 0;
                foreach($queueCursor as $queue)
                {
                        if($queue['username'] == $username)
                        {
                                $inqueue = 1;
                        }
                }
                if($inqueue){
                        echo -2;
                }
                else{
                    echo -1;
                }
            exit;
        }

        $mongoQueueQuery['tokbox_id'] = $tokboxId;
        $queueCursor = $this->_queue_collection->find($mongoQueueQuery);

        $index = 0;
        foreach($queueCursor as $queue)
        {
            if($index == 0 && $queue['username'] == $username)
            {
                echo 0;
                exit;
            }
            if($queue['username'] == $username)
            {
                echo $index;
                exit;
            }
            $index++;
        }
    }

    function delete_queue($username)
    {
        $username = base64_decode($username);

        $mongoQueueQuery['username'] = $username;
        $this->_queue_collection->deleteMany($mongoQueueQuery);
    }

    function tech_assistance($tokboxId)
    {
        $mongoTechQuery['tokbox_id'] = $tokboxId;
        $this->_tech_support_collection->deleteMany($mongoTechQuery);

        $techRow['tokbox_id'] = $tokboxId;
        $insertOneResult = $this->_tech_support_collection->insertOne($techRow);
    }

    function update_connection($userId)
    {
        $currentTime = date('Y-m-d H:i:s');
        $data['updated_at'] = $currentTime;
        $this->uta_model->updateConnection($userId, $data);
    }
}
