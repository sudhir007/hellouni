<?php
require APPPATH . "vendor/autoload.php";

use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;

include_once APPPATH . 'mongo/autoload.php';

class Webinar extends MY_Controller
{
    private $_tokboxId = '46220952';
    private $_tokboxSecret = '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2';

    private $_token_collection;
    private $_webinar_collection;
    private $_chat_collection;
    private $_user_collection;
    private $_live_users_collection;

    function __construct()
    {
        parent::__construct();

        $mongo_server = 'localhost';
        $mongo_port = '27017';
        $mongo_client = 'mongodb://' . $mongo_server . ':' . $mongo_port;
        $client = new MongoDB\Client($mongo_client);
        $mongo_db = $client->HelloUni;

        $this->_token_collection = $mongo_db->selectCollection('tokens');
        $this->_webinar_collection = $mongo_db->selectCollection('webinars');
        $this->_chat_collection = $mongo_db->selectCollection('chats');
        $this->_user_collection = $mongo_db->selectCollection('users');
        $this->_live_users_collection = $mongo_db->selectCollection('live_users');
    }

    function index()
    {
        echo "In webinar index";
    }

    function organizer()
    {
        $tokenId = $this->input->get('id');
        if(!$tokenId)
        {
            redirect('/');
        }

        $this->load->model('tokbox_model');
        $this->load->model('webinar/common_model');

        $tokenId = base64_decode($tokenId);
        //$tokenId = base64_decode(strtoupper($tokenId));

        // Token Valication Old Code Starts
        /*$tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
        if(!$tokboxDetail)
        {
            redirect('/');
        }*/
        // Token Valication Old Code Ends

        // Token Valication New Code Starts
        $mongoTokenId = iconv('UTF-8', 'UTF-8//IGNORE', $tokenId);
        $mongoTokenQuery['id'] = ['$eq' => $mongoTokenId];
        $tokenCount = $this->_token_collection->count($mongoTokenQuery);
        if(!$tokenCount)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }
        $tokboxDetail = $this->_token_collection->findOne($mongoTokenQuery);
        // Token Valication New Code Ends

        // Webinar Valication New Code Starts
        $mongoWebinarQuery['tokbox_token_id'] = ['$eq' => $mongoTokenId];
        $webinarCount = $this->_webinar_collection->count($mongoWebinarQuery);
        if(!$webinarCount)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }
        // Webinar Valication New Code Ends

        // Webinar Valication Old Code Starts
        /*
        $whereCondition = array("tokbox_token_id" => $tokenId);
        $webinarData = $this->common_model->get($whereCondition, 'webinar_master');
        if(!$webinarData)
        {
            redirect('/');
        }
        */
        // Webinar Valication Old Code Ends

        $usrdata = $this->session->userdata('user') ? $this->session->userdata('user') : $this->session->userdata('usrdata');
        if(!$usrdata)
        {
            redirect('/meeting/webinar/oform?id=' . base64_encode($tokenId) . '&utype=organizer');
        }

        /*$forceStart = $tokboxDetail->force_start;

        if($tokenId == 8 && !$forceStart)
        {
            $launchTime = strtotime(date('2020-08-18 21:00:00'));
        }
        else
        {
            $launchTime = strtotime(date('2020-07-30 17:30:00'));
        }
        $launchTime = strtotime(date('2020-08-20 18:30:00'));
        $currentTime = strtotime(date('Y-m-d H:i:s'));
        if($currentTime < $launchTime)
    	{
            $this->render_page('templates/meeting/launch',$this->outputData);
        }
        {*/
            $username = $usrdata['username'];
            $userId = $usrdata['id'];
            $userType = $usrdata['typename'] == 'Guest' ? 'GUEST' : 'USER';

            $connectionMetaData = $username;
            $sessionId = $tokboxDetail['session_id'];

            //$connectionData['connected'] = 0;
            //$this->tokbox_model->updateConnectionBySession($connectionData, $sessionId);
            $polling_questions = [];

            // Poll Questions Start
            /*
            $polling_questions = $this->tokbox_model->getAllActivePolls();

            if($polling_questions)
            {
                foreach($polling_questions as $index => $poll_question)
                {
                    $polling_questions[$index]['question_string'] = $poll_question['question'];
                    $choices = [];
                    $options = json_decode($poll_question['options'], true);
                    foreach($options as $option)
                    {
                        if($option == $poll_question['answer'])
                        {
                            $choices['correct'] = $option;
                        }
                        else
                        {
                            $choices['wrong'][] = $option;
                        }
                    }
                    $polling_questions[$index]['choices'] = $choices;
                }
            }
            */
            // Poll Questions End

            $commonChat = array();
            $privateChat = array();

            // Old Chat Starts
            /*
            $sentMessages = $this->tokbox_model->getChatFromUserSession($userId, $userType, $sessionId);
            $receivedMessages = $this->tokbox_model->getChatToUserSession($userId, $userType, $sessionId);

            $sentUsers = array();
            $sentGuests = array();
            $sentUserNames = array();
            $sentGuestNames = array();
            if($receivedMessages)
            {
                foreach($receivedMessages as $receivedMessage)
                {
                    if($receivedMessage['user_type'] == 'USER' && !in_array($receivedMessage['from'], $sentUsers))
                    {
                        $sentUsers[] = $receivedMessage['from'];
                    }
                    else if($receivedMessage['user_type'] == 'GUEST' && !in_array($receivedMessage['from'], $sentGuests))
                    {
                        $sentGuests[] = $receivedMessage['from'];
                    }
                }

                if($sentUsers)
                {
                    $sentUserNames = $this->tokbox_model->getUserName($sentUsers);
                }
                if($sentGuests)
                {
                    $sentGuestNames = $this->tokbox_model->getGuestName($sentGuests);
                }
            }

            $allMessages = [];
            if($sentMessages)
            {
                foreach($sentMessages as $sentMessage)
                {
                    $allMessages[$sentMessage['id']] = $sentMessage;
                }
            }
            if($receivedMessages)
            {
                foreach($receivedMessages as $receivedMessage)
                {
                    $allMessages[$receivedMessage['id']] = $receivedMessage;
                }
            }

            if($allMessages)
            {
                ksort($allMessages);
                foreach($allMessages as $index => $message)
                {
                    if($message['chat_of'] == 'theirs')
                    {
                        if(in_array($message['from'], $sentUsers))
                        {
                            foreach($sentUserNames as $sentUserName)
                            {
                                if($sentUserName['id'] == $message['from'])
                                {
                                    $message['name'] = $sentUserName['name'];
                                    $allMessages[$index] = $message;
                                }
                            }
                        }
                        else if(in_array($message['from'], $sentGuests))
                        {
                            foreach($sentGuestNames as $sentGuestName)
                            {
                                if($sentGuestName['id'] == $message['from'])
                                {
                                    $message['name'] = $sentGuestName['name'];
                                    $allMessages[$index] = $message;
                                }
                            }
                        }
                    }
                    else
                    {
                        $message['name'] = $username;
                        $allMessages[$index] = $message;
                    }
                }
            }

            if($allMessages)
            {
                foreach($allMessages as $chatMessage)
                {
                    if($chatMessage['to'] == -1)
                    {
                        $commonChat[] = $chatMessage;
                    }
                    else
                    {
                        $privateChat[] = $chatMessage;
                    }
                }
            }*/
            // Old Chat Ends

            // New Chat Starts
            $mongoChatQuery['session_id'] = $sessionId;
            $chatCount = $this->_chat_collection->count($mongoChatQuery);
            $chats = [];
            if($chatCount)
            {
                $chatCursor = $this->_chat_collection->find($mongoChatQuery);
                foreach($chatCursor as $chat)
                {
                    $chat['chat_of'] = 'mine';
                    $chat['name'] = $username;
                    if($chat['from'] != $userId)
                    {
                        $chat['chat_of'] = 'theirs';

                        $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                        $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                        $chat['name'] = $chatUserDetail['username'];
                    }
                    if($chat['name'])
                    {
                        $chats[] = $chat;
                    }
                }
            }
            // New Chat Ends

            $running_poll = [];
            // Running Poll Starts
            //$running_poll = $this->tokbox_model->getRunningPollByToken($tokenId);
            // Running Poll Ends

            $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

            $data['token'] = $opentok->generateToken($sessionId, array(
                'role' => 'moderator',
                'expireTime' => time()+(7 * 24 * 60 * 60),
                'data' =>  $connectionMetaData
            ));

            $data['sessionId'] = $sessionId;
            $data['username'] = $username;
            $data['tokboxId'] = $tokenId;
            $data['polling'] = 0;
            $data['polling_questions'] = json_encode($polling_questions);
            $data['common_chat_history'] = $chats;
            $data['private_chat_history'] = $privateChat;
            $data['apiKey'] = $this->_tokboxId;
            $data['running_poll'] = $running_poll;

            $this->load->view('templates/meeting/organizer', $data);
            //$this->load->view('templates/meeting/panelist', $data);
        //}
    }

    function panelist()
    {
        $tokenId = $this->input->get('id');
        if(!$tokenId)
        {
            redirect('/');
        }

        $this->load->model('tokbox_model');
        $this->load->model('webinar/common_model');

        $tokenId = base64_decode($tokenId);
        //$tokenId = base64_decode(strtoupper($tokenId));

        // Token Valication Old Code Starts
        /*
        $tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
        if(!$tokboxDetail)
        {
            redirect('/');
        }*/
        // Token Valication Old Code Ends

        // Token Valication New Code Starts
        $mongoTokenId = iconv('UTF-8', 'UTF-8//IGNORE', $tokenId);
        $mongoTokenQuery['id'] = ['$eq' => $mongoTokenId];
        $tokenCount = $this->_token_collection->count($mongoTokenQuery);
        if(!$tokenCount)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }
        $tokboxDetail = $this->_token_collection->findOne($mongoTokenQuery);
        // Token Valication New Code Ends

        // Webinar Valication New Code Starts
        $mongoWebinarQuery['tokbox_token_id'] = ['$eq' => $mongoTokenId];
        $webinarCount = $this->_webinar_collection->count($mongoWebinarQuery);
        if(!$webinarCount)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }
        // Webinar Valication New Code Ends

        // Webinar Valication Old Code Starts
        /*
        $whereCondition = array("tokbox_token_id" => $tokenId);
        $webinarData = $this->common_model->get($whereCondition, 'webinar_master');
        if(!$webinarData)
        {
            redirect('/');
        }
        */
        // Webinar Valication Old Code Ends

        $usrdata = $this->session->userdata('user') ? $this->session->userdata('user') : $this->session->userdata('usrdata');
        if(!$usrdata)
        {
            redirect('/meeting/webinar/oform?id=' . base64_encode($tokenId) . '&utype=panelist');
        }

        $forceStart = $tokboxDetail['force_start'];
    	$this->outputData['stimezone'] = date_default_timezone_get();
    	//$ip = "72.229.28.185"; //$_SERVER['REMOTE_ADDR'];
        $ip = $_SERVER['REMOTE_ADDR'];
        $ipInfo = file_get_contents('http://ip-api.com/json/' . $ip);
        $ipInfo = json_decode($ipInfo);
        $timezone = $ipInfo->timezone ? $ipInfo->timezone : date_default_timezone_get();
        //date_default_timezone_set($timezone);

        if($tokenId == 21 && !$forceStart)
        {
            $launchDate = '2020-09-21 19:00:00';
        	$date = new DateTime($launchDate, new DateTimeZone('Asia/Calcutta'));
        	$date->setTimezone(new DateTimeZone($timezone));
            $launchTime = strtotime($date->format('Y-m-d H:i:s'));
        }
        else if($tokenId == 28 && !$forceStart)
        {
            $launchDate = '2020-08-16 17:00:00';
            $date = new DateTime($launchDate, new DateTimeZone('Asia/Calcutta'));
            $date->setTimezone(new DateTimeZone($timezone));
            $launchTime = strtotime($date->format('Y-m-d H:i:s'));
        }
        else if(!$this->input->get('test'))
        {
            //$launchTime = strtotime(date('2021-02-08 19:00:00'));
            $launchDate = '2021-02-08 19:00:00';
            $date = new DateTime($launchDate, new DateTimeZone('Asia/Calcutta'));
            $date->setTimezone(new DateTimeZone($timezone));
            $launchTime = strtotime($date->format('Y-m-d H:i:s'));
        }
        else
        {
            //$launchTime = strtotime(date('2021-02-08 19:00:00'));
            $launchDate = '2020-02-08 19:00:00';
            $date = new DateTime($launchDate, new DateTimeZone('Asia/Calcutta'));
            $date->setTimezone(new DateTimeZone($timezone));
            $launchTime = strtotime($date->format('Y-m-d H:i:s'));
        }

        $currentTime = strtotime(date('Y-m-d H:i:s'));
        $this->outputData['token_id'] = $tokenId;
        $this->outputData['timezone'] = $timezone;
        if($currentTime < $launchTime)
    	{
            $this->render_page('templates/meeting/launch',$this->outputData);
        }
        {
            $username = $usrdata['username'];
            $userId = $usrdata['id'];
            $userType = $usrdata['typename'] == 'Guest' ? 'GUEST' : 'USER';

            $connectionMetaData = $username;
            $sessionId = $tokboxDetail['session_id'];

            $polling_questions = [];

            // Poll Questions Start
            /*
            $polling_questions = $this->tokbox_model->getAllActivePolls();

            if($polling_questions)
            {
                foreach($polling_questions as $index => $poll_question)
                {
                    $polling_questions[$index]['question_string'] = $poll_question['question'];
                    $choices = [];
                    $options = json_decode($poll_question['options'], true);
                    foreach($options as $option)
                    {
                        if($option == $poll_question['answer'])
                        {
                            $choices['correct'] = $option;
                        }
                        else
                        {
                            $choices['wrong'][] = $option;
                        }
                    }
                    $polling_questions[$index]['choices'] = $choices;
                }
            }
            */

            $commonChat = array();
            $privateChat = array();

            // Old Chat Starts
            /*
            $sentMessages = $this->tokbox_model->getChatFromUserSession($userId, $userType, $sessionId);
            $receivedMessages = $this->tokbox_model->getChatToUserSession($userId, $userType, $sessionId);

            $sentUsers = array();
            $sentGuests = array();
            $sentUserNames = array();
            $sentGuestNames = array();
            if($receivedMessages)
            {
                foreach($receivedMessages as $receivedMessage)
                {
                    if($receivedMessage['user_type'] == 'USER' && !in_array($receivedMessage['from'], $sentUsers))
                    {
                        $sentUsers[] = $receivedMessage['from'];
                    }
                    else if($receivedMessage['user_type'] == 'GUEST' && !in_array($receivedMessage['from'], $sentGuests))
                    {
                        $sentGuests[] = $receivedMessage['from'];
                    }
                }

                if($sentUsers)
                {
                    $sentUserNames = $this->tokbox_model->getUserName($sentUsers);
                }
                if($sentGuests)
                {
                    $sentGuestNames = $this->tokbox_model->getGuestName($sentGuests);
                }
            }

            $allMessages = [];
            if($sentMessages)
            {
                foreach($sentMessages as $sentMessage)
                {
                    $allMessages[$sentMessage['id']] = $sentMessage;
                }
            }
            if($receivedMessages)
            {
                foreach($receivedMessages as $receivedMessage)
                {
                    $allMessages[$receivedMessage['id']] = $receivedMessage;
                }
            }

            if($allMessages)
            {
                ksort($allMessages);
                foreach($allMessages as $index => $message)
                {
                    if($message['chat_of'] == 'theirs')
                    {
                        if(in_array($message['from'], $sentUsers))
                        {
                            foreach($sentUserNames as $sentUserName)
                            {
                                if($sentUserName['id'] == $message['from'])
                                {
                                    $message['name'] = $sentUserName['name'];
                                    $allMessages[$index] = $message;
                                }
                            }
                        }
                        else if(in_array($message['from'], $sentGuests))
                        {
                            foreach($sentGuestNames as $sentGuestName)
                            {
                                if($sentGuestName['id'] == $message['from'])
                                {
                                    $message['name'] = $sentGuestName['name'];
                                    $allMessages[$index] = $message;
                                }
                            }
                        }
                    }
                    else
                    {
                        $message['name'] = $username;
                        $allMessages[$index] = $message;
                    }
                }
            }

            if($allMessages)
            {
                foreach($allMessages as $chatMessage)
                {
                    if($chatMessage['to'] == -1)
                    {
                        $commonChat[] = $chatMessage;
                    }
                    else
                    {
                        $privateChat[] = $chatMessage;
                    }
                }
            }
            */
            // Old Chat Ends

            // New Chat Starts
            $mongoChatQuery['session_id'] = $sessionId;
            $chatCount = $this->_chat_collection->count($mongoChatQuery);
            $chats = [];
            if($chatCount)
            {
                $chatCursor = $this->_chat_collection->find($mongoChatQuery);
                foreach($chatCursor as $chat)
                {
                    $chat['chat_of'] = 'mine';
                    $chat['name'] = $username;
                    if($chat['from'] != $userId)
                    {
                        $chat['chat_of'] = 'theirs';

                        $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                        $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                        $chat['name'] = $chatUserDetail['username'];
                    }
                    if($chat['name'])
                    {
                        $chats[] = $chat;
                    }
                }
            }
            // New Chat Ends

            $running_poll = [];
            // Running Poll Starts
            //$running_poll = $this->tokbox_model->getRunningPollByToken($tokenId);
            // Running Poll Ends

            $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

            $data['token'] = $opentok->generateToken($sessionId, array(
                'role' => 'moderator',
                'expireTime' => time()+(7 * 24 * 60 * 60),
                'data' =>  $connectionMetaData
            ));

            $data['sessionId'] = $sessionId;
            $data['username'] = $username;
            $data['tokboxId'] = $tokenId;
            $data['polling'] = 0;
            $data['polling_questions'] = json_encode($polling_questions);
            $data['common_chat_history'] = $chats;
            $data['private_chat_history'] = $privateChat;
            $data['apiKey'] = $this->_tokboxId;
            $data['running_poll'] = $running_poll;

            //$this->load->view('templates/meeting/organizer', $data);
            $this->load->view('templates/meeting/panelist', $data);
        }
    }

    function attendee()
    {
        $tokenId = $this->input->get('id');
        if(!$tokenId)
        {
            redirect('/');
        }

        $this->load->model('tokbox_model');
        $this->load->model('webinar/common_model');

        $tokenId = base64_decode($tokenId);
        //$tokenId = base64_decode(strtoupper($tokenId));

        // Token Valication Old Code Starts
        /*
        $tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
        if(!$tokboxDetail)
        {
            redirect('/');
        }
        */
        // Token Valication Old Code Ends

        // Token Valication New Code Starts
        $mongoTokenId = iconv('UTF-8', 'UTF-8//IGNORE', $tokenId);
        $mongoTokenQuery['id'] = ['$eq' => $mongoTokenId];
        $tokenCount = $this->_token_collection->count($mongoTokenQuery);
        if(!$tokenCount)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }
        $tokboxDetail = $this->_token_collection->findOne($mongoTokenQuery);
        // Token Valication New Code Ends

        // Webinar Valication New Code Starts
        $mongoWebinarQuery['tokbox_token_id'] = ['$eq' => $mongoTokenId];
        $webinarCount = $this->_webinar_collection->count($mongoWebinarQuery);
        if(!$webinarCount)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }
        // Webinar Valication New Code Ends

        // Webinar Valication Old Code Starts
        /*
        $whereCondition = array("tokbox_token_id" => $tokenId);
        $webinarData = $this->common_model->get($whereCondition, 'webinar_master');
        if(!$webinarData)
        {
            redirect('/');
        }
        */
        // Webinar Valication Old Code Ends

        $usrdata = $this->session->userdata('user') ? $this->session->userdata('user') : $this->session->userdata('usrdata');
        if(!$usrdata)
        {
            //redirect('/meeting/webinar/aform?id=' . base64_encode($tokenId));
            $data['error'] = "Please login first to join meeting";
            $this->load->view('templates/meeting/attendee', $data);
        }
        else
        {
            $forceStart = $tokboxDetail['force_start'];
            $this->outputData['token_id'] = $tokenId;
    		$this->outputData['stimezone'] = date_default_timezone_get();
    		$ip = $_SERVER['REMOTE_ADDR'];
        	$ipInfo = file_get_contents('http://ip-api.com/json/' . $ip);
            $ipInfo = json_decode($ipInfo);
            $timezone = $ipInfo->timezone ? $ipInfo->timezone : date_default_timezone_get();
            date_default_timezone_set($timezone);
            $this->outputData['timezone'] = $timezone;
            if($tokenId == 21 && !$forceStart)
            {
                $launchTime = strtotime(date('2020-09-23 19:00:00'));
            }
            else if($tokenId == 28 && !$forceStart)
            {
                $launchTime = strtotime(date('2020-09-16 17:00:00'));
            }
            else if(!$this->input->get('test'))
            {
                $launchTime = strtotime(date('2021-02-08 19:00:00'));
            }
            else
            {
                $launchTime = strtotime(date('2020-02-08 19:00:00'));
            }
            $currentTime = strtotime(date('Y-m-d H:i:s'));
            if($currentTime < $launchTime)
        	{
                $this->render_page('templates/meeting/launch',$this->outputData);
            }
            else
            {
                $username = $usrdata['username'];
                $userId = $usrdata['id'];
                $userEmail = $usrdata['email'];
                $userType = $usrdata['typename'] == 'Guest' ? 'GUEST' : 'USER';

                $connectionMetaData = $username;
                $sessionId = $tokboxDetail['session_id'];

                $polling_questions = [];

                // Poll Questions Start
                /*
                $polling_questions = $this->tokbox_model->getAllActivePolls();

                if($polling_questions)
                {
                    foreach($polling_questions as $index => $poll_question)
                    {
                        $polling_questions[$index]['question_string'] = $poll_question['question'];
                        $choices = [];
                        $options = json_decode($poll_question['options'], true);
                        foreach($options as $option)
                        {
                            if($option == $poll_question['answer'])
                            {
                                $choices['correct'] = $option;
                            }
                            else
                            {
                                $choices['wrong'][] = $option;
                            }
                        }
                        $polling_questions[$index]['choices'] = $choices;
                    }
                }
                */

                $commonChat = array();
                $privateChat = array();

                // Old Chat Starts
                /*
                $sentMessages = $this->tokbox_model->getChatFromUserSession($userId, $userType, $sessionId);
                $receivedMessages = $this->tokbox_model->getChatToUserSession($userId, $userType, $sessionId);

                $sentUsers = array();
                $sentGuests = array();
                $sentUserNames = array();
                $sentGuestNames = array();
                if($receivedMessages)
                {
                    foreach($receivedMessages as $receivedMessage)
                    {
                        if($receivedMessage['user_type'] == 'USER' && !in_array($receivedMessage['from'], $sentUsers))
                        {
                            $sentUsers[] = $receivedMessage['from'];
                        }
                        else if($receivedMessage['user_type'] == 'GUEST' && !in_array($receivedMessage['from'], $sentGuests))
                        {
                            $sentGuests[] = $receivedMessage['from'];
                        }
                    }

                    if($sentUsers)
                    {
                        $sentUserNames = $this->tokbox_model->getUserName($sentUsers);
                    }
                    if($sentGuests)
                    {
                        $sentGuestNames = $this->tokbox_model->getGuestName($sentGuests);
                    }
                }

                $allMessages = [];
                if($sentMessages)
                {
                    foreach($sentMessages as $sentMessage)
                    {
                        $allMessages[$sentMessage['id']] = $sentMessage;
                    }
                }
                if($receivedMessages)
                {
                    foreach($receivedMessages as $receivedMessage)
                    {
                        $allMessages[$receivedMessage['id']] = $receivedMessage;
                    }
                }

                if($allMessages)
                {
                    ksort($allMessages);
                    foreach($allMessages as $index => $message)
                    {
                        if($message['chat_of'] == 'theirs')
                        {
                            if(in_array($message['from'], $sentUsers))
                            {
                                foreach($sentUserNames as $sentUserName)
                                {
                                    if($sentUserName['id'] == $message['from'])
                                    {
                                        $message['name'] = $sentUserName['name'];
                                        $allMessages[$index] = $message;
                                    }
                                }
                            }
                            else if(in_array($message['from'], $sentGuests))
                            {
                                foreach($sentGuestNames as $sentGuestName)
                                {
                                    if($sentGuestName['id'] == $message['from'])
                                    {
                                        $message['name'] = $sentGuestName['name'];
                                        $allMessages[$index] = $message;
                                    }
                                }
                            }
                        }
                        else
                        {
                            $message['name'] = $username;
                            $allMessages[$index] = $message;
                        }
                    }
                }

                if($allMessages)
                {
                    foreach($allMessages as $chatMessage)
                    {
                        if($chatMessage['to'] == -1)
                        {
                            $commonChat[] = $chatMessage;
                        }
                        else
                        {
                            $privateChat[] = $chatMessage;
                        }
                    }
                }
                */
                // Old Chat Ends

                // New Chat Starts
                $mongoChatQuery['session_id'] = $sessionId;
                $chatCount = $this->_chat_collection->count($mongoChatQuery);
                $chats = [];
                if($chatCount)
                {
                    $chatCursor = $this->_chat_collection->find($mongoChatQuery);
                    foreach($chatCursor as $chat)
                    {
                        if($chat['from'] == $userId)
                        {
                            $chat['chat_of'] = 'mine';
                            $chat['name'] = $username;
                        }
                        else if($chat['from_role'] == 'PUBLISHER')
                        {
                            $chat['chat_of'] = 'theirs';

                            $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                            $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                            $chat['name'] = $chatUserDetail['username'];
                        }
                        if($chat['name'])
                        {
                            $chats[] = $chat;
                        }
                    }
                }
                // New Chat Ends

                $running_poll = [];
                // Running Poll Starts
                //$running_poll = $this->tokbox_model->getRunningPollByToken($tokenId);
                // Running Poll Ends

                $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

                $data['token'] = $opentok->generateToken($sessionId, array(
                    'role' => 'subscriber',
                    'expireTime' => time()+(7 * 24 * 60 * 60),
                    'data' =>  $connectionMetaData
                ));
                $data['sessionId'] = $sessionId;
                $data['username'] = $username;
                $data['tokboxId'] = $tokenId;
                $data['polling'] = 0;
                $data['polling_questions'] = json_encode($polling_questions);
                $data['common_chat_history'] = $chats;
                $data['private_chat_history'] = $privateChat;
                $data['apiKey'] = $this->_tokboxId;
                $data['email'] = $userEmail;
                $data['running_poll'] = $running_poll;

                $this->load->view('templates/meeting/attendee', $data);
            }
        }
    }

    function aform()
    {
        $this->load->model('tokbox_model');

        if($this->input->post())
        {
            $post_data = $this->input->post();
            $post_data['have_i20'] = $post_data['have_i20'] ? $post_data['have_i20'] : NULL;
            $post_data['filling_visa'] = $post_data['filling_visa'] ? $post_data['filling_visa'] : NULL;
            $post_data['name'] = $post_data['first_name'] . ' ' . $post_data['last_name'];
            $tokenId = $post_data['token_id'];

            if(!trim($post_data['first_name']) || !trim($post_data['email']))
            {
                $post_data['error'] = "Name & Email are mandatory";
                $this->load->view('organizer_form', $post_data);
            }
            else
            {
                unset($post_data['token_id']);
                $addGuest = $this->tokbox_model->addGuest($post_data);
                $addGuest['username'] = $addGuest['name'];
                $addGuest['typename'] = 'Guest';
                $this->session->set_userdata('usrdata', $addGuest);
                redirect('/meeting/webinar/attendee?id=' . base64_encode($tokenId));
            }
        }
        else
        {
            $tokenId = $this->input->get('id');
            if(!$tokenId)
            {
                redirect('/');
            }

            $tokenId = base64_decode($tokenId);
            //$tokenId = base64_decode(strtoupper($tokenId));

            // Token Valication Old Code Starts
            /*
            $tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
            if(!$tokboxDetail)
            {
                redirect('/');
            }*/
            // Token Valication Old Code Ends

            // Token Valication New Code Starts
            $mongoTokenId = iconv('UTF-8', 'UTF-8//IGNORE', $tokenId);
            $mongoTokenQuery['id'] = ['$eq' => $mongoTokenId];
            $tokenCount = $this->_token_collection->count($mongoTokenQuery);
            if(!$tokenCount)
            {
                echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
                exit;
            }
            $tokboxDetail = $this->_token_collection->findOne($mongoTokenQuery);
            // Token Valication New Code Ends

            $sessionId = $tokboxDetail['session_id'];

            $data['session_id'] = $sessionId;
            $data['token_id'] = $tokenId;
            $this->load->view('templates/meeting/attendee_form', $data);
        }
    }

    function oform()
    {
        $this->load->model('tokbox_model');

        if($this->input->post())
        {
            $post_data = $this->input->post();
            $post_data['name'] = $post_data['first_name'] . ' ' . $post_data['last_name'];
            $tokenId = $post_data['token_id'];

            if(!trim($post_data['first_name']) || !trim($post_data['email']))
            {
                $post_data['error'] = "Name & Email are mandatory";
                $this->load->view('organizer_form', $post_data);
            }
            else
            {
                unset($post_data['token_id']);
                $addGuest = $this->tokbox_model->addGuest($post_data);
                $addGuest['username'] = $addGuest['name'];
                $addGuest['typename'] = 'Guest';
                $this->session->set_userdata('usrdata', $addGuest);
                $type = $post_data['type'];
                redirect('/meeting/webinar/' . $type . '?id=' . base64_encode($tokenId));
            }
        }
        else
        {
            $tokenId = $this->input->get('id');
            $type = $this->input->get('utype');
            if(!$tokenId)
            {
                redirect('/');
            }

            $tokenId = base64_decode($tokenId);
            //$tokenId = base64_decode(strtoupper($tokenId));

            // Token Valication Old Code Starts
            /*
            $tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
            if(!$tokboxDetail)
            {
                redirect('/');
            }
            */
            // Token Valication Old Code Ends

            // Token Valication New Code Starts
            $mongoTokenId = iconv('UTF-8', 'UTF-8//IGNORE', $tokenId);
            $mongoTokenQuery['id'] = ['$eq' => $mongoTokenId];
            $tokenCount = $this->_token_collection->count($mongoTokenQuery);
            if(!$tokenCount)
            {
                echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
                exit;
            }
            $tokboxDetail = $this->_token_collection->findOne($mongoTokenQuery);
            // Token Valication New Code Ends

            $sessionId = $tokboxDetail['session_id'];

            $data['type'] = $type;
            $data['session_id'] = $sessionId;
            $data['token_id'] = $tokenId;
            $this->load->view('templates/meeting/organizer_form', $data);
        }
    }

    function runpoll()
    {
        $post_data = json_decode(file_get_contents("php://input"), true);
        if($post_data)
        {
            $this->load->model('tokbox_model');
            $action = $post_data['action'];
            switch($action)
            {
                case 'add':
                    $data['token_id'] = $post_data['tid'];
                    $data['connection_id'] = $post_data['cid'];
                    $data['question_index'] = $post_data['qi'];
                    $this->tokbox_model->addRunPoll($data);
                    break;
                case 'delete':
                    $tokenId = $post_data['tid'];
                    $this->tokbox_model->deleteRunPoll($tokenId);
                    break;
            }
        }
    }

    function check_time()
    {
        $this->load->model('tokbox_model');
        $post_data = json_decode(file_get_contents("php://input"), true);
        //date_default_timezone_set($post_data['timezone']);
        $tokenId = $post_data['tid'];
        if($tokenId == 21)
        {
            $launchDate = '2020-09-23 19:00:00';
        }
        else if($tokenId == 28)
        {
            $launchDate = '2020-09-16 17:00:00';
        }
        $launchDate = '2021-02-08 19:00:00';
     	$date = new DateTime($launchDate, new DateTimeZone('Asia/Calcutta'));
    	$date->setTimezone(new DateTimeZone($post_data['timezone']));
    	//echo $date->format('Y-m-d H:i:s');

     	$launchTime = strtotime($date->format('Y-m-d H:i:s')) * 1000;
        //$launchTime = strtotime(date('2020-08-21 19:30:00')) * 1000;

        //$tokenId = $post_data['tid'];

        // Token Valication Old Code Starts
        $tokboxDetail = $this->tokbox_model->getTokenById($tokenId);
        // Token Valication Old Code Ends

        // Token Valication New Code Starts
        $mongoTokenId = iconv('UTF-8', 'UTF-8//IGNORE', $tokenId);
        $mongoTokenQuery['id'] = ['$eq' => $mongoTokenId];
        $tokenCount = $this->_token_collection->count($mongoTokenQuery);
        if(!$tokenCount)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }
        $tokboxDetail = $this->_token_collection->findOne($mongoTokenQuery);
        // Token Valication New Code Ends

        $forceStart = $tokboxDetail['force_start'];
        if($forceStart)
        {
            $launchTime = 0;
        }

        /*$userTime = $post_data['time'];

        $distance = $launchTime - $userTime;

        $days = floor($distance / (1000 * 60 * 60 * 24));
        $hours = floor(($distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        $minutes = floor(($distance % (1000 * 60 * 60)) / (1000 * 60));
        $seconds = floor(($distance % (1000 * 60)) / 1000);

        $response = ["days" => $days, "hours" => $hours, "minutes" => $minutes, "seconds" => $seconds, "launch" => $launchTime];
        echo json_encode($response);*/
        //echo $post_data['timezone'] . " " . date_default_timezone_get();
        echo $launchTime;
    }

    function startSession()
    {
        $this->load->model('tokbox_model');
        $post_data = json_decode(file_get_contents("php://input"), true);
        $tokenId = $post_data['tid'];
        $updatedData['force_start'] = 1;
        $this->tokbox_model->editToken($updatedData, $tokenId);
        echo 1;
    }

    function connection()
    {
        $usrdata = $this->session->userdata('usrdata') ? $this->session->userdata('usrdata') : $this->session->userdata('user');
        $post_data = json_decode(file_get_contents("php://input"), true);

        $this->load->model('tokbox_model');

        $data['connection_id'] = $post_data['connectionId'];
        $data['session_id'] = $post_data['sessionId'];
        $data['tokbox_id'] = $post_data['tokboxId'];
        $data['user_type'] = ($usrdata['typename'] == 'Guest') ? 'GUEST' : 'USER';
        $data['user_id'] = $usrdata['id'];
        $data['username'] = $post_data['username'];
        $data['role'] = isset($post_data['role']) ? $post_data['role'] : '';

        $this->tokbox_model->addWebinarConnection($data);

        $mongoLiveUserQuery['user_id'] = $usrdata['id'];
        $deleteResult = $this->_live_users_collection->deleteMany($mongoLiveUserQuery);
        $insertOneResult = $this->_live_users_collection->insertOne($data);
    }

    function get_connections()
    {
        $attendees = [];
        $organizers = [];
        $panelists = [];

        $liveUsersCount = $this->_live_users_collection->count();
        if($liveUsersCount)
        {
            $alreadyAdded = [];
            $liveUsersCursor = $this->_live_users_collection->find();
            foreach($liveUsersCursor as $liveUser)
            {
                if(!in_array($liveUser['username'], $alreadyAdded))
                {
                    switch($liveUser['role'])
                    {
                        case 'Attendee':
                            $attendees[] = $liveUser;
                            break;
                        case 'Panelist':
                            $panelists[] = $liveUser;
                            break;
                        case 'Organizer':
                            $organizers[] = $liveUser;
                            break;
                    }

                    $alreadyAdded[] = $liveUser['username'];
                }
            }
        }

        $finalResult = [
            'attendees' => $attendees,
            'panelists' => $panelists,
            'organizers' => $organizers,
            'attendee_count' => count($attendees),
            'panelist_count' => count($panelists),
            'organizer_count' => count($organizers)
        ];

        echo json_encode($finalResult);
    }

    function delete_connection()
    {
        $post_data = json_decode(file_get_contents("php://input"), true);

        $this->load->model('tokbox_model');

        $mongoLiveUserQuery['connection_id'] = $post_data['connectionId'];
        $deleteResult = $this->_live_users_collection->deleteMany($mongoLiveUserQuery);
    }
}
