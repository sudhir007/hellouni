<?php
require_once APPPATH . 'libraries/Mail/sMail.php';

require APPPATH . "vendor/autoload.php";
include_once APPPATH . 'mongo/autoload.php';

use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;

class Loanmela extends MY_Controller
{
    private $_tokboxId = '46220952';
    private $_tokboxSecret = '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2';
    private $_allowedStudents = 10;

    private $_tokenSessionIds = [
        '199' => '1_MX40NjIyMDk1Mn5-MTYxNDg0ODYxNzEyMH5NbkQ4OVRxbjBCVFVra2RQd1RSM2o4V1d-fg',
        '200' => '1_MX40NjIyMDk1Mn5-MTYxNDg0ODYxNzM5OH52UC93MndOU3ZaY01yVStTaElJNWQ3eU5-fg',
        '201' => '1_MX40NjIyMDk1Mn5-MTYxNDg0ODYxNzY3NH5rbXI0L3NISWUyKzJ0MWR2cUMrc3JuTk5-fg',
        '202' => '2_MX40NjIyMDk1Mn5-MTYxNDg0ODYxNzk0Nn5WK1NXRUU2QUNFdWRHTkVTWjNMczZMOSt-fg',
        '203' => '1_MX40NjIyMDk1Mn5-MTYxNDg0ODYxODIxNH40blF1eGxhOFpNV01lUUNkQjlkdVFHM05-fg',
        '204' => '1_MX40NjIyMDk1Mn5-MTYxNDg0ODYxODQ4OH53ek5pRnphQlpjZFBHWk5ITzBzT0R6Zi9-fg',
        '205' => '1_MX40NjIyMDk1Mn5-MTYxNDg0ODYxODc2NH5rUWVJNHkranBDN0tSNDExZ2NYK2FFbDN-fg',
        '206' => '2_MX40NjIyMDk1Mn5-MTYxNDg0ODYxOTA1NX53WXZlWHBndHR5c21OMHpHSGREbi9PSHR-fg',
        '207' => '2_MX40NjIyMDk1Mn5-MTYxNDg0ODYxOTQyN35uNXlMM2d1YXRHTkxYY1ZuYnhNU2l5TXZ-fg',
        '208' => '2_MX40NjIyMDk1Mn5-MTYxNDg0ODYxOTczM35Edm1wempzUFFLUVg1bk1kdXRsSmMxNWd-fg',
        '209' => '1_MX40NjIyMDk1Mn5-MTYxNDg0ODYyMDA0Mn5qRHFZY3h0TnN5VmVqU0R4Ky9YK2hMUll-fg',
        '210' => '2_MX40NjIyMDk1Mn5-MTYxNDg0ODYyMDM1MX5CYVlGelA2UlFlZElvWlZVVHdKS1NVWHh-fg'
    ];

    private $_tokenTypes = [
        'main' => 199,
        'bob' => 200,
        'sbi' => 201,
        'hdfc' => 202,
        'avanse' => 203,
        'incred' => 204,
        'auxilo' => 205,
        'icici' => 206,
        'axis' => 207,
        'mpower' => 208,
        'prodigy' => 209,
        'us_bank' => 210
    ];

    private $_counselorTokens = [];

    private $_counselors = [
        'jenny_mednonca',
        'manasi_bandre',
        'rakesh_baleshwar',
        'zainab_b',
        'ranbir_matharoo',
        'aman_kaundal',
        'hiren_rathod',
        'harkiran_sawhney',
        'tanmay_kudyadi',
        'nik_admin',
        'sudhir_admin',
        'deepika_admin'
    ];

    private $_background_images = [
        'url' => 'https://www.hellouni.org/application/images/loan_mela/common.jpg'
    ];

    private $_counselor_background_images = [
        '200' => 'https://www.hellouni.org/application/images/loan_mela/bob.jpg',
        '201' => 'https://www.hellouni.org/application/images/loan_mela/sbi.jpg',
        '202' => 'https://www.hellouni.org/application/images/loan_mela/hdfc.jpg',
        '203' => 'https://www.hellouni.org/application/images/loan_mela/avanse.jpg',
        '204' => 'https://www.hellouni.org/application/images/loan_mela/incred.jpg',
        '205' => 'https://www.hellouni.org/application/images/loan_mela/auxilo.jpg',
        '206' => 'https://www.hellouni.org/application/images/loan_mela/icici.jpg',
        '207' => 'https://www.hellouni.org/application/images/loan_mela/axis.jpg',
        '208' => 'https://www.hellouni.org/application/images/loan_mela/mpower.jpg',
        '209' => 'https://www.hellouni.org/application/images/loan_mela/prodigy.jpg',
        '210' => 'https://www.hellouni.org/application/images/loan_mela/salliemae.jpg'
    ];

    private $_superAdmins = [
        'banit_sawhney',
        'akshay_dhumal'
    ];

    private $_panelists = [
        'shabaaz_shaikh',
        'nikhilesh_kada',
        'nikita_chokawala',
        'amit_premchandani',
        'amar_joshi',
        'ankit_agarwal',
        'vivek_singh',
        'swapnil_suvarna',
        'sagar_sura',
        'sakshi_kumari',
        'arbaaz_hashmi',
        'yuvish_singh',
        'neeraj_panwar',
        'prashant_singh',
        'gautam_kotak',
        'shrikant_baheti',
        'kartik_bhagat',
        'rahul_erdettin',
        'deepak_kumar'
    ];

    private $_panelistTokens = [
        'shabaaz_shaikh' => 200,
        'amit_premchandani' => 202,
        'amar_joshi' => 202,
        'ankit_agarwal' => 203,
        'vivek_singh' => 203,
        'swapnil_suvarna' => 205,
        'sagar_sura' => 205,
        'nikhilesh_kada' => 206,
        'nikita_chokawala' => 206,
        'arbaaz_hashmi' => 208,
        'yuvish_singh' => 208,
        'sakshi_kumari' => 209,
        'neeraj_panwar' => 207,
        'prashant_singh' => 207,
        'gautam_kotak' => 204,
        'shrikant_baheti' => 204,
        'kartik_bhagat' => 202,
        'rahul_erdettin' => 202
    ];

    private $_user_collection;
    private $_chat_collection;
    private $_ip_collection;
    private $_attender_collection;
    private $_connection_collection;
    private $_seminar_log_collection;
    private $_room_collection;
    private $_queue_collection;

    function __construct()
    {
        /*echo '<script type="text/javascript">alert("Loan mela has not been started yet. Please come back after some time."); window.location.href="/";</script>';
        exit;*/

        parent::__construct();
        $this->load->model(['seminar_model']);

        $mongo_server = 'localhost';
        $mongo_port = '27017';
        $mongo_client = 'mongodb://' . $mongo_server . ':' . $mongo_port;
        $client = new MongoDB\Client($mongo_client);
        $mongo_db = $client->HelloUni;

        $this->_user_collection = $mongo_db->selectCollection('users');
        $this->_chat_collection = $mongo_db->selectCollection('chats');
        $this->_ip_collection = $mongo_db->selectCollection('users_ips');
        $this->_attender_collection = $mongo_db->selectCollection('attenders');
        $this->_connection_collection = $mongo_db->selectCollection('connections');
        $this->_seminar_log_collection = $mongo_db->selectCollection('seminar_logs');
        $this->_room_collection = $mongo_db->selectCollection('rooms');
        $this->_queue_collection = $mongo_db->selectCollection('queue');
    }

    function organizer($sessionType, $tokenId, $username)
    {
        if(!$tokenId)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$sessionType)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if($sessionType == 'w')
        {
            $seminarLogDetail = $this->_seminar_log_collection->findOne();
            if($seminarLogDetail['current_session'] == 'ONE_ON_ONE')
            {
                redirect('/meeting/loanmela/organizer/o/' . $tokenId . '/' . $username);
            }
        }
        else
        {
            $seminarLogDetail = $this->_seminar_log_collection->findOne();
            if($seminarLogDetail['current_session'] != 'ONE_ON_ONE')
            {
                echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
                exit;
            }
        }

        $data['base64_token_id'] = $tokenId;
        $data['base64_username'] = $username;

        $tokenId = base64_decode($tokenId);
        $username = base64_decode($username);

        if($sessionType == 'o')
        {
            $this->_background_images['url'] = $this->_counselor_background_images[$tokenId];
        }

        if(!isset($this->_tokenSessionIds[$tokenId]))
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/";</script>';
            exit;
        }

        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        $userId = $userDetail['id'];

        $connectionMetaData = $username;
        $sessionId = $this->_tokenSessionIds[$tokenId];

        $mongoChatQuery['session_id'] = $sessionId;
        $chatCount = $this->_chat_collection->count($mongoChatQuery);
        $chats = [];
        if($chatCount)
        {
            $chatCursor = $this->_chat_collection->find($mongoChatQuery);
            foreach($chatCursor as $chat)
            {
                $chat['chat_of'] = 'mine';
                $chat['name'] = $username;
                if($chat['from'] != $userId)
                {
                    $chat['chat_of'] = 'theirs';

                    $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                    $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                    $chat['name'] = $chatUserDetail['username'];
                }
                if($chat['name'])
                {
                    $chats[] = $chat;
                }
            }
        }

        $this->seminar_model->deleteConnection($userId);
        $mongoConnectionQuery['user_id'] = (int)$userId;
        $this->_connection_collection->deleteMany($mongoConnectionQuery);

        $insertConnectionData['user_id'] = $userId;
        $insertConnectionData['tokbox_id'] = $tokenId;
        $insertConnectionData['username'] = $username;
        $insertConnectionData['type'] = 'Organizer';
        $this->seminar_model->insertConnection($insertConnectionData);

        $row['user_id'] = (int)$userId;
        $row['username'] = $username;
        $row['tokbox_id'] = (int)$tokenId;
        $row['session_id'] = $sessionId;
        $row['user_type'] = 'Organizer';
        $insertOneResult = $this->_connection_collection->insertOne($row);

        $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'moderator',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['sessionId'] = $sessionId;
        $data['username'] = $username;
        $data['userId'] = $userId;
        $data['tokboxId'] = $tokenId;
        $data['apiKey'] = $this->_tokboxId;
        $data['common_chat_history'] = $chats;
        $data['sessionType'] = $sessionType;
        $data['backgroundImage'] = $this->_background_images;

        $this->load->view('templates/meeting/organizer_loanmela', $data);
    }

    function panelist($sessionType, $tokenId, $username)
    {
        if(!$tokenId)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$sessionType)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if($sessionType == 'w')
        {
            $seminarLogDetail = $this->_seminar_log_collection->findOne();
            if($seminarLogDetail['current_session'] == 'ONE_ON_ONE')
            {
                redirect('/meeting/loanmela/panelist/o/' . $tokenId . '/' . $username);
            }
        }
        else
        {
            $seminarLogDetail = $this->_seminar_log_collection->findOne();
            if($seminarLogDetail['current_session'] != 'ONE_ON_ONE' && !$this->input->get('p'))
            {
                redirect('/meeting/loanmela/rooms/' . $username);
                exit;
            }
        }

        $data['base64_token_id'] = $tokenId;
        $data['base64_username'] = $username;

        $tokenId = base64_decode($tokenId);
        $username = base64_decode($username);

        if($sessionType == 'o')
        {
            if(!in_array($username, $this->_superAdmins) && !in_array($username, $this->_counselors))
            {
                $tokenId = $this->_panelistTokens[$username];
            }
            $this->_background_images['url'] = $this->_counselor_background_images[$tokenId];
        }

        if(!isset($this->_tokenSessionIds[$tokenId]))
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/";</script>';
            exit;
        }

        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        $userId = $userDetail['id'];

        $connectionMetaData = $username;
        $sessionId = $this->_tokenSessionIds[$tokenId];

        $mongoChatQuery['session_id'] = $sessionId;
        $chatCount = $this->_chat_collection->count($mongoChatQuery);
        $chats = [];
        if($chatCount)
        {
            $chatCursor = $this->_chat_collection->find($mongoChatQuery);
            foreach($chatCursor as $chat)
            {
                if($chat['from'] == $userId)
                {
                    $chat['chat_of'] = 'mine';
                    $chat['name'] = $username;
                }
                else if($chat['from_role'] == 'PUBLISHER')
                {
                    $chat['chat_of'] = 'theirs';

                    $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                    $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                    $chat['name'] = $chatUserDetail['username'];
                }
                if($chat['name'])
                {
                    $chats[] = $chat;
                }
            }
        }

        $this->seminar_model->deleteConnection($userId);

        $mongoConnectionQuery['user_id'] = (int)$userId;
        $this->_connection_collection->deleteMany($mongoConnectionQuery);

        if($sessionType == 'o')
        {
            $mongoRoomQuery['tokbox_id'] = (int)$tokenId;
            $roomPresent = $this->_room_collection->count($mongoRoomQuery);
            if($roomPresent)
            {
                $mongoConnectionCountQuery['tokbox_id'] = (int)$tokenId;
                $mongoConnectionCountQuery['type'] = 'Panelist';

                $panelistPresent = $this->_connection_collection->count($mongoConnectionCountQuery);
                if(!$panelistPresent)
                {
                    $roomData['status'] = 1;
                    $this->seminar_model->updateRoom($tokenId, $roomData);

                    $this->_room_collection->deleteMany($mongoRoomQuery);
                    $roomData['tokbox_id'] = (int)$tokenId;
                    $this->_room_collection->insertOne($roomData);
                }
            }
            else
            {
                $roomData['tokbox_id'] = (int)$tokenId;
                $this->seminar_model->addRoom($roomData);

                $roomData['status'] = 1;
                $this->_room_collection->insertOne($roomData);
            }
        }

        $insertConnectionData['user_id'] = $userId;
        $insertConnectionData['tokbox_id'] = $tokenId;
        $insertConnectionData['username'] = $username;
        $insertConnectionData['type'] = 'Panelist';
        $this->seminar_model->insertConnection($insertConnectionData);

        $row['user_id'] = (int)$userId;
        $row['username'] = $username;
        $row['tokbox_id'] = (int)$tokenId;
        $row['session_id'] = $sessionId;
        $row['type'] = 'Panelist';
        $insertOneResult = $this->_connection_collection->insertOne($row);

        $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'moderator',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['sessionId'] = $sessionId;
        $data['username'] = $username;
        $data['userId'] = $userId;
        $data['tokboxId'] = $tokenId;
        $data['apiKey'] = $this->_tokboxId;
        $data['common_chat_history'] = $chats;
        $data['sessionType'] = $sessionType;
        $data['backgroundImage'] = $this->_background_images;
        $data['allowedStudents'] = $this->_allowedStudents;

        $this->load->view('templates/meeting/panelist_loanmela', $data);
    }

    function student($sessionType, $tokenId, $username, $linkCount = 1)
    {
        $blockedStudents = [];

        /*if(!$this->input->get('rand') && !array_key_exists(base64_decode($username), $this->_counselors))
        {
            echo '<script type="text/javascript">alert("The session has not been started yet. Please come back after some time."); window.location.href="/";</script>';
            exit;
        }*/

        if(!$sessionType)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$tokenId)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if($sessionType == 'w')
        {
            $seminarLogDetail = $this->_seminar_log_collection->findOne();
            if($seminarLogDetail['current_session'] == 'ONE_ON_ONE')
            {
                redirect('/meeting/loanmela/rooms/' . $username);
            }
        }
        else
        {
            $seminarLogDetail = $this->_seminar_log_collection->findOne();
            if($seminarLogDetail['current_session'] != 'ONE_ON_ONE')
            {
                echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
                exit;
            }
        }

        $data['base64_token_id'] = $tokenId;
        $data['base64_username'] = $username;

        $tokenId = base64_decode($tokenId);
        $username = base64_decode($username);

        if(in_array($username, $blockedStudents))
        {
            echo '<script type="text/javascript">alert("Sorry! You are not authorized."); window.location.href="/";</script>';
            exit;
        }

        if(!isset($this->_tokenSessionIds[$tokenId]))
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/";</script>';
            exit;
        }
        else
        {
            $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        }
        $userId = $userDetail['id'];
        $linkAllowed = $userDetail['link_allowed'];
        if($linkCount > $linkAllowed)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }
        $ipUserId = $userId;
        if($linkCount != 1)
        {
            $ipUserId = $userId . '-' . $linkCount;
        }

        $userIpAddress = $this->get_client_ip();

        $mongoIpQuery['user_id'] = ['$eq' => $ipUserId];
        $userIpDetail = $this->_ip_collection->findOne($mongoIpQuery);
        if($userIpDetail)
        {
            $free_allow = ['nik_uni_1', 'nik_uni_2', 'nikhil', 'test_nik', 'nikhil_g', 'nik_edu_counselor'];

            $lastIp = $userIpDetail['ip_address'];
            $logout = $this->input->get('l');
            if($lastIp != $userIpAddress && !$logout && !in_array($username, $free_allow))
            {
                $data['errorMessage'] = 'You are logged in from another system. Do you want to logout from other system and continue here?<br/><br/><br/><a href="/meeting/loanmela/student/' . $sessionType . '/' . $data['base64_token_id'] . '/' . $data['base64_username'] . '?l=1"><button type="button">Yes</button></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="/meeting/loanmela/student/' . $sessionType . '/' . $data['base64_token_id'] . '/' . $data['base64_username'] . '?l=0"><button type="button">No</button></a>';
                $this->load->view('templates/meeting/seminar_error', $data);
                exit;
            }
            if($logout)
            {
                $this->seminar_model->deleteIp($ipUserId);

                $insertData['user_id'] = $ipUserId;
                $insertData['ip_address'] = $userIpAddress;
                $this->seminar_model->insertIp($insertData);

                $mongoIpQuery["user_id"] = $ipUserId;
                $deleteResult = $this->_ip_collection->deleteMany($mongoIpQuery);

                $this->_ip_collection->insertOne($insertData);
            }
        }
        else
        {
            $insertData['user_id'] = $ipUserId;
            $insertData['ip_address'] = $userIpAddress;
            $this->seminar_model->insertIp($insertData);

            $this->_ip_collection->insertOne($insertData);
        }

        $this->seminar_model->deleteConnection($userId);

        $mongoConnectionQuery['user_id'] = (int)$userId;
        $this->_connection_collection->deleteMany($mongoConnectionQuery);

        if($sessionType == 'o')
        {
            $mongoRoomQuery['tokbox_id'] = (int)$tokenId;
            $roomDetail = $this->_room_collection->findOne($mongoRoomQuery);

            if(!$roomDetail)
            {
                echo '<script type="text/javascript">alert("The room is unavailable. Please join any other room."); window.location.href="/meeting/loanmela/rooms/' . $data['base64_username'] . '";</script>';
                exit;
            }

            $roomStatus = $roomDetail['status'];
            if(!$roomStatus)
            {
                $mongoConnectionCountQuery['tokbox_id'] = (int)$tokenId;
                $mongoConnectionCountQuery['type'] = 'Student';

                $studentCount = $this->_connection_collection->count($mongoConnectionCountQuery);
                if($studentCount >= $this->_allowedStudents)
                {
                    echo '<script type="text/javascript">alert("Sorry! The room is full. Please join any other room."); window.location.href="/meeting/loanmela/rooms/' . $data['base64_username'] . '";</script>';
                    exit;
                }
            }
        }

        $insertConnectionData['user_id'] = $userId;
        $insertConnectionData['tokbox_id'] = $tokenId;
        $insertConnectionData['username'] = $username;
        $insertConnectionData['type'] = 'Student';
        $this->seminar_model->insertConnection($insertConnectionData);

        $connectionMetaData = $username;
        $sessionId = $this->_tokenSessionIds[$tokenId];

        $row['user_id'] = (int)$userId;
        $row['username'] = $username;
        $row['tokbox_id'] = (int)$tokenId;
        $row['session_id'] = $sessionId;
        $row['type'] = 'Student';
        $insertOneResult = $this->_connection_collection->insertOne($row);

        $this->seminar_model->deleteQueue($userId);
        $mongoQueueQuery['user_id'] = (int)$userId;
        $this->_queue_collection->deleteMany($mongoQueueQuery);

        $mongoAttenderQuery['user_id'] = $userId;
        //$deleteResult = $this->_attender_collection->deleteMany($mongoAttenderQuery);

        $row['user_id'] = $userId;
        $row['username'] = $username;
        $row['tokbox_id'] = $tokenId;
        $row['session_id'] = $sessionId;
        $row['sessionType'] = $sessionType == 'o' ? 'ONE_ON_ONE' : 'WEBINAR';
        $insertOneResult = $this->_attender_collection->insertOne($row);

        $mongoChatQuery['session_id'] = $sessionId;
        $chatCount = $this->_chat_collection->count($mongoChatQuery);
        $chats = [];
        if($chatCount)
        {
            $chatCursor = $this->_chat_collection->find($mongoChatQuery);
            foreach($chatCursor as $chat)
            {
                if($chat['from'] == $userId)
                {
                    $chat['chat_of'] = 'mine';
                    $chat['name'] = $username;
                }
                else if($chat['from_role'] == 'PUBLISHER')
                {
                    $chat['chat_of'] = 'theirs';

                    $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                    $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                    $chat['name'] = $chatUserDetail['username'];
                }
                if($chat['name'])
                {
                    $chats[] = $chat;
                }
            }
        }

        if($sessionType == 'o')
        {
            $this->_background_images['url'] = $this->_counselor_background_images[$tokenId];
        }

        $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => $sessionType == 'o' ? 'publisher' : 'subscriber',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['sessionId'] = $sessionId;
        $data['userId'] = $userId;
        $data['username'] = $username;
        $data['tokboxId'] = $tokenId;
        $data['apiKey'] = $this->_tokboxId;
        $data['linkCount'] = $linkCount;
        $data['common_chat_history'] = $chats;
        $data['backgroundImage'] = $this->_background_images;

        if($sessionType == 'o')
        {
            $this->load->view('templates/meeting/student_loanmela', $data);
        }
        else
        {
            $this->load->view('templates/meeting/student_attendee_loanmela', $data);
        }
    }

    function counsellor($sessionType, $tokenId, $username)
    {
        if(!$tokenId)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$sessionType)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if($sessionType == 'w')
        {
            $seminarLogDetail = $this->_seminar_log_collection->findOne();
            if($seminarLogDetail['current_session'] == 'ONE_ON_ONE')
            {
                redirect('/meeting/loanmela/organizer/o/' . $tokenId . '/' . $username);
            }
        }
        else
        {
            $seminarLogDetail = $this->_seminar_log_collection->findOne();
            if($seminarLogDetail['current_session'] != 'ONE_ON_ONE')
            {
                echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
                exit;
            }
        }

        $data['base64_token_id'] = $tokenId;
        $data['base64_username'] = $username;

        $tokenId = base64_decode($tokenId);
        $username = base64_decode($username);

        if(!isset($this->_tokenSessionIds[$tokenId]))
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/";</script>';
            exit;
        }

        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        $userId = $userDetail['id'];

        $connectionMetaData = $username;
        $sessionId = $this->_tokenSessionIds[$tokenId];

        $mongoChatQuery['session_id'] = $sessionId;
        $chatCount = $this->_chat_collection->count($mongoChatQuery);
        $chats = [];
        if($chatCount)
        {
            $chatCursor = $this->_chat_collection->find($mongoChatQuery);
            foreach($chatCursor as $chat)
            {
                $chat['chat_of'] = 'mine';
                $chat['name'] = $username;
                if($chat['from'] != $userId)
                {
                    $chat['chat_of'] = 'theirs';

                    $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                    $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                    $chat['name'] = $chatUserDetail['username'];
                }
                if($chat['name'])
                {
                    $chats[] = $chat;
                }
            }
        }

        $this->seminar_model->deleteConnection($userId);

        $insertConnectionData['user_id'] = $userId;
        $insertConnectionData['tokbox_id'] = $tokenId;
        $insertConnectionData['username'] = $username;
        $insertConnectionData['type'] = 'Counsellor';
        $this->seminar_model->insertConnection($insertConnectionData);

        $mongoConnectionQuery['user_id'] = (int)$userId;
        $this->_connection_collection->deleteMany($mongoConnectionQuery);

        $row['user_id'] = (int)$userId;
        $row['username'] = $username;
        $row['tokbox_id'] = (int)$tokenId;
        $row['session_id'] = $sessionId;
        $row['type'] = 'Counsellor';
        $insertOneResult = $this->_connection_collection->insertOne($row);

        $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'subscriber',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['sessionId'] = $sessionId;
        $data['username'] = $username;
        $data['userId'] = $userId;
        $data['tokboxId'] = $tokenId;
        $data['apiKey'] = $this->_tokboxId;
        $data['common_chat_history'] = $chats;
        $data['sessionType'] = $sessionType;
        $data['backgroundImage'] = $this->_background_images;

        $this->load->view('templates/meeting/counsellor_attendee_loanmela', $data);
    }

    function panel($sessionType, $tokenId, $username)
    {
        if(!$tokenId)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$sessionType)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if($sessionType == 'w')
        {
            $seminarLogDetail = $this->_seminar_log_collection->findOne();
            if($seminarLogDetail['current_session'] == 'ONE_ON_ONE')
            {
                redirect('/meeting/loanmela/panelist/o/' . $tokenId . '/' . $username);
            }
        }
        else
        {
            $seminarLogDetail = $this->_seminar_log_collection->findOne();
            if($seminarLogDetail['current_session'] != 'ONE_ON_ONE')
            {
                echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
                exit;
            }
        }

        $data['base64_token_id'] = $tokenId;
        $data['base64_username'] = $username;

        $tokenId = base64_decode($tokenId);
        $username = base64_decode($username);

        if(!isset($this->_tokenSessionIds[$tokenId]))
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/";</script>';
            exit;
        }

        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        $userId = $userDetail['id'];

        $connectionMetaData = $username;
        $sessionId = $this->_tokenSessionIds[$tokenId];

        $mongoChatQuery['session_id'] = $sessionId;
        $chatCount = $this->_chat_collection->count($mongoChatQuery);
        $chats = [];
        if($chatCount)
        {
            $chatCursor = $this->_chat_collection->find($mongoChatQuery);
            foreach($chatCursor as $chat)
            {
                if($chat['from'] == $userId)
                {
                    $chat['chat_of'] = 'mine';
                    $chat['name'] = $username;
                }
                else if($chat['from_role'] == 'PUBLISHER')
                {
                    $chat['chat_of'] = 'theirs';

                    $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                    $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                    $chat['name'] = $chatUserDetail['username'];
                }
                if($chat['name'])
                {
                    $chats[] = $chat;
                }
            }
        }

        $this->seminar_model->deleteConnection($userId);

        $insertConnectionData['user_id'] = $userId;
        $insertConnectionData['tokbox_id'] = $tokenId;
        $insertConnectionData['username'] = $username;
        $insertConnectionData['type'] = 'Panel';
        $this->seminar_model->insertConnection($insertConnectionData);

        $mongoConnectionQuery['user_id'] = (int)$userId;
        $this->_connection_collection->deleteMany($mongoConnectionQuery);

        $row['user_id'] = (int)$userId;
        $row['username'] = $username;
        $row['tokbox_id'] = (int)$tokenId;
        $row['session_id'] = $sessionId;
        $row['type'] = 'Panel';
        $insertOneResult = $this->_connection_collection->insertOne($row);

        $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'subscriber',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['sessionId'] = $sessionId;
        $data['username'] = $username;
        $data['userId'] = $userId;
        $data['tokboxId'] = $tokenId;
        $data['apiKey'] = $this->_tokboxId;
        $data['common_chat_history'] = $chats;
        $data['sessionType'] = $sessionType;
        $data['backgroundImage'] = $this->_background_images;

        $this->load->view('templates/meeting/panelist_attendee_loanmela', $data);
    }

    function rooms($username, $linkCount = 1)
    {
        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $blockedStudents = [];

        $decodedUsername = base64_decode($username);

        if(in_array($decodedUsername, $blockedStudents))
        {
            echo '<script type="text/javascript">alert("Sorry! You are not authorized."); window.location.href="/";</script>';
            exit;
        }

        $decodedUsername = iconv('UTF-8', 'UTF-8//IGNORE', $decodedUsername);
        $mongoUserQuery['username'] = ['$eq' => $decodedUsername];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/";</script>';
            exit;
        }

        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        $userId = $userDetail['id'];
        $this->seminar_model->deleteConnection($userId);

        $mongoConnectionQuery['user_id'] = (int)$userId;
        $this->_connection_collection->deleteMany($mongoConnectionQuery);

        $seminarLogDetail = $this->_seminar_log_collection->findOne();
        if($seminarLogDetail['current_session'] == 'ONE_ON_ONE')
        {
            if(in_array($decodedUsername, $this->_counselors))
            {
                /*$tokenId = $this->_counselorTokens[$decodedUsername];
                $tokenId = base64_encode($tokenId);
                $tokenId = str_replace("=", "", $tokenId);
                redirect('/meeting/loanmela/organizer/o/' . $tokenId . '/' . $username);*/
                redirect('/admin/meeting/seminar/index/' . $username);
            }
            else if(in_array($decodedUsername, $this->_panelists))
            {
                $tokenId = $this->_panelistTokens[$decodedUsername];
                $tokenId = base64_encode($tokenId);
                $tokenId = str_replace("=", "", $tokenId);
                redirect('/meeting/loanmela/panelist/o/' . $tokenId . '/' . $username);
            }
        }
        else
        {
            if(in_array($decodedUsername, $this->_counselors))
            {
                redirect('/meeting/loanmela/counsellor/w/MTk5/' . $username);
            }
            else if(in_array($decodedUsername, $this->_panelists))
            {
                redirect('/meeting/loanmela/panel/w/MTk5/' . $username);
            }
            else if(in_array($decodedUsername, $this->_superAdmins))
            {
                redirect('/meeting/loanmela/organizer/w/MTk5/' . $username);
            }
            redirect('/meeting/loanmela/student/w/MTk5/' . $username);
        }

        $userType = 'student';
        $webinarUserType = 'student';
        if(in_array($decodedUsername, $this->_superAdmins))
        {
            $userType = 'organizer';
            $webinarUserType = 'organizer';
        }
        $this->outputData['username'] = $username;
        $this->outputData['linkCount'] = $linkCount;
        $this->outputData['userType'] = $userType;
        $this->outputData['webinarUserType'] = $webinarUserType;
        $this->render_page('templates/meeting/rooms_loanmela', $this->outputData);
    }

    function check_user($userId, $linkCount = 1)
    {
        $userIpAddress = $this->get_client_ip();
        if($linkCount != 1)
        {
            $userId = $userId  . '-' . $linkCount;
        }

        $mongoIpQuery['user_id'] = ['$eq' => $userId];
        $userIpDetail = $this->_ip_collection->findOne($mongoIpQuery);
        if($userIpDetail)
        {
            $lastIp = $userIpDetail['ip_address'];
            if($lastIp != $userIpAddress)
            {
                echo 0;
                exit;
            }
        }
        echo 1;
    }

    function error()
    {
        $data['errorMessage'] = "You just logged in from another system. You've been logged out from this system. Please contact to support.";
        $this->load->view('templates/meeting/seminar_error', $data);
    }

    function delete_connection($username)
    {
        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            return;
        }

        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        $userId = $userDetail['id'];

        $this->seminar_model->deleteConnection($userId);

        $mongoQuery["username"] = $username;
        $deleteResult = $this->_connection_collection->deleteMany($mongoQuery);
    }

    function add_connection($username, $tokenId, $sessionType)
    {
        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            return;
        }

        $userType = 'Student';
        if(in_array($username, $this->_superAdmins) || in_array($username, $this->_counselors))
        {
            $userType = 'Organizer';
        }
        else if(in_array($username, $this->_panelists))
        {
            $userType = 'Panelist';
        }

        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        $userId = $userDetail['id'];

        $this->seminar_model->deleteConnection($userId);

        $mongoQuery["username"] = $username;
        $deleteResult = $this->_connection_collection->deleteMany($mongoQuery);

        $sessionId = $this->_tokenSessionIds[$tokenId];

        $insertConnectionData['user_id'] = $userId;
        $insertConnectionData['tokbox_id'] = $tokenId;
        $insertConnectionData['username'] = $username;
        $insertConnectionData['type'] = $userType;
        $this->seminar_model->insertConnection($insertConnectionData);

        $row['user_id'] = (int)$userId;
        $row['username'] = $username;
        $row['tokbox_id'] = (int)$tokenId;
        $row['session_id'] = $sessionId;
        $row['type'] = $userType;
        $insertOneResult = $this->_connection_collection->insertOne($row);
    }

    function get_connections($tokenId)
    {
        $connections = [];
        $mongoConnectionQuery['tokbox_id'] = (int)$tokenId;
        $connectionCursor = $this->_connection_collection->find($mongoConnectionQuery);
        foreach($connectionCursor as $connection)
        {
            $connections[] = $connection;
        }

        echo json_encode($connections);
    }

    function get_connection_count($tokenId)
    {
        $connections = [];
        $mongoConnectionQuery['tokbox_id'] = (int)$tokenId;
        $mongoConnectionQuery['type'] = 'Student';
        $connectionCount = $this->_connection_collection->count($mongoConnectionQuery);

        echo $connectionCount;
    }

    function chat()
    {
        $post_data = json_decode(file_get_contents("php://input"), true);

        $data['from'] = $post_data['user_id'];
        $data['to'] = -1;
        $data['to_user_type'] = NULL;
        $data['session_id'] = $post_data['sessionId'];
        $data['tokbox_id'] = $post_data['tokboxId'];
        $data['user_type'] = 'USER';
        $data['message'] = $post_data['msg'];
        $data['from_role'] = isset($post_data['from_role']) ? $post_data['from_role'] : NULL;
        $data['chat_id'] = isset($post_data['chat_id']) ? $post_data['chat_id'] : NULL;


        $this->seminar_model->storeChat($data);

        $insertOneResult = $this->_chat_collection->insertOne($data);
    }

    function check_webinar()
    {
        $seminarLogDetail = $this->_seminar_log_collection->findOne();
        if($seminarLogDetail['current_session'] == 'ONE_ON_ONE')
        {
            echo 1;
            exit;
        }
        echo 0;
    }

    function thankyou()
    {
        $this->load->view('templates/meeting/seminar_thankyou');
    }

    function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        return $ipaddress;
    }

    function end_webinar()
    {
        $this->_seminar_log_collection->deleteMany([]);
        $row['current_session'] = 'ONE_ON_ONE';
        $this->seminar_model->updateSeminarLog($row);
        $this->_seminar_log_collection->insertOne($row);
    }

    function check_rooms($username)
    {
        $decodedUsername = base64_decode($username);
        $decodedUsername = iconv('UTF-8', 'UTF-8//IGNORE', $decodedUsername);
        $mongoUserQuery['username'] = ['$eq' => $decodedUsername];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            echo json_encode([]);
            exit;
        }

        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        $userId = $userDetail['id'];
        $allRooms = [];

        for($i = 200; $i <= 210; $i++)
        {
            $base64TokboxId = base64_encode($i);
			$base64TokboxId = str_replace("=", "", $base64TokboxId);

            $room['tokbox_id'] = $i;
            $room['available'] = 1;
            $room['in_queue'] = 0;
            $room['en_tokbox_id'] = $base64TokboxId;

            /*$mongoRoomQuery['tokbox_id'] = (int)$i;
            $roomDetail = $this->_room_collection->findOne($mongoRoomQuery);
            if($roomDetail)
            {
                $roomStatus = $roomDetail['status'];
                if($roomStatus)
                {
                    $mongoConnectionQuery['tokbox_id'] = (int)$i;
                    $mongoConnectionQuery['type'] = 'Student';
                    $connectionCount = $this->_connection_collection->count($mongoConnectionQuery);

                    if($connectionCount < $this->_allowedStudents)
                    {
                        $mongoQueueQuery['tokbox_id'] = (int)$i;
                        $queueCount = $this->_queue_collection->count($mongoQueueQuery);
                        if($queueCount)
                        {
                            $queueCursor = $this->_queue_collection->find($mongoQueueQuery);
                            $qCount = 0;
                            foreach($queueCursor as $queue)
                            {
                                if($queue['user_id'] == $userId && ($qCount + $connectionCount) < $this->_allowedStudents)
                                {
                                    $room['available'] = 1;
                                    $room['in_queue'] = 1;
                                    break;
                                }
                                $qCount++;
                            }
                            if(!$room['in_queue'] && ($queueCount + $connectionCount) < $this->_allowedStudents)
                            {
                                $room['available'] = 1;
                            }
                        }
                        else
                        {
                            $room['available'] = 1;
                        }
                    }
                }
            }*/

            $allRooms[] = $room;
        }

        echo json_encode($allRooms);
    }

    function lock_room($tokenId)
    {
        $mongoRoomQuery['tokbox_id'] = (int)$tokenId;

        $roomData['status'] = 0;
        $this->seminar_model->updateRoom($tokenId, $roomData);

        $this->_room_collection->deleteMany($mongoRoomQuery);

        $roomData['tokbox_id'] = (int)$tokenId;
        $this->_room_collection->insertOne($roomData);
    }

    function unlock_room($tokenId)
    {
        $mongoRoomQuery['tokbox_id'] = (int)$tokenId;

        $roomData['status'] = 1;
        $this->seminar_model->updateRoom($tokenId, $roomData);

        $this->_room_collection->deleteMany($mongoRoomQuery);

        $roomData['tokbox_id'] = (int)$tokenId;
        $this->_room_collection->insertOne($roomData);
    }

    function check_room_status($tokenId)
    {
        $mongoRoomQuery['tokbox_id'] = (int)$tokenId;
        $roomDetail = $this->_room_collection->findOne($mongoRoomQuery);
        echo $roomDetail['status'];
    }

    function join_queue($username, $tokenId)
    {
        $username = base64_decode($username);
        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            echo 0;
            exit;
        }

        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        $userId = $userDetail['id'];

        $this->seminar_model->deleteQueue($userId);

        $mongoQueueQuery['user_id'] = (int)$userId;
        $this->_queue_collection->deleteMany($mongoQueueQuery);

        $tokenId = base64_decode($tokenId);

        $data['user_id'] = (int)$userId;
        $data['tokbox_id'] = (int)$tokenId;
        $this->seminar_model->addQueue($data);

        $this->_queue_collection->insertOne($data);

        echo 1;
    }

    function delete_queue($username)
    {
        $username = base64_decode($username);
        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            echo 0;
            exit;
        }

        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        $userId = $userDetail['id'];

        $this->seminar_model->deleteQueue($userId);

        $mongoQueueQuery['user_id'] = (int)$userId;
        $this->_queue_collection->deleteMany($mongoQueueQuery);

        echo 1;
    }

    function register()
    {
        $this->render_page('templates/meeting/register_student', $this->outputData);
    }

    function register_user()
    {
        if(!$this->input->post('user_email') || !$this->input->post('user_mobile') || !$this->input->post('user_name'))
        {
            echo "All fields are mandatory";
            exit;
        }
        $userEmailId = $this->input->post('user_email');
        $userMobileNumber = $this->input->post('user_mobile');

        $userDetail = $this->seminar_model->validUserInfo($userEmailId, $userMobileNumber);

        if($userDetail)
        {
            $userName = $userDetail[0]['username'];

            $usernamecode = base64_encode($userName);
            $usernamecode = str_replace("=", "", $usernamecode);

            echo "https://hellouni.org/meeting/loanmela/rooms/" . $usernamecode;
            exit();
        }
        else
        {
            $userData['name'] = $this->input->post('user_name');
            $userData['email'] = $this->input->post('user_email');
            $userData['phone'] = $this->input->post('user_mobile');

            $userData['type'] = 5;
            $userData['username'] = strtolower(str_replace(" ", "_", $userData['name']));
            $userData['password'] = md5($userData['username'] . "@123");

            $userExist = $this->seminar_model->checkUsername($userData['username']);
            if(!$userExist)
            {
                $this->seminar_model->addUser($userData);
            }
            $usernamecode = base64_encode($userData['username']);
            $usernamecode = str_replace("=", "", $usernamecode);

            echo "https://hellouni.org/meeting/loanmela/rooms/" . $usernamecode;
            exit();
        }
    }

    function common()
    {
        if($this->input->post())
        {
            $email = $this->input->post('email');
            $userDetail = $this->seminar_model->validUserInfo($email, "NAA");
            if(!$userDetail)
            {
                echo '<script type="text/javascript">alert("Sorry! Wrong email. Use the same email with which you registered."); window.location.href="/";</script>';
                exit;
            }

            $userName = $userDetail[0]['username'];

            $usernamecode = base64_encode($userName);
            $usernamecode = str_replace("=", "", $usernamecode);

            redirect("https://hellouni.org/meeting/loanmela/rooms/" . $usernamecode);
        }
        $this->load->view('templates/meeting/loanmela_common', $data);
    }
}
