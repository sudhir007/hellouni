<?php

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

require_once APPPATH . 'libraries/Mail/sMail.php';

require APPPATH . "vendor/autoload.php";
include_once APPPATH . 'mongo/autoload.php';

use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;

class Visaseminar extends MY_Controller
{
    private $_tokboxId = '46220952';
    private $_tokboxSecret = '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2';

    private $_tokenSessionIds = [
        '157' => '1_MX40NjIyMDk1Mn5-MTYxMDM2MjYxOTYyNn5JRkpyK3FJcEY3MEVwTFMvTVRaQjhKVk5-fg',
        '158' => '1_MX40NjIyMDk1Mn5-MTYxMDM2Mjk3NjI4MX40VS9RYXF2T1RHTUlWZXZOMjhaSWRCKy9-fg',
        '159' => '1_MX40NjIyMDk1Mn5-MTYxMDM2Mjk3NjYxNn5kL01BbWw5WHRZV3VWaG5xWTI2Ylh5MlR-fg',
        '160' => '2_MX40NjIyMDk1Mn5-MTYxMDM2Mjk3NjkyMn5CSHJpN2FJTzVqbENNS0lYWXA1b2k1V0p-fg',
        '161' => '1_MX40NjIyMDk1Mn5-MTYxMDM2Mjk3NzIyNn5yZER5R3V0eGR4eHNPeGp3bm9rYTM2SFV-fg',
        '162' => '1_MX40NjIyMDk1Mn5-MTYxMDM2Mjk3NzUzMn4yWmJUc09RK2VlQ0RhNzFVOTU3V1ExZzN-fg',
        '163' => '1_MX40NjIyMDk1Mn5-MTYxMDM2Mjk3Nzg0MX56WDB4amorN3N5K0JGeUVKNEVWK0x5cjd-fg',
        '164' => '1_MX40NjIyMDk1Mn5-MTYxMTEyNjU3MDAwMn41a0R3SGpFNEVmemdzR3loV1NwaGRvNHF-fg',
        '165' => '1_MX40NjIyMDk1Mn5-MTYxMTEyNjU3MDM0M343UTJYaDZDZXJPSUxjWWZBWi9VZlV6NlR-fg',
        '166' => '1_MX40NjIyMDk1Mn5-MTYxMTEyNjU3MDYxN35EYjZWMjM3RS9ISDFCRzhzSXhnUTZyRUR-fg',
        '167' => '1_MX40NjIyMDk1Mn5-MTYxMTEyNjU3MDk1MH56b083M1hvd3pNWUJNNWlxNWpqSXdmckh-fg',
        '168' => '2_MX40NjIyMDk1Mn5-MTYxMTEyNjU3MTIyM35jLzE3Y1FDUXgrNXZVQ3Y1RVNzSXFqb3Z-fg',
        '169' => '1_MX40NjIyMDk1Mn5-MTYxMTEyNjU3MTQ4OH5qS292WWhRdmR0WHliczIrdFJjZEIwcnV-fg',
        '170' => '2_MX40NjIyMDk1Mn5-MTYxMTEyNjU3MTg4MX5MVzJHY2lqVmNBbVhVZHRSU1hoWUVxL0Z-fg',
        '171' => '2_MX40NjIyMDk1Mn5-MTYxMTEyNjU3MjE3OH54UGxYcUltdGtoM2kvWXowa21Pc3JQK0R-fg',
        '172' => '2_MX40NjIyMDk1Mn5-MTYxMTEyNjU3MjQ5Mn5QV3NDclIvTWxCZEVpNjJyclU5OWdUcWZ-fg',
        '173' => '2_MX40NjIyMDk1Mn5-MTYxMTEyNjU3Mjc5M35kN0xrcVVuM0x5Vk9ZTjA1VEVSZWJSWE5-fg',
        '174' => '2_MX40NjIyMDk1Mn5-MTYxMTEyNjU3MzA2M35LK1pabmxWY1NsclJDWm1sNkwrZis1UjN-fg',
        '175' => '1_MX40NjIyMDk1Mn5-MTYxMTEyNjU3MzQxM342aHBya3ZuSXIwbC9sdTNLUzAyQkw2enB-fg',
        '176' => '2_MX40NjIyMDk1Mn5-MTYxMTc0ODQ3NzQwNH5ndTlYcHk1VkFCUGw1bXdHN1djWTBVaE5-fg',
        '177' => '1_MX40NjIyMDk1Mn5-MTYxMTc0ODQ3NzgyMH52L2FkaUdXZzJKZkRTMFBkeHZsWlZpNGZ-fg'
    ];

    private $_tokenTypes = [
        'main' => 157,
        'usa' => [158, 159, 160, 161, 162, 163],
        'canada' => 164,
        'europe' => 165,
        'australia' => 166,
        'thane' => [167, 168],
        'borivalli' => [169, 170],
        'santacruz' => [171, 172],
        'pune' => 173,
        'unimoni' => 174,
        'frr' => 175,
        'axis' => 176,
        'thomas_cook' => 177
    ];

    private $_allRooms = [
        'counselling' => [158, 159, 160, 161, 162, 163, 164, 165, 166],
        'education_loan_counselling' => [167, 168, 169, 170, 171, 172, 173],
        'other' => [174, 175, 176, 177]
    ];

    private $_counselors = [
        'srivathsa_vaidya' => 158,
        'kavita_m' => 158,
        'rahul_bhat' => 159,
        'pavni_gaurangi' => 159,
        'priyanka_more' => 159,
        'yamnesh_patel' => 160,
        'amruta_udawant' => 160,
        'sanjana_sanzgiri' => 161,
        'anil_kulkarni' => 161,
        'saloni_magre' => 162,
        'priyanka_mirkar' => 162,
        'mercy_selvam' => 162,
        'komal_mirkar' => 163,
        'alisha_chhabra' => 163,
        'shagufta_sayyed' => 163,
        'sonali_palkar' => 164,
        'sanchan_jaiswal' => 164,
        'manasi_rayanade' => 164,
        'akshay_bhandari' => 165,
        'sanhita_bhattacharya' => 165,
        'ranjana_singh' => 166,
        'vaidehi_purohit' => 166,
        'shiji_george' => 166,
        'ranbir_matharoo' => 167,
        'akshay_dhumal' => 168,
        'jenny_mednonca' => 169,
        'zainab_b' => 170,
        'aman_kaundal' => 171,
        'manasi_bandre' => 172,
        'rakesh_baleshwar' => 173,
        'unimoni' => 174,
        'frr_forex' => 175,
        'axis_bank' => 176,
        'thomas_cook' => 177
    ];

    private $_background_images = [
        'url' => 'https://www.hellouni.org/application/images/visa_seminar/usa_common.jpg',
        'usa_url' => 'https://www.hellouni.org/application/images/visa_seminar/usa_common.jpg',
        'canada_url' => 'https://www.hellouni.org/application/images/visa_seminar/canada_common.jpg',
        'europe_url' => 'https://www.hellouni.org/application/images/visa_seminar/europe_common.jpg',
        'eduloan_url' => 'https://www.hellouni.org/application/images/visa_seminar/education_loan_common.jpg',
        'sponsor_url' => 'https://www.hellouni.org/application/images/visa_seminar/sponsor.jpg'
    ];

    private $_counselor_background_images = [
        '158' => 'https://www.hellouni.org/application/images/visa_seminar/usa_room_1.jpg',
        '159' => 'https://www.hellouni.org/application/images/visa_seminar/usa_room_2.jpg',
        '160' => 'https://www.hellouni.org/application/images/visa_seminar/usa_room_3.jpg',
        '161' => 'https://www.hellouni.org/application/images/visa_seminar/usa_room_4.jpg',
        '162' => 'https://www.hellouni.org/application/images/visa_seminar/usa_room_5.jpg',
        '163' => 'https://www.hellouni.org/application/images/visa_seminar/usa_room_6.jpg',
        '164' => 'https://www.hellouni.org/application/images/visa_seminar/canada_room.jpg',
        '165' => 'https://www.hellouni.org/application/images/visa_seminar/europe_room.jpg',
        '166' => 'https://www.hellouni.org/application/images/visa_seminar/aus_room.jpg',
        '167' => 'https://www.hellouni.org/application/images/visa_seminar/education_loan_ranbir_singh.jpg',
        '168' => 'https://www.hellouni.org/application/images/visa_seminar/education_loan_akshay_dhumal.jpg',
        '169' => 'https://www.hellouni.org/application/images/visa_seminar/education_loan_jenny_mednonca.jpg',
        '170' => 'https://www.hellouni.org/application/images/visa_seminar/education_loan_zainab_b.jpg',
        '171' => 'https://www.hellouni.org/application/images/visa_seminar/education_loan_aman_kaundal.jpg',
        '172' => 'https://www.hellouni.org/application/images/visa_seminar/education_loan_manasi_bandre.jpg',
        '173' => 'https://www.hellouni.org/application/images/visa_seminar/education_loan_rakesh_baleshwar.jpg',
        '174' => 'https://www.hellouni.org/application/images/visa_seminar/forex_partner_unimoni.jpg',
        '175' => 'https://www.hellouni.org/application/images/visa_seminar/sponsor.jpg',
        '176' => 'https://www.hellouni.org/application/images/visa_seminar/forex_partner_axis.jpg',
        '177' => 'https://www.hellouni.org/application/images/visa_seminar/forex_partner_thomas_cook.jpg'
    ];

    private $_superAdmins = [
        'deepika_admin',
        'banit_sawhney',
        'hiren_rathod',
        'harkiran_sawhney',
        'nik_admin',
        'tanmay_kudyadi',
        'sudhir_admin'
    ];

    private $_user_collection;
    private $_chat_collection;
    private $_ip_collection;
    private $_attender_collection;
    private $_connection_collection;
    //private $_token_collection;

    function __construct()
    {
        /*echo '<script type="text/javascript">alert("The session has not been started yet. Please come back after some time."); window.location.href="/";</script>';
        exit;*/

        parent::__construct();
        $this->load->model(['visaseminar_model']);

        $mongo_server = 'localhost';
        $mongo_port = '27017';
        $mongo_client = 'mongodb://' . $mongo_server . ':' . $mongo_port;
        $client = new MongoDB\Client($mongo_client);
        $mongo_db = $client->HelloUni;

        $this->_user_collection = $mongo_db->selectCollection('users');
        $this->_chat_collection = $mongo_db->selectCollection('chats');
        $this->_ip_collection = $mongo_db->selectCollection('users_ips');
        $this->_attender_collection = $mongo_db->selectCollection('attenders');
        $this->_connection_collection = $mongo_db->selectCollection('connections');
        $this->_temp_collection = $mongo_db->selectCollection('temporary');

        /*$this->_token_collection = $mongo_db->selectCollection('tokens');

        $tokenCursor = $this->_token_collection->find();
        foreach($tokenCursor as $token)
        {
            $this->_tokenSessionIds[$token['id']] = $token['session_id'];
        }*/
    }

    function organizer($sessionType, $tokenId, $username)
    {
        if(!$tokenId)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$sessionType)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if($sessionType == 'w')
        {
            $isWebinarDone = get_cookie('webinar_completed');
            if($isWebinarDone)
            {
                redirect('/meeting/visaseminar/organizer/o/' . $tokenId . '/' . $username);
            }

            $mongoConnectionQuery['type'] = 'ONE_ON_ONE';
            $connectionCount = $this->_connection_collection->count($mongoConnectionQuery);
            if($connectionCount)
            {
                redirect('/meeting/visaseminar/organizer/o/' . $tokenId . '/' . $username);
            }
        }

        $data['base64_token_id'] = $tokenId;
        $data['base64_username'] = $username;

        $tokenId = base64_decode($tokenId);
        $username = base64_decode($username);

        if($sessionType == 'o')
        {
            if(!in_array($username, $this->_superAdmins))
            {
                $tokenId = $this->_counselors[$username];
            }
            $this->_background_images['url'] = $this->_counselor_background_images[$tokenId];
        }

        if(!isset($this->_tokenSessionIds[$tokenId]))
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/";</script>';
            exit;
        }

        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        $userId = $userDetail['id'];

        $connectionMetaData = $username;
        $sessionId = $this->_tokenSessionIds[$tokenId];

        $mongoChatQuery['session_id'] = $sessionId;
        $chatCount = $this->_chat_collection->count($mongoChatQuery);
        $chats = [];
        if($chatCount)
        {
            $chatCursor = $this->_chat_collection->find($mongoChatQuery);
            foreach($chatCursor as $chat)
            {
                $chat['chat_of'] = 'mine';
                $chat['name'] = $username;
                if($chat['from'] != $userId)
                {
                    $chat['chat_of'] = 'theirs';

                    $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                    $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                    $chat['name'] = $chatUserDetail['username'];
                }
                if($chat['name'])
                {
                    $chats[] = $chat;
                }
            }
        }

        $connectionType = $sessionType == 'w' ? 'WEBINAR' : 'ONE_ON_ONE';
        $this->visaseminar_model->deleteConnection($userId, $connectionType);

        $insertConnectionData['user_id'] = $userId;
        $insertConnectionData['tokbox_id'] = $tokenId;
        $insertConnectionData['username'] = $username;
        $insertConnectionData['type'] = $connectionType;
        $this->visaseminar_model->insertConnection($insertConnectionData);

        $row['user_id'] = $userId;
        $row['username'] = $username;
        $row['tokbox_id'] = $tokenId;
        $row['session_id'] = $sessionId;
        $row['type'] = $connectionType;
        //$insertOneResult = $this->_connection_collection->insertOne($row);

        $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'moderator',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['sessionId'] = $sessionId;
        $data['username'] = $username;
        $data['userId'] = $userId;
        $data['tokboxId'] = $tokenId;
        $data['apiKey'] = $this->_tokboxId;
        $data['common_chat_history'] = $chats;
        $data['sessionType'] = $sessionType;
        $data['backgroundImage'] = $this->_background_images;

        $this->load->view('templates/meeting/organizer_visaseminar', $data);
    }

    function panelist($sessionType, $tokenId, $username)
    {
        if(!$tokenId)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$sessionType)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if($sessionType == 'w')
        {
            $isWebinarDone = get_cookie('webinar_completed');
            if($isWebinarDone)
            {
                redirect('/meeting/visaseminar/panelist/o/' . $tokenId . '/' . $username);
            }

            $mongoConnectionQuery['type'] = 'ONE_ON_ONE';
            $connectionCount = $this->_connection_collection->count($mongoConnectionQuery);
            if($connectionCount)
            {
                redirect('/meeting/visaseminar/panelist/o/' . $tokenId . '/' . $username);
            }
        }

        $data['base64_token_id'] = $tokenId;
        $data['base64_username'] = $username;

        $tokenId = base64_decode($tokenId);
        $username = base64_decode($username);

        if($sessionType == 'o')
        {
            if(!in_array($username, $this->_superAdmins))
            {
                $tokenId = $this->_counselors[$username];
            }
            $this->_background_images['url'] = $this->_counselor_background_images[$tokenId];
        }

        if(!isset($this->_tokenSessionIds[$tokenId]))
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/";</script>';
            exit;
        }

        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        $userId = $userDetail['id'];

        $connectionMetaData = $username;
        $sessionId = $this->_tokenSessionIds[$tokenId];

        $mongoChatQuery['session_id'] = $sessionId;
        $chatCount = $this->_chat_collection->count($mongoChatQuery);
        $chats = [];
        if($chatCount)
        {
            $chatCursor = $this->_chat_collection->find($mongoChatQuery);
            foreach($chatCursor as $chat)
            {
                if($chat['from'] == $userId)
                {
                    $chat['chat_of'] = 'mine';
                    $chat['name'] = $username;
                }
                else if($chat['from_role'] == 'PUBLISHER')
                {
                    $chat['chat_of'] = 'theirs';

                    $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                    $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                    $chat['name'] = $chatUserDetail['username'];
                }
                if($chat['name'])
                {
                    $chats[] = $chat;
                }
            }
        }

        $connectionType = $sessionType == 'w' ? 'WEBINAR' : 'ONE_ON_ONE';
        $this->visaseminar_model->deleteConnection($userId, $connectionType);

        $insertConnectionData['user_id'] = $userId;
        $insertConnectionData['tokbox_id'] = $tokenId;
        $insertConnectionData['username'] = $username;
        $insertConnectionData['type'] = $connectionType;
        $this->visaseminar_model->insertConnection($insertConnectionData);

        $row['user_id'] = $userId;
        $row['username'] = $username;
        $row['tokbox_id'] = $tokenId;
        $row['session_id'] = $sessionId;
        $row['type'] = $connectionType;
        //$insertOneResult = $this->_connection_collection->insertOne($row);

        $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => 'moderator',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['sessionId'] = $sessionId;
        $data['username'] = $username;
        $data['userId'] = $userId;
        $data['tokboxId'] = $tokenId;
        $data['apiKey'] = $this->_tokboxId;
        $data['common_chat_history'] = $chats;
        $data['sessionType'] = $sessionType;
        $data['backgroundImage'] = $this->_background_images;

        $this->load->view('templates/meeting/panelist_visaseminar', $data);
    }

    function student($sessionType, $tokenId, $username, $linkCount = 1)
    {
        $blockedStudents = [
            'miten_dalsaniya','harshkumar_patel','satyam_swami','himanshu_more','vaishnavi_sawant','maitreyee_gupte','durgeshbonde','suyog_bachhav',
            'atharva_karmarkar','apoorva_janorkar','kaustubh1321@gmail.com','pranav_gajarmal','juie_darwade','sudheendra_katikar','Ketaki',
            'deepti_tanpure','sidharth_sabadra','Aishwarya9850','snehallokesh','aditi_subhedar','rushikesh_pharate','palash_jhamb','vishakha_kasherwal',
            'vivek_m_shah','himani_bajaj','juhi_dalal','sachi_parekh','janhavipatil80@gmail.com','purva_mundlye','isha_mundlye','ayushj','sakshi_shinde'
        ];

        /*if(!$this->input->get('rand') && !array_key_exists(base64_decode($username), $this->_counselors))
        {
            echo '<script type="text/javascript">alert("The session has not been started yet. Please come back after some time."); window.location.href="/";</script>';
            exit;
        }*/

        if(!$sessionType)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$tokenId)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        if($sessionType == 'w')
        {
            $isWebinarDone = get_cookie('webinar_completed');
            if($isWebinarDone)
            {
                if(array_key_exists(base64_decode($username), $this->_counselors))
                {
                    redirect('/meeting/visaseminar/organizer/o/' . $tokenId . '/' . $username);
                }
                redirect('/meeting/visaseminar/rooms/' . $username);
            }

            $mongoConnectionQuery['type'] = 'ONE_ON_ONE';
            $connectionCount = $this->_connection_collection->count($mongoConnectionQuery);
            if($connectionCount)
            {
                if(array_key_exists(base64_decode($username), $this->_counselors))
                {
                    redirect('/meeting/visaseminar/organizer/o/' . $tokenId . '/' . $username);
                }
                redirect('/meeting/visaseminar/rooms/' . $username);
            }
        }

        $data['base64_token_id'] = $tokenId;
        $data['base64_username'] = $username;

        $tokenId = base64_decode($tokenId);
        $username = base64_decode($username);

        if(in_array($username, $blockedStudents))
        {
            echo '<script type="text/javascript">alert("Sorry! You are not authorized."); window.location.href="/";</script>';
            exit;
        }

        if(!isset($this->_tokenSessionIds[$tokenId]))
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/";</script>';
            exit;
        }
        else
        {
            $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        }
        $userId = $userDetail['id'];
        $linkAllowed = $userDetail['link_allowed'];
        if($linkCount > $linkAllowed)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }
        $ipUserId = $userId;
        if($linkCount != 1)
        {
            $ipUserId = $userId . '-' . $linkCount;
        }

        $userIpAddress = $this->get_client_ip();

        $mongoIpQuery['user_id'] = ['$eq' => $ipUserId];
        $userIpDetail = $this->_ip_collection->findOne($mongoIpQuery);
        if($userIpDetail)
        {
            $free_allow = ['nik_uni_1', 'nik_uni_2', 'nikhil', 'test_nik', 'nikhil_g', 'nik_edu_counselor'];

            $lastIp = $userIpDetail['ip_address'];
            $logout = $this->input->get('l');
            if($lastIp != $userIpAddress && !$logout && !in_array($username, $free_allow))
            {
                $data['errorMessage'] = 'You are logged in from another system. Do you want to logout from other system and continue here?<br/><br/><br/><a href="/meeting/visaseminar/student/' . $sessionType . '/' . $data['base64_token_id'] . '/' . $data['base64_username'] . '?l=1"><button type="button">Yes</button></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="/meeting/visaseminar/student/' . $sessionType . '/' . $data['base64_token_id'] . '/' . $data['base64_username'] . '?l=0"><button type="button">No</button></a>';
                $this->load->view('templates/meeting/visaseminar_error', $data);
                exit;
            }
            if($logout)
            {
                $this->visaseminar_model->deleteIp($ipUserId);

                $insertData['user_id'] = $ipUserId;
                $insertData['ip_address'] = $userIpAddress;
                $this->visaseminar_model->insertIp($insertData);

                $mongoIpQuery["user_id"] = $ipUserId;
                $deleteResult = $this->_ip_collection->deleteMany($mongoIpQuery);

                $this->_ip_collection->insertOne($insertData);
            }
        }
        else
        {
            $insertData['user_id'] = $ipUserId;
            $insertData['ip_address'] = $userIpAddress;
            $this->visaseminar_model->insertIp($insertData);

            $this->_ip_collection->insertOne($insertData);
        }

        $connectionType = $sessionType == 'w' ? 'WEBINAR' : 'ONE_ON_ONE';
        $this->visaseminar_model->deleteConnection($userId, $connectionType);

        $insertConnectionData['user_id'] = $userId;
        $insertConnectionData['tokbox_id'] = $tokenId;
        $insertConnectionData['username'] = $username;
        $insertConnectionData['type'] = $connectionType;
        $this->visaseminar_model->insertConnection($insertConnectionData);

        $connectionMetaData = $username;
        $sessionId = $this->_tokenSessionIds[$tokenId];

        $row['user_id'] = $userId;
        $row['username'] = $username;
        $row['tokbox_id'] = $tokenId;
        $row['session_id'] = $sessionId;
        $row['type'] = $connectionType;
        //$insertOneResult = $this->_connection_collection->insertOne($row);

        $mongoAttenderQuery['user_id'] = $userId;
        $deleteResult = $this->_attender_collection->deleteMany($mongoAttenderQuery);

        $row['user_id'] = $userId;
        $row['username'] = $username;
        $row['tokbox_id'] = $tokenId;
        $row['session_id'] = $sessionId;
        $row['type'] = $connectionType;
        $insertOneResult = $this->_attender_collection->insertOne($row);

        $mongoChatQuery['session_id'] = $sessionId;
        $chatCount = $this->_chat_collection->count($mongoChatQuery);
        $chats = [];
        if($chatCount)
        {
            $chatCursor = $this->_chat_collection->find($mongoChatQuery);
            foreach($chatCursor as $chat)
            {
                if($chat['from'] == $userId)
                {
                    $chat['chat_of'] = 'mine';
                    $chat['name'] = $username;
                }
                else if($chat['from_role'] == 'PUBLISHER')
                {
                    $chat['chat_of'] = 'theirs';

                    $mongoChatUserQuery['id'] = ['$eq' => $chat['from']];
                    $chatUserDetail = $this->_user_collection->findOne($mongoChatUserQuery);
                    $chat['name'] = $chatUserDetail['username'];
                }
                if($chat['name'])
                {
                    $chats[] = $chat;
                }
            }
        }

        if($sessionType == 'o')
        {
            $this->_background_images['url'] = $this->_counselor_background_images[$tokenId];
        }

        if(in_array($tokenId, $this->_allRooms['counselling']))
        {
            set_cookie('counselling_room', 1, 36000);
        }
        else if(in_array($tokenId, $this->_allRooms['education_loan_counselling']))
        {
            set_cookie('eduloan_counselling_room', 1, 36000);
        }
        else if(in_array($tokenId, $this->_allRooms['other']))
        {
            set_cookie('other_room', 1, 36000);
        }

        $opentok = new OpenTok($this->_tokboxId, $this->_tokboxSecret);

        $data['token'] = $opentok->generateToken($sessionId, array(
            'role' => $sessionType == 'o' ? 'publisher' : 'subscriber',
            'expireTime' => time()+(7 * 24 * 60 * 60),
            'data' =>  $connectionMetaData
        ));

        $data['sessionId'] = $sessionId;
        $data['userId'] = $userId;
        $data['username'] = $username;
        $data['tokboxId'] = $tokenId;
        $data['apiKey'] = $this->_tokboxId;
        $data['linkCount'] = $linkCount;
        $data['common_chat_history'] = $chats;
        $data['backgroundImage'] = $this->_background_images;

        if($sessionType == 'o')
        {
            $this->load->view('templates/meeting/student_visaseminar', $data);
        }
        else
        {
            $this->load->view('templates/meeting/student_attendee_visaseminar', $data);
        }
    }

    function rooms($username, $linkCount = 1)
    {
        if(!$username)
        {
            echo '<script type="text/javascript">alert("The URL is incorrect."); window.location.href="/";</script>';
            exit;
        }

        $blockedStudents = [
            'miten_dalsaniya','harshkumar_patel','satyam_swami','himanshu_more','vaishnavi_sawant','maitreyee_gupte','durgeshbonde','suyog_bachhav',
            'atharva_karmarkar','apoorva_janorkar','kaustubh1321@gmail.com','pranav_gajarmal','juie_darwade','sudheendra_katikar','Ketaki',
            'deepti_tanpure','sidharth_sabadra','Aishwarya9850','snehallokesh','aditi_subhedar','rushikesh_pharate','palash_jhamb','vishakha_kasherwal',
            'vivek_m_shah','himani_bajaj','juhi_dalal','sachi_parekh','janhavipatil80@gmail.com','purva_mundlye','isha_mundlye','ayushj','sakshi_shinde'
        ];

        $decodedUsername = base64_decode($username);

        if(in_array($decodedUsername, $blockedStudents))
        {
            echo '<script type="text/javascript">alert("Sorry! You are not authorized."); window.location.href="/";</script>';
            exit;
        }

        $decodedUsername = iconv('UTF-8', 'UTF-8//IGNORE', $decodedUsername);
        $mongoUserQuery['username'] = ['$eq' => $decodedUsername];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            echo '<script type="text/javascript">alert("Invalid Account. Please Register First."); window.location.href="/";</script>';
            exit;
        }

        $this->outputData['username'] = $username;
        $this->outputData['linkCount'] = $linkCount;
        $this->render_page('templates/meeting/visaseminar', $this->outputData);
    }

    function check_user($userId, $linkCount = 1)
    {
        $userIpAddress = $this->get_client_ip();
        if($linkCount != 1)
        {
            $userId = $userId  . '-' . $linkCount;
        }

        $mongoIpQuery['user_id'] = ['$eq' => $userId];
        $userIpDetail = $this->_ip_collection->findOne($mongoIpQuery);
        if($userIpDetail)
        {
            $lastIp = $userIpDetail['ip_address'];
            if($lastIp != $userIpAddress)
            {
                echo 0;
                exit;
            }
        }
        echo 1;
    }

    function error()
    {
        $data['errorMessage'] = "You just logged in from another system. You've been logged out from this system. Please contact to support.";
        $this->load->view('templates/meeting/visaseminar_error', $data);
    }

    function delete_connection($username)
    {
        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            return;
        }

        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        $userId = $userDetail['id'];

        $this->visaseminar_model->deleteConnection($userId);

        $mongoQuery["username"] = $username;
        $deleteResult = $this->_connection_collection->deleteMany($mongoQuery);
    }

    function add_connection($username, $tokenId, $sessionType)
    {
        $username = iconv('UTF-8', 'UTF-8//IGNORE', $username);
        $mongoUserQuery['username'] = ['$eq' => $username];
        $userCount = $this->_user_collection->count($mongoUserQuery);
        if(!$userCount)
        {
            return;
        }

        $userDetail = $this->_user_collection->findOne($mongoUserQuery);
        $userId = $userDetail['id'];

        $this->visaseminar_model->deleteConnection($userId);

        $mongoQuery["username"] = $username;
        $deleteResult = $this->_connection_collection->deleteMany($mongoQuery);

        $connectionType = $sessionType == 'w' ? 'WEBINAR' : 'ONE_ON_ONE';
        $sessionId = $this->_tokenSessionIds[$tokenId];

        $insertConnectionData['user_id'] = $userId;
        $insertConnectionData['tokbox_id'] = $tokenId;
        $insertConnectionData['username'] = $username;
        $insertConnectionData['type'] = $connectionType;
        $this->visaseminar_model->insertConnection($insertConnectionData);

        $row['user_id'] = $userId;
        $row['username'] = $username;
        $row['tokbox_id'] = $tokenId;
        $row['session_id'] = $sessionId;
        $row['type'] = $connectionType;
        //$insertOneResult = $this->_connection_collection->insertOne($row);
    }

    function get_connections($tokenId)
    {
        $connectionType = $tokenId == 157 ? 'WEBINAR' : 'ONE_ON_ONE';
        $connections = [];
        $mongoConnectionQuery['tokbox_id'] = $tokenId;
        $mongoConnectionQuery['type'] = $connectionType;
        $connectionCursor = $this->_connection_collection->find($mongoConnectionQuery);
        foreach($connectionCursor as $connection)
        {
            $connections[] = $connection;
        }

        echo json_encode($connections);
    }

    function join_tid($type)
    {
        $tokenTypes = $this->_tokenTypes[$type];
        switch($type)
        {
            case 'usa':
                $mongoConnectionQuery = [
                    ['$group' => ['_id' => '$tokbox_id', 'count' => ['$sum' => 1]]],
                    ['$sort' => ['count' => 1]]
                ];
                $tokboxId = '';
                $connectionCursor = $this->_connection_collection->aggregate($mongoConnectionQuery);
                foreach($connectionCursor as $connection)
                {
                    if(in_array($connection['_id'], $tokenTypes))
                    {
                        $tokboxId = $connection['_id'];
                        break;
                    }
                }
                if(!$tokboxId)
                {
                    $random = mt_rand(0, 5);
                    $tokboxId = $tokenTypes[$random];
                }
                break;
            case 'thane':
            case 'borivalli':
            case 'santacruz':
                $random = mt_rand(0, 1);
                $tokboxId = $tokenTypes[$random];
                break;
            default:
                $tokboxId = $tokenTypes;
                break;
        }

        $base64_token_id = base64_encode($tokboxId);
        $base64_token_id = str_replace("=", "", $base64_token_id);
        echo $base64_token_id;
    }

    function chat()
    {
        $post_data = json_decode(file_get_contents("php://input"), true);

        $data['from'] = $post_data['user_id'];
        $data['to'] = -1;
        $data['to_user_type'] = NULL;
        $data['session_id'] = $post_data['sessionId'];
        $data['tokbox_id'] = $post_data['tokboxId'];
        $data['user_type'] = 'USER';
        $data['message'] = $post_data['msg'];
        $data['from_role'] = isset($post_data['from_role']) ? $post_data['from_role'] : NULL;
        $data['chat_id'] = isset($post_data['chat_id']) ? $post_data['chat_id'] : NULL;


        $this->visaseminar_model->storeChat($data);

        $insertOneResult = $this->_chat_collection->insertOne($data);
    }

    function get_stats()
    {
        //$totalRegistrations = $this->_user_collection->count();

        $totalRegistrations = 1477;

        $connectionType = 'ONE_ON_ONE';
        $mongoConnectionQuery['type'] = $connectionType;
        $activeParticipants = $this->_connection_collection->count($mongoConnectionQuery);

        $sessionAttended = 0;

        $attendersCount = $this->_attender_collection->count($mongoConnectionQuery);
        if($attendersCount && $activeParticipants)
        {
            $attendersCursor = $this->_attender_collection->find($mongoConnectionQuery);
            $activeParticipantsCursor = $this->_connection_collection->find($mongoConnectionQuery);

            $allAttenders = [];
            $allActiveParticipants = [];
            foreach($attendersCursor as $attender)
            {
                $allAttenders[] = $attender;
            }
            foreach($activeParticipantsCursor as $activeParticipant)
            {
                $allActiveParticipants[] = $activeParticipant;
            }

            foreach($allAttenders as $attender)
            {
                $found = false;
                foreach($allActiveParticipants as $activeParticipant)
                {
                    if($attender['user_id'] == $activeParticipant['user_id'])
                    {
                        $found = true;
                        break;
                    }
                }
                if(!$found)
                {
                    $sessionAttended++;
                }
            }
        }

        //$activeRooms = mt_rand(10, 15);
        $activeRooms = 0;

        $allStats = [
            'total_registrations' => $totalRegistrations,
            'active_participants' => $activeParticipants,
            'session_attended' => $sessionAttended,
            'active_rooms' => $activeRooms
        ];

        echo json_encode($allStats);
    }

    function check_webinar()
    {
        $mongoConnectionQuery['type'] = 'ONE_ON_ONE';
        $connectionCount = $this->_connection_collection->count($mongoConnectionQuery);
        if($connectionCount)
        {
            set_cookie('webinar_completed', 1, 36000);
            exit;
        }
        set_cookie('webinar_completed', 0, 36000);
        echo 1;
    }

    function get_webinar_link($username, $linkCount = 1)
    {
        $organizers = [
            'deepika_admin',
            'banit_sawhney',
            'hiren_rathod',
            'harkiran_sawhney',
            'nik_admin',
            'tanmay_kudyadi',
            'sudhir_admin',
            'akshay_bhandari',
            'sonali_palkar'
        ];

        $decodedUsername = base64_decode($username);
        $url = '/meeting/visaseminar/student/w/MTU3/' . $username . '/' . $linkCount;
        if(in_array($decodedUsername, $organizers))
        {
            $url = '/meeting/visaseminar/organizer/w/MTU3/' . $username . '/' . $linkCount;
        }

        echo $url;
    }

    function thankyou()
    {
        $this->load->view('templates/meeting/visaseminar_thankyou');
    }

    function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        return $ipaddress;
    }

    function visaSeminarUrl()
    {

        $userEmailId = $this->input->post('user_email') ? $this->input->post('user_email') : 'NAAA';
        $userMobileNumber = $this->input->post('user_mobile') ? $this->input->post('user_mobile') : 'NAAA';

        $userDetail = $this->visaseminar_model->validUserInfo($userEmailId,$userMobileNumber);

        if($userDetail && $userDetail[0]['link_allowed'])
        {

            $tokenId = 433; //$userDetail[0]['tokbox_id'];
            $userName = $userDetail[0]['username'];

            $tokenId = base64_encode($tokenId);
            $usernamecode = base64_encode($userName);

            $usernamecode = str_replace("=", "", $usernamecode);
            $tokenId = str_replace("=", "", $tokenId);

            echo "https://hellouni.org/event/launch/$tokenId/$usernamecode";
            //echo "https://hellouni.org/event/rooms/$usernamecode";
            exit();
        }
        else
        {
            echo "Invalid User";
            exit();
        }

        //https://hellouni.org/meeting/visaseminar/student/w/<token id>/<encoded username>
        //https://hellouni.org/meeting/visaseminar/rooms/<encoded username>
    }

    function visaSeminarUrlParent()
    {

        $this->load->model('webinar/common_model');

        $userEmailId = $this->input->post('user_email') ? $this->input->post('user_email') : 'NAAA';
        $userMobileNumber = $this->input->post('user_mobile') ? $this->input->post('user_mobile') : 'NAAA';
        $linkNumber = $this->input->post('link_number') ? $this->input->post('link_number') : 2;

        $userDetail = $this->visaseminar_model->validUserInfo($userEmailId,$userMobileNumber);

        if($userDetail)
        {
            $userName = $userDetail[0]['username'];
            $studentEmail = $userDetail[0]['email'];
            $condition = ["username" => $userName];
            $updateData = [
                "link_allowed" => $linkNumber
            ];

            $updateObj = $this->common_model->upsert($condition,$updateData,"user_master");

            $mongoUserQuery['username'] = $userName;
            $updateMongoUser = ['$set' => ['link_allowed' => $linkNumber]];
            $this->_user_collection->updateOne($mongoUserQuery, $updateMongoUser);

            $tokenId = 157; //$userDetail[0]['tokbox_id'];

            $tokenId = base64_encode($tokenId);
            $usernamecode = base64_encode($userName);

            $usernamecode = str_replace("=", "", $usernamecode);
            $tokenId = str_replace("=", "", $tokenId);

            $mailSubject = "HelloUni - Your Link to access the VISA Finance Webinar (Parent Link : $linkNumber)";
            $mailTemplate = "Hi ,

                                Hope this email finds you and your near & dear ones well... Hope all of you are staying positive & testing negative...

                                We have received a request to generate a URL for you to access the VISA Seminar.

                                Please open the link below on your Laptop (for best result) using Chrome Browser (for best result) to enter the Visa Webinar:

                                URL : https://hellouni.org/meeting/visaseminar/rooms/$usernamecode/$linkNumber

                              Regards,
                              Team HelloUni
                              Tel: +91 81049 09690
                              Email: info@hellouni.org

                               <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

            $ccMailList = '';
            $mailAttachments = '';

            //$studentEmail = 'nikhil.gupta@issc.in';

            $sendMail = sMail($studentEmail, "Student", $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

            if($sendMail){

              echo "https://hellouni.org/meeting/visaseminar/rooms/$usernamecode/$linkNumber";
              exit();

            } else {

              echo "https://hellouni.org/meeting/visaseminar/rooms/$usernamecode/$linkNumber";
              exit();

            }

        }
        else
        {
            echo "Invalid User";
            exit();
        }

        //https://hellouni.org/meeting/visaseminar/student/w/<token id>/<encoded username>
        //https://hellouni.org/meeting/visaseminar/rooms/<encoded username>
    }

    function temp()
    {
        echo '<script type="text/javascript">alert("The link is not active anymore."); window.location.href="/";</script>';
        exit;

        if($this->input->post())
        {
            $name = $this->input->post('name');
            $username = strtolower(str_replace(" ", "_", $name));

            $checkUser = $this->visaseminar_model->getUserByUsername($username);
            if(!$checkUser)
            {
                $user['username'] = $username;
                $user['name'] = $name;
                $user['type'] = 3;
                $user['status'] = 1;
                $user['password'] = md5($username);
                $user['link_allowed'] = 1;

                $lastInsertId = $this->visaseminar_model->insertUser($user);
                $user['id'] = $lastInsertId;

                $mongoUserQuery['username'] = $username;
                $this->_user_collection->deleteMany($mongoUserQuery);
                $this->_user_collection->insertOne($user);
            }

            /*$tempTokenTypes = [158, 164, 165, 166, 167, 169, 171, 173, 175, 176, 177];
            $storedTokenTypes = [];
            $tokenId = '';

            $tempCursor = $this->_temp_collection->find();
            foreach($tempCursor as $storedTokenType)
            {
                $storedTokenTypes[] = $storedTokenType['token_id'];
            }

            foreach($tempTokenTypes as $tempTokenType)
            {
                if(!in_array($tempTokenType, $storedTokenTypes))
                {
                    $mongoTempQuery['username'] = $username;
                    $this->_temp_collection->deleteMany($mongoTempQuery);

                    $row['token_id'] = $tempTokenType;
                    $row['name'] = $name;
                    $row['username'] = $username;

                    $this->_temp_collection->insertOne($row);

                    $tokenId = $tempTokenType;
                    break;
                }
            }*/

            $base64Username = base64_encode($username);
            $base64Username = str_replace("=", "", $base64Username);

            /*if($tokenId)
            {
                redirect('/meeting/visaseminar/organizer/w/MTU3/' . $base64Username . '?t=1');
            }
            else
            {
                redirect('/meeting/visaseminar/student/w/MTU3/' . $base64Username . '?t=1');
            }*/
            redirect('/meeting/visaseminar/student/w/MTU3/' . $base64Username);
        }
        $this->load->view('templates/meeting/temp_visaseminar');
    }

    function reset($username, $type = '')
    {
        delete_cookie('webinar_completed');
        if($type == 'c')
        {
            redirect('/meeting/visaseminar/student/w/MTU3/' . $username);
        }
        else if($type == 'o')
        {
            redirect('/meeting/visaseminar/organizer/w/MTU3/' . $username);
        }
        else
        {
            redirect('/meeting/visaseminar/rooms/' . $username);
        }
    }
}
