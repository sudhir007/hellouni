<?php

require_once APPPATH . 'libraries/Mail/sMail.php';

class Webinarmaster extends MY_Controller {

      public $outputData;		//Holds the output data for each view
      public $loggedInUser;


	 function __construct()

    {

        parent::__construct();

        $this->load->library('template');

        $this->lang->load('enduser/home', $this->config->item('language_code'));

		$this->load->library('form_validation');

		$this->load->model('location/country_model');

    $this->load->model('webinar/webinar_model');

    $this->load->model('webinar/common_model');

     $this->load->model('course/course_model');

		$this->load->model('course/degree_model');

    $this->load->model('user/user_model');
    $this->load->model('tokbox_model');

    $this->load->model('redi_model');

      //  track_user();


    }

    function webinarlist(){

      $this->load->helper('cookie_helper');

      $usrdata=$this->session->userdata('user');
      $userId = 0;

 	     if(empty($usrdata)){

         $this->outputData['login'] = "NO";
         $this->outputData['webinarlist'] = $this->webinar_model->webinar_list($userId);

       } else {

         $userId = $usrdata['id'];
         $this->outputData['login'] = "YES";
         $this->outputData['webinarlist'] = $this->webinar_model->webinar_list($userId);

       }


       $this->render_page('templates/webinar/webinar_list',$this->outputData);

    }

    function webinarapply(){

      $this->load->helper('cookie_helper');

      $usrdata=$this->session->userdata('user');

 	     if(empty($usrdata)){
         //redirect('user/account');
         echo "/user/account";
         exit();
       }

       $userId = $usrdata['id'];
       $webinarId = $this->input->post('webinar_id') ?  $this->input->post('webinar_id') : 0;

       $webinarLogData = [
         "webinar_id" => $webinarId,
         "user_id" => $userId,
         "webinar_applied" => 1
       ];

       $webinarObj = $this->common_model->post($webinarLogData,"webinar_log_master");

       $whereId = ["id"=> $webinarId];
       $webinarInfoObj = $this->common_model->get($whereId,"webinar_master");

       $studentName = $usrdata['name'];
       $studentEmail = $usrdata['email'];
       $webinarName = $webinarInfoObj['data'][0]['webinar_name'];
       $webinarDate = $webinarInfoObj['data'][0]['webinar_date'];
       $webinarTime = $webinarInfoObj['data'][0]['webinar_time'];

       $webinarDate = date('D, d F Y', strtotime($webinarDate));

       $mailSubject = "HelloUni - You have Registered for the Webinar: $webinarName";
       $mailTemplate = "Hi $studentName,

                          Thank you for registering for the Webinar: $webinarName.

                          We are glad to have you participate in the session!

                          Please see details below to join the Webinar:

                          1. You will receive a mail containing the joining link 30 minutes before the webinar begins.

                          2. You can also join the session by logging in to hellouni.org and on the Webinar Page, click on the Join button for your chosen Webinar.


                          Date - $webinarDate
                          Time - $webinarTime

                          Regards,
                          Team HelloUni
                          Tel: +91 81049 09690
                          Email: info@hellouni.org


                          <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

       $ccMailList = '';
       $mailAttachments = '';

       //$studentEmail = 'nikhil.gupta@issc.in';

       $sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

       if($sendMail){
         echo "/webinar/webinarmaster/webinarlist";
       } else {
         echo "/webinar/webinarmaster/webinarlist";
       }

    }

    function webinarjoin(){

      $this->load->helper('cookie_helper');

      $usrdata=$this->session->userdata('user');

 	     if(empty($usrdata)){
         //redirect('user/account');
         echo "/user/account";
         exit();
       }

       $userId = $usrdata['id'];
       $webinarId = $this->input->post('webinar_id') ?  $this->input->post('webinar_id') : 0;

       $webinarLogData = [
         "webinar_attented" => 1
       ];

       $whereWebinarlog = [
         "webinar_id" => $webinarId,
         "user_id" => $userId
     ];

       $webinarObj = $this->common_model->put($whereWebinarlog,$webinarLogData,"webinar_log_master");
       $whereId = ["id"=> $webinarId];
       $webinarInfoObj = $this->common_model->get($whereId,"webinar_master");
       $tokboxId = $webinarInfoObj['data'][0]['tokbox_token_id'];

       //echo "/webinar/webinarmaster/webinarlist";
       echo "/meeting/webinar/attendee?id=" . base64_encode($tokboxId);
    }

    function webinarongoing(){

      $this->load->helper('cookie_helper');

      $usrdata=$this->session->userdata('user');

 	     if(empty($usrdata)){
         echo "NO";
         exit();
       }

       $userId = $usrdata['id'];
       $ongoingWebinar = $this->webinar_model->getOngoingWebinar($userId);

       if($ongoingWebinar) {

         foreach ($ongoingWebinar as $cwkey => $cwvalue) {

           $webinarName = $cwvalue['webinar_name'];
           $webinarId = $cwvalue['id'];
           $webinarTime = $cwvalue['webinar_time'];

           $webDate = new DateTime($cwvalue['webinar_date']." ".$cwvalue['webinar_time']);
           $webinarStartDate = $webDate->format('H:i:s');

           $date = new DateTime('now',new DateTimeZone('Asia/Kolkata'));
           $ctime = $date->format('H:i:s');

           $start = strtotime($ctime);
           $end = strtotime($webinarStartDate);

           $mins = ($start - $end) / 60;

           if ($start >  $end && $mins < 60) {

             $response['ongoing_webinar'] = $ongoingWebinar[$cwkey];
             $response['name'] = $webinarName;
             $response['id'] = $webinarId;

             header('Content-Type: application/json');
             echo (json_encode($response));
             exit();

           } else {
             echo "NO";
             exit();
           }

         }

       } else {
         echo "NO";
         exit();

       }

    }

    function webinarrating(){

      $this->load->helper('cookie_helper');

      $usrdata=$this->session->userdata('user');

 	     if(empty($usrdata)){
         redirect('user/account');
       }

       $userId = $usrdata['id'];
       $webinarId = $this->input->post('webinar_id') ?  $this->input->post('webinar_id') : 0;
       $webinarRating = $this->input->post('webinar_rating') ?  $this->input->post('webinar_rating') : 4;

       $webinarLogData = [
         "webinar_rating" => $webinarRating
       ];

       $whereWebinarlog = [
         "webinar_id" => $webinarId,
         "user_id" => $userId
     ];

       $webinarObj = $this->common_model->put($whereWebinarlog,$webinarLogData,"webinar_log_master");

       echo "Applied successfully ..!!!";
    }

    function fairList(){

      $this->load->helper('cookie_helper');

      $usrdata=$this->session->userdata('user');

 	     if(empty($usrdata)){
         redirect('user/account');
         exit();
       }

       $userId = $usrdata['id'];

       $this->outputData['fairlist'] = $this->webinar_model->virtualFairParticipants($userId);

       $this->render_page('templates/webinar/fair_list',$this->outputData);

    }

    function fairapply(){

      $this->load->helper('cookie_helper');

      $usrdata=$this->session->userdata('user');

 	     if(empty($usrdata)){
         //redirect('user/account');
         echo "/user/account";
         exit();
       }

       $this->load->model('redi_model');
       $fairId = $this->redi_model->getKey('FAIR-ID');

       $userId = $usrdata['id'];
       //$fairId = $this->input->post('fair_id') ?  $this->input->post('fair_id') : 0;

       $fairDataCheck = $this->webinar_model->dataCheck($userId);
       if(!empty($fairDataCheck)){
         echo "FILLDATA";
         exit();
       }

      $fairListObj = $this->webinar_model->checkAppliedFair($userId,$fairId);
       if(count($fairListObj) > 0){
         echo "APPLIED";
         exit();
       }

       $webinarLogData = [
           "fair_id" => $fairId,
           "user_id" => $userId
        ];

       $webinarObj = $this->common_model->post($webinarLogData,"fair_log_master");

       $whereId = ["id"=> $fairId];
       $webinarInfoObj = $this->common_model->get($whereId,"fair_master");

       $studentName = $usrdata['name'];
       $studentEmail = $usrdata['email'];
       $webinarName = $webinarInfoObj['data'][0]['name'];
       $webinarDate = $webinarInfoObj['data'][0]['fair_date'];
       $webinarTime = $webinarInfoObj['data'][0]['fair_time'];

       $webinarDate = date('D, d F Y', strtotime($webinarDate));

       $mailSubject = "You are Applied for $webinarName";
       $mailTemplate = "Hi $studentName,

                          Thank you for Applying for the $webinarName

                          We are glad to have you participate in the session!

                          In order to help you navigate the Virtual Fair and make best use of the platform, we have included a few instructions below:

                            1. Please make sure to use your Laptop to attend this virtual fair
                            2. Login to your <a href='https://www.hellouni.org/user/account'>HelloUni Account</a> using the credentials.
                            3. Click on - Attend the Virtual Fair icon
                            4. Identify the universities you wish to connect with & start interacting!
                            5. Avail fee waivers, announcements on scholarships and decisions and get all your queries answered!

                        Virtual Fair Schedule:

                        Date - $webinarDate
                        Time - $webinarTime

                        You can also invite your friends, batchmates, seniors and juniors as well by clicking on Invite Friends!

                          Regards,
                          Team HelloUni
                          Tel: +91 81049 09690
                          Email: info@hellouni.org


                          <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

       $ccMailList = '';
       $mailAttachments = '';

       //$studentEmail = 'nikhil.gupta@issc.in';

       $sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

       if($sendMail){
         echo "/university/virtualfair/list";
       } else {
         echo "/university/virtualfair/list";
       }

    }

    function fairUniversityList(){

      $this->load->helper('cookie_helper');

      $usrdata=$this->session->userdata('user');
      $response['userLoggedIn'] = true;

 	     if(empty($usrdata)){
         $response['userLoggedIn'] = false;
         exit();
       }

       $this->load->model('redi_model');
       $fairId = $this->redi_model->getKey('FAIR-ID');

      //$fairId = $this->input->post('fair_id') ?  $this->input->post('fair_id') : 0;

      $webinarInfoObj = $this->webinar_model->fair_webinar_list($fairId);

      $response['list'] = $webinarInfoObj;
      header('Content-Type: application/json');
      echo (json_encode($response));

    }

    function faircheck(){

      $this->load->helper('cookie_helper');

      $usrdata=$this->session->userdata('user');
      $response['userLoggedIn'] = true;

 	     if(empty($usrdata)){
         $response['userLoggedIn'] = false;
         exit();
       }
       $userId = $usrdata['id'];
       $response['fairlist'] = $this->webinar_model->fair_list($userId);

      header('Content-Type: application/json');
      echo (json_encode($response));

    }

    function fairDataUpdate(){

      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
        echo "/user/account";
        exit();
      }

      $userId = $usrdata['id'];

      $studentData = array(
          'intake_month' => $this->input->post('intake_month'),
          'intake_year' => $this->input->post('intake_year'),
          'comp_exam' => $this->input->post('comp_exam'),
          'comp_exam_score' => $this->input->post('comp_exam_score'),
          'gpa_exam' => $this->input->post('gpa_exam'),
          'gpa_exam_score' => $this->input->post('gpa_exam_score'),
          'work_exp' => $this->input->post('work_exp'),
          'work_exp_month' => $this->input->post('work_exp_month')
      );

      $whereCondition = [
        "user_id" => $userId
      ];

      $webinarObj = $this->common_model->put($whereCondition,$studentData,"student_details");

      $this->load->model('redi_model');
      $fairId = $this->redi_model->getKey('FAIR-ID');
      //$fairId = $this->input->post('fair_id') ?  $this->input->post('fair_id') : 0;

      $webinarLogData = [
          "fair_id" => $fairId,
          "user_id" => $userId
       ];

      $webinarObj = $this->common_model->post($webinarLogData,"fair_log_master");

      $whereId = ["id"=> $fairId];
      $webinarInfoObj = $this->common_model->get($whereId,"fair_master");

      $studentName = $usrdata['name'];
      $studentEmail = $usrdata['email'];
      $webinarName = $webinarInfoObj['data'][0]['name'];
      $webinarDate = $webinarInfoObj['data'][0]['fair_date'];
      $webinarTime = $webinarInfoObj['data'][0]['fair_time'];

      $webinarDate = date('D, d F Y', strtotime($webinarDate));

      $mailSubject = "You are Applied for $webinarName";
      $mailTemplate = "Hi $studentName,

                         Thank you for Applying for the $webinarName

                         We are glad to have you participate in the session!

                         In order to help you navigate the Virtual Fair and make best use of the platform, we have included a few instructions below:

                         1. Please make sure to use your Laptop to attend this virtual fair
                         2. Login to your <a href='https://www.hellouni.org/user/account'>HelloUni Account</a> using the credentials.
                         3. Click on - Attend the Virtual Fair icon
                         4. Identify the universities you wish to connect with & start interacting!
                         5. Avail fee waivers, announcements on scholarships and decisions and get all your queries answered!

                       Virtual Fair Schedule:

                       Date - $webinarDate
                       Time - $webinarTime

                       You can also invite your friends, batchmates, seniors and juniors as well by clicking on Invite Friends!

                         Regards,
                         Team HelloUni
                         Tel: +91 81049 09690
                         Email: info@hellouni.org


                         <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

      $ccMailList = '';
      $mailAttachments = '';

      //$studentEmail = 'nikhil.gupta@issc.in';

      $sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

      echo "SUCCESS";

    }

    function virtualFairParticipantsList(){

      $this->load->helper('cookie_helper');

      $usrdata=$this->session->userdata('user');

 	     if(empty($usrdata)){
         redirect('user/account');
         exit();
       }

       $userId = $usrdata['id'];
       $fairId = $this->uri->segment('4');
       //$fairId = 1;

       $fairListObj = $this->webinar_model->virtualFairParticipants($userId,$fairId);
       $this->outputData['fairlist'] = $fairListObj;

       $totalRegistration = [];
       foreach ($fairListObj as $keyflo => $valueflo) {
         // code...
         $fairName = $valueflo['fair_name'];
         array_push($totalRegistration,$valueflo['total_registrations'],$valueflo['default_total_student']);
       }

       $totalCount = array_sum($totalRegistration);

       $this->outputData['precounselling_available'] = "NO";
       $precounsellingCheck = $this->webinar_model->preCounsellingAvailabilityCheck();
       if($precounsellingCheck){
         $this->outputData['precounselling_available'] = "YES";
       }
       $this->outputData['fair_name'] = $fairName;
       $this->outputData['fair_total_registration'] = $totalCount;

       $this->render_page('templates/webinar/fair_list',$this->outputData);

    }

    function virtualFairParticipantsListNew(){
	/*echo '<script type="text/javascript">alert("This link is invalid."); window.location.href="/search/university";</script>';
	exit;*/

      $this->load->helper('cookie_helper');
      $this->load->model('redi_model');

      $usrdata=$this->session->userdata('user');

 	     if(empty($usrdata)){
         redirect('user/account');
         exit();
       }

       $userId = $usrdata['id'];
       $fairId = $this->redi_model->getKey('FAIR-ID');
       //$fairId = 1;

       $totalRegistrationkey = "Total-Registration";
       $totalRegistrationCount = $this->redi_model->getKey($totalRegistrationkey);

       $totalLiveStudentkey = "Total-Live-Student";
       $totalLiveStudentCount = $this->redi_model->getKey($totalLiveStudentkey);

       $totalSessionKey = "Total-Sessions";
       $totalSessionCount = $this->redi_model->getKey($totalSessionKey);

       $precounsellingKey = "Precounselling-Done-$userId";
       $precounsellingStatus = $this->redi_model->getKey($precounsellingKey);

       if(!$precounsellingStatus){

         $precounsellingKey = "Precounselling-Done-$userId";
         $setPrecounsellingStatus = $this->redi_model->setKey($precounsellingKey,"NO");
         $precounsellingStatus = "NO";

       }

       $fareNameKey = "Fair-Name";
       $fairName = $this->redi_model->getKey($fareNameKey);

       $precounsellingCheck = $this->webinar_model->preCounsellingAvailabilityCheck();

       foreach ($precounsellingCheck as $keypc => $valuepc) {
         if($valuepc['limit_of_student'] > $valuepc['currently_handling']){
           $precounsellingCheck[$keypc]['button_type'] = "JOIN";
         } else{
           $precounsellingCheck[$keypc]['button_type'] = "JOINQUEUE";
         }
        }

        $this->outputData['precounselling_list'] = $precounsellingCheck;
        $this->outputData['precounselling_status'] = $precounsellingStatus;

        $universityLiveCount = 65;

      if($precounsellingStatus == "YES"){

        $checkFairObj = $this->redi_model->getKey("FAIR-USER-ID-$userId");
        $finalFairObj = json_decode($checkFairObj,true);

        if($finalFairObj){
          $fairListObj = $finalFairObj;
        } else {
          $fairListObj = $this->webinar_model->virtualFairParticipantsNew($userId,$fairId);
          $setFairListObj = $this->redi_model->setKey("FAIR-USER-ID-$userId",json_encode($fairListObj));
        }

        $fairListNew = [];
        $totalRegistration = [];
        $trackedParticipants = [];
        $bookedUniversities = false;

        foreach ($fairListObj as $keyflo => $valueflo) {

             if($valueflo['book_type'] == "current_user_slot"){

               $fairListObj[$keyflo]['button_name'] = "JOIN";
               $fairListObj[$keyflo]['button_type'] = "ENABLED";

               $universityLiveCount = $universityLiveCount +1;
               $bookedUniversities = true;

             } else if($valueflo['book_type'] == "booked_user_slot"){
                 $bookedUniversities = true;

               $fairListObj[$keyflo]['button_name'] = "JOIN";
               $fairListObj[$keyflo]['button_type'] = "DISABLED";

               $universityLiveCount = $universityLiveCount +1;

             } else if($valueflo['book_type'] == "university_active_slot"){
                 $checkRegistration = $this->webinar_model->checkRegistration($userId, $fairId, $valueflo['id']);
                 if($checkRegistration)
                 {
                     $fairListObj[$keyflo]['book_type'] = 'already_attended';
                     $sacount = json_decode($valueflo['student_attending']);

                     if(count($sacount) < $valueflo['limit_of_student'])
                     {
                         $fairListObj[$keyflo]['button_name'] = "JOIN";
                         $fairListObj[$keyflo]['button_type'] = "ENABLED";
                     }
                     else
                     {
                         $fairListObj[$keyflo]['button_name'] = "BOOK";
                         $fairListObj[$keyflo]['button_type'] = "ENABLED";
                     }
                     $bookedUniversities = true;
                 }
               else
               {
                   $key = 'SLOTS-' . $userId;
                   $bookedSlots = $this->redi_model->getKey($key);
                   $expired = false;
                   if($bookedSlots)
                   {
                       $bookedSlots = json_decode($bookedSlots, true);
                       $bookedSlotsString = implode(',', $bookedSlots);
                       $expiredSlots = $this->webinar_model->getExpiredSlots($bookedSlotsString, $userId);
                       if($expiredSlots)
                       {
                           foreach($expiredSlots as $expiredSlot)
                           {
                               if($expiredSlot['participant_id'] == $valueflo['id'])
                               {
                                   $fairListObj[$keyflo]['book_type'] = 'already_expired';
                                   $sacount = json_decode($valueflo['student_attending']);
                                   if($sacount < $valueflo['limit_of_student'])
                                   {
                                       $fairListObj[$keyflo]['button_name'] = "JOIN";
                                       $fairListObj[$keyflo]['button_type'] = "ENABLED";
                                   }
                                   else
                                   {
                                       $fairListObj[$keyflo]['button_name'] = "BOOK";
                                       $fairListObj[$keyflo]['button_type'] = "ENABLED";
                                   }
                                   $bookedUniversities = true;
                                   $expired = true;
                                   break;
                               }
                           }
                       }
                   }
                   if(!$expired)
                   {
                       $scount = json_decode($valueflo['student_booked']);
                       if(count($scount) < $valueflo['limit_of_student']){
                         $fairListObj[$keyflo]['button_name'] = "JOIN";
                         $fairListObj[$keyflo]['button_type'] = "ENABLED";
                       }else{
                         $fairListObj[$keyflo]['button_name'] = "BOOK";
                         $fairListObj[$keyflo]['button_type'] = "ENABLED";
                       }
                   }
               }

               $universityLiveCount = $universityLiveCount +1;

             } else {
                 $studentBooked = $valueflo['student_booked'] ? json_decode($valueflo['student_booked'], true) : [];
                 if($studentBooked && in_array($userId, $studentBooked))
                 {
                     $bookedUniversities = true;
                     $fairListObj[$keyflo]['book_type'] = "booked_user_slot";
                     $fairListObj[$keyflo]['button_name'] = "JOIN";
                     $fairListObj[$keyflo]['button_type'] = "DISABLED";
                 }
               else{
                   $fairListObj[$keyflo]['button_name'] = "JOIN";
                   $fairListObj[$keyflo]['button_type'] = "ENABLED";
               }
             }

             if($valueflo['participant_type'] == "GROUP"){

               $universityIds = json_decode($valueflo['university_ids']);

               foreach ($universityIds as $keyids) {
                 $getUniInfo = $this->webinar_model->uniInfoById($keyids);
                 $universityName = $getUniInfo[0]['name'];
 		             $universityLogo = $getUniInfo[0]['logo'];

                 $fairListObj[$keyflo]['university_name'] = $universityName;
		             $fairListObj[$keyflo]['img'] = $universityLogo;

                 $fairListObj[] = $fairListObj[$keyflo];

               }
               unset($fairListObj[$keyflo]);

             }
             //$fairListNew[] = $fairListObj[$keyflo];
        }

        $universityName = array_column($fairListObj, 'university_name');
        array_multisort($universityName, SORT_ASC, $fairListObj);

        $this->outputData['fairlist'] = $fairListObj;

      }

      //Eduloans Counselling
      $eduloanscounsellingCheck = $this->webinar_model->eduCounsellingAvailabilityCheck();

      foreach ($eduloanscounsellingCheck as $keyed => $valueed) {
        if($valueed['limit_of_student'] > $valueed['currently_handling']){
          $eduloanscounsellingCheck[$keyed]['button_type'] = "JOIN";
        } else{
          $eduloanscounsellingCheck[$keyed]['button_type'] = "JOINQUEUE";
        }
       }

       $this->outputData['edu_counselling_list'] = $eduloanscounsellingCheck;

       //Post counselling
       $postcounsellingCheck = $this->webinar_model->postCounsellingAvailabilityCheck();

       foreach ($postcounsellingCheck as $keypt => $valuept) {
         if($valuept['limit_of_student'] > $valuept['currently_handling']){
           $postcounsellingCheck[$keypt]['button_type'] = "JOIN";
         } else{
           $postcounsellingCheck[$keypt]['button_type'] = "JOINQUEUE";
         }
        }

        $this->outputData['post_counselling_list'] = $postcounsellingCheck;


       $this->outputData['fair_name'] = $fairName;
       $this->outputData['fair_id'] = $fairId;
       $this->outputData['university_live_count'] = $universityLiveCount;
       $this->outputData['fair_total_registration'] = $totalRegistrationCount;
       $this->outputData['total_live_student'] = $totalLiveStudentCount;
       $this->outputData['total_session'] = $totalSessionCount;
       $this->outputData['bookedUniversities'] = $bookedUniversities;

       $this->render_page('templates/webinar/myFairList',$this->outputData);

    }

    function virtualFairParticipantsListRefresh(){

      $this->load->helper('cookie_helper');
      $this->load->model('redi_model');

      $usrdata=$this->session->userdata('user');

 	     if(empty($usrdata)){
         redirect('user/account');
         exit();
       }

       $userId = $usrdata['id'];
       $fairId = $this->redi_model->getKey('FAIR-ID');
       //$fairId = 1;


       $totalRegistrationkey = "Total-Registration";
       $totalRegistrationCount = $this->redi_model->getKey($totalRegistrationkey);

       $totalLiveStudentkey = "Total-Live-Student";
       $totalLiveStudentCount = $this->redi_model->getKey($totalLiveStudentkey);

       $totalSessionKey = "Total-Sessions";
       $totalSessionCount = $this->redi_model->getKey($totalSessionKey);

       $precounsellingKey = "Precounselling-Done-$userId";
       $precounsellingStatus = $this->redi_model->getKey($precounsellingKey);

       if(!$precounsellingStatus){

         $precounsellingKey = "Precounselling-Done-$userId";
         $setPrecounsellingStatus = $this->redi_model->setKey($precounsellingKey,"NO");
         $precounsellingStatus = "NO";

       }

       $fareNameKey = "Fair-Name";
       $fairName = $this->redi_model->getKey($fareNameKey);

       $precounsellingCheck = $this->webinar_model->preCounsellingAvailabilityCheck();

       foreach ($precounsellingCheck as $keypc => $valuepc) {
         if($valuepc['limit_of_student'] > $valuepc['currently_handling']){
           $precounsellingCheck[$keypc]['button_type'] = "JOIN";
         } else{
           $precounsellingCheck[$keypc]['button_type'] = "JOINQUEUE";
         }
        }

        $response['precounselling_list'] = $precounsellingCheck;
        $response['precounselling_status'] = $precounsellingStatus;

        $universityLiveCount = 65;
        $fairListNew = [];

      if($precounsellingStatus == "YES") {

        $checkFairObj = $this->redi_model->getKey("FAIR-USER-ID-$userId");
        $finalFairObj = json_decode($checkFairObj,true);

        if($finalFairObj){
          $fairListObj = $finalFairObj;
        } else {
          $fairListObj = $this->webinar_model->virtualFairParticipantsNew($userId,$fairId);
          $setFairListObj = $this->redi_model->setKey("FAIR-USER-ID-$userId",json_encode($fairListObj));
        }

       $universityLiveCount = 0;

       $totalRegistration = [];
       $trackedParticipants = [];
       $bookedUniversities = false;
       foreach ($fairListObj as $keyflo => $valueflo) {
           /*if(in_array($valueflo['id'], $trackedParticipants))
           {
               continue;
           }
           $trackedParticipants[] = $valueflo['id'];*/

         //$fairListObj[$keyflo]['fair_start_time'] = date('D, d F Y - h:i:s a', strtotime($valueflo['fair_start_time']));
         $fairListObj[$keyflo]['fair_start_time'] = date('h:i:s a', strtotime($valueflo['fair_start_time']));
         $fairListObj[$keyflo]['fair_end_time'] = date('h:i:s a', strtotime($valueflo['fair_end_time']));

            if($valueflo['book_type'] == "current_user_slot"){

              $fairListObj[$keyflo]['button_name'] = "JOIN";
              $fairListObj[$keyflo]['button_type'] = "ENABLED";

              $universityLiveCount = $universityLiveCount +1;
              $bookedUniversities = true;

            } else if($valueflo['book_type'] == "booked_user_slot"){

              $fairListObj[$keyflo]['button_name'] = "JOIN";
              $fairListObj[$keyflo]['button_type'] = "DISABLED";

              $universityLiveCount = $universityLiveCount +1;
              $bookedUniversities = true;

            } else if($valueflo['book_type'] == "university_active_slot"){

                $checkRegistration = $this->webinar_model->checkRegistration($userId, $fairId, $valueflo['id']);
                if($checkRegistration)
                {
                    $fairListObj[$keyflo]['book_type'] = 'already_attended';
                    $sacount = json_decode($valueflo['student_attending']);
                    if(count($sacount) < $valueflo['limit_of_student'])
                    {
                        $fairListObj[$keyflo]['button_name'] = "JOIN";
                        $fairListObj[$keyflo]['button_type'] = "ENABLED";
                    }
                    else
                    {
                        $fairListObj[$keyflo]['button_name'] = "JOIN";
                        $fairListObj[$keyflo]['button_type'] = "ENABLED";
                    }
                    $bookedUniversities = true;
                }
              else
              {
                  $key = 'SLOTS-' . $userId;
                  $bookedSlots = $this->redi_model->getKey($key);
                  $expired = false;
                  if($bookedSlots)
                  {
                      $bookedSlots = json_decode($bookedSlots, true);
                      $bookedSlotsString = implode(',', $bookedSlots);
                      $expiredSlots = $this->webinar_model->getExpiredSlots($bookedSlotsString, $userId);
                      if($expiredSlots)
                      {
                          foreach($expiredSlots as $expiredSlot)
                          {
                              if($expiredSlot['participant_id'] == $valueflo['id'])
                              {
                                  $fairListObj[$keyflo]['book_type'] = 'already_expired';
                                  $sacount = json_decode($valueflo['student_attending']);
                                  if($sacount < $valueflo['limit_of_student'])
                                  {
                                      $fairListObj[$keyflo]['button_name'] = "JOIN";
                                      $fairListObj[$keyflo]['button_type'] = "ENABLED";
                                  }
                                  else
                                  {
                                      $fairListObj[$keyflo]['button_name'] = "JOIN";
                                      $fairListObj[$keyflo]['button_type'] = "ENABLED";
                                  }
                                  $bookedUniversities = true;
                                  $expired = true;
                                  break;
                              }
                          }
                      }
                  }
                  if(!$expired)
                  {
                      $scount = json_decode($valueflo['student_booked']);
                      if($scount < $valueflo['limit_of_student']){
                        $fairListObj[$keyflo]['button_name'] = "JOIN";
                        $fairListObj[$keyflo]['button_type'] = "ENABLED";
                      }else{
                        $fairListObj[$keyflo]['button_name'] = "JOIN";
                        $fairListObj[$keyflo]['button_type'] = "ENABLED";
                      }
                  }
              }

              $universityLiveCount = $universityLiveCount +1;

            } else {
                $studentBooked = $valueflo['student_booked'] ? json_decode($valueflo['student_booked'], true) : [];
                if($studentBooked && in_array($userId, $studentBooked))
                {
                    $bookedUniversities = true;
                    $fairListObj[$keyflo]['book_type'] = "booked_user_slot";
                    $fairListObj[$keyflo]['button_name'] = "JOIN";
                    $fairListObj[$keyflo]['button_type'] = "DISABLED";
                }
              else{
                  $fairListObj[$keyflo]['button_name'] = "JOIN";
                  $fairListObj[$keyflo]['button_type'] = "ENABLED";
              }
            }

            if($valueflo['participant_type'] == "GROUP"){

              $universityIds = json_decode($valueflo['university_ids']);

              foreach ($universityIds as $keyids) {
                $getUniInfo = $this->webinar_model->uniInfoById($keyids);
                $universityName = $getUniInfo[0]['name'];
		$universityLogo = $getUniInfo[0]['logo'];

                $fairListObj[$keyflo]['university_name'] = $universityName;
		$fairListObj[$keyflo]['img'] = $universityLogo;

                $fairListObj[] = $fairListObj[$keyflo];

              }
              unset($fairListObj[$keyflo]);

            }
            //$fairListNew[] = $fairListObj[$keyflo];
       }

       $universityName = array_column($fairListObj, 'university_name');
       array_multisort($universityName, SORT_ASC, $fairListObj);

       $response['fairlist'] = $fairListObj;
     }

     //Eduloans Counselling
     $eduloanscounsellingCheck = $this->webinar_model->eduCounsellingAvailabilityCheck();

     foreach ($eduloanscounsellingCheck as $keyed => $valueed) {
       if($valueed['limit_of_student'] > $valueed['currently_handling']){
         $eduloanscounsellingCheck[$keyed]['button_type'] = "JOIN";
       } else{
         $eduloanscounsellingCheck[$keyed]['button_type'] = "JOINQUEUE";
       }
      }

      $response['edu_counselling_list'] = $eduloanscounsellingCheck;

      //Post counselling
      $postcounsellingCheck = $this->webinar_model->postCounsellingAvailabilityCheck();

      foreach ($postcounsellingCheck as $keypt => $valuept) {
        if($valuept['limit_of_student'] > $valuept['currently_handling']){
          $postcounsellingCheck[$keypt]['button_type'] = "JOIN";
        } else{
          $postcounsellingCheck[$keypt]['button_type'] = "JOINQUEUE";
        }
       }

       $response['post_counselling_list'] = $postcounsellingCheck;


       $response['fair_id'] = $fairId;
       $response['fair_name'] = $fairName;
       $response['university_live_count'] = $universityLiveCount;
       $response['fair_total_registration'] = $totalRegistrationCount;
       $response['total_live_student'] = $totalLiveStudentCount;
       $response['total_sessions'] = $totalSessionCount;
       $response['bookedUniversities'] = $bookedUniversities;

       header('Content-Type: application/json');
       echo (json_encode($response));

    }

    function availableSlots(){
      $this->load->helper('cookie_helper');

      $usrdata=$this->session->userdata('user');

 	     if(empty($usrdata)){
         redirect('user/account');
         exit();
       }

       $participantId = $this->input->post('participant_id');
       $userId = $usrdata['id'];

       $slotList = $this->webinar_model->availableSlots($participantId);

       $slotOption = [];
       $bookCount = 0 ;
       $response['slot_available'] = "YES";

       foreach ($slotList as $keysl => $valuesl) {
         $bookCount = count(json_decode($valuesl['student_booked']));
         $valuesl['is_full'] = 0;
         if($bookCount >= $valuesl['max_allowed_students']){
             $valuesl['is_full'] = 1;
         }
         $slotOption[] = $valuesl;

       }

       //var_dump($slotOption);

       if(count($slotOption) == 0){
         $response['slot_available'] = "NO";
       }

       $response['slot_list'] = $slotOption;
       header('Content-Type: application/json');
       echo (json_encode($response));

    }

    function applySlot(){

      $this->load->helper('cookie_helper');
      $this->load->model('redi_model');

      $usrdata=$this->session->userdata('user');

 	     if(empty($usrdata)){
         echo"NOTLOGGEDIN";
         exit();
       }

       $participantId = $this->input->post('participant_id');
       $slotId = $this->input->post('slot_id');
       $userId = $usrdata['id'];

       $slotInfoByIdCheck = $this->webinar_model->userSlotInfo($participantId,$userId);

       if($slotInfoByIdCheck){
         echo"ALREADY BOOKED SLOT ...!!!";
         exit();
       }

       $whereCondition = ["id" => $slotId];
       $slotInfoById = $this->common_model->get($whereCondition,"virtual_fair_slots");

       $slotStartTime = $slotInfoById['data'][0]['start_time'];

       $slotCheckObj = $this->webinar_model->checkSlotByUser($userId,$slotStartTime);

       if($slotCheckObj){
         echo"SLOT CONFLICT WITH ".$slotCheckObj[0]['university_name'] ." TIME - ".$slotStartTime;
         exit();
       }

				$studentBookedData = json_decode($slotInfoById['data'][0]['student_booked']) ? json_decode($slotInfoById['data'][0]['student_booked']) : [] ;
        $fixedCount = $slotInfoById['data'][0]['max_allowed_students'];

        if(count($studentBookedData) >= $fixedCount) { echo "SLOTFULL"; exit(); }

        array_push($studentBookedData, intval($userId));

        $studentBookedData = json_encode($studentBookedData);

        $updateDataSlot = ["student_booked" => $studentBookedData];
        $conditionSlotUpdate = ["id" => $slotId];

       $slotList = $this->common_model->put($conditionSlotUpdate,$updateDataSlot,"virtual_fair_slots");

       $conditionPUpdate = ["id" => $participantId];

       $getCountParticipants =  $this->common_model->get($conditionPUpdate,"virtual_fair_participants");

       $totalRegistration = $getCountParticipants['data'][0]['total_registrations'];
       $addTotalRegistration = $totalRegistration + 1;

       $updateDatap = ["total_registrations" => $addTotalRegistration];

       $updateCount = $this->common_model->put($conditionPUpdate,$updateDatap,"virtual_fair_participants");
       $key = 'SLOTS-' . $userId;
       $bookedSlots = $this->redi_model->getKey($key);
       if($bookedSlots)
       {
           $bookedSlots = json_decode($bookedSlots, true);
           if(!in_array($slotId, $bookedSlots))
           {
               $bookedSlots[] = $slotId;
           }
       }
       else
       {
           $bookedSlots[] = $slotId;
       }
       $bookedSlots = json_encode($bookedSlots);
       $this->redi_model->setKeyWithExpiry($key, $bookedSlots, 36000);

       echo "SUCCESS";

    }

    function deleteSlot(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

 	     if(empty($usrdata)){
         echo "NOTLOGGEDIN";
         exit();
       }

       $participantId = $this->input->post('participant_id');
       $userId = $usrdata['id'];

       $slotInfoById = $this->webinar_model->userSlotInfo($participantId,$userId);

       if($slotInfoById){

        $slotId = $slotInfoById[0]['id'];

				$studentBookedData = json_decode($slotInfoById[0]['student_booked']) ;

        $studentBookedData = array_diff($studentBookedData, array(intval($userId)));

        $studentBookedData = json_encode($studentBookedData);

        $updateDataSlot = ["student_booked" => $studentBookedData];
        $conditionSlotUpdate = ["id" => $slotId];

        $slotList = $this->common_model->put($conditionSlotUpdate,$updateDataSlot,"virtual_fair_slots");

       echo "DELETED";
     } else {
       echo "NODATAAVAILABLE";
     }

    }

    function joinUniversityFair(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

       if(empty($usrdata)){
         echo "NOTLOGGEDIN";
         exit();
       }


       $tokboxId = $this->input->post('token_id');
       echo "/meeting/virtualfair/attendee?id=" . base64_encode($tokboxId);

       /*$participantId = $this->input->post('participant_id');
       $userId = $usrdata['id'];

       $joinTokenObj = $this->webinar_model->userSlotInfo($participantId,$userId);

       if($joinTokenObj){
         $tokboxId = $joinTokenObj[0]['token_id'];
         echo "/meeting/virtualfair/attendee?id=" . base64_encode($tokboxId);
       } else {
         echo "NOTAVAILABLE";
     }*/

    }

    function upcomingUniversityFairSlotByUser(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

       if(empty($usrdata)){
         echo "NOTLOGGEDIN";
         exit();
       }

       $participantId = $this->input->post('participant_id');
       $userId = $usrdata['id'];

       $joinTokenObj = $this->webinar_model->userSlotInfo($userId);

       if($joinTokenObj){
         $tokboxId = $joinTokenObj[0]['token_id'];
         echo "/meeting/virtualfair/attendee?id=" . base64_encode($tokboxId);
       } else {
         echo "NOTAVAILABLE";
       }

    }

    function preCounsellingAvailability(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

 	     if(empty($usrdata)){
         echo "NOTLOGGEDIN";
         exit();
       }

       $precounsellingCheck = $this->webinar_model->preCounsellingAvailabilityCheck();

       if($precounsellingCheck){
         echo "AVAILABLE";
       } else {
         echo "NOTAVAILABLE";
       }

    }

    function joinPreCounselling(){

      $this->load->helper('cookie_helper');
      $this->load->model('redi_model');

      $usrdata=$this->session->userdata('user');

       if(empty($usrdata)){
         echo "NOTLOGGEDIN";
         exit();
       }

       $userId = $usrdata['id'];
       $fairId = $this->redi_model->getKey('FAIR-ID');

       $precounsellingCheck = $this->webinar_model->preCounsellingAvailabilityCheck();

       if($precounsellingCheck){

         $tokboxId = 0 ;
         $pid = 0 ;
         $cureentHandling = 0;
	       $availableList = [];

         foreach ($precounsellingCheck as $keypc => $valuepc) {
           if($valuepc['limit_of_student'] > $valuepc['currently_handling']){
             $availableList[] = $precounsellingCheck[$keypc];
           } else {
             $tokboxId = 0;
           }
          }

          if(!empty($availableList)){
            $selectCounsellor = $availableList[array_rand($availableList)];

            $tokboxId = $selectCounsellor['token_id'];
            $pid = $selectCounsellor['id'];
            $cureentHandling = $selectCounsellor['currently_handling'];
          }

          if($tokboxId == 0){

            $postDataUpert = ["fair_id" => $fairId, "user_id" => $userId, "status" => 0];
            $whereCondition = ["user_id" => $userId];

            $bookPrecounselling = $this->common_model->upsert($whereCondition,$postDataUpert,"virtualfailr_counselor_booking");
            echo "NOTAVAILABLEINQUEUE";

          } else {

          $precounsellingKey = "Precounselling-Done-$userId";
          $precounsellingStatus = $this->redi_model->setKey($precounsellingKey,"YES");

          $userId = $usrdata['id'];
          $postData = ["status" => '1'];
          $whereCondition = ["user_id" => $userId];
          $isPrecouselling = $this->common_model->get($whereCondition, 'virtualfailr_counselor_booking');

          if($isPrecouselling['data'])
          {
              $bookPrecounselling = $this->common_model->put($whereCondition,$postData,"virtualfailr_counselor_booking");
          }
          else
          {
              $postData["user_id"] = $userId;
              $bookPrecounselling = $this->common_model->post($postData,"virtualfailr_counselor_booking");
          }

          $currentHandlingNew = $cureentHandling +1;
          $postDatap = ["currently_handling" => $currentHandlingNew];
          $whereConditionp = ["id" => $pid];
          $bookPrecounselling = $this->common_model->put($whereConditionp,$postDatap,"virtual_fair_participants");

            echo "/meeting/virtualfair/attendee_pre_counselling?id=" . base64_encode($tokboxId);
          }

       } else {
         echo "NOTAVAILABLE";
       }

    }

    function bookPreCounselling(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

       if(empty($usrdata)){
         echo "NOTLOGGEDIN";
         exit();
       }

       $userId = $usrdata['id'];
       $fairId = $this->redi_model->getKey('FAIR-ID');
       $postData = ["fair_id" => $fairId, "user_id" => $userId, "status" => 0];
       $whereCondition = ["user_id" => $userId];
       $bookPrecounselling = $this->common_model->upsert($whereCondition, $postData, 'virtualfailr_counselor_booking');

       echo "SUCCESS";

    }

    function checkPreCounsellingQueue(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

       if(empty($usrdata)){
         echo "NOTLOGGEDIN";
         exit();
       }

       $userId = $usrdata['id'];
       $fairId = $this->redi_model->getKey('FAIR-ID');
       $whereCondition = ["user_id" => $userId];
       $checkPreCounsellingQueueObj = $this->webinar_model->queueCheck($fairId, 'virtualfailr_counselor_booking');

       if($checkPreCounsellingQueueObj[0]['user_id'] == $userId){
           $precounsellingCheck = $this->webinar_model->preCounsellingAvailabilityCheck();
           $counselorAvailable = 0;

           foreach ($precounsellingCheck as $keypc => $valuepc) {
             if($valuepc['limit_of_student'] > $valuepc['currently_handling']){
               $counselorAvailable = 1;
             }
            }
         if($counselorAvailable)
         {
             echo "JOINPRECOUNSELLING";
         }
         else
         {
             echo "NOTINQUEUE";
         }
       } else {
          echo "NOTINQUEUE";
       }

    }

    function bookedSlotReminder(){
      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');
      $this->load->model('redi_model');

       if(empty($usrdata)){
         echo "NOTLOGGEDIN";
         exit();
       }

       $userId = $usrdata['id'];

       $checkBookedSessions = $this->webinar_model->bookedSlotReminder($userId);

       if($checkBookedSessions){
         //echo $checkBookedSessions[0]['participant_id'];
         $startFairTime = $checkBookedSessions[0]['start_time'];
         $cdate = date('yy-m-d H:m:s');

          if($startFairTime > $cdate){
            echo"JOINCOMING";
          }else{
            echo"JOIN";
          }


       } else {
           $key = 'SLOTS-' . $userId;
           $bookedSlots = $this->redi_model->getKey($key);
           if($bookedSlots)
           {
               $bookedSlots = json_decode($bookedSlots, true);
               //TODO: check one by one
               $bookedSlotsString = implode(',', $bookedSlots);
               $expiredSlots = $this->webinar_model->getExpiredSlotsCount($bookedSlotsString, $userId);
               if($expiredSlots){
                   echo $expiredSlots->count;
               }
               else
               {
                   echo "NOJOIN";
               }
           }
           else
           {
               echo "NOJOIN";
           }
       }

    }

    function fairRegistrationCount(){

      $this->load->model('redi_model');

      $fairId = $this->redi_model->getKey('FAIR-ID');
      $registrationCount = $this->webinar_model->fairRegistrationCount($fairId);

      $count = $registrationCount[0]['total_count'];
      $fairName = $registrationCount[0]['fair_name'];

      $countKey = "Total-Registration";
      $fairNameKey = "Fair-Name";

      $value1 = $this->redi_model->setKey($countKey,$count);

      $value2 = $this->redi_model->setKey($fairNameKey,$fairName);

    }

    function fairLiveStudentCount(){

      $this->load->model('redi_model');

      $fairId = $this->redi_model->getKey('FAIR-ID');

      $RegistrationCount = $this->webinar_model->fairLiveStudentCount($fairId);
      $allStudenIds = [];
      $universityWiseCounts = [];

      foreach ($RegistrationCount as $keyrc => $valuerc) {
        array_push($allStudenIds,json_encode($valuerc['student_booked']),json_encode($valuerc['student_attending']));

        $universityWiseCounts[$valuerc['participant_id']][] = json_encode($valuerc['student_booked']);
        $universityWiseCounts[$valuerc['participant_id']][] = json_encode($valuerc['student_attending']);

      }

      $uniqueStudent = array_unique($allStudenIds);

      $liveStudent = count($uniqueStudent);

      $key = "Total-Live-Student";

      $value = $this->redi_model->setKey($key,$liveStudent);

      $array_meged=array();

      foreach ($universityWiseCounts as $keywc => $valuewc) {
        // code...
          $array_meged[$keywc] = count(array_unique(array_reduce($universityWiseCounts[$keywc], 'array_merge', [])));
      }

      foreach ($array_meged as $keyam => $value) {

         $value = $this->redi_model->setKey("University-Id-$keyam", $value);

          }

    }

    function totalFairSession(){

      $this->load->model('redi_model');

      $fairId = $this->redi_model->getKey('FAIR-ID');
      $RegistrationCount = $this->webinar_model->totalFairSession($fairId);

      $count = $RegistrationCount[0]['total_count'];

      $key = "Total-Sessions";

      $value = $this->redi_model->setKey($key,$count);

    }

    function totalCounselling(){

      $this->load->model('redi_model');

      $fairId = $this->redi_model->getKey('FAIR-ID');

      $totalpre = 0;
      $totalpredone = 0;
      $totalprenotdone = 0;

      $totalpost = 0;
      $totalpostdone = 0;
      $totalpostnotdone = 0;

      $totaledu = 0;
      $totaledudone = 0;
      $totaledunotdone = 0;

      $totalCounsellingCount = $this->webinar_model->totalCounselling($fairId);

      foreach ($totalCounsellingCount as $keytcc => $valuetcc) {

        if($valuetcc['c_type'] == 'pre' && $valuetcc['status'] == '1'){

          $totalpredone = $valuetcc['total_count'];
          $value1 = $this->redi_model->setKey('Total-Pre-Counselling-Done',$totalpredone);

        } elseif ($valuetcc['c_type'] == 'pre' && $valuetcc['status'] == '0') {

          $totalprenotdone = $valuetcc['total_count'];
          $value2 = $this->redi_model->setKey('Total-Pre-Counselling-Not-Done',$totalprenotdone);

        } elseif($valuetcc['c_type'] == 'post' && $valuetcc['status'] == '1'){

          $totalpostdone = $valuetcc['total_count'];
          $value3 = $this->redi_model->setKey('Total-Post-Counselling-Done',$totalpostdone);

        } elseif ($valuetcc['c_type'] == 'post' && $valuetcc['status'] == '0') {

          $totalpostnotdone = $valuetcc['total_count'];
          $value4 = $this->redi_model->setKey('Total-Post-Counselling-Not-Done',$totalpostnotdone);

        } elseif($valuetcc['c_type'] == 'edu' && $valuetcc['status'] == '1'){

          $totaledudone = $valuetcc['total_count'];
          $value5 = $this->redi_model->setKey('Total-Edu-Counselling-Done',$totaledudone);

        } elseif ($valuetcc['c_type'] == 'edu' && $valuetcc['status'] == '0') {

          $totaledunotdone = $valuetcc['total_count'];
          $value6 = $this->redi_model->setKey('Total-Edu-Counselling-Not-Done',$totaledunotdone);

        }

      }

      $totalpre = $totalpredone + $totalprenotdone ;
      $value7 = $this->redi_model->setKey('Total-Pre-Counselling',$totalpre);

      $value10 = $this->redi_model->setKey('Total-Attended',$totalpre);

      $totalpost = $totalpostdone + $totalpostnotdone ;
      $value8 = $this->redi_model->setKey('Total-Post-Counselling',$totalpost);

      $totaledu = $totaledudone + $totaledunotdone ;
      $value9 = $this->redi_model->setKey('Total-Edu-Counselling',$totaledu);


    }

    //educounselling codes
    function joinEduCounselling(){

      $this->load->helper('cookie_helper');
      $this->load->model('redi_model');

      $usrdata=$this->session->userdata('user');

       if(empty($usrdata)){
         echo "NOTLOGGEDIN";
         exit();
       }

       $userId = $usrdata['id'];
       $fairId = $this->redi_model->getKey('FAIR-ID');

       $precounsellingCheck = $this->webinar_model->eduCounsellingAvailabilityCheck();

       if($precounsellingCheck){

         $tokboxId = 0 ;
         $pid = 0 ;
         $cureentHandling = 0;
	       $availableList = [];

         foreach ($precounsellingCheck as $keypc => $valuepc) {
           if($valuepc['limit_of_student'] > $valuepc['currently_handling']){
             $availableList[] = $precounsellingCheck[$keypc];
           } else {
             $tokboxId = 0;
           }
          }

          if(!empty($availableList)){
            $selectCounsellor = $availableList[array_rand($availableList)];

            $tokboxId = $selectCounsellor['token_id'];
            $pid = $selectCounsellor['id'];
            $cureentHandling = $selectCounsellor['currently_handling'];
          }

          if($tokboxId == 0){

            $postDataUpert = ["fair_id" => $fairId, "user_id" => $userId, "status" => 0];
            $whereCondition = ["user_id" => $userId];

            $bookPrecounselling = $this->common_model->upsert($whereCondition,$postDataUpert,"virtualfailr_edu_counselor_booking");
            echo "NOTAVAILABLEINQUEUE";

          } else {

          $userId = $usrdata['id'];
          $postData = ["status" => '1'];
          $whereCondition = ["user_id" => $userId];

          $isPrecouselling = $this->common_model->get($whereCondition, 'virtualfailr_edu_counselor_booking');

          if($isPrecouselling['data'])
          {
              $bookPrecounselling = $this->common_model->put($whereCondition,$postData,"virtualfailr_edu_counselor_booking");
          }
          else
          {
              $postData["user_id"] = $userId;
              $bookPrecounselling = $this->common_model->post($postData,"virtualfailr_edu_counselor_booking");
          }

          $currentHandlingNew = $cureentHandling +1;
          $postDatap = ["currently_handling" => $currentHandlingNew];
          $whereConditionp = ["id" => $pid];
          $bookPrecounselling = $this->common_model->put($whereConditionp,$postDatap,"virtual_fair_participants");

            echo "/meeting/virtualfair/attendee_edu_counselling?id=" . base64_encode($tokboxId);
          }

       } else {
         echo "NOTAVAILABLE";
       }

    }

    function bookEduCounselling(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

       if(empty($usrdata)){
         echo "NOTLOGGEDIN";
         exit();
       }

       $userId = $usrdata['id'];
       $fairId = $this->redi_model->getKey('FAIR-ID');
       $postData = ["fair_id" => $fairId, "user_id" => $userId, "status" => 0];
       $whereCondition = ["user_id" => $userId];
       $bookPrecounselling = $this->common_model->upsert($whereCondition, $postData, 'virtualfailr_edu_counselor_booking');

       echo "SUCCESS";

    }

    function checkEduCounsellingQueue(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

       if(empty($usrdata)){
         echo "NOTLOGGEDIN";
         exit();
       }

       $userId = $usrdata['id'];
       $fairId = $this->redi_model->getKey('FAIR-ID');
       $whereCondition = ["user_id" => $userId];
       $checkPreCounsellingQueueObj = $this->webinar_model->queueCheck($fairId, 'virtualfailr_edu_counselor_booking');

       if($checkPreCounsellingQueueObj[0]['user_id'] == $userId){

         $educounsellingCheck = $this->webinar_model->eduCounsellingAvailabilityCheck();
         $counselorAvailable = 0;

         foreach ($educounsellingCheck as $keypc => $valuepc) {
           if($valuepc['limit_of_student'] > $valuepc['currently_handling']){
             $counselorAvailable = 1;
           }
          }

       if($counselorAvailable)
       {
           echo "JOINEDUCOUNSELLING";
       }
       else
       {
           echo "NOTINQUEUE";
       }

      } else {
          echo "NOTINQUEUE";
       }

    }

    //postcounselling codes
    function joinPostCounselling(){

      $this->load->helper('cookie_helper');

      $usrdata=$this->session->userdata('user');

       if(empty($usrdata)){
         echo "NOTLOGGEDIN";
         exit();
       }

       $userId = $usrdata['id'];
       $fairId = $this->redi_model->getKey('FAIR-ID');

       $precounsellingCheck = $this->webinar_model->postCounsellingAvailabilityCheck();

       if($precounsellingCheck){

         $tokboxId = 0 ;
         $pid = 0 ;
         $cureentHandling = 0;
	       $availableList = [];

         foreach ($precounsellingCheck as $keypc => $valuepc) {
           if($valuepc['limit_of_student'] > $valuepc['currently_handling']){
             $availableList[] = $precounsellingCheck[$keypc];
           } else {
             $tokboxId = 0;
           }
          }

          if(!empty($availableList)){
            $selectCounsellor = $availableList[array_rand($availableList)];

            $tokboxId = $selectCounsellor['token_id'];
            $pid = $selectCounsellor['id'];
            $cureentHandling = $selectCounsellor['currently_handling'];
          }

          if($tokboxId == 0){

            $postDataUpert = ["fair_id" => $fairId, "user_id" => $userId, "status" => 0];
            $whereCondition = ["user_id" => $userId];

            $bookPrecounselling = $this->common_model->upsert($whereCondition,$postDataUpert,"virtualfailr_post_counselor_booking");
            echo "NOTAVAILABLEINQUEUE";

          } else {

          $userId = $usrdata['id'];
          $postData = ["status" => '1'];
          $whereCondition = ["user_id" => $userId];

          $isPrecouselling = $this->common_model->get($whereCondition, 'virtualfailr_post_counselor_booking');

          if($isPrecouselling['data'])
          {
              $bookPrecounselling = $this->common_model->put($whereCondition,$postData,"virtualfailr_post_counselor_booking");
          }
          else
          {
              $postData["user_id"] = $userId;
              $bookPrecounselling = $this->common_model->post($postData,"virtualfailr_post_counselor_booking");
          }

          $currentHandlingNew = $cureentHandling +1;
          $postDatap = ["currently_handling" => $currentHandlingNew];
          $whereConditionp = ["id" => $pid];
          $bookPrecounselling = $this->common_model->put($whereConditionp,$postDatap,"virtual_fair_participants");

            echo "/meeting/virtualfair/attendee_post_counselling?id=" . base64_encode($tokboxId);
          }

       } else {
         echo "NOTAVAILABLE";
       }

    }

    function bookPostCounselling(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

       if(empty($usrdata)){
         echo "NOTLOGGEDIN";
         exit();
       }

       $userId = $usrdata['id'];
       $fairId = $this->redi_model->getKey('FAIR-ID');
       $postData = ["fair_id" => $fairId, "user_id" => $userId, "status" => 0];
       $whereCondition = ["user_id" => $userId];
       $bookPrecounselling = $this->common_model->upsert($whereCondition, $postData, 'virtualfailr_post_counselor_booking');

       echo "SUCCESS";

    }

    function checkPostCounsellingQueue(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

       if(empty($usrdata)){
         echo "NOTLOGGEDIN";
         exit();
       }

       $userId = $usrdata['id'];
       $fairId = $this->redi_model->getKey('FAIR-ID');
       $whereCondition = ["user_id" => $userId];
       $checkPreCounsellingQueueObj = $this->webinar_model->queueCheck($fairId, 'virtualfailr_post_counselor_booking');

       if($checkPreCounsellingQueueObj[0]['user_id'] == $userId){

         $postcounsellingCheck = $this->webinar_model->postCounsellingAvailabilityCheck();
         $counselorAvailable = 0;

         foreach ($postcounsellingCheck as $keypc => $valuepc) {
           if($valuepc['limit_of_student'] > $valuepc['currently_handling']){
             $counselorAvailable = 1;
           }
          }

       if($counselorAvailable)
       {
           echo "JOINPOSTCOUNSELLING";
       }
       else
       {
           echo "NOTINQUEUE";
       }

       } else {
          echo "NOTINQUEUE";
       }

    }

    function checkUpcomingFair(){

      $this->load->model('redi_model');

      $checkNUpdate = $this->webinar_model->checkUpcomingFair();

      $fairName = $checkNUpdate[0]['fair_name'];
      $fairId = $checkNUpdate[0]['fair_id'];

      $storeFairName = $this->redi_model->setKey('FAIR-NAME',$fairName);

      $storeFairId = $this->redi_model->setKey('FAIR-ID',$fairId);

    }

    function updateUserFairInfo(){

      $fairId = $this->redi_model->getKey('FAIR-ID');

      $fairUserInfo = $this->webinar_model->userFairInfo($fairId);

      foreach ($fairUserInfo as $keyfui => $valuefui) {

        $userId = $valuefui['user_id'];

        $checkNUpdate = $this->webinar_model->virtualFairParticipantsNew($userId,$fairId);

        $storeFairName = $this->redi_model->setKey("FAIR-USER-ID-$userId",json_encode($checkNUpdate));

      }

    }

}
