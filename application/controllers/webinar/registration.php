<?php

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

require_once APPPATH . 'libraries/Mail/sMail.php';
//require_once APPPATH . 'libraries/codeigniter-predis/src/Redis.php';


class Registration extends MY_Controller {

      public $outputData;		//Holds the output data for each view
      public $loggedInUser;


	 function __construct()

    {

        parent::__construct();

        $this->load->library('template');

        $this->lang->load('enduser/home', $this->config->item('language_code'));

		$this->load->library('form_validation');

		$this->load->model('location/country_model');

    $this->load->model('webinar/webinar_model');

    $this->load->model('webinar/common_model');

     $this->load->model('course/course_model');

		$this->load->model('course/degree_model');

    $this->load->model('user/user_model');

      //  track_user();


    }

    function registrationForm(){

    $this->load->model('redi_model');
    $value = $this->redi_model->getKey('Role-1');
    $userObj = json_decode($value, true);

    //var_dump($userObj);
    $webinarId = $this->uri->segment('4');

      $outputData = [];

      $this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));

  		$this->outputData['mainstreams'] = $this->course_model->getAllCollegeCategories();

      $this->outputData['mainstreams'] = $this->course_model->getAllCollegeCategories();

  		$this->outputData['degrees'] = $this->degree_model->getDegree(array('degree_master.status'=>1));

      $this->outputData['webinar_id'] = $webinarId;


        $this->render_page('templates/webinar/registration_form',$this->outputData);

    }

    function registrationsignup(){

      $userName     	= $this->input->post('user_name');
      $userEmail 		  = $this->input->post('user_email');
      $userMobile 	  = $this->input->post('user_mobile');
      $userPassword	  = $this->input->post('user_password');
      $userCountry 		= $this->input->post('desired_country');
      $userMainstream = $this->input->post('desired_mainstream');
      $userSubcourse 	= $this->input->post('desired_subcourse');
      $userLevelofcourse 		= $this->input->post('desired_levelofcourse');
      $webinarId      = $this->input->post('webinar_id');

      $checkMobileVerified = $this->webinar_model->checkMasterOtp($userMobile);

      if(!$checkMobileVerified){
        echo "Mobile Number Not Verified ..!!!";
        exit();
      }

      $email = $this->input->post('user_email');
      $userId = $this->input->post('user_id');

      $checkUserExist = $this->webinar_model->checkUserLogin($userEmail,$userMobile);

      if($checkUserExist){
        echo "With Email-ID Or Mobile Account Already Exists";
        exit();
      }

      $checkUserIDExist = $this->webinar_model->checkUserIDLogin($userId);

      if($checkUserIDExist){
        echo "User Name Already Exists. Please Select Different User Name ...!!!";
        exit();
      }

 	 $data = array(

 			'name' 			=>  $this->input->post('user_name'),
 			'email' 		=>  $this->input->post('user_email'),
 			'phone' 		=>  $this->input->post('user_mobile'),
 			'mainstream'   => $this->input->post('desired_mainstream'),
 			'courses' 		=> $this->input->post('desired_subcourse'),
 			'degree' 		=> $this->input->post('desired_levelofcourse'),
 			'countries'    => $this->input->post('desired_country')

 		);

 	 if($data){
     $leadId = $this->user_model->insertLead($data);
 	 }

 	  $sessiondata = array(
      'id' 			=> $leadId,
 	    'name'			=> $this->input->post('user_name'),
      'email'		=> $this->input->post('user_email'),
      'phone' 		=> $this->input->post('user_mobile'),
 	    'mainstream'   => $this->input->post('desired_mainstream'),
 	    'coursecategories' 		=> $this->input->post('desired_subcourse'),
      'degree' 		=> $this->input->post('desired_levelofcourse'),
 	    'countries'    => $this->input->post('desired_country'),
      'password'     => md5($this->input->post('user_password'))
      );


      $this->session->set_userdata('leaduser', $sessiondata);

      $data = array(

          'name' 			=>  $this->input->post('user_name'),
          'username'=> $this->input->post('user_id'),
          'email' 		=>  $this->input->post('user_email'),
          'password' 		=>  md5($this->input->post('user_password')),
          'image'			=>  '',
          'type' 			=>  '5',
          'createdate' 	=>  date('Y-m-d H:i:s'),
          'activation' 	=>	md5(time()),
          'fid'			=>  '',
          'registration_type' 		=>	'WEBINAR',
          'registration_id'       =>  $webinarId,
          'status' 		=>	'1',
          'phone'        => $this->input->post('user_mobile')

      );



      $user_id = $this->user_model->insertUserMaster($data);

      $session_data = array(

         'id' => $user_id,

         'name'=> $this->input->post('user_name'),
         'username'=> $this->input->post('user_id'),

         'email' => $this->input->post('user_email'),

         'type' => 5,

         'typename' => 'Student',

         'fid' =>'',

         'is_logged_in' => true

     );

     $this->session->set_userdata('user', $session_data);




      $student_data = array(

          'user_id' 				=>	$user_id,

          'phone'        => $this->input->post('user_mobile'),

          'facilitator_id' 				=>	1,

          'originator_id' 				=>	$user_id

      );

      $this->user_model->insertStudentDetails($student_data);


      $studentName = $this->input->post('user_name');
      $studentEmail = $this->input->post('user_email');
      $username = $this->input->post('user_id');
      $password = $this->input->post('user_password');
      $mailSubject = "HelloUni - Thank you for Registering for the Webinar Series";
      $mailTemplate = "Hi $studentName,

                        Thank you for registering with HelloUni!

                        At HelloUni, we help you connect with institutions so that you can obtain clear and honest feedback on your questions and doubts directly from the University!

                        We hope you obtain maximum benefit from HelloUni, we sincerely applaud your aspirations and will do our utmost to help you reach your goals!

                        Your Login Credentials :

                        Username : $username
                        Password : $password

                        Regards,
                        Team HelloUni
                        Tel: +91 81049 09690
                        Email: info@hellouni.org

                         <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

      $ccMailList = '';
      $mailAttachments = '';

      //$studentEmail = 'nikhil.gupta@issc.in';

      $sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

      if($webinarId){
        $applyWebinar = $this->webinarapplyRegistration($webinarId);
      }

      if($sendMail){
        echo "SUCCESS";
      } else {
        echo "SUCCESS";
      }

}

 private function webinarapplyRegistration($webinarId){

  $usrdata=$this->session->userdata('user');

   $userId = $usrdata['id'];
   $webinarId = $webinarId;

   $webinarLogData = [
     "webinar_id" => $webinarId,
     "user_id" => $userId,
     "webinar_applied" => 1
   ];

   $webinarObj = $this->common_model->post($webinarLogData,"webinar_log_master");

   $whereId = ["id"=> $webinarId];
   $webinarInfoObj = $this->common_model->get($whereId,"webinar_master");

   $studentName = $usrdata['name'];
   $studentEmail = $usrdata['email'];
   $webinarName = $webinarInfoObj['data'][0]['webinar_name'];
   $webinarDate = $webinarInfoObj['data'][0]['webinar_date'];
   $webinarTime = $webinarInfoObj['data'][0]['webinar_time'];

   $webinarDate = date('D, d F Y', strtotime($webinarDate));

   $mailSubject = "HelloUni - You have Registered for the Webinar: $webinarName";
   $mailTemplate = "Hi $studentName,

                      Thank you for registering for the Webinar: $webinarName.

                      We are glad to have you participate in the session!

                      Please see details below to join the Webinar:

                      1. You will receive a mail containing the joining link 30 minutes before the webinar begins.

                      2. You can also join the session by logging in to hellouni.org and on the Webinar Page, click on the Join button for your chosen Webinar.

                      Date - $webinarDate
                      Time - $webinarTime

                      Regards,
                      Team HelloUni
                      Tel: +91 81049 09690
                      Email: info@hellouni.org


                      <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

   $ccMailList = '';
   $mailAttachments = '';

   //$studentEmail = 'nikhil.gupta@issc.in';

   $sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

   if($sendMail){
     echo "SUCCESS";
   } else {
     echo "SUCCESS";
   }

}

    function sentotp(){

      $mobileNumber = $this->input->post('mobile_number') ? $this->input->post('mobile_number') : 0;
      $userEmail = $this->input->post('user_email') ? $this->input->post('user_email') : 0;

      if(!($mobileNumber) || strlen($mobileNumber) != 10)
        {
          echo "Please provide valid mobile number !!!";
          exit();
        }

        $mobileAlreadyAvailable = $this->webinar_model->checkLogin($mobileNumber, $userEmail);

        if($mobileAlreadyAvailable){
            echo "Mobile Number Already Registered";
            exit();
        }

        $mobileAlreadyVerified = $this->webinar_model->checkMasterOtp($mobileNumber);

        if($mobileAlreadyVerified){
            echo "Mobile Number Already Verified. Fill other field and submit Form..!!!";
            exit();
        }


        $otp = mt_rand(100000, 999999);
        $insertData =["mobile_number" => $mobileNumber, "otp" => $otp];
        $whereMobile = ["mobile_number" => $mobileNumber];
        $isexists = $this->webinar_model->insertotp($whereMobile, $insertData,"otp_master");

        if(filter_var($userEmail, FILTER_VALIDATE_EMAIL)){

          $mailSubject = "HelloUni - OTP Verification Code !";
          $mailTemplate = "Hi User,

                             Please use this OTP to validate your login on Hellouni - $otp

                             Regards,
                             Team HelloUni
                             Tel: +91 81049 09690
                             Email: info@hellouni.org


                             <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

          $ccMailList = '';
          $mailAttachments = '';

          //$studentEmail = 'nikhil.gupta@issc.in';

          $sendMail = sMail($userEmail, 'User', $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);
        }

        echo "OTP Sent ON Given MObile Number AND Email-ID";

    }

    function verifyotp(){

      $mobileNumber = $this->input->post('mobile_number') ?  $this->input->post('mobile_number') : 0;
      $otp = $this->input->post('otp') ?  $this->input->post('otp') : 0;

      if( !($otp) || strlen($otp) != 6)  {
          echo "OTP Is Missing OR Invalid OTP";
          exit();
        }

        if(!($mobileNumber))  {
          echo "Mobile Number Is Missing";
          exit();
        }

        $isexists = $this->webinar_model->checkMobileOTP($mobileNumber, $otp);

        if(!$isexists)  {
          echo "Mobile Number Or OTP is Invalid";
          exit();
        }

        $whereCondition = ['mobile_number' => $mobileNumber, 'otp' => $otp];
        $otpMasterUpdate = ['status' => 1];

        $updateMobileOTP = $this->common_model->put($whereCondition, $otpMasterUpdate, 'otp_master');

        echo "Successfully Verified ...!!!";

    }


    function webinarlist(){

      $this->$outputData = [];

      $this->load->helper('cookie_helper');

      $usrdata=$this->session->userdata('user');
      $userId = 0;

 	     if(empty($usrdata)){

         $webinarList = $this->webinar_model->webinar_list($userId);

       } else {

         $userId = $usrdata['id'];

         $webinarList = $this->webinar_model->webinar_list($userId);

       }

       $this->$outputData['webinar_list'] = $webinarList;

       $this->render_page('templates/webinar/webinar_list',$this->outputData);

    }

    function webinarapply(){

      $this->load->helper('cookie_helper');

      $usrdata=$this->session->userdata('user');

 	     if(empty($usrdata)){
         redirect('user/account');
       }

       $userId = $usrdata['id'];
       $webinarId = $webinarId = $otp = $this->input->post('webinar_id') ?  $this->input->post('webinar_id') : 0;

       $webinarLogData = [
         "webinar_id" => $webinarId,
         "user_id" => $userId,
         "webinar_applied" => 1
       ];

       $webinarObj = $this->common_model->post($webinarLogData,"webinar_log_master");

       echo "Applied successfully ..!!!";
    }

    function webinarjoin(){

      $this->load->helper('cookie_helper');

      $usrdata=$this->session->userdata('user');

 	     if(empty($usrdata)){
         redirect('user/account');
       }

       $userId = $usrdata['id'];
       $webinarId = $otp = $this->input->post('webinar_id') ?  $this->input->post('webinar_id') : 0;

       $webinarLogData = [
         "webinar_attented" => 1
       ];

       $whereWebinarlog = [
         "webinar_id" => $webinarId,
         "user_id" => $userId
     ];

       $webinarObj = $this->common_model->put($whereWebinarlog,$webinarLogData,"webinar_log_master");

       echo "Applied successfully ..!!!";
    }

    function webinarrating(){

      $this->load->helper('cookie_helper');

      $usrdata=$this->session->userdata('user');

 	     if(empty($usrdata)){
         redirect('user/account');
       }

       $userId = $usrdata['id'];
       $webinarId = $otp = $this->input->post('webinar_id') ?  $this->input->post('webinar_id') : 0;
       $webinarRating = $otp = $this->input->post('webinar_rating') ?  $this->input->post('webinar_rating') : 4;

       $webinarLogData = [
         "webinar_rating" => $webinarRating
       ];

       $whereWebinarlog = [
         "webinar_id" => $webinarId,
         "user_id" => $userId
     ];

       $webinarObj = $this->common_model->put($whereWebinarlog,$webinarLogData,"webinar_log_master");

       echo "Applied successfully ..!!!";
    }

    function eduFairrRegistrationForm1(){

      $fairId = $this->uri->segment('3') ? $this->uri->segment('3') : 1;
      //$fairId = 1;

      $otherCollege[0]['id'] = '';
      $otherCollege[0]['city'] = '';
      $otherCollege[0]['college'] = 'Other';
      $otherCollege[0]['university'] = 'Other';
      $indianColleges = $this->user_model->getIndianColleges();
      $allIndianColleges = array_merge($otherCollege, $indianColleges);
      $this->outputData['colleges'] = $allIndianColleges;

      $this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));

  		$this->outputData['mainstreams'] = $this->course_model->getAllCollegeCategories();

      $this->outputData['mainstreams'] = $this->course_model->getAllCollegeCategories();

  		$this->outputData['degrees'] = $this->degree_model->getDegree(array('degree_master.status'=>1));

      $this->outputData['fair_id'] = $fairId;

      $this->render_page('templates/webinar/eduFairRegistration_form',$this->outputData);

    }

    function LoanMelaRegistrationForm(){

      $utmSource = isset($_GET['utm_source']) ? $_GET['utm_source'] : NULL;

      $this->outputData['utm_source'] = $utmSource;
      $this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));


      $this->render_page('templates/webinar/LoanMelaRegistrationForm',$this->outputData);

    }

    function EarlybirdRegistration(){

      // $utmSource = isset($_GET['utm_source']) ? $_GET['utm_source'] : NULL;

      // $this->outputData['utm_source'] = $utmSource;
      // $this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));

      $this->render_page('templates/webinar/EarlybirdRegistrationForm');

    }

    function UkIerlandRegistrationForm(){

       $utmSource = isset($_GET['utm_source']) ? $_GET['utm_source'] : NULL;

       $this->outputData['utm_source'] = $utmSource;
       $this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));


      $this->render_page('templates/webinar/UK_Ierland_Registration');

    }

    function eduloansFairRegistration(){

      $userFirstName     	= $this->input->post('user_first_name');
      $userLastName     	= $this->input->post('user_last_name');
      $userEmail 		  = $this->input->post('user_email');
      $userMobile 	  = $this->input->post('user_mobile');
      $userCountry 		= $this->input->post('user_desired_country');
      $userCity 		= $this->input->post('user_city');
      $userLoanType = $this->input->post('user_loan_type');
      $userIntakeYear 	= $this->input->post('user_intake_year');
      $utmSource 		= $this->input->post('utm_source');

      $loanMelaDay = "22012022";

      $checkMobileVerified = $this->webinar_model->checkMasterOtp($userMobile);

      if(!$checkMobileVerified){

        $response['status'] = "FAILED";
        $response['msg'] = "Mobile Number Not Verified ..!!!";

        header('Content-Type: application/json');
        echo (json_encode($response));
        exit();
      }


      $checkEduloanDay = $this->webinar_model->checkEduloansUser($userMobile,$loanMelaDay);

      if($checkEduloanDay){

        $response['status'] = "FAILED";
        $response['msg'] = "Already Registered For Loan Mela ..!!!";

        header('Content-Type: application/json');
        echo (json_encode($response));
        exit();
      }

      $postData = [
        'first_name' => $userFirstName,
        'last_name' => $userLastName,
        'email' => $userEmail,
        'mobile_number' => $userMobile,
        'city_name' => $userCity,
        'country_name' => $userCountry,
        'intake_year' => $userIntakeYear,
        'loan_type' => $userLoanType,
        'utm_source' => $utmSource,
        'loan_mela_day' => $loanMelaDay
      ];

      $insertObj = $this->webinar_model->common_model->post($postData, 'eduloans_fair_user');

      $response['status'] = "SUCCESS";
      $response['msg'] = "Registered For Loan Mela ..!!!";

      header('Content-Type: application/json');
      echo (json_encode($response));
      exit();

    }

    function fairRegistration(){

      $this->load->model('redi_model');
      $fairId = $this->redi_model->getKey('FAIR-ID');

      $userName     	= $this->input->post('user_name');
      $userEmail 		  = $this->input->post('user_email');
      $userMobile 	  = $this->input->post('user_mobile');
      $userPassword	  = $this->input->post('user_password');
      $userCountry 		= $this->input->post('desired_country');
      $userMainstream = $this->input->post('desired_mainstream');
      $userSubcourse 	= $this->input->post('desired_subcourse');
      $userLevelofcourse 		= $this->input->post('desired_levelofcourse');
      //$fairId      = $this->input->post('fair_id') ? $this->input->post('fair_id') : 1;

      //$checkMobileVerified = $this->webinar_model->checkMasterOtp($userMobile);
      $checkMobileVerified = 1;

      if(!$checkMobileVerified){
        echo "Mobile Number Not Verified ..!!!";
        exit();
      }

      $email = $this->input->post('user_email');
      $userId = $this->input->post('user_id');

      $checkUserExist = $this->webinar_model->checkUserLogin($userEmail,$userMobile);

      if($checkUserExist){
        echo "With Email-ID Or Mobile Account Already Exists";
        exit();
      }

      $checkUserIDExist = $this->webinar_model->checkUserIDLogin($userId);

      if($checkUserIDExist){
        echo "User Name Already Exists. Please Select Different User Name ...!!!";
        exit();
      }

 	 $data = array(

 			'name' 			=>  $this->input->post('user_name'),
 			'email' 		=>  $this->input->post('user_email'),
 			'phone' 		=>  $this->input->post('user_mobile'),
      'degree' 		=> $this->input->post('desired_levelofcourse'),
 			'mainstream'   => $this->input->post('desired_mainstream'),
 			'courses' 		=> $this->input->post('desired_subcourse'),
 			'countries'    => $this->input->post('desired_country')
 		);

 	 if($data){
     $leadId = $this->user_model->insertLead($data);
 	 }

 	  $sessiondata = array(
      'id' 			=> $leadId,
 	    'name'			=> $this->input->post('user_name'),
      'email'		=> $this->input->post('user_email'),
      'phone' 		=> $this->input->post('user_mobile'),
 	    'mainstream'   => $this->input->post('desired_mainstream'),
 	    'coursecategories' 		=> $this->input->post('desired_subcourse'),
      'degree' 		=> $this->input->post('desired_levelofcourse'),
 	    'countries'    => $this->input->post('desired_country'),
      'password'     => md5($this->input->post('user_password'))
      );


      $this->session->set_userdata('leaduser', $sessiondata);

      $data = array(

          'name' 			=>  $this->input->post('user_name'),
          'username'=> $this->input->post('user_id'),
          'email' 		=>  $this->input->post('user_email'),
          'password' 		=>  md5($this->input->post('user_password')),
          'image'			=>  '',
          'type' 			=>  '5',
          'createdate' 	=>  date('Y-m-d H:i:s'),
          'activation' 	=>	md5(time()),
          'fid'			=>  '',
          'registration_type' 		=>	'FAIR',
          'registration_id'       =>  $fairId,
          'status' 		=>	'1',
          'phone'        => $this->input->post('user_mobile')

      );



      $user_id = $this->user_model->insertUserMaster($data);

      $session_data = array(

         'id' => $user_id,

         'name'=> $this->input->post('user_name'),
         'username'=> $this->input->post('user_id'),

         'email' => $this->input->post('user_email'),

         'type' => 5,

         'typename' => 'Student',

         'fid' =>'',

         'is_logged_in' => true

     );

     $this->session->set_userdata('user', $session_data);

      $student_data = array(

          'user_id' 				=>	$user_id,

          'phone'        => $this->input->post('user_mobile'),

          'facilitator_id' 				=>	1,

          'current_city' => $this->input->post('ccity'),

          'current_school' => $this->input->post('cschool') != 'Other' ? $this->input->post('cschool') : $this->input->post('other_cschool'),

          'intake_month' => $this->input->post('intake_month'),
          'intake_year' => $this->input->post('intake_year'),
          'comp_exam' => $this->input->post('comp_exam'),
          'comp_exam_score' => $this->input->post('comp_exam_score'),
          'gpa_exam' => $this->input->post('gpa_exam'),
          'gpa_exam_score' => $this->input->post('gpa_exam_score'),
          'work_exp' => $this->input->post('work_exp'),
          'work_exp_month' => $this->input->post('work_exp_month'),

          'originator_id' 				=>	$user_id

      );

      $this->user_model->insertStudentDetails($student_data);

      if($fairId){

          $webinarLogData = [
            "fair_id" => $fairId,
            "user_id" => $user_id
          ];

          $webinarObj = $this->common_model->post($webinarLogData,"fair_log_master");
      }

      $whereId = ["id"=> $fairId];

      $webinarInfoObj = $this->common_model->get($whereId,"fair_master");

      $webinarName = $webinarInfoObj['data'][0]['name'];
      $webinarDate = $webinarInfoObj['data'][0]['fair_date'];
      $webinarTime = $webinarInfoObj['data'][0]['fair_time'];

      $webinarDate = date('D, d F Y', strtotime($webinarDate));

      $studentName = $this->input->post('user_name');
      $studentEmail = $this->input->post('user_email');
      $username = $this->input->post('user_id');
      $password = $this->input->post('user_password');
      $encryptedUsername = base64_encode($username);

      $mailSubject = "You are registered for $webinarName";
      $mailTemplate = "Dear $studentName,

                        Thank you for registering for the $webinarName

                        Your Username is: $username
                        Your Password is: $password


                          In order to help you navigate the Virtual Fair and make best use of the platform, we have included a few instructions below:

                          1. Please make sure to use your Laptop to attend this virtual fair
                          2. Login to your <a href='https://www.hellouni.org/user/account'>HelloUni Account</a> using the above credentials.
                          3. Click on- Attend the Virtual Fair icon
                          4. Identify the universities you wish to connect with & start interacting!
                          5. Avail fee waivers, announcements on scholarships and decisions and get all your queries answered!

                        Virtual Fair Schedule:

                        Date : $webinarDate
                        Time : $webinarTime

                        You can also invite your friends, batchmates, seniors and juniors as well by clicking on Invite Friends!

                        Regards,
                        Team HelloUni
                        Tel: +91 81049 09690
                        Email: info@hellouni.org

                         <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

      $ccMailList = '';
      $mailAttachments = '';


      $sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

      if($sendMail){
        echo "SUCCESS";
      } else {
        echo "SUCCESS";
      }

}

//university registration
function universityForm(){

  $this->render_page('templates/webinar/university_registration_form',$this->outputData);

}

function universitySubmit(){


  $universityName = $this->input->post('university_name');
  $userName     	= $this->input->post('user_name');
  $userEmail 		  = $this->input->post('user_email');
  $userMobile 	  = $this->input->post('user_mobile');
  $upcomingvr	    = $this->input->post('upcomingvr');

  $emailCheck = $this->common_model->get(["email" => $userEmail],"university_registration");

  if($emailCheck['data']){
    echo "Email-Id Already Registered For LUCKY DRAW...!!!";
    exit();
  }

  $registerData = [
    "university_name" => $universityName,
    "name" => $userName,
    "email" => $userEmail,
    "mobile_number" => $userMobile,
    "vr" => $upcomingvr
  ];

  $webinarObj = $this->common_model->post($registerData,"university_registration");

  $mailSubject = "Thank you for registering for the Lucky Draw! | HelloUni.org";
  $mailTemplate = '<div style="width:100%; font-family: verdana, sans-serif; font-size: 10pt; color: #000000;">
  <div style="text-align:center;">
  <img width="100%" alt="HelloUni" src="https://imperialpublic.s3.ap-south-1.amazonaws.com/imperial+airc+banner.jpg"></div>
  <p>Dear '.$userName.',</p>
  <p style="text-align:justify; margin-bottom:1.3em;">Thank you for your interest in our Services and for registering for the <strong><u>Lucky Draw</u></strong>! We are truly focused on making <strong>HelloUni</strong> the pre-eminent Pan-India Digital Recruitment Platform! Our focus is on face-to-face interaction between institutions and students to enable high quality outreach and conversion!</p>
  <p style="margin-bottom:1.3em;"><u>Strengths of the system</u>:</p>
  <ul style="margin-bottom:1.3em;">
  <li style="padding-bottom: 6px;"><strong>Pan-India Recruitment</strong> &mdash; Not limited by geography, we can reach out across the country and in the future, we would be looking to expand beyond India.</li>
  <li style="padding-bottom: 6px;"><strong>Face-to-face Meetings</strong> &mdash; Interact directly with students in 1-to-1 or 1-to-Many settings.</li>
  <li style="padding-bottom: 6px;"><strong>Student Profile Screening</strong> &mdash; Entire student profile is visible to determine their potential and scope for virtual meeting / followup.</li>
  <li style="padding-bottom: 6px;"><strong>Admin Panel (Backend)</strong> &mdash; Access to modify / update all the information pertaining to the University that is displayed to the students on the portal. In addition, student profile and data can be accessed from the Admin Panel.</li>
  </ul>
  <p style="text-align: justify; margin-top: 1.5em;">Our team will reach out to you by 15<sup>th</sup> December to announce the results as well as to setup a short training session to take you through the platform!  If you wish to have the training session prior, please contact us at <a href="mailto:partnerships@imperial-overseas.com">partnerships@imperial-overseas.com</a>.</p>
  <p style="text-align: justify; margin-top: 1.5em;">Until then, Happy Exploring!</p>
  <p style="text-align: justify; margin-top: 1.5em;"><a href="https://youtu.be/p5n6iPuIVdk"> </a></p>
  </div>';

  $ccMailList = '';
  $mailAttachments = '';
  $mailTemplate = preg_replace('/[\n\r]+/', '', $mailTemplate);

  $sendMail = sMail($userEmail, $universityName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

  if($sendMail){
    echo "SUCCESS";
  } else {
    echo "SUCCESS";
  }

}

//counsellor registration
function counsellorForm(){

  $this->render_page('templates/webinar/counsellor_registration_form',$this->outputData);

}

// Visa Seminar

function visaSeminarForm(){

  $studentId = $this->uri->segment('4') ? $this->uri->segment('4') : 1;

  $this->outputData['student_id'] = $studentId;

  $studentName = $this->common_model->get(['id'=> $studentId], "user_master");

  $this->outputData['student_name'] = $studentName['data'][0]['name'];

  $this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));

  $this->outputData['mainstreams'] = $this->course_model->getAllCollegeCategories();

  $this->outputData['mainstreams'] = $this->course_model->getAllCollegeCategories();

  $this->outputData['degrees'] = $this->degree_model->getDegree(array('degree_master.status'=>1));

  $this->render_page('templates/webinar/visaSeminarForm',$this->outputData);

}

function visaSeminarRegistration(){

  $userId     	= $this->input->post('user_id');

  $alreadyRegistered = $this->common_model->get(["user_id" => $userId], 'visa_seminar_detail');

  if($alreadyRegistered['data']){
    echo "REDIRECTVISAPAGE";
    exit();
  }

  $studentName     	= $this->input->post('student_name');
  $studentCountry 		= $this->input->post('desired_country');
  $studentMainstream = $this->input->post('desired_mainstream');
  $studentSubcourse 	= $this->input->post('desired_subcourse');
  $studentLevelofcourse 		= $this->input->post('desired_levelofcourse');

  $competitiveExam  = $this->input->post('competitive_exam');
  $ce_one_score     = $this->input->post('ce_one_score');
  $ce_two_score 		= $this->input->post('ce_two_score');
  $ce_three_score   = $this->input->post('ce_three_score');
  $ce_total_score 	= $this->input->post('ce_total_score');

  $langExam  = $this->input->post('lang_exam');
  $reading_score     = $this->input->post('reading_score');
  $writing_score 		= $this->input->post('writing_score');
  $speaking_score   = $this->input->post('speaking_score');
  $listening_score 	= $this->input->post('listening_score');
  $lang_total_score 	= $this->input->post('lang_total_score');

  $undergraduate_score  = $this->input->post('undergraduate_score');
  $tweleth_score     = $this->input->post('tweleth_score');
  $tenth_score 		= $this->input->post('tenth_score');
  $undergraduate_university_name   = $this->input->post('undergraduate_university_name');
  $undergraduate_college_name 	= $this->input->post('undergraduate_college_name');
  $undergraduate_major 	= $this->input->post('undergraduate_major');
  $education_finance_type 	= $this->input->post('education_finance_type');

  $university_applied  = $this->input->post('university_applied');
  $current_status     = $this->input->post('current_status');

  $whereUserIdCondition = ["user_id" => $userId];

  $visaSeminarMasterData = [
    "user_id" => $userId,
    "student_name" => $studentName,
    "wish_to_pursue" => $studentName,
    "major_selected" => $studentName,
    "specialization" => $studentName,
    "competitive_exam" => $competitiveExam,
    "ce_total_mark" => $ce_total_score,
    "ce_mark_one" => $ce_one_score,
    "ce_mark_two" => $ce_two_score,
    "ce_mark_three" => $ce_three_score,
    "language_exam" => $langExam,
    "le_total_mark" => $lang_total_score,
    "le_mark_one" => $reading_score,
    "le_mark_two" => $writing_score,
    "le_mark_three" => $speaking_score,
    "le_mark_three" => $listening_score,
    "undergraduate_marks" => $undergraduate_score,
    "twelth_marks" => $tweleth_score,
    "tenth_marks" => $tenth_score,
    "undergraduate_major" => $undergraduate_major,
    "undergraduate_college" => $undergraduate_college_name,
    "undergraduate_university" => $undergraduate_university_name,
    "education_loan" => $education_finance_type

  ];

  $insertVisaSeminarObj = $this->common_model->post($visaSeminarMasterData,"visa_seminar_detail");

  $university_applied_array = explode(',', $university_applied);
  $current_status_array = explode(',', $current_status);

  foreach ($university_applied_array as $key => $value) {

    $visaSeminarUniversityAppliedData = [
      "user_id" => $userId,
      "university_name" => $value,
      "current_status" => $current_status_array[$key]
    ];

    $insertVisaSeminarUniversityObj = $this->common_model->post($visaSeminarUniversityAppliedData,"visa_seminar_university_applied_detail");

  }

  echo "SUCCESS";

}

function adminAccess(){

  $this->load->helper('cookie_helper');
  $this->render_page('templates/webinar/studentvisaseminarurl',$this->outputData);

}

function imperialEventData(){



      /*$eventVenue = $this->input->post('event_venue') ? $this->input->post('event_venue') : 0;
      $userRequiredCountry = $this->input->post('student_required_country') ? $this->input->post('student_required_country') : 0;
      $userRequiredDegree = $this->input->post('student_required_degree') ? $this->input->post('student_required_degree') : 0;
      $userGre = $this->input->post('student_gre') ? $this->input->post('student_gre') : 0;*/
      /*$userCity = $this->input->post('user_city') ? $this->input->post('user_city') : 0;
      $userLoan = $this->input->post('user_loan') ? $this->input->post('user_loan') : 0;
      $userLoanType = $this->input->post('user_loan_type') ? $this->input->post('user_loan_type') : 0;
      $userUniversity = $this->input->post('user_university') ? $this->input->post('user_university') : 0;
      $userEnrolled = $this->input->post('user_enrolled') ? $this->input->post('user_enrolled') : 0;
      $userEnrolledBranch = $this->input->post('user_enrolled_branch') ? $this->input->post('user_enrolled_branch') : 0;*/

      $eventVenue = $this->input->post('event_venue') ? $this->input->post('event_venue') : 0;
      $userName = $this->input->post('student_name') ? $this->input->post('student_name') : 0;
      $userEmail = $this->input->post('student_email') ? $this->input->post('student_email') : 0;
      $userPhone = $this->input->post('student_phone') ? $this->input->post('student_phone') : 0;
      $userIntake = $this->input->post('student_intake') ? $this->input->post('student_intake') : 0;
      $userSource = $this->input->post('user_source') ? $this->input->post('user_source') : 0;
      $userUtmSource = $this->input->post('user_utm_source') ? $this->input->post('user_utm_source') : 0;
      $userGre = $this->input->post('student_gre') ? $this->input->post('student_gre') : 0;
      $userRequiredCountry = $this->input->post('student_required_country') ? $this->input->post('student_required_country') : 0;
      $userRequiredDegree = $this->input->post('student_required_degree') ? $this->input->post('student_required_degree') : 0;
      $userEnrolled = $this->input->post('user_enrolled') ? $this->input->post('user_enrolled') : 0;
      $userEnrolledBranch = $this->input->post('user_enrolled_branch') ? $this->input->post('user_enrolled_branch') : 0;

      $userCheck = $this->webinar_model->imperialEventUserCheck($userEmail, $userPhone);

      if($userCheck){

        echo "REGISTERED_EMAIL";

      } else {

        $insertData = [
          "name" => $userName,
          "email" => $userEmail,
          "phone" => $userPhone,
          "event_location" => $eventVenue,
          "intake" => $userIntake,
          "country" => $userRequiredCountry,
          "degree" => $userRequiredDegree,
          "gre" => $userGre,
          "imperial_enrolled" => $userEnrolled,
          "branch_name" => $userEnrolledBranch,
          "utm_source" => $userUtmSource,
          "source" => $userSource
        ];


      $insertDataObj = $this->common_model->post($insertData, "imperial_event_data");

      $studentName = $userName;
      $studentEmail = $userEmail;


      $mailSubject = "Global Study Abroad Event | Imperial World Education Fair 2023 ";

      $mailTemplate = "Hi $studentName,

      Thank you for registering for the Global Study Abroad Event / Imperial World Education Fair 2023.

      We are glad to have you participate in the session!

      Please see details below given by you:
        1. Selcted Country : $userRequiredCountry
        2. Selcted Intake : $userIntake
        3. Venue : $eventVenue

      With regards,
      <p><img src='https://www.imperialonlinestudy.com/static/media/logo.857ab95d.png' style='width: 126px; height:40px;'></p>
      <strong>Team Imperial Overseas</strong>
            ";

      $ccMailList = '';
      $mailAttachments = '';

      //$studentEmail = 'nikhil.gupta@issc.in';

      //$sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

      if($sendMail){
        echo "SUCCESS";
      } else {
        echo "SUCCESS";
      }

    }

  }


  function earlybirdRegistrationFormData(){

      $userName = $this->input->post('first_name') ? $this->input->post('first_name') : 0;
      $userEmail = $this->input->post('user_email') ? $this->input->post('user_email') : 0;
      $userPhone = $this->input->post('user_mobile') ? $this->input->post('user_mobile') : 0;
      $userEngTest = $this->input->post('user_engtest') ? $this->input->post('user_engtest') : 0;
      $userEngTestScore = $this->input->post('user_engtestscore') ? $this->input->post('user_engtestscore') : 0;
      $userGreScore = $this->input->post('user_GRE_Score') ? $this->input->post('user_GRE_Score') : 0;
      $userRequiredCountry = $this->input->post('user_required_country') ? $this->input->post('user_required_country') : 0;
      $userRequiredDegree = $this->input->post('user_required_degree') ? $this->input->post('user_required_degree') : 0;
      $userIntake = $this->input->post('user_intake') ? $this->input->post('user_intake') : 0;
      $userSource = "Hellouni";

      $userCheck = $this->webinar_model->imperialEventUserCheck($userEmail, $userPhone);

      if($userCheck){

        echo "REGISTERED_EMAIL";

      } else {

        $insertData = [
          "name" => $userName,
          "email" => $userEmail,
          "phone" => $userPhone,
          "any_english_test" => $userEngTest,
          "any_english_mark" => $userEngTestScore,
          "gre" => $userGreScore,
          "country" => $userRequiredCountry,
          "degree" => $userRequiredDegree,
          "intake" => $userIntake,
          "source" => $userSource
        ];


      $insertDataObj = $this->common_model->post($insertData, "imperial_event_data");

      $studentName = $userName;
      $studentEmail = $userEmail;


      $mailSubject = "Thank you for registering with HelloUNI Platform!";

      $mailTemplate = "Dear $studentName,

          Thank you for registering with Hello<b>UNI</b> Platform!

          What We are?
          Hello<b>UNI</b> is your ultimate gateway to prestigious universities across the globe! We are here to empower you in every step along the way, to convert your study abroad plans into reality!

          OUR Services!
          - Interact directly with Universities face-to-face & virtually.
          - Obtain clear information regarding Scholarships / Eligibility / Timeline / Assistantships / Costs / Courses etc.
          - Easy & Convenient Application Process - One form to apply to Multiple Universities.
          - Documentation - SOP / LOR / Resume.
          - Guidance from experienced counsellors for all your study abroad queries.
          - VISA Guidance.
          - Education Loan Guidance.

          We are glad to assist your road to education abroad! We received your inquiry and are happy to take this forward. Our respective admission expert will call you within 24hrs and clear your doubts and guide you throughout your entire study abroad process.

          <b>If you need any further assistance or have any queries you can always reach us @ +91-93219-88360 or susheel.g@hellouni.org</b>

          Here is the Snapshot of your profile which we received from you:

          Name: $studentName
          Email: $userEmail
          Mobile: $userPhone
          Country: $userRequiredCountry
          Degree: $userRequiredDegree
          Intake: $userIntake

          We wish you all the best for your further process, Feel free to reach us!

          Thank You,
          Regards,

          Team,

          Hello UNI (Powered by Imperial Platforms Pvt Ltd),
          M: +91-93219-88360 or E: susheel.g@hellouni.org
          #211, Vijay Sai Towers, 2nd Floor, Opp. BJP Office Road,
          Above Reliance Digital, Vivek Nagar, Kukatpally, Hyderabad, Telangana 500072
            ";

      $ccMailList = '';
      $mailAttachments = '';

      //$studentEmail = 'nikhil.gupta@issc.in';

      $sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

      if($sendMail){
        echo "SUCCESS";
      } else {
        echo "SUCCESS";
      }

    }

  }


}
