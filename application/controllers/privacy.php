<?php
class Privacy extends My_Controller
{
    function __construct()
    {
        parent::__construct();
        track_user();
    }

    function index()
    {
        $this->render_page('templates/privacy');
    }

    function mypage()
    {
        $this->render_page('templates/webinar/myFairList');
    }
}
?>
