<?php
class Session extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model(['session_model']);
    }

    function index()
    {
        $mapped_url = $this->session_model->get_mapped_url();
        if($mapped_url)
        {
            redirect($mapped_url->mapped_url);
        }
    }
}
?>
