<?php
require APPPATH . "vendor/autoload.php";
require_once APPPATH . 'libraries/Mail/sMail.php';

use OpenTok\OpenTok;
class Cron extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->load->model(array('tokbox_model', 'user/user_model'));
        $opentok = new OpenTok('46220952', '3cd0c42ef3cdbf90adbeeed651a5cec8c8a28ba2');

        $comingSlots = $this->tokbox_model->getAllComingSlots();
        if($comingSlots)
        {
            foreach($comingSlots as $comingSlot)
            {
                $participants = array(
                    'slot_id' => $comingSlot['id'],
                    'user_id' => $comingSlot['user_id'],
                    'university_id' => $comingSlot['university_id']
                );
                $json_participant = json_encode($participants);

                $tokenDetail = $this->tokbox_model->getTokenByParticipant($json_participant);
                if($tokenDetail)
                {
                    if($tokenDetail->session_id)
                    {
                        $sessionId = $tokenDetail->session_id;
                        $tokenId = $tokenDetail->id;
                        $token = $opentok->generateToken($sessionId);
                        $tokbox_data['token'] = $token;
                        $currentTime = date('Y-m-d H:i:s');
                        $expiry = strtotime($currentTime) + 3600;
                        $expiryTime = date('Y-m-d H:i:s', $expiry);
                        $tokbox_data['expiry_time'] = $expiryTime;
                        $this->tokbox_model->editToken($tokbox_data, $tokenId);

                        $studentId = $comingSlot['user_id'];
                        $universityLoginId = $comingSlot['university_login_id'];

                        $condition = array('user_master.id' => $studentId);
                        $studentDetail = $this->user_model->getUserByid($condition);

                        $condition = array('user_master.id' => $universityLoginId);
                        $universityDetail = $this->user_model->getUserByid($condition);

                        $universityEmail = $universityDetail->email;
                        $universityName = $universityDetail->name;

                        $studentName = $studentDetail->name;
                        $studentEmail = $studentDetail->email;

                        /*echo "Student Mail - " . $studentEmail . "\n";
                        echo "University Mail - " . $universityEmail . "\n";

                        $studentEmail = 'nikhil.gupta@issc.in';
                        $universityEmail = 'nikhil.gupta@blocmatrix.com';*/

                        $ccMailList = '';
                        $mailAttachments = '';

                        $encodedTokboxId = base64_encode($tokenId);
                        $encodedUniversityId = base64_encode($universityLoginId);

                        $mailSubject = "Conference URL For One To One With $universityName";
                        $mailTemplate = "Hi $studentName

                                            Please click on the following URL to join the conference with $universityName

                                            <a href='https://www.hellouni.org/tokbox?id=" . $encodedTokboxId . "'>https://www.hellouni.org/tokbox?id=" . $encodedTokboxId . "</a>

                                            <b>Thanks and Regards,
                                            HelloUni Coordinator
                                            +91 81049 09690</b>

                                            <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

                        $sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

                        $mailSubject = "Conference URL For One To One With $studentName";
                        $mailTemplate = "Hi $universityName,

                                        Please click on the following URL to join the conference with $studentName

                                        <a href='https://www.hellouni.org/tokbox?id=" . $encodedTokboxId . "&uid=" . $encodedUniversityId . "'>https://www.hellouni.org/tokbox?id=" . $encodedTokboxId . "&uid=" . $encodedUniversityId . "</a>

                                        <b>Thanks and Regards,
                                        HelloUni Coordinator
                                        +91 81049 09690</b>

                                        <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

                        $sendMail = sMail($universityEmail, $universityName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);
                    }
                }
            }
        }
    }

    function webinarAutoMailer(){

      $this->load->model(array('tokbox_model', 'user/user_model'));

        $comingWebinar = $this->tokbox_model->getAllComingWebinar();

        if($comingWebinar) {

          foreach ($comingWebinar as $cwkey => $cwvalue) {
            // code...

            $webinarName = $cwvalue['webinar_name'];
            $webinarLink = $cwvalue['webinar_link'];
            $webinarTime = $cwvalue['webinar_time'];
            $webinarTokenId = $cwvalue['tokbox_token_id'];

            $webDate = new DateTime($cwvalue['webinar_date']." ".$cwvalue['webinar_time']);
            $webinarStartDate = $webDate->format('H:i:s');

            $date = new DateTime('now',new DateTimeZone('Asia/Kolkata'));
            $ctime = $date->format('H:i:s');

            $start = strtotime($ctime);
            $end = strtotime($webinarStartDate);

            $mins = ($end - $start) / 60;

            if ($mins >  0 && $mins < 31) {

              $webMasterLog = $this->tokbox_model->webinarUserInfo($cwvalue['id']);

              if($webMasterLog){
                foreach ($webMasterLog as $wlkey => $wlvalue) {
                  // code...
                  $studentName = $wlvalue['name'];
                  $studentEmail = $wlvalue['email'];
                  $ccMailList = "";
                  $mailAttachments = "";

                  $mailSubject = "Faculty-led Technical Webinar - $webinarName - Session Link - $webinarTime Today!";
                  $mailTemplate = "Dear $studentName,

                                  Thank you for registering for the Webinar: $webinarName.

                                  In order to join the session, please see the two options below:

                                  1. At $webinarTime Today, join directly using the link: https://www.hellouni.org/meeting/webinar/attendee?id=$webinarTokenId

                                  2. You can also join the session by logging in to HelloUni using your Username and Password you selected while registering for the Webinar. Once logged in, on the Webinar Page, click on the Join button for your registered Webinar.


                                  Regards,
                                  Team HelloUni
                                  Tel: +91 81049 09690
                                  Email: info@hellouni.org

                                  <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

                  $sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);


                }
              }

            }

          }

        }

    }
}
?>
