<?php

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

require_once APPPATH . 'libraries/Mail/sMail.php';

class Counsellormaster extends MY_Controller {

      public $outputData;		//Holds the output data for each view
      public $loggedInUser;


	 function __construct(){

    parent::__construct();

    $this->load->library('template');
    $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
		$this->load->model('location/country_model');
    $this->load->model('webinar/webinar_model');
    $this->load->model('webinar/common_model');
    $this->load->model('counsellor/counsellor_model');

    $this->load->model('course/course_model');
		$this->load->model('course/degree_model');
    $this->load->model('user/user_model');
    $this->load->model('tokbox_model');
    $this->load->model('redi_model');

    }

    function dashboard(){

      $this->load->helper('cookie_helper');

      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
      //  redirect('user/account');
      //  exit();
      }

      $counsellorId = $usrdata['id'] ? $usrdata['id'] : 0;
      $key = "COUNSELLOR-CHILD-".$counsellorId;
      $keyExist = $this->redi_model->getKey($key);

      if($keyExist){

         $allChild = $keyExist;

      } else {

        $getChild = $this->counsellor_model->getChild($counsellorId);

        $allChild = $getChild && $getChild[0]['child_ids'] ? $getChild[0]['child_ids'].", $counsellorId" : $counsellorId;

        $setKey = $this->redi_model->setKey($key, $allChild);
      }

      $applicationDetail = $this->counsellor_model->applicationData($allChild);
      $visaApplicationDetail = $this->counsellor_model->visaApplicationData($allChild);
      $studentDetail = $this->counsellor_model->studentData($allChild);

      //var_dump($allChild);
      //var_dump($studentDetail);
      //die();

      //application
      $this->outputData['totalApplicationCount'] = 0;
      $this->outputData['approvedApplicationCount'] = 0;
      $this->outputData['rejectedApplicationCount'] = 0;

      $approvedStatusArray = ['ADMIT_RECEIVED_CONDITIONAL', 'ADMIT_RECEIVED_UNCONDITIONAL', 'VISA_PROCESS_FINANCIAL_DOCUMENTS', 'VISA_APPLIED', 'VISA_APPROVED', 'VISA_REJECTED', 'VISA_APPROVED_OFFER'];
      $rejectedStatusArray = ['ADMIT_DENIED', 'ADMIT_WITHDRAW'];

      foreach ($applicationDetail as $keyad => $valuead) {

        if(in_array($valuead['status'], $approvedStatusArray)){
          $this->outputData['approvedApplicationCount'] += $valuead['count'];
          $this->outputData['totalApplicationCount'] += $valuead['count'];
        } else if(in_array($valuead['status'], $rejectedStatusArray)){
          $this->outputData['rejectedApplicationCount'] += $valuead['count'];
          $this->outputData['totalApplicationCount'] += $valuead['count'];
        } else if($valuead['status'] == 'APPLICATION_PROCESS'){
          $this->outputData['totalApplicationCount'] += $valuead['count'];
        } else if($valuead['status'] == 'APPLICATION_SUBMITTED'){
          $this->outputData['totalApplicationCount'] += $valuead['count'];
        }

      }

      //offers
      $this->outputData['totalOfferCount'] = 0;
      $this->outputData['approvedOfferCount'] = 0;
      $this->outputData['rejectedOfferCount'] = 0;

      foreach ($applicationDetail as $keyoad => $valueoad) {

        if($valueoad['status'] == 'ADMIT_RECEIVED_UNCONDITIONAL'){
          $this->outputData['approvedOfferCount'] = $valueoad['count'];
          $this->outputData['totalOfferCount'] = $this->outputData['totalOfferCount'] + $valueoad['count'];
        } else if($valueoad['status'] == 'ADMIT_DENIED'){
          $this->outputData['rejectedOfferCount'] = $valueoad['count'];
          $this->outputData['totalOfferCount'] = $this->outputData['totalOfferCount'] + $valueoad['count'];
        }

      }

      //payment
      $totalPaymentCount = 0 ;
      $approvedPaymentCount = 0 ;
      $rejectedPaymentCount = 0 ;

      //visa
      $this->outputData['totalVisaCount'] = 0;
      $this->outputData['approvedVisaCount'] = 0;
      $this->outputData['rejevtedVisaCount'] = 0;

      foreach ($visaApplicationDetail as $keyvad => $valuevad) {

        if($valuevad['status'] == 'VISA_APPROVED'){
          $this->outputData['approvedVisaCount'] = $valuevad['count'];
        } else if($valuevad['status'] == 'VISA_REJECTED'){
          $this->outputData['rejevtedVisaCount'] = $valuevad['count'];
        }

        $this->outputData['totalVisaCount'] += $valuevad['count'];
      }

      //student
      $this->outputData['totalStudentCount'] = 0;
      $this->outputData['interestedStudentCount'] = 0;
      $this->outputData['potentialStudentCount'] = 0;

      foreach ($studentDetail as $keysd => $valuesd) {

        if($valuesd['status'] == 'INTERESTED'){
          $this->outputData['interestedStudentCount'] = $valuesd['count'];
          $this->outputData['totalStudentCount'] = $this->outputData['totalStudentCount'] + $valuesd['count'];
        } else if($valuesd['status'] == 'POTENTIAL'){
          $this->outputData['potentialStudentCount'] = $valuesd['count'];
          $this->outputData['totalStudentCount'] = $this->outputData['totalStudentCount'] + $valuesd['count'];
        }
      }

      $this->outputData['totalPaymentCount'] = $totalPaymentCount;
      $this->outputData['approvedPaymentCount'] = $approvedPaymentCount;
      $this->outputData['rejectedPaymentCount'] = $rejectedPaymentCount;
      $this->outputData['start_date'] = $usrdata['createdate'] ? date('Y-m-d', strtotime($usrdata['createdate'])) : date('Y-m-d') ;
      $this->outputData['end_date'] = date('Y-m-d');

      $this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));

      $this->render_page('templates/counsellor/dashboard',$this->outputData);

    }

    function graphDashboard(){

      $this->load->helper('cookie_helper');

      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
      //  redirect('user/account');
      //  exit();
      }

      $counsellorId = $usrdata['id'] ? $usrdata['id'] : 0;
      $key = "COUNSELLOR-CHILD-".$counsellorId;
      $keyExist = $this->redi_model->getKey($key);

      if($keyExist){

         $allChild = $keyExist;

      } else {

        $getChild = $this->counsellor_model->getChild($counsellorId);

        $allChild = $getChild && $getChild[0]['child_ids'] ? $getChild[0]['child_ids'].", $counsellorId" : $counsellorId;

        $setKey = $this->redi_model->setKey($key, $allChild);
      }

      //Grpah Data
      $graphDetail = $this->counsellor_model->graphData($allChild);

      $applicationGraph = [];
      $offerGraph = [];
      $visaGraph = [];
      $paymentGraph = [];
      $studentGraph = [];

      $monthArray = [1,2,3,4,5,6,7,8,9,10,11,12];
      $applicationStatusArray = ['ADMIT_RECEIVED_CONDITIONAL','ADMIT_DENIED','APPLICATION_PROCESS','APPLICATION_SUBMITTED'];
      $offerStatusArray = ['ADMIT_RECEIVED_UNCONDITIONAL','ADMIT_DENIED'];
      $visaStatusArray = ['VISA_APPROVED','VISA_REJECTED'];
      $paymentStatusArray = [];
      $studentStatusArray = ['INTERESTED','POTENTIAL'];

      if($graphDetail){
        foreach ($graphDetail['application_data'] as $keyadg => $valueadg) {
          if(in_array($valueadg['status'], $applicationStatusArray)){
            $applicationGraph[$valueadg['graph_month']][] = $valueadg['count'];
          }
        }

        foreach ($graphDetail['application_data'] as $keyoadg => $valueoadg) {
          if(in_array($valueoadg['status'], $offerStatusArray)){
            $offerGraph[$valueoadg['graph_month']][] = $valueoadg['count'];
          }
        }

        foreach ($graphDetail['visa_data'] as $keyvdg => $valuevdg) {
          if(in_array($valuevdg['status'], $visaStatusArray)){
            $visaGraph[$valuevdg['graph_month']][] = $valuevdg['count'];
          }
        }

        foreach ($graphDetail['student_data'] as $keysdg => $valuesdg) {
          if(in_array($valuesdg['status'], $studentStatusArray)){
            $studentGraph[$valuesdg['graph_month']][] = $valuesdg['count'];
          }
        }

      }

      $applicationGraphValues = [];
      $offerGraphValues = [];
      $visaGraphValues = [];
      $paymentGraphValues = [NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL];
      $studentGraphValues = [];

      foreach ($monthArray as $keyma) {

        if(isset($applicationGraph[$keyma])){
          $applicationGraphValues[] = array_sum($applicationGraph[$keyma]);
        }else{
          $applicationGraphValues[] = NULL;
        }

        if(isset($offerGraph[$keyma])){
          $offerGraphValues[] = array_sum($offerGraph[$keyma]);
        }else{
          $offerGraphValues[] = NULL;
        }

        if(isset($visaGraph[$keyma])){
          $visaGraphValues[] = array_sum($visaGraph[$keyma]);
        }else{
          $visaGraphValues[] = NULL;
        }

        if(isset($studentGraph[$keyma])){
          $studentGraphValues[] = array_sum($studentGraph[$keyma]);
        }else{
          $studentGraphValues[] = NULL;
        }

      }

      $response = [];

      $response['applicationGraphValues'] = $applicationGraphValues;
      $response['paymentGraphValues'] = $paymentGraphValues;
      $response['offerGraphValues'] = $offerGraphValues;
      $response['visaGraphValues'] = $visaGraphValues;
      $response['studentGraphValues'] = $studentGraphValues;

      header('Content-Type: application/json');
      echo (json_encode($response));

    }

    function dashboardFilter(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
      //  redirect('user/account');
      //  exit();
      }

      $counsellorId = $usrdata['id'] ? $usrdata['id'] : 0;

      $key = "COUNSELLOR-CHILD-".$counsellorId;

      $keyExist = $this->redi_model->getKey($key);

      if($keyExist){

         $allChild = $keyExist;

      } else{

        $getChild = $this->counsellor_model->getChild($counsellorId);

        $allChild = $getChild && $getChild[0]['child_ids'] ? $getChild[0]['child_ids'].", $counsellorId" : $counsellorId;

        $setKey = $this->redi_model->setKey($key, $allChild);
      }

      $startDate = $this->input->post('start_date');
      $endDate = $this->input->post('end_date');
      $countryIds = $this->input->post('country_id');
      $intake_year = $this->input->post('intake_year');
      $intake_month = $this->input->post('intake_month');
      //$applicationStatus = $this->input->post('application_status');
      //$offerStatus = $this->input->post('offer_status');

      $dateConditons = "";
      $countryCondition = "";
      $intakeCondition = "";
      $appStatusCondition = "";
      $offerTYpeCondition = "";

      $applicationDetail = $this->counsellor_model->filterApplicationData($allChild, $startDate, $endDate, $countryIds, $intake_year, $intake_month);
      $visaApplicationDetail = $this->counsellor_model->filterVisaApplicationData($allChild, $startDate, $endDate, $countryIds, $intake_year, $intake_month);
      $studentDetail = $this->counsellor_model->filterStudentData($allChild, $startDate, $endDate, $countryIds, $intake_year, $intake_month);

      //Grpah Data
      $graphDetail = $this->counsellor_model->graphDataFilter($allChild, $startDate, $endDate, $countryIds, $intake_year, $intake_month);

      $applicationGraph = [];
      $offerGraph = [];
      $visaGraph = [];
      $paymentGraph = [];
      $studentGraph = [];

      $monthArray = [1,2,3,4,5,6,7,8,9,10,11,12];
      $applicationStatusArray = ['ADMIT_RECEIVED_CONDITIONAL','ADMIT_DENIED','APPLICATION_PROCESS','APPLICATION_SUBMITTED'];
      $offerStatusArray = ['ADMIT_RECEIVED_UNCONDITIONAL','ADMIT_DENIED'];
      $visaStatusArray = ['VISA_APPROVED','VISA_REJECTED'];
      $paymentStatusArray = [];
      $studentStatusArray = ['INTERESTED','POTENTIAL'];

      if($graphDetail){
        foreach ($graphDetail['application_data'] as $keyadg => $valueadg) {
          if(in_array($valueadg['status'], $applicationStatusArray)){
            $applicationGraph[$valueadg['graph_month']][] = $valueadg['count'];
          }
        }

        foreach ($graphDetail['application_data'] as $keyoadg => $valueoadg) {
          if(in_array($valueoadg['status'], $offerStatusArray)){
            $offerGraph[$valueoadg['graph_month']][] = $valueoadg['count'];
          }
        }

        foreach ($graphDetail['visa_data'] as $keyvdg => $valuevdg) {
          if(in_array($valuevdg['status'], $visaStatusArray)){
            $visaGraph[$valuevdg['graph_month']][] = $valuevdg['count'];
          }
        }

        foreach ($graphDetail['student_data'] as $keysdg => $valuesdg) {
          if(in_array($valuesdg['status'], $studentStatusArray)){
            $studentGraph[$valuesdg['graph_month']][] = $valuesdg['count'];
          }
        }

      }

      $applicationGraphValues = [];
      $offerGraphValues = [];
      $visaGraphValues = [];
      $paymentGraphValues = [NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL];
      $studentGraphValues = [];

      foreach ($monthArray as $keyma) {

        if($applicationGraph[$keyma]){
          $applicationGraphValues[] = array_sum($applicationGraph[$keyma]);
        }else{
          $applicationGraphValues[] = NULL;
        }

        if($offerGraph[$keyma]){
          $offerGraphValues[] = array_sum($offerGraph[$keyma]);
        }else{
          $offerGraphValues[] = NULL;
        }

        if($visaGraph[$keyma]){
          $visaGraphValues[] = array_sum($visaGraph[$keyma]);
        }else{
          $visaGraphValues[] = NULL;
        }

        if($studentGraph[$keyma]){
          $studentGraphValues[] = array_sum($studentGraph[$keyma]);
        }else{
          $studentGraphValues[] = NULL;
        }

      }

      $response = [];

      $response['applicationGraphValues'] = $applicationGraphValues;
      $response['paymentGraphValues'] = $paymentGraphValues;
      $response['offerGraphValues'] = $offerGraphValues;
      $response['visaGraphValues'] = $visaGraphValues;
      $response['studentGraphValues'] = $studentGraphValues;

      //application
      $response['totalApplicationCount'] = 0;
      $response['approvedApplicationCount'] = 0;
      $response['rejectedApplicationCount'] = 0;

      foreach ($applicationDetail as $keyad => $valuead) {

        if($valuead['status'] == 'ADMIT_RECEIVED_CONDITIONAL'){
          $response['approvedApplicationCount'] = $valuead['count'];
          $response['totalApplicationCount'] = $response['totalApplicationCount'] + $valuead['count'];
        } else if($valuead['status'] == 'ADMIT_DENIED'){
          $response['rejectedApplicationCount'] = $valuead['count'];
          $response['totalApplicationCount'] = $response['totalApplicationCount'] + $valuead['count'];
        } else if($valuead['status'] == 'APPLICATION_PROCESS'){
          $response['totalApplicationCount'] = $response['totalApplicationCount'] + $valuead['count'];
        } else if($valuead['status'] == 'APPLICATION_SUBMITTED'){
          $response['totalApplicationCount'] = $response['totalApplicationCount'] + $valuead['count'];
        }

      }


      //offers
      $response['totalOfferCount'] = 0;
      $response['approvedOfferCount'] = 0;
      $response['rejectedOfferCount'] = 0;

      foreach ($applicationDetail as $keyoad => $valueoad) {

        if($valueoad['status'] == 'ADMIT_RECEIVED_UNCONDITIONAL'){
          $response['approvedOfferCount'] = $valueoad['count'];
          $response['totalOfferCount'] = $response['totalOfferCount'] + $valueoad['count'];
        } else if($valueoad['status'] == 'ADMIT_WITHDRAW'){
          $response['rejectedOfferCount'] = $valueoad['count'];
          $response['totalOfferCount'] = $response['totalOfferCount'] + $valueoad['count'];
        }

      }

      //payment
      $totalPaymentCount = 0 ;
      $approvedPaymentCount = 0 ;
      $rejectedPaymentCount = 0 ;


      //visa
      $response['totalVisaCount'] = 0;
      $response['approvedVisaCount'] = 0;
      $response['rejevtedVisaCount'] = 0;

      foreach ($visaApplicationDetail as $keyvad => $valuevad) {

        if($valuevad['status'] == 'VISA_APPROVED'){
          $response['approvedVisaCount'] = $valuevad['count'];
        } else if($valuevad['status'] == 'VISA_REJECTED'){
          $response['rejevtedVisaCount'] = $valuevad['count'];
        }

        $response['totalVisaCount'] = $response['totalVisaCount'] + $valuesd['count'];
      }

      //student
      $response['totalStudentCount'] = 0;
      $response['interestedStudentCount'] = 0;
      $response['potentialStudentCount'] = 0;

      foreach ($studentDetail as $keysd => $valuesd) {

        if($valuesd['status'] == 'INTERESTED'){
          $response['interestedStudentCount'] = $valuesd['count'];
          $response['totalStudentCount'] = $response['totalStudentCount'] + $valuesd['count'];
        } else if($valuesd['status'] == 'POTENTIAL'){
          $response['potentialStudentCount'] = $valuesd['count'];
          $response['totalStudentCount'] = $response['totalStudentCount'] + $valuesd['count'];
        }
      }

       $response['totalPaymentCount'] = $totalPaymentCount;
       $response['approvedPaymentCount'] = $approvedPaymentCount;
       $response['rejectedPaymentCount'] = $rejectedPaymentCount;


       header('Content-Type: application/json');
       echo (json_encode($response));

    }

    function counsellorTeamView(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
      //  redirect('user/account');
      //  exit();
      }

      $this->render_page('templates/counsellor/team',$this->outputData);

    }

    function counsellorTeam(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
      //  redirect('user/account');
      //  exit();
      }

      $counsellorId = $usrdata['id'] ? $usrdata['id'] : 5826;

      $response['team_list'] = $this->counsellor_model->getTeamDetail($counsellorId);

      header('Content-Type: application/json');
      echo (json_encode($response));

    }

    function applicationView(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
      //  redirect('user/account');
      //  exit();
      }

      $startDate = $this->input->get('startDate') ? $this->input->get('startDate') : '';
      $endDate = $this->input->get('endDate') ? $this->input->get('endDate') : '';
      $countryIds = $this->input->get('countryId') ? $this->input->get('countryId') : '';
      $intake_year = $this->input->get('intake_year') ? $this->input->get('intake_year') : '';
      $intake_month = $this->input->get('intake_month') ? $this->input->get('intake_month') : '';
      $lead_state_type = $this->input->get('leadstatetype') ? $this->input->get('leadstatetype') : '';

      $this->outputData['start_date'] = $startDate;
      $this->outputData['end_date'] = $endDate;
      $this->outputData['country_id'] = $countryIds;
      $this->outputData['intake_year'] = $intake_year;
      $this->outputData['intake_month'] = $intake_month;
      $this->outputData['lead_state_type'] = $lead_state_type;

      $lead_state_array = [
        ["id" => 'total_application', "value" => 'TOTAL APPLICATION'],
        ["id" => 'approved_application', "value" => 'APPROVED APPLICATION'],
        ["id" => 'rejected_application', "value" => 'REJECTED APPLICATION'],
        ["id" => 'total_offers', "value" => 'TOTAL OFFERS'],
        ["id" => 'approved_offers', "value" => 'APPROVED OFFERS'],
        ["id" => 'rejected_offers', "value" => 'REJECTED OFFERS'],
        ["id" => 'total_payments', "value" => 'TOTAL PAYMENTS'],
        ["id" => 'approved_payments', "value" => 'APPROVED PAYMENTS'],
        ["id" => 'rejected_payments', "value" => 'REJECTED PAYMENTS'],
        ["id" => 'total_visa', "value" => 'TOTAL VISA'],
        ["id" => 'approved_visa', "value" => 'APPROVED VISA'],
        ["id" => 'rejected_visa', "value" => 'REJECTED VISA']
      ];

      $this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));
      $this->outputData['lead_state_dropdown'] = $lead_state_array;

      $this->render_page('templates/counsellor/applicationlist',$this->outputData);

    }

    function applicationList(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
      //  redirect('user/account');
      //  exit();
      }

      $counsellorId = $usrdata['id'] ? $usrdata['id'] : 5826;
      $key = "COUNSELLOR-CHILD-".$counsellorId;

      $keyExist = $this->redi_model->getKey($key);

      if($keyExist){

         $allChild = $keyExist;

      } else{

        $getChild = $this->counsellor_model->getChild($counsellorId);

        $allChild = $getChild && $getChild[0]['child_ids'] ? $getChild[0]['child_ids'].", $counsellorId" : $counsellorId;

        $setKey = $this->redi_model->setKey($key, $allChild);
      }

      $response['application_list'] = $this->counsellor_model->applicationList($allChild);

      header('Content-Type: application/json');
      echo (json_encode($response));

    }

    function applicationFilterList(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
      //  redirect('user/account');
      //  exit();
      }

      $counsellorId = $usrdata['id'] ? $usrdata['id'] : 5826;
      $key = "COUNSELLOR-CHILD-".$counsellorId;

      $keyExist = $this->redi_model->getKey($key);

      if($keyExist){

         $allChild = $keyExist;

      } else{

        $getChild = $this->counsellor_model->getChild($counsellorId);

        $allChild = $getChild && $getChild[0]['child_ids'] ? $getChild[0]['child_ids'].", $counsellorId" : $counsellorId;

        $setKey = $this->redi_model->setKey($key, $allChild);
      }

      $startDate = $this->input->post('start_date');
      $endDate = $this->input->post('end_date');
      $countryIds = $this->input->post('country_id');
      $intake_year = $this->input->post('intake_year');
      $intake_month = $this->input->post('intake_month');
      $status = $this->input->post('application_status') ? $this->input->post('application_status') : 0;
      $statusFilter = "";

      if($status){

        if($status == 'total_application'){

          $statusFilter = "'ADMIT_RECEIVED_CONDITIONAL', 'ADMIT_DENIED', 'APPLICATION_PROCESS', 'APPLICATION_SUBMITTED'";

        } else if($status == 'approved_application'){

          $statusFilter = "'ADMIT_RECEIVED_CONDITIONAL'";

        } else if($status == 'rejected_application'){

          $statusFilter = "'ADMIT_DENIED'";

        } else if($status == 'total_offers'){

          $statusFilter = "'ADMIT_RECEIVED_UNCONDITIONAL', 'ADMIT_DENIED'";

        } else if($status == 'approved_offers'){

          $statusFilter = "'ADMIT_RECEIVED_UNCONDITIONAL'";

        } else if($status == 'rejected_offers'){

          $statusFilter = "'ADMIT_WITHDRAW'";

        } else if($status == 'total_payments'){

          $statusFilter = "''";

        } else if($status == 'approved_payments'){

          $statusFilter = "";

        } else if($status == 'rejected_payments'){

          $statusFilter = "";

        } else if($status == 'total_visa'){

          $statusFilter = "'VISA_APPROVED', 'VISA_REJECTED'";

        } else if($status == 'approved_visa'){

          $statusFilter = "'VISA_APPROVED'";

        } else if($status == 'rejected_visa'){

          $statusFilter = "'VISA_REJECTED'";

        }

      }

      $response['application_list'] = $this->counsellor_model->applicationFilterList($allChild, $startDate, $endDate, $countryIds, $intake_year, $intake_month, $statusFilter);

      header('Content-Type: application/json');
      echo (json_encode($response));

    }

    function studentView(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
      //  redirect('user/account');
      //  exit();
      }

      $startDate = $this->input->get('startDate') ? $this->input->get('startDate') : '';
      $endDate = $this->input->get('endDate') ? $this->input->get('endDate') : '';
      $countryIds = $this->input->get('countryId') ? $this->input->get('countryId') : '';
      $intake_year = $this->input->get('intake_year') ? $this->input->get('intake_year') : '';
      $intake_month = $this->input->get('intake_month') ? $this->input->get('intake_month') : '';
      $lead_state_type = $this->input->get('leadstatetype') ? $this->input->get('leadstatetype') : '';

      $this->outputData['start_date'] = $startDate;
      $this->outputData['end_date'] = $endDate;
      $this->outputData['country_id'] = $countryIds;
      $this->outputData['intake_year'] = $intake_year;
      $this->outputData['intake_month'] = $intake_month;
      $this->outputData['lead_state_type'] = $lead_state_type;

      $lead_state_array = [
        ["id" => 'total_student', "value" => 'TOTAL STUDENT'],
        ["id" => 'interested_student', "value" => 'INTERESTED STUDENT'],
        ["id" => 'potential_student', "value" => 'POTENTIAL STUDENT']
      ];

      $this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));
      $this->outputData['lead_state_dropdown'] = $lead_state_array;

      $this->render_page('templates/counsellor/studentlist',$this->outputData);

    }

    function studentList(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
      //  redirect('user/account');
      //  exit();
      }

      $counsellorId = $usrdata['id'] ? $usrdata['id'] : 5826;
      $key = "COUNSELLOR-CHILD-".$counsellorId;

      $keyExist = $this->redi_model->getKey($key);

      if($keyExist){

         $allChild = $keyExist;

      } else{

        $getChild = $this->counsellor_model->getChild($counsellorId);

        $allChild = $getChild && $getChild[0]['child_ids'] ? $getChild[0]['child_ids'].", $counsellorId" : $counsellorId;

        $setKey = $this->redi_model->setKey($key, $allChild);
      }

      $response['student_list'] = $this->counsellor_model->studentList($allChild);

      header('Content-Type: application/json');
      echo (json_encode($response));

    }

    function studentFilterList(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
      //  redirect('user/account');
      //  exit();
      }

      $counsellorId = $usrdata['id'] ? $usrdata['id'] : 5826;
      $key = "COUNSELLOR-CHILD-".$counsellorId;

      $keyExist = $this->redi_model->getKey($key);

      if($keyExist){

         $allChild = $keyExist;

      } else{

        $getChild = $this->counsellor_model->getChild($counsellorId);

        $allChild = $getChild && $getChild[0]['child_ids'] ? $getChild[0]['child_ids'].", $counsellorId" : $counsellorId;

        $setKey = $this->redi_model->setKey($key, $allChild);
      }

      $startDate = $this->input->post('start_date');
      $endDate = $this->input->post('end_date');
      $countryIds = $this->input->post('country_id');
      $intake_year = $this->input->post('intake_year');
      $intake_month = $this->input->post('intake_month');
      $status = $this->input->post('application_status') ? $this->input->post('application_status') : 0;
      $statusFilter = "";

      if($status){

        if($status == 'total_student'){

          $statusFilter = "'INTERESTED', 'POTENTIAL'";

        } else if($status == 'interested_student'){

          $statusFilter = "'INTERESTED'";

        } else if($status == 'potential_student'){

          $statusFilter = "'POTENTIAL'";

        }

      }

      $response['student_list'] = $this->counsellor_model->studentFilterList($allChild, $startDate, $endDate, $countryIds, $intake_year, $intake_month, $statusFilter);

      header('Content-Type: application/json');
      echo (json_encode($response));

    }

    function profileUpdateForm(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
      //  redirect('user/account');
      //  exit();
      }

      $counsellorId = $this->uri->segment('3') ? $this->uri->segment('3') : 1;

      $counsellorData = $this->counsellor_model->profileDetail($counsellorId);
      $counsellorData[0]['other_details'] = json_decode($counsellorData[0]['other_details'], true);
      $this->outputData['profile_detail'] = $counsellorData[0];

      $indianState = $this->common_model->get(['country_id'=> 99],'state_master');
      $indianCity = $this->common_model->get(['state_id'=> $counsellorData[0]['state']],'city_master');

      $counsellorBankInfo = $this->common_model->get(["user_id" => $counsellorId] , "partners_bank_info");
      $counsellorDocument = $this->common_model->get(["user_id" => $counsellorId] , "document");

      $this->outputData['bank_info'] = $counsellorBankInfo['data'][0];
      $this->outputData['doc_list'] = $counsellorDocument['data'] ? $counsellorDocument['data'] : [];

      $this->outputData['state_list'] = $indianState['data'];
      $this->outputData['city_list'] = $indianCity['data'];


      $this->render_page('templates/counsellor/counsellor_profile_update_form',$this->outputData);

    }

    function profileUpdate(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
       redirect('user/account');
       exit();
      }

      $userName     	= $this->input->post('user_name');
      $userEmail 		  = $this->input->post('user_email');
      $userMobile 	  = $this->input->post('user_mobile');
      $userPassword	  = $this->input->post('user_password') ? $this->input->post('user_password') : 0;
      $userState 		  = $this->input->post('state_id');
      $userCity       = $this->input->post('city_id');
      $userAdd       = $this->input->post('user_add');
      $agencyName     = $this->input->post('agency_name') ? $this->input->post('agency_name') : 0;
      $userId         = $this->input->post('user_id');
      $roleId         = $this->input->post('user_role');


      $userMasterData = [];
      $partnersData = [];

      if($userPassword){

        $userMasterData = [
          "name" => $userName,
          "email" => $userEmail,
          "phone" => $userMobile,
          "password" => md5($userPassword),
          "type" => $roleId
        ];

      } else {

        $userMasterData = [
          "name" => $userName,
          "email" => $userEmail,
          "phone" => $userMobile,
          "type" => $roleId
        ];

      }

      if($agencyName){

        $partnersData = [
          "state" => $userState,
          "city" => $userCity,
          "address" => $userAdd,
          "other_details" => json_encode(["agency_name" => $agencyName])
        ];

      } else {

        $partnersData = [
          "state" => $userState,
          "city" => $userCity,
          "address" => $userAdd
        ];

      }

      $whereUserCondition = ["id" => $userId ];
      $wherePartnersCondition = ["user_id" => $userId ];
      //echo "userid-$userId";
      //exit();

      $userMasterUpdate = $this->common_model->put($whereUserCondition,$userMasterData,"user_master");
      $partnersMasterUpdate = $this->common_model->put($wherePartnersCondition,$partnersData,"partners");

      echo "SUCCESS";

    }


    function updateBankInfo(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
       redirect('user/account');
       exit();
      }

      $companyName   	= $this->input->post('company_name') ? $this->input->post('company_name') : NULL;
      $ifscCode 		  = $this->input->post('ifsc_code') ? $this->input->post('ifsc_code') : NULL;
      $swiftCode 	    = $this->input->post('swift_code') ? $this->input->post('swift_code') : NULL;
      $companyAddress	= $this->input->post('company_address') ? $this->input->post('company_address') : NULL;
      $bankName 		  = $this->input->post('bank_name') ? $this->input->post('bank_name') : NULL;
      $accountNumber  = $this->input->post('account_number') ? $this->input->post('account_number') : NULL;
      $branchName     = $this->input->post('branch_name') ? $this->input->post('branch_name') : NULL;
      $userId         = $this->input->post('user_id') ? $this->input->post('user_id') : NULL;


      $wherePartnersCondition = ["user_id" => $userId ];
      $bankInfoData = [
        "user_id" => $userId,
        "company_name" => $companyName,
        "ifsc_code" => $ifscCode,
        "swift_code" => $swiftCode,
        "company_address" => $companyAddress,
        "bank_name" => $bankName,
        "account_number" => $accountNumber,
        "branch_name" => $branchName
      ];

      $userMasterUpdate = $this->common_model->upsert($wherePartnersCondition, $bankInfoData, "partners_bank_info");

      echo "SUCCESS";

    }

    function updateDoc() {

    $this->load->helper('cookie_helper');
		$this->load->model('user/user_model');
		$userdata = $this->session->userdata('user');

	    if(!$userdata)
        {
            redirect('user/account');
        }

        $postVariables = $this->input->post();
        $userId = $postVariables['user_id'];
        $userType = $postVariables['user_type'];
        $documentMasterId = $postVariables['document_master_id'];


    		if (!file_exists('./uploads/partner_' . $userId)) {
        		mkdir('./uploads/partner_' . $userId, 0777, true);
    		 }

        $uploadFilePath     = './uploads/partner_' . $userId;
        $uploadPath         = '/uploads/partner_' . $userId;
        $pathInfo           = pathinfo($_FILES['document']['name']);
        $tempfilename       = preg_replace('/\s+/', '_', $pathInfo['filename']);
        $tempfilename       = str_replace('.', '_', $tempfilename);
        $tempfilename       = str_replace('&', '_', $tempfilename);
        $tempfilename       = str_replace('-', '_', $tempfilename);
        $tempfilename       = str_replace('+', '_', $tempfilename);

        $fileExt                    = $pathInfo['extension'];
        $fileName                   = $userId . '_' . $documentMasterId . '_' . $tempfilename . '.' . $fileExt;
        $config['upload_path']      = $uploadFilePath;
        $config['file_name']        = $fileName;
        $config['allowed_types']    = 'gif|jpg|png|JPEG|JPG|jpeg|pdf|PDF';
        $config['max_size']         = 2048;
        $config['max_width']        = 10240;
        $config['max_height']       = 7680;

        $this->load->library('upload', $config);

        $parentDocument = $this->user_model->getDocumentByStudentType($userId, $documentMasterId);

        if($parentDocument)
        {
            $oldFile = $_SERVER['DOCUMENT_ROOT'] . $uploadPath . '/' . $parentDocument->name;
            if(file_exists($oldFile))
            {
                unlink($oldFile);
            }
        }

        if(!$this->upload->do_upload('document'))
        {
            //$this->outputData['document_error'] = $this->upload->display_errors();
            echo $this->upload->display_errors();
            exit();
        }
        else
        {
            if($studentDocument)
            {
                $updatedData = [
                    "name" => $fileName,
                    "url" => base_url() . $uploadPath . '/' . $fileName
                ];
                $documentUpdate = $this->user_model->updateDocumentByStudentType($userId, $documentMasterId, $updatedData);
            }
            else
            {
                $insertData = [
                    "user_id"           => $userId,
                    "user_type"           => $userType,
                    "name"              => $fileName,
                    "url"      => base_url() . $uploadPath . '/' . $fileName,
                    "document_meta_id"  => $documentMasterId
                ];

                $documentInsert = $this->user_model->addDocument($insertData);
            }

            echo "SUCCESS";

            //redirect('/user/account/update');
        }
    }

    function courseListByUniversityId(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
      //  redirect('user/account');
      //  exit();
      }

      $universityId = $this->uri->segment('4');
      $searchText = $_GET['query'];

      $courseList = $this->counsellor_model->courseListByUniversityId($universityId, $searchText);

      $skeys = [];

      foreach ($courseList as $keyv => $valuev) {

        $skeys [] = ['id' => $valuev['id'],'name' => $valuev['name']];

      }

  //echo "<pre>";
  //print_r($fsearchKeyArray);
  $response['options'] = $skeys;
  header('Content-Type: application/json');
  echo (json_encode($response));

    }

    function counsellorsEarnings(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
        redirect('user/account');
        exit();
      }

      $counsellorId = $usrdata['id'] ? $usrdata['id'] : 5826;
      $key = "COUNSELLOR-CHILD-".$counsellorId;

      $keyExist = $this->redi_model->getKey($key);

      if($keyExist){

         $allChild = $keyExist;

      } else {

        $getChild = $this->counsellor_model->getChild($counsellorId);

        $allChild = $getChild && $getChild[0]['child_ids'] ? $getChild[0]['child_ids'].", $counsellorId" : $counsellorId;

        $setKey = $this->redi_model->setKey($key, $allChild);
      }

      $response = $this->counsellor_model->studentData($allChild);

      $potentialEarningsArray = ['APPLICATION_PROCESS', 'APPLICATION_SUBMITTED', 'ADMIT_RECEIVED_CONDITIONAL', 'ADMIT_RECEIVED_UNCONDITIONAL'];
      $actualEarningsArray = ['DISBURSED', 'COMPLETED'];

      $totalEarning = 0;
      $potentialEarning = 0;
      $actualEarning = 0;

      foreach ($response as $key => $value) {

        if(in_array($value['status'], $potentialEarningsArray)){
          $potentialEarning = $potentialEarning + $value['count'];
        }

        if(in_array($value['status'], $actualEarning)){
          $actualEarning = $actualEarning + $value['count'];
        }

        $totalEarning = $totalEarning + $value['count'];

      }

      $this->outputData['earning_data'] = [
        ['stage' => 'Total Earning', 'earning' => $totalEarning * 2000, "leads" => $totalEarning],
        ['stage' => 'Potential Earning', 'earning' => $potentialEarning * 2000, "leads" => $potentialEarning],
        ['stage' => 'Actual Earning', 'earning' => $actualEarning * 2000, "leads" => $actualEarning]
      ];

      $this->render_page('templates/counsellor/earnings',$this->outputData);

    }

  }

?>
