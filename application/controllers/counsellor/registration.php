<?php

/*ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);*/

require_once APPPATH . 'libraries/Mail/sMail.php';
//require_once APPPATH . 'libraries/codeigniter-predis/src/Redis.php';


class Registration extends MY_Controller {

      public $outputData;		//Holds the output data for each view
      public $loggedInUser;


	 function __construct()

    {

        parent::__construct();

        $this->load->library('template');

        $this->lang->load('enduser/home', $this->config->item('language_code'));

		$this->load->library('form_validation');

		$this->load->model('location/country_model');

    $this->load->model('webinar/webinar_model');

    $this->load->model('webinar/common_model');

     $this->load->model('course/course_model');

		$this->load->model('course/degree_model');

    $this->load->model('user/user_model');

      //  track_user();


    }

    //counsellor registration
    function counsellorForm(){

      $indianState = $this->common_model->get(['country_id'=> 99],'state_master');
      $this->outputData['state_list'] = $indianState['data'];

      $this->render_page('templates/counsellor/counsellor_registration_form',$this->outputData);

    }

    function utaRegistration(){
      $this->render_page('templates/meeting/UTARegistration');
    }

    function thankYou(){
      $this->render_page('templates/meeting/UTAThankyouPage');
    }

    function cityListByStateId(){

      $stateId = $this->input->post('state_id');

      $cityListByStateId = $this->common_model->get(['state_id'=> $stateId],'city_master');

      $response['city_list'] = $cityListByStateId['data'];

      header('Content-Type: application/json');
      echo (json_encode($response));

    }

    function counsellorSubmit(){

      $userName     	= $this->input->post('user_name');
      $userEmail 		  = $this->input->post('user_email');
      $userMobile 	  = $this->input->post('user_mobile');
      $userPassword	  = $this->input->post('user_password');
      $userState 		  = $this->input->post('state_id');
      $userCity       = $this->input->post('city_id');
      $userAdd       = $this->input->post('user_add');
      $agencyName     = $this->input->post('agency_name');
      $userId         = $this->input->post('user_id');
      $roleId         = 8;

      $checkMobileVerified = $this->webinar_model->checkMasterOtp($userMobile);

      if(!$checkMobileVerified){
        echo "Mobile Number Not Verified ..!!!";
        exit();
      }

      $checkUserExist = $this->webinar_model->checkUserLogin($userEmail,$userMobile);

      if($checkUserExist){
        echo "With Email-ID Or Mobile Account Already Exists";
        exit();
      }

      $checkUserIDExist = $this->webinar_model->checkUserIDLogin($userId);

      if($checkUserIDExist){
        echo "User Name Already Exists. Please Select Different User Name ...!!!";
        exit();
      }

      $data = array(

          'name' 			=>  $this->input->post('user_name'),
          'username'  => $this->input->post('user_id'),
          'email' 		=>  $this->input->post('user_email'),
          'password' 	=>  md5($this->input->post('user_password')),
          'image'			=>  '',
          'type' 			=>  '8',
          'createdate' 	=>  date('Y-m-d H:i:s'),
          'activation' 	=>	md5(time()),
          'fid'			  =>  '',
          'status' 		=>	'1',
          'phone'     => $this->input->post('user_mobile')

      );

      $user_id = $this->user_model->insertUserMaster($data);

      $session_data = array(

         'id' => $user_id,
         'name'=> $this->input->post('user_name'),
         'username'=> $this->input->post('user_id'),
         'email' => $this->input->post('user_email'),
         'type' => 8,
         'typename' => 'Partner Counselor',
         'fid' =>'',
         'is_logged_in' => true

     );

     $this->session->set_userdata('user', $session_data);

      $counsellor_data = array(
          'user_id' 				=> $user_id,
          'country'         => 99,
          'state'           => $userState,
          'city'            => $userCity,
          "address"         => $userAdd,
          'facilitator_id'	=> json_encode([1]),
          'originator_id' 	=> $user_id,
          'other_details'   => json_encode(["agency_name" => $agencyName])
      );

      $this->common_model->post($counsellor_data,'partners');


      $studentName = $this->input->post('user_name');
      $studentEmail = $this->input->post('user_email');
      $username = $this->input->post('user_id');
      $password = $this->input->post('user_password');
      $mailSubject = "HelloUni - Thank you for Registering for the Webinar Series";
      $mailTemplate = "Hi $studentName,

                        Thank you for registering with HelloUni!

                        At HelloUni, we help you connect with institutions so that you can obtain clear and honest feedback on your questions and doubts directly from the University!

                        We hope you obtain maximum benefit from HelloUni, we sincerely applaud your aspirations and will do our utmost to help you reach your goals!

                        Your Login Credentials :

                        Username : $username
                        Password : $password

                        Regards,
                        Team HelloUni
                        Tel: +91 81049 09690
                        Email: info@hellouni.org

                         <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

      $ccMailList = '';
      $mailAttachments = '';

      //$studentEmail = 'nikhil.gupta@issc.in';

      $sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

      if($sendMail){
        echo "SUCCESS";
      } else {
        echo "SUCCESS";
      }

    }

    //counsellor Team registration
    function counsellorTeamForm(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
        redirect('user/account');
        exit();
      }

      $roleId = $userData['type'];

      $indianState = $this->common_model->get(['country_id'=> 99],'state_master');
      $this->outputData['state_list'] = $indianState['data'];
      $this->outputData['role_id'] = $roleId;

      $this->render_page('templates/counsellor/counsellor_team_registration_form',$this->outputData);

    }

    function counsellorTeamSubmit(){

      $this->load->helper('cookie_helper');
      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
        redirect('user/account');
        exit();
      }

      $userName     	= $this->input->post('user_name');
      $userEmail 		  = $this->input->post('user_email');
      $userMobile 	  = $this->input->post('user_mobile');
      $userPassword	  = $this->input->post('user_password');
      //$userDesignation  = $this->input->post('user_designation');
      $userState 		  = $this->input->post('state_id');
      $userCity       = $this->input->post('city_id');
      $userAdd       = $this->input->post('user_add');
      $agencyName     = $this->input->post('agency_name');
      $userId       = $this->input->post('user_mobile');
      $roleId         = $this->input->post('role_id');;

      $parentId = $usrdata['id'] ? $usrdata['id'] : 1 ;

      /*$checkMobileVerified = $this->webinar_model->checkMasterOtp($userMobile);

      if(!$checkMobileVerified){
        echo "Mobile Number Not Verified ..!!!";
        exit();
      }*/

      $checkUserExist = $this->webinar_model->checkUserLogin($userEmail,$userMobile);

      if($checkUserExist){
        echo "With Email-ID Or Mobile Account Already Exists";
        exit();
      }

      $checkUserIDExist = $this->webinar_model->checkUserIDLogin($userId);

      if($checkUserIDExist){
        echo "User Name Already Exists. Please Select Different User Name ...!!!";
        exit();
      }

      $data = array(

          'name' 			=>  $this->input->post('user_name'),
          'username'  => $this->input->post('user_mobile'),
          'email' 		=>  $this->input->post('user_email'),
          'password' 	=>  md5($this->input->post('user_password')),
          'image'			=>  '',
          'type' 			=>  $this->input->post('role_id'),
          'parent_id' =>  $parentId,
          'createdate'=>  date('Y-m-d H:i:s'),
          'activation'=>	md5(time()),
          'fid'			  =>  '',
          'status' 		=>	'1',
          'phone'     => $this->input->post('user_mobile')

      );

      $user_id = $this->user_model->insertUserMaster($data);

      $counsellor_data = array(
          'user_id' 				=>	$user_id,
          'facilitator_id'	=>	json_encode([1]),
          'originator_id' 	=>	$parentId,
          'other_details'   =>  json_encode(["agency_name" => $agencyName]),
          "state" => $userState,
          "city" => $userCity,
          "designation" => $userDesignation,
          "address" => $userAdd
      );

      $this->common_model->post($counsellor_data,'partners');


      $studentName = $this->input->post('user_name');
      $studentEmail = $this->input->post('user_email');
      $username = $this->input->post('user_id');
      $password = $this->input->post('user_password');
      $mailSubject = "HelloUni - Thank you for Registering for the Webinar Series";
      $mailTemplate = "Hi $studentName,

                        Thank you for registering with HelloUni!

                        At HelloUni, we help you connect with institutions so that you can obtain clear and honest feedback on your questions and doubts directly from the University!

                        We hope you obtain maximum benefit from HelloUni, we sincerely applaud your aspirations and will do our utmost to help you reach your goals!

                        Your Login Credentials :

                        Username : $username
                        Password : $password

                        Regards,
                        Team HelloUni
                        Tel: +91 81049 09690
                        Email: info@hellouni.org

                         <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

      $ccMailList = '';
      $mailAttachments = '';

      //$studentEmail = 'nikhil.gupta@issc.in';

      $sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

      if($sendMail){
        echo "SUCCESS";
      } else {
        echo "SUCCESS";
      }

    }

    function studentsignupform(){

        $this->outputData['countries'] = $this->country_model->getCountries(array('country_master.status'=>1));

    		$this->outputData['mainstreams'] = $this->course_model->getAllCollegeCategories();

        $this->outputData['mainstreams'] = $this->course_model->getAllCollegeCategories();

    		$this->outputData['degrees'] = $this->degree_model->getDegree(array('degree_master.status'=>1));

        $this->render_page('templates/counsellor/student_add_form',$this->outputData);
    }

    function studentsignup(){

      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
        redirect('user/account');
        exit();
      }

      $counsellorId = $usrdata['id'] ? $usrdata['id'] : 0;

        $userName     	= $this->input->post('user_name');
        $userEmail 		  = $this->input->post('user_email');
        $userMobile 	  = $this->input->post('user_mobile');
        $userPassword	  = $this->input->post('user_password');
        $userCountry 		= $this->input->post('desired_country');
        $userMainstream = $this->input->post('desired_mainstream');
        $userSubcourse 	= $this->input->post('desired_subcourse');
        $userLevelofcourse 		= $this->input->post('desired_levelofcourse');

        $email = $this->input->post('user_email');
        $userId = $this->input->post('user_mobile');

        $checkUserExist = $this->webinar_model->checkUserLogin($userEmail,$userMobile);

        if($checkUserExist){
          //echo "With Email-ID Or Mobile Account Already Exists";
          $response["status"] = "With Email-ID Or Mobile Account Already Exists";
          header('Content-Type: application/json');
          echo (json_encode($response));
          exit();
        }

        $data = array(

            'name' 			=>  $this->input->post('user_name'),
            'username'=> $this->input->post('user_mobile'),
            'email' 		=>  $this->input->post('user_email'),
            'password' 		=>  md5($this->input->post('user_password')),
            'image'			=>  '',
            'type' 			=>  '5',
            'createdate' 	=>  date('Y-m-d H:i:s'),
            'activation' 	=>	md5(time()),
            'fid'			=>  '',
            'status' 		=>	'1',
            'registration_type' 		=>	'COUNSELLOR',
            'phone'        => $this->input->post('user_mobile')

        );

        $user_id = $this->user_model->insertUserMaster($data);

        $student_data = array(

            'user_id' 				=>	$user_id,
            'phone'        => $this->input->post('user_mobile'),
            'facilitator_id' 				=>	1,
            'originator_id' 				=>	$counsellorId

        );

        $studentId = $this->user_model->insertStudentDetails($student_data);

        $countriesId = is_array($this->input->post('desired_country')) ? json_encode($this->input->post('desired_country')) : json_encode(explode(",", $this->input->post('desired_country'))) ;

        $desireInfoObj = [

          'student_id' => $studentId,
          'desired_country' => $countriesId,
          'desired_course_name' => $this->input->post('desired_mainstream'),
          'desired_sub_course_name' => $this->input->post('desired_subcourse'),
          'desired_course_level' => $this->input->post('desired_levelofcourse')

        ];

        $this->user_model->insertDesireDetails($desireInfoObj);


        $studentName = $this->input->post('user_name');
        $studentEmail = $this->input->post('user_email');
        $username = $this->input->post('user_mobile');
        $password = $this->input->post('user_password');
        $mailSubject = "HelloUni - Thank you for Registering On HELLOUNI Platform.";
        $mailTemplate = "Hi $studentName,

                          Thank you for registering with HelloUni!

                          At HelloUni, we help you connect with institutions so that you can obtain clear and honest feedback on your questions and doubts directly from the University!

                          We hope you obtain maximum benefit from HelloUni, we sincerely applaud your aspirations and will do our utmost to help you reach your goals!

                          Your Login Credentials :

                          Username : $username
                          Password : $password

                          Regards,
                          Team HelloUni
                          Tel: +91 81049 09690
                          Email: info@hellouni.org

                           <img src='https://www.hellouni.org/img/hello_uni_mail.png'>";

        $ccMailList = '';
        $mailAttachments = '';

        //$studentEmail = 'nikhil.gupta@issc.in';

        //$sendMail = sMail($studentEmail, $studentName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);

        $response["status"] = "SUCCESS";
        $response["student_id"] = $user_id;
        header('Content-Type: application/json');
        echo (json_encode($response));

    }

    function studentofferlistview(){

      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
        //redirect('user/account');
        //exit();
      }

      $userId = $this->uri->segment('5');
      $this->outputData['user_id'] = $userId;

      $this->render_page('templates/counsellor/offerlist',$this->outputData);

    }

    function studentofferlist(){

      $userdata=$this->session->userdata('user');

      if(empty($userdata)){
        redirect('user/account');
        exit();
      }

      $userId = $this->uri->segment('4');
      $userType = $userdata['type'];
      $universityId = 0;
      if($userType == 3){
        $universityId = $userdata['university_id'];
      }
      $this->load->model('counsellor/counsellor_model');

      $offerListObj = $this->counsellor_model->studentofferlist($universityId);

      $response['offer_list'] = $offerListObj;
      $response['user_id'] = $userId;
      $response['university_id'] = $universityId;

      header('Content-Type: application/json');
      echo (json_encode($response));

    }


    function studentCommentListview(){


      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
        redirect('user/account');
        exit();
      }

      $this->load->model('user/user_model');

      $userId = $this->uri->segment('5');
      $this->outputData['user_id'] = $userId;

      $appliedUniversities = $this->user_model->getAppliedUniversities($userId);

      $commentTypes = array(
          'STUDENT' => 'Student',
          'UNIVERSITY' => 'University'
      );

      $leadStateList = array(
          'INTERESTED' => 'INTERESTED',
          'POTENTIAL' => 'POTENTIAL',
          'APPLICATION_PROCESS' => 'APPLICATION_PROCESS',
          'APPLICATION_SUBMITTED' => 'APPLICATION_SUBMITTED',
          'ADMIT_RECEIVED_CONDITIONAL' => 'ADMIT_RECEIVED_CONDITIONAL',
          'ADMIT_RECEIVED_UNCONDITIONAL' => 'ADMIT_RECEIVED_UNCONDITIONAL',
          'VISA_PROCESS_FINANCIAL_DOCUMENTS' => 'VISA_PROCESS_FINANCIAL_DOCUMENTS',
          'VISA_APPLIED' => 'VISA_APPLIED',
          'VISA_APPROVED' => 'VISA_APPROVED',
          'VISA_APPROVED_OFFER' => 'VISA_APPROVED_OFFER',
          'VISA_REJECTED' => 'VISA_REJECTED',
          'ADMIT_DENIED' => 'ADMIT_DENIED',
          'NOT_INTERESTED' => 'NOT_INTERESTED',
          'POTENTIAL_DECLINED' => 'POTENTIAL_DECLINED'
      );

      $this->outputData['applied_universities'] = $appliedUniversities;
      $this->outputData['comment_types'] = $commentTypes;
      $this->outputData['lead_state_list'] = $leadStateList;
      $this->outputData['student_login_id'] = $userId;

      $this->render_page('templates/user/comments',$this->outputData);

    }

    function studentCommentlist(){

      $this->load->helper('cookie_helper');
      $userdata=$this->session->userdata('user');
      if(empty($userdata))
      {
          redirect('user/account');
          exit();
      }

      $this->load->model('counsellor/counsellor_model');
      $userType = $userdata['type'];
      $userId = $this->uri->segment('4') ? $this->uri->segment('4') : 0;
      $entityId = $userdata['id'];

      $communications = $this->counsellor_model->getCommentsByStudent($userId);

      header('Content-Type: application/json');
      echo json_encode($communications);

    }

    function addComment(){

      $this->load->helper('cookie_helper');
      $userdata=$this->session->userdata('user');
      if(empty($userdata))
      {
          echo "INVALIDLOGIN";
          redirect('user/account');
          exit();
      }

      $usertype = $userdata['type'];

      //$entity_id = $usertype == 3 ? $userdata['university_id'] : NULL;
      $entity_id = $this->input->post('university_id') ? $this->input->post('university_id') : NULL;
      $entity_type = $usertype == 3 ? 'UNIVERSITY' : 'STUDENT';
      $student_id = $this->input->post('student_login_id') ? $this->input->post('student_login_id') : NULL;
      $added_by = $userdata['id'];
      $comments = $this->input->post('comment');

      $postData = [
        "entity_id" => $entity_id,
        "entity_type" => $entity_type,
        "student_login_id" => $student_id,
        "added_by" => $added_by,
        "comment" => $comments
      ];

      $insertCommentObj = $this->common_model->post($postData,"comments");

      echo "SUCCESS";

    }

    function add()
    {
        $userdata = $this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $postedData = $this->input->post();
        $postedData['added_by'] = $userdata['id'];

        $leadState = $postedData['lead_state'] ? $postedData['lead_state'] : NULL;
        $entityId = $postedData['entity_id'] ? $postedData['entity_id'] : NULL;
        $studentLoginId = $postedData['student_login_id'] ? $postedData['student_login_id'] : NULL;


        if($entityId){

          $whereLeadAppliedCondition = ["id" => $entityId];
          $leadAppliedPut = ["status" => $leadState];

          $leadAppliedDataUpdate = $this->common_model->put($whereLeadAppliedCondition, $leadAppliedPut, "users_applied_universities");
          $postedData['entity_id'] = $leadAppliedDataUpdate[0]['university_id'];
        }

        if($leadState){

          $getLeadStatus = $this->common_model->get(["user_id"=>$studentLoginId], "users_applied_universities");

					$stateArray = array('NOT_INTERESTED', 'POTENTIAL_DECLINED', 'APPLICATION_REVIEW_DECLINED', 'WITHDRAWN', 'REJECTED', 'VISA_REJECTED', 'INTERESTED', 'POTENTIAL', 'APPLICATION_PROCESS', 'APPLICATION_SUBMITTED', 'ADMIT_RECEIVED_CONDITIONAL', 'ADMIT_RECEIVED_UNCONDITIONAL', 'VISA_PROCESS_FINANCIAL_DOCUMENTS', 'VISA_APPLIED', 'VISA_APPROVED', 'VISA_APPROVED_OFFER', 'DISBURSAL_DOCUMENTATION', 'PARTIALLY_DISBURSED', 'DISBURSED', 'BEYOND_INTAKE');

							$currentState = -1;
	        		$maxState = -1;

							if($getLeadStatus['data']){

								foreach ($getLeadStatus['data'] as $keyls => $valuels) {
									$currentState = array_search($valuels['status'], $stateArray);
                  echo "$currentState";
									if($maxState < $currentState){
	                        $maxState = $currentState;
	                }
								}
								$leadState = $stateArray[$maxState];

							}

          $dateupdate = ["status" => $leadState];
          $dateWhere = ["user_id" => $studentLoginId];

          $leadAppliedDataUpdate = $this->common_model->put($dateWhere, $dateupdate, "student_details");
        }

        unset($postedData['lead_state']);

	      $this->common_model->post($postedData,"comments");

        redirect('/counsellor/student/comments/view/' . $postedData['student_login_id']);
    }

    function studentCommentListviewApplication(){


      $usrdata=$this->session->userdata('user');

      if(empty($usrdata)){
        redirect('user/account');
        exit();
      }

      $this->load->model('user/user_model');

      $userId = $this->uri->segment('5');
      $appId = $this->uri->segment('6');


      $appliedUniversities = $this->user_model->getApplicationInfo($userId,$appId);


      $this->outputData['application_info'] = $appliedUniversities;
      $this->outputData['user_id'] = $userId;
      $this->outputData['app_id'] = $appId;
      $this->outputData['student_login_id'] = $userId;

      $this->render_page('templates/user/comments',$this->outputData);

    }

    function studentCommentlistApplication(){

      $this->load->helper('cookie_helper');
      $userdata=$this->session->userdata('user');
      if(empty($userdata))
      {
          redirect('user/account');
          exit();
      }

      $this->load->model('counsellor/counsellor_model');
      $userType = $userdata['type'];
      $userId = $this->uri->segment('4') ? $this->uri->segment('4') : 0;
      $appId = $this->uri->segment('5') ? $this->uri->segment('5') : 0;
      $entityId = $userdata['id'];

      $communications = $this->counsellor_model->getCommentsByStudentApplication($userId,$appId);

      header('Content-Type: application/json');
      echo json_encode($communications);

    }

    function addCommentApplication(){

      $this->load->helper('cookie_helper');
      $userdata=$this->session->userdata('user');
      if(empty($userdata))
      {
          echo "INVALIDLOGIN";
          redirect('user/account');
          exit();
      }

      $usertype = $userdata['type'];
      $couselllorEmail = $userdata['email'] ? $userdata['email'] : 0;
      //$entity_id = $usertype == 3 ? $userdata['university_id'] : NULL;
      $app_id = $this->input->post('app_id') ? $this->input->post('app_id') : NULL;
      $entity_type = $usertype == 3 ? 'UNIVERSITY' : 'STUDENT';
      $student_id = $this->input->post('student_login_id') ? $this->input->post('student_login_id') : NULL;
      $added_by = $userdata['id'];
      $comments = $this->input->post('comment');

      $this->load->model('counsellor/counsellor_model');
      
      $communications = $this->counsellor_model->getCommentsByStudentApplication($student_id,$app_id);

      $student_name = $communications[0]['student_name'];

       $myAppliedUniversities = $this->common_model->getAppliedUniversitiesByUser($student_id,$app_id);
      
      $University_name = $myAppliedUniversities[0]['name'];
      $Course = $myAppliedUniversities[0]['university_application_course'];
      $Intake = $myAppliedUniversities[0]['university_course_intake_month'].' '.$myAppliedUniversities[0]['university_course_intake_year'];
      $facilitatorEmailId = $myAppliedUniversities[0]['facilitator_email'];
      $facilitatorName = $myAppliedUniversities[0]['facilitator_name'].' '.$myAppliedUniversities[0]['facilitator_last_name'];

      $postData = [
        "entity_id" => $app_id,
        "entity_type" => $entity_type,
        "student_login_id" => $student_id,
        "added_by" => $added_by,
        "comment" => $comments
      ];

      $insertCommentObj = $this->common_model->post($postData,"comments");

      $whereUserc = [ "id" => $app_id ];
      $updateDataObj = [ "comment_update_date" => date("Y-m-d h:m:s")];

      $updateUserApplicationObj = $this->common_model->put($whereUserc,$updateDataObj,"users_applied_universities");

      $notificationData = [
        "user_id" => $student_id,
        "title" => "Comments From Agent",
        "description" => $comments,
        "created_by" => $added_by
      ];

      $updateApplication = $this->common_model->post($notificationData, "notification_master");

      if($facilitatorEmailId != ''){
       // $ccMailList = "sudhir.kanojia@imperialplatforms.com,jay.rathod@imperialplatforms.com,$couselllorEmail";
      $ccMailList = "priyankadthool@gmail.com";
      $mailAttachments = '';
      //$facilitatorName = '';
      //$facilitatorEmailId = "priyankadthool@gmail.com";

       $mailSubject = "HelloUni – Update – $student_name – $University_name – New Comment Added ";

       $mailTemplate = "Dear Team,

                          There is been an update on the portal for below mentioned Applicant
                          Student Name - $student_name
                          University Name - $University_name
                          Course - $Course
                          Intake - $Intake

                          Comment - $comments


                           <b>Thanks and Regards,
                           HelloUni Co-ordinator
                           </b>

                           <img src='https://www.hellouni.org/img/hello_uni_mail.png'>


                           Kindly Login to the Application portal (portal Link) to check
                          <b>**Note</b>: This is an automated email, please do not reply to this email**
                          ";

       $sendMail = sMail($facilitatorEmailId, $facilitatorName, $mailSubject, $mailTemplate, $ccMailList, $mailAttachments);
       
      }

      
       

      echo "SUCCESS";

    }

    function addApplication()
    {
        $userdata = $this->session->userdata('user');
        if(empty($userdata))
        {
            redirect('common/login');
        }

        $postedData = $this->input->post();
        $postedData['added_by'] = $userdata['id'];

        $leadState = $postedData['lead_state'] ? $postedData['lead_state'] : NULL;
        $entityId = $postedData['entity_id'] ? $postedData['entity_id'] : NULL;
        $studentLoginId = $postedData['student_login_id'] ? $postedData['student_login_id'] : NULL;


        if($entityId){

          $whereLeadAppliedCondition = ["id" => $entityId];
          $leadAppliedPut = ["status" => $leadState];

          $leadAppliedDataUpdate = $this->common_model->put($whereLeadAppliedCondition, $leadAppliedPut, "users_applied_universities");
          $postedData['entity_id'] = $leadAppliedDataUpdate[0]['university_id'];
        }

        if($leadState){

          $getLeadStatus = $this->common_model->get(["user_id"=>$studentLoginId], "users_applied_universities");

          $stateArray = array('NOT_INTERESTED', 'POTENTIAL_DECLINED', 'APPLICATION_REVIEW_DECLINED', 'WITHDRAWN', 'REJECTED', 'VISA_REJECTED', 'INTERESTED', 'POTENTIAL', 'APPLICATION_PROCESS', 'APPLICATION_SUBMITTED', 'ADMIT_RECEIVED_CONDITIONAL', 'ADMIT_RECEIVED_UNCONDITIONAL', 'VISA_PROCESS_FINANCIAL_DOCUMENTS', 'VISA_APPLIED', 'VISA_APPROVED', 'VISA_APPROVED_OFFER', 'DISBURSAL_DOCUMENTATION', 'PARTIALLY_DISBURSED', 'DISBURSED', 'BEYOND_INTAKE');

              $currentState = -1;
              $maxState = -1;

              if($getLeadStatus['data']){

                foreach ($getLeadStatus['data'] as $keyls => $valuels) {
                  $currentState = array_search($valuels['status'], $stateArray);
                  echo "$currentState";
                  if($maxState < $currentState){
                          $maxState = $currentState;
                  }
                }
                $leadState = $stateArray[$maxState];

              }

          $dateupdate = ["status" => $leadState];
          $dateWhere = ["user_id" => $studentLoginId];

          $leadAppliedDataUpdate = $this->common_model->put($dateWhere, $dateupdate, "student_details");
        }

        unset($postedData['lead_state']);

        $this->common_model->post($postedData,"comments");

        redirect('/counsellor/student/comments/view/' . $postedData['student_login_id']);
    }

}

?>
