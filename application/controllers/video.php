<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Video extends CI_Controller
{
	public function index()
	{
        $data['room_id'] = $this->input->get('id');
		$this->load->view('video/index', $data);
	}

    public function demo()
    {
        $data['room_id'] = $this->input->get('id');
		$this->load->view('video/demo', $data);
    }

	public function config()
	{
		$config = '{
		    "path": "/peerjs",
		    "host": "peer.egamesplay.com",
		    "port": "443",
		    "config": {
		        "iceServers": [
		            {"url": "stun:stun.rummydesk.com:5349"},
		            {"url": "turn:turn.rummydesk.com:5349?transport=tcp", "username": "nik", "credential": "nik123"}
		        ]
		    }
		}';

		echo base64_encode($config);
	}

	public function conference($room_id, $user_name)
	{
		$data['room_id'] = $room_id;
		$data['user_name'] = $user_name;
		$this->load->view('video/conference', $data);
	}

	public function meet($room_id = NULL)
	{
		$data['room_id'] = $room_id;
		$this->load->view('video/meet', $data);
	}
}
