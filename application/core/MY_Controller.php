<?php

class MY_Controller extends CI_Controller {

    protected $data = array();

    function __construct() {
        parent::__construct();
    }

    function render_page($view) {
        $this->load->model('elements/settings_model');
        $sliderApproved = $this->input->get('preview') ? 0 : 1;
        $allUniversities = $this->settings_model->getAllUniversities($sliderApproved);
        $this->outputData['top_universities'] = $allUniversities;

        if($this->input->get('preview'))
        {
            $previewUniversity = $this->input->get('preview');
            foreach($allUniversities as $university)
            {
                if($university['id'] == $previewUniversity)
                {
                    $this->outputData['preview_university'] = $university;
                }
            }
        }

        $settings = $this->settings_model->getSettings();
        foreach($settings as $setting){
            $this->outputData[$setting->key] 	= $setting->value ;
        }

        /* $userdata=$this->session->userdata('user');
        if($userdata){ */
        //$this->outputData['user'] = $userdata;

        if($this->uri->segment('1')=='' || $this->uri->segment('2')=='home'){
            $this->load->view('templates/common/header', $this->outputData);
        }
        else if($this->uri->segment('1')=='utaday'){
            $this->outputData['config_title'] = 'University of Texas at Arlington - Register for UTA Day & Avail Opportunity for Application Fee Waiver';
            $this->outputData['config_meta_desc'] = 'Meet Individual Faculty and Department members directly online during University of Texas at Arlington - UTA Day & Avail Opportunity for Application Fee Waiver! Registration is Free for Prospective Students! Get all your doubts cleared now!';
            $this->outputData['config_meta_title'] = 'University of Texas at Arlington - Register for UTA Day & Avail Opportunity for Application Fee Waiver';
            $this->load->view('templates/common/header_solid', $this->outputData);
        }
        else{
            $this->load->view('templates/common/header_solid', $this->outputData);
        }

        //  $this->load->view('templates/common/sidebar', $this->data);
        $this->load->view($view, $this->data);
        $this->load->view('templates/common/footer', $this->data);
        /* }else{

        redirect('common/login');
        }*/
    }

}  // end-class MY_Controller
//...


?>
