<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Details extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()

    {

        parent::__construct();

       // $this->load->library('template');
      //  $this->load->model('user/user_model');
		//$this->load->model('administrator/settings_model');


     // $this->lang->load('enduser/home', $this->config->item('language_code'));

		//$this->load->model('youraccount/global_model');

       		//$this->load->model('youraccount/auth_model');

		//$this->load->library('form_validation');



        

    }

	
	function index()

	{    $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
	   $this->load->model('user/user_model');
	   $this->load->model('course/course_model');
	   
	   $condition1 = array('user_master.id'=>$this->uri->segment(4),'user_master.status'=>1);
	   $condition2 = array();
	   $user_details = $this->user_model->checkUser($condition1);
	   $uni_details = $this->user_model->getUniversityDetailsByid(array('university_id'=>$this->uri->segment(4)));
	   
	  // print_r($user_details); exit;
	   
	   //User details
	   $this->outputData['id'] 						= $user_details->id;
	   $this->outputData['name'] 					= $user_details->name;
	   $this->outputData['email'] 					= $user_details->email;
	   
	   //University Details
	   $this->outputData['about'] 					= $uni_details->about;
	   $this->outputData['website'] 				= $uni_details->website;
	   $this->outputData['student'] 				= $uni_details->student;
	   $this->outputData['internationalstudent'] 	= $uni_details->internationalstudent;
	   $this->outputData['facilities'] 				= $uni_details->facilities;
	   $this->outputData['address'] 				= $uni_details->address;
	   $this->outputData['about'] 					= $uni_details->about;
	   $this->outputData['country'] 				= $uni_details->country;
	   $this->outputData['state'] 					= $uni_details->state;
	   $this->outputData['city'] 					= $uni_details->city;
	   
	   
	    $this->outputData['mainstream'] 		= $this->input->post('mainstream');
		$this->outputData['courses'] 		= $this->input->post('courses');
		$this->outputData['degree'] 		= $this->input->post('drg');
		$this->outputData['countries'] = $this->input->post('countries'); 
				
		$uni_search_criteria=$this->session->userdata('uni_search_criteria');
		
		$condition3 = array(
							'university_to_degree_to_course.degree_id'		=> $uni_search_criteria['degree'],
							'university_to_degree_to_course.course_id'		=> $uni_search_criteria['subcourse'],
							'university_to_degree_to_course.university_id'	=> $this->uri->segment(4)
					  );
		 
		$university_to_degree_to_course = $this->user_model->getuniversity_to_degree_to_course($condition3);
		
	   $this->outputData['id'] 					= $university_to_degree_to_course->id;
	   $this->outputData['degree_id'] 			= $university_to_degree_to_course->degree_id;
	   $this->outputData['degree_name']			= $this->degree_model->getDegreeByid($university_to_degree_to_course->degree_id)->name;
	   $this->outputData['course_id'] 			= $university_to_degree_to_course->course_id;
	   $this->outputData['university_id'] 		= $university_to_degree_to_course->university_id;
	   $this->outputData['language'] 			= $university_to_degree_to_course->language;
	   $this->outputData['admissionsemester'] 	= $university_to_degree_to_course->admissionsemester;
	   $this->outputData['beginning'] 			= $university_to_degree_to_course->beginning;
	   $this->outputData['duration'] 			= $university_to_degree_to_course->duration;
	   $this->outputData['application_deadline']= $university_to_degree_to_course->application_deadline;
	   $this->outputData['details'] 			= $university_to_degree_to_course->details;
	   
	   $this->outputData['tutionfee'] 			= $university_to_degree_to_course->tutionfee;
	   $this->outputData['enrolmentfee'] 		= $university_to_degree_to_course->enrolmentfee;
	   $this->outputData['livingcost'] 			= $university_to_degree_to_course->livingcost;
	   $this->outputData['jobopportunities'] 	= $university_to_degree_to_course->jobopportunities;
	   $this->outputData['required_language'] 	= $university_to_degree_to_course->required_language;
	   $this->outputData['academicrequirement'] = $university_to_degree_to_course->academicrequirement;
		
		
		//echo '<pre>'; print_r($university_to_degree_to_course); echo '</pre>'; exit;
		$this->render_page('templates/university/university_details',$this->outputData);
    } 
	
	function create()

	{
     //Load Models - for this function
	 $this->load->model('user/user_model');
	 $this->load->model('user/auth_model');
	 $this->load->helper('cookie'); 
	
	 
     $email = $this->input->post('email');
	 
	 $data = array(
        	
			'name' 			=>  $this->input->post('name'),
			'email' 		=>  $this->input->post('email'),
			'phone' 		=>  $this->input->post('phone')
        				
		);
	 
	
	 if($data)
	 {$leadId = $this->user_model->insertLead($data); 
	 }
		 
	
	  
	  $sessiondata = array(
     'id' 			=> $leadId,
	 'name'			=> $this->input->post('name'),
     'email'		=> $this->input->post('email'),
     'phone' 		=> $this->input->post('phone'),
	 'mainstream'   => $this->input->post('mainstream'),
	 'courses' 		=> $this->input->post('courses'),
     'degree' 		=> $this->input->post('degree'),
	 'countries'    => $this->input->post('countries')
     );
	 
	 
     $this->session->set_userdata('leaduser', $sessiondata);
	 					
	 redirect('search/university');
	
} 


function logout()
 { 
  $this->load->model('user/auth_model');
  $this->auth_model->clearUserSession();
  //$this->session->set_flashdata('flash_message', $this->common_model->flash_message('success',$this->lang->line('logout_success')));
  //$this->auth_model->clearUserCookie(array('username','password'));
   //$userdata=$this->session->userdata('user');
   $this->session->unset_userdata('user');
  //unset($userdata);
  //print_r($userdata); exit();
  //$this->auth_model->clearUserCookie(array('user_name','user_password'));
  redirect('common/login');
  //$this->load->view('youraccount/index',$userdata);
    
 } //Function logout End



function forgot()

	{ 
     //Load Models - for this function
	 $this->load->helper(array('form', 'url'));
	 $this->load->library('form_validation');
	 $this->load->model('user/user_model');
	 $this->load->model('user/auth_model');
	 $this->load->helper('cookie'); 
	 
	 $this->form_validation->set_rules('credencial', 'Credencial', 'required|trim|xss_clean');
	 $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean');
	 
	 if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('templates/common/login');
			//$this->session->set_flashdata('flash_message', $this->form_validation->validation_errors());
			//redirect('common/login'); 
			//die('sssss');
		}
		else
		{  
	 
	
	 $credencial 	= $this->input->post('credencial');
	 $email 		= $this->input->post('email');
	 
	 $conditions 	=  array('infra_email_master.email'=>$email,'infra_email_master.contact_type'=>'1');
	 $query			= $this->user_model->checkUserBy_Email($conditions);
	 
	 $setting_details		= $this->settings_model->getSettings($conditions);
	 
	 foreach($setting_details as $setting){
	 if($setting->key=='config_email'){
	 $admin_email = $this->settings_model->getEmail($setting->value);
	  }
	 }
	 
	 if(count($query)> 0){
	 
	 if($query->status_id==1){
	 
	 if($credencial=='username'){
	
	 $to = $query->email;
	 $from = $admin_email;
	 $subject = 'Your User Name';
	 $username = $query->name;
	 
	 $link = '#'; $linktext = $username; 
	 $text = 'Your User Name is following -';
	 $body = $this->user_model->TempBody($username,$link,$linktext,$text);
	 
	 $this->user_model->SendMail($to,$from,$subject,$body) ;
	 $this->session->set_flashdata('flash_message', "Please check your mail to get User Name.");
	 
	 }elseif($credencial=='password'){
	 
	 // send the mail
	 $to = $query->email;
	 $from = $admin_email;
	 $subject = 'Please check you mail';
	 $username = $query->name;
	
	// $user_tbl_data['activation_key']     = md5(time());
	 
	 $user_tbl_data = array('reset_key' => md5(time()));
	 
	 $this->user_model->Update_User_activation($user_tbl_data,$query->id);
	 
	 
	 $link = base_url().'common/login/confirm/'.$query->id.'/'.$user_tbl_data['reset_key'];
	 $linktext = $link; 
	 $text = 'Please click the link below or copy and fire it in the browser to reset password';
	 
	 $body = $this->user_model->TempBody($username,$link,$linktext, $text);
	 
	 $this->user_model->SendMail($to,$from,$subject,$body) ;
	 $this->session->set_flashdata('flash_message', "Please check your mail to reset password.");
	 
	  }
	 }elseif($query->status_id==2){
	 
	 $this->session->set_flashdata('flash_message', "Sorry!Your account is Inactive. Please activate the account first.");
	
	 }elseif($query->status_id==3){
	 
	 $this->session->set_flashdata('flash_message', "Sorry!Your account is Blocked. Please contact to supaer admin.");
	
	 }elseif($query->status_id==4){
	 
	 $this->session->set_flashdata('flash_message', "Sorry!Your account is Deleted.");
	
	 }else{
	 
	  $this->session->set_flashdata('flash_message', "Sorry!Invalid email. Please sign up first.");
	 }
	 
	 }else{
	
	$this->session->set_flashdata('flash_message', "Sorry!Invalid email. Please sign up first.");
	}
	
	
	}
	
	 redirect('common/login');
	
} 


function confirm()

	{ 
	  if($this->uri->segment('5')){
	  
	  
	  $check_reset_data = array('infra_user_master.id' =>$this->uri->segment('4'),'infra_user_master.reset_key' => $this->uri->segment('5'),'infra_user_master.status_id' =>'1');
	  $query = $this->user_model->CheckReset($check_reset_data);
	  
	  if(count($query)> 0){
	  
	  $check_security_data = array('user_security_question.user_id' =>$this->uri->segment('4'));
	  $security_query = $this->user_model->getSecurity_Credential($check_security_data);
	 
	  $this->outputData['security_question1']= $security_query->question1; 
	  $this->outputData['security_question2']= $security_query->question2;
	  
	  $this->load->view('templates/common/security_verify',$this->outputData);
	  
	  }else{
	  
	     
	  
	  }
	  /*echo '<pre>';
	  print_r($query);
	  echo '</pre>';*/
	  }
	}

	
	function verification(){
	
	//echo $this->input->post('currenturl'); exit;
	
	 $check_security_data = array('user_security_question.user_id' =>$this->uri->segment('4'));
	 $query = $this->user_model->getSecurity_Credential($check_security_data);
	 
	 
	 $security_ans1 		= md5($this->input->post('security_ans1'));
	 
	 $security_ans2 		= md5($this->input->post('security_ans2'));
	 
	 if($query->answer1==$security_ans1 && $query->answer2==$security_ans2){
	 
	 $this->outputData['userid']= $this->uri->segment('4'); 
	  $this->load->view('templates/common/resetpassword',$this->outputData);
	 
	 }else{
	 
	  $this->session->set_flashdata('flash_message', "Sorry! Incorrect Security answeres. Please try again");
	  redirect($this->input->post('currenturl'));
	 }
	
	}
	
	
	function resetpassword(){
  
     if($this->input->post('submit')){
	
	 $user_tbl_data = array('infra_user_master.password' => md5($this->input->post('newpassword')));
	 
	 $this->user_model->Update_User_activation($user_tbl_data,$this->uri->segment('4'));
	 
	 
	 
	 
	 
	  $this->session->set_flashdata('flash_message', "Congrats! Pasword has been reset. Please Log In");
	  redirect('common/login');
	  
	 }
    }
	

}//End  Home Class





?>