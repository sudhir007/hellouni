<style>
    .shade_border{
        background:url(img/shade1.png) repeat-x; height:11px;
    }
    .search_by{
        background-color:#f3f4f4; box-shadow:5px 5px 5px #eae8e8; height:auto;
        padding-top: 10px;
        padding-left: 30px;
        padding-right: 30px;
    }
    .search_by h4{
        color:#3c3c3c;
        font-family:lato_regular;
        font-weight:500;
    }
    .table_heading{
        padding:5px;
        font-weight:bold;
    }
    .more_detail{
        background-color:#F6881F;
        text-transform:uppercase;
        height:28px;
        border:0px;
        color:#000;
        border-radius:2px;
        width:25px;
    }
    .right_side_form{
        background:#3c3c3c;
        box-shadow:5px 5px 2px #CCCCCC;
        height:auto;
    }
    label{
        color:#818182;
        font-size:11px;
        font-weight:300;
        margin-top:5px;
    }
    .input_field{
        width:400px !important;
        height:30px;
    }
    .more_detail{
        background-color:#F6881F;
        text-transform:uppercase;
        height:28px;
        border:0px;
        color:#fff;
        border-radius:2px;
    }
    .input_container{
        position:relative;
        padding:0;
        margin:0;
        padding-top: 25px;
    }
    #input{
        margin:0;
        padding-left: 62px;
    }
    #input_img{
        position:absolute;
        bottom:150px;
        left:15px;
        width:50px;
        height:27px;
        margin-left:-5px;
    }
    #input2{
        margin:0;
        padding-left: 62px;
    }
    #input_img2{
        position:absolute;
        bottom:100px;
        left:15px;
        width:50px;
        height:27px;
    }
    #input3{
        margin:0;
        padding-left: 62px;
    }
    #input_img3{
        position:absolute;
        bottom:163px;
        left:15px;
        width:50px;
        height:27px;
    }
    .login_background{
        background-color:#f3f4f6; height:auto;
    }
    .form_font_color{
        color:#818182;
    }
    .form_font_color a{
        color:#818182;
    }
    .button_sign_in{
        background-color:#F6881F;
        border:none;
        width: 100px;
        height: 28px;
        margin-bottom:20px;
    }
    .sign_line{
        background:url(img/line2.png) repeat-y;
        height:100px;
    }
    .login_head{
        color:#F6881F;
        padding-left:0px;
    }
    .login_head2{
        text-transform:capitalize;
        color:#3C3C3C;"
    }
    .header_icons ul li{
        display:inline-block;
        list-style-type:none;
        margin-bottom:-10px;
    }
    .sep{
        margin-top: 20px;
        margin-bottom: 20px;
        background-color: #e6c3c3;
    }
    .invalid_color{
        color:red;
    }
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }
    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }
    tr:nth-child(even) {
        background-color: #dddddd;
    }
    #12thidd{
        display:none;
    }
    #diploma_id{
        display:none;
    }
    #masters_id{
        display:none;
    }
    #locationcountries{
        widows:50%;
    }
    #state{
        width:50%;
    }
    #city{
        width:23%;
    }
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 18px;
        font-weight: 700;
        color:#F6881F;
    }
    .tab button:hover {
        background-color: #ddd;
    }
    .tab button.active {
        background-color: #ccc;
    }
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }
    .showcontent{
        display: block;
    }
    .update_button{
        text-align: center;
    }
    #exam_stage, #exam_date, #result_date, #exam_score, #english_exam_stage, #english_exam_date, #english_result_date, #english_exam_score, #exam_score_gre, #exam_score_gmat, #exam_score_sat{
        display: none;
    }
    #personal_details .form-inline .form-group{
        margin-bottom: 15px;
    }
    #competitive_exam label, #english_proficiency_exam label, #exam_stage label, #exam_date label, #result_date label, #english_exam_stage label{
        margin-left: 10px;
        margin-right: 5px;
    }
    #exam_score label, #english_exam_score label{
        margin-left: 25px;
    }
    .col-lg-12, .col-lg-6{
        padding-left: 0px;
    }
    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }
    /* Modal Content */
        .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 50%;
    }
    /* The Close Button */
    .close {
        color: #8e0f0f;
        float: right;
        font-size: 28px;
        font-weight: bold;
        margin: -20px -10px -10px 0px;
    }
    .close:hover,.close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }
    #internship-description{
        text-align: center;
        font-size: 16px;
        padding: 10px;
    }
</style>

<div class="shade_border"></div>
    <div class="container login_background" >
        <div class="col-lg-12 col-md-12"> <h4 class="form_font_color" style="text-align:center;"><?=$university_name?></h4></div>
        <div class="col-lg-12 col-md-12">
            <?php
            if($this->session->flashdata('flash_message'))
            {
                ?>
                <h6 class="invalid_color"><?php echo $this->session->flashdata('flash_message');?></h6>
                <?php
            }
            ?>
            <div id="personal_details" class = "tabcontent showcontent">
                <form action="<?php echo base_url()?>university/details/application_submit" class="form-group form-inline input_container" method="post" enctype="multipart/form-data">
                    <div class="form-group col-md-12">
                        <label class="col-md-6">University Name</label>
                        <input type="text" name="university_name" class="form-control form_font_color input_field" value="<?=$university_name?>" disabled>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">College*</label>
                        <select class="form-control col-md-6" id="college" name="college">
                           <?php
                           foreach($colleges as $college_id => $college_name)
                           {
                               ?>
                               <option value="<?php echo $college_id;?>" <?=isset($applied_university_detail) && $applied_university_detail['college_id'] == $college_id ? 'selected' : ''?>><?php echo $college_name;?></option>
                                <?php
                            }
                            ?>
                         </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">Login URL</label>
                        <input type="text" name="login_url" class="form-control form_font_color input_field" value="<?=isset($applied_university_detail) ? $applied_university_detail['login_url'] : ''?>">
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">Username</label>
                        <input type="text" name="username" class="form-control form_font_color input_field" value="<?=isset($applied_university_detail) ? $applied_university_detail['username'] : ''?>">
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">Password</label>
                        <input type="password" name="password" class="form-control form_font_color input_field" value="<?=isset($applied_university_detail) ? $applied_university_detail['password'] : ''?>">
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">Application Deadline</label>
                        <input type="date" name="application_deadline" class="form-control form_font_color input_field" value="<?=isset($applied_university_detail) ? $applied_university_detail['dead_line'] : ''?>">
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">GRE (Code)</label>
                        <input type="text" name="gre_code" class="form-control form_font_color input_field" value="<?=isset($applied_university_detail) ? $applied_university_detail['gre_code'] : ''?>">
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">TOEFL (Code)</label>
                        <input type="text" name="toefl_code" class="form-control form_font_color input_field" value="<?=isset($applied_university_detail) ? $applied_university_detail['toefl_code'] : ''?>">
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">SOP (Statement Of Purpose)</label>
                        <label>Yes &nbsp;</label><input type="radio" name="sop_needed" value="1" onclick="showNoOfWords()" <?=isset($applied_university_detail) && isset($applied_university_detail['sop']['sop_needed']) && $applied_university_detail['sop']['sop_needed'] == 1 ? 'checked' : ''?>>
                        <label>No &nbsp;</label><input type="radio" name="sop_needed" value="0" onclick="hideNoOfWords()" <?=isset($applied_university_detail) && isset($applied_university_detail['sop']['sop_needed']) && $applied_university_detail['sop']['sop_needed'] == 0 ? 'checked' : ''?>>
                    </div>
                    <div class="form-group col-md-12" id="sop-no-of-words" style="display:<?=$sop_no_of_words?>">
                        <label class="col-md-6">SOP - Number Of Words</label>
                        <input type="text" name="sop_no_of_words" class="form-control form_font_color input_field" value="<?=isset($applied_university_detail) && isset($applied_university_detail['sop']['sop_no_of_words']) ? $applied_university_detail['sop']['sop_no_of_words'] : ''?>">
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">LOR (Letter Of Recommendation)</label>
                        <label>Yes &nbsp;</label><input type="radio" name="lor_needed" value="1" onclick="showLOR()" <?=isset($applied_university_detail) && isset($applied_university_detail['lor']['lor_needed']) && $applied_university_detail['lor']['lor_needed'] == 1 ? 'checked' : ''?>>
                        <label>No &nbsp;</label><input type="radio" name="lor_needed" value="0" onclick="hideLOR()" <?=isset($applied_university_detail) && isset($applied_university_detail['lor']['lor_needed']) && $applied_university_detail['lor']['lor_needed'] == 0 ? 'checked' : ''?>>
                    </div>
                    <div class="form-group col-md-12" id="lor-required" style="display:<?=$lor_required?>">
                        <label class="col-md-6">LOR Required</label>
                        <select class="form-control col-md-6" name="lor_required">
                           <?php
                           for($i = 1; $i <= 10; $i++)
                           {
                               ?>
                               <option value="<?=$i?>" <?=isset($applied_university_detail) && isset($applied_university_detail['lor']['lor_required']) && $applied_university_detail['lor']['lor_required'] == $i ? 'selected' : ''?>><?php echo $i;?></option>
                                <?php
                            }
                            ?>
                         </select>
                    </div>
                    <div class="form-group col-md-12" id="lor-type" style="display:<?=$lor_type?>">
                        <label class="col-md-6">LOR Type</label>
                        <label>Offline &nbsp;</label><input type="radio" name="lor_type" value="offline" <?=isset($applied_university_detail) && isset($applied_university_detail['lor']['lor_type']) && $applied_university_detail['lor']['lor_type'] == "offline" ? 'checked' : ''?>>
                        <label>Online &nbsp;</label><input type="radio" name="lor_type" value="online" <?=isset($applied_university_detail) && isset($applied_university_detail['lor']['lor_type']) && $applied_university_detail['lor']['lor_type'] == "online" ? 'checked' : ''?>>
                        <label>Online (After Application) &nbsp;</label><input type="radio" name="lor_type" value="online_after_application" <?=isset($applied_university_detail) && isset($applied_university_detail['lor']['lor_type']) && $applied_university_detail['lor']['lor_type'] == "online_after_application" ? 'checked' : ''?>>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">Transcripts</label>
                        <label>After Admit &nbsp;</label><input type="radio" name="transcript" value="after_admit" <?=isset($applied_university_detail) && isset($applied_university_detail['transcripts']['transcript']) && $applied_university_detail['transcripts']['transcript'] == "after_admit" ? 'checked' : ''?>>
                        <label>Before Admin &nbsp;</label><input type="radio" name="transcript" value="before_admit" <?=isset($applied_university_detail) && isset($applied_university_detail['transcripts']['transcript']) && $applied_university_detail['transcripts']['transcript'] == "before_admit" ? 'checked' : ''?>>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">Transcripts Submitted?</label>
                        <label>Yes &nbsp;</label><input type="radio" name="transcript_submitted" value="1" <?=isset($applied_university_detail) && isset($applied_university_detail['transcripts']['transcript_submitted']) && $applied_university_detail['transcripts']['transcript_submitted'] == "1" ? 'checked' : ''?>>
                        <label>No &nbsp;</label><input type="radio" name="transcript_submitted" value="0" <?=isset($applied_university_detail) && isset($applied_university_detail['transcripts']['transcript_submitted']) && $applied_university_detail['transcripts']['transcript_submitted'] == "0" ? 'checked' : ''?>>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">Application Fee</label>
                        <input type="text" name="application_fee" class="form-control form_font_color input_field" value="<?=isset($applied_university_detail) && isset($applied_university_detail['application_fee']['application_fee']) ? $applied_university_detail['application_fee']['application_fee'] : ''?>">
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">Application Fee Paid?</label>
                        <label>Yes &nbsp;</label><input type="radio" name="application_fee_paid" value="1" <?=isset($applied_university_detail) && isset($applied_university_detail['application_fee']['application_fee_paid']) && $applied_university_detail['application_fee']['application_fee_paid'] == "1" ? 'checked' : ''?>>
                        <label>No &nbsp;</label><input type="radio" name="application_fee_paid" value="0" <?=isset($applied_university_detail) && isset($applied_university_detail['application_fee']['application_fee_paid']) && $applied_university_detail['application_fee']['application_fee_paid'] == "0" ? 'checked' : ''?>>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">Request URL</label>
                        <input type="text" name="request_url" class="form-control form_font_color input_field" value="<?=isset($applied_university_detail) ? $applied_university_detail['requested_url'] : ''?>">
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">Status</label>
                        <select class="form-control col-md-6" name="status">
                           <option value="In Process" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "In Process" ? 'selected' : ''?>>In Process</option>
                           <option value="Submitted" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "Submitted" ? 'selected' : ''?>>Submitted</option>
                           <option value="Admit Received" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "Admit Received" ? 'selected' : ''?>>Admit Received</option>
                         </select>
                    </div>
                    <div class="update_button">
                        <input type="hidden" name="university_id" value="<?=$university_id?>">
                        <input type="submit" class="button_sign_in" value="APPLY">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>
        <div id="internship-description">
            Dear <?=($user_data) ? $user_data['name'] : ''?>, We thank you for showing interest in the Internship Program. The University representative will reach out to you shortly for further details.
        </div>
    </div>
</div>
<script type="text/javascript">
    function showNoOfWords(){
        document.getElementById("sop-no-of-words").style.display = "block";
    }
    function hideNoOfWords(){
        document.getElementById("sop-no-of-words").style.display = "none";
    }
    function showLOR(){
        document.getElementById("lor-required").style.display = "block";
        document.getElementById("lor-type").style.display = "block";
    }
    function hideLOR(){
        document.getElementById("lor-required").style.display = "none";
        document.getElementById("lor-type").style.display = "none";
    }
</script>
