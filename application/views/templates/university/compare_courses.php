<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #000;
  text-align: left;
  padding: 8px;
}

th{
	font-weight: bold;
}

tr:nth-child(even) {
  background-color: #dddddd;
}

.course_name{
    text-align: center;
    font-size: 20px;
    font-weight: bold;
    margin: 10px;
}

.compare_table{
    margin: 10px;
}

.align-vertical{
    vertical-align : middle;
    text-align:center;
}
</style>
<div class="container">
    <div class="row">
        <div class="course_name"><?=ucwords($course_name)?></div>
        <table class="compare_table">
            <thead>
                <th>University</th>
                <th>College</th>
                <th>Degree</th>
                <th>Duration</th>
                <th>Total Fees ($)</th>
                <th>Deadline URL</th>
                <th>Eligibility GPA</th>
                <th>Eligibility GRE</th>
                <th>Eligibility TOEFL</th>
                <th>Eligibility IELTS</th>
                <th></th>
            </thead>
            <tbody>
                <?php
                $visitedUniversities = array();
                $visitedColleges = array();
                foreach($comparedData as $data)
                {
                    ?>
                    <tr>
                        <?php
                        if(!in_array($data['university_id'], $visitedUniversities))
                        {
                            ?>
                            <td rowspan="<?=$data['university_span']?>" class="align-vertical"><?=$data['university_name']?></td>
                            <?php
                        }
                        if(!in_array($data['college_id'], $visitedColleges))
                        {
                            $visitedColleges[] = $data['college_id'];
                            ?>
                            <td rowspan="<?=$data['college_span']?>" class="align-vertical"><?=$data['college_name']?></td>
                            <?php
                        }
                        ?>
                        <td class="align-vertical"><?=$data['degree_name']?></td>
                        <td class="align-vertical"><?=$data['duration'] ? $data['duration'] . ' Months' : ''?></td>
                        <td class="align-vertical"><?=$data['total_fees']?></td>
                        <td class="align-vertical"><?=$data['deadline_link']?></td>
                        <td class="align-vertical"><?=$data['eligibility_gpa'] . ' / ' . $data['gpa_scale']?></td>
                        <td class="align-vertical"><?=$data['eligibility_gre']?></td>
                        <td class="align-vertical"><?=$data['eligibility_toefl']?></td>
                        <td class="align-vertical"><?=$data['eligibility_ielts']?></td>
                        <?php
                        if(!in_array($data['university_id'], $visitedUniversities))
                        {
                            $visitedUniversities[] = $data['university_id'];
                            ?>
                            <td rowspan="<?=$data['university_span']?>" class="align-vertical">
                                <form action="/university/details/connect" method="post">
                                    <input type="submit" name="connect" value="<?=$data['connected'] ? 'Connected' : 'Connect'?>" <?=$data['connected'] ? 'disabled' : ''?>>
                                    <input type="hidden" name="university_id" value="<?=$data['university_id']?>">
                                    <input type="hidden" name="university_login_id" value="<?=$data['university_login_id']?>">
                                </form>
                            </td>
                            <?php
                        }
                        ?>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
