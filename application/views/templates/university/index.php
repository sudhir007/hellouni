<style>
/*table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}*/

thead{
    background-color: #f6882c;
    color: white;
    font-size: 14px;
    font-weight: 600;
    letter-spacing: 0.8px;
}

.table>thead>tr>th{
    vertical-align: middle;
    text-align: center;
    padding: 8px 10px 8px 10px;
}
 
/*td, th {
  border: 1px solid #000;
  text-align: left;
  padding: 8px;
}

th{
	font-weight: bold;
}*/

/*tr:nth-child(even) {
  background-color: #dddddd;
}*/
/*.table>tbody>tr{
    
}*/

.table>tbody>tr>td{
    vertical-align: middle;
    text-align: center;
    padding: 8px 10px 8px 10px;
}

.table>tbody>tr:hover{
    background-color: #815cd5;
    color: white;
}

.course_name{
    text-align: center;
    font-size: 20px;
    font-weight: bold;
    margin: 10px;
}

.connectbtn{
        border-radius: 7px;
    color: white;
}
/*.compare_table{
    margin: 10px;
}*/

.sort_by{
	background-color: #f6881f
}
</style>
<div class="clearfix"></div>
<div id="page_loader">
   <div>
      <section style="background-color: rgba(0, 0, 0, 0);background-repeat: no-repeat;background-image: url(<?php echo base_url();?>application/images/findUniversity.jpg); background-size: cover;background-position: center center;width: 100%;height: 250%;box-shadow: inset 0 0 0 2000px rgba(4, 4, 4, 0.5);">
         <div class="container" style="height: 280px;">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="z-index: 5 !important; text-align: center;">
                  <h1 class="page-top-title" style="margin-top: 8%;color: #ffffff; text-align:center;font-weight:bold;font-size:37px;"><span>COMPARE UNIVERSIIES</span></h1>
                  <!-- <h2 class="page-top-title" style="margin-top: 2%;color: #ffffff; text-align:center;font-weight:bold;font-size:37px;"><span><?=$course_detail['department_name']?></span>
                  </h2> -->
                  <!-- <img src="<?php echo base_url();?>uploads/arizona_state/logo.jpg" style="width:150px;"><a href="<?php echo base_url();?>arizona-state-university" target="_blank" style="font-size:15px;color:#F6881F"> -->
               </div>
            </div>
         </div>
      </section>
      <div class="container">
         <div class="col-md-12" style="overflow: auto;margin-top: 20px;">
           <!--  <h1>compareee</h1> -->
                <table class="table table-hover table-responsive compare_table">
                    <thead>
                        <th>University</th>
                        <th>Country</th>
                        <th>Website</th>
                        <th>Established (Year)</th>
                        <th>Type</th>
                        <th>Total Students</th>
                        <th>Total Students (UG)</th>
                        <th>Total Students (PG)</th>
                        <th>Total International Students</th>
                        <th <?=$sort_by == 'ranking_world' ? 'class="sort_by"' : ''?>>World Ranking</th>
                        <th>National Ranking</th>
                        <th <?=$sort_by == 'admission_success_rate' ? 'class="sort_by"' : ''?>>Admission Success Rate</th>
                        <th <?=$sort_by == 'placement_percentage' ? 'class="sort_by"' : ''?>>Placement</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <?php
                        $visitedUniversities = array();
                        $visitedColleges = array();
                        foreach($comparedData as $data)
                        {
                            ?>
                            <tr>
                                <td><?=$data['name']?></td>
                                <td><?=$data['country_name']?></td>
                                <td><?=$data['website']?></td>
                                <td><?=$data['establishment_year']?></td>
                                <td><?=$data['type']?></td>
                                <td><?=$data['total_students']?></td>
                                <td><?=$data['total_students_UG']?></td>
                                <td><?=$data['total_students_PG']?></td>
                                <td><?=$data['total_students_international']?></td>
                                <td <?=$sort_by == 'ranking_world' ? 'class="sort_by"' : ''?>><?=$data['ranking_world']?></td>
                                <td><?=$data['ranking_usa']?></td>
                                <td <?=$sort_by == 'admission_success_rate' ? 'class="sort_by"' : ''?>><?=$data['admission_success_rate'] ? $data['admission_success_rate'] . '%' : 'NA'?></td>
                                <td <?=$sort_by == 'placement_percentage' ? 'class="sort_by"' : ''?>><?=$data['placement_percentage'] ? $data['placement_percentage'] . '%' : 'NA'?></td>
                                <td>
                                    <form action="/university/details/connect" method="post">
                                        <input class="btn btn-md orangeBg connectbtn" type="submit" name="connect" value="<?=$data['connected'] ? 'Connected' : 'Connect'?>" <?=$data['connected'] ? 'disabled' : ''?>>
                                        <input type="hidden" name="university_id" value="<?=$data['id']?>">
                                        <input type="hidden" name="university_login_id" value="<?=$data['user_id']?>">
                                    </form>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
         </div>
      </div>
    </div>
</div>

