<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<script type="text/javascript" src="
   https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<style type="text/css">
   #example1 td a {
   color: #f6882c !important;
   }
   .styleCss{
   font-size: 14px;
   background-color: #f6882c;
   color: white;
   font-weight: 600;
   letter-spacing: 0.8px;
   }
   .formControl{
   color: #fff !important;
   background-color: #8c57c5 !important;
   /* border: 0px; */
   font-size: 12px;
   border: 0px !important;
   box-shadow: inset 0 0 0 rgba(0,0,0,.075) !important;
   margin-top: 15px;
   }
   #example1 td.details-control {
   cursor: pointer;
   }
   #example1 tr.shown td {
   background-color: #815dd5;
   color: #fff !important;
   border: none;
   border-right: 1px solid #815dd5;
   }
   #example1 tr.shown td:last-child {
   border-right: none;
   }
   #example1 tr.details-row .details-table tr:first-child td {
   color: #fff;
   background: #f6882c;
   border: none;
   }
   #example1 tr.details-row .details-table > td {
   padding: 0;
   }
   #example1 tr.details-row .details-table .fchild td:first-child {
   cursor: pointer;
   }
   #example1 tr.details-row .details-table .fchild td:first-child:hover {
   background: #fff;
   }
   #example1 .form-group.agdb-dt-lst {
   padding: 2px;
   height: 23px;
   margin-bottom: 0;
   }
   #example1 .form-group.agdb-dt-lst .form-control {
   height: 23px;
   padding: 2px;
   }
   #example1 .adb-dtb-gchild {
   padding-left: 2px;
   }
   #example1 .adb-dtb-gchild td {
   background: #f5fafc;
   padding-left: 15px;
   }
   #example1 .adb-dtb-gchild td:first-child {
   cursor: default;
   }
   #example1 .fchild ~ .adb-dtb-gchild {
   /* display: none;
   */
   }
   .dataTables_wrapper{
   overflow: auto;
   }
   .listTableView thead tr{
   background-color: #f6882c;
   color: white;
   }

   .panelprimaryHeading {
    color: #fff;
    background-color: #815dd5 !important;
    border-color: #815dd5 !important;
   }

.panelBody{
    border-top: 3px solid #815dd5;
}

table.dataTable thead .sorting{
      text-align: center !important;
    vertical-align: middle !important;
}

table.dataTable thead tr {
      background-color: #815dd5 !important;
    color: white !important;
}
.btnOrange{
       background-color: #f6882c;
    border-color: #f6882c;
}
</style>
<div class="container">
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
            University : Manage Courses
          </h4>
        </section>

        <!-- Main content -->
        <section class="content">
           <div class="panel panel-primary">
            <div class="panel-heading panelprimaryHeading">Courses Filter</div>
            <div class="panel-body">
      			  <?php if($this->session->flashdata('flash_message')){ ?>
      				<div class="alert alert-success alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                      <h4><i class="icon fa fa-check"></i> Success</h4>
                      <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
      			  <?php } ?>
                  <div class="col-xs-12">
                    <form name="filter" id="filter" action="<?php echo base_url();?>university/course/filter" method="POST">
                    <div class="col-md-2">
                      <select style="width: 150px" name="university_id" id="university_id" onchange="this.form.submit()">
                        <option value="">Select University</option>
                        <?php
                        if(isset($university_list) && !empty($university_list)){
                        foreach ($university_list as $ulkey => $ulvalue) {
                        ?>
                        <option value="<?php echo $ulvalue['id'] ; ?>" <?php if(isset($selected_university_id) && $selected_university_id == $ulvalue['id']){ echo "selected"; } ?>><?php echo $ulvalue['name'] ; ?></option>
                      <?php } } ?>
                      </select>
                    </div>

                    <div class="col-md-2">
                      <select style="width: 150px" name="campus_id" id="campus_id" onchange="this.form.submit()">
                        <option value="">Select Campus</option>
                        <?php
                        if(isset($campus_list) && !empty($campus_list)){
                        foreach ($campus_list as $clkey => $clvalue) {
                        ?>
                        <option value="<?php echo $clvalue['id'] ; ?>" <?php if(isset($selected_campus_id) && $selected_campus_id == $clvalue['id']){ echo "selected"; } ?>><?php echo $clvalue['name'] ; ?></option>
                      <?php } } ?>
                      </select>
                    </div>

                    <div class="col-md-2">
                      <select style="width: 150px" name="college_id" id="college_id" onchange="this.form.submit()">
                        <option value="">Select College</option>
                        <?php
                        if(isset($college_list) && !empty($college_list)){
                        foreach ($college_list as $cgkey => $cgvalue) {
                        ?>
                        <option value="<?php echo $cgvalue['id'] ; ?>" <?php if(isset($selected_college_id) && $selected_college_id == $cgvalue['id']){ echo "selected"; } ?>><?php echo $cgvalue['name'] ; ?></option>
                      <?php } } ?>
                      </select>
                    </div>

                    <div class="col-md-2">
                      <select style="width: 150px" name="department_id" id="department_id" onchange="this.form.submit()">
                        <option value="">Select Department</option>
                        <?php
                        if(isset($department_list) && !empty($department_list)){
                        foreach ($department_list as $dlkey => $dlvalue) {
                        ?>
                        <option value="<?php echo $dlvalue['id'] ; ?>" <?php if(isset($selected_department_id) && $selected_department_id == $dlvalue['id']){ echo "selected"; } ?>> <?php echo $dlvalue['name'] ; ?></option>
                      <?php } } ?>
                      </select>
                    </div>

                    <div class="col-md-2">
                      <select style="width: 150px" name="course_id" id="course_id" onchange="this.form.submit()">
                        <option value="">Select Course</option>
                        <?php
                        if(isset($course_list) && !empty($course_list)){
                        foreach ($course_list as $cokey => $covalue) {
                        ?>
                        <option value="<?php echo $covalue['id'] ; ?>" <?php if(isset($selected_course_id) && $selected_course_id == $covalue['id']){ echo "selected"; } ?>> <?php echo $covalue['name'].' - '. $covalue['dname']; ?></option>
                      <?php } } ?>
                      </select>
                    </div>
                  </form>
                  </div>
            </div>
            </div> 
            <div class="panel panel-primary panelBody">
                <div class="panel-body ">
			  <form action="<?php echo base_url();?>university/university/multidelete" method="post">

                <div class="box-header">
                 <a href="<?php echo base_url();?>university/course/form"><button type="button" style="width:12%; float:left; margin-right:5px;" class="btn btn-block btn-primary"><i class="fa fa-plus"></i> Add Course</button></a>
				 <button type="submit" class="btn btn-block btn-danger multidel" style="width:11%; display:none;"><i class="fa fa-minus"></i> Delete University</button>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th></th>
                        <th>Name</th>
                        <th>College</th>
                        <th>University</th>
                        <th>Degree</th>
                        <th>Duration</th>
                        <th>Deadline Link</th>
                        <th>Total Fees</th>
                        <th>Option</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php $i=1;
					 foreach($courses as $usr){ ?>
                      <tr>
                        <td><input type="checkbox" class="multichk" name="chk[]" value="<?php echo $usr->id;?>"/></td>
                        <td><?php echo $usr->name;?></td>
                        <td><?php echo $usr->college_name;?></td>
                        <td><?php echo $usr->university_name;?></td>
                        <td><?php echo $usr->degree_name;?></td>
                        <td><?php echo $usr->duration;?></td>
                        <td><?php echo $usr->deadline_link;?></td>
                        <td><?php echo $usr->total_fees;?></td>
						 <td><a href="<?php echo base_url();?>university/course/form/<?php echo $usr->id;?>" title="Modify"><i class="fa fa-edit"></i></a> &nbsp; <a href="javascript:void();" title="Delete" class="del" id="<?php echo $usr->id;?>"><i class="fa fa-trash-o"></i></a></td>
					<?php } ?>
                      </tr>

                    </tbody>
                    <tfoot>
                      <tr>
                          <th></th>
                          <th>Name</th>
                          <th>College</th>
                          <th>University</th>
                          <th>Degree</th>
                          <th>Duration</th>
                          <th>Deadline Link</th>
                          <th>Total Fees</th>
                          <th>Option</th>
                      </tr>
                    </tfoot>
                  </table>
				</form>
          </div>
          </div>    
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example1').DataTable();
  } );
</script>