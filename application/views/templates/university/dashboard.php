<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<style type="text/css">
   p , .guidance li, td{
   font-size: 16px;
   line-height: 1.6;
   margin:0 0 10px;
   color:#000;
   }
   .guidance ol li{
   list-style:decimal;
   margin-left:24px;
   }
   .pricing-column-features li {
   color:#000;
   font-size: 16px;
   line-height: 1.6;
   }
   .italic {
   font-style:italic;
   }
   .text-center {
   text-align:center;
   }
   .pricing-column-price {
   font-size: 24px;
   font-weight: 700;
   }
   .tutor-plan {
   font-weight: 700;
   }
   .btn {
   padding:8px 12px;
   }
   .dashed-border  {
   border-bottom: 1px dashed #dddcde;
   }
   .formControl{
   color: #fff !important;
   background-color: #8c57c5 !important;
   /* border: 0px; */
   font-size: 12px;
   border: 0px !important;
   box-shadow: inset 0 0 0 rgba(0,0,0,.075) !important;

    margin-top: 15px;

   }
   .ht30{
   height: 30px;
   }
   /*section {
   width: 100%;
   display: inline-flex;
   align-items: center;
   justify-content: center;
   }
   */
   section .flex-col {
   padding: 15px;
   }
   section .flex-col .cards {
   max-width: 380px;
   height: 110px;
   background-color: #fff;
   padding: 18px;
   position: relative;
   border-top-right-radius: 5px;
    border-top-left-radius: 5px;
    box-shadow: 0px 0px 6px 0px hsl(229, 6%, 66%);
   }
   section .flex-col .cards h3 {
   font-size: 1.8rem;
       margin: 0px 0 8px;
   text-align: center;
   }
   section .flex-col .cards p {
   text-align: center;
   margin-top: 5px;
   /* line-height: 2; */
   font-size: 3.5rem;
   color: #f6882c;
   font-weight: 600;
   letter-spacing: 1px;
   }
   section .flex-col .cards img {
   position: absolute;
   bottom: 10px;
   right: 5px;
   width: 20%;
   }
   section .one {
   border-top: 5px hsl(27deg 92% 57%) solid;
   }
   /*section .two {
   border-top: 5px hsl(0, 78%, 62%) solid;
   }
   section .three {
   border-top: 5px hsl(34, 97%, 64%) solid;
   }
   section .four {
   border-top: 5px hsl(212, 86%, 64%) solid;
   }*/
   @media (max-width: 960px) {
   section {
   flex-direction: column;
   }
   section .flex-col {
   padding: 0 40px;
   }
   header {
   padding: 0 45px 20px;
   }
   }
   @media (max-width: 425px) {
   header h1 {
   font-size: 1.4rem;
   }
   }
   @import url(https://fonts.googleapis.com/css?family=Anonymous+Pro:400,700);
   /*body {
   background: #E7ECEC;
   font-family: 'Anonymous Pro', sans-serif;
   font-weight: 400;
   font-size: 14px;
   -webkit-font-smoothing: antialiased;
   -moz-osx-font-smoothing: grayscale;
   }
   * {
   box-sizing: border-box;
   backface-visibility: hidden;
   }*/
   .area {
   position: absolute;
   /*top: 50%;*/
   left: 85%;
   margin-top: -15px;
   margin-left: -15px;
   }
   .toggle {
   display: block;
   position: relative;
   background: #FCFCFC;
   border: none;
   width: 44px;
   height: 44px;
   line-height: 44px;
   text-align: center;
   border-radius: 100%;
   cursor: pointer;
   border: 4px solid rgba(255, 255, 255, 0.3);
   transition: all 0.2s ease;
   z-index: 4;
   outline: none;
   }
   .toggle:hover {
   transform: scale(0.8);
   }
   .toggle.active {
   transform: none;
   border: 4px solid #f6882c;
   }
   .toggle.active .icon {
   transform: rotate(315deg);
   }
   .toggle.active ~ .card {
   opacity: 1;
   transform: rotate(0);
   z-index: 1;
   }
   .toggle.active ~ .card h1,
   .toggle.active ~ .card p {
   opacity: 1;
   transform: translateY(0);
   }
   .bubble {
   position: absolute;
   border: none;
   width: 40px;
   height: 40px;
   border-radius: 100%;
   cursor: pointer;
   transition: all 0.3s ease;
   z-index: 1;
   top: 2px;
   left: 2px;
   }
   /*.bubble:before {
   content: "";
   position: absolute;
   width: 120px;
   height: 120px;
   opacity: 0;
   border-radius: 100%;
   top: -40px;
   left: -40px;
   background: #f6882c;
   animation: bubble 3s ease-out infinite;
   }
   .bubble:after {
   content: "";
   position: absolute;
   width: 120px;
   height: 120px;
   opacity: 0;
   border-radius: 100%;
   top: -40px;
   left: -40px;
   z-index: 1;
   background: #f6882c;
   animation: bubble 3s 0.8s ease-out infinite;
   }*/
   .icon {
   display: inline-block;
   position: relative;
   width: 16px;
   height: 16px;
   z-index: 5;
   transition: all 0.3s ease-out;
   }
   .icon .horizontal {
   position: absolute;
   width: 100%;
   height: 4px;
   background: linear-gradient(-134deg, #8c57c5 0%, #6e1bca 100%);
   top: 50%;
   margin-top: -2px;
   }
   .icon .vertical {
   position: absolute;
   width: 4px;
   height: 100%;
   left: 50%;
   margin-left: -2px;
   background: linear-gradient(-134deg, #8c57c5 0%, #6e1bca 100%);
   }
   @keyframes bubble {
   0% {
   opacity: 0;
   transform: scale(0);
   }
   5% {
   opacity: 1;
   }
   100% {
   opacity: 0;
   }
   }
   .card {
   /*width: 400px;*/
   overflow: hidden;
   position: relative;
   background: #FCFCFC;
   border-radius: 10px;
   margin-top: -25px;
   margin-left: 25px;
   opacity: 0;
   transform: rotate(5deg);
   transform-origin: top left;
   transition: all 0.2s ease-out;
   border: 1px solid #8c57c5;
   }
   .card-content {
   display: inline-block;
   vertical-align: middle;
   width: auto;
   padding: 15px;
   /*border-right: 1px solid #f0f0f0;*/
   }
   .addStuBtn{
   width: inherit;
   color: #f6882c;
   border-radius: 10px;
   margin-top: 12%;
   }

   .subBox{
          padding-right: 0px;
    padding-left: 0px;
    height: auto;
    text-align: center;
        border-radius: 5px;
        box-shadow: 0px 0px 6px 0px hsl(229, 6%, 66%);
   }

   .innerSubBox{
      background-color: #f6882c;
    height: inherit;
        padding: 3px 0px;
   }

   .innerSubBox p{
             color: white;

          margin: 0 0 0px !important;
   }

   .innerSubBox h4{
          color: white;

          margin: 05px 0 5px;
   }

.highcharts-figure, .highcharts-data-table table {
  min-width: 360px;
  max-width: 100%;
  margin: 1em auto;
}

.highcharts-data-table table {
   font-family: Verdana, sans-serif;
   border-collapse: collapse;
   border: 1px solid #EBEBEB;
   margin: 10px auto;
   text-align: center;
   width: 100%;
   max-width: 100%;
}
.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}
.highcharts-data-table th {
   font-weight: 600;
  padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
  padding: 0.5em;
      text-align: center;
}

.highcharts-data-table thead tr{
  background: #f6882c;
  color: white;
  font-size: 14px;
}
.highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}
.highcharts-data-table tr:hover {
  background: #8c57c62e;
}
</style>
<div class="container">
   <div class="row" style="padding: 30px 0px 30px 0px;display: flex;">
      <div class="col-md-3 purpleBg" style="box-shadow: 0px 0px 8px 0px #dcd9d9;    padding: 15px 15px 15px 15px;">
         <div class="col-md-12" style="border-radius: 6px;text-align: center;background-color: white;">
            <h4 style="color: #f6882c;">Filters</h4>
         </div>
         <div class="col-md-12">
            <label class="form-control formControl">From Date</label>
            <input type="date" id="fromDate" name="fromDate" class="searchoptions form-control ht30" value="<?=$start_date?>">
         </div>
         <div class="col-md-12">
            <label class="form-control formControl">To Date</label>
            <input type="date" id="toDate" name="toDate" class="searchoptions form-control ht30" onchange="filterdata()" value="<?=$end_date?>">
         </div>
         <div class="col-md-12">
            <label class="form-control formControl">COUNTRY *</label>
            <select class="form-control ht30" name="countryId" id="countryId" onchange="filterdata()">
              <option value=""> Select Country </option>
               <?php foreach($countries as $country){?>
               <option value="<?php echo strtoupper($country->name);?>" data-badge=""> <?php echo strtoupper($country->name);?> </option>
               <?php } ?>
             </select>

         </div>
         <div class="col-md-12">
            <label class="form-control formControl">INTAKE YEAR</label>
            <select class=" form-control ht30" id="intake_year" name="intake_year" onchange="filterdata()">
               <option value="">Select Intake Year</option>
               <option value="2020">2020</option>
               <option value="2021">2021</option>
               <option value="2022">2022</option>
               <option value="2023">2023</option>
            </select>
         </div>
         <div class="col-md-12">
            <label class="form-control formControl">INTAKE</label>
            <select class="form-control ht30" id="intake_month" name="intake_month" >
               <option value="">Select Intake Month</option>
               <option value="January">January</option>
               <option value="February">February</option>
               <option value="March">March</option>
               <option value="April">April</option>
               <option value="May">May</option>
               <option value="June">June</option>
               <option value="July">July</option>
               <option value="August">August</option>
               <option value="September">September</option>
               <option value="October">October</option>
               <option value="November">November</option>
               <option value="December">December</option>
            </select>
         </div>


      </div>
      <div class="col-md-9">
         <div class="row">
            <section>
               <div class="col-md-12">
                  <div class="col-md-4 flex-col">
                    <a href="#" onclick="listpage('total_application')">
                     <div class="cards one">

                        <h3>Applications</h3>
                        <p><span id="total_application" ><?=$totalApplicationCount?></span></p>
                     </div>
                   </a>
                      <div class="col-md-12 subBox">
                        <a href="#" onclick="listpage('approved_application')">
                           <div class="col-md-6 innerSubBox" style="border-bottom-left-radius: 5px;">
                           <p>Approved</p>
                           <h4><span id="approved_application" ><?=$approvedApplicationCount?></span></h4>
                           </div>
                         </a>
                         <a href="#" onclick="listpage('rejected_application')">
                           <div class="col-md-6 innerSubBox" style="border-bottom-right-radius: 5px;border-left: 3px solid white;">
                           <p>Rejected</p>
                           <h4><span id="rejected_application" ><?=$rejectedApplicationCount?></span></h4>
                           </div>
                         </a>
                      </div>
                  </div>
                  <div class="col-md-4 flex-col">
                    <a href="#" onclick="listpage('total_offers')">
                     <div class="cards one">

                        <h3>Offers</h3>
                        <p><span id="total_offers" ><?=$totalOfferCount?></span></p>
                        <!-- <img src="<?php echo base_url();?>application/images/Studentdashboard/Offers.png" > -->
                     </div>
                   </a>
                     <div class="col-md-12 subBox">
                       <a href="#" onclick="listpage('approved_offers')">
                           <div class="col-md-6 innerSubBox" style="border-bottom-left-radius: 5px;">
                           <p>Approved</p>
                           <h4><span id="approved_offers" ><?=$approvedOfferCount?></span></h4>
                           </div>
                         </a>
                         <a href="#" onclick="listpage('rejected_offers')">
                           <div class="col-md-6 innerSubBox" style="border-bottom-right-radius: 5px;border-left: 3px solid white;">
                           <p>Rejected</p>
                           <h4><span id="rejected_offers" ><?=$rejectedOfferCount?></span></h4>
                           </div>
                         </a>
                      </div>
                  </div>
                  <div class="col-md-4 flex-col">
                    <a href="#" onclick="listpage('total_student')">
                     <div class="cards one">

                        <h3>Students</h3>
                        <p><span id="total_student" ><?=$totalStudentCount?></span></p>
                     </div>
                   </a>
                     <div class="col-md-12 subBox">
                       <a href="#" onclick="listpage('interested_student')">
                           <div class="col-md-6 innerSubBox" style="border-bottom-left-radius: 5px;">
                           <p>Interested</p>
                           <h4><span id="interested_student" ><?=$interestedStudentCount?></span></h4>
                           </div>
                         </a>
                         <a href="#" onclick="listpage('potential_student')">
                           <div class="col-md-6 innerSubBox" style="border-bottom-right-radius: 5px;border-left: 3px solid white;">
                           <p>Potential</p>
                           <h4><span id="potential_student" ><?=$potentialStudentCount?></span></h4>
                           </div>
                         </a>
                      </div>
                  </div>

               </div>
            </section>
         </div>
         <div class="row">
            <div id="container" style="height: 400px; width: 100%"></div>
         </div>
      </div>
   </div>
   <!-- <div id="container">
      <div id="chart"></div>
      </div>  -->
   <div class="row">
   </div>
   <div class="row">
      <div class="col-lg-12 guidance">
         <p>We at Hello Uni look forward to guide the students towards their interest and dreams. We act as not just counselors and look to guide students in the best interest possible. </p>
      </div>
   </div>
</div>

<script type="text/javascript">


$.ajax({

type: "GET",

url: "/university/dashboard/graph",

cache: false,

success: function(result){

  var seriesData = [{
     name: 'Application',
     data: result.applicationGraphValues
    }, {
     name: 'Offers',
     data: result.offerGraphValues
    }, {
     name: 'Students',
     data: result.studentGraphValues
    }];

  Highcharts.chart('container', {

  title: {
   text: 'Main Title'
  },

  subtitle: {
   text: 'Any subtitle'
  },

  yAxis: {
   title: {
     text: 'Number of Studnets'
   }
  },

  xAxis: {
    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
   accessibility: {
     //rangeDescription: 'Range: 2020 to 2027'
     rangeDescription: 'Range: Jan to Dec'
   }
  },

  legend: {
   layout: 'vertical',
   align: 'right',
   verticalAlign: 'middle'
  },

  plotOptions: {
   series: {
     label: {
       connectorAllowed: false
     }
     //,
     //pointStart: 2020
   }
  },

  series : seriesData,

  responsive: {
   rules: [{
     condition: {
       maxWidth: 500
     },
     chartOptions: {
       legend: {
         layout: 'horizontal',
         align: 'center',
         verticalAlign: 'bottom'
       }
     }
   }]
  }

  });

}
});

//
function filterdata(){

  var startDate = $('#fromDate').val();
  var endDate = $('#toDate').val();
  var countryId = $('#countryId').val();
  var intake_month = $('#intake_month').val();
  var intake_year = $('#intake_year').val();
  //var application_status = $('#application_status').val();
  //var offer_status = $('#offer_status').val();


  var dataString = 'start_date=' + startDate + '&end_date=' + endDate + '&country_id=' + countryId + '&intake_month=' + intake_month + '&intake_year=' + intake_year ;

  event.preventDefault();

  $.ajax({

  type: "POST",

  url: "/university/dashboard/filter",

  data: dataString,

  cache: false,

  success: function(result){

   document.getElementById("total_application").innerHTML = result.totalApplicationCount;
   document.getElementById("approved_application").innerHTML = result.approvedApplicationCount;
   document.getElementById("rejected_application").innerHTML = result.rejectedApplicationCount;

   document.getElementById("total_offers").innerHTML = result.totalOfferCount;
   document.getElementById("approved_offers").innerHTML = result.approvedOfferCount;
   document.getElementById("rejected_offers").innerHTML = result.rejectedOfferCount;


   document.getElementById("total_student").innerHTML = result.totalStudentCount;
   document.getElementById("interested_student").innerHTML = result.interestedStudentCount;
   document.getElementById("potential_student").innerHTML = result.potentialStudentCount;

   var ajaxSeriesData = [{
      name: 'Application',
      data: result.applicationGraphValues
     }, {
      name: 'Offers',
      data: result.offerGraphValues
     }, {
      name: 'Students',
      data: result.studentGraphValues
     }];

     Highcharts.chart('container', {

     title: {
      text: 'Main Title'
     },

     subtitle: {
      text: 'Any subtitle'
     },

     yAxis: {
      title: {
        text: 'Number of Studnets'
      }
     },

     xAxis: {
       categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      accessibility: {
        //rangeDescription: 'Range: 2020 to 2027'
        rangeDescription: 'Range: Jan to Dec'
      }
     },

     legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
     },

     plotOptions: {
      series: {
        label: {
          connectorAllowed: false
        }
        //,
        //pointStart: 2020
      }
     },

     series : ajaxSeriesData,

     responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
     }

     });

  }

      });

}

function listpage(leadstatetype){

  var startDate = $('#fromDate').val() ? $('#fromDate').val() : '';
  var endDate = $('#toDate').val() ? $('#toDate').val() : '';
  var countryId = $('#countryId').val() ? $('#countryId').val() : '';
  var intake_month = $('#intake_month').val() ? $('#intake_month').val() : '';

  if( leadstatetype == 'total_student' || leadstatetype == 'interested_student' || leadstatetype == 'potential_student'){
    window.location.href = "/university/student?leadstatetype="+leadstatetype+"&startDate="+startDate+"&endDate="+endDate+"&countryId="+countryId+"&intake_month="+intake_month;

  } else {
    window.location.href = "/university/application?leadstatetype="+leadstatetype+"&startDate="+startDate+"&endDate="+endDate+"&countryId="+countryId+"&intake_month="+intake_month;

  }

}


</script>
