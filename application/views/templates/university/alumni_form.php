<style type="text/css">
    @import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");
    .login-block{
        background: #004b7a;  /* fallback for old browsers */
        background: -webkit-linear-gradient(to bottom, #fafafb, #d8d9da);  /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to bottom, #fafafb, #d8d9da); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
        float:left;
        width:100%;
        padding : 50px 0;
    }
    .banner-sec{
        background:url(<?php echo base_url();?>application/images/register.png)  no-repeat left bottom;
        background-size:cover;
        min-height:600px;
        border-radius: 0 10px 10px 0;
        padding:0;
        background-position: center;
        margin-top: 25px;
    }

    .login-sec{
        padding: 50px 30px;
        position:relative;
    }
    .login-sec .copy-text{
        position:absolute;
        width:80%;
        bottom:20px;
        font-size:13px;
        text-align:center;
    }
    .login-sec h2{
        margin-bottom:30px;
        font-weight:800;
        font-size:30px;
        color: #004b7a;
    }
    .login-sec h2:after{
        content:" ";
        width:100px;
        height:5px;
        background:#f9992f;
        display:block;
        margin-top:20px;
        border-radius:3px;
        margin-left:auto;
        margin-right:auto
    }
    .btn-login{
        background: #f9992f;
        color:#fff;
        font-weight:600;
    }
    .form-control{
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    }

    p {
        font-size: 16px;
        line-height: 1.6;
        margin:0 0 10px;
    }
    .italic {
        font-style:italic;
    }
    .padtb {
        padding:30px 0px;
    }
</style>

<section class="login-block">
    <div class="container" style="background:#fff; border-radius: 10px; box-shadow: 5px 5px 0px rgba(0,0,0,0.1);">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6 login-sec">
                <h2 class="text-center"> Alumnus Form </h2>
                <?php
                if(isset($profile_detail->id))
                {
                    ?>
                    <form class="login-form" class="form-horizontal" onsubmit="updateProfile()" method="post">
                        <div class="col-md-12 form-group">
                            <div class="col-md-12">
                                <label for="inputEmail3" class="text-uppercase"> Name * </label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?=$profile_detail->name?>" required>
                            </div>
                        </div>

                        <div class="col-md-12 form-group">
                            <div class="col-md-12">
                                <label for="inputEmail3" class="text-uppercase">Email * </label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?=$profile_detail->email?>" required>
                            </div>
                        </div>

                        <div class="col-md-12 form-group">
                            <div class="col-md-12">
                                <label for="inputEmail3" class="text-uppercase"> Password * </label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                            </div>
                        </div>
                        <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?=$profile_detail->id?>">

                        <div class="form-check" style="text-align: center;">
                            <!-- <button type="submit" class="btn btn-login" ng-click="submitForm()">Submit</button> -->
                            <button type="button" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                            <input type="submit"  style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Update"></input>
                        </div>
                    </form>
                    <?php
                }
                else
                {
                    ?>
                    <form class="login-form" class="form-horizontal" onsubmit="addProfile()" method="post">
                        <div class="col-md-12 form-group">
                            <div class="col-md-12">
                                <label for="inputEmail3" class="text-uppercase"> Name * </label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
                            </div>
                        </div>

                        <div class="col-md-12 form-group">
                            <div class="col-md-12">
                                <label for="inputEmail3" class="text-uppercase">Email * </label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                            </div>
                        </div>

                        <div class="col-md-12 form-group">
                            <div class="col-md-12">
                                <label for="inputEmail3" class="text-uppercase">Username * </label>
                                <input type="text" class="form-control" id="username" name="username" placeholder="Username"  required>
                            </div>
                        </div>

                        <div class="col-md-12 form-group">
                            <div class="col-md-12">
                                <label for="inputEmail3" class="text-uppercase"> Password * </label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                            </div>
                        </div>

                        <div class="form-check" style="text-align: center;">
                            <!-- <button type="submit" class="btn btn-login" ng-click="submitForm()">Submit</button> -->
                            <button type="button" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                            <input type="submit"  style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Add"></input>
                        </div>
                    </form>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

//registration

function updateProfile(){
    var name = $('#name').val();
    var email = $('#email').val();
    var password = $('#password').val();
    var user_id = $('#user_id').val();

    var dataString = 'name=' + name + '&email=' + email + '&password=' + password + '&id=' + user_id;

    event.preventDefault();

    $.ajax({
        type: "POST",
        url: "/university/alumni/edit",
        data: dataString,
        cache: false,
        success: function(result){
            if(result == "SUCCESS"){
                alert("Alumnus Added Successfully...!!!");
                window.location.href='/university/alumni';
            } else {
                alert(result);
            }
        }
    });
}

function addProfile(){
    var name = $('#name').val();
    var email = $('#email').val();
    var password = $('#password').val();
    var username = $('#username').val();

    var dataString = 'name=' + name + '&email=' + email + '&password=' + password + '&username=' + username;

    event.preventDefault();

    $.ajax({
        type: "POST",
        url: "/university/alumni/create",
        data: dataString,
        cache: false,
        success: function(result){
            if(result == "SUCCESS"){
                alert("Alumnus Added Successfully...!!!");
                window.location.href='/university/alumni';
            } else {
                alert(result);
            }
        }
    });
}
</script>
