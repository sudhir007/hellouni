<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<style type="text/css">
    .styleCss{
        font-size: 14px;
        background-color: #f6882c;
        color: white;
        font-weight: 600;
        letter-spacing: 0.8px;
    }
    .formControl{
        color: #fff !important;
        background-color: #8c57c5 !important;
        font-size: 12px;
        border: 0px !important;
        box-shadow: inset 0 0 0 rgba(0,0,0,.075) !important;
        margin-top: 15px;
    }
    #dt-example td.details-control {
        cursor: pointer;
    }
    #dt-example tr.shown td {
        background-color: #815dd5;
        color: #fff !important;
        border: none;
        border-right: 1px solid #815dd5;
    }
    #dt-example tr.shown td:last-child {
        border-right: none;
    }
    #dt-example tr.details-row .details-table tr:first-child td {
        color: #fff;
        background: #f6882c;
        border: none;
    }
    #dt-example tr.details-row .details-table > td {
        padding: 0;
    }
    #dt-example tr.details-row .details-table .fchild td:first-child {
        cursor: pointer;
    }
    #dt-example tr.details-row .details-table .fchild td:first-child:hover {
        background: #fff;
    }
    #dt-example .form-group.agdb-dt-lst {
        padding: 2px;
        height: 23px;
        margin-bottom: 0;
    }
    #dt-example .form-group.agdb-dt-lst .form-control {
        height: 23px;
        padding: 2px;
    }
    #dt-example .adb-dtb-gchild {
        padding-left: 2px;
    }
    #dt-example .adb-dtb-gchild td {
        background: #f5fafc;
        padding-left: 15px;
    }
    #dt-example .adb-dtb-gchild td:first-child {
        cursor: default;
    }
    .dataTables_wrapper{
        overflow: auto;
    }
    .listTableView thead tr{
        background-color: #f6882c;
        color: white;
    }
</style>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<div style="float:right"><a href="/university/alumni/add" style="color:#337ab7; font-size:14px;"><button>Add Alumnus</button></a></div>
<div class="container" style="clear:both;">
    <div class="row" style="padding: 30px 0px 30px 0px;display: flex;">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body listTableView">
                    <table id="dt-example" class="stripe row-border order-column cl-table dataTable no-footer" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Created Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#data-table').dataTable();
        $(".dataTable").on("draw.dt", function (e) {
            setCustomPagingSigns.call($(this));
        }).each(function () {
            setCustomPagingSigns.call($(this)); // initialize
        });

        function setCustomPagingSigns() {
            var wrapper = this.parent();
            wrapper.find("a.previous").text("<");
            wrapper.find("a.next").text(">");
        }

        var data = [];
        var dataString = '';

        $.ajax({
            type: "POST",
            url: "/university/alumni/filter",
            data: dataString,
            cache: false,
            success: function(result){
                var studentdata = result;
                if(studentdata.length){
                    studentdata.forEach(function(value, index){
                        data.push({
                            "Name" : value.name,
                            "Email" : value.email,
                            "Created_Date" : value.createdate,
                            "Status" : value.status_name,
                            "Action" : "<a class='btn btn-mg btn-info' href='/university/alumni/update/"+ value.id +"' > EDIT </a>"
                        });
                    });

                    var table = $('.dataTable').DataTable({
                        columns : [{
                                data : 'Name',
                                className : 'details-control',
                            },
                            {data : 'Email'},
                            {data : 'Created_Date'},
                            {data : 'Status'},
                            {data : 'Action'}
                        ],
                        data : data,
                        pagingType : 'full_numbers',
                        language : {
                            emptyTable     : 'No data to display.',
                            zeroRecords    : 'No records found!',
                            thousands      : ',',
                            loadingRecords : 'Loading...',
                            search         : 'Search:',
                            paginate       : {
                                next     : 'next',
                                previous : 'previous'
                            }
                        },
                        "bDestroy": true
                    });

                    $('.dataTable tbody').on('click', 'td.details-control', function () {
                        var tr  = $(this).closest('tr'),
                        row = table.row(tr);

                        if (row.child.isShown()) {
                            tr.next('tr').removeClass('details-row');
                            row.child.hide();
                            tr.removeClass('shown');
                        }
                        else {
                            row.child(format(row.data())).show();
                            tr.next('tr').addClass('details-row');
                            tr.addClass('shown');
                        }
                    });

                    $('#dt-example').on('click','.details-table tr',function(){
                        $('.details-table').find('.adb-dtb-gchild:not(:first-child)').slideToggle();
                    });
                }
            }
        });
    });
</script>
