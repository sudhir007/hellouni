<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<script type="text/javascript" src="
https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<style type="text/css">
   .styleCss{
   font-size: 14px;
   background-color: #f6882c;
   color: white;
   font-weight: 600;
   letter-spacing: 0.8px;
   }
   .formControl{
   color: #fff !important;
   background-color: #8c57c5 !important;
   /* border: 0px; */
   font-size: 12px;
   border: 0px !important;
   box-shadow: inset 0 0 0 rgba(0,0,0,.075) !important;
   margin-top: 15px;
   }

   #dt-example td.details-control , #items_table td.details-control{
     cursor: pointer;
}
 #dt-example tr.shown td , #items_table tr.shown td{
     background-color: #815dd5;
     color: #fff !important;
     border: none;
     border-right: 1px solid #815dd5;
}
 #dt-example tr.shown td:last-child , #items_table tr.shown td:last-child{
     border-right: none;
}
 #dt-example tr.details-row .details-table tr:first-child td , #items_table tr.details-row .details-table tr:first-child td {
     color: #fff;
     background: #f6882c;
     border: none;
}
 #dt-example tr.details-row .details-table > td {
     padding: 0;
}
 #dt-example tr.details-row .details-table .fchild td:first-child {
     cursor: pointer;
}
 #dt-example tr.details-row .details-table .fchild td:first-child:hover {
     background: #fff;
}
 #dt-example .form-group.agdb-dt-lst {
     padding: 2px;
     height: 23px;
     margin-bottom: 0;
}
 #dt-example .form-group.agdb-dt-lst .form-control {
     height: 23px;
     padding: 2px;
}
 #dt-example .adb-dtb-gchild {
     padding-left: 2px;
}
 #dt-example .adb-dtb-gchild td {
     background: #f5fafc;
     padding-left: 15px;
}
 #dt-example .adb-dtb-gchild td:first-child {
     cursor: default;
}
 #dt-example .fchild ~ .adb-dtb-gchild {
    /* display: none;
     */
}
 .dataTables_wrapper{
    overflow: auto;
 }

.listTableView thead tr{
 background-color: #f6882c;
    color: white;
}

/* The Modal (background) */
.modal {
display: none; /* Hidden by default */
position: fixed; /* Stay in place */
z-index: 1; /* Sit on top */
padding-top: 100px; /* Location of the box */
left: 0;
top: 0;
width: 100%; /* Full width */
height: 100%; /* Full height */
overflow: auto; /* Enable scroll if needed */
background-color: rgb(0,0,0); /* Fallback color */
background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}
/* Modal Content */
.modal-content {
background-color: #fefefe;
margin: auto;
padding: 20px;
border: 1px solid #888;
    width: 40%;
 z-index: 10000;
}
/* The Close Button */
.close {
color: #8e0f0f;
float: right;
font-size: 28px;
font-weight: bold;
}
.close:hover,
.close:focus {
color: #000;
text-decoration: none;
cursor: pointer;
}

</style>
<div class="container">
   <div class="row" style="padding: 30px 0px 30px 0px;display: flex;">
      <div class="col-md-3 purpleBg" style="box-shadow: 0px 0px 8px 0px #dcd9d9;    padding: 5px;">
         <div class="col-md-12" style="border-radius: 6px;text-align: center;background-color: white;">
            <h4 style="color: #f6882c;">Filters</h4>
            <a style="text-align: center; border-radius:5px;" class="btn btn-md btn-info" href="/counsellor/student"> RESET FILTER </a>
         </div>
         <div class="col-md-12">
            <label class="form-control formControl">From Date</label>
            <input type="date" id="fromDate" name="fromDate" class="searchoptions form-control ht30" value="<?=$start_date?>">
         </div>
         <div class="col-md-12">
            <label class="form-control formControl">To Date</label>
            <input type="date" id="toDate" name="toDate" class="searchoptions form-control ht30" onchange="filterdata()" value="<?=$end_date?>">
         </div>

         <div class="col-md-12">
            <label class="form-control formControl">Lead Type *</label>
            <select class="form-control ht30" name="lead_state_type" id="lead_state_type" onchange="filterdata()">
              <option value=""> Select Lead Type </option>
              <?php foreach($lead_state_dropdown as $leadState){?>
                <?php if($leadState['id'] == $lead_state_type) { ?>
                  <option value="<?php echo $leadState['id'];?>" data-badge="" selected> <?php echo strtoupper($leadState['value']);?> </option>
                <?php } else { ?>
              <option value="<?php echo $leadState['id'];?>" data-badge=""> <?php echo strtoupper($leadState['value']);?> </option>
            <?php }
          } ?>
             </select>

         </div>

         <div class="col-md-12">
            <label class="form-control formControl">COUNTRY *</label>
            <select class="form-control ht30" name="countryId" id="countryId" onchange="filterdata()">
              <option value=""> Select Country </option>
              <?php foreach($countries as $country){?>
                <?php if($country->name == $country_id) { ?>
                  <option value="<?php echo strtoupper($country->name);?>" data-badge="" selected> <?php echo strtoupper($country->name);?> </option>
                <?php } else { ?>
              <option value="<?php echo strtoupper($country->name);?>" data-badge=""> <?php echo strtoupper($country->name);?> </option>
            <?php }
          } ?>
             </select>

         </div>

         <div class="col-md-12">
            <label class="form-control formControl">INTAKE YEAR</label>
            <select class=" form-control ht30" id="intake_year" name="intake_year" onchange="filterdata()">
               <option value="">Select Intake Year</option>
               <option value="2020" <?php if($intake_year == 2020){ echo "selected";} ?>>2020</option>
               <option value="2021" <?php if($intake_year == 2021){ echo "selected";} ?>>2021</option>
               <option value="2022" <?php if($intake_year == 2022){ echo "selected";} ?>>2022</option>
               <option value="2023" <?php if($intake_year == 2023){ echo "selected";} ?>>2023</option>
            </select>
         </div>

         <div class="col-md-12">
            <label class="form-control formControl">INTAKE MONTH</label>
            <select class=" form-control ht30" id="intake_month" name="intake_month" onchange="filterdata()">
               <option value="">Select Intake Month</option>
               <option value="January" <?php if($intake_month == 'January'){ echo "selected";} ?>>January</option>
               <option value="February" <?php if($intake_month == 'February'){ echo "selected";} ?>>February</option>
               <option value="March" <?php if($intake_month == 'March'){ echo "selected";} ?>>March</option>
               <option value="April" <?php if($intake_month == 'April'){ echo "selected";} ?>>April</option>
               <option value="May" <?php if($intake_month == 'May'){ echo "selected";} ?>>May</option>
               <option value="June" <?php if($intake_month == 'June'){ echo "selected";} ?>>June</option>
               <option value="July" <?php if($intake_month == 'July'){ echo "selected";} ?>>July</option>
               <option value="August" <?php if($intake_month == 'August'){ echo "selected";} ?>>August</option>
               <option value="September" <?php if($intake_month == 'September'){ echo "selected";} ?>>September</option>
               <option value="October" <?php if($intake_month == 'October'){ echo "selected";} ?>>October</option>
               <option value="November" <?php if($intake_month == 'November'){ echo "selected";} ?>>November</option>
               <option value="December" <?php if($intake_month == 'December'){ echo "selected";} ?>>December</option>
            </select>
         </div>


      </div>
      <div class="col-md-9" >
        <div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-body listTableView">
<!-- Dynamic table was here -->
<table id="dt-example" class="stripe row-border order-column cl-table dataTable no-footer" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th>Name</th>
      <th>Email</th>
      <th>Mobile</th>
      <th>Country</th>
      <th>Lead Status</th>
      <th>Status</th>
      <th>Student Counselor</th>
      <th>Created By</th>
      <th>Created Date</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>
  </div>

 </div>
</div>
      </div>
   </div>
</div>

<div id="communicationListModal" class="modal">
   <div class="modal-content" style="width: 45%;">
      <span class="close closeSlotList">&times;</span>
      <div class="container" style="margin-top: 10px;">
         <div class="panel panel-default">
            <div class="panel-heading purplePanel"><h4 class="text-center"> Communication Form </h4></div>
            <div class="panel-body">

                    <form class="login-form" class="form-horizontal" onsubmit="addComment()" method="post">
                        <div class="col-md-12 form-group">
                            <div class="col-md-12">
                                <label for="inputEmail3" class="text-uppercase"> Type Your Message * </label>
                                <textarea  name="comment" id="comment" class="form-control" style="font-size:12px;" rows="6" cols="50"> </textarea>

                                <input type="hidden" class="form-control" id="entity_id" name="entity_id" />
                                <input type="hidden" class="form-control" id="student_id" name="student_id" />
                            </div>
                        </div>

                        <div class="form-check" style="text-align: center;">
                            <!-- <button type="submit" class="btn btn-login" ng-click="submitForm()">Submit</button> -->
                            <button type="button" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-warning whiteRestButton " id="reset" onclick="window.location.reload();">Reset</button>
                            <input type="submit"  style="text-align: center; border-radius:5px;" class="btn btn-lg purpleButton " value="Submit"></input>
                        </div>
                    </form>
            </div>
        </div>
</div>
      <div id="communication-list-modal" style="text-align: center;">

        <h3> Communication List </h3>
         <div class="panel panel-default">
        <div class="panel-body listTableView">
        <table class="row-border order-column cl-table dataTable no-footer" id="items_table" border="1">
          <thead>
              <tr>
                  <th> Type </th>
                  <th> Message </th>
                  <th> Added By </th>
                  <th> Date </th>
              </tr>
          </thead>
          <tbody id="datalist">

          </tbody>

       </table>
       </div>
       </div>
      </div>
   </div>


</div>

<script type="text/javascript">

var spanslotlist = document.getElementsByClassName("closeSlotList")[0];
spanslotlist.onclick = function() {
  $("#communicationListModal").modal('hide');
}

function communicationList(counsellorId,studentId) {

  //alert(counsellorId);
  //alert(studentId);

  var dataString = '';
  var dataurl = "/university/counsellor/communicationstudentlist/"+counsellorId+"/"+studentId;

  $.ajax({

  type: "POST",

  url: dataurl,

  data: dataString,

  cache: false,

  success: function(result){

    var communicationlist = result;

    var htmlcommunicationList = "";

    $('#entity_id').val(counsellorId);
    $('#student_id').val(studentId);

    if(communicationlist.length){

      communicationlist.forEach(function(value, index){

        htmlcommunicationList += "<tr>";
        htmlcommunicationList += "<td>"+value.entity_type+"</td>";
        htmlcommunicationList += "<td>"+value.comment+"</td>";
        htmlcommunicationList += "<td>"+value.comment_by+"</td>";
        htmlcommunicationList += "<td>"+value.added_time+"</td>";
        htmlcommunicationList += "</tr>";

      });

      $("#communicationListModal").modal('show');

      document.getElementById("datalist").innerHTML = htmlcommunicationList;

  }

}
      });

}

</script>

<script>

        $('#data-table').dataTable();
        $(".dataTable").on("draw.dt", function (e) {
          //console.log("drawing");
          setCustomPagingSigns.call($(this));
        }).each(function () {
          setCustomPagingSigns.call($(this)); // initialize
        });
        function setCustomPagingSigns() {
          var wrapper = this.parent();
          wrapper.find("a.previous").text("<");
          wrapper.find("a.next").text(">");
        }
// =============Display details onclick===============
var dataList = [
    {"Name":"Deepika","Email":"deepika@gmail.com","Country":"India","created_date":"27.159.97.60","status":"active","calls":"22","duration":"50"},
    {"Name":"Sudhir","Email":"s@gmail.com","Country":"USA","created_date":"67.135.55.135","status":"active","calls":"22","duration":"25"},
    {"Name":"Nikhil","Email":"n@gmail.com","Country":"UK","created_date":"74.193.127.5","status":"active","calls":"22","duration":"56"},
    {"Name":"Banit","Email":"b@gmail.com","Country":"Australia","created_date":"4.104.253.210","status":"active","calls":"22","duration":"22"}
  ];

  var data = [];


    var startDate = $('#fromDate').val() ? $('#fromDate').val() : '';
    var endDate = $('#toDate').val();
    var countryId = $('#countryId').val();
    var intake_month = $('#intake_month').val();
    var intake_year = $('#intake_year').val();
    var leadstatetype = $('#lead_state_type').val();

    var dataString = 'start_date=' + startDate + '&end_date=' + endDate + '&application_status=' + leadstatetype + '&country_id=' + countryId + '&intake_month=' + intake_month + '&intake_year=' + intake_year ;

  $.ajax({

    type: "POST",

    url: "/university/student/filter",

    data: dataString,

  cache: false,

  success: function(result){

    //console.log(result);

   var studentdata = result.student_list;

   if(studentdata.length){

     studentdata.forEach(function(value, index){

       data.push({
         "Name" : value.name,
         "Email" : value.email,
         "Mobile" : value.phone,
         "Country" : value.countries,
         "lead_state" : value.lead_state,
         "Status" : value.status == 1 ? "ACTIVE" : "DEACTIVE",
         "Counselor_name" : "<a class='btn btn-mg btn-info purpleButton' href='#' onclick='communicationList("+value.facilitator_id+","+value.user_id+")'> "+value.facilitator_name +"</a>",
         "Created_By" : value.createdby,
         "Created_Date" : value.createdate,
         "Action" : "<a class='btn btn-mg purpleButton' href='/student/profile/"+ value.user_id +"' > EDIT </a>"

       });

     });

     //console.log(data,"hi");

     var table = $('.dataTable').DataTable({

       // Column definitions
       columns : [
           {
               data : 'Name',
               className : 'details-control',
           },
           {data : 'Email'},
           {data : 'Mobile'},
           {data : 'Country'},
           {data : 'lead_state'},
           {data : 'Status'},
           {data : 'Counselor_name'},
           {data : 'Created_By'},
           {data : 'Created_Date'},
           {data : 'Action'}
       ],
       data : data,
       pagingType : 'full_numbers',
       // Localization
       language : {
         emptyTable     : 'No data to display.',
         zeroRecords    : 'No records found!',
         thousands      : ',',
         // processing     : 'Processing...',
         loadingRecords : 'Loading...',
         search         : 'Search:',
         paginate       : {
           next     : 'next',
           previous : 'previous'
         }
       },
       "bDestroy": true
     });

     $('.dataTable tbody').on('click', 'td.details-control', function () {
        var tr  = $(this).closest('tr'),
            row = table.row(tr);

        if (row.child.isShown()) {
          tr.next('tr').removeClass('details-row');
          row.child.hide();
          tr.removeClass('shown');
        }
        else {
          row.child(format(row.data())).show();
          tr.next('tr').addClass('details-row');
          tr.addClass('shown');
        }
     });

   $('#dt-example').on('click','.details-table tr',function(){
     $('.details-table').find('.adb-dtb-gchild:not(:first-child)').slideToggle();
       });


   } else {

     //$('#dt-example tbody').DataTable().destroy();
     $('#dt-example tbody').empty();

     var table = $('.dataTable').DataTable({

       // Column definitions
       columns : [
           {
               data : 'Name',
               className : 'details-control',
           },
           {data : 'Email'},
           {data : 'Mobile'},
           {data : 'Country'},
           {data : 'lead_state'},
           {data : 'Status'},
           {data : 'Counselor_name'},
           {data : 'Created_By'},
           {data : 'Created_Date'},
           {data : 'Action'}
       ],
       data : filterdata,
       pagingType : 'full_numbers',
       // Localization
       language : {
         emptyTable     : 'No data to display.',
         zeroRecords    : 'No records found!',
         thousands      : ',',
         // processing     : 'Processing...',
         loadingRecords : 'Loading...',
         search         : 'Search:',
         paginate       : {
           next     : 'next',
           previous : 'previous'
         }
       },
       "bDestroy": true
     });

     $('.dataTable tbody').on('click', 'td.details-control', function () {
        var tr  = $(this).closest('tr'),
            row = table.row(tr);

        if (row.child.isShown()) {
          tr.next('tr').removeClass('details-row');
          row.child.hide();
          tr.removeClass('shown');
        }
        else {
          row.child(format(row.data())).show();
          tr.next('tr').addClass('details-row');
          tr.addClass('shown');
        }
     });

   $('#dt-example').on('click','.details-table tr',function(){
     $('.details-table').find('.adb-dtb-gchild:not(:first-child)').slideToggle();
       });


   }

  }

      });

//filter list function

function filterdata(){

  var startDate = $('#fromDate').val();
  var endDate = $('#toDate').val();
  var countryId = $('#countryId').val();
  var intake_month = $('#intake_month').val();
  var intake_year = $('#intake_year').val();
  var leadstatetype = $('#lead_state_type').val();

  var dataString = 'start_date=' + startDate + '&end_date=' + endDate + '&application_status=' + leadstatetype + '&country_id=' + countryId + '&intake_month=' + intake_month + '&intake_year=' + intake_year ;

  event.preventDefault();

  $.ajax({

  type: "POST",

  url: "/university/student/filter",

  data: dataString,

  cache: false,

  success: function(result){

    //console.log(result);

   var studentdata = result.student_list;
   var filterdata = [];

   if(studentdata.length){

     $('#dt-example tbody').empty();

     studentdata.forEach(function(value, index){

       filterdata.push({
         "Name" : value.name,
         "Email" : value.email,
         "Mobile" : value.phone,
         "Country" : value.countries,
         "lead_state" : value.lead_state,
         "Status" : value.status == 1 ? "ACTIVE" : "DEACTIVE",
         "Counselor_name" : value.facilitator_name,
         "Created_By" : value.createdby,
         "Created_Date" : value.createdate,
         "Action" : "<a class='btn btn-mg btn-info purpleButton' href='/student/profile/"+ value.user_id +"' > EDIT </a>"

       });

     });

     //console.log(filterdata,"hi");

     var table = $('.dataTable').DataTable({

       // Column definitions
       columns : [
           {
               data : 'Name',
               className : 'details-control',
           },
           {data : 'Email'},
           {data : 'Mobile'},
           {data : 'Country'},
           {data : 'lead_state'},
           {data : 'Status'},
           {data : 'Counselor_name'},
           {data : 'Created_By'},
           {data : 'Created_Date'},
           {data : 'Action'}
       ],
       data : filterdata,
       pagingType : 'full_numbers',
       // Localization
       language : {
         emptyTable     : 'No data to display.',
         zeroRecords    : 'No records found!',
         thousands      : ',',
         // processing     : 'Processing...',
         loadingRecords : 'Loading...',
         search         : 'Search:',
         paginate       : {
           next     : 'next',
           previous : 'previous'
         }
       },
       "bDestroy": true
     });

     $('.dataTable tbody').on('click', 'td.details-control', function () {
        var tr  = $(this).closest('tr'),
            row = table.row(tr);

        if (row.child.isShown()) {
          tr.next('tr').removeClass('details-row');
          row.child.hide();
          tr.removeClass('shown');
        }
        else {
          row.child(format(row.data())).show();
          tr.next('tr').addClass('details-row');
          tr.addClass('shown');
        }
     });

   $('#dt-example').on('click','.details-table tr',function(){
     $('.details-table').find('.adb-dtb-gchild:not(:first-child)').slideToggle();
       });


   } else {

     //$('#dt-example tbody').DataTable().destroy();
     $('#dt-example tbody').empty();

     var table = $('.dataTable').DataTable({

       // Column definitions
       columns : [
           {
               data : 'Name',
               className : 'details-control',
           },
           {data : 'Email'},
           {data : 'Mobile'},
           {data : 'Country'},
           {data : 'lead_state'},
           {data : 'Status'},
           {data : 'Counselor_name'},
           {data : 'Created_By'},
           {data : 'Created_Date'},
           {data : 'Action'}
       ],
       data : filterdata,
       pagingType : 'full_numbers',
       // Localization
       language : {
         emptyTable     : 'No data to display.',
         zeroRecords    : 'No records found!',
         thousands      : ',',
         // processing     : 'Processing...',
         loadingRecords : 'Loading...',
         search         : 'Search:',
         paginate       : {
           next     : 'next',
           previous : 'previous'
         }
       },
       "bDestroy": true
     });

     $('.dataTable tbody').on('click', 'td.details-control', function () {
        var tr  = $(this).closest('tr'),
            row = table.row(tr);

        if (row.child.isShown()) {
          tr.next('tr').removeClass('details-row');
          row.child.hide();
          tr.removeClass('shown');
        }
        else {
          row.child(format(row.data())).show();
          tr.next('tr').addClass('details-row');
          tr.addClass('shown');
        }
     });

   $('#dt-example').on('click','.details-table tr',function(){
     $('.details-table').find('.adb-dtb-gchild:not(:first-child)').slideToggle();
       });


   }

  }

});

}


function addComment(){

        var entityId = $('#entity_id').val();
        var studentIdNew = $('#student_id').val();
        var comment = $('#comment').val();

        var dataString = 'entity_id=' + entityId + '&student_login_id=' + studentIdNew + '&comment=' + comment + '&type=STUDENT';

        event.preventDefault();

        //alert(dataString);
        $.ajax({
            type: "POST",
            url: "/university/counsellor/inserttComments",
            data: dataString,
            cache: false,
            success: function(result){
                if(result == "SUCCESS"){
                    alert("Message Sent Successfully...!!!");
                    $('#comment').val('');
                    // window.location.href='/university/counsellor/communicatiohistory/'+entityId;
                } else {
                    alert(result);
                    window.location.href='/user/account';
                }
            }
        });
    }
</script>
