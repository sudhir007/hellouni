<script type="text/javascript" src="<?php echo base_url();?>source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>source/jquery.fancybox.css?v=2.1.5" media="screen" />
<link href='https://unpkg.com/boxicons@2.1.1/css/boxicons.min.css' rel='stylesheet' />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style.css?v=1.1.0" media="screen" />
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
<script src="<?php echo base_url();?>tabs/assets/js/responsive-tabs.min.js"></script>

<script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
<script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox-plus-jquery.min.js"></script>
<style>
   .cd-tabs-content li {
   font-size: 15px;
   line-height: 1.5;
   margin:0 0 10px;
   }
   .cd-tabs-content li p {
   font-size: 15px;
   line-height: 1.5;
   color:#242425 ;
   margin-bottom: 1em;
   }
   .shade_border{
   background:url(img/shade1.png) repeat-x; height:11px;
   }
   .search_by{
   background-color:#f3f4f4; box-shadow:5px 5px 5px #eae8e8; height:auto;
   padding-top: 10px;
   padding-left: 30px;
   padding-right: 30px;
   }
   .search_by h4{
   color:#3c3c3c;
   font-family:lato_regular;
   font-weight:500;
   }
   .table_heading{
   padding:5px;
   font-weight:bold;
   }
   .more_detail{
   background-color:#F6881F;
   text-transform:uppercase;
   height:28px;
   border:0px;
   color:#000;
   border-radius:2px;
   width:25px;
   }
   .right_side_form{
   background:#3c3c3c;
   box-shadow:5px 5px 2px #CCCCCC;
   height:auto;
   margin-top:40px;
   margin-left:10px;
   color:#ffffff;
   }
   label{
   color:#fff;
   }
   .more_detail{
   background-color:#F6881F;
   text-transform:uppercase;
   height:28px;
   border:0px;
   color:#fff;
   border-radius:2px;
   }
   label{
   font-size:11px;
   font-weight:300;
   }
   .total{
   background-image:url(images/tab.png);
   width:65px;
   height:41px;
   float:left;
   }
   .total_text{
   color:#7e7e7e;
   padding-top:2px;
   font-size:12px;
   }
   .total_text2{
   color:#5b8abe;
   font-size:12px;
   }
   .people{
   float:left;
   padding:10px;
   height:30px
   }
   .font_color{
   margin-left:-13px;
   }
   .font_color h4{
   color:#374552;
   }
   .fancy_box{
   border: 1px solid #64717d ;
   margin-top:5px;
   margin-left:5px;
   margin-right:40px;
   padding:5px;
   }
   .cd-tabs h4{
   margin: 16px 0 16px;
   text-transform: uppercase;
   font-family: lato_regular;
   font-weight: 500;
   letter-spacing: 1px;
   color: #374552;
   }
   .font_class{
   font-family: 'Open Sans' !important;
   font-size: 12px !important;
   letter-spacing: 1px !important;
   width:100%;
   height:40px;
   }
   .cd-tabs-navigation a{
   font-family: lato_bold;
   font-size: 11px;
   }
   .uni_logo{
   /*top:-155px;*/
  /* position:absolute;
   margin-left:-15px;*/
   }
   .uni_logo img{
   padding:7px;background:#fff;
   }
   .header_icons{
   }
   .header_icons ul li{
   display:inline-block;
   list-style-type:none;
   margin-bottom:-10px;
   }
   .form-horizontal {
   font-size:15px;
   }
   .selected{
   text-align: justify;
   }
   .accordion {
   background-color: #eee;
   color: #444;
   cursor: pointer;
   padding: 18px;
   /*width: 100%;*/
   border: none;
   text-align: left;
   outline: none;
   font-size: 15px;
   transition: 0.4s;
   }
   .active, .accordion:hover {
   background-color: #ccc;
   }
   .panel {
   padding: 0 18px;
   display: none;
   background-color: white;
   overflow: hidden;
   }
   table {
   font-family: arial, sans-serif;
   border-collapse: collapse;
   width: 100%;
   }
   td, th {
   border: 1px solid #dddddd;
   text-align: left;
   padding: 6px;
   }
   th{
   font-weight: bold;
   }
   /*tr:nth-child(even) {
   background-color: #dddddd;
   }*/
   .nav-tabs-dropdown {
   display: none;
   border-bottom-left-radius: 0;
   border-bottom-right-radius: 0;
   }
   .nav-tabs-dropdown:before {
   content: "\e114";
   font-family: 'Glyphicons Halflings';
   position: absolute;
   right: 30px;
   }
   @media screen and (min-width: 769px) {
   #nav-tabs-wrapper {
   display: block!important;
   }
   }
   @media screen and (max-width: 768px) {
   .nav-tabs-dropdown {
   display: block;
   }
   #nav-tabs-wrapper {
   /*display: none;*/
   border-top-left-radius: 0;
   border-top-right-radius: 0;
   text-align: center;
   }
   }
   @media (min-width: 768px){
   #compare-university-modal, #course-list-modal{
   height: auto;
   overflow: auto;
   max-height: 570px;
   }
   }
   @media (min-width: 1440px){
   #compare-university-modal, #course-list-modal {
   height: auto;
   overflow: auto;
   max-height: 680px;
   }
   }
   .nav-pills > li {
   background: #374552;
   text-transform: uppercase;
   }
   .nav > li > a:hover, .nav > li > a:focus {
   color: #FF9900!important;
   }
   .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
   color: #ffffff!important;
   background-color: #FF9900;
   outline: none;
   padding: 10px;
   /* color: #888; */
   text-decoration: none;
   text-transform: uppercase;
   /* background: #eee; */
   border: none !important;
   border-radius: 0px;
   }
   .nav-pills>li>a {
   border-radius: 0px;
   }
   .nav-pills>li>a:hover {
   background-color: #8c57c4 !important;
   color: white!important;
   }
   .well {
   padding:15px;
   }
   .panel-title {
   font-weight:bold;
   }
   /* The Modal (background) */
   .modal, .coursemodal {
   display: none; /* Hidden by default */
   position: fixed; /* Stay in place */
   z-index: 1; /* Sit on top */
   padding-top: 100px; /* Location of the box */
   left: 0;
   top: 0;
   width: 100%; /* Full width */
   height: 100%; /* Full height */
   overflow: auto; /* Enable scroll if needed */
   background-color: rgb(0,0,0); /* Fallback color */
   background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
   }
   /* Modal Content */
   .modal-content  {
   background-color: #fefefe;
   margin: auto;
   padding: 20px;
   border: 1px solid #888;
   width: 50%;
   }
   .coursemodal-content  {
   background-color: #fefefe;
   margin: auto;
   padding: 20px;
   border: 1px solid #888;
   width: 80%;
   }
   /* The Close Button */
   .close {
   color: #8e0f0f;
   float: right;
   font-size: 28px;
   font-weight: bold;
   }


   .close:hover,
   .close:focus {
   color: #000;
   text-decoration: none;
   cursor: pointer;
   }
   .courseclose:hover,
   .courseclose:focus {
   color: #000;
   text-decoration: none;
   cursor: pointer;
   }
   #compare-university-modal, #course-list-modal p{
   font-size: 16px;
   text-align: center;
   font-weight: bold;
   margin: 0 0 25px;
   }
   #compare-university-form input[type='checkbox']{
   float: right;
   }
   #compare-university-form label{
   color:#000;
   font-size: 13px;
   margin-top: 10px;
   }
   .compare_universities_submit{
   text-align: center;
   font-size: 15px;
   }
   #compare-university-form input[type='submit']{
   padding: 5px;
   }
   @import url(https://fonts.googleapis.com/css?family=PT+Sans+Narrow);
   .sectionClass {
   padding: 10px 0px 0px 0px;
   position: relative;
   display: block;
   }
   .fullWidth {
   width: 100% !important;
   display: table;
   float: none;
   padding: 0;
   min-height: 1px;
   height: 100%;
   position: relative;
   }
   .sectiontitle {
   background-position: center;
   margin: 30px 0 0px;
   text-align: center;
   min-height: 20px;
   }
   .sectiontitle h2 {
   font-size: 30px;
   color: #222;
   margin-bottom: 0px;
   padding-right: 10px;
   padding-left: 10px;
   }
   .headerLine {
   width: 160px;
   height: 2px;
   display: inline-block;
   background: #101F2E;
   }
   .projectFactsWrap{
   display: flex;
   flex-direction: row;
   flex-wrap: wrap;
   /*background-image: url("<?php echo base_url();?>application/images/counters.png");
   */   
   }
   #projectFacts .fullWidth{
   padding: 0;
   }
   .projectFactsWrap .item{
   width: 33%;
   /* height: 100%; */
   padding: 14px 20px;
   text-align: center;
  /* border-radius: 15px;
   margin: 4px 8px 10px 8px;*/
   }
   /*.projectFactsWrap .item:nth-child(1){
   background: rgba(135, 3, 197, 0.5686274509803921);
   }
   .projectFactsWrap .item:nth-child(2){
    background: rgba(246, 135, 44, 0.5686274509803921);
   }
   .projectFactsWrap .item:nth-child(3){
    background: rgba(135, 3, 197, 0.5686274509803921);
   }
   .projectFactsWrap .item:nth-child(4){
   background: rgba(246, 135, 44, 0.5686274509803921);
   }
   .projectFactsWrap .item:nth-child(5){
    background: rgba(135, 3, 197, 0.5686274509803921);
   }
   .projectFactsWrap .item:nth-child(6){
   background: rgba(246, 135, 44, 0.5686274509803921);
   }*/
   .projectFactsWrap .item p.number{
   font-size: 24px;
   padding: 0;
   font-weight: bold;
       color: #f6872c;
   }
   .projectFactsWrap img{
          width: auto;
          margin-right: 10px;
   }
   .projectFactsWrap .item p{
   color: black;
   font-size: 18px;
   margin: 0;
   padding: 0px;
   }
   /*.projectFactsWrap .item span{
   width: 60px;
   background: rgb(246, 136, 44);
   height: 2px;
   display: block;
   margin: 0 auto;
   }*/
   .projectFactsWrap .item i{
   vertical-align: middle;
   font-size: 40px;
   color: rgb(246, 136, 44);
   /*background: rgb(55, 69, 82);*/
   }
   /*.projectFactsWrap .item:hover i, .projectFactsWrap .item:hover p{
   background: rgb(55, 69, 82);  
   color: white;
   }
   .projectFactsWrap .item:hover span{
   background: rgb(55, 69, 82);
   }*/
   @media (max-width: 786px){
   .projectFactsWrap .item {
   flex: 0 0 50%;
   }
   }
   .trans{
   opacity: 1;
   -webkit-transform: translateX(0px);
   transform: translateX(0px);
   -webkit-transition: all 500ms ease;
   -moz-transition: all 500ms ease;
   transition: all 500ms ease;
   }
   @media screen and (max-width: 768px) {
   .authorWindow{
   width: 210px;
   }
   .authorWindowWrapper{
   bottom: -170px;
   margin-bottom: 20px;
   }
   }
   /*cards*/
   .card{
   border-radius: 4px;
   background: #fff;
   box-shadow: 0 6px 10px rgba(0,0,0,.08), 0 0 6px rgba(0,0,0,.05);
   transition: .3s transform cubic-bezier(.155,1.105,.295,1.12),.3s box-shadow,.3s -webkit-transform cubic-bezier(.155,1.105,.295,1.12);
   padding: 0px 15px 0px 14px;
   cursor: pointer;
  min-height: 440px;

   }

   .cardInfo{
    padding: 0px 15px 5px 15px;
   }
   .card:hover{
   transform: scale(1.05);
   box-shadow: 0 10px 20px rgba(0,0,0,.12), 0 4px 8px rgba(0,0,0,.06);
   }
   .card h3{
   font-weight: 600;
   }
   .cardimg{
    padding: 20px;
    text-align: center;
    background-color: #8703c5;
   }
   /*.card img{
   position: absolute;
   top: 20px;
   right: 15px;
   max-height: 120px;
   }*/
   /*.card-1{
   background-image: url(<?php echo base_url();?>application/images/degree.png);
   background-repeat: no-repeat;
   background-position: right;
   background-size: 100px;
   padding: 4px 75px 15px 15px;
   min-height: 220px;
   }
   .card-2{
   background-image: url(<?php echo base_url();?>application/images/campus.png);
   background-repeat: no-repeat;
   background-position: right;
   background-size: 112px;
   padding: 4px 75px 15px 15px;
   min-height: 220px;
   }
   .card-3{
   background-image: url(<?php echo base_url();?>application/images/spending.png);
   background-repeat: no-repeat;
   background-position: right;
   background-size: 100px;
   padding: 4px 75px 15px 15px;
   min-height: 220px;
   }*/
   @media(max-width: 990px){
   .card{
   margin: 20px;
   }
   }
   .cardCss{
   border-radius: 4px;
    background: #fff;
    box-shadow: 0 4px 13px rgba(0,0,0,.08), 0 0 6px rgba(0,0,0,.05);
    margin: 10px 1px 10px 1px;
    border-right: 8px solid #f6872c;
   }
   /*entry req table*/
   #enquryTable td, #enquryTable th {
   border: none;
   padding: 8px;
   text-align: center;
   background-color: white;
   border-bottom: 1px solid #f7f7d8;
   vertical-align: middle;
   }
   #enquryTable tr:nth-child(even){background-color: #f2f2f2;}
   #enquryTable tr:hover {background-color: #ddd;}
   #enquryTable th {
   padding: 12px;
   text-align: center;
   background-color: #01182d;
   letter-spacing: 1px;
   color: white;
   vertical-align: middle;
   }
   .canvasjs-chart-credit{
   display: none !important;
   }
  
   /*uni image carousel*/
   .hide-bullets {
   list-style:none;
   margin-left: -40px;
   margin-top:20px;
   }
   .thumbnail {
   padding: 0;
   }
   .carousel-inner>.item>img, .carousel-inner>.item>a>img {
   width: 100%;
   }
   .col-sm-3 a {
   border: 1px solid transparent;
   border-radius: 0;
   transition: all 3s ease;
   }
   .col-sm-3 a:hover {
   border: 1px solid #ff4647;
   border-radius: 100% 60% / 30% 10%;
   background: linear-gradient(rgba(56,123,131,0.7),rgba(56,123,131,0.7));
   }
   /*College Box*/
   .box>.icon {
   text-align: center;
   position: relative;
   }
   .box>.icon>.image {
   position: relative;
   z-index: 2;
   margin: auto;
   width: 88px;
   height: 88px;
   border: 8px solid white;
   line-height: 88px;
   border-radius: 50%;
   background: #f6882c;
   vertical-align: middle;
   }
   .box>.icon:hover>.image {
   background: #333;
   }
   .box>.icon>.image>i {
   font-size: 36px !important;
   color: #fff !important;
   }
   .box>.icon:hover>.image>i {
   color: white !important;
   }
   .box>.icon>.info {
   margin-top: -24px;
   /*background: rgba(0, 0, 0, 0.04);
   border: 1px solid #e0e0e0;*/
   box-shadow: 0px 2px 4px #b4b4bf;
   padding: 15px 1px 10px 1px;
   min-height: 500px;
   }
   .box>.icon:hover>.info {
   background: rgba(0, 0, 0, 0.04);
   border-color: #e0e0e0;
   color: white;
   }
   .box>.icon>.info>h3.title {
   font-family: "Roboto", sans-serif !important;
   font-size: 16px;
   color: #222;
   font-weight: 500;
   }
   .box>.icon>.info>p {
   font-family: "Roboto", sans-serif !important;
   font-size: 13px;
   color: #666;
   line-height: 1.5em;
   margin: 20px;
   }
   .box>.icon:hover>.info>h3.title,
   .box>.icon:hover>.info>p,
   .box>.icon:hover>.info>.more>a {
   color: #222;
   }
   .box>.icon>.info>.more a {
   font-family: "Roboto", sans-serif !important;
   font-size: 12px;
   color: #222;
   line-height: 12px;
   text-transform: uppercase;
   text-decoration: none;
   }
   .box>.icon:hover>.info>.more>a {
   color: white;
   padding: 6px 8px;
   background-color: #f6882c;
   }
   .box .space {
   height: 30px;
   }
   /*top stick*/
   .headerTop {

    padding: 10px 16px;
    background: #f6882c;
    color: #000000;
    box-shadow: 0px 3px 2px #cecccc;
    margin-bottom: 15px;
    position: relative;
    height: -webkit-fill-available;

    width: 100vw;
    margin-left: calc(-49vw + 48% - -3px);

    /*padding: 5% 10% 3% 10%;*/


   }

   .parallax-wrapper {
    position: relative;
        margin-top: 3%;
    }

    .parallax-container {
        /*height: 200px;
        background-color: #0f0;
        width: 100vw;
        margin-left: calc(-50vw + 50% - 8px);
        position: relative;*/
        height: -webkit-fill-available;
    /* background-color: #0f0; */
    width: 100vw;
    margin-left: calc(-49vw + 48% - -3px);
    position: relative;
    padding: 5% 10% 3% 10%;
        background-repeat: round;
    background-image: url("<?php echo base_url();?>application/images/framephoto.png");
    }

    .parallax-container h4{
      color: white;
        /* line-height: 31px; */
        line-height: 1.3;
        text-transform: initial;
    }
   .content {
   padding: 16px;
   }
   .sticky {
   position: fixed;
    top: 0;
    /* width: 100%; */
    z-index: 10;
    left: 0;
    right: 0;
    padding: 10px 100px 0px 100px;
    text-align: center;
    height: unset;
    margin-left: 0vw;
   }

   @media only screen and (max-width: 600px) {
      .sticky {
   
    padding: 0px !important;
   
   }
   }
   .sticky + .content {
   padding-top: 102px;
   }
   .vl {
   border-right: 2px solid #f7942e;
   height: auto;
   min-height: 100px;
   }
   .pTag{
   margin: 0 0 5px !important;
   font-size: 15px !important;
   }
   .wrapper {
   position: relative;
   width: 330px;
   height: 80px;
   -moz-user-select: none;
   -webkit-user-select: none;
   -ms-user-select: none;
   user-select: none;
   float: right;
   /*margin-left: 16px;*/
   }
   .signature-pad {
   position: absolute;
   left: 0;
   top: 0;
   width:325px;
   height:75px;
   }
   /*Department*/
   /* .feature-area {
   padding-top: 113px;
   padding-bottom: 75px;
   position: relative; }*/
   @media only screen and (min-width: 320px) and (max-width: 479px) {
   .feature-area {
   padding-top: 100px; } }
   .feature-area .sec-heading .sec__title {
   line-height: 55px; }
   @media (max-width: 480px) {
   .feature-area .sec-heading .sec__title {
   line-height: 40px; } }
   .feature-area .service-button {
   margin-top: 80px;
   text-align: right; }
   @media only screen and (min-width: 768px) and (max-width: 991px) {
   .feature-area .service-button {
   text-align: left;
   margin-top: 30px;
   margin-bottom: 35px; } }
   @media only screen and (min-width: 480px) and (max-width: 767px) {
   .feature-area .service-button {
   text-align: left;
   margin-top: 30px;
   margin-bottom: 35px; } }
   @media only screen and (min-width: 320px) and (max-width: 479px) {
   .feature-area .service-button {
   text-align: left;
   margin-top: 30px;
   margin-bottom: 35px; } }
   .feature-area .feature-box {
   margin-top: 25px;
   text-align: center; }
   .feature-area .feature-box .feature-item {
       position: relative;
    background-color: #fff;
    border-radius: 20px;
    /* box-shadow: 0 0 40px rgba(82, 85, 90, 0.1); */
    box-shadow: 0 0 10px rgba(127, 131, 138, 0.22);
    transition: all 0.3s;
    /* padding: 45px 30px 40px 30px; */
    min-height: 220px;
    padding: 18px 13px 13px 13px;
    margin-bottom: 30px;
    z-index: 1;
    border-top: 8px solid #8c57c4; }
   .feature-area .feature-box .feature-item:after {
   position: absolute;
   content: '';
   bottom: 0;
   width: 100%;
   height: 2px;
   background-color: #F6881F;
   left: 0;
   border-radius: 4px;
   transform: scale(0);
   transition: all 0.3s; }
   @media (max-width: 1199px) {
   .feature-area .feature-box .feature-item {
   padding-right: 20px;
   padding-left: 20px; } }
   @media only screen and (min-width: 768px) and (max-width: 991px) {
   .feature-area .feature-box .feature-item {
   padding-right: 30px;
   padding-left: 30px;
   margin-bottom: 30px; } }
   @media only screen and (min-width: 481px) and (max-width: 767px) {
   .feature-area .feature-box .feature-item {
   width: 60%;
   margin-left: auto;
   margin-right: auto; } }
   @media only screen and (min-width: 480px) and (max-width: 767px) {
   .feature-area .feature-box .feature-item {
   padding-right: 30px;
   padding-left: 30px;
   margin-bottom: 30px; } }
   @media only screen and (min-width: 320px) and (max-width: 479px) {
   .feature-area .feature-box .feature-item {
   padding-right: 30px;
   padding-left: 30px;
   margin-bottom: 30px;
   width: 100%; } }
   .feature-area .feature-box .feature-item .feature__icon {
   color: #F6881F;
   display: inline-block;
   position: relative;
   width: 100px;
   height: 100px;
   line-height: 111px;
   margin-bottom: 5px;
   z-index: 1;
   transition: all 0.3s;
   background-color: rgba(246, 107, 93, 0.1);
   border-radius: 50%; }
   @media (max-width: 1199px) {
   .feature-area .feature-box .feature-item .feature__icon {
   font-size: 45px;
   margin-top: 20px;
   margin-bottom: 35px; } }
   @media only screen and (min-width: 768px) and (max-width: 991px) {
   .feature-area .feature-box .feature-item .feature__icon {
   font-size: 65px;
   margin-bottom: 39px; } }
   @media only screen and (min-width: 480px) and (max-width: 767px) {
   .feature-area .feature-box .feature-item .feature__icon {
   font-size: 65px;
   margin-bottom: 39px; } }
   @media only screen and (min-width: 320px) and (max-width: 479px) {
   .feature-area .feature-box .feature-item .feature__icon {
   font-size: 65px;
   margin-bottom: 39px; } }
   .feature-area .feature-box .feature-item .feature__icon:before {
   font-size: 45px; }
   .feature-area .feature-box .feature-item .feature__title {
   font-size: 20px;
   font-weight: 700;
   margin-bottom: 24px;
   text-transform: capitalize; }
   .feature-area .feature-box .feature-item .feature__title a {
   color: #233d63;
   transition: all 0.3s;
   background-color: white;
   font-size: 16px;
   border-radius: 8px; }
   .feature-area .feature-box .feature-item:hover {
   -webkit-border-radius: 0;
   -moz-border-radius: 0;
   border-radius: 0; }
   .feature-area .feature-box .feature-item:hover .feature__number {
   transform: scale(1); }
   .feature-area .feature-box .feature-item:hover .feature__icon {
   background-color: #F6881F;
   color: #fff; }
   .feature-area .feature-box .feature-item:hover .feature__title a {
   color: #F6881F; }
   .feature-area .feature-box .feature-item:hover:after {
   transform: scale(1); }
   .feature-area .feature-box .feature-item:hover:before {
   opacity: 0.2;
   visibility: visible;
   transform: scale(1); }
   /*  .feature-area .feature-box .feature-item:before {
   position: absolute;
   content: '';
   top: 0;
   left: 0;
   width: 100%;
   height: 100%;
   /*background-image: url(<?php echo base_url();?>application/images/deptLayout.png);
   background-image: url(https://techydevs.com/demos/themes/html/minzel/images/dots3.png);
   background-size: cover;
   background-position: center;
   z-index: -1;
   opacity: 0;
   visibility: hidden;
   transition: all 0.5s;
   transform: scale(0.6); }*/



   /*New Css*/
.aboutus-section {
    padding: 90px 0;
}
.aboutus-title {
    font-size: 40px;
    letter-spacing: 1.8px;
    /* line-height: 32px; */
    /* margin: 0 0 0px; */
    /* padding: 0 0 11px; */
    position: relative;
    /* text-transform: uppercase; */
    color: #313131;
}

/*about us cards*/
.wrimagecard{
  width: 200px;
    height: 200px;
    margin-top: 0;
    /* margin-right: 33px; */
    margin-bottom: 1.5rem;
    text-align: left;
    position: relative;
    background: #fff;
    box-shadow: 12px 15px 20px 0px rgba(46,61,73,0.15);
    border-radius: 12px;
    transition: all 0.3s ease;
}

.wrimagecard:hover  {
  background: #815cd5;
}

.wrimagecard h5{
  color: #f6882c;
}

.wrimagecard:hover i, .wrimagecard:hover h5, .wrimagecard:hover p{
  color: #fff;
}


.wrimagecard .fa , .wrimagecard .fas{
    position: relative;
    font-size: 50px;
    color: #815cd5;
}

.wrimagecard-topimage_header{
padding: 32px 20px 10px 20px;
}
a.wrimagecard:hover, .wrimagecard-topimage:hover {
    box-shadow: 2px 4px 8px 0px rgba(46,61,73,0.2);
    color: white;
}
.wrimagecard-topimage a {
    width: 100%;
    height: 100%;
    display: block;
}
.wrimagecard-topimage_title {
    padding: 4px 10px;
    height: 80px;
    padding-bottom: 0.75rem;
    position: relative;
    text-align: center;
}
.wrimagecard-topimage a {
    border-bottom: none;
    text-decoration: none;
    color: #525c65;
    transition: color 0.3s ease;
}

.wrimagecard-topimage_title p{
    margin: 0px;
    font-size: 12px;
    line-height: 1;
    font-weight: bolder;
    letter-spacing: 0.4px;
   word-break: break-all;
}

.col-offset-half{
 margin-left: 4.166666667%;
}

/*.featureBox{
 width: 100vw;
    position: relative;
    margin-left: -49vw;
    height: -webkit-fill-available;
    margin-top: 35px;
    left: 50%;
    background-color: black;
    background-image: url("<?php echo base_url();?>application/images/framephoto.png");
    padding: 50px 100px;
    background:linear-gradient(0deg, rgba(49, 47, 49, 0.3), rgba(74, 70, 72, 0.3)), url("<?php echo base_url();?>application/images/framephoto.png");
    background-size:cover;
}*/


/*.jumbotron{
    background-color: red;
    color:#fff;
}*/
.box{
    /*border:1px solid #ccc;*/
    padding:20px;
    text-align: center;
}

.square {
  height: 55px;
  width: 55px;
  background-color: #fff;
  transform:rotate(45deg);
  position: relative;
    left: 45%;
    bottom: 10px;
        border-radius: 5px;
}
/*.item-count {
  color: #333;
  display: table-cell;
  height: 60px;
  transform: rotate(-45deg);
  vertical-align: middle;
  width:60px;
}*/

.item-count{
      position: absolute;
    /* z-index: 1111; */
    color: #000;
    /* height: 0px; */
    transform: rotate(-45deg);
    vertical-align: middle;
    /* width: 10px; */
    top: 9px;
    left: 25px;
}


/*chart*/
.circle-chart__circle {
  animation: circle-chart-fill 2s reverse; /* 1 */
  transform: rotate(-90deg); /* 2, 3 */
  transform-origin: center; /* 4 */
}

.circle-chart__circle--negative {
  transform: rotate(-90deg) scale(1,-1); /* 1, 2, 3 */
}

.circle-chart__info {
  animation: circle-chart-appear 2s forwards;
  opacity: 0;
  transform: translateY(0.3em);
}

@keyframes circle-chart-fill {
  to { stroke-dasharray: 0 100; }
}

@keyframes circle-chart-appear {
  to {
    opacity: 1;
    transform: translateY(0);
  }
}

.circleMargin{
      margin: 10px 18px;
}

.financialTable tr>th{
  border: none;
  background-color: #8c57c5;
}

.financialTable tr>th>div{
    padding: 6px 20px;
    background-color: #eee3f7;
    /* color: black; */
    border-radius: 6px;
}

.financialTable tr>td>div{
    padding: 0px 20px;
    background-color: #8c57c5;
    /* color: black; */
    border-radius: 6px;
    text-align: left;
}

.financialTable tr>td>div>h5{

     color: white;

}

.buttonHome{
    height: 50px !important;
    color: #ffffff !important;
    background-color: #f6872c;
    font-size: 18px !important;
    border: 2px solid #f6872c ;
    border-top-left-radius: 10px;
    border-bottom-left-radius: 10px;
}

.buttonHome:hover{
  background-color: #8c57c5 !important;
  border: 2px solid #8c57c5 !important;
}

.buttonProgram{
    height: 50px !important;
    color: #ffffff !important;
    background-color: #8c57c5;
    font-size: 18px !important;
    border: 2px solid #f6872c;
    border-top-right-radius: 10px;
    border-bottom-right-radius: 10px;
}

.buttonProgram:hover{
  background-color: #f6872c !important;
  border: 2px solid #f6872c !important;
}


.flLt {
   float: left;
   }
.uni-image {
   background: #fff;
   border: 1px solid #f3f3f3;
   padding: 5px;
   position: relative;
   width: 120px;
   height: 100px;
   }

.uni-boxPurple{
   float: left;
   width: 100%;
   padding: 8px;
   position: relative;
   background-color:#ffffff;
   /*border:1px solid #f0f0f0;*/
   box-shadow: 3px 4px 6px #ccccc4;
   margin: 20px 0px;
   border-right: 7px solid #8c57c4;
   }
   .uni-boxPurple p{
   margin:0px;
   line-height:unset;
   font-size: 13px;
   }
  /* .uni-detail {
   margin-left: 160px;
   }*/
    .uni-detailModal {
   margin-left: 205px;
   }
   .uni-title {
   margin-bottom: 10px;
   width: 100%;
   }
   .uni-title a {
   font-weight: 600;
   font-size: 13px;
   color: #111111;
   }
   .uni-title span {
   color: #666666;
   }
   .uni-sub-title {
   font-weight: 600;
   font-size: 15px;
   color:#666666;
   }
   a.uni-sub-title {
   color:#666666;
   }

   .iconRowPurple .iconCss{
   background: #8c57c4;
   padding: 7px;
   height: 30px;
   width: 30px;
   border-radius: 50%;
   color: white;
   font-size: 15px;
   margin: 5px 0px 5px 0px;
   }
   .iconRowPurple strong {
   font-size: 13px;
   color: #8c57c4;
   }
   .iconRowPurple span {
   font-size: 13px;
   color: #000;
   }
   .font14{
   font-size: 14px;
   }
   .mrgtop{
   margin: 9px 0px 5px 0px; ;
   }

   .btn-brochure a {
   padding: 8px 18px 9px!important;
   }

   .twoPXPadding{
    padding-right: 2px !important;
    padding-left: 2px !important;
   }

   .faPadding{
   padding-right: 0px !important;
    padding-left: 22px !important;
   }

   .heart{
    font-size: 18px !important;
    margin: 0px 8px;
    color:red;
}

.boxAlign {
  display: inline-flex;
    flex-wrap: wrap;
    height: 250px;
    width: 30.33%;
    text-align: center;
    align-content: space-between;
    margin: 10px;
}

.displayInline{
   display: inline-flex;
}
/*.grid {
  display: grid;
  grid-column-gap: 1em;
  grid-row-gap: 1em;
  grid-template-columns: repeat(1, 1fr);
}

@media (min-width: 31em) {
  .grid {
    grid-template-columns: repeat(2, 1fr);
  }
}*/

/*New Update*/
.left, .right {
   position: relative;
    height: 160px;
    background: #fff;
    float: left;
}

.left{
   width: 20%;
}


.left:after {
  content: '';
    line-height: 0;
    font-size: 0;
    width: 0;
    height: 0;
    border-bottom: 160px solid #fff;
    border-top: 50px solid transparent;
    border-left: 0px solid transparent;
    border-right: 88px solid transparent;
    position: absolute;
    bottom: 0;
    right: -88px;
}

/*.right {
  margin-left: 60px;
  background: #eee0;
}

.right:before {
  content: '';
    line-height: 0;
    font-size: 0;
    width: 0;
    height: 0;
    border-bottom: 50px solid transparent;
    border-top: 160px solid #eee0;
    border-left: 50px solid transparent;
    border-right: 0px solid #eee0;
    position: absolute;
    top: 0px;
    left: -50px;
}*/
.callToAction{
   height: 45px !important;
    color: #ffffff !important;
    background-color: #f6872c;
    font-size: 14px !important;
    border: 2px solid #f6872c;
    border-radius: 10px;
}

.googleReview{
   font-size: 14px;
    background-color: white;
    border-radius: 10px;
    text-align: center;
    /* margin-top: 5px; */
    height: 45px;
    padding-top: 1%;
    text-align: center;
}

/* Style the tab */
.tab {
  overflow: hidden;
  background-color: #ffffff;
    box-shadow: 0px 1px 0px #f6882c;
    padding: 1.5% 0%;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #f6872c;
  color: white;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #f6872c;
    color: white;
    font-weight: 600;
    letter-spacing: 1.5px;
}

/* Style the tab content */
.tabcontent {
  display: none;
  /*padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;*/
}

.noPadding{
   padding-left: 0px !important;
   padding-right: 0px !important;
}

.formRegister input[type=text], select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

.formRegister input[type=submit] {
  width: 100%;
  background-color: #f6872c;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

.formRegister input[type=submit]:hover {
  background-color: #45a049;
}

.formRegister div {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}

.formRegister h2{
       text-transform: capitalize;
    text-align: center;
}




.faq .accordion-content {
  /*max-width: fit-content;*/
  margin: 0 auto;
  padding: 2rem;
  background: #fff;
  /*box-shadow: 0px 4px 16px rgba(0, 0, 0, 0.09);*/
  border-radius: 8px;
}
.faq .accordion-item {
  display: flex;
    flex-direction: column;
    padding: 4px 2rem;
    border-radius: 5px;
    /* border: 1px solid #ddd; */
    box-shadow: 4px 4px 9px 0px rgb(0 0 0 / 35%);
    cursor: pointer;
    background: #fff;
    margin-bottom: 1.5em;
}
.faq .item-header {
  display: flex;
  justify-content: space-between;
  column-gap: 0.2em;
}

.faq .item-icon {
  margin-top: 1rem;
  flex: 0 0 25px;
  display: grid;
  place-items: center;
  font-size: 1.25rem;
  height: 25px;
  width: 25px;
  border-radius: 4px;
  background: #f3852b;
  cursor: pointer;
  box-shadow: 0px 4px 16px rgba(0, 0, 0, 0.09);
  color: white;
    font-weight: 600;
    font-size: 18px;
}
.faq .item-icon i {
  transition: all 0.25s cubic-bezier(0.5, 0, 0.1, 1);
}
.faq .item-question {
  font-size: 15px;
    line-height: 1;
    font-weight: 600;
    color: black;
    letter-spacing: 1.5px;
}

.faq .active .item-icon i {
  transform: rotate(180deg);
  color: white;
    font-weight: 600;
    font-size: 18px;
}

.faq .active .item-question {
  font-weight: 500;
  font-weight: 600;
    color: #f6872c;
}

.faq .item-content {
  max-height: 0;
  overflow: hidden;
  transition: all 300ms ease;
}

.faq .item-answer {
  line-height: 160%;
  opacity: 0.8;
  font-size: 14px;
      margin-bottom: 10px;
}

.faq .item-answer a{
  color: #f6872c;
}



.faq header{
   height:initial;
}

.faq .active, .accordion:hover {
   background-color: #fff;
   }

h2 , h1 , h3{
       text-transform: capitalize;
}

ul, li {
   font-size: 15px;
}


/*.ranking .tabs {
  max-width: 538px;
}*/
.ranking .nav-tabs li {
  float: left;
  width: 50%;
}
.ranking .nav-tabs li:first-child a {
  border-right: 0;
  border-top-left-radius: 6px;
}
.ranking .nav-tabs li:last-child a {
  border-top-right-radius: 6px;
}
.ranking a {
  /*background: #eaeaed;
  border: 1px solid #cecfd5;*/
  color: black;
  /*display: block;*/
  font-weight: 600;
  padding: 10px 0;
  text-align: center;
  text-decoration: none;
}
/*.ranking a:hover {
  color: #ff7b29;
}*/

.ranking .active, .accordion:hover{
   background-color:white !important;
}

.ranking .nav-tabs>li.active>a{
   color: #fff !important;
    background-color: #f6872c;
  cursor: default;
}

.ranking .tab-content {
  border: 1px solid #cecfd5;
  border-radius: 0 0 6px 6px;
  border-top: 0;
  clear: both;
  padding: 24px 30px;
  position: relative;
  top: -1px;
}

form.example input[type=text] {
  padding: 10px;
  font-size: 17px;
  border: 1px solid grey;
  float: left;
  width: 80%;
  background: #f1f1f1;
}

form.example button {
  float: left;
  width: 20%;
  padding: 10px;
  background: #2196F3;
  color: white;
  font-size: 17px;
  border: 1px solid grey;
  border-left: none;
  cursor: pointer;
}

form.example button:hover {
  background: #0b7dda;
}

form.example::after {
  content: "";
  clear: both;
  display: table;
}

.overview h1{
   color: #815dd5;
    font-size: 24px;
}

.overview h2{
       font-size: 20px;
    color: #f6872c;
}


.block {
   border-radius: 10px;
    margin-top: 20px;
    min-height: 100px;
    padding: 15px 20px 5px 20px;
    box-shadow: 4px 4px 8px 0px #00000069;

}

#Admission .nav {
   background-color: ghostwhite;
   padding: 20px 15px;

}

#Admission .nav a{
   color: #3636da;
    font-size: 16px;
    letter-spacing: 0.8px;
}

#Admission .nav  li:hover {
   background-color: : #ffffff!important;
   }

#Admission  p{
   margin-bottom: 8px;
   font-size: 14px;
}

#Admission ul li{
   list-style-type: disc;
}

#Admission ol li{
   list-style-type: decimal;
}

.custom-btn {
  width: 130px;
  height: 40px;
  color: #fff;
  border-radius: 5px;
  padding: 10px 25px;
  font-family: 'Lato', sans-serif;
  font-weight: 500;
  background: transparent;
  cursor: pointer;
  transition: all 0.3s ease;
  position: relative;
  display: inline-block;
   box-shadow:inset 2px 2px 2px 0px rgba(255,255,255,.5),
   7px 7px 20px 0px rgba(0,0,0,.1),
   4px 4px 5px 0px rgba(0,0,0,.1);
  outline: none;
}

/* 3 */
.btn-3 {
  background: rgb(129 93 213);
background: linear-gradient(0deg, rgb(115 91 171) 0%, rgb(129 93 213) 100%);
  width: 130px;
  height: 40px;
  line-height: 42px;
  padding: 0;
  border: none;
  margin-bottom: 6px;
  
}
.btn-3 span {
  position: relative;
  display: block;
  width: 100%;
  height: 100%;
  text-align: center;
}
.btn-3:before,
.btn-3:after {
  position: absolute;
  content: "";
  right: 0;
  top: 0;
   background: rgb(118 91 177);
  transition: all 0.3s ease;
}
.btn-3:before {
  height: 0%;
  width: 2px;
}
.btn-3:after {
  width: 0%;
  height: 2px;
}
.btn-3:hover{
   background: transparent;
  box-shadow: none;
}
.btn-3:hover:before {
  height: 100%;
}
.btn-3:hover:after {
  width: 100%;
}
.btn-3 span:hover{
   color: rgb(118 91 177);
}
.btn-3 span:before,
.btn-3 span:after {
  position: absolute;
  content: "";
  left: 0;
  bottom: 0;
   background: rgb(118 91 177);
  transition: all 0.3s ease;
}
.btn-3 span:before {
  width: 2px;
  height: 0%;
}
.btn-3 span:after {
  width: 0%;
  height: 2px;
}
.btn-3 span:hover:before {
  height: 100%;
}
.btn-3 span:hover:after {
  width: 100%;
}
/*div.textContainer>div:nth-of-type(odd) {
  background: #e0e0e0;
}*/

.moretext {
  display: none;
}

.grid_container{
   max-width: 1200px;
  margin: 0 auto;
  display: grid;
  grid-gap: 2rem;
  justify-content: center;
  
}

.grid-item {
/*   border-bottom: thin #edf1f2 solid; */
  padding: 22.85px 0px;
  display: flex;
  align-items: center;
}

.grid-item .grid-item-img {
  display: inline-block;
  margin-left: 20px;
  vertical-align:middle;
}
.grid-item .grid-item-img img {
  width: 100px;
  height: 100px;   
  
}

.grid-item .grid-item-text{
  display: inline-block;
  margin-left: 15px;
  vertical-align:middle; 
  font-family: 'Oswald', sans-serif;
   font-weight: 400;
   color: #222;
   font-size: 18px;
 
}

@media (min-width: 599.98px) {
  .grid_container { grid-template-columns: repeat(2, 1fr); }
  
}


@media (min-width: 959.98px) {
  .grid_container { grid-template-columns: repeat(2, 1fr); }
  
  .grid-item .grid-item-text {
  font-size: 16px;
  
  }
  
}


.grid-item-img a:hover {
  cursor: pointer;
box-shadow: 0px 14px 28px rgba(21, 19, 60, 0.22), 0px 10px 10px rgba(21, 19, 60, 0.22);
}

/* .grid-item span:hover{
  text-decoration: underline;  
} */
.grid-item-text h3 {
  color: #000;
  text-transform: capitalize;
    font-size: 17px;
    text-align: left;
  /*  display: inline-block;
   padding: 15px 20px; */
  position: relative;
      margin-bottom: 5px;
}
.grid-item-text h3:hover{
cursor: pointer;
}
.grid-item-text h3:after {
  
 background: none repeat scroll 0 0 transparent;
  bottom: 0;
  content: "";
  display: block;
  height: 2px;
  left: 50%;
  position: absolute;
  background: #000;
  transition: width 0.3s ease 0s, left 0.3s ease 0s;
  width: 0;
}

.grid-item-text h3:hover:after {
  
  width: 100%; 
  left: 0; 
}


.flex {
    display: flex;
    align-items: center;
    justify-content: flex-end;
   bottom: -70px;
}
 .app-btn {
    width: 45%;
    max-width: 160px;
    color: #fff;
    margin: 20px 10px;
    text-align: left;
    border-radius: 5px;
    text-decoration: none;
    font-family: "Lucida Grande", sans-serif;
    font-size: 10px;
    text-transform: uppercase;
}
 .review{
    background-color: #ffffff !important;
    transition: background-color 0.25s linear;
}

.getCounselling{
    background-color: #f6872c !important;
    transition: background-color 0.25s linear;
}
 .app-btn.blu:hover {
    background-color: #454545;
}
 .app-btn i {
    width: 20%;
    text-align: center;
    font-size: 28px;
    margin-right: 7px;
}
 .app-btn .big-txt {
    font-size: 17px;
    text-transform: capitalize;
}

.overview ul{
       list-style: disc;
    margin-left: 2rem;
}

#programView p , #Ranking p, #Admission p, #Scholarships p, #FAQs p{
   margin: 0px 0 15px;
}

.tabButton {
    border-radius: 5px;
    background-color: #f6872c !important;
    padding: 10px !important;
    width: 18%;
    margin-top: 4px;
    float: right !important;
    margin-right: 4px;
}

.uniLogoDivimg{
       width: 60%;
    margin-left: auto;
    margin-right: auto;
    display: block;
    margin-top: 9%;
    margin-bottom: 4%;
}


@media (min-width: 1500px) and (max-width: 2000px) {
   .uniLogoDiv{
      margin-top: 3.5% !important;
   }

   }

.gallarycontainer > a {
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 2em;
}

.gallarycontainer > a > img {
  width: 100%;
  height: 100%;
  object-fit: cover;
  box-shadow: 0 2px 16px var(--shadow);
}

.gallarycontainer {
  display: grid;
  grid-gap: 10px;
  grid-template-columns: repeat(auto-fit, minmax(140px, 1fr));
  grid-auto-rows: 120px;
  grid-auto-flow: dense;
}

.horizontal {
  grid-column: span 2;
}

.vertical {
  grid-row: span 2;
}

.big {
  grid-column: span 2;
  grid-row: span 2;
}
.alsoLikeUniversity img{
    width: 60%;
}

.alsoLikeUniversity .grid-item{
    
    justify-content: center;
}

.brochure{
     display: flex;
     flex-wrap: wrap;
     /*align-items: stretch;
     justify-content: space-between;*/
     grid-template-columns: repeat(3, 1fr);
}
.brochure .card{
     flex: 0 0 200px;
    margin: 20px;
    /* border: 1px solid #ccc; */
    box-shadow: 3px 3px 6px rgb(0 0 0 / 30%);
    text-align: center;
    min-height: 250px;
    padding: 0px 0px 0px 0px;
}
.brochure img{
     max-width: 100%;
}
.brochure .texts{
     padding: 5px; margin-bottom:10px;
}
.brochure .texts button{
     border: none; padding: 5px 15px;
     background: #815dd5; color: #fff;
     font-weight: 600;
     transition: all 0.3s ease-in;
}
.brochure .texts button:hover{
     background: #f6872c; color: #fff; cursor: pointer;
}

.myChart{
       width: 755px;
    max-width: -webkit-fill-available;
    display: block;
}

.sideMenu ul{
   padding: 6px;
    background: #ffffff;
    margin-left: 0px;
    margin-top: 5%;
}

.sideMenu .nav-pills > li {
    background: #8894f1d6;
     text-transform: capitalize; 
}
</style>
<div class="clearfix"></div>
<div id="page_loader">
   
    
      <section style="background-color: rgba(0, 0, 0, 0);background-repeat: no-repeat;background-image: url(https://hellouni.org/uploads/arizona_state/banner_1.jpg?t=1654580044); background-size: cover;background-position: center center;width: 100%;height: 250%;">
        
         <div class="container-fluid textContainer" style="height: 500px;">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="z-index: 5 !important; text-align: center;">
                  <h2 class="page-top-title" style="margin-top: 7%;color: #ffffff; font-weight:bold;font-size:50px;text-shadow: 2px 2px 6px #000000bd;"><span >&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $university[0]['name']; ?> &nbsp;&nbsp;&nbsp;&nbsp;</span></h2>
               </div>
               <div class="col-md-12" >
                        <form class="example" action="/action_page.php" style="margin:auto;max-width:650px">
                          <input type="text" placeholder="Search.." name="search2" style="border-radius: 15px 0px 0px 15px !important;">
                          <button type="submit" style="background-color: #F6881F; border-radius: 0px 15px 15px 0px !important; "><i class="fa fa-search"></i></button>
                        </form>
                        
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 uniLogoDiv noPadding"  style="margin-top: 10%;">
                
                     <div class="col-md-3 noPadding" style="background-color: white;border-top-right-radius: 150px;">
                        <img src="https://hellouni.org/uploads/uni_Binghamton/logo.jpg?t=1667285056" class="uniLogoDivimg" /> 
                     </div>
                     <div class="col-md-8" > 
                        <div class="flex col-md-12" > 
                              <div class="col-md-3 googleReview" >
                                 <span style="margin-right:5px; margin-bottom: 5px;  " class="Aq14fc" aria-hidden="true"> <img src="https://www.eduloans.org/images/smm_mail/search.png" alt="" style="width: 10%;"></span>
                                 <span class="Aq14fc" aria-hidden="true">4.9</span>
                                 <g-review-stars><span class="fTKmHE99XE4__star fTKmHE99XE4__star-s" aria-label="Rated 4.9 out of 5,"><span style="width:70px"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    </span>
                                 </g-review-stars>
                                 
                              </div>
                              <div class="col-md-3">
                              <button id="progButton" type="button" class="row btn btn-lg  callToAction" >Get Free Counselling</button>
                              </div>
                        </div>
                     </div>
                  
               </div>
             
            </div>
      </section>
      
      <section class="container">
         <div class="col-md-12">
            
           
               <div class="tab">
                 <button class="tablinks active" onclick="openTab(event, 'Overview')" id="defaultOpen">Overview</button>
                 <button class="tablinks" onclick="openTab(event, 'Programs')" id="ProgramsList">Programs</button>
                 <button class="tablinks" onclick="openTab(event, 'Ranking')">Ranking</button>
                 <button class="tablinks" onclick="openTab(event, 'Admission')">Admission</button>
                 <button class="tablinks" onclick="openTab(event, 'Scholarships')">Scholarships</button>
                 <button class="tablinks" onclick="openTab(event, 'FAQs')">FAQs</button>
                 <!-- <button class="tablinks btn btn-md tabButton" >Download Brochure</button> -->
               </div>
            
         </div>
         
         <div class="container-fluid overview">
            <div class="col-md-8 col-sm-12 col-xs-12">
               <div id="Overview" class="tabcontent col-md-12 " style="display: block;">
                  <div class="col-md-12 sectionClass noPadding" id="projectFacts">
                     <h1><?php echo $university[0]['name']; ?></h1>
                     <div class="projectFactsWrap noPadding">
                        <div class="item wow fadeInUpBig animated animated" data-number="8510" style="visibility: visible;display: inline-flex;">
                           <!-- <i class="fa fa-male" ></i> -->
                           <div><img src="<?php echo base_url();?>application/images/UniPage/TotalStudents.png" alt="Total Students"></div>
                           <div>
                              <p id="number1" class="number">
                                 <?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['total_students']; }else{ echo ""; } ?>
                              </p>
                              <p>Total Students</p>
                           </div>
                        </div>
                        <div class="item wow fadeInUpBig animated animated" data-number="" style="visibility: visible;display: inline-flex;">
                           <!-- <i class="fa fa-users purple"></i> -->
                           <div><img src="<?php echo base_url();?>application/images/UniPage/internationalstudent.png" alt="International Students"></div>
                           <div>
                              <p id="number2" class="number">
                                 <?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['international_students']; }else{ echo ""; } ?>
                              </p>
                              <p>International Students</p>
                           </div>
                        </div>
                        <div class="item wow fadeInUpBig animated animated" data-number="" style="visibility: visible;display: inline-flex;">
                           <!-- <i class="fa fa-star"></i> -->
                           <div><img src="<?php echo base_url();?>application/images/UniPage/TotalDegreePrograms.png" alt="Total Degree Programs"></div>
                           <div>
                              <p id="number3" class="number">
                                 <?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['total_degree_programs']; }else{ echo ""; } ?>+
                              </p>
                              <p>Total Degree Programs</p>
                           </div>
                        </div>
                        <div class="item wow fadeInUpBig animated animated" data-number="" style="visibility: visible;display: inline-flex;">
                           <!-- <i class="fa fa-star"></i> -->
                           <div><img src="<?php echo base_url();?>application/images/UniPage/dateofestablishment.png" alt="Establishment Year"></div>
                           <div>
                              <p id="number4" class="number">
                                 <?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['establishment_year']; }else{ echo ""; } ?>
                              </p>
                              <p>Establishment Year</p>
                           </div>
                        </div>
                        <!-- <div class="item wow fadeInUpBig animated animated" data-number="" style="visibility: visible;display: inline-flex;">
                          
                           <div><img src="<?php echo base_url();?>application/images/UniPage/Ranking.png" alt="Type Of University"></div>
                           <div>
                              <p id="number5" class="number">
                                 <?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['type']; }else{ echo ""; } ?>
                              </p>
                              <p>Type Of University</p>
                           </div>
                        </div> -->
                        <div class="item wow fadeInUpBig animated animated" data-number="" style="visibility: visible;display: inline-flex;">
                           <!-- <i class="fa fa-globe purple"></i> -->
                           <div><img src="<?php echo base_url();?>application/images/UniPage/Location.png" alt="Location"></div>
                           <div>
                              <p id="number6" class="number">
                                 <?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['type']; }else{ echo ""; } ?>  
                              </p>
                              <p>University Location</p>
                           </div>
                        </div>
                        <!-- <div class="item wow fadeInUpBig animated animated" data-number="" style="visibility: visible;display: inline-flex;">
                           
                           <div><img src="<?php echo base_url();?>application/images/UniPage/Acceptance_Rate.png" alt="Acceptance Rate"></div>
                           <div>
                              <p id="number7" class="number">
                                 <?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['location']; }else{ echo ""; } ?>
                              </p>
                              <p>Acceptance Rate</p>
                           </div>
                        </div> -->
                        <!-- <div class="item wow fadeInUpBig animated animated" data-number="" style="visibility: visible;display: inline-flex;">
                           
                           <div><img src="<?php echo base_url();?>application/images/UniPage/AverageEligibility.png" alt="Average Eligibility"></div>
                           <div>
                              <p id="number8" >
                                 PG - GPA <?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['average_eligibility']['pg_gpa']; }else{ echo ""; } ?>  |  GRE <?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['average_eligibility']['pg_gre']; }else{ echo ""; } ?><br>UG – GPA <?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['average_eligibility']['ug_gpa']; }else{ echo ""; } ?>  |  SAT <?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['average_eligibility']['ug_gre']; }else{ echo ""; } ?></p>
                              <p>Average Eligibility</p>
                           </div>
                        </div> -->
                        <div class="item wow fadeInUpBig animated animated" data-number="" style="visibility: visible;display: inline-flex;">
                           <!-- <i class="fa fa-briefcase"></i> -->
                           <div><img src="<?php echo base_url();?>application/images/UniPage/Internship.png" alt="Internships"></div>
                           <div>
                              <p id="number9" class="number">
                                 <?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['internships']; }else{ echo ""; } ?></p>
                              <p>Co-Op / CPT / Internships</p>
                           </div>
                        </div>
                        <div class="item wow fadeInUpBig animated animated" data-number="" style="visibility: visible;display: inline-flex;">
                           <!-- <i class="fa fa-briefcase"></i> -->
                           <div><img src="<?php echo base_url();?>application/images/UniPage/ApplicationFee.png" alt="Application Fee"></div>
                           <div>
                              <p id="number10" class="number">
                                 <?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['application_fee']; }else{ echo ""; } ?></p>
                              <p>Application Fee</p>
                           </div>
                        </div>
                        <div class="item wow fadeInUpBig animated animated" data-number="" style="visibility: visible;display: inline-flex;">
                           <!-- <i class="fa fa-graduation-cap purple"></i> -->
                           <div><img src="<?php echo base_url();?>application/images/UniPage/AverageTutionFee.png" alt="Average Tuition Fee (Int'l)"></div>
                           <div>
                              <p id="number11" class="number">
                                 <?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['application_fee']; }else{ echo ""; } ?></p>
                              <p>Average Tuition Fee (Int'l)</p>
                           </div>
                        </div>
                        <div class="item wow fadeInUpBig animated animated" data-number="" style="visibility: visible;display: inline-flex;">
                           <!-- <i class="fa fa-graduation-cap purple"></i> -->
                           <div><img src="<?php echo base_url();?>application/images/UniPage/CostofLiving.png" alt="Average Cost of Living"></div>
                           <div>
                              <p id="number11" class="number">
                                 <?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['average_cost_living']; }else{ echo ""; } ?></p>
                              <p>Average Cost of Living</p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-12 aboutus">
                     <h1>Overview of <?php echo $university[0]['name']; ?></h1>
                     <span style="color: #4a4949;font-size: 15px;"><?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['overview_paragraph_one']; }else{ echo ""; } ?>
                     </span>
                  </div>
                  <div class="col-md-12  moretext">
                     <div class="aboutus">
                        <h2>International Students at <?php echo $university[0]['name']; ?></h2>
                        <span style="color: #4a4949;font-size: 15px;">
                           <?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['read_more_international_students']; }else{ echo ""; } ?>
                        </span>
                     </div>
                     <div class="aboutus">
                        <h2>Student Life at <?php echo $university[0]['name']; ?></h2>
                        <span>
                           <?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['read_more_student_life']; }else{ echo ""; } ?>
                        </span>
                     </div>
                     <div class="aboutus">
                        <h2>Employment Figures at <?php echo $university[0]['name']; ?></h2>
                        <span style="color: #4a4949;font-size: 15px;">
                           <?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['read_more_employment_figures']; }else{ echo ""; } ?>
                        </span>
                     </div>
                     <div class="aboutus">
                        <h2>Colleges/Schools/Popular Programs at <?php echo $university[0]['name']; ?></h2>
                        <span>
                           <?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['read_more_programs']; }else{ echo ""; } ?>
                        </span>
                     </div>
                     <div class="aboutus">
                        <h2>Research at <?php echo $university[0]['name']; ?></h2>
                        <span>
                           <?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['read_more_research']; }else{ echo ""; } ?>
                        </span>
                     </div>
                     <div class="aboutus">
                        <h2>Alumni at <?php echo $university[0]['name']; ?></h2>
                        <span>
                           <?php if(isset($university_overview_info[0])){ echo $university_overview_info[0]['read_more_alumni']; }else{ echo ""; } ?>
                        </span>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <h1>Benefits of Joining Hello Uni  </h1>
                     <iframe width="100%" height="550px" src="https://www.youtube.com/embed/8wMs6gDg1-0">
                     </iframe>
                  </div>
                  <div class="col-md-12">
                     <div class="grid_container">
                        <div class="grid-item">
                           <div class="grid-item-img"> <img src="<?php echo base_url();?>application/images/UniPage/benefits/Registration_Changers.png" alt="athreya-podcast"> </div>
                           <div class="grid-item-text"> <h3>No Registration Charges</h3><p>Register for free and begin your journey.</p> </div>
                        </div>
                        <div class="grid-item">
                           <div class="grid-item-img"> <img src="<?php echo base_url();?>application/images/UniPage/benefits/comepare_universities.png" alt="athreya-ebook"> </div>
                           <div class="grid-item-text"> <h3>Campus Universities</h3><p>Compare the top universities under one roof.</p> </div>
                        </div>
                        <div class="grid-item">
                           <div class="grid-item-img"> <img src="<?php echo base_url();?>application/images/UniPage/benefits/Education_Loan.png" alt="athreya-youtube"> </div>
                           <div class="grid-item-text"> <h3>Education Loan</h3><p>Get the hassle free education loan on a single click</p> </div>
                        </div>
                        <div class="grid-item">
                           <div class="grid-item-img"> <img src="<?php echo base_url();?>application/images/UniPage/benefits/One_plateform_multiple_services.png" alt="athreya-blog"> </div>
                           <div class="grid-item-text"> <h3>One Platform-Multiple servies</h3><p>Access to our platfrom and avail multiple benefits.</p> </div>
                        </div>
                        <div class="grid-item">
                           <div class="grid-item-img"> <img src="<?php echo base_url();?>application/images/UniPage/benefits/accommodation.png" alt="athreya-stories"> </div>
                           <div class="grid-item-text"> <h3>Accommodation and Job placements</h3><p>Multiple Benefits.</p> </div>
                        </div>
                     </div>
                  </div>
               </div>

               <div id="Programs" class="tabcontent">
                  <h1 class="aboutus-title">Masters & Bachelors Degree Programs offered at <?php echo $university[0]['name']; ?></h1> 
                  <div id="programView" class="col-lg-12 col-md-12 font_color">
                     <div class="col-md-12">
                        <div >
                           <span>
                              <?php if(isset($university_info[0])){ echo $university_info[0]['tab_two']; }else{ echo ""; } ?>
                           </span>
                        </div>
                     </div>


                     <div class="col-sm-12">
                        <h1>POPULAR PROGRAMS</h1>
                        <div class="tab-content">

                           <?php
                              foreach($colleges as $index => $college)
                              {
                                ?>
                           <div role="tabpanel" class="tab-pane fade <?=($index == 0) ? 'in active': ''?>" id="college-<?=$index?>">
                              <?php
                                 foreach($courses_details as $course_detail)
                                 {
                                  if($course_detail['college'] == $college)
                                  {
                                    ?>
                              <div class="col-md-12">
                                 <div class="uni-boxPurple">
                                    <div class="flLt">
                                       <div class="uni-image">
                                          <a target="_blank" href="#"><img class="lazy" alt="" title="" align="center" width="120px" height="100px" src="<?=$courses['university_banner']?>" style="display: inline;"></a>
                                          <div class="uni-shrtlist-image"></div>
                                       </div>
                                    </div>
                                    <div class="uni-detail">
                                       <div class="uni-title">
                                          <div class="col-md-9">
                                             <h5 class="card-title" style="color: rgb(0, 75, 122)"><i class="fa fa-graduation-cap" style="font-size:15px;color:#374552">&nbsp;</i>Course : <b><?=ucwords($course_detail['name'])?></b></h5> </div>
                                          <div class="col-md-3">
                                             <h6 class="card-title">Compare : <b><input type="radio" name="compare_course" onclick="openComparePopup('<?=$course_detail['name']?>')"></b></h6> </div>
                                       </div>
                                       <div class="clearwidth">
                                          <div class="iconRowPurple">
                                             <div class="col-md-12 twoPXPadding">
                                                <div class="col-md-4 twoPXPadding">
                                                   <div class="col-md-2"><i class="fas fa-school iconCss">&nbsp;&nbsp;</i></div>
                                                   <div class="col-md-10 faPadding"><strong> Department Name <span>&nbsp; <?=ucwords($course_detail['department'])?></span></strong></div>
                                                </div>
                                                <div class="col-md-4 twoPXPadding">
                                                   <div class="col-md-2"><i class="far fa-clock iconCss">&nbsp;&nbsp;</i></div>
                                                   <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong> Duration <span>&nbsp; <?=$course_detail['course_duration']?> Months</span></strong></div>
                                                </div>
                                                <div class="col-md-4 twoPXPadding">
                                                   <div class="col-md-2"><i class="far fa-calendar-alt iconCss"></i></div>
                                                   <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong> Start Month <span>&nbsp; <?=$course_detail['start_months']?></span></strong></div>
                                                </div>
                                             </div>
                                             <div class="col-md-12 twoPXPadding" style="margin-top: 8px !important;    padding-bottom: 9px;border-bottom: 2px solid black;">
                                                <div class="col-md-4 twoPXPadding">
                                                   <div class="col-md-2"><i class="fas fa-money-check-alt iconCss"></i></div>
                                                   <div class="col-md-10 faPadding"><strong> Application Fees <span>&nbsp; <?=$course_detail['application_fee_course']?></span></strong></div>
                                                </div>
                                                <div class="col-md-4 twoPXPadding">
                                                   <div class="col-md-2"><i class="fa fa-money iconCss">&nbsp;&nbsp;</i></div>
                                                   <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong> Salary Expected<span>&nbsp; <?=$course_detail['eligibility_salary']?></span></strong></div>
                                                </div>
                                                <div class="col-md-4 twoPXPadding">
                                                   <div class="col-md-2"><i class="fas fa-university iconCss"></i></div>
                                                   <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong> Stay Back Option <span>&nbsp; <?=$course_detail['stay_back_option']?> / Months</span></strong></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row" style="margin-top: 10px !important">
                                          <div class="col-md-6 btn-col btn-brochure" style="text-align: left;margin-top: 12px;"> <a href="/course/coursemaster/courselist/<?=$course_detail['course_id']?>" class="button-style" target="_blank" style="background-color: #8c57c4;">View Detail</a> </div>
                                          <div class="col-md-4 font14 mrgtop float-right flRt"> 
                                             <i class="fa fa-eye">&nbsp;<?=$course_detail['course_view_count']?></i> | &nbsp; 
                                             <i class="fa fa-heart">&nbsp;<?=$course_detail['course_fav_count']?></i> | &nbsp; 
                                             <i class="fa fa-star">&nbsp;<?=$course_detail['course_likes_count']?></i> </div>
                                          <div class="col-md-2 flRt customInputs float-right mrgtop"> <i class="heart fa fa-heart-o"> </i> <span class="common-sprite"></span> </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <?php
                                 }
                                 }
                                 ?>
                           </div>
                           <?php
                              }
                              ?>


                           <!-- <div role="tabpanel" class="tab-pane fade" id="college-1">
                              <h1>two</h1>
                           </div>
                           <div role="tabpanel" class="tab-pane fade" id="college-2">
                           <h1>three</h1>
                           </div>
                           <div role="tabpanel" class="tab-pane fade" id="college-3">
                           <h1>Four</h1>
                           </div>
                           <div role="tabpanel" class="tab-pane fade" id="college-4">
                           <h1>Five</h1>
                           </div>
                           <div role="tabpanel" class="tab-pane fade" id="college-5">
                           <h1>Six</h1>
                           </div>
                           <div role="tabpanel" class="tab-pane fade" id="college-6">
                           <h1>Seven</h1>
                           </div> -->
                        </div>
                     </div>
                  </div>
               </div>
               <div id="Ranking" class="tabcontent ranking">
                  <h1><?php echo $university[0]['name']; ?> Ranking</h1>
                  <div class="col-md-12 ">
                     <span>
                        <?php if(isset($university_info[0])){ echo $university_info[0]['tab_three']; }else{ echo ""; } ?>
                     </span>
                  </div>
                  <div class="col-md-11">
                     <ul class="nav nav-tabs" style="list-style: none;margin-left: 0px">
                      <li class="active"><a data-toggle="tab" href="#tab-1">University Ranking</a></li>
                      <li><a data-toggle="tab" href="#tab-2">Course Ranking</a></li>
                     </ul>
                     <div class="tab-content">
                      <div id="tab-1" class="tab-pane fade in active">
                        <p><h3>#601-900</h3><h5>University Grade : ARWU (Shanghai Ranking)2020</h5></p>
                        <p><h3>#601-800</h3><h5>University Ranking : THE (Time Higher Education)2021</h5></p>
                        <p><h3>#298 - 500</h3><h5>National University Ranking : World Report 2021</h5></p>
                        <p><h3>#471</h3><h5>Global Universities : US News 2021</h5></p>
                      </div>
                      <div id="tab-2" class="tab-pane fade">
                        <p><h3>#601-700</h3><h5>University Ranking : ARWU (Shanghai Ranking)2020</h5></p>
                        <p><h3>#601-800</h3><h5>University Ranking : THE (Time Higher Education)2021</h5></p>
                        <p><h3>#298</h3><h5>National University Ranking : US News & World Report 2021</h5></p>
                        <p><h3>#471</h3><h5>Global Universities : US News & World Report 2021</h5></p>
                      </div>
                     </div>
                  </div>
                  <div class="col-md-12 noPadding">
                     <h3>Brochure</h3>
                     <div class="col-md-12 brochure noPadding">
                        <section class="card">
                           <img src="https://farm1.staticflickr.com/505/31980127730_ea81689413_m.jpg" alt="sample image">
                           <div class="texts">
                              <h2>Brochure 1</h2>
                              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry </p>
                              <button type="submit">Download Brochure</button>
                           </div>
                        </section>
                        <section class="card">
                           <img src="https://farm2.staticflickr.com/1871/30499973808_68ddd5960b_m.jpg" alt="sample image">
                           <div class="texts">
                              <h2>Brochure 2</h2>
                              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                              <button type="submit">Download Brochure</button>
                           </div>
                        </section>
                        <section class="card">
                           <img src="https://farm6.staticflickr.com/5730/22609622376_93c3560c8b_m.jpg" alt="sample image">
                           <div class="texts">
                              <h2>Brochure 3</h2>
                              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                              <button type="submit">Download Brochure</button>
                           </div>
                        </section>
                        <section class="card">
                           <img src="https://farm5.static.flickr.com/4094/4923591327_151f96ea71_m.jpg" alt="sample image">
                           <div class="texts">
                              <h2>Brochure 4</h2>
                              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                              <button type="submit">Download Brochure</button>
                           </div>
                        </section>
                        <section class="card">
                           <img src="https://farm6.static.flickr.com/5007/5250579551_b7f6b8bec6_m.jpg" alt="sample image">
                              <div class="texts">
                                 <h2>Brochure 5</h2>
                                 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                 <button type="submit">Download Brochure</button>
                             </div>
                        </section>
                        <section class="card">
                           <img src="https://farm6.static.flickr.com/5007/5250579551_b7f6b8bec6_m.jpg" alt="sample image">
                              <div class="texts">
                                 <h2>Brochure 6</h2>
                                 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                 <button type="submit">Download Brochure</button>
                             </div>
                        </section>
                     </div>
                  </div>
                  <div class="col-md-12 ">
                     <h3>Graph Representation</h3>
                     <canvas id="myChart" ></canvas>
                  </div>
               </div>
               <div id="Admission" class="tabcontent">
                  <h1>Table of Content</h1>
                  <div class="row">
                     <div id="top" class="col-md-12">
                        <ol class="nav">
                          <li ><a class="nav-link" href="#section1">Admissions at <?php echo $university[0]['name']; ?></a></li>
                          <li class="nav-item"><a class="nav-link" href="#section2">Application Fees at <?php echo $university[0]['name']; ?></a></li>
                          <li class="nav-item"><a class="nav-link" href="#section3">SAT/ACT for admission to <?php echo $university[0]['name']; ?></a></li>
                          <li class="nav-item"><a class="nav-link" href="#section4">GRE/GMAT for admission to <?php echo $university[0]['name']; ?></a></li>
                          <li ><a class="nav-link" href="#section5">Proof of English Language Proficiency for Admission to <?php echo $university[0]['name']; ?></a></li>
                          <li class="nav-item"><a class="nav-link" href="#section6">Application Requirements at <?php echo $university[0]['name']; ?></a></li>
                          <li class="nav-item"><a class="nav-link" href="#section7">Admission Timeline and Deadlines for <?php echo $university[0]['name']; ?></a></li>
                          <li class="nav-item"><a class="nav-link" href="#section8">How to apply, i.e. the Application Process and Financial Assistance for <?php echo $university[0]['name']; ?> ?</a></li>
                          <li class="nav-item"><a class="nav-link" href="#section9">I-20 for International Students for admission to <?php echo $university[0]['name']; ?></a></li>
                          <li class="nav-item"><a class="nav-link" href="#section10"><?php echo $university[0]['name']; ?> Tuition Fees for International Students</a></li>
                        </ol> 
                     </div>
                     <div class="col-md-12">
                        <div id="section1" class="block">
                           <h5>Admissions at <?php echo $university[0]['name']; ?></h5>
                           <span><?php if(isset($university_admission_info[0])){ echo $university_admission_info[0]['a_one']; }else{ echo ""; } ?></span>
                           <a class="custom-btn btn-3" href="#top"><span>Back to Top</span></a>
                        </div>
                        <div id="section2" class="block">
                           <h5>Application Fees at <?php echo $university[0]['name']; ?></h5>
                           <span><?php if(isset($university_admission_info[0])){ echo $university_admission_info[0]['a_two']; }else{ echo ""; } ?></span>
                           <a class="custom-btn btn-3" href="#top"><span>Back to Top</span></a>
                        </div>
                        <div id="section3" class="block">
                           <h5>SAT/ACT for admission to <?php echo $university[0]['name']; ?></h5>
                           <span><?php if(isset($university_admission_info[0])){ echo $university_admission_info[0]['a_three']; }else{ echo ""; } ?></span>
                           <a class="custom-btn btn-3" href="#top"><span>Back to Top</span></a>
                        </div>
                        <div id="section4" class="block">
                           <h5>GRE/GMAT for admission to <?php echo $university[0]['name']; ?></h5>
                           <span><?php if(isset($university_admission_info[0])){ echo $university_admission_info[0]['a_four']; }else{ echo ""; } ?></span>
                           <a class="custom-btn btn-3" href="#top"><span>Back to Top</span></a>
                        </div>
                        <div id="section5" class="block">
                           <h5>Proof of English Language Proficiency for Admission to <?php echo $university[0]['name']; ?></h5>
                           <span><?php if(isset($university_admission_info[0])){ echo $university_admission_info[0]['a_five']; }else{ echo ""; } ?></span>
                           <h6>TOEFL (TEST OF ENGLISH AS A FOREIGN LANGUAGE) score required at <?php echo $university[0]['name']; ?></h6>
                           <span><?php if(isset($university_admission_info[0])){ echo $university_admission_info[0]['a_five_a']; }else{ echo ""; } ?></span>
                           <h6>IELTS (INTERNATIONAL ENGLISH LANGUAGE TESTING SERVICE) score required at <?php echo $university[0]['name']; ?></h6>
                           <span><?php if(isset($university_admission_info[0])){ echo $university_admission_info[0]['a_five_b']; }else{ echo ""; } ?></span>
                             <h6>DUOLINGO ENGLISH TEST score required at <?php echo $university[0]['name']; ?></h6>
                           <span><?php if(isset($university_admission_info[0])){ echo $university_admission_info[0]['a_five_c']; }else{ echo ""; } ?></span>
                             <h6>Pearson PTE score required at <?php echo $university[0]['name']; ?></h6>
                           <span><?php if(isset($university_admission_info[0])){ echo $university_admission_info[0]['a_five_d']; }else{ echo ""; } ?></span>
                           <a class="custom-btn btn-3" href="#top"><span>Back to Top</span></a>
                        </div>
                        <div id="section6" class="block">
                           <h5>Application Requirements at <?php echo $university[0]['name']; ?> ?</h5>
                           <span><?php if(isset($university_admission_info[0])){ echo $university_admission_info[0]['a_six']; }else{ echo ""; } ?></span>
                           <h6>What does a typical application to <?php echo $university[0]['name']; ?> contain?</h6>
                           <span><?php if(isset($university_admission_info[0])){ echo $university_admission_info[0]['a_six_a']; }else{ echo ""; } ?></span>
                           <a class="custom-btn btn-3" href="#top"><span>Back to Top</span></a>
                        </div> 
                        <div id="section7" class="block">
                           <h5>Admission Timeline and Deadlines for <?php echo $university[0]['name']; ?> ?</h5>
                           <span><?php if(isset($university_admission_info[0])){ echo $university_admission_info[0]['a_seven']; }else{ echo ""; } ?></span>
                           <a class="custom-btn btn-3" href="#top"><span>Back to Top</span></a>
                        </div> 
                        <div id="section8" class="block">
                           <h5>How to Apply to <?php echo $university[0]['name']; ?> ? Application Process & Financial Assistance</h5>
                           <span><?php if(isset($university_admission_info[0])){ echo $university_admission_info[0]['a_eight']; }else{ echo ""; } ?></span>
                           <a class="custom-btn btn-3" href="#top"><span>Back to Top</span></a>
                        </div> 
                        <div id="section9" class="block">
                           <h5>I-20 for International Students for admission to <?php echo $university[0]['name']; ?> </h5>
                           <span><?php if(isset($university_admission_info[0])){ echo $university_admission_info[0]['a_nine']; }else{ echo ""; } ?></span>
                           <a class="custom-btn btn-3" href="#top"><span>Back to Top</span></a>
                        </div> 
                        <div id="section10" class="block">
                           <h5><?php echo $university[0]['name']; ?> Tuition Fees for International Students</h5>
                           <span><?php if(isset($university_admission_info[0])){ echo $university_admission_info[0]['a_ten']; }else{ echo ""; } ?></span>
                           <a class="custom-btn btn-3" href="#top"><span>Back to Top</span></a>
                        </div> 
                     </div>
                  </div>
               </div>
               <div id="Scholarships" class="tabcontent">
                  <h1>Scholarships</h1>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="aboutus">
                           <h2><?php echo $university[0]['name']; ?> Financial Assistance to International Students</h2>
                           <span><?php if(isset($university_scholarships_financial_info[0])){ echo $university_scholarships_financial_info[0]['financial_assistance']; }else{ echo ""; } ?></span>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="aboutus">
                           <h2>Financial Aid for <?php echo $university[0]['name']; ?></h2>
                           <span><?php if(isset($university_scholarships_financial_info[0])){ echo $university_scholarships_financial_info[0]['financial_aid']; }else{ echo ""; } ?>
                           </span>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="aboutus">
                           <h2>Accommodations offered for International Students at <?php echo $university[0]['name']; ?></h2>
                           <span>
                              <?php if(isset($university_scholarships_financial_info[0])){ echo $university_scholarships_financial_info[0]['accommodations_offered']; }else{ echo ""; } ?>
                           </span>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="aboutus">
                           <h2>On-Campus Accommodation at <?php echo $university[0]['name']; ?></h2>
                           <span>
                              <?php if(isset($university_scholarships_financial_info[0])){ echo $university_scholarships_financial_info[0]['on_campus']; }else{ echo ""; } ?>
                           </span>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="aboutus">
                           <h2>Off-Campus Accommodation <?php echo $university[0]['name']; ?></h2>
                           <span>
                              <?php if(isset($university_scholarships_financial_info[0])){ echo $university_scholarships_financial_info[0]['off_campus']; }else{ echo ""; } ?>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
               <div id="FAQs" class="tabcontent faq" style="margin-top: 20px;">
                  <div class="accordion-content">
                     <h1>FAQ</h1>

                     <div class="accordion-item">
                        <header class="item-header">
                           <h2 class="item-question">
                             How should I apply to <?php echo $university[0]['name']; ?> ?
                           </h2>
                           <div class="item-icon">
                             <i class='bx bx-chevron-down'></i>
                           </div>
                        </header>
                        <div class="item-content">
                           <span class="item-answer">
                             <?php if(isset($university_faq_info[0])){ echo $university_faq_info[0]['a_one']; }else{ echo ""; } ?>
                           </span>
                        </div>
                     </div>
                     <div class="accordion-item">
                        <header class="item-header">
                           <h2 class="item-question">
                             What is the application deadline for <?php echo $university[0]['name']; ?> ?
                           </h2>
                           <div class="item-icon">
                             <i class='bx bx-chevron-down'></i>
                           </div>
                        </header>
                        <div class="item-content">
                           <span class="item-answer"><?php if(isset($university_faq_info[0])){ echo $university_faq_info[0]['a_two']; }else{ echo ""; } ?></span>
                        </div>
                     </div>
                     <div class="accordion-item">
                        <header class="item-header">
                           <h2 class="item-question">What are the entry or eligibility criteria at <?php echo $university[0]['name']; ?> ?</h2>
                           <div class="item-icon">
                             <i class='bx bx-chevron-down'></i>
                           </div>
                        </header>
                        <div class="item-content">
                           <span class="item-answer">
                              <?php if(isset($university_faq_info[0])){ echo $university_faq_info[0]['a_three']; }else{ echo ""; } ?>
                           </span>
                        </div>
                     </div>
                     <div class="accordion-item">
                        <header class="item-header">
                           <h2 class="item-question">
                             What documentation is required for applying to <?php echo $university[0]['name']; ?> ?
                           </h2>
                           <div class="item-icon">
                             <i class='bx bx-chevron-down'></i>
                           </div>
                        </header>
                        <div class="item-content">
                           <span class="item-answer">
                             <?php if(isset($university_faq_info[0])){ echo $university_faq_info[0]['a_four']; }else{ echo ""; } ?>
                           </span>
                        </div>
                     </div>
                     <div class="accordion-item">
                         <header class="item-header">
                           <h2 class="item-question">
                             How diverse is the student body at <?php echo $university[0]['name']; ?> ?
                           </h2>
                           <div class="item-icon">
                             <i class='bx bx-chevron-down'></i>
                           </div>
                         </header>
                         <div class="item-content">
                           <span class="item-answer">
                             <?php if(isset($university_faq_info[0])){ echo $university_faq_info[0]['a_five']; }else{ echo ""; } ?></span>
                         </div>
                     </div>
                     <div class="accordion-item">
                         <header class="item-header">
                           <h2 class="item-question">
                             What is the acceptance rate at <?php echo $university[0]['name']; ?> ?
                           </h2>
                           <div class="item-icon">
                             <i class='bx bx-chevron-down'></i>
                           </div>
                         </header>
                         <div class="item-content">
                           <span class="item-answer">
                             <?php if(isset($university_faq_info[0])){ echo $university_faq_info[0]['a_six']; }else{ echo ""; } ?></span>
                         </div>
                     </div>
                     <div class="accordion-item">
                        <header class="item-header">
                           <h2 class="item-question">
                             What is the Ranking for <?php echo $university[0]['name']; ?> ?
                           </h2>
                           <div class="item-icon">
                             <i class='bx bx-chevron-down'></i>
                           </div>
                        </header>
                        <div class="item-content">
                           <span class="item-answer">
                             <?php if(isset($university_faq_info[0])){ echo $university_faq_info[0]['a_seven']; }else{ echo ""; } ?>
                           </span>
                        </div>
                     </div>
                     <!-- <div class="accordion-item">
                         <header class="item-header">
                           <h4 class="item-question">
                             What documentation is required for applying to <?php echo $university[0]['name']; ?> ?
                           </h4>
                           <div class="item-icon">
                             <i class='bx bx-chevron-down'></i>
                           </div>
                         </header>
                         <div class="item-content">
                           <span class="item-answer">
                             <?php if(isset($university_faq_info[0])){ echo $university_faq_info[0]['a_eight']; }else{ echo ""; } ?></span>
                         </div>
                     </div>
                     <div class="accordion-item">
                         <header class="item-header">
                           <h4 class="item-question">
                             What documentation is required for applying to <?php echo $university[0]['name']; ?> ?
                           </h4>
                           <div class="item-icon">
                             <i class='bx bx-chevron-down'></i>
                           </div>
                         </header>
                         <div class="item-content">
                           <span class="item-answer">
                             <?php if(isset($university_faq_info[0])){ echo $university_faq_info[0]['a_nine']; }else{ echo ""; } ?></span>
                         </div>
                     </div>
                     <div class="accordion-item">
                         <header class="item-header">
                           <h4 class="item-question">
                             What documentation is required for applying to <?php echo $university[0]['name']; ?> ?
                           </h4>
                           <div class="item-icon">
                             <i class='bx bx-chevron-down'></i>
                           </div>
                         </header>
                         <div class="item-content">
                           <span class="item-answer">
                             <?php if(isset($university_faq_info[0])){ echo $university_faq_info[0]['a_ten']; }else{ echo ""; } ?></span>
                         </div>
                     </div> -->
                  </div>
               </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
               <div class="col-md-12 noPadding">
                  <h1>Campus Tour</h1>
                  <iframe width="100%" height="320px" src="https://www.youtube.com/embed/8wMs6gDg1-0">
                  </iframe>
               </div>
               <div class="col-md-12 sideMenu" style="padding: 0px;display: none;">
                  <!-- <ul id="nav-tabs-wrapper" class="nav nav-tabs nav-pills nav-stacked well" style="padding: 5px; background: #ffffff;">
                     <?php
                        foreach($colleges as $index => $college)
                        {
                        ?>
                           <li class="<?=($index == 0) ? 'active': ''?>" style="display: block; padding: 0px;background: #ffffff;">
                              <a href="#" data-toggle="tab" data-target="#college-<?=$index?>" aria-expanded="true" aria-controls="vtab1" style="color: #f6882c; margin: 0px; padding: 5px;box-shadow: 1px 1px 2px #bbb8b8;text-transform: none;letter-spacing: 0.3px;    font-size: 16px;">
                                 <?=$college?>
                              </a>
                           </li>
                        <?php
                        }
                        ?>
                  </ul> -->
                  <h1>Checkout List of Collegs / Courses</h1>
                  <ul id="nav-tabs-wrapper" class="nav nav-tabs nav-pills nav-stacked well" style="padding: 5px; background: #ffffff;">
                     <?php
                        foreach($colleges as $index => $college)
                        {
                          ?>
                     <li class="<?=($index == 0) ? 'active': ''?>"><a href="#" data-toggle="tab" data-target="#college-<?=$index?>" aria-expanded="true" aria-controls="vtab1" ><?=$college?>test</a></li>
                     <?php
                        }
                        ?>
                        <!-- <li class="active" ><a href="#" data-toggle="tab" data-target="#college-0" aria-expanded="true" aria-controls="vtab1" >College of Liberal Arts and Sciences</a></li>
                        <li class="" ><a href="#" data-toggle="tab" data-target="#college-1" aria-expanded="true" aria-controls="vtab1" >W. P. Carey School of Business</a></li>
                        <li class="" ><a href="#" data-toggle="tab" data-target="#college-2" aria-expanded="true" aria-controls="vtab1" >Herberger Institute for Design and the Arts</a></li>
                        <li class="" ><a href="#" data-toggle="tab" data-target="#college-3" aria-expanded="true" aria-controls="vtab1">Ira A. Fulton Schools of Engineering</a></li> -->
                      
                     </ul>
               </div>

               <div class="col-md-12 formRegister noPadding">
                  <h1>University Session Functionality</h1>
                  <?php
                  $currentTimeZone = date("Y-m-d H:i:s");
                     if(isset($alreadyConnected) && $alreadyConnected)
                     {
                       if($alreadyConnected->slot_1 || $alreadyConnected->slot_2 || $alreadyConnected->slot_3)
                       {
                                 if(!$alreadyConnected->confirmed_slot)
                                 {
                                     ?>
                      <div class="col-md-12">
                        <form class="form-horizontal" method="post" action="/university/details/slotconfirm">
                          <div class="col-md-5" style="text-align: right;margin-top: 8px;">
                              <h4 style="color:#fff;">Please select any one slot :</h4>
                            </div>
                          <div class="col-md-4" style="display: inline-flex;margin-top: 8px;">
                              <?php
                              if($alreadyConnected->slot_1)
                              {
                                  ?>
                           <div style="color:#fff; padding:5px;"><input type="radio" name="confirmed_slot" value="<?=$alreadyConnected->slot_1?>" <?=($alreadyConnected->confirmed_slot == $alreadyConnected->slot_1)? 'checked' : ''?>>&nbsp;&nbsp;<b>Slot 1 :</b> &nbsp;&nbsp;<?=$alreadyConnected->slot_1_show?></div>
                           <?php
                              }
                              if($alreadyConnected->slot_2)
                              {
                                  ?>
                           <div style="color:#fff; padding:5px;"><input type="radio" name="confirmed_slot" value="<?=$alreadyConnected->slot_2?>" <?=($alreadyConnected->confirmed_slot == $alreadyConnected->slot_2)? 'checked' : ''?>>&nbsp;&nbsp;<b>Slot 2 :</b> &nbsp;&nbsp;<?=$alreadyConnected->slot_2_show?></div>
                           <?php
                              }
                              if($alreadyConnected->slot_3)
                              {
                                  ?>
                           <div style="color:#fff; padding:5px;"><input type="radio" name="confirmed_slot" value="<?=$alreadyConnected->slot_3?>" <?=($alreadyConnected->confirmed_slot == $alreadyConnected->slot_3)? 'checked' : ''?>>&nbsp;&nbsp;<b>Slot 3 :</b> &nbsp;&nbsp;<?=$alreadyConnected->slot_3_show?></div>
                           <?php
                              }
                              ?>
                          </div>
                          <div class="col-md-3">
                           <input type="hidden" name="slot_id" value="<?=$alreadyConnected->id?>">
                           <input type="hidden" name="university_id" value="<?=$id?>">
                           <input type="hidden" name="university_login_id" value="<?=$login_id?>">
                           <input type="hidden" name="university_website" value="<?=$website?>">
                           <input type="hidden" name="university_name" value="<?=$name?>">
                           <input type="hidden" name="university_email" value="<?=$email?>">
                           <input type="hidden" name="slug" value="<?=$slug?>">
                           <div style="text-align:left;"><input type="submit" class="btn btn-lg btn-primary" name="submit" style="margin:10px auto;" value="Submit"></div>
                          </div>


                        </form>

                      </div>

                  <?php
                     }
                     else
                     {
                         if($applied)
                         {
                             ?>
                  <div style="text-align:center;"><input type="button" class="btn btn-lg btn-primary" name="apply" style="margin:10px auto; background-color:#F6881F" value="Applied" id="apply_button" disabled></div>
                  <?php
                     }
                     else if(isset($tokbox_url) && $tokbox_url)
                     {
                         if($show_url)
                         {
                             ?>
                  <div style="margin-top: 18px; font-weight: bold;"><a href="<?=$tokbox_url?>" target="_blank">Click Here For Video Conferene</a></div>
                  <?php
                     }
                     else if($alreadyConnected->status == 'Attained')
                     {
                         ?>
                  <!--h5 style="color:#fff;">Book one to one sessions Again:</h5-->
                  <form class="form-horizontal" method="post" action="/university/details/connect">
                     <div class="form-group" style="text-align:center;">
                        <input type="submit" class="btn btn-lg btn-primary" name="submit" style="margin:10px auto;" value="Connect Again">
                     </div>
                     <input type="hidden" name="university_id" value="<?=$id?>">
                     <input type="hidden" name="university_login_id" value="<?=$login_id?>">
                     <input type="hidden" name="slug" value="<?=$slug?>">
                  </form>
                  <form method="get" action="/university/details/application">
                     <div style="text-align:center;">
                        <input type="submit" class="btn btn-lg btn-primary" name="submit" style="margin:10px auto;" value="Start Documentation">
                     </div>
                     <input type="hidden" name="uid" value="<?=$id?>">
                  </form>
                  
                  <?php
                     }
                     else if($alreadyConnected->status == 'Scheduled')
                     {
                         ?>
                  <h4 style="color:black; text-align:center;"><?=$this->session->flashdata('flash_message') ? $this->session->flashdata('flash_message') : '<p style="font-size:11px;">Your request is in process1.<br/>Please check your mail for further information.</p>'?></h4>
                  <?php
                     }
                     }
                     else
                     {
                     ?>
                  <h4 style="color:black; text-align:center;"><?=$this->session->flashdata('flash_message') ? $this->session->flashdata('flash_message') : '<p style="font-size:11px;">Your request is in process2.<br/>Please check your mail for further information.</p>'?></h4>
                  <?php
                     }
                       }
                     }
                     else
                     {
                     ?>
                  <h4 style="color:black; text-align:center;"><?=$this->session->flashdata('flash_message') ? $this->session->flashdata('flash_message') : '<p style="font-size:11px;">Your request is in process3.<br/>Please check your mail for further information.</p>'?></h4>
                  <?php
                     }
                     }
                     else
                     {
                      ?>
                    <div class="col-md-12">
                        <div class="col-md-8 col-xs-8" style="text-align: right;margin-top: 8px;">
                          <h4 class="hidden-xs visible-md visible-lg" style="color:#fff;"><?php echo $name;?> &nbsp; Book one to one sessions :</h4>
                          <h5 class="visible-xs hidden-md hidden-lg" style="color:#fff;"><?php echo $name;?> &nbsp; Book one to one sessions :</h5>
                        </div>
                        <div class="col-md-3 col-xs-3" style="text-align: left;">
                          <form class="form-horizontal" method="post" action="/university/details/connect" id="connect-university">
                             <div class="form-group" style="text-align:left;">
                                <?php
                                   if($userLoggedIn)
                                   {
                                       ?>
                                <input type="button" class="btn btn-lg btn-primary hidden-xs visible-md visible-lg" name="connect" style="margin:15px 10px 0px 10px;" value="Connect" onclick="openConsent()">
                                <input type="button" class="btn btn-md btn-primary visible-xs hidden-md hidden-lg" name="connect" style="margin:30px 10px 0px 10px;" value="Connect" onclick="openConsent()">



                                <?php
                                   }
                                   else
                                   {
                                       ?>
                                <input type="submit" class="btn btn-lg btn-primary hidden-xs visible-md visible-lg" name="submit" style="margin:15px 10px 0px 10px;" value="Connect">
                                <input type="submit" class="btn btn-md btn-primary visible-xs hidden-md hidden-lg" name="submit" style="margin:30px 10px 0px 10px;" value="Connect">
                                <?php
                                   }
                                   ?>
                             </div>
                             <input type="hidden" name="university_id" value="<?=$id?>">
                             <input type="hidden" name="university_login_id" value="<?=$login_id?>">
                             <input type="hidden" name="slug" value="<?=$slug?>">
                             <input type="hidden" name="student_signature" value="" id="student_signature">
                             <input type="hidden" name="student_name" value="" id="student_name">
                             <input type="hidden" name="student_email" value="" id="student_email">
                             <input type="hidden" name="student_phone" value="" id="student_phone">
                             <input type="hidden" name="student_dob" value="" id="student_dob">
                             <input type="hidden" name="student_country" value="" id="student_country">
                             <input type="hidden" name="student_city" value="" id="student_city">
                             <input type="hidden" name="student_passport" value="" id="student_passport">
                             <input type="hidden" name="student_intake_month" value="" id="student_intake_month">
                             <input type="hidden" name="student_intake_year" value="" id="student_intake_year">
                             <input type="hidden" name="student_mainstream" value="" id="student_mainstream">
                             <input type="hidden" name="student_subcourse" value="" id="student_subcourse">
                             <input type="hidden" name="student_level" value="" id="student_level">
                             <input type="hidden" name="student_hear_about_us" value="" id="student_hear_about_us">
                          </form>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="col-md-8 col-xs-8" style="text-align: right;margin-top: 8px;">
                          <h4 class="hidden-xs visible-md visible-lg" style="color:#fff;"><?php echo $name;?> &nbsp; Apply :</h4>
                          <h5 class="visible-xs hidden-md hidden-lg" style="color:#fff;"><?php echo $name;?> &nbsp; Apply :</h5>
                        </div>
                        <div class="col-md-3 col-xs-3" style="text-align: left;">
                          <form class="form-horizontal" method="post" action="/university/details/connect" id="connect-university">
                             <div class="form-group" style="text-align:left;">
                                <?php
                                   if($userLoggedIn)
                                   {
                                       ?>
                                <input type="button" class="btn btn-lg btn-primary" onclick="courseModal(7, 223, '<?php echo $name;?>')" value="Apply" />

                                <?php
                                   }
                                   else
                                   {
                                       ?>
                                <input type="submit" class="btn btn-lg btn-primary hidden-xs visible-md visible-lg" name="submit" style="margin:15px 10px 0px 10px;" value="Connect">
                                <input type="submit" class="btn btn-md btn-primary visible-xs hidden-md hidden-lg" name="submit" style="margin:30px 10px 0px 10px;" value="Connect">
                                <?php
                                   }
                                   ?>
                             </div>

                          </form>
                        </div>
                    </div>

                  <?php
                     }
                     ?>

                     
               </div>
               
               <div class="col-md-12 formRegister noPadding">
                  <h1>Connect With Addmission Counsellor</h1>
                  <form action="/action_page.php">
                     <label for="fname">First Name</label>
                     <input type="text" id="fname" name="firstname" placeholder="Your name..">
                     <label for="lname">Last Name</label>
                     <input type="text" id="lname" name="lastname" placeholder="Your last name..">
                     <label for="country">Country</label>
                     <select id="country" name="country">
                        <option value="australia">India</option>
                        <option value="canada">Canada</option>
                        <option value="usa">USA</option>
                     </select>
                     <input type="submit" value="Submit"> 
                  </form>
               </div>
               <div class="col-md-12">
                  <h1>Gallary</h1>
                  <div class="gallarycontainer">
                     <a href="https://source.unsplash.com/600x600/?sig=1" data-lightbox="homePortfolio" data-gallery="gallery">
                       <img src="https://source.unsplash.com/600x600/?sig=1"/>
                     </a>
                     
                     <a href="https://source.unsplash.com/600x800/?sig=12" data-lightbox="homePortfolio" data-gallery="gallery" class="vertical">
                       <img src="https://source.unsplash.com/600x800/?sig=12"/>
                     </a>
                     
                     <a href="https://source.unsplash.com/800x600/?sig=71" data-lightbox="homePortfolio" data-gallery="gallery" class="horizontal">
                       <img src="https://source.unsplash.com/800x600/?sig=71"/>
                     </a>
                     
                     <a href="https://source.unsplash.com/600x600/?sig=40" data-lightbox="homePortfolio" data-gallery="gallery">
                       <img src="https://source.unsplash.com/600x600/?sig=40"/>
                     </a>
                     
                     <a href="https://source.unsplash.com/600x600/?sig=32" data-lightbox="homePortfolio" data-gallery="gallery">
                       <img src="https://source.unsplash.com/600x600/?sig=32"/>
                     </a>
                     
                     <a href="https://source.unsplash.com/800x800/?sig=7" data-lightbox="homePortfolio" class="big" data-gallery="gallery">
                       <img src="https://source.unsplash.com/800x800/?sig=7"/>
                     </a>
                     
                     <a href="https://source.unsplash.com/600x600/?sig=111" data-lightbox="homePortfolio" data-gallery="gallery">
                       <img src="https://source.unsplash.com/600x600/?sig=111"/>
                     </a>
                     
                     <a href="https://source.unsplash.com/600x800/?sig=94" data-lightbox="homePortfolio" class="vertical" data-gallery="gallery">
                       <img src="https://source.unsplash.com/600x800/?sig=94"/>
                     </a>


                     <a href="https://source.unsplash.com/600x800/?sig=94" data-lightbox="homePortfolio" class="vertical" data-gallery="gallery">
                       <img src="https://source.unsplash.com/600x800/?sig=94"/>
                     </a>
                  </div>
               </div>
               <div class="col-md-12 alsoLikeUniversity  noPadding">
                  <h1>You May Also Like</h1>
                     <div class="grid_container">
                        <a class="grid-item" href="https://hellouni.org/university-of-south-florida" >
                          <img src="https://hellouni.org/application/images/UniLogo/University_Of_South_Florida_Logo.png?t=1667285019"/>
                        </a>
                        
                        <a class="grid-item" href="https://hellouni.org/university-of-south-florida" >
                          <img src="https://hellouni.org/application/images/UniLogo/University_Of_South_Florida_Logo.png?t=1667285019"/>
                        </a>

                        <a class="grid-item" href="https://hellouni.org/university-of-south-florida" >
                          <img src="https://hellouni.org/application/images/UniLogo/University_Of_South_Florida_Logo.png?t=1667285019"/>
                        </a>

                        <a class="grid-item" href="https://hellouni.org/university-of-south-florida" >
                          <img src="https://hellouni.org/application/images/UniLogo/University_Of_South_Florida_Logo.png?t=1667285019"/>
                        </a>
                     </div>
               </div>
            </div>

        </div>
         
      </section>
      
        
      
   </div>
   <!-- The Modal -->
   <div id="myModal" class="modal">
      <!-- Modal content -->
      <div class="row modal-content">
         <span class="close">&times;</span>
         <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="compare-university-modal"></div>
      </div>
   </div>
   <style type="text/css">
      .mrg{
      margin: 10px 5px;
      }
      .ht30{
      height: 30px;
      }
   </style>
   <div id="consent-modal" class="modal">

    <!-- Modal content -->
    <div class="row modal-content">
      <span class="close" id="consent-close">&times;</span>
      <form class="col-md-12 col-lg-12 col-sm-12 col-xs-12 form-group">
          <div id="fieldsBox">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 mrg">
                  <div class="col-md-3">
                     <input type="text" name="user_name" id="user_name" class="form-control ht30" placeholder="Your Name" value="<?=isset($user_master) && $user_master['name'] ? $user_master['name'] : ''?>">
                  </div>
                  <div class="col-md-3">
                     <input type="text" name="user_email" id="user_email" class="form-control ht30" value="<?=isset($user_master) && $user_master['email'] ? $user_master['email'] : 'Your Email'?>" disabled>
                  </div>
                  <div class="col-md-3">
                  <input type="text" name="user_phone" id="user_phone" class="form-control ht30" value="<?=isset($user_master) && $user_master['phone'] ? $user_master['phone'] : 'Your Phone'?>" disabled>
                </div>
                  <div class="col-md-3">
                  <input type="text" name="user_passport" id="user_passport" class="form-control ht30" value="<?=isset($student_details) && $student_details['passport_number'] ? $student_details['passport_number'] : ''?>" placeholder="Your Passport Number">
                </div>
          </div>
           <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 mrg">
                <div class="col-md-4">
                  <input type="date" name="user_dob" id="user_dob" class="form-control ht30" placeholder="Your DOB" value="<?=isset($student_details) && $student_details['dob'] ? $student_details['dob'] : ''?>">
                </div>
                  <div class="col-md-4">
                  <select class="form-control ht30" id="user_country" name="user_country">
                          <option value="">Select Country</option>
                          <?php
                          foreach($locationcountries as $location)
                          {
                              ?>
                              <option value="<?=$location->id . '|' . $location->name?>" <?=(isset($student_details) && $student_details['country'] && $student_details['country'] == $location->id) ? 'selected' : ''?>><?=$location->name?></option>
                              <?php
                          }
                          ?>
                      </select>
                </div>
                  <div class="col-md-4">
                  <select class="form-control ht30" id="user_city" name="user_city">
                          <option value="">Select City</option>
                          <?php
                          foreach($cities as $city)
                          {
                              ?>
                              <option value="<?=$city->id . '|' . $city->city_name?>" <?=(isset($student_details) && $student_details['city'] && $student_details['city'] == $city->id) ? 'selected' : ''?>><?=$city->city_name?></option>
                              <?php
                          }
                          ?>
                      </select>
                </div>
           </div>
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 mrg">
                <div class="col-md-4">
                    <select class="form-control ht30" id="user_mainstream" name="user_mainstream">
                        <option value="">Select Main Stream</option>
                        <?php
                        foreach($mainstreams as $mainstream)
                        {
                            ?>
                            <option value="<?=$mainstream->id . '|' . $mainstream->display_name?>" <?=(isset($lead_master) && $lead_master['mainstream'] && $lead_master['mainstream'] == $mainstream->id) ? 'selected' : ''?>><?=$mainstream->display_name?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-4">
                    <select class="form-control ht30" id="user_subcourse" name="user_subcourse">
                        <?php
                        if($subCourses)
                        {
                            ?>
                            <option value="">Select Subcourse</option>
                            <?php
                            foreach($subCourses as $subCourse)
                            {
                                ?>
                                <option value="<?=$subCourse->id . '|' . $subCourse->display_name?>" <?=(isset($lead_master) && $lead_master['courses'] && $lead_master['courses'] == $subCourse->id) ? 'selected' : ''?>><?=$subCourse->display_name?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-4">
                    <select class="form-control ht30" id="user_level" name="user_level">
                        <option value="">Select Level</option>
                        <?php
                        foreach($levels as $level)
                        {
                            ?>
                            <option value="<?=$level->id . '|' . $level->name?>" <?=(isset($lead_master) && $lead_master['degree'] && $lead_master['degree'] == $level->id) ? 'selected' : ''?>><?=$level->name?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 mrg">
                  <div class="col-md-4">
                  <select class="form-control ht30" id="user_intake_year" name="user_intake_year">
                          <option value="">Select Intake Year</option>
                          <option value="2020" <?=(isset($student_details) && $student_details['intake_year'] && $student_details['intake_year'] == "2020") ? 'selected' : ''?> >2020</option>
                          <option value="2021" <?=(isset($student_details) && $student_details['intake_year'] && $student_details['intake_year'] == "2021") ? 'selected' : ''?> >2021</option>
                          <option value="2022" <?=(isset($student_details) && $student_details['intake_year'] && $student_details['intake_year'] == "2022") ? 'selected' : ''?> >2022</option>
                      </select>
                </div>
                  <div class="col-md-4">
                  <select class="form-control ht30" id="user_intake_month" name="user_intake_month">
                          <option value="">Select Intake Month</option>
                          <option value="Jan-Mar" <?=(isset($student_details) && $student_details['intake_month'] && $student_details['intake_month'] == "Jan-Mar") ? 'selected' : ''?> >January - March</option>
                          <option value="Apr-Jun" <?=(isset($student_details) && $student_details['intake_month'] && $student_details['intake_month'] == "Apr-Jun") ? 'selected' : ''?> >April - June</option>
                          <option value="Aug-Oct" <?=(isset($student_details) && $student_details['intake_month'] && $student_details['intake_month'] == "Aug-Oct") ? 'selected' : ''?> >August - October</option>
                      </select>
                </div>
                  <div class="col-md-4">
                  <select class="form-control ht30" id="user_hear_about_us" name="user_hear_about_us">
                          <option value="">From Where Did You Hear About Us?</option>
                          <option value="Friends / Family" <?=(isset($student_details) && $student_details['hear_about_us'] && $student_details['hear_about_us'] == "Friends / Family") ? 'selected' : ''?> >Friends / Family</option>
                          <option value="Google / Facebook" <?=(isset($student_details) && $student_details['hear_about_us'] && $student_details['hear_about_us'] == "Google / Facebook") ? 'selected' : ''?> >Google / Facebook</option>
                          <option value="Stupidsid Seminar" <?=(isset($student_details) && $student_details['hear_about_us'] && $student_details['hear_about_us'] == "Stupidsid Seminar") ? 'selected' : ''?> >Stupidsid Seminar</option>
                          <option value="Stupidsid Website" <?=(isset($student_details) && $student_details['hear_about_us'] && $student_details['hear_about_us'] == "Stupidsid Website") ? 'selected' : ''?> >Stupidsid Website</option>
                          <option value="College Seminar" <?=(isset($student_details) && $student_details['hear_about_us'] && $student_details['hear_about_us'] == "College Seminar") ? 'selected' : ''?> >College Seminar</option>
                          <option value="News Paper" <?=(isset($student_details) && $student_details['hear_about_us'] && $student_details['hear_about_us'] == "News Paper") ? 'selected' : ''?> >News Paper</option>
                          <option value="Calling from Imperial" <?=(isset($student_details) && $student_details['hear_about_us'] && $student_details['hear_about_us'] == "Calling from Imperial") ? 'selected' : ''?> >Calling from Imperial</option>
                      </select>
                </div>
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 mrg" style="text-align: center;margin-top : 15px;">

                       <!--input type="button" class="btn btn-lg btn-primary" name="submitValue" value="submit" id="submitValue" onclick="openConsentModal()" disabled="disabled"-->
                       <input type="button" class="btn btn-lg btn-primary" name="submitValue" value="submit" id="submitValue" onclick="openConsentModal()">

               </div>
            </div>
       </div>
       <div class="col-md-12 mrg" id="consentBlock" style="display: none;">
         <!-- consent form -->
           <?=$consent?>

         <div class="col-md-12" style="text-align: right;margin-top : 15px;"><input type="button" class="btn btn-lg btn-primary" name="agreed" value="Agreed" onclick="connectUniversity()"></div>
         <!-- consent form end -->
       </div>
        <div class="col-md-12" ></div>

      </form>

    </div>

   </div>
</div>
<script src="js/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/signature_pad/1.5.3/signature_pad.min.js"></script>
<script>
   var acc = document.getElementsByClassName("accordion");
   var i;

   for (i = 0; i < acc.length; i++) {
     acc[i].addEventListener("click", function() {
       this.classList.toggle("active");
       var panel = this.nextElementSibling;
       if (panel.style.display === "block") {
         panel.style.display = "none";
       } else {
         panel.style.display = "block";
     $("footer").animate({marginTop: "0px"}, 'fast');
       }
     });
   }

   // Get the modal
   var modal = document.getElementById("myModal");
   var consentModal = document.getElementById("consent-modal");

   // Get the button that opens the modal
   var btn = document.getElementById("myBtn");

   // Get the <span> element that closes the modal
   var span = document.getElementsByClassName("close")[0];
   var consentSpan = document.getElementById("consent-close");

   // When the user clicks the button, open the modal
   function openComparePopup(courseName) {
   jQuery.ajax({
        type: "GET",
        url: "/university/compare/getsimilar/?course=" + courseName,
          success: function(response) {
       document.getElementById("compare-university-modal").innerHTML = response;
       modal.style.display = "block";
            }
    });
   }

   function applyUniversity(university_id, university_name, university_email) {
   jQuery.ajax({
        type: "GET",
        url: "/university/details/apply/?uid=" + university_id + "&uname=" + university_name + "&uemail=" + university_email,
    success: function(response) {
       document.getElementById("apply_button").value = "Applied";
             document.getElementById("apply_button").disabled = true;
             document.getElementById("apply_button").style.backgroundColor = "#F6881F";
        }
    });
   }

   // When the user clicks on <span> (x), close the modal
   span.onclick = function() {
   modal.style.display = "none";
   }

   consentSpan.onclick = function() {
   consentModal.style.display = "none";
   }

   // When the user clicks anywhere outside of the modal, close it
   window.onclick = function(event) {
   if (event.target == modal) {
    modal.style.display = "none";
   }
   }

   var signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
    backgroundColor: 'rgba(255, 255, 255, 0)',
    penColor: 'rgb(0, 0, 0)'
   });

   function openConsent()
   {
    consentModal.style.display = "block";
   }

   function openConsentModal() {
       var country = document.getElementById("user_country").value;
       var split_country = country.split("|");
       var city = document.getElementById("user_city").value;
       var split_city = city.split("|");
       var mainstream = document.getElementById("user_mainstream").value;
       var split_mainstream = mainstream.split("|");
       var subcourse = document.getElementById("user_subcourse").value;
       var split_subcourse = subcourse.split("|");
       var level = document.getElementById("user_level").value;
       var split_level = level.split("|");

       var studentName = document.getElementById("user_name").value;
       var dob = document.getElementById("user_dob").value;
       var passport_number = document.getElementById("user_passport").value;
       var resident = split_city[1] + ', ' + split_country[1];
       var program = split_level[1] + ', ' + split_subcourse[1] + ', ' + split_mainstream[1];
       var intake = document.getElementById("user_intake_month").value + ', ' + document.getElementById("user_intake_year").value;
       var email = document.getElementById("user_email").value;
       var mobile = document.getElementById("user_phone").value;
       var university_name = '<?=$name?>';
       var today_date = '<?=date('Y-m-d')?>';

       for(i = studentName.length; i < 80; i++){
           studentName += '&nbsp;';
       }
       for(i = dob.length; i < 40; i++){
           dob += '&nbsp;';
       }
       for(i = passport_number.length; i < 65; i++){
           passport_number += '&nbsp;';
       }
       for(i = resident.length; i < 50; i++){
           resident += '&nbsp;';
       }
       for(i = program.length; i < 70; i++){
           program += '&nbsp;';
       }
       for(i = intake.length; i < 50; i++){
           intake += '&nbsp;';
       }
       for(i = university_name.length; i < 70; i++){
           university_name += '&nbsp;';
       }
       for(i = email.length; i < 70; i++){
           email += '&nbsp;';
       }
       for(i = mobile.length; i < 70; i++){
           mobile += '&nbsp;';
       }
       for(i = today_date.length; i < 40; i++){
           today_date += '&nbsp;';
       }

       document.getElementById("student_name_1").innerHTML = '<u>' + studentName + '</u>';
       document.getElementById("student_name_2").innerHTML = '<u>' + studentName + '</u>';
       document.getElementById("dob").innerHTML = '<u>' + dob + '</u>';
       document.getElementById("passport").innerHTML = '<u>' + passport_number + '</u>';
       document.getElementById("resident").innerHTML = '<u>' + resident + '</u>';
       document.getElementById("program").innerHTML = '<u>' + program + '</u>';
       document.getElementById("intake").innerHTML = '<u>' + intake + '</u>';
       document.getElementById("university").innerHTML = '<u>' + university_name + '</u>';
       document.getElementById("email").innerHTML = '<u>' + email + '</u>';
       document.getElementById("mobile").innerHTML = '<u>' + mobile + '</u>';
       document.getElementById("date").innerHTML = '<u>' + today_date + '</u>';

       document.getElementById('consentBlock').style.display = 'block' ;
       document.getElementById('fieldsBox').style.display = 'none' ;
    }

   function connectUniversity()
   {
       var country = document.getElementById("user_country").value;
       var split_country = country.split("|");
       var city = document.getElementById("user_city").value;
       var split_city = city.split("|");
       var mainstream = document.getElementById("user_mainstream").value;
       var split_mainstream = mainstream.split("|");
       var subcourse = document.getElementById("user_subcourse").value;
       var split_subcourse = subcourse.split("|");
       var level = document.getElementById("user_level").value;
       var split_level = level.split("|");

       var country = split_country[0];
       var city = split_city[0];
       var mainstream = split_mainstream[0];
       var subcourse = split_subcourse[0];
       var level = split_level[0];

       var data = signaturePad.toDataURL('image/png');
       document.getElementById("student_signature").value = data;
       document.getElementById("student_name").value = document.getElementById("user_name").value;
       document.getElementById("student_email").value = document.getElementById("user_email").value;
       document.getElementById("student_phone").value = document.getElementById("user_phone").value;
       document.getElementById("student_dob").value = document.getElementById("user_dob").value;
       document.getElementById("student_country").value = country;
       document.getElementById("student_city").value = city;
       document.getElementById("student_mainstream").value = mainstream;
       document.getElementById("student_subcourse").value = subcourse;
       document.getElementById("student_level").value = level;
       document.getElementById("student_passport").value = document.getElementById("user_passport").value;
       document.getElementById("student_intake_month").value = document.getElementById("user_intake_month").value;
       document.getElementById("student_intake_year").value = document.getElementById("user_intake_year").value;
       document.getElementById("student_hear_about_us").value = document.getElementById("user_hear_about_us").value;
       var form = document.getElementById("connect-university");
       form.submit();
   }

   function uploadSignature()
   {
       var formData = new FormData();
       formData.append('file', $('#upload-signature')[0].files[0]);

       $.ajax({
           url : '/university/details/uploadSignature',
           type : 'POST',
           data : formData,
           processData: false,  // tell jQuery not to process the data
           contentType: false,  // tell jQuery not to set contentType
           success : function(data) {
               if(data == 1)
               {
                   alert("Signature uploaded successfully");
               }
               else
               {
                   alert("Some issue occured while uploading signature");
               }
           }
       });
   }

   window.onscroll = function() {myFunction()};

    var header = document.getElementById("myHeaderone");
    var sticky = header.offsetTop;

    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
    }



   $.fn.jQuerySimpleCounter = function( options ) {
      var settings = $.extend({
          start:  0,
          end:    100,
          easing: 'swing',
          duration: 400,
          complete: ''
      }, options );

      var thisElement = $(this);

      $({count: settings.start}).animate({count: settings.end}, {
      duration: settings.duration,
      easing: settings.easing,
      step: function() {
        var mathCount = Math.ceil(this.count);
        thisElement.text(mathCount);
      },
      complete: settings.complete
    });
  };


$('#number1').jQuerySimpleCounter({end: <?php if(isset($student) && !empty($student)){ echo $student; } else { echo '0'; } ?>,duration: 3000});
$('#number2').jQuerySimpleCounter({end: <?php if(isset($internationalstudent) && !empty($internationalstudent)){ echo $internationalstudent; } else { echo '0'; } ?>,duration: 3000});
$('#number3').jQuerySimpleCounter({end: <?php if(isset($ranking_usa) && !empty($ranking_usa)){ echo $ranking_usa; } else { echo '0'; } ?>,duration: 2000});
$('#number4').jQuerySimpleCounter({end: <?php if(isset($ranking) && !empty($ranking)){ echo $ranking; } else { echo '0'; } ?>,duration: 2500});
$('#number5').jQuerySimpleCounter({end: <?php if(isset($admission_success_rate) && !empty($admission_success_rate)){ echo $admission_success_rate; } else { echo '0'; } ?>,duration: 2500});
$('#number6').jQuerySimpleCounter({end: <?php if(isset($placement_percentage) && !empty($placement_percentage)){ echo $placement_percentage; } else { echo '0'; } ?>,duration: 2500});


// New Script

 var CourseListModal = document.getElementById("myCourseListModal");

 //var coursemodalSpan = document.getElementsByClassName("coursemodalcllose")[0];
  var coursemodalSpan = document.getElementById("courseListClose");
 //   // When the user clicks on <span> (x), close the modal
 function openCourseListModal(cat_id,uni_id,cat_name){

       var dataString = 'cat_id=' + cat_id + "&uni_id=" + uni_id;

       $.ajax({

       type: "POST",

       url: "/university/details/getCourseByCatIdUniId",

       data: dataString,

       cache: false,

       success: function(result){

         //alert(result);
         CourseListModal.style.display = "block";
         console.log(result);

         var courses = result.courses;

         if(courses.length){

                //var courseHTMLL = '<form id="compare-course-form" name="compare-course-form" action="/course/compare" method="POST">';
                var courseHTMLL = '<h4><b>Browse by Discipline : </b><font color="#f9992f"><b>' + cat_name + '</b></font></h4>';

               courses.forEach(function(value, index){

               courseHTMLL += '<div class="col-md-12">'
                                 +'<div class="uni-boxPurple">'
                                 +'<div class="flLt">'
                                 +'<div class="uni-image">'
                                 +'<a target="_blank" href="#"><img class="lazy" alt="' + value.university_name + '" title="' + value.university_name + '" align="center" width="190px" height="175px" src="' + value.university_banner + '" style="display: inline;"></a>'
                                 +'<div class="uni-shrtlist-image"></div>'
                                 +'</div>'
                                 +'</div>'
                                 +'<div class="uni-detailModal">'
                                 +'<div class="uni-title">'
                                 +'<div class="col-md-9">'
                                 +'<h4 class="card-title" style="color: rgb(0, 75, 122)"><i class="fa fa-graduation-cap" style="font-size:15px;color:#374552">&nbsp;</i>Course : <b>'+value.name+'</b></h4>'
                                 +'</div>'
                              +'</div>'
                              +'<div class="clearwidth">'
                                  +'<div class="iconRowPurple">'
                                     +'<div class="col-md-12 col-sx-12 col-sm-12 twoPXPadding">'
                                          +'<div class="col-md-4 col-xs-12 col-sm-12 twoPXPadding">'
                                                +'<div class="col-md-2 col-xs-2 col-sm-2"><i class="fas fa-school iconCss" >&nbsp;&nbsp;</i></div>'
                                                +'<div class="col-md-10 col-xs-10 col-sm-10 faPadding"><strong>  Department Name <span>&nbsp; ' + value.department_name + '</span></strong></div>'
                                           +'</div>'
                                           +'<div class="col-md-4 twoPXPadding">'
                                                +'<div class="col-md-2 col-xs-2 col-sm-2"><i class="far fa-clock iconCss" >&nbsp;&nbsp;</i></div>'
                                                +'<div class="col-md-10 col-xs-10 col-sm-10 faPadding" style="margin-top: 10px;"><strong>  Duration <span>&nbsp; ' + value.duration + ' Months</span></strong></div>'
                                           +'</div>'
                                           +'<div class="col-md-4 col-xs-12 col-sm-12 twoPXPadding">'
                                                +'<div class="col-md-2 col-xs-2 col-sm-2"><i class="far fa-calendar-alt iconCss"></i></div>'
                                                +'<div class="col-md-10 col-xs-10 col-sm-10 faPadding" style="margin-top: 10px;"><strong>  Start Month <span>&nbsp; ' + value.start_months + '</span></strong></div>'
                                           +'</div>'
                                     +'</div>'
                                     +'<div class="col-md-12 twoPXPadding" style="margin-top: 8px !important;    padding-bottom: 9px;border-bottom: 2px solid black;">'
                                           +'<div class="col-md-4 col-xs-12 col-sm-12 twoPXPadding">'
                                                +'<div class="col-md-2 col-xs-2 col-sm-2"><i class="fas fa-money-check-alt iconCss"></i></div>'
                                                +'<div class="col-md-10 col-xs-10 col-sm-10 faPadding"><strong>  Application Fees <span>&nbsp; ' + value.application_fee + '  per year</span></strong></div>'
                                           +'</div>'
                                           +'<div class="col-md-4 col-xs-12 col-sm-12 twoPXPadding">'
                                                +'<div class="col-md-2 col-xs-2 col-sm-2"><i class="fa fa-money iconCss" >&nbsp;&nbsp;</i></div>'
                                                +'<div class="col-md-10 col-xs-10 col-sm-10 faPadding" style="margin-top: 10px;"><strong>  Salary Expected<span>&nbsp; ' + value.expected_salary + '</span></strong></div>'
                                           +'</div>'
                                           +'<div class="col-md-4 col-xs-12 col-sm-12 twoPXPadding">'
                                                +'<div class="col-md-2 col-xs-2 col-sm-2"><i class="fas fa-university iconCss"></i></div>'
                                                +'<div class="col-md-10 col-xs-10 col-sm-10 faPadding" style="margin-top: 10px;"><strong>  Stay Back Option <span>&nbsp; ' + value.no_of_month_work_permit + '</span></strong></div>'
                                           +'</div>'
                                     +'</div>'
                                  +'</div>'
                              +'</div>'
                               +'<div class="row" style="margin-top: 10px !important">'
                                  +'<div class="col-md-6 col-xs-12 col-sm-12 btn-col btn-brochure" style="text-align: left;margin-top: 12px;">'
                                       +'<a href="/course/coursemaster/courselist/'+ value.id +'" class="button-style" target="_blank" style="background-color: #8c57c4;">View Detail</a>'
                                  +'</div>'
                                  +'<div class="col-md-4 col-xs-12 col-sm-12 font14 mrgtop float-right flRt">'
                                       +'<i class="fa fa-eye" >&nbsp; '+ value.course_view_count +'</i> | &nbsp;'
                                       +'<i class="fa fa-heart" >&nbsp; '+ value.course_likes_count +'</i> | &nbsp;'
                                       +'<i class="fa fa-star" >&nbsp; '+ value.course_fav_count +'</i>'
                                  +'</div>'
                                  +'<div class="col-md-2 flRt customInputs float-right mrgtop">'
                                            +'<i class="heart fa fa-heart-o"> </i>'
                                             +'<span class="common-sprite"></span>'
                                  +'</div>'
                               +'</div>'
                         +'</div>'
                     +'</div>'
               +'</div>' ;

               });

               //courseHTMLL += '</form><p><a class="btn" href="#" target="_blank"></a></p>';
               document.getElementById("course-list-modal").innerHTML = courseHTMLL;

         }

      }

             });
}

    coursemodalSpan.onclick = function() {
   CourseListModal.style.display = "none";
   }

   window.onclick = function(event) {
   if (event.target == CourseListModal) {
    CourseListModal.style.display = "none";
   }
   }

   $(document).ready(function(){
        $('#user_mainstream').on('change', function() {
            var dataString = 'id='+this.value;
            $.ajax({
                type: "POST",
                url: "/search/university/getSubcoursesList",
                data: dataString,
                cache: false,
                success: function(result){
                    $('#user_subcourse').html(result);
                }
            });
        });
   if ($("i").hasClass("fa-heart-o")){
    // Do something if class exists
       $(".heart.fa-heart-o").click(function() {
         $(this).removeClass('fa-heart-o');
         $(this).addClass('fa-heart');
         $(".fa-heart").append('<style>{pointer-events:none;}</style>');
       });
   } else {
       // Do something if class does not exist
      // $(".fa-heart").css({"pointer-events": "none !important"});
      $('.fa-heart').append('<style>{pointer-events:none !important;}</style>');
   }

});
 $("#ProgramsList").on("click", function(){
    $(".sideMenu").show();
});

function openTab(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  if(cityName == 'Programs'){
   $(".sideMenu").show();
  } else {
   $(".sideMenu").hide();
  }

  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";

}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
<script>
var xValues = ["Australia", "England", "UAE", "USA", "ETC"];
var yValues = [55, 49, 44, 24, 15];
var barColors = [
  "rgba(246,136,44,1)",
  "rgba(246,136,44,0.9)",
  "rgba(246,136,44,0.8)",
  "rgba(246,136,44,0.7)",
  "rgba(246,136,44,0.6)"
];

new Chart("myChart", {
  type: "bar",
  data: {
    labels: xValues,
    datasets: [{
      backgroundColor: barColors,
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }],
    }
  }
});
</script>
<script>
// let slideIndex = 1;
// showSlides(slideIndex);

// function plusSlides(n) {
//   showSlides(slideIndex += n);
// }

// function currentSlide(n) {
//   showSlides(slideIndex = n);
// }

// function showSlides(n) {
//   let i;
//   let slides = document.getElementsByClassName("mySlides");
//   let dots = document.getElementsByClassName("demo");
//   let captionText = document.getElementById("caption");
//   if (n > slides.length) {slideIndex = 1}
//   if (n < 1) {slideIndex = slides.length}
//   for (i = 0; i < slides.length; i++) {
//     slides[i].style.display = "none";
//   }
//   for (i = 0; i < dots.length; i++) {
//     dots[i].className = dots[i].className.replace(" active", "");
//   }
//   slides[slideIndex-1].style.display = "block";
//   dots[slideIndex-1].className += " active";
//   captionText.innerHTML = dots[slideIndex-1].alt;
// }


var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}



const accordionBtns = document.querySelectorAll(".item-header");

accordionBtns.forEach((accordion) => {
  accordion.onclick = function () {
    this.classList.toggle("active");

    let content = this.nextElementSibling;
    console.log(content);

    if (content.style.maxHeight) {
      //this is if the accordion is open
      content.style.maxHeight = null;
    } else {
      //if the accordion is currently closed
      content.style.maxHeight = content.scrollHeight + "px";
      console.log(content.style.maxHeight);
    }
  };
});


// Show the first tab by default
// $('.tabs-stage div').hide();
// $('.tabs-stage div:first').show();
// $('.tabs-nav li:first').addClass('tab-active');

// // Change tab class and display content
// $('.tabs-nav a').on('click', function(event){
//   event.preventDefault();
//   $('.tabs-nav li').removeClass('tab-active');
//   $(this).parent().addClass('tab-active');
//   $('.tabs-stage div').hide();
//   $($(this).attr('href')).show();
// });

$('.moreless-button').click(function(e) {
  $('.moretext').slideToggle();
  if ($('.moreless-button').text() == "Read more") {
    $(this).text("Read less")
  } else {
    $(this).text("Read more")
  } e.preventDefault();
});

$(document).ready(function(){
   $('a[href^="#"]').on('click',function (e) {
       e.preventDefault();
       var target = this.hash;
       var $target = $(target);
       $('html, body').stop().animate({
           'scrollTop': $target.offset().top
       }, 900, 'swing', function () {
           // window.location.hash = target;
       });
   });
});


$(document).on("click", '[data-toggle="data-lightbox"]', function(event) {
  event.preventDefault();
  $(this).ekkoLightbox();
});
</script>


<!-- Bootstrap Core JavaScript -->
<!--<script src="js/bootstrap.min.js"></script>-->
<!-- Plugin JavaScript -->
<!--  <script src="js/jquery.easing.min.js"></script>-->
<!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
<!--  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>-->
<!-- Custom Theme JavaScript -->
<!--<script src="js/main.js"></script>--> <!-- Resource jQuery -->
