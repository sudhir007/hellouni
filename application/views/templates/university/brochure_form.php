<style>
   .panelprimaryHeading {
   color: #fff;
   background-color: #815dd5 !important;
   border-color: #815dd5 !important;
   }
   .panelBody{
   border-top: 3px solid #815dd5;
   }
   .form-control{
   height: 30px;
   }
</style>
<div class="container">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h4>University : Add Brochure</h4>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="panel panel-primary">
            <div class="panel-heading panelprimaryHeading">Add Brochure</div>
            <div class="panel-body">

                    <?php
                    if($this->session->flashdata('flash_message'))
                    {
                        ?>
                        <div class="alert alert-error alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                            <h4><i class="icon fa fa-circle-o"></i> Error</h4>
                            <?php echo $this->session->flashdata('flash_message'); ?>
                        </div>
                        <?php
                    }
                    if($this->uri->segment('4'))
                    {
                        ?>
                        <form class="form-horizontal" action="<?php echo base_url();?>university/brochure/edit/<?php if(isset($id)) echo $id;?>" method="post" name="addbrochure" enctype="multipart/form-data">
                        <?php
                    }
                    else
                    {
                        ?>
                        <form class="form-horizontal" action="<?php echo base_url();?>university/brochure/create" method="post" name="addbrochure" enctype="multipart/form-data">
                        <?php
                    }
                    ?>
                        <div class="box-body">
                            <!--div class="form-group">
                                <label for="university" class="col-sm-2 control-label">University *</label>
                                <div class="col-sm-10">
                                    <?php
                                    /*
                                    if(!$university_id)
                                    {
                                        ?>
                                        <select class="form-control" name="university">
                                            <?php
                                            foreach($universities as $universityData)
                                            {
                                                ?>
                                                <option value="<?=$universityData->id?>" <?php if(isset($university) && $universityData->id==$university){ echo 'selected="selected"'; } ?>><?=$universityData->name?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                        <select class="form-control" name="university" disabled>
                                            <?php
                                            foreach($universities as $universityData)
                                            {
                                                ?>
                                                <option value="<?=$universityData->id?>" <?php if($universityData->id==$university_id){ echo 'selected="selected"'; } ?>><?=$universityData->name?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                        <?php
                                    }
                                    */
                                    ?>
                                </div>
                            </div-->
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">Title *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="title" name="brochure_title" placeholder="Brochure Title" value="<?php if(isset($brochure_title)) echo $brochure_title; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="image" class="col-sm-2 control-label">Brochure</label>
                                <div class="col-sm-10">
                                    <?php
                                    if(isset($brochure_file_name))
                                    {
                                        ?>
                                        <img src="<?php echo base_url().$brochure_file_name; ?>" class="profileimg"/>
                                        <input type="hidden" name="image" value="<?php echo $brochure_file_name; ?>"/>
                                        <?php
                                    }
                                    ?>
                                    <input type="file" name="brochure_file_name" id="brochure_file_name" />
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                            <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                            <?php
                            if($this->uri->segment('4'))
                            {
                                ?>
                                <button type="submit" class="btn btn-info pull-right">Update</button>
                                <?php
                            }
                            else
                            {
                                ?>
                                <button type="submit" class="btn btn-info pull-right">Create</button>
                                <?php
                            }
                            ?>
                        </div><!-- /.box-footer -->
                    </form>
            </div>
        </div>   
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
</div>
