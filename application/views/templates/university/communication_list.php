<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<style type="text/css">
    .styleCss{
        font-size: 14px;
        background-color: #f6882c;
        color: white;
        font-weight: 600;
        letter-spacing: 0.8px;
    }
    .formControl{
        color: #fff !important;
        background-color: #8c57c5 !important;
        font-size: 12px;
        border: 0px !important;
        box-shadow: inset 0 0 0 rgba(0,0,0,.075) !important;
        margin-top: 15px;
    }
    #dt-example td.details-control {
        cursor: pointer;
    }
    #dt-example tr.shown td {
        background-color: #815dd5;
        color: #fff !important;
        border: none;
        border-right: 1px solid #815dd5;
    }
    #dt-example tr.shown td:last-child {
        border-right: none;
    }
    #dt-example tr.details-row .details-table tr:first-child td {
        color: #fff;
        background: #f6882c;
        border: none;
    }
    #dt-example tr.details-row .details-table > td {
        padding: 0;
    }
    #dt-example tr.details-row .details-table .fchild td:first-child {
        cursor: pointer;
    }
    #dt-example tr.details-row .details-table .fchild td:first-child:hover {
        background: #fff;
    }
    #dt-example .form-group.agdb-dt-lst {
        padding: 2px;
        height: 23px;
        margin-bottom: 0;
    }
    #dt-example .form-group.agdb-dt-lst .form-control {
        height: 23px;
        padding: 2px;
    }
    #dt-example .adb-dtb-gchild {
        padding-left: 2px;
    }
    #dt-example .adb-dtb-gchild td {
        background: #f5fafc;
        padding-left: 15px;
    }
    #dt-example .adb-dtb-gchild td:first-child {
        cursor: default;
    }
    .dataTables_wrapper{
        overflow: auto;
    }
    .listTableView thead tr{
        background-color: #f6882c;
        color: white;
    }

    
</style>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<div class="container" style="margin-top: 10px;">
         <div class="panel panel-default">
            <div class="panel-heading purplePanel"><h4 class="text-center"> Communication Form </h4></div>
            <div class="panel-body">
                
                    <form class="login-form" class="form-horizontal" onsubmit="addComment()" method="post">
                        <div class="col-md-12 form-group">
                            <div class="col-md-12">
                                <label for="inputEmail3" class="text-uppercase"> Type Your Message * </label>
                                <textarea  name="comment" id="comment" class="form-control" style="font-size:12px;" rows="6" cols="50"> </textarea>

                                <input type="hidden" class="form-control" id="entity_id" name="entity_id" value="<?=$entity_id?>">
                            </div>
                        </div>

                        <div class="form-check" style="text-align: center;">
                            <!-- <button type="submit" class="btn btn-login" ng-click="submitForm()">Submit</button> -->
                            <button type="button" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-warning whiteRestButton " id="reset" onclick="window.location.reload();">Reset</button>
                            <input type="submit"  style="text-align: center; border-radius:5px;" class="btn btn-lg  purpleButton" value="Submit"></input>
                        </div>
                    </form>
            </div>
        </div>
</div>

<div class="container" style="clear:both;">
    <div class="row" style="padding: 30px 0px 30px 0px;display: flex;">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body listTableView">
                    <table id="dt-example" class="stripe row-border order-column cl-table dataTable no-footer" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th> Type </th>
                                <th> Message </th>
                                <th> Added By </th>
                                <th> Date </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {

      var dataentityId = $('#entity_id').val();

        $('#data-table').dataTable();
        $(".dataTable").on("draw.dt", function (e) {
            setCustomPagingSigns.call($(this));
        }).each(function () {
            setCustomPagingSigns.call($(this)); // initialize
        });

        function setCustomPagingSigns() {
            var wrapper = this.parent();
            wrapper.find("a.previous").text("<");
            wrapper.find("a.next").text(">");
        }

        var data = [];
        var dataString = '';
        var dataurl = "/university/counsellor/chfilter/"+dataentityId;

        $.ajax({
            type: "POST",
            url: dataurl,
            data: dataString,
            cache: false,
            success: function(result){
              console.log("hello",result);
                var studentdata = result;
                if(studentdata.length){
                    studentdata.forEach(function(value, index){
                        data.push({
                            "Type" : value.entity_type,
                            "Message" : value.comment,
                            "Added_by" : value.comment_by,
                            "DateTime" : value.added_time
                        });
                    });

                    var table = $('.dataTable').DataTable({
                        columns : [
                          {data : 'Type'},
                          {
                                data : 'Message',
                                className : 'details-control',
                            },
                            {data : 'Added_by'},
                            {data : 'DateTime'}
                        ],
                        data : data,
                        pagingType : 'full_numbers',
                        language : {
                            emptyTable     : 'No data to display.',
                            zeroRecords    : 'No records found!',
                            thousands      : ',',
                            loadingRecords : 'Loading...',
                            search         : 'Search:',
                            paginate       : {
                                next     : 'next',
                                previous : 'previous'
                            }
                        },
                        "bDestroy": true
                    });

                    $('.dataTable tbody').on('click', 'td.details-control', function () {
                        var tr  = $(this).closest('tr'),
                        row = table.row(tr);

                        if (row.child.isShown()) {
                            tr.next('tr').removeClass('details-row');
                            row.child.hide();
                            tr.removeClass('shown');
                        }
                        else {
                            row.child(format(row.data())).show();
                            tr.next('tr').addClass('details-row');
                            tr.addClass('shown');
                        }
                    });

                    $('#dt-example').on('click','.details-table tr',function(){
                        $('.details-table').find('.adb-dtb-gchild:not(:first-child)').slideToggle();
                    });
                }
            }
        });
    });

    function addComment(){

        var entityId = $('#entity_id').val();
        var comment = $('#comment').val();

        var dataString = 'entity_id=' + entityId + '&comment=' + comment;

        event.preventDefault();

        $.ajax({
            type: "POST",
            url: "/university/counsellor/insertComments",
            data: dataString,
            cache: false,
            success: function(result){
                if(result == "SUCCESS"){
                    alert("Message Sent Successfully...!!!");
                    window.location.href='/university/counsellor/communicatiohistory/'+entityId;
                } else {
                    alert(result);
                    window.location.href='/user/account';
                }
            }
        });
    }
</script>
