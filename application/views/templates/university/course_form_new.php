
<style>
     #ajaxSpinnerImage {
          display: none;
     }

     #ajaxSpinnerContainer {
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
     }
     .panelprimaryHeading {
   color: #fff;
   background-color: #815dd5 !important;
   border-color: #815dd5 !important;
   }
   .panelBody{
   border-top: 3px solid #815dd5;
   }
   .form-control{
   height: 30px;
   }
</style>
<div class="container">
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
            University : Add Course
          </h4>
          <div id="ajaxSpinnerContainer" >
          <img src="<?php echo base_url();?>images/ajax-loader.gif" id="ajaxSpinnerImage" title="working..." />
        </div>
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="panel panel-primary">
            <div class="panel-heading panelprimaryHeading">Add Course</div>
            <div class="panel-body">
                <!-- form start -->
                <?php if($this->session->flashdata('flash_message')){ ?>
  				<div class="alert alert-error alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                  <h4><i class="icon fa fa-circle-o"></i> Error</h4>
                  <?php echo $this->session->flashdata('flash_message'); ?>
                </div>
  			  <?php } ?>

          <?php if($this->uri->segment('4')){?>
          <form autocomplete="off" class="form-horizontal" action="<?php echo base_url();?>university/course/edit/<?php if(isset($id)) echo $id;?>" method="post" name="addadmin">
          <?php }else{?>
                 <form autocomplete="off" class="form-horizontal" action="<?php echo base_url();?>university/course/create" method="post" name="adduni">
                 <?php } ?>


                  <div class="box-body">

                    <div class="form-group">
                         <label for="department" class="col-sm-2 control-label">University *</label>
                          <div class="col-sm-10">
                          <select class="form-control" name="university" id="university" onchange="filter('campus');">
                            <option value="">Select University</option>
                            <?php
                            if(isset($university_list) && !empty($university_list)){
                            foreach ($university_list as $ulkey => $ulvalue) {
                            ?>
                            <option value="<?php echo $ulvalue['id'] ; ?>" <?php if(isset($selected_university_id) && $selected_university_id == $ulvalue['id']){ echo "selected"; } ?>><?php echo $ulvalue['name'] ; ?></option>
                          <?php } } ?>
                          </select>
                          </div>
                    </div>

                    <div class="form-group">
                         <label for="department" class="col-sm-2 control-label">Campus *</label>
                          <div class="col-sm-10">
                          <select class="form-control" name="campus" id="campus" onchange="filter('college')">
                            <option value="">Select Campus</option>
                            <?php
                            if(isset($campus_list) && !empty($campus_list)){
                            foreach ($campus_list as $clkey => $clvalue) {
                            ?>
                            <option value="<?php echo $clvalue['id'] ; ?>" <?php if(isset($selected_campus_id) && $selected_campus_id == $clvalue['id']){ echo "selected"; } ?>><?php echo $clvalue['name'] ; ?></option>
                          <?php } } ?>
                          </select>
                          </div>
                    </div>

                    <div class="form-group">
                         <label for="department" class="col-sm-2 control-label">College *</label>
                          <div class="col-sm-10">
                          <select class="form-control" name="college" id="college" onchange="filter('department')">
                            <option value="">Select College</option>
                            <?php
                            if(isset($college_list) && !empty($college_list)){
                            foreach ($college_list as $cgkey => $cgvalue) {
                            ?>
                            <option value="<?php echo $cgvalue['id'] ; ?>" <?php if(isset($selected_college_id) && $selected_college_id == $cgvalue['id']){ echo "selected"; } ?>><?php echo $cgvalue['name'] ; ?></option>
                          <?php } } ?>
                          </select>
                          </div>
                    </div>

                      <div class="form-group">
                           <label for="department" class="col-sm-2 control-label">Department *</label>
                            <div class="col-sm-10">
                            <select class="form-control" name="department" id="department">
                                <option value="">Select department</option>
                                <?php
                                if(isset($department_list) && !empty($department_list)){
                                foreach ($department_list as $dlkey => $dlvalue) {
                                ?>
                                <option value="<?php echo $dlvalue['id'] ; ?>" <?php if(isset($selected_department_id) && $selected_department_id == $dlvalue['id']){ echo "selected"; } ?>><?php echo $dlvalue['name'] ; ?></option>
                              <?php } } ?>
                            </select>
                            </div>
                      </div>
                      <div class="form-group">
                           <label for="category_id" class="col-sm-2 control-label">Sub Stream *</label>
                            <div class="col-sm-10">
                            <select class="form-control" name="category_id">
                                <option value="">Please Select</option>
                                <?php
                                foreach($categories as $categoryData)
                                {
                                    ?>
                                    <option value="<?=$categoryData->id?>" <?php if(isset($category) && $categoryData->id==$category){ echo 'selected="selected"'; } ?>><?=$categoryData->display_name?></option>
                                    <?php
                                }
                                 ?>
                            </select>
                            </div>
                      </div>
                      <div class="form-group">
                        <label for="degree_substream" class="col-sm-2 control-label">Course Degree Substream</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="degree_substream">
                                <option value="">Please Select</option>
                                <?php
                                foreach($degree_substreams as $degree_substream_name)
                                {
                                    ?>
                                    <option value="<?=$degree_substream_name?>" <?php if(isset($degree_substream) && $degree_substream_name==$degree_substream){ echo 'selected="selected"'; } ?>><?=$degree_substream_name?></option>
                                    <?php
                                }
                                 ?>
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                           <label for="degree" class="col-sm-2 control-label">Degree *</label>
                            <div class="col-sm-10">
                            <select class="form-control" name="degree">
                                <?php
                                foreach($degrees as $degreeData)
                                {
                                    ?>
                                    <option value="<?=$degreeData->id?>" <?php if(isset($degree) && $degreeData->id==$degree){ echo 'selected="selected"'; } ?>><?=$degreeData->name?></option>
                                    <?php
                                }
                                 ?>
                            </select>
                            </div>
                      </div>
                    <div class="form-group">
                      <label for="name" class="col-sm-2 control-label">Name *</label>
                      <div class="col-sm-10">
                        <input type="name" class="form-control" id="name" name="name" placeholder="Course Name" value="<?php if(isset($name)) echo $name; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="description" class="col-sm-2 control-label">Description</label>
                      <div class="col-sm-10">
                        <textarea rows="4" class="form-control" id="description" name="description" placeholder="Description"><?php if(isset($description)) echo $description; ?></textarea>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="duration" class="col-sm-2 control-label">Duration</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="duration" name="duration" placeholder="Duration (In Months) e.g. 24" value="<?php if(isset($duration)) echo $duration; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="deadline_link" class="col-sm-2 control-label">Deadline Link</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="deadline_link" name="deadline_link" placeholder="Deadline Link" value="<?php if(isset($deadline_link)) echo $deadline_link; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="no_of_intake" class="col-sm-2 control-label">Number Of Intake</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="no_of_intake" name="no_of_intake" placeholder="Number Of Intake" value="<?php if(isset($no_of_intake)) echo $no_of_intake; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="intake_months" class="col-sm-2 control-label">Intake Months</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="intake_months" name="intake_months" placeholder="Intake Months(Comma Separated)" value="<?php if(isset($intake_months)) echo $intake_months; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <label for="application_fee" class="col-sm-2 control-label">Application Fee</label>
                        <label>Yes &nbsp;</label><input type="radio" name="application_fee" value="Yes" <?=isset($application_fee) && $application_fee == 'Yes' ? 'checked' : ''?> onclick="showApplicationAmount()">
                        <label>No &nbsp;</label><input type="radio" name="application_fee" value="No" <?=isset($application_fee) && $application_fee == 'No' ? 'checked' : ''?> onclick="hideApplicationAmount()">
                    </div>
                    <div class="form-group" id="application-fee-amount-required" style="display:<?=$applicationFee?>">
                      <label for="application_fee_amount" class="col-sm-2 control-label">Application Fee Amount</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="application_fee_amount" name="application_fee_amount" placeholder="Application Fee Amount" value="<?php if(isset($application_fee_amount)) echo $application_fee_amount; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <label for="application_fee_waiver_imperial" class="col-sm-2 control-label">Application Fee Waiver Imperial</label>
                        <label>Yes &nbsp;</label><input type="radio" name="application_fee_waiver_imperial" value="Yes" <?=isset($application_fee_waiver_imperial) && $application_fee_waiver_imperial == 'Yes' ? 'checked' : ''?>>
                        <label>No &nbsp;</label><input type="radio" name="application_fee_waiver_imperial" value="No" <?=isset($application_fee_waiver_imperial) && $application_fee_waiver_imperial == 'No' ? 'checked' : ''?>>
                    </div>
                    <?php
                    /*
                    <div class="form-group">
                      <label for="deadline_date_spring" class="col-sm-2 control-label">Deadline Date Spring</label>
                      <div class="col-sm-10">
                        <input type="text" class="mws-datepicker small" id="datepicker" name="deadline_date_spring" value="<?php if(isset($deadline_date_spring)) echo $deadline_date_spring; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="deadline_date_fall" class="col-sm-2 control-label">Deadline Date Fall</label>
                      <div class="col-sm-10">
                        <input type="text" class="mws-datepicker small" id="datepicker_1" name="deadline_date_fall" value="<?php if(isset($deadline_date_fall)) echo $deadline_date_fall; ?>">
                      </div>
                    </div>
                    <!--div class="form-group">
                      <label for="intake" class="col-sm-2 control-label">Intake</label>
                      <div class="col-sm-10">
                          <select class="form-control" name="intake">
                              <option value="FALL" <?php if(isset($intake) && $intake=='FALL'){ echo 'selected="selected"'; } ?>>Fall</option>
                              <option value="SPRING" <?php if(isset($intake) && $intake=='SPRING'){ echo 'selected="selected"'; } ?>>Spring</option>
                          </select>
                      </div>
                  </div-->
                    */
                    ?>
                    <div class="form-group">
                      <label for="total_fees" class="col-sm-2 control-label">Total Fees</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="total_fees" name="total_fees" placeholder="Total Fees" value="<?php if(isset($total_fees)) echo $total_fees; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="cost_of_living_year" class="col-sm-2 control-label">Cost Of Living (Year)</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="cost_of_living_year" name="cost_of_living_year" placeholder="Cost Of Living (Year)" value="<?php if(isset($cost_of_living_year)) echo $cost_of_living_year; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="contact_title" class="col-sm-2 control-label">Contact Title</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="contact_title" name="contact_title" placeholder="Contact Title" value="<?php if(isset($contact_title)) echo $contact_title; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="contact_name" class="col-sm-2 control-label">Contact Name</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="contact_name" name="contact_name" placeholder="Contact Name" value="<?php if(isset($contact_name)) echo $contact_name; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="contact_email" class="col-sm-2 control-label">Contact Email</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="contact_email" name="contact_email" placeholder="Contact Email" value="<?php if(isset($contact_email)) echo $contact_email; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="eligibility_gpa" class="col-sm-2 control-label">Eligibility GPA</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" id="eligibility_gpa" name="eligibility_gpa" placeholder="Eligibility GPA" value="<?php if(isset($eligibility_gpa)) echo $eligibility_gpa; ?>">
                      </div>
                      <label for="gpa_scale" class="col-sm-2 control-label">GPA Scale</label>
                      <div class="col-sm-4">
                          <select class="form-control" name="gpa_scale">
                              <option value="">Please Select</option>
                              <option value="4" <?php if(isset($gpa_scale) && $gpa_scale==4){ echo 'selected="selected"'; } ?>>4.0</option>
                              <option value="5" <?php if(isset($gpa_scale) && $gpa_scale==5){ echo 'selected="selected"'; } ?>>5.0</option>
                              <option value="10" <?php if(isset($gpa_scale) && $gpa_scale==10){ echo 'selected="selected"'; } ?>>10</option>
                              <option value="100" <?php if(isset($gpa_scale) && $gpa_scale==10){ echo 'selected="selected"'; } ?>>100</option>
                          </select>
                      </div>
                    </div>
                    <div class="form-group">
                        <label for="test_required" class="col-sm-2 control-label">Test Required</label>
                        <label>Yes &nbsp;</label><input type="radio" name="test_required" value="Yes" <?=isset($test_required) && $test_required == 'Yes' ? 'checked' : ''?>>
                        <label>No &nbsp;</label><input type="radio" name="test_required" value="No" <?=isset($test_required) && $test_required == 'No' ? 'checked' : ''?>>
                    </div>
                    <div class="form-group">
                      <label for="eligibility_gre_quant" class="col-sm-2 control-label">Eligibility GRE (Quant)</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" id="eligibility_gre_quant" name="eligibility_gre_quant" placeholder="Eligibility GRE (Quant)" value="<?php if(isset($eligibility_gre_quant)) echo $eligibility_gre_quant; ?>">
                      </div>
                      <label for="eligibility_gre_verbal" class="col-sm-2 control-label">Eligibility GRE (Verbal)</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" id="eligibility_gre_verbal" name="eligibility_gre_verbal" placeholder="Eligibility GRE (Verbal)" value="<?php if(isset($eligibility_gre_verbal)) echo $eligibility_gre_verbal; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="eligibility_gmat" class="col-sm-2 control-label">Eligibility GMAT</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="eligibility_gmat" name="eligibility_gmat" placeholder="Eligibility GMAT" value="<?php if(isset($eligibility_gmat)) echo $eligibility_gmat; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="eligibility_sat" class="col-sm-2 control-label">Eligibility SAT</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="eligibility_sat" name="eligibility_sat" placeholder="Eligibility SAT" value="<?php if(isset($eligibility_sat)) echo $eligibility_sat; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <label for="eligibility_english_proficiency" class="col-sm-2 control-label">Eligibility English Proficiency</label>
                        <label>Yes &nbsp;</label><input type="radio" name="eligibility_english_proficiency" value="Yes" <?=isset($eligibility_english_proficiency) && $eligibility_english_proficiency == 'Yes' ? 'checked' : ''?>>
                        <label>No &nbsp;</label><input type="radio" name="eligibility_english_proficiency" value="No" <?=isset($eligibility_english_proficiency) && $eligibility_english_proficiency == 'No' ? 'checked' : ''?>>
                    </div>
                    <div class="form-group">
                      <label for="eligibility_toefl" class="col-sm-2 control-label">Eligibility TOEFL</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="eligibility_toefl" name="eligibility_toefl" placeholder="Eligibility TOEFL" value="<?php if(isset($eligibility_toefl)) echo $eligibility_toefl; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="eligibility_ielts" class="col-sm-2 control-label">Eligibility IELTS</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="eligibility_ielts" name="eligibility_ielts" placeholder="Eligibility IELTS" value="<?php if(isset($eligibility_ielts)) echo $eligibility_ielts; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="eligibility_pte" class="col-sm-2 control-label">Eligibility PTE</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="eligibility_pte" name="eligibility_pte" placeholder="Eligibility PTE" value="<?php if(isset($eligibility_pte)) echo $eligibility_pte; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="eligibility_duolingo" class="col-sm-2 control-label">Eligibility Duolingo</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="eligibility_duolingo" name="eligibility_duolingo" placeholder="Eligibility Duolingo" value="<?php if(isset($eligibility_duolingo)) echo $eligibility_duolingo; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <label for="eligibility_work_experience" class="col-sm-2 control-label">Eligibility Work Experience</label>
                        <label>Yes &nbsp;</label><input type="radio" name="eligibility_work_experience" value="Yes" <?=isset($eligibility_work_experience) && $eligibility_work_experience == 'Yes' ? 'checked' : ''?>>
                        <label>No &nbsp;</label><input type="radio" name="eligibility_work_experience" value="No" <?=isset($eligibility_work_experience) && $eligibility_work_experience == 'No' ? 'checked' : ''?>>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">LOR (Letter Of Recommendation)</label>
                        <label>Yes &nbsp;</label><input type="radio" name="lor_needed" value="1" onclick="showLOR()" <?=isset($lor->lor_needed) && $lor->lor_needed == 1 ? 'checked' : ''?>>
                        <label>No &nbsp;</label><input type="radio" name="lor_needed" value="0" onclick="hideLOR()" <?=isset($lor->lor_needed) && $lor->lor_needed == 0 ? 'checked' : ''?>>
                    </div>
                    <div class="form-group" id="lor-required" style="display:<?=$lor_required?>">
                        <label class="col-sm-2 control-label">LOR Required</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="lor_required">
                               <?php
                               for($i = 1; $i <= 10; $i++)
                               {
                                   ?>
                                   <option value="<?=$i?>" <?=isset($lor->lor_required) && $lor->lor_required == $i ? 'selected' : ''?>><?php echo $i;?></option>
                                    <?php
                                }
                                ?>
                             </select>
                         </div>
                    </div>
                    <div class="form-group" id="lor-type" style="display:<?=$lor_type?>">
                        <label class="col-sm-2 control-label">LOR Type</label>
                        <label>Offline &nbsp;</label><input type="radio" name="lor_type" value="offline" <?=isset($lor->lor_type) && $lor->lor_type == "offline" ? 'checked' : ''?>>
                        <label>Online &nbsp;</label><input type="radio" name="lor_type" value="online" <?=isset($lor->lor_type) && $lor->lor_type == "online" ? 'checked' : ''?>>
                        <label>Online (After Application) &nbsp;</label><input type="radio" name="lor_type" value="online_after_application" <?=isset($lor->lor_type) && $lor->lor_type == "online_after_application" ? 'checked' : ''?>>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">SOP (Statement Of Purpose)</label>
                        <label>Yes &nbsp;</label><input type="radio" name="sop_needed" value="1" onclick="showNoOfWords()" <?=isset($sop->sop_needed) && $sop->sop_needed == 1 ? 'checked' : ''?>>
                        <label>No &nbsp;</label><input type="radio" name="sop_needed" value="0" onclick="hideNoOfWords()" <?=isset($sop->sop_needed) && $sop->sop_needed == 0 ? 'checked' : ''?>>
                    </div>
                    <div class="form-group" id="sop-no-of-words" style="display:<?=$sop_no_of_words?>">
                        <label class="col-sm-2 control-label">SOP - Number Of Words</label>
                        <div class="col-sm-10">
                            <input type="text" name="sop_no_of_words" class="form-control" value="<?=isset($sop->sop_no_of_words) ? $sop->sop_no_of_words : ''?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="resume_required" class="col-sm-2 control-label">Resume Required</label>
                        <label>Yes &nbsp;</label><input type="radio" name="resume_required" value="Yes" <?=isset($resume_required) && $resume_required == 'Yes' ? 'checked' : ''?>>
                        <label>No &nbsp;</label><input type="radio" name="resume_required" value="No" <?=isset($resume_required) && $resume_required == 'No' ? 'checked' : ''?>>
                    </div>

                    <div class="form-group">
                      <label for="admission_requirements" class="col-sm-2 control-label">Admission Requirements</label>
                      <div class="col-sm-10">
                        <textarea rows="4" class="form-control" id="admission_requirements" name="admission_requirements" placeholder="Admission Requirements"><?php if(isset($admission_requirements)) echo $admission_requirements; ?></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="expected_salary" class="col-sm-2 control-label">Salary Expected</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="expected_salary" name="expected_salary" placeholder="Salary Expected" value="<?php if(isset($expected_salary)) echo $expected_salary; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <label for="backlog_accepted" class="col-sm-2 control-label">Backlog Accepted</label>
                        <label>Yes &nbsp;</label><input type="radio" name="backlog_accepted" value="Yes" <?=isset($backlog_accepted) && $backlog_accepted == 'Yes' ? 'checked' : ''?> onclick="showNoOfBacklogs()">
                        <label>No &nbsp;</label><input type="radio" name="backlog_accepted" value="No" <?=isset($backlog_accepted) && $backlog_accepted == 'No' ? 'checked' : ''?> onclick="hideNoOfBacklogs()">
                    </div>
                    <div class="form-group" id="number-of-backlogs-accepted" style="display:<?=$backlog?>">
                      <label for="number_of_backlogs" class="col-sm-2 control-label">Number Of Backlogs</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="number_of_backlogs" name="number_of_backlogs" placeholder="Number Of Backlogs" value="<?php if(isset($number_of_backlogs)) echo $number_of_backlogs; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <label for="drop_years" class="col-sm-2 control-label">Drop Years</label>
                        <label>Yes &nbsp;</label><input type="radio" name="drop_years" value="Yes" <?=isset($drop_years) && $drop_years == 'Yes' ? 'checked' : ''?>>
                        <label>No &nbsp;</label><input type="radio" name="drop_years" value="No" <?=isset($drop_years) && $drop_years == 'No' ? 'checked' : ''?>>
                    </div>

                    <div class="form-group">
                        <label for="scholarship_available" class="col-sm-2 control-label">Scholarship Available</label>
                        <label>Yes &nbsp;</label><input type="radio" name="scholarship_available" value="Yes" <?=isset($scholarship_available) && $scholarship_available == 'Yes' ? 'checked' : ''?>>
                        <label>No &nbsp;</label><input type="radio" name="scholarship_available" value="No" <?=isset($scholarship_available) && $scholarship_available == 'No' ? 'checked' : ''?>>
                    </div>
                    <div class="form-group">
                        <label for="with_15_years_education" class="col-sm-2 control-label">Within 15 Years Education</label>
                        <label>Yes &nbsp;</label><input type="radio" name="with_15_years_education" value="Yes" <?=isset($with_15_years_education) && $with_15_years_education == 'Yes' ? 'checked' : ''?>>
                        <label>No &nbsp;</label><input type="radio" name="with_15_years_education" value="No" <?=isset($with_15_years_education) && $with_15_years_education == 'No' ? 'checked' : ''?>>
                    </div>
                    <div class="form-group">
                        <label for="wes_requirement" class="col-sm-2 control-label">WES Requirement</label>
                        <label>Yes &nbsp;</label><input type="radio" name="wes_requirement" value="Yes" <?=isset($wes_requirement) && $wes_requirement == 'Yes' ? 'checked' : ''?>>
                        <label>No &nbsp;</label><input type="radio" name="wes_requirement" value="No" <?=isset($wes_requirement) && $wes_requirement == 'No' ? 'checked' : ''?>>
                    </div>
                    <div class="form-group">
                        <label for="work_while_studying" class="col-sm-2 control-label">Work While Studying</label>
                        <label>Yes &nbsp;</label><input type="radio" name="work_while_studying" value="Yes" <?=isset($work_while_studying) && $work_while_studying == 'Yes' ? 'checked' : ''?>>
                        <label>No &nbsp;</label><input type="radio" name="work_while_studying" value="No" <?=isset($work_while_studying) && $work_while_studying == 'No' ? 'checked' : ''?>>
                    </div>
                    <div class="form-group">
                        <label for="cooperate_internship_participation" class="col-sm-2 control-label">Co-operate Internship Participation</label>
                        <label>Yes &nbsp;</label><input type="radio" name="cooperate_internship_participation" value="Yes" <?=isset($cooperate_internship_participation) && $cooperate_internship_participation == 'Yes' ? 'checked' : ''?>>
                        <label>No &nbsp;</label><input type="radio" name="cooperate_internship_participation" value="No" <?=isset($cooperate_internship_participation) && $cooperate_internship_participation == 'No' ? 'checked' : ''?>>
                    </div>
                    <div class="form-group">
                        <label for="work_permit_needed" class="col-sm-2 control-label">Work Permit After Course</label>
                        <label>Yes &nbsp;</label><input type="radio" name="work_permit_needed" value="Yes" <?=isset($work_permit_needed) && $work_permit_needed == 'Yes' ? 'checked' : ''?> onclick="showNoOfMonths()">
                        <label>No &nbsp;</label><input type="radio" name="work_permit_needed" value="No" <?=isset($work_permit_needed) && $work_permit_needed == 'No' ? 'checked' : ''?> onclick="hideNoOfMonths()">
                    </div>
                    <div class="form-group" id="no-of-months-accepted" style="display:<?=$workPermit?>">
                      <label for="no_of_months" class="col-sm-2 control-label">Number Of Months</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="no_of_months" name="no_of_months" placeholder="Number Of Months" value="<?php if(isset($no_of_months)) echo $no_of_months; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="start_months" class="col-sm-2 control-label">Course Start Months</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="start_months" name="start_months" placeholder="Course Start Months(Comma Separated)" value="<?php if(isset($start_months)) echo $start_months; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="decision_months" class="col-sm-2 control-label">Course Decision Months</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="decision_months" name="decision_months" placeholder="Course Decision Months(Comma Separated)" value="<?php if(isset($decision_months)) echo $decision_months; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <label for="conditional_admit" class="col-sm-2 control-label">Conditional Admit</label>
                        <label>Yes &nbsp;</label><input type="radio" name="conditional_admit" value="Yes" <?=isset($conditional_admit) && $conditional_admit == 'Yes' ? 'checked' : ''?>>
                        <label>No &nbsp;</label><input type="radio" name="conditional_admit" value="No" <?=isset($conditional_admit) && $conditional_admit == 'No' ? 'checked' : ''?>>
                    </div>

                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
					<button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                    <?php
                    if($this->uri->segment('4'))
                    {
                        ?>
                        <button type="submit" class="btn btn-info pull-right">Update</button>
                        <?php
                    }
                    else
                    {
                        ?>
                        <button type="submit" class="btn btn-info pull-right">Create</button>
                        <?php
                    }
                    ?>

                  </div><!-- /.box-footer -->
                </form>
             </div>
          </div> 
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
</div>
      <script type="text/javascript">
          function showNoOfWords(){
              document.getElementById("sop-no-of-words").style.display = "block";
          }
          function hideNoOfWords(){
              document.getElementById("sop-no-of-words").style.display = "none";
          }
          function showLOR(){
              document.getElementById("lor-required").style.display = "block";
              document.getElementById("lor-type").style.display = "block";
          }
          function hideLOR(){
              document.getElementById("lor-required").style.display = "none";
              document.getElementById("lor-type").style.display = "none";
          }
          function showApplicationAmount(){
              document.getElementById("application-fee-amount-required").style.display = "block";
          }
          function hideApplicationAmount(){
              document.getElementById("application-fee-amount-required").style.display = "none";
          }
          function showNoOfBacklogs(){
              document.getElementById("number-of-backlogs-accepted").style.display = "block";
          }
          function hideNoOfBacklogs(){
              document.getElementById("number-of-backlogs-accepted").style.display = "none";
          }
          function showNoOfMonths(){
              document.getElementById("no-of-months-accepted").style.display = "block";
          }
          function hideNoOfMonths(){
              document.getElementById("no-of-months-accepted").style.display = "none";
          }
      </script>

      <script type="text/javascript">

      function filter(entity_type){

        var entityId = 0 ;
        if(entity_type == "campus"){
          entityId = $('#university').val();
        }

        if(entity_type == "college"){
          entityId = $('#campus').val();
        }

        if(entity_type == "department"){
          entityId = $('#college').val();
        }

        sataString = {
          "entity_id" : entityId,
          "entity_type" : entity_type
        }

        $("#ajaxSpinnerImage").show();

        $.ajax({

        type: "POST",

        url: "/admin/filtercontroller/commonDropdown",

        data: sataString,

        cache: false,

        success: function(result){

          $("#ajaxSpinnerImage").hide();
          document.getElementById(entity_type).innerHTML = result;

        }

      });

      }
      </script>
