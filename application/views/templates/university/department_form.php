<style>
   #ajaxSpinnerImage {
   display: none;
   }
   #ajaxSpinnerContainer {
   height: 100%;
   display: flex;
   justify-content: center;
   align-items: center;
   }
   .panelprimaryHeading {
   color: #fff;
   background-color: #815dd5 !important;
   border-color: #815dd5 !important;
   }
   .panelBody{
   border-top: 3px solid #815dd5;
   }
   .form-control{
   height: 30px;
   }
</style>
<div class="container">
   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
         <h4>
            University : Add Department
         </h4>
         <div id="ajaxSpinnerContainer" >
            <img src="<?php echo base_url();?>images/ajax-loader.gif" id="ajaxSpinnerImage" title="working..." />
         </div>
      </section>
      <!-- Main content -->
      <section class="content">
         <div class="panel panel-primary">
            <div class="panel-heading panelprimaryHeading">Add Department</div>
            <div class="panel-body">
                  <!-- form start -->
                  <?php if($this->session->flashdata('flash_message')){ ?>
                  <div class="alert alert-error alert-dismissible">
                     <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                     <h4><i class="icon fa fa-circle-o"></i> Error</h4>
                     <?php echo $this->session->flashdata('flash_message'); ?>
                  </div>
                  <?php } ?>
                  <?php if($this->uri->segment('4')){?>
                  <form class="form-horizontal" action="<?php echo base_url();?>university/department/edit/<?php if(isset($id)) echo $id;?>" method="post" name="addadmin">
                     <?php }else{?>
                  <form class="form-horizontal" action="<?php echo base_url();?>university/department/create" method="post" name="adduni">
                     <?php } ?>
                     <div class="box-body">
                        <div class="form-group">
                           <label for="department" class="col-sm-2 control-label">University *</label>
                           <div class="col-sm-10">
                              <select class="form-control" name="university" id="university" onchange="filter('campus');">
                                 <option value="">Select University</option>
                                 <?php
                                    if(isset($university_list) && !empty($university_list)){
                                    foreach ($university_list as $ulkey => $ulvalue) {
                                    ?>
                                 <option value="<?php echo $ulvalue['id'] ; ?>" <?php if(isset($selected_university_id) && $selected_university_id == $ulvalue['id']){ echo "selected"; } ?>><?php echo $ulvalue['name'] ; ?></option>
                                 <?php } } ?>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="department" class="col-sm-2 control-label">Campus *</label>
                           <div class="col-sm-10">
                              <select class="form-control" name="campus" id="campus" onchange="filter('college')">
                                 <option value="">Select Campus</option>
                                 <?php
                                    if(isset($campus_list) && !empty($campus_list)){
                                    foreach ($campus_list as $clkey => $clvalue) {
                                    ?>
                                 <option value="<?php echo $clvalue['id'] ; ?>" <?php if(isset($selected_campus_id) && $selected_campus_id == $clvalue['id']){ echo "selected"; } ?>><?php echo $clvalue['name'] ; ?></option>
                                 <?php } } ?>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="department" class="col-sm-2 control-label">College *</label>
                           <div class="col-sm-10">
                              <select class="form-control" name="college" id="college" onchange="filter('department')">
                                 <option value="">Select College</option>
                                 <?php
                                    if(isset($college_list) && !empty($college_list)){
                                    foreach ($college_list as $cgkey => $cgvalue) {
                                    ?>
                                 <option value="<?php echo $cgvalue['id'] ; ?>" <?php if(isset($selected_college_id) && $selected_college_id == $cgvalue['id']){ echo "selected"; } ?>><?php echo $cgvalue['name'] ; ?></option>
                                 <?php } } ?>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="name" class="col-sm-2 control-label">Name *</label>
                           <div class="col-sm-10">
                              <input type="name" class="form-control" id="name" name="name" placeholder="Department Name" value="<?php if(isset($name)) echo $name; ?>">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="contact_title" class="col-sm-2 control-label">Contact Title</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" id="contact_title" name="contact_title" placeholder="Contact Title" value="<?php if(isset($contact_title)) echo $contact_title; ?>">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="contact_name" class="col-sm-2 control-label">Contact Name</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" id="contact_name" name="contact_name" placeholder="Contact Name" value="<?php if(isset($contact_name)) echo $contact_name; ?>">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="contact_email" class="col-sm-2 control-label">Contact Email</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" id="contact_email" name="contact_email" placeholder="Contact Email" value="<?php if(isset($contact_email)) echo $contact_email; ?>">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="name" class="col-sm-2 control-label">Enrollment</label>
                           <div class="col-sm-10">
                              <?php
                                 if(isset($enrollment))
                                 {
                                     foreach($enrollment as $index => $enrollmentData)
                                     {
                                         ?>
                              <div  id="no_of_enrollments_<?=($index + 1)?>" <?=$index ? 'style="margin-top:10px;"' :''?>>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-3">
                                    <label for="enrollment_year" class="col-sm-2" style="padding-left: 0px;">Year</label>
                                    <input type="text" id="enrollment_year_<?=($index + 1)?>" name="enrollment_year[]" placeholder="Enrollment Year" value="<?=$enrollmentData['year']?>">
                                 </div>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-4">
                                    <label for="enrollment_numbers" class="col-sm-4" style="padding-left: 0px;">Number</label>
                                    <input type="text" id="enrollment_numbers_<?=($index + 1)?>" name="enrollment_numbers[]" placeholder="No of Enrollment" value="<?=$enrollmentData['number']?>">
                                 </div>
                                 <?php
                                    if(!$index)
                                    {
                                        ?>
                                 <div ><input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addEnrollment()"></div>
                                 <?php
                                    }
                                    else
                                    {
                                        ?>
                                 <div ><input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteEnrollment(<?=($index + 1)?>)"></div>
                                 <?php
                                    }
                                    ?>
                              </div>
                              <?php
                                 }
                                 }
                                 else
                                 {
                                 ?>
                              <div  id="no_of_enrollments_1">
                                 <div style="float:left; padding-left: 0px;" class="col-sm-3">
                                    <label for="enrollment_year" class="col-sm-2" style="padding-left: 0px;">Year</label>
                                    <input type="text" id="enrollment_year_1" name="enrollment_year[]" placeholder="Enrollment Year" value="<?php if(isset($enrollment)) echo $enrollment; ?>">
                                 </div>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-4">
                                    <label for="enrollment_numbers" class="col-sm-4" style="padding-left: 0px;">Number</label>
                                    <input type="text" id="enrollment_numbers_1" name="enrollment_numbers[]" placeholder="No of Enrollment" value="<?php if(isset($enrollment)) echo $enrollment; ?>">
                                 </div>
                                 <div ><input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addEnrollment()"></div>
                              </div>
                              <?php
                                 }
                                 ?>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="name" class="col-sm-2 control-label">College Research Expenditure</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" id="college_research_expenditure" name="college_research_expenditure" placeholder="College Research Expenditure" value="<?php if(isset($college_research_expenditure)) echo $college_research_expenditure; ?>">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="name" class="col-sm-2 control-label">Research Expenditure Per Faculty</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" id="research_expenditure_per_faculty" name="research_expenditure_per_faculty" placeholder="Research Expenditure Per Faculty" value="<?php if(isset($research_expenditure_per_faculty)) echo $research_expenditure_per_faculty; ?>">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="name" class="col-sm-2 control-label">Ranking</label>
                           <div class="col-sm-10">
                              <?php
                                 if(isset($ranking))
                                 {
                                     foreach($ranking as $index => $rankingData)
                                     {
                                         ?>
                              <div  id="ranking_<?=($index + 1)?>" <?=$index ? 'style="margin-top:10px;"' :''?>>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-3">
                                    <label for="ranking_year" class="col-sm-2" style="padding-left: 0px;" >Year</label>
                                    <input type="text" id="ranking_year_<?=($index + 1)?>" name="ranking_year[]" placeholder="Year" value="<?=$rankingData['year']?>">
                                 </div>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-4">
                                    <label for="ranking_by_whom" class="col-sm-4" style="padding-left: 0px;">By Whom</label>
                                    <input type="text" id="ranking_by_whom_<?=($index + 1)?>" name="ranking_by_whom[]" placeholder="By Whom" value="<?=$rankingData['by_whom']?>">
                                 </div>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-3">
                                    <label for="ranking" class="col-sm-3" style="padding-left: 0px;">Number</label>
                                    <input type="text" id="ranking_<?=($index + 1)?>" name="ranking[]" placeholder="Number" value="<?=$rankingData['ranking']?>">
                                 </div>
                                 <?php
                                    if(!$index)
                                    {
                                        ?>
                                 <div ><input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addRanking()"></div>
                                 <?php
                                    }
                                    else
                                    {
                                        ?>
                                 <div ><input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteRanking(<?=($index + 1)?>)"></div>
                                 <?php
                                    }
                                    ?>
                              </div>
                              <?php
                                 }
                                 }
                                 else
                                 {
                                 ?>
                              <div  id="ranking_1">
                                 <div style="float:left; padding-left: 0px;" class="col-sm-3">
                                    <label for="ranking_year" class="col-sm-2" style="padding-left: 0px;" >Year</label>
                                    <input type="text" id="ranking_year_1" name="ranking_year[]" placeholder="Year" value="<?php if(isset($enrollment)) echo $enrollment; ?>">
                                 </div>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-4">
                                    <label for="ranking_by_whom" class="col-sm-4" style="padding-left: 0px;">By Whom</label>
                                    <input type="text" id="ranking_by_whom_1" name="ranking_by_whom[]" placeholder="By Whom" value="<?php if(isset($enrollment)) echo $enrollment; ?>">
                                 </div>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-3">
                                    <label for="ranking" class="col-sm-3" style="padding-left: 0px;">Number</label>
                                    <input type="text" id="ranking_1" name="ranking[]" placeholder="Number" value="<?php if(isset($enrollment)) echo $enrollment; ?>">
                                 </div>
                                 <div ><input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addRanking()"></div>
                              </div>
                              <?php
                                 }
                                 ?>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="name" class="col-sm-2 control-label">Total No of Enrollments</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" id="total_enrollment" name="total_enrollment" placeholder="Total No of Enrollments" value="<?php if(isset($total_enrollment)) echo $total_enrollment; ?>">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="name" class="col-sm-2 control-label">Total No of International Enrollments</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" id="international_enrollment" name="international_enrollment" placeholder="Total No of International Enrollments" value="<?php if(isset($international_enrollment)) echo $international_enrollment; ?>">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="name" class="col-sm-2 control-label">Application Deadline (US Citizens)</label>
                           <div class="col-sm-10">
                              <?php
                                 if(isset($application_deadline_usa))
                                 {
                                     foreach($application_deadline_usa as $index => $deadlineUSAData)
                                     {
                                         ?>
                              <div  id="deadline_usa_<?=($index + 1)?>" <?=$index ? 'style="margin-top:10px;"' :''?>>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-3">
                                    <label for="deadline_usa_year" class="col-sm-2" style="padding-left: 0px;">Year</label>
                                    <input type="text" id="deadline_usa_year_<?=($index + 1)?>" name="deadline_usa_year[]" placeholder="Year" value="<?=$deadlineUSAData['year']?>">
                                 </div>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-4">
                                    <label for="deadline_usa_calendar" class="col-sm-4" style="padding-left: 0px;">Date</label>
                                    <input type="text" class="mws-datepicker small datepicker" id="deadline_usa_calendar_<?=($index + 1)?>" name="deadline_usa_calendar[]" value="<?=$deadlineUSAData['date']?>">
                                 </div>
                                 <?php
                                    if(!$index)
                                    {
                                        ?>
                                 <div ><input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addDeadlineUSA()"></div>
                                 <?php
                                    }
                                    else
                                    {
                                        ?>
                                 <div ><input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteDeadlineUSA(<?=($index + 1)?>)"></div>
                                 <?php
                                    }
                                    ?>
                              </div>
                              <?php
                                 }
                                 }
                                 else
                                 {
                                 ?>
                              <div  id="deadline_usa_1">
                                 <div style="float:left; padding-left: 0px;" class="col-sm-3">
                                    <label for="deadline_usa_year" class="col-sm-2" style="padding-left: 0px;">Year</label>
                                    <input type="text" id="deadline_usa_year_1" name="deadline_usa_year[]" placeholder="Year" value="<?php if(isset($enrollment)) echo $enrollment; ?>">
                                 </div>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-4">
                                    <label for="deadline_usa_calendar" class="col-sm-4" style="padding-left: 0px;">Date</label>
                                    <input type="text" class="mws-datepicker small datepicker" id="deadline_usa_calendar_1" name="deadline_usa_calendar[]" value="<?php if(isset($enrollment)) echo $enrollment; ?>">
                                 </div>
                                 <div ><input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addDeadlineUSA()"></div>
                              </div>
                              <?php
                                 }
                                 ?>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="name" class="col-sm-2 control-label">Application Deadline (International)</label>
                           <div class="col-sm-10">
                              <?php
                                 if(isset($application_deadline_international))
                                 {
                                     foreach($application_deadline_international as $index => $data)
                                     {
                                         ?>
                              <div  id="deadline_international_<?=($index + 1)?>" <?=$index ? 'style="margin-top:10px;"' :''?>>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-3">
                                    <label for="deadline_international_year" class="col-sm-2" style="padding-left: 0px;">Year</label>
                                    <input type="text" id="deadline_international_year_<?=($index + 1)?>" name="deadline_international_year[]" placeholder="Year" value="<?=$data['year']?>">
                                 </div>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-4">
                                    <label for="deadline_international_calendar" class="col-sm-4" style="padding-left: 0px;">Date</label>
                                    <input type="text" class="mws-datepicker small datepicker" id="deadline_international_calendar_<?=($index + 1)?>" name="deadline_international_calendar[]" value="<?=$data['date']?>">
                                 </div>
                                 <?php
                                    if(!$index)
                                    {
                                        ?>
                                 <div ><input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addDeadlineInternational()"></div>
                                 <?php
                                    }
                                    else
                                    {
                                        ?>
                                 <div ><input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteDeadlineInternational(<?=($index + 1)?>)"></div>
                                 <?php
                                    }
                                    ?>
                              </div>
                              <?php
                                 }
                                 }
                                 else
                                 {
                                 ?>
                              <div  id="deadline_international_1">
                                 <div style="float:left; padding-left: 0px;" class="col-sm-3">
                                    <label for="deadline_international_year" class="col-sm-2" style="padding-left: 0px;">Year</label>
                                    <input type="text" id="deadline_international_year_1" name="deadline_international_year[]" placeholder="Year" value="<?php if(isset($enrollment)) echo $enrollment; ?>">
                                 </div>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-4">
                                    <label for="deadline_international_calendar" class="col-sm-4" style="padding-left: 0px;">Date</label>
                                    <input type="text" class="mws-datepicker small datepicker" id="deadline_international_calendar_1" name="deadline_international_calendar[]" value="<?php if(isset($enrollment)) echo $enrollment; ?>">
                                 </div>
                                 <div ><input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addDeadlineInternational()"></div>
                              </div>
                              <?php
                                 }
                                 ?>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="admissions_department_name" class="col-sm-2 control-label">Admission Department Name</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" id="admissions_department_name" name="admissions_department_name" placeholder="Admission Department Name" value="<?php if(isset($admissions_department_name)) echo $admissions_department_name; ?>">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="department_phone" class="col-sm-2 control-label">Department Phone</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" id="department_phone" name="department_phone" placeholder="Department Phone" value="<?php if(isset($department_phone)) echo $department_phone; ?>">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="department_email" class="col-sm-2 control-label">Department Email</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" id="department_email" name="department_email" placeholder="Department Email" value="<?php if(isset($department_email)) echo $department_email; ?>">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="name" class="col-sm-2 control-label">Application Fees (US Citizens)</label>
                           <div class="col-sm-10">
                              <?php
                                 if(isset($application_fees_usa))
                                 {
                                     foreach($application_fees_usa as $index => $feesUSA)
                                     {
                                         ?>
                              <div  id="application_fees_usa_<?=($index + 1)?>" <?=$index ? 'style="margin-top:10px;"' :''?>>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-4">
                                    <label for="application_fees_usa_currency" class="col-sm-3" style="padding-left: 0px;">Currency</label>
                                    <input type="text" id="application_fees_usa_currency_<?=($index + 1)?>" name="application_fees_usa_currency[]" placeholder="Currency" value="<?=$feesUSA['currency']?>">
                                 </div>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-4">
                                    <label for="application_fees_usa_amount" class="col-sm-4" style="padding-left: 0px;">Amount</label>
                                    <input type="text" id="application_fees_usa_amount_<?=($index + 1)?>" name="application_fees_usa_amount[]" placeholder="Amount" value="<?=$feesUSA['amount']?>">
                                 </div>
                                 <?php
                                    if(!$index)
                                    {
                                        ?>
                                 <div ><input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addFeesUSA()"></div>
                                 <?php
                                    }
                                    else
                                    {
                                        ?>
                                 <div ><input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteFeesUSA(<?=($index + 1)?>)"></div>
                                 <?php
                                    }
                                    ?>
                              </div>
                              <?php
                                 }
                                 }
                                 else
                                 {
                                 ?>
                              <div  id="application_fees_usa_1">
                                 <div style="float:left; padding-left: 0px;" class="col-sm-4">
                                    <label for="application_fees_usa_currency" class="col-sm-3" style="padding-left: 0px;">Currency</label>
                                    <input type="text" id="application_fees_usa_currency_1" name="application_fees_usa_currency[]" placeholder="Currency" value="<?php if(isset($enrollment)) echo $enrollment; ?>">
                                 </div>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-4">
                                    <label for="application_fees_usa_amount" class="col-sm-4" style="padding-left: 0px;">Amount</label>
                                    <input type="text" id="application_fees_usa_amount_1" name="application_fees_usa_amount[]" placeholder="Amount" value="<?php if(isset($enrollment)) echo $enrollment; ?>">
                                 </div>
                                 <div ><input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addFeesUSA()"></div>
                              </div>
                              <?php
                                 }
                                 ?>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="name" class="col-sm-2 control-label">Application Fees (International)</label>
                           <div class="col-sm-10">
                              <?php
                                 if(isset($application_fees_international))
                                 {
                                     foreach($application_fees_international as $index => $feesInternational)
                                     {
                                         ?>
                              <div  id="application_fees_international_<?=($index + 1)?>" <?=$index ? 'style="margin-top:10px;"' :''?>>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-4">
                                    <label for="application_fees_international_currency" class="col-sm-3" style="padding-left: 0px;">Currency</label>
                                    <input type="text" id="application_fees_international_currency_<?=($index + 1)?>" name="application_fees_international_currency[]" placeholder="Currency" value="<?=$feesInternational['currency']?>">
                                 </div>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-4">
                                    <label for="application_fees_international_amount" class="col-sm-4" style="padding-left: 0px;">Amount</label>
                                    <input type="text" id="application_fees_international_amount_<?=($index + 1)?>" name="application_fees_international_amount[]" placeholder="Amount" value="<?=$feesInternational['amount']?>">
                                 </div>
                                 <?php
                                    if(!$index)
                                    {
                                        ?>
                                 <div ><input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addFeesInternational()"></div>
                                 <?php
                                    }
                                    else
                                    {
                                        ?>
                                 <div ><input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteFeesInternational(<?=($index + 1)?>)"></div>
                                 <?php
                                    }
                                    ?>
                              </div>
                              <?php
                                 }
                                 }
                                 else
                                 {
                                 ?>
                              <div  id="application_fees_international_1">
                                 <div style="float:left; padding-left: 0px;" class="col-sm-4">
                                    <label for="application_fees_international_currency" class="col-sm-3" style="padding-left: 0px;">Currency</label>
                                    <input type="text" id="application_fees_international_currency_1" name="application_fees_international_currency[]" placeholder="Currency" value="<?php if(isset($enrollment)) echo $enrollment; ?>">
                                 </div>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-4">
                                    <label for="application_fees_international_amount" class="col-sm-4" style="padding-left: 0px;">Amount</label>
                                    <input type="text" id="application_fees_international_amount_1" name="application_fees_international_amount[]" placeholder="Amount" value="<?php if(isset($enrollment)) echo $enrollment; ?>">
                                 </div>
                                 <div ><input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addFeesInternational()"></div>
                              </div>
                              <?php
                                 }
                                 ?>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="admissions_department_url" class="col-sm-2 control-label">Admission Department URL</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" id="admissions_department_url" name="admissions_department_url" placeholder="Admission Department URL" value="<?php if(isset($admissions_department_url)) echo $admissions_department_url; ?>">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="noof_faculty" class="col-sm-2 control-label">Number Of Faculty</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" id="noof_faculty" name="noof_faculty" placeholder="Number Of Faculty" value="<?php if(isset($noof_faculty)) echo $noof_faculty; ?>">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="name" class="col-sm-2 control-label">Average GRE</label>
                           <div class="col-sm-10">
                              <?php
                                 if(isset($average_gre))
                                 {
                                     foreach($average_gre as $index => $greData)
                                     {
                                         ?>
                              <div  id="average_gre_<?=($index + 1)?>" <?=$index ? 'style="margin-top:10px;"' :''?>>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-3">
                                    <label for="average_gre_quant" class="col-sm-2" style="padding-left: 0px;" >Quant</label>
                                    <input type="text" id="average_gre_<?=($index + 1)?>" name="average_gre_quant[]" placeholder="Quant" value="<?=$greData['quant']?>">
                                 </div>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-4">
                                    <label for="average_gre_verbal" class="col-sm-4" style="padding-left: 0px;">Verbal</label>
                                    <input type="text" id="average_gre_verbal_<?=($index + 1)?>" name="average_gre_verbal[]" placeholder="Verbal" value="<?=$greData['verbal']?>">
                                 </div>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-3">
                                    <label for="average_gre_awa" class="col-sm-3" style="padding-left: 0px;">AWA</label>
                                    <input type="text" id="average_gre_awa_<?=($index + 1)?>" name="average_gre_awa[]" placeholder="AWA" value="<?=$greData['awa']?>">
                                 </div>
                                 <?php
                                    /*
                                    if(!$index)
                                    {
                                        ?>
                                 <div ><input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addAverageGRE()"></div>
                                 <?php
                                    }
                                    else
                                    {
                                        ?>
                                 <div ><input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteAverageGRE(<?=($index + 1)?>)"></div>
                                 <?php
                                    }
                                    */
                                    ?>
                              </div>
                              <?php
                                 }
                                 }
                                 else
                                 {
                                 ?>
                              <div  id="average_gre_1">
                                 <div style="float:left; padding-left: 0px;" class="col-sm-3">
                                    <label for="average_gre_quant" class="col-sm-2" style="padding-left: 0px;" >Quant</label>
                                    <input type="text" id="average_gre_quant_1" name="average_gre_quant[]" placeholder="Quant" value="">
                                 </div>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-4">
                                    <label for="average_gre_verbal" class="col-sm-4" style="padding-left: 0px;">Verbal</label>
                                    <input type="text" id="average_gre_verbal_1" name="average_gre_verbal[]" placeholder="Verbal" value="">
                                 </div>
                                 <div style="float:left; padding-left: 0px;" class="col-sm-3">
                                    <label for="average_gre_awa" class="col-sm-3" style="padding-left: 0px;">AWA</label>
                                    <input type="text" id="average_gre_awa_1" name="average_gre_awa[]" placeholder="AWA" value="">
                                 </div>
                                 <div ><input type="button" name="add" value="Add" style="margin-left: 20px;" onclick="addAverageGRE()"></div>
                              </div>
                              <?php
                                 }
                                 ?>
                           </div>
                        </div>
                     </div>
                     <!-- /.box-body -->
                     <div class="box-footer">
                        <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                        <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                        <?php
                           if($this->uri->segment('4'))
                           {
                               ?>
                        <button type="submit" class="btn btn-info pull-right">Update</button>
                        <?php
                           }
                           else
                           {
                               ?>
                        <button type="submit" class="btn btn-info pull-right">Create</button>
                        <?php
                           }
                           ?>
                     </div>
                     <!-- /.box-footer -->
                  </form>
             </div>
        </div> 
      </section>
      <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
</div>
<script type="text/javascript">
   var totalEnrollment = <?=$enrollment_count?>;
   function addEnrollment(){
       var html = '<div id="no_of_enrollments_' + (totalEnrollment + 1) + '" style="margin-top:10px;"><div style="float:left; padding-left: 0px;" class="col-sm-3"><label for="enrollment_year" class="col-sm-2" style="padding-left: 0px;">Year</label><input type="text" id="enrollment_year_' + (totalEnrollment + 1) + '" name="enrollment_year[]" placeholder="Enrollment Year" value=""></div><div style="float:left; padding-left: 0px;" class="col-sm-4"><label for="enrollment_numbers" class="col-sm-4" style="padding-left: 0px;">Number</label><input type="text" id="enrollment_numbers_' + (totalEnrollment + 1) + '" name="enrollment_numbers[]" placeholder="No of Enrollment" value=""></div><div ><input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteEnrollment(' + (totalEnrollment + 1) + ')"></div></div>';
   
       $("#no_of_enrollments_" + totalEnrollment).after(html);
       totalEnrollment++;
   }
   
   function deleteEnrollment(enrollmentId){
       $("#no_of_enrollments_" + enrollmentId).remove();
       totalEnrollment--;
   }
   
   var totalRanking = <?=$ranking_count?>;
   function addRanking(){
       var html = '<div  id="ranking_' + (totalRanking + 1) + '" style="margin-top:10px;"><div style="float:left; padding-left: 0px;" class="col-sm-3"><label for="ranking_year" class="col-sm-2" style="padding-left: 0px;" >Year</label><input type="text" id="ranking_year_' + (totalRanking + 1) + '" name="ranking_year[]" placeholder="Year" value=""></div><div style="float:left; padding-left: 0px;" class="col-sm-4"><label for="ranking_by_whom" class="col-sm-4" style="padding-left: 0px;">By Whom</label><input type="text" id="ranking_by_whom_' + (totalRanking + 1) + '" name="ranking_by_whom[]" placeholder="By Whom" value=""></div><div style="float:left; padding-left: 0px;" class="col-sm-3"><label for="ranking" class="col-sm-3" style="padding-left: 0px;">Number</label><input type="text" id="ranking_' + (totalRanking + 1) + '" name="ranking[]" placeholder="Number" value=""></div><div ><input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteRanking(' + (totalRanking + 1) + ')"></div></div>';
   
       $("#ranking_" + totalRanking).after(html);
       totalRanking++;
   }
   
   function deleteRanking(rankingId){
       $("#ranking_" + rankingId).remove();
       totalRanking--;
   }
   
   var totalUSA = <?=$application_deadline_usa_count?>;
   function addDeadlineUSA(){
       var html = '<div  id="deadline_usa_' + (totalUSA + 1) + '" style="margin-top:10px;"><div style="float:left; padding-left: 0px;" class="col-sm-3"><label for="deadline_usa_year" class="col-sm-2" style="padding-left: 0px;">Year</label><input type="text" id="deadline_usa_year_' + (totalUSA + 1) + '" name="deadline_usa_year[]" placeholder="Year" value=""></div><div style="float:left; padding-left: 0px;" class="col-sm-4"><label for="deadline_usa_calendar" class="col-sm-4" style="padding-left: 0px;">Date</label><input type="text" class="mws-datepicker small datepicker" id="deadline_usa_calendar_' + (totalUSA + 1) + '" name="deadline_usa_calendar[]" value=""></div><div ><input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteDeadlineUSA(' + (totalUSA + 1) + ')"></div></div>';
   
       $("#deadline_usa_" + totalUSA).after(html);
       totalUSA++;
   }
   
   function deleteDeadlineUSA(deadlineUSAId){
       $("#deadline_usa_" + deadlineUSAId).remove();
       totalUSA--;
   }
   
   var totalInternational = <?=$application_deadline_international_count?>;
   function addDeadlineInternational(){
       var html = '<div  id="deadline_international_' + (totalInternational + 1) + '" style="margin-top:10px;"><div style="float:left; padding-left: 0px;" class="col-sm-3"><label for="deadline_international_year" class="col-sm-2" style="padding-left: 0px;">Year</label><input type="text" id="deadline_international_year_' + (totalInternational + 1) + '" name="deadline_international_year[]" placeholder="Year" value=""></div><div style="float:left; padding-left: 0px;" class="col-sm-4"><label for="deadline_international_calendar" class="col-sm-4" style="padding-left: 0px;">Date</label><input type="text" class="mws-datepicker small datepicker" id="deadline_international_calendar_' + (totalInternational + 1) + '" name="deadline_international_calendar[]" value=""></div><div ><input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteDeadlineInternational(' + (totalInternational + 1) + ')"></div></div>';
   
       $("#deadline_international_" + totalInternational).after(html);
       totalInternational++;
   }
   
   function deleteDeadlineInternational(deadlineUSAId){
       $("#deadline_international_" + deadlineUSAId).remove();
       totalInternational--;
   }
   
   var totalFeesUSA = <?=$application_fees_usa_count?>;
   function addFeesUSA(){
       var html = '<div id="application_fees_usa_' + (totalFeesUSA + 1) + '" style="margin-top:10px;"><div style="float:left; padding-left: 0px;" class="col-sm-4"><label for="application_fees_usa_currency" class="col-sm-3" style="padding-left: 0px;">Currency</label><input type="text" id="application_fees_usa_currency_' + (totalFeesUSA + 1) + '" name="application_fees_usa_currency[]" placeholder="Currency" value=""></div><div style="float:left; padding-left: 0px;" class="col-sm-4"><label for="application_fees_usa_amount" class="col-sm-4" style="padding-left: 0px;">Amount</label><input type="text" id="application_fees_usa_amount_' + (totalFeesUSA + 1) + '" name="application_fees_usa_amount[]" placeholder="Amount" value=""></div><div ><input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteFeesUSA(' + (totalFeesUSA + 1) + ')"></div></div>';
   
       $("#application_fees_usa_" + totalFeesUSA).after(html);
       totalFeesUSA++;
   }
   
   function deleteFeesUSA(feesUSAId){
       $("#application_fees_usa_" + feesUSAId).remove();
       totalFeesUSA--;
   }
   
   var totalFeesInternational = <?=$application_fees_international_count?>;
   function addFeesInternational(){
       var html = '<div id="application_fees_international_' + (totalFeesInternational + 1) + '" style="margin-top:10px;"><div style="float:left; padding-left: 0px;" class="col-sm-4"><label for="application_fees_international_currency" class="col-sm-3" style="padding-left: 0px;">Currency</label><input type="text" id="application_fees_international_currency_' + (totalFeesInternational + 1) + '" name="application_fees_international_currency[]" placeholder="Currency" value=""></div><div style="float:left; padding-left: 0px;" class="col-sm-4"><label for="application_fees_international_amount" class="col-sm-4" style="padding-left: 0px;">Amount</label><input type="text" id="application_fees_international_amount_' + (totalFeesInternational + 1) + '" name="application_fees_international_amount[]" placeholder="Amount" value=""></div><div ><input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteFeesInternational(' + (totalFeesInternational + 1) + ')"></div></div>';
   
       $("#application_fees_international_" + totalFeesInternational).after(html);
       totalFeesInternational++;
   }
   
   function deleteFeesInternational(feesInternationalId){
       $("#application_fees_international_" + feesInternationalId).remove();
       totalFeesInternational--;
   }
   
   var totalAverageGRE = <?=$average_gre_count?>;
   function addAverageGRE(){
       var html = '<div  id="average_gre_' + (totalRanking + 1) + '" style="margin-top:10px;"><div style="float:left; padding-left: 0px;" class="col-sm-3"><label for="average_gre_quant" class="col-sm-2" style="padding-left: 0px;" >Quant</label><input type="text" id="average_gre_quant_' + (totalRanking + 1) + '" name="average_gre_quant[]" placeholder="Quant" value=""></div><div style="float:left; padding-left: 0px;" class="col-sm-4"><label for="average_gre_verbal" class="col-sm-4" style="padding-left: 0px;">Verbal</label><input type="text" id="average_gre_verbal_' + (totalRanking + 1) + '" name="average_gre_verbal[]" placeholder="Verbal" value=""></div><div style="float:left; padding-left: 0px;" class="col-sm-3"><label for="average_gre_awa" class="col-sm-3" style="padding-left: 0px;">AWA</label><input type="text" id="average_gre_awa_' + (totalRanking + 1) + '" name="average_gre_awa[]" placeholder="AWA" value=""></div><div ><input type="button" name="add" value="Delete" style="margin-left: 20px;" onclick="deleteAverageGRE(' + (totalRanking + 1) + ')"></div></div>';
   
       $("#average_gre_" + totalAverageGRE).after(html);
       totalAverageGRE++;
   }
   
   function deleteAverageGRE(averageGREId){
       $("#average_gre_" + averageGREId).remove();
       totalAverageGRE--;
   }
</script>
<script type="text/javascript">
   function filter(entity_type){
   
     var entityId = 0 ;
     if(entity_type == "campus"){
       entityId = $('#university').val();
     }
   
     if(entity_type == "college"){
       entityId = $('#campus').val();
     }
   
     sataString = {
       "entity_id" : entityId,
       "entity_type" : entity_type
     }
   
     $("#ajaxSpinnerImage").show();
   
     $.ajax({
   
     type: "POST",
   
     url: "/admin/filtercontroller/commonDropdown",
   
     data: sataString,
   
     cache: false,
   
     success: function(result){
   
       $("#ajaxSpinnerImage").hide();
       document.getElementById(entity_type).innerHTML = result;
   
     }
   
   });
   
   }
</script>