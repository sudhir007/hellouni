<script type="text/javascript" src="<?php echo base_url();?>source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>source/jquery.fancybox.css?v=2.1.5" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style.css?v=1.1.0" media="screen" />
<script src="<?php echo base_url();?>tabs/assets/js/responsive-tabs.min.js"></script>

<script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<style>
   .cd-tabs-content li {
   font-size: 15px;
   line-height: 1.5;
   margin:0 0 10px;
   }
   .cd-tabs-content li p {
   font-size: 15px;
   line-height: 1.5;
   color:#242425 ;
   margin-bottom: 1em;
   }
   .shade_border{
   background:url(img/shade1.png) repeat-x; height:11px;
   }
   .search_by{
   background-color:#f3f4f4; box-shadow:5px 5px 5px #eae8e8; height:auto;
   padding-top: 10px;
   padding-left: 30px;
   padding-right: 30px;
   }
   .search_by h4{
   color:#3c3c3c;
   font-family:lato_regular;
   font-weight:500;
   }
   .table_heading{
   padding:5px;
   font-weight:bold;
   }
   .more_detail{
   background-color:#F6881F;
   text-transform:uppercase;
   height:28px;
   border:0px;
   color:#000;
   border-radius:2px;
   width:25px;
   }
   .right_side_form{
   background:#3c3c3c;
   box-shadow:5px 5px 2px #CCCCCC;
   height:auto;
   margin-top:40px;
   margin-left:10px;
   color:#ffffff;
   }
   label{
   color:#fff;
   }
   .more_detail{
   background-color:#F6881F;
   text-transform:uppercase;
   height:28px;
   border:0px;
   color:#fff;
   border-radius:2px;
   }
   label{
   font-size:11px;
   font-weight:300;
   }
   .total{
   background-image:url(images/tab.png);
   width:65px;
   height:41px;
   float:left;
   }
   .total_text{
   color:#7e7e7e;
   padding-top:2px;
   font-size:12px;
   }
   .total_text2{
   color:#5b8abe;
   font-size:12px;
   }
   .people{
   float:left;
   padding:10px;
   height:30px
   }
   .font_color{
   margin-left:-13px;
   }
   .font_color h4{
   color:#374552;
   }
   .fancy_box{
   border: 1px solid #64717d ;
   margin-top:5px;
   margin-left:5px;
   margin-right:40px;
   padding:5px;
   }
   .cd-tabs h4{
   margin: 16px 0 16px;
   text-transform: uppercase;
   font-family: lato_regular;
   font-weight: 500;
   letter-spacing: 1px;
   color: #374552;
   }
   .font_class{
   font-family: 'Open Sans' !important;
   font-size: 12px !important;
   letter-spacing: 1px !important;
   width:100%;
   height:40px;
   }
   .cd-tabs-navigation a{
   font-family: lato_bold;
   font-size: 11px;
   }
   .uni_logo{
   /*top:-155px;*/
  /* position:absolute;
   margin-left:-15px;*/
   }
   .uni_logo img{
   padding:7px;background:#fff;
   }
   .header_icons{
   }
   .header_icons ul li{
   display:inline-block;
   list-style-type:none;
   margin-bottom:-10px;
   }
   .form-horizontal {
   font-size:15px;
   }
   .selected{
   text-align: justify;
   }
   .accordion {
   background-color: #eee;
   color: #444;
   cursor: pointer;
   padding: 18px;
   /*width: 100%;*/
   border: none;
   text-align: left;
   outline: none;
   font-size: 15px;
   transition: 0.4s;
   }
   .active, .accordion:hover {
   background-color: #ccc;
   }
   .panel {
   padding: 0 18px;
   display: none;
   background-color: white;
   overflow: hidden;
   }
   table {
   font-family: arial, sans-serif;
   border-collapse: collapse;
   width: 100%;
   }
   td, th {
   border: 1px solid #dddddd;
   text-align: left;
   padding: 6px;
   }
   th{
   font-weight: bold;
   }
   tr:nth-child(even) {
   background-color: #dddddd;
   }
   .nav-tabs-dropdown {
   display: none;
   border-bottom-left-radius: 0;
   border-bottom-right-radius: 0;
   }
   .nav-tabs-dropdown:before {
   content: "\e114";
   font-family: 'Glyphicons Halflings';
   position: absolute;
   right: 30px;
   }
   @media screen and (min-width: 769px) {
   #nav-tabs-wrapper {
   display: block!important;
   }
   }
   @media screen and (max-width: 768px) {
   .nav-tabs-dropdown {
   display: block;
   }
   #nav-tabs-wrapper {
   /*display: none;*/
   border-top-left-radius: 0;
   border-top-right-radius: 0;
   text-align: center;
   }
   }
   @media (min-width: 768px){
   #compare-university-modal, #course-list-modal{
   height: auto;
   overflow: auto;
   max-height: 570px;
   }
   }
   @media (min-width: 1440px){
   #compare-university-modal, #course-list-modal {
   height: auto;
   overflow: auto;
   max-height: 680px;
   }
   }
   .nav-pills > li {
   background: #374552;
   text-transform: uppercase;
   }
   .nav > li > a:hover, .nav > li > a:focus {
   color: #FF9900!important;
   }
   .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
   color: #ffffff!important;
   background-color: #FF9900;
   outline: none;
   padding: 10px;
   /* color: #888; */
   text-decoration: none;
   text-transform: uppercase;
   /* background: #eee; */
   border: none !important;
   border-radius: 0px;
   }
   .nav-pills>li>a {
   border-radius: 0px;
   }
   .nav-pills>li>a:hover {
   background-color: #8c57c4 !important;
   color: white!important;
   }
   .well {
   padding:15px;
   }
   .panel-title {
   font-weight:bold;
   }
   /* The Modal (background) */
   .modal, .coursemodal {
   display: none; /* Hidden by default */
   position: fixed; /* Stay in place */
   z-index: 1; /* Sit on top */
   padding-top: 100px; /* Location of the box */
   left: 0;
   top: 0;
   width: 100%; /* Full width */
   height: 100%; /* Full height */
   overflow: auto; /* Enable scroll if needed */
   background-color: rgb(0,0,0); /* Fallback color */
   background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
   }
   /* Modal Content */
   .modal-content  {
   background-color: #fefefe;
   margin: auto;
   padding: 20px;
   border: 1px solid #888;
   width: 50%;
   }
   .coursemodal-content  {
   background-color: #fefefe;
   margin: auto;
   padding: 20px;
   border: 1px solid #888;
   width: 80%;
   }
   /* The Close Button */
   .close {
   color: #8e0f0f;
   float: right;
   font-size: 28px;
   font-weight: bold;
   }


   .close:hover,
   .close:focus {
   color: #000;
   text-decoration: none;
   cursor: pointer;
   }
   .courseclose:hover,
   .courseclose:focus {
   color: #000;
   text-decoration: none;
   cursor: pointer;
   }
   #compare-university-modal, #course-list-modal p{
   font-size: 16px;
   text-align: center;
   font-weight: bold;
   margin: 0 0 25px;
   }
   #compare-university-form input[type='checkbox']{
   float: right;
   }
   #compare-university-form label{
   color:#000;
   font-size: 13px;
   margin-top: 10px;
   }
   .compare_universities_submit{
   text-align: center;
   font-size: 15px;
   }
   #compare-university-form input[type='submit']{
   padding: 5px;
   }
   @import url(https://fonts.googleapis.com/css?family=PT+Sans+Narrow);
   .sectionClass {
   padding: 10px 0px 50px 0px;
   position: relative;
   display: block;
   }
   .fullWidth {
   width: 100% !important;
   display: table;
   float: none;
   padding: 0;
   min-height: 1px;
   height: 100%;
   position: relative;
   }
   .sectiontitle {
   background-position: center;
   margin: 30px 0 0px;
   text-align: center;
   min-height: 20px;
   }
   .sectiontitle h2 {
   font-size: 30px;
   color: #222;
   margin-bottom: 0px;
   padding-right: 10px;
   padding-left: 10px;
   }
   .headerLine {
   width: 160px;
   height: 2px;
   display: inline-block;
   background: #101F2E;
   }
   .projectFactsWrap{
   display: flex;
   flex-direction: row;
   flex-wrap: wrap;
   background-image: url("<?php echo base_url();?>application/images/counters.png");
   }
   #projectFacts .fullWidth{
   padding: 0;
   }
   .projectFactsWrap .item{
   width: 33%;
   /* height: 100%; */
   padding: 10px 5px 10px 5px;
   text-align: center;
  /* border-radius: 15px;
   margin: 4px 8px 10px 8px;*/
   }
   .projectFactsWrap .item:nth-child(1){
   background: rgba(135, 3, 197, 0.5686274509803921);
   }
   .projectFactsWrap .item:nth-child(2){
    background: rgba(246, 135, 44, 0.5686274509803921);
   }
   .projectFactsWrap .item:nth-child(3){
    background: rgba(135, 3, 197, 0.5686274509803921);
   }
   .projectFactsWrap .item:nth-child(4){
   background: rgba(246, 135, 44, 0.5686274509803921);
   }
   .projectFactsWrap .item:nth-child(5){
    background: rgba(135, 3, 197, 0.5686274509803921);
   }
   .projectFactsWrap .item:nth-child(6){
   background: rgba(246, 135, 44, 0.5686274509803921);
   }
   .projectFactsWrap .item p.number{
   font-size: 40px;
   padding: 0;
   font-weight: bold;
   }
   .projectFactsWrap .item p{
   color: white;
   font-size: 25px;
   margin: 0;
   padding: 10px;
   font-family: 'Open Sans';
   }
   .projectFactsWrap .item span{
   width: 60px;
   background: rgb(246, 136, 44);
   height: 2px;
   display: block;
   margin: 0 auto;
   }
   .projectFactsWrap .item i{
   vertical-align: middle;
   font-size: 40px;
   color: rgb(246, 136, 44);
   /*background: rgb(55, 69, 82);*/
   }
   .projectFactsWrap .item:hover i, .projectFactsWrap .item:hover p{
   /*background: rgb(55, 69, 82);*/
   color: white;
   }
   .projectFactsWrap .item:hover span{
   background: rgb(55, 69, 82);
   }
   @media (max-width: 786px){
   .projectFactsWrap .item {
   flex: 0 0 50%;
   }
   }
   .trans{
   opacity: 1;
   -webkit-transform: translateX(0px);
   transform: translateX(0px);
   -webkit-transition: all 500ms ease;
   -moz-transition: all 500ms ease;
   transition: all 500ms ease;
   }
   @media screen and (max-width: 768px) {
   .authorWindow{
   width: 210px;
   }
   .authorWindowWrapper{
   bottom: -170px;
   margin-bottom: 20px;
   }
   }
   /*cards*/
   .card{
   border-radius: 4px;
   background: #fff;
   box-shadow: 0 6px 10px rgba(0,0,0,.08), 0 0 6px rgba(0,0,0,.05);
   transition: .3s transform cubic-bezier(.155,1.105,.295,1.12),.3s box-shadow,.3s -webkit-transform cubic-bezier(.155,1.105,.295,1.12);
   padding: 0px 15px 0px 14px;
   cursor: pointer;
  min-height: 440px;

   }

   .cardInfo{
    padding: 0px 15px 5px 15px;
   }
   .card:hover{
   transform: scale(1.05);
   box-shadow: 0 10px 20px rgba(0,0,0,.12), 0 4px 8px rgba(0,0,0,.06);
   }
   .card h3{
   font-weight: 600;
   }
   .cardimg{
    padding: 20px;
    text-align: center;
    background-color: #8703c5;
   }
   /*.card img{
   position: absolute;
   top: 20px;
   right: 15px;
   max-height: 120px;
   }*/
   /*.card-1{
   background-image: url(<?php echo base_url();?>application/images/degree.png);
   background-repeat: no-repeat;
   background-position: right;
   background-size: 100px;
   padding: 4px 75px 15px 15px;
   min-height: 220px;
   }
   .card-2{
   background-image: url(<?php echo base_url();?>application/images/campus.png);
   background-repeat: no-repeat;
   background-position: right;
   background-size: 112px;
   padding: 4px 75px 15px 15px;
   min-height: 220px;
   }
   .card-3{
   background-image: url(<?php echo base_url();?>application/images/spending.png);
   background-repeat: no-repeat;
   background-position: right;
   background-size: 100px;
   padding: 4px 75px 15px 15px;
   min-height: 220px;
   }*/
   @media(max-width: 990px){
   .card{
   margin: 20px;
   }
   }
   .cardCss{
   border-radius: 4px;
    background: #fff;
    box-shadow: 0 4px 13px rgba(0,0,0,.08), 0 0 6px rgba(0,0,0,.05);
    margin: 10px 1px 10px 1px;
    border-right: 8px solid #f6872c;
   }
   /*entry req table*/
   #enquryTable td, #enquryTable th {
   border: none;
   padding: 8px;
   text-align: center;
   background-color: white;
   border-bottom: 1px solid #f7f7d8;
   vertical-align: middle;
   }
   #enquryTable tr:nth-child(even){background-color: #f2f2f2;}
   #enquryTable tr:hover {background-color: #ddd;}
   #enquryTable th {
   padding: 12px;
   text-align: center;
   background-color: #01182d;
   letter-spacing: 1px;
   color: white;
   vertical-align: middle;
   }
   .canvasjs-chart-credit{
   display: none !important;
   }
   /*title box*/
   /*.why-edit-box {
   background:#fff;
   padding:37px;
   box-shadow:0 0 10px 0 #ddd;
   margin-bottom: 10px;
   height: 100%;
   margin-top: 20px;
   }
   .why-edit-box h3 {
   margin:0;
   padding-top:10px;
   line-height: 1.1em;
   color: #14365C;
   font-family: 'Open Sans',Arial,sans-serif;
   font-weight: 600;
   }
   .why-edit-box h3::after {
   position: absolute;
   content: '';
   height: 7px;
   margin: 0 auto;
   left: 0;
   top: 2.5%;
   width: 50%;
   background: #f79418;
   }
   .why-edit-box p {
   margin: 0;
   margin-top: 0px;
   font-size: 15px;
   line-height: 26px;
   margin-top: 20px;
   }*/
   /*uni image carousel*/
   .hide-bullets {
   list-style:none;
   margin-left: -40px;
   margin-top:20px;
   }
   .thumbnail {
   padding: 0;
   }
   .carousel-inner>.item>img, .carousel-inner>.item>a>img {
   width: 100%;
   }
   .col-sm-3 a {
   border: 1px solid transparent;
   border-radius: 0;
   transition: all 3s ease;
   }
   .col-sm-3 a:hover {
   border: 1px solid #ff4647;
   border-radius: 100% 60% / 30% 10%;
   background: linear-gradient(rgba(56,123,131,0.7),rgba(56,123,131,0.7));
   }
   /*College Box*/
   .box>.icon {
   text-align: center;
   position: relative;
   }
   .box>.icon>.image {
   position: relative;
   z-index: 2;
   margin: auto;
   width: 88px;
   height: 88px;
   border: 8px solid white;
   line-height: 88px;
   border-radius: 50%;
   background: #f6882c;
   vertical-align: middle;
   }
   .box>.icon:hover>.image {
   background: #333;
   }
   .box>.icon>.image>i {
   font-size: 36px !important;
   color: #fff !important;
   }
   .box>.icon:hover>.image>i {
   color: white !important;
   }
   .box>.icon>.info {
   margin-top: -24px;
   /*background: rgba(0, 0, 0, 0.04);
   border: 1px solid #e0e0e0;*/
   box-shadow: 0px 2px 4px #b4b4bf;
   padding: 15px 1px 10px 1px;
   min-height: 500px;
   }
   .box>.icon:hover>.info {
   background: rgba(0, 0, 0, 0.04);
   border-color: #e0e0e0;
   color: white;
   }
   .box>.icon>.info>h3.title {
   font-family: "Roboto", sans-serif !important;
   font-size: 16px;
   color: #222;
   font-weight: 500;
   }
   .box>.icon>.info>p {
   font-family: "Roboto", sans-serif !important;
   font-size: 13px;
   color: #666;
   line-height: 1.5em;
   margin: 20px;
   }
   .box>.icon:hover>.info>h3.title,
   .box>.icon:hover>.info>p,
   .box>.icon:hover>.info>.more>a {
   color: #222;
   }
   .box>.icon>.info>.more a {
   font-family: "Roboto", sans-serif !important;
   font-size: 12px;
   color: #222;
   line-height: 12px;
   text-transform: uppercase;
   text-decoration: none;
   }
   .box>.icon:hover>.info>.more>a {
   color: white;
   padding: 6px 8px;
   background-color: #f6882c;
   }
   .box .space {
   height: 30px;
   }
   /*top stick*/
   .headerTop {

    padding: 10px 16px;
    background: #f6882c;
    color: #000000;
    box-shadow: 0px 3px 2px #cecccc;
    margin-bottom: 15px;
    position: relative;
    height: -webkit-fill-available;

    width: 100vw;
    margin-left: calc(-49vw + 48% - -3px);

    /*padding: 5% 10% 3% 10%;*/


   }

   .parallax-wrapper {
    position: relative;
        margin-top: 3%;
    }

    .parallax-container {
        /*height: 200px;
        background-color: #0f0;
        width: 100vw;
        margin-left: calc(-50vw + 50% - 8px);
        position: relative;*/
        height: -webkit-fill-available;
    /* background-color: #0f0; */
    width: 100vw;
    margin-left: calc(-49vw + 48% - -3px);
    position: relative;
    padding: 5% 10% 3% 10%;
        background-repeat: round;
    background-image: url("<?php echo base_url();?>application/images/framephoto.png");
    }

    .parallax-container h4{
      color: white;
        /* line-height: 31px; */
        line-height: 1.3;
        text-transform: initial;
    }
   .content {
   padding: 16px;
   }
   .sticky {
   position: fixed;
    top: 0;
    /* width: 100%; */
    z-index: 10;
    left: 0;
    right: 0;
    padding: 10px 100px 0px 100px;
    text-align: center;
    height: unset;
    margin-left: 0vw;
   }

   @media only screen and (max-width: 600px) {
      .sticky {

    padding: 0px !important;

   }
   }
   .sticky + .content {
   padding-top: 102px;
   }
   .vl {
   border-right: 2px solid #f7942e;
   height: auto;
   min-height: 100px;
   }
   .pTag{
   margin: 0 0 5px !important;
   font-size: 15px !important;
   }
   .wrapper {
   position: relative;
   width: 330px;
   height: 80px;
   -moz-user-select: none;
   -webkit-user-select: none;
   -ms-user-select: none;
   user-select: none;
   float: right;
   /*margin-left: 16px;*/
   }
   .signature-pad {
   position: absolute;
   left: 0;
   top: 0;
   width:325px;
   height:75px;
   }
   /*Department*/
   /* .feature-area {
   padding-top: 113px;
   padding-bottom: 75px;
   position: relative; }*/
   @media only screen and (min-width: 320px) and (max-width: 479px) {
   .feature-area {
   padding-top: 100px; } }
   .feature-area .sec-heading .sec__title {
   line-height: 55px; }
   @media (max-width: 480px) {
   .feature-area .sec-heading .sec__title {
   line-height: 40px; } }
   .feature-area .service-button {
   margin-top: 80px;
   text-align: right; }
   @media only screen and (min-width: 768px) and (max-width: 991px) {
   .feature-area .service-button {
   text-align: left;
   margin-top: 30px;
   margin-bottom: 35px; } }
   @media only screen and (min-width: 480px) and (max-width: 767px) {
   .feature-area .service-button {
   text-align: left;
   margin-top: 30px;
   margin-bottom: 35px; } }
   @media only screen and (min-width: 320px) and (max-width: 479px) {
   .feature-area .service-button {
   text-align: left;
   margin-top: 30px;
   margin-bottom: 35px; } }
   .feature-area .feature-box {
   margin-top: 25px;
   text-align: center; }
   .feature-area .feature-box .feature-item {
       position: relative;
    background-color: #fff;
    border-radius: 20px;
    /* box-shadow: 0 0 40px rgba(82, 85, 90, 0.1); */
    box-shadow: 0 0 10px rgba(127, 131, 138, 0.22);
    transition: all 0.3s;
    /* padding: 45px 30px 40px 30px; */
    min-height: 220px;
    padding: 18px 13px 13px 13px;
    margin-bottom: 30px;
    z-index: 1;
    border-top: 8px solid #8c57c4; }
   .feature-area .feature-box .feature-item:after {
   position: absolute;
   content: '';
   bottom: 0;
   width: 100%;
   height: 2px;
   background-color: #F6881F;
   left: 0;
   border-radius: 4px;
   transform: scale(0);
   transition: all 0.3s; }
   @media (max-width: 1199px) {
   .feature-area .feature-box .feature-item {
   padding-right: 20px;
   padding-left: 20px; } }
   @media only screen and (min-width: 768px) and (max-width: 991px) {
   .feature-area .feature-box .feature-item {
   padding-right: 30px;
   padding-left: 30px;
   margin-bottom: 30px; } }
   @media only screen and (min-width: 481px) and (max-width: 767px) {
   .feature-area .feature-box .feature-item {
   width: 60%;
   margin-left: auto;
   margin-right: auto; } }
   @media only screen and (min-width: 480px) and (max-width: 767px) {
   .feature-area .feature-box .feature-item {
   padding-right: 30px;
   padding-left: 30px;
   margin-bottom: 30px; } }
   @media only screen and (min-width: 320px) and (max-width: 479px) {
   .feature-area .feature-box .feature-item {
   padding-right: 30px;
   padding-left: 30px;
   margin-bottom: 30px;
   width: 100%; } }
   .feature-area .feature-box .feature-item .feature__icon {
   color: #F6881F;
   display: inline-block;
   position: relative;
   width: 100px;
   height: 100px;
   line-height: 111px;
   margin-bottom: 5px;
   z-index: 1;
   transition: all 0.3s;
   background-color: rgba(246, 107, 93, 0.1);
   border-radius: 50%; }
   @media (max-width: 1199px) {
   .feature-area .feature-box .feature-item .feature__icon {
   font-size: 45px;
   margin-top: 20px;
   margin-bottom: 35px; } }
   @media only screen and (min-width: 768px) and (max-width: 991px) {
   .feature-area .feature-box .feature-item .feature__icon {
   font-size: 65px;
   margin-bottom: 39px; } }
   @media only screen and (min-width: 480px) and (max-width: 767px) {
   .feature-area .feature-box .feature-item .feature__icon {
   font-size: 65px;
   margin-bottom: 39px; } }
   @media only screen and (min-width: 320px) and (max-width: 479px) {
   .feature-area .feature-box .feature-item .feature__icon {
   font-size: 65px;
   margin-bottom: 39px; } }
   .feature-area .feature-box .feature-item .feature__icon:before {
   font-size: 45px; }
   .feature-area .feature-box .feature-item .feature__title {
   font-size: 20px;
   font-weight: 700;
   margin-bottom: 24px;
   text-transform: capitalize; }
   .feature-area .feature-box .feature-item .feature__title a {
   color: #233d63;
   transition: all 0.3s;
   background-color: white;
   font-size: 16px;
   border-radius: 8px; }
   .feature-area .feature-box .feature-item:hover {
   -webkit-border-radius: 0;
   -moz-border-radius: 0;
   border-radius: 0; }
   .feature-area .feature-box .feature-item:hover .feature__number {
   transform: scale(1); }
   .feature-area .feature-box .feature-item:hover .feature__icon {
   background-color: #F6881F;
   color: #fff; }
   .feature-area .feature-box .feature-item:hover .feature__title a {
   color: #F6881F; }
   .feature-area .feature-box .feature-item:hover:after {
   transform: scale(1); }
   .feature-area .feature-box .feature-item:hover:before {
   opacity: 0.2;
   visibility: visible;
   transform: scale(1); }
   /*  .feature-area .feature-box .feature-item:before {
   position: absolute;
   content: '';
   top: 0;
   left: 0;
   width: 100%;
   height: 100%;
   /*background-image: url(<?php echo base_url();?>application/images/deptLayout.png);
   background-image: url(https://techydevs.com/demos/themes/html/minzel/images/dots3.png);
   background-size: cover;
   background-position: center;
   z-index: -1;
   opacity: 0;
   visibility: hidden;
   transition: all 0.5s;
   transform: scale(0.6); }*/



   /*New Css*/
.aboutus-section {
    padding: 90px 0;
}
.aboutus-title {
    font-size: 40px;
    letter-spacing: 1.8px;
    /* line-height: 32px; */
    /* margin: 0 0 0px; */
    /* padding: 0 0 11px; */
    position: relative;
    /* text-transform: uppercase; */
    color: #313131;
}

/*about us cards*/
.wrimagecard{
  width: 200px;
    height: 200px;
    margin-top: 0;
    /* margin-right: 33px; */
    margin-bottom: 1.5rem;
    text-align: left;
    position: relative;
    background: #fff;
    box-shadow: 12px 15px 20px 0px rgba(46,61,73,0.15);
    border-radius: 12px;
    transition: all 0.3s ease;
}

.wrimagecard:hover  {
  background: #815cd5;
}

.wrimagecard h5{
  color: #f6882c;
}

.wrimagecard:hover i, .wrimagecard:hover h5, .wrimagecard:hover p{
  color: #fff;
}


.wrimagecard .fa , .wrimagecard .fas{
    position: relative;
    font-size: 50px;
    color: #815cd5;
}

.wrimagecard-topimage_header{
padding: 32px 20px 10px 20px;
}
a.wrimagecard:hover, .wrimagecard-topimage:hover {
    box-shadow: 2px 4px 8px 0px rgba(46,61,73,0.2);
    color: white;
}
.wrimagecard-topimage a {
    width: 100%;
    height: 100%;
    display: block;
}
.wrimagecard-topimage_title {
    padding: 4px 10px;
    height: 80px;
    padding-bottom: 0.75rem;
    position: relative;
    text-align: center;
}
.wrimagecard-topimage a {
    border-bottom: none;
    text-decoration: none;
    color: #525c65;
    transition: color 0.3s ease;
}

.wrimagecard-topimage_title p{
    margin: 0px;
    font-size: 12px;
    line-height: 1;
    font-weight: bolder;
    letter-spacing: 0.4px;
   word-break: break-all;
}

.col-offset-half{
 margin-left: 4.166666667%;
}

/*.featureBox{
 width: 100vw;
    position: relative;
    margin-left: -49vw;
    height: -webkit-fill-available;
    margin-top: 35px;
    left: 50%;
    background-color: black;
    background-image: url("<?php echo base_url();?>application/images/framephoto.png");
    padding: 50px 100px;
    background:linear-gradient(0deg, rgba(49, 47, 49, 0.3), rgba(74, 70, 72, 0.3)), url("<?php echo base_url();?>application/images/framephoto.png");
    background-size:cover;
}*/


/*.jumbotron{
    background-color: red;
    color:#fff;
}*/
.box{
    /*border:1px solid #ccc;*/
    padding:20px;
    text-align: center;
}

.square {
  height: 55px;
  width: 55px;
  background-color: #fff;
  transform:rotate(45deg);
  position: relative;
    left: 45%;
    bottom: 10px;
        border-radius: 5px;
}
/*.item-count {
  color: #333;
  display: table-cell;
  height: 60px;
  transform: rotate(-45deg);
  vertical-align: middle;
  width:60px;
}*/

.item-count{
      position: absolute;
    /* z-index: 1111; */
    color: #000;
    /* height: 0px; */
    transform: rotate(-45deg);
    vertical-align: middle;
    /* width: 10px; */
    top: 9px;
    left: 25px;
}


/*chart*/
.circle-chart__circle {
  animation: circle-chart-fill 2s reverse; /* 1 */
  transform: rotate(-90deg); /* 2, 3 */
  transform-origin: center; /* 4 */
}

.circle-chart__circle--negative {
  transform: rotate(-90deg) scale(1,-1); /* 1, 2, 3 */
}

.circle-chart__info {
  animation: circle-chart-appear 2s forwards;
  opacity: 0;
  transform: translateY(0.3em);
}

@keyframes circle-chart-fill {
  to { stroke-dasharray: 0 100; }
}

@keyframes circle-chart-appear {
  to {
    opacity: 1;
    transform: translateY(0);
  }
}

.circleMargin{
      margin: 10px 18px;
}

.financialTable tr>th{
  border: none;
  background-color: #8c57c5;
}

.financialTable tr>th>div{
    padding: 6px 20px;
    background-color: #eee3f7;
    /* color: black; */
    border-radius: 6px;
}

.financialTable tr>td>div{
    padding: 0px 20px;
    background-color: #8c57c5;
    /* color: black; */
    border-radius: 6px;
    text-align: left;
}

.financialTable tr>td>div>h5{

     color: white;

}

.buttonHome{
    height: 50px !important;
    color: #ffffff !important;
    background-color: #f6872c;
    font-size: 18px !important;
    border: 2px solid #f6872c ;
    border-top-left-radius: 10px;
    border-bottom-left-radius: 10px;
}

.buttonHome:hover{
  background-color: #8c57c5 !important;
  border: 2px solid #8c57c5 !important;
}

.buttonProgram{
    height: 50px !important;
    color: #ffffff !important;
    background-color: #8c57c5;
    font-size: 18px !important;
    border: 2px solid #f6872c;
    border-top-right-radius: 10px;
    border-bottom-right-radius: 10px;
}

.buttonProgram:hover{
  background-color: #f6872c !important;
  border: 2px solid #f6872c !important;
}


.flLt {
   float: left;
   }
.uni-image {
   background: #fff;
   border: 1px solid #f3f3f3;
   padding: 5px;
   position: relative;
   width: 160px;
   height: 145px;
   }

.uni-boxPurple{
   float: left;
   width: 100%;
   padding: 8px;
   position: relative;
   background-color:#ffffff;
   /*border:1px solid #f0f0f0;*/
   box-shadow: 3px 4px 6px #ccccc4;
   margin: 20px 0px;
   border-right: 7px solid #8c57c4;
   }
   .uni-boxPurple p{
   margin:0px;
   line-height:unset;
   font-size: 13px;
   }
   .uni-detail {
   margin-left: 160px;
   }
    .uni-detailModal {
   margin-left: 205px;
   }
   .uni-title {
   margin-bottom: 10px;
   width: 100%;
   }
   .uni-title a {
   font-weight: 600;
   font-size: 13px;
   color: #111111;
   }
   .uni-title span {
   color: #666666;
   }
   .uni-sub-title {
   font-weight: 600;
   font-size: 15px;
   color:#666666;
   }
   a.uni-sub-title {
   color:#666666;
   }

   .iconRowPurple .iconCss{
   background: #8c57c4;
   padding: 7px;
   height: 30px;
   width: 30px;
   border-radius: 50%;
   color: white;
   font-size: 15px;
   margin: 5px 0px 5px 0px;
   }
   .iconRowPurple strong {
   font-size: 13px;
   color: #8c57c4;
   }
   .iconRowPurple span {
   font-size: 13px;
   color: #000;
   }
   .font14{
   font-size: 14px;
   }
   .mrgtop{
   margin: 9px 0px 5px 0px; ;
   }

   .btn-brochure a {
   padding: 8px 18px 9px!important;
   }

   .twoPXPadding{
    padding-right: 2px !important;
    padding-left: 2px !important;
   }

   .faPadding{
   padding-right: 0px !important;
    padding-left: 22px !important;
   }

   .heart{
    font-size: 18px !important;
    margin: 0px 8px;
    color:red;
}

.boxAlign {
  display: inline-flex;
    flex-wrap: wrap;
    height: 250px;
    width: 30.33%;
    text-align: center;
    align-content: space-between;
    margin: 10px;
}

.displayInline{
   display: inline-flex;
}
/*.grid {
  display: grid;
  grid-column-gap: 1em;
  grid-row-gap: 1em;
  grid-template-columns: repeat(1, 1fr);
}

@media (min-width: 31em) {
  .grid {
    grid-template-columns: repeat(2, 1fr);
  }
}*/
</style>
<div class="clearfix"></div>
<div id="page_loader">
   <div>
      <?php if($banner){?>
      <section style="background-color: rgba(0, 0, 0, 0);background-repeat: no-repeat;background-image: url(<?=$banner . '?t=' . time()?>); background-size: cover;background-position: center center;width: 100%;height: 250%;">
      <!--             <img src="<?=$banner . '?t=' . time()?>" height="400px;" width="100%"  />
         --><?php }else{?>
      <section style="background-color: rgba(0, 0, 0, 0);background-repeat: no-repeat;background-image: url(<?php echo base_url();?>uploads/banner.png); background-size: cover;background-position: center center;width: 100%;height: 250%;">
         <!-- <img src="<?php echo base_url();?>uploads/banner.jpg" width="100%" height="400px" /> -->
         <?php }?>
         <div class="container" style="height: 300px;">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="z-index: 5 !important; text-align: center;">
                  <h1 class="page-top-title" style="margin-top: 50px;color: #ffffff; text-align:left;font-weight:bold;font-size:40px;"><span style="border-radius: 9px;background-color: rgba(30, 32, 33, 0.82)">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $name;?>&nbsp;&nbsp;&nbsp;&nbsp;</span></h1>
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 uniLogoDiv"  >
                 <?php
                     if($logo || $preview)
                     {
                         ?>
                  <div class="col-lg-3 visible-lg uni_logo " >
                     <img src="<?=$logo . '?t=' . time()?>" style="width:200px;" />
                     <?php }else{?>
                     <div class="col-lg-3 visible-lg uni_logo " >
                        <img src="<?php echo base_url();?>uploads/logo.png" style="width:200px;"/>
                        <?php }?>
                     </div>
                     <div class="col-sm-3 visible-sm visible-xs"><img src="images/uni-logo.png" width="70" /></div>
                  </div>
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top:30px; z-index:100">
                  <div class="col-md-3">
                     <button id="homeButton" type="button" class="row btn btn-lg btn-primary buttonHome" onclick="openDetails()" style="width: -webkit-fill-available;">Home </button>
                  </div>
                  <div class="col-md-3">
                     <button id="progButton" type="button" class="row btn btn-lg btn-primary buttonProgram" onclick="openPrograms()" style="width: -webkit-fill-available;">Popular Programs</button>
                  </div>
               </div>
               <script type="text/javascript">
                        function openDetails() {
                        var x = document.getElementById("detailsView");
                        var y = document.getElementById("programView");
                        x.style.display = "block";
                        y.style.display = "none";

                        $(".buttonProgram").css({"background-color": "#8c57c5" , "border": "2px solid #8c57c5"});
                        $(".buttonHome").css({"background-color": "#f6872c" , "border": "2px solid #8c57c5"});
                        }
                        function openPrograms() {
                        var y = document.getElementById("programView");
                        var x = document.getElementById("detailsView");
                        y.style.display = "block";
                        x.style.display = "none";

                        $(".buttonProgram").css({"background-color": "#f6872c" , "border": "2px solid #f6872c"});
                        $(".buttonHome").css({"background-color": "#8c57c5" , "border": "2px solid #f6872c"});
                        }
               </script>
            </div>
      </section>
      <!-- <?php if($banner){?>
         <img src="<?=$banner . '?t=' . time()?>" height="400px;" width="100%"  />
         <?php }else{?>
         <img src="<?php echo base_url();?>uploads/banner.jpg" width="100%" height="400px" />
         <?php }?> -->
      <section class="container">
        <div id="detailsView" class="container-fluid" style="display: block;">

            <div class="col-md-12" >
                <div class="aboutus" style="text-align: center;">
                  <h1 class="aboutus-title">About University</h1>
                  <p style="color: #4a4949;font-size: 20px;"><?php echo $about;?></p>
                </div>
            </div>
            <div class="col-md-12" style="margin-bottom: 3%;">
                <div class="col-md-2 col-sm-4 ">
                    <div class="wrimagecard wrimagecard-topimage">
                       <a href="<?=$group_url?>" target="_blank">
                        <div class="wrimagecard-topimage_header">
                          <center><i class="fa fa-whatsapp"></i></center>
                        </div>
                        <div class="wrimagecard-topimage_title">
                          <h5>Whatsapp Group</h5>
                          <p><?php echo $name;?> Whatsapp Group</p>
                        </div>
                      </a>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-offset-half">
                  <div class="wrimagecard wrimagecard-topimage">
                      <a href="<?=$website?>" target="_blank">
                      <div class="wrimagecard-topimage_header" >
                        <center><i class="fas fa-globe"></i></center>
                      </div>
                      <div class="wrimagecard-topimage_title">
                        <h5>Website</h5>
                        <p><?=$website?></p>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="col-md-2 col-sm-4 col-offset-half">
                    <div class="wrimagecard wrimagecard-topimage">
                        <a href="#">
                        <div class="wrimagecard-topimage_header" >
                          <center><i class="fas fa-university"> </i></center>
                        </div>
                        <div class="wrimagecard-topimage_title" >
                          <h5>University Grade</h5>
                          <p><?=$type?></p>
                        </div>

                      </a>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-offset-half">
                    <div class="wrimagecard wrimagecard-topimage">
                        <a href="#">
                        <div class="wrimagecard-topimage_header" >
                           <center><i class="fas fa-rocket"></i></center>
                        </div>
                        <div class="wrimagecard-topimage_title">
                          <h5>Establishment Year</h5>
                          <p><?=$establishment_year?></p>
                        </div>

                      </a>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-offset-half">
                    <div class="wrimagecard wrimagecard-topimage">
                        <a href="#">
                        <div class="wrimagecard-topimage_header" >
                           <center><i class="fas fa-university"> </i></center>
                        </div>
                        <div class="wrimagecard-topimage_title">

                          <h5>Carnegie Accreditation</h5>
                          <p><?=$carnegie_accreditation?></p>
                        </div>

                      </a>
                    </div>
                </div>
            </div>

             <div id="projectFacts" class="sectionClass">
              <div class="col-md-12">
                  <div class="projectFactsWrap ">
                      <div class="item wow fadeInUpBig animated animated" data-number="<?php if(isset($student)) echo $student;?>" style="visibility: visible;">
                         <!-- <i class="fa fa-male" ></i> -->
                         <p id="number1" class="number"><?php if(isset($student)) echo $student;?></p>
                         <span></span>
                         <p><b>Total Students</b></p>
                      </div>
                      <div class="item wow fadeInUpBig animated animated" data-number="" style="visibility: visible;">
                         <!-- <i class="fa fa-users purple"></i> -->
                         <p id="number2" class="number"><?php if(isset($internationalstudent)) echo $internationalstudent;?></p>
                         <span></span>
                         <p><b>International Students</b></p>
                      </div>
                      <div class="item wow fadeInUpBig animated animated" data-number="" style="visibility: visible;">
                         <!-- <i class="fa fa-star"></i> -->
                         <p id="number3" class="number"><?=$ranking_usa?></p>
                         <span></span>
                         <p><b>National Ranking</b></p>

                      </div>
                      <div class="item wow fadeInUpBig animated animated" data-number="" style="visibility: visible;">
                         <!-- <i class="fa fa-globe purple"></i> -->
                         <p id="number4" class="number"><?=$ranking?></p>
                         <span></span>
                         <p><b>World Ranking</b></p>

                      </div>
                      <div class="item wow fadeInUpBig animated animated" data-number="" style="visibility: visible;">
                         <!-- <i class="fa fa-briefcase"></i> -->
                         <p id="number5" class="number"><?=$placement_percentage?> %</p>
                         <span></span>
                         <p><b>Placement Percentage</b></p>
                      </div>
                      <div class="item wow fadeInUpBig animated animated" data-number="" style="visibility: visible;">
                         <!-- <i class="fa fa-graduation-cap purple"></i> -->
                         <p id="number6" class="number"><?=$admission_success_rate?> %</p>
                         <span></span>
                         <p><b>Admission Success Rate</b></p>
                      </div>
                  </div>
              </div>
            </div>
             <div class="clearfix "></div>
             <div class="parallax-wrapper">
                  <div class="parallax-container">
                      <div class="row">
                  <div class="col-md-12" style="text-align: center;    margin-bottom: 20px;">
                    <h1 class="aboutus-title" style="color: white;">Features</h1>
                  </div>

                  <div class="col-md-12" style="text-align: center; margin-top: 8px;">
                    <?php
                   foreach($usps as $usp)
                   {
                    if($usp)
                    {
                     ?>

                    <div class="boxAlign">

                        <div><div class="square">
                            <div class="row item-count">
                            <i class="fas fa-university" style="font-size: 35px;"></i></div>
                          </div><h4><?=$usp?></h4></div>
                    </div>
                     <?php
                   }
                   }
                   ?>

                  </div>
                </div>
                  </div>
              </div>
            <!--  <div class="featureBox">

                <div class="container" style="margin-left: 10px;">
                  <div class="col-md-12" style="text-align: center;">
                    <h1 class="col-md-9 aboutus-title" style="color: white;">Features</h1>
                  </div>

                  <div class="row">
                    <?php
                   foreach($usps as $usp)
                   {
                    if($usp)
                    {
                     ?>
                    <div class="col-sm-3">
                        <div class="box">
                          <div class="square">
                            <div class="row item-count">
                            <i class="fas fa-university" style="font-size: 35px;"></i></div>
                          </div>
                          <h4><?=$usp?></h4>

                        </div>
                    </div>
                     <?php
                   }
                   }
                   ?>

                  </div>
                </div>

             </div> -->
             <div class="col-md-12 headerTop " id="myHeaderone">

                  <?php
                     if(isset($alreadyConnected) && $alreadyConnected)
                     {
                       if($alreadyConnected->slot_1 || $alreadyConnected->slot_2 || $alreadyConnected->slot_3)
                       {
                                 if(!$alreadyConnected->confirmed_slot)
                                 {
                                     ?>
                      <div class="col-md-12">
                        <form class="form-horizontal" method="post" action="/university/details/slotconfirm">
                          <div class="col-md-5" style="text-align: right;margin-top: 8px;">
                              <h4 style="color:#fff;">Please select any one slot :</h4>
                            </div>
                          <div class="col-md-4" style="display: inline-flex;margin-top: 8px;">
                              <?php
                              if($alreadyConnected->slot_1)
                              {
                                  ?>
                           <div style="color:#fff; padding:5px;"><input type="radio" name="confirmed_slot" value="<?=$alreadyConnected->slot_1?>" <?=($alreadyConnected->confirmed_slot == $alreadyConnected->slot_1)? 'checked' : ''?>>&nbsp;&nbsp;<b>Slot 1 :</b> &nbsp;&nbsp;<?=$alreadyConnected->slot_1_show?></div>
                           <?php
                              }
                              if($alreadyConnected->slot_2)
                              {
                                  ?>
                           <div style="color:#fff; padding:5px;"><input type="radio" name="confirmed_slot" value="<?=$alreadyConnected->slot_2?>" <?=($alreadyConnected->confirmed_slot == $alreadyConnected->slot_2)? 'checked' : ''?>>&nbsp;&nbsp;<b>Slot 2 :</b> &nbsp;&nbsp;<?=$alreadyConnected->slot_2_show?></div>
                           <?php
                              }
                              if($alreadyConnected->slot_3)
                              {
                                  ?>
                           <div style="color:#fff; padding:5px;"><input type="radio" name="confirmed_slot" value="<?=$alreadyConnected->slot_3?>" <?=($alreadyConnected->confirmed_slot == $alreadyConnected->slot_3)? 'checked' : ''?>>&nbsp;&nbsp;<b>Slot 3 :</b> &nbsp;&nbsp;<?=$alreadyConnected->slot_3_show?></div>
                           <?php
                              }
                              ?>
                          </div>
                          <div class="col-md-3">
                           <input type="hidden" name="slot_id" value="<?=$alreadyConnected->id?>">
                           <input type="hidden" name="university_id" value="<?=$id?>">
                           <input type="hidden" name="university_login_id" value="<?=$login_id?>">
                           <input type="hidden" name="university_website" value="<?=$website?>">
                           <input type="hidden" name="university_name" value="<?=$name?>">
                           <input type="hidden" name="university_email" value="<?=$email?>">
                           <input type="hidden" name="slug" value="<?=$slug?>">
                           <div style="text-align:left;"><input type="submit" class="btn btn-lg btn-primary" name="submit" style="margin:10px auto;" value="Submit"></div>
                          </div>


                        </form>

                      </div>

                  <?php
                     }
                     else
                     {
                         if($applied)
                         {
                             ?>
                  <div style="text-align:center;"><input type="button" class="btn btn-lg btn-primary" name="apply" style="margin:10px auto; background-color:#F6881F" value="Applied" id="apply_button" disabled></div>
                  <?php
                     }
                     else if(isset($tokbox_url) && $tokbox_url)
                     {
                         if($show_url)
                         {
                             ?>
                  <div style="margin-top: 18px; font-weight: bold;"><a href="<?=$tokbox_url?>" target="_blank">Click Here For Video Conferene</a></div>
                  <?php
                     }
                     else if($alreadyConnected->status == 'Attained')
                     {
                         ?>
                  <!--h5 style="color:#fff;">Book one to one sessions Again:</h5-->
                  <form class="form-horizontal" method="post" action="/university/details/connect">
                     <div class="form-group" style="text-align:center;">
                        <input type="submit" class="btn btn-lg btn-primary" name="submit" style="margin:10px auto;" value="Connect Again">
                     </div>
                     <input type="hidden" name="university_id" value="<?=$id?>">
                     <input type="hidden" name="university_login_id" value="<?=$login_id?>">
                     <input type="hidden" name="slug" value="<?=$slug?>">
                  </form>
                  <form method="get" action="/university/details/application">
                     <div style="text-align:center;">
                        <input type="submit" class="btn btn-lg btn-primary" name="submit" style="margin:10px auto;" value="Start Documentation">
                     </div>
                     <input type="hidden" name="uid" value="<?=$id?>">
                  </form>
                  <?php
                     }
                     else if($alreadyConnected->status == 'Scheduled')
                     {
                         ?>
                  <h4 style="color:#fff; text-align:center;"><?=$this->session->flashdata('flash_message') ? $this->session->flashdata('flash_message') : '<p style="font-size:11px;">Your request is in process.<br/>Please check your mail for further information.</p>'?></h4>
                  <?php
                     }
                     }
                     else
                     {
                     ?>
                  <h4 style="color:#fff; text-align:center;"><?=$this->session->flashdata('flash_message') ? $this->session->flashdata('flash_message') : '<p style="font-size:11px;">Your request is in process.<br/>Please check your mail for further information.</p>'?></h4>
                  <?php
                     }
                       }
                     }
                     else
                     {
                     ?>
                  <h4 style="color:#fff; text-align:center;"><?=$this->session->flashdata('flash_message') ? $this->session->flashdata('flash_message') : '<p style="font-size:11px;">Your request is in process.<br/>Please check your mail for further information.</p>'?></h4>
                  <?php
                     }
                     }
                     else
                     {
                      ?>
                    <div class="col-md-12">
                        <div class="col-md-8 col-xs-8" style="text-align: right;margin-top: 8px;">
                          <h4 class="hidden-xs visible-md visible-lg" style="color:#fff;"><?php echo $name;?> &nbsp; Book one to one sessions :</h4>
                          <h5 class="visible-xs hidden-md hidden-lg" style="color:#fff;"><?php echo $name;?> &nbsp; Book one to one sessions :</h5>
                        </div>
                        <div class="col-md-3 col-xs-3" style="text-align: left;">
                          <form class="form-horizontal" method="post" action="/university/details/connect" id="connect-university">
                             <div class="form-group" style="text-align:left;">
                                <?php
                                   if($userLoggedIn)
                                   {
                                       ?>
                                <input type="button" class="btn btn-lg btn-primary hidden-xs visible-md visible-lg" name="connect" style="margin:15px 10px 0px 10px;" value="Connect" onclick="openConsent()">
                                <input type="button" class="btn btn-md btn-primary visible-xs hidden-md hidden-lg" name="connect" style="margin:30px 10px 0px 10px;" value="Connect" onclick="openConsent()">



                                <?php
                                   }
                                   else
                                   {
                                       ?>
                                <input type="submit" class="btn btn-lg btn-primary hidden-xs visible-md visible-lg" name="submit" style="margin:15px 10px 0px 10px;" value="Connect">
                                <input type="submit" class="btn btn-md btn-primary visible-xs hidden-md hidden-lg" name="submit" style="margin:30px 10px 0px 10px;" value="Connect">
                                <?php
                                   }
                                   ?>
                             </div>
                             <input type="hidden" name="university_id" value="<?=$id?>">
                             <input type="hidden" name="university_login_id" value="<?=$login_id?>">
                             <input type="hidden" name="slug" value="<?=$slug?>">
                             <input type="hidden" name="student_signature" value="" id="student_signature">
                             <input type="hidden" name="student_name" value="" id="student_name">
                             <input type="hidden" name="student_email" value="" id="student_email">
                             <input type="hidden" name="student_phone" value="" id="student_phone">
                             <input type="hidden" name="student_dob" value="" id="student_dob">
                             <input type="hidden" name="student_country" value="" id="student_country">
                             <input type="hidden" name="student_city" value="" id="student_city">
                             <input type="hidden" name="student_passport" value="" id="student_passport">
                             <input type="hidden" name="student_intake_month" value="" id="student_intake_month">
                             <input type="hidden" name="student_intake_year" value="" id="student_intake_year">
                             <input type="hidden" name="student_mainstream" value="" id="student_mainstream">
                             <input type="hidden" name="student_subcourse" value="" id="student_subcourse">
                             <input type="hidden" name="student_level" value="" id="student_level">
                             <input type="hidden" name="student_hear_about_us" value="" id="student_hear_about_us">
                          </form>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="col-md-8 col-xs-8" style="text-align: right;margin-top: 8px;">
                          <h4 class="hidden-xs visible-md visible-lg" style="color:#fff;"><?php echo $name;?> &nbsp; Apply :</h4>
                          <h5 class="visible-xs hidden-md hidden-lg" style="color:#fff;"><?php echo $name;?> &nbsp; Apply :</h5>
                        </div>
                        <div class="col-md-3 col-xs-3" style="text-align: left;">
                          <form class="form-horizontal" method="post" action="/university/details/connect" id="connect-university">
                             <div class="form-group" style="text-align:left;">
                                <?php
                                   if($userLoggedIn)
                                   {
                                       ?>
                                <input type="button" class="btn btn-lg btn-primary" onclick="courseModal(7, 223, '<?php echo $name;?>')" value="Apply" />

                                <?php
                                   }
                                   else
                                   {
                                       ?>
                                <input type="submit" class="btn btn-lg btn-primary hidden-xs visible-md visible-lg" name="submit" style="margin:15px 10px 0px 10px;" value="Connect">
                                <input type="submit" class="btn btn-md btn-primary visible-xs hidden-md hidden-lg" name="submit" style="margin:30px 10px 0px 10px;" value="Connect">
                                <?php
                                   }
                                   ?>
                             </div>

                          </form>
                        </div>
                    </div>


                  <?php
                     }
                     ?>

             </div>


             <div class="col-lg-12 col-md-12" style="margin-top:10px;">
                <div class="col-md-4">
                  <div class="card">
                    <div class="row cardimg" >
                          <img src="<?php echo base_url();?>application/images/degreeBox.png" width="75%" alt="...">
                    </div>
                    <div class="row cardInfo">


                        <h3 class="purple"><b>Degrees</b></h3>
                        <?php
                           foreach($degrees as $degree)
                           {
                            ?>
                        <p class="pTag" style="margin-bottom: 1px !important;"><i class="fa fa-graduation-cap" style="font-size:12px;color:#374552">&nbsp;</i><?=$degree?></p>
                        <?php
                           }
                           ?>

                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="card">
                    <div class="row cardimg">
                        <img src="<?php echo base_url();?>application/images/campusBox.png" width="75%" alt="...">
                    </div>
                  <div class="row cardInfo">

                      <h3 class="purple"><b>Campuses</b></h3>
                      <?php
                         foreach($campuses as $campus)
                         {
                         ?>
                      <p class="pTag"><b>Name : </b><?=$campus?></p>
                      <?php
                         }
                         ?>
                      <p class="pTag"><b>State : </b>  Arizona</p>
                      <p class="pTag"><b>City : </b> Tempe</p>
                      <p class="pTag"><b>Size : </b> 661 ACRE</p>
                      <p class="pTag"><b>Type of Accommodation : </b> On Campus</p>
                   </div>
                  </div>

                </div>
                <div class="col-md-4">
                  <div class="card">
                      <div class="row cardimg">
                        <img src="<?php echo base_url();?>application/images/researchBox.png" width="75%" alt="...">
                      </div>

                      <div class="row cardInfo">

                        <h3 class="purple"><b>Research Spending</b></h3>
                        <p class="pTag">$ <?=$research_spending?> M</p>
                     </div>
                  </div>
                </div>
             </div>
             <hr>
             <div class="col-lg-12 col-md-12" style="margin-top:10px; ">
                <div class="aboutus" style="text-align: center;">
                  <h1 class="aboutus-title">Trending Disciplines</h1>
                </div>

                   <!-- <div id="chartContainer" style="height: 300px; width: 100%;">
                   </div> -->
                  <div class="col-md-12 displayInline" >
                              <?php
                              foreach ($trending_discipline as $tdkey => $tdvalue) {
                                 // code...
                              ?>
                    <div class="col-md-2 circleMargin txtAlignCentre">
                      <svg class="circle-chart " viewbox="0 0 33.83098862 33.83098862" width="170" height="170" xmlns="http://www.w3.org/2000/svg">
                        <circle class="circle-chart__background" stroke="#8703c521" stroke-width="2" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                        <circle class="circle-chart__circle" stroke="#8703c5" stroke-width="2" stroke-dasharray="<?php echo $tdvalue; ?>,100" stroke-linecap="round" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                        <g class="circle-chart__info">
                          <text class="circle-chart__percent" x="16.91549431" y="15.5" alignment-baseline="central" text-anchor="middle" font-size="8"><?php echo $tdvalue; ?>%</text>
                          <!-- <text class="circle-chart__subline" x="16.91549431" y="20.5" alignment-baseline="central" text-anchor="middle" font-size="2">Yay 30% progress!</text> -->
                        </g>
                      </svg>
                      <h4 class="txtAlignCentre" style="font-size: 20px;"><b><?php echo $tdkey; ?></b></h4>
                    </div>
                           <?php } ?>

                  </div>


             </div>
             <div class="col-md-12" style="margin-top:10px;">
                <div class="aboutus" style="text-align: center;">
                  <h1 class="aboutus-title">Financials</h1>
                </div>

               <div class="col-md-8 col-md-offset-2" style="min-height: 412px;">

                   <div class="row">
                      <table class="table table-hover financialTable">
                         <thead>
                            <tr>
                               <th style="border-top-left-radius: 5px;"><div class="col-md-12"><h4>DESCRIPTION</h4></div></th>
                               <th style="border-top-right-radius: 5px;"><div class="col-md-12"><h4>SUBTOTAL</h4></div></th>
                            </tr>
                         </thead>
                         <tbody>
                                        <?php

                                        $tutionCost = 0;
                                        $livingCost = 0;
                                        $appCost = 0;
                                        $totalCost = 0;

                                        if(isset($fees_detail) && !empty($fees_detail)){

                                           $tutionCost = round($fees_detail[0]['tution_fees']);
                                           $livingCost = round($fees_detail[0]['cost_of_living_fees']);
                                           $appCost = round($fees_detail[0]['application_fees']);

                                           $totalCost = $tutionCost + $livingCost + $appCost;

                                        }
                                        ?>
                            <tr style="background-color: #8c57c5;">
                               <td class="col-md-7" style="border: none;"><div class="col-md-12"><h5>Avg Cost of Tuition/Year</h5></div></td>
                               <td class="col-md-5 text-right" style="border: none;"><div class="col-md-12"><h5> <?php echo $currency . ' ' . $tutionCost; ?> </h5></div></td>
                            </tr>
                            <tr style="background-color: #8c57c5;">
                               <td class="col-md-7" style="border: none;"><div class="col-md-12"><h5>Cost of Living/Year</h5></div></td>
                               <td class="col-md-5 text-right" style="border: none;"><div class="col-md-12"><h5> <?php echo $currency . ' ' . $livingCost; ?> </h5></div></td>
                            </tr>
                            <tr style="background-color: #8c57c5;">
                               <td class="col-md-7" style="border: none;"><div class="col-md-12"><h5>*Application Fee</h5></div></td>
                               <td class="col-md-5 text-right" style="border: none;"><div class="col-md-12"><h5> <?php echo $currency . ' ' . $appCost; ?>  <br>*Charged once for each program</h5></div></td>
                            </tr>
                            <tr>
                               <th style="border-bottom-left-radius: 5px;"><div class="col-md-12"><h4>Estimated Total/Year</h4></div></th>
                               <th style="border-bottom-right-radius: 5px;"><div class="col-md-12"><h4><?php echo $currency . ' ' . $totalCost; ?></h4></div></th>
                            </tr>
                         </tbody>
                      </table>
                      </td>
                   </div>
                </div>
             </div>
             <div class="col-lg-12 col-md-12" style="margin-top:10px;">

                <div class="aboutus" style="text-align: center;">
                  <h1 class="aboutus-title">Gallery</h1>
                </div>
                   <div id="main_area">

                               <?php if($university_gallery) { ?>

                                  <!-- Slider -->
                       <div class="row">
                          <div class="col-sm-5" id="slider-thumbs">
                             <!-- Bottom switcher of slider -->
                             <ul class="hide-bullets">
                                              <?php
                                              $galtcount = 1;
                                              foreach ($university_gallery as $ugvalue) {
                                                // code...
                                               ?>
                                <li class="col-sm-3 col-md-4" style="display: block;    padding: 1px 1px 1px 1px;">
                                   <a class="thumbnail" id="carousel-selector-<?php echo $galtcount; ?>" >
                                   <img src="<?php echo base_url().$ugvalue;?>" style="height: 120px;min-height: 120px;padding: inherit; width: 180px;">
                                   </a>
                                </li>
                                             <?php
                                          $galtcount = $galtcount + 1;
                                       } ?>

                             </ul>
                          </div>
                          <div class="col-sm-7">
                             <div class="col-xs-12" id="slider">
                                <!-- Top part of the slider -->
                                <div class="row">
                                   <div class="col-sm-12" id="carousel-bounding-box">
                                      <div class="carousel slide" id="myCarousel">
                                         <!-- Carousel items -->
                                         <div class="carousel-inner">
                                                                <?php
                                                                $galcount = 1;
                                                                foreach ($university_gallery as $uggvalue) {

                                                                   if($galcount == 1){
                                                                      $gactive = "active";
                                                                   } else{
                                                                      $gactive = "";
                                                                   }

                                                                 ?>
                                            <div class="<?php echo $gactive ;?> item" data-slide-number="<?php echo $galcount; ?>">
                                               <img src="<?php echo base_url().$uggvalue; ?>" style="height:400px ;max-height:400px ;min-height: 400px; width:100%;">
                                            </div>
                                                               <?php
                                                            $galcount = $galcount + 1;
                                                         } ?>

                                         </div>
                                         <!-- Carousel nav -->
                                         <!--<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                            </a>
                                            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                            </a>-->
                                      </div>
                                   </div>
                                </div>
                             </div>
                          </div>
                          <!--/Slider-->
                       </div>

                               <?php } else { ?>

                      <!-- Slider -->
                      <div class="row">
                         <div class="col-sm-5" id="slider-thumbs">
                            <!-- Bottom switcher of slider -->
                            <ul class="hide-bullets">
                               <li class="col-sm-3 col-md-4" style="display: block;    padding: 1px 1px 1px 1px;">
                                  <a class="thumbnail" id="carousel-selector-16" >
                                  <img src="<?php echo base_url();?>application/images/1.jpg" style="height: 120px;min-height: 120px;padding: inherit; width: 180px;">
                                  </a>
                               </li>
                               <li class="col-sm-3 col-md-4" style="display: block;    padding: 1px 1px 1px 1px;">
                                  <a class="thumbnail" id="carousel-selector-1"><img src="<?php echo base_url();?>application/images/2.jpg" style="height: 120px;min-height: 120px;padding: inherit;width: 180px;"></a>
                               </li>
                               <li class="col-sm-3 col-md-4" style="display: block;    padding: 1px 1px 1px 1px;">
                                  <a class="thumbnail" id="carousel-selector-2"><img src="<?php echo base_url();?>application/images/3.jpg" style="height: 120px;min-height: 120px;padding: inherit;width: 180px;"></a>
                               </li>
                               <li class="col-sm-3 col-md-4" style="display: block;    padding: 1px 1px 1px 1px;">
                                  <a class="thumbnail" id="carousel-selector-3"><img src="<?php echo base_url();?>application/images/4.png" style="height: 120px;min-height: 120px;padding: inherit;width: 180px;"></a>
                               </li>
                                <li class="col-sm-3 col-md-4" style="display: block;    padding: 1px 1px 1px 1px;">
                                  <a class="thumbnail" id="carousel-selector-4"><img src="<?php echo base_url();?>application/images/5.jpeg" style="height: 120px;min-height: 120px;padding: inherit;width: 180px;"></a>
                               </li>
                               <li class="col-sm-3 col-md-4" style="display: block;    padding: 1px 1px 1px 1px;">
                                  <a class="thumbnail" id="carousel-selector-5"><img src="<?php echo base_url();?>application/images/6.jpg" style="height: 120px;min-height: 120px;padding: inherit;width: 180px;"></a>
                               </li>
                               <!--<li class="col-sm-3" style="display: block;    padding: 1px 1px 1px 1px;">
                                  <a class="thumbnail" id="carousel-selector-6"><img src="<?php echo base_url();?>application/images/7.jpeg" style="height: 90px;min-height: 90px;padding: inherit;"></a>
                               </li>
                               <li class="col-sm-3 col-md-3" style="display: block;    padding: 1px 1px 1px 1px;">
                                  <a class="thumbnail" id="carousel-selector-7"><img src="<?php echo base_url();?>application/images/8.jpeg" style="height: 90px;min-height: 90px;padding: inherit;"></a>
                               </li> -->
                            </ul>
                         </div>
                         <div class="col-sm-7">
                            <div class="col-xs-12" id="slider">
                               <!-- Top part of the slider -->
                               <div class="row">
                                  <div class="col-sm-12" id="carousel-bounding-box">
                                     <div class="carousel slide" id="myCarousel">
                                        <!-- Carousel items -->
                                        <div class="carousel-inner">
                                           <div class="active item" data-slide-number="16">
                                              <img src="<?php echo base_url();?>application/images/1.jpg" style="height:400px ;max-height:400px ;min-height: 400px; width:100%;">
                                           </div>
                                           <div class="item" data-slide-number="1">
                                              <img src="<?php echo base_url();?>application/images/2.jpg" style="height:400px ;max-height:400px ;min-height: 400px; width:100%;">
                                           </div>
                                           <div class="item" data-slide-number="2">
                                              <img src="<?php echo base_url();?>application/images/3.jpg" style="height:400px ;max-height:400px ; min-height: 400px;width:100%;">
                                           </div>
                                           <div class="item" data-slide-number="3">
                                              <img src="<?php echo base_url();?>application/images/4.png" style="height:400px ;max-height:400px ; min-height: 400px;width:100%;">
                                           </div>
                                           <div class="item" data-slide-number="4">
                                              <img src="<?php echo base_url();?>application/images/5.jpeg" style="height:400px ;max-height:400px ;min-height: 400px; width:100%;">
                                           </div>
                                           <div class="item" data-slide-number="5">
                                              <img src="<?php echo base_url();?>application/images/6.jpg" style="max-height:400px ; min-height: 400px;width:100%;">
                                           </div>
                                          <!--  <div class="item" data-slide-number="6">
                                              <img src="<?php echo base_url();?>application/images/7.jpeg" style="max-height:400px ; width:100%;">
                                           </div>
                                           <div class="item" data-slide-number="7">
                                              <img src="<?php echo base_url();?>application/images/8.jpeg" style="max-height:400px ; width:100%;">
                                           </div> -->
                                        </div>
                                        <!-- Carousel nav -->
                                        <!--<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                           <span class="glyphicon glyphicon-chevron-left"></span>
                                           </a>
                                           <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                           <span class="glyphicon glyphicon-chevron-right"></span>
                                           </a>-->
                                     </div>
                                  </div>
                               </div>
                            </div>
                         </div>
                         <!--/Slider-->
                      </div>
                                 <?php } ?>
                   </div>

             </div>
             <script type="text/javascript">
                jQuery(document).ready(function($) {

                $('#myCarousel').carousel({
                 interval: 5000
                });

                //Handles the carousel thumbnails
                $('[id^=carousel-selector-]').click(function () {
                var id_selector = $(this).attr("id");
                try {
                var id = /-(\d+)$/.exec(id_selector)[1];
                console.log(id_selector, id);
                jQuery('#myCarousel').carousel(parseInt(id));
                } catch (e) {
                console.log('failed!', e);
                }
                });
                // When the carousel slides, auto update the text
                $('#myCarousel').on('slid.bs.carousel', function (e) {
                  var id = $('.item.active').data('slide-number');
                 $('#carousel-text').html($('#slide-content-'+id).html());
                });
                });
             </script>
             <div class="col-lg-12 col-md-12 font_color" style="margin-top:10px; ">
                <div class="aboutus" style="text-align: center;">
                  <h1 class="aboutus-title">Location</h1>
                </div>

                <h4><i class="fa fa-map-marker" style="font-size:20px;color:#f6882c;    ">&nbsp;</i> Tempe, AZ 85281, United States &nbsp;&nbsp;</h4>
                <div class="col-md-6">
                   <button id="mapBtn" type="button" class="row btn btn-lg btn-primary" onclick="myFunction1()" style="width: -webkit-fill-available;margin-right: 0px;    background-color: #8c57c4;border: 1px solid #8c57c4;border-radius: 8px;">Google Map</button>
                </div>
                <div class="col-md-6">
                   <button id="streetMapBtn" type="button" class="row btn btn-lg btn-primary" onclick="myFunction2()" style="width: -webkit-fill-available;margin-right: 0px;background-color: #f6872c;border: 1px solid #f6872c;border-radius: 8px;">Street View</button>
                </div>
             </div>
             <div id="mapView" class="col-lg-12 col-md-12 font_color" style="display: block;">
                <h4><b>Google Map</b></h4>
                        <?php if($map_normal_view){ ?>
                           <iframe src="<?=$map_normal_view?>" width="100%" height="300" frameborder="0" style="border:0"></iframe>
                        <?php } else { ?>
                           <iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d857403.4972211611!2d-112.11521492080176!3d32.91845730258146!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x872b08db3b33540f%3A0x666e717f473c3033!2sArizona%20State%20University%2C%20Tempe%20Campus%2C%20Tempe%2C%20AZ%2085281%2C%20United%20States!3m2!1d33.424239899999996!2d-111.9280527!5e0!3m2!1sen!2sin!4v1587738931155!5m2!1sen!2sin" width="100%" height="300" frameborder="0" style="border:0"></iframe>
                     <?php } ?>
             </div>
             <div id="streetView" class="col-lg-12 col-md-12 font_color" style="display: none;">
                <h4><b>Street View</b></h4>
                        <?php if($map_street_view){ ?>
                           <iframe src="<?=$map_street_view?>" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        <?php } else { ?>
                           <iframe src="https://www.google.com/maps/embed?pb=!4v1587740125967!6m8!1m7!1sHqREHZVc5rrylHbAbxa6Xw!2m2!1d33.30614513898816!2d-111.6780849645335!3f281.6317150135525!4f0!5f0.7820865974627469" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                     <?php } ?>
             </div>
             <script type="text/javascript">
                function myFunction1() {
                var x = document.getElementById("mapView");
                var y = document.getElementById("streetView");
                x.style.display = "block";
                y.style.display = "none";

                $("#mapBtn").css({"border": "1px solid #8c57c4" , "background-color": "#8c57c4"});
                $("#streetMapBtn").css({"border": "1px solid #f6872c" , "background-color": "#f6872c"});
                }
                function myFunction2() {
                var y = document.getElementById("streetView");
                var x = document.getElementById("mapView");
                y.style.display = "block";
                x.style.display = "none";


                $("#mapBtn").css({"border": "1px solid #f6872c" , "background-color": "#f6872c"});
                $("#streetMapBtn").css({"border": "1px solid #8c57c4" , "background-color": "#8c57c4"});
                }
             </script>
             <!-- <div class="col-lg-12">
                <div class="col-lg-6 col-md-6 font_color">
                   <h4>Total Students</h4>
                   <div class="total">
                      <p class="total_text">In Total<br/>
                         <span class="total_text2"><?php if(isset($student)) echo $student;?></span>
                      </p>
                   </div>
                   <div class="people"><img src="<?php echo base_url();?>images/1.png"/></div>
                   <div class="clearfix"></div>
                </div>
                <div class="col-lg-6 col-md-6 font_color">
                   <h4>International Students</h4>
                   <div class="total">
                      <p class="total_text">In Total<br/>
                         <span class="total_text2"><?php if(isset($internationalstudent)) echo $internationalstudent;?></span>
                      </p>
                   </div>
                   <div class="people"><img src="<?php echo base_url();?>images/1.png"/></div>
                </div>
                <div class="clearfix visible-xs visible-sm "></div>
                <?php if(isset($facilities)) {?>
                <div class="col-lg-6">
                   <h4>Facility</h4>
                   <p style="color:#374552;"><?=$facilities;?></p>
                </div>
                <?php
                   }?>
                </div> -->
             <div class="clearfix "></div>
             <!-- <div class="col-lg-12">
                <div class="col-lg-6 col-md-6 font_color">
                   <h4>National Ranking</h4>
                   <div class="total">
                      <p class="total_text"><?=$ranking_usa?></p>
                   </div>
                </div>
                <div class="col-lg-6 col-md-6 font_color">
                   <h4>World Ranking</h4>
                   <div class="total">
                      <div class="total">
                         <p class="total_text"><?=$ranking?></p>
                      </div>
                   </div>
                </div>
                </div> -->
             <!--  <div class="clearfix "></div>
                <div class="col-lg-12">
                   <div class="col-lg-6 col-md-6 font_color">
                      <h4>Placement Percentage</h4>
                      <div class="total">
                         <p class="total_text"><?=$placement_percentage?> %</p>
                      </div>
                   </div>
                   <div class="col-lg-6 col-md-6 font_color">
                      <h4>Admission Success Rate</h4>
                      <div class="total">
                         <div class="total">
                            <p class="total_text"><?=$admission_success_rate?> %</p>
                         </div>
                      </div>
                   </div>
                </div> -->
             <div class="clearfix "></div>
             <!--<div  style="white-space:nowrap;">-->
             <?php
                if($images)
                {
                  ?>
             <h4>Images / Videos</h4>
             <?php
                foreach($images as $image)
                {
                  ?>
             <div class="col-lg-5 col-md-3 col-sm-3 fancy_box"><img src="<?=$image?>" width="100%" height="135px;"  alt="" /></div>
             <?php
                }
                }
                ?>
             <div class="col-md-12 feature-area" style="margin-top: 20px;">

                 <div class="aboutus" style="text-align: center;">
                  <h1 class="aboutus-title">Browse by Discipline</h1>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 feature-box">
                           <?php
                           foreach ($browse_discipline as $bdkey => $bdvalue) {
                              // code...

                           ?>
                   <div class="col-md-3 col-sm-6">
                      <div class="feature-item">
                         <span class=" feature__icon" onclick="openCourseListModal(<?php echo $bdvalue['id']; ?>,<?php echo $bdvalue['university_id']; ?>,'<?php echo $bdvalue['name']; ?>')"><i class="fas <?php echo $bdvalue['icon_class']; ?>" style="font-size: 40px"></i></span>
                         <h3 class="feature__title">
                            <a  href="#" onclick="openCourseListModal(<?php echo $bdvalue['id']; ?>,<?php echo $bdvalue['university_id']; ?>,'<?php echo $bdvalue['name']; ?>')"><?php echo $bdvalue['name']; ?></a>
                         </h3>
                      </div>
                      <!-- end feature-item -->
                   </div>
                         <?php } ?>

                   <!-- end col-md-4 -->
                </div>
                <!-- end row -->
             </div>
             <!--  <button class="btn btn-primary" onclick="openCourseListPopup()">click</button> -->
             <!-- COURSE MODAL -->
             <div id="myCourseListModal" class="coursemodal" >
                <!-- Modal content -->
                <div class="row coursemodal-content">

                   <span class="close" id="courseListClose">&times;</span>
                   <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="course-list-modal">

                      <h4><b>Browse by Discipline : </b><font color="#f9992f"><b>Science</b></font></h4>

                       <div class="col-md-12" id="course_tab">
                           <div class="uni-boxPurple">
                              <div class="flLt">
                                 <div class="uni-image">
                                    <a target="_blank" href="#"><img class="lazy" alt="" title="" align="center" width="190px" height="175px" src="" style="display: inline;"></a>
                                    <div class="uni-shrtlist-image"></div>
                                 </div>
                              </div>
                              <div class="uni-detailModal">
                                 <div class="uni-title">
                                    <div class="col-md-9">
                                       <h4 class="card-title" style="color: rgb(0, 75, 122)"><i class="fa fa-graduation-cap" style="font-size:15px;color:#374552">&nbsp;</i>Course : <b>course NAME</b></h4>
                                    </div>
                                    <div class="col-md-3">
                                       <h5 class="card-title">Compare : <b><input type="radio" name="compare_course" onclick="openComparePopup('<?=$course_detail['name']?>')"></b></h5>
                                    </div>
                                 </div>
                                 <div class="clearwidth">
                                    <div class="iconRowPurple">
                                      <div class="col-md-12 twoPXPadding">
                                         <div class="col-md-4 twoPXPadding">
                                             <div class="col-md-2"><i class="fas fa-school iconCss" >&nbsp;&nbsp;</i></div>
                                             <div class="col-md-10 faPadding"><strong>  Department Name <span>&nbsp; DEPT 11</span></strong></div>
                                          </div>
                                          <div class="col-md-4 twoPXPadding">
                                             <div class="col-md-2"><i class="far fa-clock iconCss" >&nbsp;&nbsp;</i></div>
                                             <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong>  Duration <span>&nbsp; 12 Months</span></strong></div>
                                          </div>
                                          <div class="col-md-4 twoPXPadding">
                                             <div class="col-md-2"><i class="far fa-calendar-alt iconCss"></i></div>
                                             <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong>  Start Month <span>&nbsp; Aug 2021</span></strong></div>
                                          </div>
                                      </div>

                                      <div class="col-md-12 twoPXPadding" style="margin-top: 8px !important;    padding-bottom: 9px;border-bottom: 2px solid black;">
                                          <div class="col-md-4 twoPXPadding">
                                             <div class="col-md-2"><i class="fas fa-money-check-alt iconCss"></i></div>
                                             <div class="col-md-10 faPadding"><strong>  Application Fees <span>&nbsp; $34,160.00 (<i class="fa fa-inr"></i>26,05,981)  per year</span></strong></div>
                                          </div>
                                          <div class="col-md-4 twoPXPadding">
                                             <div class="col-md-2"><i class="fa fa-money iconCss" >&nbsp;&nbsp;</i></div>
                                             <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong>  Salary Expected<span>&nbsp; yes</span></strong></div>
                                          </div>
                                          <div class="col-md-4 twoPXPadding">
                                             <div class="col-md-2"><i class="fas fa-university iconCss"></i></div>
                                             <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong>  Stay Back Option <span>&nbsp; yes</span></strong></div>
                                          </div>
                                      </div>
                                    </div>

                                 </div>
                                  <div class="row" style="margin-top: 10px !important">
                                    <div class="col-md-8 btn-col btn-brochure" style="text-align: left;margin-top: 12px;">
                                       <a href="" class="button-style" target="_blank" style="background-color: #8c57c4;">View Detail</a>
                                    </div>
                                    <div class="col-md-4 font14 mrgtop float-right flRt">
                                       <i class="fa fa-eye" >&nbsp;100</i> | &nbsp;


                                       <i class="fa fa-heart" >&nbsp;220</i> | &nbsp;


                                      <i class="fa fa-star" >&nbsp;6</i>

                                    </div>
                                  </div>
                              </div>
                           </div>
                       </div>
                       <div class="col-md-12" >
                           <div class="uni-boxPurple">
                              <div class="flLt">
                                 <div class="uni-image">
                                    <a target="_blank" href="#"><img class="lazy" alt="" title="" align="center" width="190px" height="175px" src="" style="display: inline;"></a>
                                    <div class="uni-shrtlist-image"></div>
                                 </div>
                              </div>
                              <div class="uni-detail">
                                 <div class="uni-title">
                                    <div class="col-md-9">
                                       <h4 class="card-title" style="color: rgb(0, 75, 122)"><i class="fa fa-graduation-cap" style="font-size:15px;color:#374552">&nbsp;</i>Course : <b>course NAME</b></h4>
                                    </div>
                                    <div class="col-md-3">
                                       <h5 class="card-title">Compare : <b><input type="radio" name="compare_course" onclick="openComparePopup('<?=$course_detail['name']?>')"></b></h5>
                                    </div>
                                 </div>
                                 <div class="clearwidth">
                                    <div class="iconRowPurple">
                                      <div class="col-md-12 twoPXPadding">
                                         <div class="col-md-4 twoPXPadding">
                                             <div class="col-md-2"><i class="fas fa-school iconCss" >&nbsp;&nbsp;</i></div>
                                             <div class="col-md-10 faPadding"><strong>  Department Name <span>&nbsp; DEPT 11</span></strong></div>
                                          </div>
                                          <div class="col-md-4 twoPXPadding">
                                             <div class="col-md-2"><i class="far fa-clock iconCss" >&nbsp;&nbsp;</i></div>
                                             <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong>  Duration <span>&nbsp; 12 Months</span></strong></div>
                                          </div>
                                          <div class="col-md-4 twoPXPadding">
                                             <div class="col-md-2"><i class="far fa-calendar-alt iconCss"></i></div>
                                             <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong>  Start Month <span>&nbsp; Aug 2021</span></strong></div>
                                          </div>
                                      </div>

                                      <div class="col-md-12 twoPXPadding" style="margin-top: 8px !important;    padding-bottom: 9px;border-bottom: 2px solid black;">
                                          <div class="col-md-4 twoPXPadding">
                                             <div class="col-md-2"><i class="fas fa-money-check-alt iconCss"></i></div>
                                             <div class="col-md-10 faPadding"><strong>  Application Fees <span>&nbsp; $34,160.00 (<i class="fa fa-inr"></i>26,05,981)  per year</span></strong></div>
                                          </div>
                                          <div class="col-md-4 twoPXPadding">
                                             <div class="col-md-2"><i class="fa fa-money iconCss" >&nbsp;&nbsp;</i></div>
                                             <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong>  Salary Expected<span>&nbsp; yes</span></strong></div>
                                          </div>
                                          <div class="col-md-4 twoPXPadding">
                                             <div class="col-md-2"><i class="fas fa-university iconCss"></i></div>
                                             <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong>  Stay Back Option <span>&nbsp; yes</span></strong></div>
                                          </div>
                                      </div>
                                    </div>

                                 </div>
                                  <div class="row" style="margin-top: 10px !important">
                                    <div class="col-md-8 btn-col btn-brochure" style="text-align: left;margin-top: 12px;">
                                       <a href="" class="button-style" target="_blank" style="background-color: #8c57c4;">View Detail</a>
                                    </div>
                                    <div class="col-md-4 font14 mrgtop float-right flRt">
                                       <i class="fa fa-eye" >&nbsp;100</i> | &nbsp;


                                       <i class="fa fa-heart" >&nbsp;220</i> | &nbsp;


                                      <i class="fa fa-star" >&nbsp;6</i>

                                    </div>
                                  </div>
                              </div>
                           </div>
                       </div>
                       <div class="col-md-12" >
                           <div class="uni-boxPurple">
                              <div class="flLt">
                                 <div class="uni-image">
                                    <a target="_blank" href="#"><img class="lazy" alt="" title="" align="center" width="190px" height="175px" src="" style="display: inline;"></a>
                                    <div class="uni-shrtlist-image"></div>
                                 </div>
                              </div>
                              <div class="uni-detail">
                                 <div class="uni-title">
                                    <div class="col-md-9">
                                       <h4 class="card-title" style="color: rgb(0, 75, 122)"><i class="fa fa-graduation-cap" style="font-size:15px;color:#374552">&nbsp;</i>Course : <b>course NAME</b></h4>
                                    </div>
                                    <div class="col-md-3">
                                       <h5 class="card-title">Compare : <b><input type="radio" name="compare_course" onclick="openComparePopup('<?=$course_detail['name']?>')"></b></h5>
                                    </div>
                                 </div>
                                 <div class="clearwidth">
                                    <div class="iconRowPurple">
                                      <div class="col-md-12 twoPXPadding">
                                         <div class="col-md-4 twoPXPadding">
                                             <div class="col-md-2"><i class="fas fa-school iconCss" >&nbsp;&nbsp;</i></div>
                                             <div class="col-md-10 faPadding"><strong>  Department Name <span>&nbsp; DEPT 11</span></strong></div>
                                          </div>
                                          <div class="col-md-4 twoPXPadding">
                                             <div class="col-md-2"><i class="far fa-clock iconCss" >&nbsp;&nbsp;</i></div>
                                             <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong>  Duration <span>&nbsp; 12 Months</span></strong></div>
                                          </div>
                                          <div class="col-md-4 twoPXPadding">
                                             <div class="col-md-2"><i class="far fa-calendar-alt iconCss"></i></div>
                                             <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong>  Start Month <span>&nbsp; Aug 2021</span></strong></div>
                                          </div>
                                      </div>

                                      <div class="col-md-12 twoPXPadding" style="margin-top: 8px !important;    padding-bottom: 9px;border-bottom: 2px solid black;">
                                          <div class="col-md-4 twoPXPadding">
                                             <div class="col-md-2"><i class="fas fa-money-check-alt iconCss"></i></div>
                                             <div class="col-md-10 faPadding"><strong>  Application Fees <span>&nbsp; $34,160.00 (<i class="fa fa-inr"></i>26,05,981)  per year</span></strong></div>
                                          </div>
                                          <div class="col-md-4 twoPXPadding">
                                             <div class="col-md-2"><i class="fa fa-money iconCss" >&nbsp;&nbsp;</i></div>
                                             <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong>  Salary Expected<span>&nbsp; yes</span></strong></div>
                                          </div>
                                          <div class="col-md-4 twoPXPadding">
                                             <div class="col-md-2"><i class="fas fa-university iconCss"></i></div>
                                             <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong>  Stay Back Option <span>&nbsp; yes</span></strong></div>
                                          </div>
                                      </div>
                                    </div>

                                 </div>
                                  <div class="row" style="margin-top: 10px !important">
                                    <div class="col-md-8 btn-col btn-brochure" style="text-align: left;margin-top: 12px;">
                                       <a href="" class="button-style" target="_blank" style="background-color: #8c57c4;">View Detail</a>
                                    </div>
                                    <div class="col-md-4 font14 mrgtop float-right flRt">
                                       <i class="fa fa-eye" >&nbsp;100</i> | &nbsp;


                                       <i class="fa fa-heart" >&nbsp;220</i> | &nbsp;


                                      <i class="fa fa-star" >&nbsp;6</i>

                                    </div>
                                  </div>
                              </div>
                           </div>
                       </div>


                   </div>
                </div>
             </div>

         </div>
         <div id="programView" class="col-lg-12 col-md-12 font_color" style="display: none;">
            <div class="col-md-12" >
                <div class="aboutus" style="text-align: center;">
                  <h1 class="aboutus-title">POPULAR PROGRAMS</h1>
                </div>
            </div>
                     <div class="col-sm-3" style="padding: 0px;">
                        <ul id="nav-tabs-wrapper" class="nav nav-tabs nav-pills nav-stacked well" style="padding: 5px; background: #ffffff;">
                           <?php
                              foreach($colleges as $index => $college)
                              {
                                ?>
                           <li class="<?=($index == 0) ? 'active': ''?>" style="display: block; padding: 0px;background: #ffffff;"><a href="#" data-toggle="tab" data-target="#college-<?=$index?>" aria-expanded="true" aria-controls="vtab1" style="color: #f6882c; margin: 0px; padding: 5px;box-shadow: 1px 1px 2px #bbb8b8;text-transform: none;letter-spacing: 0.3px;    font-size: 16px;"><?=$college?>test</a></li>
                           <?php
                              }
                              ?>
                        </ul>
                     </div>
                     <div class="col-sm-9">
                        <div class="tab-content">

                           <?php
                              foreach($colleges as $index => $college)
                              {
                                ?>
                           <div role="tabpanel" class="tab-pane fade <?=($index == 0) ? 'in active': ''?>" id="college-<?=$index?>">
                              <?php
                                 foreach($courses_details as $course_detail)
                                 {
                                  if($course_detail['college'] == $college)
                                  {
                                    ?>
                              <div class="col-md-12" >
                                 <div class="uni-boxPurple">
                                    <div class="flLt">
                                       <div class="uni-image">
                                          <a target="_blank" href="#"><img class="lazy" alt="" title="" align="center" width="190px" height="175px" src="<?=$courses['university_banner']?>" style="display: inline;"></a>
                                          <div class="uni-shrtlist-image"></div>
                                       </div>
                                    </div>
                                    <div class="uni-detail">
                                       <div class="uni-title">
                                          <div class="col-md-9">
                                             <h4 class="card-title" style="color: rgb(0, 75, 122)"><i class="fa fa-graduation-cap" style="font-size:15px;color:#374552">&nbsp;</i>Course : <b><?=ucwords($course_detail['name'])?></b></h4>
                                          </div>
                                          <div class="col-md-3">
                                             <h5 class="card-title">Compare : <b><input type="radio" name="compare_course" onclick="openComparePopup('<?=$course_detail['name']?>')"></b></h5>
                                          </div>
                                       </div>
                                       <div class="clearwidth">
                                          <div class="iconRowPurple">
                                            <div class="col-md-12 twoPXPadding">
                                               <div class="col-md-4 twoPXPadding">
                                                   <div class="col-md-2"><i class="fas fa-school iconCss" >&nbsp;&nbsp;</i></div>
                                                   <div class="col-md-10 faPadding"><strong>  Department Name <span>&nbsp; <?=ucwords($course_detail['department'])?></span></strong></div>
                                                </div>
                                                <div class="col-md-4 twoPXPadding">
                                                   <div class="col-md-2"><i class="far fa-clock iconCss" >&nbsp;&nbsp;</i></div>
                                                   <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong>  Duration <span>&nbsp; <?=$course_detail['course_duration']?> Months</span></strong></div>
                                                </div>
                                                <div class="col-md-4 twoPXPadding">
                                                   <div class="col-md-2"><i class="far fa-calendar-alt iconCss"></i></div>
                                                   <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong>  Start Month <span>&nbsp; <?=$course_detail['start_months']?></span></strong></div>
                                                </div>
                                            </div>

                                            <div class="col-md-12 twoPXPadding" style="margin-top: 8px !important;    padding-bottom: 9px;border-bottom: 2px solid black;">
                                                <div class="col-md-4 twoPXPadding">
                                                   <div class="col-md-2"><i class="fas fa-money-check-alt iconCss"></i></div>
                                                   <div class="col-md-10 faPadding"><strong>  Application Fees <span>&nbsp; <?=$course_detail['application_fee_course']?></span></strong></div>
                                                </div>
                                                <div class="col-md-4 twoPXPadding">
                                                   <div class="col-md-2"><i class="fa fa-money iconCss" >&nbsp;&nbsp;</i></div>
                                                   <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong>  Salary Expected<span>&nbsp; <?=$course_detail['eligibility_salary']?></span></strong></div>
                                                </div>
                                                <div class="col-md-4 twoPXPadding">
                                                   <div class="col-md-2"><i class="fas fa-university iconCss"></i></div>
                                                   <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong>  Stay Back Option <span>&nbsp; <?=$course_detail['stay_back_option']?> / Months</span></strong></div>
                                                </div>
                                            </div>
                                          </div>

                                       </div>
                                        <div class="row" style="margin-top: 10px !important">
                                          <div class="col-md-6 btn-col btn-brochure" style="text-align: left;margin-top: 12px;">
                                             <a href="/course/coursemaster/courselist/<?=$course_detail['course_id']?>" class="button-style" target="_blank" style="background-color: #8c57c4;">View Detail</a>
                                          </div>
                                          <div class="col-md-4 font14 mrgtop float-right flRt">
                                             <i class="fa fa-eye" >&nbsp;<?=$course_detail['course_view_count']?></i> | &nbsp;

                                             <i class="fa fa-heart" >&nbsp;<?=$course_detail['course_fav_count']?></i> | &nbsp;

                                            <i class="fa fa-star" >&nbsp;<?=$course_detail['course_likes_count']?></i>

                                          </div>
                                         <div class="col-md-2 flRt customInputs float-right mrgtop">

                                            <i class="heart fa fa-heart-o"> </i>

                                             <span class="common-sprite"></span>
                                         </div>
                                        </div>
                                    </div>
                                 </div>
                              </div>
                              <?php
                                 }
                                 }
                                 ?>
                              <!--  <table class="table">
                                 <thead>
                                    <th>Course</th>
                                    <th>College</th>
                                    <th>Department</th>
                                    <th>Degree</th>
                                    <th>Duration</th>
                                    <th>Total Fees ($)</th>
                                    <th>Compare</th>
                                 </thead>
                                 <tbody style="background-color: white;">
                                    <?php
                                    foreach($courses_details as $course_detail)
                                    {
                                      if($course_detail['college'] == $college)
                                      {
                                        ?>
                                    <tr>
                                       <td><?=ucwords($course_detail['name'])?></td>
                                       <td><?=ucwords($course_detail['college'])?></td>
                                       <td><?=ucwords($course_detail['department'])?></td>
                                       <td><?=ucwords($course_detail['degree'])?></td>
                                       <td><?=$course_detail['course_duration']?> Months</td>
                                       <td><?=$course_detail['total_fees']?></td>
                                       <td><input type="radio" name="compare_course" onclick="openComparePopup('<?=$course_detail['name']?>')"></td>
                                    </tr>
                                    <?php
                                    }
                                    }
                                    ?>
                                 </tbody>
                                 </table> -->
                           </div>
                           <?php
                              }
                              ?>
                        </div>
                     </div>
                     <?php
                        foreach($colleges as $college)
                        {/*
                          ?>
                     <button class="accordion"><?=$college?></button>
                     <div class="panel">
                        <table>
                           <thead>
                              <th>Course</th>
                              <th>College</th>
                              <th>Degree</th>
                              <th>Duration</th>
                              <th>Total Fees</th>
                           </thead>
                           <tbody>
                              <?php
                                 foreach($courses_details as $course_detail)
                                 {
                                  if($course_detail['college'] == $college)
                                  {
                                    ?>
                              <tr>
                                 <td><?=ucwords($course_detail['name'])?></td>
                                 <td><?=ucwords($course_detail['college'])?></td>
                                 <td><?=ucwords($course_detail['degree'])?></td>
                                 <td><?=$course_detail['course_duration']?> Months</td>
                                 <td><?=$course_detail['total_fees']?></td>
                              </tr>
                              <?php
                                 }
                                 }
                                 ?>
                           </tbody>
                        </table>
                     </div>
                     <?php
                        */
                        }
                        ?>
         </div>
      </section>
      <div class="cd-tabs ">
         <div>

             <!--   <div class="font_class" style="background-color:#374552;">
                  <nav class="col-lg-9 col-lg-offset-9 col-md-9" style="width:100%;">
                     <ul class="cd-tabs-navigation" style="margin-left:-15px;">
                        <li><a data-content="about" class="selected" href="#0"><b>HOME</b></a></li>

                        <li><a data-content="course_content" href="#0"><b>POPULAR PROGRAMS</b></a></li>

                     </ul>

                  </nav>
               </div> -->
               <div class="clearfix"></div>
            </div>

         </div>

         <div class="clearfix visible-md visible-sm "></div>
         <!-- cd-tabs-content -->
         <div class="clearfix"></div>
         </p>
      </div>
   </div>
   <!-- The Modal -->
   <div id="myModal" class="modal">
      <!-- Modal content -->
      <div class="row modal-content">
         <span class="close">&times;</span>
         <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="compare-university-modal"></div>
      </div>
   </div>
   <style type="text/css">
      .mrg{
      margin: 10px 5px;
      }
      .ht30{
      height: 30px;
      }
   </style>
   <div id="consent-modal" class="modal">

    <!-- Modal content -->
    <div class="row modal-content">
      <span class="close" id="consent-close">&times;</span>
      <form class="col-md-12 col-lg-12 col-sm-12 col-xs-12 form-group">
          <div id="fieldsBox">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 mrg">
                  <div class="col-md-3">
                     <input type="text" name="user_name" id="user_name" class="form-control ht30" placeholder="Your Name" value="<?=isset($user_master) && $user_master['name'] ? $user_master['name'] : ''?>">
                  </div>
                  <div class="col-md-3">
                     <input type="text" name="user_email" id="user_email" class="form-control ht30" value="<?=isset($user_master) && $user_master['email'] ? $user_master['email'] : 'Your Email'?>" disabled>
                  </div>
                  <div class="col-md-3">
                  <input type="text" name="user_phone" id="user_phone" class="form-control ht30" value="<?=isset($user_master) && $user_master['phone'] ? $user_master['phone'] : 'Your Phone'?>" disabled>
                </div>
                  <div class="col-md-3">
                  <input type="text" name="user_passport" id="user_passport" class="form-control ht30" value="<?=isset($student_details) && $student_details['passport_number'] ? $student_details['passport_number'] : ''?>" placeholder="Your Passport Number">
                </div>
          </div>
           <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 mrg">
                <div class="col-md-4">
                  <input type="date" name="user_dob" id="user_dob" class="form-control ht30" placeholder="Your DOB" value="<?=isset($student_details) && $student_details['dob'] ? $student_details['dob'] : ''?>">
                </div>
                  <div class="col-md-4">
                  <select class="form-control ht30" id="user_country" name="user_country">
                          <option value="">Select Country</option>
                          <?php
                          foreach($locationcountries as $location)
                          {
                              ?>
                              <option value="<?=$location->id . '|' . $location->name?>" <?=(isset($student_details) && $student_details['country'] && $student_details['country'] == $location->id) ? 'selected' : ''?>><?=$location->name?></option>
                              <?php
                          }
                          ?>
                      </select>
                </div>
                  <div class="col-md-4">
                  <select class="form-control ht30" id="user_city" name="user_city">
                          <option value="">Select City</option>
                          <?php
                          foreach($cities as $city)
                          {
                              ?>
                              <option value="<?=$city->id . '|' . $city->city_name?>" <?=(isset($student_details) && $student_details['city'] && $student_details['city'] == $city->id) ? 'selected' : ''?>><?=$city->city_name?></option>
                              <?php
                          }
                          ?>
                      </select>
                </div>
           </div>
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 mrg">
                <div class="col-md-4">
                    <select class="form-control ht30" id="user_mainstream" name="user_mainstream">
                        <option value="">Select Main Stream</option>
                        <?php
                        foreach($mainstreams as $mainstream)
                        {
                            ?>
                            <option value="<?=$mainstream->id . '|' . $mainstream->display_name?>" <?=(isset($lead_master) && $lead_master['mainstream'] && $lead_master['mainstream'] == $mainstream->id) ? 'selected' : ''?>><?=$mainstream->display_name?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-4">
                    <select class="form-control ht30" id="user_subcourse" name="user_subcourse">
                        <?php
                        if($subCourses)
                        {
                            ?>
                            <option value="">Select Subcourse</option>
                            <?php
                            foreach($subCourses as $subCourse)
                            {
                                ?>
                                <option value="<?=$subCourse->id . '|' . $subCourse->display_name?>" <?=(isset($lead_master) && $lead_master['courses'] && $lead_master['courses'] == $subCourse->id) ? 'selected' : ''?>><?=$subCourse->display_name?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-4">
                    <select class="form-control ht30" id="user_level" name="user_level">
                        <option value="">Select Level</option>
                        <?php
                        foreach($levels as $level)
                        {
                            ?>
                            <option value="<?=$level->id . '|' . $level->name?>" <?=(isset($lead_master) && $lead_master['degree'] && $lead_master['degree'] == $level->id) ? 'selected' : ''?>><?=$level->name?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 mrg">
                  <div class="col-md-4">
                  <select class="form-control ht30" id="user_intake_year" name="user_intake_year">
                          <option value="">Select Intake Year</option>
                          <option value="2020" <?=(isset($student_details) && $student_details['intake_year'] && $student_details['intake_year'] == "2020") ? 'selected' : ''?> >2020</option>
                          <option value="2021" <?=(isset($student_details) && $student_details['intake_year'] && $student_details['intake_year'] == "2021") ? 'selected' : ''?> >2021</option>
                          <option value="2022" <?=(isset($student_details) && $student_details['intake_year'] && $student_details['intake_year'] == "2022") ? 'selected' : ''?> >2022</option>
                      </select>
                </div>
                  <div class="col-md-4">
                  <select class="form-control ht30" id="user_intake_month" name="user_intake_month">
                          <option value="">Select Intake Month</option>
                          <option value="Jan-Mar" <?=(isset($student_details) && $student_details['intake_month'] && $student_details['intake_month'] == "Jan-Mar") ? 'selected' : ''?> >January - March</option>
                          <option value="Apr-Jun" <?=(isset($student_details) && $student_details['intake_month'] && $student_details['intake_month'] == "Apr-Jun") ? 'selected' : ''?> >April - June</option>
                          <option value="Aug-Oct" <?=(isset($student_details) && $student_details['intake_month'] && $student_details['intake_month'] == "Aug-Oct") ? 'selected' : ''?> >August - October</option>
                      </select>
                </div>
                  <div class="col-md-4">
                  <select class="form-control ht30" id="user_hear_about_us" name="user_hear_about_us">
                          <option value="">From Where Did You Hear About Us?</option>
                          <option value="Friends / Family" <?=(isset($student_details) && $student_details['hear_about_us'] && $student_details['hear_about_us'] == "Friends / Family") ? 'selected' : ''?> >Friends / Family</option>
                          <option value="Google / Facebook" <?=(isset($student_details) && $student_details['hear_about_us'] && $student_details['hear_about_us'] == "Google / Facebook") ? 'selected' : ''?> >Google / Facebook</option>
                          <option value="Stupidsid Seminar" <?=(isset($student_details) && $student_details['hear_about_us'] && $student_details['hear_about_us'] == "Stupidsid Seminar") ? 'selected' : ''?> >Stupidsid Seminar</option>
                          <option value="Stupidsid Website" <?=(isset($student_details) && $student_details['hear_about_us'] && $student_details['hear_about_us'] == "Stupidsid Website") ? 'selected' : ''?> >Stupidsid Website</option>
                          <option value="College Seminar" <?=(isset($student_details) && $student_details['hear_about_us'] && $student_details['hear_about_us'] == "College Seminar") ? 'selected' : ''?> >College Seminar</option>
                          <option value="News Paper" <?=(isset($student_details) && $student_details['hear_about_us'] && $student_details['hear_about_us'] == "News Paper") ? 'selected' : ''?> >News Paper</option>
                          <option value="Calling from Imperial" <?=(isset($student_details) && $student_details['hear_about_us'] && $student_details['hear_about_us'] == "Calling from Imperial") ? 'selected' : ''?> >Calling from Imperial</option>
                      </select>
                </div>
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 mrg" style="text-align: center;margin-top : 15px;">

                       <!--input type="button" class="btn btn-lg btn-primary" name="submitValue" value="submit" id="submitValue" onclick="openConsentModal()" disabled="disabled"-->
                       <input type="button" class="btn btn-lg btn-primary" name="submitValue" value="submit" id="submitValue" onclick="openConsentModal()">

               </div>
            </div>
       </div>
       <div class="col-md-12 mrg" id="consentBlock" style="display: none;">
         <!-- consent form -->
           <?=$consent?>

         <div class="col-md-12" style="text-align: right;margin-top : 15px;"><input type="button" class="btn btn-lg btn-primary" name="agreed" value="Agreed" onclick="connectUniversity()"></div>
         <!-- consent form end -->
       </div>
        <div class="col-md-12" ></div>

      </form>

    </div>

   </div>
</div>
<div id="courseDataModal" class="modal">
<!-- Modal content style="display: block;"-->
   <div class="modal-content">
      <span class="close closeFillFairData">&times;</span>
      <div id="fair-data-modal" style="text-align: center;">

        <div class="row">
            <div class="col-md-12 login-sec">
                <h2 class="text-center" style="color: #8c57c5;">Select Course</h2>
                <form class="login-form" class="form-horizontal" onsubmit="applyUniversity()" method="post" autocomplete="off">

        <div class="col-md-12 form-group">
          <div class="col-md-12">
            <label for="inputEmail3" class="text-uppercase"> University Name </label>
             <input type="text" class="form-control" style="width:100%; height:30px; font-size:12px;" name="university_name" id="university_name" >
          </div>
        </div>

        <div class="col-md-12 form-group">
          <div class="col-md-12">
            <label for="inputEmail3" class="text-uppercase"> Select Course </label>
             <input type="text" name="searchtextvalue" id="searchtextvalue" class="form-control search-slt typeahead-course" data-provide="typeahead" placeholder="Type Course Name & Select From Dropdown" style="width:100%; height:30px; font-size:12px;" >
          </div>
        </div>

        <div class="form-check" style="text-align: center;">

            <input type="submit"  style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Apply"></input>

        </div>


      </form>
  </div>
</div>

      </div>
   </div>


</div>

<script src="js/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/signature_pad/1.5.3/signature_pad.min.js"></script>

<script src="<?php echo base_url();?>js/bootstrap-typeahead-min.js"></script>

<script>
   var acc = document.getElementsByClassName("accordion");
   var i;

   for (i = 0; i < acc.length; i++) {
     acc[i].addEventListener("click", function() {
       this.classList.toggle("active");
       var panel = this.nextElementSibling;
       if (panel.style.display === "block") {
         panel.style.display = "none";
       } else {
         panel.style.display = "block";
     $("footer").animate({marginTop: "0px"}, 'fast');
       }
     });
   }

   // Get the modal
   var modal = document.getElementById("myModal");
   var consentModal = document.getElementById("consent-modal");

   // Get the button that opens the modal
   var btn = document.getElementById("myBtn");

   // Get the <span> element that closes the modal
   var span = document.getElementsByClassName("close")[0];
   var consentSpan = document.getElementById("consent-close");

   // When the user clicks the button, open the modal
   function openComparePopup(courseName) {
   jQuery.ajax({
        type: "GET",
        url: "/university/compare/getsimilar/?course=" + courseName,
          success: function(response) {
       document.getElementById("compare-university-modal").innerHTML = response;
       modal.style.display = "block";
            }
    });
   }

   function applyUniversity(university_id, university_name, university_email) {
   jQuery.ajax({
        type: "GET",
        url: "/university/details/apply/?uid=" + university_id + "&uname=" + university_name + "&uemail=" + university_email,
    success: function(response) {
       document.getElementById("apply_button").value = "Applied";
             document.getElementById("apply_button").disabled = true;
             document.getElementById("apply_button").style.backgroundColor = "#F6881F";
        }
    });
   }

   // When the user clicks on <span> (x), close the modal
   span.onclick = function() {
   modal.style.display = "none";
   }

   consentSpan.onclick = function() {
   consentModal.style.display = "none";
   }

   // When the user clicks anywhere outside of the modal, close it
   window.onclick = function(event) {
   if (event.target == modal) {
    modal.style.display = "none";
   }
   }

   var signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
    backgroundColor: 'rgba(255, 255, 255, 0)',
    penColor: 'rgb(0, 0, 0)'
   });

   function openConsent()
   {
    consentModal.style.display = "block";
   }

   function openConsentModal() {
       var country = document.getElementById("user_country").value;
       var split_country = country.split("|");
       var city = document.getElementById("user_city").value;
       var split_city = city.split("|");
       var mainstream = document.getElementById("user_mainstream").value;
       var split_mainstream = mainstream.split("|");
       var subcourse = document.getElementById("user_subcourse").value;
       var split_subcourse = subcourse.split("|");
       var level = document.getElementById("user_level").value;
       var split_level = level.split("|");

       var studentName = document.getElementById("user_name").value;
       var dob = document.getElementById("user_dob").value;
       var passport_number = document.getElementById("user_passport").value;
       var resident = split_city[1] + ', ' + split_country[1];
       var program = split_level[1] + ', ' + split_subcourse[1] + ', ' + split_mainstream[1];
       var intake = document.getElementById("user_intake_month").value + ', ' + document.getElementById("user_intake_year").value;
       var email = document.getElementById("user_email").value;
       var mobile = document.getElementById("user_phone").value;
       var university_name = '<?=$name?>';
       var today_date = '<?=date('Y-m-d')?>';

       for(i = studentName.length; i < 80; i++){
           studentName += '&nbsp;';
       }
       for(i = dob.length; i < 40; i++){
           dob += '&nbsp;';
       }
       for(i = passport_number.length; i < 65; i++){
           passport_number += '&nbsp;';
       }
       for(i = resident.length; i < 50; i++){
           resident += '&nbsp;';
       }
       for(i = program.length; i < 70; i++){
           program += '&nbsp;';
       }
       for(i = intake.length; i < 50; i++){
           intake += '&nbsp;';
       }
       for(i = university_name.length; i < 70; i++){
           university_name += '&nbsp;';
       }
       for(i = email.length; i < 70; i++){
           email += '&nbsp;';
       }
       for(i = mobile.length; i < 70; i++){
           mobile += '&nbsp;';
       }
       for(i = today_date.length; i < 40; i++){
           today_date += '&nbsp;';
       }

       document.getElementById("student_name_1").innerHTML = '<u>' + studentName + '</u>';
       document.getElementById("student_name_2").innerHTML = '<u>' + studentName + '</u>';
       document.getElementById("dob").innerHTML = '<u>' + dob + '</u>';
       document.getElementById("passport").innerHTML = '<u>' + passport_number + '</u>';
       document.getElementById("resident").innerHTML = '<u>' + resident + '</u>';
       document.getElementById("program").innerHTML = '<u>' + program + '</u>';
       document.getElementById("intake").innerHTML = '<u>' + intake + '</u>';
       document.getElementById("university").innerHTML = '<u>' + university_name + '</u>';
       document.getElementById("email").innerHTML = '<u>' + email + '</u>';
       document.getElementById("mobile").innerHTML = '<u>' + mobile + '</u>';
       document.getElementById("date").innerHTML = '<u>' + today_date + '</u>';

       document.getElementById('consentBlock').style.display = 'block' ;
       document.getElementById('fieldsBox').style.display = 'none' ;
    }

   function connectUniversity()
   {
       var country = document.getElementById("user_country").value;
       var split_country = country.split("|");
       var city = document.getElementById("user_city").value;
       var split_city = city.split("|");
       var mainstream = document.getElementById("user_mainstream").value;
       var split_mainstream = mainstream.split("|");
       var subcourse = document.getElementById("user_subcourse").value;
       var split_subcourse = subcourse.split("|");
       var level = document.getElementById("user_level").value;
       var split_level = level.split("|");

       var country = split_country[0];
       var city = split_city[0];
       var mainstream = split_mainstream[0];
       var subcourse = split_subcourse[0];
       var level = split_level[0];

       var data = signaturePad.toDataURL('image/png');
       document.getElementById("student_signature").value = data;
       document.getElementById("student_name").value = document.getElementById("user_name").value;
       document.getElementById("student_email").value = document.getElementById("user_email").value;
       document.getElementById("student_phone").value = document.getElementById("user_phone").value;
       document.getElementById("student_dob").value = document.getElementById("user_dob").value;
       document.getElementById("student_country").value = country;
       document.getElementById("student_city").value = city;
       document.getElementById("student_mainstream").value = mainstream;
       document.getElementById("student_subcourse").value = subcourse;
       document.getElementById("student_level").value = level;
       document.getElementById("student_passport").value = document.getElementById("user_passport").value;
       document.getElementById("student_intake_month").value = document.getElementById("user_intake_month").value;
       document.getElementById("student_intake_year").value = document.getElementById("user_intake_year").value;
       document.getElementById("student_hear_about_us").value = document.getElementById("user_hear_about_us").value;
       var form = document.getElementById("connect-university");
       form.submit();
   }

   function uploadSignature()
   {
       var formData = new FormData();
       formData.append('file', $('#upload-signature')[0].files[0]);

       $.ajax({
           url : '/university/details/uploadSignature',
           type : 'POST',
           data : formData,
           processData: false,  // tell jQuery not to process the data
           contentType: false,  // tell jQuery not to set contentType
           success : function(data) {
               if(data == 1)
               {
                   alert("Signature uploaded successfully");
               }
               else
               {
                   alert("Some issue occured while uploading signature");
               }
           }
       });
   }

   window.onscroll = function() {myFunction()};

    var header = document.getElementById("myHeaderone");
    var sticky = header.offsetTop;

    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
    }



   $.fn.jQuerySimpleCounter = function( options ) {
      var settings = $.extend({
          start:  0,
          end:    100,
          easing: 'swing',
          duration: 400,
          complete: ''
      }, options );

      var thisElement = $(this);

      $({count: settings.start}).animate({count: settings.end}, {
      duration: settings.duration,
      easing: settings.easing,
      step: function() {
        var mathCount = Math.ceil(this.count);
        thisElement.text(mathCount);
      },
      complete: settings.complete
    });
  };


$('#number1').jQuerySimpleCounter({end: <?php if(isset($student) && !empty($student)){ echo $student; } else { echo '0'; } ?>,duration: 3000});
$('#number2').jQuerySimpleCounter({end: <?php if(isset($internationalstudent) && !empty($internationalstudent)){ echo $internationalstudent; } else { echo '0'; } ?>,duration: 3000});
$('#number3').jQuerySimpleCounter({end: <?php if(isset($ranking_usa) && !empty($ranking_usa)){ echo $ranking_usa; } else { echo '0'; } ?>,duration: 2000});
$('#number4').jQuerySimpleCounter({end: <?php if(isset($ranking) && !empty($ranking)){ echo $ranking; } else { echo '0'; } ?>,duration: 2500});
$('#number5').jQuerySimpleCounter({end: <?php if(isset($admission_success_rate) && !empty($admission_success_rate)){ echo $admission_success_rate; } else { echo '0'; } ?>,duration: 2500});
$('#number6').jQuerySimpleCounter({end: <?php if(isset($placement_percentage) && !empty($placement_percentage)){ echo $placement_percentage; } else { echo '0'; } ?>,duration: 2500});


// New Script

 var CourseListModal = document.getElementById("myCourseListModal");

 //var coursemodalSpan = document.getElementsByClassName("coursemodalcllose")[0];
  var coursemodalSpan = document.getElementById("courseListClose");
 //   // When the user clicks on <span> (x), close the modal
 function openCourseListModal(cat_id,uni_id,cat_name){

       var dataString = 'cat_id=' + cat_id + "&uni_id=" + uni_id;

       $.ajax({

       type: "POST",

       url: "/university/details/getCourseByCatIdUniId",

       data: dataString,

       cache: false,

       success: function(result){

         //alert(result);
         CourseListModal.style.display = "block";
         console.log(result);

         var courses = result.courses;

         if(courses.length){

                //var courseHTMLL = '<form id="compare-course-form" name="compare-course-form" action="/course/compare" method="POST">';
                var courseHTMLL = '<h4><b>Browse by Discipline : </b><font color="#f9992f"><b>' + cat_name + '</b></font></h4>';

               courses.forEach(function(value, index){

               courseHTMLL += '<div class="col-md-12">'
                                 +'<div class="uni-boxPurple">'
                                 +'<div class="flLt">'
                                 +'<div class="uni-image">'
                                 +'<a target="_blank" href="#"><img class="lazy" alt="' + value.university_name + '" title="' + value.university_name + '" align="center" width="190px" height="175px" src="' + value.university_banner + '" style="display: inline;"></a>'
                                 +'<div class="uni-shrtlist-image"></div>'
                                 +'</div>'
                                 +'</div>'
                                 +'<div class="uni-detailModal">'
                                 +'<div class="uni-title">'
                                 +'<div class="col-md-9">'
                                 +'<h4 class="card-title" style="color: rgb(0, 75, 122)"><i class="fa fa-graduation-cap" style="font-size:15px;color:#374552">&nbsp;</i>Course : <b>'+value.name+'</b></h4>'
                                 +'</div>'
                              +'</div>'
                              +'<div class="clearwidth">'
                                  +'<div class="iconRowPurple">'
                                     +'<div class="col-md-12 col-sx-12 col-sm-12 twoPXPadding">'
                                          +'<div class="col-md-4 col-xs-12 col-sm-12 twoPXPadding">'
                                                +'<div class="col-md-2 col-xs-2 col-sm-2"><i class="fas fa-school iconCss" >&nbsp;&nbsp;</i></div>'
                                                +'<div class="col-md-10 col-xs-10 col-sm-10 faPadding"><strong>  Department Name <span>&nbsp; ' + value.department_name + '</span></strong></div>'
                                           +'</div>'
                                           +'<div class="col-md-4 twoPXPadding">'
                                                +'<div class="col-md-2 col-xs-2 col-sm-2"><i class="far fa-clock iconCss" >&nbsp;&nbsp;</i></div>'
                                                +'<div class="col-md-10 col-xs-10 col-sm-10 faPadding" style="margin-top: 10px;"><strong>  Duration <span>&nbsp; ' + value.duration + ' Months</span></strong></div>'
                                           +'</div>'
                                           +'<div class="col-md-4 col-xs-12 col-sm-12 twoPXPadding">'
                                                +'<div class="col-md-2 col-xs-2 col-sm-2"><i class="far fa-calendar-alt iconCss"></i></div>'
                                                +'<div class="col-md-10 col-xs-10 col-sm-10 faPadding" style="margin-top: 10px;"><strong>  Start Month <span>&nbsp; ' + value.start_months + '</span></strong></div>'
                                           +'</div>'
                                     +'</div>'
                                     +'<div class="col-md-12 twoPXPadding" style="margin-top: 8px !important;    padding-bottom: 9px;border-bottom: 2px solid black;">'
                                           +'<div class="col-md-4 col-xs-12 col-sm-12 twoPXPadding">'
                                                +'<div class="col-md-2 col-xs-2 col-sm-2"><i class="fas fa-money-check-alt iconCss"></i></div>'
                                                +'<div class="col-md-10 col-xs-10 col-sm-10 faPadding"><strong>  Application Fees <span>&nbsp; ' + value.application_fee + '  per year</span></strong></div>'
                                           +'</div>'
                                           +'<div class="col-md-4 col-xs-12 col-sm-12 twoPXPadding">'
                                                +'<div class="col-md-2 col-xs-2 col-sm-2"><i class="fa fa-money iconCss" >&nbsp;&nbsp;</i></div>'
                                                +'<div class="col-md-10 col-xs-10 col-sm-10 faPadding" style="margin-top: 10px;"><strong>  Salary Expected<span>&nbsp; ' + value.expected_salary + '</span></strong></div>'
                                           +'</div>'
                                           +'<div class="col-md-4 col-xs-12 col-sm-12 twoPXPadding">'
                                                +'<div class="col-md-2 col-xs-2 col-sm-2"><i class="fas fa-university iconCss"></i></div>'
                                                +'<div class="col-md-10 col-xs-10 col-sm-10 faPadding" style="margin-top: 10px;"><strong>  Stay Back Option <span>&nbsp; ' + value.no_of_month_work_permit + '</span></strong></div>'
                                           +'</div>'
                                     +'</div>'
                                  +'</div>'
                              +'</div>'
                               +'<div class="row" style="margin-top: 10px !important">'
                                  +'<div class="col-md-6 col-xs-12 col-sm-12 btn-col btn-brochure" style="text-align: left;margin-top: 12px;">'
                                       +'<a href="/course/coursemaster/courselist/'+ value.id +'" class="button-style" target="_blank" style="background-color: #8c57c4;">View Detail</a>'
                                  +'</div>'
                                  +'<div class="col-md-4 col-xs-12 col-sm-12 font14 mrgtop float-right flRt">'
                                       +'<i class="fa fa-eye" >&nbsp; '+ value.course_view_count +'</i> | &nbsp;'
                                       +'<i class="fa fa-heart" >&nbsp; '+ value.course_likes_count +'</i> | &nbsp;'
                                       +'<i class="fa fa-star" >&nbsp; '+ value.course_fav_count +'</i>'
                                  +'</div>'
                                  +'<div class="col-md-2 flRt customInputs float-right mrgtop">'
                                            +'<i class="heart fa fa-heart-o"> </i>'
                                             +'<span class="common-sprite"></span>'
                                  +'</div>'
                               +'</div>'
                         +'</div>'
                     +'</div>'
               +'</div>' ;

               });

               //courseHTMLL += '</form><p><a class="btn" href="#" target="_blank"></a></p>';
               document.getElementById("course-list-modal").innerHTML = courseHTMLL;

         }

      }

             });
}

    coursemodalSpan.onclick = function() {
   CourseListModal.style.display = "none";
   }

   window.onclick = function(event) {
   if (event.target == CourseListModal) {
    CourseListModal.style.display = "none";
   }
   }

   $(document).ready(function(){
        $('#user_mainstream').on('change', function() {
            var dataString = 'id='+this.value;
            $.ajax({
                type: "POST",
                url: "/search/university/getSubcoursesList",
                data: dataString,
                cache: false,
                success: function(result){
                    $('#user_subcourse').html(result);
                }
            });
        });
   if ($("i").hasClass("fa-heart-o")){
    // Do something if class exists
       $(".heart.fa-heart-o").click(function() {
         $(this).removeClass('fa-heart-o');
         $(this).addClass('fa-heart');
         $(".fa-heart").append('<style>{pointer-events:none;}</style>');
       });
   } else {
       // Do something if class does not exist
      // $(".fa-heart").css({"pointer-events": "none !important"});
      $('.fa-heart').append('<style>{pointer-events:none !important;}</style>');
   }

});
</script>

<script type="text/javascript">

 var searchUniversityId = 0;
 var courseId = 0;
 var userId = 0;

function courseModal(university_id, user_id, university_name){

  $("#courseDataModal").modal('show');

  document.getElementById("university_name").value = university_name;

  searchUniversityId = university_id;
  userId = user_id;

  console.log(searchUniversityId);

}

var $input = $(".typeahead-course");
$input.typeahead({

source: function (query, process) {
   return $.get('/offer/course/textsuggestions/'+searchUniversityId, { query: query }, function (data) {
       return process(data.options);
   });
}

});

$input.change(function() {
  var current = $input.typeahead("getActive");
  if (current) {
    // Some item from your model is active!
    if (current.name == $input.val()) {
      courseId = current.id;
      // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
    } else {
      // This means it is only a partial match, you can either add a new item
      // or take the active if you don't want new items
    }
  } else {
    // Nothing is active so it is a new value (or maybe empty value)
  }
});

var modalFillFairData = document.getElementById("courseDataModal");
var spanFillFair = document.getElementsByClassName("closeFillFairData")[0];

spanFillFair.onclick = function() {
  //modalFillFairData.style.display = "none";
  $("#courseDataModal").modal('hide');
}

function applyUniversity() {

  var university_id = searchUniversityId;
  var course_id = courseId;
  var user_id = userId;

  console.log(course_id);

  event.preventDefault();

    jQuery.ajax({
         type: "GET",
         url: "/university/details/apply/?uid=" + university_id + "&course_id="+ course_id +"&user_id=" + user_id,
     success: function(response) {
        alert("You have applied successfully");
        $("#courseDataModal").modal('hide');
        //window.location.href = '/search/university';
         }
     });
}

</script>

<!-- Bootstrap Core JavaScript -->
<!--<script src="js/bootstrap.min.js"></script>-->
<!-- Plugin JavaScript -->
<!--  <script src="js/jquery.easing.min.js"></script>-->
<!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
<!--  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>-->
<!-- Custom Theme JavaScript -->
<!--<script src="js/main.js"></script>--> <!-- Resource jQuery -->
