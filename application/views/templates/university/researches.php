<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<script type="text/javascript" src="
   https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<style type="text/css">
   #example1 td a {
   color: #f6882c !important;
   }
   .styleCss{
   font-size: 14px;
   background-color: #f6882c;
   color: white;
   font-weight: 600;
   letter-spacing: 0.8px;
   }
   .formControl{
   color: #fff !important;
   background-color: #8c57c5 !important;
   /* border: 0px; */
   font-size: 12px;
   border: 0px !important;
   box-shadow: inset 0 0 0 rgba(0,0,0,.075) !important;
   margin-top: 15px;
   }
   #example1 td.details-control {
   cursor: pointer;
   }
   #example1 tr.shown td {
   background-color: #815dd5;
   color: #fff !important;
   border: none;
   border-right: 1px solid #815dd5;
   }
   #example1 tr.shown td:last-child {
   border-right: none;
   }
   #example1 tr.details-row .details-table tr:first-child td {
   color: #fff;
   background: #f6882c;
   border: none;
   }
   #example1 tr.details-row .details-table > td {
   padding: 0;
   }
   #example1 tr.details-row .details-table .fchild td:first-child {
   cursor: pointer;
   }
   #example1 tr.details-row .details-table .fchild td:first-child:hover {
   background: #fff;
   }
   #example1 .form-group.agdb-dt-lst {
   padding: 2px;
   height: 23px;
   margin-bottom: 0;
   }
   #example1 .form-group.agdb-dt-lst .form-control {
   height: 23px;
   padding: 2px;
   }
   #example1 .adb-dtb-gchild {
   padding-left: 2px;
   }
   #example1 .adb-dtb-gchild td {
   background: #f5fafc;
   padding-left: 15px;
   }
   #example1 .adb-dtb-gchild td:first-child {
   cursor: default;
   }
   #example1 .fchild ~ .adb-dtb-gchild {
   /* display: none;
   */
   }
   .dataTables_wrapper{
   overflow: auto;
   }
   .listTableView thead tr{
   background-color: #f6882c;
   color: white;
   }
   .panelprimaryHeading {
   color: #fff;
   background-color: #815dd5 !important;
   border-color: #815dd5 !important;
   }
   .panelBody{
   border-top: 3px solid #815dd5;
   }
   table.dataTable thead .sorting{
   text-align: center !important;
   vertical-align: middle !important;
   }
   table.dataTable thead tr {
   background-color: #815dd5 !important;
   color: white !important;
   }
   .btnOrange{
   background-color: #f6882c;
   border-color: #f6882c;
   }
</style>
<div class="container">
   <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
         <h4>
            University : Manage Researches
         </h4>
      </section>
      <!-- Main content -->
      <section class="content">
         <div class="panel panel-primary">
            <div class="panel-heading panelprimaryHeading">Researches </div>
            <div class="panel-body">
               <?php if($this->session->flashdata('flash_message')){ ?>
               <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                  <h4><i class="icon fa fa-check"></i> Success</h4>
                  <?php echo $this->session->flashdata('flash_message'); ?>
               </div>
               <?php } ?>
               
                  <form action="<?php echo base_url();?>university/university/multidelete" method="post">
                     <div class="box-header">
                        <a href="<?php echo base_url();?>university/research/form"><button type="button" style="width:12%; float:left; margin-right:5px;" class="btn btn-block btn-primary"><i class="fa fa-plus"></i> Add Research</button></a>
                        <button type="submit" class="btn btn-block btn-danger multidel" style="width:11%; display:none;"><i class="fa fa-minus"></i> Delete University</button>
                     </div>
                     <!-- /.box-header -->
                     <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                           <thead>
                              <tr>
                                 <th></th>
                                 <th>University</th>
                                 <th>College</th>
                                 <th>Department</th>
                                 <th>Topic</th>
                                 <th>Research URL</th>
                                 <th>Funding</th>
                                 <th>Option</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php $i=1;
                                 foreach($researches as $usr){ ?>
                              <tr>
                                 <td><input type="checkbox" class="multichk" name="chk[]" value="<?php echo $usr->id;?>"/></td>
                                 <td><?php echo $usr->university_name;?></td>
                                 <td><?php echo $usr->college_name;?></td>
                                 <td><?php echo $usr->department_name;?></td>
                                 <td><?php echo $usr->topic;?></td>
                                 <td><?php echo $usr->research_url;?></td>
                                 <td><?php echo $usr->funding;?></td>
                                 <td><a href="<?php echo base_url();?>university/research/form/<?php echo $usr->id;?>" title="Modify"><i class="fa fa-edit"></i></a> &nbsp; <a href="javascript:void();" title="Delete" class="del" id="<?php echo $usr->id;?>"><i class="fa fa-trash-o"></i></a></td>
                                 <?php } ?>
                              </tr>
                           </tbody>
                           <tfoot>
                              <tr>
                                 <th></th>
                                 <th>University</th>
                                 <th>College</th>
                                 <th>Department</th>
                                 <th>Topic</th>
                                 <th>Research URL</th>
                                 <th>Funding</th>
                                 <th>Option</th>
                              </tr>
                           </tfoot>
                        </table>
                     </div>
                     <!-- /.box-body -->
                  </form>
            </div>
          </div>
      </section>
      <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
</div>
<script type="text/javascript">
   $(document).ready(function() {
     $('#example1').DataTable();
   } );
</script>