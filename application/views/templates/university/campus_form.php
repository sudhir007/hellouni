<style type="text/css">
   .panelprimaryHeading {
    color: #fff;
    background-color: #815dd5 !important;
    border-color: #815dd5 !important;
   }

.panelBody{
    border-top: 3px solid #815dd5;
}

.form-control{
      height: 30px;
}
</style>
<div class="container">
   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
         <h3>
            University : Add Campus
         </h3>
      </section>
      <!-- Main content -->
      <section class="content">
        <div class="panel panel-primary">
            <div class="panel-heading panelprimaryHeading">Add Campus</div>
            <div class="panel-body">
                <?php if($this->session->flashdata('flash_message')){ ?>
                  <div class="alert alert-error alert-dismissible">
                     <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                     <h4><i class="icon fa fa-circle-o"></i> Error</h4>
                     <?php echo $this->session->flashdata('flash_message'); ?>
                  </div>
                  <?php } ?>
                  <?php if($this->uri->segment('4')){?>
                  <form class="form-horizontal" action="<?php echo base_url();?>university/campus/edit/<?php if(isset($id)) echo $id;?>" method="post" name="addadmin">
                     <?php }else{?>
                  <form class="form-horizontal" action="<?php echo base_url();?>university/campus/create" method="post" name="adduni">
                     <?php } ?>
                     <div class="box-body">
                        <div class="form-group">
                           <label for="department" class="col-sm-2 control-label">University *</label>
                           <div class="col-sm-10">
                              <select class="form-control" name="university" id="university">
                                 <option value="">Select University</option>
                                 <?php
                                    if(isset($university_list) && !empty($university_list)){
                                    foreach ($university_list as $ulkey => $ulvalue) {
                                    ?>
                                 <option value="<?php echo $ulvalue['id'] ; ?>" <?php if(isset($selected_university_id) && $selected_university_id == $ulvalue['id']){ echo "selected"; } ?>><?php echo $ulvalue['name'] ; ?></option>
                                 <?php } } ?>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="name" class="col-sm-2 control-label">Name *</label>
                           <div class="col-sm-10">
                              <input type="name" class="form-control" id="name" name="name" placeholder="Campus Name" value="<?php if(isset($name)) echo $name; ?>">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="state" class="col-sm-2 control-label">State</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" id="state" name="state" placeholder="State" value="<?php if(isset($state)) echo $state; ?>">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="city" class="col-sm-2 control-label">City</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" id="city" name="city" placeholder="City" value="<?php if(isset($city)) echo $city; ?>">
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="size" class="col-sm-2 control-label">Size</label>
                           <div class="col-sm-4">
                              <input type="text" class="form-control" id="size" name="size" placeholder="Size" value="<?php if(isset($size)) echo $size; ?>">
                           </div>
                           <div class="col-sm-4">
                              <select class="form-control" name="size_unit">
                                 <option value="">Please Select</option>
                                 <option value="HECTARE" <?php if(isset($size_unit) && $size_unit=="HECTARE"){ echo 'selected="selected"'; } ?>>Hectare</option>
                                 <option value="ACRE" <?php if(isset($size_unit) && $size_unit=="ACRE"){ echo 'selected="selected"'; } ?>>Acre</option>
                                 <option value="SQUARE_KILOMETER" <?php if(isset($size_unit) && $size_unit=="SQUARE_KILOMETER"){ echo 'selected="selected"'; } ?>>Square Kilometer</option>
                                 <option value="SQUARE_MILES" <?php if(isset($size_unit) && $size_unit=="SQUARE_MILES"){ echo 'selected="selected"'; } ?>>Square Miles</option>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="timezone" class="col-sm-2 control-label">Timezone</label>
                           <div class="col-sm-10">
                              <select class="form-control" name="timezone">
                                 <?php
                                    for($i = 12; $i > 0; $i-=.5)
                                    {
                                        ?>
                                 <option value="-<?=$i?>" <?php if(isset($timezone) && $timezone=="-".$i){ echo 'selected="selected"'; } ?>>GMT-<?=$i?></option>
                                 <?php
                                    }
                                    ?>
                                 <option value="0" <?php if(isset($timezone) && $timezone==0){ echo 'selected="selected"'; } ?>>GMT+0</option>
                                 <?php
                                    for($i = .5; $i <= 12; $i+=.5)
                                    {
                                        ?>
                                 <option value="+<?=$i?>" <?php if(isset($timezone) && $timezone=="+".$i){ echo 'selected="selected"'; } ?>>GMT+<?=$i?></option>
                                 <?php
                                    }
                                     ?>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="university" class="col-sm-2 control-label">Locale</label>
                           <div class="col-sm-10">
                              <select class="form-control" name="locale">
                                 <option value="">Please Select</option>
                                 <option value="large" <?php if(isset($locale) && $locale=='large'){ echo 'selected="selected"'; } ?>>City - Large</option>
                                 <option value="midsize" <?php if(isset($locale) && $locale=='midsize'){ echo 'selected="selected"'; } ?>>City - Midsize</option>
                                 <option value="small" <?php if(isset($locale) && $locale=='small'){ echo 'selected="selected"'; } ?>>City - Small</option>
                                 <option value="sub_midsize" <?php if(isset($locale) && $locale=='sub_midsize'){ echo 'selected="selected"'; } ?>>Suburb - Midsize</option>
                                 <option value="sub_large" <?php if(isset($locale) && $locale=='sub_large'){ echo 'selected="selected"'; } ?>>Suburb - Large</option>
                                 <option value="town remote" <?php if(isset($locale) && $locale=='town remote'){ echo 'selected="selected"'; } ?>>Town Remote</option>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="accomodation_type" class="col-sm-2 control-label">Accomodation Type</label>
                           <div class="col-sm-10">
                              <select class="form-control" name="accomodation_type">
                                 <option value="">Please Select</option>
                                 <option value="On Campus" <?php if(isset($accomodation_type) && $accomodation_type=='On Campus'){ echo 'selected="selected"'; } ?>>On Campus</option>
                                 <option value="Off Campus" <?php if(isset($accomodation_type) && $accomodation_type=='Off Campus'){ echo 'selected="selected"'; } ?>>Off Campus</option>
                                 <option value="Both" <?php if(isset($accomodation_type) && $accomodation_type=='Both'){ echo 'selected="selected"'; } ?>>Both</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <!-- /.box-body -->
                     <div class="box-footer">
                        <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                        <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                        <?php
                           if($this->uri->segment('4'))
                           {
                               ?>
                        <button type="submit" class="btn btn-info pull-right">Update</button>
                        <?php
                           }
                           else
                           {
                               ?>
                        <button type="submit" class="btn btn-info pull-right">Create</button>
                        <?php
                           }
                           ?>
                     </div>
                     <!-- /.box-footer -->
                  </form>
            </div>
        </div>
        
      </section>
      <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
</div>