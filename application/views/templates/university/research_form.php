<script type="text/javascript" src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>ckeditor/skins/office2013/editor.css">
<style>
   .panelprimaryHeading {
   color: #fff;
   background-color: #815dd5 !important;
   border-color: #815dd5 !important;
   }
   .panelBody{
   border-top: 3px solid #815dd5;
   }
   .form-control{
   height: 30px;
   }
</style>
<div class="container">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h4>
         University : Add Research
      </h4>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="panel panel-primary">
            <div class="panel-heading panelprimaryHeading">Add Department</div>
            <div class="panel-body">
               <!-- form start -->
               <?php if($this->uri->segment('4')){?>
               <form class="form-horizontal" action="<?php echo base_url();?>university/research/edit/<?php if(isset($id)) echo $id;?>" method="post" name="addadmin" autocomplete="off">
                  <?php }else{?>
               <form class="form-horizontal" action="<?php echo base_url();?>university/research/create" method="post" name="adduni" autocomplete="off">
                  <?php } ?>
                  <div class="box-body">
                     <div class="form-group">
                        <label for="university" class="col-sm-2 control-label">University</label>
                        <div class="col-sm-10">
                           <?php
                              if(!$university_id)
                              {
                                   ?>
                           <select class="form-control" name="university">
                              <?php
                                 foreach($universities as $universityData)
                                 {
                                     ?>
                              <option value="<?=$universityData->id?>" <?php if(isset($university) && $universityData->id==$university){ echo 'selected="selected"'; } ?>><?=$universityData->name?></option>
                              <?php
                                 }
                                  ?>
                           </select>
                           <?php
                              }
                              else
                              {
                                   ?>
                           <select class="form-control" name="university" disabled>
                              <?php
                                 foreach($universities as $universityData)
                                 {
                                     ?>
                              <option value="<?=$universityData->id?>" <?php if($universityData->id==$university_id){ echo 'selected="selected"'; } ?>><?=$universityData->name?></option>
                              <?php
                                 }
                                  ?>
                           </select>
                           <?php
                              }
                               ?>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="college" class="col-sm-2 control-label">College</label>
                        <div class="col-sm-10">
                           <select class="form-control" name="college">
                              <?php
                                 foreach($colleges as $collegeData)
                                 {
                                     ?>
                              <option value="<?=$collegeData->id?>" <?php if(isset($college) && $collegeData->id==$college){ echo 'selected="selected"'; } ?>><?=$collegeData->name?></option>
                              <?php
                                 }
                                  ?>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="department" class="col-sm-2 control-label">Department</label>
                        <div class="col-sm-10">
                           <select class="form-control" name="department">
                              <?php
                                 foreach($departments as $departmentData)
                                 {
                                     ?>
                              <option value="<?=$departmentData->id?>" <?php if(isset($department) && $departmentData->id==$department){ echo 'selected="selected"'; } ?>><?=$departmentData->name?></option>
                              <?php
                                 }
                                  ?>
                           </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="topic" class="col-sm-2 control-label">Topic</label>
                        <div class="col-sm-10">
                           <input type="text" class="form-control" id="topic" name="topic" placeholder="Research topic" value="<?php if(isset($topic)) echo $topic; ?>">
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="description" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                           <textarea rows="4" class="form-control" id="description" name="description" placeholder="Breif about research"><?php if(isset($description)) echo $description; ?></textarea>
                           <script type="text/javascript">CKEDITOR.replace( 'description', {extraPlugins: 'confighelper'} );</script>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="research_url" class="col-sm-2 control-label">Research URL</label>
                        <div class="col-sm-10">
                           <input type="text" class="form-control" id="research_url" name="research_url" placeholder="Research URL" value="<?php if(isset($research_url)) echo $research_url; ?>">
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="faculty_url" class="col-sm-2 control-label">Faculty URL</label>
                        <div class="col-sm-10">
                           <input type="text" class="form-control" id="faculty_url" name="faculty_url" placeholder="Faculty URL" value="<?php if(isset($faculty_url)) echo $faculty_url; ?>">
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="funding" class="col-sm-2 control-label">Funding</label>
                        <div class="col-sm-10">
                           <input type="text" class="form-control" id="funding" name="funding" placeholder="Funding" value="<?php if(isset($funding)) echo $funding; ?>">
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                     <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
                     <button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                     <?php
                        if($this->uri->segment('4'))
                        {
                            ?>
                     <button type="submit" class="btn btn-info pull-right">Update</button>
                     <?php
                        }
                        else
                        {
                            ?>
                     <button type="submit" class="btn btn-info pull-right">Create</button>
                     <?php
                        }
                        ?>
                  </div>
                  <!-- /.box-footer -->
               </form>
            </div>
      </div>
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
</div>