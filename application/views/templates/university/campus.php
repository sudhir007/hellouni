<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<script type="text/javascript" src="
   https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<style type="text/css">
   #example1 td a {
   color: #f6882c !important;
   }
   .styleCss{
   font-size: 14px;
   background-color: #f6882c;
   color: white;
   font-weight: 600;
   letter-spacing: 0.8px;
   }
   .formControl{
   color: #fff !important;
   background-color: #8c57c5 !important;
   /* border: 0px; */
   font-size: 12px;
   border: 0px !important;
   box-shadow: inset 0 0 0 rgba(0,0,0,.075) !important;
   margin-top: 15px;
   }
   #example1 td.details-control {
   cursor: pointer;
   }
   #example1 tr.shown td {
   background-color: #815dd5;
   color: #fff !important;
   border: none;
   border-right: 1px solid #815dd5;
   }
   #example1 tr.shown td:last-child {
   border-right: none;
   }
   #example1 tr.details-row .details-table tr:first-child td {
   color: #fff;
   background: #f6882c;
   border: none;
   }
   #example1 tr.details-row .details-table > td {
   padding: 0;
   }
   #example1 tr.details-row .details-table .fchild td:first-child {
   cursor: pointer;
   }
   #example1 tr.details-row .details-table .fchild td:first-child:hover {
   background: #fff;
   }
   #example1 .form-group.agdb-dt-lst {
   padding: 2px;
   height: 23px;
   margin-bottom: 0;
   }
   #example1 .form-group.agdb-dt-lst .form-control {
   height: 23px;
   padding: 2px;
   }
   #example1 .adb-dtb-gchild {
   padding-left: 2px;
   }
   #example1 .adb-dtb-gchild td {
   background: #f5fafc;
   padding-left: 15px;
   }
   #example1 .adb-dtb-gchild td:first-child {
   cursor: default;
   }
   #example1 .fchild ~ .adb-dtb-gchild {
   /* display: none;
   */
   }
   .dataTables_wrapper{
   overflow: auto;
   }
   .listTableView thead tr{
   background-color: #f6882c;
   color: white;
   }

   .panelprimaryHeading {
    color: #fff;
    background-color: #815dd5 !important;
    border-color: #815dd5 !important;
   }

.panelBody{
    border-top: 3px solid #815dd5;
}

table.dataTable thead .sorting{
      text-align: center !important;
    vertical-align: middle !important;
}

table.dataTable thead tr {
      background-color: #815dd5 !important;
    color: white !important;
}

.btnOrange{
       background-color: #f6882c;
    border-color: #f6882c;
}
</style>
<div class="container">
   <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
         <h3>
            University : Manage Campuses
         </h3>
      </section>
      <!-- Main content -->
      <section class="content">
         <div class="row">
            <div class="col-xs-12">
               <!-- /.box -->
               <?php if($this->session->flashdata('flash_message')){ ?>
               <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                  <h4><i class="icon fa fa-check"></i> Success</h4>
                  <?php echo $this->session->flashdata('flash_message'); ?>
               </div>
               <?php } ?>
               <div class="panel panel-primary">
                  <div class="panel-heading panelprimaryHeading">Filter</div>
                  <div class="panel-body">
                      <div class="col-md-12">
                        <form name="filter" id="filter" action="<?php echo base_url();?>university/campus/filter" method="POST">
                           <div class="col-md-4">
                              <select class="form-control" name="university_id" id="university_id" onchange="this.form.submit()">
                                 <option value="">Select University</option>
                                 <?php
                                    if(isset($university_list) && !empty($university_list)){
                                    foreach ($university_list as $ulkey => $ulvalue) {
                                    ?>
                                 <option value="<?php echo $ulvalue['id'] ; ?>" <?php if(isset($selected_university_id) && $selected_university_id == $ulvalue['id']){ echo "selected"; } ?>><?php echo $ulvalue['name'] ; ?></option>
                                 <?php } } ?>
                              </select>
                           </div>
                           <div class="col-md-4">
                              <select class="form-control" name="campus_id" id="campus_id" onchange="this.form.submit()">
                                 <option value="">Select Campus</option>
                                 <?php
                                    if(isset($campus_list) && !empty($campus_list)){
                                    foreach ($campus_list as $clkey => $clvalue) {
                                    ?>
                                 <option value="<?php echo $clvalue['id'] ; ?>" <?php if(isset($selected_campus_id) && $selected_campus_id == $clvalue['id']){ echo "selected"; } ?>><?php echo $clvalue['name'] ; ?></option>
                                 <?php } } ?>
                              </select>
                           </div>
                        </form>
                      </div>
                  </div>
               </div>
               <div class="panel panel-primary panelBody">
                  <div class="panel-body ">
                    <form action="<?php echo base_url();?>university/university/multidelete" method="post">
                     <div class="col-md-3" style="margin-bottom: 5px;padding-left: 0px;">
                        <a href="<?php echo base_url();?>university/campus/form"><button type="button" class="btn btn-block btn-primary btnOrange"><i class="fa fa-plus"></i> Add Campus</button></a>
                        <button type="submit" class="btn btn-block btn-danger multidel" style="width:11%; display:none;"><i class="fa fa-minus"></i> Delete University</button>
                     </div>
                     <!-- /.box-header -->
                     <div class="box-body listTableView">
                        <table id="example1" class="stripe row-border order-column cl-table dataTable no-footer" cellspacing="0" width="100%">
                           <thead>
                              <tr>
                                 <th></th>
                                 <th>Name</th>
                                 <th>University Name</th>
                                 <th>Option</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php $i=1;
                                 foreach($campuses as $usr){ ?>
                              <tr>
                                 <td><input type="checkbox" class="multichk" name="chk[]" value="<?php echo $usr->id;?>"/></td>
                                 <td><?php echo $usr->name;?></td>
                                 <td><?php echo $usr->university_name;?></td>
                                 <td><a href="<?php echo base_url();?>university/campus/form/<?php echo $usr->id;?>" title="Modify"><i class="fa fa-edit"></i></a> &nbsp; <a href="javascript:void();" title="Delete" class="del" id="<?php echo $usr->id;?>"><i class="fa fa-trash-o"></i></a></td>
                              </tr>
                              <?php } ?>
                           </tbody>
                           <tfoot>
                              <tr>
                                 <th></th>
                                 <th>Name</th>
                                 <th>University Name</th>
                                 <th>Option</th>
                              </tr>
                           </tfoot>
                        </table>
                     </div>
                     <!-- /.box-body -->
                  </form>
                  </div>
                </div>
              
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </section>
      <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
</div>
<script type="text/javascript">
   $('#data-table').dataTable();
          $(".dataTable").on("draw.dt", function (e) {
            //console.log("drawing");
            setCustomPagingSigns.call($(this));
          }).each(function () {
            setCustomPagingSigns.call($(this)); // initialize
          });
          function setCustomPagingSigns() {
            var wrapper = this.parent();
            wrapper.find("a.previous").text("<");
            wrapper.find("a.next").text(">");
          }

          $(document).ready(function() {
             $('#example1').DataTable();
         } );
</script>