<html>
    <head>
        <title> Hellouni : Virtual Fair 2021 </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css">
        <link href="<?=base_url()?>css/newFairDesign.css?v=1.1" rel="stylesheet">
         <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://static.opentok.com/v2/js/opentok.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
         <script>
        $( function() {
          var availableTags = [
            "ActionScript",
            "AppleScript",
            "Asp",
            "BASIC",
            "C",
            "C++",
            "Clojure",
            "COBOL",
            "ColdFusion",
            "Erlang",
            "Fortran",
            "Groovy",
            "Haskell",
            "Java",
            "JavaScript",
            "Lisp",
            "Perl",
            "PHP",
            "Python",
            "Ruby",
            "Scala",
            "Scheme"
          ];
          $( "#tags" ).autocomplete({
            source: availableTags
          });
        } );
        </script>
        <style type="text/css">
          body {
                width: 100%;
                height: 100%;
                background-color: #26002A;
                
            }

           ul.tabs{
              margin: 0px;
              padding: 0px;
              list-style: none;
            }
            ul.tabs li{
              background: white;
              color: #222;
              display: inline-block;
              padding: 10px 15px;
              cursor: pointer;
            }

            ul.tabs li.current{
              background: #57005E;
              color: #fff;
            }

            .tab-content{
              display: none;
              /*background: #ededed;*/
              padding: 15px;
            }

            .tab-content.current{
              display: inherit;
            }
            form.example input[type=text] {
                  padding: 6.5px;
                  font-size: 17px;
                  border: 1px solid grey;
                  float: left;
                  width: 80%;
                  background: #f1f1f1;
                }

                form.example button {
                  float: left;
                  width: 20%;
                  padding: 10px;
                  background: #f6872c;
                  color: white;
                  font-size: 17px;
                  border: 1px solid grey;
                  border-left: none;
                  cursor: pointer;
                }

                form.example button:hover {
                  background: #f6872c85;
                }

                form.example::after {
                  content: "";
                  clear: both;
                  display: table;
                }


        </style>

    </head>
    <body>
    <header>
        <nav class="navigation">

            <!-- Logo -->
            <div class="logo">

                <img class="img-responsive" src="/images/Fair2021/ImperialLogo-01.jpg" onclick="imageClick('https://bit.ly/2RSGRFm')" width="130" height="60" />
            </div>
            
            <!-- Navigation -->
            <ul class="menu-list">
                <li onclick="myFunction1()" style="background-color: #00000000;border: 1px solid #00000000;"><a href="<?php echo base_url();?>meeting/virtualfair/eduFairAugHomePage" >Reception</a></li>
                <li onclick="myFunction2()" style="background-color: #f6872c;border: 1px solid #f6872c;"><a href="<?php echo base_url();?>meeting/virtualfair/eduFairAugUniList" >Universities</a></li>
            </ul>

            <div class="logo rmargin">
                
                <img class="img-responsive" src="/images/Fair2021/HelloUniLogo-01.jpg" onclick="imageClick('https://bit.ly/2RSGRFm')" width="130" height="60" />
            </div>

            <!-- <div class="humbarger">
                <div class="bar"></div>
                <div class="bar2 bar"></div>
                <div class="bar"></div>
            </div> -->
        </nav>
    
   
    </header>

     <div id="universities" class="container"  style="top: 10%;position: relative;">
        <div class="col-md-12" style="margin-bottom: 8px">
          
           <form class="example ui-widget" action="/action_page.php" style="margin: auto 0% auto 65%;;max-width:400px">
            <input id="tags" type="text" placeholder="Search.." name="search2">
            <button type="submit"><i class="fa fa-search"></i></button>
          </form>

        </div>
        <div class="container">
            <ul class="tabs">
              <li class="tab-link current" data-tab="tab-1">All</li>
              <li class="tab-link" data-tab="tab-2">USA / CANADA</li>
              <li class="tab-link" data-tab="tab-3">UK</li>
              <li class="tab-link" data-tab="tab-4">EUROPE</li>
              <li class="tab-link" data-tab="tab-5">AUS / NZ</li>
            </ul>
            <div id="tab-1" class="tab-content current">
              <div class="col-md-12 detail">
              <div class="col-12 col-sm-12 col-md-12 col-lg-3 p-0" style="    margin-top: 8px;padding-left: 0px;">
                <!-- <div class="ribbon"><span> Scholership </span></div> -->
                <img src="/images/Fair2021/11.jpg" alt="">
              </div>
              <div class="col-12 col-sm-12 col-md-12 col-lg-8 p-0">
                <span class="discount-holder"><span><i class="fas fa-user"></i> 2 Exibitors Available</span></span>
                <div class="detail-box">
                  <img src="/images/Fair2021/1.jpg" alt=""  />
                  <a href="<?php echo base_url();?>meeting/virtualfair/eduFairAugUniDetails" ><h4>SAN JOSE STATE UNIVERSITY</h4></a>
                  <p>
                    SAN JOSE STATE UNIVERSITY Small Description
                  </p>
                  <ul>
                    <li><strong>No. of Total Students : </strong>   22222</li>
                    <li><strong>Ranking  : </strong> World: 112 / National: 231</li>
                    <li><strong>Placement : </strong> 99%</li>
                  </ul>
                  <!-- <h6><strong><i class="fa fa-clock"></i> Enroll time</strong> 30 Jun 2019 <span>NPR: 300/- <a href="#" class="btn btn-primary" style="font-size: 12px;">Enroll Now</a></span></h6> -->
                </div>
              </div>
              
            </div>
            </div>
            <div id="tab-2" class="tab-content">
              <div class="col-md-12 detail">
              <div class="col-12 col-sm-12 col-md-12 col-lg-3 p-0" style="    margin-top: 8px;padding-left: 0px;">
                <!-- <div class="ribbon"><span> Scholership </span></div> -->
                <img src="/images/Fair2021/12.jpg" alt="">
              </div>
              <div class="col-12 col-sm-12 col-md-12 col-lg-8 p-0">
                <span class="discount-holder"><span><i class="fas fa-user"></i> 2 Exibitors Available</span></span>
                <div class="detail-box">
                  <img src="/images/Fair2021/2.jpg" alt=""  />
                  <h4>SAN JOSE STATE UNIVERSITY</h4>
                  <p>
                    SAN JOSE STATE UNIVERSITY Small Description
                  </p>
                  <ul>
                    <li><strong>No. of Total Students : </strong>   22222</li>
                    <li><strong>Ranking  : </strong> World: 112 / National: 231</li>
                    <li><strong>Placement : </strong> 99%</li>
                  </ul>
                  <!-- <h6><strong><i class="fa fa-clock"></i> Enroll time</strong> 30 Jun 2019 <span>NPR: 300/- <a href="#" class="btn btn-primary" style="font-size: 12px;">Enroll Now</a></span></h6> -->
                </div>
              </div>
              
            </div>
            <div class="col-md-12 detail">
              <div class="col-12 col-sm-12 col-md-12 col-lg-3 p-0" style="    margin-top: 8px;padding-left: 0px;">
                <!-- <div class="ribbon"><span> Scholership </span></div> -->
                <img src="/images/Fair2021/13.jpg" alt="">
              </div>
              <div class="col-12 col-sm-12 col-md-12 col-lg-8 p-0">
                <span class="discount-holder"><span><i class="fas fa-user"></i> 2 Exibitors Available</span></span>
                <div class="detail-box">
                  <img src="/images/Fair2021/3.jpg" alt=""  />
                  <h4>SAN JOSE STATE UNIVERSITY</h4>
                  <p>
                    SAN JOSE STATE UNIVERSITY Small Description
                  </p>
                  <ul>
                    <li><strong>No. of Total Students : </strong>   22222</li>
                    <li><strong>Ranking  : </strong> World: 112 / National: 231</li>
                    <li><strong>Placement : </strong> 99%</li>
                  </ul>
                  <!-- <h6><strong><i class="fa fa-clock"></i> Enroll time</strong> 30 Jun 2019 <span>NPR: 300/- <a href="#" class="btn btn-primary" style="font-size: 12px;">Enroll Now</a></span></h6> -->
                </div>
              </div>
              
            </div>
            </div>
            <div id="tab-3" class="tab-content">
               <div class="col-md-12 detail">
              <div class="col-12 col-sm-12 col-md-12 col-lg-3 p-0" style="    margin-top: 8px;padding-left: 0px;">
                <!-- <div class="ribbon"><span> Scholership </span></div> -->
                <img src="/images/Fair2021/15.jpg" alt="">
              </div>
              <div class="col-12 col-sm-12 col-md-12 col-lg-8 p-0">
                <span class="discount-holder"><span><i class="fas fa-user"></i> 2 Exibitors Available</span></span>
                <div class="detail-box">
                  <img src="/images/Fair2021/5.jpg" alt=""  />
                  <h4>SAN JOSE STATE UNIVERSITY</h4>
                  <p>
                    SAN JOSE STATE UNIVERSITY Small Description
                  </p>
                  <ul>
                    <li><strong>No. of Total Students : </strong>   22222</li>
                    <li><strong>Ranking  : </strong> World: 112 / National: 231</li>
                    <li><strong>Placement : </strong> 99%</li>
                  </ul>
                  <!-- <h6><strong><i class="fa fa-clock"></i> Enroll time</strong> 30 Jun 2019 <span>NPR: 300/- <a href="#" class="btn btn-primary" style="font-size: 12px;">Enroll Now</a></span></h6> -->
                </div>
              </div>
              
            </div>
            <div class="col-md-12 detail">
              <div class="col-12 col-sm-12 col-md-12 col-lg-3 p-0" style="    margin-top: 8px;padding-left: 0px;">
                <!-- <div class="ribbon"><span> Scholership </span></div> -->
                <img src="https://hellouni.org/uploads/sjs_university/banner.jpg" alt="">
              </div>
              <div class="col-12 col-sm-12 col-md-12 col-lg-8 p-0">
                <span class="discount-holder"><span><i class="fas fa-user"></i> 2 Exibitors Available</span></span>
                <div class="detail-box">
                  <img src="https://hellouni.org/uploads/sjs_university/logo.png?t=1627554723" alt=""  />
                  <h4>SAN JOSE STATE UNIVERSITY</h4>
                  <p>
                    SAN JOSE STATE UNIVERSITY Small Description
                  </p>
                  <ul>
                    <li><strong>No. of Total Students : </strong>   22222</li>
                    <li><strong>Ranking  : </strong> World: 112 / National: 231</li>
                    <li><strong>Placement : </strong> 99%</li>
                  </ul>
                  <!-- <h6><strong><i class="fa fa-clock"></i> Enroll time</strong> 30 Jun 2019 <span>NPR: 300/- <a href="#" class="btn btn-primary" style="font-size: 12px;">Enroll Now</a></span></h6> -->
                </div>
              </div>
              
            </div>
            <div class="col-md-12 detail">
              <div class="col-12 col-sm-12 col-md-12 col-lg-3 p-0" style="    margin-top: 8px;padding-left: 0px;">
                <!-- <div class="ribbon"><span> Scholership </span></div> -->
                <img src="https://hellouni.org/uploads/sjs_university/banner.jpg" alt="">
              </div>
              <div class="col-12 col-sm-12 col-md-12 col-lg-8 p-0">
                <span class="discount-holder"><span><i class="fas fa-user"></i> 2 Exibitors Available</span></span>
                <div class="detail-box">
                  <img src="https://hellouni.org/uploads/sjs_university/logo.png?t=1627554723" alt=""  />
                  <h4>SAN JOSE STATE UNIVERSITY</h4>
                  <p>
                    SAN JOSE STATE UNIVERSITY Small Description
                  </p>
                  <ul>
                    <li><strong>No. of Total Students : </strong>   22222</li>
                    <li><strong>Ranking  : </strong> World: 112 / National: 231</li>
                    <li><strong>Placement : </strong> 99%</li>
                  </ul>
                  <!-- <h6><strong><i class="fa fa-clock"></i> Enroll time</strong> 30 Jun 2019 <span>NPR: 300/- <a href="#" class="btn btn-primary" style="font-size: 12px;">Enroll Now</a></span></h6> -->
                </div>
              </div>
              
            </div>
            </div>
            <div id="tab-4" class="tab-content">
              giiiiiiiiiiiiifgxdg
            </div>
            <div id="tab-5" class="tab-content">
              giiiiiiiiiiiiifgxdg
            </div>
        </div>
    
    </div>
            
           
       
       
      
      

    

        
        

        <script type="text/javascript">
          $(document).ready(function(){
  
                $('ul.tabs li').click(function(){
                  var tab_id = $(this).attr('data-tab');

                  $('ul.tabs li').removeClass('current');
                  $('.tab-content').removeClass('current');

                  $(this).addClass('current');
                  $("#"+tab_id).addClass('current');
                })

              })
        </script>
      
        <script type="text/javascript">

           
            // $(document).ready(function () {
            //     $(".slide-toggle").click(function(){
            //         $(".box").animate({
            //             width: "toggle"
            //         });
            //     });

            //     $('.btn-filter').on('click', function () {
            //         var $target = $(this).data('target');
            //         if ($target != 'all') {
            //             $('.table tr').css('display', 'none');
            //             $('.table tr[data-status="' + $target + '"]').fadeIn('slow');
            //         } else {
            //             $('.table tr').css('display', 'none').fadeIn('slow');
            //         }
            //     });
            // });

            
         
            var video = document.getElementById("mp4");
            var videoo = document.getElementById("mpp4");
            var spinner = document.getElementById("spinner");
            var spinnerr = document.getElementById("spinnerr");
            var delayMillis = 4000;
            var spinnerIsHere = 1;
            var delayMilliss = 4000;
            var spinnerIsHeree = 1;
             video.volume = 0;
             videoo.volume = 0;

            var playVid = setTimeout(function() {
              if(spinnerIsHere == 1) {
                //alert("inside2");
                // Delete element DOM
                // spinner.parentNode.removeChild(spinner);
                spinner.style.visibility = "hidden";
                spinnerIsHere = 0;
              }
              video.play();
            }, delayMillis);

           

            video.addEventListener("click", function( event ) {
              if(video.paused) {
                if(spinnerIsHere == 1) {
                 //alert("inside2");
                  // Delete element DOM
                  // spinner.parentNode.removeChild(spinner);
                  spinner.style.visibility = "hidden";
                  spinnerIsHere = 0;
                }
                clearTimeout(playVid);
                video.play();
              } else {
                video.pause();
                if(spinnerIsHere == 0) {
                  spinner.style.visibility = "visible";
                  spinnerIsHere = 1;
                }
              }
            }, false);

             var playVidd = setTimeout(function() {
              if(spinnerIsHeree == 1) {
                // Delete element DOM
                // spinner.parentNode.removeChild(spinner);
                spinnerr.style.visibility = "hidden";
                spinnerIsHeree = 0;
              }
              videoo.play();
            }, delayMilliss);

             videoo.addEventListener("click", function( event ) {
              if(videoo.paused) {
                if(spinnerIsHeree == 1) {
                  // Delete element DOM
                  // spinner.parentNode.removeChild(spinner);
                  spinnerr.style.visibility = "hidden";
                  spinnerIsHeree = 0;
                }
                clearTimeout(playVidd);
                videoo.play();
              } else {
                videoo.pause();
                if(spinnerIsHeree == 0) {
                  spinnerr.style.visibility = "visible";
                  spinnerIsHeree = 1;
                }
              }
            }, false);

             function imageClick(url) {
                  // window.location = url;
                  window.open( url , '_blank');
              }

              

        </script>

    </body>
</html>
