<html>
    <head>
        <title> Hellouni : Virtual Fair 2021 </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css">
        <link href="<?=base_url()?>css/newFairDesign.css?v=1.1" rel="stylesheet">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://static.opentok.com/v2/js/opentok.min.js"></script>
        <style type="text/css">
            body {
                width: 100%;
                height: 100%;
                margin: 0; 
                background-color: #7e449a;
                background-image: url(/images/Fair2021/homePage.jpg);
                background-position: center bottom;
                background-repeat: no-repeat;
                background-size: contain;
            }
            .alert {
              padding: 10px;
              background-color: #f6882c;
              color: white;
                z-index: 10;
                bottom: 0;
                position: fixed;
                left: 0;
                text-align: center;
            }

            .closebtn {
              margin-left: 15px;
              color: white;
              font-weight: bold;
              float: right;
              font-size: 22px;
              line-height: 20px;
              cursor: pointer;
              transition: 0.3s;
            }

            .closebtn:hover {
              color: black;
            }

            .btnpurp {
                background-color: #6d326f !important;
            }
        </style>

    </head>
    <body>
    
    <header>
        <nav class="navigation">

            <!-- Logo -->
            <div class="logo">
                
                <img class="img-responsive" src="/images/Fair2021/ImperialLogo-01.jpg" onclick="imageClick('https://bit.ly/2RSGRFm')" width="130" height="60" />
            </div>
            
            <!-- Navigation -->
            <ul class="menu-list">
                <li onclick="myFunction1()" style="background-color: #f6872c;border: 1px solid #f6872c;"><a href="<?php echo base_url();?>meeting/virtualfair/eduFairAugHomePage" >Reception</a></li>
                <li onclick="myFunction2()" style="background-color: #00000000;border: 1px solid #00000000;"><a href="<?php echo base_url();?>meeting/virtualfair/eduFairAugUniList" >Universities</a></li>
            </ul>

            <div class="logo rmargin">
                
                <img class="img-responsive" src="/images/Fair2021/HelloUniLogo-01.jpg" onclick="imageClick('https://bit.ly/2RSGRFm')" width="130" height="60" />
            </div>

            <!-- <div class="humbarger">
                <div class="bar"></div>
                <div class="bar2 bar"></div>
                <div class="bar"></div>
            </div> -->
        </nav>
    
   
    </header>
    <div class="alert">
      <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
      <h5><strong>Join Now!</strong> Small text Description</h5>
      <button class="btn btn-md btnorange btnpurp">Join Now</button>
    </div>
    <div id="Reception" class="container-fluid"  >
        <!-- <div class="bg"></div> -->
        <div class="row" >
            <div class="col-md-12 bgImage"></div>
            
            <div class="bg-text1" id="bgImageText">
                <!-- <h2>Detail ONE</h2> -->
                <h1 class="topMar headH1">Imperial's MEGA VIrtual EduFair 2021!</h1>
                <h3 class="topMar">Meet TOP Universities from across the Globe directly<br>
                and gain unique insights on your prospects and options!</h3>
            </div>
            <div class="bg-text2">
                <!-- <h2>Detail ONE</h2> -->
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div id="videoHome">
                          <video id="mp4" controls loop controlsList="nodownload noremote foobar">
                              <source src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4" type="video/mp4">
                            </video>

                          <div id="spinner" class="poster">
                            <div class="cube"></div>
                            <div class="cube c2"></div>
                            <div class="cube c4"></div>
                            <div class="cube c3"></div>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div id="videoHome">
                          <video id="mpp4" controls loop controlsList="nodownload noremote foobar">
                              <source src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4" type="video/mp4">
                            </video>

                          <div id="spinnerr" class="poster">
                            <div class="cube"></div>
                            <div class="cube c2"></div>
                            <div class="cube c4"></div>
                            <div class="cube c3"></div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-text3">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <img src="/images/Fair2021/Button1.png" onclick="imageClick('https://bit.ly/2RSGRFm')" width="275" height="220" />
                    </div>
                    <div class="col-md-3">
                        <img src="/images/Fair2021/Button2.png" onclick="imageClick('https://bit.ly/2RSGRFm')" width="275" height="220" />
                    </div>
                    <div class="col-md-3">
                        <img src="/images/Fair2021/Button3.png" onclick="imageClick('https://bit.ly/2RSGRFm')" width="275" height="220" />
                    </div>
                    <div class="col-md-3">
                        <img src="/images/Fair2021/Button4.png" onclick="imageClick('https://bit.ly/2RSGRFm')" width="275" height="220" />
                    </div>
                </div>
            </div>
        </div>
       
    </div>
        <script type="text/javascript">
         
            var video = document.getElementById("mp4");
            var videoo = document.getElementById("mpp4");
            var spinner = document.getElementById("spinner");
            var spinnerr = document.getElementById("spinnerr");
            var delayMillis = 4000;
            var spinnerIsHere = 1;
            var delayMilliss = 4000;
            var spinnerIsHeree = 1;
             video.volume = 0;
             videoo.volume = 0;

            var playVid = setTimeout(function() {
              if(spinnerIsHere == 1) {
                //alert("inside2");
                // Delete element DOM
                // spinner.parentNode.removeChild(spinner);
                spinner.style.visibility = "hidden";
                spinnerIsHere = 0;
              }
              video.play();
            }, delayMillis);

           

            video.addEventListener("click", function( event ) {
              if(video.paused) {
                if(spinnerIsHere == 1) {
                 //alert("inside2");
                  // Delete element DOM
                  // spinner.parentNode.removeChild(spinner);
                  spinner.style.visibility = "hidden";
                  spinnerIsHere = 0;
                }
                clearTimeout(playVid);
                video.play();
              } else {
                video.pause();
                if(spinnerIsHere == 0) {
                  spinner.style.visibility = "visible";
                  spinnerIsHere = 1;
                }
              }
            }, false);

             var playVidd = setTimeout(function() {
              if(spinnerIsHeree == 1) {
                // Delete element DOM
                // spinner.parentNode.removeChild(spinner);
                spinnerr.style.visibility = "hidden";
                spinnerIsHeree = 0;
              }
              videoo.play();
            }, delayMilliss);

             videoo.addEventListener("click", function( event ) {
              if(videoo.paused) {
                if(spinnerIsHeree == 1) {
                  // Delete element DOM
                  // spinner.parentNode.removeChild(spinner);
                  spinnerr.style.visibility = "hidden";
                  spinnerIsHeree = 0;
                }
                clearTimeout(playVidd);
                videoo.play();
              } else {
                videoo.pause();
                if(spinnerIsHeree == 0) {
                  spinnerr.style.visibility = "visible";
                  spinnerIsHeree = 1;
                }
              }
            }, false);

             function imageClick(url) {
                  // window.location = url;
                  window.open( url , '_blank');
              }

              
        </script>
    </body>
</html>
