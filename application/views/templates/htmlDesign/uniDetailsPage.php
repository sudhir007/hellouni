<html>
   <head>
      <title> Hellouni : Virtual Fair 2021 </title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link href="<?=base_url()?>css/newFairDesign.css?v=1.1" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="https://unpkg.com/@popperjs/core@2">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
      <script src="https://static.opentok.com/v2/js/opentok.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
      <!--  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script> -->
      <style type="text/css">
         body {
         width: 100%;
         height: 100%;
         background-color: #26002A;
         }
         .newCssClass {  width : 55px !important; }
         .popover{
          position: absolute;
          top: -154 !important;
          left: -150% !important;
          display: block;
          width: max-content;
          height: 150px;
          color: black;
         }
         .popover-content {
              padding: 9px 14px;
              transform: scale(-1, 1);
          }
          .popover-title {
              padding: 8px 14px;
              margin: 0;
              font-size: 14px;
              background-color: #f6882c;
              border-bottom: 1px solid #f6882c;
              border-radius: 5px 5px 0 0;
              transform: scale(-1, 1);
          }
          .blur-content {
            -webkit-filter: blur(4px);
            -moz-filter: blur(4px);
            -o-filter: blur(4px);
            -ms-filter: blur(4px);
            filter: blur(4px);
            /* width: 100px; */
            /* height: 100px; */
            /* background-color: #ccc;*/
            pointer-events: none;
          }
          .lockIcon{
            color: white;
            font-size: 50px;
            position: absolute;
            z-index: 10;
            top: 40%;
            left: 46%;
          }


          .zoom:hover {
            -ms-transform: scale(2.5); /* IE 9 */
            -webkit-transform: scale(2.5); /* Safari 3-8 */
            transform: scale(2.5); 
          }
      </style>
   </head>
   <body>
      <header>
         <nav class="navigation" style="justify-content: flex-start;">
            <!-- Logo -->
            <div class="logo">
               <img class="img-responsive" src="/images/Fair2021/ImperialLogo-01.jpg" onclick="imageClick('https://bit.ly/2RSGRFm')" width="130" height="60" />
            </div>
            <!-- Navigation -->
            <ul class="menu-list" style="border-left: 1px solid white;margin-left: 2%;padding-left: 8px;">
               <li><a href="<?php echo base_url();?>meeting/virtualfair/eduFairAugUniList" ><i class="fa fa-chevron-circle-left" style="    font-size: 20px;"></i></a>
               </li>
               <span style="color: white;margin-left: 5px;">SAN JOSE STATE UNIVERSITY</span></li>
            </ul>
            <div class="logo rmargin" style="right: 0;position: absolute;">
               <img class="img-responsive" src="/images/Fair2021/HelloUniLogo-01.jpg" onclick="imageClick('https://bit.ly/2RSGRFm')" width="130" height="60" />
            </div>
            <!-- <div class="humbarger">
               <div class="bar"></div>
               <div class="bar2 bar"></div>
               <div class="bar"></div>
               </div> -->
         </nav>
      </header>
      <div class="container" style="top: 10%;position: relative;" >
      <div class="col-md-12 col-lg-12 divview" >
        <div id="videoUniDetails">
           <video id="mp4" controls preload="none" poster="https://hellouni.org/fair2021/Binghamton_University/banner.jpg">
              <source src="https://hellouni.org/fair2021/Binghamton_University/video.mp4" type="video/mp4">
           </video>
           <div id="spinner" class="poster" style="display: none;">
              <div class="cube"></div>
              <div class="cube c2"></div>
              <div class="cube c4"></div>
              <div class="cube c3"></div>
           </div>
        </div>
        <div class="col-md-12  divview">
           <div class="col-12 col-sm-12 col-md-12 col-lg-2 p-0 nopadding0" style="    margin-top: -25px;text-align: center;">
              <img src="https://hellouni.org/fair2021/Binghamton_University/logo.jpg" alt="" style="width: 50%!important" />
           </div>
           <div class="col-12 col-sm-12 col-md-12 col-lg-6 p-0 nopadding0">
              <div class="detail-box">
                 <h4>SAN JOSE STATE UNIVERSITY</h4>
                 <p>
                    SAN JOSE STATE UNIVERSITY Small Description
                 </p>
              </div>
           </div>
           <div class="col-12 col-sm-12 col-md-12 col-lg-4 p-0 nopadding0">
              <div class="social-icons">
                 <ul class="nomargin">
                    <a href="https://hellouni.org/"><i class="fa fa-external-link-square fa-3x social-fb" id="social" title="Website"></i></a>
                    <a href="https://www.facebook.com/HelloUni-847774062258005/"><i class="fa fa-facebook-square fa-3x social-fb" id="social" title="facebook"></i></a>
                    <a href="https://twitter.com/HelloUni1"><i class="fa fa-twitter-square fa-3x social-tw" id="social" title="twitter"></i></a>
                    <a href="https://www.linkedin.com/company/hellouni/" ><i class="fa fa-linkedin-square fa-3x social-em" id="social" title="linkedin"></i></a>
                    <a href="#"><i class="fa fa-youtube-square fa-3x social-gp" id="social" title="youtube"></i></a>
                    <a href="#"><i class="fa fa-instagram fa-3x social-gp" id="social" title="instagram"></i></a>
                 </ul>
              </div>
           </div>
        </div>
        <div class="col-md-12 divview descBox">
           <h4>About SAN JOSE STATE UNIVERSITY ,</h4>
           <p>San José State University (SJSU) is a comprehensive public university located in San Jose, California, in the heart of Silicon Valley, one of the most forward-looking, dynamic and innovative places in the world. SJSU was founded in 1857 and is the oldest public university on the West Coast. Our student population is one of the most ethnically diverse in the nation and has the highest foreign student enrollment of all master’s institutions in the United States.</p>
        </div>
        <div class="col-md-12 divview features">
           <div class="col-lg-12 mt-10 mt-lg-0 d-flex ">
              <div class="headingline">
                 <h3>University Highlights</h3>
              </div>
              <div class="row align-self-center gy-4">
                <div class="col-md-6 myBox">
                   <div class="feature-box d-flex align-items-center">
                      <a href="https://www.binghamton.edu/home/research/" target="_blank"><h3>Research at Binghamton</h3></a>
                   </div>
                </div>
                <div class="col-md-6 ">
                   <div class="feature-box d-flex align-items-center">
                      <h3>Academic Excellence - https://www.binghamton.edu/grad-school/academic-programs/academic-excellence/index.html</h3>
                   </div>
                </div>
                <div class="col-md-6 ">
                   <div class="feature-box d-flex align-items-center">
                      <h3>Programs &amp; Research - https://www.binghamton.edu/grad-school/academic-programs/research.html</h3>
                   </div>
                </div>
                <div class="col-md-6 ">
                   <div class="feature-box d-flex align-items-center">
                      <h3>Student Life - https://www.binghamton.edu/grad-school/student-life/</h3>
                   </div>
                </div>
                <div class="col-md-6 ">
                   <div class="feature-box d-flex align-items-center">
                      <h3>Campus Tour - https://www.binghamton.edu/grad-school/visit/</h3>
                   </div>
                </div>
              </div>
           </div>
        </div>
        <script type="text/javascript">
          $(".myBox").click(function() {
            var url = $(this).find("a").attr("href"); 
            window.open(url, '_blank');
            return false;
          });
        </script>
        <div class="col-md-12 divview features">
           <div class="col-lg-12 mt-10 mt-lg-0 d-flex">
              <div class="headingline">
                 <h3>Meet The University</h3>
              </div>
              <div class="row align-self-center gy-4">
                 <div class="col-md-6 " >
                    <div class="feature-box d-flex align-items-center" style="    padding: 9px 20px;">
                       <h3>University Brochure : &nbsp;<button class="btn btn-md btnorange"><i class="fa fa-download"></i> Download</button></h3>
                    </div>
                 </div>
                 <div class="col-md-6 " >
                    <div class="feature-box d-flex align-items-center" style="    padding: 11px 20px;">
                       <h3>
                          <i class="fa fa-arrow-circle-down"></i> &nbsp;Talk To Us On Table : 
                          <div class="avatars popoverDiv">
                             <span class="avatar">
                             <a href="#" data-toggle="popover" ><img src="/images/Fair2021/deligates/1.jpg" width="10" height="10" class="avatarimg" />
                             </a></span>
                             <span class="avatar">
                             <img class="avatarimg"  src="/images/Fair2021/deligates/1.jpg" width="10" height="10"/>
                             </span>
                             <span class="avatar">
                             <img class="avatarimg"  src="/images/Fair2021/deligates/1.jpg" width="10" height="10"/>
                             </span>
                             <span class="avatar">
                             <a href="#" ><img class="avatarimg"  src="/images/Fair2021/deligates/1.jpg" width="10" height="10"/></a>
                             </span>
                             <!-- Variable amount more avatars -->
                          </div>
                          Available 

                       </h3>
                    </div>

                 </div>
              </div>
           </div>
        </div>
        <div class="col-md-12 divview features">
           <div class="col-lg-12 mt-10 mt-lg-0 d-flex">
              <div class="row align-self-center gy-4">
                 <div class="col-md-6 col-lg-6 newd">
                    <div class="tableBox">
                       <div class="col-md-12 c" style="padding-left: 0px;">
                          <div class="col-md-1 tableNo">
                             <h4>1</h4>
                          </div>
                          <div class="col-md-9 tableName" >
                             <h4>
                             Organizer is here
                          </div>
                          <div class="col-md-2 orgPic" >
                             <img src="/images/Fair2021/deligates/1.jpg" width="20" height="20" style="width: 45% !important;" class="zoom">
                          </div>
                          <!-- <div class="col-md-2 orgPic " data-toggle="popover" data-container="body" data-placement="top" type="button" data-html="true" id="one">
                             <img src="https://picsum.photos/70" width="20" height="20" style="width: 45% !important;">
                             </div> -->
                       </div>
                       <div class="maincircle">
                          <div class="inner">
                             <div class="cenText">
                                <h4 class="orgName">Dr.Dinesh Bhatia</h4>
                             </div>
                             <div class="circle1 circle" data-angle="0"><a href="#" id="Chairb1" class="seatlink"><img src="/images/Fair2021/Chair1.png" alt="" width="10" height="10" ></a></div>
                             <div class="circle2 circle" data-angle="45"><a href="#" id="Chairb2" class="seatlink"><img src="/images/Fair2021/Chair2.png" alt="" width="10" height="10"></a></div>
                             <div class="circle2 circle" data-angle="90"><a href="#" id="Chairb3" class="seatlink"><img src="/images/Fair2021/Chair3.png" alt="" width="10" height="10" ></a></div>
                             <div class="circle1 circle" data-angle="135"><a href="#" id="Chairb4" class="seatlink"><img src="/images/Fair2021/Chair4.png" alt="" width="10" height="10"></a></div>
                             <div class="circle2 circle" data-angle="180"><a href="#" id="Chairb5" class="seatlink"><img src="/images/Fair2021/Chair5.png" alt="" width="10" height="10"></a></div>
                             <div class="circle2 circle" data-angle="225"><a href="#" id="Chairb6" class="seatlink"><img src="/images/Fair2021/Chair6.png" alt="" width="10" height="10"></a></div>
                             <div class="circle2 circle" data-angle="270"><a href="#" id="Chairb7" class="seatlink"><img src="/images/Fair2021/Chair7.png" alt="" width="10" height="10"></a></div>
                             <div class="circle2 circle" data-angle="315"><a href="#" id="Chairb8" class="seatlink"><img src="/images/Fair2021/Chair8.png" alt="" width="10" height="10"></a></div>
                          </div>
                       </div>
                       <div class="col-md-12 boxBottom">
                          <button class="btn btn-md btnSeat">Take a seat</button>
                       </div>
                    </div>
                 </div>
                 <div class="col-md-6 col-lg-6 newd ">
                    <i class="fa fa-lock lockIcon"></i>
                    <div class="tableBox blur-content">
                       <div class="col-md-12" style="padding-left: 0px;">
                          <div class="col-md-1 tableNo">
                             <h4>2</h4>
                          </div>
                          <div class="col-md-9 tableName" >
                             <h4>
                             Organizer is here
                          </div>
                          <div class="col-md-2 orgPic">
                             <img src="/images/Fair2021/deligates/1.jpg" width="20" height="20" style="width: 45% !important;">
                          </div>
                       </div>
                       <div class="maincircle">
                          <div class="inner">
                             <div class="circle1 circle" data-angle="0"><img src="/images/Fair2021/Chair1.png" alt="" width="10" height="10"></div>
                             <div class="circle2 circle" data-angle="45"><img src="/images/Fair2021/Chair2.png" alt="" width="10" height="10"></div>
                             <div class="circle2 circle" data-angle="90"><img src="/images/Fair2021/Chair3.png" alt="" width="10" height="10" style="width: 55% !important;"></div>
                             <div class="circle1 circle" data-angle="135"><img src="/images/Fair2021/Chair4.png" alt="" width="10" height="10"></div>
                             <div class="circle2 circle" data-angle="180"><img src="/images/Fair2021/Chair5.png" alt="" width="10" height="10"></div>
                             <div class="circle2 circle" data-angle="225"><img src="/images/Fair2021/Chair6.png" alt="" width="10" height="10"></div>
                             <div class="circle2 circle" data-angle="270"><img src="/images/Fair2021/Chair7.png" alt="" width="10" height="10" style="width: 55% !important;"></div>
                             <div class="circle2 circle" data-angle="315"><img src="/images/Fair2021/Chair8.png" alt="" width="10" height="10"></div>
                          </div>
                       </div>
                       <div class="col-md-12 boxBottom">
                          <button class="btn btn-md btnSeat">Take a seat</button>
                       </div>
                    </div>
                 </div>
              </div>
              <div class="row align-self-center gy-4" style="margin-top: 16px">
                 <div class="col-md-6 col-lg-6 newd">
                    <i class="fa fa-lock lockIcon"></i>
                    <div class="tableBox blur-content">
                       <div class="col-md-12" style="padding-left: 0px;">
                          <div class="col-md-1 tableNo">
                             <h4>3</h4>
                          </div>
                          <div class="col-md-9 tableName" >
                             <h4>
                             Organizer is here
                          </div>
                          <div class="col-md-2 orgPic">
                             <img src="https://picsum.photos/70" width="20" height="20" style="width: 45% !important;">
                          </div>
                       </div>
                       <div class="maincircle">
                          <div class="inner">
                             <div class="circle1 circle" data-angle="0"><img src="/images/Fair2021/Chair1.png" alt="" width="10" height="10"></div>
                             <div class="circle2 circle" data-angle="45"><img src="/images/Fair2021/Chair2.png" alt="" width="10" height="10"></div>
                             <div class="circle2 circle" data-angle="90"><img src="/images/Fair2021/Chair3.png" alt="" width="10" height="10" style="width: 55% !important;"></div>
                             <div class="circle1 circle" data-angle="135"><img src="/images/Fair2021/Chair4.png" alt="" width="10" height="10"></div>
                             <div class="circle2 circle" data-angle="180"><img src="/images/Fair2021/Chair5.png" alt="" width="10" height="10"></div>
                             <div class="circle2 circle" data-angle="225"><img src="/images/Fair2021/Chair6.png" alt="" width="10" height="10"></div>
                             <div class="circle2 circle" data-angle="270"><img src="/images/Fair2021/Chair7.png" alt="" width="10" height="10" style="width: 55% !important;"></div>
                             <div class="circle2 circle" data-angle="315"><img src="/images/Fair2021/Chair8.png" alt="" width="10" height="10"></div>
                          </div>
                       </div>
                       <div class="col-md-12 boxBottom">
                          <button class="btn btn-md btnSeat">Take a seat</button>
                       </div>
                    </div>
                 </div>
                 <div class="col-md-6 col-lg-6 newd">
                    <div class="tableBox">
                       <div class="col-md-12" style="padding-left: 0px;">
                          <div class="col-md-1 tableNo">
                             <h4>4</h4>
                          </div>
                          <div class="col-md-9 tableName" >
                             <h4>
                             Organizer is here
                          </div>
                          <div class="col-md-2 orgPic">
                             <img src="https://picsum.photos/70" width="20" height="20" style="width: 45% !important;">
                          </div>
                       </div>
                       <div class="maincircle">
                          <div class="inner">
                             <div class="circle1 circle" data-angle="0"><img src="/images/Fair2021/Chair1.png" alt="" width="10" height="10"></div>
                             <div class="circle2 circle" data-angle="45"><img src="/images/Fair2021/Chair2.png" alt="" width="10" height="10"></div>
                             <div class="circle2 circle" data-angle="90"><img src="/images/Fair2021/Chair3.png" alt="" width="10" height="10" style="width: 55% !important;"></div>
                             <div class="circle1 circle" data-angle="135"><img src="/images/Fair2021/Chair4.png" alt="" width="10" height="10"></div>
                             <div class="circle2 circle" data-angle="180"><img src="/images/Fair2021/Chair5.png" alt="" width="10" height="10"></div>
                             <div class="circle2 circle" data-angle="225"><img src="/images/Fair2021/Chair6.png" alt="" width="10" height="10"></div>
                             <div class="circle2 circle" data-angle="270"><img src="/images/Fair2021/Chair7.png" alt="" width="10" height="10" style="width: 55% !important;"></div>
                             <div class="circle2 circle" data-angle="315"><img src="/images/Fair2021/Chair8.png" alt="" width="10" height="10"></div>
                          </div>
                       </div>
                       <div class="col-md-12 boxBottom">
                          <button class="btn btn-md btnSeat">Take a seat</button>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
      </div>
      </div>
      <script type="text/javascript">
        // data-placement="top" title="<?=$delegate['name']?>" data-content="<?=$delegate['designation']?>"
          
          $('.popoverDiv a').click(function (event) {
           event.preventDefault();
           // $('[data-toggle="popover"]').popover(); 

           $('[data-toggle="popover"]').popover({
                    placement : 'top',
                trigger : 'click',
                    html : true,
                    content : '<div class="col-md-12 nopadding0"><div class="col-md-6"><img src="/images/Fair2021/deligates/1.jpg" style="width: 80px !important;margin-top: 10px;"></div><div class="col-md-6"<div class="row"><h5 style="color: #f6882c;font-weight: 800;font-size: 16px;">Jhon Carter</h5><p>Graduate Admissions Recruiter</p></div></div></div>'
                });
         });
         // var video = document.getElementById("mp4");
         // //var videoo = document.getElementById("mpp4");
         // var spinner = document.getElementById("spinner");
         // // var spinnerr = document.getElementById("spinnerr");
         // var delayMillis = 4000;
         // var spinnerIsHere = 1;
         // // var delayMilliss = 4000;
         // // var spinnerIsHeree = 1;
         //  video.volume = 0;
         //  //videoo.volume = 0;
         
         // var playVid = setTimeout(function() {
         //   if(spinnerIsHere == 1) {
         //     //alert("inside2");
         //     // Delete element DOM
         //     // spinner.parentNode.removeChild(spinner);
         //     spinner.style.visibility = "hidden";
         //     spinnerIsHere = 0;
         //   }
         //   video.play();
         // }, delayMillis);
         
         
         
         // video.addEventListener("click", function( event ) {
         //   if(video.paused) {
         //     if(spinnerIsHere == 1) {
         //      //alert("inside2");
         //       // Delete element DOM
         //       // spinner.parentNode.removeChild(spinner);
         //       spinner.style.visibility = "hidden";
         //       spinnerIsHere = 0;
         //     }
         //     clearTimeout(playVid);
         //     video.play();
         //   } else {
         //     video.pause();
         //     if(spinnerIsHere == 0) {
         //       spinner.style.visibility = "visible";
         //       spinnerIsHere = 1;
         //     }
         //   }
         // }, false);
         
         
         
         
         
         
         
          jQuery(function($){
         
          !jQuery.easing && (jQuery.easing = {});
          !jQuery.easing.easeOutQuad && (jQuery.easing.easeOutQuad = function( p ) { return 1 - Math.pow( 1 - p, 2 ); });
          
          var circleController = {
            create: function( circle ){
              var obj = {
                angle: circle.data('angle'),
                element: circle,
                measure: $('<div />').css('width', 360 * 8 + parseFloat(circle.data('angle'))),
                update: circleController.update,
                reposition: circleController.reposition,
              };
              obj.reposition();
              return obj;
            },
            update: function( angle ){
              this.angle = angle;
              this.reposition();
            },
            reposition: function(){
              var radians = this.angle * Math.PI / 180, radius = 268   / 2;
              this.element.css({
                marginLeft: (Math.sin( radians ) * radius - 50) + 'px',
                marginTop: (Math.cos( radians ) * radius - 30) + 'px'
              });
            }
          };
          
          var spin = {
            circles: [],
            left: function(){
              var self = this;
              $.each(this.circles, function(i, circle){
                circle.measure.stop(true, false).animate(
                  { 'width': '-=45' },
                  {
                    easing: 'easeOutQuad',
                    duration: 1000,
                    step: function( now ){ circle.update( now ); }
                  }
                );
              });
            },
            right: function(){
              var self = this;
              $.each(this.circles, function(i, circle){
                circle.measure.stop(true, false).animate(
                  { 'width': '+=45' },
                  {
                    easing: 'easeOutQuad',
                    duration: 1000,
                    step: function( now ){ circle.update( now ); }
                  }
                );
              });
            },
            prep: function( circles ){
              for ( var i=0, circle; i<circles.length; i++ ) {
                this.circles.push(circleController.create($(circles[i])));
              }
            }
          };
          
          // $('#goL').click(function(){ spin.left() });
          // $('#goR').click(function(){ spin.right() });
          
          spin.prep($('.circle'));
          
         });
         
         
           // function imageClick() {
           //     // // window.location = url;
           //     // window.open( url , '_blank');
           //     var id = $(this).attr('id');
           //     alert("Image clicked"+ id);
           // }
         
           $('a.seatlink').click(function(e) { 
                e.preventDefault();
                var imgid = $(this).attr('id');
                // $container.cycle(id.replace('#', '')); 
                // return false; 
                alert("Image clicked"+ imgid);
                $(this).find('img').attr('src', '/images/Fair2021/' + imgid + '.png');
                // '/images/Fair2021/' + $id + '.png'
            });
           
              // $('.btnSeat').click(function() {
              //     alert("btn click");
              //     $('.maincircle img:hover').css({
              //         '-ms-transform': 'scale(1.2)',
              //         '-webkit-transform': 'scale(1.2)',
              //         'transform': 'scale(1.2)'
              //     });
              // });
         
              //               $(document).ready(function() {
              //    $('[data-toggle="popover"]').popover({
              //       placement: 'top',
              //       trigger: 'hover'
              //    });
              // });
         
            //   $(document).ready(function(){
            //     $('[data-toggle="popover"]').popover({
            //         placement : 'top',
            //     trigger : 'hover',
            //         html : true,
            //         content : '<div class="media"><img src="https://picsum.photos/70" width="20" height="20" class="mr-3" alt="Sample Image" style="width: 10% !important;"><div class="media-body"><h5 class="media-heading">Jhon Carter</h5><p>Excellent Bootstrap popover! I really love it.</p></div></div>'
            //     });
            // });
         
         
            // http://davidwalsh.name/javascript-debounce-function
         // var debounce = debounce || function (func, wait, immediate) {
         //   var timeout;
         //   return function() {
         //     var context = this, args = arguments;
         //     var later = function() {
         //       timeout = null;
         //       if (!immediate) func.apply(context, args);
         //     };
         //     var callNow = immediate && !timeout;
         //     clearTimeout(timeout);
         //     timeout = setTimeout(later, wait);
         //     if (callNow) func.apply(context, args);
         //   };
         // };
         
         // function hideAll() {
         // $("[data-toggle=popover]").each(function () {
         // var popover = $(this).data('bs.popover');
         // if (popover.tip().is(':visible')) popover.hide();
         // });
         // }
         
         // $("[data-toggle=popover]")
         // .popover({animation: false, container: document.body, delay: 50, html: true, trigger: 'manual', placement: 'auto top'})
         // .each(function () {
         // var popover = $(this).data('bs.popover');
         // var $tip = popover.tip();
         // var delay = popover.options.delay || 0;
         // var showDelay = delay.show || delay || 0;
         // var hideDelay = delay.hide || delay || 0;
         // var showTimeout = null
         // var hideTimeout = null;
         
         
         // $(this).add($tip).hover(function show() {
         // if (hideTimeout) {
         //    clearTimeout(hideTimeout);
         //    hideTimeout = null;
         // } else if (!$tip.is(':visible')) {
         //    hideAll();
         //    showTimeout = setTimeout(function () {
         //        popover.show();
         //        showTimeout = null;
         //    }, showDelay);
         // }
         // }, function hide() {
         // if (showTimeout) {
         //    clearTimeout(showTimeout);
         //    showTimeout = null;
         // } else if ($tip.is(':visible')) {
         //    hideTimeout = setTimeout(function () {
         //        popover.hide();
         //        hideTimeout = null;
         //    }, hideDelay);
         // }
         // });
         // });
         
         // $(window).on('scroll resize', debounce(hideAll, 100, true));
      </script>
   </body>
</html>