<style type="text/css">
    @import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");
.login-block{
    background: #004b7a;  /* fallback for old browsers */
background: -webkit-linear-gradient(to bottom, #fafafb, #d8d9da);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to bottom, #fafafb, #d8d9da); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
float:left;
width:100%;
padding : 50px 0;
}
.banner-sec{background:url(<?php echo base_url();?>application/images/register.png)  no-repeat left bottom; background-size:cover; min-height:600px; border-radius: 0 10px 10px 0; padding:0;background-position: center;    margin-top: 25px;}

.login-sec{padding: 50px 30px; position:relative;}
.login-sec .copy-text{position:absolute; width:80%; bottom:20px; font-size:13px; text-align:center;}
.login-sec h2{margin-bottom:30px; font-weight:800; font-size:30px; color: #004b7a;}
.login-sec h2:after{content:" "; width:100px; height:5px; background:#f9992f; display:block; margin-top:20px; border-radius:3px; margin-left:auto;margin-right:auto}
.btn-login{background: #f9992f; color:#fff; font-weight:600;}
.form-control{
  display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}
</style>

<section class="login-block" id="forgot-password-page">
    <div class="container" style="background:#fff; border-radius: 10px; box-shadow: 5px 5px 0px rgba(0,0,0,0.1);">
    <div class="row">
        <div class="col-md-6 login-sec">
            <h2 class="text-center">Registration For Visa Seminar</h2>
            <form class="login-form" class="form-horizontal" onsubmit="VisaSeminarResgistration()" method="post">

              <div class="col-md-12 form-group">
                <div class="col-md-12">

                  <label for="inputEmail3" class="text-uppercase"> Name : &nbsp;&nbsp; </label>
                  <input type="text" class="form-control" id="student_name" name="student_name" placeholder="Name ...." value="<?=$student_name?>" required>
                  <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?=$student_id?>" required>

                  </select>
                </div>
              </div>

                  <div class="col-md-12 form-group">
                    <div class="col-md-6">
                      <label for="inputEmail3" class="text-uppercase"> Desired Country : &nbsp;&nbsp; </label>
                       <select name="desired_country" id="desired_country" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>

                        <option value="0">Select Country</option>

                        <?php foreach($countries as $country){?>

                          <option value="<?php echo $country->name;?>" <?php if(isset($selected_country) && $selected_country==$country->name){echo 'selected="selected"';}?> ><?php echo strtoupper($country->name);?></option>

                        <?php } ?>

                        <!--<option value="India">INDIA</option>-->

                      </select>
                    </div>
                    <div class="col-md-6">
                        <label for="inputEmail3" class="text-uppercase"> Desired Mainstream &nbsp;&nbsp; </label>

                       <select name="desired_mainstream" id="desired_mainstream" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>

                         <option value="0">Select Mainstream</option>

                         <?php foreach($mainstreams as $course){?>

                         <option value="<?php echo $course->id;?>"><?php echo strtoupper($course->name);?></option>

                         <?php } ?>

                       </select>
                    </div>
                  </div>


                   <div class="col-md-12 form-group">
                    <div class="col-md-6">
                       <label for="inputEmail3" class="text-uppercase"> Desired Substream &nbsp;&nbsp; </label>

                        <select name="desired_subcourse" id="desired_subcourse" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>

                          <option value="0">Select Substream</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                       <label for="inputEmail3" class="text-uppercase"> Desired Level Of Course &nbsp;&nbsp; </label>

                             <select name="desired_levelofcourse" id="desired_levelofcourse" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>

                               <option value="0">Select Level Of Course</option>

                                             <?php foreach($degrees as $degree){ ?>

                             <option value="<?php echo $degree->id;?>" ><?php echo strtoupper($degree->name);?></option>

                             <?php } ?>

                             </select>
                    </div>


                    </div>

                    <hr style="width:100%;text-align:left;margin-left:0;color:black">

                    <div class="col-md-12 form-group">
                     <div class="col-md-12">
                        <label for="inputEmail3" class="text-uppercase"> Competitive Exam &nbsp;&nbsp; </label>

                         <select name="competitive_exam" id="competitive_exam" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" onchange="competativeCondition()" required>

                           <option value="">Select Exam</option>
                           <option value="GRE">GRE</option>
                           <option value="SAT">SAT</option>
                           <option value="GMAT">GMAT</option>
                           <option value="NOTGIVEN">NOT GIVEN</option>

                         </select>
                     </div>

                     </div>

                     <div class="col-md-12 form-group" id="ce_div_one">
                      <div class="col-md-6">
                         <label for="inputEmail3" class="text-uppercase" id="first_label"> QUANTS &nbsp;&nbsp; </label>
                         <input type="text" class="form-control" id="ce_one_score" name="ce_one_score" placeholder=" Score" >

                      </div>

                      <div class="col-md-6">
                         <label for="inputEmail3" class="text-uppercase" id="second_label"> Verbal &nbsp;&nbsp; </label>
                         <input type="text" class="form-control" id="ce_two_score" name="ce_two_score" placeholder=" Score" >

                      </div>

                      </div>

                      <div class="col-md-12 form-group" id="ce_div_two">
                       <div class="col-md-6" id="ce_div_two_sat">
                          <label for="inputEmail3" class="text-uppercase" id="third_label"> AWA &nbsp;&nbsp; </label>
                          <input type="text" class="form-control" id="ce_three_score" name="ce_three_score" placeholder=" Score" >

                       </div>

                       <div class="col-md-6">
                          <label for="inputEmail3" class="text-uppercase"> Total &nbsp;&nbsp; </label>
                          <input type="text" class="form-control" id="ce_total_score" name="ce_total_score" placeholder="Total Score" >

                       </div>

                       </div>

                       <hr style="width:100%;text-align:left;margin-left:0;color:black">

                       <div class="col-md-12 form-group">
                        <div class="col-md-12">
                           <label for="inputEmail3" class="text-uppercase"> Language Exam &nbsp;&nbsp; </label>

                            <select name="lang_exam" id="lang_exam" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" onchange="langExamCondition()" required >

                              <option value="">Select Exam</option>
                              <option value="TOEFL">TOEFL</option>
                              <option value="IELTS">IELTS</option>
                              <option value="NOTGIVEN">NOT GIVEN</option>

                            </select>
                        </div>



                        </div>

                        <div class="col-md-12 form-group" id="lang_one">

                          <div class="col-md-6">
                             <label for="inputEmail3" class="text-uppercase"> Reading Score &nbsp;&nbsp; </label>
                             <input type="text" class="form-control" id="reading_score" name="reading_score" placeholder="Reading Score" >

                          </div>

                         <div class="col-md-6">
                            <label for="inputEmail3" class="text-uppercase"> Writing Score &nbsp;&nbsp; </label>
                            <input type="text" class="form-control" id="writing_score" name="writing_score" placeholder="Writing Score" >

                         </div>



                         </div>

                         <div class="col-md-12 form-group" id="lang_two">

                           <div class="col-md-6">
                              <label for="inputEmail3" class="text-uppercase"> Speaking Score &nbsp;&nbsp; </label>
                              <input type="text" class="form-control" id="speaking_score" name="speaking_score" placeholder="Speaking Score" >

                           </div>

                          <div class="col-md-6">
                             <label for="inputEmail3" class="text-uppercase"> Listening Score &nbsp;&nbsp; </label>
                             <input type="text" class="form-control" id="listening_score" name="listening_score" placeholder="Listening Score" >

                          </div>

                          </div>

                          <div class="col-md-12 form-group" id="lang_three">

                            <div class="col-md-12" >
                               <label for="inputEmail3" class="text-uppercase"> Total Score &nbsp;&nbsp; </label>
                               <input type="text" class="form-control" id="lang_total_score" name="lang_total_score" placeholder="Total Score" >

                            </div>

                          </div>

                          <hr style="width:100%;text-align:left;margin-left:0;color:black">

                          <div class="col-md-12 form-group">
                           <div class="col-md-12">
                              <label for="inputEmail3" class="text-uppercase"> Undergraduate Score (% or GPA) &nbsp;&nbsp; </label>
                              <input type="text" class="form-control" id="undergraduate_score" name="undergraduate_score" placeholder="Undergraduate Score" required>

                           </div>

                           </div>

                           <div class="col-md-12 form-group">
                            <div class="col-md-6">
                               <label for="inputEmail3" class="text-uppercase"> 12th Score &nbsp;&nbsp; </label>
                               <input type="text" class="form-control" id="tweleth_score" name="tweleth_score" placeholder="12th Score" required>

                            </div>

                            <div class="col-md-6">
                               <label for="inputEmail3" class="text-uppercase"> 10th Score &nbsp;&nbsp; </label>
                               <input type="text" class="form-control" id="tenth_score" name="tenth_score" placeholder="10th Score" required>

                            </div>

                            </div>

                            <div class="col-md-12 form-group">
                             <div class="col-md-12">
                                <label for="inputEmail3" class="text-uppercase"> Undergraduate University &nbsp;&nbsp; </label>
                                <input type="text" class="form-control" id="undergraduate_university_name" name="undergraduate_university_name" placeholder="Undergraduate University" required>

                             </div>

                             </div>

                             <div class="col-md-12 form-group">
                              <div class="col-md-6">
                                 <label for="inputEmail3" class="text-uppercase"> Undergraduate College &nbsp;&nbsp; </label>
                                 <input type="text" class="form-control" id="undergraduate_college_name" name="undergraduate_college_name" placeholder="Undergraduate College" required>

                              </div>

                              <div class="col-md-6">
                                 <label for="inputEmail3" class="text-uppercase"> Undergraduate Major &nbsp;&nbsp; </label>
                                 <input type="text" class="form-control" id="undergraduate_major" name="undergraduate_major" placeholder="Undergraduate Major" required>

                              </div>

                              </div>

                              <hr style="width:100%;text-align:left;margin-left:0;color:black">

                              <div class="col-md-12 form-group">
                               <div class="col-md-12">
                                  <label for="inputEmail3" class="text-uppercase"> How Do You Plan to finance your education? &nbsp;&nbsp; </label>
                                  <select name="education_finance_type" id="education_finance_type" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required >

                                    <option value="0">Select </option>
                                    <option value="Family_Savings">Family Savings</option>
                                    <option value="Education_Loan">Education Loan</option>
                                    <option value="Education_Loan_And_Family_Savings">Education Loan + Family Savings</option>

                                  </select>

                               </div>



                               </div>

                               <hr style="width:100%;text-align:left;margin-left:0;color:black">

                              <div class="col-md-12 form-group" id="unique_selling_point_0">
                               <div class="col-md-5">
                                  <label for="inputEmail3" class="text-uppercase"> University Applied &nbsp;&nbsp; </label>
                                  <input type="text" class="form-control"  name="university_applied[]" placeholder="Applied University Name" required>

                               </div>

                               <div class="col-md-5">
                                  <label for="inputEmail3" class="text-uppercase"> Current Status &nbsp;&nbsp; </label>
                                  <select name="current_status[]"  class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required >

                                    <option value="">Select Status</option>
                                    <option value="Admit">Admit</option>
                                    <option value="Rejected">Rejected</option>
                                    <option value="Pending">Pending</option>
                                    <option value="Complete">Complete</option>
                                    <option value="ScholarshipIn">Scholarship In</option>

                                  </select>

                               </div>

                               <div class="col-md-2">

                                  <button type="button" class="btn btn-primary" id="verifyotp" style="margin-top: 25px;" onclick="addRow();">ADD</button>

                               </div>

                               </div>



              <div class="form-check" style="text-align: center;">
                <!-- <button type="submit" class="btn btn-login" ng-click="submitForm()">Submit</button> -->
                 <button type="button" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                  <input type="submit"  style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Register"></input>
              </div>


            </form>
        </div>
        <div class="col-md-6 banner-sec">
           <!--  <ul>
              <li>kjbkjbkjbkjbbbkjbkj kjbjkjnkj kjnkjnjk kjn</li>
                            <li>kjbkjbkjbkjbbbkjbkj kjbjkjnkj kjnkjnjk kjn</li>

              <li>kjbkjbkjbkjbbbkjbkj kjbjkjnkj kjnkjnjk kjn</li>

            </ul> -->
        </div>
    </div>
</div>
</section>


<script type="text/javascript">
          var totalUniqueSellingPoints = 0;
          totalUniqueSellingPoints--;
          function addRow()
          {
              var html = '<div id="unique_selling_point_' + (totalUniqueSellingPoints + 1) + '" class="col-md-12 form-group">'
                          + '<div class="col-md-5">'
                          + '<label for="inputEmail3" class="text-uppercase"> University Applied &nbsp;&nbsp; </label>'
                          + '<input type="text" class="form-control"  name="university_applied[]" placeholder="Applied University Name" required>'
                          + '</div>'
                          + '<div class="col-md-5">'
                          + '<label for="inputEmail3" class="text-uppercase"> Current Status &nbsp;&nbsp; </label>'
                          + '<select name="current_status[]"  class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required >'
                          + '<option value="">Select Status</option>'
                          + '<option value="Admit">Admit</option>'
                          + '<option value="Rejected">Rejected</option>'
                          + '<option value="Pending">Pending</option>'
                          + '<option value="Complete">Complete</option>'
                          + '<option value="ScholarshipIn">Scholarship In</option>'
                          + '</select>'
                          + '</div>'
                          + '<div class="col-md-2">'
                          + '<input type="button" class="btn btn-danger" name="add" value="Delete" style="margin-top: 25px;" onclick="deleteRow(' + (totalUniqueSellingPoints + 1) + ')">'
                          + '</div>'
                          + '</div>';

              $("#unique_selling_point_" + totalUniqueSellingPoints).after(html);
              totalUniqueSellingPoints++;
              console.log(totalUniqueSellingPoints);
          }

          function deleteRow(sellingPointId)
          {
              $("#unique_selling_point_" + sellingPointId).remove();
              totalUniqueSellingPoints--;
          }

          function langExamCondition(){

            var lexamvalue = $('#lang_exam').val();

            if(lexamvalue == 'NOTGIVEN'){

              $("#lang_one").hide();
              $("#lang_two").hide();
              $("#lang_three").hide();

            } else {

              $("#lang_one").show();
              $("#lang_two").show();
              $("#lang_three").show();

            }

          }

          function competativeCondition(){

            var cexamvalue = $('#competitive_exam').val();

            if(cexamvalue == 'GRE'){

              $("#ce_div_one").show();
              $("#ce_div_two").show();
              $("#ce_div_two_sat").show();

              document.getElementById('first_label').innerHTML = "Quants";
              document.getElementById('second_label').innerHTML = "Verbal";
              document.getElementById('third_label').innerHTML = "AWA";

            } else if(cexamvalue == 'SAT'){
              $("#ce_div_one").show();
              $("#ce_div_two").show();
              $("#ce_div_two_sat").hide();

              document.getElementById('first_label').innerHTML = "Reading";
              document.getElementById('second_label').innerHTML = "Math";
            } else if(cexamvalue == 'GMAT'){
              $("#ce_div_one").show();
              $("#ce_div_two").show();
              $("#ce_div_two_sat").show();

              document.getElementById('first_label').innerHTML = "Section 1";
              document.getElementById('second_label').innerHTML = "Section 2";
              document.getElementById('third_label').innerHTML = "Section 3";
            } else if(cexamvalue == 'NOTGIVEN'){
              $("#ce_div_one").hide();
              $("#ce_div_two").hide();
            }

          }
</script>

<script type="text/javascript">

 	$( document ).ready(function() {

 	$('#desired_mainstream').on('change', function() {

 			var dataString = 'id='+this.value;

 			$.ajax({

 			type: "POST",

 			url: "/search/university/getSubcoursesList",

 			data: dataString,

 			cache: false,

 			success: function(result){

 			 $('#desired_subcourse').html(result);


 			}

           });



 	   });

   })


   //registration

   function VisaSeminarResgistration(){

     var user_id = $('#user_id').val();
     var student_name = $('#student_name').val();
     var desired_country = $('#desired_country').val();
     var desired_mainstream = $('#desired_mainstream').val();
     var desired_subcourse = $('#desired_subcourse').val();
     var desired_levelofcourse = $('#desired_levelofcourse').val();

     var competitive_exam = $('#competitive_exam').val();
     var ce_one_score = $('#ce_one_score').val();
     var ce_two_score = $('#ce_two_score').val();
     var ce_three_score = $('#ce_three_score').val();
     var ce_total_score = $('#ce_total_score').val();

     var lang_exam = $('#lang_exam').val();
     var reading_score = $('#reading_score').val();
     var writing_score = $('#writing_score').val();
     var speaking_score = $('#speaking_score').val();
     var listening_score = $('#listening_score').val();
     var lang_total_score = $('#lang_total_score').val();

     var undergraduate_score = $('#undergraduate_score').val();
     var tweleth_score = $('#tweleth_score').val();
     var tenth_score = $('#tenth_score').val();
     var undergraduate_university_name = $('#undergraduate_university_name').val();
     var undergraduate_college_name = $('#undergraduate_college_name').val();
     var undergraduate_major = $('#undergraduate_major').val();

     var education_finance_type = $('#education_finance_type').val();

     var university_applied = $("input[name='university_applied[]']").map(function(){return $(this).val();}).get();
     var current_status = $("select[name='current_status[]'] option:selected").map(function(){return $(this).val();}).get();

     var dataString = 'user_id=' + user_id + '&student_name=' + student_name + '&desired_country=' + desired_country + '&desired_mainstream=' + desired_mainstream + '&desired_subcourse=' + desired_subcourse + '&desired_levelofcourse=' + desired_levelofcourse
                      + '&competitive_exam=' + competitive_exam + '&ce_one_score=' + ce_one_score + '&ce_two_score=' + ce_two_score + '&ce_three_score=' + ce_three_score + '&ce_total_score=' + ce_total_score
                      + '&lang_exam=' + lang_exam + '&reading_score=' + reading_score + '&writing_score=' + writing_score + '&speaking_score=' + speaking_score + '&listening_score=' + listening_score + '&lang_total_score=' + lang_total_score
                      + '&undergraduate_score=' + undergraduate_score + '&tweleth_score=' + tweleth_score + '&tenth_score=' + tenth_score + '&undergraduate_university_name=' + undergraduate_university_name + '&undergraduate_college_name=' + undergraduate_college_name + '&undergraduate_major=' + undergraduate_major
                      + '&education_finance_type=' + education_finance_type + '&university_applied=' + university_applied + '&current_status=' + current_status ;

     event.preventDefault();

     $.ajax({

     type: "POST",

     url: "/webinar/registration/visaSeminarRegistration",

     data: dataString,

     cache: false,

     success: function(result){

      if(result == "SUCCESS"){

        alert("Welcome To HelloUni..!!!");
        //window.location.href='/webinar/webinarmaster/webinarlist';

      } else if(result == 'With Email-ID Or Mobile Account Already Exists'){

        alert(result);
        //window.location.href='/user/account';

      } else {

        alert(result);

      }

     }

         });

   }

     </script>
