
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
<script  src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<style type="text/css">
   @import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");
   .login-block{
   background: #004b7a;  /* fallback for old browsers */
   background: -webkit-linear-gradient(to bottom, #fafafb, #d8d9da);  /* Chrome 10-25, Safari 5.1-6 */
   background: linear-gradient(to bottom, #fafafb, #d8d9da); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
   float:left;
   width:100%;
   padding : 50px 0;
   }
   /*.banner-sec{background:url(<?php echo base_url();?>application/images/eduFair.jpeg)  no-repeat left bottom; background-size:cover; min-height:600px; border-radius: 0; padding:0;background-position: center center;    margin-top: 11%;}*/
   .login-sec{padding: 50px 30px; position:relative;}
   .login-sec .copy-text{position:absolute; width:80%; bottom:20px; font-size:13px; text-align:center;}
   .login-sec h2{margin-bottom:30px; font-weight:800; font-size:30px; color: #004b7a;}
   .login-sec h2:after{content:" "; width:100px; height:5px; background:#f9992f; display:block; margin-top:20px; border-radius:3px; margin-left:auto;margin-right:auto}
   .btn-login{background: #f9992f; color:#fff; font-weight:600;}
   .form-control{
   display: block;
   width: 100%;
   height: 34px;
   padding: 6px 12px;
   font-size: 14px;
   line-height: 1.42857143;
   color: #555;
   background-color: #fff;
   background-image: none;
   border: 1px solid #ccc;
   border-radius: 4px;
   -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
   box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
   -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
   -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
   transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
   }
   .table>tbody>tr>td{
   text-align: center;
   padding: 10px;
   line-height: 1.42857143;
   vertical-align: middle;
   border-top: 1px solid #ddd;
   font-size: 14px;
   font-family: sans-serif;
   letter-spacing: 0.8px;
   font-weight: 600;
   width: 51%;
   }
   .bootstrap-select .bs-ok-default::after {
    width: 0.3em;
    height: 0.6em;
    border-width: 0 0.1em 0.1em 0;
    transform: rotate(45deg) translateY(0.5rem);
}

.btn.dropdown-toggle:focus {
    outline: none !important;
}

.bootstrap-select>.dropdown-toggle{
  background-color: white;
    border: 1px solid #ccc;
    border-radius: 4px;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    color: #555;
    font-family: inherit;
    text-transform: capitalize;
        height: 30px;
    font-size: 12px;
}
   /*.blink {
   animation: blinker 0.6s linear infinite;
   color: #1c87c9;
   font-size: 30px;
   font-weight: bold;
   font-family: sans-serif;
   }
   @keyframes blinker {
   50% { opacity: 0; }
   }*/
   /*.banner-secLogo{background:url(<?php echo base_url();?>application/images/logsUni.jpeg)  no-repeat left bottom; background-size:cover; min-height:600px; border-radius: 0 10px 10px 0; padding:0;background-position: center;    margin-top: 25px;}*/
</style>
<section class="login-block" id="forgot-password-page">
   <div class="container" style="background:#fff; border-radius: 10px; box-shadow: 5px 5px 0px rgba(0,0,0,0.1);">
      <div class="row banner-secLogo">
         <img src="<?php echo base_url();?>application/images/LoanMelaMarch.jpg"
            class="img-fluid"  alt="Responsive Image"
            width="100%" height="80%" />
      </div>
      <div class="row">
         <div class="col-md-12 login-sec">
            <h2 class="text-center">Register Now</h2>
            <form class="login-form" class="form-horizontal" onsubmit="eduFairResgistration()" method="post" autocomplete="off">
               <div class="col-md-12 form-group">
                  <div class="col-md-6">
                     <label for="inputEmail3" class="text-uppercase">First Name</label>
                     <input type="text" class="form-control" id="user_first_name" name="user_first_name" placeholder="First Name" required>
                     <input type="hidden" value="<?=$utm_source?>" name="utm_source" id="utm_source">
                  </div>
                  <div class="col-md-6">
                     <label for="inputEmail3" class="text-uppercase">Last Name</label>
                     <input type="text" class="form-control" id="user_last_name" name="user_last_name" placeholder="Last Name" required>
                  </div>
               </div>
               <div class="col-md-12 form-group">
                  <div class="col-md-6">
                     <label for="inputEmail3" class="text-uppercase">Email</label>
                     <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email Id" required>
                  </div>
                  <div class="col-md-6">
                     <label for="inputEmail3" class="text-uppercase">Mobile Number</label>
                     <input type="text" class="form-control" id="user_mobile" name="user_mobile" placeholder="Mobile Number"  required>
                  </div>
               </div>
               <!--div class="col-md-12 form-group">
                  <div class="col-md-6">
                     <label for="inputEmail3" class="text-uppercase">Mobile Number</label>
                     <input type="text" class="form-control" id="user_mobile" name="user_mobile" placeholder="Mobile Number"  required>
                  </div>
                  <div class="col-md-3">
                     <label for="inputEmail3" class="text-uppercase">OTP</label><br>
                     <input type="text" class="form-control" id="otp_number" name="otp_number" placeholder="OTP Sent On Mobile ">
                  </div>
                  <div class="col-md-2">
                     <button type="button" class="btn btn-primary" id="verifyotp" style="margin-top: 25px;">Verify</button>
                     <button type="button" class="btn btn-primary" id="send_otp" style="margin-top: 25px;" onclick="sendOTP()">Send OTP</button>
                  </div>
              </div-->

               <div class="col-md-12 form-group">

                   <div class="col-md-6">
                     <label for="inputEmail3" class="text-uppercase">City</label>
                     <input type="text" class="form-control" id="user_city" name="user_city" placeholder="City" required>
                  </div>
                   <div class="col-md-6">
                     <label for="inputEmail3" class="text-uppercase">Country of Study</label>
                     <select name="user_desired_country" id="user_desired_country" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>

                      <option value="0">Select Country</option>

                      <?php foreach($countries as $country){?>

                        <option value="<?php echo $country->name;?>" ><?php echo strtoupper($country->name);?></option>

                      <?php } ?>

                    </select>
                  </div>

                  </div>

              <div class="col-md-12 form-group">
                <div class="col-md-6">
                     <label for="inputEmail3" class="text-uppercase"> Intake Year </label>
                     <Select name="user_intake_year" id="user_intake_year" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                        <option value="">Select Year</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                     </Select>
                  </div>
                  <div class="col-md-6">
                     <label for="inputEmail3" class="text-uppercase"> Loan Type : </label>
                     <select name="user_loan_type" id="user_loan_type" multiple class="multibox selectpicker  form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                        <option>Collateral</option>
                        <option>Non-Collateral</option>
                        <option>Cosigner</option>

                    </select>
                  </div>
              </div>
               <script>
                  $(document).ready(function () {
                      $('.selectpicker').selectpicker();
                  })
              </script>
               <div class="form-check" style="text-align: center;">
                  <!-- <button type="submit" class="btn btn-login" ng-click="submitForm()">Submit</button> -->
                  <button type="button" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                  <input type="submit"  style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Register"></input>
               </div>
            </form>
         </div>
      </div>
   </div>
   <script type="text/javascript">
      $(document).ready(function(){
      $("#hide").click(function(){
        $("#secondBanner").hide();
        $("#show").show();
      });
      $("#show").click(function(){
        $("#secondBanner").show();
        $("#show").hide();
      });
      });

      function closeDiv(){
        $("#secondBanner").hide();
        $("#show").show();
      }
   </script>
</section>

<script type="text/javascript">
   $( document ).ready(function() {

   $('#desired_mainstream').on('change', function() {

       var dataString = 'id='+this.value;

       $.ajax({

       type: "POST",

       url: "/search/university/getSubcoursesList",

       data: dataString,

       cache: false,

       success: function(result){

        $('#desired_subcourse').html(result);


       }

            });



      });

          // verify otp_number
          $('#verifyotp').on('click', function() {
              var mobileNumber = $('#user_mobile').val();
              var otpNumber = $('#otp_number').val();
              var dataString = 'mobile_number=' + mobileNumber + '&otp=' + otpNumber;
              $.ajax({
                  type: "POST",
                  url: "/event/verify_otp",
                  data: dataString,
                  cache: false,
                  success: function(result){
                      result = JSON.parse(result);
                      alert(result['message']);
                  }
              });
          });


              // verify resgistrationold
              $('#resgistrationold').on('click', function() {

                   var user_name = $('#user_name').val();
                   var user_email = $('#user_email').val();
                   var user_mobile = $('#user_mobile').val();
                   var desired_country = $('#desired_country').val();
                   var desired_mainstream = $('#desired_mainstream').val();
                   var desired_subcourse = $('#desired_subcourse').val();
                   var desired_levelofcourse = $('#desired_levelofcourse').val();
                   var userPassword = $('#password').val();

                   var dataString = 'user_name=' + user_name + '&user_email=' + user_email + '&user_mobile=' + user_mobile + '&desired_country=' + desired_country + '&desired_mainstream=' + desired_mainstream + '&desired_subcourse=' + desired_subcourse + '&desired_levelofcourse=' + desired_levelofcourse + '&user_password=' + userPassword ;

                   $.ajax({

                   type: "POST",

                   url: "/webinar/registration/registrationsignup",

                   data: dataString,

                   cache: false,

                   success: function(result){


                    if(result == "SUCCESS"){
                      alert("Welcome To HelloUni..!!!");
                      window.location.href='/webinar/webinarmaster/webinarlist';
                    } else {
                      alert(result)
                    }

                   }

                       });

                  });

    })


    //registration

    function eduFairResgistration(){

      var user_first_name = $('#user_first_name').val();
      var user_last_name = $('#user_last_name').val();
      var user_email = $('#user_email').val();
      var user_mobile = $('#user_mobile').val();
      var user_city = $('#user_city').val();
      var user_desired_country = $('#user_desired_country').val();
      var user_loan_type = $('#user_loan_type').val();
      var user_intake_year = $('#user_intake_year').val();
      var otpNumber = $('#otp_number').val();


      var dataString = 'first_name=' + user_first_name + '&last_name=' + user_last_name + '&user_email=' + user_email
      + '&user_mobile=' + user_mobile + '&desired_country=' + user_desired_country
      + '&ccity=' + user_city + '&loan_type=' + user_loan_type + '&intake_year=' + user_intake_year + "&otp=001122";

      event.preventDefault();
      $.ajax({

      type: "POST",

      url: "/event/virtualfair_registration",

      data: dataString,

      cache: false,

      success: function(result){

        result = JSON.parse(result);
        if(result['success']){
            //window.location.href='/event/reception/' + result['encodedUsername'];

            alert("Congratulation!! You have successfully registered for EduLoans Loan Mela! \n You will receive an email shortly containing instructions to attend the event.");

        }
        else{
            alert(result['message']);
        }

      }

          });

    }

    // sent OTP
    function sendOTP() {
        var mobileNumber = $('#user_mobile').val();
        var mobilenumberlength = $('#user_mobile').val().length;
        var userEmail = $('#user_email').val();

        if(mobilenumberlength != 10) {
            alert("Please enter valid mobile!!!");
        }
        else if(!userEmail) {
            alert("Please enter email!!!");
        }
        else {
            var dataString = 'mobile_number=' + mobileNumber + '&user_email=' + userEmail;

            $.ajax({
                type: "POST",
                url: "/event/send_otp",
                data: dataString,
                cache: false,
                success: function(result){
                    result = JSON.parse(result);
                    alert(result['message']);
                    document.getElementById("send_otp").style.display = "none";
                    document.getElementById("verifyotp").style.display = "block";
                }
            });
        }
    }

       function checkOther(value){
           document.getElementById("other_college").style.display = "none";
           if(value == 'Other'){
               document.getElementById("other_college").style.display = "block";
           }
       }

       function checkComExam(value){
           document.getElementById("competativeexamscored").style.display = "block";
           if(value == 'NOTGIVEN'){
               document.getElementById("competativeexamscored").style.display = "none";
           }
       }

       function checkGpa(value){
           document.getElementById("gpaexamscored").style.display = "block";
           if(value == 'NOTGIVEN'){
               document.getElementById("gpaexamscored").style.display = "none";
           }
       }

       function checkWorkExp(value){
           document.getElementById("workexperiencemonthsd").style.display = "none";
           if(value == 'Yes'){
               document.getElementById("workexperiencemonthsd").style.display = "block";
           }
       }


</script>
