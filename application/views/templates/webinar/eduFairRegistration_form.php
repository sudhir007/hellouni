<style type="text/css">
    @import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");
.login-block{
    background: #004b7a;  /* fallback for old browsers */
background: -webkit-linear-gradient(to bottom, #fafafb, #d8d9da);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to bottom, #fafafb, #d8d9da); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
float:left;
width:100%;
padding : 50px 0;
}
/*.banner-sec{background:url(<?php echo base_url();?>application/images/eduFair.jpeg)  no-repeat left bottom; background-size:cover; min-height:600px; border-radius: 0; padding:0;background-position: center center;    margin-top: 11%;}*/

.login-sec{padding: 50px 30px; position:relative;}
.login-sec .copy-text{position:absolute; width:80%; bottom:20px; font-size:13px; text-align:center;}
.login-sec h2{margin-bottom:30px; font-weight:800; font-size:30px; color: #004b7a;}
.login-sec h2:after{content:" "; width:100px; height:5px; background:#f9992f; display:block; margin-top:20px; border-radius:3px; margin-left:auto;margin-right:auto}
.btn-login{background: #f9992f; color:#fff; font-weight:600;}
.form-control{
  display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}

.table>tbody>tr>td{
  text-align: center;
    padding: 10px;
    line-height: 1.42857143;
    vertical-align: middle;
    border-top: 1px solid #ddd;
    font-size: 14px;
    font-family: sans-serif;
    letter-spacing: 0.8px;
    font-weight: 600;
    width: 51%;
}
/*.blink {
      animation: blinker 0.6s linear infinite;
      color: #1c87c9;
      font-size: 30px;
      font-weight: bold;
      font-family: sans-serif;
      }
      @keyframes blinker {
      50% { opacity: 0; }
      }*/

/*.banner-secLogo{background:url(<?php echo base_url();?>application/images/logsUni.jpeg)  no-repeat left bottom; background-size:cover; min-height:600px; border-radius: 0 10px 10px 0; padding:0;background-position: center;    margin-top: 25px;}*/
</style>

<section class="login-block" id="forgot-password-page">
    <div class="container" style="background:#fff; border-radius: 10px; box-shadow: 5px 5px 0px rgba(0,0,0,0.1);">
    <div class="row banner-secLogo">
      <img src="<?php echo base_url();?>application/images/Webinar_Reg_Top_Banner.jpg"
             class="img-fluid"  alt="Responsive Image"
             width="100%" height="80%" />
    </div>
    <div class="row">
        <div class="col-md-6 login-sec">
            <h2 class="text-center">Register Now</h2>
            <form class="login-form" class="form-horizontal" onsubmit="resgistration()" method="post" autocomplete="off">
              <div class="col-md-12 form-group">
                <div class="col-md-12">
                  <label for="inputEmail3" class="text-uppercase">Full Name</label>

                    <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Full Name" required>
                    <input type="hidden" class="form-control" id="fair_id" name="fair_id" value="<?=$fair_id?>">
               </div>
              </div>

               <div class="col-md-12 form-group">
                 <div class="col-md-6">
                     <label for="inputEmail3" class="text-uppercase">Email</label>
                     <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email Id" required>
                   </div>

                   <div class="col-md-6">
                       <label for="inputEmail3" class="text-uppercase">Login ID / Username</label>
                       <input type="text" class="form-control" id="user_id" name="user_id" placeholder="Ex - rahul99" required>
                     </div>
                </div>

                <div class="col-md-12 form-group">
                    <div class="col-md-6">
                     <label for="inputEmail3" class="text-uppercase">Mobile Number</label>
                     <input type="text" class="form-control" id="user_mobile" name="user_mobile" onfocusout="sendOTP()" placeholder="Mobile Number"  required>
                    </div>
                    <div class="col-md-3">
                    <label for="inputEmail3" class="text-uppercase">OTP</label><br>
                      <input type="text" class="form-control" id="otp_number" name="otp_number" placeholder="OTP Sent On Mobile ">
                    </div>
                    <div class="col-md-2">
                       <button type="button" class="btn btn-primary" id="verifyotp" style="margin-top: 25px;">Verify</button>
                    </div>

                  </div>

                  <div class="col-md-12 form-group">
                      <div class="col-md-12">
                      <label for="inputPassword3" class="text-uppercase">Password</label>

                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required autocomplete="off">
                     </div>
                    </div>

                  <!--<div class="col-md-12 form-group">
                    <div class="col-md-6">
                      <label for="inputEmail3" class="text-uppercase"> Applying for which Level? &nbsp;&nbsp; </label>
                        <Select name="levelselector" id="levelselector" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                           <option value="0">Select Level of Applying</option>
                           <option value="Bachelors" selected>Bachelors</option>
                           <option value="Masters">Masters</option>
                        </Select>
                    </div>

                    <div class="col-md-6 levelClass" id="Bachelors" >
                      <label for="inputEmail3" class="text-uppercase"> Currently studying in ? &nbsp;&nbsp; </label>
                       <Select name="bachelorslevelselector" id="bachelorslevelselector" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                           <option value="0">Select Current Level</option>
                           <option value="9th">9th</option>
                           <option value="10th">10th</option>
                           <option value="11th">11th</option>
                           <option value="12th">12th</option>
                        </Select>
                    </div>

                    <div class="col-md-6 levelClass" id="Masters" style="display:none">
                      <label for="inputEmail3" class="text-uppercase"> Currently studying in ? &nbsp;&nbsp; </label>
                       <Select name="masterlevelselector" id="masterlevelselector" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                           <option value="0">Select Current Level</option>
                           <option value="1stYear">1st Year</option>
                           <option value="2ndYear">2nd Year</option>
                           <option value="3rdYear">3rd Year</option>
                           <option value="4thYear">4th Year</option>
                           <option value="Graduated">Graduated</option>
                        </Select>
                    </div>
                  </div>-->
                  <script type="text/javascript">
                    $(function() {
                        $('#levelselector').change(function(){
                            $('.levelClass').hide();
                            $('#' + $(this).val()).show();
                        });
                    });

                  </script>


                  <div class="col-md-12 form-group">
                    <div class="col-md-6">
                      <label for="inputEmail3" class="text-uppercase"> Intake Month </label>
                       <Select name="intakemonth" id="intakemonth" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                           <option value="">Select Month</option>
                           <option value="January"> January</option>
                           <option value="February"> February </option>
                           <option value="March"> March </option>
                           <option value="April">April </option>
                           <option value="May">May</option>
                           <option value="June">June</option>
                           <option value="July">July</option>
                           <option value="August">August</option>
                           <option value="September">September</option>
                           <option value="October">October</option>
                           <option value="November">November</option>
                           <option value="December">December</option>
                        </Select>
                    </div>
                    <div class="col-md-6">
                      <label for="inputEmail3" class="text-uppercase"> Intake Year </label>
                       <Select name="intakeyear" id="intakeyear" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                           <option value="">Select Year</option>
                           <option value="2020">2020</option>
                           <option value="2021">2021</option>
                           <option value="2022">2022</option>
                           <option value="2023">2023</option>
                        </Select>
                    </div>
                  </div>

                  <div class="col-md-12 form-group">
                    <div class="col-md-6">
                      <label for="inputEmail3" class="text-uppercase"> Competitive Exams (GRE / GMAT)  </label>
                       <Select name="competativeexam" id="competativeexam" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required onchange="checkComExam(this.value)">
                           <option value="">Select Option</option>
                           <option value="GRE"> GRE</option>
                           <option value="GMAT"> GMAT </option>
                           <option value="NOTGIVEN"> NOT GIVEN </option>
                        </Select>
                    </div>
                    <div class="col-md-6" id="competativeexamscored">
                      <label for="inputEmail3" class="text-uppercase"> Gre / GMAT Score </label>
                       <input type="text" class="form-control" id="competativeexamscore" name="competativeexamscore" placeholder="254..">
                    </div>
                  </div>

                  <div class="col-md-12 form-group">
                    <div class="col-md-6">
                      <label for="inputEmail3" class="text-uppercase"> Aggregate GPA  </label>
                       <Select name="gpaexam" id="gpaexam" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required onchange="checkGpa(this.value)">
                           <option value="">Select Option</option>
                           <option value="4"> Out Of 4</option>
                           <option value="5"> Out Of 5 </option>
                           <option value="7"> Out Of 7 </option>
                           <option value="10"> Out Of 10 </option>
                           <option value="20"> Out Of 20 </option>
                           <option value="100"> Out Of 100 </option>
                        </Select>
                    </div>
                    <div class="col-md-6"  id="gpaexamscored">
                      <label for="inputEmail3" class="text-uppercase"> GPA Score </label>
                       <input type="text" class="form-control" id="gpaexamscore" name="gpaexamscore" placeholder="254.." required>
                    </div>
                  </div>

                  <div class="col-md-12 form-group">
                    <div class="col-md-6">
                      <label for="inputEmail3" class="text-uppercase"> Work Experience  </label>
                       <Select name="workexperience" id="workexperience" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required onchange="checkWorkExp(this.value)">
                           <option value="">Select Option</option>
                           <option value="Yes"> Yes</option>
                           <option value="No"> No </option>
                        </Select>
                    </div>
                    <div class="col-md-6"  id="workexperiencemonthsd">
                      <label for="inputEmail3" class="text-uppercase"> Work Experience In months </label>
                       <input type="text" class="form-control" id="workexperiencemonths" name="workexperiencemonths" placeholder="24">
                    </div>
                  </div>

                  <div class="col-md-12 form-group">
                    <div class="col-md-6">
                      <label for="inputEmail3" class="text-uppercase"> Desired Country : &nbsp;&nbsp; </label>
                       <select name="desired_country" id="desired_country" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>

                        <option value="0">Select Country</option>

                        <?php foreach($countries as $country){?>

                          <option value="<?php echo $country->name;?>" <?php if(isset($selected_country) && $selected_country==$country->name){echo 'selected="selected"';}?> ><?php echo strtoupper($country->name);?></option>

                        <?php } ?>

                        <!--<option value="India">INDIA</option>-->

                      </select>
                    </div>
                    <div class="col-md-6">
                        <label for="inputEmail3" class="text-uppercase"> Desired Course &nbsp;&nbsp; </label>

                       <select name="desired_mainstream" id="desired_mainstream" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>

                         <option value="">Select Course</option>

                         <?php foreach($mainstreams as $course){?>

                         <option value="<?php echo $course->id;?>"><?php echo strtoupper($course->name);?></option>

                         <?php } ?>

                       </select>
                    </div>
                  </div>


                   <div class="col-md-12 form-group">
                    <div class="col-md-6">
                       <label for="inputEmail3" class="text-uppercase"> Desired Specialization &nbsp;&nbsp; </label>

                        <select name="desired_subcourse" id="desired_subcourse" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                          <option value="">Select Specialization</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                       <label for="inputEmail3" class="text-uppercase"> Desired Level Of Course &nbsp;&nbsp; </label>

                             <select name="desired_levelofcourse" id="desired_levelofcourse" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>

                               <option value="">Select Level Of Course</option>

                                             <?php foreach($degrees as $degree){ ?>

                             <option value="<?php echo $degree->id;?>" ><?php echo strtoupper($degree->name);?></option>

                             <?php } ?>

                             </select>
                    </div>


                    </div>
                    <div class="col-md-12 form-group">
                      <div class="col-md-6">
                        <label for="inputEmail3" class="text-uppercase">Current School / College you are studying at or have graduated from?</label>
                        <!--input type="text" class="form-control" id="cschool" name="cschool" placeholder="School / College" required autocomplete="off"-->
                        <select name="cschool" id="cschool" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required onchange="checkOther(this.value)">

                         <option value="">Select School / College</option>

                         <?php foreach($colleges as $college) { ?>

                           <option value="<?php echo $college['college'];?>" <?php if(isset($selected_college) && $selected_college==$college['college']){echo 'selected="selected"';}?> ><?php echo strtoupper($college['college']);?></option>

                         <?php } ?>

                         <!--<option value="India">INDIA</option>-->

                       </select>
                      </div>

                      <div class="col-md-6">
                          <label for="inputEmail3" class="text-uppercase"> Year Of Study  </label>
                          <Select name="yearstudy" id="yearstudy" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                              <option value="">Select Option</option>
                              <option value="1"> 1st Year </option>
                              <option value="2"> 2nd Year </option>
                              <option value="3"> 3rd Year </option>
                              <option value="4"> 4th Year </option>
                              <option value="5"> 5th Year </option>
                           </Select>

                      </div>
                    </div>
                    <div class="col-md-12 form-group" >

                    <div class="col-md-6" id="other_college" style="display:none;">
                        <label for="inputEmail3" class="text-uppercase"> Other College / School </label>
                        <input type="text" class="form-control" id="other_cschool" name="other_cschool" placeholder="Other College / School" autocomplete="off">

                    </div>
                    <div class="col-md-6">
                        <label for="inputEmail3" class="text-uppercase"> Which City are you currently residing in?  </label>
                        <input type="text" class="form-control" id="ccity" name="ccity" placeholder="City.." required autocomplete="off">

                    </div>
                </div>

              <div class="form-check" style="text-align: center;">
                <!-- <button type="submit" class="btn btn-login" ng-click="submitForm()">Submit</button> -->
                 <button type="button" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                  <input type="submit"  style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Register"></input>
              </div>


            </form>
        </div>
        <div class="col-md-6 banner-sec ">

          <img src="<?php echo base_url();?>application/images/Webinar_Reg_Middle_Banner.jpeg"
             class="img-fluid"  alt="Responsive Image"
             width="100%" height="100%" style="margin-top: 25%;" />
        </div>
    </div>
    <div id="secondBanner" class="row banner-secLogo">
     <!--  <a href="javascript:void(0)" class="closebtn" onclick="closeDiv()" style="color: black;font-size: 20px;float: right;margin-right: 5px;"><i class="fa fa-times" aria-hidden="true"></i></a> -->
      <img src="<?php echo base_url();?>application/images/Webinar_Reg_Bottom_Banner.jpg"
             class="img-fluid"  alt="Responsive Image"
             width="100%" height="80%" />
            <h2 style="text-align: center;color: #59419a;"> PARTICIPANTS EXPECTED FOR 2021 </h2>
      <div class="col-md-12 table-responsive">
        <div class="col-md-6 table-responsive">
          <table class="table table-bordered">
            <tbody>
              <tr>
                <td>Johns Hopkins University</td>
              </tr>
              <tr>
                <td>Rochester Institute of Technology</td>
              </tr>
              <tr>
                <td>University of Illinois at Chicago</td>
              </tr>
              <tr>
                <td>Stevens Institute of Technology</td>
              </tr>

              <tr>
                <td>University of South Florida</td>
              </tr>

              <tr>
                <td>SUNY Binghamton</td>
              </tr>

              <tr>
                <td>San Jose State University</td>
              </tr>
              <tr>
                <td>New York Institute of Technology</td>
              </tr>
              <tr>
                <td>Hult International Business School</td>
              </tr>
              <tr>
                <td>SUNY Polytechnic Institute</td>
              </tr>
              <tr>
                <td>University of Indianapolis</td>
              </tr>
              <tr>
                <td>James Cook University</td>
              </tr>
              <tr>
                <td>Cleveland State University</td>
              </tr>
              <tr>
                <td>University of Colorado Denver</td>
              </tr>
              <tr>
                <td>Oregon State University</td>
              </tr>
              <tr>
                <td>Western New England University</td>
              </tr>
              <tr>
                <td>Duke University</td>
              </tr>
              <tr>
                <td>National University</td>
              </tr>
              <tr>
                <td>ISM Germany</td>
              </tr>
              <tr>
                <td>College of the Canyons</td>
              </tr>
              <tr>
                <td>Kyoto University of Advanced Science</td>
              </tr>
              <tr>
                <td>Wright State University</td>
              </tr>
              <tr>
                <td>Saint Leo University</td>
              </tr>
              <tr>
                <td>Dallas Baptist University</td>
              </tr>
              <tr>
                <td>Dayton University</td>
              </tr>
              <tr>
                <td>University of Bridgeport</td>
              </tr>
              <tr>
                <td>University of Nebraska Kearney</td>
              </tr>
              <tr>
                <td>University of Alabama at Birmingham</td>
              </tr>
              <tr>
                <td>Washington State University</td>
              </tr>
              <tr>
                <td>Suffolk University</td>
              </tr>
              <tr>
                <td>George Mason University</td>
              </tr>
              <tr>
                <td>Saint Louis University</td>
              </tr>
              <tr>
                <td>University of Maryland Baltimore County</td>
              </tr>
              <tr>
                <td>University of Nebraska, Lincoln</td>
              </tr>



            </tbody>
          </table>
        </div>
        <div class="col-md-6 table-responsive">
          <table class="table table-bordered">
            <tbody>
              <tr>
                <td>University of Illinois Urbana-Champaign</td>
              </tr>
              <tr>
                <td>Arizona State University</td>
              </tr>
              <tr>
                <td>University of Texas at Arlington</td>
              </tr>
              <tr>
                <td>New Jersey Institute of Technology</td>
              </tr>
              <tr>
                <td>University of Arizona</td>
              </tr>
              <tr>
                <td>SRH University Heidelberg</td>
              </tr>
              <tr>
                <td>California State University Northridge</td>
              </tr>
              <tr>
                <td>Central Queensland University</td>
              </tr>
              <tr>
                <td>Oklahoma State University</td>
              </tr>
              <tr>
                <td>Victoria University of Wellington</td>
              </tr>
              <tr>
                <td>Pace University</td>
              </tr>
              <tr>
                <td>University of North Texas</td>
              </tr>
              <tr>
                <td>California State University Long Beach</td>
              </tr>
              <tr>
                <td>Florida Institute of Technology</td>
              </tr>
              <tr>
                <td>Flinders University</td>
              </tr>
              <tr>
                <td>Falmouth University</td>
              </tr>
              <tr>
                <td>Full Sail University</td>
              </tr>
              <tr>
                <td>The University of Akron</td>
              </tr>
              <tr>
                <td>Baylor University</td>
              </tr>
              <tr>
                <td>Marist College</td>
              </tr>
              <tr>
                <td>IUBH University of Applied Sciences</td>
              </tr>
              <tr>
                <td>SUNY Buffalo State</td>
              </tr>
              <tr>
                <td>University of Colorado Denver</td>
              </tr>
              <tr>
                <td>University of Kansas</td>
              </tr>
              <tr>
                <td>University of Advancing Technology</td>
              </tr>
              <tr>
                <td>Nova Southeastern University</td>
              </tr>
              <tr>
                <td>Illinois State University</td>
              </tr>
              <tr>
                <td>Colorado State University</td>
              </tr>
              <tr>
                <td>Marshall University</td>
              </tr>
              <tr>
                <td>Drew University</td>
              </tr>
              <tr>
                <td>Hofstra University</td>
              </tr>
              <tr>
                <td>Duquesne University</td>
              </tr>
              <tr>
                <td>Seattle Pacific University</td>
              </tr>

            </tbody>
          </table>
        </div>
      </div>

    </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
  $("#hide").click(function(){
    $("#secondBanner").hide();
    $("#show").show();
  });
  $("#show").click(function(){
    $("#secondBanner").show();
    $("#show").hide();
  });
});

  function closeDiv(){
    $("#secondBanner").hide();
    $("#show").show();
  }
</script>
</section>


      <!-- Content Wrapper. Contains page content -->
      <!-- <div class="content-wrapper">

        <section class="content">
          <div class="row">

            <div class="col-md-10">
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title" style="text-align: center">Register For Webinar</h3>
                </div>


                <form class="form-horizontal" onsubmit="resgistration()" method="post">

                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label">Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Name" required>
                      </div>
                    </div>

					         <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label">Email</label>
                      <div class="col-sm-8">
                        <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email" required>
                      </div>
                    </div>

                    <div class="form-group">
                       <label for="inputEmail3" class="col-sm-4 control-label">Mobile Number</label>
                       <div class="col-sm-8">
                         <input type="text" class="form-control" id="user_mobile" name="user_mobile" onfocusout="sendOTP()" placeholder="Mobile Number"  required>
                       </div>
                     </div>

                     <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">OTP</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control" id="otp_number" name="otp_number" placeholder="Sent OTP On Mobile ">
                        </div>
                        <div class="col-sm-4">
                          <button type="button" class="btn btn-warning" id="verifyotp" >Varify Mobile</button>
                        </div>
                      </div>

                     <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label"> Desired Country </label>
                        <div class="col-sm-8">
                          <select name="desired_country" id="desired_country" class="searchoptions" style="border:none; width:60%; height:30px; font-size:12px;" required>

                            <option value="0">Select Country</option>

                            <?php foreach($countries as $country){?>

                              <option value="<?php echo $country->name;?>" <?php if(isset($selected_country) && $selected_country==$country->name){echo 'selected="selected"';}?> ><?php echo strtoupper($country->name);?></option>

                            <?php } ?>


                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label"> Desired Mainstream </label>
                         <div class="col-sm-8">
                           <select name="desired_mainstream" id="desired_mainstream" class="searchoptions" style="border:none; width:60%; height:30px; font-size:12px;" required>

                             <option value="0">Select Mainstream</option>

                             <?php foreach($mainstreams as $course){?>

                             <option value="<?php echo $course->id;?>"><?php echo strtoupper($course->name);?></option>

                             <?php } ?>

                           </select>
                         </div>
                       </div>

                       <div class="form-group">
                          <label for="inputEmail3" class="col-sm-4 control-label"> Desired Substream </label>
                          <div class="col-sm-8">
                            <select name="desired_subcourse" id="desired_subcourse" class="searchoptions" style="border:none; width:60%; height:30px; font-size:12px;" required>

                              <option value="0">Select Substream</option>
                            </select>
                          </div>
                        </div>

                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label"> Desired Level Of Course </label>
                           <div class="col-sm-8">
                             <select name="desired_levelofcourse" id="desired_levelofcourse" class="searchoptions" style="border:none; width:50%; height:30px; font-size:12px;" required>

                               <option value="0">Select Level Of Course</option>

                                             <?php foreach($degrees as $degree){ ?>

                             <option value="<?php echo $degree->id;?>" ><?php echo strtoupper($degree->name);?></option>

                             <?php } ?>

                             </select>
                           </div>
                         </div>

                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label">Password</label>
                      <div class="col-sm-8">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                      </div>
                    </div>



                  </div>
                  <div class="box-footer" style="text-align: center">
					               <button type="button" style="text-align: center" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                         <input type="submit"  style="text-align: center" class="btn btn-info" value="Register"/>
                         <br/>
                         <br/>
                         <br/>
                         <br/>
                  </div>
                </form>
              </div>

            </div>
          </div>
        </section>
      </div> -->

      <script type="text/javascript">

  $( document ).ready(function() {

  $('#desired_mainstream').on('change', function() {

      var dataString = 'id='+this.value;

      $.ajax({

      type: "POST",

      url: "/search/university/getSubcoursesList",

      data: dataString,

      cache: false,

      success: function(result){

       $('#desired_subcourse').html(result);


      }

           });



     });

         // verify otp_number
         $('#verifyotp').on('click', function() {

              var mobileNumber = $('#user_mobile').val();
              var otpNumber = $('#otp_number').val();

              var dataString = 'mobile_number=' + mobileNumber + '&otp=' + otpNumber;

              $.ajax({

              type: "POST",

              url: "/webinar/registration/verifyotp",

              data: dataString,

              cache: false,

              success: function(result){

               alert(result);

              }

                  });

             });

             // verify resgistrationold
             $('#resgistrationold').on('click', function() {

                  var user_name = $('#user_name').val();
                  var user_email = $('#user_email').val();
                  var user_mobile = $('#user_mobile').val();
                  var desired_country = $('#desired_country').val();
                  var desired_mainstream = $('#desired_mainstream').val();
                  var desired_subcourse = $('#desired_subcourse').val();
                  var desired_levelofcourse = $('#desired_levelofcourse').val();
                  var userPassword = $('#password').val();

                  var dataString = 'user_name=' + user_name + '&user_email=' + user_email + '&user_mobile=' + user_mobile + '&desired_country=' + desired_country + '&desired_mainstream=' + desired_mainstream + '&desired_subcourse=' + desired_subcourse + '&desired_levelofcourse=' + desired_levelofcourse + '&user_password=' + userPassword ;

                  $.ajax({

                  type: "POST",

                  url: "/webinar/registration/registrationsignup",

                  data: dataString,

                  cache: false,

                  success: function(result){


                   if(result == "SUCCESS"){
                     alert("Welcome To HelloUni..!!!");
                     window.location.href='/webinar/webinarmaster/webinarlist';
                   } else {
                     alert(result)
                   }

                  }

                      });

                 });

   })


   //registration

   function resgistration(){

     var user_name = $('#user_name').val();
     var user_email = $('#user_email').val();
     var user_mobile = $('#user_mobile').val();
     var desired_country = $('#desired_country').val();
     var desired_mainstream = $('#desired_mainstream').val();
     var desired_subcourse = $('#desired_subcourse').val();
     var ccity = $('#ccity').val();
     var cschool = $('#cschool').val();
     var othercschool = $('#other_cschool').val();
     var masterlevelselector = $('#masterlevelselector').val();
     var bachelorslevelselector = $('#bachelorslevelselector').val();
     var desired_levelofcourse = $('#desired_levelofcourse').val();
     var userPassword = $('#password').val();
     var fairId = $('#fair_id').val();
     var userId = $('#user_id').val();

     //
     var intake_month = $('#intakemonth').val();
     var intake_year = $('#intakeyear').val();
     var comp_exam = $('#competativeexam').val();
     var comp_exam_score = $('#competativeexamscore').val();
     var gpa_exam = $('#gpaexam').val();
     var gpa_exam_score= $('#gpaexamscore').val();
     var work_exp = $('#workexperience').val();
     var work_exp_month = $('#workexperiencemonths').val();

     var dataString = 'user_name=' + user_name + '&user_email=' + user_email + '&user_mobile=' + user_mobile + '&desired_country=' + desired_country + '&desired_mainstream=' + desired_mainstream + '&desired_subcourse=' + desired_subcourse + '&desired_levelofcourse=' + desired_levelofcourse + '&user_password=' + userPassword + '&fair_id=' + fairId + '&user_id=' + userId + '&ccity=' + ccity
     + '&cschool=' + cschool + '&bachelorslevelselector=' + bachelorslevelselector + '&masterlevelselector=' + masterlevelselector + '&other_cschool=' + othercschool + '&intake_month=' + intake_month + "&intake_year=" + intake_year
     + '&comp_exam=' + comp_exam + '&comp_exam_score=' + comp_exam_score + "&gpa_exam=" + gpa_exam + "&gpa_exam_score=" + gpa_exam_score + "&work_exp=" + work_exp + "&work_exp_month=" + work_exp_month;
     event.preventDefault();
     $.ajax({

     type: "POST",

     url: "/webinar/registration/fairRegistration",

     data: dataString,

     cache: false,

     success: function(result){

      if(result == "SUCCESS"){

        alert("Welcome To HelloUni..!!!");
        window.location.href='/search/university';

      } else if(result == 'With Email-ID Or Mobile Account Already Exists'){

        alert(result);
        window.location.href='/user/account';

      } else {

        alert(result);

      }

     }

         });

   }

   // sent OTP
   function sendOTP() {


        var mobileNumber = $('#user_mobile').val();
        var mobilenumberlength = $('#user_mobile').val().length;
        var userEmail = $('#user_email').val();

        if(mobilenumberlength != 10) {

          alert("Mobile Number Not Proper ...!!!");

        } else {
          var dataString = 'mobile_number=' + mobileNumber + '&user_email=' + userEmail;

          $.ajax({

          type: "POST",

          url: "/webinar/registration/sentotp",

          data: dataString,

          cache: false,

          success: function(result){
            if(result == 'Mobile Number Already Registered'){

              alert(result);
              window.location.href='/user/account';

            } else{

              alert(result);

            }

          }
              });
        }

      }

      function checkOther(value){
          document.getElementById("other_college").style.display = "none";
          if(value == 'Other'){
              document.getElementById("other_college").style.display = "block";
          }
      }

      function checkComExam(value){
          document.getElementById("competativeexamscored").style.display = "block";
          if(value == 'NOTGIVEN'){
              document.getElementById("competativeexamscored").style.display = "none";
          }
      }

      function checkGpa(value){
          document.getElementById("gpaexamscored").style.display = "block";
          if(value == 'NOTGIVEN'){
              document.getElementById("gpaexamscored").style.display = "none";
          }
      }

      function checkWorkExp(value){
          document.getElementById("workexperiencemonthsd").style.display = "none";
          if(value == 'Yes'){
              document.getElementById("workexperiencemonthsd").style.display = "block";
          }
      }

     </script>
