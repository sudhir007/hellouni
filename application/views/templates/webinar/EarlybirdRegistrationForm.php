
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://www.imperial-overseas.com/styles/main.css">
<link rel="stylesheet" type="text/css" href="https://www.imperial-overseas.com/styles/vendor.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
<script  src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<style type="text/css">

p{
    font-size: 14px;
}
   @import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");
   .login-block{
   background: #004b7a;  /* fallback for old browsers */
   background: -webkit-linear-gradient(to bottom, #fafafb, #d8d9da);  /* Chrome 10-25, Safari 5.1-6 */
   /*background: linear-gradient(to bottom, #fafafb, #d8d9da);  */
   background: linear-gradient(to bottom, #ffffff, #7146a1);
   /*float:left;*/
   width:100%;
   margin-top: -2%;
   /*padding : 50px 0;*/
   }
   /*.banner-sec{background:url(<?php echo base_url();?>application/images/eduFair.jpeg)  no-repeat left bottom; background-size:cover; min-height:600px; border-radius: 0; padding:0;background-position: center center;    margin-top: 11%;}*/
   .login-sec{padding: 50px 30px; position:relative;}
   .login-sec .copy-text{position:absolute; width:80%; bottom:20px; font-size:13px; text-align:center;}
   .login-sec h2{margin-bottom:30px; font-weight:800; font-size:30px; color: #004b7a;}
   .login-sec h2:after{content:" "; width:100px; height:5px; background:#f9992f; display:block; margin-top:20px; border-radius:3px; margin-left:auto;margin-right:auto}
   .btn-login{background: #f9992f; color:#fff; font-weight:600;}
   .form-control{
   display: block;
   width: 100%;
   height: 34px;
   padding: 6px 12px;
   font-size: 14px;
   line-height: 1.42857143;
   color: #555;
   background-color: #fff;
   background-image: none;
   border: 1px solid #ccc;
   border-radius: 4px;
   -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
   box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
   -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
   -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
   transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
   }
   .table>tbody>tr>td{
   text-align: center;
   padding: 10px;
   line-height: 1.42857143;
   vertical-align: middle;
   border-top: 1px solid #ddd;
   font-size: 14px;
   font-family: sans-serif;
   letter-spacing: 0.8px;
   font-weight: 600;
   width: 51%;
   }
   .bootstrap-select .bs-ok-default::after {
    width: 0.3em;
    height: 0.6em;
    border-width: 0 0.1em 0.1em 0;
    transform: rotate(45deg) translateY(0.5rem);
}

.btn.dropdown-toggle:focus {
    outline: none !important;
}

.bootstrap-select>.dropdown-toggle{
  background-color: white;
    border: 1px solid #ccc;
    border-radius: 4px;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    color: #555;
    font-family: inherit;
    text-transform: capitalize;
        height: 30px;
    font-size: 12px;
}

.row:after, .row:before {
    content: " ";
    display: table;
}

section.content-section {
    padding: 15px 0;
}
.callToAction {
    background: #815dd5;
    font-size: 11px;
    padding: 15px 0;
    margin-top: 24px;
}

.btn.btn-primary {
    background-color: #f39d3f;
    border-color: #f39d3f;
    padding: 14px 12px;padding: 14px 12px;
}
.btn-primary.active.focus, .btn-primary.active:focus, .btn-primary.active:hover, .btn-primary:active.focus, .btn-primary:active:focus, .btn-primary:active:hover{
  background-color: #FF6600;
    border-color: #FF6600;
}
.pt10 {
    padding-top: 10px;
}


.underline {
    position: relative;
    padding-bottom: 10px;
    margin-bottom: 22px;
}

.fntMd {
    font-size: 16px;
}

.uspSession i {
    font-size: 18px;
    margin-right: 8px;
    color: #f56528;
}

#reservation_form {
    padding: 2rem 2rem;
    border-radius: 5px;
    box-shadow: 1px 1px 12px 3px #0000003b;
    background-color: white;
}

.btn-colored {
    background-color: #f56528;
}

.text-white {
    color: #fff;
}

.mb30 {
    margin-bottom: 30px!important;
}

section.content-section:nth-of-type(even) {
    background-color: #fff;
}

.countryDiv {
    box-shadow: 5px 5px 7px 4px #aba8a861;
    margin: 2% 0%;
    border-radius: 9px;
    padding: 2% 2%;
}

.countryDiv img {
    width: 100%;
}

.mpowerSection p {
    margin-bottom: 10px;
    text-align: justify;
}

.fntMd {
    font-size: 16px;
}

.mpowerSection .nav-tabs {
    border-bottom: 1px solid #f56528;
}

.nav-tabs {
    margin-bottom: 15px;
}

.mpowerSection .nav-tabs>li.active>a {
    color: #f56528;
    background-color: #ffffff;
    border: 1px solid #f56528;
    border-bottom-color: transparent;
    cursor: default;
    font-weight: bold;
}

.bubble-wrap {
  overflow: hidden;
  height: 600px;
}

.bubbles {
  position: relative;
  background: salmon;
}

.bubble {
  position: absolute;
  width: 152px;
  height: 152px;
  border-radius: 50%;
  box-shadow: 
    0 15px 35px rgba(0, 0, 0, 0.1),
    0 3px 10px rgba(0, 0, 0, 0.1);
  background-image: url('https://www.imperial-overseas.com/images/seminar2022/UK/uniLogoStrip.jpg');
  background-size: 1076px 1076px;
}

.logo1 { background-position:   0      0; }
.logo2 { background-position:  -154px  0; }
.logo3 { background-position:  -308px  0; }
.logo4 { background-position:  -462px  0; }
.logo5 { background-position:  -616px  0; }
.logo6 { background-position:  -770px  0; }
.logo7 { background-position:  -924px  0; }
.logo8 { background-position:   0     -154px; }
.logo9 { background-position:  -154px -154px; }
.logo10 { background-position: -308px -154px; }
.logo11 { background-position: -462px -154px; }
.logo12 { background-position: -616px -154px; }
.logo13 { background-position: -770px -154px; }
.logo14 { background-position: -924px -154px; }
.logo15 { background-position:  0     -308px; }
.logo16 { background-position: -154px -308px; }
.logo17 { background-position: -308px -308px; }
.logo18 { background-position: -462px -308px; }
.logo19 { background-position: -616px -308px; }
.logo20 { background-position: -770px -308px; }
.logo21 { background-position: -924px -308px; }
.logo22 { background-position:  0     -462px; }
.logo23 { background-position: -154px -462px; }
.logo24 { background-position: -308px -462px; }
.logo25 { background-position: -462px -462px; }
.logo26 { background-position: -616px -462px; }
.logo27 { background-position: -770px -462px; }
.logo28 { background-position: -924px -462px; }
.logo29 { background-position:  0     -616px; }
.logo30 { background-position: -154px -616px; }
.logo31 { background-position: -308px -616px; }
.logo32 { background-position: -462px -616px; }
.logo33 { background-position: -616px -616px; }
.logo34 { background-position: -770px -616px; }
.logo35 { background-position: -924px -616px; }
.logo36 { background-position:  0     -770px; }
.logo37 { background-position: -154px -770px; }
.logo38 { background-position: -308px -770px; }
.logo39 { background-position: -462px -770px; }
.logo40 { background-position: -616px -770px; }
.logo41 { background-position: -770px -770px; }
.logo42 { background-position: -924px -770px; }
.logo43 { background-position:  0     -924px; }
.logo44 { background-position: -154px -924px; }
.logo45 { background-position: -308px -924px; }
.logo46 { background-position: -462px -924px; }
.logo47 { background-position: -616px -924px; }
.logo48 { background-position: -770px -924px; }
.logo49 { background-position: -924px -924px; }

body {
  background: #E6EEF7;
}

.stats {
        display: flex;
        flex-wrap: wrap;
        gap: 40px;
        justify-content: center;
      }
      
      .card {
        width: 330px;
        /* background: #264653; */
        height: 200px;
        float: left;
        color: #fff;
        text-align: center;
        padding: 5px;
        /* box-shadow: 1px 1px 0px 7px #cccccc38; */
        margin: 40px 0px 40px 0px;
        border: 2px solid #f56528;
        border-radius: 5px;
      }
      
      .stats img {
        width: 30%;
        height: 43%;
        border-radius: 5px;
        position: relative;
        top: -27%;
      }
      
      .card h2 {
        padding: 5px;
        margin-bottom: 0px;
        padding-bottom: 0px;
        color: #f56528;
        font-size: 36px;
      }
      
      .stats p {
        padding: 5px;
        font-size: 16px;
        color: black;
        margin-bottom: 1px;
      }
      
      .card:hover {
        cursor: pointer;
        box-shadow: 0px 0px 4px #ccc;
      }
      
      @media (min-width: 320px) and (max-width: 1024px) {
        .stats {
          gap: 0px;
        }
      }

    .mt-35 {
        margin-top: -35px;
    }

    .thatDiv h3{
  color: #f56528;
    font-weight: 800;
}
.thatDiv .smallP{
  color: black;
    font-weight: 500;
}

 .shadow-effect {
        background: #fff;
        padding: 20px;
        border-radius: 4px;
        text-align: center;
        border: 1px solid #ECECEC;
        box-shadow: 0 19px 38px rgba(0, 0, 0, 0.10), 0 15px 12px rgba(0, 0, 0, 0.02);
        height: 550px;
      }
      
      #customers-testimonials .shadow-effect p {
        font-family: inherit;
        font-size: 15px;
        line-height: 1.5;
        margin: 0 0 17px 0;
        font-weight: 300;
      }
      
      .testimonial-name {
        margin: -17px auto 0;
        display: table;
        width: auto;
        background: #f56528;
        padding: 9px 35px;
        border-radius: 12px;
        text-align: center;
        color: #fff;
        box-shadow: 0 9px 18px rgba(0, 0, 0, 0.12), 0 5px 7px rgba(0, 0, 0, 0.05);
      }
      
      #customers-testimonials .item {
        text-align: center;
        padding: 25px;
        margin-bottom: 50px;
        opacity: .5;
        -webkit-transform: scale3d(0.8, 0.8, 1);
        transform: scale3d(0.8, 0.8, 1);
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
      }
      
      #customers-testimonials .owl-item.active.center .item {
        opacity: 1;
        -webkit-transform: scale3d(1.0, 1.0, 1);
        transform: scale3d(1.0, 1.0, 1);
      }
      
      .owl-carousel .owl-item img {
        transform-style: preserve-3d;
        max-width: 90px;
        margin: 0 auto 17px;
      }
      
      #customers-testimonials.owl-carousel .owl-dots .owl-dot.active span,
      #customers-testimonials.owl-carousel .owl-dots .owl-dot:hover span {
        background: #3190E7;
        transform: translate3d(0px, -50%, 0px) scale(0.7);
      }
      
      #customers-testimonials.owl-carousel .owl-dots {
        display: inline-block;
        width: 100%;
        text-align: center;
      }
      
      #customers-testimonials.owl-carousel .owl-dots .owl-dot {
        display: inline-block;
      }
      
      #customers-testimonials.owl-carousel .owl-dots .owl-dot span {
        background: #3190E7;
        display: inline-block;
        height: 20px;
        margin: 0 2px 5px;
        transform: translate3d(0px, -50%, 0px) scale(0.3);
        transform-origin: 50% 50% 0;
        transition: all 250ms ease-out 0s;
        width: 20px;
      }

      .imageFlow {
    margin-left: auto !important;
    margin-right: auto !important;
    display: block;
}

@media (min-width: 320px) and (max-width: 1024px) {

        .main .item {
          height: 80vh;
          position: relative;
        }
        .main .item .cover {
          top: -25% !important;
          left: 25px !important;
        }


        .main .item .cover .header-content span,
        .main .item .cover .header-content h4 {
          font-size: 18px;
        }
        .main .item .cover .header-content h6 {
          font-size: 14px;
        }
        .main .item .cover .header-content {
          padding: 35px;
        }

        .main .item .cover .bottom-content
          {

          } 

        .main .item .cover .btn {
          padding: 10px 16px;
          width: 50%;
          margin-left: 2px
        }
        .main ul {
          font-size: 16px;
        }

        .shadow-effect{
              height: 760px;
        }

        .underlineP:after{
          left: 15%;  
        }
        .mb20 {
            margin-bottom: 2%;
             height: auto !important; 
        }
      }

      #loader {
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        background: rgba(0,0,0,0.75) url('http://www.marcorpsa.com/ee/images/loading2.gif') no-repeat center center;
        z-index: 10000;
      }
   /*.blink {
   animation: blinker 0.6s linear infinite;
   color: #1c87c9;
   font-size: 30px;
   font-weight: bold;
   font-family: sans-serif;
   }
   @keyframes blinker {
   50% { opacity: 0; }
   }*/
   /*.banner-secLogo{background:url(<?php echo base_url();?>application/images/logsUni.jpeg)  no-repeat left bottom; background-size:cover; min-height:600px; border-radius: 0 10px 10px 0; padding:0;background-position: center;    margin-top: 25px;}*/
</style>
<section class="login-block" id="forgot-password-page">
   <div class="container-fluid" style="background:#fff; border-radius: 10px; box-shadow: 5px 5px 0px rgba(0,0,0,0.1);">
      <div class="row banner-secLogo">
         <img src="<?php echo base_url();?>images/LandingPage.jpg"
            class="img-fluid"  alt="Responsive Image"
            width="100%" height="80%" />
      </div>
      <!-- <div class="row">
         <div class="col-md-12 login-sec">
            <h2 class="text-center">Register Now</h2>
            <form class="login-form" class="form-horizontal" onsubmit="eduFairResgistration()" method="post" autocomplete="off">
               <div class="col-md-12 form-group">
                  <div class="col-md-6">
                     <label for="inputEmail3" class="text-uppercase">First Name</label>
                     <input type="text" class="form-control" id="user_first_name" name="user_first_name" placeholder="First Name" required>
                     <input type="hidden" value="<?=$utm_source?>" name="utm_source" id="utm_source">
                  </div>
                  <div class="col-md-6">
                     <label for="inputEmail3" class="text-uppercase">Last Name</label>
                     <input type="text" class="form-control" id="user_last_name" name="user_last_name" placeholder="Last Name" required>
                  </div>
               </div>
               <div class="col-md-12 form-group">
                  <div class="col-md-6">
                     <label for="inputEmail3" class="text-uppercase">Email</label>
                     <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email Id" required>
                  </div>
                  <div class="col-md-6">
                     <label for="inputEmail3" class="text-uppercase">Mobile Number</label>
                     <input type="text" class="form-control" id="user_mobile" name="user_mobile" placeholder="Mobile Number"  required>
                  </div>
               </div>
               <div class="col-md-12 form-group">
                  <div class="col-md-6">
                     <label for="inputEmail3" class="text-uppercase">City</label>
                     <input type="text" class="form-control" id="user_city" name="user_city" placeholder="City" required>
                  </div>
                   <div class="col-md-6">
                     <label for="inputEmail3" class="text-uppercase">Country of Study</label>
                     <select name="user_desired_country" id="user_desired_country" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                      <option value="0">Select Country</option>
                      <?php foreach($countries as $country){?>
                        <option value="<?php echo $country->name;?>" ><?php echo strtoupper($country->name);?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

              <div class="col-md-12 form-group">
                <div class="col-md-6">
                     <label for="inputEmail3" class="text-uppercase"> Intake Year </label>
                     <Select name="user_intake_year" id="user_intake_year" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                        <option value="">Select Year</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                     </Select>
                  </div>
                  <div class="col-md-6">
                     <label for="inputEmail3" class="text-uppercase"> Loan Type : </label>
                     <select name="user_loan_type" id="user_loan_type" multiple class="multibox selectpicker  form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                        <option>Collateral</option>
                        <option>Non-Collateral</option>
                        <option>Cosigner</option>
                    </select>
                  </div>
              </div>
               <script>
                  $(document).ready(function () {
                      $('.selectpicker').selectpicker();
                  })
              </script>
               <div class="form-check" style="text-align: center;">
                  <button type="button" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                  <input type="submit"  style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Register"></input>
               </div>
            </form>
         </div>
      </div> -->
   </div>
   <script type="text/javascript">
      $(document).ready(function(){
      $("#hide").click(function(){
        $("#secondBanner").hide();
        $("#show").show();
      });
      $("#show").click(function(){
        $("#secondBanner").show();
        $("#show").hide();
      });
      });

      function closeDiv(){
        $("#secondBanner").hide();
        $("#show").show();
      }
   </script>
</section>
<section style="padding: 0 !important;">
  <div class="callToAction footer-bar-3 hidden-xs" style="background: #815dd5;margin-top: 0px !important;">
    <div class="container">
      <div class="row">

        <div class="col-md-4">
          <div class="row visible-md visible-lg" style="text-align: center;"> <a class="btn btn-primary btn-block" id="Registerbutton12">Register Now!</a> </div>
          <div class="row visible-xs visible-sm" style="text-align: center;"> <a class="btn btn-primary btn-block" id="RegisterbuttonSmall12">Register Now!</a> </div>
        </div>
        <div class="col-md-8 pt10">
          <h5 style="color: white;">Get Admission in Top Universities anywehre in the World !</h5> 
        </div>
      </div>
    </div>
  </div>
</section>
<section class="content-section" >
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-12 section-header text-center visible-md visible-lg">
          <h1 class="underline" >What We are?</h1> 
        </div>
        <div class="col-md-12 text-center visible-xs visible-sm">
          <h4 class="underline " >What We are?</h4> 
        </div>
        <div class="col-md-12 text-justify">
          <p class="text-justify fntMd" ><b style="text-transform: capitalize;">Hello<span style="color: #f39d3f">UNI</span></b> is your ultimate gateway to prestigious universities across the globe! We are here to empower you in every step along the way, to convert your study abroad plans into reality!</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-12 section-header text-center visible-md visible-lg">
          <h1 class="underline" ><b style="text-transform: capitalize;">Hello<span style="color: #f39d3f">UNI</span></b> is the answer!</h1> 
        </div>
        <div class="col-md-12 text-center visible-xs visible-sm">
          <h4 class="underline " ><b style="text-transform: capitalize;">Hello<span style="color: #f39d3f">UNI</span></b> is the answer!</h4> 
        </div>
        <div class="col-md-12 text-justify">
          <ul class="fntMd uspSession">
            <li><i class="fa fa-check list-icon"></i>Interact directly with Universities face-to-face & virtually.</li>
            <li><i class="fa fa-check list-icon"></i>Obtain clear information regarding Scholarships / Eligibility / Timeline / Assistantships / Costs / Courses etc.</li>
            <li><i class="fa fa-check list-icon"></i>Easy & Convenient Application Process - One form to apply to Multiple Universities.</li>
            <li><i class="fa fa-check list-icon"></i>Documentation - SOP / LOR / Resume.</li>
            <li><i class="fa fa-check list-icon"></i>Guidance from experienced counsellors for all your study abroad queries.</li>
            <li><i class="fa fa-check list-icon"></i>VISA Guidance.</li>
            <li ><i class="fa fa-check list-icon"></i>Education Loan Guidance.</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row stats">
      <div class="card"><img src="https://www.imperial-overseas.com/images/seminar2022/1.jpg" />
        <div class="mt-35">
          <h2>21232+++</h2>
          <p>Admits Received from Leading Universities</p>
        </div>
      </div>
      <div class="card"><img src="https://www.imperial-overseas.com/images/seminar2022/2.jpg" />
        <div class="mt-35">
          <p>Students guided to over</p>
          <h2>195++</h2>
          <p>Courses (Masters & Bachelors)</p>
        </div>
      </div>
      <div class="card"><img src="https://www.imperial-overseas.com/images/seminar2022/3.jpg" />
        <div class="mt-35">
          <h2>53611+++</h2>
          <p>Students Sent Overseas</p>
        </div>
      </div>
    </div>
    <div class="row stats" >
      <div class="card"><img src="https://www.imperial-overseas.com/images/seminar2022/4.jpg" />
        <div class="mt-35">
          <p>Students Sent to</p>
          <h2>212++</h2>
          <p>Universities Globally</p>
        </div>
      </div>
      <div class="card"><img src="https://www.imperial-overseas.com/images/seminar2022/5.jpg" />
        <div class="mt-35">
          <p>Scholarships / Tuition Waiver of over </p>
          <h2>50 Mn USD</h2> </div>
      </div>
      <div class="card" ><img src="https://www.imperial-overseas.com/images/seminar2022/6.jpg" />
        <div class="mt-35">
          <h2>842++</h2>
          <p id="RegisterForm">Admits in Top 25 Engineeing Schools (US)</p>
        </div>
      </div>
    </div>
    <!-- end row -->
  </div>
  <!-- end container -->
</section>

<section class="content-section" id="aboutus" style="background-image: url('https://www.imperial-overseas.com/images/salient-usa.jpg');">
  <div class="container" >
    <div class="row">
      <div class="col-md-12">
        <!-- <div class="col-md-12 section-header text-center">
               <h2 class="section-title underline text-black">What is GRE?</h2>
            </div> -->
        <div class="col-md-7 text-justify">
          
          <div class="col-md-12 section-header text-center visible-md visible-lg">
            <h1 class="underline" style="color: white;">Who are we?</h1> 
          </div>
          <div class="col-md-12 text-center visible-xs visible-sm">
            <h4 class="underline" style="color: white;">Who are we?</h4> 
          </div>
          <p class="fntMd" style="color: white;">Many students want to get into their dream universities abroad, but only a few know the right approach, and very rarely do they understand the admissions and visa process. This is where we - <b style="text-transform: capitalize;">Hello<span style="color: #f39d3f">UNI</span></b>, as study abroad consultants, who have been in the business for more than a decade now, come into the picture.</p>
          <ul class="uspSession fntSm" style="color: white;">
            <li><i class="fa fa-check list-icon"></i>We have placed over 10,000 students globally in 600+ institutes, getting them over USD 60 Million in scholarships.</li>
            <li><i class="fa fa-check list-icon"></i>In addition, 55% of our counselors have Masters from different countries, and 30% even have PhDs from abroad – making them great leaders and advisors in studying abroad.</li>
            <li><i class="fa fa-check list-icon"></i>We have been successful in placing students in leading universities, including University of Manchester, Warwick University, Newcastle University, Imperial College London, University of Nottingham, University of York, University of Southampton, University of Leeds, University of Liverpool, University of Glasgow, University of Birmingham, Queen Mary University of London, University of Exeter, Queens University Belfast.</li>
            <li id="RegisterFormSmall"><i class="fa fa-check list-icon"></i>We are driven by a genuine commitment toward the success of each of our students, and our existence and growth revolve around their growth. So every day, we roll up our sleeves to ensure that students get the most hassle-free and personalized experience at such an exciting decision-making time!</li>
          </ul>
        </div>
        <div class="col-md-5">
          <div class="col-md-12 section-header text-center">
            <h2 class="underline visible-md visible-lg" style="margin-top: 1.5rem;">Register Now !</h2> 
            <h2 class="underline visible-xs visible-sm" style="margin-top: 1.5rem;color: white;">Register Now !</h2></div>
          <form id="reservation_form" name="reservation_form" class="reservation-form mt-20" onsubmit="earlybirdRegistrationForm()" method="post" autocomplete="off">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group mb-20">
                  <input placeholder="Full Name" id="student_name" name="student_name" class="form-control fieldss" type="text" required> </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group ">
                  <input type="email" id="student_email" name="student_email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Email" class="form-control fieldss"  required>
                  <p id="msgdis1" style="display:none;font-size:12px;color:#fff;">Email Id Already Exists</p>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group mb-20">
                  <input name="student_phone" class="form-control fieldss" placeholder="Mobile No" aria-required="true" id="student_phone" type="text" pattern="[6789][0-9]{9}" title="Phone number starts with 6-9 and remaing 9 digit with 0-9" required > </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group mb-15">
                  <select name="English_Test" id="English_Test" class="fieldss form-control" >
                    <option value="">- Any English Test -</option>
                    <option value="IELTS">IELTS</option>
                    <option value="TOEFL">TOEFL</option>
                    <option value="PTE">PTE</option>
                    <option value="DET">DET</option> 
                  </select>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group mb-20">
                  <input placeholder="English Test Score" id="English_Test_Score" name="English_Test_Score" class="form-control fieldss" type="text" > </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group mb-20">
                  <input placeholder="GRE Score" id="GRE_Score" name="GRE_Score" class="form-control fieldss" type="text" > </div>
              </div>
              
              <div class="col-sm-12">
                <div class="form-group mb-15">
                  <select name="student_required_country" id="student_required_country" class="form-control fieldss" required>
                    <option value="">- Country Choice -</option>
                    <option value="USA">USA</option>
                    <option value="UK">UK</option>
                    <option value="Canada">Canada</option>
                    <option value="Germany">Germany</option>
                    <option value="Australia">Australia</option>
                    <option value="NewZealand">New Zealand</option>
                    <option value="Ireland">Ireland</option>
                    <option value="Other">Other</option>
                  </select>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group mb-15">
                  <select name="student_required_degree" id="student_required_degree" class="form-control fieldss" >
                    <option value="">- Looking for which Degree -</option>
                    <option value="Bachelors">Bachelors</option>
                    <option value="Graduation">Graduation</option>
                    <option value="PostGraduation">Post Graduation</option>
                    <option value="Masters">Masters</option>
                    <option value="MBA">MBA</option>
                    <option value="PHD">PHD</option>
                    <option value="Other">Other</option>
                  </select>
                </div>
              </div>
              
              <div class="col-sm-12">
                <div class="form-group mb-20">
                  <select name="student_intake" id="student_intake" class="fieldss form-control" required>
                    <option value="">- For which Intake? -</option>
                    <option value="Sep2023">Sep 2023</option>
                    <option value="Jan2024">Jan 2024 </option>
                    <option value="Sep2024">Sep 2024</option>
                    <option value="Jan2025">Jan 2025</option>
                    <option value="Sep2025">Sep 2025</option>
                  </select>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group mb-0 mt-10">
                  <button type="submit" class="btn btn-colored btn-theme-colored2 text-white btn-lg btn-block font-size-18" value="Register" >Submit</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- end row -->
  </div>
  <!-- end container -->
</section>

<section class="content-section">
    <div class="container">
      <div class="col-md-12">
          <div class="col-md-12 section-header text-center  visible-md visible-lg">
              <h1 class="underline">What We Offer!</h1>
          </div>
          <div class="col-md-12 section-header text-center  visible-xs visible-sm">
          <h4 class="underline">What We Offer!</h4> 
          </div>
          
          <div class="text-center visible-xs visible-sm"> <h5 class="underline">Our Services</h5></div>
          <div class="row text-center">
            <div class="col-md-4">
              <div class="icon mb30"> <img src="https://www.imperial-overseas.com/images/icon/Counselling.png" alt="Counselling" class="icon-svg-2x"></div>
              <h5>&nbsp;<b>Counselling</b></h5>
              <p>We guide and design the best path with the right knowledge and insight for you for MBBS Abroad.</p>
            </div>
            <div class="col-md-4">
              <div class="icon mb30"> <img src="https://www.imperial-overseas.com/images/icon/University-Shortlisting.png" alt="University Shortlisting" class="icon-svg-2x"></div>
              <h5>&nbsp;<b>University Shortlisting</b></h5>
              <p>We help you to find the perfect and most suitable university for your career.</p>
            </div>
            <div class="col-md-4">
              <div class="icon mb30"> <img src="https://www.imperial-overseas.com/images/icon/Application.png" alt="Application" class="icon-svg-2x"></div>
              <h5>&nbsp;<b>Application</b></h5>
              <p>We apply your application directly to your selected university.</p>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-4">
              <div class="icon mb30"> <img src="https://www.imperial-overseas.com/images/icon/Admission-and-Documentation.png" alt="Admission and Documentation" class="icon-svg-2x"></div>
              <h5>&nbsp;<b>Admission and Documentation</b></h5>
              <p>We complete all your documentation processes.</p>
            </div>
            <div class="col-md-4">
              <div class="icon mb30"> <img src="https://www.imperial-overseas.com/images/icon/VisaTickets.png" alt="Visa &amp; Tickets" class="icon-svg-2x"></div>
              <h5>&nbsp;<b>Visa &amp; Tickets</b></h5>
              <p> Once your invitation arrives, we apply for your visa and tickets.</p>
            </div>
            <div class="col-md-4">
              <div class="icon mb30"> <img src="https://www.imperial-overseas.com/images/icon/Travel-Assistance.png" alt="Travel Assistance" class="icon-svg-2x"></div>
              <h5>&nbsp;<b>Travel Assistance</b></h5>
              <p> From India to the University hostel, we all take care of your travel arrangements.</p>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-4">
              <div class="icon mb30"> <img src="https://www.imperial-overseas.com/images/icon/Accommodation.png" alt="Accommodation" class="icon-svg-2x"></div>
              <h5>&nbsp;<b>Accommodation</b></h5>
              <p> We provide government/ private hostels as per requirement.</p>
            </div>
            <div class="col-md-4">
              <div class="icon mb30"> <img src="https://www.imperial-overseas.com/images/icon/Local-Support.png" alt="Local Support" class="icon-svg-2x"></div>
              <h5>&nbsp;<b>Local Support</b></h5>
              <p> We have our local representative for your support.</p>
            </div>
            <div class="col-md-4">
              <div class="icon mb30"> <img src="https://www.imperial-overseas.com/images/icon/Forex-Support.png" alt="Forex Support" class="icon-svg-2x"></div>
              <h5>&nbsp;<b>Forex Support</b></h5>
              <p>We render most economical foreign exchange support from our tie-up forex company.</p>
            </div>
          </div>
      </div>
      <div class="clearfix"></div>  
    </div> <!-- end container --> 
</section>
<section class="content-section mpowerSection" id="Mpower">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-12 section-header text-center visible-md visible-lg">
          <h1 class="underline" >Countries</h1> 
        </div>
        <div class="col-md-12 text-center visible-xs visible-sm">
          <h4 class="underline" >Countries</h4> 
        </div>
        <div class="col-md-12 text-justify">
          <!-- <p class="text-justify fntMd">Imperial Overseas – India’s leading study abroad consultancy, along with education loan giants – <a href="https://www.mpowerfinancing.com/" target="_">MPOWER Financing</a>, is arranging multiple events across three major cities – Mumbai, Pune, and Bangalore, to raise awareness on education options available abroad and ways of financing through loans. Every session that will last for 2 hours will be an interactive panel discussion of notable industry professionals in their respective domains. In addition, students can attend with their parents/guardians to resolve their queries.</p> -->
          
         
          <!-- <p class="sub-heading fntMd"><a href="https://www.mpowerfinancing.com/" target="_"><span style="color: #f2bc33">M</span><span style="color: #5CA1D8">POWER</span></a> was founded in 2014 by Manu Smadja and Mike Davis, two former international students with a shared dream: to remove the financial obstacles students often face when pursuing an international education. We’re a passionate team of global citizens helping students from around the world pursue their dreams.</p> -->
          
          
        </div>
      </div>
    </div>
    <div class="col-md-12 countryDiv">
      <div class="col-md-4 col-sm-12 col-xs-12">
        <div class="text-center">
          <h2 class="underline visible-md visible-lg">Study In</h2>
          <h5 class="text-center visible-xs visible-sm" >Study In</h5>
          <img src="https://www.imperial-overseas.com/images/seminar2022/UK/USA.jpg" alt="study abroad consultants" >

        </div>
        <div>
          <div class="row visible-md visible-lg" style="text-align: center; margin-top: 10px"> <a class="btn btn-primary btn-block" id="Registerbutton14">Register Now!</a> </div>
          <div class="row visible-xs visible-sm" style="text-align: center;"> <a class="btn btn-primary btn-block" id="RegisterbuttonSmall14">Register Now!</a> </div>
        </div>
        <p class="sub-heading fntMd"></p>
      </div>
      <div class="col-md-8">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#homeUSA">About USA</a></li>
          <li><a data-toggle="tab" href="#menuUSA1">SALIENT FEATURES</a></li>
          <li><a data-toggle="tab" href="#menuUSA3">EXPENSES</a></li>
        </ul>
        <div class="tab-content" style="max-height: 500px;overflow: auto;">
          <div id="homeUSA" class="tab-pane fade in active">
            <h3>About USA</h3>
            <p>USA is the undisputed leader of higher education, it has some of the best Universities in the world with outstanding experiential-focused curriculum that prepares students to be industry ready. The research work conducted is cutting-edge and pioneering, with every subject and domain conceivable open for study. Work opportunities far surpass every other country, be it for starting salary, advancement and number of prospects. It is also the most multicultural, attracting talents from across the globe, enabling you to network with the best individuals in your chosen field.</p>
            

          </div>
          
          <div id="menuUSA1" class="tab-pane fade">
            <h3>EDUCATION SCENARIO TO STUDY IN USA</h3>
            <ul>
              <li><i class="fa fa-check list-icon"></i> WORLD-CLASS INSTITUTIONS</li>
              <li><i class="fa fa-check list-icon"></i> DIVERSE CULTURES</li>
              <li><i class="fa fa-check list-icon"></i> FLEXIBLE PROGRAMS</li>
              <li><i class="fa fa-check list-icon"></i> AFFORDABLE TUITION FOR ALL BUDGETS</li>
              <li><i class="fa fa-check list-icon"></i> EASY SCHOLARSHIPS</li>
              <li><i class="fa fa-check list-icon"></i> CREDIBILITY OF INSTITUTIONS</li>
              <li><i class="fa fa-check list-icon"></i> WORK OPPORTUNITIES</li>
            </ul>
          </div>
          <div id="menuUSA3" class="tab-pane fade">
            <h3>EXPENSES TO STUDY IN USA</h3>
            <ul>
              <li><i class="fa fa-check list-icon"></i> <b>Undergraduate:</b> US $40,000 per year Colleges: US $28,000 per year</li>
              <li><i class="fa fa-check list-icon"></i> <b>Postgraduate:</b> US $45,000 per year Colleges: US $28,375 per year</li>
            </ul>
          </div>
          <!-- <div id="menu4" class="tab-pane fade">
            <h3>VARIOUS SCHOLARSHIP OPPORTUNITIES ARE AVAILABLE ON</h3>
            <ul class="tagscolor">
              <li><i class="fa fa-check list-icon"></i> Scholarship Search: <a href="http://www.scholarship-search.org.uk/" target="_blank">www.scholarship-search.org.uk/</a></li>
              <li><i class="fa fa-check list-icon"></i> Education UK: <a href="http://www.educationuk.org/global/articles/scholarships-financial-support/" target="_blank">www.educationuk.org/global/articles/scholarships-financial-support/</a></li>
              <li><i class="fa fa-check list-icon"></i> The British Council: <a href="http://www.britishcouncil.org/" target="_blank">www.britishcouncil.org/</a></li>
            </ul>
          </div> -->
          
        </div>
      </div>
    </div>
    <div class="col-md-12 countryDiv">
      <div class="col-md-12 visible-xs visible-sm">
        <div class="text-center">
          <h2 class="underline visible-md visible-lg">Study In</h2>
          <h5 class="text-center visible-xs visible-sm" >Study In</h5>
          <img src="https://www.imperial-overseas.com/images/seminar2022/UK/Canada.jpg" alt=" study in canada" >
          <div>
            
            <div class="row visible-xs visible-sm" style="text-align: center;"> <a class="btn btn-primary btn-block" id="RegisterbuttonSmall11">Register Now!</a> </div>
          </div>
          <p class="sub-heading fntMd"></p>
        </div>
      </div>
      <div class="col-md-8">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#home4">About CANADA</a></li>
          <li><a data-toggle="tab" href="#menu41">SALIENT FEATURES</a></li>
          <li><a data-toggle="tab" href="#menu43">EXPENSES</a></li>
        </ul>
        <div class="tab-content" style="max-height: 500px;overflow: auto;">
          <div id="home4" class="tab-pane fade in active">
            <h3>About CANADA</h3>
            <p>Canada is the home of the top Universities in the world with high-quality education hence students’ priority choice for higher education. Canadian education strongly focuses on research and development, so for research scholars, no country is better than Canada. The Canadian government offers excellent support for research in various fields where economic growth is promising. Tuition fees in Canada are relatively cheap compared to the US and UK, and there are great chances of getting a scholarship in Universities. Canada is safe and peaceful, diversified and multicultural, giving authentic vibes of friendliness. A Canadian post-graduation work permit allows staying up to 3 years in the country. During this period, international work experience can be gained and later, permanent residency can be applied.</p>
            

          </div>
          <div id="menu41" class="tab-pane fade">
            <h3>SALIENT FEATURES TO STUDY CANADA</h3>
            
            <ul>
              <li><i class="fa fa-check list-icon"></i> Affordable Education in Canada</li>
              <li><i class="fa fa-check list-icon"></i> Cultural diversity: 1.37 million of the population in Canada is Indo-Canadian.</li>
              <li><i class="fa fa-check list-icon"></i> Quality of life: Canada ranks best in terms of quality of life.</li>
              <li><i class="fa fa-check list-icon"></i> Student-friendly cities of Toronto, Ontario and Montreal make it easier for international students to adjust to a new country.</li>
              <li><i class="fa fa-check list-icon"></i> Practical education: Most of the programs in Canada are offered in a co-op format.</li>
              <li><i class="fa fa-check list-icon"></i> Canadian universities emphasize a lot of skill development through internships as well as theoretical education.</li>
              <li><i class="fa fa-check list-icon"></i> Most Easy Country to Settle using PR</li>
            </ul>
          </div>
          <div id="menu43" class="tab-pane fade">
            <h3>EXPENSES TO STUDY IN CANADA</h3>
              <div class="col-md-6" >
                     <table class="table table-bordered">
                        <tbody>
                           <tr>
                              <th>
                                 Undergraduate
                              </th>
                              <td>
                                 Rs. 60-70 Lakhs
                              </td>
                           </tr>
                           <tr>
                              <th>PG Diploma</th>
                              <td>Rs. 15-20 Lakhs</td>
                           </tr>
                           <tr>
                              <th>
                                 Master’s 
                              </th>
                              <td>
                                 Rs. 30-35 Lakhs
                              </td>
                           </tr>
                           <tr>
                              <th>PhD</th>
                              <td>fully funded by the state.</td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
            </ul>
          </div>
          
          
        </div>
      </div>
      <div class="col-md-4 visible-lg visible-md">
        <div class="text-center">
          <h2 class="underline visible-md visible-lg">Study In</h2>
          <h5 class="text-center visible-xs visible-sm" >Study In</h5>
          <img src="https://www.imperial-overseas.com/images/seminar2022/UK/Canada.jpg" alt="study in canada" >
          <div>
            <div class="row visible-md visible-lg" style="text-align: center; margin-top: 10px"> <a class="btn btn-primary btn-block" id="Registerbutton11">Register Now!</a> </div>
          </div>
          <p class="sub-heading fntMd"></p>
        </div>
      </div>
    </div>
    <div class="col-md-12 countryDiv">
      <div class="col-md-4 col-sm-12 col-xs-12">
        <div class="text-center">
          <h2 class="underline visible-md visible-lg">Study In</h2>
          <h5 class="text-center visible-xs visible-sm" >Study In</h5>
          <img src="https://www.imperial-overseas.com/images/seminar2022/UK/UK.jpg" alt="universities in uk for international students" >

        </div>
        <div>
          <div class="row visible-md visible-lg" style="text-align: center; margin-top: 10px"> <a class="btn btn-primary btn-block" id="Registerbutton7">Register Now!</a> </div>
          <div class="row visible-xs visible-sm" style="text-align: center;"> <a class="btn btn-primary btn-block" id="RegisterbuttonSmall7">Register Now!</a> </div>
        </div>
        <p class="sub-heading fntMd"></p>
      </div>
      <div class="col-md-8">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#home">About UK</a></li>
          <li><a data-toggle="tab" href="#menu1">SALIENT FEATURES</a></li>
          <li><a data-toggle="tab" href="#menu3">EXPENSES</a></li>
          <li><a data-toggle="tab" href="#menu4">SCHOLARSHIP</a></li>
        </ul>
        <div class="tab-content" style="max-height: 500px;overflow: auto;">
          <div id="home" class="tab-pane fade in active">
            <h3>About UK</h3>
            <p>The United Kingdom is actually the short conventional name for the United Kingdom of Great Britain and Northern Ireland; note – the island of Great Britain includes England, Scotland, and Wales. It is one of the oldest monarchies who at one time held about one fourth of the earth’s surface as its colonies under the British Empire in the 19th century. The United Kingdom is a developed country and has the world’s fifth-largest economy by nominal GDP and ninth-largest economy by purchasing power parity. It was the world’s first industrialised country and the world’s foremost power during the 19th and early 20th centuries. The United Kingdom has a temperate climate, with plentiful rainfall all year round.</p>
            

          </div>
          
          <div id="menu1" class="tab-pane fade">
            <h3>EDUCATION SCENARIO TO STUDY IN UK</h3>
            <h5>TYPES OF TAUGHT DEGREES IN UK UNIVERSITIES</h5>
            <ul>
              <li><i class="fa fa-check list-icon"></i> Academic Excellence and Quality Education</li>
              <li><i class="fa fa-check list-icon"></i> Strong Research Infrastructure</li>
              <li><i class="fa fa-check list-icon"></i> Opportunities offered by UK education system and Blended Programs</li>
              <li><i class="fa fa-check list-icon"></i> Work while you study</li>
              <li><i class="fa fa-check list-icon"></i> Scholarship and financial support</li>
            
              <li><i class="fa fa-check list-icon"></i> 1 Year MSc</li>
              <li><i class="fa fa-check list-icon"></i> 15 Years education is accepted</li>
              <li><i class="fa fa-check list-icon"></i> Possibility of PR</li>
              <li><i class="fa fa-check list-icon"></i> No GRE/GMAT required</li>
             <!--  <li><i class="fa fa-check list-icon"></i> Undergraduate: GBP 11000 per year</li>
              <li><i class="fa fa-check list-icon"></i> Postgraduate: GBP 12000 approximately, Management courses can go up to GBP 20000 for many top business schools</li> -->
            </ul>
          </div>
          <div id="menu3" class="tab-pane fade">
            <h3>EXPENSES TO STUDY IN UK</h3>
            <ul>
              <li><i class="fa fa-check list-icon"></i> <b>Undergraduate:</b> GBP 11000 per year</li>
              <li><i class="fa fa-check list-icon"></i> <b>Postgraduate:</b> GBP 12000 approximately, Management courses can go up to GBP 20000 for many top business schools</li>
            </ul>
          </div>
          <div id="menu4" class="tab-pane fade">
            <h3>VARIOUS SCHOLARSHIP OPPORTUNITIES ARE AVAILABLE ON</h3>
            <ul class="tagscolor">
              <li><i class="fa fa-check list-icon"></i> Scholarship Search: <a href="http://www.scholarship-search.org.uk/" target="_blank">www.scholarship-search.org.uk/</a></li>
              <li><i class="fa fa-check list-icon"></i> Education UK: <a href="http://www.educationuk.org/global/articles/scholarships-financial-support/" target="_blank">www.educationuk.org/global/articles/scholarships-financial-support/</a></li>
              <li><i class="fa fa-check list-icon"></i> The British Council: <a href="http://www.britishcouncil.org/" target="_blank">www.britishcouncil.org/</a></li>
            </ul>
          </div>
          
        </div>
      </div>
    </div>
    <div class="col-md-12 countryDiv">
      <div class="col-md-12 visible-xs visible-sm">
        <div class="text-center">
          <h2 class="underline visible-md visible-lg">Study In</h2>
          <h5 class="text-center visible-xs visible-sm" >Study In</h5>
          <img src="https://www.imperial-overseas.com/images/seminar2022/UK/Germany.jpg" alt="universities in germany for international students" >
          <div>
          <div class="row visible-xs visible-sm" style="text-align: center;"> <a class="btn btn-primary btn-block" id="RegisterbuttonSmall10">Register Now!</a> </div>
        </div>
          <p class="sub-heading fntMd"></p>
        </div>
      </div>
      <div class="col-md-8">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#home3">About Germany</a></li>
          <li><a data-toggle="tab" href="#menu31">SALIENT FEATURES</a></li>
          <li><a data-toggle="tab" href="#menu33">EXPENSES</a></li>
        </ul>
        <div class="tab-content" style="max-height: 500px;overflow: auto;">
          <div id="home3" class="tab-pane fade in active">
            <h3>About Germany</h3>
            <p>We owe a lot to Germany when it comes to the history of education and universities. Germany is the birthplace of what we call ‘research universities’ – big places with ivory towers and libraries where new knowledge is produced every day. With an amazing reputation for scientific discoveries and technology, Germany is home to some of the highest ranked universities in the world. Places like RWTH University, TUM, and KIT are recognised all over the world for being among the best international universities worldwide, and offer competitive Master&#39;s, Bachelor&#39;s, and Ph.D degrees.</p>
          </div>
          <div id="menu31" class="tab-pane fade">
            <h3>SALIENT FEATURES TO STUDY GERMANY</h3>
            <ul>
              <li><i class="fa fa-check list-icon"></i> German universities are tuition-free</li>
              <li><i class="fa fa-check list-icon"></i> Germany is at the forefront of Engineering</li>
              <li><i class="fa fa-check list-icon"></i> Top-quality education</li>
              <li><i class="fa fa-check list-icon"></i> Achieve German language excellency</li>
              <li><i class="fa fa-check list-icon"></i> Excellent job prospects</li>
            </ul>
          </div>
          
          <div id="menu33" class="tab-pane fade">
            <h3>EXPENSES TO STUDY IN Germany</h3>
          
            <div class="col-md-6">
               <p>Education in public schools in Germany is free, only private schools and colleges may have costs attached to it. Accommodation in Germany costs from around EUR 400- 500 per month. But scholarships are available on merit for deserving candidates.</p>
            </div>
            
          </div>
          
        </div>
      </div>
      <div class="col-md-4 visible-lg visible-md">
        <div class="text-center">
          <h2 class="underline visible-md visible-lg">Study In</h2>
          <h5 class="text-center visible-xs visible-sm" >Study In</h5>
          <img src="https://www.imperial-overseas.com/images/seminar2022/UK/Germany.jpg" alt="universities in germany for international students" >
          <div>
          <div class="row visible-md visible-lg" style="text-align: center; margin-top: 10px"> <a class="btn btn-primary btn-block" id="Registerbutton10">Register Now!</a> </div>
        
        </div>
          <p class="sub-heading fntMd"></p>
        </div>
      </div>
    </div>
    <div class="col-md-12 countryDiv">
      <div class="col-md-4 col-sm-12 col-xs-12">
        <div class="text-center">
          <h2 class="underline visible-md visible-lg">Study In</h2>
          <h5 class="text-center visible-xs visible-sm" >Study In</h5>
          <img src="https://www.imperial-overseas.com/images/seminar2022/UK/Australia.jpg" alt="australian universities" >
          <div>
            <div class="row visible-md visible-lg" style="text-align: center; margin-top: 10px"> <a class="btn btn-primary btn-block" id="Registerbutton8">Register Now!</a> </div>
            
          </div>
          <p class="sub-heading fntMd"></p>
        </div>
      </div>
      <div class="col-md-8">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#home1">About Australia</a></li>
          <li><a data-toggle="tab" href="#menu11">SALIENT FEATURES</a></li>
          <li><a data-toggle="tab" href="#menu13">EXPENSES</a></li><!-- 
          <li><a data-toggle="tab" href="#menu14">SCHOLARSHIP</a></li> -->
        </ul>
        <div class="tab-content" style="max-height: 500px;overflow: auto;">
          <div id="home1" class="tab-pane fade in active">
            <h3>About Australia</h3>
            <p> The QS Best Student Cities has consistently ranked seven Australian cities amongst the top Best Student Cities in the world, which includes Melbourne, Sydney, Canberra, Brisbane, Adelaide, Perth and the Gold Coast.Summers are from December to February and the maximum temperature rises to 38 ºC</p>

          </div>
          <div id="menu11" class="tab-pane fade">
            <h3>SALIENT FEATURES TO STUDY Australia</h3>
            <ul>
              <li><i class="fa fa-check list-icon"></i> Work-integrated learning- projects, simulation, case-studies</li>
              <li><i class="fa fa-check list-icon"></i> Internships- part of the curriculum</li>
              <li><i class="fa fa-check list-icon"></i> Can work full-time while studying</li>
              <li><i class="fa fa-check list-icon"></i> 2-year study- gives ample time for students to learn and work alongside</li>
              <li><i class="fa fa-check list-icon"></i> World Class Cities – Sydney/Melbourne/ Brisbane/Perth.</li>
              <li><i class="fa fa-check list-icon"></i> Easy PR opportunities</li>
              <li><i class="fa fa-check list-icon"></i> Safe, friendly environment</li>
              <li><i class="fa fa-check list-icon"></i> 15 Years of education is Accepted.</li>
              <li><i class="fa fa-check list-icon"></i> Great weather! </li>
            </ul>
          </div>
          
          <div id="menu13" class="tab-pane fade">
            <h3>EXPENSES TO STUDY IN AUSTRALIA</h3>
            
            <ul>
              <li><i class="fa fa-check list-icon"></i> <b>Undergraduate:</b>  Rs. 60-70 Lakhs</li>
              <li><i class="fa fa-check list-icon"></i> <b>PG Diploma:</b>  Rs. 35-40 Lakhs</li>
            </ul>
          </div>
          <!-- <div id="menu14" class="tab-pane fade">
            <h3>VARIOUS SCHOLARSHIP OPPORTUNITIES ARE AVAILABLE ON</h3>
            <ul class="tagscolor">
              <li><i class="fa fa-check list-icon"></i> Scholarship Search: <a href="http://www.scholarship-search.org.uk/" target="_blank">www.scholarship-search.org.uk/</a></li>
              <li><i class="fa fa-check list-icon"></i> Education UK: <a href="http://www.educationuk.org/global/articles/scholarships-financial-support/" target="_blank">www.educationuk.org/global/articles/scholarships-financial-support/</a></li>
              <li><i class="fa fa-check list-icon"></i> The British Council: <a href="http://www.britishcouncil.org/" target="_blank">www.britishcouncil.org/</a></li>
            </ul>
          </div> -->
          
        </div>
      </div>            
    </div>
    <div class="col-md-12 countryDiv">
      <div class="col-md-12 visible-xs visible-sm">
        <div class="text-center">
          <h2 class="underline visible-md visible-lg">Study In</h2>
          <h5 class="text-center visible-xs visible-sm" >Study In</h5>
          <img src="https://www.imperial-overseas.com/images/seminar2022/UK/NZ.jpg" alt=" study in new zealand" >
          <div>
            <div class="row visible-xs visible-sm" style="text-align: center;"> <a class="btn btn-primary btn-block" id="RegisterbuttonSmall9">Register Now!</a> </div>
          </div>
          <p class="sub-heading fntMd"></p>
        </div>
      </div>
      <div class="col-md-8">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#home2">About New Zealand</a></li>
          <li><a data-toggle="tab" href="#menu21">SALIENT FEATURES</a></li>
          <li><a data-toggle="tab" href="#menu23">EXPENSES</a></li>
        </ul>
        <div class="tab-content" style="max-height: 500px;overflow: auto;">
          <div id="home2" class="tab-pane fade in active">
            <h3>About New Zealand</h3>
            <p>New Zealand is a young country where independence, initiative and resourcefulness are more highly regarded than status or rules. As a student here you'll be encouraged to be questioning, flexible and to seek your own answers by thinking for yourself.</p>

          </div>
          <div id="menu21" class="tab-pane fade">
            <h3>SALIENT FEATURES TO STUDY New Zealand</h3>
            <ul>
              <li><i class="fa fa-check list-icon"></i> Multiple courses and at different level is offered like: Bachelor, Graduate Diploma, PG Diploma, Masters and PhD</li>
              <li><i class="fa fa-check list-icon"></i> IFees are economical as compare to other countries</li>
              <li><i class="fa fa-check list-icon"></i> Scholarships options available</li>
              <li><i class="fa fa-check list-icon"></i> PR friendly country-You can apply for a visa to work in New Zealand for up to 3 years after you finish your study</li>
              <li><i class="fa fa-check list-icon"></i> Ranked no. 3 in the world to settle down</li>
              <li><i class="fa fa-check list-icon"></i> Great scope for PhD scholar</li>
              <li><i class="fa fa-check list-icon"></i> Safe and peaceful</li>
              <li><i class="fa fa-check list-icon"></i> Quality of life</li>
              <li><i class="fa fa-check list-icon"></i> You can work to support your studies </li>
            </ul>
          </div>
          
          <div id="menu23" class="tab-pane fade">
            <h3>EXPENSES TO STUDY IN NEW ZEALAND</h3>
            

            <div class="col-md-6">
               <table class="table table-bordered">
                  <tbody>
                     <tr>
                        <th>
                           Diplomas/Certificates 
                        </th>
                        <td>
                           <span class="vc_table_content">NZ$ 10000 to NZ$ 15000 per year</span>
                        </td>
                     </tr>
                     <tr>
                        <th>
                           Undergraduate
                        </th>
                        <td>
                           NZ$ 18000 to NZ$ 32000 per year
                        </td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <div class="col-md-6">
               <table class="table table-bordered">
                  <tbody>
                     <tr>
                        <th>Postgraduate</th>
                        <td>NZ$ 20000 to NZ$ 37000 per year</td>
                     </tr>
                     <tr>
                        <th>PhD</th>
                        <td>NZ$ 6500 to NZ$ 9000</td>
                     </tr>
                  </tbody>
               </table>
               <div class="clearfix"></div>
            </div>
            
          </div>
          
          
        </div>
      </div>
      <div class="col-md-4 visible-lg visible-md">
        <div class="text-center">
          <h2 class="underline visible-md visible-lg">Study In</h2>
          <h5 class="text-center visible-xs visible-sm" >Study In</h5>
          <img src="https://www.imperial-overseas.com/images/seminar2022/UK/NZ.jpg" alt=" study in new zealand" >
          <div>
          <div class="row visible-md visible-lg" style="text-align: center; margin-top: 10px"> <a class="btn btn-primary btn-block" id="Registerbutton10">Register Now!</a> </div>
        
        </div>
          <p class="sub-heading fntMd"></p>
        </div>
      </div>
    </div>
    <div class="col-md-12 countryDiv">
      <div class="col-md-4 col-sm-12 col-xs-12">
        <div class="text-center">
          <h2 class="underline visible-md visible-lg">Study In</h2>
          <h5 class="text-center visible-xs visible-sm" >Study In</h5>
          <img src="https://www.imperial-overseas.com/images/seminar2022/UK/Ireland.jpg" alt="top universities in ireland" >
          <div>

          <div class="row visible-md visible-lg" style="text-align: center; margin-top: 10px"> <a class="btn btn-primary btn-block" id="Registerbutton15">Register Now!</a> </div>
          <div class="row visible-xs visible-sm" style="text-align: center;"> <a class="btn btn-primary btn-block" id="RegisterbuttonSmall5">Register Now!</a> </div>
        </div>
          <p class="sub-heading fntMd"></p>
        </div>
      </div>
      <div class="col-md-8">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#homeI1">About IRELAND</a></li>
          <li><a data-toggle="tab" href="#menuI12">SALIENT FEATURES</a></li>
          <li><a data-toggle="tab" href="#menuI13">EXPENSES</a></li>
        </ul>
        <div class="tab-content" style="max-height: 500px;overflow: auto;">
          <div id="homeI1" class="tab-pane fade in active">
            <h3>About IRELAND</h3>
            <p>The Republic of Ireland is separated from the UK by the North Channel and has been recently considered as one of the top destinations for students. It is not to be confused with Northern Ireland, a part of the United Kingdom although they used to be one country in the past. They are the second highest consumers of beer and the local language of Irish is now spoken by only 10% of the total population, and the rest speak English.</p>

          </div>
          <div id="menuI12" class="tab-pane fade">
            <h3>SALIENT FEATURES TO STUDY IRELAND</h3>
            <ul>
              <li><i class="fa fa-check list-icon"></i> Irish Education System - Extensive choice of courses & universities</li>
              <li><i class="fa fa-check list-icon"></i> There are 22 world-class universities in Ireland</li>
              <li><i class="fa fa-check list-icon"></i> All universities are in the top 3% of universities in the world</li>
              <li><i class="fa fa-check list-icon"></i> 1 year MSc followed by 2 years of PSW visa</li>
              <li><i class="fa fa-check list-icon"></i> 15 Years of Education Accepted</li>
              <li><i class="fa fa-check list-icon"></i> Highly Affordable Education</li>
              <li><i class="fa fa-check list-icon"></i> Easy Visa & PR process</li>
              <li><i class="fa fa-check list-icon"></i> Part-time work is allowed</li>
              <li><i class="fa fa-check list-icon"></i> Home to European Headquarters for many of Top 10 companies - Google, HP, Apple, IBM, Facebook, Linkedin, Twitter, Pfizer, GSK and Genzyme </li>
            </ul>
          </div>
          
          <div id="menuI13" class="tab-pane fade">
            <h3>EXPENSES TO STUDY IN IRELAND</h3>
          
            <div class="col-md-6">
               <table class="table table-bordered">
                  <tbody>
                     <tr>
                        <th>
                           Undergraduate
                        </th>
                        <td>
                           Euro 9000 - Euro 12000 per year
                        </td>
                     </tr>
                     <tr>
                        <th>
                           Graduate
                        </th>
                        <td>
                           10000 - Euro 15000 per year
                        </td>
                     </tr>
                  </tbody>
               </table>
               <br>
               <div class="clearfix"></div>
            </div>
            
          </div>
          
        </div>
      </div>
      
    </div>
  </div>
</section>
<section class="content-section">
  <div class="callToAction footer-bar-3 hidden-xs" style="background: #815dd5;">
    <div class="container">
      <div class="row">
        <div class="col-md-8 pt10">
          <h5 style="color: white;">Get Admission in Top Universities anywehre in the World !</h5> </div>
        <div class="col-md-3">
          <div class="row visible-md visible-lg" style="text-align: center;"> <a class="btn btn-primary btn-block" id="Registerbuttonlast">Register Now!</a> </div>
          <div class="row visible-xs visible-sm" style="text-align: center;"> <a class="btn btn-primary btn-block" id="RegisterbuttonSmalllast">Register Now!</a> </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="content-section" id="Panelist">
  <div class="container-fluid">
    <div class="col-md-12">
      <div class="col-md-12 section-header text-center  visible-md visible-lg" >
        <h1 class="underline" style="margin-bottom: 5px !important;">STUDENTS WE PLACED IN</h1> 
      </div>
      <div class="text-center visible-xs visible-sm">
        <h3 class="underline">STUDENTS WE PLACED IN</h3>
      </div>
    </div>
    <div class="col-md-12">
      <div class="bubble-wrap">
        <div class="bubbles"></div>
      </div>
    </div>
   
  </div>
  <!-- end container -->
</section>
<section class="content-section">
    <div class="col-md-12 section-header text-center visible-md visible-lg">
      <h1 class="underline" >Our History</h1> 
    </div>
    <div class="col-md-12 text-center visible-xs visible-sm">
      <h5 class="underline" >Our History</h5> 
    </div>
    <img src="<?php echo base_url();?>images/OurJourney.jpg" class="img-responsive imageFlow" /> 
</section>
<section class="testimonials thatDiv" style="background-color: white;" id="testimonials">
  <div class="container">
    
    <div class="col-md-12 section-header text-center visible-md visible-lg">
      <h2 class="underline" >Testimonials</h2> 
    </div>
    <div class="col-md-12 text-center visible-xs visible-sm">
      <h5 class="underline" >Testimonials</h5> 
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div id="customers-testimonials" class="owl-carousel">
          <!--TESTIMONIAL 1 -->
          <div class="item">
            <div class="shadow-effect"> 
              <div>
                <h3>Shubhankar Mulay</h3>
                <p class="smallP">MSc Sports Management, <br>University of Stirling, <br>Sep 2022</p>
              </div>
              <p>My experience with Imperial started when a friend suggested me to visit them, & it turned out to literally be a life changing experience. The Imperial family helped me with everything; writing a proper statement of purpose, narrowing down my options of courses, figuring out the best universities, and applying to all the relevant ones. Thank you to everyone, especially Saloni ma'am who was with me throughout every step. I would 100% suggest Imperial Overseas Education Consultants to anyone who has a desire to go abroad for higher studies.</p>
            </div>
            
          </div>
          <!--END OF TESTIMONIAL 1 -->
          <!--TESTIMONIAL 1 -->
          <div class="item">
            <div class="shadow-effect"> 
              <div>
                <h3>Sagar Badlani</h3>
                <p class="smallP">GRE SCORE: 332  (Verbal - 162 , Quant - 170 , AWA - 4.5)<br>Georgia Institute of Technology<br>USA, Fall 2022</p>
              </div>
              <p>"A moment of serendipity is a wink from destiny." For me, this moment was the day I chose Imperial. The entire team has been impeccable with their assistance and guidance. I would always doubt myself but Hiren Sir would reassure me that I would get into a good college. Thank you, sir, for solving the tiniest of questions I had. Sanhita ma'am was a massive help during my visa process and finances. Shilpa ma'am and Shagufta ma'am ensured I faced no obstacles during my time at Imperial Santacruz. I'm Still smiling! Thank you, “Team Imperial”, for helping me achieve my dreams.</p>
            </div>
            
          </div>
          <!--END OF TESTIMONIAL 1 -->
          <!--TESTIMONIAL 3 -->
          <div class="item">
            <div class="shadow-effect"> 
              <div>
                <h3>Eshan Venugopal</h3>
                <p class="smallP">GRE SCORE: 324 (Verbal - 161 , Quant - 168 , AWA - 4)<br>New York University<br>USA, INTAKE Fall 2022</p>
              </div>
              <p>It has been a lovely experience with Imperial Overseas. Honestly, the entire process of applying to the universities abroad, right from giving exams, selecting the universities, application, visa process is a bit tedious but with the guidance of HIren sir, Shilpa ma'am, Sonal Sonal ma'am and Shagufta ma'am, it never felt that way. Without Imperial, I wouldn't have gotten the admit from NYU, as they had the fee waiver code. LOL!! Overall, it has been a lovely experience here and I would recommend Imperial 100%</p>
            </div>
          </div>
          <!--END OF TESTIMONIAL 3 -->
          
          <!--TESTIMONIAL 2 -->
          <div class="item">
            <div class="shadow-effect"> 
              <div>
                <h3>Sanskruti Manepatil</h3>
                <p class="smallP">MA Design Managemenr, <br>Coventry University, Coventry, <br>Jan 2022</p>
              </div>
              <p>Finally I have landed to my dreamland. Imperial made my dream come true, firstly I was trying out for US but when I started losing hopes there Saloni mam came up like an angel and she showed my right path to choose UK. She was all along in this whole journey there were too many hurdles to cross but Saloni mam hold my hand like a kid and helped my cross the horizon.Today I am studying masters in illustrator and animation at Coventry University and I am very happy to study here. It feels complete.
              </p>
            </div>
          </div>
          <!--END OF TESTIMONIAL 2 -->
          <!--TESTIMONIAL 3 -->
          <div class="item">
            <div class="shadow-effect"> 
              <div>
                <h3>Parth Shah</h3>
                <p class="smallP">Unitec- New Zealand<br>Master of Applied Business<br>Major Business Analytics</p>
              </div>
              <p>I just got my visa for New Zealand. I would like to thank Katie mam, she is one of the best counsellor I have ever came across. I have been a tough student for her constantly irritating her for documents and visa process, she has never backed out she was available for 24*7 and very friendly. The way she helped me and guided to get the visa it was amazing. I would again thank katie mam and imperial overseas for guiding and showing the right path to get visa. My best wishes to katie mam and her staff.I have no words to explain so i thank her from the bottom of my heart.</p>
            </div>
          </div>
          <!--END OF TESTIMONIAL 3 -->
          <!--TESTIMONIAL 4 -->
          <div class="item">
            <div class="shadow-effect"> 
              <div>
                <h3>Akash dhavade</h3>
                <p class="smallP">Dublin City University<br>MSc Computer science Major Data science<br>Ireland 2022</p>
              </div>
              <p>I reached out to imperial seeking help for my higher studies. Imperial counsellor Katie ma’am cleared all my doubts and pointed me to the right direction. It has been a smooth and joyful experience working with the imperial team, all questions were answered. I aimed for few universities in Ireland and got one of the best university in Ireland for 2022 intake , and i got it with the right guidance of katie ma’am and due to all the efforts of imperial team,Today I received my student visa. Recommend Imperial Overseas Education consultancy highly . :)</p>
            </div>
          </div>
          <!--END OF TESTIMONIAL 4 -->
          <!--TESTIMONIAL 5 -->
          <div class="item">
            <div class="shadow-effect"> 
              <div>
                <h3>Fiona Rodrigues</h3>
                <p class="smallP">RMIT Australia<br>Master of Business Marketing July 2022</p>
              </div>
              <p>I had a great experience with Imperial Overseas Education Consultancy who helped me on my smooth journey to apply for my master’s program in Australia for the July 2022 Intake. Special shout out to my agent Katie D’Souza who was supportive through all the processes right from the start till the end as it can be stressful given the uncertain waiting periods. No matter how many times I called her, she would be available and answer my queries. Although I was anxious throughout the process, she was there to encourage and reassure me that everything would be fine. Thank you so much! 😊</p>
            </div>
          </div>
          <!--END OF TESTIMONIAL 5 -->
          <div class="item">
            <div class="shadow-effect"> 
              <div>
                <h3>Luvneet Nitin Utchil</h3>
                <p class="smallP">The University of Sheffield<br>MA Urban Design and Planning<br>September 2022</p>
              </div>
              <p>Exactly 9 months back i had met the imperial team. They made it very much comfortable for me with all my admission process even though when i had my academics going on. Same level of kindness, importance, sincerity i could see throughout this time line. Today i got my visa and ready to move aborad. I would like to thank Sunil sir, Vaidehi ma’am and the whole Imperial thane team for their cooperation and support. 😊</p>
            </div>
          </div>
          <div class="item">
            <div class="shadow-effect"> 
              <div>
                <h3>Vashishtha Joshi</h3>
                <p class="smallP">Georgian College , Canada<br>Sept 2022</p>
              </div>
              <p>I always thought that the Canada Visa application process would be full of anxiety and fear of missing out on some important document. That's where Imperial Overseas played a significant part for me. Apart from being updated with the latest Visa guidelines & recent changes, Imperial made my application process very smooth & structured. Special thanks to Swapnil Sir for solving every doubt and always being there at every stage of the process.</p>
            </div>
          </div>
          <div class="item">
            <div class="shadow-effect"> 
              <div>
                <h3>Lisenda Dmello</h3>
                <p class="smallP">Lambton College Canada<br>Jan 2023</p>
              </div>
              <p> I want to express my gratitude to the entire team of Imperial Overseas for making my dream of studying abroad a reality. I’d like to especially thank Mr. Swapnil Karwal for guiding me through the entire process starting from appropriate course selection till Visa acceptance, he was extremely efficient and helped me with my queries. I would recommend getting in touch with Imperial Overseas when you are looking into studying abroad, because they have excellent counsellors who’ll provide you the right guidance required.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container" style="margin-top: 2%;">
    <div class="row" >
      <div class="col-md-6 col-sm-12 col-xs-12">
         <iframe width="95%" height="300" src="https://www.youtube.com/embed/OWB1WO2QwJc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      <div class="col-md-6 col-sm-12 col-xs-12">
        <iframe width="95%" height="300" src="https://www.youtube.com/embed/QSQMtryyihE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
  </div>
  <div class="container" style="margin-top: 2%;">
    <div class="row" >
      <div class="col-md-6 col-sm-12 col-xs-12">
         <iframe width="95%" height="300" src="https://www.youtube.com/embed/XGyM8H2mOCc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      <div class="col-md-6 col-sm-12 col-xs-12">
        <iframe width="95%" height="300" src="https://www.youtube.com/embed/jRogqNciXn4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
    </div>
</section>

<section class="content-section">
  <div class="callToAction footer-bar-3 hidden-xs">
    <div class="container">
      <div class="row">
        <div class="col-md-8 pt10">
          <h5 style="color: white;">Meet representatives from over 20+ Universities globally !</h5> </div>
        <div class="col-md-3">
          <div class="row visible-md visible-lg" style="text-align: center;"> <a class="btn btn-primary btn-block" id="Registerbutton13">Register Now!</a> </div>
          <div class="row visible-xs visible-sm" style="text-align: center;"> <a class="btn btn-primary btn-block" id="RegisterbuttonSmall13">Register Now!</a> </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="loader"></div>

<script src="https://www.imperial-overseas.com/scripts/vendor.js"></script>
<script src="https://www.imperial-overseas.com/scripts/main.js"></script>
<script src="https://www.imperial-overseas.com/scripts/plugins.js"></script>
<script src="https://cdn.rawgit.com/josephg/noisejs/master/perlin.js"></script>
<script type="text/javascript">
 

const SCROLL_SPEED = 1.5;
const NOISE_SPEED = 0.008;
const NOISE_AMOUNT = 5;
const CANVAS_WIDTH = 3600;

const bubblesEl = document.querySelector(".bubbles");
const bubbleSpecs = [
  { s: 0.6, x: 1134, y: 45 },
  { s: 0.6, x: 1620, y: 271 },
  { s: 0.6, x: 1761, y: 372 },
  { s: 0.6, x: 2499, y: 79 },
  { s: 0.6, x: 2704, y: 334 },
  { s: 0.6, x: 2271, y: 356 },
  { s: 0.6, x: 795, y: 226 },
  { s: 0.6, x: 276, y: 256 },
  { s: 0.6, x: 1210, y: 365 },
  { s: 0.6, x: 444, y: 193 },
  { s: 0.6, x: 2545, y: 387 },
  { s: 0.8, x: 1303, y: 193 },
  { s: 0.8, x: 907, y: 88 },
  { s: 0.8, x: 633, y: 320 },
  { s: 0.8, x: 323, y: 60 },
  { s: 0.8, x: 129, y: 357 },
  { s: 0.8, x: 1440, y: 342 },
  { s: 0.8, x: 1929, y: 293 },
  { s: 0.8, x: 2135, y: 198 },
  { s: 0.8, x: 2276, y: 82 },
  { s: 0.8, x: 2654, y: 182 },
  { s: 0.8, x: 2783, y: 60 },
  { x: 1519, y: 118 },
  { x: 1071, y: 233 },
  { x: 1773, y: 148 },
  { x: 2098, y: 385 },
  { x: 2423, y: 244 },
  { x: 901, y: 385 },
  { x: 624, y: 111 },
  { x: 75, y: 103 },
  { x: 413, y: 367 },
  { x: 2895, y: 271 },
  { x: 1990, y: 75 },
  { x: 3160, y: 225 },
  { s: 0.6, x: 3000, y: 100 },
  { s: 0.6, x: 3180, y: 50 },
  { s: 0.6, x: 3060, y: 400 },
  { x: 3260, y: 400 },
  { s: 0.8, x: 3380, y: 90 },
  { x: 3500, y: 300 },
  { x: 3600, y: 75 },
  { s: 0.6, x: 3725, y: 220 },
  { s: 0.8, x: 3700, y: 400 }
  // { x: 3900, y: 85 }
];

class Bubbles {
  constructor(specs) {
    this.bubbles = [];

    specs.forEach((spec, index) => {
      this.bubbles.push(new Bubble(index, spec));
    });

    requestAnimationFrame(this.update.bind(this));
  }

  update() {
    this.bubbles.forEach((bubble) => bubble.update());
    this.raf = requestAnimationFrame(this.update.bind(this));
  }
}

class Bubble {
  constructor(index, { x, y, s = 1 }) {
    this.index = index;
    this.x = x;
    this.y = y;
    this.scale = s;

    this.noiseSeedX = Math.floor(Math.random() * 64000);
    this.noiseSeedY = Math.floor(Math.random() * 64000);

    this.el = document.createElement("div");
    this.el.className = `bubble logo${this.index + 1}`;
    bubblesEl.appendChild(this.el);
  }

  update() {
    this.noiseSeedX += NOISE_SPEED;
    this.noiseSeedY += NOISE_SPEED;
    let randomX = noise.simplex2(this.noiseSeedX, 0);
    let randomY = noise.simplex2(this.noiseSeedY, 0);

    this.x -= SCROLL_SPEED;
    this.xWithNoise = this.x + randomX * NOISE_AMOUNT;
    this.yWithNoise = this.y + randomY * NOISE_AMOUNT;

    if (this.x < -200) {
      this.x = CANVAS_WIDTH;
    }

    this.el.style.transform = `translate(${this.xWithNoise}px, ${this.yWithNoise}px) scale(${this.scale})`;
  }
}

// For perlin noise
noise.seed(Math.floor(Math.random() * 6000));

const bubbles = new Bubbles(bubbleSpecs);

 jQuery(document).ready(function($) {
            "use strict";
            //  TESTIMONIALS CAROUSEL HOOK
            $('#customers-testimonials').owlCarousel({
              loop: true,
              center: true,
              items: 3,
              margin: 0,
              autoplay: true,
              dots: true,
              autoplayTimeout: 7500,
              smartSpeed: 400,
              responsive: {
                0: {
                  items: 1
                },
                768: {
                  items: 2
                },
                1170: {
                  items: 3
                }
              }
            });
          });

$("#Registerbutton").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterForm").offset().top
            }, 2000);
          });
          $("#RegisterbuttonSmall").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterFormSmall").offset().top
            }, 2000);
          });
          $("#Registerbutton1").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterForm").offset().top
            }, 2000);
          });
          $("#RegisterbuttonSmall1").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterFormSmall").offset().top
            }, 2000);
          });
          $("#Registerbutton2").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterForm").offset().top
            }, 2000);
          });
          $("#RegisterbuttonSmall2").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterFormSmall").offset().top
            }, 2000);
          });
          $("#Registerbutton3").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterForm").offset().top
            }, 2000);
          });
          $("#RegisterbuttonSmall3").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterFormSmall").offset().top
            }, 2000);
          });
          $("#Registerbutton4").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterForm").offset().top
            }, 2000);
          });
          $("#RegisterbuttonSmall4").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterFormSmall").offset().top
            }, 2000);
          });
          $("#Registerbutton5").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterForm").offset().top
            }, 2000);
          });
          $("#RegisterbuttonSmall5").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterFormSmall").offset().top
            }, 2000);
          });
          $("#Registerbutton6").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterForm").offset().top
            }, 2000);
          });
          $("#RegisterbuttonSmall6").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterFormSmall").offset().top
            }, 2000);
          });
          $("#Registerbutton7").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterForm").offset().top
            }, 2000);
          });
          $("#RegisterbuttonSmall7").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterFormSmall").offset().top
            }, 2000);
          });
          $("#Registerbutton8").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterForm").offset().top
            }, 2000);
          });
          $("#RegisterbuttonSmall8").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterFormSmall").offset().top
            }, 2000);
          });
          $("#Registerbutton9").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterForm").offset().top
            }, 2000);
          });
          $("#RegisterbuttonSmall9").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterFormSmall").offset().top
            }, 2000);
          });
          $("#Registerbutton10").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterForm").offset().top
            }, 2000);
          });
          $("#RegisterbuttonSmall10").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterFormSmall").offset().top
            }, 2000);
          });
          $("#Registerbutton11").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterForm").offset().top
            }, 2000);
          });
          $("#RegisterbuttonSmall11").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterFormSmall").offset().top
            }, 2000);
          });
          $("#Registerbutton12").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterForm").offset().top
            }, 2000);
          });
          $("#RegisterbuttonSmall12").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterFormSmall").offset().top
            }, 2000);
          });
          $("#Registerbutton13").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterForm").offset().top
            }, 2000);
          });
          $("#RegisterbuttonSmall13").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterFormSmall").offset().top
            }, 2000);
          });
          $("#Registerbutton14").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterForm").offset().top
            }, 2000);
          });
          $("#RegisterbuttonSmall14").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterFormSmall").offset().top
            }, 2000);
          });
          $("#Registerbutton15").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterForm").offset().top
            }, 2000);
          });
          $("#RegisterbuttonSmall15").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterFormSmall").offset().top
            }, 2000);
          });
          $("#Registerbuttonlast").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterForm").offset().top
            }, 2000);
          });
          $("#RegisterbuttonSmalllast").click(function() {
            $('html, body').animate({
              scrollTop: $("#RegisterFormSmall").offset().top
            }, 2000);
          });

           $(document).ready(function() {
            function smoothScroll(target) {
              const headerHeight = $("#main-nav").outerHeight() + 10;
              $("html,body").animate({
                "scrollTop": target.offset().top - headerHeight
              }, 550);
            }
            $("#main-nav ul li a").on("click", function(event) {
              if(this.getAttribute("href").charAt(0) == "#") {
                event.preventDefault();
                smoothScroll($(this.hash));
              } else {
                //just let link work
              }
            });
            $(window).scroll(function() {
              let scrollPos = $(this).scrollTop();
              let scrollDistance = scrollPos + 130;
              $("#main-nav ul li a[href^='#']").each(function() {
                let currLink = $(this);
                let refElement = $(currLink.attr("href"));
                if(refElement.position().top <= scrollDistance && refElement.position().top + refElement.height() > scrollDistance) {
                  $("#main-nav ul li a").removeClass("active");
                  currLink.addClass("active");
                } else {
                  currLink.removeClass("active");
                }
              });
            });
          });
          $(document).ready(function() {
            function smoothScroll(target) {
              const headerHeight = $("#small-nav").outerHeight() + 20;
              $("html,body").animate({
                "scrollTop": target.offset().top - headerHeight
              }, 550);
            }
            $("#small-nav ul li a").on("click", function(event) {
              if(this.getAttribute("href").charAt(0) == "#") {
                event.preventDefault();
                smoothScroll($(this.hash));
              } else {
                //just let link work
              }
            });
            $(window).scroll(function() {
              let scrollPos = $(this).scrollTop();
              let scrollDistance = scrollPos + 130;
              $("#small-nav ul li a[href^='#']").each(function() {
                let currLink = $(this);
                let refElement = $(currLink.attr("href"));
                if(refElement.position().top <= scrollDistance && refElement.position().top + refElement.height() > scrollDistance) {
                  $("#small-nav ul li a").removeClass("active");
                  currLink.addClass("active");
                } else {
                  currLink.removeClass("active");
                }
              });
            });
          });

var spinner = $('#loader');
function earlybirdRegistrationForm(){

      var user_first_name = $('#student_name').val();
      var user_email = $('#student_email').val();
      var user_mobile = $('#student_phone').val();
      var user_engtest = $('#English_Test').val();
      var user_engtestscore = $('#English_Test_Score').val();
      var user_GRE_Score = $('#GRE_Score').val();
      var user_required_country = $('#student_required_country').val();
      var user_required_degree = $('#student_required_degree').val();
      var user_intake = $('#student_intake').val();


      var dataString = 'first_name=' + user_first_name + '&user_email=' + user_email
      + '&user_mobile=' + user_mobile + '&user_engtest=' + user_engtest + '&user_engtestscore=' + user_engtestscore
      + '&user_GRE_Score=' + user_GRE_Score + '&user_required_country=' + user_required_country + '&user_required_degree=' + user_required_degree + '&user_intake=' + user_intake;

      event.preventDefault();
      spinner.show();
      $.ajax({

      type: "POST",

      url: "/EarlybirdRegistration/register",

      data: dataString,

      cache: false,

      success: function(result){

        if(result == 'SUCCESS'){
            spinner.hide();

            alert("Congratulation!! You have successfully registered.");
            window.location.reload();
        }
        else{
            alert(result['message']);
        }

       }

          });

    }

</script>
