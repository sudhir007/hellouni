<!-- Add the slick-theme.css if you want default styling -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<!-- Add the slick-theme.css if you want default styling -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
<style>
   #errmsg{margin-left:10px; color:red;}
   .shade_border{
   background:url(img/shade1.png) repeat-x; height:11px;
   }
   .uni{
   background-color:#f4f4f4; padding:0px; margin:10px;
   color:#161616;
   }
   .uni p{
   font-size: 11px;
   margin:5px;
   }
   .favourites{
   color:#fff;  background-color:#394553; float:left; margin-left:1px; padding:6px; font-size:11px;
   }
   .compare{
   color:#fff; width:118px; background-color:#394553; float:left; margin-right:1px; padding:6px;font-size:11px;
   }
   .search_form
   {
   border:solid 2px #ededed; background:#f6f6f6; padding-bottom:15px;
   }
   .detail{
   background:#f6881f; text-align:center; padding-top:6px; padding-left:11px; height:40px; color:#fff;
   }
   .my-search{
   font-size: 20px;
   font-weight: 600;
   margin-bottom: 10px;
   }
   .my-search-box, .my-search-box div{
   padding: 0px;
   }
   .my-search-box-heading{
   font-size: 16px;
   font-weight: bold;
   }
   .my-search-box-text{
   font-size: 14px;
   }
   .sort-filter {
   font-size: 16px;
   line-height: 1.6;
   margin:0 0 10px;
   color:#000;
   }
   .font-11 {
   font-size: 11px;
   }
   .font-16 {
   font-size: 16px !important;
   font-weight: 700 !important;
   letter-spacing: 0.5px !important;
   color: Black !important;
   }
   .uni-image {
   background: #fff;
   border: 1px solid #f3f3f3;
   padding: 5px;
   position: relative;
   width: 184px;
   }
   ul.uni-cont li {
   background: #fbfbfb;
   border: 1px solid #f3f3f3;
   margin-bottom: 8px;
   list-style: none;
   }
   .clearwidth {
   width: 100%;
   float: left;
   }
   .univ-tab-details {
   margin: 30px 0 10px 0!important;
   }
   .flLt {
   float: left;
   }
   .flRt {
   float: right;
   }
   .uni-box {
   float: left;
   width: 100%;
   padding: 8px;
   position: relative;
   background-color: #fbfbfb;
   /* border: 1px solid #f0f0f0; */
   box-shadow: 2px 2px 4px #ccccc4;
   margin: 20px 0px;
   }
   .uni-box p {
   margin:0px;
   line-height:unset;
   }
   .uni-detail {
   margin-left: 208px;
   }
   .uni-title {
   margin-bottom: 10px;
   width: 90%;
   }
   .uni-title a {
   font-weight: 600;
   font-size: 13px;
   color: #111111;
   }
   .uni-title span {
   color: #666666;
   }
   .uni-sub-title {
   font-weight: 600;
   font-size: 15px;
   color:#666666;
   }
   a.uni-sub-title {
   color:#666666;
   }
   .uni-course-details {
   width: 100%;
   border-right: 1px solid #dadada;
   background: #fff;
   /*margin: 8px 0;*/
   padding: 8px;
   position: relative;
   /*margin-right: 25px;*/
   }
   .uni-course-details:before {
   width: 100%;
   height: 1px;
   right: -1px;
   bottom: -1px;
   background-image: -webkit-gradient(linear,0 100%,0 0,from(#fff),to(#dadada));
   background-image: -webkit-linear-gradient(right,#dadada,#fff);
   background-image: -moz-linear-gradient(right,#dadada,#fff);
   background-image: -o-linear-gradient(#fff,#dadada);
   }
   .uni-course-details:after, .uni-course-details:before {
   content: "";
   position: absolute;
   }
   .detail-col {
   width: 145px;
   font-size: 13px;
   color: #111111;
   }
   .detail-col p {
   margin-bottom: 3px;
   }
   .detail-col strong {
   color: #666666;
   margin-bottom: 5px;
   display: block;
   font-weight: 400;
   }
   .btn-brochure a {
   padding: 8px 18px 9px!important;
   }
   a.button-style {
   padding: 6px 9px;
   }
   .compare-box {
   font-size: 14px;
   color: #000;
   width: 140px;
   line-height: 15px;
   }
   .compare-box p{
   display:inline;
   margin:0px;
   }
   div {
   background: 0 0;
   border: 0;
   margin: 0;
   outline: 0;
   padding: 0;
   vertical-align: baseline;
   }
   .sponsered-text {
   color: #999;
   font-size: 12px;
   font-style: italic;
   margin: 15px 0 5px 0;
   display: none;
   }
   .tar {
   text-align: right;
   }
   .button-style, .login-btn, a.button-style, a.login-btn {
   background: #F6881F;
   border: 1px solid transparent;
   padding: 5px 10px;
   color: #fff;
   font-size: 14px;
   text-decoration: none;
   vertical-align: middle;
   display: inline-block;
   }
   .add-shortlist, .added-shortlist {
   background-position: 0 -51px;
   width: 78px;
   height: 77px;
   position: absolute;
   top: 0;
   right: 0px;
   cursor: pointer;
   }
   .cate-sprite {
   background: url(/images/save-fav-btn-white.png);
   display: inline-block;
   font-style: none;
   vertical-align: middle;
   }
   .saved-fav {
   background: url(/images/save-fav-btn.png);
   }
   /* The Modal (background) */
   .modal {
   display: none; /* Hidden by default */
   position: fixed; /* Stay in place */
   z-index: 1; /* Sit on top */
   padding-top: 100px; /* Location of the box */
   left: 0;
   top: 0;
   width: 100%; /* Full width */
   height: 100%; /* Full height */
   overflow: auto; /* Enable scroll if needed */
   background-color: rgb(0,0,0); /* Fallback color */
   background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
   }
   /* Modal Content */
   .modal-content {
   background-color: #fefefe;
   margin: auto;
   padding: 20px;
   border: 1px solid #888;
   width: 40%;
   z-index: 10000;
   }
   /* The Close Button */
   .close {
   color: #8e0f0f;
   float: right;
   font-size: 28px;
   font-weight: bold;
   }
   .close:hover,
   .close:focus {
   color: #000;
   text-decoration: none;
   cursor: pointer;
   }
   #compare-university-modal{
   text-align: center;
   }
   #compare-university-modal p{
   font-size: 16px;
   font-weight: bold;
   margin: 0 0 15px;
   }
   #compare-university-modal label{
   color:#000;
   font-size: 13px;
   margin-top: 10px;
   font-weight: 500;
   }
   .compare_universities_submit{
   text-align: center;
   font-size: 15px;
   margin-top: 10px;
   }
   #compare-university-modal input[type='submit']{
   padding: 5px;
   }
   .filterable {
   margin-top: 15px;
   }
   .filterable .panel-heading .pull-right {
   margin-top: -20px;
   }
   .filterable .filters input[disabled] {
   background-color: transparent;
   border: none;
   cursor: auto;
   box-shadow: none;
   padding: 0;
   height: auto;
   }
   .filterable .filters input[disabled]::-webkit-input-placeholder {
   color: #333;
   }
   .filterable .filters input[disabled]::-moz-placeholder {
   color: #333;
   }
   .filterable .filters input[disabled]:-ms-input-placeholder {
   color: #333;
   }
   .bannerSection{
   background-color: rgba(0, 0, 0, 0);
   background-repeat: no-repeat;
   background-image: url(<?php echo base_url();?>application/images/topBannerVF.png);
   background-size: cover;
   background-position: center center;
   width: 100%;
   /*height: 170px;*/
   }
   .aboutus-section {
   padding: 90px 0;
   }
   .aboutus-title {
   font-size: 35px;
   letter-spacing: 1.8px;
   /* line-height: 32px; */
   /* margin: 0 0 0px; */
   /* padding: 0 0 11px; */
   position: relative;
   /* text-transform: uppercase; */
   color: #313131;
   }
   #items_table tbody>tr>th {
   padding: 8px;
   line-height: 1.42857143;
   vertical-align: top;
   border-top: 1px solid #ddd;
   text-align: center;
   font-size: 14px;
   font-weight: 600;
   }
   #items_table tbody>tr>td {
   padding: 8px;
   line-height: 1.42857143;
   vertical-align: top;
   border-top: 1px solid #ddd;
   text-align: center;
   font-size: 13px;
   letter-spacing: 0.5px;
   }
   .sectionClass {
   padding: 20px 0px 50px 0px;
   position: relative;
   display: block;
   }
   .fullWidth {
   width: 100% !important;
   display: table;
   float: none;
   padding: 0;
   min-height: 1px;
   height: 100%;
   position: relative;
   }
   .sectiontitle {
   background-position: center;
   margin: 30px 0 0px;
   text-align: center;
   min-height: 20px;
   }
   .sectiontitle h2 {
   font-size: 30px;
   color: #222;
   margin-bottom: 0px;
   padding-right: 10px;
   padding-left: 10px;
   }
   .headerLine {
   width: 160px;
   height: 2px;
   display: inline-block;
   background: #101F2E;
   }
   .projectFactsWrap{
   display: flex;
   margin-top: 30px;
   flex-direction: row;
   flex-wrap: wrap;
   }
   #projectFacts .fullWidth{
   padding: 0;
   }
   .projectFactsWrap .item{
    width: 20%;
    height: 100%;
    padding: 25px 5px;
    text-align: center;
    border-radius: 25%;
    margin: 20px;
    margin-top: -150px;
    /*min-height: 200px;*/
   }
   .projectFactsWrap .item:nth-child(1){
   background: rgb(246 136 44);
   }
   .projectFactsWrap .item:nth-child(2){
   background: rgb(246 136 44);
   }
   .projectFactsWrap .item:nth-child(3){
   background: rgb(246 136 44);
   }
   .projectFactsWrap .item:nth-child(4){
   background: rgb(246 136 44);
   }
   .projectFactsWrap .item p.number{
   font-size: 25px;
   padding: 0;
   font-weight: bold;
   }
   .projectFactsWrap .item p{
   color: black;
   font-size: 16px;
   margin: 0;
   padding: 10px;
   font-family: 'Open Sans';
   font-weight: 600;
   }
   .projectFactsWrap .item span{
   width: 60px;
   background: rgba(255, 255, 255, 0.8);
   height: 2px;
   display: block;
   margin: 0 auto;
   }
   .projectFactsWrap .item i{
   vertical-align: middle;
   font-size: 50px;
   color: rgba(255, 255, 255, 0.8);
   }
   .projectFactsWrap .item:hover i, .projectFactsWrap .item:hover p{
   color: white;
   }
   .projectFactsWrap .item:hover span{
   background: white;
   }
   @media (max-width: 786px){
   .projectFactsWrap .item {
   flex: 0 0 50%;
   }
   }
   h1:after{
   content: "";
   position: absolute;
   left: 40%;
   margin-left: 0;
   bottom: 0;
   width: 20%;
   border-bottom: 4px solid white;
   }
   h1:before {
   content: "";
   position: absolute;
   left: 40%;
   margin-left: 0;
   top: 0;
   width: 20%;
   border-bottom: 4px solid white;
   }
</style>
<style type="text/css">
   .spanText{
   font: 550 14px Open Sans, sans-serif;
   }
   /*#wrapper {
   padding-left: 123px;
   transition: all 0.4s ease 0s;
   margin-top: 0px !important;
   }*/
   #sidebar-wrapper {
   /*margin-left: -250px;
   left: 250px;*/
   width: 288px;
   background: #8749a3;
   position: fixed;
   height: 35%;
   top: 40%;
   /*overflow-y: auto;*/
   overflow: hidden;
   transition: all 0.4s ease 0s;
   box-shadow: 1px 2px 4px #c1c1ca;
   }
   /* #wrapper.active {
   padding-left: 0;
   }
   #wrapper.active #sidebar-wrapper {
   left: 0;
   }*/
   /*  #page-content-wrapper {
   width: 100%;
   }*/
   .sidebar-nav {
   position: absolute;
   top: 0;
   width: 288px;
   list-style: none;
   margin: 0;
   padding: 0;
   }
   .sidebar-nav li {
   line-height: 20px;
   text-indent: 0px;
   }
   .sidebar-nav li a {
   color: #ffffff;
   display: block;
   text-decoration: none;
   padding-left: 5px;
   /*margin-left: 10px;*/
   }
   .sidebar-nav li a span:before {
   position: absolute;
   left: 0;
   color: #41484c;
   text-align: center;
   width: 20px;
   line-height: 18px;
   }
   .sidebar-nav li a span b{
   font: 550 14px Open Sans, sans-serif !important;
   }
   .sidebar-nav li a:hover,
   .sidebar-nav li.active {
   color: #ffffff;
   background: rgba(230, 227, 224, 0.41);
   text-decoration: none;
   }
   .sidebar-nav li a:active,
   .sidebar-nav li a:focus {
   text-decoration: none;
   }
   .sidebar-nav > .sidebar-brand {
   height: 65px;
   line-height: 60px;
   font-size: 18px;
   }
   .sidebar-nav > .sidebar-brand a {
   color: #f9992f;
   }
   .sidebar-nav > .sidebar-brand a:hover {
   color: #fff;
   background: none;
   }
   .sidebar-nav-sub {
   position: inherit;
   top: 0;
   width: 250px;
   list-style: none;
   margin: 0;
   padding: 0;
   }
   .sidebar-nav-sub li {
   line-height: 40px;
   text-indent: 12px;
   }
   .sidebar-nav-sub li a {
   color: #fff;
   display: block;
   text-decoration: none;
   padding-left: 25px;
   }
   .sidebar-nav-sub li a span:before {
   position: absolute;
   left: 7;
   color: #41484c;
   text-align: center;
   width: 20px;
   line-height: 18px;
   }
   .sidebar-nav-sub li a:hover,
   .sidebar-nav-sub li.active {
   color: blcak;
   background: rgba(243, 243, 243, 0.57);
   text-decoration: none;
   }
   .sidebar-nav-sub li a:active,
   .sidebar-nav-sub li a:focus {
   text-decoration: none;
   }
   .sidebar-nav-sub > .sidebar-brand {
   height: 31px;
   line-height: 20px;
   font-size: 16px;
   }
   .sidebar-nav-sub > .sidebar-brand a {
   color: rgba(153, 153, 153, 0.8117647058823529);
   }
   .sidebar-nav-sub > .sidebar-brand a:hover {
   color: #014b79;
   background: white;
   }
   .content-header {
   height: 65px;
   line-height: 65px;
   }
   .content-header h1 {
   margin: 0;
   margin-left: 20px;
   line-height: 65px;
   display: inline-block;
   }
   #menu-toggle {
   text-decoration: none;
   }
   .btn-menu {
   color: #000;
   }
   .inset {
   padding: 5px 20px 20px 20px;
   }
   @media screen and (max-width: 992px){
   #sidebar-wrapper {
   left: 0;
   width: inherit;
   }
   #wrapper.active {
   position: top;
   left: 250px;
   }
   .scrollSpyArea.active #sidebar-wrapper {
   left: 250px;
   width: 100px;
   transition: all 0.4s ease 0s;
   }
   .inset {
   padding: 15px;
   }
   }
   @media (max-width:767px) {
   /* #wrapper {
   padding-left: 0;
   }*/
   #sidebar-wrapper {
   left: 0;
   }
   #wrapper.active {
   position: top;
   left: 250px;
   }
   .scrollSpyArea.active #sidebar-wrapper {
   left: 250px;
   width: 100px;
   transition: all 0.4s ease 0s;
   }
   .sidebar-nav>li>a:focus {
   text-decoration: none;
   background-color: #eee;
   color: orange;
   }
   .inset {
   padding: 15px;
   }
   }
   .legendOne {
   text-align: center;
   display: block;
   width: 100%;
   padding: 1px 1px 1px 12px;
   margin-bottom: 20px;
   font-size: 30px;
   line-height: inherit;
   color: rgb(255, 249, 249);
   border: 0;
   border-radius: 6px;
   border-bottom: 1px solid #e5e5e5;
   background-color: #62367d;
   }
   .wellBox {
   min-height: 20px;
   padding: 3px 19px 5px 19px;
   margin-bottom: 15px;
   background-color: #ffffff;
   /* border: 1px solid #e3e3e3; */
   border-radius: 4px;
   /*-webkit-box-shadow: inset 0 0px 0px rgba(0,0,0,.05) !important;
   box-shadow: inset 0 0px 0px rgba(0,0,0,.05) !important; */
   box-shadow: 2px 2px 3px rgba(3, 75, 120, 0.78);
   }
   .team-style-five .team-items .item {
   -moz-box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
   -webkit-box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
   -o-box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
   box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
   padding-top: 0px;
   background-color: #62367d;
   margin: 10px;
   height: 115px;
   border-radius: 10px;
   }
   .team-style-five .single-item.team-standard .thumb {
   overflow: hidden;
   }
   .team-style-five .team-items .item .thumb img{
   width: 100%;
   height: 28%;
   }
   .team-style-five .team-items .item .thumb {
   position: relative;
   z-index: 1;
   height: 115px;
   }
   .team-style-five .team-items .item .info {
   background: #ffffff none repeat scroll 0 0;
   padding: 20px 20px;
   }
   .team-style-five .team-items .item .info h3 {
   font-weight: 700;
   margin-bottom: 5px;
   font-size: 18px;
   font-family:"montserrat", sans-serif;
   text-transform: uppercase;
   }
   .team-style-five .single-item.team-standard .item .info span {
   /*  display: inline-block;
   font-family: "montserrat",sans-serif;
   margin-bottom: 15px;
   padding-bottom: 10px;
   position: relative;
   text-transform: capitalize;*/
   font-size: 13px;
   color: #1cb9c8;
   }
   .team-style-five .single-item.team-standard .item .info .spanName::after {
   background: #1cb9c8 none repeat scroll 0 0;
   bottom: 0;
   content: "";
   height: 2px;
   left: 50%;
   margin-left: -20px;
   position: absolute;
   width: 40px;
   }
   .team-style-five .team-items .item .info p {
   margin-bottom: 0;
   color: #666666;
   text-align: left;
   font-size: 14px;
   /*line-height: 25px;*/
   }
   @import url("https://fonts.googleapis.com/css?family=Lato:400,400i,700");
   /* body {
   display: flex;
   justify-content: center;
   }*/
   .slick-wrapper {
   width: 100%;
   background-color: white;
   }
   .slide-item {
   background-color: #f6f6f7;
   color: black;
   /*display: flex !important;*/
   justify-content: center;
   align-items: center;
   text-align: center;
   height: 270px;
   border: 10px solid white;
   }
   .slide-item h3 {
   font-family: Lato, sans-serif;
   font-size: 40px;
   }
   .slide-item p{
   margin: 0 0 0px !important;
   }
   .slide-item h5{
   margin: 2px 0 2px;
   }
   .slick-prev, .slick-next {
   font-size: 0;
   line-height: 0;
   position: absolute;
   top: 100% !important;
   bottom: 0% !important;
   display: block;
   width: 59px;
   height: 45px;
   padding: 0;
   -webkit-transform: translate(0, -50%);
   -ms-transform: translate(0, -50%);
   transform: translate(0, -50%);
   /* cursor: pointer; */
   /* color: transparent; */
   border: none;
   /* outline: none; */
   background: #674299 !important;
   margin: 23px 45%;
   }
   .font14{
   font-size: 12px !important;
   }
   .uniBox{
   padding-right: 0px !important;
   padding-left: 0px !important;
   }
   .joinbtn{
   background: #f6872c !important;
   border-color: #f6872c !important;
   border-radius: 5px;
   }
   .joinQueuebtn , .joinQueuebtn:hover {
   background: #ffffff !important;
   border-color: #ffffff !important;
   border-radius: 5px;
   color: black;
   }
   .slick-slide img {
   display: initial;
   }
   .sidebar-nav li a:focus{
      color: #f6882c !important;

   }
   /* Extra large devices (medium laptops and desktops, 1400px and up) */
   @media only screen and (min-width: 1700px) {
   .projectFactsWrap .item{
   width: 17%;
   margin-left: 6%;
   }
   }

  .modal-backdrop.in{
   z-index:0;
   }

</style>
<section class="bannerSection">
   <!-- <img src="<?php echo base_url();?>uploads/banner.jpg" width="100%" height="400px" /> -->
   <!-- <a class="thumbnail" id="carousel-selector-1"><img src="<?php echo base_url();?>application/images/asucampus.jpg" style="height: 90px;min-height: 90px;padding: inherit;"></a> -->
   <div class="container" style="height: 400px;">
      <div class="row">
         <div class="col-md-12" style="margin-top: 80px;">
            <h1 class="page-top-title" style="color: #ffffff; text-align:center;font-weight:bold;font-size:45px;    margin-top: 25px;">IMPERIAL VIRTUAL</h1>
            <h1 class="page-top-title" style="color: #ffffff; text-align:center;font-weight:bold;font-size:45px;margin-bottom: 25px;">EDUCATION FAIR 2020</h1>
         </div>
      </div>
   </div>
</section>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <!-- Main content -->
   <section>
   <div class="col-md-12">
      <div class="col-md-2 col-lg-2 col-sm-2 hidden-xs"></div>
      <div class="col-md-10 col-lg-10 col-sm-10 hidden-xs">
      <div id="projectFacts" class="sectionClass">
                  <div class="projectFactsWrap " style="margin-left: 50px">
                     <div class="item wow fadeInUpBig animated animated " data-number="200" style="visibility: visible;">
                        <img src="<?php echo base_url();?>application/images/icons/1.png" width="20%">
                        <p id="number1" class="number"><?=$fair_total_registration?></p>
                        <span></span>
                        <p>Total Registrations</p>
                     </div>
                     <div class="item wow fadeInUpBig animated animated" data-number="55" style="visibility: visible;">
                        <img src="<?php echo base_url();?>application/images/icons/2.png" width="35%">
                        <p id="number2" class="number"><?=$total_live_student?></p>
                        <span></span>
                        <p>Active Participants</p>
                     </div>
                     <div class="item wow fadeInUpBig animated animated" data-number="359" style="visibility: visible;">
                        <img src="<?php echo base_url();?>application/images/icons/3.png" width="20%">
                        <p id="number3" class="number"><?=$university_live_count?></p>
                        <span></span>
                        <p>Online Universities</p>
                     </div>
                     <div class="item wow fadeInUpBig animated animated" data-number="246" style="visibility: visible;">
                        <img src="<?php echo base_url();?>application/images/icons/4.png" width="20%">
                        <p id="number4" class="number"><?=$total_session?></p>
                        <span></span>
                        <p>Sessions Attended</p>
                     </div>
                  </div>
             </div>
         </div>
      </div>
   </section>
   <section>
      <div class="container-fluid">
         <div class="row scrollSpyArea" >
            <div class="col-md-3 col-lg-3 col-sm-3 hidden-xs">
               <div id="sidebar-wrapper">
                  <nav id="spy">
                     <ul class="sidebar-nav nav">
                        <li>
                           <h4 style="color: #f6872c;text-align: center;background-color: white;padding: 10px 0px;    margin:0px;">Steps To Follow :</h4>
                        </li>
                        <li>
                           <a href="#PreCounselling"><span><img src="<?php echo base_url();?>application/images/icons/stepLogo.png" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><b>Pre Counselling</b></a>
                        </li>
                        <li id="my-booked-university" <?=($bookedUniversities) ? 'style="display:block"' : 'style="display:none"'?>>
                           <a href="#Booked_Universities" data-scroll><span><img src="<?php echo base_url();?>application/images/icons/stepLogo.png" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><b>My Booked Universities</b></a>
                        </li>
                        <li>
                           <a href="#Universities" data-scroll><span><img src="<?php echo base_url();?>application/images/icons/stepLogo.png" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><b>Universities</b></a>
                        </li>
                        <li>
                           <a href="#EducationLoan" data-scroll><span><img src="<?php echo base_url();?>application/images/icons/stepLogo.png" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><b>Education Loan / Financial Counselling</b></a>
                        </li>
                        <li>
                           <a href="#PostCounselling" data-scroll><span><img src="<?php echo base_url();?>application/images/icons/stepLogo.png" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><b>Post Counselling</b></a>
                        </li>
                        <li>
                           <a href="#" data-scroll class="btn btn-md btn-primary" style="text-align: center; padding-left: 0px;    background-color: #f6882c;">
                           <span><b>Help Desk</b></span>
                           </a>
                        </li>
                     </ul>
                  </nav>
               </div>
            </div>
            <div class="col-md-9 col-lg-9 col-sm-9">
                <?php
                if($precounselling_status == 'NO')
                {
                    ?>
                    <div class="page-content inset" data-spy="scroll" data-target="#spy" id="pre-counselling-box">
                       <div class="row team-style-five" id="PreCounselling">
                          <div class="jumbotron text-center row " style="margin-bottom: 15px;">
                             <h2 style="font-size: 30px">Pre Counselling</h2>
                             <button class="btn btn-md btn-info joinbtn" onclick='participateListUpdate(<?=$fair_id?>)'>Refresh</button>

                             <div id="precounsellingdiv">
                             <?php
                             $countpcl = 0;
                             foreach ($precounselling_list as $keypcl => $valuepcl) {
                               $countpcl = $countpcl + 1;
                             ?>
                             <div class="col-md-4 col-sm-12 team-items">
                                <div class="single-item text-center team-standard team-style-block">
                                   <div class="item">
                                      <div class="thumb">
                                         <h2 style="color: white;"> Room <?php echo $countpcl; ?></h2>
                                         <div class="col-md-12">
                                           <?php if($valuepcl['button_type'] == "JOIN") { ?>
                                            <button class="btn btn-md btn-info joinbtn" onclick='joinPreCounselling()' >Join</button>
                                          <?php } else { ?>
                                            <button class="btn btn-md btn-info joinbtn" onclick='joinQueue()' >Join Queue</button>
                                          <?php } ?>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                             </div>

                           <?php } ?>
                         </div>

                          </div>
                       </div>
                    </div>
                    <?php
                }
                ?>

               <div class="row">
                 <?php if( $precounselling_status == "NO" ){ ?>
                  <div id="universityImageBox" class="col-md-12 wellBox" style="padding-bottom: 55px;">
                     <h3 class="legendOne" id="Universities">Universities</h3>
                     <h2 style="text-align: center;">First Complete Pre Counselling to interact with universities </h2>
                     <img src="<?php echo base_url();?>application/images/mostLogo.jpg" class="img-fluid"  alt="Responsive Image" width="100%" height="80%" />
                  </div>
                <?php } else {
                    if($bookedUniversities)
                    {
                        ?>
                        <div id="bookedUniversityListBox" style="padding-bottom: 55px;">
                           <h3 class="legendOne" id="Booked_Universities">Booked Universities</h3>
                           <div class="slick-wrapper" >
                              <div id="slick1">

                                <?php foreach ($fairlist as $keyfl => $valuefl) {
                                  // code...
                                  if($valuefl['book_type'] == 'booked_user_slot' || $valuefl['book_type'] == 'current_user_slot') {
                               ?>
                                 <div class="slide-item">
                                    <div class="col-md-12">
                                       <img src="<?php echo base_url();?><?=$valuefl['img']?>" class="img-fluid" alt="Responsive Image" width="120" height="120" />
                                    </div>
                                    <div class="col-md-12 uniBox" id="booked-participant-<?=$valuefl['id']?>">
                                       <h5><?php echo  $valuefl['university_name']; ?></h5>
                                       <p><i class="fa fa-clock-o">&nbsp;</i><b> Total Intrested Student : </b><?php echo $valuefl['default_total_student'] + $valuefl['total_registrations'] ; ?></p>
                                           <p><i class="fa fa-calendar" >&nbsp;</i><b> Booked Slot : </b> <?php echo date('h:i:s a', strtotime($valuefl['fair_start_time'])) . " - " . date('h:i:s a', strtotime($valuefl['fair_end_time'])); ?></p>

                                           <?php
                                           if($valuefl['button_name'] == 'JOIN' && $valuefl['button_type'] == 'ENABLED') {
                                               ?>
                                           <button class="btn btn-md btn-primary font14" onclick="joinUniversity(<?php echo $valuefl['token_id']; ?>)" > JOIN </button>
                                           <button class="btn btn-md btn-danger font14" onclick="slotOption(<?php echo $valuefl['id']; ?>)" >Book Slot</button>
                                         <?php
                                     } else if($valuefl['button_name'] == 'JOIN' && $valuefl['button_type'] == 'DISABLED') {
                                         ?>
                                           <button class="btn btn-md btn-danger font14" disabled> JOIN </button>
                                           <button class="btn btn-md btn-danger font14" onclick="deleteSlot(<?php echo $valuefl['id']; ?>)" >Delete Slot</button>
                                         <?php
                                     }
                                            ?>
                                    </div>
                                 </div>
                               <?php }
                               else if($valuefl['book_type'] == 'already_attended') {
                            ?>
                              <div class="slide-item">
                                 <div class="col-md-12">
                                    <img src="<?php echo base_url();?><?=$valuefl['img']?>" class="img-fluid" alt="Responsive Image" width="120" height="120" />
                                 </div>
                                 <div class="col-md-12 uniBox" id="booked-participant-<?=$valuefl['id']?>">
                                    <h5><?php echo  $valuefl['university_name']; ?></h5>
                                    <p><i class="fa fa-clock-o">&nbsp;</i><b> Total Intrested Student : </b><?php echo $valuefl['default_total_student'] + $valuefl['total_registrations'] ; ?></p>
                                    <p><i class="fa fa-clock-o">&nbsp;</i><b> Already Attended </b></p>
                                        <p><i class="fa fa-calendar" >&nbsp;</i><b> Next Slot : </b> <?php echo date('h:i:s a', strtotime($valuefl['fair_start_time'])) . " - " . date('h:i:s a', strtotime($valuefl['fair_end_time'])); ?></p>
                                        <?php
                                        if($valuefl['button_name'] == 'JOIN' && $valuefl['button_type'] == 'ENABLED') {
                                            ?>
                                        <button class="btn btn-md btn-primary font14" onclick="joinUniversity(<?php echo $valuefl['token_id']; ?>)" > JOIN </button>
                                        <!--button class="btn btn-md btn-danger font14" onclick="slotOption(<?php echo $valuefl['id']; ?>)" >Book Again</button-->
                                      <?php
                                  } else{
                                      ?>
                                        <button class="btn btn-md btn-danger font14" onclick="slotOption(<?php echo $valuefl['id']; ?>)" >Book Again</button>
                                      <?php
                                  }
                                         ?>

                                 </div>
                              </div>
                            <?php }
                            else if($valuefl['book_type'] == 'already_expired') {
                         ?>
                           <div class="slide-item">
                              <div class="col-md-12">
                                 <img src="<?php echo base_url();?><?=$valuefl['img']?>" class="img-fluid" alt="Responsive Image" width="120" height="120" />
                              </div>
                              <div class="col-md-12 uniBox" id="booked-participant-<?=$valuefl['id']?>">
                                 <h5><?php echo  $valuefl['university_name']; ?></h5>
                                 <p><i class="fa fa-clock-o">&nbsp;</i><b> Total Intrested Student : </b><?php echo $valuefl['default_total_student'] + $valuefl['total_registrations'] ; ?></p>
                                 <p><i class="fa fa-clock-o">&nbsp;</i><b> Slot Expired </b></p>
                                     <p><i class="fa fa-calendar" >&nbsp;</i><b> Next Slot : </b> <?php echo date('h:i:s a', strtotime($valuefl['fair_start_time'])) . " - " . date('h:i:s a', strtotime($valuefl['fair_end_time'])); ?></p>
                                     <?php
                                     if($valuefl['button_name'] == 'JOIN' && $valuefl['button_type'] == 'ENABLED') {
                                         ?>
                                     <button class="btn btn-md btn-primary font14" onclick="joinUniversity(<?php echo $valuefl['token_id']; ?>)" > JOIN </button>
                                     <!--button class="btn btn-md btn-danger font14" onclick="slotOption(<?php echo $valuefl['id']; ?>)" >Book Again</button-->
                                   <?php
                               } else{
                                   ?>
                                     <button class="btn btn-md btn-danger font14" onclick="slotOption(<?php echo $valuefl['id']; ?>)" >Book Again</button>
                                   <?php
                               }
                                      ?>
                              </div>
                           </div>
                         <?php }
                           }?>
                              </div>
                           </div>
                        </div>
                        <?php
                    }
                    ?>
                  <div id="universityListBox" style="padding-bottom: 55px;">
                     <h3 class="legendOne" id="Universities">Universities</h3>
                     <div class="slick-wrapper" >
                        <div id="slick2">

                          <?php foreach ($fairlist as $keyfl => $valuefl) {
                            // code...
                            if($valuefl['book_type'] != 'booked_user_slot' && $valuefl['book_type'] != 'current_user_slot' && $valuefl['book_type'] != 'already_expired' && $valuefl['book_type'] != 'already_attended') {
                         ?>
                           <div class="slide-item">
                              <div class="col-md-12">
                                 <img src="<?php echo base_url();?><?=$valuefl['img']?>" class="img-fluid" alt="Responsive Image" width="120" height="120" />
                              </div>
                              <div class="col-md-12 uniBox" id="participant-<?=$valuefl['id']?>">
                                 <h5><?php echo  $valuefl['university_name']; ?></h5>
                                 <p><i class="fa fa-clock-o">&nbsp;</i><b> Total Intrested Student : </b><?php echo $valuefl['default_total_student'] + $valuefl['total_registrations'] ; ?></p>
                                     <p><i class="fa fa-calendar" >&nbsp;</i><b> Next Slot : </b> <?php echo date('h:i:s a', strtotime($valuefl['fair_start_time'])) . " - " . date('h:i:s a', strtotime($valuefl['fair_end_time'])); ?></p>
                                 <?php
                                 if($valuefl['button_name'] == 'JOIN' && $valuefl['button_type'] == 'ENABLED') {
                                     ?>
                                 <button class="btn btn-md btn-primary font14" onclick="joinUniversity(<?php echo $valuefl['token_id']; ?>)" > JOIN </button>
                               <?php
                           } else if($valuefl['button_name'] == 'JOIN' && $valuefl['button_type'] == 'DISABLED') {
                               ?>
                                 <button class="btn btn-md btn-danger font14" disabled> JOIN </button>
                                 <button class="btn btn-md btn-danger font14" onclick="deleteSlot(<?php echo $valuefl['id']; ?>)" >Delete Slot</button>
                               <?php
                           } else {
                               ?>
                                 <button class="btn btn-md btn-danger font14" onclick="slotOption(<?php echo $valuefl['id']; ?>)" >Book Slot</button>
                               <?php
                           }

                                  ?>
                              </div>
                           </div>
                         <?php } }?>

                           <!--<div class="slide-item">
                              <div class="col-md-12" style="margin-top: 5%;">
                                 <img src="<?php echo base_url();?>application/images/UniLogo/University-Logos_0003_640px-Pace_University_Logo.svg.png" class="img-fluid" alt="Responsive Image"
                                    width="50%" height="50%" />
                              </div>
                              <div class="col-md-12 uniBox">
                                 <button class="btn btn-md btn-primary font14">Booke Slot</button>
                              </div>
                           </div>
                           <div class="slide-item">
                              <div class="col-md-12" style="margin-top: 5%;">
                                 <img src="<?php echo base_url();?>application/images/UniLogo/University-Logos_0024_Stevens-Secondary-PMSColor-R.png" class="img-fluid" alt="Responsive Image"
                                    width="50%" height="50%" />
                              </div>
                              <div class="col-md-12 uniBox" >
                                 <button class="btn btn-md btn-primary font14">Booked Slot</button>
                                 <button class="btn btn-md btn-danger font14">Delete Slot</button>
                              </div>
                           </div>
                           <div class="slide-item">
                              <div class="col-md-12" style="margin-top: 5%;">
                                 <img src="<?php echo base_url();?>application/images/UniLogo/University-Logos_0064_1200px-University_of_Akron_seal.svg.png" class="img-fluid" alt="Responsive Image"
                                    width="50%" height="50%" />
                              </div>
                              <div class="col-md-12 uniBox" >
                                 <button class="btn btn-md btn-primary font14">Booked Slot</button>
                              </div>
                           </div>
                           <div class="slide-item">
                              <div class="col-md-12" style="margin-top: 5%;">
                                 <img src="<?php echo base_url();?>application/images/UniLogo/University-Logos_0034_Marshall-University-LOGO.png" class="img-fluid" alt="Responsive Image"
                                    width="50%" height="50%" />
                              </div>
                              <div class="col-md-12 uniBox">
                                 <h5>Northeastern University</h5>
                                 <p><i class="fa fa-calendar" >&nbsp;</i><b>Date  : </b> Sat, 19 September 2020</p>
                                 <p><i class="fa fa-clock-o">&nbsp;</i><b>Time : </b>04:05:00 pm IST</p>
                                 <button class="btn btn-md btn-primary font14">Booked Slot</button>
                                 <button class="btn btn-md btn-danger font14">Delete Slot</button>
                              </div>
                           </div>
                           <div class="slide-item">
                              <div class="col-md-12" style="margin-top: 5%;">
                                 <img src="<?php echo base_url();?>application/images/UniLogo/University-Logos_0057_banner_osu_logo.png" class="img-fluid" alt="Responsive Image"
                                    width="50%" height="50%" />
                              </div>
                              <div class="col-md-12 uniBox"  >
                                 <button class="btn btn-md btn-primary font14">Booke Slot</button>
                              </div>
                           </div>
                           <div class="slide-item">
                              <div class="col-md-12" style="margin-top: 5%;">
                                 <img src="<?php echo base_url();?>application/images/UniLogo/University-Logos_0061_116641.png" class="img-fluid" alt="Responsive Image"
                                    width="50%" height="50%" />
                              </div>
                              <div class="col-md-12 uniBox"  >
                                 <button class="btn btn-md btn-primary font14">Booked Slot</button>
                                 <button class="btn btn-md btn-danger font14">Delete Slot</button>
                              </div>
                           </div>
                           <div class="slide-item">
                              <div class="col-md-12" style="margin-top: 5%;">
                                 <img src="<?php echo base_url();?>application/images/UniLogo/University-Logos_0037_Logo_IUBH.png" class="img-fluid" alt="Responsive Image"
                                    width="50%" height="50%" />
                              </div>
                              <div class="col-md-12 uniBox"  >
                                 <button class="btn btn-md btn-primary font14">Booked Slot</button>
                              </div>
                           </div>
                           <div class="slide-item">
                              <h3>9</h3>
                           </div>
                           <div class="slide-item">
                              <h3>10</h3>
                           </div>
                           <div class="slide-item">
                              <h3>11</h3>
                           </div>
                           <div class="slide-item">
                              <h3>12</h3>
                           </div>
                           <div class="slide-item">
                              <h3>13</h3>
                           </div>
                           <div class="slide-item">
                              <h3>14</h3>
                           </div>
                           <div class="slide-item">
                              <h3>15</h3>
                           </div>
                           <div class="slide-item">
                              <h3>16</h3>
                           </div>
                           <div class="slide-item">
                              <h3>17</h3>
                           </div>
                           <div class="slide-item">
                              <h3>18</h3>
                           </div>
                           <div class="slide-item">
                              <h3>19</h3>
                           </div>
                           <div class="slide-item">
                              <h3>20</h3>
                           </div>
                           <div class="slide-item">
                              <h3>21</h3>
                           </div>
                           <div class="slide-item">
                              <h3>22</h3>
                           </div>
                           <div class="slide-item">
                              <h3>23</h3>
                           </div>
                           <div class="slide-item">
                              <h3>24</h3>
                           </div>
                           <div class="slide-item">
                              <h3>25</h3>
                           </div>
                           <div class="slide-item">
                              <h3>26</h3>
                           </div>
                           <div class="slide-item">
                              <h3>27</h3>
                           </div>
                           <div class="slide-item">
                              <h3>28</h3>
                           </div>
                           <div class="slide-item">
                              <h3>29</h3>
                           </div>
                           <div class="slide-item">
                              <h3>30</h3>
                           </div>
                           <div class="slide-item">
                              <h3>31</h3>
                           </div>
                           <div class="slide-item">
                              <h3>32</h3>
                           </div>
                           <div class="slide-item">
                              <h3>33</h3>
                           </div>
                           <div class="slide-item">
                              <h3>34</h3>
                           </div>
                           <div class="slide-item">
                              <h3>35</h3>
                           </div>
                           <div class="slide-item">
                              <h3>36</h3>
                           </div>
                           <div class="slide-item">
                              <h3>37</h3>
                           </div>
                           <div class="slide-item">
                              <h3>38</h3>
                           </div>
                           <div class="slide-item">
                              <h3>39</h3>
                           </div>
                           <div class="slide-item">
                              <h3>40</h3>
                           </div>
                           <div class="slide-item">
                              <h3>41</h3>
                           </div>
                           <div class="slide-item">
                              <h3>42</h3>
                           </div>
                           <div class="slide-item">
                              <h3>43</h3>
                           </div>
                           <div class="slide-item">
                              <h3>44</h3>
                           </div>
                           <div class="slide-item">
                              <h3>45</h3>
                           </div>
                           <div class="slide-item">
                              <h3>46</h3>
                           </div>
                           <div class="slide-item">
                              <h3>47</h3>
                           </div>
                           <div class="slide-item">
                              <h3>48</h3>
                           </div>
                           <div class="slide-item">
                              <h3>49</h3>
                           </div>
                           <div class="slide-item">
                              <h3>50</h3>
                           </div>
                           <div class="slide-item">
                              <h3>51</h3>
                           </div>
                           <div class="slide-item">
                              <h3>52</h3>
                           </div>
                           <div class="slide-item">
                              <h3>53</h3>
                           </div>
                           <div class="slide-item">
                              <h3>54</h3>
                           </div>
                           <div class="slide-item">
                              <h3>55</h3>
                           </div>
                           <div class="slide-item">
                              <h3>56</h3>
                           </div>
                           <div class="slide-item">
                              <h3>57</h3>
                           </div>
                           <div class="slide-item">
                              <h3>58</h3>
                           </div>
                           <div class="slide-item">
                              <h3>59</h3>
                           </div>
                           <div class="slide-item">
                              <h3>60</h3>
                           </div>
                           <div class="slide-item">
                              <h3>61</h3>
                           </div>
                           <div class="slide-item">
                              <h3>62</h3>
                           </div>
                           <div class="slide-item">
                              <h3>63</h3>
                           </div>
                           <div class="slide-item">
                              <h3>64</h3>
                           </div>
                           <div class="slide-item">
                              <h3>65</h3>
                           </div>
                           <div class="slide-item">
                              <h3>66</h3>
                           </div>
                           <div class="slide-item">
                              <h3>67</h3>
                           </div>-->
                        </div>
                     </div>
                  </div>

                <?php } ?>

                  <div class="col-md-12 wellBox team-style-five">
                     <h2 class="legendOne" id="EducationLoan">Education Loan / Finance Counselling </h2>
                     <div class="jumbotron text-center row " style="margin-bottom: 15px;">
                        <!-- <h2 style="font-size: 30px">Pre Counselling</h2> -->

                        <div id="educounsellingreplace">

                        <?php
                        $educount = 0;
                        foreach ($edu_counselling_list as $keyecl => $valueecl) {
                          // code...
                          $educount = $educount + 1;

                        ?>
                        <div class="col-md-4 col-sm-12 team-items">
                           <div class="single-item text-center team-standard team-style-block">
                              <div class="item">
                                 <div class="thumb">
                                    <h2 style="color: white;">Room <?php echo $educount; ?></h2>
                                    <div class="col-md-12">
                                      <?php if($valueecl['button_type'] == 'JOIN') { ?>
                                       <button class="btn btn-md btn-info joinbtn" onclick="joinEduCounselling()">Join</button>
                                     <?php } else { ?>
                                       <button class="btn btn-md btn-info joinQueuebtn" onclick="joinEduQueue()">Join Queue</button>
                                       <?php } ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                      <?php } ?>

                     </div>
                  </div>
                  <div class="col-md-12 wellBox team-style-five">
                     <h2 class="legendOne" id="PostCounselling">Post Counselling</h2>
                     <div class="jumbotron text-center row " style="margin-bottom: 15px;">
                        <!-- <h2 style="font-size: 30px">Post Counselling</h2> -->

                        <div id="postcounsellingreplace">
                        <?php
                        $postcount = 0;
                        foreach ($post_counselling_list as $keypcl => $valuepcl) {
                          // code...
                          $postcount = $postcount + 1;

                        ?>
                        <div class="col-md-4 col-sm-12 team-items">
                           <div class="single-item text-center team-standard team-style-block">
                              <div class="item">
                                 <div class="thumb">
                                    <h2 style="color: white;">Room <?php echo $postcount; ?></h2>
                                    <div class="col-md-12">
                                      <?php if($valuepcl['button_type'] == 'JOIN') { ?>
                                       <button class="btn btn-md btn-info joinbtn" onclick="joinPostCounselling()">Join</button>
                                     <?php } else { ?>
                                       <button class="btn btn-md btn-info joinQueuebtn" onclick="joinPostQueue()">Join Queue</button>
                                       <?php } ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <?php } ?>
                      </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
</div>
</section>
<!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="slotListModal" class="modal">
<!-- Modal content style="display: block;"-->
   <div class="modal-content" style="width: 45%;">
      <span class="close closeSlotList">&times;</span>
      <div id="slot-list-modal" style="text-align: center;">

        <h3> Available Slots </h3>

        <table class="table" id="items_table" border="1">

       </table>

      </div>
   </div>


</div>

<div id="reminderJoinModal" class="modal">
<!-- Modal content style="display: block;"-->
   <div class="modal-content">
      <span class="close closeReminderModal">&times;</span>
      <div id="reminder-join-modal" style="text-align: center;">

      </div>
   </div>


</div>


<script type="text/javascript">

$( document ).ready(function() {

$('#desired_mainstream').on('change', function() {

var dataString = 'id='+this.value;

$.ajax({

type: "POST",

url: "/search/university/getSubcoursesList",

data: dataString,

cache: false,

success: function(result){

 $('#desired_subcourse').html(result);


}

     });

});

})

// sent OTP
function fairapply(wid) {


  var stype = "loggedIn";

    var dataString = 'source=' + stype + '&fair_id=' + wid;

    $.ajax({

    type: "POST",

    url: "/webinar/webinarmaster/fairapply",

    data: dataString,

    cache: false,

    success: function(result){

     //alert(result);
     window.location.href=result;

    }
        });
}

// sent OTP
function applySlot(sid,pid) {


     var stype = "fair";

       var dataString = 'source=' + stype + "&slot_id=" + sid +"&participant_id=" + pid;

       $.ajax({

       type: "POST",

       url: "/webinar/webinarmaster/applySlot",

       data: dataString,

       cache: false,

       success: function(result){

        //alert(result);
        if(result == "NOTLOGGEDIN"){
          window.location.href="/user/account";
        } else if(result == "SLOTFULL"){
          //$("#slotListModal").modal('hide');
          alert("SLOT FULL");
        } else if(result == "SUCCESS"){

          var numberofslotbook = getCookie("slotcount") ? getCookie("slotcount") : 0;
          var increaseCount = parseInt(numberofslotbook) + 1;
          setCookie("slotcount",parseInt(increaseCount),1);

          $("#slotListModal").modal('hide');

          var htmltext = "";
              htmltext += '<h3> Info Message <h3>'
                       + '<div> SLOT BOOKED SUCCESSFULLY...!!!</div>';

          document.getElementById("reminder-join-modal").innerHTML = htmltext;
          $("#reminderJoinModal").modal('show');

          location.reload();
          //window.location.href="/university/virtualfair/list";
        } else {
          alert(result);
        }

       }
           });
   }

   function deleteSlot(pid) {


        var stype = "fair";

          var dataString = 'source=' + stype + "&participant_id=" + pid;

          $.ajax({

          type: "POST",

          url: "/webinar/webinarmaster/deleteSlot",

          data: dataString,

          cache: false,

          success: function(result){

           //alert(result);
           if(result == "NOTLOGGEDIN"){

             window.location.href="/user/account";

           } else if(result == "DELETED"){

             var numberofslotbook = getCookie("slotcount") ? getCookie("slotcount") : 0;
             var increaseCount = parseInt(numberofslotbook) - 1;
             setCookie("precounselling",parseInt(increaseCount),1);

             $("#reminderJoinModal").modal('hide');
             var htmltext = "";
                 htmltext += '<h3> Info Message <h3>'
                          + '<h5> SLOT DELETED. APPLY FOR NEW SLOT...!!!</h5>';

             document.getElementById("reminder-join-modal").innerHTML = htmltext;
             $("#reminderJoinModal").modal('show');

             location.reload();
             //window.location.href="/university/virtualfair/list";

           } else if(result == "NODATAAVAILABLE"){

             $("#reminderJoinModal").modal('hide');
             var htmltext = "";
                 htmltext += '<h3> Info Message <h3>'
                          + '<h5> YOU HAVE NOT APPLIED FOR THIS UNIVERSITY...!!!</h5>';

             document.getElementById("reminder-join-modal").innerHTML = htmltext;
             $("#reminderJoinModal").modal('show');

           }

          }
              });
      }

   var spanslotlist = document.getElementsByClassName("closeSlotList")[0];
   spanslotlist.onclick = function() {
     $("#slotListModal").modal('hide');
   }

   function slotOption(pid) {

          var dataString = "participant_id=" + pid;

          $.ajax({

          type: "POST",

          url: "/webinar/webinarmaster/availableSlots",

          data: dataString,

          cache: false,

          success: function(result){

            if(result.slot_available == "NO"){

              $("#reminderJoinModal").modal('hide');
              var htmltext = "";
                  htmltext += '<h3> Info Message <h3>'
                           + '<h5> ALL slots are full...!!!</h5>';

              document.getElementById("reminder-join-modal").innerHTML = htmltext;
              $("#reminderJoinModal").modal('show');

            } else {

              var slotlist = result.slot_list;

              if(slotlist.length){

                var htmlTextList = "<table class='table' border='1'><tr><th> Slot Start Time </th><th> Slot End Time </th><th>Action</th></tr></table>";

                slotlist.forEach(function(value, index){

                  htmlTextList += '<tr><td>' + value.start_time + '</td><td>'+ value.end_time + '</td>';
                  if(value.status == '0'){
                      htmlTextList += '<td>On Break</td>';
                  }
                  else if(value.is_full == '1'){
                      htmlTextList += '<td>Room Full</td>';
                  }
                  else{
                      htmlTextList += '<td><button style="text-align: center; border-radius: 10px; box-shadow: 2px 2px 2px #c7c7c7c2;" class="btn btn-md btn-info"  onclick="applySlot('+value.id+','+value.participant_id+')"><b><i class="fa fa-sign-in" style="font-size:15px;color:white;">&nbsp;</i> Apply </b></button></td></tr>';
                  }

                });

                //modal show fix view
                $("#slotListModal").modal('show');

                document.getElementById("items_table").innerHTML = htmlTextList;

              }
            }

          }
              });
      }

      function joinPreCounselling(){

        var dataString = "participant_id=" + 0;

        $.ajax({

        type: "POST",

        url: "/webinar/webinarmaster/joinPreCounselling",

        data: dataString,

        cache: false,

        success: function(result){
          if(result == "NOTAVAILABLEINQUEUE"){
            setCookie("precounsellingcount",1,1);

            $("#reminderJoinModal").modal('hide');
            var htmltext = "";
                htmltext += '<h3> Info Message <h3>'
                         + '<h5> You are in queue for pre-counseling, you will be attended very shortly by one of our counselors!</h5>';

            document.getElementById("reminder-join-modal").innerHTML = htmltext;
            $("#reminderJoinModal").modal('show');

          } else if(result == "NOTAVAILABLE"){
            alert("Counsellors Are Busy...!!!");
          } else {

	    $("#reminderJoinModal").modal('hide');
            setCookie("precounsellingcount",0,1);

            var win = window.open(result, 'vfair');
            win.focus();
          }
        }
            });

      }

      function joinQueue(){

        var dataString = "participant_id=" + 0;

        $.ajax({

        type: "POST",

        url: "/webinar/webinarmaster/bookPreCounselling",

        data: dataString,

        cache: false,

        success: function(result){

          if(result == "SUCCESS"){

            setCookie("precounsellingcount",1,1);

            $("#reminderJoinModal").modal('hide');
            var htmltext = "";
                htmltext += '<h3> Info Message <h3>'
                         + '<h5> You are in queue for pre-counseling, you will be attended very shortly by one of our counselors!</h5>';

            document.getElementById("reminder-join-modal").innerHTML = htmltext;
            $("#reminderJoinModal").modal('show');

          } else{
            alert(result);
          }

        }
            });
      }

      function joinUniversity(tid){

        var dataString = "token_id=" + tid;

        $.ajax({

        type: "POST",

        url: "/webinar/webinarmaster/joinUniversityFair",

        data: dataString,

        cache: false,

        success: function(result){

          if(result == "NOTAVAILABLE"){
            alert("Counsellors Are Busy...!!!");
          } else{
            //window.location.href=result;
            //alert(result);

            var numberofslotbook = getCookie("slotcount") ? getCookie("slotcount") : 0;
            var increaseCount = parseInt(numberofslotbook) - 1;
            setCookie("slotcount",parseInt(increaseCount),1);


            var win = window.open(result, 'vfair');
            win.focus();
          }

        }
            });
      }
      var allParticipants = {};

    function  participateListUpdate(fid){

      var dataString = "fair_id="+fid;

      $.ajax({

      type: "POST",

      url: "/webinar/webinarmaster/virtualFairParticipantsListRefresh",

      data: dataString,

      cache: false,

      success: function(result){

        var precounselling = result.precounselling_status;

        if(precounselling == "YES"){
            var precounsellorReminder = getCookie("precounsellingcount");

            if( precounsellorReminder != null && precounsellorReminder > 0){
              setCookie("precounsellingcount",0,1);
              window.location.href = '/university/virtualfair/list/1';
            }
            /*else{
                window.location.href = '/university/virtualfair/list/1';
            }*/

          /*$("#universityImageBox").hide();
          document.getElementById("universityListBox").style.display = "block";
          if(document.getElementById("bookedUniversityListBox"))
          {
              document.getElementById("bookedUniversityListBox").style.display = "block";
          }
          if(document.getElementById("pre-counselling-box"))
          {
              document.getElementById("pre-counselling-box").style.display = "none";
          }
          $('.slick-slider').get(0).slick.setPosition();*/
        } else{
          $("#universityListBox").hide();

          if(document.getElementById("bookedUniversityListBox"))
          {
              $("#bookedUniversityListBox").hide();
          }
          document.getElementById("universityImageBox").style.display = "block";
        }

        var precounsellinglist = result.precounselling_list;

        if(precounsellinglist.length){

          var htmlPrecounsellingList = "";
          var rcount = 0;


          precounsellinglist.forEach(function(value, index){

            rcount = rcount +1;

            var buttonview = "";

            if(value.button_type == "JOIN"){
              buttonview +=' <button class="btn btn-md btn-info joinbtn" onclick="joinPreCounselling()" >Join</button>';
            } else {
              buttonview +=' <button class="btn btn-md btn-info joinQueuebtn" onclick="joinQueue()" >Join Queue</button>';
            }

            htmlPrecounsellingList += '<div class="col-md-4 col-sm-12 team-items">'
                                    +'<div class="single-item text-center team-standard team-style-block">'
                                    +'<div class="item">'
                                    +'<div class="thumb">'
                                    +'<h2 style="color: white;"> Room '+rcount+'</h2>'
                                    +'<div class="col-md-12">'
                                    + buttonview
                                    +'</div>'
                                    +'</div>'
                                    +'</div>'
                                    +'</div>'
                                    +'</div>';

          });

	if(document.getElementById("precounsellingdiv")){
          document.getElementById("precounsellingdiv").innerHTML = htmlPrecounsellingList;
	}

        }



        var fairlist = result.fairlist;
        var tracked_participants = [];
        if(fairlist !== undefined && fairlist.length){

          fairlist.forEach(function(value, index){
              tracked_participants.push(value.id);
            var studentCount = 0;

                studentCount = parseInt(value.default_total_student) + parseInt(value.total_registrations);

            var timeView = '';
            var buttonType = '';
            var htmlFairList = "";

            if(value.book_type == 'booked_user_slot' || value.book_type == 'current_user_slot'){
                timeView += '<p><i class="fa fa-calendar" >&nbsp;</i><b> Booked Slot : </b>'+ value.fair_start_time + ' - ' + value.fair_end_time + '</p>';
                if(value.button_name == 'JOIN' && value.button_type == 'ENABLED')
                {
                    buttonType += "<button class='btn btn-md btn-primary font14'  onclick='joinUniversity("+value.token_id+")'> Join </button>";
                    buttonType += "<button class='btn btn-md btn-primary font14' onclick='slotOption("+value.id+")'>Book Slot</button>";
                }
                else if(value.button_name == 'JOIN' && value.button_type == 'DISABLED')
                {
                    buttonType += "<button class='btn btn-md btn-primary font14' disabled> Join </button>"
                                +"<button class='btn btn-md btn-primary font14'  onclick='deleteSlot("+value.id+")'> Delete Slot </button>";
                }
                htmlFairList += '<h5>'+value.university_name+'</h5>'
                   +'<p><i class="fa fa-clock-o">&nbsp;</i><b> Total Intrested Student : </b>'+ studentCount +'</p>'
                   + timeView
                   + buttonType;

                  document.getElementById("booked-participant-" + value.id).innerHTML = htmlFairList;

            }
            else if(value.book_type == 'already_attended'){
                timeView += '<p><i class="fa fa-clock-o">&nbsp;</i><b> Already Attended </b></p>';
                timeView += '<p><i class="fa fa-calendar" >&nbsp;</i><b> Next Slot : </b>'+ value.fair_start_time + ' - ' + value.fair_end_time + '</p>';
                if(value.button_name == 'JOIN' && value.button_type == 'ENABLED')
                {
                    buttonType += "<button class='btn btn-md btn-primary font14'  onclick='joinUniversity("+value.token_id+")'> Join </button>";
                    //buttonType += "<button class='btn btn-md btn-primary font14' onclick='slotOption("+value.id+")'>Book Again</button>"
                }
                else
                {
                    buttonType +="<button class='btn btn-md btn-primary font14' onclick='slotOption("+value.id+")'>Book Again</button>"
                }
                htmlFairList += '<h5>'+value.university_name+'</h5>'
                   +'<p><i class="fa fa-clock-o">&nbsp;</i><b> Total Intrested Student : </b>'+ studentCount +'</p>'
                   + timeView
                   + buttonType;

                  document.getElementById("booked-participant-" + value.id).innerHTML = htmlFairList;

            }
            else if(value.book_type == 'already_expired'){
                timeView += '<p><i class="fa fa-clock-o">&nbsp;</i><b> Slot Expired </b></p>';
                timeView += '<p><i class="fa fa-calendar" >&nbsp;</i><b> Next Slot : </b>'+ value.fair_start_time + ' - ' + value.fair_end_time + '</p>';
                if(value.button_name == 'JOIN' && value.button_type == 'ENABLED')
                {
                    buttonType += "<button class='btn btn-md btn-primary font14'  onclick='joinUniversity("+value.token_id+")'> Join </button>";
                    //buttonType += "<button class='btn btn-md btn-primary font14' onclick='slotOption("+value.id+")'>Book Again</button>"
                }
                else
                {
                    buttonType +="<button class='btn btn-md btn-primary font14' onclick='slotOption("+value.id+")'>Book Again</button>"
                }
                htmlFairList += '<h5>'+value.university_name+'</h5>'
                   +'<p><i class="fa fa-clock-o">&nbsp;</i><b> Total Intrested Student : </b>'+ studentCount +'</p>'
                   + timeView
                   + buttonType;

                  document.getElementById("booked-participant-" + value.id).innerHTML = htmlFairList;

            }
            else {
              timeView += '<p><i class="fa fa-calendar" >&nbsp;</i><b> Start Slot Time  : </b>'+ value.fair_start_time + ' - ' + value.fair_end_time + '</p>';
              if(value.button_name == 'JOIN' && value.button_type == 'ENABLED')
              {
                  buttonType += "<button class='btn btn-md btn-primary font14'  onclick='joinUniversity("+value.token_id+")'> Join </button>";
              }
              else if(value.button_name == 'JOIN' && value.button_type == 'DISABLED')
              {
                  buttonType += "<button class='btn btn-md btn-primary font14' disabled> Join </button>"
                              +"<button class='btn btn-md btn-primary font14'  onclick='deleteSlot("+value.id+")'> Delete Slot </button>";
              } else if(value.button_name == 'BOOK' && value.button_type == 'ENABLED'){
                  buttonType +="<button class='btn btn-md btn-primary font14'  onclick='slotOption("+value.id+")'>Book Slot</button>"
              }
              htmlFairList += '<h5>'+value.university_name+'</h5>'
                 +'<p><i class="fa fa-clock-o">&nbsp;</i><b> Total Intrested Student : </b>'+ studentCount +'</p>'
                 + timeView
                 + buttonType;
                 if(!document.getElementById("participant-" + value.id))
                 {
                     window.location.href = '/university/virtualfair/list/1';
                 }
                 else
                 {
                     document.getElementById("participant-" + value.id).innerHTML = htmlFairList;
                 }
            }

        });

        for(let key in allParticipants){
            if(tracked_participants.indexOf(key) === -1){
                var elementType = allParticipants[key];
                if(elementType == 'booked'){
                    document.getElementById("booked-participant-" + key).innerHTML = "University is on break. Will get back soon";
                }
                else{
                    document.getElementById("participant-" + key).innerHTML = "University is on break. Will get back soon";
                }
            }
        }
      }

      var educounsellinglist = result.edu_counselling_list;

      if(educounsellinglist.length){

        var htmlEducounsellingList = "";
        var educount = 0;

        educounsellinglist.forEach(function(value, index){

          educount = educount +1;

          var buttonview = "";

          if(value.button_type == "JOIN"){
            buttonview +=' <button class="btn btn-md btn-info joinbtn" onclick="joinEduCounselling()" >Join</button>';
          } else {
            buttonview +=' <button class="btn btn-md btn-info joinQueuebtn" onclick="joinEduQueue()" >Join Queue</button>';
          }

          htmlEducounsellingList += '<div class="col-md-4 col-sm-12 team-items">'
                                  +'<div class="single-item text-center team-standard team-style-block">'
                                  +'<div class="item">'
                                  +'<div class="thumb">'
                                  +'<h2 style="color: white;"> Room '+educount+'</h2>'
                                  +'<div class="col-md-12">'
                                  + buttonview
                                  +'</div>'
                                  +'</div>'
                                  +'</div>'
                                  +'</div>'
                                  +'</div>';

        });

        document.getElementById("educounsellingreplace").innerHTML = htmlEducounsellingList;

      }

      var postcounsellinglist = result.post_counselling_list;

      if(postcounsellinglist.length){

        var htmlPostcounsellingList = "";
        var pcount = 0;

        postcounsellinglist.forEach(function(value, index){

          pcount = pcount +1;

          var buttonview = "";

          if(value.button_type == "JOIN"){
            buttonview +=' <button class="btn btn-md btn-info joinbtn" onclick="joinPostCounselling()" >Join</button>';
          } else {
            buttonview +=' <button class="btn btn-md btn-info joinQueuebtn" onclick="joinPostQueue()" >Join Queue</button>';
          }

          htmlPostcounsellingList += '<div class="col-md-4 col-sm-12 team-items">'
                                  +'<div class="single-item text-center team-standard team-style-block">'
                                  +'<div class="item">'
                                  +'<div class="thumb">'
                                  +'<h2 style="color: white;"> Room '+pcount+'</h2>'
                                  +'<div class="col-md-12">'
                                  + buttonview
                                  +'</div>'
                                  +'</div>'
                                  +'</div>'
                                  +'</div>'
                                  +'</div>';

        });

        document.getElementById("postcounsellingreplace").innerHTML = htmlPostcounsellingList;

      }

    }
          });
    }

</script>

<script type="text/javascript">


var reminderList = document.getElementsByClassName("closeReminderModal")[0];
reminderList.onclick = function() {
  $("#reminderJoinModal").modal('hide');
}

function checkUpcomingSlot(){

 var dataString = "user_id=" + 0;

 $.ajax({

 type: "POST",

 url: "/webinar/webinarmaster/bookedSlotReminder",

 data: dataString,

 cache: false,

 success: function(result){

   if(result == "NOTLOGGEDIN"){
     window.location.href = '/user/account';
   } else if( result == "NOJOIN"){
     //alert("Not booked Any Slot ...!!!");
     /*var numberofslotbook = getCookie("slotcount") ? getCookie("slotcount") : 0;
     var increaseCount = parseInt(numberofslotbook) - 1;
     setCookie("slotcount",parseInt(increaseCount),1);*/
   } else if( result == "JOINCOMING"){

     var htmltext = "";
         htmltext += '<h3> Info Message <h3>'
                  + '<h5> Your Booked Slot Is Going Start In Next One Minute. Please Join!</h5>';

     document.getElementById("reminder-join-modal").innerHTML = htmltext;
     $("#reminderJoinModal").modal('show');

   } else if( result == "JOIN"){

     var htmltext = "";
         htmltext += '<h3> Info Message <h3>'
                  + '<h5> Your Booked Slot Started Already. please Join!</h5>';

     document.getElementById("reminder-join-modal").innerHTML = htmltext;
     $("#reminderJoinModal").modal('show');

   }
   else{
       var numberofslotbook = getCookie("slotcount") ? getCookie("slotcount") : 0;
       if(parseInt(numberofslotbook) > 0)
       {
           var increaseCount = parseInt(numberofslotbook) - parseInt(result);
           setCookie("slotcount",parseInt(increaseCount),1);
       }
   }

 }
     });
}

function checkUpcomingPreCounselling(){

 var dataString = "user_id=" + 0;

 $.ajax({

 type: "POST",

 url: "/webinar/webinarmaster/checkPreCounsellingQueue",

 data: dataString,

 cache: false,

 success: function(result){

   if(result == "NOTLOGGEDIN"){
     window.location.href = '/user/account';
   } else if( result == "NOTINQUEUE"){
     //alert("Not booked Any Slot ...!!!");
   } else{
     //alert("pre counselling join");
     $("#reminderJoinModal").modal('hide');
     var htmltext = "";
         htmltext += '<h3> JOIN NOW <h3>'
                  + '<h5> Your Booked Session Is Starting.</h5> <br/> <button class="btn btn-md btn-info joinbtn" onclick="joinPreCounselling()" >Join</button>';

     document.getElementById("reminder-join-modal").innerHTML = htmltext;
     $("#reminderJoinModal").modal('show');
   }

 }
     });
}


//Edu counselling
function joinEduCounselling(){

  var dataString = "participant_id=" + 0;

  $.ajax({

  type: "POST",

  url: "/webinar/webinarmaster/joinEduCounselling",

  data: dataString,

  cache: false,

  success: function(result){

    if(result == "NOTAVAILABLEINQUEUE"){

      setCookie("educounsellingcount",1,1);

      $("#reminderJoinModal").modal('hide');
      var htmltext = "";
          htmltext += '<h3> Info Message <h3>'
                   + '<h5> You are in queue for Education Loan Counseling, you will be attended very shortly by one of our counselors!</h5>';

      document.getElementById("reminder-join-modal").innerHTML = htmltext;
      $("#reminderJoinModal").modal('show');

    } else if(result == "NOTLOGGEDIN"){
      window.location.href = '/user/account';
    } else if(result == "NOTAVAILABLE"){
      alert("No Counsellor Is Active...!!!");
    } else {

      $("#reminderJoinModal").modal('hide');

      setCookie("educounsellingcount",0,1);
      //set educounselling done
      setCookie("educounsellingdone",1,1);

      var win = window.open(result, 'vfair');
      win.focus();
    }
  }
      });

}

function joinEduQueue(){

  var dataString = "participant_id=" + 0;

  $.ajax({

  type: "POST",

  url: "/webinar/webinarmaster/bookEduCounselling",

  data: dataString,

  cache: false,

  success: function(result){

    if(result == "SUCCESS"){

      setCookie("educounsellingcount",1,1);

      $("#reminderJoinModal").modal('hide');
      var htmltext = "";
          htmltext += '<h3> Info Message <h3>'
                   + '<div> You are in queue for Education Loan Counseling, you will be attended very shortly by one of our counselors!</div>';

      document.getElementById("reminder-join-modal").innerHTML = htmltext;
      $("#reminderJoinModal").modal('show');
    } else{
      alert(result);
    }

  }
      });
}

function checkUpcomingEduCounselling(){

 var dataString = "user_id=" + 0;

 $.ajax({

 type: "POST",

 url: "/webinar/webinarmaster/checkEduCounsellingQueue",

 data: dataString,

 cache: false,

 success: function(result){

   if(result == "NOTLOGGEDIN"){
     window.location.href = '/user/account';
   } else if( result == "NOTINQUEUE"){
     //alert("Not booked Any Slot ...!!!");
   } else{
     //alert("Edu counselling join..!!!");
     $("#reminderJoinModal").modal('hide');
     var htmltext = "";
         htmltext += '<h3> JOIN NOW <h3>'
                  + '<h5> All your Finance / Education Loans concerns can now be resolved by our Finance Team! </h5> </br> <button class="btn btn-md btn-info joinbtn" onclick="joinEduCounselling()" >Join</button>';

     document.getElementById("reminder-join-modal").innerHTML = htmltext;
     $("#reminderJoinModal").modal('show');
   }

 }
     });
}

//POST counselling
function joinPostCounselling(){

  var dataString = "participant_id=" + 0;

  $.ajax({

  type: "POST",

  url: "/webinar/webinarmaster/joinPostCounselling",

  data: dataString,

  cache: false,

  success: function(result){

    if(result == "NOTAVAILABLEINQUEUE"){
      setCookie("postcounsellingcount",1,1);

      $("#reminderJoinModal").modal('hide');
      var htmltext = "";
          htmltext += '<h3> Info Message <h3>'
                   + '<div> You are in queue for post-counseling, you will be attended very shortly by one of our counselors!</div>';

      document.getElementById("reminder-join-modal").innerHTML = htmltext;
      $("#reminderJoinModal").modal('show');

    } else if(result == "NOTLOGGEDIN"){
      window.location.href = '/user/account';
    } else if(result == "NOTAVAILABLE"){
      alert("No Counsellor Is Active...!!!");
    } else {

      $("#reminderJoinModal").modal('hide');
      setCookie("postcounsellingcount",0,1);

      //set educounselling done
      setCookie("postcounsellingdone",1,1);

      var win = window.open(result, 'vfair');
      win.focus();
    }
  }
      });

}

function joinPostQueue(){

  var dataString = "participant_id=" + 0;

  $.ajax({

  type: "POST",

  url: "/webinar/webinarmaster/bookPostCounselling",

  data: dataString,

  cache: false,

  success: function(result){

    if(result == "SUCCESS"){

      setCookie("postcounsellingcount",1,1);

      $("#reminderJoinModal").modal('hide');
      var htmltext = "";
          htmltext += '<h3> Info Message <h3>'
                   + '<h5> You are in queue for post-counseling, you will be attended very shortly by one of our counselors!</h5>';

      document.getElementById("reminder-join-modal").innerHTML = htmltext;
      $("#reminderJoinModal").modal('show');

    } else{
      alert(result);
    }

  }
      });
}

function checkUpcomingPostCounselling(){

 var dataString = "user_id=" + 0;

 $.ajax({

 type: "POST",

 url: "/webinar/webinarmaster/checkPostCounsellingQueue",

 data: dataString,

 cache: false,

 success: function(result){

   if(result == "NOTLOGGEDIN"){
     window.location.href = '/user/account';
   } else if( result == "NOTINQUEUE"){
     //alert("Not booked Any Slot ...!!!");
   } else{
     //alert("Post Counselling Join...!!!");
     $("#reminderJoinModal").modal('hide');
     var htmltext = "";
         htmltext += '<h3> JOIN NOW <h3>'
                  + '<h5> To gain full advantage of the Fair, you are now requested to attend the post-counseling session! <h5> <br/> <button class="btn btn-md btn-info joinbtn" onclick="joinPostCounselling()" >Join</button></div>';

     document.getElementById("reminder-join-modal").innerHTML = htmltext;
     $("#reminderJoinModal").modal('show');
   }

 }
     });
}



function setCookie(name, value, days) {
 var expires = "";
 if (days) {
   var date = new Date();
   date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
   expires = "; expires=" + date.toUTCString();
 }
 document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function setCookieMinute(name, value, days) {
 var expires = "";
 if (days) {
   var date = new Date();
   date.setTime(date.getTime() + (days * 60 * 1000));
   expires = "; expires=" + date.toUTCString();
 }
 document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
 var nameEQ = name + "=";
 var ca = document.cookie.split(';');
 for (var i = 0; i < ca.length; i++) {
   var c = ca[i];
   while (c.charAt(0) == ' ') c = c.substring(1, c.length);
   if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
 }
 return null;
}
</script>
<?php
if($precounselling_status == 'NO')
{
    ?>
    <script type="text/javascript">
    $(document).ready(function(){
    var precounsellingtoken = 1;
    var increaseCount = parseInt(precounsellingtoken);
    setCookie("precounsellingcount",parseInt(increaseCount),1);

    var htmltext = "";
        htmltext += '<h3> Message <h3>'
                 + '<h5> All students are required to go through a short pre-counseling before meeting the Universities.</h5> <br/> <button class="btn btn-md btn-info joinbtn" onclick="joinPreCounselling()" >Join</button> ';

    document.getElementById("reminder-join-modal").innerHTML = htmltext;
    $("#reminderJoinModal").modal('show');
   });
    </script>
    <?php
}
else
{
    ?>
    <script type="text/javascript">
    <?php
    foreach ($fairlist as $keyfl => $valuefl)
    {
        if($valuefl['book_type'] == 'booked_user_slot' || $valuefl['book_type'] == 'current_user_slot' || $valuefl['book_type'] == 'already_expired' || $valuefl['book_type'] == 'already_attended')
        {
            ?>
            allParticipants[<?=$valuefl['id']?>] = 'booked';
            <?php
        }
        else
        {
            ?>
            allParticipants[<?=$valuefl['id']?>] = 'not-booked';
            <?php
        }
    }
    ?>
    //console.log(allParticipants);
    </script>
    <?php
}
?>
<script type="text/javascript">
$(document).ready(function(){
 //setCookie("slotcount",1,1);

 /*var numberofslotbook = getCookie("slotcount") ? getCookie("slotcount") : 0;
 var increaseCount = parseInt(numberofslotbook) - 1;
 setCookie("slotcount",parseInt(increaseCount),1);*/

 });

 function slotReminderCall(){
   var slotReminder = getCookie("slotcount");

   console.log("hello", slotReminder);

   if( slotReminder != null && slotReminder > 0){
     checkUpcomingSlot();
   }
   else{
       setCookie("slotcount",0,1);
   }
 }

 function precounsellingReminderCall(){
   var precounsellorReminder = getCookie("precounsellingcount");

   console.log("hello", precounsellorReminder);

   if( precounsellorReminder != null && precounsellorReminder > 0){
     checkUpcomingPreCounselling();
   }
   else{
       setCookie("precounsellingcount",0,1);
   }
 }

 function educounsellingReminderCall(){
   var precounsellorReminder = getCookie("educounsellingcount");

   console.log("hello-edu", precounsellorReminder);

   if( precounsellorReminder != null && precounsellorReminder > 0){
     checkUpcomingEduCounselling();
   }
   else{
       setCookie("educounsellingcount",0,1);
   }
 }

 function postcounsellingReminderCall(){
   var precounsellorReminder = getCookie("postcounsellingcount");

   console.log("hello-post", precounsellorReminder);

   if( precounsellorReminder != null && precounsellorReminder > 0){
     checkUpcomingPostCounselling();
   }
   else{
       setCookie("postcounsellingcount",0,1);
   }
 }

 var intervalSlotBooking = setInterval(function () { slotReminderCall(); }, 60000);

 var intervalPrecounselling = setInterval(function () { precounsellingReminderCall(); }, 60000);

 var intervalStatusUpdate = setInterval(function () { participateListUpdate(1); }, 60000);

 var intervalEducounselling = setInterval(function () { educounsellingReminderCall(); }, 60000);

 var intervalPostcounselling = setInterval(function () { postcounsellingReminderCall(); }, 60000);

</script>
<script type="text/javascript">

  var mouseX = 0;
  var mouseY = 0;
  var popupCounter = 0;

   document.addEventListener("mousemove", function(e) {
     mouseX = e.clientX;
     mouseY = e.clientY;
   //  document.getElementById("coordinates").innerHTML = "<br />X: " + e.clientX + "px<br />Y: " + e.clientY + "px";
   });

   setCookieMinute("educounsellingdone",-1,1);

   $(document).mouseleave(function () {
     if (mouseY < 100) {
       if (popupCounter < 1) {
         //alert("Please do not close the tab!");

	 var precounsellingcountcheck = getCookie("precounsellingcount");
         if(precounsellingcountcheck != null && precounsellingcountcheck == 0){

         var postcounsellorDone = getCookie("postcounsellingdone");
         var educounsellorDone = getCookie("educounsellingdone");

         if(educounsellorDone == null || educounsellorDone < 0){

           setCookieMinute("educounsellingdone",1,1);

           $("#reminderJoinModal").modal('hide');
           var htmltext = "";
               htmltext += '<h5> Message <h5>'
                        + '<div> All your Finance / Education Loans concerns can now be resolved by our Finance Team! Please Join Eduloans counseling <button class="btn btn-md btn-info joinbtn" onclick="joinEduCounselling()" >Join</button></div>';

           document.getElementById("reminder-join-modal").innerHTML = htmltext;
           $("#reminderJoinModal").modal('show');



         } else if(postcounsellorDone == null || postcounsellorDone < 0){

           setCookieMinute("postcounsellingdone",1,1);

           $("#reminderJoinModal").modal('hide');
           var htmltext = "";
               htmltext += '<h3> Message <h3>'
                        + '<div> To gain full advantage of the Fair, you are now requested to attend the post-counseling session! Please Join <button class="btn btn-md btn-info joinbtn" onclick="joinPostCounselling()" >Join</button></div>';

           document.getElementById("reminder-join-modal").innerHTML = htmltext;
           $("#reminderJoinModal").modal('show');



         }
	}
       }
       //popupCounter ++;
     }
   });

</script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script type="text/javascript">
   $.fn.jQuerySimpleCounter = function( options ) {
       var settings = $.extend({
           start:  0,
           end:    100,
           easing: 'swing',
           duration: 400,
           complete: ''
       }, options );

       var thisElement = $(this);

       $({count: settings.start}).animate({count: settings.end}, {
       duration: settings.duration,
       easing: settings.easing,
       step: function() {
         var mathCount = Math.ceil(this.count);
         thisElement.text(mathCount);
       },
       complete: settings.complete
     });
   };


   $('#number1').jQuerySimpleCounter({end: <?=$fair_total_registration?>,duration: 3000});
   $('#number2').jQuerySimpleCounter({end: <?=$total_live_student?>,duration: 3000});
   $('#number3').jQuerySimpleCounter({end: <?=$university_live_count?>,duration: 2500});
   $('#number4').jQuerySimpleCounter({end: <?=$total_session?>,duration: 2500});



     /* AUTHOR LINK */
      $('.about-me-img').hover(function(){
             $('.authorWindowWrapper').stop().fadeIn('fast').find('p').addClass('trans');
         }, function(){
             $('.authorWindowWrapper').stop().fadeOut('fast').find('p').removeClass('trans');
         });

</script>
<script type="text/javascript">
   /*Menu-toggle*/
   // $("#menu-toggle").click(function(e) {
   //     e.preventDefault();
   //     $("#wrapper").toggleClass("active");
   // });
   //var offset = 80;
   /*Scroll Spy*/


   $('.scrollSpyArea').scrollspy({ target: '#spy', offset:200});

    headerHeight = $("#header").height() + 5;
    secHeight = $("#imgSec").height() + headerHeight;

   /*Smooth link animation*/
   $('a[href*=#]:not([href=#])').click(function() {
      $('#sidebar-wrapper').css({'top' : headerHeight });
       if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

           var target = $(this.hash);
           target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
           if (target.length) {
               $('html,body').animate({
                   scrollTop: target.offset().top - headerHeight
               }, 1000);
               return false;

           }

       }

   });
   var headerHeight = $(".navbar").height();
   var lastScrollTop = 0;
   $(window).scroll(function(event){
     var st = $(this).scrollTop();
     if (st > lastScrollTop){
         // downscroll code
          $('#sidebar-wrapper').css({'top' : headerHeight });
     } else {
     }
     lastScrollTop = st;
   });
   $(window).scroll(function(){
    if ($(this).scrollTop() == '0') {
        var element = document.getElementById('sidebar-wrapper');
        element.style.top = null;
    }
   });

   $('#slick1').slick({
    rows: 1,
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4
   });

   $('#slick2').slick({
    rows: 5,
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4
   });

   $(document).ready(function(){
   //  var slider = $('#addressesList').slick({
   // });
   $("#joinOne").click(function(){
      //alert("clciked");
    // $("#universityListBox").show();
    $("#universityImageBox").hide();
    document.getElementById("universityListBox").style.display = "block";
    if(document.getElementById("bookedUniversityListBox"))
    {
        document.getElementById("bookedUniversityListBox").style.display = "block";
    }
    // $('#universityListBox').attr('style','display: block !important');
    // $('.slick__slider', context).trigger('resize');
    $('.slick-slider').get(0).slick.setPosition();

   });

   });

</script>

<script type="text/javascript">
    var z = setInterval(function() {
        var random_number = (Math.floor(Math.random() * 1000));
        document.getElementById("number2").innerHTML = random_number;
    }, 5000);
</script>
