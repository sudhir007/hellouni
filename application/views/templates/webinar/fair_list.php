<style>

#errmsg{margin-left:10px; color:red;}

 .shade_border{

background:url(img/shade1.png) repeat-x; height:11px;

}

.uni{

background-color:#f4f4f4; padding:0px; margin:10px;

color:#161616;



}

.uni p{

font-size: 11px;

margin:5px;

}



.favourites{

color:#fff;  background-color:#394553; float:left; margin-left:1px; padding:6px; font-size:11px;

}

.compare{

color:#fff; width:118px; background-color:#394553; float:left; margin-right:1px; padding:6px;font-size:11px;

}

.search_form

{

border:solid 2px #ededed; background:#f6f6f6; padding-bottom:15px;

}

.detail{

background:#f6881f; text-align:center; padding-top:6px; padding-left:11px; height:40px; color:#fff;

}
.my-search{
    font-size: 20px;
font-weight: 600;
margin-bottom: 10px;
}
.my-search-box, .my-search-box div{
    padding: 0px;
}
.my-search-box-heading{
    font-size: 16px;
    font-weight: bold;
}
.my-search-box-text{
    font-size: 14px;
}


.sort-filter {
    font-size: 16px;
    line-height: 1.6;
    margin:0 0 10px;
    color:#000;
}

.font-11 {
    font-size: 11px;
}
.font-16 {
    font-size: 16px !important;
    font-weight: 700 !important;
    letter-spacing: 0.5px !important;
    color: Black !important;
}
.uni-image {
    background: #fff;
    border: 1px solid #f3f3f3;
    padding: 5px;
    position: relative;
    width: 184px;
}
ul.uni-cont li {
    background: #fbfbfb;
    border: 1px solid #f3f3f3;
    margin-bottom: 8px;
    list-style: none;
}
.clearwidth {
    width: 100%;
    float: left;
}
.univ-tab-details {
    margin: 30px 0 10px 0!important;
}

.flLt {
    float: left;
}
.flRt {
    float: right;
}

.uni-box {
    float: left;
    width: 100%;
    padding: 8px;
    position: relative;
    background-color: #fbfbfb;
    /* border: 1px solid #f0f0f0; */
    box-shadow: 2px 2px 4px #ccccc4;
    margin: 20px 0px;
}
.uni-box p {
    margin:0px;
    line-height:unset;
}
.uni-detail {
    margin-left: 208px;
}
.uni-title {
    margin-bottom: 10px;
    width: 90%;
}
.uni-title a {
    font-weight: 600;
    font-size: 13px;
    color: #111111;
}
.uni-title span {
    color: #666666;
}
.uni-sub-title {
    font-weight: 600;
    font-size: 15px;
    color:#666666;
}
a.uni-sub-title {
    color:#666666;
}
.uni-course-details {
    width: 100%;
    border-right: 1px solid #dadada;
    background: #fff;
    /*margin: 8px 0;*/
    padding: 8px;
    position: relative;
    /*margin-right: 25px;*/
}
.uni-course-details:before {
    width: 100%;
    height: 1px;
    right: -1px;
    bottom: -1px;
    background-image: -webkit-gradient(linear,0 100%,0 0,from(#fff),to(#dadada));
    background-image: -webkit-linear-gradient(right,#dadada,#fff);
    background-image: -moz-linear-gradient(right,#dadada,#fff);
    background-image: -o-linear-gradient(#fff,#dadada);
}
.uni-course-details:after, .uni-course-details:before {
    content: "";
    position: absolute;
}
.detail-col {
    width: 145px;
    font-size: 13px;
    color: #111111;
}
.detail-col p {
    margin-bottom: 3px;
}
.detail-col strong {
    color: #666666;
    margin-bottom: 5px;
    display: block;
    font-weight: 400;
}
.btn-brochure a {
    padding: 8px 18px 9px!important;
}
a.button-style {
    padding: 6px 9px;
}
.compare-box {
    font-size: 14px;
    color: #000;
    width: 140px;
    line-height: 15px;
}
.compare-box p{
    display:inline;
    margin:0px;
}

div {
    background: 0 0;
    border: 0;
    margin: 0;
    outline: 0;
    padding: 0;
    vertical-align: baseline;
}
.sponsered-text {
    color: #999;
    font-size: 12px;
    font-style: italic;
    margin: 15px 0 5px 0;
    display: none;
}
.tar {
    text-align: right;
}
.button-style, .login-btn, a.button-style, a.login-btn {
    background: #F6881F;
    border: 1px solid transparent;
    padding: 5px 10px;
    color: #fff;
    font-size: 14px;
    text-decoration: none;
    vertical-align: middle;
    display: inline-block;
}
.add-shortlist, .added-shortlist {
    background-position: 0 -51px;
    width: 78px;
    height: 77px;
    position: absolute;
    top: 0;
    right: 0px;
    cursor: pointer;
}

.cate-sprite {
    background: url(/images/save-fav-btn-white.png);
    display: inline-block;
    font-style: none;
    vertical-align: middle;
}

.saved-fav {
    background: url(/images/save-fav-btn.png);
}


/* The Modal (background) */
.modal {
display: none; /* Hidden by default */
position: fixed; /* Stay in place */
z-index: 1; /* Sit on top */
padding-top: 100px; /* Location of the box */
left: 0;
top: 0;
width: 100%; /* Full width */
height: 100%; /* Full height */
overflow: auto; /* Enable scroll if needed */
background-color: rgb(0,0,0); /* Fallback color */
background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}
/* Modal Content */
.modal-content {
background-color: #fefefe;
margin: auto;
padding: 20px;
border: 1px solid #888;
    width: 40%;
 z-index: 10000;
}
/* The Close Button */
.close {
color: #8e0f0f;
float: right;
font-size: 28px;
font-weight: bold;
}
.close:hover,
.close:focus {
color: #000;
text-decoration: none;
cursor: pointer;
}

#compare-university-modal{
  text-align: center;
}

#compare-university-modal p{
  font-size: 16px;
    font-weight: bold;
  margin: 0 0 15px;
}

#compare-university-modal label{
  color:#000;
  font-size: 13px;
  margin-top: 10px;
  font-weight: 500;
}

.compare_universities_submit{
  text-align: center;
    font-size: 15px;
  margin-top: 10px;
}

#compare-university-modal input[type='submit']{
  padding: 5px;
}

.filterable {
margin-top: 15px;
}
.filterable .panel-heading .pull-right {
    margin-top: -20px;
}
.filterable .filters input[disabled] {
    background-color: transparent;
    border: none;
    cursor: auto;
    box-shadow: none;
    padding: 0;
    height: auto;
}
.filterable .filters input[disabled]::-webkit-input-placeholder {
    color: #333;
}
.filterable .filters input[disabled]::-moz-placeholder {
    color: #333;
}
.filterable .filters input[disabled]:-ms-input-placeholder {
    color: #333;
}

.bannerSection{
  background-color: rgba(0, 0, 0, 0);
    background-repeat: no-repeat;
    background-image: url(<?php echo base_url();?>application/images/webinarBanner.jpg);
    background-size: cover;
    background-position: center center;
    width: 100%;
    height: 170px;
}

.aboutus-section {
    padding: 90px 0;
}
.aboutus-title {
    font-size: 35px;
    letter-spacing: 1.8px;
    /* line-height: 32px; */
    /* margin: 0 0 0px; */
    /* padding: 0 0 11px; */
    position: relative;
    /* text-transform: uppercase; */
    color: #313131;
}

#items_table tbody>tr>th {
   padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: 1px solid #ddd;
    text-align: center;
    font-size: 14px;
    font-weight: 600;
   }

#items_table tbody>tr>td {
   padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: 1px solid #ddd;
    text-align: center;
    font-size: 13px;
    letter-spacing: 0.5px;
   }
</style>

<section class="bannerSection">
         <!-- <img src="<?php echo base_url();?>uploads/banner.jpg" width="100%" height="400px" /> -->
         <!-- <a class="thumbnail" id="carousel-selector-1"><img src="<?php echo base_url();?>application/images/asucampus.jpg" style="height: 90px;min-height: 90px;padding: inherit;"></a> -->
         <div class="container" style="height: 300px;">
            <div class="row">
               <div class="col-md-12">
                  <h1 class="page-top-title" style="margin-top: 50px;color: #ffffff; text-align:center;font-weight:bold;font-size:37px;"><span style="border-radius: 9px;background-color: rgba(30, 32, 33, 0.82)">&nbsp;&nbsp;&nbsp;&nbsp;WEBINAR&nbsp;&nbsp;&nbsp;&nbsp;</span></h1>
               </div>
            </div>
         </div>
      </section>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- Main content -->
        <section class="content">
          <div class="container" style="margin-top:20px;">
          <div class="row">
              <div class="col-lg-12" style="color:#000;">

              <div class="aboutus" style="text-align: center;">
                <h1 class="aboutus-title"> <?=$fair_name?></h1><br>
                <h3> Total Registraion : <?=$fair_total_registration?> <button  style='text-align: center; border-radius: 10px; box-shadow: 2px 2px 2px #c7c7c7c2; ' class='btn btn-md btn-info'  onclick='participateListUpdate(<?=$fair_id?>)'><b><i class='fa fa-sign-in' style='font-size:15px;color:white;'>&nbsp;</i> Refresh Status </b></span></>
              </div>

              <div class="uni-box">
              <div class="col-md-12" style="color:#000;">
                <div class="col-md-6">
                  <label for="inputEmail3" class="text-uppercase">Pre-Counselling With Top Management : </label>
                </div>
                  <div class="col-md-6">
                  <?php if($precounselling_available == 'YES'){?>
                  <button  style='text-align: center; border-radius: 10px; box-shadow: 2px 2px 2px #c7c7c7c2; ' class='btn btn-md btn-info'  onclick='joinPreCounselling()'><b><i class='fa fa-sign-in' style='font-size:15px;color:white;'>&nbsp;</i>Join Now</b></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <?php } else {?>
                  <button  style='text-align: center; border-radius: 10px; box-shadow: 2px 2px 2px #c7c7c7c2; ' class='btn btn-md btn-info'  onclick='joinQueue()'><b><i class='fa fa-sign-in' style='font-size:15px;color:white;'>&nbsp;</i> Join Queue </b></span>
                  <?php } ?>
                   </div>
              </div>
            </div>

            <div id="items_div">
          <?php
  foreach($fairlist as $webinar_list){
          ?>

          <div class="uni-box">
                  <div class="flLt">
                      <div class="uni-image">
                          <a target="_blank" href="#">
                            <img class="lazy" data-original="/uploads/texas_arlington/banner_3.jpg" alt="<?=$webinar_list['university_name']?>" title="<?=$webinar_list['university_name']?>" align="center" width="172" height="115" src="/uploads/texas_arlington/banner_3.jpg" style="display: inline;"></a>
                          <div class="uni-shrtlist-image">
                          </div>
                      </div>
                  </div>
                  <div class="uni-detail">

                              <!-- <a target="_blank" href="#" id="university-2" class="font-16"><h4><?=$webinar_list['name']?></h4></a> -->





                      <div class="course-touple clearwidth">

                          <div class="clearwidth">
                              <div class="uni-course-details flLt">
                                <div class="col-md-12">
                                    <div class="row">
                                      <a target="_blank" href="#" id="university-2" class="font-16"><h4 style="margin: 0px 0px 4px 0px !important;"><?=$webinar_list['university_name']?></h4></a>
                                    </div>
                                    <div class="row">

                                      <div class="col-md-3">

                                        <h5 class="card-title" style="margin: 8px 0 8px;font-weight: 700;text-transform: none;letter-spacing: 1px;color: black; line-height: 1.5; display: inline;">
                                          <font color="#f9992f"><i class="fa fa-users" style="font-size:15px;color:#374552">&nbsp;</i>
                                            <b>Intrested Student :  </b></font> <b><?php echo $webinar_list['default_total_student'] + $webinar_list['total_registrations'] ; ?></b></h5>

                                      </div>


                                        <?php if($webinar_list['book_type'] == 'booked_user_slot'){ ?>
                                        <div class="col-md-4">
                                          <h5 class="card-title" style="margin: 8px 0 8px;font-weight: 700;text-transform: none;letter-spacing: 1px;color: black;     line-height: 1.5;"><font color="#f9992f"><i class="fa fa-check-square-o" style="font-size:15px;color:#374552">&nbsp;</i>
                                            <b> YOUR Booked Time Slot : </b></font> <b><?php $userSlotTime = date('D, d F Y - h:i:s a', strtotime($webinar_list['fair_start_time'])); echo $userSlotTime; ?></b>
                                          </h5>
                                        </div>
                                      <?php } ?>

                                      <?php if($webinar_list['book_type'] == 'current_user_slot' || $webinar_list['book_type'] == 'university_active_slot'){ ?>
                                      <div class="col-md-4">
                                        <h5 class="card-title" style="margin: 8px 0 8px;font-weight: 700;text-transform: none;letter-spacing: 1px;color: black;     line-height: 1.5;"><font color="#f9992f"><i class="fa fa-check-square-o" style="font-size:15px;color:#374552">&nbsp;</i>
                                          <b> Start Time Slot : </b></font> <b><?php $userSlotTime = date('D, d F Y - h:i:s a', strtotime($webinar_list['fair_start_time'])); echo $userSlotTime; ?></b>
                                        </h5>
                                      </div>
                                    <?php } ?>

                                    <?php if($webinar_list['book_type'] == 'university_inactive_slot'){ ?>
                                    <div class="col-md-4">
                                      <h5 class="card-title" style="margin: 8px 0 8px;font-weight: 700;text-transform: none;letter-spacing: 1px;color: black;     line-height: 1.5;"><font color="#f9992f"><i class="fa fa-check-square-o" style="font-size:15px;color:#374552">&nbsp;</i>
                                        <b> Start Time Slot : </b></font> <b><?php $userSlotTime = date('D, d F Y - h:i:s a', strtotime($webinar_list['fair_start_time'])); echo $userSlotTime; ?></b>
                                      </h5>
                                    </div>
                                  <?php } ?>

                                        <div class="col-md-3">
                                        </div>
                                        <div class="col-md-4">
                                        </div>
                                        <div class="col-md-2 btnMobile" style="margin-top: 6px;text-align: right;">
                                        <!--<a href="<?php echo $url; ?>" class='btn btn-md btn-info' target="_blank" style="border-radius: 10px; box-shadow: 2px 2px 2px #c7c7c7c2;"><i class="fa fa-google" style="font-size:15px;color:white;">&nbsp;</i>
                                        <b>Add to gCal</b> </a>-->
                                      </div>
                                    </div>
                                    <div class="row" style="margin-top: 3px;">
                                        <div class="col-md-3">
                                          <!--<p><i class="fa fa-calendar" style="font-size:15px;color:#374552">&nbsp;</i><b>Date  : </b></p>
                                          <p><?php $webinarDate = date('D, d F Y', strtotime($webinar_list['start_time'])); echo "$webinarDate";?></p>-->
                                        </div>

                                        <div class="col-md-3">
                                          <!--<p><i class="fa fa-clock-o" style="font-size:15px;color:#374552">&nbsp;</i><b>Time : </b></p>
                                          <p><?php $webinarTime = date('h:i:s a', strtotime($webinar_list['start_time'])); echo "$webinarTime IST";?></p>-->
                                        </div>
                                        <div class="col-md-6 btnMobile" style="text-align: right;padding-right: 0px; margin: inherit;">

                                        <?php if ($webinar_list['button_name'] == 'JOIN' && $webinar_list['button_type'] == 'ENABLED') {
                                                  echo "<button  style='text-align: center; border-radius: 10px; box-shadow: 2px 2px 2px #c7c7c7c2; ' class='btn btn-md btn-info'  onclick='joinUniversity(".$webinar_list['id'].")'><b><i class='fa fa-sign-in' style='font-size:15px;color:white;'>&nbsp;</i>Join</b></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                                } else if ($webinar_list['button_name'] == 'JOIN' && $webinar_list['button_type'] == 'DISABLED') {
                                                  echo "<button  style='text-align: center; border-radius: 10px; box-shadow: 2px 2px 2px #c7c7c7c2; ' class='btn btn-md btn-info'  onclick='joinUniversity(".$webinar_list['id'].")' disabled><b><i class='fa fa-sign-in' style='font-size:15px;color:white;'>&nbsp;</i>Join</b></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                                  echo "<button  style='text-align: center; border-radius: 10px; box-shadow: 2px 2px 2px #c7c7c7c2; ' class='btn btn-md btn-info'  onclick='deleteSlot(".$webinar_list['id'].")'><b><i class='fa fa-sign-in' style='font-size:15px;color:white;'>&nbsp;</i> Delete Slot </b></span>";
                                                } else if($webinar_list['button_name'] == 'BOOK' && $webinar_list['button_type'] == 'ENABLED'){
                                                echo "<button  style='text-align: center; border-radius: 10px; box-shadow: 2px 2px 2px #c7c7c7c2;' class='btn btn-md btn-warning '  onclick='slotOption(".$webinar_list['id'].")'><b><i class='fa fa-check-circle' style='font-size:15px;color:white;'>&nbsp;</i>Book Slot</b></button>";
                                              }
                                        ?>


                                      </div>
                                    </div>

                                </div>

                              </div>

                          </div>
                      </div>

                  </div>
              </div>

            <?php
              }
              ?>
              </div>
        </div>

    </div>
  </div>

          </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <div id="slotListModal" class="modal">
      <!-- Modal content style="display: block;"-->
         <div class="modal-content" style="width: 45%;">
            <span class="close closeSlotList">&times;</span>
            <div id="slot-list-modal" style="text-align: center;">

              <h3> Available Slots </h3>

              <table class="table" id="items_table" border="1">

             </table>

            </div>
         </div>


      </div>

      <script type="text/javascript">

  $( document ).ready(function() {

  $('#desired_mainstream').on('change', function() {

      var dataString = 'id='+this.value;

      $.ajax({

      type: "POST",

      url: "/search/university/getSubcoursesList",

      data: dataString,

      cache: false,

      success: function(result){

       $('#desired_subcourse').html(result);


      }

           });

     });

   })

   // sent OTP
   function fairapply(wid) {


        var stype = "loggedIn";

          var dataString = 'source=' + stype + '&fair_id=' + wid;

          $.ajax({

          type: "POST",

          url: "/webinar/webinarmaster/fairapply",

          data: dataString,

          cache: false,

          success: function(result){

           //alert(result);
           window.location.href=result;

          }
              });
      }

      // sent OTP
      function applySlot(sid,pid) {


           var stype = "fair";

             var dataString = 'source=' + stype + "&slot_id=" + sid +"&participant_id=" + pid;

             $.ajax({

             type: "POST",

             url: "/webinar/webinarmaster/applySlot",

             data: dataString,

             cache: false,

             success: function(result){

              //alert(result);
              if(result == "NOTLOGGEDIN"){
                window.location.href="/user/account";
              } else if(result == "SLOTFULL"){
                //$("#slotListModal").modal('hide');
                alert("SLOT FULL");
              } else if(result == "SUCCESS"){

                var numberofslotbook = getCookie("slotcount") ? getCookie("slotcount") : 0;
                var increaseCount = parseInt(numberofslotbook) + 1;
                setCookie("slotcount",parseInt(increaseCount),1);

                $("#slotListModal").modal('hide');
                alert("Slot Booked...");
                location.reload();
                //window.location.href="/university/virtualfair/list";
              } else {
                alert(result);
              }

             }
                 });
         }

         function deleteSlot(pid) {


              var stype = "fair";

                var dataString = 'source=' + stype + "&participant_id=" + pid;

                $.ajax({

                type: "POST",

                url: "/webinar/webinarmaster/deleteSlot",

                data: dataString,

                cache: false,

                success: function(result){

                 //alert(result);
                 if(result == "NOTLOGGEDIN"){

                   window.location.href="/user/account";

                 } else if(result == "DELETED"){

                   var numberofslotbook = getCookie("slotcount") ? getCookie("slotcount") : 0;
                   var increaseCount = parseInt(numberofslotbook) - 1;
                   setCookie("precounselling",parseInt(increaseCount),1);

                   alert("SLOT DELETED. APPLY FOR NEW SLOT.");
                   location.reload();
                   //window.location.href="/university/virtualfair/list";

                 } else if(result == "NODATAAVAILABLE"){

                   alert("YOU HAVE NOT APPLIED FOR THIS UNIVERSITY...!!!");

                 }

                }
                    });
            }

         var spanslotlist = document.getElementsByClassName("closeSlotList")[0];
         spanslotlist.onclick = function() {
           $("#slotListModal").modal('hide');
         }

         function slotOption(pid) {

                var dataString = "participant_id=" + pid;

                $.ajax({

                type: "POST",

                url: "/webinar/webinarmaster/availableSlots",

                data: dataString,

                cache: false,

                success: function(result){

                  if(result.slot_available == "NO"){

                    alert("Slots Are Full..");

                  } else {

                    var slotlist = result.slot_list;

                    if(slotlist.length){

                      var htmlTextList = "<table class='table' border='1'><tr><th> Slot Start Time </th><th> Slot End Time </th><th>Action</th></tr></table>";

                      slotlist.forEach(function(value, index){

                        htmlTextList += '<tr><td>' + value.start_time + '</td><td>'+ value.end_time + '</td><td><button style="text-align: center; border-radius: 10px; box-shadow: 2px 2px 2px #c7c7c7c2;" class="btn btn-md btn-info"  onclick="applySlot('+value.id+','+value.participant_id+')"><b><i class="fa fa-sign-in" style="font-size:15px;color:white;">&nbsp;</i> Apply </b></button></td></tr>';

                      });

                      //modal show fix view
                      $("#slotListModal").modal('show');

                      document.getElementById("items_table").innerHTML = htmlTextList;

                    }
                  }

                }
                    });
            }

            function joinPreCounselling(){

              var dataString = "participant_id=" + 0;

              $.ajax({

              type: "POST",

              url: "/webinar/webinarmaster/joinPreCounselling",

              data: dataString,

              cache: false,

              success: function(result){

                if(result == "NOTAVAILABLE"){
                  alert("Counsellors Are Busy...!!!");
                } else{
                  //console.log(result);
                  //window.location.href=result;
                  //alert(result);
                  //var win = window.open(result, '_blank');
                  var win = window.open(result, 'vfair');
                  win.focus();
                }
              }
                  });

            }

            function joinQueue(){

              var dataString = "participant_id=" + 0;

              $.ajax({

              type: "POST",

              url: "/webinar/webinarmaster/bookPreCounselling",

              data: dataString,

              cache: false,

              success: function(result){

                if(result == "SUCCESS"){
                  alert(result);
                } else{
                  alert(result);
                }

              }
                  });
            }

            function joinUniversity(pid){

              var dataString = "participant_id=" + pid;

              $.ajax({

              type: "POST",

              url: "/webinar/webinarmaster/joinUniversityFair",

              data: dataString,

              cache: false,

              success: function(result){

                if(result == "NOTAVAILABLE"){
                  alert("Counsellors Are Busy...!!!");
                } else{
                  //window.location.href=result;
                  //alert(result);

                  var numberofslotbook = getCookie("slotcount") ? getCookie("slotcount") : 0;
                  var increaseCount = parseInt(numberofslotbook) - 1;
                  setCookie("slotcount",parseInt(increaseCount),1);


                  var win = window.open(result, 'vfair');
                  win.focus();
                }

              }
                  });
            }

          function  participateListUpdate(fid){

            var dataString = "fair_id="+fid;

            $.ajax({

            type: "POST",

            url: "/webinar/webinarmaster/virtualFairParticipantsListRefresh",

            data: dataString,

            cache: false,

            success: function(result){

              var fairlist = result.fairlist;

              if(fairlist.length){

                var htmlFairList = "";

                fairlist.forEach(function(value, index){

                  var studentCount = 0;

                      studentCount = parseInt(value.default_total_student) + parseInt(value.total_registrations);

                  var timeView = '';
                  if(value.book_type == 'booked_user_slot'){
                      timeView += '<div class="col-md-4">'
                                +'<h5 class="card-title" style="margin: 8px 0 8px;font-weight: 700;text-transform: none;letter-spacing: 1px;color: black;     line-height: 1.5;"><font color="#f9992f"><i class="fa fa-check-square-o" style="font-size:15px;color:#374552">&nbsp;</i>'
                                +'<b> YOUR Booked Time Slot AJAX: </b></font> <b>'+ value.fair_start_time +'</b>'
                                +'</h5>'
                                + '</div>';
                  } else {
                    timeView += '<div class="col-md-4">'
                              +'<h5 class="card-title" style="margin: 8px 0 8px;font-weight: 700;text-transform: none;letter-spacing: 1px;color: black;     line-height: 1.5;"><font color="#f9992f"><i class="fa fa-check-square-o" style="font-size:15px;color:#374552">&nbsp;</i>'
                              +'<b> Start Time Slot AJAX : </b></font> <b>'+ value.fair_start_time +'</b>'
                              +'</h5>'
                              + '</div>';
                  }

                  var buttonType = '';
                  if(value.button_name == 'JOIN' && value.button_type == 'ENABLED')
                  {
                      buttonType += "<button  style='text-align: center; border-radius: 10px; box-shadow: 2px 2px 2px #c7c7c7c2; ' class='btn btn-md btn-info'  onclick='joinUniversity("+value.id+")'><b><i class='fa fa-sign-in' style='font-size:15px;color:white;'>&nbsp;</i>Join</b></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                  }
                  else if(value.button_name == 'JOIN' && value.button_type == 'DISABLED')
                  {
                      buttonType += "<button  style='text-align: center; border-radius: 10px; box-shadow: 2px 2px 2px #c7c7c7c2; ' class='btn btn-md btn-info'  onclick='joinUniversity("+value.id+")' disabled><b><i class='fa fa-sign-in' style='font-size:15px;color:white;'>&nbsp;</i>Join</b></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                  +"<button  style='text-align: center; border-radius: 10px; box-shadow: 2px 2px 2px #c7c7c7c2; ' class='btn btn-md btn-info'  onclick='deleteSlot("+value.id+")'><b><i class='fa fa-sign-in' style='font-size:15px;color:white;'>&nbsp;</i> Delete Slot </b></span>";
                  } else if(value.button_name == 'BOOK' && value.button_type == 'ENABLED'){
                      buttonType +="<button  style='text-align: center; border-radius: 10px; box-shadow: 2px 2px 2px #c7c7c7c2;' class='btn btn-md btn-warning '  onclick='slotOption("+value.id+")'><b><i class='fa fa-check-circle' style='font-size:15px;color:white;'>&nbsp;</i>Book Slot</b></button>"
                  }

                  htmlFairList +='<div class="uni-box">'
                          +'<div class="flLt">'
                              +'<div class="uni-image">'
                                  +'<a target="_blank" href="#">'
                                    +'<img class="lazy" data-original="/uploads/texas_arlington/banner_3.jpg" alt="" title="" align="center" width="172" height="115" src="/uploads/texas_arlington/banner_3.jpg" style="display: inline;"></a>'
                                  +'<div class="uni-shrtlist-image">'
                                  +'</div>'
                              +'</div>'
                          +'</div>'
                          +'<div class="uni-detail">'
                          +'<div class="course-touple clearwidth">'
                          +'<div class="clearwidth">'
                              +'<div class="uni-course-details flLt">'
                                +'<div class="col-md-12">'
                                    +'<div class="row">'
                                      +'<a target="_blank" href="#" id="university-2" class="font-16"><h4 style="margin: 0px 0px 4px 0px !important;">'+value.university_name+'</h4></a>'
                                    +'</div>'
                                    +'<div class="row">'
                                      +'<div class="col-md-3">'
                                        +'<h5 class="card-title" style="margin: 8px 0 8px;font-weight: 700;text-transform: none;letter-spacing: 1px;color: black; line-height: 1.5; display: inline;">'
                                          +'<font color="#f9992f"><i class="fa fa-users" style="font-size:15px;color:#374552">&nbsp;</i>'
                                            +'<b>Intrested Student :  </b></font> <b>'+ studentCount +'</b></h5>'
                                      +'</div>'
                                      + timeView
                                      +'<div class="col-md-3">'
                                        +'</div>'
                                        +'<div class="col-md-4">'
                                        +'</div>'
                                        +'<div class="col-md-2 btnMobile" style="margin-top: 6px;text-align: right;">'
                                      +'</div>'
                                    +'</div>'
                                    +'<div class="row" style="margin-top: 3px;">'
                                        +'<div class="col-md-3"> '
                                        +'</div>'
                                        +'<div class="col-md-3">  '
                                          +'</div>'
                                          +'<div class="col-md-6 btnMobile" style="text-align: right;padding-right: 0px; margin: inherit;">'
                                          + buttonType
                                          +'</div>'
                                    +'</div>'
                                +'</div>'
                              +'</div>'
                          +'</div>'
                      +'</div>'
                  +'</div>'
              +'</div>'


                });

                document.getElementById("items_div").innerHTML = htmlFairList;

            }

          }
                });
          }

     </script>

     <script type="text/javascript">


     function checkUpcomingSlot(){

       var dataString = "user_id=" + 0;

       $.ajax({

       type: "POST",

       url: "/webinar/webinarmaster/bookedSlotReminder",

       data: dataString,

       cache: false,

       success: function(result){

         if(result == "NOTLOGGEDIN"){
           window.location.href = '/user/account';
         } else if( result == "NOJOIN"){
           alert("Not booked Any Slot ...!!!");
         } else{
           alert("upcoming slot Info JOIN now");
         }

       }
           });
     }

     function setCookie(name, value, days) {
       var expires = "";
       if (days) {
         var date = new Date();
         date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
         expires = "; expires=" + date.toUTCString();
       }
       document.cookie = name + "=" + (value || "") + expires + "; path=/";
     }

     function getCookie(name) {
       var nameEQ = name + "=";
       var ca = document.cookie.split(';');
       for (var i = 0; i < ca.length; i++) {
         var c = ca[i];
         while (c.charAt(0) == ' ') c = c.substring(1, c.length);
         if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
       }
       return null;
     }
     </script>

     <script type="text/javascript">
     $(document).ready(function(){
       setCookie("slotcount",1,1);

       /*var numberofslotbook = getCookie("slotcount") ? getCookie("slotcount") : 0;
       var increaseCount = parseInt(numberofslotbook) - 1;
       setCookie("slotcount",parseInt(increaseCount),1);*/

       });

       function slotReminderCall(){
         var slotReminder = getCookie("slotcount");

         console.log("hello", slotReminder);

         if( slotReminder != null && slotReminder !=0){
           checkUpcomingSlot();
         }
       }

       //var intervalSlotBooking = setInterval(function () { slotReminderCall(); }, 60000);

       //var intervalStatusUpdate = setInterval(function () { participateListUpdate(1); }, 60000);

     </script>
