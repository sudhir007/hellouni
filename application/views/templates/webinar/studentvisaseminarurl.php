<style type="text/css">
    @import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");
.login-block{
    background: #004b7a;  /* fallback for old browsers */
background: -webkit-linear-gradient(to bottom, #fafafb, #d8d9da);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to bottom, #fafafb, #d8d9da); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
float:left;
width:100%;
padding : 50px 0;
}
.banner-sec{background:url(<?php echo base_url();?>application/images/register.png)  no-repeat left bottom; background-size:cover; min-height:600px; border-radius: 0 10px 10px 0; padding:0;background-position: center;    margin-top: 25px;}

.login-sec{padding: 50px 30px; position:relative;}
.login-sec .copy-text{position:absolute; width:80%; bottom:20px; font-size:13px; text-align:center;}
.login-sec h2{margin-bottom:30px; font-weight:800; font-size:30px; color: #004b7a;}
.login-sec h2:after{content:" "; width:100px; height:5px; background:#f9992f; display:block; margin-top:20px; border-radius:3px; margin-left:auto;margin-right:auto}
.btn-login{background: #f9992f; color:#fff; font-weight:600;}
.form-control{
  display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}
</style>

<section class="login-block" id="forgot-password-page">
    <div class="container" style="background:#fff; border-radius: 10px; box-shadow: 5px 5px 0px rgba(0,0,0,0.1);">
    <div class="row">
        <div class="col-md-6 login-sec">
            <h2 class="text-center">Visa Seminar Urls</h2>

            <div class="col-md-12 form-group">
             <div class="col-md-12">
               <label for="inputEmail3" class="text-uppercase">Student Visa Seminar Link DISPLAY HERE <button onclick="copyFunction()">Copy URL</button></label>
               <textarea class="form-control" id="resultdata" style="font-size:12px;" rows="4" cols="50"> </textarea>
             </div>
           </div>

            <form class="login-form" class="form-horizontal" onsubmit="resgistration()" method="post">


               <div class="col-md-12 form-group">
                <div class="col-md-12">
                    <label for="inputEmail3" class="text-uppercase">Student Email</label>
                    <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email">
                  </div>

                </div>

                <div class="col-md-12 form-group">
                    <div class="col-md-12">
                     <label for="inputEmail3" class="text-uppercase">Student Mobile Number</label>
                     <input type="text" class="form-control" id="user_mobile" name="user_mobile" placeholder="Mobile Number" >
                    </div>

                  </div>

                     <div class="col-md-12 form-group">
                      <div class="col-md-12">
                        <label for="inputEmail3" class="text-uppercase"> Type &nbsp;&nbsp; </label>

                         <select name="user_type" id="user_type" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required >

                           <option value=""> Select Type </option>
                           <option value="STUDENT">STUDENT</option>
                           <option value="PARENT1">PARENT 1</option>
                           <option value="PARENT2">PARENT 2</option>

                         </select>
                       </div>
                    </div>

              <div class="form-check" style="text-align: center;">
                <!-- <button type="submit" class="btn btn-login" ng-click="submitForm()">Submit</button> -->
                 <button type="button" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                  <input type="submit"  style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="GET URL"></input>
              </div>


            </form>
        </div>
        <div class="col-md-6 banner-sec">
           <!--  <ul>
              <li>kjbkjbkjbkjbbbkjbkj kjbjkjnkj kjnkjnjk kjn</li>
                            <li>kjbkjbkjbkjbbbkjbkj kjbjkjnkj kjnkjnjk kjn</li>

              <li>kjbkjbkjbkjbbbkjbkj kjbjkjnkj kjnkjnjk kjn</li>

            </ul> -->
        </div>
    </div>
</div>
</section>




      <script type="text/javascript">

 	$( document ).ready(function() {

 	$('#desired_mainstream').on('change', function() {

 			var dataString = 'id='+this.value;

 			$.ajax({

 			type: "POST",

 			url: "/search/university/getSubcoursesList",

 			data: dataString,

 			cache: false,

 			success: function(result){

 			 $('#desired_subcourse').html(result);


 			}

           });



 	   });

         // verify otp_number
         $('#verifyotp').on('click', function() {

              var mobileNumber = $('#user_mobile').val();
              var otpNumber = $('#otp_number').val();

        			var dataString = 'mobile_number=' + mobileNumber + '&otp=' + otpNumber;

        			$.ajax({

        			type: "POST",

        			url: "/webinar/registration/verifyotp",

        			data: dataString,

        			cache: false,

        			success: function(result){

        			 alert(result);

        			}

                  });

        	   });

             // verify resgistrationold
             $('#resgistrationold').on('click', function() {

                  var user_name = $('#user_name').val();
                  var user_email = $('#user_email').val();
                  var user_mobile = $('#user_mobile').val();
                  var desired_country = $('#desired_country').val();
                  var desired_mainstream = $('#desired_mainstream').val();
                  var desired_subcourse = $('#desired_subcourse').val();
                  var desired_levelofcourse = $('#desired_levelofcourse').val();
                  var userPassword = $('#password').val();

            			var dataString = 'user_name=' + user_name + '&user_email=' + user_email + '&user_mobile=' + user_mobile + '&desired_country=' + desired_country + '&desired_mainstream=' + desired_mainstream + '&desired_subcourse=' + desired_subcourse + '&desired_levelofcourse=' + desired_levelofcourse + '&user_password=' + userPassword ;

            			$.ajax({

            			type: "POST",

            			url: "/webinar/registration/registrationsignup",

            			data: dataString,

            			cache: false,

            			success: function(result){


                   if(result == "SUCCESS"){
                     alert("Welcome To HelloUni..!!!");
                     window.location.href='/webinar/webinarmaster/webinarlist';
                   } else {
                     alert(result)
                   }

            			}

                      });

            	   });

   })


   //registration

   function resgistration(){


     var user_email = $('#user_email').val();
     var user_mobile = $('#user_mobile').val() ? $('#user_mobile').val() : 'NAA';
     var user_type = $('#user_type').val();
     var link_number = 2;

     var visaurl = "/student/visa/seminarlink";

     if(user_type == 'STUDENT'){
        visaurl = "/student/visa/seminarlink";
     } else {
        visaurl = "/student/visa/seminarparentlink";
        link_number = user_type == "PARENT1" ? 2 : 3;
     }


     var dataString = 'user_email=' + user_email + '&user_mobile=' + user_mobile + '&link_number=' + link_number ;
     event.preventDefault();

     $.ajax({

     type: "POST",

     url: visaurl,

     data: dataString,

     cache: false,

     success: function(result){

       $('#resultdata').val(result);

     }

         });

   }

      function copyFunction() {
  var copyText = document.getElementById("resultdata");
  copyText.select();
  copyText.setSelectionRange(0, 99999)
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}

     </script>
