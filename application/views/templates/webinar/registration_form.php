<style type="text/css">
    @import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");
.login-block{
    background: #004b7a;  /* fallback for old browsers */
background: -webkit-linear-gradient(to bottom, #fafafb, #d8d9da);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to bottom, #fafafb, #d8d9da); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
float:left;
width:100%;
padding : 50px 0;
}
.banner-sec{background:url(<?php echo base_url();?>application/images/register.png)  no-repeat left bottom; background-size:cover; min-height:600px; border-radius: 0 10px 10px 0; padding:0;background-position: center;    margin-top: 25px;}

.login-sec{padding: 50px 30px; position:relative;}
.login-sec .copy-text{position:absolute; width:80%; bottom:20px; font-size:13px; text-align:center;}
.login-sec h2{margin-bottom:30px; font-weight:800; font-size:30px; color: #004b7a;}
.login-sec h2:after{content:" "; width:100px; height:5px; background:#f9992f; display:block; margin-top:20px; border-radius:3px; margin-left:auto;margin-right:auto}
.btn-login{background: #f9992f; color:#fff; font-weight:600;}
.form-control{
  display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}
</style>

<section class="login-block" id="forgot-password-page">
    <div class="container" style="background:#fff; border-radius: 10px; box-shadow: 5px 5px 0px rgba(0,0,0,0.1);">
    <div class="row">
        <div class="col-md-6 login-sec">
            <h2 class="text-center">Registration For Webinar</h2>
            <form class="login-form" class="form-horizontal" onsubmit="resgistration()" method="post">
              <div class="col-md-12 form-group">
                <div class="col-md-12">
                  <label for="inputEmail3" class="text-uppercase">Name</label>

                    <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Name" required>
                    <input type="hidden" class="form-control" id="webinar_id" name="webinar_id" value="<?=$webinar_id?>">
               </div>
              </div>

               <div class="col-md-12 form-group">
                <div class="col-md-6">
                    <label for="inputEmail3" class="text-uppercase">Email</label>
                    <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email" required>
                  </div>

                  <div class="col-md-6">
                      <label for="inputEmail3" class="text-uppercase">Login ID</label>
                      <input type="text" class="form-control" id="user_id" name="user_id" placeholder="Login Id" required>
                    </div>

                </div>

                <div class="col-md-12 form-group">
                    <div class="col-md-6">
                     <label for="inputEmail3" class="text-uppercase">Mobile Number</label>
                     <input type="text" class="form-control" id="user_mobile" name="user_mobile" placeholder="Mobile Number"  onfocusout="sendOTP()" required>
                    </div>
                    <div class="col-md-3">
                    <label for="inputEmail3" class="text-uppercase">OTP</label><br>
                      <input type="text" class="form-control" id="otp_number" name="otp_number" placeholder="OTP Sent On Mobile ">
                    </div>
                    <div class="col-md-2">
                       <button type="button" class="btn btn-primary" id="verifyotp" style="margin-top: 25px;">Verify</button>
                    </div>

                  </div>

                  <div class="col-md-12 form-group">
                    <div class="col-md-6">
                      <label for="inputEmail3" class="text-uppercase"> Desired Country : &nbsp;&nbsp; </label>
                       <select name="desired_country" id="desired_country" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>

                        <option value="0">Select Country</option>

                        <?php foreach($countries as $country){?>

                          <option value="<?php echo $country->name;?>" <?php if(isset($selected_country) && $selected_country==$country->name){echo 'selected="selected"';}?> ><?php echo strtoupper($country->name);?></option>

                        <?php } ?>

                        <!--<option value="India">INDIA</option>-->

                      </select>
                    </div>
                    <div class="col-md-6">
                        <label for="inputEmail3" class="text-uppercase"> Desired Mainstream &nbsp;&nbsp; </label>

                       <select name="desired_mainstream" id="desired_mainstream" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>

                         <option value="0">Select Mainstream</option>

                         <?php foreach($mainstreams as $course){?>

                         <option value="<?php echo $course->id;?>"><?php echo strtoupper($course->name);?></option>

                         <?php } ?>

                       </select>
                    </div>
                  </div>


                   <div class="col-md-12 form-group">
                    <div class="col-md-6">
                       <label for="inputEmail3" class="text-uppercase"> Desired Substream &nbsp;&nbsp; </label>

                        <select name="desired_subcourse" id="desired_subcourse" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>

                          <option value="0">Select Substream</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                       <label for="inputEmail3" class="text-uppercase"> Desired Level Of Course &nbsp;&nbsp; </label>

                             <select name="desired_levelofcourse" id="desired_levelofcourse" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>

                               <option value="0">Select Level Of Course</option>

                                             <?php foreach($degrees as $degree){ ?>

                             <option value="<?php echo $degree->id;?>" ><?php echo strtoupper($degree->name);?></option>

                             <?php } ?>

                             </select>
                    </div>


                    </div>

                     <div class="col-md-12 form-group">
                      <div class="col-md-12">
                      <label for="inputPassword3" class="text-uppercase">Password</label>

                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                     </div>
                    </div>

              <div class="form-check" style="text-align: center;">
                <!-- <button type="submit" class="btn btn-login" ng-click="submitForm()">Submit</button> -->
                 <button type="button" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                  <input type="submit"  style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Register"></input>
              </div>


            </form>
        </div>
        <div class="col-md-6 banner-sec">
           <!--  <ul>
              <li>kjbkjbkjbkjbbbkjbkj kjbjkjnkj kjnkjnjk kjn</li>
                            <li>kjbkjbkjbkjbbbkjbkj kjbjkjnkj kjnkjnjk kjn</li>

              <li>kjbkjbkjbkjbbbkjbkj kjbjkjnkj kjnkjnjk kjn</li>

            </ul> -->
        </div>
    </div>
</div>
</section>


      <!-- Content Wrapper. Contains page content -->
      <!-- <div class="content-wrapper">

        <section class="content">
          <div class="row">

            <div class="col-md-10">
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title" style="text-align: center">Register For Webinar</h3>
                </div>


                <form class="form-horizontal" onsubmit="resgistration()" method="post">

                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label">Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Name" required>
                      </div>
                    </div>

					         <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label">Email</label>
                      <div class="col-sm-8">
                        <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email" required>
                      </div>
                    </div>

                    <div class="form-group">
                       <label for="inputEmail3" class="col-sm-4 control-label">Mobile Number</label>
                       <div class="col-sm-8">
                         <input type="text" class="form-control" id="user_mobile" name="user_mobile" onfocusout="sendOTP()" placeholder="Mobile Number"  required>
                       </div>
                     </div>

                     <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">OTP</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control" id="otp_number" name="otp_number" placeholder="Sent OTP On Mobile ">
                        </div>
                        <div class="col-sm-4">
                          <button type="button" class="btn btn-warning" id="verifyotp" >Varify Mobile</button>
                        </div>
                      </div>

                     <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label"> Desired Country </label>
                        <div class="col-sm-8">
                          <select name="desired_country" id="desired_country" class="searchoptions" style="border:none; width:60%; height:30px; font-size:12px;" required>

                            <option value="0">Select Country</option>

                            <?php foreach($countries as $country){?>

                              <option value="<?php echo $country->name;?>" <?php if(isset($selected_country) && $selected_country==$country->name){echo 'selected="selected"';}?> ><?php echo strtoupper($country->name);?></option>

                            <?php } ?>


                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label"> Desired Mainstream </label>
                         <div class="col-sm-8">
                           <select name="desired_mainstream" id="desired_mainstream" class="searchoptions" style="border:none; width:60%; height:30px; font-size:12px;" required>

                             <option value="0">Select Mainstream</option>

                             <?php foreach($mainstreams as $course){?>

                             <option value="<?php echo $course->id;?>"><?php echo strtoupper($course->name);?></option>

                             <?php } ?>

                           </select>
                         </div>
                       </div>

                       <div class="form-group">
                          <label for="inputEmail3" class="col-sm-4 control-label"> Desired Substream </label>
                          <div class="col-sm-8">
                            <select name="desired_subcourse" id="desired_subcourse" class="searchoptions" style="border:none; width:60%; height:30px; font-size:12px;" required>

                              <option value="0">Select Substream</option>
                            </select>
                          </div>
                        </div>

                        <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label"> Desired Level Of Course </label>
                           <div class="col-sm-8">
                             <select name="desired_levelofcourse" id="desired_levelofcourse" class="searchoptions" style="border:none; width:50%; height:30px; font-size:12px;" required>

                               <option value="0">Select Level Of Course</option>

                                             <?php foreach($degrees as $degree){ ?>

                             <option value="<?php echo $degree->id;?>" ><?php echo strtoupper($degree->name);?></option>

                             <?php } ?>

                             </select>
                           </div>
                         </div>

                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label">Password</label>
                      <div class="col-sm-8">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                      </div>
                    </div>



                  </div>
                  <div class="box-footer" style="text-align: center">
					               <button type="button" style="text-align: center" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                         <input type="submit"  style="text-align: center" class="btn btn-info" value="Register"/>
                         <br/>
                         <br/>
                         <br/>
                         <br/>
                  </div>
                </form>
              </div>

            </div>
          </div>
        </section>
      </div> -->

      <script type="text/javascript">

 	$( document ).ready(function() {

 	$('#desired_mainstream').on('change', function() {

 			var dataString = 'id='+this.value;

 			$.ajax({

 			type: "POST",

 			url: "/search/university/getSubcoursesList",

 			data: dataString,

 			cache: false,

 			success: function(result){

 			 $('#desired_subcourse').html(result);


 			}

           });



 	   });

         // verify otp_number
         $('#verifyotp').on('click', function() {

              var mobileNumber = $('#user_mobile').val();
              var otpNumber = $('#otp_number').val();

        			var dataString = 'mobile_number=' + mobileNumber + '&otp=' + otpNumber;

        			$.ajax({

        			type: "POST",

        			url: "/webinar/registration/verifyotp",

        			data: dataString,

        			cache: false,

        			success: function(result){

        			 alert(result);

        			}

                  });

        	   });

             // verify resgistrationold
             $('#resgistrationold').on('click', function() {

                  var user_name = $('#user_name').val();
                  var user_email = $('#user_email').val();
                  var user_mobile = $('#user_mobile').val();
                  var desired_country = $('#desired_country').val();
                  var desired_mainstream = $('#desired_mainstream').val();
                  var desired_subcourse = $('#desired_subcourse').val();
                  var desired_levelofcourse = $('#desired_levelofcourse').val();
                  var userPassword = $('#password').val();

            			var dataString = 'user_name=' + user_name + '&user_email=' + user_email + '&user_mobile=' + user_mobile + '&desired_country=' + desired_country + '&desired_mainstream=' + desired_mainstream + '&desired_subcourse=' + desired_subcourse + '&desired_levelofcourse=' + desired_levelofcourse + '&user_password=' + userPassword ;

            			$.ajax({

            			type: "POST",

            			url: "/webinar/registration/registrationsignup",

            			data: dataString,

            			cache: false,

            			success: function(result){


                   if(result == "SUCCESS"){
                     alert("Welcome To HelloUni..!!!");
                     window.location.href='/webinar/webinarmaster/webinarlist';
                   } else {
                     alert(result)
                   }

            			}

                      });

            	   });

   })


   //registration

   function resgistration(){

     var user_name = $('#user_name').val();
     var user_email = $('#user_email').val();
     var user_mobile = $('#user_mobile').val();
     var desired_country = $('#desired_country').val();
     var desired_mainstream = $('#desired_mainstream').val();
     var desired_subcourse = $('#desired_subcourse').val();
     var desired_levelofcourse = $('#desired_levelofcourse').val();
     var userPassword = $('#password').val();
     var webinarId = $('#webinar_id').val();
     var userId = $('#user_id').val();

     var dataString = 'user_name=' + user_name + '&user_email=' + user_email + '&user_mobile=' + user_mobile + '&desired_country=' + desired_country + '&desired_mainstream=' + desired_mainstream + '&desired_subcourse=' + desired_subcourse + '&desired_levelofcourse=' + desired_levelofcourse + '&user_password=' + userPassword + '&webinar_id=' + webinarId + '&user_id=' + userId ;
     event.preventDefault();
     $.ajax({

     type: "POST",

     url: "/webinar/registration/registrationsignup",

     data: dataString,

     cache: false,

     success: function(result){

      if(result == "SUCCESS"){

        alert("Welcome To HelloUni..!!!");
        window.location.href='/webinar/webinarmaster/webinarlist';

      } else if(result == 'With Email-ID Or Mobile Account Already Exists'){

        alert(result);
        window.location.href='/user/account';

      } else {

        alert(result);

      }

     }

         });

   }

   // sent OTP
   function sendOTP() {


        var mobileNumber = $('#user_mobile').val();
        var mobilenumberlength = $('#user_mobile').val().length;
        var userEmail = $('#user_email').val();

        if(mobilenumberlength != 10) {

          alert("Mobile Number Not Proper ...!!!");

        } else {
          var dataString = 'mobile_number=' + mobileNumber + '&user_email=' + userEmail;

          $.ajax({

          type: "POST",

          url: "/webinar/registration/sentotp",

          data: dataString,

          cache: false,

          success: function(result){
            if(result == 'Mobile Number Already Registered'){

              alert(result);
              window.location.href='/user/account';

            } else{

              alert(result);

            }

          }
              });
        }

      }

     </script>
