<style>

#errmsg{margin-left:10px; color:red;}

 .shade_border{

background:url(img/shade1.png) repeat-x; height:11px;

}

.uni{

background-color:#f4f4f4; padding:0px; margin:10px;

color:#161616;



}

.uni p{

font-size: 11px;

margin:5px;

}



.favourites{

color:#fff;  background-color:#394553; float:left; margin-left:1px; padding:6px; font-size:11px;

}

.compare{

color:#fff; width:118px; background-color:#394553; float:left; margin-right:1px; padding:6px;font-size:11px;

}

.search_form

{

border:solid 2px #ededed; background:#f6f6f6; padding-bottom:15px;

}

.detail{

background:#f6881f; text-align:center; padding-top:6px; padding-left:11px; height:40px; color:#fff;

}
.my-search{
    font-size: 20px;
font-weight: 600;
margin-bottom: 10px;
}
.my-search-box, .my-search-box div{
    padding: 0px;
}
.my-search-box-heading{
    font-size: 16px;
    font-weight: bold;
}
.my-search-box-text{
    font-size: 14px;
}


.sort-filter {
    font-size: 16px;
    line-height: 1.6;
    margin:0 0 10px;
    color:#000;
}

.font-11 {
    font-size: 11px;
}
.font-16 {
    font-size: 16px !important;
    font-weight: 700 !important;
    letter-spacing: 0.5px !important;
    color: Black !important;
}
.uni-image {
    background: #fff;
    border: 1px solid #f3f3f3;
    padding: 5px;
    position: relative;
    width: 184px;
}
ul.uni-cont li {
    background: #fbfbfb;
    border: 1px solid #f3f3f3;
    margin-bottom: 8px;
    list-style: none;
}
.clearwidth {
    width: 100%;
    float: left;
}
.univ-tab-details {
    margin: 30px 0 10px 0!important;
}

.flLt {
    float: left;
}
.flRt {
    float: right;
}

.uni-box {
    float: left;
    width: 100%;
    padding: 8px;
    position: relative;
    background-color: #fbfbfb;
    /* border: 1px solid #f0f0f0; */
    box-shadow: 2px 2px 4px #ccccc4;
    margin: 20px 0px;
}
.uni-box p {
    margin:0px;
    line-height:unset;
}
.uni-detail {
    margin-left: 208px;
}
.uni-title {
    margin-bottom: 10px;
    width: 90%;
}
.uni-title a {
    font-weight: 600;
    font-size: 13px;
    color: #111111;
}
.uni-title span {
    color: #666666;
}
.uni-sub-title {
    font-weight: 600;
    font-size: 15px;
    color:#666666;
}
a.uni-sub-title {
    color:#666666;
}
.uni-course-details {
    width: 100%;
    border-right: 1px solid #dadada;
    background: #fff;
    /*margin: 8px 0;*/
    padding: 8px;
    position: relative;
    /*margin-right: 25px;*/
}
.uni-course-details:before {
    width: 100%;
    height: 1px;
    right: -1px;
    bottom: -1px;
    background-image: -webkit-gradient(linear,0 100%,0 0,from(#fff),to(#dadada));
    background-image: -webkit-linear-gradient(right,#dadada,#fff);
    background-image: -moz-linear-gradient(right,#dadada,#fff);
    background-image: -o-linear-gradient(#fff,#dadada);
}
.uni-course-details:after, .uni-course-details:before {
    content: "";
    position: absolute;
}
.detail-col {
    width: 145px;
    font-size: 13px;
    color: #111111;
}
.detail-col p {
    margin-bottom: 3px;
}
.detail-col strong {
    color: #666666;
    margin-bottom: 5px;
    display: block;
    font-weight: 400;
}
.btn-brochure a {
    padding: 8px 18px 9px!important;
}
a.button-style {
    padding: 6px 9px;
}
.compare-box {
    font-size: 14px;
    color: #000;
    width: 140px;
    line-height: 15px;
}
.compare-box p{
    display:inline;
    margin:0px;
}

div {
    background: 0 0;
    border: 0;
    margin: 0;
    outline: 0;
    padding: 0;
    vertical-align: baseline;
}
.sponsered-text {
    color: #999;
    font-size: 12px;
    font-style: italic;
    margin: 15px 0 5px 0;
    display: none;
}
.tar {
    text-align: right;
}
.button-style, .login-btn, a.button-style, a.login-btn {
    background: #F6881F;
    border: 1px solid transparent;
    padding: 5px 10px;
    color: #fff;
    font-size: 14px;
    text-decoration: none;
    vertical-align: middle;
    display: inline-block;
}
.add-shortlist, .added-shortlist {
    background-position: 0 -51px;
    width: 78px;
    height: 77px;
    position: absolute;
    top: 0;
    right: 0px;
    cursor: pointer;
}

.cate-sprite {
    background: url(/images/save-fav-btn-white.png);
    display: inline-block;
    font-style: none;
    vertical-align: middle;
}

.saved-fav {
    background: url(/images/save-fav-btn.png);
}


/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 30%;
}

/* The Close Button */
.close {
  color: #8e0f0f;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

#compare-university-modal{
  text-align: center;
}

#compare-university-modal p{
  font-size: 16px;
    font-weight: bold;
  margin: 0 0 15px;
}

#compare-university-modal label{
  color:#000;
  font-size: 13px;
  margin-top: 10px;
  font-weight: 500;
}

.compare_universities_submit{
  text-align: center;
    font-size: 15px;
  margin-top: 10px;
}

#compare-university-modal input[type='submit']{
  padding: 5px;
}

.filterable {
margin-top: 15px;
}
.filterable .panel-heading .pull-right {
    margin-top: -20px;
}
.filterable .filters input[disabled] {
    background-color: transparent;
    border: none;
    cursor: auto;
    box-shadow: none;
    padding: 0;
    height: auto;
}
.filterable .filters input[disabled]::-webkit-input-placeholder {
    color: #333;
}
.filterable .filters input[disabled]::-moz-placeholder {
    color: #333;
}
.filterable .filters input[disabled]:-ms-input-placeholder {
    color: #333;
}

.bannerSection{
  background-color: rgba(0, 0, 0, 0);
    background-repeat: no-repeat;
    background-image: url(<?php echo base_url();?>application/images/webinarBanner.jpg);
    background-size: cover;
    background-position: center center;
    width: 100%;
    height: 170px;
}

.aboutus-section {
    padding: 90px 0;
}
.aboutus-title {
    font-size: 35px;
    letter-spacing: 1.8px;
    /* line-height: 32px; */
    /* margin: 0 0 0px; */
    /* padding: 0 0 11px; */
    position: relative;
    /* text-transform: uppercase; */
    color: #313131;
}
</style>

<section class="bannerSection">
         <!-- <img src="<?php echo base_url();?>uploads/banner.jpg" width="100%" height="400px" /> -->
         <!-- <a class="thumbnail" id="carousel-selector-1"><img src="<?php echo base_url();?>application/images/asucampus.jpg" style="height: 90px;min-height: 90px;padding: inherit;"></a> -->
         <div class="container" style="height: 300px;">
            <div class="row">
               <div class="col-md-12">
                  <h1 class="page-top-title" style="margin-top: 50px;color: #ffffff; text-align:center;font-weight:bold;font-size:37px;"><span style="border-radius: 9px;background-color: rgba(30, 32, 33, 0.82)">&nbsp;&nbsp;&nbsp;&nbsp;WEBINAR&nbsp;&nbsp;&nbsp;&nbsp;</span></h1>
               </div>
            </div>
         </div>
      </section>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- Main content -->
        <section class="content">
          <div class="container" style="margin-top:20px;">
          <div class="row">
              <div class="col-lg-12" style="color:#000;">
                  
              <div class="aboutus" style="text-align: center;">
                <h1 class="aboutus-title">Webinar List</h1>
              </div>


          <?php
  foreach($webinarlist as $webinar_list){
          ?>

          <div class="uni-box">
                  <div class="flLt">
                      <div class="uni-image">
                          <a target="_blank" href="#">
                            <img class="lazy" data-original="/uploads/texas_arlington/banner_3.jpg" alt="University of Texas at Arlington" title="University of Texas at Arlington" align="center" width="172" height="115" src="/uploads/texas_arlington/banner_3.jpg" style="display: inline;"></a>
                          <div class="uni-shrtlist-image">
                          </div>
                      </div>
                  </div>
                  <div class="uni-detail">

                              <!-- <a target="_blank" href="#" id="university-2" class="font-16"><h4><?=$webinar_list['webinar_name']?></h4></a> -->



                               <!--  <?php

                                $webDate = new DateTime($webinar_list['webinar_date']." ".$webinar_list['webinar_time']);
                                $webinarStartDate = $webDate->format('YmdHis');
                                $webinarStartDate =  date('Ymd\THis',strtotime($webinarStartDate));
                                //$webinarStartDate = date('Ymd',strtotime($webDate));
                                $webinarEndDate = date('Ymd\THis',strtotime($webinarStartDate . ' + 2 hours'));
                                $description = false;
                                $location = false;

                                //$url = 'http://www.google.com/calendar/event?action=TEMPLATE';
                                $url = "https://calendar.google.com/calendar/r/eventedit?text=";
                                $url .= '' . rawurlencode($webinar_list['webinar_name']);
                                $url .= '&dates=' . $webinarStartDate . '/' . $webinarEndDate ;
                                if ($description) {
                                              $url .= '&details=' . rawurlencode($description);
                                            }
                                if ($location) {
                                              $url .= '&location=' . rawurlencode($location);
                                            }

                                ?> -->
                                <!-- <a href="<?php echo $url; ?>" class='btn btn-info pull-right' target="_blank">
                                  Add to gCal </a> -->

                                 <!--  <span class="font-14 pull-right"> <?=$webinar_list['webinar_date']?> <?=$webinar_list['webinar_time']?></span>
 -->


                      <div class="course-touple clearwidth">
                          <!--p><a target="_blank" href="#" class="uni-sub-title">Computer Science&nbsp;MSc</a></p-->

                                <?php

                                $webDate = new DateTime($webinar_list['webinar_date']." ".$webinar_list['webinar_time']);
                                $webinarStartDate = $webDate->format('YmdHis');
                                $webinarStartDate =  date('Ymd\THis',strtotime($webinarStartDate));
                                //$webinarStartDate = date('Ymd',strtotime($webDate));
                                $webinarEndDate = date('Ymd\THis',strtotime($webinarStartDate . ' + 2 hours'));
                                $description = false;
                                $location = false;

                                //$url = 'http://www.google.com/calendar/event?action=TEMPLATE';
                                $url = "https://calendar.google.com/calendar/r/eventedit?text=";
                                $url .= '' . rawurlencode($webinar_list['webinar_name']);
                                $url .= '&dates=' . $webinarStartDate . '/' . $webinarEndDate ;
                                if ($description) {
                                              $url .= '&details=' . rawurlencode($description);
                                            }
                                if ($location) {
                                              $url .= '&location=' . rawurlencode($location);
                                            }

                                ?>
                          <div class="clearwidth">
                              <div class="uni-course-details flLt">
                                <div class="col-md-12">
                                    <div class="row">
                                      <a target="_blank" href="#" id="university-2" class="font-16"><h4 style="margin: 0px 0px 4px 0px !important;"><?=$webinar_list['webinar_name']?></h4></a>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                          <h5 class="card-title" style="margin: 8px 0 8px;font-weight: 700;text-transform: none;letter-spacing: 1px;color: black;    line-height: 1.5;"><font color="#f9992f"><i class="fa fa-user" style="font-size:15px;color:#374552">&nbsp;</i><b>Speaker :</b></font> <b><?=$webinar_list['webinar_host_name']?></b></h5>
                                        </div>

                                        <div class="col-md-4">
                                          <h5 class="card-title" style="margin: 8px 0 8px;font-weight: 700;text-transform: none;letter-spacing: 1px;color: black;     line-height: 1.5;"><font color="#f9992f"><i class="fa fa-check-square-o" style="font-size:15px;color:#374552">&nbsp;</i><b>Topic Covered : </b></font> <b><?=$webinar_list['webinar_topics']?></b></h5>
                                        </div>
                                        <div class="col-md-3">
                                          <h5 class="card-title" style="margin: 8px 0 8px;font-weight: 700;text-transform: none;letter-spacing: 1px;color: black;     line-height: 1.5;"><font color="#f9992f"><i class="fa fa-users" style="font-size:15px;color:#374552">&nbsp;</i><b>Intrested Student :  </b></font> <b><?php echo $webinar_list['default_attend_count'] + $webinar_list['acount'] ; ?></b></h5>

                                        </div>
                                        <div class="col-md-2 btnMobile" style="margin-top: 6px;text-align: right;">
                                        <a href="<?php echo $url; ?>" class='btn btn-md btn-info' target="_blank" style="border-radius: 10px; box-shadow: 2px 2px 2px #c7c7c7c2;"><i class="fa fa-google" style="font-size:15px;color:white;">&nbsp;</i>
                                        <b>Add to gCal</b> </a>
                                      </div>
                                    </div>
                                    <div class="row" style="margin-top: 3px;">
                                        <div class="col-md-3">
                                          <p><i class="fa fa-calendar" style="font-size:15px;color:#374552">&nbsp;</i><b>Date  : </b></p>
                                          <p><?php $webinarDate = date('D, d F Y', strtotime($webinar_list['webinar_date'])); echo "$webinarDate";?></p>
                                        </div>

                                        <div class="col-md-3">
                                          <p><i class="fa fa-clock-o" style="font-size:15px;color:#374552">&nbsp;</i><b>Time : </b></p>
                                          <p><?=$webinar_list['webinar_time']?></p>
                                        </div>
                                        <div class="col-md-6 btnMobile" style="text-align: right;padding-right: 0px; margin: inherit;">
                                         <?php if($login == "NO") { ?>
                                          <a class='btn btn-md btn-primary' href="/webinar/registration/registrationForm/<?php echo $webinar_list['id']; ?>" class="button-style" style="border-radius: 10px; box-shadow: 2px 2px 2px #c7c7c7c2;"><i class="fa fa-pencil-square-o" style="font-size:15px;color:white;">&nbsp;</i><b>Register Now</b></a>
                                      <?php  } else { ?>
                                        <?php if ($webinar_list['selected'] == 'YES' && $webinar_list['status'] == '1') {
                                          echo "<button  style='text-align: center; border-radius: 10px; box-shadow: 2px 2px 2px #c7c7c7c2; ' class='btn btn-md btn-info'  onclick='joinwebinar(".$webinar_list['id'].")'><b><i class='fa fa-sign-in' style='font-size:15px;color:white;'>&nbsp;</i>Join Webinar</b></span>";
                                        //} else if($webinar_list['selected'] == 'YES' && $webinar_list['webinar_live'] == "0" && $webinar_list['status'] == '1')  {  // to be add in if condition() && $webinar_list['webinar_live'] == "1"
                                          //echo "<span class='button-style btn-md btn-success' style='color:green; border-radius: 10px; box-shadow: 2px 2px 2px #c7c7c7c2;'><b><i class='fa fa-check-square' style='font-size:15px;color:white;'>&nbsp;</i>Applied</b> </span>";
                                        } else {
                                          echo "<button  style='text-align: center; border-radius: 10px; box-shadow: 2px 2px 2px #c7c7c7c2;' class='btn btn-md btn-warning '  onclick='webinarapply(".$webinar_list['id'].")'><b><i class='fa fa-check-circle' style='font-size:15px;color:white;'>&nbsp;</i>Apply</b></button>";


                                        }
                                        ?>

                                      <?php } ?>
                                      </div>
                                    </div>

                                 <!--  <div class="col-md-2">


                                  </div> -->
                                </div>
                                  <!-- <div class="detail-col flLt" style="width:50%;">
                                      <strong>Intrested Student Count</strong>
                    <p><?php echo $webinar_list['default_attend_count'] + $webinar_list['acount'] ; ?></p>
                                  </div> -->

                  <!-- <div class="detail-col flLt" style="width:50%;">
                    <strong>Topic Covered</strong>
                    <p><?=$webinar_list['webinar_topics']?></p>
                  </div> -->
                <!--  <div class="detail-col flLt" style="width:20%;">
                    <strong>Placement</strong>
                    <p>48%</p>
                  </div>-->
                              </div>
                              <!-- <div class="btn-col btn-brochure" style="margin-top: 27px !important">

                                  <?php if($login == "NO") { ?>
                                    <a class='btn btn-warning' href="/webinar/registration/registrationForm" class="button-style">Register to Attend</a>
                                <?php  } else { ?>
                                  <?php if ($webinar_list['selected'] == 'YES' && $webinar_list['webinar_live'] == "YES" && $webinar_list['status'] == '1') {
                                    echo "<button  style='text-align: center' class='btn btn-warning'  onclick='joinwebinar(".$webinar_list['id'].")'>Join Webinar</span>";
                                  } else if($webinar_list['selected'] == 'YES' && $webinar_list['webinar_live'] == "NO" && $webinar_list['status'] == '1')  {
                                    echo "<span class='button-style btn-success' style='color:green;'> Applied </span>";
                                  } else {
                                    echo "<button  style='text-align: center' class='btn btn-warning'  onclick='webinarapply(".$webinar_list['id'].")'>Apply</button>";
                                  }
                                  ?>

                                <?php } ?>


                              </div> -->
                          </div>


                            <!--<div class="compare-box flRt customInputs">
                                <input type="checkbox" name="compare_university[]" value="2" onclick="openComparePopup()">
                                <span class="common-sprite"></span>
                                <label><p>Add to compare</p></label>
                  <input type="hidden" name="sort_by" value="ranking_world">
                </div>-->

                      </div>

                  </div>
              </div>

                      <?php
              }
              ?>

        </div>

    </div>
  </div>

          </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <script type="text/javascript">

 	$( document ).ready(function() {

 	$('#desired_mainstream').on('change', function() {

 			var dataString = 'id='+this.value;

 			$.ajax({

 			type: "POST",

 			url: "/search/university/getSubcoursesList",

 			data: dataString,

 			cache: false,

 			success: function(result){

 			 $('#desired_subcourse').html(result);


 			}

           });

 	   });



   })

   // sent OTP
   function webinarapply(wid) {


        var stype = "loggedIn";

          var dataString = 'source=' + stype + '&webinar_id=' + wid;

          $.ajax({

          type: "POST",

          url: "/webinar/webinarmaster/webinarapply",

          data: dataString,

          cache: false,

          success: function(result){

           //alert(result);
           window.location.href=result;

          }
              });
      }

      // sent OTP
      function joinwebinar(wid) {


           var stype = "join";

             var dataString = 'source=' + stype + "&webinar_id=" + wid;

             $.ajax({

             type: "POST",

             url: "/webinar/webinarmaster/webinarjoin",

             data: dataString,

             cache: false,

             success: function(result){

              //alert(result);
              window.location.href=result;

             }
                 });
         }

     </script>
