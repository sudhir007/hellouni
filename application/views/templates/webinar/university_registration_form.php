<style type="text/css">
    @import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");
.login-block{
    background: #004b7a;  /* fallback for old browsers */
background: -webkit-linear-gradient(to bottom, #fafafb, #d8d9da);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to bottom, #fafafb, #d8d9da); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
float:left;
width:100%;
padding : 50px 0;
}
.banner-sec{background:url(<?php echo base_url();?>application/images/register.png)  no-repeat left bottom; background-size:cover; min-height:600px; border-radius: 0 10px 10px 0; padding:0;background-position: center;    margin-top: 25px;}

.login-sec{padding: 50px 30px; position:relative;}
.login-sec .copy-text{position:absolute; width:80%; bottom:20px; font-size:13px; text-align:center;}
.login-sec h2{margin-bottom:30px; font-weight:800; font-size:30px; color: #004b7a;}
.login-sec h2:after{content:" "; width:100px; height:5px; background:#f9992f; display:block; margin-top:20px; border-radius:3px; margin-left:auto;margin-right:auto}
.btn-login{background: #f9992f; color:#fff; font-weight:600;}
.form-control{
  display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}

.litype{
  list-style-type: disc;
}
</style>
<!--<div>
   <section style="background-color: rgba(0, 0, 0, 0);background-repeat: no-repeat;background-image: url(<?php echo base_url();?>application/images/airc_virtual_booth_banner.jpg); background-size: cover;background-position: center center;width: 100%;height: 100%;" class="about-section">
      <div class="container" style="height: 330px;">
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="z-index: 5 !important; text-align: center;">
               <h1 class="page-top-title" style="margin-top: 8%;color: #ffffff; text-align:center;font-weight:bold;font-size:37px;"><span></span></h1>
            </div>
         </div>
      </div>
   </section>
</div>-->

<section class="login-block">

  <div class="container" style="background:#fff; border-radius: 10px; box-shadow: 5px 5px 0px rgba(0,0,0,0.1);">
  <div class="row banner-secLogo">
    <img src="<?php echo base_url();?>application/images/airc_virtual_booth_banner.jpg"
           class="img-fluid"  alt="Responsive Image"
           width="100%" height="80%" />
  </div>

    <div class="row">

      <div class="text-center">
        <br/>
        <p>
          Welcome to <font color="#f9992f"><b>Hello-Uni</b></font>! India's leading student outreach platform! <br>
          Our innovative web-conferencing portal enables institutions to connect directly with students, face-to-face at a Pan-India Level!
        </p>

        <p>
          We are now offering Five Universities <b><u>Free Listing, Marketing & Enrollment Support</u></b> on our portal for 1 year! <br>
          The selected institutions will receive <b><u>FREE Promotion</u></b> on HelloUni for an entire year to help strengthen their student recruitment activities, Pan-India!
        </p>
      </div>

        <div class="col-md-6 login-sec">
            <h2 class="text-center"> University Registration </h2>
            <form class="login-form" class="form-horizontal" onsubmit="resgistration()" method="post">
              <div class="col-md-12 form-group">
                <div class="col-md-12">
                  <label for="inputEmail3" class="text-uppercase">University Name * </label>

                    <input type="text" class="form-control" id="university_name" name="university_name" placeholder="University Name" required>

               </div>
              </div>

              <div class="col-md-12 form-group">
                <div class="col-md-12">
                  <label for="inputEmail3" class="text-uppercase"> Delegate Name * </label>

                    <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Name" required>

               </div>
              </div>

              <div class="col-md-12 form-group">
                <div class="col-md-12">
                  <label for="inputEmail3" class="text-uppercase">Designation *</label>

                    <input type="text" class="form-control" id="designation" name="designation" placeholder="Name" required>

               </div>
              </div>

               <div class="col-md-12 form-group">
                <div class="col-md-12">
                    <label for="inputEmail3" class="text-uppercase">Email * </label>
                    <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email" required>
                  </div>
                </div>

                <div class="col-md-12 form-group">
                    <div class="col-md-12">
                     <label for="inputEmail3" class="text-uppercase">Mobile Number ( WhatsApp ) * </label>
                     <input type="text" class="form-control" id="user_mobile" name="user_mobile" placeholder="Mobile Number"  required>
                    </div>

                  </div>

                  <div class="col-md-12 form-group">
                      <div class="col-md-12">
                       <label for="inputEmail3" class="text-uppercase">Is there any Virtual Event you would be looking to host in the coming months?  If interested, you could let us know about the event and we could strategize a way to host it on HelloUni? </label>
                       <!--<Select name="upcomingvr" id="upcomingvr" class="form-control" style="width:100%; height:30px; font-size:12px;">
                           <option value="">Select Option</option>
                           <option value="Yes"> Yes</option>
                           <option value="No"> No </option>
                        </Select>-->
                        <textarea  name="upcomingvr" id="upcomingvr" class="form-control" style="font-size:12px;" rows="4" cols="50">

                        </textarea>
                      </div>

                    </div>

              <div class="form-check" style="text-align: center;">
                <!-- <button type="submit" class="btn btn-login" ng-click="submitForm()">Submit</button> -->
                 <button type="button" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                  <input type="submit"  style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Register"></input>
              </div>


            </form>
        </div>
        <div class="col-md-6 banner-sec">
           <!--  <ul>
              <li>kjbkjbkjbkjbbbkjbkj kjbjkjnkj kjnkjnjk kjn</li>
                            <li>kjbkjbkjbkjbbbkjbkj kjbjkjnkj kjnkjnjk kjn</li>

              <li>kjbkjbkjbkjbbbkjbkj kjbjkjnkj kjnkjnjk kjn</li>

            </ul> -->
        </div>



    </div>

    <div class="container" style="margin-bottom: 10px;">
         <h4> Strengths of the system: </h4>

         <ul style="color: #4a4949; font-size: 15px; ">
            <li class="litype"> <b> Pan-India Recruitment </b> — Not limited by geography, we can reach out across the country and in the future, we would be looking to expand beyond India.</li>
             <li class="litype"> <b> Face-to-face Meetings </b> — Interact directly with students in 1-to-1 or Many-to-1 settings.</li>
            <li class="litype"> <b> Student Profile Screening </b> — Entire student profile is visible to determine their potential and scope for virtual meeting / followup.</li>
            <li class="litype"> <b> Highlight University Strengths </b> — All the information displayed about the University can be easily edited from your Admin Panel login.</li>
          </ul>
       </div>

</div>
</section>

<style>
   p {
   font-size: 16px;
   line-height: 1.6;
   margin:0 0 10px;
   }
   .italic {
   font-style:italic;
   }
   .padtb {
   padding:30px 0px;
   }
</style>

  <script type="text/javascript">

   //registration

   function resgistration(){

     var university_name = $('#university_name').val();
     var user_name = $('#user_name').val();
     var user_email = $('#user_email').val();
     var user_mobile = $('#user_mobile').val();
     var upcomingvr = $('#upcomingvr').val();

     var dataString = 'university_name='+ university_name +'&user_name=' + user_name + '&user_email=' + user_email + '&user_mobile=' + user_mobile + '&upcomingvr=' + upcomingvr ;

     event.preventDefault();
     $.ajax({

     type: "POST",

     url: "/university/register/submit",

     data: dataString,

     cache: false,

     success: function(result){

      if(result == "SUCCESS"){

        alert("Thank you for registering for the Lucky Draw! | HelloUni.org");
        window.location.href='/university/register';

      } else if(result == 'With Email-ID Or Mobile Account Already Exists'){

        alert(result);
        window.location.href='/user/account';

      } else {

        alert(result);

      }

     }

         });

   }

   // sent OTP
   function sendOTP() {


        var mobileNumber = $('#user_mobile').val();
        var mobilenumberlength = $('#user_mobile').val().length;

        if(mobilenumberlength != 10) {

          alert("Mobile Number Not Proper ...!!!");

        } else {
          var dataString = 'mobile_number=' + mobileNumber;

          $.ajax({

          type: "POST",

          url: "/webinar/registration/sentotp",

          data: dataString,

          cache: false,

          success: function(result){
            if(result == 'Mobile Number Already Registered'){

              alert(result);
              window.location.href='/user/account';

            } else{

              alert(result);

            }

          }
              });
        }

      }

     </script>
