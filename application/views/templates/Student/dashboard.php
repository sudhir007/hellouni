<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<style type="text/css">
   p , .guidance li, td{
   font-size: 16px;
   line-height: 1.6;
   margin:0 0 10px;
   color:#000;
   }
   .guidance ol li{
   list-style:decimal;
   margin-left:24px;
   }
   .pricing-column-features li {
   color:#000;
   font-size: 16px;
   line-height: 1.6;
   }
   .italic {
   font-style:italic;
   }
   .text-center {
   text-align:center;
   }
   .pricing-column-price {
   font-size: 24px;
   font-weight: 700;
   }
   .tutor-plan {
   font-weight: 700;
   }
   .btn {
   padding:8px 12px;
   }
   .dashed-border  {
   border-bottom: 1px dashed #dddcde;
   }
   .formControl{
   color: #fff !important;
   background-color: #8c57c5 !important;
   /* border: 0px; */
   font-size: 12px;
   border: 0px !important;
   box-shadow: inset 0 0 0 rgba(0,0,0,.075) !important;

    margin-top: 15px;

   }
   .ht30{
   height: 30px;
   }
   /*section {
   width: 100%;
   display: inline-flex;
   align-items: center;
   justify-content: center;
   }
   */
   section .flex-col {
   padding: 15px;
   }
   section .flex-col .cards {
   max-width: 380px;
   height: 110px;
   background-color: #fff;
   padding: 18px;
   position: relative;
   border-top-right-radius: 5px;
    border-top-left-radius: 5px;
    box-shadow: 0px 0px 6px 0px hsl(229, 6%, 66%);
   }
   section .flex-col .cards h3 {
   font-size: 1.8rem;
       margin: 0px 0 8px;
   text-align: center;
   }
   section .flex-col .cards p {
   text-align: center;
   margin-top: 5px;
   /* line-height: 2; */
   font-size: 3.5rem;
   color: #f6882c;
   font-weight: 600;
   letter-spacing: 1px;
   }
   section .flex-col .cards img {
   position: absolute;
   bottom: 10px;
   right: 5px;
   width: 20%;
   }
   section .one {
   border-top: 5px hsl(27deg 92% 57%) solid;
   }
   /*section .two {
   border-top: 5px hsl(0, 78%, 62%) solid;
   }
   section .three {
   border-top: 5px hsl(34, 97%, 64%) solid;
   }
   section .four {
   border-top: 5px hsl(212, 86%, 64%) solid;
   }*/
   @media (max-width: 960px) {
   section {
   flex-direction: column;
   }
   section .flex-col {
   padding: 0 40px;
   }
   header {
   padding: 0 45px 20px;
   }
   }
   @media (max-width: 425px) {
   header h1 {
   font-size: 1.4rem;
   }
   }
   @import url(https://fonts.googleapis.com/css?family=Anonymous+Pro:400,700);
   /*body {
   background: #E7ECEC;
   font-family: 'Anonymous Pro', sans-serif;
   font-weight: 400;
   font-size: 14px;
   -webkit-font-smoothing: antialiased;
   -moz-osx-font-smoothing: grayscale;
   }
   * {
   box-sizing: border-box;
   backface-visibility: hidden;
   }*/
   .area {
   position: absolute;
   /*top: 50%;*/
   left: 85%;
   margin-top: -15px;
   margin-left: -15px;
   }
   .toggle {
   display: block;
   position: relative;
   background: #FCFCFC;
   border: none;
   width: 44px;
   height: 44px;
   line-height: 44px;
   text-align: center;
   border-radius: 100%;
   cursor: pointer;
   border: 4px solid rgba(255, 255, 255, 0.3);
   transition: all 0.2s ease;
   z-index: 4;
   outline: none;
   }
   .toggle:hover {
   transform: scale(0.8);
   }
   .toggle.active {
   transform: none;
   border: 4px solid #f6882c;
   }
   .toggle.active .icon {
   transform: rotate(315deg);
   }
   .toggle.active ~ .card {
   opacity: 1;
   transform: rotate(0);
   z-index: 1;
   }
   .toggle.active ~ .card h1,
   .toggle.active ~ .card p {
   opacity: 1;
   transform: translateY(0);
   }
   .bubble {
   position: absolute;
   border: none;
   width: 40px;
   height: 40px;
   border-radius: 100%;
   cursor: pointer;
   transition: all 0.3s ease;
   z-index: 1;
   top: 2px;
   left: 2px;
   }
   /*.bubble:before {
   content: "";
   position: absolute;
   width: 120px;
   height: 120px;
   opacity: 0;
   border-radius: 100%;
   top: -40px;
   left: -40px;
   background: #f6882c;
   animation: bubble 3s ease-out infinite;
   }
   .bubble:after {
   content: "";
   position: absolute;
   width: 120px;
   height: 120px;
   opacity: 0;
   border-radius: 100%;
   top: -40px;
   left: -40px;
   z-index: 1;
   background: #f6882c;
   animation: bubble 3s 0.8s ease-out infinite;
   }*/
   .icon {
   display: inline-block;
   position: relative;
   width: 16px;
   height: 16px;
   z-index: 5;
   transition: all 0.3s ease-out;
   }
   .icon .horizontal {
   position: absolute;
   width: 100%;
   height: 4px;
   background: linear-gradient(-134deg, #8c57c5 0%, #6e1bca 100%);
   top: 50%;
   margin-top: -2px;
   }
   .icon .vertical {
   position: absolute;
   width: 4px;
   height: 100%;
   left: 50%;
   margin-left: -2px;
   background: linear-gradient(-134deg, #8c57c5 0%, #6e1bca 100%);
   }
   @keyframes bubble {
   0% {
   opacity: 0;
   transform: scale(0);
   }
   5% {
   opacity: 1;
   }
   100% {
   opacity: 0;
   }
   }
   .card {
   /*width: 400px;*/
   overflow: hidden;
   position: relative;
   background: #FCFCFC;
   border-radius: 10px;
   margin-top: -25px;
   margin-left: 25px;
   opacity: 0;
   transform: rotate(5deg);
   transform-origin: top left;
   transition: all 0.2s ease-out;
   border: 1px solid #8c57c5;
   }
   .card-content {
   display: inline-block;
   vertical-align: middle;
   width: auto;
   padding: 15px;
   /*border-right: 1px solid #f0f0f0;*/
   }
   .addStuBtn{
   width: inherit;
   color: #f6882c;
   border-radius: 10px;
   margin-top: 12%;
   }
   
   .subBox{
          padding-right: 0px;
    padding-left: 0px;
    height: auto;
    text-align: center;
        border-radius: 5px;
        box-shadow: 0px 0px 6px 0px hsl(229, 6%, 66%);
   }

   .innerSubBox{
      background-color: #f6882c;
    height: inherit;
        padding: 3px 0px;
   }

   .innerSubBox p{
             color: white;

          margin: 0 0 0px !important; 
   }

   .innerSubBox h4{
          color: white;

          margin: 05px 0 5px; 
   }

.highcharts-figure, .highcharts-data-table table {
  min-width: 360px; 
  max-width: 100%;
  margin: 1em auto;
}

.highcharts-data-table table {
   font-family: Verdana, sans-serif;
   border-collapse: collapse;
   border: 1px solid #EBEBEB;
   margin: 10px auto;
   text-align: center;
   width: 100%;
   max-width: 100%;
}
.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}
.highcharts-data-table th {
   font-weight: 600;
  padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
  padding: 0.5em;
      text-align: center;
}

.highcharts-data-table thead tr{
  background: #f6882c;
  color: white;
  font-size: 14px;
}
.highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}
.highcharts-data-table tr:hover {
  background: #8c57c62e;
}
</style>
<div class="container">
   <div class="row" style="padding: 30px 0px 30px 0px;display: flex;">
      <div class="col-md-3 purpleBg" style="box-shadow: 0px 0px 8px 0px #dcd9d9;    padding: 15px 15px 15px 15px;">
         <div class="col-md-12" style="border-radius: 6px;text-align: center;background-color: white;">
            <h4 style="color: #f6882c;">Filters</h4>
         </div>
         <div class="col-md-12">
            <label class="form-control formControl">From Date</label>
            <input type="date" id="fromDate" name="fromDate" class="searchoptions form-control ht30">
         </div>
         <div class="col-md-12">
            <label class="form-control formControl">To Date</label>
            <input type="date" id="toDate" name="toDate" class="searchoptions form-control ht30">
         </div>
         <div class="col-md-12">
            <label class="form-control formControl">COUNTRY *</label>
            <select name="country" id="country" class="searchoptions form-control ht30" >
               <option value="0">Select Country</option>
               <option value="USA"  >USA</option>
               <option value="Canada"  >CANADA</option>
               <option value="Australia"  >AUSTRALIA</option>
               <option value="Germany"  >GERMANY</option>
               <option value="New Zealand"  >NEW ZEALAND</option>
               <option value="Singapore"  >SINGAPORE</option>
               <option value="Ireland"  >IRELAND</option>
               <option value="UK"  >UK</option>
               <option value="Dubai"  >DUBAI</option>
               <option value="China"  >CHINA</option>
               <option value="Russia"  >RUSSIA</option>
               <option value="Philippines"  >PHILIPPINES</option>
               <option value="Switzerland"  >SWITZERLAND</option>
               <option value="Poland"  >POLAND</option>
               <option value="Netherlands"  >NETHERLANDS</option>
               <option value="Italy"  >ITALY</option>
               <option value="France"  >FRANCE</option>
               <option value="Japan"  >JAPAN</option>
            </select>
         </div>
         <div class="col-md-12">
            <label class="form-control formControl">INTAKE</label>
            <select class=" form-control ht30" >
               <option value="">Select Intake Month</option>
               <option value="January">January</option>
               <option value="February">February</option>
               <option value="March">March</option>
               <option value="April">April</option>
               <option value="May">May</option>
               <option value="June">June</option>
               <option value="July">July</option>
               <option value="August">August</option>
               <option value="September">September</option>
               <option value="October">October</option>
               <option value="November">November</option>
               <option value="December">December</option>
            </select>
         </div>
         <div class="col-md-12">
            <label class="form-control formControl">Application Status</label>
            <select name="subcourse1" id="subcourse1" class="searchoptions form-control ht30" >
               <option value="0">Select Substream</option>
            </select>
         </div>
         <div class="col-md-12">
            <label class="form-control formControl">Offer Type</label>
            <select name="degree" id="degree" class="searchoptions form-control ht30" >
               <option value="0">Select Level Of Course</option>
               <option value="1" >1</option>
               <option value="2" >1</option>
               <option value="3" >3</option>
               <option value="4" >4</option>
            </select>
         </div>
         
         <!-- <div class="col-md-12">
            <div class="col-lg-12 col-md-12 text-right" style="color:#000;">
               <input type="submit" class="btn btn-md addStuBtn" value="SEARCH" name="search" id="search">
            </div>
         </div> -->
      </div>
      <div class="col-md-9">
         <div class="row">
            <section>
               <div class="col-md-12">
                  <div class="col-md-4 flex-col">
                     <div class="cards one">
                        <!-- <div class="area">
                           <div class="toggle" id="applications">
                              <div class="icon">
                                 <div class="horizontal"></div>
                                 <div class="vertical"></div>
                              </div>
                           </div>
                           <div class="bubble"></div>
                           <div class="card">
                              <div class="card-content">
                                 <label class="form-control formControl">Type</label>
                                 <select name="degree" id="degree" class="searchoptions form-control ht30" >
                                    <option value="0">Select</option>
                                    <option value="1" >Approved</option>
                                    <option value="2" >Rejected</option>
                                    <option value="3" >Pending</option>
                                    <option value="4" >Inprocess</option>
                                 </select>
                              </div>
                           </div>
                        </div> -->
                        <h3>Applications</h3>
                        <p>150</p>
                        <!-- <img src="<?php echo base_url();?>application/images/Studentdashboard/Application.png"> -->
                     </div>
                      <div class="col-md-12 subBox">
                           <div class="col-md-6 innerSubBox" style="border-bottom-left-radius: 5px;">
                           <p>Approved</p>
                           <h4>100</h4>
                           </div>
                           <div class="col-md-6 innerSubBox" style="border-bottom-right-radius: 5px;border-left: 3px solid white;">
                           <p>Rejected</p>
                           <h4>120</h4>
                           </div>
                      </div>
                  </div>
                  <div class="col-md-4 flex-col">
                     <div class="cards one">
                        <!-- <div class="area">
                           <div class="toggle" id="offers">
                              <div class="icon">
                                 <div class="horizontal"></div>
                                 <div class="vertical"></div>
                              </div>
                           </div>
                           <div class="bubble"></div>
                           <div class="card">
                              <div class="card-content">
                                 <label class="form-control formControl">Type</label>
                                 <select name="degree" id="degree" class="searchoptions form-control ht30" >
                                    <option value="0">Select</option>
                                    <option value="1" >1</option>
                                    <option value="2" >1</option>
                                    <option value="3" >3</option>
                                    <option value="4" >4</option>
                                 </select>
                              </div>
                           </div>
                        </div> -->
                        <h3>Offers</h3>
                        <p>120</p>
                        <!-- <img src="<?php echo base_url();?>application/images/Studentdashboard/Offers.png" > -->
                     </div>
                     <div class="col-md-12 subBox">
                           <div class="col-md-6 innerSubBox" style="border-bottom-left-radius: 5px;">
                           <p>Approved</p>
                           <h4>100</h4>
                           </div>
                           <div class="col-md-6 innerSubBox" style="border-bottom-right-radius: 5px;border-left: 3px solid white;">
                           <p>Rejected</p>
                           <h4>120</h4>
                           </div>
                      </div>
                  </div>
                  <div class="col-md-4 flex-col">
                     <div class="cards one">
                        <!-- <div class="area">
                           <div class="toggle" id="payments">
                              <div class="icon">
                                 <div class="horizontal"></div>
                                 <div class="vertical"></div>
                              </div>
                           </div>
                           <div class="bubble"></div>
                           <div class="card">
                              <div class="card-content">
                                 <label class="form-control formControl">Type</label>
                                 <select name="degree" id="degree" class="searchoptions form-control ht30" >
                                    <option value="0">Select</option>
                                    <option value="1" >1</option>
                                    <option value="2" >1</option>
                                    <option value="3" >3</option>
                                    <option value="4" >4</option>
                                 </select>
                              </div>
                           </div>
                        </div> -->
                        <h3>Payments</h3>
                        <p>120</p>
                        <!-- <img src="<?php echo base_url();?>application/images/Studentdashboard/Payments.png" > -->
                     </div>
                     <div class="col-md-12 subBox">
                           <div class="col-md-6 innerSubBox" style="border-bottom-left-radius: 5px;">
                           <p>Approved</p>
                           <h4>100</h4>
                           </div>
                           <div class="col-md-6 innerSubBox" style="border-bottom-right-radius: 5px;border-left: 3px solid white;">
                           <p>Rejected</p>
                           <h4>120</h4>
                           </div>
                      </div>
                  </div>
               </div>
               <div class="col-md-12">
                  <div class="col-md-4 flex-col">
                     <div class="cards one">
                        <!-- <div class="area">
                           <div class="toggle" id="visa">
                              <div class="icon">
                                 <div class="horizontal"></div>
                                 <div class="vertical"></div>
                              </div>
                           </div>
                           <div class="bubble"></div>
                           <div class="card">
                              <div class="card-content">
                                 <label class="form-control formControl">Type</label>
                                 <select name="degree" id="degree" class="searchoptions form-control ht30" >
                                    <option value="0">Select</option>
                                    <option value="1" >1</option>
                                    <option value="2" >1</option>
                                    <option value="3" >3</option>
                                    <option value="4" >4</option>
                                 </select>
                              </div>
                           </div>
                        </div> -->
                        <h3>Visa</h3>
                        <p>150</p>
                        <!-- <img src="<?php echo base_url();?>application/images/Studentdashboard/Visa.png" > -->
                     </div>
                     <div class="col-md-12 subBox">
                           <div class="col-md-6 innerSubBox" style="border-bottom-left-radius: 5px;">
                           <p>Approved</p>
                           <h4>100</h4>
                           </div>
                           <div class="col-md-6 innerSubBox" style="border-bottom-right-radius: 5px;border-left: 3px solid white;">
                           <p>Rejected</p>
                           <h4>120</h4>
                           </div>
                      </div>
                  </div>
                  <div class="col-md-4 flex-col">
                     <div class="cards one">
                        <!-- <div class="area">
                           <div class="toggle" id="Students">
                              <div class="icon">
                                 <div class="horizontal"></div>
                                 <div class="vertical"></div>
                              </div>
                           </div>
                           <div class="bubble"></div>
                           <div class="card">
                              <div class="card-content">
                                 <label class="form-control formControl">Type</label>
                                 <select name="degree" id="degree" class="searchoptions form-control ht30" >
                                    <option value="0">Select</option>
                                    <option value="1" >1</option>
                                    <option value="2" >1</option>
                                    <option value="3" >3</option>
                                    <option value="4" >4</option>
                                 </select>
                              </div>
                           </div>
                        </div> -->
                        <h3>Students</h3>
                        <p>120</p>
                        <!-- <img src="<?php echo base_url();?>application/images/Studentdashboard/Students.png" > -->
                     </div>
                     <div class="col-md-12 subBox">
                           <div class="col-md-6 innerSubBox" style="border-bottom-left-radius: 5px;">
                           <p>Approved</p>
                           <h4>100</h4>
                           </div>
                           <div class="col-md-6 innerSubBox" style="border-bottom-right-radius: 5px;border-left: 3px solid white;">
                           <p>Rejected</p>
                           <h4>120</h4>
                           </div>
                      </div>
                  </div>
                  <div class="col-md-4 flex-col">
                     <div class="cards one">
                        <!-- <div class="area">
                           <div class="toggle" id="payments">
                              <div class="icon">
                                 <div class="horizontal"></div>
                                 <div class="vertical"></div>
                              </div>
                           </div>
                           <div class="bubble"></div>
                           <div class="card">
                              <div class="card-content">
                                 <label class="form-control formControl">Type</label>
                                 <select name="degree" id="degree" class="searchoptions form-control ht30" >
                                    <option value="0">Select</option>
                                    <option value="1" >1</option>
                                    <option value="2" >1</option>
                                    <option value="3" >3</option>
                                    <option value="4" >4</option>
                                 </select>
                              </div>
                           </div>
                        </div> -->
                        <h3>Payments</h3>
                        <p>120</p>
                        <!-- <img src="<?php echo base_url();?>application/images/Studentdashboard/Offers.png" > -->
                     </div>
                     <div class="col-md-12 subBox">
                           <div class="col-md-6 innerSubBox" style="border-bottom-left-radius: 5px;">
                           <p>Approved</p>
                           <h4>100</h4>
                           </div>
                           <div class="col-md-6 innerSubBox" style="border-bottom-right-radius: 5px;border-left: 3px solid white;">
                           <p>Rejected</p>
                           <h4>120</h4>
                           </div>
                      </div>
                  </div>
               </div>
            </section>
         </div>
         <div class="col-md-12">
            <figure class="highcharts-figure" >
              <div id="container" style="width: 100%"></div>
              <p class="highcharts-description">
                content any description 
              </p>
            </figure>
         </div>
      </div>
   </div>
   <!-- <div id="container">
      <div id="chart"></div>
      </div>  --> 
   <div class="row">
   </div>
   <div class="row">
      <div class="col-lg-12 guidance">
         <p>We at Hello Uni look forward to guide the students towards their interest and dreams. We act as not just counselors and look to guide students in the best interest possible. </p>
      </div>
   </div>
</div>
<!-- <div class="container">
   <div class="row">
      <p>&nbsp;</p>
   </div>
   <div class="row col-sm-12" style="overflow-x: auto;">
      <table class="table table-bordered pricing-table-features">
         <thead class="thead-default">
            <tr>
               <th>
               </th>
               <th class="text-center">
                  <h3>Free Guidance</h3>
               </th>
               <th class="text-center">
                  <h3>Application Plan</h3>
               </th>
               <th class="bg-primary text-center" style="background-color: #6e6e6e;">
                  <h3 style="color:#FFFFFF">Complete Plan </h3>
               </th>
            </tr>
         </thead>
         <tbody>
            <tr>
               <th scope="row">
                  <h3 class="tutor-plan">Price</h3>
               </th>
               <td class="text-center">
                  <div class="pricing-column-price ">
                     FREE
                  </div>
                  <small>Easy to upgrade</small>
               </td>
               <td class="text-center">
                  <div class="pricing-column-price">
                     <i class="fas fa-rupee-sign"></i> 25,000
                  </div>
                  <small> </small>
               </td>
               <td style="background-color: #eeeeee;" class="text-center">
                  <div class="pricing-column-price">
                    <i class="fas fa-rupee-sign"></i> 37,500<small> </small>
                  </div>
                  <small> </small>
               </td>
            </tr>
            <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="Once you have connected with the university through our online web-conferencing tool we can support you in your applications.  We would help you with the Universities deadlines, fees structure etc and address clarifications. " >
                     <p class="tutor-plan " style="border-bottom: 1px dashed #dddcde;display:inline;">Co-ordinate between student and University</p>
                     <h5></h5>
                  </span>
               </th>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td style="background-color: #eeeeee;">
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
            </tr>
            <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="We have created a Whats App and discussion platform where in you are not only connected with the co-applicant but also connected with Hello Uni co –ordinator as well as university delegate. The same can be used to highlight issues and provide real time addressing of  the issues.">
                     <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">Whats App group & Discussion Forum</p>
                  </span>
               </th>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td style="background-color: #eeeeee;">
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
            </tr>
            <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="Once you are ready to submit the online applications  our counsellors would help you check the applications. Many Universities also provide us fee waiver codes where in we can use the same to reduce your cost of applying to the institutions.  We also run live waivers and cash back schemes with regard to the University. ">
                     <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">Check University Applications</p>
                  </span>
               </th>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td style="background-color: #eeeeee;">
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
            </tr>
            
            <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="We provide you document review guidance.  ">
                     <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">Document Review Guidance </p>
                  </span>
               </th>
               <td>
               </td>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td style="background-color:#eeeeee">
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
            </tr>
            <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="We help you explore all universities globally. We through our empanelled counsellors would have one on one guidance sessions that would help you select all the relevant Universities.">
                     <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">University Selection Guidance </p>
                  </span>
               </th>
               <td>
               </td>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td style="background-color:#eeeeee">
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
            </tr>
            <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="We help you prepare and send transcripts to universities. ">
                     <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">Transcripts Review </p>
                  </span>
               </th>
               <td>
               </td>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td style="background-color:#eeeeee">
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
            </tr>
            <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="We guide and fill completely form DS160 and help you with the documentation required for Visa purpose.">
                     <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">Visa Financial Documents & Interview Preparation</p>
                  </span>
               </th>
               <td>
               </td>
               <td>
               </td>
               <td style="background-color:#eeeeee">
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
            </tr>
            <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="We through our empanelled dedicated loan officer help students select the best education loan provider.  We also provide various Cash backs and references.">
                     <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">Guidance on Education Loan</p>
                  </span>
               </th>
               <td>
               </td>
               <td>
               </td>
               <td style="background-color:#eeeeee">
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
            </tr>
            <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="We help you select the best insurance provider during your education.">
                     <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">Guidance on Medical Insurance</p>
                  </span>
               </th>
               <td>
               </td>
               <td>
               </td>
               <td style="background-color:#eeeeee">
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
            </tr>
            <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="We work with many ticketing partners where in we would subsidise your  cost involved with your air travel. ">
                     <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">Assistance on Ticket Booking </p>
                  </span>
               </th>
               <td>
               </td>
               <td>
               </td>
               <td style="background-color:#eeeeee">
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
            </tr>
            <tr>
               <th scope="row">
               </th>
               <td class="text-center">
                  <a href="/user/account/signup" class="btn btn-default btn-md tutor-btn">Sign Up Now</a>
               </td>
               <td class="text-center">
                  <a href="/user/account/signup" class="btn btn-default btn-md tutor-btn">Sign Up Now</a>
               </td>
               <td style="background-color: #eeeeee;" class="text-center">
                  <a href="/user/account/signup" class="btn btn-default btn-md tutor-btn">Sign Up Now</a>
               </td>
            </tr>
         </tbody>
      </table>
   </div>
   </div> -->
<!-- <div class="container">
   <div class="row">
      <p>&nbsp;</p>
   </div>
   <div class="row">
      <div class="col-md-6">
         <img style="float: left; width:100%;" alt=" " src="images/note-taking.jpg">
      </div>
      <div class="col-md-6">
         <h2>How it works</h2>
         <ul class="pricing-column-features">
            <li>
               <i class="fa fa-check-circle"></i>
               Sign up and receive a welcome email
            </li>
            <li>
               <i class="fa fa-check-circle"></i>
               Complete <b>basic form for your application</b>
            </li>
            <li>
               <i class="fa fa-check-circle"></i>
               Payment through our <b>online gateway system</b>
            </li>
            <li>
               <i class="fa fa-check-circle"></i>
               You would receive an <b>enrollment from Imperial Overseas our Empanelled Education counselors</b>
            </li>
            <li>
               <i class="fa fa-check-circle"></i>
               Imperial's dedicated Guidance counselor would reach out to you and start your process immediately.
            </li>
         </ul>
      </div>
   </div>
   <div class="row">
      <p>&nbsp;</p>
   </div>
   </div> -->
<!--link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous"-->
<script>
   // var selector1 = document.querySelector("#applications");
   // var selector2 = document.querySelector("#offers");
   // var selector3 = document.querySelector("#payments");
   
   // selector1.addEventListener("click", function() {
   //   if (this.classList.contains("active")) {
   //     this.className = "toggle";
   //   } else {
   //     this.className += " active";
   //   }
   // });
   
   // selector2.addEventListener("click", function() {
   //   if (this.classList.contains("active")) {
   //     this.className = "toggle";
   //   } else {
   //     this.className += " active";
   //   }
   // });
   
   // selector3.addEventListener("click", function() {
   //   if (this.classList.contains("active")) {
   //     this.className = "toggle";
   //   } else {
   //     this.className += " active";
   //   }
   // });
   
   // var coll = document.getElementsByClassName("collapsible");
   // var i;
   
   // for (i = 0; i < coll.length; i++) {
   // coll[i].addEventListener("click", function() {
   //  this.classList.toggle("active");
   //  var content = this.nextElementSibling;
   //  if (content.style.display === "block") {
   //    content.style.display = "none";
   //  } else {
   //    content.style.display = "block";
   //  }
   // });
   // }

   Highcharts.chart('container', {

  title: {
    text: 'Main Title'
  },

  subtitle: {
    text: 'Any subtitle'
  },

  yAxis: {
    title: {
      text: 'Number of Studnets'
    }
  },

  xAxis: {
    accessibility: {
      rangeDescription: 'Range: 2010 to 2017'
    }
  },

  legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'middle'
  },

  plotOptions: {
    series: {
      label: {
        connectorAllowed: false
      },
      pointStart: 2010
    }
  },

  series: [{
    name: 'Application',
    data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
  }, {
    name: 'Offers',
    data: [24916, 124700, 139200, 139851, 142490, 30282, 38121, 40434]
  }, {
    name: 'Payments',
    data: [111744, 17722, 116005, 119771, 120185, 24377, 232147, 39387]
  }, {
    name: 'Visa',
    data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227]
  }, {
    name: 'Other',
    data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111]
  }],

  responsive: {
    rules: [{
      condition: {
        maxWidth: 500
      },
      chartOptions: {
        legend: {
          layout: 'horizontal',
          align: 'center',
          verticalAlign: 'bottom'
        }
      }
    }]
  }

});
</script>