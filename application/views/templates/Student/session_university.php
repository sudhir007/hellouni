<style>
    .counselors-list
    {
        display: none;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Student<small>Manage Sessions</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Session Management</a></li>
            <li class="active">Universities</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <?php
                if($this->session->flashdata('flash_message'))
                {
                    ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                        <h4><i class="icon fa fa-check"></i> Success</h4>
                        <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
                    <?php
                }
                ?>

                <div class="box">
                    <form action="<?php echo base_url();?>student/session/book" method="post">
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>University Name</th>
                                        <th>Country</th>
                                        <th>Total Students</th>
                                        <th>Ranking (World)</th>
                                        <th>Admission Success Rate</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i=1;
                                    foreach($universities as $university)
                                    {
                                        ?>
                                        <tr>
                                            <td><?php echo $university->name;?></td>
                                            <td><?php echo $university->country_name;?></td>
                                            <td><?php echo $university->total_students;?></td>
                                            <td><?php echo $university->ranking_world;?></td>
                                            <td><?php echo $university->admission_success_rate . '%';?></td>
                                            <td>
                                                <a href="/admin/student/session/students?uid=<?php echo $university->id;?>&ulid=<?php echo $university->login_id;?>" target="_blank">Book Session</a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>University Name</th>
                                        <th>Country</th>
                                        <th>Total Students</th>
                                        <th>Ranking (World)</th>
                                        <th>Admission Success Rate</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </form>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
    function showCounselors(studentId)
    {
        document.getElementById("assigned-counselor-" + studentId).style.display = "none";
        document.getElementById("counselors-list-" + studentId).style.display = "block";
    }

    function updateCounselor(studentId, counselor)
    {
        var splitCounselor = counselor.split('|');
        var counselorId = splitCounselor[0];
        var counselorName = splitCounselor[1];
        $.ajax({
            type: "POST",
            url: "/admin/student/index/update",
            data: {"student_id": studentId, "facilitator_id": counselorId},
            cache: false,
            success: function(result){
                if(result == 1){
                    alert('Success');
                    document.getElementById("assigned-counselor-" + studentId).innerHTML = counselorName;
                }
                else{
                    alert(result);
                }
                document.getElementById("assigned-counselor-" + studentId).style.display = "block";
                document.getElementById("counselors-list-" + studentId).style.display = "none";
            }
        });
    }
</script>
