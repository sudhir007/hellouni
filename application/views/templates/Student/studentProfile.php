<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.7/js/dataTables.fixedHeader.min.js"></script>
<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<style type="text/css">
    .mytabs{
            margin: 2% 0px;
    }
   .mytabs .nav-tabs {
   background-color: #815dd5;
   }
   .mytabs .nav-tabs>li>a : hover {
   color: black;
   }
   .prfileBox3{
    background-color: #815dd5;
    margin: 2% 0px;
    padding: 1%;
    border-radius: 7px;
   }
   .margin2{
    margin: 2px 0 2px !important;
   }
   .ptag{
    margin: 0px;
    font-size: 13px;
    line-height: 1.8;
    letter-spacing: 1px;
   }
   .h3Heading{
    background-color: #f6882c;
    padding: 8px 2px;
    text-align: center;
    color: white;
   }

   .boxAppList{
    box-shadow: 0px 0px 12px #d8d1d1;
    border-radius: 7px;
   }

   .listtableMain thead tr {
    color: #815dd5;
    font-size: 13px;
    /* font-weight: 600; */
    letter-spacing: 0.8px;
   }

   table.dataTable.stripe tbody tr.odd, table.dataTable.display tbody tr.odd{
        background-color: #ffffff !important;
   }

   table.dataTable.display tbody tr.odd>.sorting_1, table.dataTable.order-column.stripe tbody tr.odd>.sorting_1{
        background-color: #ffffff !important;
   }

   table.dataTable.display tbody tr.even>.sorting_1, table.dataTable.order-column.stripe tbody tr.even>.sorting_1{
        background-color: #ffffff !important;
   }

   .form-control {
    height: 30px !important;
   }

   .prplButton{
    color: #fff;
    background: #8703c5;
    border-color: #8703c5;
    border-radius: 12px;
   }

   .resetButton{
        color: #8c57c6;
    background: #f1f1f1;
    border-color: #8703c5;
    border-radius: 12px;
   }

   radio-item {
  display: inline-block;
  position: relative;
  padding: 0 6px;
  /*margin: 10px 0 0;*/
}

.radio-item input[type='radio'] {
  display: none;
}

.radio-item label {
  color: #666;
  font-weight: normal;
}

.radio-item label:before {
      content: " ";
    display: inline-block;
    position: relative;
    top: 4px;
    margin: 0 5px 0 0;
    width: 15px;
    height: 15px;
    border-radius: 11px;
    border: 2px solid #f6882c;
    background-color: transparent;
}

.radio-item input[type=radio]:checked + label:after {
  border-radius: 11px;
    width: 7px;
    height: 7px;
    position: absolute;
    top: 8px;
    left: 10px;
    content: " ";
    display: block;
    background: #f6882c;
}

.tabName{
    letter-spacing: 0.6px;
    font-size: 14px;
}
</style>
<div class="container">
   <div class="col-sm-3 prfileBox3">
      <!--left col-->
      <div class="text-center">
         <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar img-circle img-thumbnail" alt="avatar" style="width: 40%;">
         <h4 style="color: white">Deepika Gavankar</h4>
      </div>
      </hr><br>
      <div class="panel panel-default">
         <div class="panel-heading"><h5 class="margin2">Basic Details &nbsp;<i class="far fa-address-card"></i></h5></div>
         <div class="panel-body"><p class="ptag"><b class="purple">Email id : </b>deepikagavankar@gmail.com<br><b class="purple">Ph. No. : </b>9987654321 </p></div>
      </div>
      <ul class="list-group">
         <li class="list-group-item text-muted"><h5 class="margin2">Activity &nbsp;<i class="fa fa-dashboard fa-1x"></i></h5></li>
         <li class="list-group-item text-right"><span class="pull-left"><strong class="purple">Basic Profile</strong></span> 75%</li>
         <li class="list-group-item text-right"><span class="pull-left"><strong class="purple">Assigned To</strong></span> Banit Sawhney</li>
         <li class="list-group-item text-right"><span class="pull-left"><strong class="purple">Intake</strong></span> Aug-Sep 2020</li>
         <li class="list-group-item text-right"><span class="pull-left"><strong class="purple">City</strong></span> Mumbai</li>
         <li class="list-group-item text-right"><span class="pull-left"><strong class="purple">Comment History</strong></span>
           <a href="/counsellor/student/offerlist/view/<?=$user_id;?>" target="_blank" style="color: #f6882c"> <b>Get Offers</b> </a></li>
      </ul>
      <!-- <div class="panel panel-default">
         <div class="panel-heading">Extra Panel</div>
         <div class="panel-body">
            <i class="fa fa-facebook fa-2x"></i> <i class="fa fa-github fa-2x"></i> <i class="fa fa-twitter fa-2x"></i> <i class="fa fa-pinterest fa-2x"></i> <i class="fa fa-google-plus fa-2x"></i>
         </div>
      </div> -->
   </div>
   <div class="col-sm-9 mytabs">
        <div class="col-md-12 boxAppList" style="margin-bottom: 3%;">
            <h3 class="h3Heading">Application List</h3>
            <!-- <table class="table">
                <thead>
                  <tr>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Email</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>John</td>
                    <td>Doe</td>
                    <td>john@example.com</td>
                  </tr>
                  <tr>
                    <td>Mary</td>
                    <td>Moe</td>
                    <td>mary@example.com</td>
                  </tr>
                  <tr>
                    <td>July</td>
                    <td>Dooley</td>
                    <td>july@example.com</td>
                  </tr>
                </tbody>
            </table> -->
            <div class="col-md-12" style="overflow: scroll;margin-bottom: 1%;">
                <table id="example" class="display listtableMain" style="width:100%">
                    <thead>
                        <tr>
                            <th>Univrsity Name</th>
                            <th>Course</th>
                            <th>Country</th>
                            <th>Age</th>
                            <th>Start date</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Tiger Nixon</td>
                            <td>System Architect</td>
                            <td>Edinburgh</td>
                            <td>61</td>
                            <td>2011/04/25</td>
                            <td>$320,800</td>
                        </tr>

                        <tr>
                            <td>Airi Satou</td>
                            <td>Accountant</td>
                            <td>Tokyo</td>
                            <td>33</td>
                            <td>2008/11/28</td>
                            <td>$162,700</td>
                        </tr>
                        <tr>
                            <td>Brielle Williamson</td>
                            <td>Integration Specialist</td>
                            <td>New York</td>
                            <td>61</td>
                            <td>2012/12/02</td>
                            <td>$372,000</td>
                        </tr>
                        <tr>
                            <td>Herrod Chandler</td>
                            <td>Sales Assistant</td>
                            <td>San Francisco</td>
                            <td>59</td>
                            <td>2012/08/06</td>
                            <td>$137,500</td>
                        </tr>
                        <tr>
                            <td>Rhona Davidson</td>
                            <td>Integration Specialist</td>
                            <td>Tokyo</td>
                            <td>55</td>
                            <td>2010/10/14</td>
                            <td>$327,900</td>
                        </tr>
                        <tr>
                            <td>Colleen Hurst</td>
                            <td>Javascript Developer</td>
                            <td>San Francisco</td>
                            <td>39</td>
                            <td>2009/09/15</td>
                            <td>$205,500</td>
                        </tr>
                        <tr>
                            <td>Sonya Frost</td>
                            <td>Software Engineer</td>
                            <td>Edinburgh</td>
                            <td>23</td>
                            <td>2008/12/13</td>
                            <td>$103,600</td>
                        </tr>
                        <tr>
                            <td>Jena Gaines</td>
                            <td>Office Manager</td>
                            <td>London</td>
                            <td>30</td>
                            <td>2008/12/19</td>
                            <td>$90,560</td>
                        </tr>
                        <tr>
                            <td>Quinn Flynn</td>
                            <td>Support Lead</td>
                            <td>Edinburgh</td>
                            <td>22</td>
                            <td>2013/03/03</td>
                            <td>$342,000</td>
                        </tr>
                        <tr>
                            <td>Charde Marshall</td>
                            <td>Regional Director</td>
                            <td>San Francisco</td>
                            <td>36</td>
                            <td>2008/10/16</td>
                            <td>$470,600</td>
                        </tr>
                        <tr>
                            <td>Haley Kennedy</td>
                            <td>Senior Marketing Designer</td>
                            <td>London</td>
                            <td>43</td>
                            <td>2012/12/18</td>
                            <td>$313,500</td>
                        </tr>
                        <tr>
                            <td>Tatyana Fitzpatrick</td>
                            <td>Regional Director</td>
                            <td>London</td>
                            <td>19</td>
                            <td>2010/03/17</td>
                            <td>$385,750</td>
                        </tr>
                        <tr>
                            <td>Michael Silva</td>
                            <td>Marketing Designer</td>
                            <td>London</td>
                            <td>66</td>
                            <td>2012/11/27</td>
                            <td>$198,500</td>
                        </tr>
                        <tr>
                            <td>Paul Byrd</td>
                            <td>Chief Financial Officer (CFO)</td>
                            <td>New York</td>
                            <td>64</td>
                            <td>2010/06/09</td>
                            <td>$725,000</td>
                        </tr>

                        <tr>
                            <td>Jackson Bradshaw</td>
                            <td>Director</td>
                            <td>New York</td>
                            <td>65</td>
                            <td>2008/09/26</td>
                            <td>$645,750</td>
                        </tr>
                        <tr>
                            <td>Olivia Liang</td>
                            <td>Support Engineer</td>
                            <td>Singapore</td>
                            <td>64</td>
                            <td>2011/02/03</td>
                            <td>$234,500</td>
                        </tr>

                        <tr>
                            <td>Zenaida Frank</td>
                            <td>Software Engineer</td>
                            <td>New York</td>
                            <td>63</td>
                            <td>2010/01/04</td>
                            <td>$125,250</td>
                        </tr>
                        <tr>
                            <td>Zorita Serrano</td>
                            <td>Software Engineer</td>
                            <td>San Francisco</td>
                            <td>56</td>
                            <td>2012/06/01</td>
                            <td>$115,000</td>
                        </tr>
                        <tr>
                            <td>Jennifer Acosta</td>
                            <td>Junior Javascript Developer</td>
                            <td>Edinburgh</td>
                            <td>43</td>
                            <td>2013/02/01</td>
                            <td>$75,650</td>
                        </tr>

                        <tr>
                            <td>Lael Greer</td>
                            <td>Systems Administrator</td>
                            <td>London</td>
                            <td>21</td>
                            <td>2009/02/27</td>
                            <td>$103,500</td>
                        </tr>

                        <tr>
                            <td>Donna Snider</td>
                            <td>Customer Support</td>
                            <td>New York</td>
                            <td>27</td>
                            <td>2011/01/25</td>
                            <td>$112,000</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Office</th>
                            <th>Age</th>
                            <th>Start date</th>
                            <th>Salary</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div class="col-md-12 boxAppList" style="padding: 2% 2%;">
          <ul class="nav nav-tabs" style="padding-top: 9px;padding-left: 9px;">
             <li class="active"><a data-toggle="tab" href="#home"><b class="tabName">Home</b></a></li>
             <li><a data-toggle="tab" href="#messages"><b class="tabName">Menu 1</b></a></li>
             <li><a data-toggle="tab" href="#settings"><b class="tabName">Menu 2</b></a></li>
          </ul>
          <div class="tab-content">
             <div class="tab-pane active" id="home">
                <hr>
                <form class="form" action="##" method="post" id="registrationForm">
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="first_name">
                            <h5>First name</h5>
                         </label>
                         <input type="text" class="form-control" name="first_name" id="first_name" placeholder="first name" title="enter your first name if any.">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="last_name">
                            <h5>Last name</h5>
                         </label>
                         <input type="text" class="form-control" name="last_name" id="last_name" placeholder="last name" title="enter your last name if any.">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="phone">
                            <h5>Phone</h5>
                         </label>
                         <input type="text" class="form-control" name="phone" id="phone" placeholder="enter phone" title="enter your phone number if any.">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="mobile">
                            <h5>Mobile</h5>
                         </label>
                         <input type="text" class="form-control" name="mobile" id="mobile" placeholder="enter mobile number" title="enter your mobile number if any.">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="email">
                            <h5>Email</h5>
                         </label>
                         <input type="email" class="form-control" name="email" id="email" placeholder="you@email.com" title="enter your email.">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="email">
                            <h5>City</h5>
                         </label>
                         <select name="City" id="City" class="form-control " >
                           <option value="0">Select city</option>
                           <option value="Mumbai" >Mumbai</option>
                           <option value="Delhi" >Delhi</option>
                           <option value="Pune" >Pune</option>
                           <option value="Bangalore" >Bangalore</option>
                        </select>
                      </div>
                   </div>
                    <div class="form-group">
                        <div class="col-xs-6">
                           <label for="email">
                             <h5>Gender</h5>
                           </label>
                           <div class="col-md-12">
                           <form class="radio-item form-control">
                            <label class="radio-inline">
                              <input type="radio" name="optradio" checked>Male
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="optradio">Female
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="optradio">Transgender
                            </label>
                          </form></div>
                        </div>
                    </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="password">
                            <h5>Level Course</h5>
                         </label>
                         <select name="degree" id="degree" class="searchoptions form-control " >
                           <option value="0">Select Level Of Course</option>
                           <option value="1" >1</option>
                           <option value="2" >1</option>
                           <option value="3" >3</option>
                           <option value="4" >4</option>
                        </select>
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="password2">
                            <h5>Level Type</h5>
                         </label>
                         <select class="selectpicker form-control" multiple data-live-search="true">
                          <option>Type 1</option>
                          <option>Type 2</option>
                          <option>Type 3</option>
                          <option>Type 4</option>
                          <option>Type 5</option>
                          <option>Type 6</option>
                        </select>
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-12">
                         <br>
                         <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                         <button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                      </div>
                   </div>
                </form>
                <hr>
             </div>
             <!--/tab-pane-->
             <div class="tab-pane" id="messages">
                <h2></h2>
                <hr>
                <form class="form" action="##" method="post" id="registrationForm">
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="first_name">
                            <h5>First name</h5>
                         </label>
                         <input type="text" class="form-control" name="first_name" id="first_name" placeholder="first name" title="enter your first name if any.">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="last_name">
                            <h5>Last name</h5>
                         </label>
                         <input type="text" class="form-control" name="last_name" id="last_name" placeholder="last name" title="enter your last name if any.">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="phone">
                            <h5>Phone</h5>
                         </label>
                         <input type="text" class="form-control" name="phone" id="phone" placeholder="enter phone" title="enter your phone number if any.">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="mobile">
                            <h5>Mobile</h5>
                         </label>
                         <input type="text" class="form-control" name="mobile" id="mobile" placeholder="enter mobile number" title="enter your mobile number if any.">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="email">
                            <h5>Email</h5>
                         </label>
                         <input type="email" class="form-control" name="email" id="email" placeholder="you@email.com" title="enter your email.">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="email">
                            <h5>Location</h5>
                         </label>
                         <input type="email" class="form-control" id="location" placeholder="somewhere" title="enter a location">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="password">
                            <h5>Password</h5>
                         </label>
                         <input type="password" class="form-control" name="password" id="password" placeholder="password" title="enter your password.">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="password2">
                            <h5>Verify</h5>
                         </label>
                         <input type="password" class="form-control" name="password2" id="password2" placeholder="password2" title="enter your password2.">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-12">
                         <br>
                         <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                         <button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                      </div>
                   </div>
                </form>
             </div>
             <!--/tab-pane-->
             <div class="tab-pane" id="settings">
                <hr>
                <form class="form" action="##" method="post" id="registrationForm">
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="first_name">
                            <h5>First name</h5>
                         </label>
                         <input type="text" class="form-control" name="first_name" id="first_name" placeholder="first name" title="enter your first name if any.">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="last_name">
                            <h5>Last name</h5>
                         </label>
                         <input type="text" class="form-control" name="last_name" id="last_name" placeholder="last name" title="enter your last name if any.">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="phone">
                            <h5>Phone</h5>
                         </label>
                         <input type="text" class="form-control" name="phone" id="phone" placeholder="enter phone" title="enter your phone number if any.">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="mobile">
                            <h5>Mobile</h5>
                         </label>
                         <input type="text" class="form-control" name="mobile" id="mobile" placeholder="enter mobile number" title="enter your mobile number if any.">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="email">
                            <h5>Email</h5>
                         </label>
                         <input type="email" class="form-control" name="email" id="email" placeholder="you@email.com" title="enter your email.">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="email">
                            <h5>Location</h5>
                         </label>
                         <input type="email" class="form-control" id="location" placeholder="somewhere" title="enter a location">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="password">
                            <h5>Password</h5>
                         </label>
                         <input type="password" class="form-control" name="password" id="password" placeholder="password" title="enter your password.">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="password2">
                            <h5>Verify</h5>
                         </label>
                         <input type="password" class="form-control" name="password2" id="password2" placeholder="password2" title="enter your password2.">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-12">
                         <br>
                         <button class="btn btn-lg prplButton pull-right" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                         <!--<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>-->
                      </div>
                   </div>
                </form>
             </div>
          </div>
          <!--/tab-pane-->
        </div>
   </div>
   <!--/tab-content-->
</div>
<!--/col-9-->
</div>
<script type="text/javascript">
   $(document).ready(function() {


   var readURL = function(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();

           reader.onload = function (e) {
               $('.avatar').attr('src', e.target.result);
           }
   
           reader.readAsDataURL(input.files[0]);
       }
   }


   $(".file-upload").on('change', function(){
       readURL(this);
   });
   });

   $(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#example thead tr').clone(true).appendTo( '#example thead' );
    $('#example thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );

        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    var table = $('#example').DataTable( {
        orderCellsTop: true
    } );
} );
</script>
