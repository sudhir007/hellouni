<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<script type="text/javascript" src="
https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<style type="text/css">
   .styleCss{
   font-size: 14px;
   background-color: #f6882c;
   color: white;
   font-weight: 600;
   letter-spacing: 0.8px;
   }
   .formControl{
   color: #fff !important;
   background-color: #8c57c5 !important;
   /* border: 0px; */
   font-size: 12px;
   border: 0px !important;
   box-shadow: inset 0 0 0 rgba(0,0,0,.075) !important;
   margin-top: 15px;
   }

   #dt-example td.details-control {
     cursor: pointer;
}
 #dt-example tr.shown td {
     background-color: #815dd5;
     color: #fff !important;
     border: none;
     border-right: 1px solid #815dd5;
}
 #dt-example tr.shown td:last-child {
     border-right: none;
}
 #dt-example tr.details-row .details-table tr:first-child td {
     color: #fff;
     background: #f6882c;
     border: none;
}
 #dt-example tr.details-row .details-table > td {
     padding: 0;
}
 #dt-example tr.details-row .details-table .fchild td:first-child {
     cursor: pointer;
}
 #dt-example tr.details-row .details-table .fchild td:first-child:hover {
     background: #fff;
}
 #dt-example .form-group.agdb-dt-lst {
     padding: 2px;
     height: 23px;
     margin-bottom: 0;
}
 #dt-example .form-group.agdb-dt-lst .form-control {
     height: 23px;
     padding: 2px;
}
 #dt-example .adb-dtb-gchild {
     padding-left: 2px;
}
 #dt-example .adb-dtb-gchild td {
     background: #f5fafc;
     padding-left: 15px;
}
 #dt-example .adb-dtb-gchild td:first-child {
     cursor: default;
}
 #dt-example .fchild ~ .adb-dtb-gchild {
    /* display: none;
     */
}
 .dataTables_wrapper{
    overflow: auto;
 }

.listTableView thead tr{
 background-color: #f6882c;
    color: white;
}
</style>
<div class="container">
   <div class="row" style="padding: 30px 0px 30px 0px;display: flex;">
      <div class="col-md-3 purpleBg" style="box-shadow: 0px 0px 8px 0px #dcd9d9;    padding: 5px;">
         <div class="col-md-12" style="border-radius: 6px;text-align: center;background-color: white;">
            <h4 style="color: #f6882c;">Filters</h4>
         </div>
         <div class="col-md-12">
            <label class="form-control formControl">From Date</label>
            <input type="date" id="fromDate" name="fromDate" class="searchoptions form-control ht30">
         </div>
         <div class="col-md-12">
            <label class="form-control formControl">To Date</label>
            <input type="date" id="toDate" name="toDate" class="searchoptions form-control ht30">
         </div>
         <div class="col-md-12">
            <label class="form-control formControl">COUNTRY *</label>
            <select name="country" id="country" class="searchoptions form-control ht30" >
               <option value="0">Select Country</option>
               <option value="USA"  >USA</option>
               <option value="Canada"  >CANADA</option>
               <option value="Australia"  >AUSTRALIA</option>
               <option value="Germany"  >GERMANY</option>
               <option value="New Zealand"  >NEW ZEALAND</option>
               <option value="Singapore"  >SINGAPORE</option>
               <option value="Ireland"  >IRELAND</option>
               <option value="UK"  >UK</option>
               <option value="Dubai"  >DUBAI</option>
               <option value="China"  >CHINA</option>
               <option value="Russia"  >RUSSIA</option>
               <option value="Philippines"  >PHILIPPINES</option>
               <option value="Switzerland"  >SWITZERLAND</option>
               <option value="Poland"  >POLAND</option>
               <option value="Netherlands"  >NETHERLANDS</option>
               <option value="Italy"  >ITALY</option>
               <option value="France"  >FRANCE</option>
               <option value="Japan"  >JAPAN</option>
            </select>
         </div>
         <div class="col-md-12">
            <label class="form-control formControl">INTAKE</label>
            <select class=" form-control ht30" >
               <option value="">Select Intake Month</option>
               <option value="January">January</option>
               <option value="February">February</option>
               <option value="March">March</option>
               <option value="April">April</option>
               <option value="May">May</option>
               <option value="June">June</option>
               <option value="July">July</option>
               <option value="August">August</option>
               <option value="September">September</option>
               <option value="October">October</option>
               <option value="November">November</option>
               <option value="December">December</option>
            </select>
         </div>
         <div class="col-md-12">
            <label class="form-control formControl">Application Status</label>
            <select name="subcourse1" id="subcourse1" class="searchoptions form-control ht30" >
               <option value="0">Select Substream</option>
            </select>
         </div>
         <div class="col-md-12">
            <label class="form-control formControl">Offer Type</label>
            <select name="degree" id="degree" class="searchoptions form-control ht30" >
               <option value="0">Select Level Of Course</option>
               <option value="1" >1</option>
               <option value="2" >1</option>
               <option value="3" >3</option>
               <option value="4" >4</option>
            </select>
         </div>
         <!-- <div class="col-md-12">
            <div class="col-lg-12 col-md-12 text-right" style="color:#000;">
               <input type="submit" class="btn btn-md addStuBtn" value="SEARCH" name="search" id="search">
            </div>
            </div> -->
      </div>
      <div class="col-md-9" >
        <div class="col-md-12">
    <div class="panel panel-default">        
        <div class="panel-body listTableView">
<!-- Dynamic table was here -->
<table id="dt-example" class="stripe row-border order-column cl-table dataTable no-footer" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th>Name</th>
      <th>Email</th>
      <th>Country</th>
      <th>Created Date</th>
      <th>Status</th>
      <th>Calls</th>
      <th>Duration</th>
    </tr>
  </thead>
  <tbody>
    <!-- <tr>
      <td>Techwyse</td>
      <td>22</td>
      <td>10</td>
      <td>12</td>
      <td>23 Feb 2017</td>
      <td>Active</td>
      <td>5,290</td>
      <td>7,245m</td>
      <td>$2,275.75</td>
      <td>[V][E]</td>
    </tr>
    <tr>
      <td>Advanced Plumbing</td>
      <td>22</td>
      <td>10</td>
      <td>12</td>
      <td>23 Feb 2017</td>
      <td>Active</td>
      <td>5,290</td>
      <td>7,245m</td>
      <td>$2,275.75</td>
      <td>[V][E]</td>
    </tr>
    <tr>
      <td>API ALarm Inc.</td>
      <td>22</td>
      <td>10</td>
      <td>12</td>
      <td>23 Feb 2017</td>
      <td>Active</td>
      <td>5,290</td>
      <td>7,245m</td>
      <td>$2,275.75</td>
      <td>[V][E]</td>
    </tr> -->
  </tbody>
</table>
  </div>
                
 </div>
</div>
      </div>
   </div>
</div>
<script>
     
        $('#data-table').dataTable();
        $(".dataTable").on("draw.dt", function (e) {
          //console.log("drawing");
          setCustomPagingSigns.call($(this));
        }).each(function () {
          setCustomPagingSigns.call($(this)); // initialize
        });
        function setCustomPagingSigns() {
          var wrapper = this.parent();
          wrapper.find("a.previous").text("<");
          wrapper.find("a.next").text(">");
        }
// =============Display details onclick===============
var data = [
    {"Name":"Deepika","Email":"deepika@gmail.com","Country":"India","created_date":"27.159.97.60","status":"active","calls":"22","duration":"50"},
    {"Name":"Sudhir","Email":"s@gmail.com","Country":"USA","created_date":"67.135.55.135","status":"active","calls":"22","duration":"25"},
    {"Name":"Nikhil","Email":"n@gmail.com","Country":"UK","created_date":"74.193.127.5","status":"active","calls":"22","duration":"56"},
    {"Name":"Banit","Email":"b@gmail.com","Country":"Australia","created_date":"4.104.253.210","status":"active","calls":"22","duration":"22"}        
  ];
  
  var gChild1 = '<tr class="adb-dtb-gchild">'+
                  '<td>'+
                  'Local'+
                  '</td>'+              
                  '<td>22</td>'+
                  '<td>21</td>'+
                  '<td>20</td>'+
                  '<td>21</td>'+
                  '<td>18</td>'+
                  '<td>19</td>'+
                  '<td>16</td>'+
                  '<td>22</td>'+
                  '<td>13</td>'+
                  '<td>16</td>'+
                  '<td>20</td>'+
                  '<td>21</td>'+
                  '<td>19</td>'+'</tr>';
  var gChild2 = '<tr class="adb-dtb-gchild">'+
                  '<td>'+
                  'Toll Free'+
                  '</td>'+              
                  '<td>22</td>'+
                  '<td>21</td>'+
                  '<td>20</td>'+
                  '<td>21</td>'+
                  '<td>18</td>'+
                  '<td>19</td>'+
                  '<td>16</td>'+
                  '<td>22</td>'+
                  '<td>13</td>'+
                  '<td>16</td>'+
                  '<td>20</td>'+
                  '<td>21</td>'+
                  '<td>19</td>'+'</tr>';

  function format (data) {
      return '<div class="details-container">'+
          '<table cellpadding="0" cellspacing="0" border="0" class="details-table" style="width: -webkit-fill-available;">'+
              '<tr>'+
                  '<td>'+
                  '<div class="form-group agdb-dt-lst"><select class="form-control" id="dt-sel-year"><option value="cYear">Current Year</option><option>2016</option><option>2015</option><option>2014</option></select></div>'+
                  '</td>'+
                  '<td>Total</td>'+
                  '<td>May &apos;17</td>'+
                  '<td>Apr &apos;17</td>'+
                  '<td>Mar &apos;17</td>'+
                  '<td>Feb &apos;17</td>'+
                  '<td>Jan &apos;17</td>'+
                  '<td>Dec &apos;16</td>'+
                  '<td>Nov &apos;16</td>'+
                  '<td>Oct &apos;16</td>'+
                  '<td>Sep &apos;16</td>'+
                  '<td>Aug &apos;16</td>'+
                  '<td>Jul &apos;16</td>'+
                  '<td>Jun &apos;16</td>'+
              '</tr>'+
              '<tr class="fchild">'+
                  '<td>'+
                  'Numbers'+
                  '</td>'+              
                  '<td>22</td>'+
                  '<td>21</td>'+
                  '<td>20</td>'+
                  '<td>21</td>'+
                  '<td>18</td>'+
                  '<td>19</td>'+
                  '<td>16</td>'+
                  '<td>22</td>'+
                  '<td>13</td>'+
                  '<td>16</td>'+
                  '<td>20</td>'+
                  '<td>21</td>'+
                  '<td>19</td>'+
                '</tr>'+
                gChild1 +
                gChild2 +
                '<tr class="fchild">'+
                  '<td>'+
                  'Calls'+
                  '</td>'+              
                  '<td>22</td>'+
                  '<td>21</td>'+
                  '<td>20</td>'+
                  '<td>21</td>'+
                  '<td>18</td>'+
                  '<td>19</td>'+
                  '<td>16</td>'+
                  '<td>22</td>'+
                  '<td>13</td>'+
                  '<td>16</td>'+
                  '<td>20</td>'+
                  '<td>21</td>'+
                  '<td>19</td>'+
                '</tr>'+
                '<tr class="fchild">'+
                  '<td>'+
                  'Duration'+
                  '</td>'+              
                  '<td>22</td>'+
                  '<td>21</td>'+
                  '<td>20</td>'+
                  '<td>21</td>'+
                  '<td>18</td>'+
                  '<td>19</td>'+
                  '<td>16</td>'+
                  '<td>22</td>'+
                  '<td>13</td>'+
                  '<td>16</td>'+
                  '<td>20</td>'+
                  '<td>21</td>'+
                  '<td>19</td>'+
              '</tr>'+
              '<tr class="fchild">'+
                  '<td>'+
                  'Invoice'+
                  '</td>'+              
                  '<td>22</td>'+
                  '<td>21</td>'+
                  '<td>20</td>'+
                  '<td>21</td>'+
                  '<td>18</td>'+
                  '<td>19</td>'+
                  '<td>16</td>'+
                  '<td>22</td>'+
                  '<td>13</td>'+
                  '<td>16</td>'+
                  '<td>20</td>'+
                  '<td>21</td>'+
                  '<td>19</td>'+
              '</tr>'+
            '</table>'+
        '</div>';
  };
  
  var table = $('.dataTable').DataTable({

    // Column definitions
    columns : [      
        {
            data : 'Name',
            className : 'details-control',
        },
        {data : 'Email'},
        {data : 'Country'},
        {data : 'created_date'},
        {data : 'status'},
        {data : 'calls'},
        {data : 'duration'}
    ],    
    data : data,    
    pagingType : 'full_numbers',    
    // Localization
    language : {
      emptyTable     : 'No data to display.',
      zeroRecords    : 'No records found!',
      thousands      : ',',
      // processing     : 'Processing...',
      loadingRecords : 'Loading...',
      search         : 'Search:',
      paginate       : {
        next     : 'next',
        previous : 'previous'
      }
    }
  });
 
  $('.dataTable tbody').on('click', 'td.details-control', function () {
     var tr  = $(this).closest('tr'),
         row = table.row(tr);
    
     if (row.child.isShown()) {
       tr.next('tr').removeClass('details-row');
       row.child.hide();
       tr.removeClass('shown');
     }
     else {
       row.child(format(row.data())).show();
       tr.next('tr').addClass('details-row');
       tr.addClass('shown');
     }
  });

$('#dt-example').on('click','.details-table tr',function(){
  $('.details-table').find('.adb-dtb-gchild:not(:first-child)').slideToggle();       
    });

//dropdown style
setTimeout(function(){
  $('#dt-sel-year').select2({});
}, 2000);
</script>