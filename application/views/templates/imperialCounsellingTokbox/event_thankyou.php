<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>HelloUni Visa Seminar</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="https://hellouni.org/admin/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://hellouni.org/admin/bootstrap/css/bootstrap.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="https://hellouni.org/admin/dist/css/AdminLTE.min.css">

        <link rel="icon" type="image/png" sizes="96x96" href="https://hellouni.org/admin/images/favicon-96x96.png">
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition login-page" style="background-color: white;">
        <div class="login-box">
            <div class="login-logo">
                <img src="https://hellouni.org/admin/images/HelloUni-Logo.png" style="width: 75%;">
            </div>
            <div class="login-box-body">
                <form action="/test/load" method="get">
                    <div class="form-group has-feedback" >
                        <img src="<?php echo base_url();?>application/images/thankYou.jpg" alt="thankYou" width="100%" height="100%">
                    </div>
                </form>
            </div>
        </div>



        <script>
          $(document).ready(function() {
            $('#overlay').modal('show');
            /*setTimeout(function() {
              $('#overlay').modal('hide');
            }, 5000);*/


          });

           function submitFeedbackApplication(){

                var username = $("#event_username").val();
                var rate_event = $("#rate_event").val();
                var event_suggestion = $("#event_suggestion").val();

                var dataString = 'username=' + username + '&rate_event=' + rate_event + '&event_suggestion=' + event_suggestion;

               event.preventDefault();
                  $.ajax({
                    type: "POST",
                    url: "/user/account/submitFeedbackApplication",
                    data: dataString,
                    cache: false,
                    success: function(result) {
                        //alert(result);
                      if(result == "SUCCESS") {
                        alert("Feedback Submitted Successfully!!!");
                        location.reload();
                      } else {
                        alert(result);
                        location.reload();
                      }
                    }
                  });
            }
        </script>

        <body>
          <div class="modal fade" id="overlay">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <!-- <h4 class="modal-title">Student Feedback</h4> -->
                  <h2 class="text-center" style="color: #8c57c5;">Student Feedback</h2>
                </div>
                <div class="modal-body">
                  <!-- <p>Context here</p> -->
                  <div class="col-md-12 form-group">
                      <div class="col-md-12">
                        <label for="inputEmail3" class="text-uppercase"> Rate Event :</label>
                         <select name="rate_event" id="rate_event" class="form-control">
                           <option value="">Select Rating</option>
                           <option value="1">1</option>
                           <option value="2">2</option>
                           <option value="3">3</option>
                           <option value="4">4</option>
                           <option value="5">5</option>

                         </select>
                        
                      </div>
                    </div>

                    <div class="col-md-12 form-group">
                      <div class="col-md-12">
                        <label for="inputEmail3" class="text-uppercase"> Suggestion :</label>
                        <textarea class="form-control" rows="5" id="event_suggestion" style="height: auto !important;"></textarea>
                         
                      </div>
                    </div>

                    <div class="form-check" style="text-align: center;">
                        <input type="button"  onclick="submitFeedbackApplication()" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Submit"></input>
                        <input type="hidden" name="event_username" id="event_username" value="<?=$username?>">
                    </div>
                </div>
              </div>
            </div>
          </div>
        </body>

 
    </body>
</html>
