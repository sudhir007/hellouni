<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
<style>
   div {
       background: 0 0;
       border: 0;
       margin: 0;
       outline: 0;
       padding: 0;
       vertical-align: baseline;
   }
   .bannerSection{
       background-color: rgba(0, 0, 0, 0);
       background-repeat: no-repeat;
       /*background-image: url(<?php echo base_url();?>application/images/loan_mela_january_2022/banner.jpg);*/
       background-size: cover;
       background-position: center center;
       width: 100%;
   }
   .sectionClass {
       padding: 20px 0px 50px 0px;
       position: relative;
       display: block;
   }
   .projectFactsWrap{
       display: flex;
       margin-top: 30px;
       flex-direction: row;
       flex-wrap: wrap;
   }
   .projectFactsWrap .item{
       width: 20%;
       height: 100%;
       padding: 25px 5px;
       text-align: center;
       border-radius: 25%;
       margin: 20px;
       margin-top: -150px;
   }
   .projectFactsWrap .item:nth-child(1){
       background: rgb(246 136 44);
   }
   .projectFactsWrap .item:nth-child(2){
       background: rgb(246 136 44);
   }
   .projectFactsWrap .item:nth-child(3){
       background: rgb(246 136 44);
   }
   .projectFactsWrap .item:nth-child(4){
       background: rgb(246 136 44);
   }
   .projectFactsWrap .item p.number{
       font-size: 25px;
       padding: 0;
       font-weight: bold;
   }
   .projectFactsWrap .item p{
       color: black;
       font-size: 16px;
       margin: 0;
       padding: 10px;
       font-family: 'Open Sans';
       font-weight: 600;
   }
   .projectFactsWrap .item span{
       width: 60px;
       background: rgba(255, 255, 255, 0.8);
       height: 2px;
       display: block;
       margin: 0 auto;
   }
   .projectFactsWrap .item i{
       vertical-align: middle;
       font-size: 50px;
       color: rgba(255, 255, 255, 0.8);
   }
   .projectFactsWrap .item:hover i, .projectFactsWrap .item:hover p{
       color: white;
   }
   .projectFactsWrap .item:hover span{
       background: white;
   }
   @media (max-width: 786px){
       .projectFactsWrap .item {
           flex: 0 0 50%;
       }
   }
   h1:after{
       content: "";
       position: absolute;
       left: 40%;
       margin-left: 0;
       bottom: 0;
       width: 20%;
       border-bottom: 4px solid white;
   }
   h1:before {
       content: "";
       position: absolute;
       left: 40%;
       margin-left: 0;
       top: 0;
       width: 20%;
       border-bottom: 4px solid white;
   }
   .inset {
       padding: 15px;
    }
   @media screen and (max-width: 992px){
       .inset {
           padding: 15px;
        }
   }
   .team-style-five .team-items .item {
       -moz-box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
       -webkit-box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
       -o-box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
       box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
       padding-top: 0px;
       background-color: #62367d;
       margin: 10px;
       height: 115px;
       border-radius: 10px;
   }
   .team-style-five .single-item.team-standard .thumb {
       overflow: hidden;
   }
   .team-style-five .team-items .item .thumb img{
       width: 100%;
       height: 28%;
   }
   .team-style-five .team-items .item .thumb {
       position: relative;
       z-index: 1;
       height: 115px;
   }
   .team-style-five .team-items .item .info {
       background: #ffffff none repeat scroll 0 0;
       padding: 20px 20px;
   }
   .team-style-five .team-items .item .info h3 {
       font-weight: 700;
       margin-bottom: 5px;
       font-size: 18px;
       font-family:"montserrat", sans-serif;
       text-transform: uppercase;
   }
   .team-style-five .single-item.team-standard .item .info span {
       font-size: 13px;
       color: #1cb9c8;
   }
   .team-style-five .single-item.team-standard .item .info .spanName::after {
       background: #1cb9c8 none repeat scroll 0 0;
       bottom: 0;
       content: "";
       height: 2px;
       left: 50%;
       margin-left: -20px;
       position: absolute;
       width: 40px;
   }
   .team-style-five .team-items .item .info p {
       margin-bottom: 0;
       color: #666666;
       text-align: left;
       font-size: 14px;
   }
   @import url("https://fonts.googleapis.com/css?family=Lato:400,400i,700");
   .joinbtn{
       background: #f6872c !important;
       border-color: #f6872c !important;
       border-radius: 5px;
       display: inline;
   }
   .joinbtn-queue{
       background: #f6872c !important;
       border-color: #f6872c !important;
       border-radius: 5px;
       display: none;
   }
   @media only screen and (min-width: 1700px) {
       .projectFactsWrap .item{
           width: 17%;
           margin-left: 6%;
       }
   }

   .pulse {
    display: block;
    border-radius: 8px;
    background: #f6882c;
    cursor: pointer;
    transform: scale(1);
    box-shadow: 0 0 0 rgb(246 136 44);
    animation: pulse 1.5s infinite;
}
    .pulse:hover {
      animation: none;
    }

    @-webkit-keyframes pulse {
      0% {
        transform: scale(0.95);
        -webkit-box-shadow: 0 0 0 0 rgba(98,54,125, 0.4);
      }
      70% {
        transform: scale(1);
          -webkit-box-shadow: 0 0 0 10px rgba(98,54,125, 1);
      }
      100% {
        transform: scale(0.95);
          -webkit-box-shadow: 0 0 0 0 rgba(98,54,125, 0);
      }
    }
    @keyframes pulse {
      0% {
        transform: scale(0.95);
        -moz-box-shadow: 0 0 0 0 rgba(98,54,125, 0.4);
        box-shadow: 0 0 0 0 rgba(98,54,125, 0.4);
      }
      70% {
        transform: scale(1);
          -moz-box-shadow: 0 0 0 10px rgba(98,54,125, 0);
          box-shadow: 0 0 0 10px rgba(98,54,125, 0);
      }
      100% {
        transform: scale(0.95);
          -moz-box-shadow: 0 0 0 0 rgba(98,54,125, 0);
          box-shadow: 0 0 0 0 rgba(98,54,125, 0);
      }
    }

    .modal{
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        z-index: 112;
    }

    .modal-content{
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 50%;
    }

    #modal-iframe-content{
        width: 70%;
        height: 80%;
    }

    #queue-message {
      position: fixed;
      background-color: #F6881F;
      top: 35%;
      z-index: 5;
      width: 200px;
    }
</style>

<section class="bannerSection">
    <div class="container" style="height: 250px;">
        
    </div>
</section>

<div id="queue-message" style="text-align: center; font-size:18px; font-weight: bold; font-family: monospace; margin-top: 10px;"></div>

<div class="content-wrapper">
    <section>
        <div class="container-fluid">
            <div class="row scrollSpyArea" >
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="page-content inset" data-spy="scroll" data-target="#spy">
                        <div class="row team-style-five">
                            <div class="jumbotron text-center row " style="margin-bottom: 15px;">
                                <h2 style="font-size: 30px">Thank you for joining you are in queue...!!!</h2>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    var chk_queue = setInterval(function() {
        $.ajax({
            url: '/imperialCounsellingTokbox/event/check_queue/<?=$username?>',
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                data = JSON.parse(data);
                if(data['success']){
                    /*if(data['join'] == "YES"){
                        window.location.href = "/imperialCounsellingTokbox/event/student/" + data['room_id'] + "/<?=$username?>";
                    }
                    else{
                        window.location.href = "/imperialCounsellingTokbox/event/student/" + data['room_id'] + "/<?=$username?>";
                    }*/

                    window.location.href = "/imperialCounsellingTokbox/event/student/" + data['room_id'] + "/<?=$username?>";
                }
            },
            error: function error() {
            }
        });
    }, 10000);
</script>
