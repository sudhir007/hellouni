<style type="text/css">
    @import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");
    .login-block{
        background: #004b7a;  /* fallback for old browsers */
        background: -webkit-linear-gradient(to bottom, #fafafb, #d8d9da);  /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to bottom, #fafafb, #d8d9da); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
        float:left;
        width:100%;
        padding : 50px 0;
    }
    .login-sec{padding: 50px 30px; position:relative;}
    .login-sec .copy-text{position:absolute; width:80%; bottom:20px; font-size:13px; text-align:center;}
    .login-sec h2{margin-bottom:30px; font-weight:800; font-size:30px; color: #004b7a;}
    .login-sec h2:after{content:" "; width:100px; height:5px; background:#f9992f; display:block; margin-top:20px; border-radius:3px; margin-left:auto;margin-right:auto}
    .btn-login{background: #f9992f; color:#fff; font-weight:600;}
    .form-control{
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    }
    .table>tbody>tr>td{
        text-align: center;
        padding: 10px;
        line-height: 1.42857143;
        vertical-align: middle;
        border-top: 1px solid #ddd;
        font-size: 14px;
        font-family: sans-serif;
        letter-spacing: 0.8px;
        font-weight: 600;
        width: 51%;
    }

    fieldset {
        padding: .35em .625em .75em;
        margin: 0 2px;
        border: 1px solid silver;
    }

    legend {
        padding: 0;
        border: 0;
        margin-bottom: 20px;
        width: unset;
    }

    .multiselect {
  width: auto;
  margin: 5px;
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 100%;
  font-weight: bold;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes label {
  display: block;
}

#checkboxes label:hover {
  background-color: #1e90ff;
}
</style>

 <section class="login-block" id="forgot-password-page">
    <div class="container" style="background:#fff; border-radius: 10px; box-shadow: 5px 5px 0px rgba(0,0,0,0.1);">

        <div class="row">
            <div class="col-md-12 login-sec">
                <h2 class="text-center"> Register For The Imperial Counselling Information Session !</h2>
                <form class="login-form" class="form-horizontal" onsubmit="resgistration()" method="post" autocomplete="off">
                    <fieldset>
                        <legend> Personal Detail : </legend>
                        <div class="col-md-12 form-group">
                            <div class="col-md-6">
                                <label for="first_name" class="text-uppercase">First Name<font color="red">*</font></label>
                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" required>
                                <inpur type="hidden" value="<?=$university_id?>" id="university_id" name="university_id" />
                            </div>

                            <div class="col-md-6">
                                <label for="last_name" class="text-uppercase">Last Name<font color="red">*</font></label>
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" required>
                            </div>
                        </div>



                        <div class="col-md-12 form-group">
                            <div class="col-md-6">
                                <label for="user_mobile" class="text-uppercase">Mobile Number<font color="red">*</font></label>
                                <input type="text" class="form-control" id="user_mobile" name="user_mobile" placeholder="Mobile Number" required>
                            </div>
                            <div class="col-md-6">
                                <label for="user_email" class="text-uppercase">Email<font color="red">*</font></label>
                                <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email Id" required>
                            </div>

                        </div>

                        <div class="col-md-12 form-group">
                            <div class="col-md-6">
                                <label for="first_name" class="text-uppercase"> City <font color="red">*</font></label>
                                <input type="text" class="form-control" id="user_city" name="user_city" placeholder="Mumbai, Pune etc..." required>
                            </div>

                            <div class="col-md-6">
                                <label for="last_name" class="text-uppercase">Imperial Enrolled <font color="red">*</font></label>
                                <Select name="intakeyear" id="user_enrolled" name="user_enrolled" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;">
                                    <option value="">Select Option</option>
                                    <option value="YES">Yes</option>
                                    <option value="NO">No</option>

                                </Select>
                            </div>
                        </div>

                        <div class="col-md-12 form-group">

                        </div>
                    </fieldset>

                    <div class="form-check" style="text-align: center;">
                        <input type="submit"  style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Register"></input>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    function resgistration(){

        var first_name = $('#first_name').val();
        var last_name = $('#last_name').val();
        var user_email = $('#user_email').val();
        var user_mobile = $('#user_mobile').val();
        var user_city = $('#user_city').val();
        var user_enrolled = $('#user_enrolled').val();
        var university_id = <?=$university_id?>;

        var dataString = 'first_name=' + first_name + '&last_name=' + last_name + '&user_email=' + user_email + '&user_mobile=' + user_mobile
                          + '&user_city=' + user_city + '&user_enrolled=' + user_enrolled + '&university_id=' + university_id;

        event.preventDefault();

        $.ajax({
            type: "POST",
            url: "/imperialCounsellingTokbox/event/virtualfair_registration",
            data: dataString,
            cache: false,
            success: function(result){
                console.log(result);
                result = JSON.parse(result);
                if(result['success']){

                    alert("Congratulation!! You have registered successfully. Redirecting to Counselling Room");

                    window.location.href='/imperialCounsellingTokbox/event/rooms/' + result['encodedUsername'];

                    setCookie("imperial_counselling_url", "/imperialCounsellingTokbox/event/rooms/" + result['encodedUsername'],1);

                }
                else{
                    alert(result['message']);
                }
            }
        });
    }


    function setCookie(name, value, days) {
      var expires = "";
      if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
      }
      document.cookie = name + "=" + (value || "") + expires + "; path=/";
    }

    function getCookie(name) {
      var nameEQ = name + "=";
      var ca = document.cookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
      }
      return null;
    }

</script>
<script type="text/javascript">

$(document).ready(function(){

  var counsellingUrl = getCookie("imperial_counselling_url") ? getCookie("imperial_counselling_url") : "NOURL";

  if(counsellingUrl == "NOURL"){

  } else {
    window.location.href = counsellingUrl;
  }
  });

</script>
