<?php if($this->uri->segment('5') && $this->uri->segment('5')=='view'){

$view='disabled'; ?>

<script type="text/javascript">
$( document ).ready(function() {
$("#cleditor").cleditor()[0].disable("true");
//$(".btn .btn-primary .pull-right").attr('disabled' , true);
$('.pull-right').prop('disabled', true);

});

</script>
<?php } else { $view=''; }


 ?>
        
        <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- Statistics Button Container -->
            	
                
                <!-- Panels Start -->
				
				 <?php if($msg = $this->session->flashdata('flash_message')){?> 
				<div class="mws-form-message success"><?php echo $msg;?></div>
				 <?php } ?>
				 
				<?php if(validation_errors()){?> 
                <div class="mws-form-message error"><?php echo validation_errors(); ?></div>
                <?php } ?>

            	<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-magic"></i> Amenities</span>
                    </div>
                    <div class="mws-panel-body no-padding">
					
					  <?php if($this->uri->segment('4')){?>
					   <form class="mws-form wzd-default" action="<?php echo base_url();?>website-element/amenity/edit_amenity/<?php if(isset($id)) echo $id;?>" method="post">
					  <?php }else{?>
                        <form class="mws-form wzd-default" action="<?php echo base_url();?>website-element/amenity/create" method="post">
                      <?php } ?>
                            <fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> General</legend>
                                <div id class="mws-form-row">
                                    <label class="mws-form-label">Name <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" name="amenity_name" <?php echo $view; ?> class="required large" value="<?php if(isset($name)) echo $name; ?>" required>
                                    </div>
                                </div>
                              
                                <div class="mws-form-row">
                                    <label class="mws-form-label">Description</label>
                                    <div class="mws-form-item">
                                        <textarea name="amenity_description" id="cleditor" rows="" cols="" class="required large"><?php if(isset($description)) echo $description; ?></textarea>
                                    </div>
                                </div>
                                 <div class="mws-form-row">
                                    <label class="mws-form-label">Sort Order</label>
                                    <div class="mws-form-item">
                                        <input type="text" name="amenity_sort_order" <?php echo $view; ?> class="required email large" value="<?php if(isset($sort_order)) echo $sort_order; ?>">
                                    </div>
                                </div>
								
								
								 <div class="mws-form-row">
                                    <label class="mws-form-label">Amenity For</label>
                                    <div class="mws-form-item">
										<select name="amenity_for" <?php echo $view; ?> >
										<option value="room" <?php echo $view; ?> <?php if(isset($for) && $for=='room'){ echo 'selected="selected"'; } ?>>Room</option>
										<option value="property" <?php echo $view; ?> <?php if(isset($for) && $for=='property'){ echo 'selected="selected"'; } ?>>Property</option>										   
										</select>
                                    </div>
                                </div>
								
								
								 <div class="mws-form-row">
                                    <label class="mws-form-label">Amenity Type</label>
                                    <div class="mws-form-item">
										<select name="amenity_type" <?php echo $view; ?>>
										<?php foreach($amenities as $amenity) {?>
											
											<?php if(isset($type_id) && $type_id== $amenity->id){?>
											<option value="<?php echo $amenity->id;?>" selected="selected"><?php echo $amenity->type;?></option>
                                           <?php }else {?>
										   <option value="<?php echo $amenity->id;?>"><?php echo $amenity->type;?></option>
										   <?php }
										     }?>									   
										</select>
                                    </div>
                                </div>
								
								
								    
								
								
								
							</fieldset>
                        </form>
                    </div>
                </div>

              

                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->
			
