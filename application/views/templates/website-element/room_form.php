<?php if($this->uri->segment('5') && $this->uri->segment('5')=='view'){

$view='disabled'; ?>

<script type="text/javascript">
$( document ).ready(function() {
$("#cleditor").cleditor()[0].disable("true");
//$(".btn .btn-primary .pull-right").attr('disabled' , true);
$('.pull-right').prop('disabled', true);

});

</script>
<?php } else { $view=''; }


 ?>
        
        <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- Statistics Button Container -->
            	
                
                <!-- Panels Start -->
				
				 <?php if($msg = $this->session->flashdata('flash_message')){?> 
				<div class="mws-form-message success"><?php echo $msg;?></div>
				 <?php } ?>
				 
				<?php if(validation_errors()){?> 
                <div class="mws-form-message error"><?php echo validation_errors(); ?></div>
                <?php } ?>

            	<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-magic"></i> Room</span>
                    </div>
                    <div class="mws-panel-body no-padding">
					
					  <?php if($this->uri->segment('4')){?>
					   <form class="mws-form wzd-default" action="<?php echo base_url();?>website-element/room/edit_room/<?php if(isset($id)) echo $id;?>" method="post">
					  <?php }else{?>
                        <form class="mws-form wzd-default" action="<?php echo base_url();?>website-element/room/create" method="post">
                      <?php } ?>
                            <fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> General</legend>
                                <div id class="mws-form-row">
                                    <label class="mws-form-label">Name <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" name="room_name" <?php echo $view; ?> class="required large" value="<?php if(isset($name)) echo $name; ?>" required>
                                    </div>
                                </div>
                              
                                <div class="mws-form-row">
                                    <label class="mws-form-label">Description</label>
                                    <div class="mws-form-item">
                                        <textarea name="room_description" id="cleditor"  rows="" cols="" class="required large"><?php if(isset($description)) echo $description; ?></textarea>
                                    </div>
                                </div>
                                 <div class="mws-form-row">
                                    <label class="mws-form-label">Sort Order</label>
                                    <div class="mws-form-item">
                                        <input type="text" name="room_sort_order" <?php echo $view; ?> class="required email large" value="<?php if(isset($sort_order)) echo $sort_order; ?>">
                                    </div>
                                </div>
								
								<div class="mws-form-row">
                                    <label class="mws-form-label">Rate<span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" name="room_rate" <?php echo $view; ?> class="required email large" value="<?php if(isset($room_rate)) echo $room_rate; ?>">
                                    </div>
                                </div>
								
								<div class="mws-form-row">
                                    <label class="mws-form-label">Enquery<span class="required">*</span></label>
                                    <div class="mws-form-item">
                                     <span>Enable</span>  
									<input type="radio" name="room_enquery" <?php echo $view; ?> value="1" <?php if(isset($selected_enquery) && $selected_enquery== '1') { echo "checked"; } ?>/>&nbsp;&nbsp;
									<span>Disable</span>
									<input type="radio" name="room_enquery" <?php echo $view; ?> value="0" <?php if(isset($selected_enquery) && $selected_enquery== '0') { echo "checked"; } ?>/>&nbsp;&nbsp;
                                    </div>
                                </div>
								
								<div class="mws-form-row">
                                    <label class="mws-form-label">Book<span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <span>Enable</span>  
									<input type="radio" name="room_booked" value="1" <?php echo $view; ?> <?php if(isset($selected_booked) && $selected_booked== '1') { echo "checked"; } ?>/>&nbsp;&nbsp;
									<span>Disable</span>
									<input type="radio" name="room_booked" value="0" <?php echo $view; ?> <?php if(isset($selected_booked) && $selected_booked== '0') { echo "checked"; } ?>/>&nbsp;&nbsp;
                                    </div>
                                </div>
							</fieldset>
							
							<fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i>Links</legend>
								
								
								
							<div class="mws-form-row">
                                        <label class="mws-form-label">Property</label>
                                        <div class="mws-form-item">
                                        <select name="mult_properties[]" multiple="multiple" size="10" class="large" <?php echo $view; ?>>    
										<?php if($this->uri->segment('4')){ 
										    foreach($properties as $prop){
											?>
										<option value="<?php echo $prop->id;?>" <?php foreach($prop_room as $pr){ if($prop->id==$pr->property_id){ echo 'selected="selected"' ; break; } } ?>><?php echo $prop->name; ?></option>
											<?php }
										     } else{ 
											     foreach($properties as $prop){?>
									    <option value="<?php echo $prop->id;?>"><?php echo $prop->name; ?></option>
											<?php } }?>
											
										</select>	 
                                        </div>
                                    </div>
								
							<div class="mws-form-row">
                                        <label class="mws-form-label">Amenity</label>
                                        <div class="mws-form-item">
                                        <select name="mult_amenities[]" multiple="multiple" size="10" class="large" <?php echo $view; ?>>    
										<?php if($this->uri->segment('4')){ 
										    foreach($amenities as $key){
											?>
										<option value="<?php echo $key->id;?>" <?php foreach($amenty_room as $amnty){ if($key->id==$amnty->amenities_id){ echo 'selected="selected"' ; break; } } ?>><?php echo $key->name; ?></option>
											<?php }
										     } else{ 
											     foreach($amenities as $key){?>
									    <option value="<?php echo $key->id;?>"><?php echo $key->name; ?></option>
											<?php } }?>
											
										</select>	 
                                        </div>
                                    </div>
                             
							  <div class="mws-form-row">
                                    <label class="mws-form-label">Photo Gallery</label>
                                    <div class="mws-form-item">
										<select name="photo_gallery[]" multiple="multiple" size="10" class="large" <?php echo $view; ?>>    
										<?php if($this->uri->segment('4')){ 
										    foreach($photos as $photo){
											?>
										<option value="<?php echo $