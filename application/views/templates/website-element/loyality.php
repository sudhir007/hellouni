
    
        <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- Statistics Button Container -->
            	
                
                <!-- Panels Start -->
                 
            	 <?php if($msg = $this->session->flashdata('flash_message')){?> 
    <div class="mws-form-message success"><?php echo $msg;?></div>
     <?php } ?>
	 
	 
	  <?php if($ermsg = $this->session->flashdata('error_message')){?> 
    <div class="mws-form-message error"><?php echo $ermsg;?></div>
     <?php } ?>
				<div class="mws-panel grid_8">
				<form action="<?php echo base_url();?>website-element/loyality/delete_mult" method="post">
                	<div class="mws-panel-header">
					
                    	<span><i class="icon-table"></i> Loyality
						<a href="<?php echo base_url();?>website-element/loyality/form"><button type="button" class="btn btn-small">Insert</button></a>
						<button type="submit" name="delete-Brand" id="del" class="btn btn-small">Delete</button>
						
						</span>
						
					
						
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-datatable-fn mws-table">
                            <thead>
                                <tr>
                                    <th style="width: 1px;">Select</th>
									<th>Logo</th>
									<th>Name</th>
                                  
                                    <th>Status</th>
                                    <th>Sort Order</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($loyalities as $loyality):?>
								<tr>
                                    <td><input type="checkbox" name="chk[]" class="brand_chk" id="<?php echo $loyality->id;?>" value="<?php echo $loyality->id;?>"/></td>
									
									<td><?php if(isset($loyality->path)||isset($loyality->path)!=''){?>
									<img src="<?php echo $loyality->path;?>" alt="" id="thumb0" width="50px" height="50px">
                                    <?php }else{?>
									<img src="<?php echo base_url();?>images/no_image.jpg" width="100" height="100" alt="" class="elfileimg">
									<?php }?></td>
                                    <td><?php echo $loyality->name;?></td>
                                    
                                    <td><?php echo $loyality->status_name;?></td>
									<td><?php echo $loyality->sort_order;?></td>
									<td class=" ">
                                        <span class="btn-group">
                                            
											 <a class="btn btn-small" href="<?php echo base_url();?>website-element/loyality/form/<?php echo $loyality->id;?>/view"><i class="icon-search"></i></a>
											
											
                                            <a class="btn btn-small" href="<?php echo base_url();?>website-element/loyality/form/<?php echo $loyality->id;?>"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small delete-row" id="<?php echo base_url(); ?>website-element/loyality/delete/<?php echo $loyality->id;?>" href="javascript:void(o);"><i class="icon-trash"></i></a></span>
                                    </td>
                                </tr>
                               <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
					
					</form>
                </div>

                                 

                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->
			
