
        
        <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- Statistics Button Container -->
            	
                 <?php if($msg = $this->session->flashdata('flash_message')){?> 
				<div class="mws-form-message success"><?php echo $msg;?></div>
				 <?php } ?>
				 
				 	<?php if(validation_errors()){?> 
                <div class="mws-form-message error"><?php echo validation_errors(); ?></div>
                <?php } ?>
                <!-- Panels Start -->

            	<div class="mws-panel grid_8">
				<form action="<?php echo base_url();?>website-element/property/delete_mult_properties" method="post">
                	<div class="mws-panel-header">
                    	<span><i class="icon-table"></i> Property
						<a href="<?php echo base_url();?>website-element/property/form"><button type="button" class="btn btn-small">Insert</button></a>
						<button type="submit" class="btn btn-small" id="del" name="delete-Property">Delete</button>
						
						</span>
						
					
						
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-datatable-fn mws-table">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>Property</th>
                                    <th>Logo</th>
                                    <th>Email</th>
                                    <th>Sort Order</th>
									<th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
							
							<?php foreach($properties as $property){ ?>
                                <tr>
                                    <td><input type="checkbox" name="chk[]" value="<?php echo $property->id ;?>" /></td>
                                    <td><?php echo $property->name ;?></td>
                                    <td><?php if(isset($property->path)||isset($property->path)!=''){?>
									<img src="<?php echo $property->path;?>" width="50px" height="50px" alt="" id="thumb0">
									<?php }else{?>
									 <img src="<?php echo base_url();?>images/no_image.jpg" width="100" height="100" alt="" class="elfileimg">
									<?php }?></td>
                                    <td><?php echo $property->email ;?></td>
                                    <td><?php echo $property->sort_order ;?></td>
									<td class=" ">
                                        <span class="btn-group">
                                            <a class="btn btn-small" href="<?php echo base_url();?>website-element/property/form/<?php echo $property->id;?>/view"><i class="icon-search"></i></a>
                                            <a class="btn btn-small" href="<?php echo base_url();?>website-element/property/form/<?php echo $property->id;?>"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small delete-row" id="<?php echo base_url(); ?>website-element/property/delete_property/<?php echo $property->id;?>" href="javascript:void(o);"><i class="icon-trash"></i></a>
                                        </span>
                                    </td>
                                </tr>
							<?php } ?>	
                               
                            </tbody>
                        </table>
                    </div>
					</form>
                </div>

              

                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->
			
