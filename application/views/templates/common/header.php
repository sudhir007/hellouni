<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="description" content="<?php echo $config_meta_desc;?>">
    <meta name="author" content="<?php echo $config_meta_title;?>">

     <title><?php echo $config_title;?></title>

    <!-- Bootstrap Core CSS -->
  	<?php //include 'head.php'; ?>

	  <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>css/grayscale.css" rel="stylesheet">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  <script type="text/javascript" src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>
	<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style.css" />-->
  <!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-8VVWHKYC7V"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-8VVWHKYC7V');
</script>
<style>
.header_icons{
}
.header_icons ul li{
	display:inline-block;
	list-style-type:none;
	margin-bottom:-10px;
}

.my-account{
	position:absolute;
	background-color:#EBEBEB;
	right:22px;
	top:62px;
	border-radius:7px;
	padding:10px;
	z-index:9999;
}
#my_account{
	display:none;
}
.image_round{
	border-radius:40px;
}
.border_line{
	border:1px solid #fff;
	width:129px;


}
.edit_profile{
width:88px;background-color:#F6881F; border-radius:15px; margin-top: 5px;
    margin-bottom: 10px; text-transform:capitalize;;
	margin-left: 10px;
	font-size:12px;
}
.session{
width:88px;background-color:#3C3C3C; border-radius:15px; margin-top:10px; text-transform:capitalize;
margin-left: 10px;
font-size:12px;
color:#ffffff;
}
.profile_img{
margin-top:5px;
}
.nav>li>a{
    padding: 10px 10px;
}
</style>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

<div class="social-slide hidden-xs " >
	<ul>


      <?php
      if(!$this->session->userdata('user')){
          ?>
          <li><a href="<?php echo base_url();?>location/country" id="get-started"><img src="/images/connect-btn.png"></a></li>
          <?php
      }
      else{
          ?>
          <li style="display: none;"><a href="<?php echo base_url();?>location/country" id="get-started"><img src="/images/connect-btn.png" ></a></li>
          <?php
      }
      ?>

		<li><a href="<?php echo base_url();?>additional-services-offers"><img src="/images/offers-btn.png"></a></li>
	</ul>
</div>


<style>
/*******  slide-out nav begin ******/
.social-slide {
  position: fixed;
  top: 200px;
  right:0px;
  z-index:1000;
}
.social-slide ul {
  padding: 0px;
  -webkit-transform: translate(-270px, 0);
  -moz-transform: translate(-270px, 0);
  -ms-transform: translate(-270px, 0);
  -o-transform: translate(-270px, 0);
  transform: translate(270px, 0);
}
.social-slide ul li {
  display: block;
  margin: 8px;
  background-color: #F6881F;
  width: 324px;
  text-align: left;
  padding: 4px;
   -webkit-border-radius: 30px 30px 30px 30px;
  -moz-border-radius: 30px 30px 30px 30px;
  border-radius: 30px 30px 30px 30px;
  -webkit-transition: all 1s;
  -moz-transition: all 1s;
  -ms-transition: all 1s;
  -o-transition: all 1s;
  transition: all 1s;
}
.social-slide ul li:hover {
  -webkit-transform: translate(110px, 0);
  -moz-transform: translate(110px, 0);
  -ms-transform: translate(110px, 0);
  -o-transform: translate(110px, 0);
  transform: translate(-110px, 0);
  background-color: #F6881F;
  cursor:pointer;
}

#my_accountinfo{
    height: 40px;
}


/*@media (min-width: 1200px){
  .container {
      width: 1330px !important;
  }
}*/
/*******  slide-out nav end ******/
</style>


    <!-- Navigation -->
    <?php  //include 'header.php'; ?>


<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    	<!--<div  class="row" style="background:url(<?php echo base_url();?>img/header_trans.png) repeat-x; padding: 2px 0px;">-->
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#">
                   <img src="<?php echo base_url();?>img/logo.png?v=1.1" width="200" />
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse" style="margin-top: 10px;">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <?php
                        if(!$this->session->userdata('user')){
                            ?>
                            <!-- <a class="page-scroll" href="<?php echo base_url();?>">Home</a> -->
                            <?php
                        }
                        else{
                            ?>
                          <!--  <a class="page-scroll" href="<?php echo base_url() . "search/university";?>">Home</a> -->
                            <?php
                        }
                        ?>
                    </li>
                     <li>
                      <!--  <a class="page-scroll" href="<?php echo base_url();?>university">University</a> -->
                    </li>
                     <li>
                      <!--  <a class="page-scroll" href="<?php echo base_url();?>researches">Research Insights</a> -->
                    </li>
                    <li>
                    <!--   <a class="page-scroll" href="<?php echo base_url();?>internships">Internships</a> -->
                   </li>
                    <li>
                      <!-- <a class="page-scroll" href="<?php echo base_url();?>friends-n-alumns">Friends &amp; Alumni</a> -->
                   </li>
                  <!--  <?php
                   if($this->session->userdata('user')){
                       ?>
                       <li>
                        <!--  <a class="page-scroll" href="<?php echo base_url();?>join-a-group">Whatsapp networking</a> -->
                      </li>
                       <?php
                   }
                   ?> -->
                  <li>
                    <!--   <a class="page-scroll" href="<?php echo base_url();?>additional-services-offers">Offers</a> -->
                   </li>

                   <!-- <li>
                      <a class="page-scroll" href="<?php echo base_url();?>guidance">Guidance</a>
                  </li> -->
                  <!-- <li>
                       <a class="btn btn-sm" style="padding: 10px 13px 0px 11px;width: auto;height: 40px;background-color: #F6881F;border: medium none;border-radius: 100px;font-size: 14px;color: #FFF;margin-right: 4px" href="<?php echo base_url();?>location/country">GET STARTED</a>
                   </li> -->
                   <li>
                        <?php
                        if(!$this->session->userdata('user')){
                            ?>
                          <!--  <a class="btn btn-sm" style="padding: 10px 13px 0px 11px;width: auto;height: 40px;background-color: #F6881F;border: medium none;border-radius: 100px;font-size: 14px;color: #FFF;margin-right: 4px" href="<?php echo base_url();?>location/country">GET STARTED</a> -->
                            <?php
                        }
                        else{
                            ?>
                        <!--    <a class="page-scroll" href="" style="display: none;"></a> -->
                            <?php
                        }
                        ?>
                    </li>
                  <li class="logout_padding" id="<?php if($this->session->userdata('user')){ echo 'my_accountinfo';}?>">
                  <?php if(!$this->session->userdata('user')){?>
                <!--  <a class="btn btn-sm" style="padding: 8px 12px 0px 10px;width: auto;height: 40px;background-color: #F6881F;border: medium none;border-radius: 100px;font-size: 14px;color: #FFF;" href="<?php echo base_url();?>user/account/"><img src="<?php echo base_url();?>img/login.png"> <span style="font-size:11px; padding:0; margin:0;color: white;">LOGIN</span> LOGIN</a>-->
                  <?php }else{?>
                <!--  <span style="color: white; margin: 1px 2px 0px 15px;">
            <strong>Hi <?php $usr = explode(" ",$this->session->userdata('user')['name']); echo $usr[0];?></strong></span>
       <a class="btn btn-sm" style="padding: 5px 12px 0px 10px;width: auto;background-color: #F6881F;border: medium none;border-radius: 100px;font-size: 14px;color: #FFF;" href="<?php echo base_url();?>user/account/logout"><img src="<?php echo base_url();?>img/login.png">LOGOUT</a>-->
                  <?php }?>

                  </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

 <!--header end-->
