<!DOCTYPE html>

<html lang="en">

<head>



    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="<?php echo $config_meta_desc;?>">

    <meta name="author" content="<?php echo $config_meta_title;?>">



    <title><?php echo $config_title;?></title>



    <!-- Bootstrap Core CSS -->

    <?php //include 'head.php'; ?>

    <link rel="stylesheet" href="<?php echo base_url();?>css/reset.css">

    <link href="<?php echo base_url();?>tabs/assets/css/responsive-tabs.css" rel="stylesheet">

    <link href="<?php echo base_url();?>tabs/assets/css/responsive-tabs2.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>src/DateTimePicker.css" />

    <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">
      <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
      <link href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css" rel="stylesheet">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


    <!-- Custom CSS -->

    <link href="<?php echo base_url();?>css/grayscale.css" rel="stylesheet">

  <link href="<?php echo base_url();?>jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url();?>css/reveal.css">


  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- Attach necessary scripts -->

    <!-- <script type="text/javascript" src="jquery-1.4.4.min.js"></script> -->

      <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

    <?php if($this->uri->segment('1')=='university'){?>

      <!--<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.min.js"></script>-->



    <?php }else{?>

    <!--<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>-->

    <script type="text/javascript" src="<?php echo base_url();?>js/jquery.reveal.js"></script>

    <?php }?>



    <script type="text/javascript" src="<?php echo base_url();?>js/bootstrap.min.js"></script>

        <!-- Google tag (gtag.js) -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-8VVWHKYC7V"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-8VVWHKYC7V');
        </script>






    <style type="text/css">



      .big-link { display:block; margin-top: 100px; text-align: center; font-size: 70px; color: #06f; }

    </style>



    <style>

     .shade_border{

    background:url(img/shade1.png) repeat-x; height:11px;

  }

    </style>



  <style>

.header_icons{

}

.header_icons ul li{

  display:inline-block;

  list-style-type:none;

  margin-bottom:-10px;



}



.my-account{

  position:absolute;
  background-color:#EBEBEB;
  right:22px;
  top:62px;
  border-radius:7px;
  padding:10px;
  z-index:9999;

}

#my_account{

  display:none;

}

.image_round{

  border-radius:40px;
    margin-left: -15px;
}

.border_line{

  border:1px solid #fff;

  width:100%;
    margin-left: 20px;




}

.edit_profile{

width:88px;background-color:#F6881F; border-radius:15px; margin-top: 5px;

    margin-bottom: 10px; text-transform:capitalize;;

  margin-left: 10px;

  font-size:12px;

}

.session{

width:88px;background-color:#3C3C3C; border-radius:15px; margin-top:10px; text-transform:capitalize;

margin-left: 10px;

font-size:12px;
color:#ffffff;
}

.profile_img{

margin-top:5px;

}

#my_accountinfo{
    height: 40px;
}

.container{
    width: 90%;
}


.icon-button {
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 40px;
  height: 40px;
  color: #f9992f;
  background: #f5f5f5;
  border: none;
  outline: none;
  border-radius: 50%;
}

.icon-button:hover {
  cursor: pointer;
}

.icon-button:active {
  background: #cccccc;
}

.icon-button__badge {
  position: absolute;
  top: -5px;
  right: -5px;
  width: 20px;
  height: 20px;
  background: red;
  color: #ffffff;
  font-weight: 600;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
}

</style>

</head>



<body>


  <ul class="social-slide hidden-xs">
   <?php
      if(!$this->session->userdata('user')){
          ?>
          <li><a href="<?php echo base_url();?>location/country" id="get-started"><img src="/images/connect-btn.png"></a></li>
          <?php
      }
      else{
          ?>
          <li style="display: none;"><a href="<?php echo base_url();?>location/country" id="get-started"><img src="/images/connect-btn.png" ></a></li>
          <?php
      }
      ?>

    <li><a href="<?php echo base_url();?>additional-services-offers"><img src="/images/offers-btn.png"></a></li>
  </ul>


<style>
/*******  slide-out nav begin ******/
.social-slide {
  position: fixed;
  top: 200px;
  right:0px;
  z-index:1000;
}
.social-slide {
  padding: 0px;
  -webkit-transform: translate(-270px, 0);
  -moz-transform: translate(-270px, 0);
  -ms-transform: translate(-270px, 0);
  -o-transform: translate(-270px, 0);
  transform: translate(270px, 0);
}
.social-slide li {
  display: block;
  margin: 8px;
  background-color: #F6881F;
  width: 324px;
  text-align: left;
  padding: 4px;
   -webkit-border-radius: 30px 30px 30px 30px;
  -moz-border-radius: 30px 30px 30px 30px;
  border-radius: 30px 30px 30px 30px;
  -webkit-transition: all 1s;
  -moz-transition: all 1s;
  -ms-transition: all 1s;
  -o-transition: all 1s;
  transition: all 1s;
}
.social-slide li:hover {
  -webkit-transform: translate(110px, 0);
  -moz-transform: translate(110px, 0);
  -ms-transform: translate(110px, 0);
  -o-transform: translate(110px, 0);
  transform: translate(-110px, 0);
  background-color: #F6881F;
  cursor:pointer;
}


/*******  slide-out nav end ******/



</style>

    <!-- Navigation -->

    <?php  //include 'header.php'; ?>

  <style>

.header_icons{

}

.header_icons ul li{

  display:inline-block;

  list-style-type:none;

  margin-bottom:-10px;

}

.logout_padding{

padding-top:3px;

}

.notloginFav{

cursor:pointer;

}
.nav>li>a{
    padding: 10px 10px;
}

/* Important styles */
#toggle {
    display: block;
    width: 45px;
    height: 25px;
    margin: 9px auto 22px;
}

#toggle span:after,
#toggle span:before {
  content: "";
  position: absolute;
  left: 0;
  top: -9px;
}
#toggle span:after{
  top: 9px;
}
#toggle span {
  position: relative;
  display: block;
}

#toggle span,
#toggle span:after,
#toggle span:before {
  width: 100%;
  height: 5px;
  background-color: #fff;
  transition: all 0.3s;
  backface-visibility: hidden;
  border-radius: 2px;
}

/* on activation */
#toggle.on span {
  background-color: transparent;
}
#toggle.on span:before {
  transform: rotate(45deg) translate(5px, 5px);
}
#toggle.on span:after {
  transform: rotate(-45deg) translate(7px, -8px);
}
#toggle.on + #menu {
  opacity: 1;
  visibility: visible;
      z-index: 10;

}

/* menu appearance*/
#menu {
  position: relative;
  color: #fff;
  /*width: 200px;*/
  padding: 10px;
  margin: auto;
  font-family: "Segoe UI", Candara, "Bitstream Vera Sans", "DejaVu Sans", "Bitstream Vera Sans", "Trebuchet MS", Verdana, "Verdana Ref", sans-serif;
  text-align: center;
  border-radius: 4px;
  background: #815cd5;
  box-shadow: 0 1px 8px rgba(0,0,0,0.05);
  /* just for this demo */
  opacity: 0;
  visibility: hidden;
  transition: opacity .4s;
  border: solid 3px #f6872c;
}
#menu:after {
  position: absolute;
  top: -20px;
  left: 85px;
  content: "";
  display: block;
  border-left: 15px solid transparent;
  border-right: 15px solid transparent;
  border-bottom: 20px solid #f6872c;
}
#liColor {
  color: #999;
}
/*ul, li, li a {
  list-style: none;
  display: block;
  margin: 0;
  padding: 0;
}
li a {
  padding: 5px;
  color: #888;
  text-decoration: none;
  transition: all .2s;
}
li a:hover,
li a:focus {
  background: #1ABC9C;
  color: #fff;
}*/

/*@media (min-width: 1200px){
  .container {
      width: 1330px !important;
  }
}*/


.dropbtn {
  padding: 10px;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #ffffff;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {
  background-color: #ddd;
}

.dropdown:hover .dropdown-content {
  display: block;
}


#notification-list {
  width: 300px;
  max-height: 400px;
  overflow-y: scroll;
}

.dropdown-menu > .panel {
  border: none;
  margin: -5px 0;
}

.panel-heading {
  background-color: #f1f1f1;
  border-bottom: 1px solid #dedede;
}

.activity-item i {
  float: left;
  margin-top: 3px;
  font-size: 16px;
}

div.activity {
  margin-left: 28px;
}

div.activity-item {
  padding: 7px 12px;
}

#notification-list div.activity-item {
  border-top: 1px solid #f5f5f5;
}

#notification-list div.activity-item a {
  font-weight: 600;
}

div.activity span {
  display: block;
  color: #999;
  font-size: 11px;
  line-height: 16px;
}

#notifications i.fa {
  font-size: 17px;
}

.noty_type_error * {
  font-weight: normal !important;
}

.noty_type_error a {
  font-weight: bold !important;
}

.noty_bar.noty_type_error a, .noty_bar.noty_type_error i {
  color: #fff
}

.noty_bar.noty_type_information a {
  color: #fff;
  font-weight: bold;
}

.noty_type_error div.activity span
{
  color: #fff
}

.noty_type_information div.activity span
{
  color: #fefefe
}

.no-notification {
  padding: 10px 5px;
  text-align: center;
}








.noty-manager-wrapper {
  position: relative;
  display: inline-block !important;
}

.noty-manager-bubble
{
  position: absolute;
  top: -8px;
  background-color: #fb6b5b;
  color: #fff;
  padding: 2px 5px !important;
  font-size: 9px;
  line-height: 12px;
  text-align: center;
  white-space: nowrap;
  vertical-align: baseline;
  cursor: pointer;
  height: 15px;
  font-weight: bold;

  border-radius: 2px;
  -moz-border-radius: 2px;
  -webkit-border-radius: 2px;
  box-shadow:1px 1px 1px rgba(0,0,0,.1);
  opacity: 0;
}
</style>





<nav class="navbar navbar-custom  top-nav-collapse" role="navigation">

      <!-- <div class="" style="background-color:#3C3C3C; padding: 2px 0px; border-bottom: 1px solid #A4A4A4; ">

          <div class="container" style="background-color:#3C3C3C;position:relative;">

              <div class="pull-left header_icons"><ul style="padding-left:0px;"><li><img src="<?php echo base_url();?>img/1.png"><?php echo $config_email; ?></li>

                 <li><img src="<?php echo base_url();?>img/2.png"> <?php echo $config_phone; ?></li></ul></div>

                <div class="pull-right hidden-xs header_icons"><ul>

                <li><a href="<?php echo $config_facebook; ?>"><img src="<?php echo base_url();?>img/f.png"></a></li>

                <li><a href="<?php echo $config_twitter; ?>"><img src="<?php echo base_url();?>img/t.png"></a></li>

                <li><a href="<?php echo $config_gplus; ?>"><img src="<?php echo base_url();?>img/g.png"></a></li>

                <li><a href="<?php echo $config_linkedin; ?>"><img src="<?php echo base_url();?>img/in.png"></a></li>

                <li class="logout_padding" id="<?php if($this->session->userdata('user')){ echo 'my_accountinfo';}?>">

        <?php if(!$this->session->userdata('user')){?>

        <a href="<?php echo base_url();?>user/account/"><img src="<?php echo base_url();?>img/login.png"><span style="font-size:11px; padding:0; margin:0;">LOGIN</span></a>

        <?php }else{?>

        Hi <?php $usr = explode(" ",$this->session->userdata('user')['name']); echo $usr[0];?>,&nbsp;<a href="<?php echo base_url();?>user/account/logout"><img src="<?php echo base_url();?>img/login.png"><span style="font-size:11px; padding:0; margin:0;">LOGOUT</span></a>

        <?php }?>



        </li>

                <li> <?php if(!$this->session->userdata('user')){?>



        <a href="<?php echo base_url();?>user/account" class="notloginFav" title="Please Login First"><img src="<?php echo base_url();?>img/fav.png"><span style="font-size:11px; padding:0; margin:0;">FAVOURITES</span></a>

        <?php }else{?>



        <a href="<?php echo base_url();?>user/favourites" class="notloginFav"><img src="<?php echo base_url();?>img/fav.png"><span style="font-size:11px; padding:0; margin:0;">FAVOURITES</span></a>

        <?php }?>

        </li>

                </ul></div>-->


          <!-- <div id="my_account"  class="col-md-2 my-account additional">

<div class="col-lg-4 profile_img">

<?php if($this->session->userdata('user') && isset($this->session->userdata('user')['fid']) && $this->session->userdata('user')['fid']){?>

<img src="http://graph.facebook.com/<?php echo $this->session->userdata('user')['fid'];?>/picture?type=normal" width="65" class="image_round" />

<?php }else{ ?>

<img src="<?php echo base_url();?>images/no_image.jpg" width="65" class="image_round" />

<?php } ?>

</div>

<div class="col-lg-7">

<div><a href="<?php echo base_url();?>user/account/update"><button class="btn edit_profile" >Edit Profile</button></a>

</div>

<div class="border_line"></div>

<div><a href="<?php echo base_url();?>search/university"><button class="btn session" >My Searches</button></a></div>

</div>
</div> -->


            <!-- </div>

         </div>  -->

         <?php
         $userData = $this->session->userdata('user');

         if($userData['type'] == 8 || $userData['type'] == 9) { ?>

           <div class="container">

               <div class="navbar-header">

                   <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">

                       <i class="fa fa-bars"></i>

                   </button>

                   <a class="navbar-brand page-scroll" href="#">

                      <img src="<?php echo base_url();?>img/logo.png?v=1.1" width="200">

                   </a>

               </div>



               <!-- Collect the nav links, forms, and other content for toggling -->

               <div class="collapse navbar-collapse navbar-right navbar-main-collapse" style="margin-top: 10px; padding-right:0px;">

                   <ul class="nav navbar-nav">

                       <!-- Hidden li included to remove active class from about link when scrolled up past about section -->

                       <li class="hidden active">

                           <a href="#page-top"></a>

                       </li>


                       <li>

                               <a class="page-scroll" href="<?php echo base_url();?>counsellor/dashboard" >Dashboard</a>

                       </li>

                        <li>
                           <a class="page-scroll" href="<?php echo base_url();?>counsellor/application">Application List</a>
                       </li>

                       <li>
                          <a class="page-scroll" href="<?php echo base_url();?>counsellor/student">Student List</a>
                      </li>

                      <li>
                         <a class="page-scroll" href="<?php echo base_url();?>university">University List</a>
                     </li>

                      <li>
                         <a class="page-scroll" href="<?php echo base_url();?>counsellor/create/application">Create Application</a>
                     </li>

                     <li id="notificationsli">
                       <button id="notifications" href="#" id="drop3" role="button" onclick="notificationUpdate()" class="dropdown-toggle icon-button" data-toggle="dropdown">
                        <span class="material-icons">notifications</span>
                        <span class="icon-button__badge" id="notification_count">0</span>
                      </button>

                     <div id="notification-container" class="dropdown-menu" role="menu" aria-labelledby="drop3">

                        <section class="panel">
                            <header class="panel-heading">
                                <strong>Notifications</strong>
                            </header>
                            <div id="notification-list" class="list-group list-group-alt">

                              <div style="">
                                <div class="noty-manager-list-item noty-manager-list-item-error">
                                  <div class="activity-item"> <i class="fa fa-envelope "></i>
                                    <div class="activity">
                                      AAA Application Status changed<span>2 minutes ago</span>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div style="">
                                <div class="noty-manager-list-item noty-manager-list-item-error">
                                  <div class="activity-item"> <i class="fa fa-envelope "></i>
                                    <div class="activity">
                                      PQR Status changed<span>14 minutes ago</span>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div style="">
                                <div class="noty-manager-list-item noty-manager-list-item-error">
                                  <div class="activity-item"> <i class="fa fa-envelope"></i>
                                    <div class="activity">
                                      AAA Application Status changed<span>20 minutes ago</span>
                                    </div>
                                  </div>
                                </div>
                              </div>


                              <!-- <div style=""><div class="noty-manager-list-item noty-manager-list-item-error"><div class="activity-item"> <i class="fa fa-shopping-cart text-success"></i> <div class="activity"> <a href="#">Eugene</a> ordered 2 copies of <a href="#">OEM license</a> <span>14 minutes ago</span> </div> </div></div></div> -->

                            </div>
                            <footer class="panel-footer">
                                <!-- <a href="#" class="pull-right"><i class="fa fa-cog"></i></a> -->
                                <a href="<?php echo base_url();?>counsellor/notification" data-toggle="class:show animated fadeInRight" style="color: #f9992f">See all the notifications</a>
                            </footer>
                        </section>

                    </div>
                    </li>
                        <!--<li>
                           <a class="page-scroll" href="<?php echo base_url();?>counsellor/team">Team Member</a>
                       </li>-->

                     <li class="logout_padding" id="<?php if($this->session->userdata('user')){ echo 'my_accountinfo';}?>">

                     <?php if(!$this->session->userdata('user')){?>

                     <a class="btn btn-sm" style="padding: 8px 12px 0px 10px;width: auto;height: 40px;background-color: #F6881F;border: medium none;border-radius: 100px;font-size: 14px;color: #FFF;" href="<?php echo base_url();?>user/account/"><img src="<?php echo base_url();?>img/login.png">LOGIN</a>

                     <?php }else{?>
                     <!-- <span style="color: white; margin: 1px 2px 0px 15px;">
                         <strong>Hi <?php $usr = explode(" ",$this->session->userdata('user')['name']); echo $usr[0];?></strong></span>
                    <a class="btn btn-sm" style="padding: 5px 12px 0px 10px;width: auto;background-color: #F6881F;border: medium none;border-radius: 100px;font-size: 14px;color: #FFF;" href="<?php echo base_url();?>user/account/logout"><img src="<?php echo base_url();?>img/login.png">LOGOUT</a> -->
                     <a href="#menu" id="toggle"><span></span></a>

                       <div id="menu">
                         <ul>
                           <li style="margin-bottom: 5px;font-size: 13px;"><span style="color:#f6881f; margin: 1px 2px 0px 15px;">
                         <strong>Hi <?php $usr = explode(" ",$this->session->userdata('user')['name']); echo $usr[0];?></strong></span></li>

                           <?php if($userData['type'] == 8 || $userData['type'] == 9) { ?><li><a href="<?php echo base_url();?>counsellor/team"><b>Team Member</b></a></li><?php } ?>
                           <li><a href="<?php echo base_url();?>counsellor/notification"><b>Notification</b></a></li>
                           <li><a href="<?php echo base_url();?>counsellor/earnings"><b>Earnings</b></a></li>
                           <li><a href="<?php echo base_url();?>counsellor/profile/<?php echo $this->session->userdata('user')['id']; ?>"><b>Edit Profile</b></a></li>
                           <li><a href="<?php echo base_url();?>user/account/logout"><b>Logout</b></a></li>
                         </ul>
                       </div>
                     <?php }?>




                     </li>

                   </ul>

               </div>

               <!-- /.navbar-collapse -->

           </div>

         <?php }
         else if($userData['type'] == 3) { ?>

           <div class="container-fluid">

               <div class="navbar-header">

                   <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">

                       <i class="fa fa-bars"></i>

                   </button>

                   <a class="navbar-brand page-scroll" href="#">

                      <img src="<?php echo base_url();?>img/logo.png?v=1.1" width="200">

                   </a>

               </div>



               <!-- Collect the nav links, forms, and other content for toggling -->

               <div class="collapse navbar-collapse navbar-right navbar-main-collapse" style="margin-top: 10px; padding-right:0px;">

                   <ul class="nav navbar-nav">

                       <!-- Hidden li included to remove active class from about link when scrolled up past about section -->

                       <li class="hidden active">

                           <a href="#page-top"></a>

                       </li>

                       <li>

                               <a class="page-scroll" href="<?php echo base_url();?>university/dashboard" >Dashboard</a>

                       </li>
                       <li class="dropdown">
                            <a class="dropbtn page-scroll">Student Management</a>
                            <div class="dropdown-content">
                              <a href="<?php echo base_url();?>university/student">Students List</a>
                              <a href="<?php echo base_url();?>university/application">Applications List</a>
                              <a href="<?php echo base_url();?>counsellor/add/student">Add Student</a>
                            </div>
                       </li>

                       <li class="dropdown">
                            <a class="dropbtn page-scroll">University Management</a>
                            <div class="dropdown-content">
                              <a href="<?php echo base_url();?>university/campus">Campuses</a>
                              <a href="<?php echo base_url();?>university/college">Colleges</a>
                              <a href="<?php echo base_url();?>university/department">Departments</a>
                              <a href="<?php echo base_url();?>university/course">Course</a>
                              <a href="<?php echo base_url();?>university/research">Research</a>
                              <a href="<?php echo base_url();?>university/brochure">Brochure</a>
                              <a href="<?php echo base_url();?>university/internship">Internship</a>
                            </div>
                       </li>
                       <li class="dropdown">
                           <a class="dropbtn page-scroll">Session Management</a>
                           <div class="dropdown-content">

                                 <a href="<?php echo base_url(); ?>user/session/"> All Sessions</a>
                                 <a href="<?php echo base_url(); ?>user/session/coming"> Coming Sessions</a>
                                 <a href="<?php echo base_url(); ?>user/session/unassigned"> New Slot Request</a>
                                 <a href="<?php echo base_url(); ?>user/session/unconfirmed"> Student Unconfirmed Slot</a>
                                 <a href="<?php echo base_url(); ?>user/session/completed"> Completed Sessions</a>
                                 <a href="<?php echo base_url(); ?>user/session/predefineslots"> PreDefine Slots University</a>
                             </div>
                       </li>

                       <li class="dropdown">
                            <a class="dropbtn page-scroll">My Team</a>
                            <div class="dropdown-content">
                              <a href="<?php echo base_url();?>university/representative">University Delegate</a>
                              <a href="<?php echo base_url();?>university/alumni">Alumni</a>
                              <a href="<?php echo base_url();?>university/counsellor">Hellouni Counsellor List</a>
                            </div>
                       </li>

                     <li class="logout_padding" id="<?php if($this->session->userdata('user')){ echo 'my_accountinfo';}?>">

                     <?php if(!$this->session->userdata('user')){?>

                     <a class="btn btn-sm" style="padding: 8px 12px 0px 10px;width: auto;height: 40px;background-color: #F6881F;border: medium none;border-radius: 100px;font-size: 14px;color: #FFF;" href="<?php echo base_url();?>user/account/"><img src="<?php echo base_url();?>img/login.png">LOGIN</a>

                     <?php }else{?>
                     <!-- <span style="color: white; margin: 1px 2px 0px 15px;">
                         <strong>Hi <?php $usr = explode(" ",$this->session->userdata('user')['name']); echo $usr[0];?></strong></span>
                    <a class="btn btn-sm" style="padding: 5px 12px 0px 10px;width: auto;background-color: #F6881F;border: medium none;border-radius: 100px;font-size: 14px;color: #FFF;" href="<?php echo base_url();?>user/account/logout"><img src="<?php echo base_url();?>img/login.png">LOGOUT</a> -->
                     <a href="#menu" id="toggle"><span></span></a>

                       <div id="menu">
                         <ul>
                           <li style="margin-bottom: 5px;font-size: 13px;"><span style="color:#f6881f; margin: 1px 2px 0px 15px;">
                         <strong>Hi <?php $usr = explode(" ",$this->session->userdata('user')['name']); echo $usr[0];?></strong></span></li>
                           <li><a href="<?php echo base_url();?>counsellor/profile/<?php echo $this->session->userdata('user')['id']; ?>"><b>Edit Profile</b></a></li>
                           <li><a href="<?php echo base_url();?>user/account/logout"><b>Logout</b></a></li>
                         </ul>
                       </div>
                     <?php }?>




                     </li>

                   </ul>

               </div>

               <!-- /.navbar-collapse -->

           </div>

         <?php }
         else { ?>

        <div class="container">

            <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">

                    <i class="fa fa-bars"></i>

                </button>

                <a class="navbar-brand page-scroll" href="/">

                   <img src="<?php echo base_url();?>img/logo.png?v=1.1" width="200">

                </a>

            </div>



            <!-- Collect the nav links, forms, and other content for toggling -->

            <div class="collapse navbar-collapse navbar-right navbar-main-collapse" style="margin-top: 10px; padding-right:0px;">

                <ul class="nav navbar-nav">

                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->

                    <li class="hidden active">

                        <a href="#page-top"></a>

                    </li>

                    <li>
                        <?php
                        if(!$this->session->userdata('user')){
                            ?>
                            <!-- <a class="page-scroll" href="<?php echo base_url();?>" >Home</a> -->
                            <?php
                        }
                        else{
                            ?>
                        <!--  <a class="page-scroll" href="<?php echo base_url() . "university/textsearch?searchtextvalue=usa+university&afl=yes";?>">Home</a> -->
                            <?php
                        }
                        ?>
                    </li>
                    <?php
                    if(!$this->session->userdata('user')){
                        ?>
                     <li>
                        <!-- <a class="page-scroll" href="<?php echo base_url();?>university">University</a> -->
                    </li>
                  <?php } ?>
                     <li>
                      <!--  <a class="page-scroll" href="<?php echo base_url();?>researches">Research Insights</a> -->
                    </li>
                    <li>
                    <!--  <a class="page-scroll" href="<?php echo base_url();?>internships">Internships</a> -->
                   </li>
                    <li>
                      <!-- <a class="page-scroll" href="<?php echo base_url();?>friends-n-alumns">Friends &amp; Alumni</a> -->
                   </li>
                   <?php
                   if($this->session->userdata('user')){
                       ?>
                       <!-- <li>
                          <a class="page-scroll" href="<?php echo base_url();?>join-a-group">Whatsapp networking</a>
                      </li> -->
                       <?php
                   }
                   ?>
                  <li>
                      <!-- <a class="page-scroll" href="<?php echo base_url();?>additional-services-offers">Offers</a> -->
                   </li>
                   <?php
                    if($this->session->userdata('user')){
                        ?>
                        <li>
                          <!-- <a class="page-scroll" href="<?php echo base_url();?>webinar/webinarmaster/webinarlist">Webinar</a> -->
                       </li>
                        <?php
                    }
                    ?>
                   <!-- <li>
                      <a class="page-scroll" href="<?php echo base_url();?>guidance">Guidance</a>
                  </li> -->
                  <li class="logout_padding" id="<?php if($this->session->userdata('user')){ echo 'my_accountinfo';}?>">

                  <?php if(!$this->session->userdata('user')){?>

                  <!-- <a class="btn btn-sm" style="padding: 8px 12px 0px 10px;width: auto;height: 40px;background-color: #F6881F;border: medium none;border-radius: 100px;font-size: 14px;color: #FFF;" href="<?php echo base_url();?>user/account/"><img src="<?php echo base_url();?>img/login.png">LOGIN</a> -->

                  <?php }else{?>
                  <!-- <span style="color: white; margin: 1px 2px 0px 15px;">
                      <strong>Hi <?php $usr = explode(" ",$this->session->userdata('user')['name']); echo $usr[0];?></strong></span>
                 <a class="btn btn-sm" style="padding: 5px 12px 0px 10px;width: auto;background-color: #F6881F;border: medium none;border-radius: 100px;font-size: 14px;color: #FFF;" href="<?php echo base_url();?>user/account/logout"><img src="<?php echo base_url();?>img/login.png">LOGOUT</a> -->
                  <a href="#menu" id="toggle"><span></span></a>

                    <div id="menu">
                      <ul>
                        <li style="margin-bottom: 5px;font-size: 13px;"><span style="color:#f6881f; margin: 1px 2px 0px 15px;">
                      <strong>Hi <?php $usr = explode(" ",$this->session->userdata('user')['name']); echo $usr[0];?></strong></span></li>
                        <li><a href="<?php echo base_url();?>user/account/update"><b>Edit Profile</b></a></li>
                        <li><a href="<?php echo base_url();?>search/university"><b>My Searches</b></a></li>
                        <li><a href="<?php echo base_url();?>user/favourites"><b>My Favourites</b></a></li>
                        <li><a href="<?php echo base_url();?>internships/my"><b>My Internships</b></a></li>
                        <li><a href="<?php echo base_url();?>user/account/logout"><b>Logout</b></a></li>
                      </ul>
                    </div>
                  <?php }?>




                  </li>

                </ul>

            </div>

            <!-- /.navbar-collapse -->

        </div>

      <?php } ?>

        <!-- /.container -->

    </nav>

  <!--document.getElementById('my_account').style.display = 'block';-->
<script type="text/javascript">
  var theToggle = document.getElementById('toggle');

// based on Todd Motto functions
// https://toddmotto.com/labs/reusable-js/

// hasClass
function hasClass(elem, className) {
  return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
}
// addClass
function addClass(elem, className) {
    if (!hasClass(elem, className)) {
      elem.className += ' ' + className;
    }
}
// removeClass
function removeClass(elem, className) {
  var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, ' ') + ' ';
  if (hasClass(elem, className)) {
        while (newClass.indexOf(' ' + className + ' ') >= 0 ) {
            newClass = newClass.replace(' ' + className + ' ', ' ');
        }
        elem.className = newClass.replace(/^\s+|\s+$/g, '');
    }
}
// toggleClass
function toggleClass(elem, className) {
  var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, " " ) + ' ';
    if (hasClass(elem, className)) {
        while (newClass.indexOf(" " + className + " ") >= 0 ) {
            newClass = newClass.replace( " " + className + " " , " " );
        }
        elem.className = newClass.replace(/^\s+|\s+$/g, '');
    } else {
        elem.className += ' ' + className;
    }
}

theToggle.onclick = function() {
   toggleClass(this, 'on');
   return false;
}


$( document ).ready(function() {

    $('#notifications').click(function( event ) {

        console.log( "Thanks for visiting!" );

      $('#notificationsli').toggleClass('open');

    });

});

function notificationInfo(){

  $.ajax({
    type: "GET",
    url: "/counsellor/notification/list",
    cache: false,
    success: function(result) {

      if(result.status == "SUCCESS") {

        document.getElementById("notification_count").innerHTML = result.notification_count;

        var  data = result.notification_list;

        if(data){

          console.log(data);
            document.getElementById("notification-list").innerHTML = '';

            var dataHtml = "";

            data.forEach(function(value, index){

                  dataHtml += "<div style=''>";
                  dataHtml += "<div class='noty-manager-list-item noty-manager-list-item-error'>";
                  dataHtml += "<div class='activity-item'> <i class='fa fa-envelope'></i>";
                  dataHtml += "<div class='activity'>";
                  dataHtml +=  "" + value.description + "";
                  dataHtml +=  "<span>"+value.date_created+"</span>";
                  dataHtml += "</div>";
                  dataHtml += "</div>";
                  dataHtml += "</div>";
                  dataHtml += "</div>";

            })

            document.getElementById("notification-list").innerHTML = dataHtml;
        }

      } else if(result.status == 'NOTVALIDUSER') {
        //alert(result);
        window.location.href = '/user/account';
      } else {
        alert(result);
        //location.reload();
      }
    }
  });

}

notificationInfo();

function notificationUpdate(){

  $.ajax({
    type: "GET",
    url: "/counsellor/notification/seen",
    cache: false,
    success: function(result) {

      console.log(result);

    }
  });

}


</script>
