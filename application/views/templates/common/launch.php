<html lang="en">
<head>
  <title>Launching Hellouni</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

  <style type="text/css">
.footer-bottom{
    bottom: 0;
    position: fixed;
}

.footer1{
    bottom: 4rem;
    position: fixed;
    width: 85%;
}



.imageRow{
    background-image: url("/img/LaunchingSoonBanner01-01.png");
    height: -webkit-fill-available;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: contain;

}

.weRow{
    font-size: 35px;
    font-weight: lighter;
    letter-spacing: 1px;
    font-family: inherit;
    margin-bottom: 5px;
}

.launchRow{
    font-size: 5rem;
    text-transform: uppercase;
    font-weight: 700;
    letter-spacing: 5px;
    margin: 5px 0 16px;
    text-shadow: 3px 3px black;
}

.pBy{
    margin: 16px 0 16px;
    text-transform: initial;
    font-weight: 600;
    letter-spacing: 1.5px;
    font-style: oblique;
}

.borderCount{
    border-right: 3px solid #ef7829;
}
.orange{
  color: #f6872c !important;
}

.wht{
  color: #ffffff !important;
}
.txtAlignCentre{
  text-align: center;
}
</style>
</head>
<body>

<div class="container" style="margin: 8% auto;    width: 95%;">
   <div class="col-md-12 imageRow">
      <div class="col-lg-12 " >
        <div class="col-md-12" style="padding: 10% 0px;">
            <div class="col-md-7">
                <div class="row txtAlignCentre ">
                    <h5 class="text-center wht weRow">WE'RE</h5>
                </div>
                <div class="row txtAlignCentre ">
                    <h1 class="text-center wht launchRow">LAUNCHING SOON</h1>
                </div>
                <div class="row">
                    <div class="col-md-3 txtAlignCentre borderCount">
                        <h1 id="day" class="text-center wht"></h1>
                        <h4 class="orange"><b>Days</b></h4>
                    </div>
                    <div class="col-md-3 txtAlignCentre borderCount">
                        <h1 id="hour" class="text-center wht"></h1>
                        <h4 class="orange">Hours</h4>
                    </div>
                    <div class="col-md-3 txtAlignCentre borderCount">
                        <h1 id="min" class="text-center wht"></h1>
                        <h4 class="orange">Minutes</h4>
                    </div>
                    <div class="col-md-3 txtAlignCentre">
                        <h1 id="sec" class="text-center wht"></h1>
                        <h4 class="orange">Seconds</h4>
                    </div>
                    <h1 id="demo"></h1>
                </div>
                <div class="row txtAlignCentre ">
                    <h4 class="text-center wht pBy">Powered By</h4>
                    <img src="/img/ImperialLogo.png" width="35%" alt="...">
                </div>


            </div>
        </div>
      </div>
   </div>
</div>


<script type="text/javascript">

var countDownDate = new Date("July 27, 2020 20:00:00").getTime();

// Update the count down every 1 second
var x = setInterval(function() {


  var now = new Date().getTime();

  var distance = countDownDate - now;

  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  document.getElementById("day").innerHTML = days ;
  document.getElementById("hour").innerHTML = hours ;
  document.getElementById("min").innerHTML = minutes ;
  document.getElementById("sec").innerHTML = seconds ;

  // If the count down is over
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "LAUNCHING...";
    window.location.reload();
  }
}, 1000);
</script>
</body>
</html>
<?php
exit;
?>
