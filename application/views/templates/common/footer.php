<head>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <!-- <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>-->
   <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
   <!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> -->
   <!-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
   <!-- Plugin JavaScript -->
   <script src="<?php echo base_url();?>js/jquery.easing.min.js"></script>
   <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
   <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>
   <!-- Custom Theme JavaScript -->
   <script src="<?php echo base_url();?>js/grayscale.js"></script>
   <script src="<?php echo base_url();?>js/main.js"></script>
   <!--<script src="<?php echo base_url();?>js/jquery-1.11.0.js" type="text/javascript"></script>-->
   <script src="<?php echo base_url();?>jqvmap/jquery.vmap.js" type="text/javascript"></script>
   <script src="<?php echo base_url();?>jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
   <!--<script src="<?php echo base_url();?>jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>-->
   <script type="text/javascript" src="<?php echo base_url();?>source/jquery.fancybox.js?v=2.1.5"></script>
   <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
   <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
   <style type="text/css">
      ul {
    margin: 0px;
    padding: 0px;
}
.footer-section {
  background: #151414;
  position: relative;
}
.footer-cta {
  border-bottom: 1px solid #373636;
}
.single-cta i {
  color: #ff5e14;
  font-size: 30px;
  float: left;
  margin-top: 8px;
}
.cta-text {
  padding-left: 15px;
  display: inline-block;
}
.cta-text h4 {
  color: #fff;
  font-size: 20px;
  font-weight: 600;
  margin-bottom: 2px;
}
.cta-text span {
  color: #757575;
  font-size: 15px;
}
.footer-content {
  position: relative;
  z-index: 2;
}
.footer-pattern img {
  position: absolute;
  top: 0;
  left: 0;
  height: 330px;
  background-size: cover;
  background-position: 100% 100%;
}
.footer-logo {
  margin-bottom: 30px;
}
.footer-logo img {
    max-width: 200px;
}
.footer-text p {
  margin-bottom: 14px;
  font-size: 14px;
      color: #7e7e7e;
  line-height: 28px;
}
.footer-social-icon span {
  color: #fff;
  display: block;
  font-size: 20px;
  font-weight: 700;
  font-family: 'Poppins', sans-serif;
  margin-bottom: 20px;
}
.footer-social-icon a {
  color: #fff;
  font-size: 16px;
  margin-right: 15px;
}
.footer-social-icon i {
  height: 40px;
  width: 40px;
  text-align: center;
  line-height: 38px;
  border-radius: 50%;
}
.facebook-bg{
  background: #3B5998;
}
.twitter-bg{
  background: #55ACEE;
}
.google-bg{
  background: #DD4B39;
}
.footer-widget-heading h3 {
  color: #fff;
  font-size: 20px;
  font-weight: 600;
  margin-bottom: 40px;
  position: relative;
}
.footer-widget-heading h3::before {
  content: "";
  position: absolute;
  left: 0;
  bottom: -15px;
  height: 2px;
  width: 50px;
  background: #ff5e14;
}
.footer-widget ul li {
  display: inline-block;
  float: left;
  width: 50%;
  margin-bottom: 12px;
}
.footer-widget ul li a:hover{
  color: #ff5e14;
}
.footer-widget ul li a {
  color: #878787;
  text-transform: capitalize;
}
.subscribe-form {
  position: relative;
  overflow: hidden;
}
.subscribe-form input {
  width: 100%;
  padding: 14px 28px;
  background: #2E2E2E;
  border: 1px solid #2E2E2E;
  color: #fff;
}
.subscribe-form button {
    position: absolute;
    right: 0;
    background: #ff5e14;
    padding: 13px 20px;
    border: 1px solid #ff5e14;
    top: 0;
}
.subscribe-form button i {
  color: #fff;
  font-size: 22px;
  transform: rotate(-6deg);
}
.copyright-area{
  background: #202020;
  padding: 25px 0;
}
.copyright-text p {
  margin: 0;
  font-size: 14px;
  color: #878787;
}
.copyright-text p a{
  color: #ff5e14;
}
.footer-menu li {
  display: inline-block;
  margin-left: 20px;
}
.footer-menu li:hover a{
  color: #ff5e14;
}
.footer-menu li a {
  font-size: 14px;
  color: #878787;
}

.pt-5{
   padding-top: 3rem;
    padding-bottom: 3rem;
}

   </style>
</head>

<footer class="footer-section">
        <div class="container">
            <div class="footer-cta pt-5 pb-5">
                <div class="row">
                    <div class="col-xl-4 col-md-4 mb-30">
                        <div class="single-cta" style="display: flex;">
                            <i class="fas fa-map-marker-alt"></i>
                            <div class="cta-text">
                                <h4>Find us</h4>
                                <span>Hello UNI, #211, Vijay Sai Towers, 2nd Floor, Opp. BJP Office Road, Above Reliance Digital, Vivek Nagar, Kukatpally, Hyderabad, Telangana 500072</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-4 mb-30">
                        <div class="single-cta">
                            <i class="fas fa-phone"></i>
                            <div class="cta-text">
                                <h4>Call us</h4>
                                <span>+91 93219 88360</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-4 mb-30">
                        <div class="single-cta">
                            <i class="far fa-envelope-open"></i>
                            <div class="cta-text">
                                <h4>Mail us</h4>
                                <span>susheel.g@hellouni.org</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-content pt-5 pb-5">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 mb-50">
                        <div class="footer-widget">
                            <div class="footer-logo">
                                <a href="index.html"><img src="https://hellouni.org/img/logo.png?v=1.1" class="img-fluid" alt="logo"></a>
                            </div>
                            <div class="footer-text">
                                <p>Hellouni is your ultimate gateway to prestigious universities across the globe! We are here to empower you in every step along the way, to convert your study abroad plans into reality!</p>
                            </div>
                            <div class="footer-social-icon">
                                <span>Follow us</span>
                                <a href="https://www.facebook.com/HelloUni-847774062258005/"><i class="fab fa-facebook-f facebook-bg"></i></a>
                                <a href="https://twitter.com/HelloUni1"><i class="fab fa-twitter twitter-bg"></i></a>
                                <a href="https://hellouni.org/guidance#"><i class="fab fa-google-plus-g google-bg"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30">
                        <div class="footer-widget">
                            <div class="footer-widget-heading">
                                <h3>Useful Links</h3>
                            </div>
                            <ul>
                                <li><a href="https://hellouni.org/">Home</a></li>
                                <li><a href="https://hellouni.org/about">About Us</a></li>
                                <li><a href="https://hellouni.org/university">Universities</a></li>
                                <li><a href="https://hellouni.org/join-a-group">Join Group</a></li>
                                <li><a href="https://hellouni.org/additional-services-offers">Offers</a></li>
                                <li><a href="https://hellouni.org/privacy">Privacy Policy</a></li>
                                <li><a href="https://hellouni.org/guidance">Guidance</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-50">
                        <div class="footer-widget">
                            <div class="footer-widget-heading">
                                <h3>Subscribe</h3>
                            </div>
                            <div class="footer-text mb-25">
                                <p>Don’t miss to subscribe to our new feeds, kindly fill the form below.</p>
                            </div>
                            <div class="subscribe-form">
                                <form action="#">
                                    <input type="text" placeholder="Email Address">
                                    <button><i class="fab fa-telegram-plane"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 text-center text-lg-left">
                        <div class="copyright-text">
                            <p>Copyright &copy; 2018, All Right Reserved <a href="https://hellouni.org/">HELLOUNI</a></p>
                        </div>
                    </div>
                    <!-- <div class="col-xl-6 col-lg-6 d-none d-lg-block text-right">
                        <div class="footer-menu">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Terms</a></li>
                                <li><a href="#">Privacy</a></li>
                                <li><a href="#">Policy</a></li>
                                <li><a href="#">Contact</a></li>
                            </ul>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </footer>
<!-- Bootstrap Core JavaScript -->
<script type="text/javascript">
   jQuery(document).ready(function() {

    // jQuery('#itemslider').carousel({ interval: 3000, cycle: true });

    //      jQuery('.carousel-showmanymoveone .item').each(function(){
    //      var itemToClone = jQuery(this);

    //      for (var i=1;i<6;i++) {
    //      itemToClone = itemToClone.next();

    //      if (!itemToClone.length) {
    //      itemToClone = jQuery(this).siblings(':first');
    //      }

    //      itemToClone.children(':first-child').clone()
    //      .addClass("cloneditem-"+(i))
    //      .appendTo(jQuery(this));
    //      }
    //    });


    jQuery(".fancybox-opened").fancybox({'width':400,

                           'height':300,

                           'autoSize' : false});

      //var countries = 'in,us,id';

      var countries = jQuery('#_countries').val();



      var res = countries.split(",");

      for (i = 0; i < res.length; i++) {

         //alert(res[i]);

      }



      jQuery('#vmap').vectorMap({

          map: 'world_en',

          backgroundColor: '#fff',

          color: '#cccccc',

         hoverColor: '#F6881F',

            selectedColor: '#F6881F',

          hoverOpacity: 0.6,



          enableZoom: true,

         showTooltip: true,



         // values: sample_data,

          scaleColors: ['#cccccc', '#d5d5d5'],

          normalizeFunction: 'polynomial',

         //normalizeFunction: 'linear',

         multiSelectRegion: true,

            onRegionClick: function (event, code, region) {

            // if it's not in the approved list, do nothing, else handle it

            if (countries.toLowerCase().indexOf(code) <= -1) {

            event.preventDefault();

            }

            else {

            //handle state click

               }

            }





         /*onRegionOver: function(event,code, region)

    {

      alert(code);

    }*/

         /*map: 'world_en',



            enableZoom: true,

            showTooltip: true,

            multiSelectRegion: true,

            selectedRegions: ['FR']*/

       /* map: 'world_en',

        backgroundColor: '#fff',

          color: '#ffffff',

           //hoverColor: '#F6881F',

             selectedColor: '#F6881F',

           values: sample_data,

          scaleColors: ['#cccccc', '#d5d5d5'],

            enableZoom: true,

            showTooltip: true,

        values: sample_data,

            multiSelectRegion: true,

            selectedRegions: ['IN']*/



      });

      //jQuery('#vmap').vectorMap('set', 'colors', {us: '#0000ff'});









   jQuery('.stream').click(function(){

      //alert(jQuery(this).attr("id"));

      var id = jQuery(this).attr("id");

      if(jQuery("input[id=chk-"+id+"]").is(":checked"))

            { jQuery("input[id=chk-"+id+"]").prop("checked", false);

              jQuery(this).addClass("grayscale");



        }else{

            jQuery("input[id=chk-"+id+"]").prop("checked", true);

         jQuery(this).removeClass("grayscale");

        }



   });







   jQuery('.stream').click(function(){

   //alert(this.id);

      //$('#myModal').html("AA");

      var val = [];

          /*jQuery(':checkbox:checked').each(function(i){

            val[i] = $(this).val();

          });



      //var dataString = 'email='+email;

      var jsonString = 'email='+JSON.stringify(val);*/

         $.ajax({



         type: "POST",



         /*url: "<?php echo base_url();?>elements/menu/test",*/



         url: "<?php echo base_url();?>course/course/getSubcoursesList",



         //data: jsonString,



         cache: false,



         success: function(result){



          //$('#myModal').html(result);

          // $('#myModal').show("AA");

          //alert(result);





             // $('#newserrormessage').text(result);





         }



            });



   });







   jQuery("#my_accountinfo").hover(function(){

          $("#my_account").show();

       $(".additional").show();



          }, function(){

         //$("#my_account").hide();

      });





   jQuery(".additional").hover(function(){

          $(this).show();



          }, function(){

         $(this).hide();

      });









   });

   /*function page_loader(){

      //alert($('page_loader.php').html())

      alert(optionVal.length);

      //$('#page_loader').load('page_loader.php');

   }

   jQuery('#vmap path').click(function(){

      alert(jQuery(this).attr("id"));

   });*/

   function next_page(e){

      //var chk=jQuery("input[name=check_list]");



      //alert("ABC");

      //e.preventDefault();

      var array = [];

      jQuery('select :selected').each(function(i,value){

          array[i] = jQuery(this).val();

      alert(array[i]);

      });

      //here make your ajax call to a php file

      jQuery.ajax({

          type: "POST",

          url: "<?php echo base_url();?>page_loader.php",

          data: { selected_values: array, otherVal: 'something else' },

      success: function() {

               $('#page_loader').load('page_loader.php?country='+encodeURI(array));

              }

      });

   }

</script>
<script type="text/javascript" src="<?php echo base_url();?>src/DateTimePicker.js"></script>
<script src="<?=base_url()?>/plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
   $(function(){

   $("#dtBox").DateTimePicker(

               {

                  dateFormat: "dd-MMM-yyyy"

               });

    $('#state').change(function(){

         //alert($(this).val());

         $('#city').load('city_loader.php?state='+encodeURI($(this).val()));

      });

   });





   //Degree related



   $(".drg").on('click', function(event) {

      $(".grayscale").removeClass("nograyscale");

      $("#div"+this.id).addClass("nograyscale");

   });







   $("#lead_submit").on('click', function(event) {









   var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;





      if($("#name").val()==''){

         $("#msg").text('Name is required');

         $('#name').focus();

         event.preventDefault();

         return false;



      }else if(isNaN($("#name").val())==false){

         $("#msg").text('Name Should be valid');

         $('#name').focus();

         event.preventDefault();

         return false;



      }else if($("#mobile").val()==''){

         $("#msg").text('Mobile is required');

         $('#mobile').focus();

         event.preventDefault();

         return false;



      }else if(isNaN($("#mobile").val())==true){

         $("#msg").text('Mobile should be valid');

         $('#mobile').focus();

         event.preventDefault();

         return false;



      }else if($("#mobile").val().length!=10){

         $("#msg").text('Mobile should be at least 10 digit');

         $('#mobile').focus();

         event.preventDefault();

         return false;



      }else if($("#email").val()==''){

         $("#msg").text('Email is required');

         $('#email').focus();

         event.preventDefault();

         return false;

      }else if(!emailReg.test($("#email").val())){

         $("#msg").text('Email Should be valid');

         $('#email').focus();

         event.preventDefault();

         return false;

      }else{



            var dataString = 'email='+$('#email').val();

            event.preventDefault();



            $.ajax({



            type: "POST",



            url: "<?php echo base_url();?>user/account/checkemail",



            data: dataString,



            cache: false,



            success: function(result){

             if(result==1){

               $("#msg").text('Email is already exists.');

               $('#email').focus(); return false;



             }else{

               $("#msg").text('');

               $('#input_container').submit(); return true;

             }





            }



           });







      }







   });





   $("#my_accountinfo").mouseover(function(){

            $("#my_account").show();

      $(".additional").show();

   });



   $("#my_account").mouseover(function(){

            $("#my_account").show();

      $(".additional").show();

   });



   $("#my_account").mouseout(function(){

      $("#my_account").hide();

   });



   $("#my_accountinfo").mouseout(function(){

      $("#my_account").hide();

   });

   function showDepartments(collegeId){
       $.ajax({
           type: "GET",
           url: "/admin/university/internship/departments?cid=" + collegeId,
           success: function(result){
               $('#departments_list').html(result);
           }
       });
   }

   function showCourses(departmentId){
       $.ajax({
           type: "GET",
           url: "/admin/university/internship/courses?did=" + departmentId,
           success: function(result){
               $('#courses_list').html(result);
           }
       });
   }

</script>
<script>
   var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
   if (isMobile) {
     //alert(navigator.userAgent);
     //hide footer when input box is on focus
   $(document).on('focus', 'input, textarea, select', function() {
       //$("div.copyright").hide();
   $('div.copyright').css('margin-bottom', '0px' );
      $('div.footer-strip').css('position', 'relative' );
   });

   //show footer when input is NOT on focus
   $(document).on('blur', 'input, textarea, select', function() {
       //$("div.copyright").show();
   $('div.copyright').css('margin-bottom', '60px' );
      $('div.footer-strip').css('position', 'fixed' );
   });

   }

   $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd',  minDate:new Date() });
   $( "#datepicker_1" ).datepicker({ dateFormat: 'yy-mm-dd',  minDate:new Date() });
   $( "#datepicker_2" ).datepicker({ dateFormat: 'yy-mm-dd',  minDate:new Date() });
   $( "#datepicker_3" ).datepicker({ dateFormat: 'yy-mm-dd',  minDate:new Date() });


</script>



</body>
</html>
