<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<style>
   .getStartedBtnDiv{
   position: relative;
   padding: 0rem;
   top: -180px;
   left: 65px;
   }
   .getStartedBtn{
   width: 100%;
   font-size: 3rem;
   font-weight: 400;
   text-transform: capitalize;
   /* height: 6rem; */
   border-radius: 15px;
   background-color: #ffffff;
   color: #f6882c;
   letter-spacing: 1px;
   }
   .getStartedBtnDiv .btn:hover {
   background-color: #f6882c !important;
   }
   .search-sec{
   padding: 2rem;
   }
   .search-slt{
   display: block;
   width: 100%;
   font-size: 2rem;
   line-height: 1.5;
   color: #55595c;
   background-color: #fff;
   background-image: none;
   border: 1px solid #ccc;
   height: calc(5rem + 10px) !important;
   border-radius: 15px;
   }
   .noPadding{
   padding-right: 0px !important;
   padding-left: 0px!important;
   }
   .wrn-btn{
   width: 100%;
   font-size: 2.5rem;
   font-weight: 400;
   text-transform: capitalize;
   height: calc(5rem + 10px) !important;
   border-radius: 15px;
   background-color: #F6881F;
   }
   @media (min-width: 992px){
   .search-sec{
   position: relative;
   /*top: -114px;*/
   /* background: rgba(26, 70, 104, 0.51);*/
   }
   }
   @media (max-width: 992px){
   .search-sec{
   background: #815cd5;
   }
   .search-slt{
   font-size: 1.5rem;
   height: calc(3rem + 10px) !important;
   border-radius: 12px;
   margin: 9px;
   }
   .wrn-btn{
   font-size: 1.5rem;
   height: calc(4rem + 10px) !important;
   border-radius: 15px;
   margin-left: 8px;
   }
   }
   .aboutus-section {
   padding: 90px 0;
   }
   .aboutus-title {
   font-size: 45px;
   letter-spacing: 1.8px;
   /* line-height: 32px; */
   /* margin: 0 0 0px; */
   /* padding: 0 0 11px; */
   position: relative;
   /* text-transform: uppercase; */
   color: #313131;
   }
   /*.aboutus-title::after {
   background: #fdb801 none repeat scroll 0 0;
   bottom: 0;
   content: "";
   height: 2px;
   left: 0;
   position: absolute;
   width: 100%;
   }*/projectFactsWrap
   a:hover, a:active {
   color: #ffb901;
   text-decoration: none;
   outline: 0;
   }
   .feature .feature-box .iconset, .iconsetLast {
   background: #fff none repeat scroll 0 0;
   float: left;
   position: relative;
   width: 18%;
   }
   .feature .feature-box .iconset::after {
   background: #fdb801 none repeat scroll 0 0;
   content: "";
   height: 150%;
   left: 43%;
   position: absolute;
   top: 100%;
   width: 1px;
   }
   .feature .feature-box .iconsetLast::after {
   background: #fdb801 none repeat scroll 0 0;
   content: "";
   height: 0%;
   left: 43%;
   position: absolute;
   top: 100%;
   width: 1px;
   }
   .feature .feature-box .feature-content h4 {
   color: #0f0f0f;
   font-size: 18px;
   letter-spacing: 0;
   line-height: 22px;
   margin: 0 0 5px;
   }
   .feature .feature-box .feature-content {
   float: left;
   padding-left: 28px;
   width: 78%;
   }
   .feature .feature-box .feature-content h4 {
   color: #0f0f0f;
   font-size: 18px;
   letter-spacing: 0;
   line-height: 22px;
   margin: 0 0 5px;
   }
   .feature .feature-box .feature-content p {
   color: #606060;
   font-size: 13px;
   line-height: 22px;
   }
   .icon {
   color : #f4b841;
   padding:0px;
   font-size:40px;
   border: 1px solid #fdb801;
   border-radius: 100px;
   color: #fdb801;
   font-size: 28px;
   height: 70px;
   line-height: 70px;
   text-align: center;
   width: 70px;
   }
   }
   .video-gallery-block { position: relative; width: auto; height: 206px; overflow: hidden; margin-bottom: 30px; }
   .mb10{margin-bottom:10px;}
   .section-title { margin-bottom: 40px; }
   /* youtube lazyload css */
   .ywrapper {
   max-width: 560px; /* 680px; */
   /*margin: 60px auto;
   padding: 0 20px;*/
   margin: 0 auto;
   }
   .youtube {
   background-color: #000;
   margin-bottom: 30px;
   position: relative;
   padding-top: 56.25%;
   overflow: hidden;
   cursor: pointer;
   border-radius: 20px;
   }
   .youtube img {
   width: 100%;
   top: -16.82%;
   left: 0;
   opacity: 1;
   }
   .youtube .play-button {
   width: 68px;
   height: 48px;
   background-color: #333;
   /* box-shadow: 0 0 30px rgba( 0,0,0,0.6 ); */
   z-index: 1;
   opacity: 0.8;
   border-radius: 8px;
   background: inherit;
   }
   .youtube .play-button:before {
   content: "";
   border-style: solid;
   border-width: 10px 0 10px 18.0px;
   border-color: transparent transparent transparent #fff;
   }
   .youtube img,
   .youtube .play-button {
   cursor: pointer;
   }
   .youtube img,
   .youtube iframe,
   .youtube .play-button,
   .youtube .play-button:before {
   position: absolute;
   }
   .youtube .play-button,
   .youtube .play-button:before {
   top: 50%;
   left: 50%;
   transform: translate3d( -50%, -50%, 0 );
   }
   .youtube iframe {
   height: 100%;
   width: 100%;
   top: 0;
   left: 0;
   }
   .youtube img:hover,
   .youtube .play-button:hover {
   background-color: #ff0000;
   opacity: 1;
   }
   text-decoration: none;
   transition: all 0.3s ease-in-out;
   }
   a:hover,
   a:focus,
   a:active {
   text-decoration: none;
   }
   .btn {
   text-transform: uppercase;
   }
   .btn.btn-lg {
   padding: 6px 30px;
   }
   .thumbnail-title {
   font-size: 20px;
   margin-top: 5px;
   }
   .img-thumb-bg {
   padding: 0;
   overflow: hidden;
   min-height: 200px;
   position: relative;
   border-radius: 3px;
   background-position: center;
   background-color: transparent;
   /* background-image: url('https://www.timeshighereducation.com/sites/default/files/styles/the_breaking_news_image_style/public/national-university-of-ireland-galway-aula-maxima-quadrangle.jpg?itok=N7xXLaU8');*/
   }
   .img-thumb-bg p {
   color: #fff;
   margin-bottom: 0;
   line-height: 16px;
   }
   .img-thumb-bg .overlay {
   top: 0;
   left: 0;
   right: 0;
   bottom: 0;
   position: absolute;
   transition: all 0.3s ease-in-out;
   /*background: rgba(0, 0, 0, 0);
   background: -o-linear-gradient(top, rgba(0, 0, 0, 0) 50%, #000000 100%);
   background: -ms-linear-gradient(top, rgba(0, 0, 0, 0) 50%, #000000 100%);
   background: -moz-linear-gradient(top, rgba(0, 0, 0, 0) 50%, #000000 100%);
   background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 50%, #000000 100%);
   background: -webkit-linear-gradient(top, rgba(0, 0, 0, 0) 50%, #000000 100%);
   background: -webkit-gradient(left top, left bottom, color-stop(50%, rgba(0, 0, 0, 0)), color-stop(100%, #000000));
   filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#000000', endColorstr='#000000', GradientType=0);*/
   }
   .img-thumb-bg .caption {
   bottom: -5px;
   height: 100px;
   font-size: 12px;
   position: absolute;
   padding: 0 20px 20px;
   transition: all 0.3s ease-in-out;
   }
   .img-thumb-bg .caption .tag a {
   color: #8703c5;
   padding: 5px 10px;
   font-size: 13px;
   border-radius: 5px;
   display: inline-block;
   text-transform: uppercase;
   background-color: #fff;
   }
   .img-thumb-bg .caption .title {
   margin-top: -55px;
   font-size: 24px;
   line-height: 25px;
   text-transform: uppercase;
   }
   .img-thumb-bg .caption .title a {
   color: #b1b1b0;
   }
   .img-thumb-bg .caption .title a:hover {
   color: #fff;
   }
   /*.img-thumb-bg .caption .meta-data {
   display: none;
   color: #fff;
   font-size: 15px;
   line-height: 14px;
   margin-top: 12px;
   }
   .img-thumb-bg .caption .meta-data a {
   color: #777;
   }
   .img-thumb-bg .caption .meta-data a .fa {
   color: #2980B9;
   }
   .img-thumb-bg .caption .meta-data a:hover {
   color: #2980B9;
   }*/
   .img-thumb-bg .caption .content {
   display: none;
   }
   .img-thumb-bg:hover .overlay {
   /* background: rgba(141, 87, 197, 0.8);*/
   background: linear-gradient(to left, rgba(141, 87, 197, 0.9) , rgba(246, 136, 44,0.9));
   }
   .img-thumb-bg:hover .caption {
   bottom: 60px;
   }
   /*.img-thumb-bg:hover .caption .meta-data {
   display: block;
   }*/
   .img-thumb-bg:hover .caption .content {
   display: block;
   }
   img {
   display: inline-block;
   max-width: 100%;
   transition: all 0.3s ease-out 0s;
   vertical-align: middle;
   border-style: none;
   }
   .heading {
   padding: 0px 15px;
   font-size: 16px;
   color: #6b6b6b;
   font-weight: 500;
   }
   .tp-separator {
   display: -webkit-box;
   display: -webkit-flex;
   display: -ms-flexbox;
   flex-direction: row;
   align-items: center;
   padding: 60px 0px;
   }
   .tp-separator .tp-sep-hldr {
   position: relative;
   -webkit-box-flex: 1;
   -webkit-flex: 1 1 auto;
   -ms-flex: 1 1 auto;
   flex: 1 1 auto;
   min-width: 11%;
   }
   .tp-separator .tp-sep-hldr .tp-sep-lne {
   border-top: 1px solid #eeee;
   display: block;
   position: relative;
   width: 100%;
   }
   /*.team-area{
   padding:100px 0px;
   }*/
   .tp-padding{
   padding:0px 30px;
   }
   .team-style-block{
   display:inline-block;
   }
   .team-style-five .team-items .item {
   -moz-box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
   -webkit-box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
   -o-box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
   box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
   padding-top: 0px;
   }
   .team-style-five .single-item.team-standard .thumb {
   overflow: hidden;
   }
   .team-style-five .team-items .item .thumb img{
   width: 100%;
   height: 28%;
   }
   .team-style-five .team-items .item .thumb {
   position: relative;
   z-index: 1;
   }
   .team-style-five .single-item.team-standard .thumb .social {
   bottom: -65px;
   left: 0;
   padding: 7px 25px;
   position: absolute;
   text-align: center;
   -webkit-transition: all 0.35s ease-in-out;
   -moz-transition: all 0.35s ease-in-out;
   -ms-transition: all 0.35s ease-in-out;
   -o-transition: all 0.35s ease-in-out;
   transition: all 0.35s ease-in-out;
   z-index: 1;
   width: 100%;
   text-align: center;
   }
   .team-style-five .single-item.team-standard .item:hover .thumb .social {
   bottom: 0;
   }
   .team-style-five .single-item.team-standard .thumb .social ul {
   background: #ffffff none repeat scroll 0 0;
   border-radius: 30px;
   display: inline-block;
   padding: 10px 30px;
   }
   .team-style-five .single-item.team-standard .thumb .social ul li {
   display: inline-block;
   margin-top: 0px;
   }
   .team-style-five .single-item.team-standard .thumb .social ul li a {
   color: #1cb9c8;
   display: inline-block;
   font-size: 14px;
   margin: 5px 4px 0;
   }
   .team-style-five .team-items .item .info {
   background: #ffffff none repeat scroll 0 0;
   padding: 20px 20px;
   }
   .team-style-five .team-items .item .info h3 {
   font-weight: 700;
   margin-bottom: 5px;
   font-size: 18px;
   font-family:"montserrat", sans-serif;
   text-transform: uppercase;
   }
   .team-style-five .single-item.team-standard .item .info span {
   /*  display: inline-block;
   font-family: "montserrat",sans-serif;
   margin-bottom: 15px;
   padding-bottom: 10px;
   position: relative;
   text-transform: capitalize;*/
   font-size: 13px;
   color: #1cb9c8;
   }
   .team-style-five .single-item.team-standard .item .info .spanName::after {
   background: #1cb9c8 none repeat scroll 0 0;
   bottom: 0;
   content: "";
   height: 2px;
   left: 50%;
   margin-left: -20px;
   position: absolute;
   width: 40px;
   }
   .team-style-five .team-items .item .info p {
   margin-bottom: 0;
   color: #666666;
   text-align: left;
   font-size: 14px;
   /*line-height: 25px;*/
   }
   .hero-btn {
   background-color:#ff9900;
   color:#004b7a;
   border-radius:25px;
   /* padding:10px 20px;
   font-size:1.2em; */
   font-weight:bold;
   }
   .sectionClass {
   padding: 0px 0px 50px 0px;
   position: relative;
   display: block;
   }
   .sectiontitle {
   background-position: center;
   margin: 30px 0 0px;
   text-align: center;
   min-height: 20px;
   }
   .sectiontitle h2 {
   font-size: 30px;
   color: #222;
   margin-bottom: 0px;
   padding-right: 10px;
   padding-left: 10px;
   }
   .headerLine {
   width: 160px;
   height: 2px;
   display: inline-block;
   background: #101F2E;
   }
   .projectFactsWrap{
   display: -webkit-box;
   /*margin-top: 30px;*/
   flex-direction: row;
   flex-wrap: wrap;
   }
   .sectionClass::after {
   background: #8d57c5 none repeat scroll 0 0;
   bottom: 188px;
   content: "";
   height: 4px;
   left: 25px;
   text-align: center;
   position: absolute;
   width: 90%;
   z-index: 1;
   }
   .projectFactsWrap .item{
   width: 22%;
   height: 100%;
   padding: 50px 0px;
   text-align: center;
   margin: 5px 15px 5px 15px;
   border-radius: 50%;
   z-index: 2;
   position: relative;
   }
   .projectFactsWrap .item:nth-child(1){
   background: rgb(141, 87, 197);
   }
   .projectFactsWrap .item:nth-child(2){
   background: rgb(141, 87, 197);
   }
   .projectFactsWrap .item:nth-child(3){
   background: rgb(141, 87, 197);
   }
   .projectFactsWrap .item:nth-child(4){
   background: rgb(141, 87, 197);
   }
   .projectFactsWrap .item p.number{
   font-size: 43px;
   padding: 0;
   font-weight: bold;
   color: #f6882c;
   }
   .projectFactsWrap .item p{
   color: rgba(255, 255, 255);
   font-size: 22px;
   margin: 0;
   padding: 10px;
   font-family: 'Open Sans';
   }
   .projectFactsWrap .item span{
   width: 60px;
   background: rgba(255, 255, 255, 0.8);
   height: 2px;
   display: block;
   margin: 0 auto;
   }
   .projectFactsWrap .item i{
   vertical-align: middle;
   font-size: 50px;
   color: rgba(255, 255, 255);
   }
   .projectFactsWrap .item:hover i, .projectFactsWrap .item:hover p{
   color: white;
   }
   .projectFactsWrap .item:hover span{
   background: white;
   }
   @media (max-width: 786px){
   .projectFactsWrap .item {
   flex: 0 0 42%;
    width: 0%;
     height: 00%;
     padding: 10px 0px 2px;
    text-align: center;
    margin: 5px 15px 5px 15px;
     border-radius: 0%;
    z-index: 2;
    position: relative;
   }
   .sectionClass::after {
   display: none !important;
   }
   }
   /* AUTHOR LINK */
   .about-me-img {
   width: 120px;
   height: 120px;
   left: 10px;
   /* bottom: 30px; */
   position: relative;
   border-radius: 100px;
   }
   .about-me-img img {
   }
   .authorWindow{
   width: 600px;
   background: #75439a;
   padding: 22px 20px 22px 20px;
   border-radius: 5px;
   overflow: hidden;
   }
   .authorWindowWrapper{
   display: none;
   left: 110px;
   top: 0;
   padding-left: 25px;
   position: absolute;
   }
   .trans{
   opacity: 1;
   -webkit-transform: translateX(0px);
   transform: translateX(0px);
   -webkit-transition: all 500ms ease;
   -moz-transition: all 500ms ease;
   transition: all 500ms ease;
   }
   @media screen and (max-width: 768px) {
   .authorWindow{
   width: 210px;
   }
   .authorWindowWrapper{
   bottom: -170px;
   margin-bottom: 20px;
   }
   }
   /* Extra small devices (phones, 600px and down) */
   @media only screen and (max-width: 600px) {
   .getStartedBtnDiv{
   top: 0px;
   left: 0px;
   }
   .getStartedBtn{
   font-size: 2rem;
   }
   }
   /* Small devices (portrait tablets and large phones, 600px and up) */
   @media only screen and (min-width: 600px) {
   .getStartedBtnDiv{
   top: 0px;
   left: 0px;
   }
   .getStartedBtn{
   font-size: 2rem;
   }
   }
   /* Medium devices (landscape tablets, 768px and up) */
   @media only screen and (min-width: 768px) {
   }
   /* Large devices (laptops/desktops, 992px and up) */
   @media only screen and (min-width: 992px) {
   .getStartedBtnDiv{
   position: relative;
   padding: 0rem;
   top: -155px;
   left: 58px;
   }
   }
   /* Extra large devices (medium laptops and desktops, 1200px and up) */
   @media only screen and (min-width: 1200px) {
   .getStartedBtnDiv{
   position: relative;
   padding: 0rem;
   top: -155px;
   left: 58px;
   }
   }
   /* Extra large devices (medium laptops and desktops, 1400px and up) */
   @media only screen and (min-width: 1600px) {
   .getStartedBtnDiv{
   position: relative;
   padding: 0rem;
   top: -223px;
   left: 85px;
   }
   }
   .card-content {
   background: #ffffff;
   border: 4px;
   box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
   }
   .card-contentEvents {
   background: #ffffff;
   border: 4px;
   box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
   min-height: 375px;
   max-height: 375px;
   /*overflow: auto;*/
   }
   .card-contentFetUnivers {
   background: #ffffff;
   border: 4px;
   box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
   min-height: 400px;
   max-height: 400px;
   /*overflow: auto;*/
   }
   .card-contentFetUnivers .card-img {
   position: relative;
   /*overflow: hidden;*/
   border-radius: 0;
   z-index: 1;
   padding: 20% 15px 5px 15px;
   }
   .card-contentFetUnivers .card-img img {
   width: 90%;
   height: 9%;
   display: block;
   }
   .card-img {
   position: relative;
   /*overflow: hidden;*/
   border-radius: 0;
   z-index: 1;
   }
   .card-img img {
   width: 100%;
   height: auto;
   display: block;
   }
   .card-img span {
   position: absolute;
   z-index: auto;
   top: -15%;
   left: 3%;
   right: 3%;
   background: #f6872c;
   /* padding: 6px; */
   color: #fff;
   font-size: 12px;
   border-radius: 4px;
   /* -webkit-border-radius: 4px; */
   -moz-border-radius: 4px;
   -ms-border-radius: 4px;
   -o-border-radius: 4px;
   /* transform: translate(-50%,-50%);*/
   }
   .card-img span h5{
   font-size: 15px;
   /* margin: 0; */
   padding: 0px 11px;
   color: white;
   /* line-height: 0; */
   }
   .card-desc {
   padding: 1.25rem;
   min-height: 245px;
   }
   .card-descEvent {
   padding: 1.25rem;
   }
   .card-desc h3 {
   color: black;
   font-weight: 600;
   font-size: 1.5em;
   line-height: 1.3em;
   margin-top: 0;
   margin-bottom: 5px;
   padding: 0;
   }
   .card-descEvent h3 {
   color: #815cd5;
   font-weight: 600;
   font-size: 2em;
   line-height: 1.3em;
   margin-top: 0;
   margin-bottom: 5px;
   padding: 0;
   }
   .card-desc p {
   color: #747373;
   font-size: 14px;
   font-weight: 400;
   font-size: 1em;
   line-height: 1.5;
   margin: 0px;
   margin-bottom: 20px;
   padding: 0;
   font-family: 'Raleway', sans-serif;
   }
   .card-descEvent p{
   color: #747373;
   font-size: 14px;
   font-weight: 400;
   font-size: 1em;
   line-height: 1.5;
   margin: 0px;
   margin-bottom: 9px;
   padding: 0;
   font-family: 'Raleway', sans-serif;
   min-height: 80px;
   max-height: 80px;
   overflow: auto;
   }
   .btn-card {
   background-color: #f6882c;
   color: #fff;
   box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
   padding: .84rem 2.14rem;
   font-size: 1.3rem;
   margin: 0;
   border: 0;
   border-radius: .125rem;
   cursor: pointer;
   text-transform: uppercase;
   white-space: normal;
   word-wrap: break-word;
   color: #fff;
   }
   .btn-card:hover {
   background: #815cd5;
   }
   a.btn-card {
   text-decoration: none;
   color: #fff;
   }
   .btn-cardPurple {
   background-color: #8703c5;
   color: #fff;
   box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
   padding: .84rem 2.14rem;
   font-size: 1.3rem;
   margin: 0;
   border: 0;
   border-radius: .125rem;
   cursor: pointer;
   text-transform: uppercase;
   white-space: normal;
   word-wrap: break-word;
   color: #fff;
   }
   .btn-cardPurple:hover {
   background: #f6872c;
   }
   a.btn-cardPurple {
   text-decoration: none;
   color: #fff;
   }
   .btnPosotion{
   position: absolute;
   bottom: 10px;
   }
   /* End card section */
   /*carousel*/
   .col-centered {
   float: none;
   margin: 30px 10px 30px auto;
   }
   .carousel-control {
   width: 8%;
   width: 0px;
   }
   .carousel-control.left,
   .carousel-control.right {
   margin-right: 4px;
   margin-left: -22px;
   background-image: none;
   opacity: 1;
   }
   .carousel-control > a > span {
   color: white;
   font-size: 29px !important;
   }
   /*.carousel-col {
   position: relative;
   min-height: 1px;
   padding: 5px;
   float: left;
   }*/
   .active > div { display:none; }
   .active > div:first-child { display:block; }
   /*xs*/
   @media (max-width: 767px) {
   .carousel-inner .active.left { left: -50%; }
   .carousel-inner .active.right { left: 50%; }
   .carousel-inner .next        { left:  50%; }
   .carousel-inner .prev        { left: -50%; }
   .carousel-col                { width: 50%; }
   .active > div:first-child + div { display:block; }
   }
   /*sm*/
   @media (min-width: 768px) and (max-width: 991px) {
   .carousel-inner .active.left { left: -50%; }
   .carousel-inner .active.right { left: 50%; }
   .carousel-inner .next        { left:  50%; }
   .carousel-inner .prev        { left: -50%; }
   .carousel-col                { width: 50%; }
   .active > div:first-child + div { display:block; }
   }
   /*md*/
   @media (min-width: 992px) and (max-width: 1199px) {
   .carousel-inner .active.left { left: -33%; }
   .carousel-inner .active.right { left: 33%; }
   .carousel-inner .next        { left:  33%; }
   .carousel-inner .prev        { left: -33%; }
   .carousel-col                { width: 33%; }
   .active > div:first-child + div { display:block; }
   .active > div:first-child + div + div { display:block; }
   }
   /*lg*/
   @media (min-width: 1200px) {
   .carousel-inner .active.left { left: -25%; }
   .carousel-inner .active.right{ left:  25%; }
   .carousel-inner .next        { left:  25%; }
   .carousel-inner .prev        { left: -25%; }
   .carousel-col                { width: 25%; }
   .active > div:first-child + div { display:block; }
   .active > div:first-child + div + div { display:block; }
   .active > div:first-child + div + div + div { display:block; }
   }
   .block {
   width: 200px;
   height: 200px;
   padding: 40px 10px 40px 10px;
   }
   .whiteBg {
   background: white;
   box-shadow: 0px 0px 27px 3px #d8d4d4;
   }
   /*.blue {background: blue;}
   .green {background: green;}
   .yellow {background: yellow;}*/
   .testimonial-section1 , .testimonial-section2 {
   width: 100%;
   height: 15%;
   padding: 15px;
   /* -webkit-border-radius: 5px; */
   -moz-border-radius: 5px;
   /* border-radius: 5px; */
   position: relative;
   /* border: 1px solid #fff; */
   }
   .testimonial-section1:after , .testimonial-section2:after {
   top: 100%;
   left: 50px;
   border: solid transparent;
   content: " ";
   position: absolute;
   border-top-color: #fff;
   border-width: 20px;
   margin-left: -15px;
   }
   .testimonial-desc {
   margin-top: 20px;
   text-align:left;
   padding-left: 15px;
   }
   .testimonial-desc img {
   border: 1px solid #f5f5f5;
   border-radius: 150px;
   height: 70px;
   padding: 3px;
   width: 70px;
   display:inline-block;
   vertical-align: top;
   }
   .testimonial-writer{
   display: inline-block;
   vertical-align: top;
   padding-left: 10px;
   }
   .testimonial-writer-name{
   font-weight: bold;
   }
   .testimonial-writer-designation{
   font-size: 85%;
   }
   .testimonial-writer-company{
   font-size: 85%;
   }
   /*---- Outlined Styles ----*/
   .testimonial.testimonial-default{
   }
   .testimonial.testimonial-default .testimonial-section1{
   border-color: #815cd5;
   background-color: #815cd5;
   min-height: 154px;
   }
   .testimonial.testimonial-default .testimonial-section2{
   border-color: #f6882c;
   background-color: #f6882c;
   min-height: 154px;
   }
   .testimonial.testimonial-default .testimonial-section1:after{
   border-top-color: #815cd5;
   }
   .testimonial.testimonial-default .testimonial-section2:after{
   border-top-color: #f6882c;
   }
   .testimonial.testimonial-default .testimonial-desc{
   }
   .testimonial.testimonial-default .testimonial-desc img{
   border-color: #777;
   }
   .testimonial.testimonial-default .testimonial-writer-name{
   color: #777;
   }
   .eventTagsH1{
   color: white !important;
   font-size: 67px !important;
   margin: 9px 0 0 -27px !important;
   }
   .eventTagsHh41{
   color: white !important;
   font-size: 26px !important;
   margin: 3px 0 3px !important;
   float: right;
   }
   .eventTagsHh42{
   color: white !important;
   font-size: 40px !important;
   margin: 13px 0 0 -31px !important;
   }
   .purple{
   color: #8703c5 !important;
   }
   .orange{
   color: #f6872c !important;
   }
   .wht{
   color: #ffffff !important;
   }
   .purpleBg{
   background: #8703c5 !important;
   }
   .orangeBg{
   background: #f6872c !important;;
   }
   .marginBottomP p, .marginBottomO p {
   margin-bottom : 1px !important;
   font-size: 13px;
   color: #545151;
   }
   .marginBottomP hr , .marginBottomO hr {
   margin-top: 6px !important;
   margin-bottom: 6px !important;
   }
   .researchSpan{
   position: absolute;
   z-index: auto;
   top: 7%;
   left: 3%;
   right: 3%;
   }
   /*.transition-timer-carousel .carousel-caption .carousel-caption-header {
   margin-top: 10px;
   font-size: 24px;
   }*/
   @media (min-width: 970px) {
   /* Lower the font size of the carousel caption header so that our caption
   doesn't take up the full image/slide on smaller screens */
   /*.transition-timer-carousel .carousel-caption .carousel-caption-header {
   font-size: 36px;
   }*/
   }
   .transition-timer-carousel .carousel-indicators {
   bottom: 0px;
   margin-bottom: 5px;
   }frndAlum
   .transition-timer-carousel .carousel-control {
   z-index: 11;
   }
   /*.transition-timer-carousel .transition-timer-carousel-progress-bar {
   height: 5px;
   background-color: #5cb85c;
   width: 0%;
   margin: -5px 0px 0px 0px;
   border: none;
   z-index: 11;
   position: relative;
   }*/
   .transition-timer-carousel .transition-timer-carousel-progress-bar.animate{
   /* We make the transition time shorter to avoid the slide transitioning
   before the timer bar is "full" - change the 4.25s here to fit your
   carousel's transition time */
   -webkit-transition: width 4s linear;
   -moz-transition: width 4s linear;
   -o-transition: width 4s linear;
   transition: width 4s linear;
   }
   .main .img-responsive {
   margin-bottom: 30px;
   }
   .transition-frndAlum-carousel .carousel-indicators {
   bottom: -23px;
   margin-bottom: 5px;
   }
   .transition-frndAlum-carousel .carousel-control {
   z-index: 11;
   }
   /*.transition-timer-carousel .transition-timer-carousel-progress-bar {
   height: 5px;
   background-color: #5cb85c;
   width: 0%;
   margin: -5px 0px 0px 0px;
   border: none;
   z-index: 11;
   position: relative;
   }*/
   .transition-frndAlum-carousel .transition-frndAlum-carousel-progress-bar.animate{
   /* We make the transition time shorter to avoid the slide transitioning
   before the timer bar is "full" - change the 4.25s here to fit your
   carousel's transition time */
   -webkit-transition: width 3s linear;
   -moz-transition: width 3s linear;
   -o-transition: width 3s linear;
   transition: width 3s linear;
   }
   .card-contentfrndAlum{
   background: #f6872c;border-radius: 58px;padding: 4px 4px 2px 9px;
   }
   .card-contentfrndAlumP{
   background: #815cd5;border-radius: 58px;padding: 4px 4px 2px 9px;
   }
   .card-imgfrndAlum{
   padding: 35px 20px 10px 20px; text-align: center;
   }
   .card-imgfrndAlumP{
   color:white; padding: 35px 20px 10px 20px; text-align: center;
   }
   .card-imgfrndAlum p , .card-imgfrndAlumP p{
   font-size: 15px;
   font-family: inherit;
   letter-spacing: 0.6px;
   margin: 0px 5px 0px 5px;
   }
   .card-imgfrndAlum span, .card-imgfrndAlumP span{
   left: 32px;
   top: -38px;
   width: 13%;
   border-radius: 50%;
   background: #ffffff00;
   padding: 0px 0px 0px 5px;
   }
   .img-frndAlumn{
   width:108px;
   border-radius: 50%;
   margin-bottom: 5px;
   border: 2px solid #2b2626;
   }
   .getstartedCenter{
   font-size: 3.5rem;
   font-weight: 400;
   text-transform: capitalize;
   height: calc(5rem + 10px) !important;
   border-radius: 26px;
   background: #f6882c;
   border: 1px solid #f6882c;
   box-shadow: 4px 4px 5px #736f6f;
   letter-spacing: 2px;
   }
   .getstartedCenter:hover{
   background: #815cd5;
   border: 1px solid #815cd5;
   }
   @media not all, (-webkit-transform-3d){
   .carousel-inner > .uniitem {
   transition: transform 1s ease-in-out !important;
   }
   }
   .carousel-showmanymoveone .carousel-control {
   width: 4%;
   background-image: none;
   }
   .carousel-showmanymoveone .carousel-control.left {
   margin-left: 15px;
   }
   .carousel-showmanymoveone .carousel-control.right {
   margin-right: 15px;
   }
   .carousel-showmanymoveone .cloneditem-1,
   .carousel-showmanymoveone .cloneditem-2,
   .carousel-showmanymoveone .cloneditem-3 {
   display: none;
   }

   @media only screen and (max-width: 600px) {
   .carousel-showmanymoveone .carousel-inner > .active.left,
   .carousel-showmanymoveone .carousel-inner > .prev {
   left: -100%;
   }
   .carousel-showmanymoveone .carousel-inner > .active.right,
   .carousel-showmanymoveone .carousel-inner > .next {
   left: 100%;
   }
   .carousel-showmanymoveone .carousel-inner > .left,
   .carousel-showmanymoveone .carousel-inner > .prev.right,
   .carousel-showmanymoveone .carousel-inner > .active {
   left: 0;
   }
   .carousel-showmanymoveone .carousel-inner .cloneditem-1 {
   display: none;
   }
   }

   @media all and (min-width: 768px) {
   .carousel-showmanymoveone .carousel-inner > .active.left,
   .carousel-showmanymoveone .carousel-inner > .prev {
   left: -50%;
   }
   .carousel-showmanymoveone .carousel-inner > .active.right,
   .carousel-showmanymoveone .carousel-inner > .next {
   left: 50%;
   }
   .carousel-showmanymoveone .carousel-inner > .left,
   .carousel-showmanymoveone .carousel-inner > .prev.right,
   .carousel-showmanymoveone .carousel-inner > .active {
   left: 0;
   }
   .carousel-showmanymoveone .carousel-inner .cloneditem-1 {
   display: block;
   }
   }
   @media all and (min-width: 768px) and (transform-3d), all and (min-width: 768px) and (-webkit-transform-3d) {
   .carousel-showmanymoveone .carousel-inner > .item.active.right,
   .carousel-showmanymoveone .carousel-inner > .item.next {
   -webkit-transform: translate3d(50%, 0, 0);
   transform: translate3d(50%, 0, 0);
   left: 0;
   }
   .carousel-showmanymoveone .carousel-inner > .item.active.left,
   .carousel-showmanymoveone .carousel-inner > .item.prev {
   -webkit-transform: translate3d(-50%, 0, 0);
   transform: translate3d(-50%, 0, 0);
   left: 0;
   }
   .carousel-showmanymoveone .carousel-inner > .item.left,
   .carousel-showmanymoveone .carousel-inner > .item.prev.right,
   .carousel-showmanymoveone .carousel-inner > .item.active {
   -webkit-transform: translate3d(0, 0, 0);
   transform: translate3d(0, 0, 0);
   left: 0;
   }
   }
   @media all and (min-width: 992px) {
   .carousel-showmanymoveone .carousel-inner > .active.left,
   .carousel-showmanymoveone .carousel-inner > .prev {
   left: -25%;
   }
   .carousel-showmanymoveone .carousel-inner > .active.right,
   .carousel-showmanymoveone .carousel-inner > .next {
   left: 25%;
   }
   .carousel-showmanymoveone .carousel-inner > .left,
   .carousel-showmanymoveone .carousel-inner > .prev.right,
   .carousel-showmanymoveone .carousel-inner > .active {
   left: 0;
   }
   .carousel-showmanymoveone .carousel-inner .cloneditem-2,
   .carousel-showmanymoveone .carousel-inner .cloneditem-3 {
   display: block;
   }
   }
   @media all and (min-width: 992px) and (transform-3d), all and (min-width: 992px) and (-webkit-transform-3d) {
   .carousel-showmanymoveone .carousel-inner > .item.active.right,
   .carousel-showmanymoveone .carousel-inner > .item.next {
   -webkit-transform: translate3d(25%, 0, 0);
   transform: translate3d(25%, 0, 0);
   left: 0;
   }
   .carousel-showmanymoveone .carousel-inner > .item.active.left,
   .carousel-showmanymoveone .carousel-inner > .item.prev {
   -webkit-transform: translate3d(-25%, 0, 0);
   transform: translate3d(-25%, 0, 0);
   left: 0;
   }
   .carousel-showmanymoveone .carousel-inner > .item.left,
   .carousel-showmanymoveone .carousel-inner > .item.prev.right,
   .carousel-showmanymoveone .carousel-inner > .item.active {
   -webkit-transform: translate3d(0, 0, 0);
   transform: translate3d(0, 0, 0);
   left: 0;
   }
   }
   .boxLogo{
   margin: 8px 40px 25px 40px;
   box-shadow: 0px 0px 26px 8px #ecebeb;
   padding: 50px 10px 40px 10px;
   width: 200px;
   height: 200px;
   }


   /*.multi-item-carousel .carousel .item {
      width: 400px;
    }
    .multi-item-carousel .carousel .item .item-content {
      width: 200px;
    }*/

   .multi-item-carousel .carousel-inner > .item {
      transition: 800ms ease-in-out left;
    }
    .multi-item-carousel .carousel-inner .active.left {
      left: -50%;
    }
    .multi-item-carousel .carousel-inner .active.right {
      left: 50%;
    }
    .multi-item-carousel .carousel-inner .next {
      left: 50%;
    }
    .multi-item-carousel .carousel-inner .prev {
      left: -50%;
    }

    @media all and (transform-3d), (-webkit-transform-3d) {
      .multi-item-carousel .carousel-inner > .item {
        left: 0;
        transition: 800ms ease-in-out left;
        transition: 400ms  cubic-bezier(0, 0, 0.2, 1);
        backface-visibility: hidden;
        /*transform: none!important;*/
      }
    }
    .multi-item-carousel .carouse-control.left,
    .multi-item-carousel .carouse-control.right {
      background-image: none;
    }

    @media (min-width: 1200px){
    .active > div:first-child + div + div {
    display: none;
    }
    }
    .eventDateBox{
          height: 160px;
          padding: 15px 10px;
          text-align: center;
    }

    .eventDet{
              width: 49%;
          margin-right: 10px;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    }
/*body {
  background: #333;
  color: #ddd;
}
h1 {
  color: white;
  font-size: 2.25em;
  text-align: center;
  margin-top: 1em;
  margin-bottom: 2em;
  text-shadow: 0px 2px 0px #000000;
}*/

.modal-body h1 {
  font-weight: 900;
  font-size: 2.3em;
  text-transform: uppercase;
}
.modal-body a.pre-order-btn {
  color: #000;
  background-color: gold;
  border-radius: 1em;
  padding: 1em;
  display: block;
  margin: 1.5em auto;
  width: 50%;
  font-size: 1.25em;
  font-weight: 6600;
}
.modal-body a.pre-order-btn:hover {
  background-color: #3c1e4c;
  text-decoration: none;
  color: gold;
}
</style>
<!-- <section class="intro" style="margin-top: 66px;">
   <div class="video-container">

      <iframe width="100%" height="315" src="https://www.youtube.com/embed/8wMs6gDg1-0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

   </div>

   </section>
   <div style="text-align: center;">
   <a class="btn btn-lg" style="background-color: #F6881F;border-radius: 100px;margin: 20px auto 0px;
   display: inline-block;" href="<?php echo base_url();?>location/country">Get Started</a></div>
   <div class="container"><div class="col-lg-12"><img src="img/home1.jpg" width="100%"/></div></div> -->
  <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog" style="z-index: 10000;">

        <div class="modal-content">

          <div class="modal-body text-center">


            <img src="<?php echo base_url();?>application/images/VEF2020.jpg" width="100%" alt="...">
            <a class="pre-order-btn" href="<?php echo base_url();?>university/virtualfair/"><b>Register Now</b></a>
          </div>

        </div>

      </div>
   </div>


<section id="topSection">
   <div class="container-fluid">
      <div class="row">
         <img src="<?php echo base_url();?>application/images/TopBannerFinal.jpg" width="100%" alt="...">
         <!-- <div class="col-md-3 getStartedBtnDiv">
            <a href="<?php echo base_url();?>location/country" class="btn btn-lg getStartedBtn"><b>Get Started</b></a>
            </div> -->
         <div class="col-md-8 getStartedBtnDiv">
            <form autocomplete="off" action="<?php echo base_url();?>university/textsearch" method="get">
               <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 noPadding hidden-xs visible-md visible-lg">
                           <input type="text" name="searchtextvalue" id="searchtextvalue" class="form-control search-slt typeahead" data-provide="typeahead" placeholder="Which country? Course?" style="border-radius: 15px 0px 0px 15px !important;">
                        </div>
                        <!--<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 visible-xs hidden-md hidden-lg">
                           <input type="text" name="searchtextvalue" id="searchtextvalue" class="form-control search-slt typeahead" data-provide="typeahead" placeholder="Which country? Course?" style="border-radius: 0px !important;">
                        </div>
                         <div class="col-lg-3 col-md-3 col-sm-12 p-0 noPadding">
                           <input type="text" class="form-control search-slt" placeholder="Where?(Country,University) " style="border-radius: 0px 0px 0px 0px !important;">
                           </div> -->
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 noPadding hidden-xs visible-md visible-lg">
                           <input type="submit" class="btn wrn-btn" style="background-color: #F6881F; border-radius: 0px 15px 15px 0px !important; " value="Search" ></input>
                        </div>
                        
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</section>
<script type="text/javascript">
  jQuery(document).ready(function($) {
    var alterClass = function() {
      var ww = document.body.clientWidth;
      if (ww < 400) {
        $('.test').removeClass('blue');
      } else if (ww >= 401) {
        $('.test').addClass('blue');
      };
    };
    $(window).resize(function(){
      alterClass();
    });
    //Fire it when the page first loads:
    alterClass();
  });
</script>
<!-- <section class="search-sec">
   <div class="container">
       <form action="#" method="post" novalidate="novalidate">
           <div class="row">
               <div class="col-lg-12">
                   <div class="row">
                       <div class="col-lg-3 col-md-3 col-sm-12 p-0">
                           <input type="text" class="form-control search-slt" placeholder="University">
                       </div>
                       <div class="col-lg-3 col-md-3 col-sm-12 p-0">
                           <input type="text" class="form-control search-slt" placeholder="Course">
                       </div>
                       <div class="col-lg-3 col-md-3 col-sm-12 p-0">
                           <select class="form-control search-slt" id="exampleFormControlSelect1">
                               <option>Select Type</option>
                               <option>Example one</option>
                               <option>Example one</option>
                               <option>Example one</option>
                               <option>Example one</option>
                               <option>Example one</option>
                               <option>Example one</option>
                           </select>
                       </div>
                       <div class="col-lg-3 col-md-3 col-sm-12 p-0">
                           <button type="button" class="btn wrn-btn" style="background-color: #F6881F">Search</button>
                       </div>
                   </div>
               </div>
           </div>
       </form>
   </div>
   </section> -->
<section>
   <div class="container">
      <?php //var_dump($homepage_counter[0]);
         $universityCount = 0;
         $studentCount = 0 ;
         $courseCount = 0 ;
         $countryCount = 0 ;

         foreach ($homepage_counter as $key => $value) {

           if($key == 'university_count'){
             $universityCount = $value;
           }
           if($key == 'course_count'){
             $courseCount = $value;
           }
           if($key == 'country_count'){
             $countryCount = $value;
           }
           if($key == 'user_count'){
             $studentCount = $value;
           }

         }
         ?>
      <div class="row">
         <div id="projectFacts" class="sectionClass">
            <div class="projectFactsWrap ">
               <div class="item wow fadeInUpBig animated animated" data-number="<?php echo $universityCount;?>" style="visibility: visible;">
                  <i class="fa fa-university"></i>
                  <p id="number1" class="number"><?php echo $universityCount;?></p>
                  <span></span>
                  <p><b>UNIVERSITIES</b></p>
               </div>
               <div class="item wow fadeInUpBig animated animated" data-number="<?php echo $courseCount;?>" style="visibility: visible;">
                  <i class="fa fa-book"></i>
                  <p id="number2" class="number"><?php echo $courseCount;?></p>
                  <span></span>
                  <p><b>COURSES</b></p>
               </div>
               <div class="item wow fadeInUpBig animated animated" data-number="<?php echo $countryCount;?>" style="visibility: visible;">
                  <i class="fa fa-globe"></i>
                  <p id="number3" class="number"><?php echo $countryCount;?></p>
                  <span></span>
                  <p><b>COUNTRIES</b></p>
               </div>
               <div class="item wow fadeInUpBig animated animated" data-number="<?php echo $studentCount;?>" style="visibility: visible;">
                  <i class="fa fa-users"></i>
                  <p id="number4" class="number"><?php echo $studentCount;?></p>
                  <span></span>
                  <p><b>HAPPY STUDENTS</b></p>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section>
   <div class="container">
      <div class="row">
         <div class="aboutus" style="text-align: center;">
            <h1 class="aboutus-title">How it works</h1>
            <p style="color: #4a4949;    font-size: 25px;">We help you <font color="#f9992f"><b>Choose</b></font>, <font color="#f9992f"><b>Connect</b></font> & <font color="#f9992f"><b>Apply</b></font> to desired University.
            </p>
         </div>
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10px;">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: center;">
               <div class="row">
                  <img src="<?php echo base_url();?>application/images/Choose.png" width="75%" alt="...">
               </div>
               <div class="row" style="margin: 0px 12px 0px 12px;">
                  <h3 style="color: #8703c5;">CHOOSE</h3>
                  <p style="color: #FF8700; font-size: 15px;letter-spacing: 0.8px;"><b>Choose the University that best fits your interests, profile and aspirations!</b></p>
               </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: center;">
               <div class="row">
                  <img src="<?php echo base_url();?>application/images/Connect.png" width="75%" alt="...">
               </div>
               <div class="row" style="margin: 0px 12px 0px 12px;">
                  <h3 style="color: #8703c5;">CONNECT</h3>
                  <p style="color: #FF8700; font-size: 15px;letter-spacing: 0.8px;"><b>Connect directly with the University / Alumni to receive first-hand guidance!</b></p>
               </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: center;">
               <div class="row">
                  <img src="<?php echo base_url();?>application/images/Apply.png" width="75%" alt="...">
               </div>
               <div class="row" style="margin: 0px 12px 0px 12px;">
                  <h3 style="color: #8703c5;">APPLY</h3>
                  <p style="color: #FF8700; font-size: 15px;letter-spacing: 0.8px;"><b>Apply to your chosen University assisted by dedicated HelloUni Counselors.</b></p>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section>
   <div class="container">
      <div class="row" style="text-align: center;">
         <a href="<?php echo base_url();?>location/country" class="btn btn-lg getstartedCenter"><b>Get Started</b></a>
      </div>
   </div>
</section>
<section>
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-lg-12 col-sm-6 col-12" style="margin-top: 30px;margin-bottom: 20px;">
            <div class="video-gallery-block">
               <div class="item-video">
                  <div class="youtube" data-embed="8wMs6gDg1-0">
                     <div class="play-button"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section style="margin-bottom: 30px;">

   <div class="container">
      <div class="row">
         <div class="aboutus" style="text-align: center;">
            <h1 class="aboutus-title">UPCOMING EVENTS</h1>
            <p style="color: #4a4949; font-size: 25px;">Highly informative & interactive sessions in association with Universities across the globe.
            </p>
         </div>
         <div class="col-md-12">
           <div class="carousel slide multi-item-carousel" id="theCarousel">
                <div class="carousel-inner">
                  <?php
                     $countevent = 1;

                     foreach ($upcoming_events as $ekey => $evalue) {
                       $activeE = "";
                       if($countevent == 2 || $countevent == 4)
                       {
                         $secss = "orangeBg";
                         $cssb = "orange";
                       } else {
                         $secss = "purpleBg";
                         $cssb = "purple";
                       }
                       if($countevent == 1){
                         $activeE = "active";
                       }
                       $countevent = $countevent +1;
                       // code...
                       $newMonth = date('F', strtotime($evalue[0]['webinar_date'])); //$evalue['webinar_date'];
                       $newDate = date('d', strtotime($evalue[0]['webinar_date'])); //$evalue['webinar_date'];
                       $newDay = date('D', strtotime($evalue[0]['webinar_date'])); //$evalue['webinar_date'];
                      ?>

                  <div class="item <?php echo $activeE; ?>">
                    <div class="col-md-6 col-xs-4 noPaddingCol eventDet">
                      <div class="col-md-3 <?php echo $secss; ?> eventDateBox">
                        <h6 class="wht"><?php echo $newDay; ?> </h6>
                        <h3 class="wht"><?php echo $newDate; ?> <?php echo $newMonth; ?></h3>
                        <h5 class="wht"><?php echo $evalue[0]['webinar_time']; ?></h5>
                      </div>
                      <div class="col-md-9">
                        <h3>
                          <a href="<?php echo base_url()."webinar/registration/registrationForm/".$evalue[0]['id']; ?>" class="<?php echo $cssb; ?>"> <?php echo $evalue[0]['webinar_name']; ?> </a>
                          </h3>
                        <h5><?php echo $evalue[0]['webinar_topics']; ?></h5>
                        <h5><?php echo $evalue[0]['webinar_host_name']; ?></h5>
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                  <!--  Example item end -->
                </div>
                <a class="left carousel-control" href="#theCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                <a class="right carousel-control" href="#theCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
              </div>
         </div>
         </div>

      </div>
   </div>
</section>
<section style="margin-bottom: 30px;">
   <div class="container">
      <div class="row">
         <div class="aboutus" style="text-align: center; margin-bottom: 5rem;">
            <h1 class="aboutus-title">Featured Universities & Courses</h1>
            <!-- <p style="color: #4a4949;    font-size: 25px;">Popular & Trending Courses offered by Universities
               </p> -->
         </div>
         <?php
            $cssHead = "purpleBg";
            $count = 1;
            foreach ($featured_university as $keyfu => $valuefu) {
              if($count == 2 || $count == 4){
                $cssHead = "purpleBg";
                $csst = "purple";
                $cssb = "btn-cardPurple";
              } else {
                $cssHead = "";
                $csst = "orange";
                $cssb = "btn-card";
              }
              // code...
            ?>
         <div class="col-md-3">
            <div class="card-contentFetUnivers">
               <div class="card-img">
                  <img src="<?php echo base_url();?><?php echo $valuefu[0]['university_logo']; ?>" alt="">
                  <span class="<?php echo $cssHead; ?>">
                     <h5><?php echo $valuefu[0]['university_name']; ?></h5>
                  </span>
               </div>
               <div class="card-desc marginBottomP">
                  <h3 class="<?php echo $csst; ?>">Top Courses</h3>
                  <?php
                     foreach ($valuefu as $keycu => $valuecu) {
                       // code...
                     ?>
                  <p><?php echo $valuecu['course_name']; ?></p>
                  <p><i class="fas fa-user-graduate <?php echo $csst; ?>"></i> 400  &nbsp;&nbsp; <i class="fa fa-graduation-cap <?php echo $csst; ?>"></i> <?php echo $valuecu['degree_name']; ?></p>
                  <hr>
                  <?php  } ?>
                  <br>
                  <a href="<?php echo base_url().$valuefu[0]['university_slug']; ?>" class="<?php echo $cssb; ?>">More Info</a>&nbsp;&nbsp;&nbsp;&nbsp; <a href="<?php echo base_url().$valuefu[0]['university_slug']; ?>#courses" class="<?php echo $cssb; ?>">Courses</a>
               </div>
            </div>
         </div>
         <?php
            $count = $count + 1;
            }
            ?>
         <!--<div class="col-md-3">
            <div class="card-content">
                <div class="card-img">
                    <img src="<?php echo base_url();?>application/images/4.png" alt="">
                    <span class="purpleBg"><h5>James Cook University</h5></span>
                </div>
                <div class="card-desc marginBottomO">
                    <h3 class="orange">Top Courses</h3>
                    <p>Master Of Applied Linguistics</p>
                    <p><i class="fas fa-user-graduate purple"></i> 400  &nbsp;&nbsp; <i class="fa fa-graduation-cap purple"></i> Degree</p>
                    <hr>
                    <p>Master Of Applied Linguistics</p>
                    <p><i class="fas fa-user-graduate purple"></i> 400  &nbsp;&nbsp; <i class="fa fa-graduation-cap purple"></i> Degree</p>
                    <hr>
                    <p>Master Of Applied Linguistics</p>
                    <p><i class="fas fa-user-graduate purple"></i> 400  &nbsp;&nbsp; <i class="fa fa-graduation-cap purple"></i> Degree</p>
                    <br>
                    <a href="#" class="btn-cardPurple ">More Info</a>&nbsp;&nbsp;&nbsp;&nbsp; <a href="#" class="btn-cardPurple ">Courses</a>
                </div>
            </div>
            </div>
            <div class="col-md-3">
            <div class="card-content">
                <div class="card-img">
                    <img src="<?php echo base_url();?>application/images/4.png" alt="">
                    <span><h5>James Cook University</h5></span>
                </div>
                <div class="card-desc marginBottomP">
                    <h3 class="purple">Top Courses</h3>
                    <p>Master Of Applied Linguistics</p>
                    <p><i class="fas fa-user-graduate purple"></i> 400  &nbsp;&nbsp; <i class="fa fa-graduation-cap purple"></i> Degree</p>
                    <hr>
                    <p>Master Of Applied Linguistics</p>
                    <p><i class="fas fa-user-graduate purple"></i> 400  &nbsp;&nbsp; <i class="fa fa-graduation-cap purple"></i> Degree</p>
                    <hr>
                    <p>Master Of Applied Linguistics</p>
                    <p><i class="fas fa-user-graduate purple"></i> 400  &nbsp;&nbsp; <i class="fa fa-graduation-cap purple"></i> Degree</p>
                    <br>
                    <a href="#" class="btn-card">More Info</a>&nbsp;&nbsp;&nbsp;&nbsp; <a href="#" class="btn-card">Courses</a>
                </div>
            </div>
            </div>
            <div class="col-md-3">
            <div class="card-content">
                <div class="card-img">
                    <img src="<?php echo base_url();?>application/images/4.png" alt="">
                    <span class="purpleBg"><h5>James Cook University</h5></span>
                </div>
                <div class="card-desc marginBottomO">
                    <h3 class="orange">Top Courses</h3>
                    <p>Master Of Applied Linguistics</p>
                    <p><i class="fas fa-user-graduate purple"></i> 400  &nbsp;&nbsp; <i class="fa fa-graduation-cap purple"></i> Degree</p>
                    <hr>
                    <p>Master Of Applied Linguistics</p>
                    <p><i class="fas fa-user-graduate purple"></i> 400  &nbsp;&nbsp; <i class="fa fa-graduation-cap purple"></i> Degree</p>
                    <hr>
                    <p>Master Of Applied Linguistics</p>
                    <p><i class="fas fa-user-graduate purple"></i> 400  &nbsp;&nbsp; <i class="fa fa-graduation-cap purple"></i> Degree</p>
                    <br>
                    <a href="#" class="btn-cardPurple ">More Info</a>&nbsp;&nbsp;&nbsp;&nbsp; <a href="#" class="btn-cardPurple ">Courses</a>
                </div>
            </div>
            </div>-->
      </div>
   </div>
</section>
<section style="margin-bottom: 30px;">
   <div class="container">
      <div class="row">
         <div class="aboutus" style="text-align: center;">
            <h1 class="aboutus-title">Research Insight</h1>
            <p style="color: #4a4949;    font-size: 25px;">Explore desired areas of Research & Connect with concerned Faculty.
            </p>
         </div>
         <div class="team-style-five">
            <div class="container">
               <div class="row" style="margin-bottom: 23px;">
                  <?php
                     $countf = 0;
                     foreach ($research as $rkey => $rvalue) {

                       if($countf == 3 || $countf == 4 || $countf == 5)
                       {
                         continue;
                       }
                       $countf = $countf +1 ;
                       // code...
                     ?>
                  <div class="col-md-4 col-sm-12 team-items">
                     <div class="single-item text-center team-standard team-style-block">
                        <div class="item">
                           <div class="thumb">
                              <img src="<?php echo base_url();?>application/images/Research1.png" width="100%" alt="...">
                              <span class="researchSpan">
                                 <h2 style="color: white;"><?php echo $rvalue[0]['topic']; ?></h2>
                                 <h4 style="color: white;"><?php echo $rvalue[0]['university_name']; ?></h4>
                              </span>
                              <div class="social">
                                 <ul>
                                    <li><a href="/researches"><b>Read More</b></a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php  } ?>
                  <!--<div class="col-md-4 col-sm-12 team-items">
                     <div class="single-item text-center team-standard team-style-block">
                       <div class="item">
                         <div class="thumb">
                           <img src="<?php echo base_url();?>application/images/Research2.png" width="100%" alt="...">
                           <span class="researchSpan"><h2 style="color: white;">Cryptography</h2>
                           <h4 style="color: white;">University of Maryland Baltimore county</h4></span>
                           <div class="social">
                             <ul>
                               <li><a href="/researches"><b>Read More</b></a></li>
                             </ul>
                           </div>
                         </div>
                       </div>
                     </div>
                     </div>
                     <div class="col-md-4 col-sm-12 team-items">
                     <div class="single-item text-center team-standard team-style-block">
                       <div class="item">
                         <div class="thumb">
                           <img src="<?php echo base_url();?>application/images/Research4.png" width="100%" alt="...">
                           <span class="researchSpan"><h2 style="color: white;">Stem Cells and 3D Printing </h2>
                           <h4 style="color: white;">Binghamton University (SUNY)</h4></span>
                           <div class="social">
                             <ul>
                               <li><a href="/researches"><b>Read More</b></a></li>
                             </ul>
                           </div>
                         </div>
                       </div>
                     </div>
                     </div>
                     </div>-->
               </div>
               <div class="row">
                  <?php
                     $countt = 0;
                     foreach ($research as $rtkey => $rtvalue) {

                       if($countt == 0 || $countt == 1 || $countt == 2)
                       {
                         $countt = $countt + 1;
                         continue;
                       }
                       // code...
                     ?>
                  <div class="col-md-4 col-sm-12 team-items">
                     <div class="single-item text-center team-standard team-style-block">
                        <div class="item">
                           <div class="thumb">
                              <img src="<?php echo base_url();?>application/images/Research3.png" width="100%" alt="...">
                              <span class="researchSpan">
                                 <h2 style="color: white;"><?php echo $rtvalue[0]['topic']; ?></h2>
                                 <h4 style="color: white;"><?php echo $rtvalue[0]['university_name']; ?></h4>
                              </span>
                              <div class="social">
                                 <ul>
                                    <li><a href="/researches"><b>Read More</b></a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php  } ?>
                  <!--<div class="col-md-4 col-sm-12 team-items">
                     <div class="single-item text-center team-standard team-style-block">
                       <div class="item">
                         <div class="thumb">
                           <img src="<?php echo base_url();?>application/images/Research3.png" width="100%" alt="...">
                           <span class="researchSpan"><h2 style="color: white;">Chemical process design</h2>
                           <h4 style="color: white;">California State University Long beach</h4></span>
                           <div class="social">
                             <ul>
                               <li><a href="/researches"><b>Read More</b></a></li>
                             </ul>
                           </div>
                         </div>
                       </div>
                     </div>
                     </div>
                     <div class="col-md-4 col-sm-12 team-items">
                     <div class="single-item text-center team-standard team-style-block">
                       <div class="item">
                         <div class="thumb">
                           <img src="<?php echo base_url();?>application/images/Research4.png" width="100%" alt="...">
                           <span class="researchSpan"><h2 style="color: white;">Nanotechnology</h2>
                           <h4 style="color: white;">University of North Texas</h4></span>
                           <div class="social">
                             <ul>
                               <li><a href="/researches"><b>Read More</b></a></li>
                             </ul>
                           </div>
                         </div>
                       </div>
                     </div>
                     </div>
                     <div class="col-md-4 col-sm-12 team-items">
                     <div class="single-item text-center team-standard team-style-block">
                       <div class="item">
                         <div class="thumb">
                           <img src="<?php echo base_url();?>application/images/Research2.png" width="100%" alt="...">
                           <span class="researchSpan"><h2 style="color: white;">Machine Learning</h2>
                           <h4 style="color: white;">Northeastern University</h4></span>
                           <div class="social">
                             <ul>
                               <li><a href="/researches"><b>Read More</b></a></li>
                             </ul>
                           </div>
                         </div>
                       </div>
                     </div>
                     </div>-->
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section style="margin-bottom: 30px;">
   <div class="container">
      <div class="row">
         <div class="aboutus" style="text-align: center;">
            <h1 class="aboutus-title">Our Blogs</h1>
            <p style="color: #4a4949;    font-size: 25px;">Access our knowledge hub & keep abreast with the latest trends in higher education.
            </p>
         </div>
         <!-- The carousel -->
         <div id="transition-timer-carousel" class="carousel slide transition-timer-carousel" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
               <li data-target="#transition-timer-carousel" data-slide-to="0" class="active"></li>
               <li data-target="#transition-timer-carousel" data-slide-to="1"></li>
               <li data-target="#transition-timer-carousel" data-slide-to="2"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
               <div class="item active">
                  <div class="row main">
                     <div class="col-md-6 col-sm-6">
                        <div class="col-md-12 col-xs-12">
                           <div class="thumbnail img-thumb-bg">
                              <img src="<?php echo base_url();?>application/images/blogs/1.png"  alt="...">
                              <div class="overlay"></div>
                              <div class="caption">
                                 <div class="content">
                                    <div class="title"><a href="https://www.imperial-overseas.com/blog/coronavirus-and-overseas-education-what-to-expect/" target="_blank">COVID-19 AND OVERSEAS EDUCATION: WHAT TO EXPECT ?</a></div>
                                    <br>
                                    <div class="tag"><a href="#"><i class="fa fa-user"></i>  Admin &nbsp;&nbsp;<i class="fa fa-calendar"></i> April 3, 2020 </a></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                           <h3 class="purple">COVID-19 AND OVERSEAS EDUCATION: WHAT TO EXPECT ?</h3>
                           <p style="margin: 0 0 10px;font-size: 13px;line-height: 1.6;">The start of 2020 seems to have unfortunately set in motion for hysteria, panic and commotion amongst the student community, thanks to COVID-19. Students who are planning for the upcoming 2020 intake are in two minds now with regards to taking their plans further. Amongst all the negative news and comments floating online, through this article we intend to put forth all viable options feasible for students planning for their education abroad.</p>
                           <a href="https://www.imperial-overseas.com/blog/coronavirus-and-overseas-education-what-to-expect/" class="btn btn-md btn-card" target="_blank">Read More</a>
                           <h5 class="purple"><i class="fa fa-user"></i>  Admin &nbsp;&nbsp;<i class="fa fa-calendar"></i> April 3, 2020 </h5>
                        </div>
                     </div>
                     <div class="row col-sm-6">
                        <div class="col-md-6 col-xs-6">
                           <div class="thumbnail img-thumb-bg">
                              <img src="<?php echo base_url();?>application/images/blogs/studyAbroadCovid.png" width="100%" alt="...">
                              <div class="overlay"></div>
                              <div class="caption">
                                 <div class="content">
                                    <div class="title"><a href="https://www.imperial-overseas.com/blog/study-abroad-covid-19-updates-faqs/" target="_blank">Study Abroad: COVID-19 updates & FAQ’s</a></div>
                                    <br>
                                    <div class="tag"><a href="#"><i class="fa fa-user"></i>  Admin &nbsp;&nbsp;<i class="fa fa-calendar"></i> April 29, 2020 </a></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                           <div class="thumbnail img-thumb-bg">
                              <img src="<?php echo base_url();?>application/images/blogs/Pursuing-MBA-in-USA.png" width="100%" alt="...">
                              <div class="overlay"></div>
                              <div class="caption">
                                 <div class="content">
                                    <div class="title"><a href="https://www.imperial-overseas.com/blog/pursuing-mba-in-usa/" target="_blank">Pursuing MBA in USA</a></div>
                                    <br>
                                    <div class="tag"><a href="#"><i class="fa fa-user"></i>  Admin &nbsp;&nbsp;<i class="fa fa-calendar"></i> May 11, 2020 </a></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                           <div class="thumbnail img-thumb-bg">
                              <img src="<?php echo base_url();?>application/images/blogs/studu-aus.png" width="100%" alt="...">
                              <div class="overlay"></div>
                              <div class="caption">
                                 <div class="content">
                                    <div class="title"><a href="https://www.imperial-overseas.com/blog/increase-in-the-popularity-of-australia-for-indian-students-and-the-regional-benefits/" target="_blank">INCREASE IN THE POPULARITY OF AUSTRALIA FOR REGIONAL BENEFITS</a></div>
                                    <br>
                                    <div class="tag"><a href="#"><i class="fa fa-user"></i>  Admin &nbsp;&nbsp;<i class="fa fa-calendar"></i> April 3, 2020 </a></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                           <div class="thumbnail img-thumb-bg">
                              <img src="<?php echo base_url();?>application/images/blogs/study-canada.png" width="100%" alt="...">
                              <div class="overlay"></div>
                              <div class="caption">
                                 <div class="content">
                                    <div class="title"><a href="https://www.imperial-overseas.com/blog/what-makes-canada-one-of-the-top-study-abroad-destinations-for-indian-students/" target="_blank">ONE OF THE TOP STUDY ABROAD DESTINATIONS FOR INDIAN STUDENTS</a></div>
                                    <br>
                                    <div class="tag"><a href="#"><i class="fa fa-user"></i>  Admin &nbsp;&nbsp;<i class="fa fa-calendar"></i> April 4, 2020 </a></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="item">
                  <div class="row main">
                     <div class="col-md-6 col-sm-6">
                        <div class="col-md-12 col-xs-12">
                           <div class="thumbnail img-thumb-bg">
                              <img src="<?php echo base_url();?>application/images/blogs/2.png"  alt="...">
                              <div class="overlay"></div>
                              <div class="caption">
                                 <div class="content">
                                    <div class="title"><a href="https://www.imperial-overseas.com/blog/how-can-you-use-the-lockdown-period-to-develop-your-profile/" target="_blank">How can you use the lockdown period to develop your profile?</a></div>
                                    <br>
                                    <div class="tag"><a href="#"><i class="fa fa-user"></i>  Admin &nbsp;&nbsp;<i class="fa fa-calendar"></i> May 11, 2020 </a></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                           <h3 class="purple">How can you use the lockdown period to develop your profile?</h3>
                           <p style="margin: 0 0 10px;font-size: 13px;line-height: 1.6;">As the country faces a lockdown owing to the COVID-19 pandemic, you can utilize this opportunity to upscale your skills for accelerating your career growth. Although we are amidst this unprecedented event which has perturbed our state of normalcy, here are a comprehensive list of things that you may do to build your profile.</p>
                           <a href="https://www.imperial-overseas.com/blog/how-can-you-use-the-lockdown-period-to-develop-your-profile/" class="btn btn-md btn-card" target="_blank">Read More</a>
                           <h5 class="purple"><i class="fa fa-user"></i>  Admin &nbsp;&nbsp;<i class="fa fa-calendar"></i> May 11, 2020 </h5>
                        </div>
                     </div>
                     <div class="row col-sm-6">
                        <div class="col-md-6 col-xs-6">
                           <div class="thumbnail img-thumb-bg">
                              <img src="<?php echo base_url();?>application/images/blogs/01.Canada.png" width="100%" alt="...">
                              <div class="overlay"></div>
                              <div class="caption">
                                 <div class="content">
                                    <div class="title"><a href="https://www.imperial-overseas.com/blog/immigration-canadas-road-to-economic-recovery-post-covid-19/" target="_blank">Immigration – Canada’s Road to Economic Recovery Post COVID-19?</a></div>
                                    <br>
                                    <div class="tag"><a href="#"><i class="fa fa-user"></i>  Admin &nbsp;&nbsp;<i class="fa fa-calendar"></i> April 29, 2020 </a></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                           <div class="thumbnail img-thumb-bg">
                              <img src="<?php echo base_url();?>application/images/blogs/Biotechnology-Courses.png" width="100%" alt="...">
                              <div class="overlay"></div>
                              <div class="caption">
                                 <div class="content">
                                    <div class="title"><a href="https://www.imperial-overseas.com/blog/biotechnology-courses/" target="_blank">Biotechnology Courses</a></div>
                                    <br>
                                    <div class="tag"><a href="#"><i class="fa fa-user"></i>  Admin &nbsp;&nbsp;<i class="fa fa-calendar"></i> July 11, 2018 </a></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                           <div class="thumbnail img-thumb-bg">
                              <img src="<?php echo base_url();?>application/images/blogs/ms-const.png" width="100%" alt="...">
                              <div class="overlay"></div>
                              <div class="caption">
                                 <div class="content">
                                    <div class="title"><a href="https://www.imperial-overseas.com/blog/masters-in-construction-management/" target="_blank">MASTER’S IN CONSTRUCTION MANAGEMENT</a></div>
                                    <br>
                                    <div class="tag"><a href="#"><i class="fa fa-user"></i>  Admin &nbsp;&nbsp;<i class="fa fa-calendar"></i> April 29, 2020 </a></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                           <div class="thumbnail img-thumb-bg">
                              <img src="<?php echo base_url();?>application/images/blogs/Pursuing-undergraduate-degree-in-the-USA.png" width="100%" alt="...">
                              <div class="overlay"></div>
                              <div class="caption">
                                 <div class="content">
                                    <div class="title"><a href="https://www.imperial-overseas.com/blog/pursuing-undergraduate-degree-in-the-usa/" target="_blank">Pursuing undergraduate degree in the USA</a></div>
                                    <br>
                                    <div class="tag"><a href="#"><i class="fa fa-user"></i>  Admin &nbsp;&nbsp;<i class="fa fa-calendar"></i> April 29, 2020 </a></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="item">
                  <div class="row main">
                     <div class="col-md-6 col-sm-6">
                        <div class="col-md-12 col-xs-12">
                           <div class="thumbnail img-thumb-bg">
                              <img src="<?php echo base_url();?>application/images/blogs/3.png"  alt="...">
                              <div class="overlay"></div>
                              <div class="caption">
                                 <div class="content">
                                    <div class="title"><a href="https://www.imperial-overseas.com/blog/impact-of-covid-19-on-education-in-new-zealand/" target="_blank">Impact of COVID-19 on Education in New Zealand</a></div>
                                    <br>
                                    <div class="tag"><a href="#"><i class="fa fa-user"></i>  Admin &nbsp;&nbsp;<i class="fa fa-calendar"></i> May 11, 2020 </a></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                           <h3 class="purple">Impact of COVID-19 on Education in New Zealand</h3>
                           <p style="margin: 0 0 10px;font-size: 13px;line-height: 1.6;">As the world is fighting strongly against COVID-19, we hope to see the end of this pandemic soon and recover even stronger. As the entire world unites and fights this battle against the virus, there are some countries which have managed to curb the spread with their constructive measures. New Zealand, a small country with about 4.8 million people has set a strong example across the world in its war against COVID-19 with constantly diminishing cases of infected people. </p>
                           <a href="https://www.imperial-overseas.com/blog/impact-of-covid-19-on-education-in-new-zealand/" class="btn btn-md btn-card" target="_blank">Read More</a>
                           <h5 class="purple"><i class="fa fa-user"></i>  Admin &nbsp;&nbsp;<i class="fa fa-calendar"></i> May 11, 2020 </h5>
                        </div>
                     </div>
                     <div class="row col-sm-6">
                        <div class="col-md-6 col-xs-6">
                           <div class="thumbnail img-thumb-bg">
                              <img src="<?php echo base_url();?>application/images/blogs/Chemical-Engineering.png" width="100%" alt="...">
                              <div class="overlay"></div>
                              <div class="caption">
                                 <div class="content">
                                    <div class="title"><a href="https://www.imperial-overseas.com/blog/chemical-engineering/" target="_blank">Chemical Engineering</a></div>
                                    <br>
                                    <div class="tag"><a href="#"><i class="fa fa-user"></i>  Admin &nbsp;&nbsp;<i class="fa fa-calendar"></i> July 11, 2018 </a></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                           <div class="thumbnail img-thumb-bg">
                              <img src="<?php echo base_url();?>application/images/blogs/covid-19-imperial.png" width="100%" alt="...">
                              <div class="overlay"></div>
                              <div class="caption">
                                 <div class="content">
                                    <div class="title"><a href="https://www.imperial-overseas.com/blog/coronavirus-and-overseas-education-what-to-expect/" target="_blank">COVID-19 AND OVERSEAS EDUCATION: WHAT TO EXPECT?</a></div>
                                    <br>
                                    <div class="tag"><a href="#"><i class="fa fa-user"></i>  Admin &nbsp;&nbsp;<i class="fa fa-calendar"></i> April 3, 2020 </a></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                           <div class="thumbnail img-thumb-bg">
                              <img src="<?php echo base_url();?>application/images/blogs/data-science.png" width="100%" alt="...">
                              <div class="overlay"></div>
                              <div class="caption">
                                 <div class="content">
                                    <div class="title"><a href="https://www.imperial-overseas.com/blog/career-prospects-after-ms-in-data-science-big-data/" target="_blank">CAREER PROSPECTS AFTER MS IN DATA SCIENCE & BIG DATA</a></div>
                                    <br>
                                    <div class="tag"><a href="#"><i class="fa fa-user"></i>  Admin &nbsp;&nbsp;<i class="fa fa-calendar"></i> May 20, 2019 </a></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                           <div class="thumbnail img-thumb-bg">
                              <img src="<?php echo base_url();?>application/images/blogs/04.-Australia.png" width="100%" alt="...">
                              <div class="overlay"></div>
                              <div class="caption">
                                 <div class="content">
                                    <div class="title"><a href="https://www.imperial-overseas.com/blog/impact-of-covid-19-on-education-in-australia/" target="_blank">Impact of COVID-19 on Education in Australia</a></div>
                                    <br>
                                    <div class="tag"><a href="#"><i class="fa fa-user"></i>  Admin &nbsp;&nbsp;<i class="fa fa-calendar"></i> April 29, 2020 </a></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#transition-timer-carousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" style="/*margin-left: -30px;*/    font-size: 40px;color: #815cd5;font-weight: 700;"></span>
            </a>
            <a class="right carousel-control" href="#transition-timer-carousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" style="/*margin-right: -30px;*/    font-size: 40px;color: #815cd5;font-weight: 700;"></span>
            </a>
            <!-- Timer "progress bar" -->
            <!-- <hr class="transition-timer-carousel-progress-bar animate" /> -->
         </div>
      </div>
   </div>
</section>
<section style="margin-bottom: 30px;">
   <div class="container">
      <div class="row">
         <div class="aboutus" style="text-align: center;">
            <h1 class="aboutus-title">Friends & Alumni</h1>
            <p style="color: #4a4949;    font-size: 25px;">Find out first hand what Alumni of the Universities have to say about their experiences!
            </p>
         </div>
         <!-- The carousel -->
         <div id="transition-frndAlum-carousel" class="carousel slide transition-frndAlum-carousel" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
               <li data-target="#transition-frndAlum-carousel" data-slide-to="0" class="active"></li>
               <li data-target="#transition-frndAlum-carousel" data-slide-to="1"></li>
               <li data-target="#transition-frndAlum-carousel" data-slide-to="2"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
               <div class="item active">
                  <div class="row main">
                     <div class="col-md-4" style="margin-top: 15px">
                        <div class="card-content card-contentfrndAlum">
                           <div class="card-img card-imgfrndAlum">
                              <p>I hail from Mumbai the financial capital of India. From the very beginning I was interested in the advent of technology and keen to imbibe new ideas. </p>
                              <span>
                                 <h5><i class="fas fa-quote-left purple" style="    font-size: 40px;"></i></h5>
                              </span>
                           </div>
                           <div class="card-desc marginBottomP">
                              <div style="text-align: center;">
                                 <img src="https://www.imperial-overseas.com/images/team/1.jpg" alt="Avatar" class="img-frndAlumn">
                                 <h3 class="purple">Mr. Banit Sawhney</h3>
                                 <p><i class="fas fa-university purple"></i> Northeastern University  &nbsp;&nbsp;<br> <i class="fa fa-graduation-cap purple"></i> MS in Data Analytics Engineering</p>
                                 <br>
                                 <a href="#" class="btn-cardPurple ">Read More</a>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4" style="margin-top: 15px">
                        <div class="card-content card-contentfrndAlumP">
                           <div class="card-img card-imgfrndAlumP">
                              <p>I hail from Mumbai the financial capital of India. From the very beginning I was interested in the advent of technology and keen to imbibe new ideas. </p>
                              <span>
                                 <h5><i class="fas fa-quote-left orange" style="    font-size: 40px;"></i></h5>
                              </span>
                           </div>
                           <div class="card-desc marginBottomP">
                              <div style="text-align: center;">
                                 <img src="https://www.imperial-overseas.com/images/team/1.jpg" alt="Avatar" class="img-frndAlumn">
                                 <h3 class="orange">Mr. Banit Sawhney</h3>
                                 <p class="wht"><i class="fas fa-university orange"></i> Northeastern University  &nbsp;&nbsp;<br> <i class="fa fa-graduation-cap orange"></i> MS in Data Analytics Engineering</p>
                                 <br>
                                 <a href="#" class="btn-card ">Read More</a>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4" style="margin-top: 15px">
                        <div class="card-content card-contentfrndAlum">
                           <div class="card-img card-imgfrndAlum">
                              <p>I hail from Mumbai the financial capital of India. From the very beginning I was interested in the advent of technology and keen to imbibe new ideas. </p>
                              <span>
                                 <h5><i class="fas fa-quote-left purple" style="    font-size: 40px;"></i></h5>
                              </span>
                           </div>
                           <div class="card-desc marginBottomP">
                              <div style="text-align: center;">
                                 <img src="https://www.imperial-overseas.com/images/team/1.jpg" alt="Avatar" class="img-frndAlumn">
                                 <h3 class="purple">Mr. Banit Sawhney</h3>
                                 <p><i class="fas fa-university purple"></i> Northeastern University  &nbsp;&nbsp;<br> <i class="fa fa-graduation-cap purple"></i> MS in Data Analytics Engineering</p>
                                 <br>
                                 <a href="#" class="btn-cardPurple ">Read More</a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="item">
                  <div class="row main">
                     <div class="col-md-4" style="margin-top: 15px">
                        <div class="card-content card-contentfrndAlumP">
                           <div class="card-img card-imgfrndAlumP">
                              <p>I hail from Mumbai the financial capital of India. From the very beginning I was interested in the advent of technology and keen to imbibe new ideas. </p>
                              <span>
                                 <h5><i class="fas fa-quote-left orange" style="    font-size: 40px;"></i></h5>
                              </span>
                           </div>
                           <div class="card-desc marginBottomP">
                              <div style="text-align: center;">
                                 <img src="https://www.imperial-overseas.com/images/team/1.jpg" alt="Avatar" class="img-frndAlumn">
                                 <h3 class="orange">Mr. Banit Sawhney</h3>
                                 <p class="wht"><i class="fas fa-university orange"></i> Northeastern University  &nbsp;&nbsp;<br> <i class="fa fa-graduation-cap orange"></i> MS in Data Analytics Engineering</p>
                                 <br>
                                 <a href="#" class="btn-card ">Read More</a>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4" style="margin-top: 15px">
                        <div class="card-content card-contentfrndAlum">
                           <div class="card-img card-imgfrndAlum">
                              <p>I hail from Mumbai the financial capital of India. From the very beginning I was interested in the advent of technology and keen to imbibe new ideas. </p>
                              <span>
                                 <h5><i class="fas fa-quote-left purple" style="    font-size: 40px;"></i></h5>
                              </span>
                           </div>
                           <div class="card-desc marginBottomP">
                              <div style="text-align: center;">
                                 <img src="https://www.imperial-overseas.com/images/team/1.jpg" alt="Avatar" class="img-frndAlumn">
                                 <h3 class="purple">Mr. Banit Sawhney</h3>
                                 <p><i class="fas fa-university purple"></i> Northeastern University  &nbsp;&nbsp;<br> <i class="fa fa-graduation-cap purple"></i> MS in Data Analytics Engineering</p>
                                 <br>
                                 <a href="#" class="btn-cardPurple ">Read More</a>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4" style="margin-top: 15px">
                        <div class="card-content card-contentfrndAlumP">
                           <div class="card-img card-imgfrndAlumP">
                              <p>I hail from Mumbai the financial capital of India. From the very beginning I was interested in the advent of technology and keen to imbibe new ideas. </p>
                              <span>
                                 <h5><i class="fas fa-quote-left orange" style="    font-size: 40px;"></i></h5>
                              </span>
                           </div>
                           <div class="card-desc marginBottomP">
                              <div style="text-align: center;">
                                 <img src="https://www.imperial-overseas.com/images/team/1.jpg" alt="Avatar" class="img-frndAlumn">
                                 <h3 class="orange">Mr. Banit Sawhney</h3>
                                 <p class="wht"><i class="fas fa-university orange"></i> Northeastern University  &nbsp;&nbsp;<br> <i class="fa fa-graduation-cap orange"></i> MS in Data Analytics Engineering</p>
                                 <br>
                                 <a href="#" class="btn-card ">Read More</a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="item">
                  <div class="row main">
                     <div class="col-md-4" style="margin-top: 15px">
                        <div class="card-content card-contentfrndAlum">
                           <div class="card-img card-imgfrndAlum">
                              <p>I hail from Mumbai the financial capital of India. From the very beginning I was interested in the advent of technology and keen to imbibe new ideas. </p>
                              <span>
                                 <h5><i class="fas fa-quote-left purple" style="    font-size: 40px;"></i></h5>
                              </span>
                           </div>
                           <div class="card-desc marginBottomP">
                              <div style="text-align: center;">
                                 <img src="https://www.imperial-overseas.com/images/team/1.jpg" alt="Avatar" class="img-frndAlumn">
                                 <h3 class="purple">Mr. Banit Sawhney</h3>
                                 <p><i class="fas fa-university purple"></i> Northeastern University  &nbsp;&nbsp;<br> <i class="fa fa-graduation-cap purple"></i> MS in Data Analytics Engineering</p>
                                 <br>
                                 <a href="#" class="btn-cardPurple ">Read More</a>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4" style="margin-top: 15px">
                        <div class="card-content card-contentfrndAlumP">
                           <div class="card-img card-imgfrndAlumP">
                              <p>I hail from Mumbai the financial capital of India. From the very beginning I was interested in the advent of technology and keen to imbibe new ideas. </p>
                              <span>
                                 <h5><i class="fas fa-quote-left orange" style="    font-size: 40px;"></i></h5>
                              </span>
                           </div>
                           <div class="card-desc marginBottomP">
                              <div style="text-align: center;">
                                 <img src="https://www.imperial-overseas.com/images/team/1.jpg" alt="Avatar" class="img-frndAlumn">
                                 <h3 class="orange">Mr. Banit Sawhney</h3>
                                 <p class="wht"><i class="fas fa-university orange"></i> Northeastern University  &nbsp;&nbsp;<br> <i class="fa fa-graduation-cap orange"></i> MS in Data Analytics Engineering</p>
                                 <br>
                                 <a href="#" class="btn-card ">Read More</a>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4" style="margin-top: 15px">
                        <div class="card-content card-contentfrndAlum">
                           <div class="card-img card-imgfrndAlum">
                              <p>I hail from Mumbai the financial capital of India. From the very beginning I was interested in the advent of technology and keen to imbibe new ideas. </p>
                              <span>
                                 <h5><i class="fas fa-quote-left purple" style="    font-size: 40px;"></i></h5>
                              </span>
                           </div>
                           <div class="card-desc marginBottomP">
                              <div style="text-align: center;">
                                 <img src="https://www.imperial-overseas.com/images/team/1.jpg" alt="Avatar" class="img-frndAlumn">
                                 <h3 class="purple">Mr. Banit Sawhney</h3>
                                 <p><i class="fas fa-university purple"></i> Northeastern University  &nbsp;&nbsp;<br> <i class="fa fa-graduation-cap purple"></i> MS in Data Analytics Engineering</p>
                                 <br>
                                 <a href="#" class="btn-cardPurple ">Read More</a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#transition-frndAlum-carousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" style="/*margin-left: -35px;*/    font-size: 40px;color: #815cd5;font-weight: 700;"></span>
            </a>
            <a class="right carousel-control" href="#transition-frndAlum-carousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" style="/*margin-right: -35px;*/    font-size: 40px;color: #815cd5;font-weight: 700;"></span>
            </a>
         </div>
      </div>
   </div>
</section>
<!-- <section style="margin: 20px 0px 16px 0px;">
   <div class="container">
   <div class="row">
     <div class="aboutus" style="text-align: center;">
         <h1 class="aboutus-title">Friends & Alumni</h1>
         <p style="color: #4a4949;    font-size: 25px;">Donec vitae sapien ut libero venenatis faucibu. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt
         </p>
     </div>
     <div class="team-area">
       <div class="team-warp">
         <div class="team-five">
           <div class="team-style-five">
             <div class="container">
               <div class="row">
                 <div class="col-md-3 col-sm-12 team-items">
                   <div class="single-item text-center team-standard team-style-block">
                     <div class="item">
                       <div class="thumb">
                         <img src="https://www.imperial-overseas.com/images/team/1.jpg" alt="t1" >
                         <div class="social">
                           <ul>
                             <li><a href="/alumnu-profile"><b>Read My Story</b></a></li>

                           </ul>
                         </div>
                       </div>
                       <div class="info">
                         <h3>Banit Sawhney</h3>

                         <p><span id="spanName"><b>University :</b> </span>Northeastern University</p>
                         <p><span id="spanName"><b>College :</b> </span>College of Engineering</p>
                         <p><span id="spanName"><b>Course :</b> </span>MS in Data Analytics Engineering</p>
                         <p><span id="spanName"><b>Duration :</b> </span>Fall 2016 – Fall 2018</p>


                       </div>
                     </div>
                   </div>
                 </div>
                 <div class="col-md-3 col-sm-12 team-items">
                   <div class="single-item text-center team-standard team-style-block">
                     <div class="item">
                       <div class="thumb">
                         <img src="https://www.imperial-overseas.com/images/team/1.jpg" alt="t1" >
                         <div class="social">
                           <ul>
                             <li><a href="/alumnu-profile"><b>Read My Story</b></a></li>

                           </ul>
                         </div>
                       </div>
                       <div class="info">
                         <h3>Banit Sawhney</h3>

                         <p><span id="spanName"><b>University :</b> </span>Northeastern University</p>
                         <p><span id="spanName"><b>College :</b> </span>College of Engineering</p>
                         <p><span id="spanName"><b>Course :</b> </span>MS in Data Analytics Engineering</p>
                         <p><span id="spanName"><b>Duration :</b> </span>Fall 2016 – Fall 2018</p>


                       </div>
                     </div>
                   </div>
                 </div>
                 <div class="col-md-3 col-sm-12 team-items">
                   <div class="single-item text-center team-standard team-style-block">
                     <div class="item">
                       <div class="thumb">
                         <img src="https://www.imperial-overseas.com/images/team/1.jpg" alt="t1" >
                         <div class="social">
                           <ul>
                             <li><a href="/alumnu-profile"><b>Read My Story</b></a></li>

                           </ul>
                         </div>
                       </div>
                       <div class="info">
                         <h3>Banit Sawhney</h3>

                         <p><span id="spanName"><b>University :</b> </span>Northeastern University</p>
                         <p><span id="spanName"><b>College :</b> </span>College of Engineering</p>
                         <p><span id="spanName"><b>Course :</b> </span>MS in Data Analytics Engineering</p>
                         <p><span id="spanName"><b>Duration :</b> </span>Fall 2016 – Fall 2018</p>


                       </div>
                     </div>
                   </div>
                 </div>
                 <div class="col-md-3 col-sm-12 team-items">
                   <div class="single-item text-center team-standard team-style-block">
                     <div class="item">
                       <div class="thumb">
                         <img src="https://www.imperial-overseas.com/images/team/1.jpg" alt="t1" >
                         <div class="social">
                           <ul>
                             <li><a href="/alumnu-profile"><b>Read My Story</b></a></li>

                           </ul>
                         </div>
                       </div>
                       <div class="info">
                         <h3>Banit Sawhney</h3>

                         <p><span id="spanName"><b>University :</b> </span>Northeastern University</p>
                         <p><span id="spanName"><b>College :</b> </span>College of Engineering</p>
                         <p><span id="spanName"><b>Course :</b> </span>MS in Data Analytics Engineering</p>
                         <p><span id="spanName"><b>Duration :</b> </span>Fall 2016 – Fall 2018</p>


                       </div>
                     </div>
                   </div>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
   </div>
   </section>  -->
<!-- <section>
   <div class="container">
      <div class="row" style="text-align: center;">
        <div class="col-xs-11 col-md-12 col-centered">

          <div id="carousel" class="carousel slide" data-ride="carousel" data-type="multi" data-interval="4500" style="margin-left: 65px;">
            <div class="carousel-inner">
             <?php
      if(isset($preview_university))
      {
          ?>
              <div class="item active uniitem">
                <div class="carousel-col">
                  <div class="block whiteBg img-responsive"><a href="/<?=$preview_university['slug']?>"><img src="<?=$preview_university['logo']?>" class="img-responsive center-block" <?=$preview_university['slider_css']?> ></a></div>
                </div>
              </div>
              <?php
      }
      foreach($top_universities as $index => $topUniversity)
      {
          if(isset($preview_university))
          {
              if($index < 5)
              {
                  ?>
              <div class="item uniitem">
                <div class="carousel-col">
                  <div class="block whiteBg img-responsive"><a href="/<?=$topUniversity['slug']?>"><img src="<?=$topUniversity['logo']?>" class="img-responsive center-block" width="200px" ></a></div>
                </div>
              </div>
             <?php
      }
      }
      else
      {
      ?>
                              <div <?=!$index ? 'class="item active"' : 'class="item"'?>>
                                 <div class="carousel-col">
                                    <div class="block whiteBg img-responsive"><a href="/<?=$topUniversity['slug']?>"><img src="<?=$topUniversity['logo']?>" class="img-responsive center-block" <?=$topUniversity['slider_css']?> ></a></div>
                                 </div>
                              </div>
                              <?php
      }
      }
      ?>

            </div>


            <div class="left carousel-control">
              <a href="#carousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
            </div>
            <div class="right carousel-control">
              <a href="#carousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          </div>

        </div>
      </div>
    </div>
   </section>  -->
<section style="margin-bottom: 30px;">
   <div class="container">
      <div class="row">
         <div class="aboutus" style="text-align: center;    margin: 50px 0px;">
            <h1 class="aboutus-title">Popular Universities</h1>
            <p style="color: #4a4949;    font-size: 25px;">Universities Trending on HelloUni!
            </p>
         </div>
         <div class="col-md-12">
            <div class="carousel carousel-showmanymoveone slide" id="carousel123">
               <div class="carousel-inner" style="margin-left: 25px;">
                  <?php
                     if(isset($preview_university))
                     {
                         ?>
                  <div class="item active">
                     <div class="col-xs-12 col-sm-6 col-md-3 boxLogo"> <a href="/<?=$preview_university['slug']?>"><img src="<?=$preview_university['logo']?>" class="img-responsive center-block" <?=$preview_university['slider_css']?> ></a></div>
                  </div>
                  <?php
                     }
                     foreach($top_universities as $index => $topUniversity)
                     {
                         if(isset($preview_university))
                         {
                             if($index < 5)
                             {
                                 ?>
                  <div class="item">
                     <div class="col-xs-12 col-sm-6 col-md-3 boxLogo"><a href="/<?=$topUniversity['slug']?>"><img src="<?=$topUniversity['logo']?>" class="img-responsive center-block" <?=$topUniversity['slider_css']?> ></a></div>
                  </div>
                  <?php
                     }
                     }
                     else
                     {
                     ?>
                  <div  <?=!$index ? 'class="item active"' : 'class="item"'?>>
                     <div class="col-xs-12 col-sm-6 col-md-3 boxLogo"><a href="/<?=$topUniversity['slug']?>"><img src="<?=$topUniversity['logo']?>" class="img-responsive center-block" <?=$topUniversity['slider_css']?> ></a></div>
                  </div>
                  <?php
                     }
                     }
                     ?>
               </div>
               <a class="left carousel-control" href="#carousel123" data-slide="prev"><i class="glyphicon glyphicon-chevron-left" style="margin-left: -40px;    font-size: 40px;color: #815cd5;font-weight: 700;"></i></a>
               <a class="right carousel-control" href="#carousel123" data-slide="next"><i class="glyphicon glyphicon-chevron-right" style="margin-right: -40px;    font-size: 40px;color: #815cd5;font-weight: 700;"></i></a>
            </div>
         </div>
      </div>
   </div>
</section>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js'></script>
<script src="<?php echo base_url();?>js/bootstrap-typeahead-min.js"></script>
<script type="text/javascript">
   /** youtube lazyload begin **/

     ( function() {

      var youtube = document.querySelectorAll( ".youtube" );

      for (var i = 0; i < youtube.length; i++) {

        var source = "https://img.youtube.com/vi/"+ youtube[i].dataset.embed +"/sddefault.jpg";

        var image = new Image();
            image.src = source;
            image.addEventListener( "load", function() {
              youtube[ i ].appendChild( image );
            }( i ) );

            youtube[i].addEventListener( "click", function() {

              var iframe = document.createElement( "iframe" );

                  iframe.setAttribute( "frameborder", "0" );
                  iframe.setAttribute( "allowfullscreen", "" );
                  iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1" );

                  this.innerHTML = "";
                  this.appendChild( iframe );
            } );
      };

     } )();
      /** youtube lazyload end -  https://codepen.io/tutsplus/pen/RRVRro  **/

      $.fn.jQuerySimpleCounter = function( options ) {
        var settings = $.extend({
            start:  0,
            end:    100,
            easing: 'swing',
            duration: 400,
            complete: ''
        }, options );

        var thisElement = $(this);

        $({count: settings.start}).animate({count: settings.end}, {
        duration: settings.duration,
        easing: settings.easing,
        step: function() {
          var mathCount = Math.ceil(this.count);
          thisElement.text(mathCount);
        },
        complete: settings.complete
      });
    };


   $('#number1').jQuerySimpleCounter({end: <?php echo $universityCount; ?>,duration: 3000});
   $('#number2').jQuerySimpleCounter({end: <?php echo $courseCount; ?>,duration: 3000});
   $('#number3').jQuerySimpleCounter({end: <?php echo $countryCount; ?>,duration: 2000});
   $('#number4').jQuerySimpleCounter({end: <?php echo $studentCount; ?>,duration: 2500});



   // $('.carousel[data-type="multi"] .item').each(function() {
   //   var next = $(this).next();
   //   if (!next.length) {
   //     next = $(this).siblings(':first');
   //   }
   //   next.children(':first-child').clone().appendTo($(this));

   //   for (var i = 0; i < 3; i++) {
   //     next = next.next();
   //     if (!next.length) {
   //       next = $(this).siblings(':first');
   //     }

   //     next.children(':first-child').clone().appendTo($(this));
   //   }
   // });


   $(document).ready(function() {
      //Events that reset and restart the timer animation when the slides change
      $("#transition-timer-carousel").on("slide.bs.carousel", function(event) {
          //The animate class gets removed so that it jumps straight back to 0%
          $(".transition-timer-carousel-progress-bar", this)
              .removeClass("animate").css("width", "0%");
      }).on("slid.bs.carousel", function(event) {
          //The slide transition finished, so re-add the animate class so that
          //the timer bar takes time to fill up
          $(".transition-timer-carousel-progress-bar", this)
              .addClass("animate").css("width", "100%");
      });

      //Kick off the initial slide animation when the document is ready
      $(".transition-timer-carousel-progress-bar", "#transition-timer-carousel")
          .css("width", "100%");
   });

   $(document).ready(function() {
      //Events that reset and restart the timer animation when the slides change
      $("#transition-frndAlum-carousel").on("slide.bs.carousel", function(event) {
          //The animate class gets removed so that it jumps straight back to 0%
          $(".transition-frndAlum-carousel-progress-bar", this)
              .removeClass("animate").css("width", "0%");
      }).on("slid.bs.carousel", function(event) {
          //The slide transition finished, so re-add the animate class so that
          //the timer bar takes time to fill up
          $(".transition-frndAlum-carousel-progress-bar", this)
              .addClass("animate").css("width", "100%");
      });

      //Kick off the initial slide animation when the document is ready
      $(".transition-frndAlum-carousel-progress-bar", "#transition-frndAlum-carousel")
          .css("width", "100%");
   });

   /*Downloaded from https://www.codeseek.co/rtpHarry/bootstrap-3-show-many-slide-one-carousel-YPBydd */
   (function(){
    // setup your carousels as you normally would using JS
    // or via data attributes according to the documentation
    // https://getbootstrap.com/javascript/#carousel
    $('#carousel123').carousel({ interval: 1500 });
   }());


   (function(){
    $('.carousel-showmanymoveone .item').each(function(){
      var itemToClone = $(this);

      for (var i=1;i<4;i++) {
        itemToClone = itemToClone.next();

        // wrap around if at end of item collection
        if (!itemToClone.length) {
          itemToClone = $(this).siblings(':first');
        }

        // grab item, clone, add marker class, add to collection
        itemToClone.children(':first-child').clone()
          .addClass("cloneditem-"+(i))
          .appendTo($(this));
      }
    });

   }());

   // Instantiate the Bootstrap carousel
$('.multi-item-carousel').carousel({
  interval: false
});

// for every slide in carousel, copy the next slide's item in the slide.
// Do the same for the next, next item.
$('.multi-item-carousel .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));


  if (next.next().length>0) {
    next.next().children(':first-child').clone().appendTo($(this));
  } else {
    $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
  }
});

</script>
<script type="text/javascript">
   var $input = $(".typeahead");

   /* $input.typeahead({
     source: function() {
       $.ajax({

       type: "GET",

       url: "/Croncontroller/searchKeyArray",

       cache: false,

       success: function(result){

        //$('#desired_subcourse').html(result);
        suggetion_set = result;
        console.log(suggetion_set);

         }
            });
   },
     autoSelect: true
   });*/


   $input.typeahead({
   source: function (query, process) {
      return $.get('/university/textsuggestions', { query: query }, function (data) {
          return process(data.options);
      });
   },
   autoSelect: false
   });

   $input.change(function() {
     var current = $input.typeahead("getActive");
     if (current) {
       // Some item from your model is active!
       if (current.name == $input.val()) {
         // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
       } else {
         // This means it is only a partial match, you can either add a new item
         // or take the active if you don't want new items
       }
     } else {
       // Nothing is active so it is a new value (or maybe empty value)
     }
   });


   function searchUniversity(){

     var search_text = $('#searchtextvalue').val();


     var dataString = 'query=' + search_text;

     $.ajax({

     type: "GET",

     url: "/freeTextSearch",

     data: dataString,

     cache: false,

     success: function(result){

      console.log(result);
      alert("Welcome To HelloUni..!!!");

     }

         });

   }

   $(document).ready(function(){
   //$('#myModal').modal('show');
    });
</script>
<style>
   .tt-query {
   -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
   -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
   box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
   }
   .tt-hint {
   color: #815cd5
   }
   .dropdown-menu {    /* used to be tt-dropdown-menu in older versions */
   width: -webkit-fill-available;
   margin-top: 4px;
   padding: 4px 0;
   font-size: 15px;
   background-color: #fff;
   border: 1px solid #ccc;
   border: 1px solid rgba(0, 0, 0, 0.2);
   -webkit-border-radius: 4px;
   -moz-border-radius: 4px;
   border-radius: 4px;
   -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
   -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
   box-shadow: 0 5px 10px rgba(0,0,0,.2);
   }
   .dropdown-menu>.active>a, .dropdown-menu>.active>a:focus, .dropdown-menu>.active>a:hover{
   color: #fff;
   text-decoration: none;
   background-color: #f6882c;
   outline: 0;
   }
   .dropdown-menu>li>a{
   display: block;
   padding: 4px 20px;
   clear: both;
   font-weight: 400;
   line-height: 1.8;
   color: #8759cc;
   white-space: nowrap;
   }
   .tt-suggestion {
   padding: 3px 20px;
   line-height: 24px;
   }
   .tt-suggestion.tt-cursor,.tt-suggestion:hover {
   color: #fff;
   background-color: #815cd5;
   }
   .tt-suggestion p {
   margin: 0;
   }
</style>
