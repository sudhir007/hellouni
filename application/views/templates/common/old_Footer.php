<head>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <!-- <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>-->
   <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
   <!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> -->
   <!-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
   <!-- Plugin JavaScript -->
   <script src="<?php echo base_url();?>js/jquery.easing.min.js"></script>
   <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
   <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>
   <!-- Custom Theme JavaScript -->
   <script src="<?php echo base_url();?>js/grayscale.js"></script>
   <script src="<?php echo base_url();?>js/main.js"></script>
   <!--<script src="<?php echo base_url();?>js/jquery-1.11.0.js" type="text/javascript"></script>-->
   <script src="<?php echo base_url();?>jqvmap/jquery.vmap.js" type="text/javascript"></script>
   <script src="<?php echo base_url();?>jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
   <!--<script src="<?php echo base_url();?>jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>-->
   <script type="text/javascript" src="<?php echo base_url();?>source/jquery.fancybox.js?v=2.1.5"></script>
   <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
   <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
   <style type="text/css">
      /*******************/
      /*.footer-strip { position:fixed; bottom:0; left:0; right:0; background:white; border-top:3px solid #0b2c6e; z-index:10;}
      .footer-strip-art {background:#004b7a; border-top:3px solid #0b2c6e;}
      .footer-strip abbr { position:relative; width:100%; display:block;}
      .footer-strip abbr:after { content:""; background:url(../images/blue-arrow-str.png) no-repeat 0 0; width:20px; height:10px; position:absolute;
      bottom:-10px; left:50%;}
      .tetf { display:inline-block; font:normal 12px Arial, Helvetica, sans-serif; color:#00000c;}*/
      /***********************/
      /*#slider-control img{
      padding-top: 60%;
      margin: 0 auto;
      }
      @media screen and (max-width: 992px){
      #slider-control img {
      padding-top: 70px;
      margin: 0 auto;
      }
      }*/
      /*.carousel-showmanymoveone .carousel-control {
      width: 4%;
      background-image: none;
      }
      .carousel-showmanymoveone .carousel-control.left {
      margin-left: 5px;
      }
      .carousel-showmanymoveone .carousel-control.right {
      margin-right: 5px;
      }
      .carousel-showmanymoveone .cloneditem-1,
      .carousel-showmanymoveone .cloneditem-2,
      .carousel-showmanymoveone .cloneditem-3,
      .carousel-showmanymoveone .cloneditem-4,
      .carousel-showmanymoveone .cloneditem-5 {
      display: none;
      }
      .carousel-showmanymoveone .clonedpartneritem-1,
      .carousel-showmanymoveone .clonedpartneritem-2,
      .carousel-showmanymoveone .clonedpartneritem-3,
      .carousel-showmanymoveone .clonedpartneritem-4,
      .carousel-showmanymoveone .clonedpartneritem-5 {
      display: none;
      }
      @media all and (min-width: 768px) {
      .carousel-showmanymoveone .carousel-inner > .active.left,
      .carousel-showmanymoveone .carousel-inner > .prev {
      left: -50%;
      }
      .carousel-showmanymoveone .carousel-inner > .active.right,
      .carousel-showmanymoveone .carousel-inner > .next {
      left: 50%;
      }
      .carousel-showmanymoveone .carousel-inner > .left,
      .carousel-showmanymoveone .carousel-inner > .prev.right,
      .carousel-showmanymoveone .carousel-inner > .active {
      left: 0;
      }
      .carousel-showmanymoveone .carousel-inner .cloneditem-1 {
      display: block;
      }
      }
      @media all and (min-width: 768px) and (transform-3d), all and (min-width: 768px) and (-webkit-transform-3d) {
      .carousel-showmanymoveone .carousel-inner > .item.active.right,
      .carousel-showmanymoveone .carousel-inner > .item.next {
      -webkit-transform: translate3d(50%, 0, 0);
      transform: translate3d(50%, 0, 0);
      left: 0;
      }
      .carousel-showmanymoveone .carousel-inner > .item.active.left,
      .carousel-showmanymoveone .carousel-inner > .item.prev {
      -webkit-transform: translate3d(-50%, 0, 0);
      transform: translate3d(-50%, 0, 0);
      left: 0;
      }
      .carousel-showmanymoveone .carousel-inner > .item.left,
      .carousel-showmanymoveone .carousel-inner > .item.prev.right,
      .carousel-showmanymoveone .carousel-inner > .item.active {
      -webkit-transform: translate3d(0, 0, 0);
      transform: translate3d(0, 0, 0);
      left: 0;
      }
      }
      @media all and (min-width: 992px) {
      .carousel-showmanymoveone .carousel-inner > .active.left,
      .carousel-showmanymoveone .carousel-inner > .prev {
      left: -16.666%;
      }
      .carousel-showmanymoveone .carousel-inner > .active.right,
      .carousel-showmanymoveone .carousel-inner > .next {
      left: 16.666%;
      }
      .carousel-showmanymoveone .carousel-inner > .left,
      .carousel-showmanymoveone .carousel-inner > .prev.right,
      .carousel-showmanymoveone .carousel-inner > .active {
      left: 0;
      }
      .carousel-showmanymoveone .carousel-inner .cloneditem-2,
      .carousel-showmanymoveone .carousel-inner .cloneditem-3,
      .carousel-showmanymoveone .carousel-inner .cloneditem-4,
      .carousel-showmanymoveone .carousel-inner .cloneditem-5,
      .carousel-showmanymoveone .carousel-inner .cloneditem-6  {
      display: block;
      }
      }
      @media all and (min-width: 992px) and (transform-3d), all and (min-width: 992px) and (-webkit-transform-3d) {
      .carousel-showmanymoveone .carousel-inner > .item.active.right,
      .carousel-showmanymoveone .carousel-inner > .item.next {
      -webkit-transform: translate3d(16.666%, 0, 0);
      transform: translate3d(16.666%, 0, 0);
      left: 0;
      }
      .carousel-showmanymoveone .carousel-inner > .item.active.left,
      .carousel-showmanymoveone .carousel-inner > .item.prev {
      -webkit-transform: translate3d(-16.666%, 0, 0);
      transform: translate3d(-16.666%, 0, 0);
      left: 0;
      }
      .carousel-showmanymoveone .carousel-inner > .item.left,
      .carousel-showmanymoveone .carousel-inner > .item.prev.right,
      .carousel-showmanymoveone .carousel-inner > .item.active {
      -webkit-transform: translate3d(0, 0, 0);
      transform: translate3d(0, 0, 0);
      left: 0;
      }
      }*/
      /*********************************/
      .footer1 {
          background: #ffffff;
          padding-top: 15px;
          padding-right: 0;
          padding-bottom: 15px;
          padding-left: 0;
          margin: 0px 10rem 0px 10rem;
              border-top: 3px solid #815cd5;
      }

      .footer{
         color : #815cd5;
      }

      .nomargin a{
         color: #815cd5;
         font-size: 15px;
      }
      .title-widget {
      color: #898989;
      font-size: 20px;
      font-weight: 300;
      line-height: 1;
      position: relative;
      text-transform: uppercase;
      font-family: 'Fjalla One', sans-serif;
      margin-top: 0;
      margin-right: 0;
      margin-bottom: 25px;
      margin-left: 0;
      padding-left: 28px;
      }
      .title-widget::before {
      background-color: #ea5644;
      content: "";
      height: 22px;
      left: 0px;
      position: absolute;
      top: -2px;
      width: 5px;
      }
      .widget_nav_menu ul {
      list-style: outside none none;
      padding-left: 0;
      }
      .widget_archive ul li {
      background-color: rgba(0, 0, 0, 0.3);
      content: "";
      height: 3px;
      left: 0;
      position: absolute;
      top: 7px;
      width: 3px;
      }
      .widget_nav_menu ul li {
      font-size: 15px;
       font-weight: 700;
       line-height: 20px;
       position: relative;
       /* text-transform: uppercase; */
       /* border-bottom: 1px solid rgba(0, 0, 0, 0.05); */
       margin-bottom: 7px;
       padding-bottom: 7px;
       width: 95%;
       letter-spacing: 1px;
      }


      .title-median {
      color: #636363;
      font-size: 20px;
      line-height: 20px;
      margin: 0 0 15px;
      text-transform: uppercase;
      font-family: 'Fjalla One', sans-serif;
      }
      .footerp p {
         font-family: 'Gudea', sans-serif;
         font-size: 14px;
      }
      #social:hover {
      -webkit-transform:scale(1.1);
      -moz-transform:scale(1.1);
      -o-transform:scale(1.1);
      }
      #social {
      -webkit-transform:scale(0.8);
      /* Browser Variations: */
      -moz-transform:scale(0.8);
      -o-transform:scale(0.8);
      -webkit-transition-duration: 0.5s;
      -moz-transition-duration: 0.5s;
      -o-transition-duration: 0.5s;
      }
      /*
      Only Needed in Multi-Coloured Variation
      */
      .social-fb:hover {
      color: #F6881F;
      }
      .social-tw:hover {
      color: #F6881F;
      }
      .social-gp:hover {
      color: #F6881F;
      }
      .social-em:hover {
      color: #F6881F;
      }
      .nomargin { margin:0px; padding:0px;}
      .footer-bottom {
      background-color: #525150;
      min-height: 30px;
      width: 100%;
      }
      .copyright {
      color: #fff;
      line-height: 30px;
      min-height: 30px;
      padding: 7px 0;
      }
      .design {
      color: #fff;
      line-height: 30px;
      min-height: 30px;
      padding: 7px 0;
      text-align: right;
      }
      .design a {
      color: #fff;
      }

      .widget-container ul li a {
         color: #815cd5;
      }

      .contactUsDiv{
         border-left: 3px solid #4a4747;
      }
   </style>
</head>
<!-- <footer style="background-color:#F6881F;margin-bottom: 62px;">
   <div class="container">
      <div  class="row" >
         <div class="container" style="position:relative;">
            <div class="pull-left header_icons">
               <ul style="padding-left:0px;">
                  <li><img src="img/1.png">INFO@HELLOUNI.ORG</li>
                  <li><img src="<?php echo base_url();?>img/2.png"> +91 8104909690</li>
               </ul>
            </div>
            <div class="pull-right hidden-xs header_icons">
               <ul>
                  <li><a href="<?php echo $config_facebook; ?>"><img src="<?php echo base_url();?>img/f.png"></a></li>
                  <li><a href="<?php echo $config_twitter; ?>"><img src="<?php echo base_url();?>img/t.png"></a></li>
                  <li><a href="<?php echo $config_gplus; ?>"><img src="<?php echo base_url();?>img/g.png"></a></li>
                  <li><a href="<?php echo $config_linkedin; ?>"><img src="<?php echo base_url();?>img/in.png"></a></li>
                  <li> <img src="<?php echo base_url();?>img/fav.png"><span style="font-size:11px; padding:0; margin:0;">FAVOURITES</span></li>
               </ul>
            </div>
            <div id="my_account"  class="col-md-2 my-account additional">
               <div class="col-lg-4 profile_img">
                  <?php if($this->session->userdata('user')['fid']){?>
                  <img src="http://graph.facebook.com/<?php echo $this->session->userdata('user')['fid'];?>/picture?type=normal" width="65" class="image_round" />
                  <?php }else{ ?>
                  <img src="<?php echo base_url();?>images/no_image.jpg" width="65" class="image_round" />
                  <?php } ?>
               </div>
               <div class="col-lg-7">
                  <div><a href="<?php echo base_url();?>user/account/update"><button class="btn edit_profile" >Edit Profile</button></a></div>
                  <div class="border_line"></div>
                  <div><button class="btn session" >Session</button></div>
               </div>
               </div>
         </div>
      </div>
      <div class="pull-left footer_menu">
         <ul style="margin-left: 0px;">

            <li>
               <a class="page-scroll" href="<?php echo base_url();?>about">About Us</a>
            </li>
            <li>
               <a class="page-scroll" href="<?php echo base_url();?>additional-services">Offers</a>
            </li>
            <li>
               <a class="page-scroll" href="<?php echo base_url();?>guidance">Guidance</a>
            </li>
         </ul>
      </div>
      <div class="pull-right footer_menu">COPYRIGHT 2018 &copy; HELLOUNI 2018</div>
   </div>
</footer> -->
<!--footer-->
<footer class="footer1">
   <div class="container-fluid">
      <div class="col-md-12">
         <!-- row -->
         <div class="col-lg-3 col-md-3">
            <!-- widgets column left -->
            <ul class="list-unstyled clear-margins">
               <!-- widgets -->
               <li class="widget-container widget_nav_menu">
                  <!-- widgets list -->
                 <!--  <h1 class="title-widget">Useful links</h1> -->
                  <ul>
                     <li><a  href="<?php echo base_url();?>about">About Us</a></li>
                     <li><a  href="<?php echo base_url();?>additional-services">Offers</a></li>
                     <li><a  href="<?php echo base_url();?>guidance">Guidance</a></li>
                  </ul>
               </li>
            </ul>
         </div>
         <!-- widgets column left end -->
         <div class="col-lg-3 col-md-3">
            <!-- widgets column left -->
            <ul class="list-unstyled clear-margins">
               <!-- widgets -->
               <li class="widget-container widget_nav_menu">
                  <!-- widgets list -->
                  <!-- <h1 class="title-widget">Useful links</h1> -->
                  <ul>
                    <?php if(!$this->session->userdata('user')){?>
                    <li><a  href="<?php echo base_url();?>university">University</a></li>
                  <?php } else { ?>
                    <li><a  href="<?php echo base_url();?>search/university">University</a></li>
                  <?php } ?>
                     <li><a href="<?php echo base_url();?>researches">Research Insights</a></li>
                     <li><a  href="<?php echo base_url();?>webinar/webinarmaster/webinarlist">Webinar</a></li>
                     <li><a  href="<?php echo base_url();?>friends-n-alumns">Friends & Alumnus</a></li>

                  </ul>
               </li>
            </ul>
         </div>
         <!-- widgets column left end -->
         <div class="col-lg-3 col-md-3">
            <!-- widgets column left -->
            <ul class="list-unstyled clear-margins">
               <!-- widgets -->
               <li class="widget-container widget_nav_menu">
                  <!-- widgets list -->
                  <!-- <h1 class="title-widget">Useful links</h1> -->
                  <ul>
                     <li><a  href="<?php echo base_url();?>join-a-group">Join a Group</a></li>
                     <li><a href="/privacy">Privacy Policy</a></li>
                  </ul>
               </li>
            </ul>
         </div>
         <!-- widgets column left end -->
         <div class="col-lg-3 col-md-3 contactUsDiv">
            <!-- widgets column center -->
                  <h4 style="color: #F6881F;margin: 0px 0 9px !important;">Contact Detail :</h4>
                  <div class="footerp" style="color: #4a4646e0;">

                     <p><b>Email id:</b> <a href="mailto:info@hellouni.org" style="color: #4a4646e0;">info@hellouni.org</a></p>
                     <p><b>Helpline Numbers </b>
                        <b style="color:#ffc106;"></b>  +91 8104909690
                     </p>
                     <!-- <p><b>Corp Office / Postal Address</b></p>
                     <p><b>Phone Numbers : </b>7042827160, </p>
                     <p> 011-27568832, 9868387223</p> -->
                  </div>
                  <div class="social-icons">
                     <ul class="nomargin">
                        <a href="https://www.facebook.com/HelloUni-847774062258005/"><i class="fa fa-facebook-square fa-3x social-fb" id="social"></i></a>
                        <a href="https://twitter.com/HelloUni1"><i class="fa fa-twitter-square fa-3x social-tw" id="social"></i></a>
                        <a href="https://www.linkedin.com/company/hellouni/"><i class="fa fa-linkedin-square fa-3x social-em" id="social"></i></a>
                        <a href="#"><i class="fa fa-google-plus-square fa-3x social-gp" id="social"></i></a>
                     </ul>
                  </div>

         </div>
      </div>
   </div>
</footer>
<!--header-->
<div class="footer-bottom">
   <div class="container">
      <div class="row">
         <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="copyright">
               <b>© 2018, HELLOUNI, All rights reserved</b>
               <!-- COPYRIGHT 2018 &copy; HELLOUNI 2018 -->
            </div>
         </div>
         <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="design">
               <a href="https://www.hellouni.org"><b>Hellouni</b> </a> |  <a target="_blank" href="https://www.hellouni.org">MAKE RIGHT CHOICE !</a>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- <div class="footer-strip">
   <div class="site-wrapper" style="text-align:center;">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-2" style="text-align:center;margin-top: 5px;margin-bottom: 5px;background-color: #F6881F;">
                  <h4 style="color: white;"><b>Top Universities</b></h4>
            </div>
            <div class="col-xs-10 col-sm-10 col-md-10" style="box-shadow: 0px 0px 4px 0px;     padding: 2px;">
               <div class="carousel carousel-showmanymoveone slide" id="itemslider">
                  <div class="carousel-inner" style="max-height: 62px !important;margin-top: 3px;    margin-bottom: 2px;    ">
                      <?php
                      if(isset($preview_university))
                      {
                          ?>
                          <div class="item active">
                             <div class="col-xs-12 col-sm-6 col-md-2">
                                <a href="/<?=$preview_university['slug']?>"><img src="<?=$preview_university['logo']?>" class="img-responsive center-block" <?=$preview_university['slider_css']?> ></a>
                             </div>
                          </div>
                          <?php
                      }
                      foreach($top_universities as $index => $topUniversity)
                      {
                          if(isset($preview_university))
                          {
                              if($index < 5)
                              {
                                  ?>
                                  <div class="item">
                                     <div class="col-xs-12 col-sm-6 col-md-2">
                                        <a href="/<?=$topUniversity['slug']?>"><img src="<?=$topUniversity['logo']?>" class="img-responsive center-block" <?=$topUniversity['slider_css']?> ></a>
                                     </div>
                                  </div>
                                  <?php
                              }
                          }
                          else
                          {
                              ?>
                              <div <?=!$index ? 'class="item active"' : 'class="item"'?>>
                                 <div class="col-xs-12 col-sm-6 col-md-2">
                                    <a href="/<?=$topUniversity['slug']?>"><img src="<?=$topUniversity['logo']?>" class="img-responsive center-block" <?=$topUniversity['slider_css']?> ></a>
                                 </div>
                              </div>
                              <?php
                          }
                      }
                      ?>
                  </div>

               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="myModal2222" class="reveal-modal2222">
   <!--<h1>Reveal Modal Goodness</h1>
      <p>This is a default modal in all its glory, but any of the styles here can easily be changed in the CSS.</p>

      <a class="close-reveal-modal">&#215;</a>-->
</div>
<!-- Bootstrap Core JavaScript -->
<script type="text/javascript">
   jQuery(document).ready(function() {

    // jQuery('#itemslider').carousel({ interval: 3000, cycle: true });

    //      jQuery('.carousel-showmanymoveone .item').each(function(){
    //      var itemToClone = jQuery(this);

    //      for (var i=1;i<6;i++) {
    //      itemToClone = itemToClone.next();

    //      if (!itemToClone.length) {
    //      itemToClone = jQuery(this).siblings(':first');
    //      }

    //      itemToClone.children(':first-child').clone()
    //      .addClass("cloneditem-"+(i))
    //      .appendTo(jQuery(this));
    //      }
    //    });


    jQuery(".fancybox-opened").fancybox({'width':400,

                           'height':300,

                           'autoSize' : false});

      //var countries = 'in,us,id';

      var countries = jQuery('#_countries').val();



      var res = countries.split(",");

      for (i = 0; i < res.length; i++) {

         //alert(res[i]);

      }



      jQuery('#vmap').vectorMap({

          map: 'world_en',

          backgroundColor: '#fff',

          color: '#cccccc',

         hoverColor: '#F6881F',

            selectedColor: '#F6881F',

          hoverOpacity: 0.6,



          enableZoom: true,

         showTooltip: true,



         // values: sample_data,

          scaleColors: ['#cccccc', '#d5d5d5'],

          normalizeFunction: 'polynomial',

         //normalizeFunction: 'linear',

         multiSelectRegion: true,

            onRegionClick: function (event, code, region) {

            // if it's not in the approved list, do nothing, else handle it

            if (countries.toLowerCase().indexOf(code) <= -1) {

            event.preventDefault();

            }

            else {

            //handle state click

               }

            }





         /*onRegionOver: function(event,code, region)

    {

      alert(code);

    }*/

         /*map: 'world_en',



            enableZoom: true,

            showTooltip: true,

            multiSelectRegion: true,

            selectedRegions: ['FR']*/

       /* map: 'world_en',

        backgroundColor: '#fff',

          color: '#ffffff',

           //hoverColor: '#F6881F',

             selectedColor: '#F6881F',

           values: sample_data,

          scaleColors: ['#cccccc', '#d5d5d5'],

            enableZoom: true,

            showTooltip: true,

        values: sample_data,

            multiSelectRegion: true,

            selectedRegions: ['IN']*/



      });

      //jQuery('#vmap').vectorMap('set', 'colors', {us: '#0000ff'});









   jQuery('.stream').click(function(){

      //alert(jQuery(this).attr("id"));

      var id = jQuery(this).attr("id");

      if(jQuery("input[id=chk-"+id+"]").is(":checked"))

            { jQuery("input[id=chk-"+id+"]").prop("checked", false);

              jQuery(this).addClass("grayscale");



        }else{

            jQuery("input[id=chk-"+id+"]").prop("checked", true);

         jQuery(this).removeClass("grayscale");

        }



   });







   jQuery('.stream').click(function(){

   //alert(this.id);

      //$('#myModal').html("AA");

      var val = [];

          /*jQuery(':checkbox:checked').each(function(i){

            val[i] = $(this).val();

          });



      //var dataString = 'email='+email;

      var jsonString = 'email='+JSON.stringify(val);*/

         $.ajax({



         type: "POST",



         /*url: "<?php echo base_url();?>elements/menu/test",*/



         url: "<?php echo base_url();?>course/course/getSubcoursesList",



         //data: jsonString,



         cache: false,



         success: function(result){



          //$('#myModal').html(result);

          // $('#myModal').show("AA");

          //alert(result);





             // $('#newserrormessage').text(result);





         }



            });



   });







   jQuery("#my_accountinfo").hover(function(){

          $("#my_account").show();

       $(".additional").show();



          }, function(){

         //$("#my_account").hide();

      });





   jQuery(".additional").hover(function(){

          $(this).show();



          }, function(){

         $(this).hide();

      });









   });

   /*function page_loader(){

      //alert($('page_loader.php').html())

      alert(optionVal.length);

      //$('#page_loader').load('page_loader.php');

   }

   jQuery('#vmap path').click(function(){

      alert(jQuery(this).attr("id"));

   });*/

   function next_page(e){

      //var chk=jQuery("input[name=check_list]");



      //alert("ABC");

      //e.preventDefault();

      var array = [];

      jQuery('select :selected').each(function(i,value){

          array[i] = jQuery(this).val();

      alert(array[i]);

      });

      //here make your ajax call to a php file

      jQuery.ajax({

          type: "POST",

          url: "<?php echo base_url();?>page_loader.php",

          data: { selected_values: array, otherVal: 'something else' },

      success: function() {

               $('#page_loader').load('page_loader.php?country='+encodeURI(array));

              }

      });

   }

</script>
<script type="text/javascript" src="<?php echo base_url();?>src/DateTimePicker.js"></script>
<script src="<?=base_url()?>/plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
   $(function(){

   $("#dtBox").DateTimePicker(

               {

                  dateFormat: "dd-MMM-yyyy"

               });

    $('#state').change(function(){

         //alert($(this).val());

         $('#city').load('city_loader.php?state='+encodeURI($(this).val()));

      });

   });





   //Degree related



   $(".drg").on('click', function(event) {

      $(".grayscale").removeClass("nograyscale");

      $("#div"+this.id).addClass("nograyscale");

   });







   $("#lead_submit").on('click', function(event) {









   var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;





      if($("#name").val()==''){

         $("#msg").text('Name is required');

         $('#name').focus();

         event.preventDefault();

         return false;



      }else if(isNaN($("#name").val())==false){

         $("#msg").text('Name Should be valid');

         $('#name').focus();

         event.preventDefault();

         return false;



      }else if($("#mobile").val()==''){

         $("#msg").text('Mobile is required');

         $('#mobile').focus();

         event.preventDefault();

         return false;



      }else if(isNaN($("#mobile").val())==true){

         $("#msg").text('Mobile should be valid');

         $('#mobile').focus();

         event.preventDefault();

         return false;



      }else if($("#mobile").val().length!=10){

         $("#msg").text('Mobile should be at least 10 digit');

         $('#mobile').focus();

         event.preventDefault();

         return false;



      }else if($("#email").val()==''){

         $("#msg").text('Email is required');

         $('#email').focus();

         event.preventDefault();

         return false;

      }else if(!emailReg.test($("#email").val())){

         $("#msg").text('Email Should be valid');

         $('#email').focus();

         event.preventDefault();

         return false;

      }else{



            var dataString = 'email='+$('#email').val();

            event.preventDefault();



            $.ajax({



            type: "POST",



            url: "<?php echo base_url();?>user/account/checkemail",



            data: dataString,



            cache: false,



            success: function(result){

             if(result==1){

               $("#msg").text('Email is already exists.');

               $('#email').focus(); return false;



             }else{

               $("#msg").text('');

               $('#input_container').submit(); return true;

             }





            }



           });







      }







   });





   $("#my_accountinfo").mouseover(function(){

            $("#my_account").show();

      $(".additional").show();

   });



   $("#my_account").mouseover(function(){

            $("#my_account").show();

      $(".additional").show();

   });



   $("#my_account").mouseout(function(){

      $("#my_account").hide();

   });



   $("#my_accountinfo").mouseout(function(){

      $("#my_account").hide();

   });

   function showDepartments(collegeId){
       $.ajax({
           type: "GET",
           url: "/admin/university/internship/departments?cid=" + collegeId,
           success: function(result){
               $('#departments_list').html(result);
           }
       });
   }

   function showCourses(departmentId){
       $.ajax({
           type: "GET",
           url: "/admin/university/internship/courses?did=" + departmentId,
           success: function(result){
               $('#courses_list').html(result);
           }
       });
   }

</script>
<script>
   var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
   if (isMobile) {
     //alert(navigator.userAgent);
     //hide footer when input box is on focus
   $(document).on('focus', 'input, textarea, select', function() {
       //$("div.copyright").hide();
   $('div.copyright').css('margin-bottom', '0px' );
      $('div.footer-strip').css('position', 'relative' );
   });

   //show footer when input is NOT on focus
   $(document).on('blur', 'input, textarea, select', function() {
       //$("div.copyright").show();
   $('div.copyright').css('margin-bottom', '60px' );
      $('div.footer-strip').css('position', 'fixed' );
   });

   }

   $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd',  minDate:new Date() });
   $( "#datepicker_1" ).datepicker({ dateFormat: 'yy-mm-dd',  minDate:new Date() });
   $( "#datepicker_2" ).datepicker({ dateFormat: 'yy-mm-dd',  minDate:new Date() });
   $( "#datepicker_3" ).datepicker({ dateFormat: 'yy-mm-dd',  minDate:new Date() });


</script>



</body>
</html>
