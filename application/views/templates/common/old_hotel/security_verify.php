<!DOCTYPE html>

<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->

<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->

<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->

<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->

<head>

<meta charset="utf-8">



<!-- Viewport Metatag -->

<meta name="viewport" content="width=device-width,initial-scale=1.0">



<!-- Required Stylesheets -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css" media="screen">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/fonts/ptsans/stylesheet.css" media="screen">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/fonts/icomoon/style.css" media="screen">



<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/login.css" media="screen">



<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/mws-theme.css" media="screen">



<title>MWS Admin - Security Verification</title>



</head> 



<body>



    <div id="mws-login-wrapper">

        <div id="mws-login">

            <h1>Security Verification</h1>
 
  <?php 
if($msg = $this->session->flashdata('flash_message'))
{?> 
<div id="horerror">
<strong style="color:#FFFFFF;"> <?php echo $msg;?></strong><br />
</div>
<?php } 
?>
  
  
  
  

            <div class="mws-login-lock"><i class="icon-lock"></i></div>

            <div id="mws-login-form">

                <form class="mws-form" action="<?php echo base_url();?>common/login/verification/<?php echo $this->uri->segment('4');?>" method="post">

                    <div class="mws-form-row">

                        <div class="mws-form-item">

                            <span class="question"><?php if(isset($security_question1)){echo $security_question1; }?></span>

                        </div>

                    </div>
					
					

                    <div class="mws-form-row">

                        <div class="mws-form-item">

                            <input type="password" name="security_ans1" class="mws-login-password required" placeholder="Security Answere" value="">

                        </div>

                    </div>
					
					
					<div class="mws-form-row">

                        <div class="mws-form-item">

                             <span class="question"><?php if(isset($security_question2)){echo $security_question2; }?></span>
                        </div>

                    </div>
					
					<div class="mws-form-row">

                        <div class="mws-form-item">

                            <input type="password" name="security_ans2" class="mws-login-password required" placeholder="Security Answere" value="">

                        </div>

                    </div>

                    
                    <div class="mws-form-row">
                        <input type="hidden" name="currenturl" value="<?php echo current_url();?>"/>
                        <input type="submit" value="Login" class="btn btn-success mws-login-button">

                    </div>

                </form>

            </div>
			

        </div>
		

    </div>
	
	



    <!-- JavaScript Plugins -->

    <script src="<?php echo base_url(); ?>js/libs/jquery-1.8.3.min.js"></script>

    <script src="<?php echo base_url(); ?>js/libs/jquery.placeholder.min.js"></script>

    <script src="<?php echo base_url(); ?>custom-plugins/fileinput.js"></script>

    

    <!-- jQuery-UI Dependent Scripts -->

    <script src="<?php echo base_url(); ?>jui/js/jquery-ui-effects.min.js"></script>



    <!-- Plugin Scripts -->

    <script src="<?php echo base_url(); ?>plugins/validate/jquery.validate-min.js"></script>



    <!-- Login Script -->

    <script src="<?php echo base_url(); ?>js/core/login.js"></script>

 

</body>

</html>

