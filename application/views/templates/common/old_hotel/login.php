<!DOCTYPE html>

<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->

<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->

<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->

<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->

<head>

<meta charset="utf-8">



<!-- Viewport Metatag -->

<meta name="viewport" content="width=device-width,initial-scale=1.0">



<!-- Required Stylesheets -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css" media="screen">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/fonts/ptsans/stylesheet.css" media="screen">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/fonts/icomoon/style.css" media="screen">



<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/login.css" media="screen">



<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/mws-theme.css" media="screen">



<title>MWS Admin - Login Page</title>



</head> 



<body>



    <div id="mws-login-wrapper">

        <div id="mws-login">

            <h1>Login</h1>
 
  <?php 
if($msg = $this->session->flashdata('flash_message'))
{?> 
<div id="horerror">
<strong style="color:#FFFFFF;"> <?php echo $msg;?></strong><br />
</div>
<?php } 
?>

<?php if(validation_errors()){?> 
               <div id="horerror">
<strong style="color:#FFFFFF;"><?php echo validation_errors(); ?></strong><br />
</div>
<?php } ?>
  
  
  
  

            <div class="mws-login-lock"><i class="icon-lock"></i></div>

            <div id="mws-login-form">

                <form class="mws-form" action="<?php echo base_url();?>common/login/log_in" method="post">

                    <div class="mws-form-row">

                        <div class="mws-form-item">

                            <input type="text" name="username" class="mws-login-username required" placeholder="username" value="<?php if(!empty($memeber_name)){
							 echo $memeber_name; }?>">

                        </div>

                    </div>

                    <div class="mws-form-row">

                        <div class="mws-form-item">

                            <input type="password" name="password" class="mws-login-password required" placeholder="password" value="<?php if(!empty($memeber_pwd)){
							 echo $memeber_pwd; }?>">

                        </div>

                    </div>

                    <div id="mws-login-remember" class="mws-form-row mws-inset">

                        <ul class="mws-form-list inline">

                            <li>

                                <input id="remember" name="remember" type="checkbox"> 

                                <label for="remember">Remember me</label>

                            </li>

                        </ul>

                    </div>

                    <div class="mws-form-row">

                        <input type="submit" value="Login" class="btn btn-success mws-login-button">

                    </div>

                </form>

            </div>
			

        </div>
		
		
		<div id="mws-forgetpsw">

            <h1>Forget Credencial</h1>
 
  <?php 
if($msg = $this->session->flashdata('flash_message'))
{?> 
<div id="horerror">
<strong style="color:#FFFFFF;"> <?php echo $msg;?></strong><br />
</div>
<?php } 
?>
  
  		
           <div class="mws-login-lock"><i class="icon-key-2"></i></div>
            <div id="mws-forgetpsw-form">

                <form class="mws-form" action="<?php echo base_url();?>common/login/forgot" method="post">

                    
					<div class="mws-form-row">

                        <div class="mws-form-item">

                            <span class="question">User Name</span>
							<input type="radio" name="credencial" class="mws-login-username required" placeholder="Enter Email" value="username">
							
							<span class="question">Password</span>
							<input type="radio" name="credencial" class="mws-login-username required" placeholder="Enter Email" value="password">


                        </div>

                    </div>
					
					<div class="mws-form-row">

                        <div class="mws-form-item">

                            <input type="text" name="email" class="mws-login-username required" placeholder="Enter Email" value="">

                        </div>

                    </div>

                    <div class="mws-form-row">

                        <input type="submit" value="Login" class="btn btn-success mws-login-button">

                    </div>

                </form>

            </div>

        </div>
		
          <div id="loginbar"><a href="javascript:void(0)" style="float:left;" id="forgetpsw" title="Password Lost and Found">Forget password?</a>
		  <a href="" style="float:right;" title="">Back to Site</a>
		  </div>
		  
		  
          <div id="forgetbar"><a href="javascript:void(0)" style="float:left;" id="backtologin" title="Password Lost and Found">Back to login</a>
		  <a href="" style="float:right;" title="">Back to Site</a>
		  </div>

    </div>
	
	



    <!-- JavaScript Plugins -->

    <script src="<?php echo base_url(); ?>js/libs/jquery-1.8.3.min.js"></script>

    <script src="<?php echo base_url(); ?>js/libs/jquery.placeholder.min.js"></script>

    <script src="<?php echo base_url(); ?>custom-plugins/fileinput.js"></script>

    

    <!-- jQuery-UI Dependent Scripts -->

    <script src="<?php echo base_url(); ?>jui/js/jquery-ui-effects.min.js"></script>



    <!-- Plugin Scripts -->

    <script src="<?php echo base_url(); ?>plugins/validate/jquery.validate-min.js"></script>



    <!-- Login Script -->

    <script src="<?php echo base_url(); ?>js/core/login.js"></script>

<script type="text/javascript">
$(document).ready(function()
{
$("#forgetpsw").live('click',function()
{
//alert('Hello forget !!!');

$("#mws-login").slideUp();
$("#mws-forgetpsw").slideDown(); 
$("#loginbar").hide();
$("#forgetbar").show();
$("#horerror").hide();
});


$("#backtologin").live('click',function()
{
//alert('Hello back !!!');


$("#mws-login").slideDown();
$("#mws-forgetpsw").slideUp(); 
$("#loginbar").show();
$("#forgetbar").hide();
$("#horerror").hide();
});

});




 </script> 

</body>

</html>

