<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
<style>
   div {
       background: 0 0;
       border: 0;
       margin: 0;
       outline: 0;
       padding: 0;
       vertical-align: baseline;
   }
   .bannerSection{
       background-color: rgba(0, 0, 0, 0);
       background-repeat: no-repeat;
       background-image: url(<?php echo base_url();?>application/images/topBannerVF.png);
       background-size: cover;
       background-position: center center;
       width: 100%;
   }
   .sectionClass {
       padding: 20px 0px 50px 0px;
       position: relative;
       display: block;
   }
   .projectFactsWrap{
       display: flex;
       margin-top: 30px;
       flex-direction: row;
       flex-wrap: wrap;
   }
   .projectFactsWrap .item{
       width: 20%;
       height: 100%;
       padding: 25px 5px;
       text-align: center;
       border-radius: 25%;
       margin: 20px;
       margin-top: -150px;
   }
   .projectFactsWrap .item:nth-child(1){
       background: rgb(246 136 44);
   }
   .projectFactsWrap .item:nth-child(2){
       background: rgb(246 136 44);
   }
   .projectFactsWrap .item:nth-child(3){
       background: rgb(246 136 44);
   }
   .projectFactsWrap .item:nth-child(4){
       background: rgb(246 136 44);
   }
   .projectFactsWrap .item p.number{
       font-size: 25px;
       padding: 0;
       font-weight: bold;
   }
   .projectFactsWrap .item p{
       color: black;
       font-size: 16px;
       margin: 0;
       padding: 10px;
       font-family: 'Open Sans';
       font-weight: 600;
   }
   .projectFactsWrap .item span{
       width: 60px;
       background: rgba(255, 255, 255, 0.8);
       height: 2px;
       display: block;
       margin: 0 auto;
   }
   .projectFactsWrap .item i{
       vertical-align: middle;
       font-size: 50px;
       color: rgba(255, 255, 255, 0.8);
   }
   .projectFactsWrap .item:hover i, .projectFactsWrap .item:hover p{
       color: white;
   }
   .projectFactsWrap .item:hover span{
       background: white;
   }
   @media (max-width: 786px){
       .projectFactsWrap .item {
           flex: 0 0 50%;
       }
   }
   h1:after{
       content: "";
       position: absolute;
       left: 40%;
       margin-left: 0;
       bottom: 0;
       width: 20%;
       border-bottom: 4px solid white;
   }
   h1:before {
       content: "";
       position: absolute;
       left: 40%;
       margin-left: 0;
       top: 0;
       width: 20%;
       border-bottom: 4px solid white;
   }
   .inset {
       padding: 15px;
    }
   @media screen and (max-width: 992px){
       .inset {
           padding: 15px;
        }
   }
   .team-style-five .team-items .item {
       -moz-box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
       -webkit-box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
       -o-box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
       box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
       padding-top: 0px;
       background-color: #62367d;
       margin: 10px;
       height: 115px;
       border-radius: 10px;
   }
   .team-style-five .single-item.team-standard .thumb {
       overflow: hidden;
   }
   .team-style-five .team-items .item .thumb img{
       width: 100%;
       height: 28%;
   }
   .team-style-five .team-items .item .thumb {
       position: relative;
       z-index: 1;
       height: 115px;
   }
   .team-style-five .team-items .item .info {
       background: #ffffff none repeat scroll 0 0;
       padding: 20px 20px;
   }
   .team-style-five .team-items .item .info h3 {
       font-weight: 700;
       margin-bottom: 5px;
       font-size: 18px;
       font-family:"montserrat", sans-serif;
       text-transform: uppercase;
   }
   .team-style-five .single-item.team-standard .item .info span {
       font-size: 13px;
       color: #1cb9c8;
   }
   .team-style-five .single-item.team-standard .item .info .spanName::after {
       background: #1cb9c8 none repeat scroll 0 0;
       bottom: 0;
       content: "";
       height: 2px;
       left: 50%;
       margin-left: -20px;
       position: absolute;
       width: 40px;
   }
   .team-style-five .team-items .item .info p {
       margin-bottom: 0;
       color: #666666;
       text-align: left;
       font-size: 14px;
   }
   @import url("https://fonts.googleapis.com/css?family=Lato:400,400i,700");
   .joinbtn{
       background: #f6872c !important;
       border-color: #f6872c !important;
       border-radius: 5px;
   }
   @media only screen and (min-width: 1700px) {
       .projectFactsWrap .item{
           width: 17%;
           margin-left: 6%;
       }
   }

   .pulse {
    display: block;
    border-radius: 8px;
    background: #f6882c;
    cursor: pointer;
    transform: scale(1);
    box-shadow: 0 0 0 rgb(246 136 44);
    animation: pulse 1.5s infinite;
}
    .pulse:hover {
      animation: none;
    }

    @-webkit-keyframes pulse {
      0% {
        transform: scale(0.95);
        -webkit-box-shadow: 0 0 0 0 rgba(98,54,125, 0.4);
      }
      70% {
        transform: scale(1);
          -webkit-box-shadow: 0 0 0 10px rgba(98,54,125, 1);
      }
      100% {
        transform: scale(0.95);
          -webkit-box-shadow: 0 0 0 0 rgba(98,54,125, 0);
      }
    }
    @keyframes pulse {
      0% {
        transform: scale(0.95);
        -moz-box-shadow: 0 0 0 0 rgba(98,54,125, 0.4);
        box-shadow: 0 0 0 0 rgba(98,54,125, 0.4);
      }
      70% {
        transform: scale(1);
          -moz-box-shadow: 0 0 0 10px rgba(98,54,125, 0);
          box-shadow: 0 0 0 10px rgba(98,54,125, 0);
      }
      100% {
        transform: scale(0.95);
          -moz-box-shadow: 0 0 0 0 rgba(98,54,125, 0);
          box-shadow: 0 0 0 0 rgba(98,54,125, 0);
      }
    }

    .modal{
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        z-index: 112;
    }

    .modal-content{
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 50%;
    }
</style>

<section class="bannerSection">
    <div class="container" style="height: 400px;">
        <div class="row">
            <div class="col-md-12" style="margin-top: 80px;">
                <h1 class="page-top-title" style="color: #ffffff; text-align:center;font-weight:bold;font-size:45px;margin-top: 25px;">IMPERIAL VISA</h1>
                <h1 class="page-top-title" style="color: #ffffff; text-align:center;font-weight:bold;font-size:45px;margin-bottom: 25px;">SEMINAR 2021</h1>
            </div>
        </div>
    </div>
</section>

<div class="content-wrapper">
    <section>
        <div class="col-md-12">
            <div class="col-md-12 col-lg-12 col-sm-12 hidden-xs">
                <div id="projectFacts" class="sectionClass">
                    <div class="projectFactsWrap " style="margin-left: 50px">
                        <div class="item wow fadeInUpBig animated animated " data-number="200" style="visibility: visible;">
                            <img src="<?php echo base_url();?>application/images/icons/1.png" width="20%">
                            <p id="total_registrations" class="number"></p>
                            <span></span>
                            <p>Total Registrations</p>
                        </div>
                        <div class="item wow fadeInUpBig animated animated" data-number="55" style="visibility: visible;">
                            <img src="<?php echo base_url();?>application/images/icons/2.png" width="35%">
                            <p id="active_participants" class="number"></p>
                            <span></span>
                            <p>Active Participants</p>
                        </div>
                        <div class="item wow fadeInUpBig animated animated" data-number="359" style="visibility: visible;">
                            <img src="<?php echo base_url();?>application/images/icons/3.png" width="20%">
                            <p id="active_rooms" class="number">10</p>
                            <span></span>
                            <p>Active Rooms</p>
                        </div>
                        <div class="item wow fadeInUpBig animated animated" data-number="246" style="visibility: visible;">
                            <img src="<?php echo base_url();?>application/images/icons/4.png" width="20%">
                            <p id="session_attended" class="number"></p>
                            <span></span>
                            <p>Sessions Attended</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container-fluid">
            <div class="row scrollSpyArea" >
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="page-content inset" data-spy="scroll" data-target="#spy" id="imperial-counselling-box">
                        <div class="row team-style-five" id="imperial-counselling">
                            <div class="jumbotron text-center row " style="margin-bottom: 15px;">
                                <div>
                                    <div class="col-md-12 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> Webinar Session</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinWebinar()" id="webinar-button">Join</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="page-content inset" data-spy="scroll" data-target="#spy" id="imperial-counselling-box">
                        <div class="row team-style-five" id="imperial-counselling">
                            <div class="jumbotron text-center row " style="margin-bottom: 15px;">
                                <h2 style="font-size: 30px">Counselling Rooms</h2>
                                <div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> USA</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('usa')" id="usa-button">Join</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> Canada</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('canada')" id="canada-button">Join</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> European Union</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('europe')" id="australia-button">Join</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> Australia/NZ/UK/Ireland</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('australia')" id="australia-button">Join</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="page-content inset" data-spy="scroll" data-target="#spy" id="imperial-counselling-box">
                        <div class="row team-style-five" id="imperial-counselling">
                            <div class="jumbotron text-center row " style="margin-bottom: 15px;">
                                <h2 style="font-size: 30px">Educaton Loan Counselling Rooms</h2>
                                <div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> Thane</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('thane')" id="thane-button">Join</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> Borivali</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('borivalli')" id="borivalli-button">Join</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> Santacruz</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('santacruz')" id="santacruz-button">Join</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> Pune</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('pune')" id="pune-button">Join</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="page-content inset" data-spy="scroll" data-target="#spy" id="imperial-counselling-box">
                        <div class="row team-style-five" id="imperial-counselling">
                            <div class="jumbotron text-center row " style="margin-bottom: 15px;">
                                <h2 style="font-size: 30px">Other Rooms</h2>
                                <div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> Axis</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('axis')" id="axis-button">Join</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div>
   <div class="col-md-4 col-sm-12 team-items pulse">
      <div class="single-item text-center team-standard team-style-block">
         <div class="item" style="background-color: #f6882c;box-shadow: 0px 0px 0px 0 rgba(0, 0, 0, 0.06);">
            <div class="thumb">
               <h2 style="color: white;"> FRR</h2>
               <div class="col-md-12">
                  <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('frr')" id="frr-button" style="background: #62367d !important;border-color: #62367d !important;">Join</button>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

                                <!--div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> Unimoney</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('unimoney')">Join</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div-->

                                <div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> Thomas Cook</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('thomos_cook')" id="thomas-cook-button">Join</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> Unimoni</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('unimoni')" id="unimoni-button">Join</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div id="modal" class="modal" >
    <!-- Modal content -->
    <div class="row modal-content">
        <div id="quiz">
            <div style="font-size: 16px; text-align:center;">You should join at least one room from each section. Would you like to join? Press <b>Yes</b> to remain on same page or press <b>Exit</b> to exit the VISA Seminar.</div>
            <button id="exit-button" class="btn btn-md btn-primary" style="float:left;" onclick="exit()">Exit</button>
            <button id="yes-button" class="btn btn-md btn-primary" style="float:right;" onclick="hideModal()">Yes</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    function joinRoom(type){
        let isWebinarDone = get_cookie('webinar_completed');
        if(!parseInt(isWebinarDone)){
            alert("Rooms will open after webinar session is completed");
            return;
        }
        $.ajax({
            url: '/meeting/visaseminar/join_tid/' + type,
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                window.location.href = "/meeting/visaseminar/student/o/" + data + "/<?=$username?>/<?=$linkCount?>";
                /*var href = "/meeting/visaseminar/student/o/" + data + '/<?=$username?>';
                var win = window.open(href, 'visaseminar');
                win.focus();*/
            },
            error: function error() {
            }
        });
    }

    var confirmBoxClicked = 0;

    setTimeout(function(){
        document.addEventListener("mousemove", function(e) {
            if(parseInt(e.clientY) < 10 && !confirmBoxClicked){
                var counselling_room = get_cookie('counselling_room');
                var eduloan_counselling_room = get_cookie('eduloan_counselling_room');
                var other_room = get_cookie('other_room');

                if(!parseInt(counselling_room) || !parseInt(eduloan_counselling_room) || !parseInt(other_room)){
                    confirmBoxClicked = 1;
                    var modal = document.getElementById("modal");
                    modal.style.display = "block";
                    /*var r = confirm("You should join at least one room from each section. Would you like to join? If Yes, press OK else press CANCEL to exit the session.");
                    if (r == true) {
                        //console.log("Yes");
                    } else {
                        window.location.href = '/meeting/visaseminar/thankyou';
                    }*/
                }
            }
        });
    }, 10000);

    function hideModal(){
        var modal = document.getElementById("modal");
        modal.style.display = "none";
    }

    function exit(){
        window.location.href = '/meeting/visaseminar/thankyou';
    }

    function get_cookie(cname){
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++){
            var c = ca[i];
            while (c.charAt(0) == ' '){
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0){
                if(cname == 'cid'){
                    campaign_set = 1;
                }
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    $.ajax({
        url: '/meeting/visaseminar/get_stats',
        type: 'GET',
        complete: function complete() {
        },
        success: function success(data) {
            data = JSON.parse(data);
            document.getElementById("total_registrations").innerHTML = data.total_registrations;
            document.getElementById("active_participants").innerHTML = data.active_participants;
            document.getElementById("active_rooms").innerHTML = data.active_rooms;
            document.getElementById("session_attended").innerHTML = data.session_attended;
        },
        error: function error() {
        }
    });

    var stats = setInterval(function() {
        $.ajax({
            url: '/meeting/visaseminar/get_stats',
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                data = JSON.parse(data);
                document.getElementById("total_registrations").innerHTML = data.total_registrations;
                document.getElementById("active_participants").innerHTML = data.active_participants;
                document.getElementById("active_rooms").innerHTML = data.active_rooms;
                document.getElementById("session_attended").innerHTML = data.session_attended;
            },
            error: function error() {
            }
        });
    }, 5000);

    var webinar = setInterval(function() {
        let isWebinarDone = get_cookie('webinar_completed');
        if(!parseInt(isWebinarDone)){
            $.ajax({
                url: '/meeting/visaseminar/check_webinar',
                type: 'GET',
                complete: function complete() {
                },
                success: function success(data) {
                },
                error: function error() {
                }
            });
        }
    }, 5000);

    function joinWebinar(){
        //alert("The session has not been started yet. Please come back after some time.");
        //window.location.href = '/';
        let isWebinarDone = get_cookie('webinar_completed');
        //if(!parseInt(isWebinarDone)){
            $.ajax({
                url: '/meeting/visaseminar/get_webinar_link/<?=$username?>/<?=$linkCount?>',
                type: 'GET',
                complete: function complete() {
                },
                success: function success(data) {
                    window.location.href = data;
                },
                error: function error() {
                }
            });

            //window.location.href = "/meeting/visaseminar/student/w/MTU3/<?=$username?>/<?=$linkCount?>";
        //}
        //else{
            //alert("Sorry!! Webinar session has been ended. Please join any room for assistance.");
        //}
    }
</script>
