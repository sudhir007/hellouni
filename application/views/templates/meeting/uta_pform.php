<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>HelloUni UTA Seminar | Panelist Form</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="https://hellouni.org/admin/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://hellouni.org/admin/bootstrap/css/bootstrap.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="https://hellouni.org/admin/dist/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="https://hellouni.org/admin/plugins/iCheck/square/blue.css">
        <style type="text/css">
            .error{color:red;}
            .login-box-body{height: auto;}
            .login-box{width: fit-content;}
            #have_i20, #filling_visa{
                display: none;
            }
        </style>
        <link rel="icon" type="image/png" sizes="96x96" href="https://hellouni.org/admin/images/favicon-96x96.png">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <img src="https://hellouni.org/admin/images/HelloUni-Logo.png" style="width: 75%;">
            </div><!-- /.login-logo -->
            <div class="login-box-body">
                <?php
                if(isset($error))
                {
                    ?>
                    <p class="login-box-msg" style="font-size: 20px; color: red;"><?=$error?></p>
                    <?php
                }
                ?>
                <form action="/meeting/uta/pform/" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" name="first_name" id="first_name" class="form-control" placeholder="Enter Your First Name" required>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Enter Your Last Name" required>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="email" name="email" id="email" class="form-control" placeholder="Enter Your Email" required>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <input type="hidden" name="session_id" id="session_id" class="form-control" value="<?=$session_id?>">
                            <input type="hidden" name="token_id" id="token_id" class="form-control" value="<?=$token_id?>">
                            <button type="submit" id="signin" class="btn btn-primary btn-block btn-flat">Start</button>
                        </div><!-- /.col -->
                    </div>
                </form>
            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->
    </body>
</html>
