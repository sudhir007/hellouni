<html>
    <head>
        <title> Hellouni : Search | Apply | Connect </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css">
        <link href="<?=base_url()?>css/attendee_webinar.css?v=1.9" rel="stylesheet">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://static.opentok.com/v2/js/opentok.min.js"></script>

        <style type="text/css">
            body, html {
                background-color: #4a265a;
                background: url("<?php echo base_url() . $backgroundImage;?>");
                background-position: center center;
                background-size: contain;
                background-repeat: initial;
                height: 100%;
            }
        </style>
    </head>
    <body>
        <?php
        if(isset($error))
        {
            ?>
            <script type="text/javascript">
                alert("Please login to continue");
                window.location.href = '/user/account/';
            </script>
            <?php
        }
        else
        {
            ?>
            <div class="container-fluid" id="videos">
                <div class="container">
                    <div class="col-md-12 carousel slide" id="carousel-subscriber"><div class="carousel-inner" id="subscriber"></div></div>
                    <!-- Left and right controls -->
                   <a class="left carousel-control" href="#carousel-subscriber" data-slide="prev">
                     <span class="glyphicon glyphicon-chevron-left"></span>
                     <span class="sr-only">Previous</span>
                   </a>
                   <a class="right carousel-control" href="#carousel-subscriber" data-slide="next">
                     <span class="glyphicon glyphicon-chevron-right"></span>
                     <span class="sr-only">Next</span>
                   </a>
                </div>
                <div class="row" >
                    <div id="mobilePublisher" class="col-md-12"></div>
                    <div class="bg-text" id="mobilePublisherText">
                        <h2>Detail ONE</h2>
                        <h1>Some Information</h1>
                        <p>And more information or link we can add</p>
                    </div>
                </div>
                <div id="screen-preview"></div>
            </div>
            <div id="all-buttons" class="col-md-12 col-sm-12 visible-xs visible-sm">
                <div class="col-md-12 padding-5 right-align">
                    <div id="buttons">
                        <button type="button" class="slide-toggle btn btn-sm btn-info marginbox" ><i class="fas fa-comments">&nbsp;</i><b class="font-20" >Ask</b></button>
                        <button type="button"  class="btn btn-sm btn-danger" onclick="leaveMeeting()"><b class="font-20">Leave Meeting</b> </button>
                    </div>
                </div>
            </div>
            <div id="mySidenav" class="sidenav hidden-xs hidden-sm">
                <a href="#" id="MenuTab" class="openbtn" onclick="openNav()" ><i class="fas fa-bars"></i></a>
            </div>

            <div id="mySidepanel1" class="sidepanel1" >
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><i class="fa fa-times" aria-hidden="true"></i></a>
                <a href="#"><button type="button" class="slide-toggle btn btn-sm btn-info width100 Orangebtn" ><i class="fas fa-comments">&nbsp;</i><b>Ask</b></button></a>
                <a href="#" ><button type="button"  class="btn btn-sm btn-danger width100 Orangebtn" onclick="leaveMeeting()"><b>Leave Meeting</b> </button></a>
            </div>

            <div class="box" id="chat-box">
                <span>
                    <button id="chatBoxOut" onclick="expandChat()" type="button" class="btn btn-sm btn-primary marginbox hidden-xs hidden-sm" ><i class="fas fa-chevron-circle-up"></i></button>
                    <button id="chatBoxIn" onclick="collapsChat()" type="button" class="btn btn-sm btn-primary marginbox"><i class="fas fa-chevron-circle-down"></i></button>
                    <button type="button" class="slide-toggle btn btn-sm btn-danger marginbox" ><i class="fas fa-times-circle"></i></button>
                </span>
                <div class="box-inner">
                     <div id="textchat">
                        <div id="history">
                            <?php
                            if($common_chat_history)
                            {
                                foreach($common_chat_history as $history)
                                {
                                    ?>
                                    <p id="<?=$history['chat_id']?>" class="<?=$history['chat_of']?>"><?=$history['name'] . ': ' . $history['message']?></p>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <form>
                            <input type="text" placeholder="Input your text here" id="msgTxt"></input>
                            <input type="hidden" id="username" value="<?=$username?>"></input>
                        </form>
                    </div>
                </div>
            </div>

            <div id="chatModal" class="chatBoxModal" >
                  <!-- Modal content -->
                  <div class="row chatBoxModal-content" >
                     <span class="chatclose" id="chatcloseModal">&times;</span>
                     <div id="historyOTO">
                         <?php
                         if($private_chat_history)
                         {
                             foreach($private_chat_history as $history)
                             {
                                 ?>
                                 <p class="<?=$history['chat_of']?>"><?=$history['name'] . ': ' . $history['message']?></p>
                                 <?php
                             }
                         }
                         ?>
                     </div>
                     <div id="textchatOTO"></div>
                  </div>
            </div>
            <div id="footer-head" class="col-md-12 col-sm-12">
                <div id="footer-head-sub" class="col-md-12">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 left-align">
                        <div class="copyright">
                            <span class="footer-text">© 2018, </span>
                            <img src="https://www.hellouni.org/img/logo.png" width="90">&nbsp;
                            <span class="footer-text"> All rights reserved</span>
                            <!-- COPYRIGHT 2018 &copy; HELLOUNI 2018 -->
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 right-align">
                        <div class="design">
                            <a href="https://www.hellouni.org" class="footer-text"><b>Hellouni</b> </a> |  <a target="_blank" href="https://www.hellouni.org" class="footer-text">MAKE RIGHT CHOICE !</a>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                var chatBoxModal = document.getElementById("chatModal");
                var chatspan = document.getElementById("chatcloseModal");

                var apiKey = "<?=$apiKey?>";
                var sessionId = "<?=$sessionId?>";
                var token = "<?=$token?>";
                var tokboxId = "<?=$tokboxId?>";
                var session;
                var archiveID;
                var allConnections = {};
                var allStreams = {};
                var publisher;

                var organizerCount = 0;
                var publisherCount = 0;
                var subscriberCount = 0;
                var connectionCount = 0;

                var current_question_index = 0;
                var totalSubscribers = 0;

                var msgHistory = document.querySelector('#history');
                var privateHistory = document.querySelector("#historyOTO");

                document.getElementById("mobilePublisherText").style.display = "none";

                function openNav() {
                    document.getElementById("mySidepanel1").style.setProperty("width", "20%", "important");
                    document.getElementById("videos").style.setProperty("width", "80%", "important");
                }

                function closeNav() {
                    document.getElementById("mySidepanel1").style.setProperty("width", "0", "important");
                    document.getElementById("videos").style.setProperty("width", "100%", "important");
                }

                function expandChat() {
                    document.getElementById("chat-box").style.setProperty("height", "95%", "important");
                    document.getElementById("chat-box").style.display = "block";
                    document.getElementById("chatBoxOut").style.display = "none";
                    document.getElementById("chatBoxIn").style.display = "block";
                }

                function collapsChat() {
                    document.getElementById("chat-box").style.setProperty("height", "54%", "important");
                    document.getElementById("chat-box").style.display = "block";
                    document.getElementById("chatBoxIn").style.display = "none";
                    document.getElementById("chatBoxOut").style.display = "block";
                }

                chatspan.onclick = function() {
                    chatBoxModal.style.display = "none";
                }

                window.onclick = function(event) {
                    if (event.target == chatBoxModal) {
                        chatBoxModal.style.display = "none";
                    }
                }

                function startSlider(){
                    if(totalSubscribers > 3){
                        var firstItemDiv = document.createElement("div");
                        firstItemDiv.setAttribute("id", "item-first");
                        firstItemDiv.setAttribute("class", "item active");
                        document.getElementById("subscriber").appendChild(firstItemDiv);

                        var secondItemDiv = document.createElement("div");
                        secondItemDiv.setAttribute("id", "item-second");
                        secondItemDiv.setAttribute("class", "item");
                        document.getElementById("subscriber").appendChild(secondItemDiv);
                    }
                    if(totalSubscribers > 6){
                        var thirdItemDiv = document.createElement("div");
                        thirdItemDiv.setAttribute("id", "item-third");
                        thirdItemDiv.setAttribute("class", "item");
                        document.getElementById("subscriber").appendChild(thirdItemDiv);
                    }
                    if(totalSubscribers > 9){
                        var fourthItemDiv = document.createElement("div");
                        fourthItemDiv.setAttribute("id", "item-fourth");
                        fourthItemDiv.setAttribute("class", "item");
                        document.getElementById("subscriber").appendChild(fourthItemDiv);
                    }
                    if(totalSubscribers > 12){
                        var fifthItemDiv = document.createElement("div");
                        fifthItemDiv.setAttribute("id", "item-fifth");
                        fifthItemDiv.setAttribute("class", "item");
                        document.getElementById("subscriber").appendChild(fifthItemDiv);
                    }

                    if(totalSubscribers > 3){
                        var elemIndex = 0;
                        for(let key in allStreams){
                            var elem = document.getElementById(allStreams[key]);
                            if(elem !== undefined && elem){
                                $("#" + allStreams[key]).remove();
                                if(elemIndex < 3){
                                    document.getElementById("item-first").appendChild(elem);
                                }
                                else if(elemIndex >= 3 && elemIndex < 6){
                                    document.getElementById("item-second").appendChild(elem);
                                }
                                else if(elemIndex >= 6 && elemIndex < 9){
                                    document.getElementById("item-third").appendChild(elem);
                                }
                                else if(elemIndex >= 9 && elemIndex < 12){
                                    document.getElementById("item-fourth").appendChild(elem);
                                }
                                else{
                                    document.getElementById("item-fifth").appendChild(elem);
                                }
                                $("#"+allStreams[key]).css("display", "");
                                elemIndex++;
                            }
                        }
                        $('#carousel-subscriber').carousel({ interval: 60000 });
                    }
                }

                function stopSlider(){
                    for(let key in allStreams){
                        var elem = document.getElementById(allStreams[key]);
                        if(elem !== undefined && elem){
                            $("#" + allStreams[key]).remove();
                            document.getElementById("subscriber").appendChild(elem);
                        }
                    }
                    $("#item-first").remove();
                    $("#item-second").remove();
                    $("#item-third").remove();
                    $("#item-fourth").remove();
                    $("#item-fifth").remove();
                }

                $(document).ready(function () {
                    $(".slide-toggle").click(function(){
                        $(".box").animate({
                            width: "toggle"
                        });
                    });
                    $("#screen-preview").hide();

                    // Create Question objects from all_questions and add them to the Quiz object
                    for (var i = 0; i < all_questions.length; i++) {
                        // Create a new Question object
                        var question = new Question(all_questions[i]);

                        // Add the question to the instance of the Quiz object that we created previously
                        quiz.add_question(question);
                    }

                    // Render the quiz
                    var quiz_container = $('#quiz');
                    quiz.render(quiz_container);
                });

                // Handling all of our errors here by alerting them
                function handleError(error) {
                    if (error) {
                        //alert(error.message);
                    }
                }

                function initializeSession() {
                    session = OT.initSession(apiKey, sessionId);

                    // Connect to the session
                    session.connect(token, function(error) {
                        // If the connection is successful, publish to the session
                        if (error) {
                            handleError(error);
                        } else {
                            //session.publish(publisher, handleError);
                        }
                    });

                    session.on('signal:msg', function signalCallback(event) {
                        if(event.from.connectionId === session.connection.connectionId){
                            document.getElementById("chat-box").style.display="block";
                            var msg = document.createElement('p');
                            var splitData = event.data.split('|');
                            //msg.textContent = event.data;
                            msg.textContent = splitData[0];
                            msg.setAttribute("id", splitData[1]);
                            msg.className = 'mine';
                            msgHistory.appendChild(msg);
                            msg.scrollIntoView();
                        }
                        else if(event.from.permissions.publish || session.connection.permissions.publish){
                            document.getElementById("chat-box").style.display="block";
                            var msg = document.createElement('p');
                            var splitData = event.data.split('|');
                            //msg.textContent = event.data;
                            msg.textContent = splitData[0];
                            msg.setAttribute("id", splitData[1]);
                            msg.className = 'theirs';
                            msgHistory.appendChild(msg);
                            msg.scrollIntoView();
                        }
                    });

                    session.on('signal:del-msg', function signalCallback(event) {
                        $("#" + event.data).remove();
                    });

                    session.on('signal:msg-private', function signalCallback(event) {
                        var msg = document.createElement('p');
                        msg.textContent = event.data;
                        msg.className = event.from.connectionId === session.connection.connectionId ? 'mine' : 'theirs';
                        privateHistory.appendChild(msg);
                        msg.scrollIntoView();
                        allConnections[event.from.connectionId] = event.from;
                        openChatBox(event.from.connectionId);
                    });

                    session.on({
                        connectionCreated: function (event) {
                            if (event.connection.connectionId == session.connection.connectionId) {
                                $.ajax({
                                    url: '/tokbox/connection',
                                    type: 'POST',
                                    contentType: 'application/json', // send as JSON
                                    data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId, 'connectionId': event.connection.id, 'role': 'Attendee', 'myConnectionId': session.connection.connectionId, 'username': event.connection.data}),
                                    complete: function complete() {
                                        // called when complete
                                        //console.log('connection function completed');
                                    },
                                    success: function success() {
                                        // called when successful
                                        //console.log('successfully called connection');
                                    },
                                    error: function error() {
                                        // called when there is an error
                                        //console.log('error calling connection');
                                    }
                                });
                            }
                        },
                        connectionDestroyed: function connectionDestroyedHandler(event) {
                            //alert("You have been disconnected. Please refresh the page to join again");
                        },
                        streamDestroyed: function (event){
                            if(event.stream.videoType == 'camera'){
                                if(pollConnectionId && pollConnectionId == event.stream.connection.id){
                                    document.getElementById("mobilePublisher").style.display = "block";
                                    document.getElementById("mobilePublisherText").style.display = "none";
                                    pollModal.style.display = "none";
                                }

                                totalSubscribers--;
                                stopSlider();
                                startSlider();

                                $('#subscriber .OT_root').css({
                                    width: '220px',
                                    height: '140px',
                                    marginRight: '10px',
                                    marginTop: '10px'
                                });
                            }
                            if(event.stream.videoType == 'screen'){
                                //document.getElementById("subscriber").style.display = "block";
                                document.getElementById("mobilePublisher").style.display = "block";
                                document.getElementById("mobilePublisherText").style.display = "none";
                                $("#screen-preview").hide();
                            }
                            //console.log("Stream Destroyed ", event);
                            /*document.getElementById("subscriber").style.display = "block";
                            document.getElementById("mobilePublisher").style.display = "block";
                            document.getElementById("mobilePublisherText").style.display = "block";*/
                        },
                        streamCreated: function(event){
                            if(event.stream.videoType == 'camera'){
                                var subscriber = session.subscribe(event.stream, 'subscriber', {
                                    insertMode: 'append',
                                    width: '220px',
                                    height: '140px',
                                    name: event.stream.name,
                                    subscribeToAudio:true,
                                    subscribeToVideo:true,
                                    audioVolume: 100
                                }, handleError);

                                <?php
                                if(isset($running_poll['connection_id']))
                                {
                                    ?>
                                    pollConnectionId = '<?=$running_poll['connection_id']?>';
                                    if(pollConnectionId == event.stream.connection.id)
                                    {
                                        $("#submit-button").attr("disabled", false);
                                        //document.getElementById("subscriber").style.display = "none";
                                        document.getElementById("mobilePublisher").style.display = "none";
                                        document.getElementById("mobilePublisherText").style.display = "none";
                                        quiz.change_question(<?=$running_poll['question_index']?>);

                                        pollModal.style.display = "block";
                                    }
                                    <?php
                                }
                                ?>

                                totalSubscribers++;
                                allStreams[event.stream.id] = subscriber.id;
                                stopSlider();
                                startSlider();

                                $('#subscriber .OT_root').css({
                                    width: '220px',
                                    height: '140px',
                                    marginRight: '10px',
                                    marginTop: '10px'
                                });

                                var SpeakerDetection = function(subscriber, startTalking, stopTalking) {
                                    var activity = null;
                                    subscriber.on('audioLevelUpdated', function(event) {
                                        var now = Date.now();
                                        if (event.audioLevel > 0.2) {
                                            if (!activity) {
                                                activity = {timestamp: now, talking: false};
                                            }
                                            else if (activity.talking) {
                                                activity.timestamp = now;
                                            }
                                            else if (now- activity.timestamp > 500) {
                                                // detected audio activity for more than 1s
                                                // for the first time.
                                                activity.talking = true;
                                                if (typeof(startTalking) === 'function') {
                                                    startTalking();
                                                }
                                            }
                                        }
                                        else if (activity && now - activity.timestamp > 3000) {
                                            // detected low audio activity for more than 3s
                                            if (activity.talking) {
                                                if (typeof(stopTalking) === 'function') {
                                                    stopTalking();
                                                }
                                            }
                                            activity = null;
                                        }
                                    });
                                };

                                SpeakerDetection(subscriber, function() {
                                    //console.log('Started Talking ', subscriber.id, subscriber);
                                    //document.getElementById(subscriber.id).style.overflow = "inherit";
                                    document.getElementById(subscriber.id).style.setProperty("overflow", "inherit", "important");

                                    var x = document.getElementById(subscriber.id).querySelectorAll(".OT_name");
                                    x[0].style.display = "block";
                                    x[0].style.opacity = "1";
                                    x[0].style.top = "0";

                                    var y = document.getElementById(subscriber.id).querySelectorAll(".OT_widget-container");
                                    y[0].style.display = "block";
                                    y[0].style.setProperty("border", "3px solid #00ca00", "important");
                                }, function() {
                                    document.getElementById(subscriber.id).style.background = "";
                                    var x = document.getElementById(subscriber.id).querySelectorAll(".OT_name");
                                    x[0].style.display = "none";
                                    x[0].style.opacity = "0";
                                    x[0].style.top = "-25px";

                                    var y = document.getElementById(subscriber.id).querySelectorAll(".OT_widget-container");
                                    y[0].style.display = "block";
                                    y[0].style.setProperty("border", "none", "important");

                                    document.getElementById(subscriber.id).style.setProperty("overflow", "hidden", "important");
                                    console.log('stopped talking');
                                });
                            }
                            if(event.stream.videoType == 'screen'){
                                document.getElementById("subscriber").style.display = "block";
                                document.getElementById("mobilePublisher").style.display = "none";
                                document.getElementById("mobilePublisherText").style.display = "none";
                                $("#screen-preview").show();

                                session.subscribe(event.stream, 'screen-preview', {
                                    insertMode: 'append',
                                    width: '100%',
                                    height: '100%',
                                    name: '<?=$username?>'
                                }, handleError);
                            }
                            //console.log("Stream Created ", event);
                        }
                    });
                }

                function openChatBox(connectionId){
                    var html = '<input type="text" placeholder="Input your text here" id="msgTxtOTO"></input><input type="hidden" id="username-private" value="<?=$username?>"></input><input type="button" class="btn btn-danger" onclick="sendChat(\'' + connectionId + '\')" value="Send">';
                    document.getElementById("textchatOTO").innerHTML = html;
                    chatBoxModal.style.display = "block";
                }

                function sendChat(connectionId){
                    var msg = document.getElementById("msgTxtOTO").value;
                    var uname = document.getElementById("username-private").value;
                    //console.log(allConnections[connectionId]);
                    var msgEle = document.createElement('p');
                    msgEle.textContent = uname + ': ' + msg;
                    msgEle.className = 'mine';
                    privateHistory.appendChild(msgEle);
                    msgEle.scrollIntoView();

                    $.ajax({
                        url: '/tokbox/chat',
                        type: 'POST',
                        contentType: 'application/json', // send as JSON
                        data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId, 'to': connectionId, 'msg': msg}),
                        complete: function complete() {
                            // called when complete
                            //console.log('chat function completed');
                        },
                        success: function success() {
                            // called when successful
                            //console.log('successfully called chat');
                        },
                        error: function error() {
                            // called when there is an error
                            //console.log('error calling chat');
                        }
                    });

                    session.signal({
                        to: allConnections[connectionId],
                        type: 'msg-private',
                        data: uname + ': ' + msg
                    }, function signalCallback(error) {
                        if (error) {
                            console.error('Error sending signal:', error.name, error.message);
                        } else {
                            msgTxt.value = '';
                        }
                    });
                }

                // Text chat
                var form = document.querySelector('form');
                var msgTxt = document.querySelector('#msgTxt');
                var username = document.querySelector('#username');
                //var current_question_index = 0;

                // Send a signal once the user enters data in the form
                form.addEventListener('submit', function submit(event) {
                    event.preventDefault();
                    var currentTime = new Date().getTime();
                    var chatId = 'chat-' + currentTime;
                    var senderRole = session.connection.permissions.publish ? 'PUBLISHER' : 'SUBSCRIBER';
                    $.ajax({
                        url: '/tokbox/chat',
                        type: 'POST',
                        contentType: 'application/json', // send as JSON
                        data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId, 'to': 'All', 'msg': msgTxt.value, 'from_role': senderRole, 'chat_id': chatId}),
                        complete: function complete() {
                            // called when complete
                            //console.log('chat function completed');
                        },
                        success: function success() {
                            // called when successful
                            //console.log('successfully called chat');
                        },
                        error: function error() {
                            // called when there is an error
                            //console.log('error calling chat');
                        }
                    });
                    session.signal({
                        type: 'msg',
                        data: username.value + ': ' + msgTxt.value + '|' + chatId
                    }, function signalCallback(error) {
                        if (error) {
                            console.error('Error sending signal:', error.name, error.message);
                        } else {
                            msgTxt.value = '';
                        }
                    });
                });

                initializeSession();

                function leaveMeeting(){
                    window.open('/university/virtualfair/list/1', '_self');
                }
                document.getElementById("mobilePublisherText").style.display = "none";

            </script>
            <?php
        }
        ?>
    </body>
</html>
