
<!DOCTYPE html>

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>HelloUni Webinar</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <!-- <link rel="stylesheet" href="https://hellouni.org/admin/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://hellouni.org/admin/bootstrap/css/bootstrap.css"> -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="https://hellouni.org/admin/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="https://hellouni.org/admin/plugins/iCheck/square/blue.css">
  <style type="text/css">
  .error{color:red;}
  #colorstar { color: #ee8b2d;}
.badForm {color: #FF0000;}
.goodForm {color: #00FF00;}
/*.evaluation { margin-left:30px;}*/
.evaluation { text-align: center;}

.boxDecor{
  box-shadow: 2px 2px 4px 0px #d6d4d4;
  border-radius: 6px;
}

.btn-default{
  background-color: #ee8a2d;
    color: #fff;
    border-color: #ddd;
        min-width: 115px;
}
  </style>

<link rel="icon" type="image/png" sizes="96x96" href="https://hellouni.org/admin/images/favicon-96x96.png">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page" style="background-color: white; background-image: url(https://www.hellouni.org/application/images/background.jpg);">
    <!-- <div class="login-box">
      <div class="login-logo">
    <img src="https://hellouni.org/admin/images/HelloUni-Logo.png" style="width: 75%;">
      </div>
      <div class="login-box-body">
      <h1>Feedback</h1>
        <form action="/test/load" method="get">
          <div class="form-group has-feedback" >
            <img src="<?php echo base_url();?>application/images/thankYou.jpg" alt="thankYou" width="100%" height="100%">
          </div>
        </form>



      </div>

    </div> -->
    <section>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-10 col-md-offset-1" style="padding: 3rem 30rem 30rem 30rem;">
              <div class="col-md-12" style="    background-color: #815cd5;padding: 5px;text-align: center;color: white;">
                <img src="https://www.hellouni.org/img/logo.png" width="60%" alt="...">
                <h2>FEEDBACK</h2>
                <h4>Please help us serve you better by telling us about your experience so far. We want to make sure we meet your expectations.</h4>
              </div>
              <form class="form-group" style="box-shadow: 2px 2px 7px 1px #ded7d7;padding: 15px 10px;border-radius: 5px; background-color: white;" action="/meeting/oneonone/feedback" method="post">
                <div class="col-md-12">
                  <h3>Are you interested and would like to apply?</h3>
                  <select class="form-control" name="interested">
                      <option value="YES">Yes</option>
                      <option value="MAY_BE">May Be</option>
                      <option value="NO">No</option>
                  </select>
                      <!--input type="text" class="form-control boxDecor" value="Type here" required=""-->

                  <h3>Your Review <span>( Tips and Guidelines ) </span> </h3>
                      <textarea onfocus="this.value = '';"  required="" class="form-control boxDecor" name="review">Type here</textarea>
                </div>
                <div class="col-md-12" style="z-index: 1;">
                  <div class="col-md-12">
                    <div class="row lead evaluation">
                      <h3>Overall Experience</h3>
                      <div id="colorstar" class="starrr ratable" ></div>
                      <span id="count">0</span> star(s) - <span id="meaning"> </span>
                          <div class='indicators' style="display:none">
                              <div id='textwr'>What went wrong?</div>
                              <input id="rate[]" name="rate[]" type="text" placeholder="" class="form-control input-md" style="display:none;">
                              <input id="rating[]" name="rating[]" type="text" placeholder="" class="form-control input-md rateval" style="display:none;">
                              <span class="button-checkbox">
                              <button type="button" class="btn criteria" data-color="info">Punctuallity</button>
                               <input type="checkbox" class="hidden"  />
                              </span>
                              <span class="button-checkbox">
                              <button type="button" class="btn criteria" data-color="info">Assistance</button>
                               <input type="checkbox" class="hidden"  />
                              </span>
                              <span class="button-checkbox">
                              <button type="button" class="btn criteria" data-color="info">Knowledge</button>
                               <input type="checkbox" class="hidden"  />
                              </span>
                              <span class="button-checkbox">
                              <button type="button" class="btn criteria" data-color="info">Speach</button>
                               <input type="checkbox" class="hidden"  />
                              </span>
                          </div>
                      </div>
                  </div>
                  <!--div class="col-md-6">
                    <div class="row lead evaluation">
                      <h3>Timely Response</h3>
                      <div id="colorstar" class="starrr ratable" ></div>
                      <span id="count">0</span> star(s) - <span id="meaning"> </span>
                          <div class='indicators' style="display:none">
                              <div id='textwr'>What went wrong?</div>
                              <input id="rate[]" name="rate[]" type="text" placeholder="" class="form-control input-md" style="display:none;">
                              <input id="rating[]" name="rating[]" type="text" placeholder="" class="form-control input-md rateval" style="display:none;">

                              <span class="button-checkbox">
                              <button type="button" class="btn criteria" data-color="info">Punctuallity</button>
                               <input type="checkbox" class="hidden"  />
                              </span>
                              <span class="button-checkbox">
                              <button type="button" class="btn criteria" data-color="info">Assistance</button>
                               <input type="checkbox" class="hidden"  />
                              </span>
                              <span class="button-checkbox">
                              <button type="button" class="btn criteria" data-color="info">Knowledge</button>
                               <input type="checkbox" class="hidden"  />
                              </span>
                              <span class="button-checkbox">
                              <button type="button" class="btn criteria" data-color="info">Speach</button>
                               <input type="checkbox" class="hidden"  />
                              </span>
                          </div>
                      </div>
                  </div-->
                </div>
                <div class="col-md-12">
                  <h3>Any Suggessions</h3>
                  <textarea onfocus="" onblur="" required="" class="form-control boxDecor" placeholder="Type here" name="suggestion"></textarea>
                </div>
                <div class="col-md-12" style="text-align: center;">
                  <!--button type="button" class="btn btn-lg btn-primary" style="margin-top: 15px;background-color: #815cd5;border-color: #815cd5 " onclick="loadThankyou()">Submit</button-->
                  <input type="hidden" name="slotId" value="<?=$slotId?>">
                  <input type="submit" class="btn btn-lg btn-primary" style="margin-top: 15px;background-color: #815cd5;border-color: #815cd5 " value="Submit">
                </div>
              </form>
          </div>
        </div>
      </div>
    </section>

    <!-- jQuery 2.1.4 -->
    <script src="https://hellouni.org/admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="https://hellouni.org/admin/bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="https://hellouni.org/admin/plugins/iCheck/icheck.min.js"></script>
    <script>
      // $(function () {
      //   $('input').iCheck({
      //     checkboxClass: 'icheckbox_square-blue',
      //     radioClass: 'iradio_square-blue',
      //     increaseArea: '20%' // optional
      //   });
      // });
    </script>

  <script type="text/javascript">
  // $( document ).ready(function() {

  //  $( "#signin" ).click(function(e) {
  //      var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  //    var username   = $( "#username" ).val();
  //    var password = $( "#password" ).val();

  //    if(username==''){

  //    $( "#emailerror" ).text("Username is Required"); return false; e.stoppropagation();

  //    }else if(password==''){

  //     $( "#emailerror" ).text("");
  //   $( "#passworderror" ).text("Password is Empty"); return false; e.stoppropagation();
  //    }


  //    });

  //     });


  // Starrr plugin (https://github.com/dobtco/starrr)
var __slice = [].slice;

(function($, window) {
  var Starrr;

  Starrr = (function() {
    Starrr.prototype.defaults = {
      rating: void 0,
      numStars: 5,
      change: function(e, value) {}
    };

    function Starrr($el, options) {
      var i, _, _ref,
        _this = this;

      this.options = $.extend({}, this.defaults, options);
      this.$el = $el;
      _ref = this.defaults;
      for (i in _ref) {
        _ = _ref[i];
        if (this.$el.data(i) != null) {
          this.options[i] = this.$el.data(i);
        }
      }
      this.createStars();
      this.syncRating();
      this.$el.on('mouseover.starrr', 'span', function(e) {
        return _this.syncRating(_this.$el.find('span').index(e.currentTarget) + 1);
      });
      this.$el.on('mouseout.starrr', function() {
        return _this.syncRating();
      });
      this.$el.on('click.starrr', 'span', function(e) {
        return _this.setRating(_this.$el.find('span').index(e.currentTarget) + 1);
      });
      this.$el.on('starrr:change', this.options.change);
    }

    Starrr.prototype.createStars = function() {
      var _i, _ref, _results;

      _results = [];
      for (_i = 1, _ref = this.options.numStars; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--) {
        _results.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"));
      }
      return _results;
    };

    Starrr.prototype.setRating = function(rating) {
      if (this.options.rating === rating) {
        rating = void 0;
      }
      this.options.rating = rating;
      this.syncRating();
      return this.$el.trigger('starrr:change', rating);
    };

    Starrr.prototype.syncRating = function(rating) {
      var i, _i, _j, _ref;

      rating || (rating = this.options.rating);
      if (rating) {
        for (i = _i = 0, _ref = rating - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
          this.$el.find('span').eq(i).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
        }
      }
      if (rating && rating < 5) {
        for (i = _j = rating; rating <= 4 ? _j <= 4 : _j >= 4; i = rating <= 4 ? ++_j : --_j) {
          this.$el.find('span').eq(i).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
        }
      }
      if (!rating) {
        return this.$el.find('span').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
      }
    };

    return Starrr;

  })();
  return $.fn.extend({
    starrr: function() {
      var args, option;

      option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      return this.each(function() {
        var data;

        data = $(this).data('star-rating');
        if (!data) {
          $(this).data('star-rating', (data = new Starrr($(this), option)));
        }
        if (typeof option === 'string') {
          return data[option].apply(data, args);
        }
      });
    }
  });
})(window.jQuery, window);

$(function() {
  return $(".starrr").starrr();
});

$( document ).ready(function() {

    var correspondence=["","Really Bad","Bad","Fair","Good","Excelent"];

  $('.ratable').on('starrr:change', function(e, value){

     $(this).closest('.evaluation').children('#count').html(value);
     $(this).closest('.evaluation').children('#meaning').html(correspondence[value]);

     var currentval=  $(this).closest('.evaluation').children('#count').html();

    var target=  $(this).closest('.evaluation').children('.indicators');
    target.css("color","black");
    target.children('.rateval').val(currentval);
    target.children('#textwr').html(' ');


    if(value<3){
     target.css("color","red").show();
     target.children('#textwr').text('What can be improved?');
    }else{
        if(value>3){
            target.css("color","green").show();
            target.children('#textwr').html('What was done correctly?');
        }else{
       target.hide();
        }
    }

  });





  $('#hearts-existing').on('starrr:change', function(e, value){
    $('#count-existing').html(value);
  });
});





$(function () {
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'fa fa-square-o'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
});


            function loadThankyou(){
               window.open('/test/thankyou', '_self');
            }


  </script>
  </body>
