<html>
    <head>
        <title> Hellouni : Webinar | Organizer </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css">
        <link href="<?=base_url()?>css/student_oneonone.css?v=1.1" rel="stylesheet">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://static.opentok.com/v2/js/opentok.min.js"></script>

    </head>
    <body>
        <?php
        if(isset($error))
        {
            ?>
            <script type="text/javascript">
                alert("Please login to continue");
                window.location.href = '/user/account/';
            </script>
            <?php
        }
        else if(isset($timer_error))
        {
            ?>
            <div class="container-fluid" id="videos">
                <div class="row" >
                    <div id="mobilePublisher" class="col-md-12"></div>
                    <div class="bg-text" id="mobilePublisherText">
                        <h2>Detail ONE</h2>
                        <h1>Some Information</h1>
                        <p>And more information or link we can add</p>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                var webinarCountDownDate = new Date('<?=$countDownDate?>').getTime();

                // Update the count down every 1 second
                var x = setInterval(function()
                {
                    // Get today's date and time
                    var now = new Date().getTime();

                    // Find the distance between now and the count down date
                    var distance = webinarCountDownDate - now;

                    // Time calculations for days, hours, minutes and seconds
                    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                    if(distance >= 0)
                    {
                        var html = '<h2>SESSION WILL START IN</h2><h1>' + days + 'd ' + hours + 'h ' + minutes + 'm ' + seconds + 's</h1>';
                        document.getElementById("mobilePublisherText").innerHTML = html;
                        document.getElementById("mobilePublisherText").style.display = "block";
                    }
                    // If the count down is over, write some text
                    if (distance < 0)
                    {
                        //document.getElementById("mobilePublisherText").style.display = "none";
                        window.location.reload();
                    }
                }, 1000);
                document.getElementById("mobilePublisherText").style.display = "none";
            </script>
            <?php
            exit;
        }
        ?>
        <div id="meeting-timer"></div>
        <div class="container-fluid" id="videos">
            <div class="col-md-12">
                <div class="col-md-12" id="subscriber"></div>
                <div class="col-md-12" id="publisher" ></div>
                <div class="col-md-12" id="screen-preview"></div>
            </div>
        </div>
        <div id="mySidenav" class="sidenav">
            <a href="#" id="MenuTab" class="openbtn" onclick="openNav()" ><i class="fas fa-bars"></i></a>
        </div>

        <div id="mySidepanel1" class="sidepanel1" >
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><i class="fa fa-times" aria-hidden="true"></i></a>
            <a href="#" ><button type="button" class="slide-toggle btn btn-sm Orangebtn width100" ><i class="fas fa-comments">&nbsp;</i><b>Ask</b></button></a>
            <a href="#" onclick="showBrochureModal()"><button type="button" class="btn btn-sm Orangebtn width100"><b>Download Brochure</b> </button></a>
            <a href="https://www.google.com" target="_blank"><button type="button" class="btn btn-sm Orangebtn width100"><b>Apply</b> </button></a>
            <a href="#" ><button type="button"  class="btn btn-sm Orangebtn width100" onclick="leaveMeeting()"><b>Leave Meeting</b> </button></a>
        </div>

        <div class="box" id="chat-box">
            <span>
                <button id="chatBoxOut" onclick="expandChat()" type="button" class="btn btn-sm purplebtn marginbox" ><i class="fas fa-chevron-circle-up"></i></button>
                <button id="chatBoxIn" onclick="collapsChat()" type="button" class="btn btn-sm Orangebtn marginbox"><i class="fas fa-chevron-circle-down"></i></button>
                <button type="button" class="slide-toggle btn btn-sm btn-danger marginbox" ><i class="fas fa-times-circle"></i></button>
            </span>
            <div class="box-inner">
                <div id="textchat">
                    <div id="history">
                        <?php
                        if($common_chat_history)
                        {
                            foreach($common_chat_history as $history)
                            {
                                ?>
                                <p class="<?=$history['chat_of']?>"><?=$history['name'] . ': ' . $history['message']?></p>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <form>
                        <input type="text" placeholder="Input your text here" id="msgTxt"></input>
                        <input type="hidden" id="username" value="<?=$username?>"></input>
                    </form>
                </div>
            </div>
        </div>

        <div id="brochures" class="brochureModal" >
            <!-- Modal content -->
            <div class="row brochureModal-content">
                <a href="javascript:void(0)" class="closebtn" onclick="closeBrochureModal()"><i class="fa fa-times" aria-hidden="true"></i></a>
                <select name="brochure" onchange="downloadBrochure(this.value)">
                    <option value="">Select Brochure</option>
                    <?php
                    if($brochures)
                    {
                        foreach($brochures as $brochure)
                        {
                            ?>
                            <option value="<?=$brochure->id?>"><?=$brochure->brochure_title?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>

        <div id="pollId" class="pollModal" >
            <!-- Modal content -->
            <div class="row pollModal-content">
                <div id="poll-timer"></div>
                <div id="quiz">
                    <h1 id="quiz-name"></h1>
                    <button id="submit-button" class="btn btn-md btn-primary">Submit Answers</button>
                    <button id="next-question-button" class="btn btn-md btn-primary">Next Question</button>
                    <button id="prev-question-button" class="btn btn-md btn-primary">Previous</button>
                    <div id="quiz-results">
                        <p id="quiz-results-message"></p>
                        <p id="quiz-results-score"></p>
                    </div>
                </div>
            </div>
        </div>
        <div id="footer-head" class="col-md-12 col-sm-12">
            <div id="footer-head-sub" class="col-md-12">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 left-align">
                    <div class="copyright">
                        <span class="footer-text">© 2018, </span>
                        <img src="https://www.hellouni.org/img/logo.png" width="90">&nbsp;
                        <span class="footer-text"> All rights reserved</span>
                        <!-- COPYRIGHT 2018 &copy; HELLOUNI 2018 -->
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 right-align">
                    <div class="design">
                        <a href="https://www.hellouni.org" class="footer-text"><b>Hellouni</b> </a> |  <a target="_blank" href="https://www.hellouni.org" class="footer-text">MAKE RIGHT CHOICE !</a>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var apiKey = "<?=$apiKey?>";
            var sessionId = "<?=$sessionId?>";
            var token = "<?=$token?>";
            var tokboxId = "<?=$tokboxId?>";
            var session;
            var archiveID;
            var allConnections = {};
            var allStreams = {};
            var publisher;

            var msgHistory = document.querySelector('#history');

            var pollModal = document.getElementById("pollId");

            // Text chat
            var form = document.querySelector('form');
            var msgTxt = document.querySelector('#msgTxt');
            var username = document.querySelector('#username');

            var totalSubscribers = 0;

            function openNav() {
                document.getElementById("mySidepanel1").style.setProperty("width", "20%", "important");
                document.getElementById("videos").style.setProperty("width", "80%", "important");
            }

            function closeNav() {
                document.getElementById("mySidepanel1").style.setProperty("width", "0", "important");
                document.getElementById("videos").style.setProperty("width", "100%", "important");
            }

            function expandChat() {
                document.getElementById("chat-box").style.setProperty("height", "95%", "important");
                document.getElementById("chat-box").style.display = "block";
                document.getElementById("chatBoxOut").style.display = "none";
                document.getElementById("chatBoxIn").style.display = "block";
            }

            function collapsChat() {
                document.getElementById("chat-box").style.setProperty("height", "54%", "important");
                document.getElementById("chat-box").style.display = "block";
                document.getElementById("chatBoxIn").style.display = "none";
                document.getElementById("chatBoxOut").style.display = "block";
            }

            window.onclick = function(event) {
                if (event.target == pollModal) {
                    pollModal.style.display = "none";
                }
            }

            function openPollPopup() {
                document.getElementById("publisher").style.display = "none";
                document.getElementById("subscriber").style.display = "none";
                document.getElementById("poll-start-button").style.display = "none";
                document.getElementById("poll-end-button").style.display = "inline-block";

                session.signal({
                    type: 'poll-start',
                    data: ''
                }, function signalCallback(error) {
                    if (error) {
                        //console.error('Error sending signal:', error.name, error.message);
                    } else {
                        //console.log("polling started");
                    }
                });
            }

            function closePollPopup() {
                document.getElementById("publisher").style.display = "block";
                document.getElementById("subscriber").style.display = "block";
                document.getElementById("poll-end-button").style.display = "none";
                document.getElementById("poll-start-button").style.display = "inline-block";

                session.signal({
                    type: 'poll-stop',
                    data: ''
                }, function signalCallback(error) {
                    if (error) {
                        //console.error('Error sending signal:', error.name, error.message);
                    } else {
                        //console.log("polling started");
                    }
                });
            }

            $(document).ready(function () {
                $(".slide-toggle").click(function(){
                    $(".box").animate({
                        width: "toggle"
                    });
                });

                $('.btn-filter').on('click', function () {
                    var $target = $(this).data('target');
                    if ($target != 'all') {
                        $('.table tr').css('display', 'none');
                        $('.table tr[data-status="' + $target + '"]').fadeIn('slow');
                    } else {
                        $('.table tr').css('display', 'none').fadeIn('slow');
                    }
                });
            });

            // Handling all of our errors here by alerting them
            function handleError(error) {
                if (error) {
                    //alert(error.message);
                }
            }

            function initializeSession() {
                session = OT.initSession(apiKey, sessionId);

                // Create a publisher
                publisher = OT.initPublisher('publisher', {
                    insertMode: 'append',
                    width: '100%',
                    height: '100%',
                    name: '<?=$username?>',
                    publishAudio:true,
                    publishVideo:true,
                    style: {buttonDisplayMode: 'on', nameDisplayMode: "on"}
                    //videoSource: 'screen'
                }, handleError);

                // Connect to the session
                session.connect(token, function(error) {
                    // If the connection is successful, publish to the session
                    if (error) {
                        handleError(error);
                    } else {
                        var node = document.createElement("button");
                        node.setAttribute("id", "video-button-" + publisher.id);
                        node.setAttribute("class", "OT_edge-bar-item OT_mode-on OT_video AVButton fas fa-video");
                        node.setAttribute("onClick", "disableVideo()")
                        document.getElementById(publisher.id).appendChild(node);
                        // document.getElementById("video-button-" + publisher.id).innerHTML = "Disable Video";
                         document.getElementById("video-button-" + publisher.id).style.setProperty("background-color", "#ffffff00", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("font-size", "25px", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("right", "30px", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("top", "4px", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("color", "white", "important");

                        session.publish(publisher, handleError);
                    }
                });

                session.on('signal:msg', function signalCallback(event) {
                    if(event.from.connectionId === session.connection.connectionId){
                        document.getElementById("chat-box").style.display="block";
                        var msg = document.createElement('p');
                        msg.textContent = event.data;
                        msg.className = 'mine';
                        msgHistory.appendChild(msg);
                        msg.scrollIntoView();
                    }
                    else if(event.from.permissions.publish || session.connection.permissions.publish){
                        document.getElementById("chat-box").style.display="block";
                        var msg = document.createElement('p');
                        msg.textContent = event.data;
                        msg.className = 'theirs';
                        msgHistory.appendChild(msg);
                        msg.scrollIntoView();
                    }
                });

                session.on('signal:poll-start', function signalCallback(event) {
                    document.getElementById("subscriber").style.display = "none";
                    document.getElementById("publisher").style.display = "none";
                    if(event.from.connectionId != session.connection.connectionId){
                        document.getElementById("poll-start-button").style.display = "none";
                        document.getElementById("poll-end-button").style.display = "none";
                    }

                    var countDownDate = 180000 + new Date().getTime();
                    // Update the count down every 1 second
                    var x = setInterval(function(){
                        // Get today's date and time
                        var now = new Date().getTime();

                        // Find the distance between now and the count down date
                        var distance = countDownDate - now;

                        // Time calculations for days, hours, minutes and seconds
                        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                        if(distance >= 0){
                            document.getElementById("poll-timer").innerHTML = "<b>Time</b> " + minutes + "m " + seconds + "s ";
                        }
                        // If the count down is over, write some text
                        if (distance < 0)
                        {
                            document.getElementById("quiz").innerHTML = "<h1>Time Up! This window will be closed in 5 sec</h1>";
                        }

                        if(distance < -5000)
                        {
                            clearInterval(x);
                            document.getElementById("poll-start-button").style.display = "none";
                            document.getElementById("poll-end-button").style.display = "none";

                            pollModal.style.display = "none";
                            document.getElementById("publisher").style.display = "block";
                            document.getElementById("subscriber").style.display = "block";
                        }
                    }, 1000);
                    pollModal.style.display = "block";
                });

                session.on('signal:poll-stop', function signalCallback(event) {
                    document.getElementById("poll-start-button").style.display = "inline-block";
                    document.getElementById("poll-end-button").style.display = "none";
                    pollModal.style.display = "none";
                    document.getElementById("subscriber").style.display = "block";
                    document.getElementById("publisher").style.display = "block";
                });

                session.on({
                    connectionCreated: function (event) {
                        $.ajax({
                            url: '/meeting/oneonone/connection',
                            type: 'POST',
                            contentType: 'application/json', // send as JSON
                            data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId, 'connectionId': event.connection.id}),
                            complete: function complete() {
                                // called when complete
                                //console.log('connection function completed');
                            },
                            success: function success() {
                                // called when successful
                                //console.log('successfully called connection');
                            },
                            error: function error() {
                                // called when there is an error
                                //console.log('error calling connection');
                            }
                        });
                    },
                    connectionDestroyed: function connectionDestroyedHandler(event) {
                        if (event.connection.connectionId != session.connection.connectionId) {
                        }
                        else{
                            alert("You have been disconnected. Please refresh the page to join again");
                        }
                    },
                    streamDestroyed: function (event){
                        //console.log("Stream Destroyed ", event);
                        if(event.stream.videoType == 'camera'){
                            totalSubscribers--;

                            var gridWidth = screen.width - 100,
                                gridHeight = 800,
                                gridItems = totalSubscribers,
                                grid_str = '',
                                $grid = $('#subscriber'),
                                i;

                            function dimensions(x) {
                                let quotient = parseInt(x / 4);
                                let remainder = x % 4;

                                let h = 800 / ( quotient + 1);
                                let w = screen.width - 200;
                                if(quotient == 0)
                                {
                                    w = w / remainder;
                                }
                                else
                                {
                                    w = w / 3;
                                }

                                $('#subscriber div').css({
                                    width: w,
                                    height: h
                                });
                            };

                            $grid.width(gridWidth).height(gridHeight);

                            let subscriberId = allStreams[event.stream.id];

                            //console.log(allStreams);
                            document.getElementById(subscriberId).style.display = 'none';

                            if(totalSubscribers > 0){
                                dimensions(totalSubscribers);
                            }

                            if(totalSubscribers == 0){
                                document.getElementById("publisher").setAttribute("style", "width:100%; height:88%;");
                            }
                        }
                        if(event.stream.videoType == 'screen'){
                            document.getElementById("subscriber").style.display = "block";
                            document.getElementById("screen-preview").style.display = "none";
                            document.getElementById("publisher").setAttribute("style", "width:22%; height:20%; float: left; position: fixed; bottom: 6%; left: 0%; z-index: 100;");
                        }
                    },
                    streamCreated: function(event){
                        if(event.stream.videoType == 'camera'){
                            var subscriber = session.subscribe(event.stream, 'subscriber', {
                                insertMode: 'append',
                                width: '100%',
                                height: '100%',
                                name: event.stream.name,
                            }, handleError);

                            totalSubscribers++;
                            allStreams[event.stream.id] = subscriber.id;

                            var gridWidth = screen.width - 100,
                                gridHeight = 800,
                                gridItems = totalSubscribers,
                                grid_str = '',
                                $grid = $('#subscriber'),
                                i;

                            function dimensions(x) {
                                // Old Function
                                /*$('#subscriber div').css({
                                    width: x,
                                    height: x
                                });*/

                                let quotient = parseInt(x / 4);
                                let remainder = x % 4;

                                let h = 800 / ( quotient + 1);
                                let w = screen.width - 200;
                                if(quotient == 0)
                                {
                                    w = w / remainder;
                                }
                                else
                                {
                                    w = w / 3;
                                }

                                $('#subscriber div').css({
                                    width: w,
                                    height: h
                                });
                            };

                            $grid.width(gridWidth).height(gridHeight);

                            /*for(i = 0 ; i < gridItems ; i++) {
                                grid_str+= '<div class="grid_col"></div>';
                            }

                            $grid.html(grid_str);*/


                            document.getElementById("publisher").setAttribute("style", "width:22%; height:20%;");

                            document.getElementById(subscriber.id).style.display = 'inline-block';
                            document.getElementById(subscriber.id).style.boxSizing = 'border-box';
                            document.getElementById(subscriber.id).style.border = '2px solid white';
                            document.getElementById(subscriber.id).style.margin = '2px';

                            //document.getElementById(subscriber.id).style.height = 'inherit';
                            //document.getElementById(subscriber.id).style.width = 'inherit';

                            // Old Code
                            /*for(i = 1 ; i < 1000 ; i++) {
                                dimensions(i);
                                //console.log(i, $grid[0].scrollHeight, $grid.height())
                                if($grid[0].scrollHeight > $grid.height()) {
                                    break;
                                }
                            }
                            dimensions(i-1);*/

                            dimensions(totalSubscribers);

                            var SpeakerDetection = function(subscriber, startTalking, stopTalking) {
                                var activity = null;
                                subscriber.on('audioLevelUpdated', function(event) {
                                    var now = Date.now();
                                    if (event.audioLevel > 0.2) {
                                        if (!activity) {
                                            activity = {timestamp: now, talking: false};
                                        }
                                        else if (activity.talking) {
                                            activity.timestamp = now;
                                        }
                                        else if (now- activity.timestamp > 500) {
                                            activity.talking = true;
                                            if (typeof(startTalking) === 'function') {
                                                startTalking();
                                            }
                                        }
                                    }
                                    else if (activity && now - activity.timestamp > 3000) {
                                        // detected low audio activity for more than 3s
                                        if (activity.talking) {
                                            if (typeof(stopTalking) === 'function') {
                                                stopTalking();
                                            }
                                        }
                                        activity = null;
                                    }
                                });
                            };

                            SpeakerDetection(subscriber, function() {
                                //console.log('Started Talking ', subscriber.id, subscriber);
                                //document.getElementById(subscriber.id).style.overflow = "inherit";
                                document.getElementById(subscriber.id).style.setProperty("overflow", "inherit", "important");

                                var x = document.getElementById(subscriber.id).querySelectorAll(".OT_name");
                                x[0].style.display = "block";
                                x[0].style.opacity = "1";
                                x[0].style.top = "0";


                                var y = document.getElementById(subscriber.id).querySelectorAll(".OT_widget-container");
                                y[0].style.display = "block";
                                y[0].style.setProperty("border", "3px solid #00ca00", "important");



                            }, function() {
                                document.getElementById(subscriber.id).style.background = "";
                                var x = document.getElementById(subscriber.id).querySelectorAll(".OT_name");
                                x[0].style.display = "none";
                                x[0].style.opacity = "0";
                                x[0].style.top = "-25px";

                                var y = document.getElementById(subscriber.id).querySelectorAll(".OT_widget-container");
                                y[0].style.display = "block";
                                y[0].style.setProperty("border", "none", "important");

                                document.getElementById(subscriber.id).style.setProperty("overflow", "hidden", "important");
                                console.log('stopped talking');
                            });
                        }
                        if(event.stream.videoType == 'screen'){
                            $('#share-screen-button').hide();
                            document.getElementById("subscriber").style.display = "none";
                            document.getElementById("screen-preview").style.display = "block";
                            document.getElementById("publisher").setAttribute("style", "width:15%; height:15%; left:auto; right:0px; top:60px; position:absolute; margin-top:0px");
                            session.subscribe(event.stream, 'screen-preview', {
                                insertMode: 'append',
                                width: '100%',
                                height: '100%',
                                name: '<?=$username?>'
                            }, handleError);
                        }
                        //console.log("Stream Created ", event);
                    }
                });

                function resizeSubscriber() {
                    publisher.element.style.width = "100px";
                    publisher.element.style.height = "75px";
                    document.getElementById("subscriber").appendChild(publisher.element);
                }
            }

            function disableVideo(){
                publisher.publishVideo(false);
                document.getElementById("video-button-" + publisher.id).setAttribute("onClick", "enableVideo('" + publisher + "')");
                // document.getElementById("video-button-" + publisher.id).innerHTML = "Enable Video";
                document.getElementById("video-button-" + publisher.id).classList.remove('fa-video');
                document.getElementById("video-button-" + publisher.id).classList.add('fa-video-slash');

            }

            function enableVideo(){
                publisher.publishVideo(true);
                document.getElementById("video-button-" + publisher.id).setAttribute("onClick", "disableVideo('" + publisher + "')");
                // document.getElementById("video-button-" + publisher.id).innerHTML = "Disable Video";
                document.getElementById("video-button-" + publisher.id).classList.remove('fa-video-slash');
                document.getElementById("video-button-" + publisher.id).classList.add('fa-video');

            }

            function disconnectUser(connectionId){
                if (session.capabilities.forceDisconnect == 1) {
                    //alert(connectionId);
                    session.forceDisconnect(allConnections[connectionId]);
                } else {
                    alert("No permission");
                }
            }

            // Send a signal once the user enters data in the form
            form.addEventListener('submit', function submit(event) {
                var senderRole = session.connection.permissions.publish ? 'PUBLISHER' : 'SUBSCRIBER';
                event.preventDefault();
                $.ajax({
                    url: '/meeting/oneonone/chat',
                    type: 'POST',
                    contentType: 'application/json', // send as JSON
                    data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId, 'to': 'All', 'msg': msgTxt.value, 'from_role': senderRole}),
                    complete: function complete() {
                        // called when complete
                        //console.log('chat function completed');
                    },
                    success: function success() {
                        // called when successful
                        //console.log('successfully called chat');
                    },
                    error: function error() {
                        // called when there is an error
                        //console.log('error calling chat');
                    }
                });
                session.signal({
                    type: 'msg',
                    data: username.value + ': ' + msgTxt.value
                }, function signalCallback(error) {
                    if (error) {
                        //console.error('Error sending signal:', error.name, error.message);
                    } else {
                        msgTxt.value = '';
                    }
                });
            });

            initializeSession();

            function leaveMeeting(){
                window.open('/meeting/oneonone/studentFeedback?sid=<?=$slotId?>', '_self');
               //window.open('/test/thankyou', '_self');
            }

            var all_questions = <?=$polling_questions?>;
            //console.log(all_questions);

            var Quiz = function(quiz_name) {
                // Private fields for an instance of a Quiz object.
                this.quiz_name = quiz_name;
                // This one will contain an array of Question objects in the order that the questions will be presented.
                this.questions = [];
            }

            Quiz.prototype.add_question = function(question) {
                this.questions.push(question);
            }

            Quiz.prototype.render = function(container) {
                // For when we're out of scope
                var self = this;

                // Hide the quiz results modal
                $('#quiz-results').hide();

                // Write the name of the quiz
                $('#quiz-name').text(this.quiz_name);

                // Create a container for questions
                var question_container = $('<div>').attr('id', 'question').insertAfter('#quiz-name');

                // Helper function for changing the question and updating the buttons
                function change_question() {
                    self.questions[current_question_index].render(question_container);
                    $('#prev-question-button').prop('disabled', current_question_index === 0);
                    $('#next-question-button').prop('disabled', current_question_index === self.questions.length - 1);

                    // Determine if all questions have been answered
                    var all_questions_answered = true;
                    for (var i = 0; i < self.questions.length; i++) {
                        if (self.questions[i].user_choice_index === null) {
                            all_questions_answered = false;
                            break;
                        }
                    }
                    $('#submit-button').prop('disabled', !all_questions_answered);
                }

                // Render the first question
                var current_question_index = 0;
                change_question();

                // Add listener for the previous question button
                $('#prev-question-button').click(function() {
                    if (current_question_index > 0) {
                        current_question_index--;
                        change_question();
                    }
                });

                // Add listener for the next question button
                $('#next-question-button').click(function() {
                    if (current_question_index < self.questions.length - 1) {
                        self.questions[current_question_index].session_id = sessionId;
                        jQuery.ajax({
                            type: "POST",
                            contentType: "application/json",
                            data: JSON.stringify(self.questions[current_question_index]),
                            url: "/meeting/oneonone/poll",
                          success: function(response) {
                                console.log(response);
                            }
                        });
                        current_question_index++;
                        change_question();
                    }
                });

                // Add listener for the submit answers button
                $('#submit-button').click(function() {
                    // Determine how many questions the user got right
                    var score = 0;
                    for (var i = 0; i < self.questions.length; i++) {
                        if (self.questions[i].user_choice_index === self.questions[i].correct_choice_index) {
                            score++;
                        }
                        self.questions[i].session_id = sessionId;
                    }

                    //console.log(self.questions);
                    jQuery.ajax({
                        type: "POST",
                        contentType: "application/json",
                        data: JSON.stringify(self.questions[self.questions.length - 1]),
                        url: "/meeting/oneonone/poll",
                      success: function(response) {
                            console.log(response);
                        }
                    });

                    // Display the score with the appropriate message
                    var percentage = score / self.questions.length;
                    //console.log(percentage);
                    var message;
                    message = "Thank you for your valuable feedback";
                    $('#quiz-results-message').text(message);
                    //$('#quiz-results-score').html('You got <b>' + score + '/' + self.questions.length + '</b> questions correct.');
                    $('#quiz-results').slideDown();
                    $('#submit-button').slideUp();
                    $('#next-question-button').slideUp();
                    $('#prev-question-button').slideUp();
                    $('#quiz-retry-button').slideDown();

                });

                // Add a listener on the questions container to listen for user select changes. This is for determining whether we can submit answers or not.
                question_container.bind('user-select-change', function() {
                    var all_questions_answered = true;
                    for (var i = 0; i < self.questions.length; i++) {
                        if (self.questions[i].user_choice_index === null) {
                            all_questions_answered = false;
                            break;
                        }
                    }
                    $('#submit-button').prop('disabled', !all_questions_answered);
                });
            }

            // An object for a Question, which contains the question, the correct choice, and wrong choices. This block is the constructor.
            var Question = function(question_obj) {
                var question_string = question_obj.question_string;
                var correct_choice = question_obj.choices.correct;
                var wrong_choices = question_obj.choices.wrong;
                var question_id = question_obj.id;

                this.question_string = question_string;
                this.choices = [];
                this.user_choice_index = null; // Index of the user's choice selection
                this.correct_ans = correct_choice;
                this.question_id = question_id;
                this.user_ans = null;

                // Random assign the correct choice an index
                this.correct_choice_index = Math.floor(Math.random() * (wrong_choices.length + 1));
                //console.log(this.correct_choice_index, wrong_choices.length + 1);

                // Fill in this.choices with the choices
                var number_of_choices = wrong_choices.length + 1;
                for (var i = 0; i < number_of_choices; i++) {
                    if (i === this.correct_choice_index) {
                        this.choices[i] = correct_choice;
                    }
                    else {
                        // Randomly pick a wrong choice to put in this index
                        var wrong_choice_index = Math.floor(Math.random() * (wrong_choices.length));;
                        this.choices[i] = wrong_choices[wrong_choice_index];

                        // Remove the wrong choice from the wrong choice array so that we don't pick it again
                        wrong_choices.splice(wrong_choice_index, 1);
                    }
                }
            }

            Question.prototype.render = function(container) {
                // For when we're out of scope
                var self = this;

                // Fill out the question label
                var question_string_h2;
                if (container.children('h2').length === 0) {
                    question_string_h2 = $('<h2>').appendTo(container);
                }
                else {
                    question_string_h2 = container.children('h2').first();
                }
                question_string_h2.text(this.question_string);

                // Clear any radio buttons and create new ones
                if (container.children('input[type=radio]').length > 0) {
                    container.children('input[type=radio]').each(function() {
                        var radio_button_id = $(this).attr('id');
                        $(this).remove();
                        container.children('label[for=' + radio_button_id + ']').remove();
                    });
                }
                for (var i = 0; i < this.choices.length; i++) {
                    // Create the radio button
                    var choice_radio_button = $('<input>')
                    .attr('id', 'choices-' + i)
                    .attr('type', 'radio')
                    .attr('name', 'choices')
                    .attr('value', 'choices-' + i)
                    .attr('checked', i === this.user_choice_index)
                    .appendTo(container);

                    // Create the label
                    var choice_label = $('<label>')
                    .text(this.choices[i])
                    .attr('for', 'choices-' + i)
                    .appendTo(container);
                }

                // Add a listener for the radio button to change which one the user has clicked on
                $('input[name=choices]').change(function(index) {
                    var selected_radio_button_value = $('input[name=choices]:checked').val();

                    // Change the user choice index
                    self.user_choice_index = parseInt(selected_radio_button_value.substr(selected_radio_button_value.length - 1, 1));
                    self.user_ans = self.choices[self.user_choice_index];

                    // Trigger a user-select-change
                    container.trigger('user-select-change');
                });
            }

            // "Main method" which will create all the objects and render the Quiz.
            $(document).ready(function() {
                // Create an instance of the Quiz object
                var quiz = new Quiz('ANWER THE FOLLOWING');

                // Create Question objects from all_questions and add them to the Quiz object
                for (var i = 0; i < all_questions.length; i++) {
                    // Create a new Question object
                    var question = new Question(all_questions[i]);

                    // Add the question to the instance of the Quiz object that we created previously
                    quiz.add_question(question);
                }

                // Render the quiz
                var quiz_container = $('#quiz');
                quiz.render(quiz_container);
            });

            var meetingCountdown = parseInt('<?=$timer?>') + new Date().getTime();
            // Update the count down every 1 second
            var y = setInterval(function(){
                // Get today's date and time
                var now = new Date().getTime();

                // Find the distance between now and the count down date
                var distance = meetingCountdown - now;

                // Time calculations for days, hours, minutes and seconds
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                if(distance >= 0){
                    document.getElementById("meeting-timer").innerHTML = "<b>Time</b> " + minutes + "m " + seconds + "s ";
                }
                // If the count down is over, write some text
                if (distance < 0)
                {
                    //window.location.href = '/meeting/oneonone/studentFeedback?sid=<?=$slotId?>';
                    window.location.reload();
                }
            }, 1000);

            function showBrochureModal(){
                document.getElementById("brochures").style.display = 'block';
            }

            function closeBrochureModal(){
                document.getElementById("brochures").style.display = 'none';
            }

            function downloadBrochure(value){
                window.location.href = '/meeting/oneonone/download?t=' + value;
            }
        </script>
    </body>
</html>
