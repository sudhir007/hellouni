<head>
    <title>Hello Uni : Imperial's MEGA Virtual EduFair 2021</title>
</head>
<style type="text/css">
    @import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");
    .login-block{
        background: #004b7a;  /* fallback for old browsers */
        background: -webkit-linear-gradient(to bottom, #fafafb, #d8d9da);  /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to bottom, #fafafb, #d8d9da); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
        float:left;
        width:100%;
        padding : 50px 0;
    }
    .login-sec{padding: 50px 30px; position:relative;}
    .login-sec .copy-text{position:absolute; width:80%; bottom:20px; font-size:13px; text-align:center;}
    .login-sec h2{margin-bottom:30px; font-weight:800; font-size:30px; color: #004b7a;}
    .login-sec h2:after{content:" "; width:100px; height:5px; background:#f9992f; display:block; margin-top:20px; border-radius:3px; margin-left:auto;margin-right:auto}
    .btn-login{background: #f9992f; color:#fff; font-weight:600;}
    .form-control{
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    }
    .table>tbody>tr>td{
        text-align: center;
        padding: 10px;
        line-height: 1.42857143;
        vertical-align: middle;
        border-top: 1px solid #ddd;
        font-size: 14px;
        font-family: sans-serif;
        letter-spacing: 0.8px;
        font-weight: 600;
        width: 51%;
    }

    fieldset {
        padding: .35em .625em .75em;
        margin: 0 2px;
        border: 1px solid silver;
    }

    legend {
        padding: 0;
        border: 0;
        margin-bottom: 20px;
        width: unset;
    }
</style>

 <section class="login-block" id="forgot-password-page">
    <div class="container" style="background:#fff; border-radius: 10px; box-shadow: 5px 5px 0px rgba(0,0,0,0.1);">
        <div class="row banner-secLogo">
            <img src="<?php echo base_url();?>application/images/Webinar_Reg_Top_Banner.jpg" class="img-fluid"  alt="Responsive Image" width="100%" height="80%" />
        </div>

        <div class="row">
            <div class="col-md-6 login-sec">
                <h2 class="text-center">Register For The Fair!</h2>
                <form class="login-form" class="form-horizontal" onsubmit="resgistration()" method="post" autocomplete="off">
                    <fieldset>
                        <legend>Personal Detail:</legend>
                        <div class="col-md-12 form-group">
                            <div class="col-md-6">
                                <label for="first_name" class="text-uppercase">First Name<font color="red">*</font></label>
                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" required>
                            </div>

                            <div class="col-md-6">
                                <label for="last_name" class="text-uppercase">Last Name<font color="red">*</font></label>
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" required>
                            </div>
                        </div>

                        <div class="col-md-12 form-group">
                            <div class="col-md-6">
                                <label for="user_email" class="text-uppercase">Email<font color="red">*</font></label>
                                <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email Id" required>
                            </div>

                            <div class="col-md-6">
                                <label for="password" class="text-uppercase">Password<font color="red">*</font></label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required autocomplete="off">
                            </div>
                        </div>

                        <div class="col-md-12 form-group">
                            <div class="col-md-6">
                                <label for="user_mobile" class="text-uppercase">Mobile Number<font color="red">*</font></label>
                                <input type="text" class="form-control" id="user_mobile" name="user_mobile" placeholder="Mobile Number" required>
                            </div>
                            <div class="col-md-3">
                                <label for="otp_number" class="text-uppercase">OTP<font color="red">*</font></label><br>
                                <input type="text" class="form-control" id="otp_number" name="otp_number" placeholder="OTP Sent On Mobile" required>
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-primary" id="verifyotp" style="margin-top: 25px; display:none;">Verify</button>
                                <button type="button" class="btn btn-primary" id="send_otp" style="margin-top: 25px;" onclick="sendOTP()">Send OTP</button>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset>
                        <legend>Academic Detail For Better Profile Evaluation:</legend>
                        <div class="col-md-12 form-group">
                            <div class="col-md-6 form-group">
                                <label for="cschool" class="text-uppercase">Current School / College you are studying at or have graduated from?<font color="red">*</font></label>
                                <select name="cschool" id="cschool" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" onchange="checkOther(this.value)" required>
                                    <option value="">Select School / College</option>
                                    <?php
                                    foreach($colleges as $college)
                                    {
                                        ?>
                                        <option value="<?php echo $college['college'];?>" <?php if(isset($selected_college) && $selected_college==$college['college']){echo 'selected="selected"';}?> ><?php echo strtoupper($college['college']);?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-md-6 form-group" id="other_college" style="display:none;">
                                <label for="other_cschool" class="text-uppercase" style="height :50px;"> Other College / School </label>
                                <input type="text" class="form-control" id="other_cschool" name="other_cschool" placeholder="Other College / School" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-12 form-group">
                            <div class="col-md-6 form-group">
                                <label for="desired_country" class="text-uppercase" > Desired Country<font color="red">*</font> </label>
                                <select name="desired_country" id="desired_country" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                                    <option value="0">Select Country</option>
                                    <?php
                                    foreach($countries as $country)
                                    {
                                        ?>
                                        <option value="<?php echo $country->name;?>" <?php if(isset($selected_country) && $selected_country==$country->name){echo 'selected="selected"';}?> ><?php echo strtoupper($country->name);?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-md-6 form-group">
                                <label for="desired_mainstream" class="text-uppercase"> Desired Course<font color="red">*</font> </label>
                                <select name="desired_mainstream" id="desired_mainstream" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                                <option value="">Select Course</option>
                                <?php
                                foreach($mainstreams as $course)
                                {
                                    ?>
                                    <option value="<?php echo $course->id;?>"><?php echo strtoupper($course->name);?></option>
                                    <?php
                                }
                                ?>
                                </select>
                            </div>
                         </div>
                        <div class="col-md-12 form-group">
                            <div class="col-md-6 form-group">
                                <label for="intakeyear" class="text-uppercase"> Intake Year<font color="red">*</font> </label>
                                <Select name="intakeyear" id="intakeyear" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                                    <option value="">Select Year</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                </Select>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="intakemonth" class="text-uppercase"> Intake Month </label>
                                <Select name="intakemonth" id="intakemonth" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;">
                                    <option value="">Select Month</option>
                                    <option value="January"> January</option>
                                    <option value="February"> February </option>
                                    <option value="March"> March </option>
                                    <option value="April">April </option>
                                    <option value="May">May</option>
                                    <option value="June">June</option>
                                    <option value="July">July</option>
                                    <option value="August">August</option>
                                    <option value="September">September</option>
                                    <option value="October">October</option>
                                    <option value="November">November</option>
                                    <option value="December">December</option>
                                </Select>
                            </div>
                         </div>
                        <div class="col-md-12 form-group">
                            <div class="col-md-6 form-group">
                                <label for="competativeexam" class="text-uppercase"> Competitive Exams (GRE / GMAT)  </label>
                                <Select name="competativeexam" id="competativeexam" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" onchange="checkComExam(this.value)">
                                    <option value="">Select Option</option>
                                    <option value="GRE"> GRE</option>
                                    <option value="GMAT"> GMAT </option>
                                    <option value="NOTGIVEN"> NOT GIVEN </option>
                                </Select>
                            </div>

                            <div class="col-md-6 form-group" id="competativeexamscored">
                                <label for="competativeexamscore" class="text-uppercase"> Gre / GMAT Score </label>
                                <input type="text" class="form-control" id="competativeexamscore" name="competativeexamscore" placeholder="254..">
                            </div>
                        </div>
                        <div class="col-md-12 form-group">
                            <div class="col-md-6 form-group">
                                <label for="gpaexam" class="text-uppercase"> Aggregate GPA  </label>
                                <Select name="gpaexam" id="gpaexam" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" onchange="checkGpa(this.value)">
                                    <option value="">Select Option</option>
                                    <option value="4"> Out Of 4</option>
                                    <option value="5"> Out Of 5 </option>
                                    <option value="7"> Out Of 7 </option>
                                    <option value="10"> Out Of 10 </option>
                                    <option value="20"> Out Of 20 </option>
                                    <option value="100"> Out Of 100 </option>
                                </Select>
                            </div>

                            <div class="col-md-6 form-group"  id="gpaexamscored">
                                <label for="gpaexamscore" class="text-uppercase"> GPA Score </label>
                                <input type="text" class="form-control" id="gpaexamscore" name="gpaexamscore" placeholder="254..">
                            </div>
                        </div>
                        <div class="col-md-12 form-group">
                            <div class="col-md-6 form-group">
                                <label for="workexperience" class="text-uppercase"> Work Experience  </label>
                                    <Select name="workexperience" id="workexperience" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" onchange="checkWorkExp(this.value)">
                                    <option value="">Select Option</option>
                                    <option value="Yes"> Yes</option>
                                    <option value="No"> No </option>
                                </Select>
                            </div>

                            <div class="col-md-6 form-group"  id="workexperiencemonthsd">
                                <label for="workexperiencemonths" class="text-uppercase"> Work Experience In months </label>
                                <input type="text" class="form-control" id="workexperiencemonths" name="workexperiencemonths" placeholder="24">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="desired_subcourse" class="text-uppercase"> Desired Specialization &nbsp;&nbsp; </label>
                                <select name="desired_subcourse" id="desired_subcourse" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;">
                                    <option value="">Select Specialization</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 form-group">
                            <div class="col-md-6 form-group">
                                <label for="desired_levelofcourse" class="text-uppercase"> Desired Level Of Course &nbsp;&nbsp; </label>
                                <select name="desired_levelofcourse" id="desired_levelofcourse" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;">
                                    <option value="">Select Level Of Course</option>
                                    <?php
                                    foreach($degrees as $degree)
                                    {
                                        ?>
                                        <option value="<?php echo $degree->id;?>" ><?php echo strtoupper($degree->name);?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="yearstudy" class="text-uppercase"> Current Year Of Study  </label>
                                <Select name="yearstudy" id="yearstudy" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;">
                                    <option value="">Select Option</option>
                                    <option value="1"> 1st Year </option>
                                    <option value="2"> 2nd Year </option>
                                    <option value="3"> 3rd Year </option>
                                    <option value="4"> 4th Year </option>
                                    <option value="5"> 5th Year </option>
                                    <option value="-1"> Already Graduated </option>
                                </Select>
                            </div>
                        </div>
                        <div class="col-md-12 form-group">
                            <div class="col-md-6 form-group">
                                <label for="ccity" class="text-uppercase"> Which City are you currently residing in?  </label>
                                <input type="text" class="form-control" id="ccity" name="ccity" placeholder="City.." autocomplete="off">
                            </div>
                        </div>
                    </fieldset>

                    <div class="form-check" style="text-align: center;">
                        <!--button type="button" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-warning" id="reset" onclick="window.location.reload();">Reset</button-->
                        <input type="submit"  style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Register & Enter"></input>
                    </div>
                </form>
            </div>

            <div class="col-md-6 login-sec">
                <h2 class="text-center">Already Registered? Sign In!</h2>
                <form class="login-form" class="form-horizontal" onsubmit="login()" method="post" autocomplete="off">
                    <div class="col-md-12 form-group">
                        <div class="col-md-6">
                            <label for="user_mobile_signin" class="text-uppercase">Mobile Number<font color="red">*</font></label>
                            <input type="text" class="form-control" id="user_mobile_signin" name="user_mobile_signin" placeholder="Mobile Number" required>
                        </div>

                        <div class="col-md-6">
                            <label for="password_signin" class="text-uppercase">Password<font color="red">*</font></label>
                            <input type="password" class="form-control" id="password_signin" name="password_signin" placeholder="Password" required autocomplete="off">
                        </div>
                    </div>

                    <div class="form-check" style="text-align: center;">
                        <input type="submit"  style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Enter The Fair"></input>
                    </div>
                </form>
                <img src="<?php echo base_url();?>application/images/Webinar_Reg_Middle_Banner.jpg" class="img-fluid"  alt="Responsive Image" width="100%" height="100%" style="margin-top: 5%;" />
            </div>

            <!--div class="col-md-6 login-sec">
                <img src="<?php echo base_url();?>application/images/Webinar_Reg_Middle_Banner.jpg" class="img-fluid"  alt="Responsive Image" width="100%" height="100%" style="margin-top: 5%;" />
            </div-->
        </div>

        <div id="secondBanner" class="row banner-secLogo">
            <img src="<?php echo base_url();?>application/images/Webinar_Reg_Bottom_Banner.jpg" class="img-fluid"  alt="Responsive Image" width="100%" height="80%" />
            <h2 style="text-align: center;color: #59419a;"> 2021 Participants Include Among Many Others... </h2>
            <div class="col-md-12 table-responsive">
                <div class="col-md-6 table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                            <tr><td>University of Illinois, Urbana-Champaign (School of Information Sciences)</td></tr>
                            <tr><td>Rochester Institute of Technology</td></tr>
                            <tr><td>Texas Tech University</td></tr>
                            <tr><td>University of Illinois Springfield</td></tr>
                            <tr><td>The University of Tennessee Knoxville</td></tr>
                            <tr><td>DePaul University</td></tr>
                            <tr><td>Savannah College of Art and Design</td></tr>
                            <tr><td>Lawrence Technological University</td></tr>
                            <tr><td>University of Plymouth</td></tr>
                            <tr><td>Abertay University</td></tr>
                            <tr><td>New Jersey Institute of Technology</td></tr>
                            <tr><td>Oklahoma State University</td></tr>
                            <tr><td>Pace University</td></tr>
                            <tr><td>San Francisco State University</td></tr>
                            <tr><td>Keck Graduate Institute</td></tr>
                            <tr><td>Deakin University</td></tr>
                            <tr><td>University of Wollongong</td></tr>
                            <tr><td>SRH University Heidelberg</td></tr>
                            <tr><td>University of Wisconsin-Milwaukee</td></tr>
                            <tr><td>Florida Institute of Technology</td></tr>
                            <tr><td>State University of New York at Geneseo</td></tr>
                            <tr><td>Hult International Business School</td></tr>
                            <tr><td>Middle Tennessee State University</td></tr>
                            <tr><td>Webster University</td></tr>
                            <tr><td>Florida Polytechnic University</td></tr>
                            <tr><td>California State University Stanislaus</td></tr>
                            <tr><td>Mercy College</td></tr>
                            <tr><td>St Mary's University</td></tr>
                            <tr><td>College of the Canyons</td></tr>
                        </tbody>
                    </table>
                </div>

                <div class="col-md-6 table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                            <tr><td>Indiana University - Purdue University Indianapolis</td></tr>
                            <tr><td>Stony Brook University</td></tr>
                            <tr><td>Arizona State University</td></tr>
                            <tr><td>University of Texas at Arlington</td></tr>
                            <tr><td>Binghamton University</td></tr>
                            <tr><td>New York Institute of Technology </td></tr>
                            <tr><td>Claremont Graduate University</td></tr>
                            <tr><td>University of Louisville (School of Public Health & Information Sciences)</td></tr>
                            <tr><td>University of Gloucestershire</td></tr>
                            <tr><td>Glasgow Caledonian University</td></tr>
                            <tr><td>University of Kentucky</td></tr>
                            <tr><td>Dallas Baptist University</td></tr>
                            <tr><td>State University of New York Polytechnic Institute</td></tr>
                            <tr><td>University of Indianapolis</td></tr>
                            <tr><td>SUNY Albany</td></tr>
                            <tr><td>Flinders University</td></tr>
                            <tr><td>Queensland University of Technology</td></tr>
                            <tr><td>ISM International School of Management Germany</td></tr>
                            <tr><td>University of Connecticut(UG Only)</td></tr>
                            <tr><td>Troy University</td></tr>
                            <tr><td>Western New England University</td></tr>
                            <tr><td>University of Applied Sciences Europe</td></tr>
                            <tr><td>Augustana University</td></tr>
                            <tr><td>Concordia University Chicago</td></tr>
                            <tr><td>North Central College</td></tr>
                            <tr><td>The University of North Texas</td></tr>
                            <tr><td>American University in Dubai</td></tr>
                            <tr><td>North Park University</td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $( document ).ready(function() {
        $('#desired_mainstream').on('change', function() {
            var dataString = 'id='+this.value;
            $.ajax({
                type: "POST",
                url: "/search/university/getSubcoursesList",
                data: dataString,
                cache: false,
                success: function(result){
                    $('#desired_subcourse').html(result);
                }
            });
        });

        // verify otp_number
        $('#verifyotp').on('click', function() {
            var mobileNumber = $('#user_mobile').val();
            var otpNumber = $('#otp_number').val();
            var dataString = 'mobile_number=' + mobileNumber + '&otp=' + otpNumber;
            $.ajax({
                type: "POST",
                url: "/event/verify_otp",
                data: dataString,
                cache: false,
                success: function(result){
                    result = JSON.parse(result);
                    alert(result['message']);
                }
            });
        });
    })

    //registration

    function resgistration(){
        var first_name = $('#first_name').val();
        var last_name = $('#last_name').val();
        var user_email = $('#user_email').val();
        var user_mobile = $('#user_mobile').val();
        var desired_country = $('#desired_country').val();
        var desired_mainstream = $('#desired_mainstream').val();
        var desired_subcourse = $('#desired_subcourse').val();
        var ccity = $('#ccity').val();
        var cschool = $('#cschool').val();
        var othercschool = $('#other_cschool').val();
        var desired_levelofcourse = $('#desired_levelofcourse').val();
        var userPassword = $('#password').val();
        var intake_month = $('#intakemonth').val();
        var intake_year = $('#intakeyear').val();
        var comp_exam = $('#competativeexam').val();
        var comp_exam_score = $('#competativeexamscore').val();
        var gpa_exam = $('#gpaexam').val();
        var gpa_exam_score= $('#gpaexamscore').val();
        var work_exp = $('#workexperience').val();
        var work_exp_month = $('#workexperiencemonths').val();
        var otpNumber = $('#otp_number').val();
        var study_year = $('#yearstudy').val();

        var dataString = 'first_name=' + first_name + '&last_name=' + last_name + '&user_email=' + user_email + '&user_mobile=' + user_mobile + '&desired_country=' + desired_country + '&desired_mainstream=' + desired_mainstream + '&desired_subcourse=' + desired_subcourse + '&desired_levelofcourse=' + desired_levelofcourse + '&user_password=' + userPassword + '&ccity=' + ccity + '&cschool=' + cschool + '&other_cschool=' + othercschool + '&intake_month=' + intake_month + "&intake_year=" + intake_year + '&comp_exam=' + comp_exam + '&comp_exam_score=' + comp_exam_score + "&gpa_exam=" + gpa_exam + "&gpa_exam_score=" + gpa_exam_score + "&work_exp=" + work_exp + "&work_exp_month=" + work_exp_month + "&otp=" + otpNumber + "&study_year=" + study_year;

        event.preventDefault();

        $.ajax({
            type: "POST",
            url: "/event/virtualfair_registration",
            data: dataString,
            cache: false,
            success: function(result){
                result = JSON.parse(result);
                if(result['success']){
                    window.location.href='/event/reception/' + result['encodedUsername'];
                    //alert("Congratulation!! You have registered successfully. Please come back on event day");
                }
                else{
                    alert(result['message']);
                }
            }
        });
    }

    function login(){
        //alert("Please come back on event day");
        var user_mobile = $('#user_mobile_signin').val();
        var userPassword = $('#password_signin').val();

        var dataString = 'user_mobile=' + user_mobile + '&user_password=' + userPassword;

        event.preventDefault();

        $.ajax({
            type: "POST",
            url: "/event/virtualfair_login",
            data: dataString,
            cache: false,
            success: function(result){
                result = JSON.parse(result);
                if(result['success']){
                    //window.location.href='/event/reception/' + result['encodedUsername'];
                    //alert("Congratulation!! You have registered successfully. Please come back on event day");
                    window.location.href='/event/reception/' + result['encodedUsername'];
                }
                else{
                    alert(result['message']);
                }
            }
        });
    }

    // sent OTP
    function sendOTP() {
        var mobileNumber = $('#user_mobile').val();
        var mobilenumberlength = $('#user_mobile').val().length;
        var userEmail = $('#user_email').val();

        if(mobilenumberlength != 10) {
            alert("Please enter valid mobile!!!");
        }
        else if(!userEmail) {
            alert("Please enter email!!!");
        }
        else {
            var dataString = 'mobile_number=' + mobileNumber + '&user_email=' + userEmail;

            $.ajax({
                type: "POST",
                url: "/event/send_otp",
                data: dataString,
                cache: false,
                success: function(result){
                    result = JSON.parse(result);
                    alert(result['message']);
                    document.getElementById("send_otp").style.display = "none";
                    document.getElementById("verifyotp").style.display = "block";
                }
            });
        }
    }

    function checkOther(value){
        document.getElementById("other_college").style.display = "none";
        if(value == 'Other'){
            document.getElementById("other_college").style.display = "block";
        }
    }

    function checkComExam(value){
        document.getElementById("competativeexamscored").style.display = "block";
        if(value == 'NOTGIVEN'){
            document.getElementById("competativeexamscored").style.display = "none";
        }
    }

    function checkGpa(value){
        document.getElementById("gpaexamscored").style.display = "block";
        if(value == 'NOTGIVEN'){
            document.getElementById("gpaexamscored").style.display = "none";
        }
    }

    function checkWorkExp(value){
        document.getElementById("workexperiencemonthsd").style.display = "none";
        if(value == 'Yes'){
            document.getElementById("workexperiencemonthsd").style.display = "block";
        }
    }

    $(function() {
        $('#levelselector').change(function(){
            $('.levelClass').hide();
            $('#' + $(this).val()).show();
        });
    });
</script>
