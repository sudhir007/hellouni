<style type="text/css">
    @import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");
    .login-block{
        background: #004b7a;  /* fallback for old browsers */
        background: -webkit-linear-gradient(to bottom, #fafafb, #d8d9da);  /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to bottom, #fafafb, #d8d9da); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
        float:left;
        width:100%;
        padding : 50px 0;
    }
    .login-sec{padding: 50px 30px; position:relative;}
    .login-sec .copy-text{position:absolute; width:80%; bottom:20px; font-size:13px; text-align:center;}
    .login-sec h2{margin-bottom:30px; font-weight:800; font-size:30px; color: #004b7a;}
    .login-sec h2:after{content:" "; width:100px; height:5px; background:#f9992f; display:block; margin-top:20px; border-radius:3px; margin-left:auto;margin-right:auto}
    .btn-login{background: #f9992f; color:#fff; font-weight:600;}
    .form-control{
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    }
    .table>tbody>tr>td{
        text-align: center;
        padding: 10px;
        line-height: 1.42857143;
        vertical-align: middle;
        border-top: 1px solid #ddd;
        font-size: 14px;
        font-family: sans-serif;
        letter-spacing: 0.8px;
        font-weight: 600;
        width: 51%;
    }

    fieldset {
        padding: .35em .625em .75em;
        margin: 0 2px;
        border: 1px solid silver;
    }

    legend {
        padding: 0;
        border: 0;
        margin-bottom: 20px;
        width: unset;
    }

    .multiselect {
  width: auto;
  margin: 5px;
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 100%;
  font-weight: bold;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes label {
  display: block;
}

#checkboxes label:hover {
  background-color: #1e90ff;
}
</style>

 <section class="login-block" id="forgot-password-page">
    <div class="container" style="background:#fff; border-radius: 10px; box-shadow: 5px 5px 0px rgba(0,0,0,0.1);">
        <div class="row banner-secLogo">
            <img src="<?php echo base_url();?>application/images/uta_7oct_event.jpg" class="img-fluid"  alt="UT Arlington - Register for UTA Day being held on 07th Oct 2021" width="100%" height="80%" />
        </div>

        <div class="row">
            <div class="col-md-12 login-sec">
                <h2 class="text-center">Register For The UTA Information Session Day !</h2>
                <form class="login-form" class="form-horizontal" onsubmit="resgistration()" method="post" autocomplete="off">
                    <fieldset>
                        <legend>Personal Detail:</legend>
                        <div class="col-md-12 form-group">
                            <div class="col-md-6">
                                <label for="first_name" class="text-uppercase">First Name<font color="red">*</font></label>
                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" required>
                            </div>

                            <div class="col-md-6">
                                <label for="last_name" class="text-uppercase">Last Name<font color="red">*</font></label>
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" required>
                            </div>
                        </div>



                        <div class="col-md-12 form-group">
                            <div class="col-md-6">
                                <label for="user_mobile" class="text-uppercase">Mobile Number<font color="red">*</font></label>
                                <input type="text" class="form-control" id="user_mobile" name="user_mobile" placeholder="Mobile Number" required>
                            </div>
                            <div class="col-md-6">
                                <label for="user_email" class="text-uppercase">Email<font color="red">*</font></label>
                                <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email Id" required>
                            </div>
                            <!--div class="col-md-3">
                                <label for="otp_number" class="text-uppercase">OTP<font color="red">*</font></label><br>
                                <input type="text" class="form-control" id="otp_number" name="otp_number" placeholder="OTP Sent On Mobile" required>
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-primary" id="verifyotp" style="margin-top: 25px; display:none;">Verify</button>
                                <button type="button" class="btn btn-primary" id="send_otp" style="margin-top: 25px;" onclick="sendOTP()">Send OTP</button>
                            </div-->
                        </div>

                        <div class="col-md-12 form-group">


                            <!--div class="col-md-6">
                                <label for="password" class="text-uppercase">Password<font color="red">*</font></label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required autocomplete="off">
                            </div-->
                        </div>
                    </fieldset>

                    <fieldset>
                        <legend>Academic Detail For Better Profile Evaluation:</legend>
                        <div class="col-md-12 form-group">
                            <div class="col-md-6 form-group">
                                <label for="yearstudy" class="text-uppercase"> Which Year are you currently studying in?</label>
                                <Select name="yearstudy" id="yearstudy" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;">
                                    <option value="">Select Option</option>
                                    <option value="1"> 1st Year </option>
                                    <option value="2"> 2nd Year </option>
                                    <option value="3"> 3rd Year </option>
                                    <option value="4"> 4th Year </option>
                                    <option value="5"> 5th Year </option>
                                    <option value="-1"> Already Graduated </option>
                                </Select>
                            </div>
                            <div class="col-md-6 form-group" >
                                <label for="current_stream" class="text-uppercase" > Current Stream / Department ? Or if already graduated, your major?</label>
                                <input type="text" class="form-control" id="current_stream" name="current_stream" placeholder="Stream / Department are you currently studying in" autocomplete="off">
                            </div>
                        </div>
                        <!--div class="col-md-12 form-group">
                            <div class="col-md-6 form-group">
                                <label for="desired_country" class="text-uppercase" > Are you currently enrolled with Imperial?</label><br>

                                  <Select name="enrolled" id="enrolled" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;">
                                      <option value="YES">Yes</option>
                                      <option value="NO">No</option>
                                  </Select>
                            </div>

                            <div class="col-md-6 form-group">
                                <label for="counselling_branch" class="text-uppercase"> Imperial Branch Most convenient for you? </label>
                                <select name="counselling_branch" id="counselling_branch" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;">
                                <option value="SANTACRUZ">Mumbai - Santacruz</option>
                                <option value="BORIVALI">Mumbai - Borivali</option>
                                <option value="THANE">Mumbai - Thane</option>
                                <option value="HINJEWADI">Pune - Hinjewadi</option>
                                <option value="KOTHRUD">Pune - Kothrud</option>
                                <option value="OUTMUMBAI">Residing Outside Mumbai / Pune</option>
                                </select>
                            </div>
                        </div-->

                         <div class="col-md-12 form-group">
                             <div class="col-md-6 form-group">
                                 <label for="desired_mainstream" class="text-uppercase"> Desired Course</label>
                                 <select name="desired_mainstream" id="desired_mainstream" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;">
                                 <option value="">Select Course</option>
                                 <?php
                                 foreach($mainstreams as $course)
                                 {
                                     ?>
                                     <option value="<?php echo $course->name;?>"><?php echo strtoupper($course->name);?></option>
                                     <?php
                                 }
                                 ?>
                                 </select>
                             </div>

                             <div class="col-md-6 form-group">
                                 <label for="intakeyear" class="text-uppercase"> Intake Year</label>
                                 <Select name="intakeyear" id="intakeyear" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;">
                                     <option value="">Select Year</option>
                                     <option value="2022">2022</option>
                                     <option value="2023">2023</option>
                                     <option value="2024">2024</option>
                                     <option value="2025">2025</option>
                                 </Select>
                             </div>
                          </div>

                        <div class="col-md-12 form-group">
                            <div class="col-md-6 form-group">
                                <label for="intakemonth" class="text-uppercase"> Intake Month</label>
                                <Select name="intakemonth" id="intakemonth" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;">
                                    <option value="">Select Month</option>
                                    <option value="January"> January</option>
                                    <option value="February"> February </option>
                                    <option value="March"> March </option>
                                    <option value="April">April </option>
                                    <option value="May">May</option>
                                    <option value="June">June</option>
                                    <option value="July">July</option>
                                    <option value="August">August</option>
                                    <option value="September">September</option>
                                    <option value="October">October</option>
                                    <option value="November">November</option>
                                    <option value="December">December</option>
                                </Select>
                            </div>

                            <div class="col-md-6 form-group">
                                <label for="cschool" class="text-uppercase">Which UTA Department Members would you be interested in meeting?</label>

                                <div class="multiselect ">
                                    <div class="selectBox " onclick="showCheckboxes()">
                                      <select class="form-control searchoptions" name="uta_department" id="uta_department">
                                        <option>Select an option</option>
                                      </select>
                                      <div class="overSelect"></div>
                                    </div>
                                    <div id="checkboxes" style="padding: 1%;">
                                      <label for="one">
                                        <input type="checkbox" id="computer_science" />Computer Science</label>
                                      <label for="two">
                                        <input type="checkbox" id="electrical_engineering" />Electrical Engineering</label>
                                      <label for="three">
                                        <input type="checkbox" id="engineering_college" />College of Engineering (All Other Majors)</label>
                                        <label for="one">
                                        <input type="checkbox" id="business_college" />College of Business (All Other Majors)</label>
                                      <label for="two">
                                        <input type="checkbox" id="general_uta" />General UTA (Non-Engineering And Non-Business)</label>
                                      <label for="three">
                                        <input type="checkbox" id="OTHER" />Other</label>

                                    </div>
                                </div>
                            </div>




                        </div>


                        <div class="col-md-12 form-group">
                            <div class="col-md-6 form-group">
                                <label for="cschool" class="text-uppercase">Current School / College you are studying at or have graduated from?</label>
                                <select name="cschool" id="cschool" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" onchange="checkOther(this.value)">
                                    <option value="">Select School / College</option>
                                    <?php
                                    foreach($colleges as $college)
                                    {
                                        ?>
                                        <option value="<?php echo $college['college'];?>" <?php if(isset($selected_college) && $selected_college==$college['college']){echo 'selected="selected"';}?> ><?php echo strtoupper($college['college']);?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-6 form-group" id="other_college" style="display:none;">
                                <label for="other_cschool" class="text-uppercase"> Other College / School </label>
                                <input type="text" class="form-control" id="other_cschool" name="other_cschool" placeholder="Other College / School" autocomplete="off">
                            </div>
                        </div>

                    </fieldset>

                    <div class="form-check" style="text-align: center;">
                        <!--button type="button" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-warning" id="reset" onclick="window.location.reload();">Reset</button-->
                        <input type="submit"  style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Register"></input>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $( document ).ready(function() {
        $('#desired_mainstream').on('change', function() {
            var dataString = 'id='+this.value;
            $.ajax({
                type: "POST",
                url: "/search/university/getSubcoursesList",
                data: dataString,
                cache: false,
                success: function(result){
                    $('#desired_subcourse').html(result);
                }
            });
        });

        // verify otp_number
        $('#verifyotp').on('click', function() {
            var mobileNumber = $('#user_mobile').val();
            var otpNumber = $('#otp_number').val();
            var dataString = 'mobile_number=' + mobileNumber + '&otp=' + otpNumber;
            $.ajax({
                type: "POST",
                url: "/event/verify_otp",
                data: dataString,
                cache: false,
                success: function(result){
                    result = JSON.parse(result);
                    alert(result['message']);
                }
            });
        });
    })

    //registration

    function resgistration(){
        var first_name = $('#first_name').val();
        var last_name = $('#last_name').val();
        var user_email = $('#user_email').val();
        var user_mobile = $('#user_mobile').val();
        var userPassword = $('#password').val();
        var uta_department = [];
        var study_year = $('#yearstudy').val();
        var current_stream = $('#current_stream').val();
        var cschool = $('#cschool').val();
        var othercschool = $('#other_cschool').val();
        var enrolled = $('#enrolled').val();
        var counselling_branch = $('#counselling_branch').val();
        var desired_mainstream = $('#desired_mainstream').val();
        var intake_month = $('#intakemonth').val();
        var intake_year = $('#intakeyear').val();
        var otpNumber = $('#otp_number').val();

        if($('#computer_science').is(":checked"))
        {
            uta_department.push('computer_science');
        }
        if($('#electrical_engineering').is(":checked"))
        {
            uta_department.push('electrical_engineering');
        }
        if($('#engineering_college').is(":checked"))
        {
            uta_department.push('engineering_college');
        }
        if($('#business_college').is(":checked"))
        {
            uta_department.push('business_college');
        }
        if($('#general_uta').is(":checked"))
        {
            uta_department.push('general_uta');
        }

        uta_department = uta_department.toString();

        var dataString = 'first_name=' + first_name + '&last_name=' + last_name + '&user_email=' + user_email + '&user_mobile=' + user_mobile + '&desired_mainstream=' + desired_mainstream + '&user_password=' + userPassword + '&cschool=' + cschool + '&other_cschool=' + othercschool + '&intake_month=' + intake_month + "&intake_year=" + intake_year + "&otp=" + otpNumber + "&study_year=" + study_year + "&uta_department=" + uta_department + "&current_stream=" + current_stream + "&enrolled=" + enrolled + "&counselling_branch=" + counselling_branch;

        event.preventDefault();

        $.ajax({
            type: "POST",
            url: "/event/virtualfair_registration",
            data: dataString,
            cache: false,
            success: function(result){
                console.log(result);
                result = JSON.parse(result);
                if(result['success']){
                    //window.location.href='/event/reception/' + result['encodedUsername'];
                    alert("Congratulation!! You have registered successfully. Please come back on event day");
                }
                else{
                    alert(result['message']);
                }
            }
        });
    }

    function login(){
        //alert("Please come back on event day");
        var user_mobile = $('#user_mobile_signin').val();
        var userPassword = $('#password_signin').val();

        var dataString = 'user_mobile=' + user_mobile + '&user_password=' + userPassword;

        event.preventDefault();

        $.ajax({
            type: "POST",
            url: "/event/virtualfair_login",
            data: dataString,
            cache: false,
            success: function(result){
                result = JSON.parse(result);
                if(result['success']){
                    //window.location.href='/event/reception/' + result['encodedUsername'];
                    //alert("Congratulation!! You have registered successfully. Please come back on event day");
                    window.location.href='/event/reception/' + result['encodedUsername'];
                }
                else{
                    alert(result['message']);
                }
            }
        });
    }

    // sent OTP
    function sendOTP() {
        var mobileNumber = $('#user_mobile').val();
        var mobilenumberlength = $('#user_mobile').val().length;
        var userEmail = $('#user_email').val();

        if(mobilenumberlength != 10) {
            alert("Please enter valid mobile!!!");
        }
        else if(!userEmail) {
            alert("Please enter email!!!");
        }
        else {
            var dataString = 'mobile_number=' + mobileNumber + '&user_email=' + userEmail;

            $.ajax({
                type: "POST",
                url: "/event/send_otp",
                data: dataString,
                cache: false,
                success: function(result){
                    result = JSON.parse(result);
                    alert(result['message']);
                    document.getElementById("send_otp").style.display = "none";
                    document.getElementById("verifyotp").style.display = "block";
                }
            });
        }
    }

    function checkOther(value){
        document.getElementById("other_college").style.display = "none";
        if(value == 'Other'){
            document.getElementById("other_college").style.display = "block";
        }
    }

    function checkComExam(value){
        document.getElementById("competativeexamscored").style.display = "block";
        if(value == 'NOTGIVEN'){
            document.getElementById("competativeexamscored").style.display = "none";
        }
    }

    function checkGpa(value){
        document.getElementById("gpaexamscored").style.display = "block";
        if(value == 'NOTGIVEN'){
            document.getElementById("gpaexamscored").style.display = "none";
        }
    }

    function checkWorkExp(value){
        document.getElementById("workexperiencemonthsd").style.display = "none";
        if(value == 'Yes'){
            document.getElementById("workexperiencemonthsd").style.display = "block";
        }
    }

    $(function() {
        $('#levelselector').change(function(){
            $('.levelClass').hide();
            $('#' + $(this).val()).show();
        });
    });

    var expanded = false;

    function showCheckboxes() {
      var checkboxes = document.getElementById("checkboxes");
      if (!expanded) {
        checkboxes.style.display = "block";
        expanded = true;
      } else {
        checkboxes.style.display = "none";
        expanded = false;
      }
    }
</script>
