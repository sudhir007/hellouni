<html>
    <head>
        <title> Hellouni : Virtual Fair 2021 </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css">
        <link href="<?=base_url()?>css/newFairDesign.css?v=1.1" rel="stylesheet">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://static.opentok.com/v2/js/opentok.min.js"></script>
        <style type="text/css">
            body {
                width: 100%;
                height: 100%;
                background-color: #7e449a;
                background-image: url(/images/Fair2021/homePage.jpg);
                background-position: center bottom;
                background-repeat: no-repeat;
                background-size: contain;
            }
            .alert {
              padding: 10px;
              background-color: #f6882c;
              color: white;
                z-index: 10;
                bottom: 0;
                position: fixed;
                left: 0;
                text-align: center;
                display: none; /* Hidden by default */
            }

            .closebtn {
              margin-left: 15px;
              color: white;
              font-weight: bold;
              float: right;
              font-size: 22px;
              line-height: 20px;
              cursor: pointer;
              transition: 0.3s;
            }

            .closebtn:hover {
              color: black;
            }

            .btnpurp {
                background-color: #6d326f !important;
             }

            .modal{
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
                z-index: 112;
            }
            .modal-content{
                background-color: #fefefe;
                margin: auto;
                border: 1px solid #888;
                width: 50%;
            }
            #modal-join-queue-content{
                width: 50%;
                height: 20%;
            }
            #modal-iframe-content{
                width: 85%;
                height: 95%;
            }
        </style>

    </head>
    <body>
    <header>
        <nav class="navigation">

            <!-- Logo -->
            <div class="logo">

                <img class="img-responsive" src="/images/Fair2021/ImperialLogo-01.jpg" width="130" height="60" />
            </div>

            <!-- Navigation -->
            <ul class="menu-list">
                <li style="background-color: #f6872c;border: 1px solid #f6872c;text-align: center;"><a href="javascript::void();" ><small>Welcome</small><br>Reception</a></li>
                <li style="background-color: #00000000;border: 1px solid #00000000;text-align: center;"><a href="javascript::void();" onclick="joinImperialRoom('PRE')"><small>Step 1</small><br>Pre-Counseling</a></li>
                <li style="background-color: #00000000;border: 1px solid #00000000;text-align: center;"><a href="<?php echo base_url();?>event/booths/<?=$encodedUsername?>" ><small>Step 2</small><br>Meet Universities!</a></li>
                <li style="background-color: #00000000;border: 1px solid #00000000;text-align: center;"><a href="javascript::void();" onclick="joinImperialRoom('FINANCE')"><small>Step 3</small><br>Finance Counseling</a></li>
                <li style="background-color: #00000000;border: 1px solid #00000000;text-align: center;"><a href="javascript::void();" onclick="joinImperialRoom('POST')"><small>Step 4</small><br>Post Counseling</a></li>
            </ul>

            <div class="logo rmargin">
                <img class="img-responsive" src="/images/Fair2021/HelloUniLogo-01.jpg" width="130" height="60" />
            </div>

            <!-- <div class="humbarger">
                <div class="bar"></div>
                <div class="bar2 bar"></div>
                <div class="bar"></div>
            </div> -->
        </nav>


    </header>
    <div id="Reception" class="container-fluid"  >
        <div class="row" >
            <div class="col-md-12 bgImage"></div>

            <div class="bg-text1" id="bgImageText">
                <!-- <h2>Detail ONE</h2> -->
                <h1 class="topMar headH1">Imperial's MEGA VIrtual EduFair 2021!</h1>
                <h3 class="topMar">Meet TOP Universities from across the Globe directly<br>
                and gain unique insights on your prospects and options!</h3>
            </div>
            <div class="bg-text2">
                <!-- <h2>Detail ONE</h2> -->
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div id="videoHome">
                          <video id="mp4" controls loop controlsList="nodownload noremote foobar">
                              <source src="/fair2021/video_1.mp4" type="video/mp4">
                            </video>

                          <div id="spinner" class="poster">
                            <div class="cube"></div>
                            <div class="cube c2"></div>
                            <div class="cube c4"></div>
                            <div class="cube c3"></div>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div id="videoHome">
                          <video id="mpp4" controls loop controlsList="nodownload noremote foobar">
                              <source src="/fair2021/video_2.mp4" type="video/mp4">
                            </video>

                          <div id="spinnerr" class="poster">
                            <div class="cube"></div>
                            <div class="cube c2"></div>
                            <div class="cube c4"></div>
                            <div class="cube c3"></div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-text3">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <img src="/images/Fair2021/Button1.png" onclick="joinImperialRoom('PRE')" width="275" height="220" />
                    </div>
                    <div class="col-md-3">
                        <img src="/images/Fair2021/Button2.png" onclick="gotoBooth()" width="275" height="220" />
                    </div>
                    <div class="col-md-3">
                        <img src="/images/Fair2021/Button3.png" onclick="joinImperialRoom('FINANCE')" width="275" height="220" />
                    </div>
                    <div class="col-md-3">
                        <img src="/images/Fair2021/Button4.png" onclick="joinImperialRoom('POST')" width="275" height="220" />
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div id="modal-iframe" class="modal" >
       <!-- Modal content -->
       <div class="row modal-content" id="modal-iframe-content">
          <div style="width:100%; height:100%;">
             <button id="close-iframe" class="btn btn-md btn-primary leaveBtn" style="float:right;" onclick="closeIframe()">Leave Meeting</button>
             <iframe id="forPostyouradd" class="responsive-iframe embed-responsive-item" data-src="" src="about:blank" style="height: 96%; width: 100%; background:#ffffff; display:none" allowfullscreen></iframe>
          </div>
       </div>
    </div>

    <div class="alert" id="modal-join-queue">
      <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
      <div style="text-align:center; font-weight:bold;" id="modal-join-queue-message"></div>
      <button class="btn btn-md btnred btnpurp" onclick="joinQueueNo()" style="float:left;">May Be Later</button>
      <button class="btn btn-md btnorange btnpurp" onclick="joinQueueYes()" id="room-page" style="float:right;">Go To Room Page</button>
      <button class="btn btn-md btnorange btnpurp" onclick="joinRoom()" id="join-room" style="float:right;">Join Room</button>
    </div>
    <div class="alert" id="modal-join-event">
      <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
      <div style="text-align:center;" id="modal-join-event-message"></div>
      <button class="btn btn-md btnorange btnpurp" onclick="checkEvents()" id="event-page">Check Events</button>
      <button class="btn btn-md btnorange btnpurp" onclick="joinEventNow()" id="join-event">Join Event</button>
      <button class="btn btn-md btnred btnpurp" onclick="joinEventLater()">May Be Later</button>
    </div>
        <script type="text/javascript">

            var video = document.getElementById("mp4");
            var videoo = document.getElementById("mpp4");
            var spinner = document.getElementById("spinner");
            var spinnerr = document.getElementById("spinnerr");
            var delayMillis = 4000;
            var spinnerIsHere = 1;
            var delayMilliss = 4000;
            var spinnerIsHeree = 1;
             video.volume = 0;
             videoo.volume = 0;

            var playVid = setTimeout(function() {
              if(spinnerIsHere == 1) {
                //alert("inside2");
                // Delete element DOM
                // spinner.parentNode.removeChild(spinner);
                spinner.style.visibility = "hidden";
                spinnerIsHere = 0;
              }
              video.play();
            }, delayMillis);



            video.addEventListener("click", function( event ) {
              if(video.paused) {
                if(spinnerIsHere == 1) {
                 //alert("inside2");
                  // Delete element DOM
                  // spinner.parentNode.removeChild(spinner);
                  spinner.style.visibility = "hidden";
                  spinnerIsHere = 0;
                }
                clearTimeout(playVid);
                video.play();
              } else {
                video.pause();
                if(spinnerIsHere == 0) {
                  spinner.style.visibility = "visible";
                  spinnerIsHere = 1;
                }
              }
            }, false);

             var playVidd = setTimeout(function() {
              if(spinnerIsHeree == 1) {
                // Delete element DOM
                // spinner.parentNode.removeChild(spinner);
                spinnerr.style.visibility = "hidden";
                spinnerIsHeree = 0;
              }
              videoo.play();
            }, delayMilliss);

             videoo.addEventListener("click", function( event ) {
              if(videoo.paused) {
                if(spinnerIsHeree == 1) {
                  // Delete element DOM
                  // spinner.parentNode.removeChild(spinner);
                  spinnerr.style.visibility = "hidden";
                  spinnerIsHeree = 0;
                }
                clearTimeout(playVidd);
                videoo.play();
              } else {
                videoo.pause();
                if(spinnerIsHeree == 0) {
                  spinnerr.style.visibility = "visible";
                  spinnerIsHeree = 1;
                }
              }
            }, false);

             function imageClick(url) {
                  // window.location = url;
                  //window.open( url , '_blank');
              }

              function bookConsulation(){
                  $.ajax({
                      url: '/event/book_consultation/<?=$encodedUsername?>',
                      type: 'GET',
                      complete: function complete() {
                      },
                      success: function success(data) {
                          alert("Thanks for booking. You'll get a call from our counselor shortly");
                      },
                      error: function error() {
                      }
                  });
              }

              function joinImperialRoom(roomType){
                  let joinQueueModal = document.getElementById("modal-join-queue");
                  joinQueueModal.style.display = "none";

                  $.ajax({
                      url: '/event/check_available_room/<?=$encodedUsername?>/' + roomType,
                      type: 'GET',
                      complete: function complete() {
                      },
                      success: function success(data) {
                          if(data){
                              roomId = data;

                              let src = "/event/student/" + roomId + "/<?=$encodedUsername?>";

                              var iframe  = document.getElementById("forPostyouradd");
                              iframe.setAttribute('src', src);
                              iframe.style.display = 'block';

                              document.getElementById("modal-iframe").style.display = "block";
                          }
                          else{
                              alert("All rooms are full. Please try after sometime");
                          }
                      },
                      error: function error() {
                          alert("Some issue has been occured. Please refresh the page");
                      }
                  });
              }

              function gotoBooth(){
                  window.location.href = '/event/booths/<?=$encodedUsername?>';
              }

              function joinEventLater(){
                  let joinEventModal = document.getElementById("modal-join-event");
                  joinEventModal.style.display = "none";
              }

              function checkEvents(){
                  let joinEventModal = document.getElementById("modal-join-event");
                  joinEventModal.style.display = "none";

                  window.location.href='/event/schedule/<?=$encodedUsername?>';
              }

              function joinEventNow(){
                  let joinEventModal = document.getElementById("modal-join-event");
                  joinEventModal.style.display = "none";

                  let src = "/event/webinar/" + roomId + "/<?=$encodedUsername?>";

                  var iframe  = document.getElementById("forPostyouradd");
                  iframe.setAttribute('src', src);
                  iframe.style.display = 'block';

                  document.getElementById("modal-iframe").style.display = "block";
              }

              var joinQueue = 0;
              var joinEvent = 0;
              var roomId = '';
              var boothId = '';

              var x = setInterval(function() {
                  let joinQueueModal = document.getElementById("modal-join-queue");
                  joinQueueModal.style.display = "none";
                  if(joinQueue){
                      $.ajax({
                          url: '/event/delete_queue/<?=$encodedUsername?>',
                          type: 'GET',
                          complete: function complete() {
                          },
                          success: function success(data) {
                              joinQueue = 0;
                          },
                          error: function error() {
                              alert("Some issue has been occured. Please refresh the page");
                          }
                      });
                  }
                  else{
                      $.ajax({
                          url: '/event/check_queue/<?=$encodedUsername?>',
                          type: 'GET',
                          complete: function complete() {
                          },
                          success: function success(data) {
                              data = JSON.parse(data);
                              if(data.success){
                                  let msg = data.msg;
                                  joinQueue = data.join;
                                  if(joinQueue)
                                  {
                                      roomId = data.room_id;
                                      document.getElementById("join-room").style.display = "block";
                                      document.getElementById("room-page").style.display = "none";
                                  }
                                  else
                                  {
                                      document.getElementById("join-room").style.display = "none";
                                      document.getElementById("room-page").style.display = "block";
                                  }
                                  boothId = data.booth_id;
                                  document.getElementById("modal-join-queue-message").innerHTML = msg;
                                  let joinQueueModal = document.getElementById("modal-join-queue");
                                  joinQueueModal.style.display = "block";
                              }
                          },
                          error: function error() {
                              alert("Some issue has been occured. Please refresh the page");
                          }
                      });
                  }
              }, 15000);

              function joinQueueNo(){
                  let joinQueueModal = document.getElementById("modal-join-queue");
                  joinQueueModal.style.display = "none";
              }

              function joinQueueYes(){
                  let joinQueueModal = document.getElementById("modal-join-queue");
                  joinQueueModal.style.display = "none";

                  window.location.href='/event/booth/' + boothId + '/<?=$encodedUsername?>';
              }

              function joinRoom(){
                  let joinQueueModal = document.getElementById("modal-join-queue");
                  joinQueueModal.style.display = "none";

                  let src = "/event/student/" + roomId + "/<?=$encodedUsername?>";

                  var iframe  = document.getElementById("forPostyouradd");
                  iframe.setAttribute('src', src);
                  iframe.style.display = 'block';

                  document.getElementById("modal-iframe").style.display = "block";
              }

              function closeIframe(){
                  $.ajax({
                      url: '/event/leave_meeting/' + roomId + '/<?=$encodedUsername?>',
                      type: 'GET',
                      complete: function complete() {
                      },
                      success: function success(data) {
                          var iframe  = document.getElementById("forPostyouradd");
                          iframe.setAttribute('src', '');

                          let iframeModal = document.getElementById("modal-iframe");
                          iframeModal.style.display = "none";
                      },
                      error: function error() {
                          alert("Some issue has been occured. Please refresh the page");
                          //window.location.href='/event/booth/<?=$encodedBoothId?>/<?=$encodedUsername?>';
                      }
                  });
              }

              $.ajax({
                  url: '/event/user_online/<?=$encodedUsername?>',
                  type: 'GET',
                  complete: function complete() {
                  },
                  success: function success(data) {
                  },
                  error: function error() {
                  }
              });

              var userOnline = setInterval(function() {
                  $.ajax({
                      url: '/event/user_online/<?=$encodedUsername?>',
                      type: 'GET',
                      complete: function complete() {
                      },
                      success: function success(data) {
                      },
                      error: function error() {
                      }
                  });
              }, 60000);

              <?php
              /*
              var event_check = setInterval(function() {
                  let joinEventModal = document.getElementById("modal-join-event");
                  joinEventModal.style.display = "none";
                  if(joinEvent){
                      $.ajax({
                          url: '/event/delete_queue/<?=$encodedUsername?>',
                          type: 'GET',
                          complete: function complete() {
                          },
                          success: function success(data) {
                              joinEvent = 0;
                          },
                          error: function error() {
                              alert("Some issue has been occured. Please refresh the page");
                          }
                      });
                  }
                  else{
                      $.ajax({
                          url: '/event/coming_fair_event',
                          type: 'GET',
                          complete: function complete() {
                          },
                          success: function success(data) {
                              data = JSON.parse(data);
                              if(data.success){
                                  let msg = data.msg;
                                  joinEvent = data.join;
                                  if(joinEvent)
                                  {
                                      roomId = data.room_id;
                                      document.getElementById("join-event").style.display = "block";
                                      document.getElementById("event-page").style.display = "none";
                                  }
                                  else
                                  {
                                      document.getElementById("join-event").style.display = "none";
                                      document.getElementById("event-page").style.display = "block";
                                  }
                                  document.getElementById("modal-join-event-message").innerHTML = msg;
                                  let joinEventModal = document.getElementById("modal-join-event");
                                  joinEventModal.style.display = "block";
                              }
                          },
                          error: function error() {
                              alert("Some issue has been occured. Please refresh the page");
                          }
                      });
                  }
              }, 30000);
              */
              ?>
        </script>
    </body>
</html>
