<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>HelloUni Visa Seminar | Temorary Page</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="https://www.hellouni.org/admin/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://www.hellouni.org/admin/bootstrap/css/bootstrap.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="https://www.hellouni.org/admin/dist/css/AdminLTE.min.css">

        <style type="text/css">
            .error{color:red;}
        </style>
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-box-body">
                <form action="/meeting/visaseminar/temp" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" name="name" id="name" class="form-control" placeholder="Your Name">
                    </div>

                    <div class="row">
                        <div class="col-xs-4">
                            <button type="submit" id="signin" class="btn btn-primary btn-block btn-flat">Submit</button>
                        </div><!-- /.col -->
                    </div>
                </form>
            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->
    </body>
</html>
