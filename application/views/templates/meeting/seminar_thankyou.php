<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>HelloUni Visa Seminar</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="https://hellouni.org/admin/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://hellouni.org/admin/bootstrap/css/bootstrap.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="https://hellouni.org/admin/dist/css/AdminLTE.min.css">

        <link rel="icon" type="image/png" sizes="96x96" href="https://hellouni.org/admin/images/favicon-96x96.png">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition login-page" style="background-color: white;">
        <div class="login-box">
            <div class="login-logo">
                <img src="https://hellouni.org/admin/images/HelloUni-Logo.png" style="width: 75%;">
            </div>
            <div class="login-box-body">
                <form action="/test/load" method="get">
                    <div class="form-group has-feedback" >
                        <img src="<?php echo base_url();?>application/images/thankYou.jpg" alt="thankYou" width="100%" height="100%">
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
