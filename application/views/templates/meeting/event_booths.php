<!DOCTYPE html>
<html>
   <head>
      <title> Hellouni : Virtual Fair 2021 </title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
      <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css">
      <link href="<?=base_url()?>css/newFairDesign.css?v=1.1" rel="stylesheet">
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
      <style type="text/css">
         body {
         width: 100%;
         height: 100%;
         background-color: #26002A;
         }
         ul.tabs{
         margin: 0px;
         padding: 0px;
         list-style: none;
         }
         ul.tabs li{
         background: white;
         color: #222;
         display: inline-block;
         padding: 10px 15px;
         cursor: pointer;
         }
         ul.tabs li.current{
         background: #57005E;
         color: #fff;
         }
         .tab-content{
         display: none;
         /*background: #ededed;*/
         padding: 15px;
         }
         .tab-content.current{
         display: inherit;
         }
         form.example input[type=text] {
         padding: 6.5px;
         font-size: 17px;
         border: 1px solid grey;
         float: left;
         width: 80%;
         background: #f1f1f1;
         }
         form.example button {
         float: left;
         width: 15%;
         padding: 10px;
         background: #f6872c;
         color: white;
         font-size: 12px;
         border: 1px solid grey;
         border-left: none;
         cursor: pointer;
         }
         form.example button:hover {
         background: #f6872c85;
         }
         form.example::after {
         content: "";
         clear: both;
         display: table;
         }
         .modal{
         display: none; /* Hidden by default */
         position: fixed; /* Stay in place */
         z-index: 1; /* Sit on top */
         padding-top: 100px; /* Location of the box */
         left: 0;
         top: 0;
         width: 100%; /* Full width */
         height: 100%; /* Full height */
         overflow: auto; /* Enable scroll if needed */
         background-color: rgb(0,0,0); /* Fallback color */
         background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
         z-index: 112;
         }
         .modal-content{
         background-color: #fefefe;
         margin: auto;
         border: 1px solid #888;
         width: 50%;
         }
         .alert {
              padding: 10px;
              background-color: #f6882c;
              color: white;
                z-index: 10;
                bottom: 0;
                position: fixed;
                left: 0;
                text-align: center;
                display: none; /* Hidden by default */
            }

            .closebtn {
              margin-left: 15px;
              color: white;
              font-weight: bold;
              float: right;
              font-size: 22px;
              line-height: 20px;
              cursor: pointer;
              transition: 0.3s;
            }

            .closebtn:hover {
              color: black;
            }

            .btnpurp {
                background-color: #6d326f !important;
             }
         #modal-iframe-content{
         width: 85%;
         height: 95%;
         }
      </style>
   </head>
   <body>
      <header>
         <nav class="navigation">
            <!-- Logo -->
            <div class="logo">
               <img class="img-responsive" src="/images/Fair2021/ImperialLogo-01.jpg" onclick="imageClick('https://bit.ly/2RSGRFm')" width="130" height="60" />
            </div>
            <!-- Navigation -->
            <ul class="menu-list">
               <li style="background-color: #00000000;border: 1px solid #00000000;text-align: center;"><a href="<?php echo base_url();?>event/reception/<?=$encodedUsername?>" ><small>Welcome</small><br>Reception</a></li>
               <li style="background-color: #00000000;border: 1px solid #00000000;text-align: center;"><a href="javascript:void(0);" onclick="joinImperialRoom('PRE')"><small>Step 1</small><br>Pre-Counseling</a></li>
               <li style="background-color: #f6872c;border: 1px solid #f6872c;text-align: center;"><a href="<?php echo base_url();?>event/booths/<?=$encodedUsername?>" ><small>Step 2</small><br>Meet Universities!</a></li>
               <li style="background-color: #00000000;border: 1px solid #00000000;text-align: center;"><a href="javascript:void(0);" onclick="joinImperialRoom('FINANCE')"><small>Step 3</small><br>Finance Counseling</a></li>
               <li style="background-color: #00000000;border: 1px solid #00000000;text-align: center;"><a href="javascript:void(0);" onclick="joinImperialRoom('POST')"><small>Step 4</small><br>Post Counseling</a></li>

            </ul>
            <div class="logo rmargin">
               <img class="img-responsive" src="/images/Fair2021/HelloUniLogo-01.jpg" onclick="imageClick('https://bit.ly/2RSGRFm')" width="130" height="60" />
            </div>
         </nav>
      </header>
      <div id="universities" class="container"  style="top: 10%;position: relative;padding-top: 6%;">
          <div class="col-md-12" style="margin-bottom: 8px">

               <form class="example ui-widget" action="/action_page.php" style="margin: auto 0% auto 65%;;max-width:400px">
                 <input id="tags" type="text" placeholder="Search.." name="search2">
                 <button type="button" onclick="showResult()"><i class="fa fa-search"></i></button>
               </form>

             </div>
         <div class="container">
            <ul class="tabs">
               <li class="tab-link current" data-tab="tab-1">All</li>
               <li class="tab-link" data-tab="tab-2">USA / CANADA</li>
               <li class="tab-link" data-tab="tab-3">UK</li>
               <li class="tab-link" data-tab="tab-4">EUROPE</li>
               <li class="tab-link" data-tab="tab-5">AUS / NZ</li>
            </ul>
            <div id="tab-1" class="tab-content current">
               <?php
                  foreach($fairUniversities as $fairUniversity)
                  {
                      ?>
               <div class="col-md-12 detail">
                  <div class="col-12 col-sm-12 col-md-12 col-lg-3 p-0" style="margin-top: 8px;padding-left: 0px;">
                     <img src="<?=$fairUniversity['campus_photos']?>" alt="">
                  </div>
                  <div class="col-12 col-sm-12 col-md-12 col-lg-8 p-0">
                     <span class="discount-holder"><span><i class="fas fa-user"></i> <?=$fairUniversity['noof_delegates']?> Exibitors Available</span></span>
                     <div class="detail-box">
                        <img src="<?=$fairUniversity['logo']?>" alt=""  />
                        <a href="<?php echo base_url();?>event/booth/<?=$fairUniversity['encodedId']?>/<?=$encodedUsername?>" >
                           <h4><?=$fairUniversity['name']?></h4>
                        </a>
                        <p><?=$fairUniversity['one_line_description']?></p>
                        <!--ul>
                           <li><strong>No. of Total Students : </strong>   22222</li>
                           <li><strong>Ranking  : </strong> World: 112 / National: 231</li>
                           <li><strong>Placement : </strong> 99%</li>
                           </ul-->
                     </div>
                  </div>
               </div>
               <?php
                  }
                  ?>
            </div>
            <div id="tab-2" class="tab-content">
                <?php
                   foreach($fairUniversities as $fairUniversity)
                   {
                       if($fairUniversity['country'] == 'USA' || $fairUniversity['country'] == 'Canada' || $fairUniversity == 'UK, USA')
                       {
                       ?>
                <div class="col-md-12 detail">
                   <div class="col-12 col-sm-12 col-md-12 col-lg-3 p-0" style="margin-top: 8px;padding-left: 0px;">
                      <img src="<?=$fairUniversity['campus_photos']?>" alt="">
                   </div>
                   <div class="col-12 col-sm-12 col-md-12 col-lg-8 p-0">
                      <span class="discount-holder"><span><i class="fas fa-user"></i> <?=$fairUniversity['noof_delegates']?> Exibitors Available</span></span>
                      <div class="detail-box">
                         <img src="<?=$fairUniversity['logo']?>" alt=""  />
                         <a href="<?php echo base_url();?>event/booth/<?=$fairUniversity['encodedId']?>/<?=$encodedUsername?>" >
                            <h4><?=$fairUniversity['name']?></h4>
                         </a>
                         <p><?=$fairUniversity['one_line_description']?></p>
                         <!--ul>
                            <li><strong>No. of Total Students : </strong>   22222</li>
                            <li><strong>Ranking  : </strong> World: 112 / National: 231</li>
                            <li><strong>Placement : </strong> 99%</li>
                            </ul-->
                      </div>
                   </div>
                </div>
                <?php
            }
                   }
                   ?>
            </div>
            <div id="tab-3" class="tab-content">
                <?php
                   foreach($fairUniversities as $fairUniversity)
                   {
                       if($fairUniversity['country'] == 'UK' || $fairUniversity['country'] == 'UK, USA')
                       {
                       ?>
                <div class="col-md-12 detail">
                   <div class="col-12 col-sm-12 col-md-12 col-lg-3 p-0" style="margin-top: 8px;padding-left: 0px;">
                      <img src="<?=$fairUniversity['campus_photos']?>" alt="">
                   </div>
                   <div class="col-12 col-sm-12 col-md-12 col-lg-8 p-0">
                      <span class="discount-holder"><span><i class="fas fa-user"></i> <?=$fairUniversity['noof_delegates']?> Exibitors Available</span></span>
                      <div class="detail-box">
                         <img src="<?=$fairUniversity['logo']?>" alt=""  />
                         <a href="<?php echo base_url();?>event/booth/<?=$fairUniversity['encodedId']?>/<?=$encodedUsername?>" >
                            <h4><?=$fairUniversity['name']?></h4>
                         </a>
                         <p><?=$fairUniversity['one_line_description']?></p>
                         <!--ul>
                            <li><strong>No. of Total Students : </strong>   22222</li>
                            <li><strong>Ranking  : </strong> World: 112 / National: 231</li>
                            <li><strong>Placement : </strong> 99%</li>
                            </ul-->
                      </div>
                   </div>
                </div>
                <?php
            }
                   }
                   ?>
            </div>
            <div id="tab-4" class="tab-content">
                <?php
                   foreach($fairUniversities as $fairUniversity)
                   {
                       if($fairUniversity['country'] == 'Germany' || $fairUniversity['country'] == 'Croatia')
                       {
                       ?>
                <div class="col-md-12 detail">
                   <div class="col-12 col-sm-12 col-md-12 col-lg-3 p-0" style="margin-top: 8px;padding-left: 0px;">
                      <img src="<?=$fairUniversity['campus_photos']?>" alt="">
                   </div>
                   <div class="col-12 col-sm-12 col-md-12 col-lg-8 p-0">
                      <span class="discount-holder"><span><i class="fas fa-user"></i> <?=$fairUniversity['noof_delegates']?> Exibitors Available</span></span>
                      <div class="detail-box">
                         <img src="<?=$fairUniversity['logo']?>" alt=""  />
                         <a href="<?php echo base_url();?>event/booth/<?=$fairUniversity['encodedId']?>/<?=$encodedUsername?>" >
                            <h4><?=$fairUniversity['name']?></h4>
                         </a>
                         <p><?=$fairUniversity['one_line_description']?></p>
                         <!--ul>
                            <li><strong>No. of Total Students : </strong>   22222</li>
                            <li><strong>Ranking  : </strong> World: 112 / National: 231</li>
                            <li><strong>Placement : </strong> 99%</li>
                            </ul-->
                      </div>
                   </div>
                </div>
                <?php
            }
                   }
                   ?>
            </div>
            <div id="tab-5" class="tab-content">
                <?php
                   foreach($fairUniversities as $fairUniversity)
                   {
                       if($fairUniversity['country'] == 'Australia')
                       {
                       ?>
                <div class="col-md-12 detail">
                   <div class="col-12 col-sm-12 col-md-12 col-lg-3 p-0" style="margin-top: 8px;padding-left: 0px;">
                      <img src="<?=$fairUniversity['campus_photos']?>" alt="">
                   </div>
                   <div class="col-12 col-sm-12 col-md-12 col-lg-8 p-0">
                      <span class="discount-holder"><span><i class="fas fa-user"></i> <?=$fairUniversity['noof_delegates']?> Exibitors Available</span></span>
                      <div class="detail-box">
                         <img src="<?=$fairUniversity['logo']?>" alt=""  />
                         <a href="<?php echo base_url();?>event/booth/<?=$fairUniversity['encodedId']?>/<?=$encodedUsername?>" >
                            <h4><?=$fairUniversity['name']?></h4>
                         </a>
                         <p><?=$fairUniversity['one_line_description']?></p>
                         <!--ul>
                            <li><strong>No. of Total Students : </strong>   22222</li>
                            <li><strong>Ranking  : </strong> World: 112 / National: 231</li>
                            <li><strong>Placement : </strong> 99%</li>
                            </ul-->
                      </div>
                   </div>
                </div>
                <?php
            }
                   }
                   ?>
            </div>
         </div>
      </div>
      <div class="alert" id="modal-join-queue">
          <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
          <div style="text-align:center; font-weight: bold;" id="modal-join-queue-message"></div>
          <button class="btn btn-md btnred btnpurp" onclick="joinQueueNo()" style="float:left;">May Be Later</button>
          <button class="btn btn-md btnorange btnpurp" onclick="joinQueueYes()" id="room-page" style="float:right;">Go To Room Page</button>
          <button class="btn btn-md btnorange btnpurp" onclick="joinRoom()" id="join-room" style="float:right;">Join Room</button>
        </div>
        <div id="modal-iframe" class="modal" >
           <!-- Modal content -->
           <div class="row modal-content" id="modal-iframe-content">
              <div style="width:100%; height:100%;">
                 <button id="close-iframe" class="btn btn-md btn-primary leaveBtn" style="float:right;" onclick="closeIframe()">Leave Meeting</button>
                 <iframe id="forPostyouradd" class="responsive-iframe embed-responsive-item" data-src="" src="about:blank" style="height: 96%; width: 100%; background:#ffffff; display:none" allowfullscreen></iframe>
              </div>
           </div>
        </div>
        <div class="alert" id="modal-join-event">
          <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
          <div style="text-align:center;" id="modal-join-event-message"></div>
          <button class="btn btn-md btnorange btnpurp" onclick="checkEvents()" id="event-page">Check Events</button>
          <button class="btn btn-md btnorange btnpurp" onclick="joinEventNow()" id="join-event">Join Event</button>
          <button class="btn btn-md btnred btnpurp" onclick="joinEventLater()">May Be Later</button>
        </div>
      <script type="text/javascript">
         $(document).ready(function(){

               $('ul.tabs li').click(function(){
                 var tab_id = $(this).attr('data-tab');

                 $('ul.tabs li').removeClass('current');
                 $('.tab-content').removeClass('current');

                 $(this).addClass('current');
                 $("#"+tab_id).addClass('current');
               })

             })
      </script>
      <script type="text/javascript">
             var joinQueue = 0;
             var joinEvent = 0;
             var roomId = '';
             var boothId = '';
             var universityNames = JSON.parse('<?=$universityNames?>');
             var allUniversities = JSON.parse('<?=$allUniversities?>');

             $( function() {
               $( "#tags" ).autocomplete({
                 source: universityNames
               });
             } );
             var x = setInterval(function() {
                 let joinQueueModal = document.getElementById("modal-join-queue");
                 joinQueueModal.style.display = "none";
                 if(joinQueue){
                     $.ajax({
                         url: '/event/delete_queue/<?=$encodedUsername?>',
                         type: 'GET',
                         complete: function complete() {
                         },
                         success: function success(data) {
                             joinQueue = 0;
                         },
                         error: function error() {
                             alert("Some issue has been occured. Please refresh the page");
                         }
                     });
                 }
                 else{
                     $.ajax({
                         url: '/event/check_queue/<?=$encodedUsername?>',
                         type: 'GET',
                         complete: function complete() {
                         },
                         success: function success(data) {
                             data = JSON.parse(data);
                             if(data.success){
                                 let msg = data.msg;
                                 joinQueue = data.join;
                                 if(joinQueue)
                                 {
                                     roomId = data.room_id;
                                     document.getElementById("join-room").style.display = "block";
                                     document.getElementById("room-page").style.display = "none";
                                 }
                                 else
                                 {
                                     document.getElementById("join-room").style.display = "none";
                                     document.getElementById("room-page").style.display = "block";
                                 }
                                 boothId = data.booth_id;
                                 document.getElementById("modal-join-queue-message").innerHTML = msg;
                                 let joinQueueModal = document.getElementById("modal-join-queue");
                                 joinQueueModal.style.display = "block";
                             }
                         },
                         error: function error() {
                             alert("Some issue has been occured. Please refresh the page");
                         }
                     });
                 }
             }, 15000);

             function joinQueueNo(){
                 let joinQueueModal = document.getElementById("modal-join-queue");
                 joinQueueModal.style.display = "none";
             }

             function joinEventLater(){
                 let joinEventModal = document.getElementById("modal-join-event");
                 joinEventModal.style.display = "none";
             }

             function joinQueueYes(){
                 let joinQueueModal = document.getElementById("modal-join-queue");
                 joinQueueModal.style.display = "none";

                 window.location.href='/event/booth/' + boothId + '/<?=$encodedUsername?>';
             }

             function checkEvents(){
                 let joinEventModal = document.getElementById("modal-join-event");
                 joinEventModal.style.display = "none";

                 window.location.href='/event/schedule/<?=$encodedUsername?>';
             }

             function joinRoom(){
                 let joinQueueModal = document.getElementById("modal-join-queue");
                 joinQueueModal.style.display = "none";

                 let src = "/event/student/" + roomId + "/<?=$encodedUsername?>";

                 var iframe  = document.getElementById("forPostyouradd");
                 iframe.setAttribute('src', src);
                 iframe.style.display = 'block';

                 document.getElementById("modal-iframe").style.display = "block";
             }

             function joinEventNow(){
                 let joinEventModal = document.getElementById("modal-join-event");
                 joinEventModal.style.display = "none";

                 let src = "/event/webinar/" + roomId + "/<?=$encodedUsername?>";

                 var iframe  = document.getElementById("forPostyouradd");
                 iframe.setAttribute('src', src);
                 iframe.style.display = 'block';

                 document.getElementById("modal-iframe").style.display = "block";
             }

             function closeIframe(){
                 $.ajax({
                     url: '/event/leave_meeting/' + roomId + '/<?=$encodedUsername?>',
                     type: 'GET',
                     complete: function complete() {
                     },
                     success: function success(data) {
                         var iframe  = document.getElementById("forPostyouradd");
                         iframe.setAttribute('src', '');

                         let iframeModal = document.getElementById("modal-iframe");
                         iframeModal.style.display = "none";
                     },
                     error: function error() {
                         alert("Some issue has been occured. Please refresh the page");
                         //window.location.href='/event/booth/<?=$encodedBoothId?>/<?=$encodedUsername?>';
                     }
                 });
             }

             function showResult(){
                 let searchedUniversityName = document.getElementById("tags").value;
                 let uniId = '';
                 for(let key in allUniversities){
                     if(allUniversities[key] == searchedUniversityName){
                         uniId = key;
                         break;
                     }
                 }
                 if(uniId){
                     $.ajax({
                         url: '/event/university/' + uniId,
                         type: 'GET',
                         complete: function complete() {
                         },
                         success: function success(data) {
                             data = JSON.parse(data);

                             let searchBoxHtml = '<div class="col-md-12" style="margin-bottom: 8px"><form class="example ui-widget" action="/action_page.php" style="margin: auto 0% auto 65%;;max-width:400px"><input id="tags" type="text" placeholder="Search.." name="search2" class="ui-autocomplete-input" autocomplete="off"><button type="button" onclick="showResult()"><i class="fa fa-search"></i></button></form></div>';

                             let universityHtml = '<div class="col-md-12 detail"><div class="col-12 col-sm-12 col-md-12 col-lg-3 p-0" style="margin-top: 8px;padding-left: 0px;"><img src="' + data.campus_photos + '" alt=""></div><div class="col-12 col-sm-12 col-md-12 col-lg-8 p-0"><span class="discount-holder"><span><i class="fas fa-user"></i> ' + data.noof_delegates + ' Exibitors Available</span></span><div class="detail-box"><img src="https://hellouni.org' + data.logo + '" alt="' + data.name + '"><a href="https://hellouni.org/event/booth/' + data.encodedId + '/<?=$encodedUsername?>"><h4>' + data.name + '</h4></a><p>' + data.one_line_description + '</p></div></div></div>';

                             document.getElementById("universities").innerHTML = searchBoxHtml + universityHtml;

                             $( "#tags" ).autocomplete({
                               source: universityNames
                             });
                         },
                         error: function error() {
                             alert("Some issue has been occured. Please refresh the page");
                             //window.location.href='/event/booth/<?=$encodedBoothId?>/<?=$encodedUsername?>';
                         }
                     });
                 }
                 else{
                     window.location.href='/event/booths/<?=$encodedUsername?>';
                 }
             }

             $.ajax({
                 url: '/event/user_online/<?=$encodedUsername?>',
                 type: 'GET',
                 complete: function complete() {
                 },
                 success: function success(data) {
                 },
                 error: function error() {
                 }
             });

             var userOnline = setInterval(function() {
                 $.ajax({
                     url: '/event/user_online/<?=$encodedUsername?>',
                     type: 'GET',
                     complete: function complete() {
                     },
                     success: function success(data) {
                     },
                     error: function error() {
                     }
                 });
             }, 60000);

             <?php
             /*
             var event_check = setInterval(function() {
                 let joinEventModal = document.getElementById("modal-join-event");
                 joinEventModal.style.display = "none";
                 if(joinEvent){
                     $.ajax({
                         url: '/event/delete_queue/<?=$encodedUsername?>',
                         type: 'GET',
                         complete: function complete() {
                         },
                         success: function success(data) {
                             joinEvent = 0;
                         },
                         error: function error() {
                             alert("Some issue has been occured. Please refresh the page");
                         }
                     });
                 }
                 else{
                     $.ajax({
                         url: '/event/coming_fair_event',
                         type: 'GET',
                         complete: function complete() {
                         },
                         success: function success(data) {
                             data = JSON.parse(data);
                             if(data.success){
                                 let msg = data.msg;
                                 joinEvent = data.join;
                                 if(joinEvent)
                                 {
                                     roomId = data.room_id;
                                     document.getElementById("join-event").style.display = "block";
                                     document.getElementById("event-page").style.display = "none";
                                 }
                                 else
                                 {
                                     document.getElementById("join-event").style.display = "none";
                                     document.getElementById("event-page").style.display = "block";
                                 }
                                 document.getElementById("modal-join-event-message").innerHTML = msg;
                                 let joinEventModal = document.getElementById("modal-join-event");
                                 joinEventModal.style.display = "block";
                             }
                         },
                         error: function error() {
                             alert("Some issue has been occured. Please refresh the page");
                         }
                     });
                 }
             }, 30000);
             */
             ?>

             function joinImperialRoom(roomType){
                 let joinQueueModal = document.getElementById("modal-join-queue");
                 joinQueueModal.style.display = "none";

                 $.ajax({
                     url: '/event/check_available_room/<?=$encodedUsername?>/' + roomType,
                     type: 'GET',
                     complete: function complete() {
                     },
                     success: function success(data) {
                         if(data){
                             roomId = data;

                             let src = "/event/student/" + roomId + "/<?=$encodedUsername?>";

                             var iframe  = document.getElementById("forPostyouradd");
                             iframe.setAttribute('src', src);
                             iframe.style.display = 'block';

                             document.getElementById("modal-iframe").style.display = "block";
                         }
                         else{
                             alert("All rooms are full. Please try after sometime");
                         }
                     },
                     error: function error() {
                         alert("Some issue has been occured. Please refresh the page");
                     }
                 });
             }
         </script>
   </body>
</html>
