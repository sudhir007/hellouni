<html lang="en">
<head>
  <title>Launching Hellouni</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

  <style type="text/css">
.footer-bottom{
    bottom: 0;
    position: fixed;
}

.footer1{
    bottom: 4rem;
    position: fixed;
    width: 85%;
}



.imageRow{
    background-image: url(/images/VisaSeminar2022.jpg);
    height: 100vh;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: contain;
    /*box-shadow: inset 0 0 0 2000px rgba(4, 4, 4, 0.54);*/
}

.weRow{
    font-size: 35px;
    font-weight: lighter;
    letter-spacing: 1px;
    font-family: inherit;
    margin-bottom: 5px;
}

.launchRow{
    font-size: 10rem;
    text-transform: uppercase;
    letter-spacing: 5px;
    margin: 5px 0 16px;
    font-family: inherit;
    font-weight: bold;
}

.pBy{
    margin: 16px 0 16px;
    text-transform: initial;
    font-weight: 600;
    letter-spacing: 1.5px;
    font-style: oblique;
}

.borderCount{
    border-right: 3px solid #ef7829;
}
.orange{
  color: #f6872c !important;
}

.wht{
  color: #ffffff !important;
}
.txtAlignCentre{
  text-align: center;
}
.timebox{
  width:28%;
  text-align:center;
  padding:1px;
  margin:0px auto;
}

.heding1{
      margin-top: 9.2%;
    font-size: 50px;
    font-family: inherit;
    font-weight: bold;
    letter-spacing: 1px;
}
#demo{
    color: #ffffff;
}
</style>
</head>
<body>

<div class="container" style="width: 100vw; background-color: #f2f2f2;">
   <div class="col-md-12 imageRow">
      <div class="col-lg-12 " >
        <div class="col-md-12" style="padding: 14% 0px;">
            <div class="col-md-12">
                <!-- <div class="row txtAlignCentre ">
                    <h5 class="text-center wht weRow">WEBINAR WILL</h5>
                </div> -->
                <div class="row txtAlignCentre ">
                    <h1 class="text-center wht heding1" >USA Visa Finance Seminar 2022</h1>
                </div>
                <div class="row timebox">
                    <div class="col-md-3 txtAlignCentre borderCount" hidden>
                        <h1 id="day" class="text-center wht"></h1>
                    </div>
                    <div class="col-md-6 txtAlignCentre " style="display: inline-flex;">
                        <h1 id="hour" class="text-center wht launchRow"></h1>
                        <h1 class="text-center wht launchRow">:</h1>
                        <h1 id="min" class="text-center wht launchRow"></h1>
                        <h1 class="text-center wht launchRow">:</h1>
                        <h1 id="sec" class="text-center wht launchRow"></h1>
                    </div>

                    <h1 id="demo"></h1>
                </div>
                <!--div class="row txtAlignCentre ">
                    <h4 class="text-center wht pBy">Powered By</h4>
                    <img src="/img/ImperialLogo.png" width="35%" alt="...">
                </div-->


            </div>
        </div>
      </div>
   </div>
</div>


<script type="text/javascript">

//var countDownDate = new Date("August 20, 2020 18:30:00").getTime();

// Update the count down every 1 second
var x = setInterval(function() {
//var usaTime = new Date().toLocaleString("en-US", {timeZone: "America/New_York"});
  //var now = new Date(usaTime).getTime();
  var now = new Date().getTime();
  var distance = 0;

  /*
    $.ajax({
        url: '/meeting/webinar/check_time',
        type: 'POST',
        contentType: 'application/json', // send as JSON
        data: JSON.stringify({'tid': '<?=$token_id?>', 'timezone': '<?=$timezone?>', 'stimezone': '<?=$stimezone?>'}),
        complete: function complete() {
        },
        success: function success(data) {
            distance = parseInt(data) - now;

            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            document.getElementById("day").innerHTML = days ;
            document.getElementById("hour").innerHTML = hours ;
            document.getElementById("min").innerHTML = minutes ;
            document.getElementById("sec").innerHTML = seconds ;

            if (distance < 0) {
              clearInterval(x);
              document.getElementById("demo").innerHTML = "LAUNCHING...";
              window.location.reload();
            }
        },
        error: function error() {
        }
    });
    */

    $.ajax({
        url: '/event/check_time',
        type: 'POST',
        contentType: 'application/json', // send as JSON
        data: JSON.stringify({}),
        complete: function complete() {
        },
        success: function success(data) {
            distance = parseInt(data) - now;

            if (distance < 0) {
              clearInterval(x);

              var randomNumber = Math.floor((Math.random() * 300) + 1);

              document.getElementById("demo").innerHTML = "LAUNCHING IN " + randomNumber + " SEC...";

              randomNumber = randomNumber * 1000;

              setTimeout(function () {
                  window.location.href = "/event/webinar/<?=$tokboxId?>/<?=$username?>";
              }, randomNumber);

              //window.location.href = "/event/rooms/<?=$username?>";
            }
            else{
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                document.getElementById("day").innerHTML = days ;
                document.getElementById("hour").innerHTML = hours ;
                document.getElementById("min").innerHTML = minutes ;
                document.getElementById("sec").innerHTML = seconds ;
            }
        },
        error: function error() {
        }
    });
}, 1000);
</script>
</body>
</html>
<?php
exit;
?>
