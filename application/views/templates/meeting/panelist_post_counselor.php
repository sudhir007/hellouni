<html>
    <head>
        <title> Hellouni : Webinar | Organizer </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css">
        <link href="<?=base_url()?>css/panelist_pre_counselor.css?v=1.1" rel="stylesheet">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://static.opentok.com/v2/js/opentok.min.js"></script>

        <style type="text/css">
            body, html {
                background-color: #4a265a;
                background: url("<?php echo base_url() . 'application/images/University_Backgrounds/Post_Counselor.png';?>");
                background-position: center center;
                background-size: contain;
                background-repeat: initial;
                height: 100%;
            }

            .unitableDesign tbody {
                background-color: white;
            }

            .unitableDesign>tbody>tr>td{
                text-align: center;
                vertical-align: middle;
                padding: 8px 12px !important;
                font-size: 14px;
            }

            .unitableDesign tr:first-of-type td:first-of-type {
                border-top-left-radius: 10px;
            }

            .unitableDesign tr:first-of-type td:last-of-type {
                border-top-right-radius: 10px;
            }

            .unitableDesign tr:last-of-type td:first-of-type {
                border-bottom-left-radius: 10px;
            }

            .unitableDesign tr:last-of-type td:last-of-type {
                border-bottom-right-radius: 10px;
            }

            .tdborder{
                border-right: 3px solid #815cd5;
            }

            .notopBorder{
                border-top: aliceblue !important;
            }

            .textHt{
                color: #f6872c;
                font-weight: 700;
                letter-spacing: 0.7px;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid" id="videos">
            <div class="col-md-12" id="subscriber"></div>
            <div class="col-md-12" id="publisher" ></div>
            <div class="col-md-12" id="screen-preview"></div>
        </div>
        <div id="mySidenav" class="sidenav">
            <a href="#" id="MenuTab" class="openbtn" onclick="openNav()" ><i class="fas fa-bars"></i></a>
        </div>

        <div id="mySidepanel1" class="sidepanel1" >
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><i class="fa fa-times" aria-hidden="true"></i></a>
            <a href="#" id="share-screen-button"><button type="button" id="screen" onClick="javascript:shareScreen()" class="btn btn-sm btn-primary width100 Orangebtn"><i class="fas fa-chalkboard-teacher">&nbsp;</i><b>Share Screen</b></button></a>

            <a href="#" ><button type="button" class="slide-toggle btn btn-sm btn-info width100 Orangebtn" ><i class="fas fa-comments">&nbsp;</i><b>Ask</b></button></a>
            <a href="javascript::void();" class="btn btn-sm btn-info width100 Orangebtn" onclick="openListPopup()"><i class="fas fa-users">&nbsp;</i><b>Show Students</b> <span id="attendees-count"></span></a>
            <a href="#" ><button type="button" class="btn btn-sm btn-danger width100 Orangebtn" onclick="leaveMeeting()"><b>Leave Meeting</b> </button></a>
            <div class="col-md-12">
                <h2></h2>
                <table class="table unitableDesign">
                    <tbody>
                        <tr>
                            <td class="textHt tdborder notopBorder">Waiting In Queue (Yet to be met)</td>
                            <td id="waiting-in-queue"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="box" id="chat-box">
            <span>
                <button id="chatBoxOut" onclick="expandChat()" type="button" class="btn btn-sm btn-primary marginbox" ><i class="fas fa-chevron-circle-up"></i></button>
                <button id="chatBoxIn" onclick="collapsChat()" type="button" class="btn btn-sm btn-primary marginbox"><i class="fas fa-chevron-circle-down"></i></button>
                <button type="button" class="slide-toggle btn btn-sm btn-danger marginbox" ><i class="fas fa-times-circle"></i></button>
            </span>
            <div class="box-inner">
                <div id="textchat">
                    <div id="history">
                        <?php
                        if($common_chat_history)
                        {
                            foreach($common_chat_history as $history)
                            {
                                ?>
                                <p id="<?=$history['chat_id']?>" class="<?=$history['chat_of']?>"><?=$history['name'] . ': ' . $history['message']?></p>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <form>
                        <input type="text" placeholder="Input your text here" id="msgTxt"></input>
                        <input type="hidden" id="username" value="<?=$username?>"></input>
                    </form>
                </div>
            </div>
        </div>

        <div id="chatModal" class="chatBoxModal" >
            <!-- Modal content -->
            <div class="row chatBoxModal-content" >
                <span class="chatclose" id="chatcloseModal">&times;</span>
                <div id="historyOTO">
                    <?php
                    if($private_chat_history)
                    {
                        foreach($private_chat_history as $history)
                        {
                            ?>
                            <p id="<?=$history['chat_id']?>" class="<?=$history['chat_of']?>"><?=$history['name'] . ': ' . $history['message']?></p>
                            <?php
                        }
                    }
                    ?>
                </div>
                <div id="textchatOTO"></div>
            </div>
        </div>

        <div id="attendeesList" class="attendeesListModal" >
            <!-- Modal content -->
            <div class="row attendeesList-content">
                <span class="listclose" id="closeModal">&times;</span>
                <div class="col-md-12 content">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="table-container">
                                <table class="table table-filter">
                                    <thead>
                                        <tr>
                                            <th>Student Name</th>
                                            <th>Competitive Exam Given</th>
                                            <th>Competitive Exam Score</th>
                                            <th>GPA Scale</th>
                                            <th>GPA Score</th>
                                            <th>Work Experience</th>
                                            <th>Course</th>
                                            <th>Intake</th>
                                        </tr>
                                    </thead>
                                    <tbody id="attendees-list"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="footer-head" class="col-md-12 col-sm-12">
            <div id="footer-head-sub" class="col-md-12">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 left-align">
                    <div class="copyright">
                        <span class="footer-text">© 2018, </span>
                        <img src="https://www.hellouni.org/img/logo.png" width="90">&nbsp;
                        <span class="footer-text"> All rights reserved</span>
                        <!-- COPYRIGHT 2018 &copy; HELLOUNI 2018 -->
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 right-align">
                    <div class="design">
                        <a href="https://www.hellouni.org" class="footer-text"><b>Hellouni</b> </a> |  <a target="_blank" href="https://www.hellouni.org" class="footer-text">MAKE RIGHT CHOICE !</a>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var apiKey = "<?=$apiKey?>";
            var sessionId = "<?=$sessionId?>";
            var token = "<?=$token?>";
            var tokboxId = "<?=$tokboxId?>";
            var session;
            var archiveID;
            var allConnections = {};
            var allStreams = {};
            var publisher;

            var organizerCount = 0;
            var publisherCount = 0;
            var subscriberCount = 0;
            var connectionCount = 0;

            var totalSubscribers = 0;
            var screenShared = 0;

            var msgHistory = document.querySelector('#history');
            var privateHistory = document.querySelector("#historyOTO");

            var chatBoxModal = document.getElementById("chatModal");
            var chatspan = document.getElementById("chatcloseModal");

            var attendeesListModal = document.getElementById("attendeesList");
            var coursespan = document.getElementById("closeModal");

            // Text chat
            var form = document.querySelector('form');
            var msgTxt = document.querySelector('#msgTxt');
            var username = document.querySelector('#username');

            function openNav() {
                document.getElementById("mySidepanel1").style.setProperty("width", "20%", "important");
            }

            function closeNav() {
                document.getElementById("mySidepanel1").style.setProperty("width", "0", "important");
                document.getElementById("videos").style.setProperty("width", "100%", "important");
            }

            function expandChat() {
                document.getElementById("chat-box").style.setProperty("height", "95%", "important");
                document.getElementById("chat-box").style.display = "block";
                document.getElementById("chatBoxOut").style.display = "none";
                document.getElementById("chatBoxIn").style.display = "block";
            }

            function collapsChat() {
                document.getElementById("chat-box").style.setProperty("height", "54%", "important");
                document.getElementById("chat-box").style.display = "block";
                document.getElementById("chatBoxIn").style.display = "none";
                document.getElementById("chatBoxOut").style.display = "block";
            }

            chatspan.onclick = function() {
                chatBoxModal.style.display = "none";
            }

            function openListPopup() {
                attendeesListModal.style.display = "block";
                document.getElementById("attendeesList").style.setProperty("z-index", "120");
            }

            coursespan.onclick = function() {
                attendeesListModal.style.display = "none";
            }

            window.onclick = function(event) {
                if (event.target == chatBoxModal) {
                    chatBoxModal.style.display = "none";
                }
                if (event.target == attendeesListModal) {
                    attendeesListModal.style.display = "none";
                }
            }

            $(document).ready(function () {
                $(".slide-toggle").click(function(){
                    $(".box").animate({
                        width: "toggle"
                    });
                });

                $('.btn-filter').on('click', function () {
                    var $target = $(this).data('target');
                    if ($target != 'all') {
                        $('.table tr').css('display', 'none');
                        $('.table tr[data-status="' + $target + '"]').fadeIn('slow');
                    } else {
                        $('.table tr').css('display', 'none').fadeIn('slow');
                    }
                });
            });

            // Handling all of our errors here by alerting them
            function handleError(error) {
                if (error) {
                    //alert(error.message);
                }
            }

            function initializeSession() {
                session = OT.initSession(apiKey, sessionId);

                // Create a publisher
                publisher = OT.initPublisher('subscriber', {
                    insertMode: 'append',
                    width: '100%',
                    height: '100%',
                    name: '<?=$username?>',
                    publishAudio:true,
                    publishVideo:true,
                    style: {buttonDisplayMode: 'on', nameDisplayMode: "on"}
                }, handleError);

                // Connect to the session
                session.connect(token, function(error) {
                    // If the connection is successful, publish to the session
                    if (error) {
                        handleError(error);
                    } else {
                        var node = document.createElement("button");
                        node.setAttribute("id", "video-button-" + publisher.id);
                        node.setAttribute("class", "OT_edge-bar-item OT_mode-on OT_video AVButton fas fa-video");
                        node.setAttribute("onClick", "disableVideo()")
                        document.getElementById(publisher.id).appendChild(node);
                        // document.getElementById("video-button-" + publisher.id).innerHTML = "Disable Video";
                         document.getElementById("video-button-" + publisher.id).style.setProperty("background-color", "#ffffff00", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("font-size", "25px", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("right", "30px", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("top", "4px", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("color", "white", "important");

                        session.publish(publisher, handleError);
                    }
                });

                session.on('signal:msg', function signalCallback(event) {
                    if(event.from.connectionId === session.connection.connectionId){
                        document.getElementById("chat-box").style.display="block";
                        var msg = document.createElement('p');
                        var splitData = event.data.split('|');
                        //msg.textContent = event.data;
                        msg.textContent = splitData[0];
                        msg.setAttribute("id", splitData[1]);
                        msg.className = 'mine';
                        msgHistory.appendChild(msg);
                        msg.scrollIntoView();
                    }
                    else if(event.from.permissions.publish || session.connection.permissions.publish){
                        document.getElementById("chat-box").style.display="block";
                        var msg = document.createElement('p');
                        var splitData = event.data.split('|');
                        //msg.textContent = event.data;
                        msg.textContent = splitData[0];
                        msg.setAttribute("id", splitData[1]);
                        msg.className = 'theirs';
                        msgHistory.appendChild(msg);
                        msg.scrollIntoView();
                    }
                });

                session.on('signal:del-msg', function signalCallback(event) {
                    $("#" + event.data).remove();
                });

                session.on('signal:msg-private', function signalCallback(event) {
                    var msg = document.createElement('p');
                    msg.textContent = event.data;
                    msg.className = event.from.connectionId === session.connection.connectionId ? 'mine' : 'theirs';
                    privateHistory.appendChild(msg);
                    msg.scrollIntoView();
                    allConnections[event.from.connectionId] = event.from;
                    openChatBox(event.from.connectionId);
                });

                session.on({
                    connectionCreated: function (event) {
                        /*if (event.connection.connectionId == session.connection.connectionId) {
                            $.ajax({
                                url: '/tokbox/connection',
                                type: 'POST',
                                contentType: 'application/json', // send as JSON
                                data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId, 'connectionId': event.connection.id, 'role': 'Panelist', 'myConnectionId': session.connection.connectionId, 'username': event.connection.data}),
                                complete: function complete() {
                                    // called when complete
                                    //console.log('connection function completed');
                                },
                                success: function success() {
                                    // called when successful
                                    //console.log('successfully called connection');
                                },
                                error: function error() {
                                    // called when there is an error
                                    //console.log('error calling connection');
                                }
                            });
                        }*/
                        document.getElementById("attendees-list").innerHTML = '';
                        $.ajax({
                            url: '/meeting/virtualfair/pre_student_detail?pid=<?=$participant_id?>',
                            type: 'GET',
                            success: function success(data) {
                                if(data){
                                    data = JSON.parse(data);
                                    if(data.length){
	                                    data.forEach(function(value){
        	                                var tr_node = document.createElement("tr");
                	                        tr_node.setAttribute("id", "attendees-list-" + value.id);

                        	                var tdHtml = '<td>' + value.name + '</td><td>' + value.comp_exam + '</td><td>' + value.comp_exam_score + '</td><td>' + value.gpa_exam + '</td><td>' + value.gpa_exam_score + '</td><td>' + value.work_exp_month + '</td><td>' + value.desired_course + '</td><td>' + value.intake_month + ' ' + value.intake_year + '</td>';

                                	        document.getElementById("attendees-list").appendChild(tr_node);
                                        	document.getElementById("attendees-list-" + value.id).innerHTML = tdHtml;
                                    	});
                                    }
                                }
                            },
                            error: function error() {
                            }
                        });
                    },
                    connectionDestroyed: function connectionDestroyedHandler(event) {
                        //console.log("Destroyed");
                        document.getElementById("attendees-list").innerHTML = '';
                        $.ajax({
                            url: '/meeting/virtualfair/delete_counselor_attendee?pid=<?=$participant_id?>&uid=' + event.connection.data,
                            type: 'GET',
                            complete: function complete() {
                                // called when complete
                                //console.log('connection function completed');
                            },
                            success: function success(data) {
                                if(data){
                                    data = JSON.parse(data);
                                    if(data.length){
	                                    data.forEach(function(value){
        	                                var tr_node = document.createElement("tr");
                	                        tr_node.setAttribute("id", "attendees-list-" + value.id);

                        	                var tdHtml = '<td>' + value.name + '</td><td>' + value.comp_exam + '</td><td>' + value.comp_exam_score + '</td><td>' + value.gpa_exam + '</td><td>' + value.gpa_exam_score + '</td><td>' + value.work_exp_month + '</td><td>' + value.desired_course + '</td><td>' + value.intake_month + ' ' + value.intake_year + '</td>';

                                	        document.getElementById("attendees-list").appendChild(tr_node);
                                        	document.getElementById("attendees-list-" + value.id).innerHTML = tdHtml;
                                    	});
                                    }
                                }
                                // called when successful
                                //console.log('successfully called connection');
                            },
                            error: function error() {
                                // called when there is an error
                                //console.log('error calling connection');
                            }
                        });
                    },
                    streamDestroyed: function (event){
                        if(event.stream.videoType == 'camera'){
                            totalSubscribers--;

                            var gridWidth = '100%',
                                gridHeight = '100%',
                                gridItems = totalSubscribers,
                                grid_str = '',
                                $grid = $('#subscriber'),
                                i;

                            function dimensions(x) {
                                let quotient = parseInt(x / 4);
                                let remainder = x % 4;

                                var w = 25;
                                var l = 40;
                                var divW = 96;
                                var t = 30;

                                var screenDivisor = totalSubscribers + 1;
                                switch(screenDivisor){
                                    case 1:
                                        console.log("I am in 1");
                                        break;
                                    case 2:
                                        w = 50;
                                        l = 20;
                                        divW = 45;
                                        break;
                                    case 3:
                                    case 4:
                                        w = 50;
                                        l = 20;
                                        divW = 45;
                                        t = 20;
                                        break;
                                    case 5:
                                    case 6:
                                        w = 70;
                                        l = 15;
                                        divW = 30;
                                        t = 20;
                                        break;
                                    case 7:
                                    case 8:
                                        w = 98;
                                        l = 5;
                                        divW = 23;
                                        t = 20;
                                        break;
                                    case 9:
                                    case 10:
                                    case 11:
                                    case 12:
                                        w = 98;
                                        l = 5;
                                        divW = 23;
                                        t = 3;
                                        //console.log("I am in 9");
                                        break;
                                }

                                let attrb = "width: " + w + "%; height: 30%; position: relative; left: " + l + "%; top: " + t + "%;";
                                document.getElementById("subscriber").setAttribute("style", attrb);

                                $('#subscriber .OT_root').css({
                                    width: divW + '%',
                                    height: '100%',
                                    marginRight: '10px'
                                });
                            };

                            //$grid.width(gridWidth).height(gridHeight);

                            let subscriberId = allStreams[event.stream.id];

                            //console.log(allStreams);
                            document.getElementById(subscriberId).style.display = 'none';

                            if(!screenShared)
                            {
                                dimensions(totalSubscribers);
                            }

                            if(screenShared)
                            {
                                var w = 100;
                                var divW = 15;

                                var screenDivisor = totalSubscribers + 1;
                                switch(screenDivisor){
                                    case 2:
                                        w = 25;
                                        divW = 45;
                                        break;
                                    case 3:
                                        w = 35;
                                        divW = 30;
                                        break;
                                    case 4:
                                        w = 45;
                                        divW = 23;
                                        break;
                                    case 5:
                                        w = 55;
                                        divW = 18;
                                        break;
                                    case 6:
                                        w = 65;
                                        divW = 15;
                                        break;
                                    case 7:
                                        w = 75;
                                        divW = 13;
                                        break;
                                    case 8:
                                        w = 85;
                                        divW = 12;
                                        break;
                                    case 9:
                                        w = 95;
                                        divW = 10;
                                        break;
                                    case 10:
                                        w = 100;
                                        divW = 9;
                                        break;
                                    case 11:
                                    case 12:
                                        w = 100;
                                        divW = 9;
                                        t = 3;
                                        //console.log("I am in 9");
                                        break;
                                }

                                //document.getElementById("screen-preview").style.display = "block";
                                let attrb = "width: " + w + "%; height: 14%; position: relative; left: 0; top: 1%;";
                                document.getElementById("subscriber").setAttribute("style", attrb);

                                $('#subscriber .OT_root').css({
                                    width: divW + '%',
                                    height: '100%',
                                    marginRight: '10px'
                                });
                            }
                        }
                        if(event.stream.videoType == 'screen'){
                            screenShared = 0;
                            document.getElementById("screen-preview").style.display = "none";
                            $('#share-screen-button').show();

                            var w = 25;
                            var l = 40;
                            var divW = 96;
                            var t = 30;

                            var screenDivisor = totalSubscribers + 1;
                            switch(screenDivisor){
                                case 1:
                                    //console.log("I am in 1");
                                    break;
                                case 2:
                                    w = 50;
                                    l = 20;
                                    divW = 45;
                                    break;
                                case 3:
                                case 4:
                                    w = 50;
                                    l = 20;
                                    divW = 45;
                                    t = 20;
                                    break;
                                case 5:
                                case 6:
                                    w = 70;
                                    l = 15;
                                    divW = 30;
                                    t = 20;
                                    break;
                                case 7:
                                case 8:
                                    w = 98;
                                    l = 5;
                                    divW = 23;
                                    t = 20;
                                    break;
                                case 9:
                                case 10:
                                case 11:
                                case 12:
                                    w = 98;
                                    l = 5;
                                    divW = 23;
                                    t = 3;
                                    //console.log("I am in 9");
                                    break;
                            }

                            let attrb = "width: " + w + "%; height: 30%; position: relative; left: " + l + "%; top: " + t + "%;";
                            document.getElementById("subscriber").setAttribute("style", attrb);

                            $('#subscriber .OT_root').css({
                                width: divW + '%',
                                height: '100%',
                                marginRight: '10px'
                            });
                        }
                    },
                    streamCreated: function(event){
                        if(event.stream.videoType == 'camera'){
                            var subscriber = session.subscribe(event.stream, 'subscriber', {
                                insertMode: 'append',
                                width: '100%',
                                height: '100%',
                                name: event.stream.name,
                            }, handleError);

                            //console.log(allStreams);
                            totalSubscribers++;
                            allStreams[event.stream.id] = subscriber.id;

                            var gridWidth = '100%',
                                gridHeight = '100%',
                                gridItems = totalSubscribers,
                                grid_str = '',
                                $grid = $('#subscriber'),
                                i;

                            function dimensions(x) {
                                let quotient = parseInt(x / 4);
                                let remainder = x % 4;

                                var w = 25;
                                var l = 40;
                                var divW = 96;
                                var t = 30;

                                var screenDivisor = totalSubscribers + 1;
                                switch(screenDivisor){
                                    case 1:
                                        console.log("I am in 1");
                                        break;
                                    case 2:
                                        w = 50;
                                        l = 20;
                                        divW = 45;
                                        break;
                                    case 3:
                                    case 4:
                                        w = 50;
                                        l = 20;
                                        divW = 45;
                                        t = 20;
                                        break;
                                    case 5:
                                    case 6:
                                        w = 70;
                                        l = 15;
                                        divW = 30;
                                        t = 20;
                                        break;
                                    case 7:
                                    case 8:
                                        w = 98;
                                        l = 5;
                                        divW = 23;
                                        t = 20;
                                        break;
                                    case 9:
                                    case 10:
                                    case 11:
                                    case 12:
                                        w = 98;
                                        l = 5;
                                        divW = 23;
                                        t = 3;
                                        //console.log("I am in 9");
                                        break;
                                }

                                let attrb = "width: " + w + "%; height: 30%; position: relative; left: " + l + "%; top: " + t + "%;";
                                document.getElementById("subscriber").setAttribute("style", attrb);

                                $('#subscriber .OT_root').css({
                                    width: divW + '%',
                                    height: '100%',
                                    marginRight: '10px'
                                });
                            };

                            if(!screenShared)
                            {
                                document.getElementById(subscriber.id).style.display = 'inline-block';
                                document.getElementById(subscriber.id).style.boxSizing = 'border-box';
                                document.getElementById(subscriber.id).style.border = '2px solid white';
                                document.getElementById(subscriber.id).style.margin = '2px';

                                dimensions(totalSubscribers);
                            }

                            if(screenShared)
                            {
                                var w = 100;
                                var divW = 15;

                                var screenDivisor = totalSubscribers + 1;
                                switch(screenDivisor){
                                    case 2:
                                        w = 25;
                                        divW = 45;
                                        break;
                                    case 3:
                                        w = 35;
                                        divW = 30;
                                        break;
                                    case 4:
                                        w = 45;
                                        divW = 23;
                                        break;
                                    case 5:
                                        w = 55;
                                        divW = 18;
                                        break;
                                    case 6:
                                        w = 65;
                                        divW = 15;
                                        break;
                                    case 7:
                                        w = 75;
                                        divW = 13;
                                        break;
                                    case 8:
                                        w = 85;
                                        divW = 12;
                                        break;
                                    case 9:
                                        w = 95;
                                        divW = 10;
                                        break;
                                    case 10:
                                        w = 100;
                                        divW = 9;
                                        break;
                                    case 11:
                                    case 12:
                                        w = 100;
                                        divW = 9;
                                        t = 3;
                                        //console.log("I am in 9");
                                        break;
                                }

                                //document.getElementById("screen-preview").style.display = "block";
                                let attrb = "width: " + w + "%; height: 14%; position: relative; left: 0; top: 1%;";
                                document.getElementById("subscriber").setAttribute("style", attrb);

                                $('#subscriber .OT_root').css({
                                    width: divW + '%',
                                    height: '100%',
                                    marginRight: '10px'
                                });
                            }

                            var SpeakerDetection = function(subscriber, startTalking, stopTalking) {
                                var activity = null;
                                subscriber.on('audioLevelUpdated', function(event) {
                                    var now = Date.now();
                                    if (event.audioLevel > 0.2) {
                                        if (!activity) {
                                            activity = {timestamp: now, talking: false};
                                        }
                                        else if (activity.talking) {
                                            activity.timestamp = now;
                                        }
                                        else if (now- activity.timestamp > 500) {
                                            activity.talking = true;
                                            if (typeof(startTalking) === 'function') {
                                                startTalking();
                                            }
                                        }
                                    }
                                    else if (activity && now - activity.timestamp > 3000) {
                                        // detected low audio activity for more than 3s
                                        if (activity.talking) {
                                            if (typeof(stopTalking) === 'function') {
                                                stopTalking();
                                            }
                                        }
                                        activity = null;
                                    }
                                });
                            };

                            SpeakerDetection(subscriber, function() {
                                //console.log('Started Talking ', subscriber.id, subscriber);
                                //document.getElementById(subscriber.id).style.overflow = "inherit";
                                document.getElementById(subscriber.id).style.setProperty("overflow", "inherit", "important");

                                var x = document.getElementById(subscriber.id).querySelectorAll(".OT_name");
                                x[0].style.display = "block";
                                x[0].style.opacity = "1";
                                x[0].style.top = "0";


                                var y = document.getElementById(subscriber.id).querySelectorAll(".OT_widget-container");
                                y[0].style.display = "block";
                                y[0].style.setProperty("border", "3px solid #00ca00", "important");



                            }, function() {
                                document.getElementById(subscriber.id).style.background = "";
                                var x = document.getElementById(subscriber.id).querySelectorAll(".OT_name");
                                x[0].style.display = "none";
                                x[0].style.opacity = "0";
                                x[0].style.top = "-25px";

                                var y = document.getElementById(subscriber.id).querySelectorAll(".OT_widget-container");
                                y[0].style.display = "block";
                                y[0].style.setProperty("border", "none", "important");

                                document.getElementById(subscriber.id).style.setProperty("overflow", "hidden", "important");
                                console.log('stopped talking');
                            });
                        }
                        if(event.stream.videoType == 'screen'){
                            screenShared = 1;
                            $('#share-screen-button').hide();
                            var w = 100;
                            var divW = 15;

                            var screenDivisor = totalSubscribers + 1;
                            switch(screenDivisor){
                                case 2:
                                    w = 25;
                                    divW = 45;
                                    break;
                                case 3:
                                    w = 35;
                                    divW = 30;
                                    break;
                                case 4:
                                    w = 45;
                                    divW = 23;
                                    break;
                                case 5:
                                    w = 55;
                                    divW = 18;
                                    break;
                                case 6:
                                    w = 65;
                                    divW = 15;
                                    break;
                                case 7:
                                    w = 75;
                                    divW = 13;
                                    break;
                                case 8:
                                    w = 85;
                                    divW = 12;
                                    break;
                                case 9:
                                    w = 95;
                                    divW = 10;
                                    break;
                                case 10:
                                    w = 100;
                                    divW = 9;
                                    break;
                                case 11:
                                case 12:
                                    w = 100;
                                    divW = 9;
                                    t = 3;
                                    //console.log("I am in 9");
                                    break;
                            }

                            document.getElementById("screen-preview").style.display = "block";
                            let attrb = "width: " + w + "%; height: 14%; position: relative; left: 0; top: 1%;";
                            document.getElementById("subscriber").setAttribute("style", attrb);

                            $('#subscriber .OT_root').css({
                                width: divW + '%',
                                height: '100%',
                                marginRight: '10px'
                            });
                            session.subscribe(event.stream, 'screen-preview', {
                                insertMode: 'append',
                                width: '100%',
                                height: '100%',
                                name: '<?=$username?>'
                            }, handleError);
                        }
                        //console.log("Stream Created ", event);
                    }
                });

                function resizeSubscriber() {
                    publisher.element.style.width = "100px";
                    publisher.element.style.height = "75px";
                    document.getElementById("subscriber").appendChild(publisher.element);
                }
            }

            function disableVideo(){
                publisher.publishVideo(false);
                document.getElementById("video-button-" + publisher.id).setAttribute("onClick", "enableVideo('" + publisher + "')");
                // document.getElementById("video-button-" + publisher.id).innerHTML = "Enable Video";
                document.getElementById("video-button-" + publisher.id).classList.remove('fa-video');
                document.getElementById("video-button-" + publisher.id).classList.add('fa-video-slash');

            }

            function enableVideo(){
                publisher.publishVideo(true);
                document.getElementById("video-button-" + publisher.id).setAttribute("onClick", "disableVideo('" + publisher + "')");
                // document.getElementById("video-button-" + publisher.id).innerHTML = "Disable Video";
                document.getElementById("video-button-" + publisher.id).classList.remove('fa-video-slash');
                document.getElementById("video-button-" + publisher.id).classList.add('fa-video');

            }

            function disconnectUser(connectionId){
                if (session.capabilities.forceDisconnect == 1) {
                    //alert(connectionId);
                    session.forceDisconnect(allConnections[connectionId]);
                } else {
                    alert("No permission");
                }
            }

            var group_chat_connections = [];
            function showGroupChatButton(value){
                var group_connections = document.getElementsByName("group_chat_connections[]");
                //console.log(value);
                var index = group_chat_connections.indexOf(value);
                if (index > -1) {
                    group_chat_connections.splice(index, 1);
                }
                else{
                    group_chat_connections.push(value);
                }
                if(group_chat_connections.length){
                    document.getElementById("group-chat-button").style.display = "block";
                }
                else{
                    document.getElementById("group-chat-button").style.display = "none";
                }
            }

            function openGroupChat(){
                var html = '<input type="text" placeholder="Input your text here" id="msgTxtOTO"></input><input type="hidden" id="username-private" value="<?=$username?>"></input><input type="button" class="btn btn-danger" onclick="sendGroupChat()" value="Send">';
                document.getElementById("textchatOTO").innerHTML = html;
                // modal.style.display = "block";
                chatBoxModal.style.display = "block";
            }

            function sendGroupChat(connectionId){
                var msg = document.getElementById("msgTxtOTO").value;
                var uname = document.getElementById("username-private").value;
                //console.log(allConnections[connectionId]);
                var msgEle = document.createElement('p');
                msgEle.textContent = uname + ': ' + msg;
                msgEle.className = 'mine';
                privateHistory.appendChild(msgEle);
                msgEle.scrollIntoView();

                for(var i = 0; i < group_chat_connections.length; i++){
                    $.ajax({
                        url: '/tokbox/chat',
                        type: 'POST',
                        contentType: 'application/json', // send as JSON
                        data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId, 'to': group_chat_connections[i], 'msg': msg}),
                        complete: function complete() {
                            // called when complete
                            console.log('chat function completed');
                        },
                        success: function success() {
                            // called when successful
                            console.log('successfully called chat');
                        },
                        error: function error() {
                            // called when there is an error
                            console.log('error calling chat');
                        }
                    });
                    session.signal({
                        to: allConnections[group_chat_connections[i]],
                        type: 'msg-private',
                        data: uname + ': ' + msg
                    }, function signalCallback(error) {
                        if (error) {
                            console.error('Error sending signal:', error.name, error.message);
                        } else {
                            msgTxt.value = '';
                        }
                    });
                }

            }

            function openChatBox(connectionId){
                var html = '<input type="text" placeholder="Input your text here" id="msgTxtOTO"></input><input type="hidden" id="username-private" value="<?=$username?>"></input><input type="button" class="btn btn-danger" onclick="sendChat(\'' + connectionId + '\')" value="Send">';
                document.getElementById("textchatOTO").innerHTML = html;
                // modal.style.display = "block";
                chatBoxModal.style.display = "block";
            }

            function sendChat(connectionId){
                var msg = document.getElementById("msgTxtOTO").value;
                var uname = document.getElementById("username-private").value;
                //console.log(allConnections[connectionId]);
                var msgEle = document.createElement('p');
                msgEle.textContent = uname + ': ' + msg;
                msgEle.className = 'mine';
                privateHistory.appendChild(msgEle);
                msgEle.scrollIntoView();

                $.ajax({
                    url: '/tokbox/chat',
                    type: 'POST',
                    contentType: 'application/json', // send as JSON
                    data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId, 'to': connectionId, 'msg': msg}),
                    complete: function complete() {
                        // called when complete
                        console.log('chat function completed');
                    },
                    success: function success() {
                        // called when successful
                        console.log('successfully called chat');
                    },
                    error: function error() {
                        // called when there is an error
                        console.log('error calling chat');
                    }
                });

                session.signal({
                    to: allConnections[connectionId],
                    type: 'msg-private',
                    data: uname + ': ' + msg
                }, function signalCallback(error) {
                    if (error) {
                        console.error('Error sending signal:', error.name, error.message);
                    } else {
                        msgTxt.value = '';
                    }
                });
            }

            function shareScreen(){
                $('#share-screen-button').hide();
                OT.checkScreenSharingCapability(function(response) {
                    if(!response.supported || response.extensionRegistered === false) {
                        $('#share-screen-button').show();
                    } else if (response.extensionInstalled === false) {
                        $('#share-screen-button').show();
                    } else {
                        // Create a publisher for screen
                        document.getElementById("screen-preview").style.display = "none";
                        var publisherScreen = OT.initPublisher('screen-preview', {
                            maxResolution:{width: 1920, height: 1080},
                            videoSource: 'screen'
                        }, handleError);

                        session.publish(publisherScreen, function(error) {
                            if (error) {
                                // Look error.message to see what went wrong.
                            }
                            else{
                                $('#share-screen-button').hide();
                            }
                        });

                        publisherScreen.on('streamDestroyed', function(event) {
                            //document.getElementById("publisher").style.display = "block";
                            $('#share-screen-button').show();
                            var node = document.createElement("div");
                            node.setAttribute("id", "screen-preview");
                            node.setAttribute("style", "display:none; margin-top: 3%;");
                            document.getElementById("videos").appendChild(node);
                            if (event.reason === 'mediaStopped') {
                                // User clicked stop sharing
                            } else if (event.reason === 'forceUnpublished') {
                                // A moderator forced the user to stop sharing.
                            }
                        });
                    }
                });
            }

            // Send a signal once the user enters data in the form
            form.addEventListener('submit', function submit(event) {
                var senderRole = session.connection.permissions.publish ? 'PUBLISHER' : 'SUBSCRIBER';
                event.preventDefault();
                var currentTime = new Date().getTime();
                var chatId = 'chat-' + currentTime;
                $.ajax({
                    url: '/tokbox/chat',
                    type: 'POST',
                    contentType: 'application/json', // send as JSON
                    data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId, 'to': 'All', 'msg': msgTxt.value, 'from_role': senderRole, 'chat_id': chatId}),
                    complete: function complete() {
                        // called when complete
                        //console.log('chat function completed');
                    },
                    success: function success() {
                        // called when successful
                        //console.log('successfully called chat');
                    },
                    error: function error() {
                        // called when there is an error
                        //console.log('error calling chat');
                    }
                });
                session.signal({
                    type: 'msg',
                    data: username.value + ': ' + msgTxt.value + '|' + chatId
                }, function signalCallback(error) {
                    if (error) {
                        //console.error('Error sending signal:', error.name, error.message);
                    } else {
                        msgTxt.value = '';
                    }
                });
            });

            initializeSession();

            function leaveMeeting(){
                window.open('/admin/common/login/logout', '_self');
            }

            var t_r = setInterval(function() {
              var now = new Date().getTime();
              var distance = 0;

                $.ajax({
                    url: '/meeting/virtualfair/post_counselor_waiting',
                    type: 'GET',
                    success: function success(data) {
                        document.getElementById("waiting-in-queue").innerHTML = data;
                    },
                    error: function error() {
                    }
                });
            }, 60000);

            /*function makeAvailable(){
                $.ajax({
                    url: '/meeting/virtualfair/update_pre_counselor',
                    type: 'POST',
                    contentType: 'application/json', // send as JSON
                    data: JSON.stringify({'id': '<?=$participant_id?>', 'available': 1}),
                    complete: function complete() {
                    },
                    success: function success(data) {
                        alert("You are available now.");
                    },
                    error: function error() {
                    }
                });
            }

            var y = setInterval(function() {
                $.ajax({
                    url: '/meeting/virtualfair/waiting_students_pre_counselling',
                    type: 'GET',
                    success: function success(data) {
                        document.getElementById("waiting-students").textContent = data;
                    },
                    error: function error() {
                    }
                });
            }, 10000);*/
        </script>
    </body>
</html>
