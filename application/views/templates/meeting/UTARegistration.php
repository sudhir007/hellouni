<head>
   <title>Hello Uni : UTA Information Session Day 2021</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
</head>
<style type="text/css">
   @import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");
   .login-block{
   background: #004b7a;  /* fallback for old browsers */
   background: -webkit-linear-gradient(to bottom, #fafafb, #d8d9da);  /* Chrome 10-25, Safari 5.1-6 */
   background: linear-gradient(to bottom, #fafafb, #d8d9da); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
   float:left;
   width:100%;
   padding : 50px 0;
   }
   .login-sec{padding: 50px 30px; position:relative;}
   .login-sec .copy-text{position:absolute; width:80%; bottom:20px; font-size:13px; text-align:center;}
   .login-sec h2{margin-bottom:30px; font-weight:800; font-size:30px; color: #f6882c;    text-align: center;}
   .login-sec h3{margin-bottom:30px; font-weight:600; font-size:25px; color: #f9992f;text-align: left;}
   .login-sec h2:after{content:" "; width:100px; height:5px; background:##815dd5; display:block; margin-top:20px; border-radius:3px; margin-left:auto;margin-right:auto}
   /*.login-sec h3:after{content:" "; width:300px; height:5px; background:#f9992f; display:block; margin-top:20px; border-radius:3px; margin-left:auto;margin-right:auto}*/
   .btn-login{background: #f9992f; color:#fff; font-weight:600;}
   .form-control{
   display: block;
   width: 100%;
   height: 34px;
   padding: 6px 12px;
   font-size: 14px;
   line-height: 1.42857143;
   color: #555;
   background-color: #fff;
   background-image: none;
   border: 1px solid #ccc;
   border-radius: 4px;
   -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
   box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
   -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
   -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
   transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
   }
   .table>tbody>tr>td{
   text-align: center;
   padding: 10px;
   line-height: 1.42857143;
   vertical-align: middle;
   border-top: 1px solid #ddd;
   font-size: 14px;
   font-family: sans-serif;
   letter-spacing: 0.8px;
   font-weight: 600;
   width: 51%;
   }
   fieldset {
   padding: .35em .625em .75em;
   margin: 0 2px;
   border: 1px solid silver;
   background-color: #e0ecf7;
   }
   .fieldset1 {
   padding: .35em .625em .75em;
   margin: 0 2px;
   border: 0px solid silver;
   background-color: #ffffff;
   }
   legend {
   padding: 0;
   border: 0;
   margin-bottom: 20px;
   width: unset;
   }
   .multiselect {
   width: auto;
   margin: 5px;
   }
   .selectBox {
   position: relative;
   }
   .selectBox select {
   width: 100%;
   font-weight: bold;
   }
   .overSelect {
   position: absolute;
   left: 0;
   right: 0;
   top: 0;
   bottom: 0;
   }
   #checkboxes {
   display: none;
   border: 1px #dadada solid;
   }
   #checkboxes label {
   display: block;
   }
   #checkboxes label:hover {
   background-color: #1e90ff;
   }
   #myBtn {
   text-align: center;
   font-size: 18px;
   border: none;
   outline: none;
   background-color: #f46127;
   color: white;
   cursor: pointer;
   padding: 15px;
   border-radius: 4px;
   }
   #myBtn:hover {
   background-color: #555;
   }
   .video-container {
   position: relative;
   padding-bottom: 68.25%; /* 16:9 */
   height: 0;
   }
   .video-container iframe {
   position: absolute;
   top: 0;
   left: 0;
   width: 100%;
   height: 100%;
   }
   .divv p{
   font-size: 18px;
   }
   .divv ul{
   list-style: inside;
   font-size: 20px;
   }

   .margBottm{
        margin-bottom: 10px;
   }
   .container{
    width: 90%;
}
.noPadding{
        padding-right: 0px;
    padding-left: 0px;
}

.modal-dialog {
    width: 600px;
    margin: 5% auto;
    z-index: 10000;
}

.modal-header .close {
    margin-top: -30px;
}
</style>
<section class="login-block" id="forgot-password-page">
   <div class="container" style="background:#fff; border-radius: 10px; box-shadow: 5px 5px 0px rgba(0,0,0,0.1);">
      <div class="row banner-secLogo">
         <img src="<?php echo base_url();?>application/images/UTA.jpg" class="img-fluid"  alt="Responsive Image" width="100%" height="80%" />
      </div>
      <div class="row divv">
         <div class="col-md-12 login-sec">
            <div class="col-md-8">
               <div class="col-md-12">
                  <h3 class="text-center">Thinking about Studying at University of Texas at Arlington?</h3>
                  <p style="text-align: justify;">As the largest university in North Texas and second largest in The University of Texas System, UTA is located in the heart of Dallas-Fort Worth, challenging students to engage with the world around them in ways that make a measurable impact. UTA offers state-of-the-art facilities that encourage students to be critical thinkers. Through academic, internship, and research programs, our students receive real-world experiences that help them contribute to their community and, ultimately, the world. We have more than 180 baccalaureate, masters', and doctoral degree programs, and more than 60,000 students walking our campus or engaging in online coursework each year.</p>
               </div>
               <img src="<?php echo base_url();?>application/images/utaCampus.jpg" class="img-fluid"  alt="Responsive Image" width="100%" height="80%">

            </div>
            <div class="col-md-4">
               <form class="login-form" class="form-horizontal" onsubmit="resgistration()" method="post" autocomplete="off">
                  <fieldset>
                     <!-- <legend>Personal Detail:</legend> -->
                     <div class="col-md-12">
                        <h2>REGISTER FOR UTA DAY!</h2>
                        <h4 style="text-transform: capitalize;text-align: center;margin-bottom: 40px;">Meet Faculty members & avail opportunity to receive an application fee waiver!</h4>
                     </div>
                     <div class="col-md-12 form-group">
                        <div class="col-md-12 margBottm">
                           <label for="first_name" class="text-uppercase">First Name<font color="red">*</font></label>
                           <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" required>
                        </div>
                        <div class="col-md-12 margBottm">
                           <label for="last_name" class="text-uppercase">Last Name<font color="red">*</font></label>
                           <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" required>
                        </div>
                     </div>
                     <div class="col-md-12 form-group">
                        <div class="col-md-12 margBottm">
                           <label for="user_email" class="text-uppercase">Email<font color="red">*</font></label>
                           <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email Id" required>
                        </div>

                     </div>
                     <div class="col-md-12 form-group">
                        <div class="col-md-12 margBottm">
                           <label for="user_mobile" class="text-uppercase">Mobile Number<font color="red">*</font></label>
                           <input type="text" class="form-control" id="user_mobile" name="user_mobile" placeholder="Mobile Number" required>
                        </div>
                        <div class="col-md-12 margBottm">
                           <label for="user_message" class="text-uppercase">Message</label>
                           <textarea class="form-control" placeholder="We are open to student feedback" id="user_message"></textarea>
                        </div>
                     </div>
                     <div class="form-check" style="text-align: center;">
                        <input type="submit"  style="text-align: center; border-radius:5px;background-color: #f46127;border-color: #f46128;" class="btn btn-lg btn-info" value="Register"></input>
                     </div>
                  </fieldset>
                  <!-- <fieldset>
                     <legend>Academic Detail :</legend>
                     <div class="col-md-12 form-group">
                         <div class="col-md-12 form-group">
                             <label for="cschool" class="text-uppercase">Which UTA Department Members would you be interested in meeting?<font color="red">*</font></label>

                             <div class="multiselect ">
                                 <div class="selectBox " onclick="showCheckboxes()">
                                   <select class="form-control searchoptions">
                                     <option>Select an option</option>
                                   </select>
                                   <div class="overSelect"></div>
                                 </div>
                                 <div id="checkboxes" style="padding: 1%;">
                                   <label for="one">
                                     <input type="checkbox" id="COMPUTERSCIENCE" />Computer Science</label>
                                   <label for="two">
                                     <input type="checkbox" id="ELECTRICALENGINEERING" />Electrical Engineering</label>
                                   <label for="three">
                                     <input type="checkbox" id="COLLEGEOFENGINEERING(ALLOTHERMAJORS)" />College of Engineering (All Other Majors)</label>
                                     <label for="one">
                                     <input type="checkbox" id="COLLEGEOFBUSINESS(ALLOTHERMAJORS)" />College of Business (All Other Majors)</label>
                                   <label for="two">
                                     <input type="checkbox" id="GENERALUTA(NON-ENGINEERING&NON-BUSINESS)" />General UTA (Non-Engineering And Non-Business)</label>
                                   <label for="three">
                                     <input type="checkbox" id="OTHER" />Other</label>

                                 </div>
                             </div>
                         </div>

                         <div class="col-md-12 form-group" >
                             <label for="other_cschool" class="text-uppercase" >Which Year are you currently studying in? <font color="red">*</font></label>
                             <select name="cyear" id="cyear" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" onchange="checkOther(this.value)" required>
                                 <option value="">Select year</option>
                                 <option value="2NDYEAR">2nd Year</option>
                                 <option value="3RDYEAR">3rd Year</option>
                                 <option value="4THYEAR">4th Year</option>
                                 <option value="GRADUATE">Graduate</option>option>
                             </select>
                         </div>
                     </div>
                     <div class="col-md-12 form-group">
                         <div class="col-md-12 form-group" >
                             <label for="other_cschool" class="text-uppercase" > Current Stream / Department ? Or if already graduated, your major? <font color="red">*</font></label>
                             <input type="text" class="form-control" id="Stream" name="Stream" placeholder="Stream / Department are you currently studying in" autocomplete="off">
                         </div>
                         <div class="col-md-12 form-group">
                             <label for="cschool" class="text-uppercase">Current School / College you are studying at or have graduated from?<font color="red">*</font></label>
                             <select name="cschool" id="cschool" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" onchange="checkOther(this.value)" required>
                                 <option value="">Select School / College</option>
                                 <?php
                        foreach($colleges as $college)
                        {
                            ?>
                                     <option value="<?php echo $college['college'];?>" <?php if(isset($selected_college) && $selected_college==$college['college']){echo 'selected="selected"';}?> ><?php echo strtoupper($college['college']);?></option>
                                     <?php
                        }
                        ?>
                             </select>
                         </div>


                     </div>
                     <div class="col-md-12 form-group">
                         <div class="col-md-12 form-group">
                             <label for="desired_country" class="text-uppercase" > Are you currently enrolled with Imperial?<font color="red">*</font> </label><br>
                              <input type="radio" id="yes" name="imperial" value="YES">
                               <label for="yes">Yes</label><br>
                               <input type="radio" id="no" name="imperial" value="NO">
                               <label for="no">No</label><br>
                         </div>

                         <div class="col-md-12 form-group">
                             <label for="desired_mainstream" class="text-uppercase"> Imperial Branch Most convenient for you?<font color="red">*</font> </label>
                             <select name="impBranch" id="impBranch" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                             <option value="SANTACRUZ">Mumbai - Santacruz</option>
                             <option value="BORIVALI">Mumbai - Borivali</option>
                             <option value="THANE">Mumbai - Thane</option>
                             <option value="HINJEWADI">Pune - Hinjewadi</option>
                             <option value="KOTHRUD">Pune - Kothrud</option>
                             <option value="OUTMUMBAI">Residing Outside Mumbai / Pune</option>
                             </select>
                         </div>
                      </div>
                     <div class="col-md-12 form-group">
                         <div class="col-md-12 form-group">
                             <label for="desired_mainstream" class="text-uppercase"> Desired Course<font color="red">*</font> </label>
                             <select name="desired_mainstream" id="desired_mainstream" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                             <option value="">Select Course</option>
                             <?php
                        foreach($mainstreams as $course)
                        {
                            ?>
                                 <option value="<?php echo $course->id;?>"><?php echo strtoupper($course->name);?></option>
                                 <?php
                        }
                        ?>
                             </select>
                         </div>
                         <div class="col-md-12 form-group">
                             <label for="intakeyear" class="text-uppercase"> Intake Year<font color="red">*</font> </label>
                             <Select name="intakeyear" id="intakeyear" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                                 <option value="">Select Year</option>
                                 <option value="2020">2020</option>
                                 <option value="2021">2021</option>
                                 <option value="2022">2022</option>
                                 <option value="2023">2023</option>
                             </Select>
                         </div>

                      </div>

                     </fieldset> -->
               </form>

            </div>
            <div class="col-md-12" style="    margin-top: 3%;">
                <div class="col-md-8 noPadding">
                   <div class="col-md-12 noPadding">
                      <h3 class="text-center">On UTA Day, meet individual Faculty and Department members to get their insights on:</h3>
                      <div class="col-md-6">
                         <ul>
                            <li>Admissibility</li>
                            <li>Scholarship Chances</li>
                            <li>Course / Curriculum</li>
                         </ul>
                      </div>
                      <div class="col-md-6">
                         <ul>
                            <li>CPT / RA / TA / GA</li>
                            <li>Research</li>
                         </ul>
                      </div>
                   </div>
                   <div class="col-md-12 noPadding">
                      <h3 class="text-center">Take your career to the next level!</h3>
                      <div class="col-md-6">
                         <ul>
                            <li>Computer Science</li>
                            <li>Information Systems</li>
                            <li>Electrical & Computer Engineering</li>
                            <li>Industrial Engineering</li>
                         </ul>
                      </div>
                      <div class="col-md-6">
                         <ul>
                            <li>Data Science</li>
                            <li>Business Analytics</li>
                            <li>Mechanical Engineering</li>
                            <li>Engineering Management</li>
                         </ul>
                      </div>
                      <div class="col-md-12">
                         <h4 style="color: #f9992f;font-size: 18px;font-weight: 600;">Learn about all the courses at UTA and opportunities for research on UTA Day</h4>
                      </div>
                   </div>
                </div>
                <div class="col-md-4 noPadding">
                  <h3>Student Testimonial</h3>
                  <div class="video-container">
                     <iframe width="560" height="315" src="https://www.youtube.com/embed/soQ0QJygTHA?autoplay=0&fs=0&iv_load_policy=3&showinfo=0&rel=0&cc_load_policy=0&start=0&end=0&origin=http://youtubeembedcode.com" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  </div>
               </div>
            </div>
            <div class="col-md-12">

               <div class="col-md-12">
                  <h3 class="text-center">About HelloUni:</h3>
                  <p style="text-align: justify;">HelloUni is an innovative digital platform that connects you directly with University representatives across USA, Canada, UK, Germany, Australia, New Zealand, Ireland, Netherlands and other European Nations. Interact directly with University representatives to help you understand all your options and clear all your doubts! HelloUni is the ultimate platform for you to ensure that your Global Education is as optimized, clear and hassle free!</p>
               </div>
               <div class="col-md-12">
                  <h3 class="text-center">How can HelloUni help you?</h3>
                  <p style="text-align: justify;">We have partnered with Universities across the globe to help students directly interact with them and ensure a seamless application process. It is as simple as Choose Your University --> Connect with Them --> Start your Application through Us. Our Team of highly experienced counsellors will guide along every step of the way, including Profile Building, Documentation, Help with your Statement of Purpose (SOP), Letter of Recommendation (LOR), Resume, Visa Application and Guidance, Financial Documentation and help with securing an Education Loan. We cover the entire process, end-to-end until departure to your dream University!</p>
               </div>
            </div>
            <div class="col-md-12" style="text-align: center;">
                <button id="myBtn" title="Go to top" data-toggle="modal" data-target="#myModal">APPLY NOW TO REGISTER FOR UTA DAY</button>
            </div>
            <!-- "window.location.href='<?php echo base_url();?>/thankyou'" -->
         </div>
      </div>
   </div>
</section>
<div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title" style="font-weight: 800;font-size: 30px;color: #f6882c;text-align: center;">REGISTER FOR UTA DAY!</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <form class="login-form" class="form-horizontal" onsubmit="resgistration()" method="post" autocomplete="off">
                  <fieldset class="fieldset1">
                     <!-- <legend>Personal Detail:</legend> -->
                     <div class="col-md-12">

                        <h4 style="text-transform: capitalize;text-align: center;margin-bottom: 40px;">Meet Faculty members & avail opportunity to receive an application fee waiver!</h4>
                     </div>

                        <div class="col-md-12 margBottm">
                           <label for="first_name" class="text-uppercase">First Name<font color="red">*</font></label>
                           <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" required>
                        </div>
                        <div class="col-md-12 margBottm">
                           <label for="last_name" class="text-uppercase">Last Name<font color="red">*</font></label>
                           <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" required>
                        </div>

                        <div class="col-md-12 margBottm">
                           <label for="user_email" class="text-uppercase">Email<font color="red">*</font></label>
                           <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email Id" required>
                        </div>


                        <div class="col-md-12 margBottm">
                           <label for="user_mobile" class="text-uppercase">Mobile Number<font color="red">*</font></label>
                           <input type="text" class="form-control" id="user_mobile" name="user_mobile" placeholder="Mobile Number" required>
                        </div>
                        <div class="col-md-12 margBottm">
                           <label for="user_message" class="text-uppercase">Message</label>
                           <textarea class="form-control" placeholder="We are open to student feedback" id="user_message"></textarea>
                        </div>

                     <div class="form-check" style="text-align: center;">
                        <input type="submit"  style="text-align: center; border-radius:5px;background-color: #f46127;border-color: #f46128;" class="btn btn-lg btn-info" value="Register" onclick="window.location.href='<?php echo base_url();?>/thankyou'"></input>
                     </div>
                  </fieldset>
                  <!-- <fieldset>
                     <legend>Academic Detail :</legend>
                     <div class="col-md-12 form-group">
                         <div class="col-md-12 form-group">
                             <label for="cschool" class="text-uppercase">Which UTA Department Members would you be interested in meeting?<font color="red">*</font></label>

                             <div class="multiselect ">
                                 <div class="selectBox " onclick="showCheckboxes()">
                                   <select class="form-control searchoptions">
                                     <option>Select an option</option>
                                   </select>
                                   <div class="overSelect"></div>
                                 </div>
                                 <div id="checkboxes" style="padding: 1%;">
                                   <label for="one">
                                     <input type="checkbox" id="COMPUTERSCIENCE" />Computer Science</label>
                                   <label for="two">
                                     <input type="checkbox" id="ELECTRICALENGINEERING" />Electrical Engineering</label>
                                   <label for="three">
                                     <input type="checkbox" id="COLLEGEOFENGINEERING(ALLOTHERMAJORS)" />College of Engineering (All Other Majors)</label>
                                     <label for="one">
                                     <input type="checkbox" id="COLLEGEOFBUSINESS(ALLOTHERMAJORS)" />College of Business (All Other Majors)</label>
                                   <label for="two">
                                     <input type="checkbox" id="GENERALUTA(NON-ENGINEERING&NON-BUSINESS)" />General UTA (Non-Engineering And Non-Business)</label>
                                   <label for="three">
                                     <input type="checkbox" id="OTHER" />Other</label>

                                 </div>
                             </div>
                         </div>

                         <div class="col-md-12 form-group" >
                             <label for="other_cschool" class="text-uppercase" >Which Year are you currently studying in? <font color="red">*</font></label>
                             <select name="cyear" id="cyear" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" onchange="checkOther(this.value)" required>
                                 <option value="">Select year</option>
                                 <option value="2NDYEAR">2nd Year</option>
                                 <option value="3RDYEAR">3rd Year</option>
                                 <option value="4THYEAR">4th Year</option>
                                 <option value="GRADUATE">Graduate</option>option>
                             </select>
                         </div>
                     </div>
                     <div class="col-md-12 form-group">
                         <div class="col-md-12 form-group" >
                             <label for="other_cschool" class="text-uppercase" > Current Stream / Department ? Or if already graduated, your major? <font color="red">*</font></label>
                             <input type="text" class="form-control" id="Stream" name="Stream" placeholder="Stream / Department are you currently studying in" autocomplete="off">
                         </div>
                         <div class="col-md-12 form-group">
                             <label for="cschool" class="text-uppercase">Current School / College you are studying at or have graduated from?<font color="red">*</font></label>
                             <select name="cschool" id="cschool" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" onchange="checkOther(this.value)" required>
                                 <option value="">Select School / College</option>
                                 <?php
                        foreach($colleges as $college)
                        {
                            ?>
                                     <option value="<?php echo $college['college'];?>" <?php if(isset($selected_college) && $selected_college==$college['college']){echo 'selected="selected"';}?> ><?php echo strtoupper($college['college']);?></option>
                                     <?php
                        }
                        ?>
                             </select>
                         </div>


                     </div>
                     <div class="col-md-12 form-group">
                         <div class="col-md-12 form-group">
                             <label for="desired_country" class="text-uppercase" > Are you currently enrolled with Imperial?<font color="red">*</font> </label><br>
                              <input type="radio" id="yes" name="imperial" value="YES">
                               <label for="yes">Yes</label><br>
                               <input type="radio" id="no" name="imperial" value="NO">
                               <label for="no">No</label><br>
                         </div>

                         <div class="col-md-12 form-group">
                             <label for="desired_mainstream" class="text-uppercase"> Imperial Branch Most convenient for you?<font color="red">*</font> </label>
                             <select name="impBranch" id="impBranch" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                             <option value="SANTACRUZ">Mumbai - Santacruz</option>
                             <option value="BORIVALI">Mumbai - Borivali</option>
                             <option value="THANE">Mumbai - Thane</option>
                             <option value="HINJEWADI">Pune - Hinjewadi</option>
                             <option value="KOTHRUD">Pune - Kothrud</option>
                             <option value="OUTMUMBAI">Residing Outside Mumbai / Pune</option>
                             </select>
                         </div>
                      </div>
                     <div class="col-md-12 form-group">
                         <div class="col-md-12 form-group">
                             <label for="desired_mainstream" class="text-uppercase"> Desired Course<font color="red">*</font> </label>
                             <select name="desired_mainstream" id="desired_mainstream" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                             <option value="">Select Course</option>
                             <?php
                        foreach($mainstreams as $course)
                        {
                            ?>
                                 <option value="<?php echo $course->id;?>"><?php echo strtoupper($course->name);?></option>
                                 <?php
                        }
                        ?>
                             </select>
                         </div>
                         <div class="col-md-12 form-group">
                             <label for="intakeyear" class="text-uppercase"> Intake Year<font color="red">*</font> </label>
                             <Select name="intakeyear" id="intakeyear" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                                 <option value="">Select Year</option>
                                 <option value="2020">2020</option>
                                 <option value="2021">2021</option>
                                 <option value="2022">2022</option>
                                 <option value="2023">2023</option>
                             </Select>
                         </div>

                      </div>

                     </fieldset> -->
               </form>
        </div>

        <!-- Modal footer -->
      <!--   <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div> -->

      </div>
    </div>
  </div>
<script type="text/javascript">
   $( document ).ready(function() {
       $('#desired_mainstream').on('change', function() {
           var dataString = 'id='+this.value;
           $.ajax({
               type: "POST",
               url: "/search/university/getSubcoursesList",
               data: dataString,
               cache: false,
               success: function(result){
                   $('#desired_subcourse').html(result);
               }
           });
       });

       // verify otp_number
       $('#verifyotp').on('click', function() {
           var mobileNumber = $('#user_mobile').val();
           var otpNumber = $('#otp_number').val();
           var dataString = 'mobile_number=' + mobileNumber + '&otp=' + otpNumber;
           $.ajax({
               type: "POST",
               url: "/event/verify_otp",
               data: dataString,
               cache: false,
               success: function(result){
                   result = JSON.parse(result);
                   alert(result['message']);
               }
           });
       });
   })

   //registration

   function resgistration(){
       var first_name = $('#first_name').val();
       var last_name = $('#last_name').val();
       var user_email = $('#user_email').val();
       var user_mobile = $('#user_mobile').val();
       var user_message = $('#user_message').val();
       /*var desired_country = $('#desired_country').val();
       var desired_mainstream = $('#desired_mainstream').val();
       var desired_subcourse = $('#desired_subcourse').val();
       var ccity = $('#ccity').val();
       var cschool = $('#cschool').val();
       var othercschool = $('#other_cschool').val();
       var desired_levelofcourse = $('#desired_levelofcourse').val();
       var userPassword = $('#password').val();
       var intake_month = $('#intakemonth').val();
       var intake_year = $('#intakeyear').val();
       var comp_exam = $('#competativeexam').val();
       var comp_exam_score = $('#competativeexamscore').val();
       var gpa_exam = $('#gpaexam').val();
       var gpa_exam_score= $('#gpaexamscore').val();
       var work_exp = $('#workexperience').val();
       var work_exp_month = $('#workexperiencemonths').val();
       var otpNumber = $('#otp_number').val();
       var study_year = $('#yearstudy').val();*/

       //var dataString = 'first_name=' + first_name + '&last_name=' + last_name + '&user_email=' + user_email + '&user_mobile=' + user_mobile + '&desired_country=' + desired_country + '&desired_mainstream=' + desired_mainstream + '&desired_subcourse=' + desired_subcourse + '&desired_levelofcourse=' + desired_levelofcourse + '&user_password=' + userPassword + '&ccity=' + ccity + '&cschool=' + cschool + '&other_cschool=' + othercschool + '&intake_month=' + intake_month + "&intake_year=" + intake_year + '&comp_exam=' + comp_exam + '&comp_exam_score=' + comp_exam_score + "&gpa_exam=" + gpa_exam + "&gpa_exam_score=" + gpa_exam_score + "&work_exp=" + work_exp + "&work_exp_month=" + work_exp_month + "&otp=" + otpNumber + "&study_year=" + study_year;

       var dataString = 'first_name=' + first_name + '&last_name=' + last_name + '&user_email=' + user_email + '&user_mobile=' + user_mobile + '&user_message=' + user_message;

       event.preventDefault();

       $.ajax({
           type: "POST",
           url: "/event/virtualfair_registration",
           data: dataString,
           cache: false,
           success: function(result){
               result = JSON.parse(result);
               if(result['success']){
                   //window.location.href='/event/reception/' + result['encodedUsername'];
                   window.location.href='/thankyou';
                   //alert("Congratulation!! You have registered successfully. Please come back on event day");
               }
               else{
                   alert(result['message']);
               }
           }
       });
   }

   function login(){
       //alert("Please come back on event day");
       var user_mobile = $('#user_mobile_signin').val();
       var userPassword = $('#password_signin').val();

       var dataString = 'user_mobile=' + user_mobile + '&user_password=' + userPassword;

       event.preventDefault();

       $.ajax({
           type: "POST",
           url: "/event/virtualfair_login",
           data: dataString,
           cache: false,
           success: function(result){
               result = JSON.parse(result);
               if(result['success']){
                   //window.location.href='/event/reception/' + result['encodedUsername'];
                   //alert("Congratulation!! You have registered successfully. Please come back on event day");
                   window.location.href='/event/reception/' + result['encodedUsername'];
               }
               else{
                   alert(result['message']);
               }
           }
       });
   }

   // sent OTP
   function sendOTP() {
       var mobileNumber = $('#user_mobile').val();
       var mobilenumberlength = $('#user_mobile').val().length;
       var userEmail = $('#user_email').val();

       if(mobilenumberlength != 10) {
           alert("Please enter valid mobile!!!");
       }
       else if(!userEmail) {
           alert("Please enter email!!!");
       }
       else {
           var dataString = 'mobile_number=' + mobileNumber + '&user_email=' + userEmail;

           $.ajax({
               type: "POST",
               url: "/event/send_otp",
               data: dataString,
               cache: false,
               success: function(result){
                   result = JSON.parse(result);
                   alert(result['message']);
                   document.getElementById("send_otp").style.display = "none";
                   document.getElementById("verifyotp").style.display = "block";
               }
           });
       }
   }

   function checkOther(value){
       document.getElementById("other_college").style.display = "none";
       if(value == 'Other'){
           document.getElementById("other_college").style.display = "block";
       }
   }

   function checkComExam(value){
       document.getElementById("competativeexamscored").style.display = "block";
       if(value == 'NOTGIVEN'){
           document.getElementById("competativeexamscored").style.display = "none";
       }
   }

   function checkGpa(value){
       document.getElementById("gpaexamscored").style.display = "block";
       if(value == 'NOTGIVEN'){
           document.getElementById("gpaexamscored").style.display = "none";
       }
   }

   function checkWorkExp(value){
       document.getElementById("workexperiencemonthsd").style.display = "none";
       if(value == 'Yes'){
           document.getElementById("workexperiencemonthsd").style.display = "block";
       }
   }

   $(function() {
       $('#levelselector').change(function(){
           $('.levelClass').hide();
           $('#' + $(this).val()).show();
       });
   });

   var expanded = false;

   function showCheckboxes() {
     var checkboxes = document.getElementById("checkboxes");
     if (!expanded) {
       checkboxes.style.display = "block";
       expanded = true;
     } else {
       checkboxes.style.display = "none";
       expanded = false;
     }
   }
</script>
