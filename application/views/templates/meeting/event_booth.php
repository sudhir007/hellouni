<html>
   <head>
      <title> Hellouni : Virtual Fair 2021 </title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link href="<?=base_url()?>css/newFairDesign.css?v=1.1" rel="stylesheet">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
      <style type="text/css">
         body {
             width: 100%;
             height: 100%;
             background-color: #26002A;
         }
         .alert {
           padding: 10px;
           background-color: #f6882c;
           color: white;
             z-index: 10;
             bottom: 0;
             position: fixed;
             left: 0;
             text-align: center;
             display: none; /* Hidden by default */
         }

         .closebtn {
           margin-left: 15px;
           color: white;
           font-weight: bold;
           float: right;
           font-size: 22px;
           line-height: 20px;
           cursor: pointer;
           transition: 0.3s;
         }

         .closebtn:hover {
           color: black;
         }

         .btnpurp {
             background-color: #6d326f !important;
          }
         .modal{
             display: none; /* Hidden by default */
             position: fixed; /* Stay in place */
             z-index: 1; /* Sit on top */
             padding-top: 100px; /* Location of the box */
             left: 0;
             top: 0;
             width: 100%; /* Full width */
             height: 100%; /* Full height */
             overflow: auto; /* Enable scroll if needed */
             background-color: rgb(0,0,0); /* Fallback color */
             background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
             z-index: 112;
         }
         .modal-content{
             background-color: #fefefe;
             margin: auto;
             border: 1px solid #888;
             width: 50%;
         }
         #modal-iframe-content{
             width: 85%;
             height: 95%;
         }
         #modal-join-queue-content, #modal-check-queue-content{
             width: 50%;
             height: 20%;
         }

        .popover{
          position: absolute;
          top: -154 !important;
          left: -150% !important;
          display: block;
          width: max-content;
          height: 150px;
          color: black;
         }
         .popover-content {
              padding: 9px 14px;
              transform: scale(-1, 1);
          }
          .popover-title {
              padding: 8px 14px;
              margin: 0;
              font-size: 14px;
              background-color: #f6882c;
              border-bottom: 1px solid #f6882c;
              border-radius: 5px 5px 0 0;
              transform: scale(-1, 1);
          }
          .blur-content {
            -webkit-filter: blur(4px);
            -moz-filter: blur(4px);
            -o-filter: blur(4px);
            -ms-filter: blur(4px);
            filter: blur(4px);
            /* width: 100px; */
            /* height: 100px; */
            /* background-color: #ccc;*/
            pointer-events: none;
          }
          .lockIcon{
            color: white;
            font-size: 50px;
            position: absolute;
            z-index: 10;
            top: 40%;
            left: 46%;
          }


          .zoom:hover {
            -ms-transform: scale(2.5); /* IE 9 */
            -webkit-transform: scale(2.5); /* Safari 3-8 */
            transform: scale(2.5);
          }

          .feature-box h3 a{
             color: #FFFFFF !important;
         }

      </style>
   </head>
   <body>
      <header>
         <nav class="navigation" style="justify-content: flex-start;">
            <!-- Logo -->
            <div class="logo"><img class="img-responsive" src="/images/Fair2021/ImperialLogo-01.jpg" onclick="imageClick('https://bit.ly/2RSGRFm')" width="130" height="60" /></div>
            <!-- Navigation -->
            <ul class="menu-list" style="border-left: 1px solid white;margin-left: 2%;padding-left: 8px;">
               <li style="width: 30%"><a href="<?php echo base_url();?>event/booths/<?=$encodedUsername?>" ><i class="fa fa-chevron-circle-left" style="font-size:20px;"></i> Go Back</a></li>
               <span style="color: white;margin-left: 5px; font-size: 20px; font-weight: bold;"><?=$fairUniversity['name']?></span></li>
            </ul>
            <div class="logo rmargin" style="right: 0;position: absolute;">
               <img class="img-responsive" src="/images/Fair2021/HelloUniLogo-01.jpg" onclick="imageClick('https://bit.ly/2RSGRFm')" width="130" height="60" />
            </div>
         </nav>
      </header>
      <div class="container" style="top: 10%;position: relative;" >
      <div class="col-md-12 col-lg-12 divview" >
        <div id="videoUniDetails">
            <!-- Auto play & javascript code at line 390 -->
           <!--video id="mp4" controls loop controlsList="nodownload noremote foobar">
              <source src="<?=$fairUniversity['video']?>" type="video/mp4">
           </video-->
           <!-- No Auto play -->
           <video id="mp4" controls preload="none" poster="<?=$fairUniversity['campus_photos']?>">
              <source src="<?=$fairUniversity['video']?>" type="video/mp4">
           </video>
           <div id="spinner" class="poster" style="display: none;">
              <div class="cube"></div>
              <div class="cube c2"></div>
              <div class="cube c4"></div>
              <div class="cube c3"></div>
           </div>
        </div>
        <div class="col-md-12  divview">

           <div class="col-12 col-sm-12 col-md-12 col-lg-3 p-0 nopadding0" style="    margin-top: -25px;text-align: center;">
              <img src="<?=$fairUniversity['logo']?>" alt="" style="width: 45%!important" />
           </div>
           <div class="col-12 col-sm-12 col-md-12 col-lg-6 p-0 nopadding0">
              <div class="detail-box">
                 <h4><?=$fairUniversity['name']?></h4>
                 <p>
                    <?=$fairUniversity['one_line_description']?>
                 </p>
              </div>
           </div>
           <div class="col-12 col-sm-12 col-md-12 col-lg-3 p-0 nopadding0">
              <div class="social-icons">
                 <ul class="nomargin">
                    <a href="<?=$fairUniversity['website']?>" target="_blank"><i class="fa fa-external-link-square fa-3x social-fb" id="social" title="Website"></i></a>
                    <a href="<?=$fairUniversity['facebook_page']?>" target="_blank"><i class="fa fa-facebook-square fa-3x social-fb" id="social" title="facebook"></i></a>
                    <a href="<?=$fairUniversity['twitter_page']?>" target="_blank"><i class="fa fa-twitter-square fa-3x social-tw" id="social" title="twitter"></i></a>
                    <a href="<?=$fairUniversity['linkedin_page']?>" target="_blank"><i class="fa fa-linkedin-square fa-3x social-em" id="social" title="linkedin"></i></a>
                    <a href="<?=$fairUniversity['youtube_page']?>" target="_blank"><i class="fa fa-youtube-square fa-3x social-gp" id="social" title="youtube"></i></a>
                    <a href="<?=$fairUniversity['instagram_page']?>"><i class="fa fa-instagram fa-3x social-gp" id="social" title="instagram"></i></a>
                 </ul>
              </div>
           </div>
        </div>
        <div class="col-md-12 divview descBox">
           <h4>About <?=$fairUniversity['name']?> ,</h4>
           <p><?=$fairUniversity['long_description']?></p>
        </div>
        <?php
        if($fairUniversity['usp'])
        {
            ?>
            <div class="col-md-12 divview features">
               <div class="col-lg-12 mt-10 mt-lg-0 d-flex ">
                  <div class="headingline">
                     <h3>University Highlights</h3>
                  </div>
                  <div class="row align-self-center gy-4">
                      <?php
                      $usps = json_decode($fairUniversity['usp'], true);
                      foreach($usps as $usp)
                      {
                          ?>
                          <div class="col-md-6 ">
                             <div class="feature-box d-flex align-items-center">
                                <h3><?=$usp?></h3>
                             </div>
                          </div>
                          <?php
                      }
                      ?>
                  </div>
               </div>
            </div>
            <?php
        }
        ?>
        <div class="col-md-12 divview features">
           <div class="col-lg-12 mt-10 mt-lg-0 d-flex">
              <div class="headingline">
                 <h3>Meet The University</h3>
              </div>
              <div class="row align-self-center gy-4">
                 <?php
                 if($fairUniversity['brochures'])
                 {
                     ?>
                     <div class="col-md-6 " >
                        <div class="feature-box d-flex align-items-center" style="    padding: 9px 20px;">
                           <h3>University Brochure : &nbsp;<a href="/event/download_brochure/<?=$encodedBoothId?>"><button class="btn btn-md btnorange"><i class="fa fa-download"></i> Download</button></a></h3>
                        </div>
                     </div>
                     <?php
                 }
                 ?>
                 <div <?=$fairUniversity['brochures'] ? 'class="col-md-6"' : 'class="col-md-12"'?> >
                    <div class="feature-box d-flex align-items-center" style="    padding: 11px 20px;">
                       <h3>
                          <i class="fa fa-arrow-circle-down"></i> &nbsp;Talk To Us On Table :
                          <div class="avatars popoverDiv">
                              <?php
                              foreach($fairDelegates as $index => $delegate)
                              {
                                  ?>
                                  <span class="avatar">
                                  <a href="#" data-toggle="popover-<?=$index?>"><img src="https://picsum.photos/70" width="10" height="10" class="avatarimg"/>
                                  </a></span>
                                  <?php
                              }
                              ?>
                             <!-- Variable amount more avatars -->
                          </div>
                          Available
                       </h3>
                    </div>
                 </div>
              </div>
           </div>
        </div>
        <div class="col-md-12 divview features">
           <div class="col-lg-12 mt-10 mt-lg-0 d-flex">
              <?php
              foreach($encodedRoomIds as $index => $encodedRoomDetail)
              {
                  if($index == 0)
                  {
                      ?>
                      <div class="row align-self-center gy-4">
                      <?php
                  }
                  if($index == 2)
                  {
                      ?>
                      <div class="row align-self-center gy-4" style="margin-top: 16px">
                      <?php
                  }
                  ?>
                  <div class="col-md-6 col-lg-6 newd">
                      <div class="tableBox <?=$encodedRoomDetail['room_id'] == -1 ? 'blur-content' : ''?>">
                        <div class="col-md-12 c" style="padding-left: 0px;">
                           <div class="col-md-1 tableNo">
                              <h4><?=($index+1)?></h4>
                           </div>
                           <div class="col-md-9 tableName" >

                              <!-- Important div dont comment this div -->
                          </div>
                           <div class="col-md-2 orgPic">
                              <img src="https://picsum.photos/70" width="20" height="20" style="width: 45% !important;" class="zoom">
                           </div>
                        </div>
                        <div class="maincircle">
                           <div class="inner">
                              <div class="cenText">
                                 <h4 class="orgName"><?=$encodedRoomDetail['room_id'] != -1 ? $panelist[$encodedRoomDetail['room_id']] : 'Not Available'?></h4>
                              </div>
                              <?php
                              for($i = 0; $i < $encodedRoomDetail['student_count']; $i++)
                              {
                                  $imgId = $i + 1;
                                  ?>
                                  <div class="circle<?=$imgId?> circle Chairb<?=$imgId?>" data-angle="<?=$i * 45?>"><a href="#"><img src="/images/Fair2021/Chairb<?=$imgId?>.png" alt="" width="10" height="10" id="img-<?=$encodedRoomDetail['room_id']. '-' . $imgId?>"></a></div>
                                  <?php
                              }
                              for($j = $encodedRoomDetail['student_count']; $j < 8; $j++)
                              {
                                  $imgId = $j + 1;
                                  if($encodedRoomDetail['room_id'] != -1)
                                  {
                                      ?>
                                      <div class="circle<?=$imgId?> circle Chair<?=$imgId?>" data-angle="<?=$j * 45?>"><a href="javascript:void(0);" class="seatlink" onclick="openRoom('<?=$encodedRoomDetail["room_id"]?>', '<?=$imgId?>', this)"><img src="/images/Fair2021/Chair<?=$imgId?>.png" alt="" width="10" height="10" /></a></div>
                                      <?php
                                  }
                                  else
                                  {
                                      ?>
                                      <div class="circle<?=$imgId?> circle Chair<?=$imgId?>" data-angle="<?=$j * 45?>"><a href="javascript:void(0);" id="Chairb<?=$imgId?>" class="seatlink"><img src="/images/Fair2021/Chair<?=$imgId?>.png" alt="" width="10" height="10" ></a></div>
                                      <?php
                                  }
                              }
                              ?>
                           </div>
                        </div>
                        <div class="col-md-12 boxBottom" onclick="joinQueue('<?=$encodedRoomDetail['room_id']?>')" <?=$encodedRoomDetail['student_count'] < $roomLimit ? 'style="display: none;"' : ''?>>
                           <button class="btn btn-md btnSeat">Join Queue</button>
                        </div>
                        <div class="col-md-12 boxBottom" <?=$encodedRoomDetail['student_count'] >= $roomLimit ? 'style="display: none;"' : ''?>>
                           <button class="btn btn-md btnSeat">Take A Seat</button>
                        </div>
                     </div>
                  </div>
                  <?php
                  if($index == 1 || ($index == 0 && count($encodedRoomIds) == 1) || $index == 3 || ($index == 2 && count($encodedRoomIds) == 3))
                  {
                      ?>
                       </div>
                      <?php
                  }
                  ?>
                  <?php
              }
              ?>
           </div>
        </div>
      </div>
      </div>
      <div id="modal-iframe" class="modal" >
         <!-- Modal content -->
         <div class="row modal-content" id="modal-iframe-content">
            <div style="width:100%; height:100%;">
               <button id="close-iframe" class="btn btn-md btn-primary leaveBtn" style="float:right;" onclick="closeIframe()">Leave Meeting</button>
               <iframe id="forPostyouradd" class="responsive-iframe embed-responsive-item" data-src="" src="about:blank" style="height: 96%; width: 100%; background:#ffffff; display:none" allowfullscreen></iframe>
            </div>
         </div>
      </div>
      <div id="modal-join-queue" class="modal" >
         <!-- Modal content -->
         <div class="row modal-content" id="modal-join-queue-content">
             <div style="text-align:center; font-weight:bold;">
                 You are already in queue for a different room. Would you like to remove from other room and join this room queue?
             </div>
            <div style="width:100%; height:100%; margin-top:20px; text-align:center;">
               <button class="btn btn-md btn-primary" onclick="joinQueueYes()" style="float:left;">Yes</button>
               <button class="btn btn-md btn-primary" onclick="joinQueueNo()" style="float:right;">No</button>
            </div>
         </div>
      </div>

      <div class="alert" id="modal-check-queue">
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
        <div style="text-align:center; font-weight: bold;" id="modal-check-queue-message"></div>
        <button class="btn btn-md btnred btnpurp" onclick="joinQueueNo()" style="float:left;">May Be Later</button>
        <button class="btn btn-md btnorange btnpurp" onclick="gotoBooth()" id="room-page" style="float:right;">Go To Room Page</button>
        <button class="btn btn-md btnorange btnpurp" onclick="joinRoom()" id="join-room" style="float:right;">Join Room</button>
      </div>
      <script type="text/javascript">
          $('.popoverDiv a').click(function (event) {
           event.preventDefault();
           // $('[data-toggle="popover"]').popover();

           <?php
           foreach($fairDelegates as $index => $delegate)
           {
               ?>
               $('[data-toggle="popover-<?=$index?>"]').popover({
                        placement : 'top',
                    trigger : 'click',
                        html : true,
                         content : '<div class="col-md-12 nopadding0"><div class="col-md-6"><img src="<?=$delegate['profile_pic']?>" style="width: 80px !important;margin-top: 10px;"></div><div class="col-md-6"<div class="row"><h5 style="color: #f6882c;font-weight: 800;font-size: 16px;"><?=$delegate['name']?></h5><p><?=$delegate['designation']?></p></div></div></div>'
                    });
               <?php
           }
           ?>

         });
      </script>
      <script type="text/javascript">
      /*
         var video = document.getElementById("mp4");
         var spinner = document.getElementById("spinner");
         var delayMillis = 4000;
         var spinnerIsHere = 1;
         video.volume = 0;

         var playVid = setTimeout(function() {
             if(spinnerIsHere == 1) {
                 spinner.style.visibility = "hidden";
                 spinnerIsHere = 0;
             }
             video.play();
         }, delayMillis);

         video.addEventListener("click", function( event ) {
             if(video.paused) {
                 if(spinnerIsHere == 1) {
                     spinner.style.visibility = "hidden";
                     spinnerIsHere = 0;
                 }
                 clearTimeout(playVid);
                 video.play();
             } else {
                 video.pause();
                 if(spinnerIsHere == 0) {
                     spinner.style.visibility = "visible";
                     spinnerIsHere = 1;
                 }
             }
         }, false);
         */

         var currentRoomId = '';
         var queuedRoomId = '';

         var joinQueueCheck = 0;
         var joinRoomId = '';
         var boothId = '';

         var chairId = '';
         var chairObj = '';

         jQuery(function($){
             !jQuery.easing && (jQuery.easing = {});
             !jQuery.easing.easeOutQuad && (jQuery.easing.easeOutQuad = function( p ) { return 1 - Math.pow( 1 - p, 2 ); });

             var circleController = {
                 create: function( circle ){
                     var obj = {
                         angle: circle.data('angle'),
                         element: circle,
                         measure: $('<div />').css('width', 360 * 8 + parseFloat(circle.data('angle'))),
                         update: circleController.update,
                         reposition: circleController.reposition,
                     };
                     obj.reposition();
                     return obj;
                 },
                 update: function( angle ){
                     this.angle = angle;
                     this.reposition();
                 },
                 reposition: function(){
                     var radians = this.angle * Math.PI / 180, radius = 268   / 2;
                     this.element.css({
                         marginLeft: (Math.sin( radians ) * radius - 50) + 'px',
                         marginTop: (Math.cos( radians ) * radius - 30) + 'px'
                     });
                 }
             };

             var spin = {
                 circles: [],
                 left: function(){
                     var self = this;
                     $.each(this.circles, function(i, circle){
                         circle.measure.stop(true, false).animate(
                             { 'width': '-=45' },
                             {
                                 easing: 'easeOutQuad',
                                 duration: 1000,
                                 step: function( now ){ circle.update( now ); }
                             }
                         );
                     });
                 },
                 right: function(){
                     var self = this;
                     $.each(this.circles, function(i, circle){
                         circle.measure.stop(true, false).animate(
                             { 'width': '+=45' },
                             {
                                 easing: 'easeOutQuad',
                                 duration: 1000,
                                 step: function( now ){ circle.update( now ); }
                             }
                         );
                     });
                 },
                 prep: function( circles ){
                     for ( var i=0, circle; i<circles.length; i++ ) {
                         this.circles.push(circleController.create($(circles[i])));
                     }
                 }
             };

             spin.prep($('.circle'));

         });

         function openRoom(roomId, imgId, thisObj){
             $(thisObj).find('img').attr('src', '/images/Fair2021/Chairb' + imgId + '.png');

             let src = "/event/student/" + roomId + "/<?=$encodedUsername?>";

             currentRoomId = roomId;
             chairId = imgId;
             chairObj = thisObj;

             var iframe  = document.getElementById("forPostyouradd");
             iframe.setAttribute('src', src);
             iframe.style.display = 'block';

             document.getElementById("modal-iframe").style.display = "block";
         }

         // function hideAll() {
         //     $("[data-toggle=popover]").each(function () {
         //         var popover = $(this).data('bs.popover');
         //         if (popover.tip().is(':visible')) popover.hide();
         //     });
         // }

         function closeIframe(){
             $.ajax({
                 url: '/event/leave_meeting/' + currentRoomId + '/<?=$encodedUsername?>',
                 type: 'GET',
                 complete: function complete() {
                 },
                 success: function success(data) {
                     var iframe  = document.getElementById("forPostyouradd");
                     iframe.setAttribute('src', '');

                     let iframeModal = document.getElementById("modal-iframe");
                     iframeModal.style.display = "none";

                     if(chairId && chairObj)
                     {
                         $(chairObj).find('img').attr('src', '/images/Fair2021/Chair' + chairId + '.png');
                     }
                 },
                 error: function error() {
                     alert("Some issue has been occured. Please refresh the page");
                     //window.location.href='/event/booth/<?=$encodedBoothId?>/<?=$encodedUsername?>';
                 }
             });
         }

         // $("[data-toggle=popover]")
         // .popover({animation: false, container: document.body, delay: 50, html: true, trigger: 'manual', placement: 'auto top'})
         // .each(function () {
         //     var popover = $(this).data('bs.popover');
         //     var $tip = popover.tip();
         //     var delay = popover.options.delay || 0;
         //     var showDelay = delay.show || delay || 0;
         //     var hideDelay = delay.hide || delay || 0;
         //     var showTimeout = null
         //     var hideTimeout = null;

         //     $(this).add($tip).hover(function show() {
         //         if (hideTimeout) {
         //             clearTimeout(hideTimeout);
         //             hideTimeout = null;
         //         }
         //         else if (!$tip.is(':visible')) {
         //             hideAll();
         //             showTimeout = setTimeout(function () {
         //                 popover.show();
         //                 showTimeout = null;
         //             }, showDelay);
         //         }
         //     },
         //     function hide() {
         //         if (showTimeout) {
         //             clearTimeout(showTimeout);
         //             showTimeout = null;
         //         }
         //         else if ($tip.is(':visible')) {
         //             hideTimeout = setTimeout(function () {
         //                 popover.hide();
         //                 hideTimeout = null;
         //             }, hideDelay);
         //         }
         //     });
         // });

         function joinQueue(roomId){
             queuedRoomId = roomId;
             $.ajax({
                 url: '/event/join_queue/' + roomId + '/<?=$encodedUsername?>',
                 type: 'GET',
                 complete: function complete() {
                 },
                 success: function success(data) {
                     data = JSON.parse(data);
                     if(data.success){
                         alert(data.msg);
                     }
                     else{
                         let joinQueueModal = document.getElementById("modal-join-queue");
                         joinQueueModal.style.display = "block";
                     }
                 },
                 error: function error() {
                     alert("Some issue has been occured. Please refresh the page");
                 }
             });
         }

         function joinQueueYes(){
             let joinQueueModal = document.getElementById("modal-join-queue");
             joinQueueModal.style.display = "none";

             $.ajax({
                 url: '/event/join_queue/' + queuedRoomId + '/<?=$encodedUsername?>/1',
                 type: 'GET',
                 complete: function complete() {
                 },
                 success: function success(data) {
                     data = JSON.parse(data);
                     alert(data.msg);
                 },
                 error: function error() {
                     alert("Some issue has been occured. Please refresh the page");
                 }
             });
         }

         function gotoBooth(){
             let joinQueueModal = document.getElementById("modal-check-queue");
             joinQueueModal.style.display = "none";

             window.location.href='/event/booth/' + boothId + '/<?=$encodedUsername?>';
         }

         function joinQueueNo(){
             let joinQueueModal = document.getElementById("modal-join-queue");
             joinQueueModal.style.display = "none";

             let checkQueueModal = document.getElementById("modal-check-queue");
             checkQueueModal.style.display = "none";
         }

         function joinRoom(){
             let joinQueueModal = document.getElementById("modal-check-queue");
             joinQueueModal.style.display = "none";
             currentRoomId = joinRoomId;

             let src = "/event/student/" + joinRoomId + "/<?=$encodedUsername?>";

             var iframe  = document.getElementById("forPostyouradd");
             iframe.setAttribute('src', src);
             iframe.style.display = 'block';

             document.getElementById("modal-iframe").style.display = "block";
         }

         var x = setInterval(function() {
             let joinQueueModal = document.getElementById("modal-check-queue");
             joinQueueModal.style.display = "none";
             if(joinQueueCheck){
                 $.ajax({
                     url: '/event/delete_queue/<?=$encodedUsername?>',
                     type: 'GET',
                     complete: function complete() {
                     },
                     success: function success(data) {
                         joinQueueCheck = 0;
                     },
                     error: function error() {
                         alert("Some issue has been occured. Please refresh the page");
                     }
                 });
             }
             else{
                 $.ajax({
                     url: '/event/check_queue/<?=$encodedUsername?>',
                     type: 'GET',
                     complete: function complete() {
                     },
                     success: function success(data) {
                         data = JSON.parse(data);
                         if(data.success){
                             let msg = data.msg;
                             joinQueueCheck = data.join;
                             if(joinQueueCheck)
                             {
                                 joinRoomId = data.room_id;
                                 document.getElementById("join-room").style.display = "block";
                                 document.getElementById("room-page").style.display = "none";

                                 let changeImgId = 'img-' + joinRoomId + '-1';
                                 document.getElementById(changeImgId).src = '/images/Fair2021/Chair1.png';
                             }
                             else
                             {
                                 document.getElementById("join-room").style.display = "none";
                                 document.getElementById("room-page").style.display = "block";
                             }
                             boothId = data.booth_id;
                             document.getElementById("modal-check-queue-message").innerHTML = msg;
                             let joinQueueModal = document.getElementById("modal-check-queue");
                             joinQueueModal.style.display = "block";
                         }
                     },
                     error: function error() {
                         alert("Some issue has been occured. Please refresh the page");
                     }
                 });
             }
         }, 15000);

         $.ajax({
             url: '/event/user_online/<?=$encodedUsername?>',
             type: 'GET',
             complete: function complete() {
             },
             success: function success(data) {
             },
             error: function error() {
             }
         });

         var userOnline = setInterval(function() {
             $.ajax({
                 url: '/event/user_online/<?=$encodedUsername?>',
                 type: 'GET',
                 complete: function complete() {
                 },
                 success: function success(data) {
                 },
                 error: function error() {
                 }
             });
         }, 60000);
      </script>
   </body>
</html>
