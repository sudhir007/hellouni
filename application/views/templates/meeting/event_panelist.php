<html>
    <head>
        <title> Hellouni-Imperial : Faculty Connect | NYU Tandon, Virginia Tech, Iowa State & Michigan State </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css">
        <link href="<?=base_url()?>css/organizer_visaseminar.css?v=1.0.1" rel="stylesheet">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://static.opentok.com/v2/js/opentok.min.js"></script>

        <style>
            body{
                background: url('<?=$backgroundImage['url']?>');
                background-size: 100vw 100vh;
            }
            .box{
                display: none;
                height: 60%;
            }
            #subscriber{
                margin-top: 5px;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid" id="videos">
            <div class="col-md-12" id="subscriber"></div>
            <div class="col-md-12" id="publisher" ></div>
            <div class="col-md-12" id="screen-preview"></div>

            <div class="container">
                <div class="col-md-12 carousel" id="carousel-subscriber">
                    <div class="carousel__body">
                        <div class="carousel__slider" id="subscriber-student"></div>
                        <div class="carousel__prev"><!--i class="far fa-angle-left"></i--></div>
                        <div class="carousel__next"><!--i class="far fa-angle-right"></i--></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="mySidenav" class="sidenav">
            <a href="#" id="MenuTab" class="openbtn" onclick="openNav()" ><i class="fas fa-bars"></i></a>
        </div>

        <div id="mySidepanel1" class="sidepanel1">
            <a href="javascript:void(0);" class="closebtn" onclick="closeNav()"><i class="fa fa-times" aria-hidden="true"></i></a>
            <!--<a href="javascript:void(0);" title="Start Recoring" id="start-recording"> <button type="button" id="start" onClick="javascript:startArchive()" class="btn btn-sm btn-primary width100"><i class="fas fa-plus-circle">&nbsp;</i><b>Start Recording</b></button></a>
            <a href="javascript:void(0);" id="stop-recording"><button type="button" id="stop" onClick="javascript:stopArchive()" class="btn btn-sm btn-danger width100"><i class="fas fa-minus-circle">&nbsp;</i><b>Stop Recording</b></button></a>-->
            <a href="javascript:void(0);" id="share-screen-button"><button type="button" id="screen" onClick="javascript:shareScreen()" class="btn btn-sm btn-primary width100 Orangebtn"><i class="fas fa-chalkboard-teacher">&nbsp;</i><b>Share Screen</b></button></a>

            <a href="javascript:void(0);"><button type="button" class="slide-toggle btn btn-sm btn-info width100 Orangebtn" ><i class="fas fa-comments">&nbsp;</i><b>Chat</b></button></a>
            <?php
            if($student_detail)
            {
                ?>
                <!-- <a href="javascript:void(0);"><button type="button" class="btn btn-sm btn-info width100 Orangebtn" onclick="openListPopup()"><i class="fas fa-users">&nbsp;</i><b>Students</b> <span id="attendees-count"></span></button></a> -->
                <?php
            }
            else
            {
                ?>
                <!-- <a href="javascript:void(0);" id="logout-student"></a> -->
                <?php
            }
            ?>
            <!--a href="javascript:void(0);"><button type="button" class="btn btn-sm btn-info width100 Orangebtn" onclick="goToMyPage()"><i class="fas fa-users">&nbsp;</i><b>My Page</b> <span id="attendees-count"></span></button></a-->
            <a href="javascript:void(0);"><button type="button" class="btn btn-sm btn-info width100 Orangebtn" onclick="callHelp()"><i class="fas fa-users">&nbsp;</i><b>Tech Assistance</b> </button></a>
            <a href="javascript:void(0);" ><button type="button" class="btn btn-sm btn-danger width100 Orangebtn" onclick="leaveMeeting()"><b>Leave Meeting</b> </button></a>
            <!--<a href="/event/download_brochure/<?=$tokboxId?>" target="_blank"><button type="button" class="btn btn-sm btn-info width100"><b>Download Brochure</b> </button></a>
            <a href="javascript:void(0);"><button type="button" class="btn btn-sm btn-info width100 Orangebtn"><i class="fas fa-users">&nbsp;</i><b>Waiting In Queue</b> <span id="waiting-count"></span></button></a>-->
        </div>

        <div class="box" id="chat-box">
            <span>
                <button id="chatBoxOut" onclick="expandChat()" type="button" class="btn btn-sm btn-primary marginbox" ><i class="fas fa-chevron-circle-up"></i></button>
                <button id="chatBoxIn" onclick="collapsChat()" type="button" class="btn btn-sm btn-primary marginbox"><i class="fas fa-chevron-circle-down"></i></button>
                <button type="button" class="slide-toggle btn btn-sm btn-danger marginbox" ><i class="fas fa-times-circle"></i></button>
            </span>
            <div class="box-inner">
                <div id="textchat">
                    <div id="history">
                        <?php
                        if($common_chat_history)
                        {
                            foreach($common_chat_history as $history)
                            {
                                ?>
                                <p id="<?=$history['chat_id']?>" class="<?=$history['chat_of']?>"><?=$history['name'] . ': ' . $history['message']?></p>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <form>
                        <input type="text" placeholder="Input your text here" id="msgTxt"></input>
                        <input type="hidden" id="username" value="<?=$username?>"></input>
                    </form>
                </div>
            </div>
        </div>
        <div id="attendeesList" class="attendeesListModal" >
            <!-- Modal content -->
            <div class="row attendeesList-content">
                <span class="listclose" id="closeModal">&times;</span>
                <div class="col-md-12 content">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="table-container">
                                <table class="table table-filter">
                                    <thead>
                                        <tr>
                                            <th>Student Name</th>
                                            <th>Student Email</th>
                                            <th>Allow</th>
                                            <!-- <th>Intake</th>
                                            <th>Logout</th> -->
                                        </tr>
                                    </thead>
                                    <tbody id="attendees-list"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="footer-head" class="col-md-12 col-sm-12">
            <div id="footer-head-sub" class="col-md-12">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 left-align">
                    <div class="copyright">
                        <span class="footer-text">© 2018, </span>
                        <img src="https://admin.hellouni.org/img/logo.png" width="90">&nbsp;
                        <span class="footer-text"> All rights reserved</span>
                        <!-- COPYRIGHT 2018 &copy; HELLOUNI 2018 -->
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 right-align">
                    <div class="design">
                        <a href="https://www.hellouni.org" class="footer-text"><b>Hellouni</b> </a> |  <a target="_blank" href="https://www.hellouni.org" class="footer-text">MAKE RIGHT CHOICE !</a>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var apiKey = "<?=$apiKey?>";
            var sessionId = "<?=$sessionId?>";
            var token = "<?=$token?>";
            var tokboxId = "<?=$tokboxId?>";
            var base64TokenId = "<?=$base64_token_id?>";
            var base64Username = "<?=$base64_username?>";

            var session;
            var allConnections = {};
            var allStreams = {};
            var allStreamsStudents = {};
            var publisher;

            var organizerCount = 0;
            var publisherCount = 0;
            var subscriberCount = 0;
            var connectionCount = 0;

            var totalSubscribers = 0;
            var totalSubscribersStudents = 0;
            var screenShared = 0;

            var msgHistory = document.querySelector('#history');

            var attendeesListModal = document.getElementById("attendeesList");
            var coursespan = document.getElementById("closeModal");

            // Text chat
            var form = document.querySelector('form');
            var msgTxt = document.querySelector('#msgTxt');
            var username = document.querySelector('#username');

            function openNav() {
                document.getElementById("mySidepanel1").style.setProperty("width", "20%", "important");
            }

            function closeNav() {
                document.getElementById("mySidepanel1").style.setProperty("width", "0", "important");
                document.getElementById("videos").style.setProperty("width", "100%", "important");
            }

            function expandChat() {
                document.getElementById("chat-box").style.setProperty("height", "95%", "important");
                document.getElementById("chat-box").style.display = "block";
                document.getElementById("chatBoxOut").style.display = "none";
                document.getElementById("chatBoxIn").style.display = "block";
            }

            function collapsChat() {
                document.getElementById("chat-box").style.setProperty("height", "54%", "important");
                document.getElementById("chat-box").style.display = "block";
                document.getElementById("chatBoxIn").style.display = "none";
                document.getElementById("chatBoxOut").style.display = "block";
            }

            function openListPopup() {
                attendeesListModal.style.display = "block";
                document.getElementById("attendeesList").style.setProperty("z-index", "120");
            }

            coursespan.onclick = function() {
                attendeesListModal.style.display = "none";
            }

            window.onclick = function(event) {
                if (event.target == attendeesListModal) {
                    attendeesListModal.style.display = "none";
                }
            }

            $(document).ready(function () {
                $(".slide-toggle").click(function(){
                    $(".box").animate({
                        width: "toggle"
                    });
                });

                $('.btn-filter').on('click', function () {
                    var $target = $(this).data('target');
                    if ($target != 'all') {
                        $('.table tr').css('display', 'none');
                        $('.table tr[data-status="' + $target + '"]').fadeIn('slow');
                    } else {
                        $('.table tr').css('display', 'none').fadeIn('slow');
                    }
                });
            });

            // Handling all of our errors here by alerting them
            function handleError(error) {
                if (error) {
                    //alert(error.message);
                    let device = navigator.userAgent;
                    $.ajax({
                        url: '/event/log_error',
                        type: 'POST',
                        contentType: 'application/json', // send as JSON
                        data: JSON.stringify({'username': '<?=$username?>', device: device, 'error': error}),
                        complete: function complete() {
                            // called when complete
                            //console.log('chat function completed');
                        },
                        success: function success() {
                            // called when successful
                            //console.log('successfully called chat');
                        },
                        error: function error() {
                            // called when there is an error
                            //console.log('error calling chat');
                        }
                    });
                }
            }

            function initializeSession() {
                session = OT.initSession(apiKey, sessionId);

                // Create a publisher
                publisher = OT.initPublisher('subscriber', {
                    insertMode: 'append',
                    width: '100%',
                    height: '100%',
                    name: '<?=$panelistName?>',
                    publishAudio:true,
                    publishVideo:true,
                    style: {buttonDisplayMode: 'on', nameDisplayMode: "on"}
                }, handleError);

                // Connect to the session
                session.connect(token, function(error, event) {
                    // If the connection is successful, publish to the session
                    if (error) {
                        handleError(error);
                    } else {
                        var node = document.createElement("button");
                        node.setAttribute("id", "video-button-" + publisher.id);
                        node.setAttribute("class", "OT_edge-bar-item OT_mode-on OT_video AVButton fas fa-video");
                        node.setAttribute("onClick", "disableVideo()")
                        document.getElementById(publisher.id).appendChild(node);
                        // document.getElementById("video-button-" + publisher.id).innerHTML = "Disable Video";
                         document.getElementById("video-button-" + publisher.id).style.setProperty("background-color", "#ffffff00", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("font-size", "25px", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("right", "30px", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("top", "4px", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("color", "white", "important");

                        session.publish(publisher, handleError);
                    }
                });

                session.on('signal:msg', function signalCallback(event) {
                    if(event.from.connectionId === session.connection.connectionId){
                        document.getElementById("chat-box").style.display="block";
                        var msg = document.createElement('p');
                        var splitData = event.data.split('|');
                        //msg.textContent = event.data;
                        msg.textContent = splitData[0];
                        msg.setAttribute("id", splitData[1]);
                        msg.className = 'mine';
                        msgHistory.appendChild(msg);
                        msg.scrollIntoView();
                    }
                    else if(event.from.permissions.publish || session.connection.permissions.publish){
                        document.getElementById("chat-box").style.display="block";
                        var splitData = event.data.split('|');

                        // Old Code
                        var msg = document.createElement('p');
                        //msg.textContent = event.data;
                        msg.textContent = splitData[0];
                        msg.setAttribute("id", splitData[1]);
                        msg.className = 'theirs';
                        msgHistory.appendChild(msg);
                        msg.scrollIntoView();
                    }
                });

                session.on('signal:del-msg', function signalCallback(event) {
                    $("#" + event.data).remove();
                });

                session.on('archiveStarted', function archiveStarted(event) {
                    archiveID = event.id;
                    //console.log('Archive started ' + archiveID);
                    $('#start-recording').hide();
                    $('#stop-recording').hide();
                });

                session.on('archiveStopped', function archiveStopped(event) {
                    archiveID = event.id;
                    $('#start-recording').show();
                });

                session.on({
                    connectionCreated: function (event) {
                    },
                    connectionDestroyed: function connectionDestroyedHandler(event) {
                        //console.log("Connection destroyed");
                    },
                    streamDestroyed: function (event){
                        if(event.stream.videoType == 'camera'){
                            if(event.stream.connection.permissions.forceDisconnect == 1){
                                totalSubscribers--;

                                var gridWidth = '100%',
                                    gridHeight = '100%',
                                    gridItems = totalSubscribers,
                                    grid_str = '',
                                    $grid = $('#subscriber'),
                                    i;

                                function dimensions(x) {
                                    let quotient = parseInt(x / 4);
                                    let remainder = x % 4;

                                    var w = 25;
                                    var l = 30;
                                    var divW = 96;
                                    var t = 30;
                                    var h = 25;

                                    var screenDivisor = totalSubscribers + 1;
                                    switch(screenDivisor){
                                        case 1:
                                            console.log("I am in 1");
                                            break;
                                        case 2:
                                            w = 50;
                                            l = 20;
                                            divW = 45;
                                            break;
                                        case 3:
                                            w = 70;
                                            l = 10;
                                            divW = 30;
                                            t = 30;
                                            break;
                                        case 4:
                                            w = 90;
                                            l = 5;
                                            divW = 20;
                                            t = 30;
                                            break;
                                        case 5:
                                        case 6:
                                            w = 90;
                                            l = 0;
                                            divW = 20;
                                            t = 30;
                                            h = 20;
                                            break;
                                        case 7:
                                        case 8:
                                            w = 98;
                                            l = 5;
                                            divW = 23;
                                            t = 20;
                                            break;
                                        case 9:
                                        case 10:
                                        case 11:
                                        case 12:
                                            w = 98;
                                            l = 5;
                                            divW = 23;
                                            t = 3;
                                            //console.log("I am in 9");
                                            break;
                                    }

                                    //let attrb = "width: " + w + "%; height: 30%; position: relative; left: " + l + "%; top: " + t + "%;";
                                    let attrb = "width: " + w + "%; height: " + h + "%; position: relative; left: " + l + "%;";
                                    document.getElementById("subscriber").setAttribute("style", attrb);

                                    $('#subscriber .OT_root').css({
                                        width: divW + '%',
                                        height: '100%',
                                        border: 0,
                                        margin: '5px; 10px 0 0'
                                    });
                                };

                                //$grid.width(gridWidth).height(gridHeight);

                                let subscriberId = allStreams[event.stream.id];

                                //console.log(allStreams);
                                document.getElementById(subscriberId).style.display = 'none';

                                if(!screenShared)
                                {
                                    dimensions(totalSubscribers);
                                }

                                if(screenShared)
                                {
                                    var w = 12;
                                    var divW = 100;

                                    //document.getElementById("screen-preview").style.display = "block";
                                    let attrb = "width: " + w + "%; height: 100%; position: absolute; left: 0; top: 2%; overflow-y: auto;";
                                    document.getElementById("subscriber").setAttribute("style", attrb);

                                    $('#subscriber .OT_root').css({
                                        width: divW + '%',
                                        height: '20%',
                                        marginRight: '10px'
                                    });
                                }
                            }
                            else{
                                var subscriberStudentId = allStreamsStudents[event.stream.id];
                                totalSubscribersStudents--;
                                $("#item-" + subscriberStudentId).remove();

                                $('#subscriber-student .OT_root').css({
                                    width: '240px',
                                    height: '160px',
                                    marginRight: '10px',
                                    marginTop: '10px'
                                });

                                initCarousal();
                            }
                        }
                        if(event.stream.videoType == 'screen'){
                            screenShared = 0;
                            document.getElementById("screen-preview").style.display = "none";
                            $('#share-screen-button').show();

                            var w = 25;
                            var l = 30;
                            var divW = 96;
                            var t = 30;
                            var h = 20;

                            var screenDivisor = totalSubscribers + 1;
                            switch(screenDivisor){
                                case 1:
                                    //console.log("I am in 1");
                                    break;
                                case 2:
                                    w = 50;
                                    l = 30;
                                    divW = 45;
                                    break;
                                case 3:
                                    w = 70;
                                    l = 10;
                                    divW = 30;
                                    t = 30;
                                    break;
                                case 4:
                                    w = 90;
                                    l = 5;
                                    divW = 20;
                                    t = 30;
                                    break;
                                case 5:
                                case 6:
                                    w = 90;
                                    l = 0;
                                    divW = 20;
                                    t = 30;
                                    h = 20;
                                    break;
                                case 7:
                                case 8:
                                    w = 98;
                                    l = 5;
                                    divW = 23;
                                    t = 20;
                                    break;
                                case 9:
                                case 10:
                                case 11:
                                case 12:
                                    w = 98;
                                    l = 5;
                                    divW = 23;
                                    t = 3;
                                    //console.log("I am in 9");
                                    break;
                            }

                            //let attrb = "width: " + w + "%; height: 30%; position: relative; left: " + l + "%; top: " + t + "%;";
                            let attrb = "width: " + w + "%; height: " + h + "%; position: relative; left: " + l + "%;";
                            document.getElementById("subscriber").setAttribute("style", attrb);

                            $('#subscriber .OT_root').css({
                                width: divW + '%',
                                height: '100%',
                                border: 0,
                                margin: '5px 10px 0 0'
                            });
                        }
                    },
                    streamCreated: function(event){
                        if(event.stream.videoType == 'camera'){
                            if(event.stream.connection.permissions.forceDisconnect == 1){
                                var subscriber = session.subscribe(event.stream, 'subscriber', {
                                    insertMode: 'append',
                                    width: '100%',
                                    height: '100%',
                                    name: event.stream.name,
                                }, handleError);

                                //console.log(allStreams);
                                totalSubscribers++;
                                allStreams[event.stream.id] = subscriber.id;

                                var gridWidth = '100%',
                                    gridHeight = '100%',
                                    gridItems = totalSubscribers,
                                    grid_str = '',
                                    $grid = $('#subscriber'),
                                    i;

                                function dimensions(x) {
                                    let quotient = parseInt(x / 4);
                                    let remainder = x % 4;

                                    var w = 25;
                                    var l = 30;
                                    var divW = 96;
                                    var t = 30;
                                    var h = 25;

                                    var screenDivisor = totalSubscribers + 1;

                                    console.log(totalSubscribers, screenDivisor);
                                    switch(screenDivisor){
                                        case 1:
                                            console.log("I am in 1");
                                            break;
                                        case 2:
                                            w = 50;
                                            l = 30;
                                            divW = 45;
                                            break;
                                        case 3:
                                            w = 70;
                                            l = 10;
                                            divW = 30;
                                            t = 30;
                                            break;
                                        case 4:
                                            w = 90;
                                            l = 5;
                                            divW = 20;
                                            t = 30;
                                            break;
                                        case 5:
                                        case 6:
                                            w = 90;
                                            l = 0;
                                            divW = 20;
                                            t = 30;
                                            h = 20;
                                            break;
                                        case 7:
                                        case 8:
                                            w = 98;
                                            l = 5;
                                            divW = 23;
                                            t = 20;
                                            break;
                                        case 9:
                                        case 10:
                                        case 11:
                                        case 12:
                                            w = 98;
                                            l = 5;
                                            divW = 23;
                                            t = 3;
                                            //console.log("I am in 9");
                                            break;
                                    }

                                    //let attrb = "width: " + w + "%; height: 30%; position: relative; left: " + l + "%; top: " + t + "%;";
                                    let attrb = "width: " + w + "%; height: " + h + "%; position: relative; left: " + l + "%;";
                                    document.getElementById("subscriber").setAttribute("style", attrb);

                                    $('#subscriber .OT_root').css({
                                        width: divW + '%',
                                        height: '100%',
                                        border: 0,
                                        margin: '5px 10px 0 0'
                                    });
                                };

                                if(!screenShared)
                                {
                                    document.getElementById(subscriber.id).style.display = 'inline-block';
                                    document.getElementById(subscriber.id).style.boxSizing = 'border-box';
                                    document.getElementById(subscriber.id).style.border = '2px solid white';
                                    document.getElementById(subscriber.id).style.margin = '2px';

                                    dimensions(totalSubscribers);
                                }

                                if(screenShared)
                                {
                                    var w = 12;
                                    var divW = 100;

                                    //document.getElementById("screen-preview").style.display = "block";
                                    let attrb = "width: " + w + "%; height: 100%; position: absolute; left: 0; top: 2%; overflow-y:auto;";
                                    document.getElementById("subscriber").setAttribute("style", attrb);

                                    $('#subscriber .OT_root').css({
                                        width: divW + '%',
                                        height: '20%',
                                        marginRight: '10px'
                                    });
                                }
                            }
                            else{
                                var subscriber = session.subscribe(event.stream, 'subscriber-student', {
                                    insertMode: 'append',
                                    width: '240px',
                                    height: '160px',
                                    name: event.stream.name,
                                    subscribeToAudio:true,
                                    subscribeToVideo:true,
                                    audioVolume: 100
                                }, handleError);

                                totalSubscribersStudents++;
                                allStreamsStudents[event.stream.id] = subscriber.id;

                                var itemDiv = document.createElement("div");
                                itemDiv.setAttribute("id", "item-" + subscriber.id);
                                itemDiv.setAttribute("class", "carousel__slider__item");
                                document.getElementById("subscriber-student").appendChild(itemDiv);

                                var item3DDiv = document.createElement("div");
                                item3DDiv.setAttribute("id", "item-3d-" + subscriber.id);
                                item3DDiv.setAttribute("class", "item__3d-frame");
                                document.getElementById("item-" + subscriber.id).appendChild(item3DDiv);

                                var item3DBoxDiv = document.createElement("div");
                                item3DBoxDiv.setAttribute("id", "item-3d-box-" + subscriber.id);
                                item3DBoxDiv.setAttribute("class", "item__3d-frame__box item__3d-frame__box--front");
                                document.getElementById("item-3d-" + subscriber.id).appendChild(item3DBoxDiv);

                                var elem = document.getElementById(subscriber.id);
                                $("#" + subscriber.id).remove();
                                document.getElementById("item-3d-box-" + subscriber.id).appendChild(elem);

                                var itemLeftDiv = document.createElement("div");
                                itemLeftDiv.setAttribute("class", "item__3d-frame__box item__3d-frame__box--left");
                                document.getElementById("item-3d-"  + subscriber.id).appendChild(itemLeftDiv);

                                var itemRightDiv = document.createElement("div");
                                itemRightDiv.setAttribute("class", "item__3d-frame__box item__3d-frame__box--right");
                                document.getElementById("item-3d-"  + subscriber.id).appendChild(itemRightDiv);

                                $('#subscriber-student .OT_root').css({
                                    width: '240px',
                                    height: '160px',
                                    marginRight: '10px',
                                    marginTop: '10px'
                                });

                                initCarousal();
                            }

                            var SpeakerDetection = function(subscriber, startTalking, stopTalking) {
                                var activity = null;
                                subscriber.on('audioLevelUpdated', function(event) {
                                    var now = Date.now();
                                    if (event.audioLevel > 0.2) {
                                        if (!activity) {
                                            activity = {timestamp: now, talking: false};
                                        }
                                        else if (activity.talking) {
                                            activity.timestamp = now;
                                        }
                                        else if (now- activity.timestamp > 500) {
                                            activity.talking = true;
                                            if (typeof(startTalking) === 'function') {
                                                startTalking();
                                            }
                                        }
                                    }
                                    else if (activity && now - activity.timestamp > 3000) {
                                        // detected low audio activity for more than 3s
                                        if (activity.talking) {
                                            if (typeof(stopTalking) === 'function') {
                                                stopTalking();
                                            }
                                        }
                                        activity = null;
                                    }
                                });
                            };

                            SpeakerDetection(subscriber, function() {
                                document.getElementById(subscriber.id).style.setProperty("overflow", "inherit", "important");

                                var x = document.getElementById(subscriber.id).querySelectorAll(".OT_name");
                                x[0].style.display = "block";
                                x[0].style.opacity = "1";
                                x[0].style.top = "0";


                                var y = document.getElementById(subscriber.id).querySelectorAll(".OT_widget-container");
                                y[0].style.display = "block";
                                y[0].style.setProperty("border", "3px solid #00ca00", "important");



                            }, function() {
                                document.getElementById(subscriber.id).style.background = "";
                                var x = document.getElementById(subscriber.id).querySelectorAll(".OT_name");
                                x[0].style.display = "none";
                                x[0].style.opacity = "0";
                                x[0].style.top = "-25px";

                                var y = document.getElementById(subscriber.id).querySelectorAll(".OT_widget-container");
                                y[0].style.display = "block";
                                y[0].style.setProperty("border", "none", "important");

                                document.getElementById(subscriber.id).style.setProperty("overflow", "hidden", "important");
                            });
                        }
                        if(event.stream.videoType == 'screen'){
                            screenShared = 1;
                            $('#share-screen-button').hide();
                            var w = 12;
                            var divW = 100;

                            document.getElementById("screen-preview").style.display = "block";
                            let attrb = "width: " + w + "%; height: 100%; position: absolute; left: 0; top: 2%; overflow-y:auto;";
                            document.getElementById("subscriber").setAttribute("style", attrb);

                            $('#subscriber .OT_root').css({
                                width: divW + '%',
                                height: '20%',
                                marginRight: '10px'
                            });
                            session.subscribe(event.stream, 'screen-preview', {
                                insertMode: 'append',
                                width: '100%',
                                height: '100%',
                                name: '<?=$username?>'
                            }, handleError);
                        }
                        //console.log("Stream Created ", event);
                    }
                });

                function resizeSubscriber() {
                    publisher.element.style.width = "100px";
                    publisher.element.style.height = "75px";
                    document.getElementById("subscriber").appendChild(publisher.element);
                }
            }

            function disableVideo(){
                publisher.publishVideo(false);
                document.getElementById("video-button-" + publisher.id).setAttribute("onClick", "enableVideo('" + publisher + "')");
                // document.getElementById("video-button-" + publisher.id).innerHTML = "Enable Video";
                document.getElementById("video-button-" + publisher.id).classList.remove('fa-video');
                document.getElementById("video-button-" + publisher.id).classList.add('fa-video-slash');

            }

            function enableVideo(){
                publisher.publishVideo(true);
                document.getElementById("video-button-" + publisher.id).setAttribute("onClick", "disableVideo('" + publisher + "')");
                // document.getElementById("video-button-" + publisher.id).innerHTML = "Disable Video";
                document.getElementById("video-button-" + publisher.id).classList.remove('fa-video-slash');
                document.getElementById("video-button-" + publisher.id).classList.add('fa-video');

            }

            function disconnectUser(connectionId){
                if (session.capabilities.forceDisconnect == 1) {
                    //alert(connectionId);
                    session.forceDisconnect(allConnections[connectionId]);
                } else {
                    alert("No permission");
                }
            }

            function shareScreen(){
                $('#share-screen-button').hide();
                OT.checkScreenSharingCapability(function(response) {
                    if(!response.supported || response.extensionRegistered === false) {
                        $('#share-screen-button').show();
                    } else if (response.extensionInstalled === false) {
                        $('#share-screen-button').show();
                    } else {
                        // Create a publisher for screen
                        document.getElementById("screen-preview").style.display = "none";
                        var publisherScreen = OT.initPublisher('screen-preview', {
                            maxResolution:{width: 1920, height: 1080},
                            videoSource: 'screen'
                        }, handleError);

                        session.publish(publisherScreen, function(error) {
                            if (error) {
                                $('#share-screen-button').show();
                                var node = document.createElement("div");
                                node.setAttribute("id", "screen-preview");
                                node.setAttribute("style", "display:none; margin-top: 3%;");
                                document.getElementById("videos").appendChild(node);
                                // Look error.message to see what went wrong.
                            }
                            else{
                                $('#share-screen-button').hide();
                            }
                        });

                        publisherScreen.on('streamDestroyed', function(event) {
                            //document.getElementById("publisher").style.display = "block";
                            $('#share-screen-button').show();
                            var node = document.createElement("div");
                            node.setAttribute("id", "screen-preview");
                            node.setAttribute("style", "display:none; margin-top: 3%;");
                            document.getElementById("videos").appendChild(node);
                            if (event.reason === 'mediaStopped') {
                                // User clicked stop sharing
                            } else if (event.reason === 'forceUnpublished') {
                                // A moderator forced the user to stop sharing.
                            }
                        });
                    }
                });
            }

            // Send a signal once the user enters data in the form
            form.addEventListener('submit', function submit(event) {
                var senderRole = session.connection.permissions.publish ? 'PANELIST' : 'SUBSCRIBER';
                event.preventDefault();
                var currentTime = new Date().getTime();
                var chatId = 'chat-' + currentTime;
                $.ajax({
                    url: '/event/chat',
                    type: 'POST',
                    contentType: 'application/json', // send as JSON
                    data: JSON.stringify({'user_id': '<?=$userId?>', 'sessionId': sessionId, 'tokboxId': tokboxId, 'to': 'All', 'msg': msgTxt.value, 'from_role': senderRole, 'chat_id': chatId}),
                    complete: function complete() {
                        // called when complete
                        //console.log('chat function completed');
                    },
                    success: function success() {
                        // called when successful
                        //console.log('successfully called chat');
                    },
                    error: function error() {
                        // called when there is an error
                        //console.log('error calling chat');
                    }
                });
                session.signal({
                    type: 'msg',
                    data: username.value + ': ' + msgTxt.value + '|' + chatId
                }, function signalCallback(error) {
                    if (error) {
                        //console.error('Error sending signal:', error.name, error.message);
                    } else {
                        msgTxt.value = '';
                    }
                });
            });

            // Start recording
            function startArchive() { // eslint-disable-line no-unused-vars
                $('#start-recording').hide();
                $.ajax({
                    url: '/meeting/oneonone/startArchive',
                    type: 'POST',
                    contentType: 'application/json', // send as JSON
                    data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId}),
                    complete: function complete() {
                        // called when complete
                        //console.log('startArchive() complete');
                        $('#stop-recording').show();
                    },
                    success: function success() {
                        // called when successful
                        //console.log('successfully called startArchive()');
                    },
                    error: function error() {
                        // called when there is an error
                        //console.log('error calling startArchive()');
                    }
                });
            }

            // Stop recording
            function stopArchive() { // eslint-disable-line no-unused-vars
                $.post('/tokbox/stopArchive/' + archiveID);
                $('#stop-recording').hide();
            }

            initializeSession();

            function leaveMeeting(){
                window.location.href = '/event/thankyou/<?=$base64_username?>';
            }

            var carousel = document.getElementsByClassName('carousel')[0],
                slider = carousel.getElementsByClassName('carousel__slider')[0],
                items = carousel.getElementsByClassName('carousel__slider__item'),
                prevBtn = carousel.getElementsByClassName('carousel__prev')[0],
                nextBtn = carousel.getElementsByClassName('carousel__next')[0];

            var width, height, totalWidth, margin = 20,
                currIndex = 0,
                interval, intervalTime = 4000;

            function initCarousal() {
                carousel = document.getElementsByClassName('carousel')[0],
                slider = carousel.getElementsByClassName('carousel__slider')[0],
                items = carousel.getElementsByClassName('carousel__slider__item'),
                prevBtn = carousel.getElementsByClassName('carousel__prev')[0],
                nextBtn = carousel.getElementsByClassName('carousel__next')[0];

                resize();
                //move(Math.floor(items.length / 2));
                //bindEvents();

                //timer();
            }

            function resize() {
                width= '200',
                height= '160',
                totalWidth = width * items.length;
                if(items.length > 5)
                {
                    totalWidth = width * 5;
                }

                slider.style.width = totalWidth + "px";

                for(var i = 0; i < items.length; i++) {
                    let item = items[i];
                    //item.style.width = (width - (margin * 2)) + "px";
                    item.style.width = width + "px";
                    item.style.height = height + "px";
                }
            }

            function move(index) {

                if(index < 1) index = items.length;
                if(index > items.length) index = 1;
                currIndex = index;

                for(var i = 0; i < items.length; i++) {
                    let item = items[i],
                        box = item.getElementsByClassName('item__3d-frame')[0];
                    if(i == (index - 1)) {
                        item.classList.add('carousel__slider__item--active');
                        box.style.transform = "perspective(1200px)";
                    } else {
                      item.classList.remove('carousel__slider__item--active');
                        box.style.transform = "perspective(1200px) rotateY(" + (i < (index - 1) ? 40 : -40) + "deg)";
                    }
                }

                slider.style.transform = "translate3d(" + ((index * -width) + (width / 2) + window.innerWidth / 4) + "px, 0, 0)";
            }

            function timer() {
                clearInterval(interval);
                interval = setInterval(() => {
                  move(++currIndex);
                }, intervalTime);
            }

            function prev() {
              move(--currIndex);
              timer();
            }

            function next() {
              move(++currIndex);
              timer();
            }


            function bindEvents() {
                window.onresize = resize;
                prevBtn.addEventListener('click', () => { prev(); });
                nextBtn.addEventListener('click', () => { next(); });
            }

            var chk_connections = setInterval(function() {
                $.ajax({
                    url: '/event/get_connections/' + tokboxId,
                    type: 'GET',
                    complete: function complete() {
                    },
                    success: function success(data) {
                        data = JSON.parse(data);
                        <?php
                        if($student_detail)
                        {
                            ?>
                            document.getElementById("attendees-count").innerHTML = '(' + data.length + ')';
                            document.getElementById("attendees-list").innerHTML = '';
                            if(data.length){
                                var attendeeList = '';
                                data.forEach(function(value){
                                    var tr_node = document.createElement("tr");
                                    tr_node.setAttribute("id", "attendees-list-" + value.user_id);

                                    if(value.allow == undefined || (value.token != undefined && !value.token)){
                                        var tdHtml = '<td>' + value.name + '</td><td>' + value.email + '</td><td></td>';
                                        document.getElementById("attendees-list").appendChild(tr_node);
                                        document.getElementById("attendees-list-" + value.user_id).innerHTML = tdHtml;
                                    }
                                    else if(value.allow){
                                        var tdHtml = '<td>' + value.name + '</td><td>' + value.email + '</td><td><button class="btn btn-sm btn-info width100 Orangebtn" onClick="disallowStudent(' + value.user_id + ')">Disallow</button></td>';
                                        document.getElementById("attendees-list").appendChild(tr_node);
                                        document.getElementById("attendees-list-" + value.user_id).innerHTML = tdHtml;
                                    }
                                    else{
                                        var tdHtml = '<td>' + value.name + '</td><td>' + value.email + '</td><td><button class="btn btn-sm btn-info width100 Orangebtn" onClick="allowStudent(' + value.user_id + ')">Allow</button></td>';
                                        document.getElementById("attendees-list").appendChild(tr_node);
                                        document.getElementById("attendees-list-" + value.user_id).innerHTML = tdHtml;
                                    }
                                })
                            }
                            <?php
                        }
                        else
                        {
                            ?>
                            if(data.length){
                                data.forEach(function(value){
                                    var tdHtml = '<button type="button" class="btn btn-sm btn-info width100 Orangebtn" onClick="removeConnection(\'' + value.username + '\', ' + value.user_id + ')"><i class="fas fa-users">&nbsp;</i><b>Logout Student</b>';

                                    document.getElementById("logout-student").innerHTML = tdHtml;
                                })
                            }
                            <?php
                        }
                        ?>
                    },
                    error: function error() {
                    }
                });
            }, 10000);

            /*var chk_queue_count = setInterval(function() {
                $.ajax({
                    url: '/event/check_queue_count/' + tokboxId,
                    type: 'GET',
                    complete: function complete() {
                    },
                    success: function success(data) {
                        document.getElementById("waiting-count").innerHTML = '(' + data + ')';
                    },
                    error: function error() {
                    }
                });
            }, 30000);*/

            function removeConnection(username, userId){
                // Old code
                //document.getElementById("attendees-list-" + userId).remove();
                session.signal({
                    type: 'disconnect-me',
                    data: username
                }, function signalCallback(error) {
                });
            }

            function goToMyPage(){
                let href = "/event/booth/<?=$boothId?>/<?=$base64_username?>";
                var win = window.open(href, 'fair2021');
                win.focus();
            }

            function callHelp(){
                $.ajax({
                    url: '/event/get_help/<?=$base64_token_id?>',
                    type: 'GET',
                    complete: function complete() {
                    },
                    success: function success(data) {
                        alert("Our representative will join this room soon.");
                    },
                    error: function error() {
                    }
                });
            }

            function allowStudent(userId){
                $.ajax({
                    url: '/event/allow_student/' + userId,
                    type: 'GET',
                    complete: function complete() {
                    },
                    success: function success(data) {
                        alert("Student is allowed to share audio video");
                    },
                    error: function error() {
                    }
                });
            }

            function disallowStudent(userId){
                $.ajax({
                    url: '/event/disallow_student/' + userId,
                    type: 'GET',
                    complete: function complete() {
                    },
                    success: function success(data) {
                        alert("Student is allowed to share audio video");
                    },
                    error: function error() {
                    }
                });
            }
        </script>
    </body>
</html>
