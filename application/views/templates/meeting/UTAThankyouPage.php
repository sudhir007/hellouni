<head>
   <title>Hello Uni : UTA Information Session Day 2021</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
</head>
<style type="text/css">
   @import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");
   .login-block{
   background: #004b7a;  /* fallback for old browsers */
   background: -webkit-linear-gradient(to bottom, #fafafb, #d8d9da);  /* Chrome 10-25, Safari 5.1-6 */
   background: linear-gradient(to bottom, #fafafb, #d8d9da); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
   float:left;
   width:100%;
   padding : 50px 0;
   }
   .login-sec{padding: 50px 30px; position:relative;}
   .login-sec .copy-text{position:absolute; width:80%; bottom:20px; font-size:13px; text-align:center;}
   .login-sec h2{margin-bottom:30px; font-weight:800; font-size:40px; color: #f46127;    text-align: center;}
   .login-sec h3{    margin-bottom: 30px;
    font-weight: 600;
    font-size: 25px;
    color: #000000;
    text-align: center;}
   .login-sec h2:after{content:" "; width:1000px; height:5px; background:#f46127; display:block; margin-top:20px; border-radius:3px; margin-left:auto;margin-right:auto}
   /*.login-sec h3:after{content:" "; width:300px; height:5px; background:#f9992f; display:block; margin-top:20px; border-radius:3px; margin-left:auto;margin-right:auto}*/
   .btn-login{background: #f46127; color:#fff; font-weight:600;}
   .form-control{
   display: block;
   width: 100%;
   height: 34px;
   padding: 6px 12px;
   font-size: 14px;
   line-height: 1.42857143;
   color: #555;
   background-color: #fff;
   background-image: none;
   border: 1px solid #ccc;
   border-radius: 4px;
   -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
   box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
   -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
   -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
   transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
   }

   .divv p{
   font-size: 18px;
   }

</style>
<section class="login-block" id="forgot-password-page">
   <div class="container" style="background:#fff; border-radius: 10px; box-shadow: 5px 5px 0px rgba(0,0,0,0.1);">
      <div class="row banner-secLogo">
         <img src="<?php echo base_url();?>application/images/UTA.jpg" class="img-fluid"  alt="Responsive Image" width="100%" height="80%" />
      </div>
      <div class="row divv">
         <div class="col-md-12 login-sec">
           
               <div class="col-md-12" style="text-align: center;">
                  <h2 class="text-center">Thank you for registering for UTA Day!</h2>
                  <h3 class="text-center">The event will begin this <b>Friday, 29th October from 6:00 PM to 8:00 PM</b></h3>
                  <p>You can join the event using your registered email address: <a href="https://hellouni.org/event/login" target="_blank" class="btn btn-lg btn-login">Join Here</a></p>
                  <p>For any doubts / to get in touch with us please contact / WhatsApp us at: +91 7045912005</p>
               </div>
              
           
            
         </div>
      </div>
   </div>
</section>
