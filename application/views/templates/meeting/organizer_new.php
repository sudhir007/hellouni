<html>
    <head>
        <title> Hellouni : Webinar | Organizer </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css">
        <link href="<?=base_url()?>css/organizer_webinar.css?v=1.9" rel="stylesheet">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://static.opentok.com/v2/js/opentok.min.js"></script>

    </head>
    <body>
        <div class="container-fluid" id="videos">
            <!--div class="col-md-12">
                <div class="col-md-12" id="subscriber"></div>
                <div class="col-md-12" id="publisher" ></div>
            </div>
            <div id="screen-preview" class="container-fluid"></div-->
            <div class="col-md-12" id="subscriber"></div>
            <div class="col-md-12" id="publisher" ></div>
            <div class="col-md-12" id="screen-preview"></div>
        </div>
        <div id="mySidenav" class="sidenav">
            <a href="#" id="MenuTab" class="openbtn" onclick="openNav()" ><i class="fas fa-bars"></i></a>
        </div>

        <div id="mySidepanel1" class="sidepanel1" >
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><i class="fa fa-times" aria-hidden="true"></i></a>
            <a href="#" title="Start Recoring" id="start-recording"> <button type="button" id="start" onClick="javascript:startArchive()" class="btn btn-sm btn-primary width100 Orangebtn"><i class="fas fa-plus-circle">&nbsp;</i><b>Start Recording</b></button></a>
            <a href="#" id="stop-recording"><button type="button" id="stop" onClick="javascript:stopArchive()" class="btn btn-sm btn-danger width100 Orangebtn"><i class="fas fa-minus-circle">&nbsp;</i><b>Stop Recording</b></button></a>
            <a href="#" id="share-screen-button"><button type="button" id="screen" onClick="javascript:shareScreen()" class="btn btn-sm btn-primary width100 Orangebtn"><i class="fas fa-chalkboard-teacher">&nbsp;</i><b>Share Screen</b></button></a>

            <div class="panel-group" id="accordion1">
                <div class="panel">
                    <div class="panel-heading">
                        <!--a data-toggle="collapse" data-parent="#accordion1" data-target="#collapseOne" href="#" aria-expanded="true" aria-controls="collapseOne" class="btn btn-sm btn-success Orangebtn"><i class="fas fa-users">&nbsp;</i>Show attendees</a-->
                        <a href="javascript::void();" class="btn btn-sm btn-success Orangebtn" onclick="openListPopup()"><i class="fas fa-users">&nbsp;</i>Show attendees <span id="attendees-count"></span></a>
                    </div>

                    <div id="collapseOne" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table table-bordered listTable">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th></th>
                                        <th><button type="button" onclick="openListPopup()" class="btn btn-sm btn-warning"><i class="fas fa-external-link-alt"></i></button></th>
                                    </tr>
                                </thead>
                                <tbody id="attendees-list-menu"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <a href="#" ><button type="button" class="slide-toggle btn btn-sm btn-info width100 Orangebtn" ><i class="fas fa-comments">&nbsp;</i><b>Ask</b></button></a>
            <?php
            if($polling)
            {
                ?>
                <a href="#">
                    <button type="button" onclick="openPollPopup()" class="btn btn-sm btn-success width100 Orangebtn" id="poll-start-button"><i class="fas fa-poll">&nbsp;</i><b>Polling</b> </button>
                    <button type="button" onclick="closePollPopup()" class="btn btn-sm btn-danger width100 Orangebtn" id="poll-end-button"><i class="fas fa-poll">&nbsp;</i><b>End Polling</b> </button>
                </a>
                <a href="#">
                    <button type="button" onclick="openPollResult()" class="btn btn-sm btn-success width100 Orangebtn" id="poll-open-result-button"><i class="fas fa-poll">&nbsp;</i><b>Poll Result</b> </button>
                    <button type="button" onclick="closePollResult()" class="btn btn-sm btn-danger width100 Orangebtn" id="poll-close-result-button"><i class="fas fa-poll">&nbsp;</i><b>Close Poll Result</b> </button>
                </a>
                <?php
            }
            ?>
            <a href="#" ><button type="button" class="btn btn-sm btn-danger width100 Orangebtn" onclick="leaveMeeting()"><b>Leave Meeting</b> </button></a>
            <a href="#" ><button type="button" class="btn btn-sm btn-danger width100 Orangebtn" onclick="startSession()"><b>Start Session</b> </button></a>
        </div>

        <div class="box" id="chat-box">
            <span>
                <button id="chatBoxOut" onclick="expandChat()" type="button" class="btn btn-sm btn-primary marginbox" ><i class="fas fa-chevron-circle-up"></i></button>
                <button id="chatBoxIn" onclick="collapsChat()" type="button" class="btn btn-sm btn-primary marginbox"><i class="fas fa-chevron-circle-down"></i></button>
                <button type="button" class="slide-toggle btn btn-sm btn-danger marginbox" ><i class="fas fa-times-circle"></i></button>
            </span>
            <div class="box-inner">
                <div id="textchat">
                    <div id="history">
                        <?php
                        if($common_chat_history)
                        {
                            foreach($common_chat_history as $history)
                            {
                                if($history['chat_of'] == 'mine')
                                {
                                    ?>
                                    <p id="<?=$history['chat_id']?>" class="<?=$history['chat_of']?>"><?=$history['name'] . ': ' . $history['message']?>
                                        <span style="margin-top: -10px; float: left; font-size: 14px; cursor: pointer;" onclick="deleteChat('<?=$history['chat_id']?>')">x</span>
                                    </p>
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <p id="<?=$history['chat_id']?>" class="<?=$history['chat_of']?>"><?=$history['name'] . ': ' . $history['message']?>
                                        <span style="margin-top: -10px; margin-right: -5px; font-size: 14px; cursor: pointer;" onclick="deleteChat('<?=$history['chat_id']?>')">x</span>
                                    </p>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                    <form>
                        <input type="text" placeholder="Input your text here" id="msgTxt"></input>
                        <input type="hidden" id="username" value="<?=$username?>"></input>
                    </form>
                </div>
            </div>
        </div>
        <div id="attendeesList" class="attendeesListModal" >
            <!-- Modal content -->
            <div class="row attendeesList-content">
                <span class="listclose" id="closeModal">&times;</span>
                <div class="col-md-12 content">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="pull-left">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-filter Orangebtn filterButton" data-target="Publisher">Panelist</button>
                                    <button type="button" class="btn btn-filter Orangebtn filterButton" data-target="Organizer">Organizer</button>
                                    <button type="button" class="btn btn-filter Orangebtn filterButton" data-target="Subscriber">Attendees</button>
                                    <button type="button" class="btn btn-filter Orangebtn filterButton" data-target="all">All</button>
                                    <button type="button" class="btn btn-filter Orangebtn filterButton" onclick="openGroupChat()" id="group-chat-button">Send Message</button>
                                </div>
                            </div>
                            <div class="table-container">
                                <table class="table table-filter">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="attendees-list"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="pollId" class="pollModal" >
            <!-- Modal content -->
            <div class="row pollModal-content">
                <div id="poll-timer"></div>
                <div class="col-md-12" style="margin-bottom: 30px;">
                    <h2>List Of Polling Questions</h2>
                    <div class="col-md-12" id="questionList">
                        
                    </div>

                </div>
                <div class="col-md-12" id="quizOne">
                    
                </div>
                <div class="col-md-12" id="quiz">
                    <h1 id="quiz-name"></h1>
                    <!--button id="submit-button" class="btn btn-md btn-primary">Submit Answers</button-->
                    <button id="poll-attended-button" class="btn btn-md btn-primary" style="float:left;">Poll Attended <span id="poll-attenders"></span></button>
                    <button id="prev-question-button" class="btn btn-md btn-primary">Previous</button>
                    <button id="next-question-button" class="btn btn-md btn-primary">Next Question</button>
                    <button id="poll-result-share-button" class="btn btn-md btn-primary" style="float:right;" onclick="sharePollResult()">Share Result</button>
                    <!--div id="quiz-results">
                        <p id="quiz-results-message"></p>
                        <p id="quiz-results-score"></p>
                    </div-->
                </div>
            </div>
        </div>

        <div id="pollResultId" class="pollModal" >
            <!-- Modal content -->
            <div class="row pollModal-content">
                <div id="quiz-results"></div>
            </div>
        </div>

        <div id="chatModal" class="chatBoxModal" >
            <!-- Modal content -->
            <div class="row chatBoxModal-content" >
                <span class="chatclose" id="chatcloseModal">&times;</span>
                <div id="historyOTO">
                    <?php
                    if($private_chat_history)
                    {
                        foreach($private_chat_history as $history)
                        {
                            ?>
                            <p class="<?=$history['chat_of']?>"><?=$history['name'] . ': ' . $history['message']?>
                                <span style="margin-top: -10px; margin-right: -5px; font-size: 14px; cursor: pointer;">x</span>
                            </p>
                            <?php
                        }
                    }
                    ?>
                </div>
                <div id="textchatOTO"></div>
            </div>
        </div>
        <div id="footer-head" class="col-md-12 col-sm-12">
            <div id="footer-head-sub" class="col-md-12">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 left-align">
                    <div class="copyright">
                        <span class="footer-text">© 2018, </span>
                        <img src="https://www.hellouni.org/img/logo.png" width="90">&nbsp;
                        <span class="footer-text"> All rights reserved</span>
                        <!-- COPYRIGHT 2018 &copy; HELLOUNI 2018 -->
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 right-align">
                    <div class="design">
                        <a href="https://www.hellouni.org" class="footer-text"><b>Hellouni</b> </a> |  <a target="_blank" href="https://www.hellouni.org" class="footer-text">MAKE RIGHT CHOICE !</a>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var apiKey = "<?=$apiKey?>";
            var sessionId = "<?=$sessionId?>";
            var token = "<?=$token?>";
            var tokboxId = "<?=$tokboxId?>";
            var session;
            var archiveID;
            var allConnections = {};
            var attendeeButtonConnection = {};
            var allStreams = {};
            var publisher;

            var organizerCount = 0;
            var publisherCount = 0;
            var subscriberCount = 0;
            var connectionCount = 0;
            var totalSubscribers = 0;
            var pollAttenders = {};

            var screenShared = 0;
            var pollShared = 0;

            var msgHistory = document.querySelector('#history');
            var privateHistory = document.querySelector("#historyOTO");

            var attendeesListModal = document.getElementById("attendeesList");
            var coursespan = document.getElementById("closeModal");

            var pollModal = document.getElementById("pollId");
            var pollResultModal = document.getElementById("pollResultId");

            var chatBoxModal = document.getElementById("chatModal");
            var chatspan = document.getElementById("chatcloseModal");

            // Text chat
            var form = document.querySelector('form');
            var msgTxt = document.querySelector('#msgTxt');
            var username = document.querySelector('#username');

            function openNav() {
                document.getElementById("mySidepanel1").style.setProperty("width", "20%", "important");
                //document.getElementById("videos").style.setProperty("width", "80%", "important");
            }

            function closeNav() {
                document.getElementById("mySidepanel1").style.setProperty("width", "0", "important");
                document.getElementById("videos").style.setProperty("width", "100%", "important");
            }

            function expandChat() {
                document.getElementById("chat-box").style.setProperty("height", "95%", "important");
                document.getElementById("chat-box").style.display = "block";
                document.getElementById("chatBoxOut").style.display = "none";
                document.getElementById("chatBoxIn").style.display = "block";
            }

            function collapsChat() {
                document.getElementById("chat-box").style.setProperty("height", "54%", "important");
                document.getElementById("chat-box").style.display = "block";
                document.getElementById("chatBoxIn").style.display = "none";
                document.getElementById("chatBoxOut").style.display = "block";
            }

            function openListPopup() {
                attendeesListModal.style.display = "block";
            }

            coursespan.onclick = function() {
                attendeesListModal.style.display = "none";
            }

            chatspan.onclick = function() {
                chatBoxModal.style.display = "none";
            }

            window.onclick = function(event) {
                if (event.target == attendeesListModal) {
                    attendeesListModal.style.display = "none";
                }
                else if (event.target == pollModal) {
                    pollModal.style.display = "none";
                }
                else if (event.target == chatBoxModal) {
                    chatBoxModal.style.display = "none";
                }
            }

            function openPollPopup() {
                document.getElementById("publisher").style.display = "none";
                document.getElementById("subscriber").style.display = "none";
                document.getElementById("poll-start-button").style.display = "none";
                document.getElementById("poll-end-button").style.display = "inline-block";
                //console.log(quiz);
                //console.log(quiz.current_question_index);

                session.signal({
                    type: 'poll-start',
                    data: quiz.current_question_index
                }, function signalCallback(error) {
                    if (error) {
                        //console.error('Error sending signal:', error.name, error.message);
                    } else {
                        //console.log("polling started");
                        $.ajax({
                            url: '/meeting/webinar/runpoll',
                            type: 'POST',
                            contentType: 'application/json', // send as JSON
                            data: JSON.stringify({'action': 'add', 'tid': tokboxId, 'cid': session.connection.connectionId, 'qi': quiz.current_question_index}),
                            complete: function complete() {
                                // called when complete
                                //console.log('run poll saved');
                            },
                            success: function success() {
                                // called when successful
                                console.log('successfully saved running poll');
                            },
                            error: function error() {
                                // called when there is an error
                                console.log('error in saving running poll');
                            }
                        });
                    }
                });
            }

            function closePollPopup() {
                document.getElementById("publisher").style.display = "block";
                document.getElementById("subscriber").style.display = "block";
                document.getElementById("poll-end-button").style.display = "none";
                document.getElementById("poll-start-button").style.display = "inline-block";

                session.signal({
                    type: 'poll-stop',
                    data: ''
                }, function signalCallback(error) {
                    if (error) {
                        //console.error('Error sending signal:', error.name, error.message);
                    } else {
                        //console.log("polling started");
                        $.ajax({
                            url: '/meeting/webinar/runpoll',
                            type: 'POST',
                            contentType: 'application/json', // send as JSON
                            data: JSON.stringify({'action': 'delete', 'tid': tokboxId}),
                            complete: function complete() {
                                // called when complete
                                //console.log('run poll saved');
                            },
                            success: function success() {
                                // called when successful
                                console.log('successfully saved running poll');
                            },
                            error: function error() {
                                // called when there is an error
                                console.log('error in saving running poll');
                            }
                        });
                    }
                });
            }

            function openPollResult() {
                document.getElementById("poll-open-result-button").style.display = "none";
                document.getElementById("poll-close-result-button").style.display = "inline-block";

                $.ajax({
                    url: '/tokbox/pollResult/' + sessionId,
                    type: 'GET',
                    complete: function complete() {
                    },
                    success: function success(data) {
                        var w = 100;
                        var divW = 15;

                        var screenDivisor = totalSubscribers + 1;
                        switch(screenDivisor){
                            case 2:
                                w = 25;
                                divW = 45;
                                break;
                            case 3:
                                w = 35;
                                divW = 30;
                                break;
                            case 4:
                                w = 45;
                                divW = 23;
                                break;
                            case 5:
                                w = 55;
                                divW = 18;
                                break;
                            case 6:
                                w = 65;
                                divW = 15;
                                break;
                            case 7:
                                w = 75;
                                divW = 13;
                                break;
                            case 8:
                                w = 85;
                                divW = 12;
                                break;
                            case 9:
                                w = 95;
                                divW = 10;
                                break;
                            case 10:
                                w = 100;
                                divW = 9;
                                break;
                            case 11:
                            case 12:
                                w = 100;
                                divW = 9;
                                t = 3;
                                //console.log("I am in 9");
                                break;
                        }

                        document.getElementById("screen-preview").style.display = "none";
                        let attrb = "width: " + w + "%; height: 14%; position: relative; left: 0; top: 1%;";
                        document.getElementById("subscriber").setAttribute("style", attrb);

                        $('#subscriber .OT_root').css({
                            width: divW + '%',
                            height: '100%',
                            marginRight: '10px'
                        });

                        document.getElementById("quiz-results").innerHTML = data;
                        document.getElementById("quiz-results").style.display = "block";
                        pollResultModal.style.display = "block";
                    },
                    error: function error() {
                    }
                });
            }

            function closePollResult() {
                session.signal({
                    type: 'poll-stop',
                    data: ''
                }, function signalCallback(error) {
                    if (error) {
                        //console.error('Error sending signal:', error.name, error.message);
                    } else {
                        $.ajax({
                            url: '/meeting/webinar/runpoll',
                            type: 'POST',
                            contentType: 'application/json', // send as JSON
                            data: JSON.stringify({'action': 'delete', 'tid': tokboxId}),
                            complete: function complete() {
                                // called when complete
                                //console.log('run poll saved');
                            },
                            success: function success() {
                                // called when successful
                                console.log('successfully saved running poll');
                            },
                            error: function error() {
                                // called when there is an error
                                console.log('error in saving running poll');
                            }
                        });
                        //console.log("polling started");
                    }
                });
            }

            function sharePollResult(){
                //console.log(quiz.current_question_index, quiz.questions[quiz.current_question_index], quiz.questions);
                var questionId = quiz.questions[quiz.current_question_index].question_id;
                $.ajax({
                    url: '/tokbox/pollResult/' + sessionId + '/' + questionId,
                    type: 'GET',
                    complete: function complete() {
                        // called when complete
                        //console.log('connection function completed');
                    },
                    success: function success(data) {
                        session.signal({
                            type: 'share-poll-result',
                            data: data
                        }, function signalCallback(error) {
                            if (error) {
                                //console.error('Error sending signal:', error.name, error.message);
                            } else {
                                //console.log("polling started");
                            }
                        });
                    },
                    error: function error() {
                        // called when there is an error
                        //console.log('error calling connection');
                    }
                });
            }

            var all_questions = <?=$polling_questions?>;
            // console.log("LIST :", all_questions);
            

            var strQuestion = '<ol>'
                all_questions.forEach(function(questList, index) {
                    // console.log("index", index);
                  strQuestion += '<li id="'+questList.id+'">'+ questList.question_string + '<button id="pollQuestionPush" class="btn btn-md btn-primary" onclick="pushQuestionToAll(\'' + index +'\')" style="margin-left:5px">Push</button> <span></span><button id="pollQuestionClose" class="btn btn-md btn-danger" onclick="closeQuestionToAll(\'' + index +'\')" style="margin-left:5px">Close</button> </li>';
                      
                }); 
                strQuestion += '</ol>';
                document.getElementById("questionList").innerHTML = strQuestion;

            
                function pushQuestionToAll(QId) {
                    console.log("Qid: ",QId);
                    var current_question_index = QId;
                    console.log("current_question_index: ",current_question_index);
                    
                    
                    var quizOne = new Quiz('ANSWER THE FOLLOWING Que');
                    
                    var question = new Question(all_questions[current_question_index]);
                    console.log("list only ", question);
                   
                    quizOne.add_question(question);
                      
                    var quiz_containerOne = $('#quizOne');
                    quizOne.render(quiz_containerOne);
                   
                    

                }

            var Quiz = function(quiz_name) {
                // Private fields for an instance of a Quiz object.
                this.quiz_name = quiz_name;
                // var current_question_index = 0 ;
                // this.current_question_index = 0;
                this.current_question_index = 0 ;
                // This one will contain an array of Question objects in the order that the questions will be presented.
                this.questions = [];
                this.question_container = $('<div>').attr('id', 'question').insertAfter('#quiz-name');
            }

            

            Quiz.prototype.add_question = function(question) {
                this.questions.push(question);
            }

            Quiz.prototype.change_question = function (current_question_index) {
                this.questions[current_question_index].render(this.question_container);
                $('#next-question-button').prop('disabled', current_question_index === this.questions.length - 1);
            }

            

            Quiz.prototype.render = function(container) {
                // For when we're out of scope
                var self = this;

                // Hide the quiz results modal
                $('#quiz-results').hide();

                // Write the name of the quiz
                $('#quiz-name').text(this.quiz_name);

                // Create a container for questions
                //var question_container = $('<div>').attr('id', 'question').insertAfter('#quiz-name');
                self.question_container = $('<div>').attr('id', 'question').insertAfter('#quiz-name');

                // Helper function for changing the question and updating the buttons
                /*function change_question() {
                    self.questions[current_question_index].render(question_container);
                    $('#prev-question-button').prop('disabled', current_question_index === 0);
                    $('#next-question-button').prop('disabled', current_question_index === self.questions.length - 1);

                    // Determine if all questions have been answered
                    var all_questions_answered = true;
                    for (var i = 0; i < self.questions.length; i++) {
                        if (self.questions[i].user_choice_index === null) {
                            all_questions_answered = false;
                            break;
                        }
                    }
                    $('#submit-button').prop('disabled', !all_questions_answered);
                }*/

                // Render the first question
                //var current_question_index = 0;
                self.change_question(this.current_question_index);


                

                // Add listener for the previous question button
                $('#prev-question-button').click(function() {
                    if (self.current_question_index > 0) {
                        self.current_question_index--;
                        self.change_question(self.current_question_index);
                        session.signal({
                            type: 'poll-start',
                            data: self.current_question_index
                        }, function signalCallback(error) {
                            if (error) {
                                //console.error('Error sending signal:', error.name, error.message);
                            } else {
                                //console.log("polling started");
                            }
                        });
                    }
                });

                // Add listener for the next question button
                $('#next-question-button').click(function() {
                    if (self.current_question_index < self.questions.length - 1) {
                        self.questions[self.current_question_index].session_id = sessionId;
                        /*jQuery.ajax({
                            type: "POST",
                            contentType: "application/json",
                            data: JSON.stringify(self.questions[current_question_index]),
                            url: "/tokbox/poll",
                          success: function(response) {
                                console.log(response);
                            }
                        });*/
                        document.getElementById("poll-attenders").innerHTML = '';
                        self.current_question_index++;
                        self.change_question(self.current_question_index);
                        session.signal({
                            type: 'poll-start',
                            data: self.current_question_index
                        }, function signalCallback(error) {
                            if (error) {
                                //console.error('Error sending signal:', error.name, error.message);
                            } else {
                                //console.log("polling started");
                                $.ajax({
                                    url: '/meeting/webinar/runpoll',
                                    type: 'POST',
                                    contentType: 'application/json', // send as JSON
                                    data: JSON.stringify({'action': 'add', 'tid': tokboxId, 'cid': session.connection.connectionId, 'qi': self.current_question_index}),
                                    complete: function complete() {
                                        // called when complete
                                        //console.log('run poll saved');
                                    },
                                    success: function success() {
                                        // called when successful
                                        console.log('successfully saved running poll');
                                    },
                                    error: function error() {
                                        // called when there is an error
                                        console.log('error in saving running poll');
                                    }
                                });
                            }
                        });
                    }
                });

                // Add listener for the submit answers button
                $('#submit-button').click(function() {
                    // Determine how many questions the user got right
                    var score = 0;
                    for (var i = 0; i < self.questions.length; i++) {
                        if (self.questions[i].user_choice_index === self.questions[i].correct_choice_index) {
                            score++;
                        }
                        self.questions[i].session_id = sessionId;
                    }

                    //console.log(self.questions);
                    jQuery.ajax({
                        type: "POST",
                        contentType: "application/json",
                        data: JSON.stringify(self.questions[self.questions.length - 1]),
                        url: "/tokbox/poll",
                      success: function(response) {
                            console.log(response);
                        }
                    });

                    // Display the score with the appropriate message
                    var percentage = score / self.questions.length;
                    //console.log(percentage);
                    var message;
                    message = "Thank you for your valuable feedback";
                    //$('#quiz-results-message').text(message);
                    //$('#quiz-results-score').html('You got <b>' + score + '/' + self.questions.length + '</b> questions correct.');
                    $('#quiz-results').slideDown();
                    $('#submit-button').slideUp();
                    $('#next-question-button').slideUp();
                    $('#prev-question-button').slideUp();
                    $('#quiz-retry-button').slideDown();

                });

                // Add a listener on the questions container to listen for user select changes. This is for determining whether we can submit answers or not.
                self.question_container.bind('user-select-change', function() {
                    var all_questions_answered = true;
                    for (var i = 0; i < self.questions.length; i++) {
                        if (self.questions[i].user_choice_index === null) {
                            all_questions_answered = false;
                            break;
                        }
                    }
                    $('#submit-button').prop('disabled', !all_questions_answered);
                });
            }

            // An object for a Question, which contains the question, the correct choice, and wrong choices. This block is the constructor.
            var Question = function(question_obj) {
                var question_string = question_obj.question_string;
                var correct_choice = question_obj.choices.correct;
                var wrong_choices = question_obj.choices.wrong;
                var question_id = question_obj.id;

                this.question_string = question_string;
                this.choices = [];
                this.user_choice_index = null; // Index of the user's choice selection
                this.correct_ans = correct_choice;
                this.question_id = question_id;
                this.user_ans = null;

                // Random assign the correct choice an index
                this.correct_choice_index = Math.floor(Math.random() * (wrong_choices.length + 1));
                //console.log(this.correct_choice_index, wrong_choices.length + 1);

                // Fill in this.choices with the choices
                /*var number_of_choices = wrong_choices.length + 1;
                for (var i = 0; i < number_of_choices; i++) {
                    if (i === this.correct_choice_index) {
                        this.choices[i] = correct_choice;
                    }
                    else {
                        // Randomly pick a wrong choice to put in this index
                        var wrong_choice_index = Math.floor(Math.random() * (wrong_choices.length));;
                        this.choices[i] = wrong_choices[wrong_choice_index];

                        // Remove the wrong choice from the wrong choice array so that we don't pick it again
                        wrong_choices.splice(wrong_choice_index, 1);
                    }
                }*/
                this.choices = wrong_choices;
            }

            Question.prototype.render = function(container) {
                // For when we're out of scope
                var self = this;

                // Fill out the question label
                var question_string_h2;
                if (container.children('h2').length === 0) {
                    question_string_h2 = $('<h2>').appendTo(container);
                }
                else {
                    question_string_h2 = container.children('h2').first();
                }
                question_string_h2.text(this.question_string);

                // Clear any radio buttons and create new ones
                if (container.children('input[type=radio]').length > 0) {
                    container.children('input[type=radio]').each(function() {
                        var radio_button_id = $(this).attr('id');
                        $(this).remove();
                        container.children('label[for=' + radio_button_id + ']').remove();
                    });
                }
                for (var i = 0; i < this.choices.length; i++) {
                    // Create the radio button
                    var choice_radio_button = $('<input>')
                    .attr('id', 'choices-' + i)
                    .attr('type', 'radio')
                    .attr('name', 'choices')
                    .attr('value', 'choices-' + i)
                    .attr('checked', i === this.user_choice_index)
                    .appendTo(container);

                    // Create the label
                    var choice_label = $('<label>')
                    .text(this.choices[i])
                    .attr('for', 'choices-' + i)
                    .appendTo(container);
                }

                // Add a listener for the radio button to change which one the user has clicked on
                $('input[name=choices]').change(function(index) {
                    var selected_radio_button_value = $('input[name=choices]:checked').val();

                    // Change the user choice index
                    self.user_choice_index = parseInt(selected_radio_button_value.substr(selected_radio_button_value.length - 1, 1));
                    self.user_ans = self.choices[self.user_choice_index];

                    // Trigger a user-select-change
                    container.trigger('user-select-change');
                });
            }

            var quiz = new Quiz('ANSWER THE FOLLOWING');

            $(document).ready(function () {
                $(".slide-toggle").click(function(){
                    $(".box").animate({
                        width: "toggle"
                    });
                });

                $('.btn-filter').on('click', function () {
                    var $target = $(this).data('target');
                    if ($target != 'all') {
                        $('.table tr').css('display', 'none');
                        $('.table tr[data-status="' + $target + '"]').fadeIn('slow');
                    } else {
                        $('.table tr').css('display', 'none').fadeIn('slow');
                    }
                });

                // Create Question objects from all_questions and add them to the Quiz object
                for (var i = 0; i < all_questions.length; i++) {
                    // Create a new Question object
                    var question = new Question(all_questions[i]);
                    // console.log("list only ", question.question_string);
                    // Add the question to the instance of the Quiz object that we created previously
                    quiz.add_question(question);
                    // document.getElementById("questionList").innerHTML = question.question_string;
                }
                    //questionList
                // Render the quiz
                var quiz_container = $('#quiz');
                quiz.render(quiz_container);
                //console.log(quiz);
            });

            

            // Handling all of our errors here by alerting them
            function handleError(error) {
                if (error) {
                    //alert(error.message);
                }
            }

            function initializeSession() {
                session = OT.initSession(apiKey, sessionId);

                // Create a publisher
                publisher = OT.initPublisher('subscriber', {
                    insertMode: 'append',
                    width: '100%',
                    height: '100%',
                    name: '<?=$username?>',
                    publishAudio:true,
                    publishVideo:true,
                    style: {buttonDisplayMode: 'on', nameDisplayMode: "on"}
                    //videoSource: 'screen'
                }, handleError);

                // Connect to the session
                session.connect(token, function(error) {
                    // If the connection is successful, publish to the session
                    if (error) {
                        handleError(error);
                    } else {
                        var node = document.createElement("button");
                        node.setAttribute("id", "video-button-" + publisher.id);
                        node.setAttribute("class", "OT_edge-bar-item OT_mode-on OT_video AVButton fas fa-video");
                        node.setAttribute("onClick", "disableVideo()")
                        document.getElementById(publisher.id).appendChild(node);
                        // document.getElementById("video-button-" + publisher.id).innerHTML = "Disable Video";
                         document.getElementById("video-button-" + publisher.id).style.setProperty("background-color", "#ffffff00", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("font-size", "25px", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("right", "30px", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("top", "4px", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("color", "white", "important");

                        session.publish(publisher, handleError);
                    }
                });

                session.on('signal:msg', function signalCallback(event) {
                    if(event.from.connectionId === session.connection.connectionId){
                        document.getElementById("chat-box").style.display="block";
                        var msg = document.createElement('p');
                        var splitData = event.data.split('|');
                        //msg.textContent = event.data;
                        //msg.textContent = splitData[0];
                        msg.setAttribute("id", splitData[1]);
                        msg.className = 'mine';
                        msgHistory.appendChild(msg);
                        document.getElementById(splitData[1]).innerHTML = splitData[0] + '<span style="margin-top: -10px; float: left; font-size: 14px; cursor: pointer;" onclick="deleteChat(\'' + splitData[1] + '\')">x</span>';
                        msg.scrollIntoView();
                    }
                    else if(event.from.permissions.publish || session.connection.permissions.publish){
                        document.getElementById("chat-box").style.display="block";
                        var msg = document.createElement('p');
                        var splitData = event.data.split('|');
                        //msg.textContent = event.data;
                        //msg.textContent = splitData[0];
                        msg.setAttribute("id", splitData[1]);
                        msg.className = 'theirs';
                        msgHistory.appendChild(msg);
                        document.getElementById(splitData[1]).innerHTML = splitData[0] + '<span style="margin-top: -10px; margin-right: -5px; font-size: 14px; cursor: pointer;" onclick="deleteChat(\'' + splitData[1] + '\')">x</span>';
                        msg.scrollIntoView();
                    }
                });

                session.on('signal:del-msg', function signalCallback(event) {
                    $("#" + event.data).remove();
                });

                session.on('signal:submit-poll', function signalCallback(event) {
                    //$("#" + event.data).remove();
                    pollAttenders[event.data]++;
                    if(subscriberCount){
                        var attendersPercentage = pollAttenders[event.data] * 100 / (subscriberCount);
                        document.getElementById("poll-attenders").innerHTML = '(' + attendersPercentage.toFixed(2) + '%)';
                    }
                });

                session.on('signal:poll-start', function signalCallback(event) {
                    pollShared = 1;
                    $('#share-screen-button').hide();
                    //console.log(event.data);
                    //document.getElementById("subscriber").style.display = "none";
                    //document.getElementById("publisher").style.display = "none";
                    /*var w = 25 * (totalSubscribers + 1);
                    let attrb = "width: " + w + "%; height: 20%; position: relative; left: 0; top: 1%;";
                    document.getElementById("subscriber").setAttribute("style", attrb);

                    var divW = 96 / (totalSubscribers + 1);

                    $('#subscriber .OT_root').css({
                        width: divW + '%',
                        height: '100%',
                        marginRight: '10px'
                    });*/
                    var w = 100;
                    var divW = 15;

                    var screenDivisor = totalSubscribers + 1;
                    switch(screenDivisor){
                        case 2:
                            w = 25;
                            divW = 45;
                            break;
                        case 3:
                            w = 35;
                            divW = 30;
                            break;
                        case 4:
                            w = 45;
                            divW = 23;
                            break;
                        case 5:
                            w = 55;
                            divW = 18;
                            break;
                        case 6:
                            w = 65;
                            divW = 15;
                            break;
                        case 7:
                            w = 75;
                            divW = 13;
                            break;
                        case 8:
                            w = 85;
                            divW = 12;
                            break;
                        case 9:
                            w = 95;
                            divW = 10;
                            break;
                        case 10:
                            w = 100;
                            divW = 9;
                            break;
                        case 11:
                        case 12:
                            w = 100;
                            divW = 9;
                            t = 3;
                            //console.log("I am in 9");
                            break;
                    }

                    document.getElementById("screen-preview").style.display = "none";
                    let attrb = "width: " + w + "%; height: 14%; position: relative; left: 0; top: 1%;";
                    document.getElementById("subscriber").setAttribute("style", attrb);

                    $('#subscriber .OT_root').css({
                        width: divW + '%',
                        height: '100%',
                        marginRight: '10px'
                    });
                    if(event.from.connectionId != session.connection.connectionId){
                        document.getElementById("poll-start-button").style.display = "none";
                        document.getElementById("poll-end-button").style.display = "none";
                    }
                    quiz.change_question(event.data);
                    if(pollAttenders[event.data] === undefined){
                        pollAttenders[event.data] = 0;
                    }

                    /*var countDownDate = 180000 + new Date().getTime();
                    // Update the count down every 1 second
                    var x = setInterval(function(){
                        // Get today's date and time
                        var now = new Date().getTime();

                        // Find the distance between now and the count down date
                        var distance = countDownDate - now;

                        // Time calculations for days, hours, minutes and seconds
                        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                        if(distance >= 0){
                            document.getElementById("poll-timer").innerHTML = "<b>Time</b> " + minutes + "m " + seconds + "s ";
                        }
                        // If the count down is over, write some text
                        if (distance < 0)
                        {
                            document.getElementById("quiz").innerHTML = "<h1>Time Up! This window will be closed in 5 sec</h1>";
                        }

                        if(distance < -5000)
                        {
                            clearInterval(x);
                            document.getElementById("poll-start-button").style.display = "none";
                            document.getElementById("poll-end-button").style.display = "none";

                            pollModal.style.display = "none";
                            document.getElementById("publisher").style.display = "block";
                            document.getElementById("subscriber").style.display = "block";
                        }
                    }, 1000);*/
                    pollModal.style.display = "block";
                });

                session.on('signal:poll-stop', function signalCallback(event) {
                    pollShared = 0;
                    $('#share-screen-button').show();
                    document.getElementById("poll-start-button").style.display = "inline-block";
                    document.getElementById("poll-open-result-button").style.display = "inline-block";
                    document.getElementById("poll-end-button").style.display = "none";
                    document.getElementById("poll-close-result-button").style.display = "none";
                    pollModal.style.display = "none";
                    pollResultModal.style.display = "none";

                    /*var w = 25 * (totalSubscribers + 1);
                    var l = 40 / (totalSubscribers + 1);
                    let attrb = "width: " + w + "%; height: 30%; position: relative; left: " + l + "%; top: 30%;";
                    document.getElementById("subscriber").setAttribute("style", attrb);

                    var divW = 96 / (totalSubscribers + 1);

                    $('#subscriber .OT_root').css({
                        width: divW + '%',
                        height: '100%',
                        marginRight: '10px'
                    });*/
                    var w = 25;
                    var l = 40;
                    var divW = 96;
                    var t = 30;

                    var screenDivisor = totalSubscribers + 1;
                    switch(screenDivisor){
                        case 1:
                            //console.log("I am in 1");
                            break;
                        case 2:
                            w = 50;
                            l = 20;
                            divW = 45;
                            break;
                        case 3:
                        case 4:
                            w = 50;
                            l = 20;
                            divW = 45;
                            t = 20;
                            break;
                        case 5:
                        case 6:
                            w = 70;
                            l = 15;
                            divW = 30;
                            t = 20;
                            break;
                        case 7:
                        case 8:
                            w = 98;
                            l = 5;
                            divW = 23;
                            t = 20;
                            break;
                        case 9:
                        case 10:
                        case 11:
                        case 12:
                            w = 98;
                            l = 5;
                            divW = 23;
                            t = 3;
                            //console.log("I am in 9");
                            break;
                    }

                    let attrb = "width: " + w + "%; height: 30%; position: relative; left: " + l + "%; top: " + t + "%;";
                    document.getElementById("subscriber").setAttribute("style", attrb);

                    $('#subscriber .OT_root').css({
                        width: divW + '%',
                        height: '100%',
                        marginRight: '10px'
                    });
                });

                session.on('signal:share-poll-result', function signalCallback(event) {
                    pollModal.style.display = "none";
                    if(event.from.connectionId == session.connection.connectionId && quiz.current_question_index < quiz.questions.length - 1){
                        quiz.current_question_index++;
                    }
                    document.getElementById("poll-start-button").style.display = "none";
                    document.getElementById("poll-end-button").style.display = "none";

                    /*var w = 25 * (totalSubscribers + 1);
                    let attrb = "width: " + w + "%; height: 20%; position: relative; left: 0; top: 1%;";
                    document.getElementById("subscriber").setAttribute("style", attrb);

                    var divW = 96 / (totalSubscribers + 1);

                    $('#subscriber .OT_root').css({
                        width: divW + '%',
                        height: '100%',
                        marginRight: '10px'
                    });*/
                    var w = 100;
                    var divW = 15;

                    var screenDivisor = totalSubscribers + 1;
                    switch(screenDivisor){
                        case 2:
                            w = 25;
                            divW = 45;
                            break;
                        case 3:
                            w = 35;
                            divW = 30;
                            break;
                        case 4:
                            w = 45;
                            divW = 23;
                            break;
                        case 5:
                            w = 55;
                            divW = 18;
                            break;
                        case 6:
                            w = 65;
                            divW = 15;
                            break;
                        case 7:
                            w = 75;
                            divW = 13;
                            break;
                        case 8:
                            w = 85;
                            divW = 12;
                            break;
                        case 9:
                            w = 95;
                            divW = 10;
                            break;
                        case 10:
                            w = 100;
                            divW = 9;
                            break;
                        case 11:
                        case 12:
                            w = 100;
                            divW = 9;
                            t = 3;
                            //console.log("I am in 9");
                            break;
                    }

                    document.getElementById("screen-preview").style.display = "none";
                    let attrb = "width: " + w + "%; height: 14%; position: relative; left: 0; top: 1%;";
                    document.getElementById("subscriber").setAttribute("style", attrb);

                    $('#subscriber .OT_root').css({
                        width: divW + '%',
                        height: '100%',
                        marginRight: '10px'
                    });

                    document.getElementById("quiz-results").innerHTML = event.data + '<button id="poll-result-close-button" class="btn btn-md btn-primary" onclick="closePollResult()" style="margin-left:48%">Close</button>';
                    document.getElementById("quiz-results").style.display = "block";
                    pollResultModal.style.display = "block";
                });

                session.on('archiveStarted', function archiveStarted(event) {
                    archiveID = event.id;
                    //console.log('Archive started ' + archiveID);
                    $('#start-recording').hide();
                    $('#stop-recording').hide();
                });

                session.on('archiveStopped', function archiveStopped(event) {
                    archiveID = event.id;
                    $('#start-recording').show();
                });

                session.on('signal:msg-private', function signalCallback(event) {
                    var msg = document.createElement('p');
                    msg.textContent = event.data;
                    msg.className = event.from.connectionId === session.connection.connectionId ? 'mine' : 'theirs';
                    privateHistory.appendChild(msg);
                    msg.scrollIntoView();
                    allConnections[event.from.connectionId] = event.from;
                    openChatBox(event.from.connectionId);
                });

                session.on({
                    connectionCreated: function (event) {
                        if (event.connection.connectionId == session.connection.connectionId) {
                            $.ajax({
                                url: '/tokbox/connection',
                                type: 'POST',
                                contentType: 'application/json', // send as JSON
                                data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId, 'connectionId': event.connection.id, 'role': 'Organizer', 'myConnectionId': session.connection.connectionId, 'username': event.connection.data}),
                                complete: function complete() {
                                    // called when complete
                                    //console.log('connection function completed');
                                },
                                success: function success() {
                                    // called when successful
                                    //console.log('successfully called connection');
                                },
                                error: function error() {
                                    // called when there is an error
                                    //console.log('error calling connection');
                                }
                            });
                        }
                        else{
                            $.ajax({
                                url: '/tokbox/connection',
                                type: 'POST',
                                contentType: 'application/json', // send as JSON
                                data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId, 'connectionId': event.connection.id, 'myConnectionId': session.connection.connectionId, 'username': event.connection.data}),
                                complete: function complete() {
                                    // called when complete
                                    //console.log('connection function completed');
                                },
                                success: function success(data) {
                                    if(data){
                                        data = JSON.parse(data);
                                        var attendees = data.attendees;
                                        var organizers = data.organizers;
                                        var panelists = data.panelists;

                                        organizerCount = data.organizer_count;
                                        publisherCount = data.panelist_count;
                                        subscriberCount = data.attendee_count;

                                        connectionCount = organizerCount + publisherCount + subscriberCount;

                                        if(organizerCount){
                                            organizers.forEach(function(organizerData){
                                                if(attendeeButtonConnection[organizerData.connection_id] === undefined){
                                                    var tr_node = document.createElement("tr");
                                                    tr_node.setAttribute("id", "attendees-list-" + organizerData.connection_id);

                                                    var tr_node1 = document.createElement("tr");
                                                    tr_node1.setAttribute("id", "attendees-list-menu-" + organizerData.connection_id);

                                                    var tdHtml1 = '<td>' + connectionCount + '</td><td>' + organizerData.username + '</td><td>Organizer</td><td class="text-center"><a href="javascript:void" onClick="openChatBox(\'' + organizerData.connection_id + '\')"><i class="fas fa-comments">&nbsp;</i></a></td>';

                                                    tr_node.setAttribute("data-status", "Organizer");

                                                    var tdHtml = '<td><input type="checkbox" value="' + organizerData.connection_id + '" onclick="showGroupChatButton(this.value)"></td><td>' + organizerData.username + '</td><td>Organizer</td><td class="text-center"><a href="javascript:void" onClick="openChatBox(\'' + organizerData.connection_id + '\')"><i class="fas fa-comments">&nbsp;</i></a></td>';

                                                    document.getElementById("attendees-list").appendChild(tr_node);
                                                    document.getElementById("attendees-list-" + organizerData.connection_id).innerHTML = tdHtml;

                                                    document.getElementById("attendees-list-menu").appendChild(tr_node1);
                                                    document.getElementById("attendees-list-menu-" + organizerData.connection_id).innerHTML = tdHtml1;

                                                    attendeeButtonConnection[organizerData.connection_id] = organizerData;
                                                }
                                            })
                                        }
                                        if(publisherCount){
                                            panelists.forEach(function(panelistData){
                                                if(attendeeButtonConnection[panelistData.connection_id] === undefined){
                                                    var tr_node = document.createElement("tr");
                                                    tr_node.setAttribute("id", "attendees-list-" + panelistData.connection_id);

                                                    var tr_node1 = document.createElement("tr");
                                                    tr_node1.setAttribute("id", "attendees-list-menu-" + panelistData.connection_id);

                                                    var tdHtml1 = '<td>' + connectionCount + '</td><td>' + panelistData.username + '</td><td>Panelist</td><td class="text-center"><a href="javascript:void" onClick="openChatBox(\'' + panelistData.connection_id + '\')"><i class="fas fa-comments">&nbsp;</i></a></td>';

                                                    tr_node.setAttribute("data-status", "Publisher");

                                                    var tdHtml = '<td><input type="checkbox" value="' + panelistData.connection_id + '" onclick="showGroupChatButton(this.value)"></td><td>' + panelistData.username + '</td><td>Panelist</td><td class="text-center"><a href="javascript:void" onClick="openChatBox(\'' + panelistData.connection_id + '\')"><i class="fas fa-comments">&nbsp;</i></a></td>';

                                                    document.getElementById("attendees-list").appendChild(tr_node);
                                                    document.getElementById("attendees-list-" + panelistData.connection_id).innerHTML = tdHtml;

                                                    document.getElementById("attendees-list-menu").appendChild(tr_node1);
                                                    document.getElementById("attendees-list-menu-" + panelistData.connection_id).innerHTML = tdHtml1;

                                                    attendeeButtonConnection[panelistData.connection_id] = panelistData;
                                                }
                                            })
                                        }
                                        if(subscriberCount){
                                            attendees.forEach(function(attendeeData){
                                                if(attendeeButtonConnection[attendeeData.connection_id] === undefined){
                                                    var tr_node = document.createElement("tr");
                                                    tr_node.setAttribute("id", "attendees-list-" + attendeeData.connection_id);

                                                    var tr_node1 = document.createElement("tr");
                                                    tr_node1.setAttribute("id", "attendees-list-menu-" + attendeeData.connection_id);

                                                    var tdHtml1 = '<td>' + connectionCount + '</td><td>' + attendeeData.username + '</td><td>Attendee</td><td class="text-center"><a href="javascript:void" onClick="openChatBox(\'' + attendeeData.connection_id + '\')"><i class="fas fa-comments">&nbsp;</i></a></td>';

                                                    tr_node.setAttribute("data-status", "Subscriber");

                                                    var tdHtml = '<td><input type="checkbox" value="' + attendeeData.connection_id + '" onclick="showGroupChatButton(this.value)"></td><td>' + attendeeData.username + '</td><td>Attendee</td><td class="text-center"><a href="javascript:void" onClick="openChatBox(\'' + attendeeData.connection_id + '\')"><i class="fas fa-comments">&nbsp;</i></a></td>';

                                                    document.getElementById("attendees-list").appendChild(tr_node);
                                                    document.getElementById("attendees-list-" + attendeeData.connection_id).innerHTML = tdHtml;

                                                    document.getElementById("attendees-list-menu").appendChild(tr_node1);
                                                    document.getElementById("attendees-list-menu-" + attendeeData.connection_id).innerHTML = tdHtml1;

                                                    attendeeButtonConnection[attendeeData.connection_id] = attendeeData;
                                                }
                                            })
                                        }

                                        document.getElementById("attendees-count").innerHTML = '(' + connectionCount + ')';
                                    }
                                    // called when successful
                                    //console.log('successfully called connection');
                                },
                                error: function error() {
                                    // called when there is an error
                                    //console.log('error calling connection');
                                }
                            });

                            allConnections[event.connection.connectionId] = event.connection;

                            /*if (session.capabilities.publish == 1) {
                                var tr_node = document.createElement("tr");
                                tr_node.setAttribute("id", "attendees-list-" + event.connection.connectionId);

                                var tr_node1 = document.createElement("tr");
                                tr_node1.setAttribute("id", "attendees-list-menu-" + event.connection.connectionId);

                                connectionCount++;
                                var tdHtml1 = '<td>' + connectionCount + '</td><td>' + event.connection.data + '</td><td>Organizer</td><td class="text-center"><a href="javascript:void" onClick="openChatBox(\'' + event.connection.connectionId + '\')"><i class="fas fa-comments">&nbsp;</i></a></td>';

                                if(event.connection.permissions.forceDisconnect){
                                    organizerCount++;
                                    tr_node.setAttribute("data-status", "Organizer");

                                    var tdHtml = '<td><input type="checkbox" value="' + event.connection.connectionId + '" onclick="showGroupChatButton(this.value)"></td><td>' + event.connection.data + '</td><td>Organizer</td><td class="text-center"><a href="javascript:void" onClick="openChatBox(\'' + event.connection.connectionId + '\')"><i class="fas fa-comments">&nbsp;</i></a></td>';
                                }
                                else if(event.connection.permissions.publish){
                                    publisherCount++;
                                    tr_node.setAttribute("data-status", "Publisher");

                                    var tdHtml = '<td><input type="checkbox" value="' + event.connection.connectionId + '" onclick="showGroupChatButton(this.value)"></td><td>' + event.connection.data + '</td><td>Publisher</td><td class="text-center"><a href="javascript:void" onClick="openChatBox(\'' + event.connection.connectionId + '\')"><i class="fas fa-comments">&nbsp;</i></a></td>';
                                }
                                else{
                                    subscriberCount++;
                                    tr_node.setAttribute("data-status", "Subscriber");

                                    var tdHtml = '<td><input type="checkbox" value="' + event.connection.connectionId + '" onclick="showGroupChatButton(this.value)"></td><td>' + event.connection.data + '</td><td>Subscriber</td><td class="text-center"><a href="javascript:void" onClick="openChatBox(\'' + event.connection.connectionId + '\')"><i class="fas fa-comments">&nbsp;</i></a></td>';
                                }

                                document.getElementById("attendees-list").appendChild(tr_node);
                                document.getElementById("attendees-list-" + event.connection.connectionId).innerHTML = tdHtml;

                                document.getElementById("attendees-list-menu").appendChild(tr_node1);
                                document.getElementById("attendees-list-menu-" + event.connection.connectionId).innerHTML = tdHtml1;

                                allConnections[event.connection.connectionId] = event.connection;
                            }

                            document.getElementById("attendees-count").innerHTML = '(' + connectionCount + ')';
                            //resizeSubscriber();*/
                        }
                    },
                    connectionDestroyed: function connectionDestroyedHandler(event) {
                        if (event.connection.connectionId != session.connection.connectionId) {
                            if(document.getElementById("attendees-list-" + event.connection.connectionId) !== undefined){
                                var element = document.getElementById("attendees-list-" + event.connection.connectionId);
                                if(element !== undefined && element){
                                    element.parentNode.removeChild(element);
                                }

                                connectionCount--;
                                document.getElementById("attendees-count").innerHTML = '(' + connectionCount + ')';
                            }
                        }
                        else{
                            alert("You have been disconnected. Please refresh the page to join again");
                        }

                        $.ajax({
                            url: '/tokbox/delconnection',
                            type: 'POST',
                            contentType: 'application/json', // send as JSON
                            data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId, 'connectionId': event.connection.id, 'myConnectionId': session.connection.connectionId}),
                            complete: function complete() {
                                // called when complete
                                //console.log('connection function completed');
                            },
                            success: function success(data) {
                                data = JSON.parse(data);
                                var attendees = data.attendees;
                                var organizers = data.organizers;
                                var panelists = data.panelists;

                                organizerCount = data.organizer_count;
                                publisherCount = data.panelist_count;
                                subscriberCount = data.attendee_count;

                                connectionCount = organizerCount + publisherCount + subscriberCount;
                                document.getElementById("attendees-count").innerHTML = '(' + connectionCount + ')';
                            },
                            error: function error() {
                                // called when there is an error
                                //console.log('error calling connection');
                            }
                        });
                    },
                    streamDestroyed: function (event){
                        //console.log("Stream Destroyed ", event);
                        /*document.getElementById("subscriber").style.display = "block";
                        document.getElementById("publisher").style.display = "block";
                        $('#share-screen-button').show();
                        document.getElementById("publisher").setAttribute("style", "margin-top: 180px;");*/

                        if(event.stream.videoType == 'camera'){
                            totalSubscribers--;

                            var gridWidth = '100%',
                                gridHeight = '100%',
                                gridItems = totalSubscribers,
                                grid_str = '',
                                $grid = $('#subscriber'),
                                i;

                            function dimensions(x) {
                                let quotient = parseInt(x / 4);
                                let remainder = x % 4;

                                /*let h = 400 / ( quotient + 1);
                                var w = screen.width / 2;
                                if(quotient == 0)
                                {
                                    w = w / remainder;
                                }
                                else
                                {
                                    w = w / 3;
                                }

                                $('#subscriber div').css({
                                    width: w,
                                    height: h
                                });*/

                                /*if(remainder > 0 && remainder <= 3)
                                {
                                    var w = 25 * (remainder + 1);
                                    var l = 40 / (remainder + 1);
                                    if(remainder == 3)
                                    {
                                        l = 0;
                                    }
                                    let attrb = "width: " + w + "%; height: 30%; position: relative; left: " + l + "%; top: 30%;";
                                    document.getElementById("subscriber").setAttribute("style", attrb);

                                    var divW = 96 / (remainder + 1);

                                    $('#subscriber .OT_root').css({
                                        width: divW + '%',
                                        height: '100%',
                                        marginRight: '10px'
                                    });
                                }*/
                                var w = 25;
                                var l = 40;
                                var divW = 96;
                                var t = 30;

                                var screenDivisor = totalSubscribers + 1;
                                switch(screenDivisor){
                                    case 2:
                                        w = 50;
                                        l = 20;
                                        divW = 45;
                                        break;
                                    case 3:
                                    case 4:
                                        w = 50;
                                        l = 20;
                                        divW = 45;
                                        t = 20;
                                        break;
                                    case 5:
                                    case 6:
                                        w = 70;
                                        l = 15;
                                        divW = 30;
                                        t = 20;
                                        break;
                                    case 7:
                                    case 8:
                                        w = 98;
                                        l = 5;
                                        divW = 23;
                                        t = 20;
                                        break;
                                    case 9:
                                    case 10:
                                    case 11:
                                    case 12:
                                        w = 98;
                                        l = 5;
                                        divW = 23;
                                        t = 3;
                                        console.log("I am in 9");
                                        break;
                                }

                                let attrb = "width: " + w + "%; height: 30%; position: relative; left: " + l + "%; top: " + t + "%;";
                                document.getElementById("subscriber").setAttribute("style", attrb);

                                $('#subscriber .OT_root').css({
                                    width: divW + '%',
                                    height: '100%',
                                    marginRight: '10px'
                                });
                            };

                            //$grid.width(gridWidth).height(gridHeight);

                            let subscriberId = allStreams[event.stream.id];

                            //console.log(allStreams);
                            document.getElementById(subscriberId).style.display = 'none';

                            /*if(totalSubscribers > 0){
                                dimensions(totalSubscribers);
                            }*/
                            if(!screenShared && !pollShared)
                            {
                                dimensions(totalSubscribers);
                            }

                            if(screenShared || pollShared)
                            {
                                var w = 100;
                                var divW = 15;

                                var screenDivisor = totalSubscribers + 1;
                                switch(screenDivisor){
                                    case 2:
                                        w = 25;
                                        divW = 45;
                                        break;
                                    case 3:
                                        w = 35;
                                        divW = 30;
                                        break;
                                    case 4:
                                        w = 45;
                                        divW = 23;
                                        break;
                                    case 5:
                                        w = 55;
                                        divW = 18;
                                        break;
                                    case 6:
                                        w = 65;
                                        divW = 15;
                                        break;
                                    case 7:
                                        w = 75;
                                        divW = 13;
                                        break;
                                    case 8:
                                        w = 85;
                                        divW = 12;
                                        break;
                                    case 9:
                                        w = 95;
                                        divW = 10;
                                        break;
                                    case 10:
                                        w = 100;
                                        divW = 9;
                                        break;
                                    case 11:
                                    case 12:
                                        w = 100;
                                        divW = 9;
                                        t = 3;
                                        //console.log("I am in 9");
                                        break;
                                }

                                //document.getElementById("screen-preview").style.display = "block";
                                let attrb = "width: " + w + "%; height: 14%; position: relative; left: 0; top: 1%;";
                                document.getElementById("subscriber").setAttribute("style", attrb);

                                $('#subscriber .OT_root').css({
                                    width: divW + '%',
                                    height: '100%',
                                    marginRight: '10px'
                                });
                            }

                            /*if(totalSubscribers == 0){
                                //document.getElementById("publisher").setAttribute("style", "width:100%; height:88%;");
                                document.getElementById("subscriber").setAttribute("style", "width: 25%; height: 30%; bottom: 10px; position: relative; left: 30%; top: 30%;");
                                $('#subscriber div').css({
                                    width: '100%',
                                    height: '100%'
                                });
                            }*/
                        }
                        if(event.stream.videoType == 'screen'){
                            screenShared = 0;
                            $('#poll-start-button').show();
                            $('#poll-end-button').hide();
                            //document.getElementById("subscriber").style.display = "block";
                            document.getElementById("screen-preview").style.display = "none";
                            //document.getElementById("publisher").setAttribute("style", "width:22%; height:20%; float: left; position: fixed; bottom: 6%; left: 0%; z-index: 100;");
                            $('#share-screen-button').show();

                            /*var w = 25 * (totalSubscribers + 1);
                            var l = 40 / (totalSubscribers + 1);
                            let attrb = "width: " + w + "%; height: 30%; position: relative; left: " + l + "%; top: 30%;";
                            document.getElementById("subscriber").setAttribute("style", attrb);

                            var divW = 96 / (totalSubscribers + 1);

                            $('#subscriber .OT_root').css({
                                width: divW + '%',
                                height: '100%',
                                marginRight: '10px'
                            });*/

                            var w = 25;
                            var l = 40;
                            var divW = 96;
                            var t = 30;

                            var screenDivisor = totalSubscribers + 1;
                            switch(screenDivisor){
                                case 1:
                                    //console.log("I am in 1");
                                    break;
                                case 2:
                                    w = 50;
                                    l = 20;
                                    divW = 45;
                                    break;
                                case 3:
                                case 4:
                                    w = 50;
                                    l = 20;
                                    divW = 45;
                                    t = 20;
                                    break;
                                case 5:
                                case 6:
                                    w = 70;
                                    l = 15;
                                    divW = 30;
                                    t = 20;
                                    break;
                                case 7:
                                case 8:
                                    w = 98;
                                    l = 5;
                                    divW = 23;
                                    t = 20;
                                    break;
                                case 9:
                                case 10:
                                case 11:
                                case 12:
                                    w = 98;
                                    l = 5;
                                    divW = 23;
                                    t = 3;
                                    //console.log("I am in 9");
                                    break;
                            }

                            let attrb = "width: " + w + "%; height: 30%; position: relative; left: " + l + "%; top: " + t + "%;";
                            document.getElementById("subscriber").setAttribute("style", attrb);

                            $('#subscriber .OT_root').css({
                                width: divW + '%',
                                height: '100%',
                                marginRight: '10px'
                            });
                        }
                    },
                    streamCreated: function(event){
                        if(event.stream.videoType == 'camera'){
                            var subscriber = session.subscribe(event.stream, 'subscriber', {
                                insertMode: 'append',
                                width: '100%',
                                height: '100%',
                                name: event.stream.name,
                            }, handleError);

                            //console.log(allStreams);
                            totalSubscribers++;
                            allStreams[event.stream.id] = subscriber.id;

                            var gridWidth = '100%',
                                gridHeight = '100%',
                                gridItems = totalSubscribers,
                                grid_str = '',
                                $grid = $('#subscriber'),
                                i;

                            function dimensions(x) {
                                let quotient = parseInt(x / 4);
                                let remainder = parseInt(x % 4);

                                /*let h = 400 / ( quotient + 1);
                                var w = screen.width / 2;
                                if(quotient == 0)
                                {
                                    w = w / remainder;
                                }
                                else
                                {
                                    w = w / 3;
                                }*/
                                //console.log(remainder, x);

                                /*if(remainder > 0 && remainder <= 3)
                                {
                                    var w = 25 * (remainder + 1);
                                    var l = 40 / (remainder + 1);
                                    if(remainder == 3)
                                    {
                                        l = 0;
                                    }
                                    let attrb = "width: " + w + "%; height: 30%; position: relative; left: " + l + "%; top: 30%;";
                                    document.getElementById("subscriber").setAttribute("style", attrb);

                                    var divW = 96 / (remainder + 1);

                                    $('#subscriber .OT_root').css({
                                        width: divW + '%',
                                        height: '100%',
                                        marginRight: '10px'
                                    });
                                }*/

                                /*$('#subscriber div').css({
                                    width: w,
                                    height: h
                                });*/
                                var w = 25;
                                var l = 40;
                                var divW = 96;
                                var t = 30;

                                var screenDivisor = totalSubscribers + 1;
                                switch(screenDivisor){
                                    case 1:
                                        //console.log("I am in 1");
                                        break;
                                    case 2:
                                        w = 50;
                                        l = 20;
                                        divW = 45;
                                        break;
                                    case 3:
                                    case 4:
                                        w = 50;
                                        l = 20;
                                        divW = 45;
                                        t = 20;
                                        break;
                                    case 5:
                                    case 6:
                                        w = 70;
                                        l = 15;
                                        divW = 30;
                                        t = 20;
                                        break;
                                    case 7:
                                    case 8:
                                        w = 98;
                                        l = 5;
                                        divW = 23;
                                        t = 20;
                                        break;
                                    case 9:
                                    case 10:
                                    case 11:
                                    case 12:
                                        w = 98;
                                        l = 5;
                                        divW = 23;
                                        t = 3;
                                        console.log("I am in 9");
                                        break;
                                }

                                let attrb = "width: " + w + "%; height: 30%; position: relative; left: " + l + "%; top: " + t + "%;";
                                document.getElementById("subscriber").setAttribute("style", attrb);

                                $('#subscriber .OT_root').css({
                                    width: divW + '%',
                                    height: '100%',
                                    marginRight: '10px'
                                });
                            };

                            //$grid.width(gridWidth).height(gridHeight);

                            //document.getElementById("publisher").setAttribute("style", "width:22%; height:20%;");

                            if(!screenShared && !pollShared)
                            {
                                document.getElementById(subscriber.id).style.display = 'inline-block';
                                document.getElementById(subscriber.id).style.boxSizing = 'border-box';
                                document.getElementById(subscriber.id).style.border = '2px solid white';
                                document.getElementById(subscriber.id).style.margin = '2px';

                                dimensions(totalSubscribers);
                            }

                            if(screenShared || pollShared)
                            {
                                var w = 100;
                                var divW = 15;

                                var screenDivisor = totalSubscribers + 1;
                                switch(screenDivisor){
                                    case 2:
                                        w = 25;
                                        divW = 45;
                                        break;
                                    case 3:
                                        w = 35;
                                        divW = 30;
                                        break;
                                    case 4:
                                        w = 45;
                                        divW = 23;
                                        break;
                                    case 5:
                                        w = 55;
                                        divW = 18;
                                        break;
                                    case 6:
                                        w = 65;
                                        divW = 15;
                                        break;
                                    case 7:
                                        w = 75;
                                        divW = 13;
                                        break;
                                    case 8:
                                        w = 85;
                                        divW = 12;
                                        break;
                                    case 9:
                                        w = 95;
                                        divW = 10;
                                        break;
                                    case 10:
                                        w = 100;
                                        divW = 9;
                                        break;
                                    case 11:
                                    case 12:
                                        w = 100;
                                        divW = 9;
                                        t = 3;
                                        //console.log("I am in 9");
                                        break;
                                }

                                //document.getElementById("screen-preview").style.display = "block";
                                let attrb = "width: " + w + "%; height: 14%; position: relative; left: 0; top: 1%;";
                                document.getElementById("subscriber").setAttribute("style", attrb);

                                $('#subscriber .OT_root').css({
                                    width: divW + '%',
                                    height: '100%',
                                    marginRight: '10px'
                                });
                            }

                            var SpeakerDetection = function(subscriber, startTalking, stopTalking) {
                                var activity = null;
                                subscriber.on('audioLevelUpdated', function(event) {
                                    var now = Date.now();
                                    if (event.audioLevel > 0.2) {
                                        if (!activity) {
                                            activity = {timestamp: now, talking: false};
                                        }
                                        else if (activity.talking) {
                                            activity.timestamp = now;
                                        }
                                        else if (now- activity.timestamp > 500) {
                                            activity.talking = true;
                                            if (typeof(startTalking) === 'function') {
                                                startTalking();
                                            }
                                        }
                                    }
                                    else if (activity && now - activity.timestamp > 3000) {
                                        // detected low audio activity for more than 3s
                                        if (activity.talking) {
                                            if (typeof(stopTalking) === 'function') {
                                                stopTalking();
                                            }
                                        }
                                        activity = null;
                                    }
                                });
                            };

                            SpeakerDetection(subscriber, function() {
                                //console.log('Started Talking ', subscriber.id, subscriber);
                                //document.getElementById(subscriber.id).style.overflow = "inherit";
                                document.getElementById(subscriber.id).style.setProperty("overflow", "inherit", "important");

                                var x = document.getElementById(subscriber.id).querySelectorAll(".OT_name");
                                x[0].style.display = "block";
                                x[0].style.opacity = "1";
                                x[0].style.top = "0";


                                var y = document.getElementById(subscriber.id).querySelectorAll(".OT_widget-container");
                                y[0].style.display = "block";
                                y[0].style.setProperty("border", "3px solid #00ca00", "important");



                            }, function() {
                                document.getElementById(subscriber.id).style.background = "";
                                var x = document.getElementById(subscriber.id).querySelectorAll(".OT_name");
                                x[0].style.display = "none";
                                x[0].style.opacity = "0";
                                x[0].style.top = "-25px";

                                var y = document.getElementById(subscriber.id).querySelectorAll(".OT_widget-container");
                                y[0].style.display = "block";
                                y[0].style.setProperty("border", "none", "important");

                                document.getElementById(subscriber.id).style.setProperty("overflow", "hidden", "important");
                                console.log('stopped talking');
                            });
                        }
                        if(event.stream.videoType == 'screen'){
                            screenShared = 1;
                            $('#poll-start-button').hide();
                            $('#share-screen-button').hide();
                            $('#poll-end-button').hide();
                            /*document.getElementById("subscriber").style.display = "none";
                            document.getElementById("screen-preview").style.display = "block";
                            document.getElementById("publisher").setAttribute("style", "width:15%; height:15%; left:auto; right:0px; top:60px; position:absolute; margin-top:0px");*/
                            var w = 100;
                            var divW = 15;

                            var screenDivisor = totalSubscribers + 1;
                            switch(screenDivisor){
                                case 2:
                                    w = 25;
                                    divW = 45;
                                    break;
                                case 3:
                                    w = 35;
                                    divW = 30;
                                    break;
                                case 4:
                                    w = 45;
                                    divW = 23;
                                    break;
                                case 5:
                                    w = 55;
                                    divW = 18;
                                    break;
                                case 6:
                                    w = 65;
                                    divW = 15;
                                    break;
                                case 7:
                                    w = 75;
                                    divW = 13;
                                    break;
                                case 8:
                                    w = 85;
                                    divW = 12;
                                    break;
                                case 9:
                                    w = 95;
                                    divW = 10;
                                    break;
                                case 10:
                                    w = 100;
                                    divW = 9;
                                    break;
                                case 11:
                                case 12:
                                    w = 100;
                                    divW = 9;
                                    t = 3;
                                    //console.log("I am in 9");
                                    break;
                            }

                            document.getElementById("screen-preview").style.display = "block";
                            let attrb = "width: " + w + "%; height: 14%; position: relative; left: 0; top: 1%;";
                            document.getElementById("subscriber").setAttribute("style", attrb);

                            $('#subscriber .OT_root').css({
                                width: divW + '%',
                                height: '100%',
                                marginRight: '10px'
                            });

                            /*document.getElementById("screen-preview").style.display = "block";
                            var w = 25 * (totalSubscribers + 1);
                            let attrb = "width: " + w + "%; height: 20%; position: relative; left: 0; top: 1%;";
                            document.getElementById("subscriber").setAttribute("style", attrb);

                            var divW = 96 / (totalSubscribers + 1);

                            $('#subscriber .OT_root').css({
                                width: divW + '%',
                                height: '100%',
                                marginRight: '10px'
                            });
                            //document.getElementById("subscriber").setAttribute("style", "position: absolute;left: 0;top: 0;width: 100%;z-index: 10;display: block;max-height: 150px;overflow-x: scroll;overflow-y: hidden;overflow: overlay;");
                            //document.getElementById("publisher").setAttribute("style", "width:15%; height:15%; left:auto; right:0px; top:60px; position:absolute; margin-top:0px");

                            //let subscriberId = allStreams[event.stream.id];
                            //console.log(allStreams);
                            /*for(let key in allStreams)
                            {
                                if(document.getElementById(allStreams[key]) !== undefined && document.getElementById(allStreams[key]))
                                {
                                    document.getElementById(allStreams[key]).setAttribute("style", "widht: 240px;height:150px;");
                                }
                            }*/

                            //console.log(allStreams);
                            //document.getElementById(subscriberId).style.display = 'none';
                            session.subscribe(event.stream, 'screen-preview', {
                                insertMode: 'append',
                                width: '100%',
                                height: '100%',
                                name: '<?=$username?>'
                            }, handleError);
                        }
                        <?php
                        if(isset($running_poll['connection_id']))
                        {
                            ?>
                            var pollConnectionId = '<?=$running_poll['connection_id']?>';
                            if(pollConnectionId == event.stream.connection.id)
                            {
                                pollShared = 1;
                                $('#share-screen-button').hide();
                                var w = 100;
                                var divW = 15;

                                var screenDivisor = totalSubscribers + 1;
                                switch(screenDivisor){
                                    case 2:
                                        w = 25;
                                        divW = 45;
                                        break;
                                    case 3:
                                        w = 35;
                                        divW = 30;
                                        break;
                                    case 4:
                                        w = 45;
                                        divW = 23;
                                        break;
                                    case 5:
                                        w = 55;
                                        divW = 18;
                                        break;
                                    case 6:
                                        w = 65;
                                        divW = 15;
                                        break;
                                    case 7:
                                        w = 75;
                                        divW = 13;
                                        break;
                                    case 8:
                                        w = 85;
                                        divW = 12;
                                        break;
                                    case 9:
                                        w = 95;
                                        divW = 10;
                                        break;
                                    case 10:
                                        w = 100;
                                        divW = 9;
                                        break;
                                    case 11:
                                    case 12:
                                        w = 100;
                                        divW = 9;
                                        t = 3;
                                        //console.log("I am in 9");
                                        break;
                                }

                                document.getElementById("screen-preview").style.display = "block";
                                let attrb = "width: " + w + "%; height: 14%; position: relative; left: 0; top: 1%;";
                                document.getElementById("subscriber").setAttribute("style", attrb);

                                $('#subscriber .OT_root').css({
                                    width: divW + '%',
                                    height: '100%',
                                    marginRight: '10px'
                                });
                                if(event.stream.connection.id != session.connection.connectionId){
                                    document.getElementById("poll-start-button").style.display = "none";
                                    document.getElementById("poll-end-button").style.display = "none";
                                }
                                quiz.change_question(<?=$running_poll['question_index']?>);
                                if(pollAttenders[event.data] === undefined){
                                    pollAttenders[event.data] = 0;
                                }
                                pollModal.style.display = "block";
                            }
                            <?php
                        }
                        ?>
                        //console.log("Stream Created ", event);
                    }
                });

                function resizeSubscriber() {
                    publisher.element.style.width = "100px";
                    publisher.element.style.height = "75px";
                    document.getElementById("subscriber").appendChild(publisher.element);
                }
            }

            function disableVideo(){
                publisher.publishVideo(false);
                document.getElementById("video-button-" + publisher.id).setAttribute("onClick", "enableVideo('" + publisher + "')");
                // document.getElementById("video-button-" + publisher.id).innerHTML = "Enable Video";
                document.getElementById("video-button-" + publisher.id).classList.remove('fa-video');
                document.getElementById("video-button-" + publisher.id).classList.add('fa-video-slash');

            }

            function enableVideo(){
                publisher.publishVideo(true);
                document.getElementById("video-button-" + publisher.id).setAttribute("onClick", "disableVideo('" + publisher + "')");
                // document.getElementById("video-button-" + publisher.id).innerHTML = "Disable Video";
                document.getElementById("video-button-" + publisher.id).classList.remove('fa-video-slash');
                document.getElementById("video-button-" + publisher.id).classList.add('fa-video');

            }

            function disconnectUser(connectionId){
                if (session.capabilities.forceDisconnect == 1) {
                    //alert(connectionId);
                    session.forceDisconnect(allConnections[connectionId]);
                } else {
                    alert("No permission");
                }
            }

            var group_chat_connections = [];
            function showGroupChatButton(value){
                var group_connections = document.getElementsByName("group_chat_connections[]");
                //console.log(value);
                var index = group_chat_connections.indexOf(value);
                if (index > -1) {
                    group_chat_connections.splice(index, 1);
                }
                else{
                    group_chat_connections.push(value);
                }
                if(group_chat_connections.length){
                    document.getElementById("group-chat-button").style.display = "block";
                }
                else{
                    document.getElementById("group-chat-button").style.display = "none";
                }
            }

            function openGroupChat(){
                var html = '<input type="text" placeholder="Input your text here" id="msgTxtOTO"></input><input type="hidden" id="username-private" value="<?=$username?>"></input><input type="button" class="btn btn-danger" onclick="sendGroupChat()" value="Send">';
                document.getElementById("textchatOTO").innerHTML = html;
                // modal.style.display = "block";
                chatBoxModal.style.display = "block";
            }

            function sendGroupChat(connectionId){
                var msg = document.getElementById("msgTxtOTO").value;
                var uname = document.getElementById("username-private").value;
                //console.log(allConnections[connectionId]);
                var msgEle = document.createElement('p');
                msgEle.textContent = uname + ': ' + msg;
                msgEle.className = 'mine';
                privateHistory.appendChild(msgEle);
                msgEle.scrollIntoView();

                for(var i = 0; i < group_chat_connections.length; i++){
                    $.ajax({
                        url: '/tokbox/chat',
                        type: 'POST',
                        contentType: 'application/json', // send as JSON
                        data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId, 'to': group_chat_connections[i], 'msg': msg}),
                        complete: function complete() {
                            // called when complete
                            console.log('chat function completed');
                        },
                        success: function success() {
                            // called when successful
                            console.log('successfully called chat');
                        },
                        error: function error() {
                            // called when there is an error
                            console.log('error calling chat');
                        }
                    });
                    session.signal({
                        to: allConnections[group_chat_connections[i]],
                        type: 'msg-private',
                        data: uname + ': ' + msg
                    }, function signalCallback(error) {
                        if (error) {
                            console.error('Error sending signal:', error.name, error.message);
                        } else {
                            msgTxt.value = '';
                        }
                    });
                }

            }

            function openChatBox(connectionId){
                var html = '<input type="text" placeholder="Input your text here" id="msgTxtOTO"></input><input type="hidden" id="username-private" value="<?=$username?>"></input><input type="button" class="btn btn-danger" onclick="sendChat(\'' + connectionId + '\')" value="Send">';
                document.getElementById("textchatOTO").innerHTML = html;
                // modal.style.display = "block";
                chatBoxModal.style.display = "block";
            }

            function sendChat(connectionId){
                var msg = document.getElementById("msgTxtOTO").value;
                var uname = document.getElementById("username-private").value;
                //console.log(allConnections[connectionId]);
                var msgEle = document.createElement('p');
                msgEle.textContent = uname + ': ' + msg;
                msgEle.className = 'mine';
                privateHistory.appendChild(msgEle);
                msgEle.scrollIntoView();

                $.ajax({
                    url: '/tokbox/chat',
                    type: 'POST',
                    contentType: 'application/json', // send as JSON
                    data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId, 'to': connectionId, 'msg': msg}),
                    complete: function complete() {
                        // called when complete
                        console.log('chat function completed');
                    },
                    success: function success() {
                        // called when successful
                        console.log('successfully called chat');
                    },
                    error: function error() {
                        // called when there is an error
                        console.log('error calling chat');
                    }
                });

                session.signal({
                    to: allConnections[connectionId],
                    type: 'msg-private',
                    data: uname + ': ' + msg
                }, function signalCallback(error) {
                    if (error) {
                        console.error('Error sending signal:', error.name, error.message);
                    } else {
                        msgTxt.value = '';
                    }
                });
            }

            function shareScreen(){
                $('#share-screen-button').hide();
                OT.checkScreenSharingCapability(function(response) {
                    if(!response.supported || response.extensionRegistered === false) {
                        $('#share-screen-button').show();
                    } else if (response.extensionInstalled === false) {
                        $('#share-screen-button').show();
                    } else {
                        // Create a publisher for screen
                        document.getElementById("screen-preview").style.display = "none";
                        var publisherScreen = OT.initPublisher('screen-preview', {
                            maxResolution:{width: 1920, height: 1080},
                            videoSource: 'screen'
                        }, handleError);

                        session.publish(publisherScreen, function(error) {
                            if (error) {
                                // Look error.message to see what went wrong.
                            }
                            else{
                                $('#share-screen-button').hide();
                            }
                        });

                        publisherScreen.on('streamDestroyed', function(event) {
                            //document.getElementById("publisher").style.display = "block";
                            $('#share-screen-button').show();
                            var node = document.createElement("div");
                            node.setAttribute("id", "screen-preview");
                            node.setAttribute("style", "display:none; margin-top: 3%;");
                            document.getElementById("videos").appendChild(node);
                            if (event.reason === 'mediaStopped') {
                                // User clicked stop sharing
                            } else if (event.reason === 'forceUnpublished') {
                                // A moderator forced the user to stop sharing.
                            }
                        });
                    }
                });
            }

            function deleteChat(chatId){
                $.ajax({
                    url: '/tokbox/delchat',
                    type: 'POST',
                    contentType: 'application/json', // send as JSON
                    data: JSON.stringify({'chat_id': chatId}),
                    complete: function complete() {
                        // called when complete
                        //console.log('chat function completed');
                    },
                    success: function success() {
                        session.signal({
                            type: 'del-msg',
                            data: chatId
                        }, function signalCallback(error) {
                            if (error) {
                                //console.error('Error sending signal:', error.name, error.message);
                            } else {
                                //msgTxt.value = '';
                            }
                        });
                    },
                    error: function error() {
                        // called when there is an error
                        //console.log('error calling chat');
                    }
                });
            }

            // Send a signal once the user enters data in the form
            form.addEventListener('submit', function submit(event) {
                var senderRole = session.connection.permissions.publish ? 'PUBLISHER' : 'SUBSCRIBER';
                event.preventDefault();
                var currentTime = new Date().getTime();
                var chatId = 'chat-' + currentTime;
                $.ajax({
                    url: '/tokbox/chat',
                    type: 'POST',
                    contentType: 'application/json', // send as JSON
                    data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId, 'to': 'All', 'msg': msgTxt.value, 'from_role': senderRole, 'chat_id': chatId}),
                    complete: function complete() {
                        // called when complete
                        //console.log('chat function completed');
                    },
                    success: function success() {
                        // called when successful
                        //console.log('successfully called chat');
                    },
                    error: function error() {
                        // called when there is an error
                        //console.log('error calling chat');
                    }
                });
                session.signal({
                    type: 'msg',
                    data: username.value + ': ' + msgTxt.value + '|' + chatId
                }, function signalCallback(error) {
                    if (error) {
                        //console.error('Error sending signal:', error.name, error.message);
                    } else {
                        msgTxt.value = '';
                    }
                });
            });

            // Start recording
            function startArchive() { // eslint-disable-line no-unused-vars
                $('#start-recording').hide();
                $.ajax({
                    url: '/tokbox/startArchive',
                    type: 'POST',
                    contentType: 'application/json', // send as JSON
                    data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId}),
                    complete: function complete() {
                        // called when complete
                        //console.log('startArchive() complete');
                        $('#stop-recording').show();
                    },
                    success: function success() {
                        // called when successful
                        //console.log('successfully called startArchive()');
                    },
                    error: function error() {
                        // called when there is an error
                        //console.log('error calling startArchive()');
                    }
                });
            }

            // Stop recording
            function stopArchive() { // eslint-disable-line no-unused-vars
                $.post('/tokbox/stopArchive/' + archiveID);
                $('#stop-recording').hide();
            }

            initializeSession();

            function leaveMeeting(){
                window.open('/test/thankyou', '_self');
            }

            function startSession(){
                $.ajax({
                    url: '/meeting/webinar/startSession',
                    type: 'POST',
                    contentType: 'application/json', // send as JSON
                    data: JSON.stringify({'tid': tokboxId}),
                    complete: function complete() {
                    },
                    success: function success() {
                    },
                    error: function error() {
                    }
                });
            }
        </script>
    </body>
</html>
