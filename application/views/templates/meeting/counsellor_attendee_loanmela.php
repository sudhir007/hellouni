<html>
    <head>
        <title> Hellouni : Visa Seminar | Student </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css">
        <link href="<?=base_url()?>css/organizer_visaseminar.css?v=1.0.1" rel="stylesheet">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://static.opentok.com/v2/js/opentok.min.js"></script>

        <style>
            body{
                background: url('<?=$backgroundImage['url']?>');
                background-size: 100vw 100vh;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid" id="videos">
            <div class="col-md-12" id="publisher" style="left:35%;"></div>
            <div class="col-md-12" id="subscriber"></div>
            <div class="col-md-12" id="screen-preview"></div>
        </div>

        <div id="mySidenav" class="sidenav">
            <a href="#" id="MenuTab" class="openbtn" onclick="openNav()" ><i class="fas fa-bars"></i></a>
        </div>

        <div id="mySidepanel1" class="sidepanel1">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><i class="fa fa-times" aria-hidden="true"></i></a>
            <a href="#" ><button type="button" class="slide-toggle btn btn-sm btn-info width100 Orangebtn" ><i class="fas fa-comments">&nbsp;</i><b>Chat</b></button></a>
            <a href="#" ><button type="button" class="btn btn-sm btn-danger width100 Orangebtn" onclick="leaveMeeting()"><b>Leave Seminar</b> </button></a>
        </div>

        <div class="box" id="chat-box">
            <span>
                <button id="chatBoxOut" onclick="expandChat()" type="button" class="btn btn-sm btn-primary marginbox" ><i class="fas fa-chevron-circle-up"></i></button>
                <button id="chatBoxIn" onclick="collapsChat()" type="button" class="btn btn-sm btn-primary marginbox"><i class="fas fa-chevron-circle-down"></i></button>
                <button type="button" class="slide-toggle btn btn-sm btn-danger marginbox" ><i class="fas fa-times-circle"></i></button>
            </span>
            <div class="box-inner">
                <div id="textchat">
                    <div id="history">
                        <?php
                        if($common_chat_history)
                        {
                            foreach($common_chat_history as $history)
                            {
                                ?>
                                <p id="<?=$history['chat_id']?>" class="<?=$history['chat_of']?>"><?=$history['name'] . ': ' . $history['message']?></p>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <form>
                        <input type="text" placeholder="Input your text here" id="msgTxt"></input>
                        <input type="hidden" id="username" value="<?=$username?>"></input>
                    </form>
                </div>
            </div>
        </div>

        <div id="footer-head" class="col-md-12 col-sm-12">
            <div id="footer-head-sub" class="col-md-12">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 left-align">
                    <div class="copyright">
                        <span class="footer-text">© 2018, </span>
                        <img src="https://www.hellouni.org/img/logo.png" width="90">&nbsp;
                        <span class="footer-text"> All rights reserved</span>
                        <!-- COPYRIGHT 2018 &copy; HELLOUNI 2018 -->
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 right-align">
                    <div class="design">
                        <a href="https://www.hellouni.org" class="footer-text"><b>Hellouni</b> </a> |  <a target="_blank" href="https://www.hellouni.org" class="footer-text">MAKE RIGHT CHOICE !</a>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            var screenWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
            //console.log("Screen ", screenWidth);

            var apiKey = "<?=$apiKey?>";
            var sessionId = "<?=$sessionId?>";
            var token = "<?=$token?>";
            var tokboxId = "<?=$tokboxId?>";
            var base64TokenId = "<?=$base64_token_id?>";
            var base64Username = "<?=$base64_username?>";

            var session;
            var allConnections = {};
            var allStreams = {};
            var allStreamsStudents = {};
            var publisher;

            var organizerCount = 0;
            var publisherCount = 0;
            var subscriberCount = 0;
            var connectionCount = 0;

            var totalSubscribers = 0;
            var totalSubscribersStudents = 0;
            var screenShared = 0;

            var msgHistory = document.querySelector('#history');

            // Text chat
            var form = document.querySelector('form');
            var msgTxt = document.querySelector('#msgTxt');
            var username = document.querySelector('#username');

            function openNav() {
                document.getElementById("mySidepanel1").style.setProperty("width", "20%", "important");
            }

            function closeNav() {
                document.getElementById("mySidepanel1").style.setProperty("width", "0", "important");
                document.getElementById("videos").style.setProperty("width", "100%", "important");
            }

            function expandChat() {
                document.getElementById("chat-box").style.setProperty("height", "95%", "important");
                document.getElementById("chat-box").style.display = "block";
                document.getElementById("chatBoxOut").style.display = "none";
                document.getElementById("chatBoxIn").style.display = "block";
            }

            function collapsChat() {
                document.getElementById("chat-box").style.setProperty("height", "54%", "important");
                document.getElementById("chat-box").style.display = "block";
                document.getElementById("chatBoxIn").style.display = "none";
                document.getElementById("chatBoxOut").style.display = "block";
            }

            $(document).ready(function () {
                $(".slide-toggle").click(function(){
                    $(".box").animate({
                        width: "toggle"
                    });
                });
            });

            // Handling all of our errors here by alerting them
            function handleError(error) {
                if (error) {
                    //alert(error.message);
                }
            }

            function initializeSession() {
                session = OT.initSession(apiKey, sessionId);

                // Create a publisher
                publisher = OT.initPublisher('publisher', {
                    insertMode: 'append',
                    width: '220px',
                    height: '140px',
                    name: '<?=$username?>',
                    publishAudio:true,
                    publishVideo:true,
                    style: {buttonDisplayMode: 'on', nameDisplayMode: "on"}
                }, handleError);

                // Connect to the session
                session.connect(token, function(error, event) {
                    // If the connection is successful, publish to the session
                    if (error) {
                        handleError(error);
                    } else {
                        session.publish(publisher, handleError);
                    }
                });

                session.on('signal:msg', function signalCallback(event) {
                    if(event.from.connectionId === session.connection.connectionId){
                        document.getElementById("chat-box").style.display="block";
                        var msg = document.createElement('p');
                        var splitData = event.data.split('|');
                        //msg.textContent = event.data;
                        msg.textContent = splitData[0];
                        msg.setAttribute("id", splitData[1]);
                        msg.className = 'mine';
                        msgHistory.appendChild(msg);
                        msg.scrollIntoView();
                    }
                    else if(event.from.permissions.publish || session.connection.permissions.publish){
                        document.getElementById("chat-box").style.display="block";
                        var msg = document.createElement('p');
                        var splitData = event.data.split('|');
                        //msg.textContent = event.data;
                        msg.textContent = splitData[0];
                        msg.setAttribute("id", splitData[1]);
                        msg.className = 'theirs';
                        msgHistory.appendChild(msg);
                        msg.scrollIntoView();
                    }
                    else{
                        document.getElementById("chat-box").style.display="block";
                        var msg = document.createElement('p');
                        var splitData = event.data.split('|');
                        //msg.textContent = event.data;
                        msg.textContent = splitData[0];
                        msg.setAttribute("id", splitData[1]);
                        msg.className = 'theirs';
                        msgHistory.appendChild(msg);
                        msg.scrollIntoView();
                    }
                });

                session.on('signal:del-msg', function signalCallback(event) {
                    $("#" + event.data).remove();
                });

                session.on('signal:end-session', function signalCallback(event) {
                    window.location.href = "/admin/meeting/seminar/index/" + base64Username;
                });

                session.on({
                    connectionCreated: function (event) {
                        // TODO connection just created
                    },
                    connectionDestroyed: function connectionDestroyedHandler(event) {
                        // TODO connection is destroyed
                    },
                    streamDestroyed: function (event){
                        if(event.stream.videoType == 'camera'){
                            totalSubscribers--;

                            var gridWidth = '100%',
                                gridHeight = '100%',
                                gridItems = totalSubscribers,
                                grid_str = '',
                                $grid = $('#subscriber'),
                                i;

                            function dimensions(x) {
                                let quotient = parseInt(x / 4);
                                let remainder = x % 4;

                                var w = 25;
                                var l = 40;
                                var divW = 96;
                                var t = 30;
                                var h = 30;

                                var screenDivisor = totalSubscribers;
                                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ){
                                    if(screenDivisor){
                                        document.getElementById("publisher").style.left = "35%";
                                        w = 100;
                                        l = 0;
                                        divW = 100;
                                        t = 3;
                                        h = 25;
                                    }
                                    else{
                                        document.getElementById("publisher").style.left = "35%";
                                    }
                                }
                                else{
                                    switch(screenDivisor){
                                        case 1:
                                            w = 80;
                                            h = 80;
                                            l = 4;
                                            t = 1;
                                            break;
                                        case 2:
                                            w = 80;
                                            l = 7;
                                            divW = 45;
                                            t = 10;
                                            h = 40;
                                            break;
                                        case 3:
                                        case 4:
                                            w = 80;
                                            l = 7;
                                            divW = 45;
                                            t = 2;
                                            h = 39;
                                            break;
                                        case 5:
                                        case 6:
                                            w = 80;
                                            l = 7;
                                            divW = 30;
                                            t = 7;
                                            h = 30;
                                            break;
                                        case 7:
                                        case 8:
                                            w = 98;
                                            l = 5;
                                            divW = 23;
                                            t = 20;
                                            break;
                                        case 9:
                                        case 10:
                                        case 11:
                                        case 12:
                                            w = 98;
                                            l = 5;
                                            divW = 23;
                                            t = 3;
                                            //console.log("I am in 9");
                                            break;
                                    }
                                }

                                let attrb = "width: " + w + "%; height: " + h + "%; position: relative; left: " + l + "%; top: " + t + "%;";
                                document.getElementById("subscriber").setAttribute("style", attrb);

                                $('#subscriber .OT_root').css({
                                    width: divW + '%',
                                    height: '100%',
                                    marginRight: '10px'
                                });
                            };

                            //$grid.width(gridWidth).height(gridHeight);

                            let subscriberId = allStreams[event.stream.id];

                            //console.log(allStreams);
                            document.getElementById(subscriberId).style.display = 'none';

                            if(!screenShared)
                            {
                                dimensions(totalSubscribers);
                            }

                            if(screenShared)
                            {
                                var w = 12;
                                var divW = 100;
                                var h = 14;
                                var l = 0;
                                var t = 20;
                                var divH = 100;
                                var position = 'absolute';
                                var divL = 0;
                                var overFlow = 'auto';

                                var mobile = false;

                                var screenDivisor = totalSubscribers;
                                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ){
                                    if(screenDivisor){
                                        document.getElementById("publisher").style.left = "3%";
                                        w = 74;
                                        l = 10;
                                        divW = 20;
                                        t = 0;
                                        h = 96;
                                        divH = 7;
                                        position = 'absolute';
                                        divL = 25;
                                        overFlow = 'none';

                                        let screen_attrb = "width: 80%; height: 30%; margin-top: 40%; position: relative; float: none; left: 0";
                                        document.getElementById("screen-preview").setAttribute("style", screen_attrb);
                                        mobile = true;
                                    }
                                    else{
                                        document.getElementById("publisher").style.left = "35%";
                                    }
                                }
                                else{
                                    document.getElementById("publisher").style.left = "0%";
                                    document.getElementById("publisher").style.width = "0%";
                                }

                                let attrb = "width: " + w + "%; height: " + h + "%; position: " + position + "; left: " + l + "%; top: " + t + "%; overflow-y:" + overFlow + ";";

                                document.getElementById("screen-preview").style.display = "block";
                                document.getElementById("subscriber").setAttribute("style", attrb);

                                if(mobile){
                                    $('#subscriber .OT_root').css({
                                        width: '220px',
                                        height: '140px',
                                        marginRight: '10px',
                                        left: divL + '%'
                                    });
                                }
                                else{
                                    $('#subscriber .OT_root').css({
                                        width: divW + '%',
                                        height: divH + '%',
                                        marginRight: '10px',
                                        left: divL + '%'
                                    });
                                }
                            }
                        }
                        if(event.stream.videoType == 'screen'){
                            screenShared = 0;
                            document.getElementById("screen-preview").style.display = "none";

                            var w = 25;
                            var l = 40;
                            var divW = 96;
                            var t = 30;
                            var h = 30;

                            var screenDivisor = totalSubscribers;
                            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ){
                                if(screenDivisor){
                                    document.getElementById("publisher").style.left = "35%";
                                    w = 100;
                                    l = 0;
                                    divW = 100;
                                    t = 3;
                                    h = 25;
                                }
                                else{
                                    document.getElementById("publisher").style.left = "35%";
                                }
                            }
                            else{
                                switch(screenDivisor){
                                    case 1:
                                        w = 80;
                                        h = 80;
                                        l = 4;
                                        t = 1;
                                        break;
                                    case 2:
                                        w = 80;
                                        l = 7;
                                        divW = 45;
                                        t = 10;
                                        h = 40;
                                        break;
                                    case 3:
                                    case 4:
                                        w = 80;
                                        l = 7;
                                        divW = 45;
                                        t = 2;
                                        h = 39;
                                        break;
                                    case 5:
                                    case 6:
                                        w = 80;
                                        l = 7;
                                        divW = 30;
                                        t = 7;
                                        h = 30;
                                        break;
                                    case 7:
                                    case 8:
                                        w = 98;
                                        l = 5;
                                        divW = 23;
                                        t = 20;
                                        break;
                                    case 9:
                                    case 10:
                                    case 11:
                                    case 12:
                                        w = 98;
                                        l = 5;
                                        divW = 23;
                                        t = 3;
                                        //console.log("I am in 9");
                                        break;
                                }
                            }
                            document.getElementById("publisher").style.left = "35%";
                            document.getElementById("publisher").style.width = "100%";

                            let attrb = "width: " + w + "%; height: " + h + "%; position: relative; left: " + l + "%; top: " + t + "%;";
                            document.getElementById("subscriber").setAttribute("style", attrb);

                            $('#subscriber .OT_root').css({
                                width: divW + '%',
                                height: '100%',
                                marginRight: '10px',
                                left: 0
                            });
                        }
                    },
                    streamCreated: function(event){
                        if(event.stream.videoType == 'camera'){
                            var subscriber = session.subscribe(event.stream, 'subscriber', {
                                insertMode: 'append',
                                width: '100%',
                                height: '100%',
                                name: event.stream.name,
                            }, handleError);

                            //console.log(allStreams);
                            totalSubscribers++;
                            allStreams[event.stream.id] = subscriber.id;

                            var gridWidth = '100%',
                                gridHeight = '100%',
                                gridItems = totalSubscribers,
                                grid_str = '',
                                $grid = $('#subscriber'),
                                i;

                            function dimensions(x) {
                                let quotient = parseInt(x / 4);
                                let remainder = x % 4;

                                var w = 25;
                                var l = 40;
                                var divW = 96;
                                var t = 30;
                                var h = 30;

                                var screenDivisor = totalSubscribers;
                                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ){
                                    if(screenDivisor){
                                        document.getElementById("publisher").style.left = "35%";
                                        w = 100;
                                        l = 0;
                                        divW = 100;
                                        t = 3;
                                        h = 25;
                                    }
                                    else{
                                        document.getElementById("publisher").style.left = "35%";
                                    }
                                }
                                else{
                                    switch(screenDivisor){
                                        case 1:
                                            w = 80;
                                            h = 80;
                                            l = 4;
                                            t = 1;
                                            break;
                                        case 2:
                                            w = 80;
                                            l = 7;
                                            divW = 45;
                                            t = 10;
                                            h = 40;
                                            break;
                                        case 3:
                                        case 4:
                                            w = 80;
                                            l = 7;
                                            divW = 45;
                                            t = 2;
                                            h = 39;
                                            break;
                                        case 5:
                                        case 6:
                                            w = 80;
                                            l = 7;
                                            divW = 30;
                                            t = 7;
                                            h = 30;
                                            break;
                                        case 7:
                                        case 8:
                                            w = 98;
                                            l = 5;
                                            divW = 23;
                                            t = 20;
                                            break;
                                        case 9:
                                        case 10:
                                        case 11:
                                        case 12:
                                            w = 98;
                                            l = 5;
                                            divW = 23;
                                            t = 3;
                                            //console.log("I am in 9");
                                            break;
                                    }
                                }

                                let attrb = "width: " + w + "%; height: " + h + "%; position: relative; left: " + l + "%; top: " + t + "%;";
                                document.getElementById("subscriber").setAttribute("style", attrb);

                                $('#subscriber .OT_root').css({
                                    width: divW + '%',
                                    height: '100%',
                                    marginRight: '10px'
                                });
                            };

                            if(!screenShared)
                            {
                                document.getElementById(subscriber.id).style.display = 'inline-block';
                                document.getElementById(subscriber.id).style.boxSizing = 'border-box';
                                document.getElementById(subscriber.id).style.border = '2px solid white';
                                document.getElementById(subscriber.id).style.margin = '2px';

                                dimensions(totalSubscribers);
                            }

                            if(screenShared)
                            {
                                var w = 12;
                                var divW = 100;
                                var h = 100;
                                var l = 0;
                                var t = 20;
                                var divH = 20;
                                var position = 'absolute';
                                var divL = 0;
                                var mobile = false;
                                var overFlow = 'auto;'

                                var screenDivisor = totalSubscribers;
                                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ){
                                    if(screenDivisor){
                                        document.getElementById("publisher").style.left = "3%";
                                        w = 74;
                                        l = 10;
                                        divW = 20;
                                        t = 0;
                                        h = 96;
                                        divH = 7;
                                        position = 'absolute';
                                        divL = 25;
                                        overFlow = 'none';

                                        let screen_attrb = "width: 80%; height: 30%; margin-top: 40%; position: relative; float: none; left: 0;";
                                        document.getElementById("screen-preview").setAttribute("style", screen_attrb);

                                        mobile = true;
                                    }
                                    else{
                                        document.getElementById("publisher").style.left = "35%";
                                    }
                                }
                                else{
                                    document.getElementById("publisher").style.left = "0%";
                                    document.getElementById("publisher").style.width = "0%";
                                }

                                let attrb = "width: " + w + "%; height: " + h + "%; position: " + position + "; left: " + l + "%; top: " + t + "%; overflow-y:" + overFlow + ";";

                                document.getElementById("screen-preview").style.display = "block";
                                document.getElementById("subscriber").setAttribute("style", attrb);

                                if(mobile){
                                    $('#subscriber .OT_root').css({
                                        width: '220px',
                                        height: '140px',
                                        marginRight: '10px',
                                        left: divL + '%'
                                    });
                                }
                                else{
                                    $('#subscriber .OT_root').css({
                                        width: divW + '%',
                                        height: divH + '%',
                                        marginRight: '10px',
                                        left: divL + '%'
                                    });
                                }
                            }

                            var SpeakerDetection = function(subscriber, startTalking, stopTalking) {
                                var activity = null;
                                subscriber.on('audioLevelUpdated', function(event) {
                                    var now = Date.now();
                                    if (event.audioLevel > 0.2) {
                                        if (!activity) {
                                            activity = {timestamp: now, talking: false};
                                        }
                                        else if (activity.talking) {
                                            activity.timestamp = now;
                                        }
                                        else if (now- activity.timestamp > 500) {
                                            activity.talking = true;
                                            if (typeof(startTalking) === 'function') {
                                                startTalking();
                                            }
                                        }
                                    }
                                    else if (activity && now - activity.timestamp > 3000) {
                                        // detected low audio activity for more than 3s
                                        if (activity.talking) {
                                            if (typeof(stopTalking) === 'function') {
                                                stopTalking();
                                            }
                                        }
                                        activity = null;
                                    }
                                });
                            };

                            SpeakerDetection(subscriber, function() {
                                //console.log('Started Talking ', subscriber.id, subscriber);
                                //document.getElementById(subscriber.id).style.overflow = "inherit";
                                document.getElementById(subscriber.id).style.setProperty("overflow", "inherit", "important");

                                var x = document.getElementById(subscriber.id).querySelectorAll(".OT_name");
                                x[0].style.display = "block";
                                x[0].style.opacity = "1";
                                x[0].style.top = "0";


                                var y = document.getElementById(subscriber.id).querySelectorAll(".OT_widget-container");
                                y[0].style.display = "block";
                                y[0].style.setProperty("border", "3px solid #00ca00", "important");



                            }, function() {
                                document.getElementById(subscriber.id).style.background = "";
                                var x = document.getElementById(subscriber.id).querySelectorAll(".OT_name");
                                x[0].style.display = "none";
                                x[0].style.opacity = "0";
                                x[0].style.top = "-25px";

                                var y = document.getElementById(subscriber.id).querySelectorAll(".OT_widget-container");
                                y[0].style.display = "block";
                                y[0].style.setProperty("border", "none", "important");

                                document.getElementById(subscriber.id).style.setProperty("overflow", "hidden", "important");
                            });
                        }
                        if(event.stream.videoType == 'screen'){
                            screenShared = 1;

                            var w = 12;
                            var divW = 100;
                            var h = 100;
                            var l = 0;
                            var t = 20;
                            var divH = 20;
                            var position = 'absolute';
                            var divL = 0;
                            var mobile = false;
                            var overFlow = 'auto';

                            var screenDivisor = totalSubscribers;
                            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ){
                                if(screenDivisor){
                                    document.getElementById("publisher").style.left = "3%";
                                    w = 74;
                                    l = 10;
                                    divW = 20;
                                    t = 0;
                                    h = 96;
                                    divH = 7;
                                    position = 'absolute';
                                    divL = 25;
                                    overFlow = 'none';

                                    let screen_attrb = "width: 80%; height: 30%; margin-top: 40%; position: relative; float: none; left: 0;";
                                    document.getElementById("screen-preview").setAttribute("style", screen_attrb);

                                    mobile = true;
                                }
                                else{
                                    document.getElementById("publisher").style.left = "35%";
                                }
                            }
                            else{
                                document.getElementById("publisher").style.left = "0%";
                                document.getElementById("publisher").style.width = "0%";
                            }

                            let attrb = "width: " + w + "%; height: " + h + "%; position: " + position + "; left: " + l + "%; top: " + t + "%; overflow-y:" + overFlow + ";";

                            document.getElementById("screen-preview").style.display = "block";
                            document.getElementById("subscriber").setAttribute("style", attrb);

                            if(mobile){
                                $('#subscriber .OT_root').css({
                                    width: '220px',
                                    height: '140px',
                                    marginRight: '10px',
                                    left: divL + '%'
                                });
                            }
                            else{
                                $('#subscriber .OT_root').css({
                                    width: divW + '%',
                                    height: divH + '%',
                                    marginRight: '10px',
                                    left: divL + '%'
                                });
                            }
                            session.subscribe(event.stream, 'screen-preview', {
                                insertMode: 'append',
                                width: '100%',
                                height: '100%',
                                name: '<?=$username?>'
                            }, handleError);
                        }
                        //console.log("Stream Created ", event);
                    }
                });

                function resizeSubscriber() {
                    publisher.element.style.width = "100px";
                    publisher.element.style.height = "75px";
                    document.getElementById("subscriber").appendChild(publisher.element);
                }
            }

            // Send a signal once the user enters data in the form
            form.addEventListener('submit', function submit(event) {
                var senderRole = session.connection.permissions.publish ? 'PUBLISHER' : 'SUBSCRIBER';
                event.preventDefault();
                var currentTime = new Date().getTime();
                var chatId = 'chat-' + currentTime;
                $.ajax({
                    url: '/meeting/loanmela/chat',
                    type: 'POST',
                    contentType: 'application/json', // send as JSON
                    data: JSON.stringify({'user_id': '<?=$userId?>', 'sessionId': sessionId, 'tokboxId': tokboxId, 'to': 'All', 'msg': msgTxt.value, 'from_role': senderRole, 'chat_id': chatId}),
                    complete: function complete() {
                        // called when complete
                        //console.log('chat function completed');
                    },
                    success: function success() {
                        // called when successful
                        //console.log('successfully called chat');
                    },
                    error: function error() {
                        // called when there is an error
                        //console.log('error calling chat');
                    }
                });
                session.signal({
                    type: 'msg',
                    data: username.value + ': ' + msgTxt.value + '|' + chatId
                }, function signalCallback(error) {
                    if (error) {
                        //console.error('Error sending signal:', error.name, error.message);
                    } else {
                        msgTxt.value = '';
                    }
                });
            });

            initializeSession();

            function leaveMeeting(){
                window.location.href = '/meeting/loanmela/thankyou';
            }
        </script>
    </body>
</html>
