<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Visa Seminar Duplicate Link Page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

        <style type="text/css">
            .imageRow{
                /*background-image: url(/images/attendee_webinar_1.jpeg);*/
                height: 75%;
                background-position: center center;
                background-repeat: no-repeat;
                background-size: contain;
                box-shadow: inset 0 0 0 2000px rgba(4, 4, 4, 0.54);
            }

            .weRow{
                font-size: 25px;
                font-weight: lighter;
                letter-spacing: 1px;
                font-family: inherit;
                margin-bottom: 5px;
            }

            .wht{
                color: #ffffff !important;
            }

            .txtAlignCentre{
                text-align: center;
            }
        </style>
    </head>

    <body>
        <div class="container" style="margin: 8% auto; width: 95%;">
            <div class="col-md-12 imageRow">
                <div class="col-lg-12 " >
                    <div class="col-md-12" style="padding: 14% 0px;">
                        <div class="col-md-12">
                            <div class="row txtAlignCentre ">
                                <h5 class="text-center wht weRow"><?=$errorMessage?></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<?php
exit;
?>
