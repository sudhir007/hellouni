<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
<style>
   div {
       background: 0 0;
       border: 0;
       margin: 0;
       outline: 0;
       padding: 0;
       vertical-align: baseline;
   }
   .bannerSection{
       background-color: rgba(0, 0, 0, 0);
       background-repeat: no-repeat;
       background-image: url(<?php echo base_url();?>application/images/v_fair_29_2024/fair_joining_page_banner.webp);
       background-size: cover;
       background-position: center center;
       width: 100%;
   }
   .sectionClass {
       padding: 20px 0px 50px 0px;
       position: relative;
       display: block;
   }
   .projectFactsWrap{
       display: flex;
       margin-top: 30px;
       flex-direction: row;
       flex-wrap: wrap;
   }
   .projectFactsWrap .item{
       width: 20%;
       height: 100%;
       padding: 25px 5px;
       text-align: center;
       border-radius: 25%;
       margin: 20px;
       margin-top: -150px;
   }
   .projectFactsWrap .item:nth-child(1){
       background: rgb(246 136 44);
   }
   .projectFactsWrap .item:nth-child(2){
       background: rgb(246 136 44);
   }
   .projectFactsWrap .item:nth-child(3){
       background: rgb(246 136 44);
   }
   .projectFactsWrap .item:nth-child(4){
       background: rgb(246 136 44);
   }
   .projectFactsWrap .item p.number{
       font-size: 25px;
       padding: 0;
       font-weight: bold;
   }
   .projectFactsWrap .item p{
       color: black;
       font-size: 16px;
       margin: 0;
       padding: 10px;
       font-family: 'Open Sans';
       font-weight: 600;
   }
   .projectFactsWrap .item span{
       width: 60px;
       background: rgba(255, 255, 255, 0.8);
       height: 2px;
       display: block;
       margin: 0 auto;
   }
   .projectFactsWrap .item i{
       vertical-align: middle;
       font-size: 50px;
       color: rgba(255, 255, 255, 0.8);
   }
   .projectFactsWrap .item:hover i, .projectFactsWrap .item:hover p{
       color: white;
   }
   .projectFactsWrap .item:hover span{
       background: white;
   }
   @media (max-width: 786px){
       .projectFactsWrap .item {
           flex: 0 0 50%;
       }
   }
   h1:after{
       content: "";
       position: absolute;
       left: 40%;
       margin-left: 0;
       bottom: 0;
       width: 20%;
       border-bottom: 4px solid white;
   }
   h1:before {
       content: "";
       position: absolute;
       left: 40%;
       margin-left: 0;
       top: 0;
       width: 20%;
       border-bottom: 4px solid white;
   }
   .inset {
       padding: 15px;
    }
   @media screen and (max-width: 992px){
       .inset {
           padding: 15px;
        }
   }
   .team-style-five .team-items .item {
       -moz-box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
       -webkit-box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
       -o-box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
       box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
       padding-top: 0px;
       background-color: #62367d;
       margin: 10px;
       height: 115px;
       border-radius: 10px;
   }
   .team-style-five .single-item.team-standard .thumb {
       overflow: hidden;
   }
   .team-style-five .team-items .item .thumb img{
       width: 100%;
       height: 28%;
   }
   .team-style-five .team-items .item .thumb {
       position: relative;
       z-index: 1;
       height: 115px;
   }
   .team-style-five .team-items .item .info {
       background: #ffffff none repeat scroll 0 0;
       padding: 20px 20px;
   }
   .team-style-five .team-items .item .info h3 {
       font-weight: 700;
       margin-bottom: 5px;
       font-size: 18px;
       font-family:"montserrat", sans-serif;
       text-transform: uppercase;
   }
   .team-style-five .single-item.team-standard .item .info span {
       font-size: 13px;
       color: #1cb9c8;
   }
   .team-style-five .single-item.team-standard .item .info .spanName::after {
       background: #1cb9c8 none repeat scroll 0 0;
       bottom: 0;
       content: "";
       height: 2px;
       left: 50%;
       margin-left: -20px;
       position: absolute;
       width: 40px;
   }
   .team-style-five .team-items .item .info p {
       margin-bottom: 0;
       color: #666666;
       text-align: left;
       font-size: 14px;
   }
   @import url("https://fonts.googleapis.com/css?family=Lato:400,400i,700");
   .joinbtn{
       background: #f6872c !important;
       border-color: #f6872c !important;
       border-radius: 5px;
       display: inline;
   }
   .joinbtn-queue{
       background: #f6872c !important;
       border-color: #f6872c !important;
       border-radius: 5px;
       display: none;
   }
   @media only screen and (min-width: 1700px) {
       .projectFactsWrap .item{
           width: 17%;
           margin-left: 6%;
       }
   }

   .pulse {
    display: block;
    border-radius: 8px;
    background: #f6882c;
    cursor: pointer;
    transform: scale(1);
    box-shadow: 0 0 0 rgb(246 136 44);
    animation: pulse 1.5s infinite;
}
    .pulse:hover {
      animation: none;
    }

    @-webkit-keyframes pulse {
      0% {
        transform: scale(0.95);
        -webkit-box-shadow: 0 0 0 0 rgba(98,54,125, 0.4);
      }
      70% {
        transform: scale(1);
          -webkit-box-shadow: 0 0 0 10px rgba(98,54,125, 1);
      }
      100% {
        transform: scale(0.95);
          -webkit-box-shadow: 0 0 0 0 rgba(98,54,125, 0);
      }
    }
    @keyframes pulse {
      0% {
        transform: scale(0.95);
        -moz-box-shadow: 0 0 0 0 rgba(98,54,125, 0.4);
        box-shadow: 0 0 0 0 rgba(98,54,125, 0.4);
      }
      70% {
        transform: scale(1);
          -moz-box-shadow: 0 0 0 10px rgba(98,54,125, 0);
          box-shadow: 0 0 0 10px rgba(98,54,125, 0);
      }
      100% {
        transform: scale(0.95);
          -moz-box-shadow: 0 0 0 0 rgba(98,54,125, 0);
          box-shadow: 0 0 0 0 rgba(98,54,125, 0);
      }
    }

    .modal{
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        z-index: 112;
    }

    .modal-content{
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 50%;
    }

    #modal-iframe-content{
        width: 70%;
        height: 80%;
    }

    #queue-message {
      position: fixed;
      background-color: #F6881F;
      top: 35%;
      z-index: 5;
      width: 200px;
    }
</style>

<section class="bannerSection">
    <div class="container" style="height: 562px;">
        <?php
        /*
        <div class="row">
            <div class="col-md-12" style="margin-top: 80px;">
                <h1 class="page-top-title" style="color: #ffffff; text-align:center;font-weight:bold;font-size:45px;margin-top: 25px;"> EduLoans Loan Mela 2022 </h1>
                <!--h1 class="page-top-title" style="color: #ffffff; text-align:center;font-weight:bold;font-size:45px;margin-bottom: 25px;">Decision Day 2021</h1-->
            </div>
        </div>
        */
        ?>
    </div>
</section>

<div id="queue-message" style="text-align: center; font-size:18px; font-weight: bold; font-family: monospace; margin-top: 10px;"></div>

<div class="content-wrapper">
    <section>
        <div class="container-fluid">
            <div class="row scrollSpyArea" >
                <?php
                /*
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="page-content inset" data-spy="scroll" data-target="#spy">
                        <div class="row team-style-five">
                            <div class="jumbotron text-center row " style="margin-bottom: 15px;">
                                <h2 style="font-size: 30px">FIs Rooms</h2>

                                <!--span style="top: 20px; position: absolute; right: 10px; font-size: 16px; font-weight: bold;" id="slot-time">Slot: <?=$startTime . " - " . $endTime?></span-->


                                <div>
                                    <!--div class="col-md-4 col-sm-4 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h4 style="color: white;">COMPUTER SCIENCE & ENGINEERING</h4>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinComputerRoom()">Join</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div-->
                                    <?php
                                    foreach($allRooms as $index => $room)
                                    {
                                        //if(in_array($room['tokbox_token_id'], [493, 494, 495]))
                                        //{
                                        //    continue;
                                        //}
                                        ?>
                                        <div class="col-md-4 col-sm-4 team-items">
                                            <div class="single-item text-center team-standard team-style-block">
                                                <div class="item">
                                                    <div class="thumb">
                                                        <h4 style="color: white;"><?=$room['designation']?></h4>
                                                        <div class="col-md-12">
                                                            <?php
                                                            if($roomStatus[$index]['status'])
                                                            {
                                                                ?>
                                                                <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('<?=$roomStatus[$index]['tokboxId']?>')">Join</button>
                                                                <?php
                                                            }
                                                            else
                                                            {
                                                                ?>
                                                                <button class="btn btn-md btn-info joinbtn" onclick="joinQueue('<?=$roomStatus[$index]['tokboxId']?>')">Join Queue</button>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                */
                ?>
                <!-- <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="page-content inset" data-spy="scroll" data-target="#spy">
                        <div class="row team-style-five">
                            <div class="jumbotron text-center row " style="margin-bottom: 15px;">
                                <h2 style="font-size: 30px">Pre-Counselling Rooms</h2>
                                <div>
                                    <div class="col-md-12 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h5 style="color: white;"> Room</h5>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinPreCounselling()" id="button-join-room-1">Join</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->

                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="page-content inset" data-spy="scroll" data-target="#spy">
                        <div class="row team-style-five">
                            <div class="jumbotron text-center row " style="margin-bottom: 15px;">
                                <h2 style="font-size: 30px">University Rooms</h2>
                                <div>
                                    <?php
                                    foreach($allRooms as $index => $room)
                                    {
                                        ?>
                                        <div class="col-md-4 col-sm-4 team-items">
                                            <div class="single-item text-center team-standard team-style-block">
                                                <div class="item">
                                                    <div class="thumb">
                                                        <h4 style="color: white;"><?=$room['designation']?></h4>
                                                        <div class="col-md-12">
                                                            <?php
                                                            if($roomStatus[$index]['status'])
                                                            {
                                                                ?>
                                                                <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('<?=$roomStatus[$index]['tokboxId']?>')">Join</button>
                                                                <?php
                                                            }
                                                            else
                                                            {
                                                                ?>
                                                                <button class="btn btn-md btn-info joinbtn" onclick="joinQueue('<?=$roomStatus[$index]['tokboxId']?>')">Join Queue</button>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="page-content inset" data-spy="scroll" data-target="#spy">
                        <div class="row team-style-five">
                            <div class="jumbotron text-center row " style="margin-bottom: 15px;">
                                <h2 style="font-size: 30px">Eduloan Rooms</h2>
                                <div>
                                    <div class="col-md-12 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h5 style="color: white;"> Room</h5>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinEduloanRoom()" >Join</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->

                 <!-- <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="page-content inset" data-spy="scroll" data-target="#spy">
                        <div class="row team-style-five">
                            <div class="jumbotron text-center row " style="margin-bottom: 15px;">
                                <h2 style="font-size: 30px">Post Counselling Rooms</h2>
                                <div>
                                    <div class="col-md-12 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h5 style="color: white;"> Room</h5>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinPostCounselling()" >Join</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->

                <!--div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="page-content inset" data-spy="scroll" data-target="#spy">
                        <div class="row team-style-five">
                            <div class="jumbotron text-center row " style="margin-bottom: 15px;">
                                <h2 style="font-size: 30px">Sponsors Rooms</h2>
                                <div>
                                    <div class="col-md-6 col-sm-6 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h5 style="color: white;"> HDFC Credila</h5>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinHDFCRoom()" >Join</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h5 style="color: white;"> FRR FOREX</h5>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinForexRoom()" >Join</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div-->
            </div>
        </div>
    </section>
</div>

<div id="modal-alert" class="modal" >
    <!-- Modal content -->
    <div class="row modal-content">
        <div>
            <div style="font-size: 16px; text-align:center;" id="modal-alert-text"></div>
            <!--button id="yes-button" class="btn btn-md btn-primary" style="float:left;" onclick="cancelQueueRoom()">Cancel</button>
            <button id="exit-button" class="btn btn-md btn-primary" style="float:right;" onclick="joinQueueRoom()">Join</button-->
            <button class="btn btn-md btn-primary" style="float:center;" onclick="closeAlertModal()">Close</button>
        </div>
    </div>
</div>

<div id="modal-queue" class="modal" >
    <!-- Modal content -->
    <div class="row modal-content">
        <div>
            <div style="font-size: 16px; text-align:center;" id="modal-queue-text"></div>
            <!--button id="yes-button" class="btn btn-md btn-primary" style="float:left;" onclick="cancelQueueRoom()">Cancel</button>
            <button id="exit-button" class="btn btn-md btn-primary" style="float:right;" onclick="joinQueueRoom()">Join</button-->
            <button class="btn btn-md btn-primary" style="float:center;" onclick="closeQueueModal()">Close</button>
        </div>
    </div>
</div>

<div id="modal-join-room" class="modal" >
    <!-- Modal content -->
    <div class="row modal-content">
        <div>
            <div style="font-size: 16px; text-align:center;">Join the room within <span id="join-timer"></span> Sec</div>
            <button id="exit-button" class="btn btn-md btn-primary" style="float:right;" onclick="joinRoom('MjEx')">Join</button>
        </div>
    </div>
</div>

<div id="modal-missed-room" class="modal" >
    <!-- Modal content -->
    <div class="row modal-content">
        <div>
            <div style="font-size: 16px; text-align:center;">Sorry!! You missed the room. Please join the queue again.</div>
            <button id="exit-button" class="btn btn-md btn-primary" style="float:right;" onclick="closeMissedRoom()">Close</button>
        </div>
    </div>
</div>

<div id="modal-iframe" class="modal" >
    <!-- Modal content -->
    <div class="row modal-content" id="modal-iframe-content">
        <div style="width:100%; height:100%;">
            <button id="close-iframe" class="btn btn-md btn-primary" style="float:left;" onclick="closeIframe()">Close</button>
            <iframe id="forPostyouradd" class="responsive-iframe embed-responsive-item" data-src="" src="about:blank" style="height: 100%; width: 100%; background:#ffffff; display:none" allowfullscreen></iframe>
        </div>
    </div>
</div>

<script type="text/javascript">
    var modalQueueText = '<?=$msg?>';
    var queueRoomId = '';
    var queueTokenId = '';

    if(modalQueueText.length){
        document.getElementById("modal-queue-text").innerHTML = modalQueueText;
        document.getElementById("modal-queue").style.display = 'block';
    }

    document.getElementById("modal-alert-text").innerHTML = "Thank You For Joining Virtual Fair...!!!";
    document.getElementById("modal-alert").style.display = 'none';

    function joinRoom(tokboxId){
        window.location.href = "/event/student/" + tokboxId + "/<?=$username?>/<?=$linkCount?>";
        /*let src = "/event/student/" + tokboxId + "/<?=$username?>/<?=$linkCount?>";

        var iframe  = document.getElementById("forPostyouradd");
        iframe.setAttribute('src', src);
        iframe.style.display = 'block';

        document.getElementById("modal-iframe").style.display = "block";*/
    }

    function joinQueueRoom(){
        $.ajax({
            url: '/event/join_queue/' + queueTokenId + '/<?=$username?>/1',
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                data = JSON.parse(data);
                if(!parseInt(data['success'])){
                    document.getElementById("modal-queue-text").innerHTML = data['msg'];
                    document.getElementById("modal-queue").style.display = 'block';

                    queueTokenId = tokenId;

                    //alert(data['msg']);
                    //window.location.href = "/event/rooms/<?=$username?>";

                    //document.getElementById("slot-time").innerHTML = 'Slot: ' + data['startTime'] + ' - ' + data['endTime'];
                }
                else{
                    document.getElementById("modal-queue").style.display = 'none';
                    alert(data['msg']);
                    //alert("Some issue occured while creating slot. Please try again or contact to our support");
                }
            },
            error: function error() {
            }
        });
        //window.location.href = "/event/student/" + queueRoomId + "/<?=$username?>/<?=$linkCount?>";
        /*let src = "/event/student/" + tokboxId + "/<?=$username?>/<?=$linkCount?>";

        var iframe  = document.getElementById("forPostyouradd");
        iframe.setAttribute('src', src);
        iframe.style.display = 'block';

        document.getElementById("modal-iframe").style.display = "block";*/
    }

    function cancelQueueRoom(){
        $.ajax({
            url: '/event/delete_queue/<?=$username?>',
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                var queueModal = document.getElementById("modal-queue");
                queueModal.style.display = "none";
                //window.location.href = "/event/rooms/<?=$username?>";
            },
            error: function error() {
            }
        });
    }

    function closeQueueModal(){
        queueModal = document.getElementById("modal-queue");
        queueModal.style.display = "none";
    }

    function closeAlertModal(){
        alertModal = document.getElementById("modal-alert");
        alertModal.style.display = "none";
    }

    function joinQueue(tokenId){
        $.ajax({
            url: '/event/join_queue/' + tokenId + '/<?=$username?>',
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                data = JSON.parse(data);
                if(!parseInt(data['success'])){
                    document.getElementById("modal-queue-text").innerHTML = data['msg'];
                    document.getElementById("modal-queue").style.display = 'block';

                    queueTokenId = tokenId;

                    //alert(data['msg']);
                    //window.location.href = "/event/rooms/<?=$username?>";

                    //document.getElementById("slot-time").innerHTML = 'Slot: ' + data['startTime'] + ' - ' + data['endTime'];
                }
                else{
                    document.getElementById("modal-queue").style.display = 'none';
                    alert(data['msg']);
                    //alert("Some issue occured while creating slot. Please try again or contact to our support");
                }
            },
            error: function error() {
            }
        });
    }

    function joinPostCounselling(){
        $.ajax({
            url: '/event/check_available_room/<?=$username?>/POST',
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                if(data){
                    window.location.href = '/event/student/' + data + '/<?=$username?>';
                }
                else{
                    alert("All rooms are full. Please join any other room or try after sometime");
                }
            },
            error: function error() {
            }
        });
    }

    function joinPreCounselling(){
        $.ajax({
            url: '/event/check_available_room/<?=$username?>/PRE',
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                if(data){
                    window.location.href = '/event/student/' + data + '/<?=$username?>';
                }
                else{
                    alert("All rooms are full. Please join any other room or try after sometime");
                }
            },
            error: function error() {
            }
        });
    }

    function joinComputerRoom(){
        $.ajax({
            url: '/event/check_available_room/<?=$username?>/COMPUTER',
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                if(data){
                    window.location.href = '/event/student/' + data + '/<?=$username?>';
                }
                else{
                    alert("All rooms are full. Please join any other room or try after sometime");
                }
            },
            error: function error() {
            }
        });
    }

    function joinImperialRoom(){
        $.ajax({
            url: '/event/check_available_room/<?=$username?>/IMPERIAL',
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                if(data && data != 0 && data != '0'){
                    window.location.href = '/event/student/' + data + '/<?=$username?>';
                }
                else{
                    alert("All rooms are full. Please wait for sometime, you will get notification once room is available.");
                }
            },
            error: function error() {
            }
        });
    }

    function joinEduloanRoom(){
        $.ajax({
            url: '/event/check_available_room/<?=$username?>/EDULOAN',
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                if(data && data != 0 && data != '0'){
                    window.location.href = '/event/student/' + data + '/<?=$username?>';
                }
                else{
                    alert("All rooms are full. Please wait for sometime, you will get notification once room is available.");
                }
            },
            error: function error() {
            }
        });
    }

    function joinHDFCRoom(){
        $.ajax({
            url: '/event/check_available_room/<?=$username?>/HDFC',
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                if(data && data != 0 && data != '0'){
                    window.location.href = '/event/student/' + data + '/<?=$username?>';
                }
                else{
                    alert("All rooms are full. Please wait for sometime, you will get notification once room is available.");
                }
            },
            error: function error() {
            }
        });
    }

    function joinForexRoom(){
        $.ajax({
            url: '/event/check_available_room/<?=$username?>/FOREX',
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                if(data && data != 0 && data != '0'){
                    window.location.href = '/event/student/' + data + '/<?=$username?>';
                }
                else{
                    alert("All rooms are full. Please wait for sometime, you will get notification once room is available.");
                }
            },
            error: function error() {
            }
        });
    }

    function hideQueueModal(){
        var queueModal = document.getElementById("modal-queue");
        queueModal.style.display = "none";
    }

    function exit(){
        delete_cookie('tid');
        window.location.href = '/event/thankyou';
    }

    function closeMissedRoom(){
        var missedRoomModal = document.getElementById("modal-missed-room");
        missedRoomModal.style.display = "none";
    }

    function closeIframe(){
        let iframeModal = document.getElementById("modal-iframe");
        iframeModal.style.display = "none";
    }

    <?php
    if($runScript)
    {
        ?>
        var popupShown = 0;
        var chk_queue = setInterval(function() {
            if(popupShown){
                cancelQueueRoom();
            }
            else{
                $.ajax({
                    url: '/event/check_queue/<?=$username?>',
                    type: 'GET',
                    complete: function complete() {
                    },
                    success: function success(data) {
                        data = JSON.parse(data);
                        if(data['success']){
                            if(!data['join']){
                                //document.getElementById("queue-message").innerHTML = data['msg'];

                                document.getElementById("modal-queue-text").innerHTML = data['msg'];
                                document.getElementById("modal-queue").style.display = 'block';
                            }
                            else{
                                window.location.href = "/event/student/" + data['room_id'] + "/<?=$username?>";
                                /*document.getElementById("queue-message").innerHTML = '';
                                document.getElementById("modal-queue-text").innerHTML = data['msg'];
                                document.getElementById("modal-queue").style.display = 'block';
                                queueRoomId = data['room_id'];
                                popupShown = 1;*/
                            }
                            //document.getElementById("modal-queue").style.display = 'none';


                            /*document.getElementById("button-join-queue-faculty").style.display = 'none';
                            document.getElementById("button-join-faculty").style.display = 'none';
                            document.getElementById("button-join-queue-faculty").style.display = 'none';
                            document.getElementById("button-join-faculty").style.display = 'inline';*/
                        }
                    },
                    error: function error() {
                    }
                });
            }
        }, 30000);

        /*var refresh_page = setInterval(function() {
            window.location.href = "/event/rooms/<?=$username?>";
        }, 300000);*/
        <?php
    }
    ?>
/*
    var empty_room = setInterval(function() {
        $.ajax({
            url: '/event/check_empty_room',
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                if(data && data != 0 && data != '0'){
                    alert(data);
                }
            },
            error: function error() {
            }
        });
    }, 150000);*/
</script>
