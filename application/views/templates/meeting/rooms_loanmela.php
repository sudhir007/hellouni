<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
<style>
   div {
       background: 0 0;
       border: 0;
       margin: 0;
       outline: 0;
       padding: 0;
       vertical-align: baseline;
   }
   .bannerSection{
       background-color: rgba(0, 0, 0, 0);
       background-repeat: no-repeat;
       background-image: url(<?php echo base_url();?>application/images/topBannerVF.png);
       background-size: cover;
       background-position: center center;
       width: 100%;
   }
   .sectionClass {
       padding: 20px 0px 50px 0px;
       position: relative;
       display: block;
   }
   .projectFactsWrap{
       display: flex;
       margin-top: 30px;
       flex-direction: row;
       flex-wrap: wrap;
   }
   .projectFactsWrap .item{
       width: 20%;
       height: 100%;
       padding: 25px 5px;
       text-align: center;
       border-radius: 25%;
       margin: 20px;
       margin-top: -150px;
   }
   .projectFactsWrap .item:nth-child(1){
       background: rgb(246 136 44);
   }
   .projectFactsWrap .item:nth-child(2){
       background: rgb(246 136 44);
   }
   .projectFactsWrap .item:nth-child(3){
       background: rgb(246 136 44);
   }
   .projectFactsWrap .item:nth-child(4){
       background: rgb(246 136 44);
   }
   .projectFactsWrap .item p.number{
       font-size: 25px;
       padding: 0;
       font-weight: bold;
   }
   .projectFactsWrap .item p{
       color: black;
       font-size: 16px;
       margin: 0;
       padding: 10px;
       font-family: 'Open Sans';
       font-weight: 600;
   }
   .projectFactsWrap .item span{
       width: 60px;
       background: rgba(255, 255, 255, 0.8);
       height: 2px;
       display: block;
       margin: 0 auto;
   }
   .projectFactsWrap .item i{
       vertical-align: middle;
       font-size: 50px;
       color: rgba(255, 255, 255, 0.8);
   }
   .projectFactsWrap .item:hover i, .projectFactsWrap .item:hover p{
       color: white;
   }
   .projectFactsWrap .item:hover span{
       background: white;
   }
   @media (max-width: 786px){
       .projectFactsWrap .item {
           flex: 0 0 50%;
       }
   }
   h1:after{
       content: "";
       position: absolute;
       left: 40%;
       margin-left: 0;
       bottom: 0;
       width: 20%;
       border-bottom: 4px solid white;
   }
   h1:before {
       content: "";
       position: absolute;
       left: 40%;
       margin-left: 0;
       top: 0;
       width: 20%;
       border-bottom: 4px solid white;
   }
   .inset {
       padding: 15px;
    }
   @media screen and (max-width: 992px){
       .inset {
           padding: 15px;
        }
   }
   .team-style-five .team-items .item {
       -moz-box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
       -webkit-box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
       -o-box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
       box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
       padding-top: 0px;
       background-color: #62367d;
       margin: 10px;
       height: 115px;
       border-radius: 10px;
   }
   .team-style-five .single-item.team-standard .thumb {
       overflow: hidden;
   }
   .team-style-five .team-items .item .thumb img{
       width: 100%;
       height: 28%;
   }
   .team-style-five .team-items .item .thumb {
       position: relative;
       z-index: 1;
       height: 115px;
   }
   .team-style-five .team-items .item .info {
       background: #ffffff none repeat scroll 0 0;
       padding: 20px 20px;
   }
   .team-style-five .team-items .item .info h3 {
       font-weight: 700;
       margin-bottom: 5px;
       font-size: 18px;
       font-family:"montserrat", sans-serif;
       text-transform: uppercase;
   }
   .team-style-five .single-item.team-standard .item .info span {
       font-size: 13px;
       color: #1cb9c8;
   }
   .team-style-five .single-item.team-standard .item .info .spanName::after {
       background: #1cb9c8 none repeat scroll 0 0;
       bottom: 0;
       content: "";
       height: 2px;
       left: 50%;
       margin-left: -20px;
       position: absolute;
       width: 40px;
   }
   .team-style-five .team-items .item .info p {
       margin-bottom: 0;
       color: #666666;
       text-align: left;
       font-size: 14px;
   }
   @import url("https://fonts.googleapis.com/css?family=Lato:400,400i,700");
   .joinbtn{
       background: #f6872c !important;
       border-color: #f6872c !important;
       border-radius: 5px;
   }
   @media only screen and (min-width: 1700px) {
       .projectFactsWrap .item{
           width: 17%;
           margin-left: 6%;
       }
   }

   .pulse {
    display: block;
    border-radius: 8px;
    background: #f6882c;
    cursor: pointer;
    transform: scale(1);
    box-shadow: 0 0 0 rgb(246 136 44);
    animation: pulse 1.5s infinite;
}
    .pulse:hover {
      animation: none;
    }

    @-webkit-keyframes pulse {
      0% {
        transform: scale(0.95);
        -webkit-box-shadow: 0 0 0 0 rgba(98,54,125, 0.4);
      }
      70% {
        transform: scale(1);
          -webkit-box-shadow: 0 0 0 10px rgba(98,54,125, 1);
      }
      100% {
        transform: scale(0.95);
          -webkit-box-shadow: 0 0 0 0 rgba(98,54,125, 0);
      }
    }
    @keyframes pulse {
      0% {
        transform: scale(0.95);
        -moz-box-shadow: 0 0 0 0 rgba(98,54,125, 0.4);
        box-shadow: 0 0 0 0 rgba(98,54,125, 0.4);
      }
      70% {
        transform: scale(1);
          -moz-box-shadow: 0 0 0 10px rgba(98,54,125, 0);
          box-shadow: 0 0 0 10px rgba(98,54,125, 0);
      }
      100% {
        transform: scale(0.95);
          -moz-box-shadow: 0 0 0 0 rgba(98,54,125, 0);
          box-shadow: 0 0 0 0 rgba(98,54,125, 0);
      }
    }

    .modal{
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        z-index: 112;
    }

    .modal-content{
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 50%;
    }

    .joinbtn-webinar{
        background: #f6872c !important;
        border-color: #f6872c !important;
        border-radius: 5px;
    }
    .joinbtn{
        background: #f6872c !important;
        border-color: #f6872c !important;
        border-radius: 5px;
    }
    .joinbtn-queue{
        background: #f6872c !important;
        border-color: #f6872c !important;
        border-radius: 5px;
        display: none;
    }
</style>

<section class="bannerSection">
    <div class="container" style="height: 400px;">
        <div class="row">
            <div class="col-md-12" style="margin-top: 80px;">
                <h1 class="page-top-title" style="color: #ffffff; text-align:center;font-weight:bold;font-size:45px;margin-top: 25px;">IMPERIAL LOAN</h1>
                <h1 class="page-top-title" style="color: #ffffff; text-align:center;font-weight:bold;font-size:45px;margin-bottom: 25px;">MELA 2021</h1>
            </div>
        </div>
    </div>
</section>

<div class="content-wrapper">
    <?php
    /*
    <section>
        <div class="col-md-12">
            <div class="col-md-12 col-lg-12 col-sm-12 hidden-xs">
                <div id="projectFacts" class="sectionClass">
                    <div class="projectFactsWrap " style="margin-left: 50px">
                        <div class="item wow fadeInUpBig animated animated " data-number="200" style="visibility: visible;">
                            <img src="<?php echo base_url();?>application/images/icons/1.png" width="20%">
                            <p id="total_registrations" class="number"></p>
                            <span></span>
                            <p>Total Registrations</p>
                        </div>
                        <div class="item wow fadeInUpBig animated animated" data-number="55" style="visibility: visible;">
                            <img src="<?php echo base_url();?>application/images/icons/2.png" width="35%">
                            <p id="active_participants" class="number"></p>
                            <span></span>
                            <p>Active Participants</p>
                        </div>
                        <div class="item wow fadeInUpBig animated animated" data-number="359" style="visibility: visible;">
                            <img src="<?php echo base_url();?>application/images/icons/3.png" width="20%">
                            <p id="active_rooms" class="number">10</p>
                            <span></span>
                            <p>Active Rooms</p>
                        </div>
                        <div class="item wow fadeInUpBig animated animated" data-number="246" style="visibility: visible;">
                            <img src="<?php echo base_url();?>application/images/icons/4.png" width="20%">
                            <p id="session_attended" class="number"></p>
                            <span></span>
                            <p>Sessions Attended</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    */
    ?>
    <section>
        <div class="container-fluid">
            <div class="row scrollSpyArea" >
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="page-content inset" data-spy="scroll" data-target="#spy" id="imperial-counselling-box">
                        <div class="row team-style-five" id="imperial-counselling">
                            <div class="jumbotron text-center row " style="margin-bottom: 15px;">
                                <div>
                                    <div class="col-md-12 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> Webinar Session</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn-webinar" onclick="joinWebinar('MTk5')" id="webinar-button">Join</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="page-content inset" data-spy="scroll" data-target="#spy" id="imperial-counselling-box">
                        <div class="row team-style-five" id="imperial-counselling">
                            <div class="jumbotron text-center row " style="margin-bottom: 15px;">
                                <h2 style="font-size: 30px">Counselling Rooms</h2>
                                <div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> BOB(Bank Of Baroda)</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('MjAw')" id="200-button-join">Join</button>
                                                        <button class="btn btn-md btn-info joinbtn-queue" onclick="joinQueue('MjAw')" id="200-button-join-queue">Join Queue</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> HDFC Credila</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('MjAy')" id="202-button-join">Join</button>
                                                        <button class="btn btn-md btn-info joinbtn-queue" onclick="joinQueue('MjAy')" id="202-button-join-queue">Join Queue</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> Avanse</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('MjAz')" id="203-button-join">Join</button>
                                                        <button class="btn btn-md btn-info joinbtn-queue" onclick="joinQueue('MjAz')" id="203-button-join-queue">Join Queue</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> Incred</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('MjA0')" id="204-button-join">Join</button>
                                                        <button class="btn btn-md btn-info joinbtn-queue" onclick="joinQueue('MjA0')" id="204-button-join-queue">Join Queue</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> Auxilo</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('MjA1')" id="205-button-join">Join</button>
                                                        <button class="btn btn-md btn-info joinbtn-queue" onclick="joinQueue('MjA1')" id="205-button-join-queue">Join Queue</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> ICICI Bank</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('MjA2')" id="206-button-join">Join</button>
                                                        <button class="btn btn-md btn-info joinbtn-queue" onclick="joinQueue('MjA2')" id="206-button-join-queue">Join Queue</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> Axis Bank</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('MjA3')" id="207-button-join">Join</button>
                                                        <button class="btn btn-md btn-info joinbtn-queue" onclick="joinQueue('MjA3')" id="207-button-join-queue">Join Queue</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> MPower</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('MjA4')" id="208-button-join">Join</button>
                                                        <button class="btn btn-md btn-info joinbtn-queue" onclick="joinQueue('MjA4')" id="208-button-join-queue">Join Queue</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> Prodigy</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('MjA5')" id="209-button-join">Join</button>
                                                        <button class="btn btn-md btn-info joinbtn-queue" onclick="joinQueue('MjA5')" id="209-button-join-queue">Join Queue</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <div class="col-md-4 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h2 style="color: white;"> US Bank</h2>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('MjEw')" id="210-button-join">Join</button>
                                                        <button class="btn btn-md btn-info joinbtn-queue" onclick="joinQueue('MjEw')" id="210-button-join-queue">Join Queue</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div id="modal-join-room" class="modal" >
    <!-- Modal content -->
    <div class="row modal-content">
        <div>
            <div style="font-size: 16px; text-align:center;">Join the room within <span id="join-timer"></span> Sec</div>
            <button id="exit-button" class="btn btn-md btn-primary" style="float:right;" onclick="joinRoom('')">Join</button>
        </div>
    </div>
</div>

<div id="modal-missed-room" class="modal" >
    <!-- Modal content -->
    <div class="row modal-content">
        <div>
            <div style="font-size: 16px; text-align:center;">Sorry!! You missed the room. Please join the queue again.</div>
            <button id="exit-button" class="btn btn-md btn-primary" style="float:right;" onclick="closeMissedRoom()">Close</button>
        </div>
    </div>
</div>

<script type="text/javascript">
    var queueId = '';
    function joinRoom(roomId){
        roomId = roomId ? roomId : queueId;
        $.ajax({
            url: '/meeting/loanmela/check_webinar',
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                if(!parseInt(data)){
                    alert("Rooms will open after webinar session is completed");
                    return;
                }
                else{
                    window.location.href = "/meeting/loanmela/<?=$userType?>/o/" + roomId + "/<?=$username?>/<?=$linkCount?>";
                }
            },
            error: function error() {
            }
        });
    }

    function joinWebinar(webinarId){
        $.ajax({
            url: '/meeting/loanmela/check_webinar',
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                if(!parseInt(data)){
                    window.location.href = '/meeting/loanmela/<?=$webinarUserType?>/w/' + webinarId + '/<?=$username?>/<?=$linkCount?>';
                }
                else{
                    alert("Sorry!! The webinar has been ended. You can join any room");
                    return;
                }
            },
            error: function error() {
            }
        });
    }

    function joinQueue(tokenId){
        $.ajax({
            url: '/meeting/loanmela/check_webinar',
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                if(!parseInt(data)){
                    alert("Rooms will open after webinar session is completed");
                    return;
                }
                else{
                    $.ajax({
                        url: '/meeting/loanmela/join_queue/<?=$username?>/' + tokenId,
                        type: 'GET',
                        complete: function complete() {
                        },
                        success: function success(data) {
                            if(!parseInt(data)){
                                alert("Invalid account");
                            }
                            else{
                                alert("You have been added in queue!!");
                            }
                        },
                        error: function error() {
                        }
                    });
                }
            },
            error: function error() {
            }
        });
    }

    function closeMissedRoom(){
        queueId = '';
        var missedRoomModal = document.getElementById("modal-missed-room");
        missedRoomModal.style.display = "none";
    }

    var timerInterval = null;
    var chk_room = null;

    function roomInterval(flag){
        if(flag){
            var chk_room = setInterval(function() {
                $.ajax({
                    url: '/meeting/loanmela/check_rooms/<?=$username?>',
                    type: 'GET',
                    complete: function complete() {
                    },
                    success: function success(data) {
                        data = JSON.parse(data);
                        if(data){
                            data.forEach(room => {
                                if(document.getElementById(room.tokbox_id + '-button-join') !== undefined && document.getElementById(room.tokbox_id + '-button-join') && document.getElementById(room.tokbox_id + '-button-join-queue') !== undefined && document.getElementById(room.tokbox_id + '-button-join-queue')){
                                    document.getElementById(room.tokbox_id + '-button-join').style.display = 'none';
                                    document.getElementById(room.tokbox_id + '-button-join-queue').style.display = 'none';

                                    if(room.available == 1){
                                        document.getElementById(room.tokbox_id + '-button-join').style.display = 'inline';
                                    }
                                    else{
                                        document.getElementById(room.tokbox_id + '-button-join-queue').style.display = 'inline';
                                    }

                                    if(room.in_queue == 1){
                                        queueId = room.en_tokbox_id;
                                        clearInterval(chk_room);
                                        roomInterval(false);
                                        joinTimerInterval(true);
                                        alert("Room is available");
                                    }
                                }
                            })
                        }
                    },
                    error: function error() {
                    }
                });
            }, 15000);
        }
        else{
            clearInterval(chk_room);
        }
    }

    function joinTimerInterval(flag){
        if(flag){
            var joinRoomModal = document.getElementById("modal-join-room");
            joinRoomModal.style.display = "block";
            var joinRoomTimer = 15;
            var timerInterval = setInterval(function() {
                if(joinRoomTimer >= 0){
                    document.getElementById("join-timer").innerHTML = joinRoomTimer;
                }
                else{
                    queueId = '';
                    var missedRoomModal = document.getElementById("modal-missed-room");
                    joinRoomModal.style.display = "none";
                    missedRoomModal.style.display = "block";
                    clearInterval(timerInterval);
                    joinTimerInterval(false);
                    roomInterval(true);

                    $.ajax({
                        url: '/meeting/loanmela/delete_queue/<?=$username?>',
                        type: 'GET',
                        complete: function complete() {
                        },
                        success: function success(data) {
                        },
                        error: function error() {
                        }
                    });
                }
                joinRoomTimer--;
            }, 1000);
        }
        else{
            clearInterval(timerInterval);
        }
    }
    roomInterval(true);
</script>
