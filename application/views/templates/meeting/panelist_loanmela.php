<html>
    <head>
        <title> Hellouni : Visa Seminar | Organizer </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css">
        <link href="<?=base_url()?>css/organizer_visaseminar.css?v=1.0.1" rel="stylesheet">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://static.opentok.com/v2/js/opentok.min.js"></script>

        <style>
            body{
                background: url('<?=$backgroundImage['url']?>');
                background-size: 100vw 100vh;
            }

            .unitableDesign tbody {
                background-color: white;
            }

            .unitableDesign>tbody>tr>td{
                text-align: center;
                vertical-align: middle;
                padding: 8px 12px !important;
                font-size: 14px;
            }

            .unitableDesign tr:first-of-type td:first-of-type {
                border-top-left-radius: 10px;
            }

            .unitableDesign tr:first-of-type td:last-of-type {
                border-top-right-radius: 10px;
            }

            .unitableDesign tr:last-of-type td:first-of-type {
                border-bottom-left-radius: 10px;
            }

            .unitableDesign tr:last-of-type td:last-of-type {
                border-bottom-right-radius: 10px;
            }

            .tdborder{
                border-right: 3px solid #815cd5;
            }

            .notopBorder{
                border-top: aliceblue !important;
            }

            .textHt{
                color: #f6872c;
                font-weight: 700;
                letter-spacing: 0.7px;
            }

            .modal{
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
                z-index: 112;
            }

            .modal-content{
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: 50%;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid" id="videos">
            <div class="col-md-12" id="subscriber"></div>
            <div class="col-md-12" id="publisher" ></div>
            <div class="col-md-12" id="screen-preview"></div>

            <div class="container">
                <div class="col-md-12 carousel" id="carousel-subscriber">
                    <div class="carousel__body">
                        <div class="carousel__slider" id="subscriber-student"></div>
                        <div class="carousel__prev"><!--i class="far fa-angle-left"></i--></div>
                        <div class="carousel__next"><!--i class="far fa-angle-right"></i--></div>
                    </div>
                </div>
            </div>
        </div>

        <div id="mySidenav" class="sidenav">
            <a href="#" id="MenuTab" class="openbtn" onclick="openNav()" ><i class="fas fa-bars"></i></a>
        </div>

        <div id="mySidepanel1" class="sidepanel1">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><i class="fa fa-times" aria-hidden="true"></i></a>
            <a href="javascript:void();" id="share-screen-button"><button type="button" id="screen" onClick="javascript:shareScreen()" class="btn btn-sm btn-primary width100 Orangebtn"><i class="fas fa-chalkboard-teacher">&nbsp;</i><b>Share Screen</b></button></a>

            <a href="javascript:void();"><button type="button" class="slide-toggle btn btn-sm btn-info width100 Orangebtn" ><i class="fas fa-comments">&nbsp;</i><b>Chat</b></button></a>
            <a href="javascript:void();" ><button type="button" class="btn btn-sm btn-danger width100 Orangebtn" onclick="leaveMeeting()"><b>Leave Meeting</b> </button></a>

            <?php
            $show = '';
            if($sessionType == 'w')
            {
                $show = 'style="display:none"';
            }
            ?>
            <div class="col-md-12" <?=$show?>>
                <h2></h2>
                <table class="table unitableDesign">
                    <tbody id="student-list-table">
                        <!--tr>
                            <td class="textHt tdborder">Waiting In Queue (Yet to be met)</td>
                            <td id="total-queue">0</td>
                        </tr-->
                        <tr>
                            <td class="notopBorder"><button type="button" class="btn btn-sm btn-info width100 Orangebtn" onclick="nextBatch()" id="next-batch-button"><b>Next Batch</b></button></td>
                            <td class="notopBorder"><button type="button" class="btn btn-sm btn-info width100 Orangebtn" id="room-lock-button"><b>Room's Locked</b></button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="box" id="chat-box">
            <span>
                <button id="chatBoxOut" onclick="expandChat()" type="button" class="btn btn-sm btn-primary marginbox" ><i class="fas fa-chevron-circle-up"></i></button>
                <button id="chatBoxIn" onclick="collapsChat()" type="button" class="btn btn-sm btn-primary marginbox"><i class="fas fa-chevron-circle-down"></i></button>
                <button type="button" class="slide-toggle btn btn-sm btn-danger marginbox" ><i class="fas fa-times-circle"></i></button>
            </span>
            <div class="box-inner">
                <div id="textchat">
                    <div id="history">
                        <?php
                        if($common_chat_history)
                        {
                            foreach($common_chat_history as $history)
                            {
                                ?>
                                <p id="<?=$history['chat_id']?>" class="<?=$history['chat_of']?>"><?=$history['name'] . ': ' . $history['message']?></p>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <form>
                        <input type="text" placeholder="Input your text here" id="msgTxt"></input>
                        <input type="hidden" id="username" value="<?=$username?>"></input>
                    </form>
                </div>
            </div>
        </div>

        <div id="modal-room-lock" class="modal" >
            <!-- Modal content -->
            <div class="row modal-content">
                <div>
                    <div style="font-size: 16px; text-align:center;">Do you want the room to be unlocked?</div>
                    <button id="exit-button" class="btn btn-md btn-primary" style="float:left;" onclick="lockRoom()">No</button>
                    <button id="exit-button" class="btn btn-md btn-primary" style="float:right;" onclick="unlockRoom()">Yes</button>
                </div>
            </div>
        </div>

        <div id="footer-head" class="col-md-12 col-sm-12">
            <div id="footer-head-sub" class="col-md-12">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 left-align">
                    <div class="copyright">
                        <span class="footer-text">© 2018, </span>
                        <img src="https://www.hellouni.org/img/logo.png" width="90">&nbsp;
                        <span class="footer-text"> All rights reserved</span>
                        <!-- COPYRIGHT 2018 &copy; HELLOUNI 2018 -->
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 right-align">
                    <div class="design">
                        <a href="https://www.hellouni.org" class="footer-text"><b>Hellouni</b> </a> |  <a target="_blank" href="https://www.hellouni.org" class="footer-text">MAKE RIGHT CHOICE !</a>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var apiKey = "<?=$apiKey?>";
            var sessionId = "<?=$sessionId?>";
            var token = "<?=$token?>";
            var tokboxId = "<?=$tokboxId?>";
            var base64TokenId = "<?=$base64_token_id?>";
            var base64Username = "<?=$base64_username?>";

            var session;
            var allConnections = {};
            var allStreams = {};
            var allStreamsStudents = {};
            var publisher;

            var organizerCount = 0;
            var publisherCount = 0;
            var subscriberCount = 0;
            var connectionCount = 0;

            var totalSubscribers = 0;
            var totalSubscribersStudents = 0;
            var screenShared = 0;

            var msgHistory = document.querySelector('#history');

            // Text chat
            var form = document.querySelector('form');
            var msgTxt = document.querySelector('#msgTxt');
            var username = document.querySelector('#username');

            var check_connections = null;
            var allowedStudents = parseInt('<?=$allowedStudents?>');

            function openNav() {
                document.getElementById("mySidepanel1").style.setProperty("width", "20%", "important");
            }

            function closeNav() {
                document.getElementById("mySidepanel1").style.setProperty("width", "0", "important");
                document.getElementById("videos").style.setProperty("width", "100%", "important");
            }

            function expandChat() {
                document.getElementById("chat-box").style.setProperty("height", "95%", "important");
                document.getElementById("chat-box").style.display = "block";
                document.getElementById("chatBoxOut").style.display = "none";
                document.getElementById("chatBoxIn").style.display = "block";
            }

            function collapsChat() {
                document.getElementById("chat-box").style.setProperty("height", "54%", "important");
                document.getElementById("chat-box").style.display = "block";
                document.getElementById("chatBoxIn").style.display = "none";
                document.getElementById("chatBoxOut").style.display = "block";
            }

            $(document).ready(function () {
                $(".slide-toggle").click(function(){
                    $(".box").animate({
                        width: "toggle"
                    });
                });

                $('.btn-filter').on('click', function () {
                    var $target = $(this).data('target');
                    if ($target != 'all') {
                        $('.table tr').css('display', 'none');
                        $('.table tr[data-status="' + $target + '"]').fadeIn('slow');
                    } else {
                        $('.table tr').css('display', 'none').fadeIn('slow');
                    }
                });
            });

            // Handling all of our errors here by alerting them
            function handleError(error) {
                if (error) {
                    //alert(error.message);
                }
            }

            function initializeSession() {
                session = OT.initSession(apiKey, sessionId);

                // Create a publisher
                publisher = OT.initPublisher('subscriber', {
                    insertMode: 'append',
                    width: '100%',
                    height: '100%',
                    name: '<?=$username?>',
                    publishAudio:true,
                    publishVideo:true,
                    style: {buttonDisplayMode: 'on', nameDisplayMode: "on"}
                }, handleError);

                // Connect to the session
                session.connect(token, function(error) {
                    // If the connection is successful, publish to the session
                    if (error) {
                        handleError(error);
                    } else {
                        var node = document.createElement("button");
                        node.setAttribute("id", "video-button-" + publisher.id);
                        node.setAttribute("class", "OT_edge-bar-item OT_mode-on OT_video AVButton fas fa-video");
                        node.setAttribute("onClick", "disableVideo()")
                        document.getElementById(publisher.id).appendChild(node);
                        // document.getElementById("video-button-" + publisher.id).innerHTML = "Disable Video";
                         document.getElementById("video-button-" + publisher.id).style.setProperty("background-color", "#ffffff00", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("font-size", "25px", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("right", "30px", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("top", "4px", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("color", "white", "important");

                        session.publish(publisher, handleError);
                    }
                });

                session.on('signal:msg', function signalCallback(event) {
                    if(event.from.connectionId === session.connection.connectionId){
                        document.getElementById("chat-box").style.display="block";
                        var msg = document.createElement('p');
                        var splitData = event.data.split('|');
                        //msg.textContent = event.data;
                        msg.textContent = splitData[0];
                        msg.setAttribute("id", splitData[1]);
                        msg.className = 'mine';
                        msgHistory.appendChild(msg);
                        msg.scrollIntoView();
                    }
                    else if(event.from.permissions.publish){
                        document.getElementById("chat-box").style.display="block";
                        var msg = document.createElement('p');
                        var splitData = event.data.split('|');
                        //msg.textContent = event.data;
                        msg.textContent = splitData[0];
                        msg.setAttribute("id", splitData[1]);
                        msg.className = 'theirs';
                        msgHistory.appendChild(msg);
                        msg.scrollIntoView();
                    }
                });

                session.on('signal:del-msg', function signalCallback(event) {
                    $("#" + event.data).remove();
                });

                session.on('signal:end-session', function signalCallback(event) {
                    window.location.href = '/meeting/loanmela/panelist/o/' + base64TokenId + '/' + base64Username;
                });

                session.on('signal:next-session', function signalCallback(event) {
                    window.location.href = '/meeting/loanmela/rooms/' + base64Username;
                });

                session.on('signal:room-locked', function signalCallback(event) {
                    if(event.from.connectionId !== session.connection.connectionId){
                        clearInterval(check_connections);
                        roomLockInterval(false);

                        var roomLockModal = document.getElementById("modal-room-lock");
                        roomLockModal.style.display = "none";

                        document.getElementById("room-lock-button").style.setProperty("background-color", "#d9534f");
                        document.getElementById("room-lock-button").style.setProperty("border-color", "#d9534f");
                    }
                });

                session.on('signal:next-batch', function signalCallback(event) {
                    if(event.from.connectionId !== session.connection.connectionId){
                        var roomLockModal = document.getElementById("modal-room-lock");
                        roomLockModal.style.display = "none";

                        document.getElementById("room-lock-button").style.setProperty("background-color", "#f6872c");
                        document.getElementById("room-lock-button").style.setProperty("border-color", "#f6872c");

                        roomLockInterval(true);
                    }
                });

                session.on({
                    connectionCreated: function (event) {
                        // TODO connection just created
                    },
                    connectionDestroyed: function connectionDestroyedHandler(event) {
                        $.ajax({
                            url: '/meeting/loanmela/delete_connection/' + event.connection.data,
                            type: 'GET',
                            complete: function complete() {
                            },
                            success: function success(data) {
                                //console.log(data);
                            },
                            error: function error() {
                            }
                        });
                    },
                    streamDestroyed: function (event){
                        if(event.stream.videoType == 'camera'){
                            if(event.stream.connection.permissions.forceDisconnect == 1){
                                totalSubscribers--;

                                var gridWidth = '100%',
                                    gridHeight = '100%',
                                    gridItems = totalSubscribers,
                                    grid_str = '',
                                    $grid = $('#subscriber'),
                                    i;

                                function dimensions(x) {
                                    let quotient = parseInt(x / 4);
                                    let remainder = x % 4;

                                    var w = 25;
                                    var l = 40;
                                    var divW = 96;
                                    var t = 30;

                                    var screenDivisor = totalSubscribers + 1;
                                    switch(screenDivisor){
                                        case 1:
                                            console.log("I am in 1");
                                            break;
                                        case 2:
                                            w = 50;
                                            l = 20;
                                            divW = 45;
                                            break;
                                        case 3:
                                        case 4:
                                            w = 50;
                                            l = 20;
                                            divW = 45;
                                            t = 20;
                                            break;
                                        case 5:
                                        case 6:
                                            w = 70;
                                            l = 15;
                                            divW = 30;
                                            t = 20;
                                            break;
                                        case 7:
                                        case 8:
                                            w = 98;
                                            l = 5;
                                            divW = 23;
                                            t = 20;
                                            break;
                                        case 9:
                                        case 10:
                                        case 11:
                                        case 12:
                                            w = 98;
                                            l = 5;
                                            divW = 23;
                                            t = 3;
                                            //console.log("I am in 9");
                                            break;
                                    }

                                    let attrb = "width: " + w + "%; height: 30%; position: relative; left: " + l + "%; top: " + t + "%;";
                                    document.getElementById("subscriber").setAttribute("style", attrb);

                                    $('#subscriber .OT_root').css({
                                        width: divW + '%',
                                        height: '100%',
                                        marginRight: '10px'
                                    });
                                };

                                //$grid.width(gridWidth).height(gridHeight);

                                let subscriberId = allStreams[event.stream.id];

                                //console.log(allStreams);
                                document.getElementById(subscriberId).style.display = 'none';

                                if(!screenShared)
                                {
                                    dimensions(totalSubscribers);
                                }

                                if(screenShared)
                                {
                                    var w = 12;
                                    var divW = 100;

                                    //document.getElementById("screen-preview").style.display = "block";
                                    let attrb = "width: " + w + "%; height: 100%; position: absolute; left: 0; top: 2%; overflow-y: auto;";
                                    document.getElementById("subscriber").setAttribute("style", attrb);

                                    $('#subscriber .OT_root').css({
                                        width: divW + '%',
                                        height: '20%',
                                        marginRight: '10px'
                                    });
                                }
                            }
                            else{
                                var subscriberStudentId = allStreamsStudents[event.stream.id];
                                totalSubscribersStudents--;
                                $("#item-" + subscriberStudentId).remove();

                                $('#subscriber-student .OT_root').css({
                                    width: '220px',
                                    height: '140px',
                                    marginRight: '10px',
                                    marginTop: '10px'
                                });
                            }
                        }
                        if(event.stream.videoType == 'screen'){
                            screenShared = 0;
                            document.getElementById("screen-preview").style.display = "none";
                            $('#share-screen-button').show();

                            var w = 25;
                            var l = 40;
                            var divW = 96;
                            var t = 30;

                            var screenDivisor = totalSubscribers + 1;
                            switch(screenDivisor){
                                case 1:
                                    //console.log("I am in 1");
                                    break;
                                case 2:
                                    w = 50;
                                    l = 20;
                                    divW = 45;
                                    break;
                                case 3:
                                case 4:
                                    w = 50;
                                    l = 20;
                                    divW = 45;
                                    t = 20;
                                    break;
                                case 5:
                                case 6:
                                    w = 70;
                                    l = 15;
                                    divW = 30;
                                    t = 20;
                                    break;
                                case 7:
                                case 8:
                                    w = 98;
                                    l = 5;
                                    divW = 23;
                                    t = 20;
                                    break;
                                case 9:
                                case 10:
                                case 11:
                                case 12:
                                    w = 98;
                                    l = 5;
                                    divW = 23;
                                    t = 3;
                                    //console.log("I am in 9");
                                    break;
                            }

                            let attrb = "width: " + w + "%; height: 30%; position: relative; left: " + l + "%; top: " + t + "%;";
                            document.getElementById("subscriber").setAttribute("style", attrb);

                            $('#subscriber .OT_root').css({
                                width: divW + '%',
                                height: '100%',
                                marginRight: '10px'
                            });
                        }
                    },
                    streamCreated: function(event){
                        if(event.stream.videoType == 'camera'){
                            if(event.stream.connection.permissions.forceDisconnect == 1){
                                var subscriber = session.subscribe(event.stream, 'subscriber', {
                                    insertMode: 'append',
                                    width: '100%',
                                    height: '100%',
                                    name: event.stream.name,
                                }, handleError);

                                //console.log(allStreams);
                                totalSubscribers++;
                                allStreams[event.stream.id] = subscriber.id;

                                var gridWidth = '100%',
                                    gridHeight = '100%',
                                    gridItems = totalSubscribers,
                                    grid_str = '',
                                    $grid = $('#subscriber'),
                                    i;

                                function dimensions(x) {
                                    let quotient = parseInt(x / 4);
                                    let remainder = x % 4;

                                    var w = 25;
                                    var l = 40;
                                    var divW = 96;
                                    var t = 30;

                                    var screenDivisor = totalSubscribers + 1;
                                    switch(screenDivisor){
                                        case 1:
                                            console.log("I am in 1");
                                            break;
                                        case 2:
                                            w = 50;
                                            l = 20;
                                            divW = 45;
                                            break;
                                        case 3:
                                        case 4:
                                            w = 50;
                                            l = 20;
                                            divW = 45;
                                            t = 20;
                                            break;
                                        case 5:
                                        case 6:
                                            w = 70;
                                            l = 15;
                                            divW = 30;
                                            t = 20;
                                            break;
                                        case 7:
                                        case 8:
                                            w = 98;
                                            l = 5;
                                            divW = 23;
                                            t = 20;
                                            break;
                                        case 9:
                                        case 10:
                                        case 11:
                                        case 12:
                                            w = 98;
                                            l = 5;
                                            divW = 23;
                                            t = 3;
                                            //console.log("I am in 9");
                                            break;
                                    }

                                    let attrb = "width: " + w + "%; height: 30%; position: relative; left: " + l + "%; top: " + t + "%;";
                                    document.getElementById("subscriber").setAttribute("style", attrb);

                                    $('#subscriber .OT_root').css({
                                        width: divW + '%',
                                        height: '100%',
                                        marginRight: '10px'
                                    });
                                };

                                if(!screenShared)
                                {
                                    document.getElementById(subscriber.id).style.display = 'inline-block';
                                    document.getElementById(subscriber.id).style.boxSizing = 'border-box';
                                    document.getElementById(subscriber.id).style.border = '2px solid white';
                                    document.getElementById(subscriber.id).style.margin = '2px';

                                    dimensions(totalSubscribers);
                                }

                                if(screenShared)
                                {
                                    var w = 12;
                                    var divW = 100;

                                    //document.getElementById("screen-preview").style.display = "block";
                                    let attrb = "width: " + w + "%; height: 100%; position: absolute; left: 0; top: 2%; overflow-y: auto;";
                                    document.getElementById("subscriber").setAttribute("style", attrb);

                                    $('#subscriber .OT_root').css({
                                        width: divW + '%',
                                        height: '20%',
                                        marginRight: '10px'
                                    });
                                }
                            }
                            else{
                                var subscriber = session.subscribe(event.stream, 'subscriber-student', {
                                    insertMode: 'append',
                                    width: '220px',
                                    height: '140px',
                                    name: event.stream.name,
                                    subscribeToAudio:true,
                                    subscribeToVideo:true,
                                    audioVolume: 100
                                }, handleError);

                                totalSubscribersStudents++;
                                allStreamsStudents[event.stream.id] = subscriber.id;

                                var itemDiv = document.createElement("div");
                                itemDiv.setAttribute("id", "item-" + subscriber.id);
                                itemDiv.setAttribute("class", "carousel__slider__item");
                                document.getElementById("subscriber-student").appendChild(itemDiv);

                                var item3DDiv = document.createElement("div");
                                item3DDiv.setAttribute("id", "item-3d-" + subscriber.id);
                                item3DDiv.setAttribute("class", "item__3d-frame");
                                document.getElementById("item-" + subscriber.id).appendChild(item3DDiv);

                                var item3DBoxDiv = document.createElement("div");
                                item3DBoxDiv.setAttribute("id", "item-3d-box-" + subscriber.id);
                                item3DBoxDiv.setAttribute("class", "item__3d-frame__box item__3d-frame__box--front");
                                document.getElementById("item-3d-" + subscriber.id).appendChild(item3DBoxDiv);

                                var elem = document.getElementById(subscriber.id);
                                $("#" + subscriber.id).remove();
                                document.getElementById("item-3d-box-" + subscriber.id).appendChild(elem);

                                var itemLeftDiv = document.createElement("div");
                                itemLeftDiv.setAttribute("class", "item__3d-frame__box item__3d-frame__box--left");
                                document.getElementById("item-3d-"  + subscriber.id).appendChild(itemLeftDiv);

                                var itemRightDiv = document.createElement("div");
                                itemRightDiv.setAttribute("class", "item__3d-frame__box item__3d-frame__box--right");
                                document.getElementById("item-3d-"  + subscriber.id).appendChild(itemRightDiv);

                                $('#subscriber-student .OT_root').css({
                                    width: '220px',
                                    height: '140px',
                                    marginRight: '10px',
                                    marginTop: '10px'
                                });

                                initCarousal();
                            }

                            var SpeakerDetection = function(subscriber, startTalking, stopTalking) {
                                var activity = null;
                                subscriber.on('audioLevelUpdated', function(event) {
                                    var now = Date.now();
                                    if (event.audioLevel > 0.2) {
                                        if (!activity) {
                                            activity = {timestamp: now, talking: false};
                                        }
                                        else if (activity.talking) {
                                            activity.timestamp = now;
                                        }
                                        else if (now- activity.timestamp > 500) {
                                            activity.talking = true;
                                            if (typeof(startTalking) === 'function') {
                                                startTalking();
                                            }
                                        }
                                    }
                                    else if (activity && now - activity.timestamp > 3000) {
                                        // detected low audio activity for more than 3s
                                        if (activity.talking) {
                                            if (typeof(stopTalking) === 'function') {
                                                stopTalking();
                                            }
                                        }
                                        activity = null;
                                    }
                                });
                            };

                            SpeakerDetection(subscriber, function() {
                                document.getElementById(subscriber.id).style.setProperty("overflow", "inherit", "important");

                                var x = document.getElementById(subscriber.id).querySelectorAll(".OT_name");
                                x[0].style.display = "block";
                                x[0].style.opacity = "1";
                                x[0].style.top = "0";


                                var y = document.getElementById(subscriber.id).querySelectorAll(".OT_widget-container");
                                y[0].style.display = "block";
                                y[0].style.setProperty("border", "3px solid #00ca00", "important");



                            }, function() {
                                document.getElementById(subscriber.id).style.background = "";
                                var x = document.getElementById(subscriber.id).querySelectorAll(".OT_name");
                                x[0].style.display = "none";
                                x[0].style.opacity = "0";
                                x[0].style.top = "-25px";

                                var y = document.getElementById(subscriber.id).querySelectorAll(".OT_widget-container");
                                y[0].style.display = "block";
                                y[0].style.setProperty("border", "none", "important");

                                document.getElementById(subscriber.id).style.setProperty("overflow", "hidden", "important");
                            });
                        }
                        if(event.stream.videoType == 'screen'){
                            screenShared = 1;
                            $('#share-screen-button').hide();
                            var w = 12;
                            var divW = 100;

                            document.getElementById("screen-preview").style.display = "block";
                            let attrb = "width: " + w + "%; height: 100%; position: absolute; left: 0; top: 2%; overflow-y: auto;";
                            document.getElementById("subscriber").setAttribute("style", attrb);

                            $('#subscriber .OT_root').css({
                                width: divW + '%',
                                height: '20%',
                                marginRight: '10px'
                            });
                            session.subscribe(event.stream, 'screen-preview', {
                                insertMode: 'append',
                                width: '100%',
                                height: '100%',
                                name: '<?=$username?>'
                            }, handleError);
                        }
                        //console.log("Stream Created ", event);
                    }
                });

                function resizeSubscriber() {
                    publisher.element.style.width = "100px";
                    publisher.element.style.height = "75px";
                    document.getElementById("subscriber").appendChild(publisher.element);
                }
            }

            function disableVideo(){
                publisher.publishVideo(false);
                document.getElementById("video-button-" + publisher.id).setAttribute("onClick", "enableVideo('" + publisher + "')");
                // document.getElementById("video-button-" + publisher.id).innerHTML = "Enable Video";
                document.getElementById("video-button-" + publisher.id).classList.remove('fa-video');
                document.getElementById("video-button-" + publisher.id).classList.add('fa-video-slash');

            }

            function enableVideo(){
                publisher.publishVideo(true);
                document.getElementById("video-button-" + publisher.id).setAttribute("onClick", "disableVideo('" + publisher + "')");
                // document.getElementById("video-button-" + publisher.id).innerHTML = "Disable Video";
                document.getElementById("video-button-" + publisher.id).classList.remove('fa-video-slash');
                document.getElementById("video-button-" + publisher.id).classList.add('fa-video');

            }

            function disconnectUser(connectionId){
                if (session.capabilities.forceDisconnect == 1) {
                    //alert(connectionId);
                    session.forceDisconnect(allConnections[connectionId]);
                } else {
                    alert("No permission");
                }
            }

            function shareScreen(){
                $('#share-screen-button').hide();
                OT.checkScreenSharingCapability(function(response) {
                    if(!response.supported || response.extensionRegistered === false) {
                        $('#share-screen-button').show();
                    } else if (response.extensionInstalled === false) {
                        $('#share-screen-button').show();
                    } else {
                        // Create a publisher for screen
                        document.getElementById("screen-preview").style.display = "none";
                        var publisherScreen = OT.initPublisher('screen-preview', {
                            maxResolution:{width: 1920, height: 1080},
                            videoSource: 'screen'
                        }, handleError);

                        session.publish(publisherScreen, function(error) {
                            if (error) {
                                // Look error.message to see what went wrong.
                            }
                            else{
                                $('#share-screen-button').hide();
                            }
                        });

                        publisherScreen.on('streamDestroyed', function(event) {
                            //document.getElementById("publisher").style.display = "block";
                            $('#share-screen-button').show();
                            var node = document.createElement("div");
                            node.setAttribute("id", "screen-preview");
                            node.setAttribute("style", "display:none; margin-top: 3%;");
                            document.getElementById("videos").appendChild(node);
                            if (event.reason === 'mediaStopped') {
                                // User clicked stop sharing
                            } else if (event.reason === 'forceUnpublished') {
                                // A moderator forced the user to stop sharing.
                            }
                        });
                    }
                });
            }

            // Send a signal once the user enters data in the form
            form.addEventListener('submit', function submit(event) {
                var senderRole = session.connection.permissions.publish ? 'PUBLISHER' : 'SUBSCRIBER';
                event.preventDefault();
                var currentTime = new Date().getTime();
                var chatId = 'chat-' + currentTime;
                $.ajax({
                    url: '/meeting/loanmela/chat',
                    type: 'POST',
                    contentType: 'application/json', // send as JSON
                    data: JSON.stringify({'user_id': '<?=$userId?>', 'sessionId': sessionId, 'tokboxId': tokboxId, 'to': 'All', 'msg': msgTxt.value, 'from_role': senderRole, 'chat_id': chatId}),
                    complete: function complete() {
                        // called when complete
                        //console.log('chat function completed');
                    },
                    success: function success() {
                        // called when successful
                        //console.log('successfully called chat');
                    },
                    error: function error() {
                        // called when there is an error
                        //console.log('error calling chat');
                    }
                });
                session.signal({
                    type: 'msg',
                    data: username.value + ': ' + msgTxt.value + '|' + chatId
                }, function signalCallback(error) {
                    if (error) {
                        //console.error('Error sending signal:', error.name, error.message);
                    } else {
                        msgTxt.value = '';
                    }
                });
            });

            initializeSession();

            function leaveMeeting(){
                window.location.href = '/meeting/loanmela/thankyou';
            }

            var carousel = document.getElementsByClassName('carousel')[0],
                slider = carousel.getElementsByClassName('carousel__slider')[0],
                items = carousel.getElementsByClassName('carousel__slider__item'),
                prevBtn = carousel.getElementsByClassName('carousel__prev')[0],
                nextBtn = carousel.getElementsByClassName('carousel__next')[0];

            var width, height, totalWidth, margin = 20,
                currIndex = 0,
                interval, intervalTime = 4000;

            function initCarousal() {
                carousel = document.getElementsByClassName('carousel')[0],
                slider = carousel.getElementsByClassName('carousel__slider')[0],
                items = carousel.getElementsByClassName('carousel__slider__item'),
                prevBtn = carousel.getElementsByClassName('carousel__prev')[0],
                nextBtn = carousel.getElementsByClassName('carousel__next')[0];

                resize();
                move(Math.floor(items.length / 2));
                bindEvents();

                timer();
            }

            function resize() {
                width= '240',
                height= '160',
                totalWidth = width * items.length;

                slider.style.width = totalWidth + "px";

                for(var i = 0; i < items.length; i++) {
                    let item = items[i];
                    item.style.width = (width - (margin * 2)) + "px";
                    item.style.height = height + "px";
                }
            }

            function move(index) {

                if(index < 1) index = items.length;
                if(index > items.length) index = 1;
                currIndex = index;

                for(var i = 0; i < items.length; i++) {
                    let item = items[i],
                        box = item.getElementsByClassName('item__3d-frame')[0];
                    if(i == (index - 1)) {
                        item.classList.add('carousel__slider__item--active');
                        box.style.transform = "perspective(1200px)";
                    } else {
                      item.classList.remove('carousel__slider__item--active');
                        box.style.transform = "perspective(1200px) rotateY(" + (i < (index - 1) ? 40 : -40) + "deg)";
                    }
                }

                slider.style.transform = "translate3d(" + ((index * -width) + (width / 2) + window.innerWidth / 4) + "px, 0, 0)";
            }

            function timer() {
                clearInterval(interval);
                interval = setInterval(() => {
                  move(++currIndex);
                }, intervalTime);
            }

            function prev() {
              move(--currIndex);
              timer();
            }

            function next() {
              move(++currIndex);
              timer();
            }


            function bindEvents() {
                window.onresize = resize;
                prevBtn.addEventListener('click', () => { prev(); });
                nextBtn.addEventListener('click', () => { next(); });
            }

            function roomLockInterval(flag){
                if(flag){
                    var check_connections = setInterval(function() {
                        $.ajax({
                            url: '/meeting/loanmela/get_connection_count/' + tokboxId,
                            type: 'GET',
                            complete: function complete() {
                            },
                            success: function success(data) {
                                data = parseInt(data);
                                if(!data){
                                    return;
                                }
                                clearInterval(check_connections);
                                roomLockInterval(false);
                                var roomLockModal = document.getElementById("modal-room-lock");

                                if(data < allowedStudents){
                                    $.ajax({
                                        url: '/meeting/loanmela/check_room_status/' + tokboxId,
                                        type: 'GET',
                                        complete: function complete() {
                                        },
                                        success: function success(roomStatus) {
                                            roomStatus = parseInt(roomStatus);

                                            if(roomStatus){
                                                document.getElementById("room-lock-button").style.setProperty("background-color", "#f6872c");
                                                document.getElementById("room-lock-button").style.setProperty("border-color", "#f6872c");

                                                roomLockModal.style.display = "block";
                                            }
                                            else{
                                                roomLockModal.style.display = "none";

                                                document.getElementById("room-lock-button").style.setProperty("background-color", "#d9534f");
                                                document.getElementById("room-lock-button").style.setProperty("border-color", "#d9534f");
                                            }
                                        },
                                        error: function error() {
                                        }
                                    });
                                }
                                else{
                                    roomLockModal.style.display = "none";

                                    document.getElementById("room-lock-button").style.setProperty("background-color", "#d9534f");
                                    document.getElementById("room-lock-button").style.setProperty("border-color", "#d9534f");

                                    lockRoom();
                                }
                            },
                            error: function error() {
                            }
                        });
                    }, 30000);
                }
                else{
                    clearInterval(check_connections);
                }
            }

            function unlockRoom(){
                var roomLockModal = document.getElementById("modal-room-lock");
                roomLockModal.style.display = "none";

                roomLockInterval(true);
            }

            function lockRoom(){
                var roomLockModal = document.getElementById("modal-room-lock");
                roomLockModal.style.display = "none";

                document.getElementById("room-lock-button").style.setProperty("background-color", "#d9534f");
                document.getElementById("room-lock-button").style.setProperty("border-color", "#d9534f");

                $.ajax({
                    url: '/meeting/loanmela/lock_room/' + tokboxId,
                    type: 'GET',
                    complete: function complete() {
                    },
                    success: function success(data) {
                        session.signal({
                            type: 'room-locked',
                            data: ''
                        }, function signalCallback(error) {
                        });
                    },
                    error: function error() {
                    }
                });
            }
            roomLockInterval(true);

            function nextBatch(){
                var roomLockModal = document.getElementById("modal-room-lock");
                roomLockModal.style.display = "none";

                document.getElementById("room-lock-button").style.setProperty("background-color", "#f6872c");
                document.getElementById("room-lock-button").style.setProperty("border-color", "#f6872c");

                roomLockInterval(true);

                $.ajax({
                    url: '/meeting/loanmela/unlock_room/' + tokboxId,
                    type: 'GET',
                    complete: function complete() {
                    },
                    success: function success(data) {
                        session.signal({
                            type: 'next-batch',
                            data: ''
                        }, function signalCallback(error) {
                        });
                    },
                    error: function error() {
                    }
                });
            }
        </script>
    </body>
</html>
