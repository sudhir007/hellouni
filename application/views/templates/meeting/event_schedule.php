<html>
    <head>
        <title> Hellouni : Virtual Fair 2021 </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css">
        <link href="<?=base_url()?>css/newFairDesign.css?v=1.1" rel="stylesheet">
         <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://static.opentok.com/v2/js/opentok.min.js"></script>


        </script>
        <style type="text/css">
          body {
                width: 100%;
                height: 100%;
                background-color: #26002A;

            }
            ul.tabs{
              margin: 0px;
              padding: 0px;
              list-style: none;
            }
            ul.tabs li{
              background: white;
              color: #222;
              display: inline-block;
              padding: 10px 15px;
              cursor: pointer;
            }

            ul.tabs li.current{
              background: #57005E;
              color: #fff;
            }

            .tab-content{
              display: none;
              /*background: #ededed;*/
              padding: 15px;
            }

            .tab-content.current{
              display: inherit;
            }

              .box {
                position: relative;
                background: #57005E;
                color: white;
                padding: 30px;
                border-radius: 10px;
                /*font-size: 15px;*/
                line-height: 25px;
                cursor: pointer;
              }

              .box .box-header {
                font-size: 18px;
                font-weight: 600;
                margin-bottom: 20px;
                display: flex;
                align-items: center;
                text-transform: uppercase;
              }
              .box .box-header img {
                border-radius: 50%;
                margin-right: 20px;
                width: 10% !important;
              }

              .marginB2{
                margin-bottom: 2%;
              }

              .btnpos{
                    position: absolute;
                  right: 5%;
              }

              .timeDisply{
                    color: white;
                text-align: center;
              }

              .marOnlyTB{
                    margin: 2% 0%;
              }
        </style>

    </head>
    <body>
    <header>
        <nav class="navigation">

            <!-- Logo -->
            <div class="logo">

                <img class="img-responsive" src="/images/Fair2021/ImperialLogo-01.jpg" onclick="imageClick('https://bit.ly/2RSGRFm')" width="130" height="60" />
            </div>

            <!-- Navigation -->
            <ul class="menu-list">
                <li onclick="myFunction1()" style="background-color: #00000000;border: 1px solid #00000000;"><a href="<?php echo base_url();?>event/reception/<?=$encodedUsername?>" >Reception</a></li>
                <li onclick="myFunction2()" style="background-color: #f6872c;border: 1px solid #f6872c;"><a href="<?php echo base_url();?>event/booths/<?=$encodedUsername?>" >Universities</a></li>
            </ul>

            <div class="logo rmargin">

                <img class="img-responsive" src="/images/Fair2021/HelloUniLogo-01.jpg" onclick="imageClick('https://bit.ly/2RSGRFm')" width="130" height="60" />
            </div>

            <!-- <div class="humbarger">
                <div class="bar"></div>
                <div class="bar2 bar"></div>
                <div class="bar"></div>
            </div> -->
        </nav>


    </header>

     <div id="universities" class="container"  style="top: 10%;position: relative;">
      <div class="container">

          <ul class="tabs">
            <li class="tab-link current" data-tab="tab-1">My Schedule</li>
            <li class="tab-link" data-tab="tab-2">All Schedule</li>
            <!-- <li class="tab-link" data-tab="tab-3">Tab Three</li>
            <li class="tab-link" data-tab="tab-4">Tab Four</li> -->
          </ul>

          <div id="tab-1" class="tab-content current">
            <div class="col-md-12 marOnlyTB">
              <div class="col-md-1 timeDisply"><h3>6.30pm</h3><h5>7.00pm</h5></div>
              <div class="col-md-11">
                  <div class="box">
                    <h3>Welcome Session</h3>
                     <div class="box-body marginB2">
                      Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups. Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups
                     </div>
                     <h3>Speaker & Host</h3>
                     <div class="box-header">
                        <img src="https://via.placeholder.com/80x80" alt="">
                        <span>
                        <h4>Prof. ABC PQR</h4>
                        <h6>H.O.D , University</h6>
                        </span>
                        <button class="btn btn-md btnorange btnpos"><i class="fa fa-sign-in"></i> Join Session</button>
                     </div>

                  </div>
              </div>
            </div>
            <div class="col-md-12 marOnlyTB">
              <div class="col-md-1 timeDisply"><h3>6.30pm</h3><h5>7.00pm</h5></div>
              <div class="col-md-11">
                  <div class="box">
                    <h3>Welcome Session</h3>
                     <div class="box-body marginB2">
                      Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups. Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups
                     </div>
                     <h3>Speaker & Host</h3>
                     <div class="box-header">
                        <img src="https://via.placeholder.com/80x80" alt="">
                        <span>
                        <h4>Prof. ABC PQR</h4>
                        <h6>H.O.D , University</h6>
                        </span>
                        <button class="btn btn-md btnorange btnpos"><i class="fa fa-sign-in"></i> Join Session</button>
                     </div>

                  </div>
              </div>
            </div>

          </div>
          <div id="tab-2" class="tab-content">
             <div class="col-md-12 marOnlyTB">
              <div class="col-md-1 timeDisply"><h3>6.30pm</h3><h5>7.00pm</h5></div>
              <div class="col-md-11">
                  <div class="box">
                    <h3>Welcome Session</h3>
                     <div class="box-body marginB2">
                      Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups. Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups
                     </div>
                     <h3>Speaker & Host</h3>
                     <div class="box-header">
                        <img src="https://via.placeholder.com/80x80" alt="">
                        <span>
                        <h4>Prof. ABC PQR</h4>
                        <h6>H.O.D , University</h6>
                        </span>
                        <button class="btn btn-md btnorange btnpos"><i class="fa fa-sign-in"></i> Join Session</button>
                     </div>

                  </div>
              </div>
            </div>
            <div class="col-md-12 marOnlyTB">
              <div class="col-md-1 timeDisply"><h3>6.30pm</h3><h5>7.00pm</h5></div>
              <div class="col-md-11">
                  <div class="box">
                    <h3>Welcome Session</h3>
                     <div class="box-body marginB2">
                      Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups. Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups
                     </div>
                     <h3>Speaker & Host</h3>
                     <div class="box-header">
                        <img src="https://via.placeholder.com/80x80" alt="">
                        <span>
                        <h4>Prof. ABC PQR</h4>
                        <h6>H.O.D , University</h6>
                        </span>
                        <button class="btn btn-md btnorange btnpos"><i class="fa fa-sign-in"></i> Join Session</button>
                     </div>

                  </div>
              </div>
            </div>
            <div class="col-md-12 marOnlyTB">
              <div class="col-md-1 timeDisply"><h3>6.30pm</h3><h5>7.00pm</h5></div>
              <div class="col-md-11">
                  <div class="box">
                    <h3>Welcome Session</h3>
                     <div class="box-body marginB2">
                      Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups. Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups
                     </div>
                     <h3>Speaker & Host</h3>
                     <div class="box-header">
                        <img src="https://via.placeholder.com/80x80" alt="">
                        <span>
                        <h4>Prof. ABC PQR</h4>
                        <h6>H.O.D , University</h6>
                        </span>
                        <button class="btn btn-md btnorange btnpos"><i class="fa fa-sign-in"></i> Join Session</button>
                     </div>

                  </div>
              </div>
            </div>
            <div class="col-md-12 marOnlyTB">
              <div class="col-md-1 timeDisply"><h3>6.30pm</h3><h5>7.00pm</h5></div>
              <div class="col-md-11">
                  <div class="box">
                    <h3>Welcome Session</h3>
                     <div class="box-body marginB2">
                      Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups. Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups
                     </div>
                     <h3>Speaker & Host</h3>
                     <div class="box-header">
                        <img src="https://via.placeholder.com/80x80" alt="">
                        <span>
                        <h4>Prof. ABC PQR</h4>
                        <h6>H.O.D , University</h6>
                        </span>
                        <button class="btn btn-md btnorange btnpos"><i class="fa fa-sign-in"></i> Join Session</button>
                     </div>

                  </div>
              </div>
            </div>
            <div class="col-md-12 marOnlyTB">
              <div class="col-md-1 timeDisply"><h3>6.30pm</h3><h5>7.00pm</h5></div>
              <div class="col-md-11">
                  <div class="box">
                    <h3>Welcome Session</h3>
                     <div class="box-body marginB2">
                      Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups. Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups
                     </div>
                     <h3>Speaker & Host</h3>
                     <div class="box-header">
                        <img src="https://via.placeholder.com/80x80" alt="">
                        <span>
                        <h4>Prof. ABC PQR</h4>
                        <h6>H.O.D , University</h6>
                        </span>
                        <button class="btn btn-md btnorange btnpos"><i class="fa fa-sign-in"></i> Join Session</button>
                     </div>

                  </div>
              </div>
            </div>
            <div class="col-md-12 marOnlyTB">
              <div class="col-md-1 timeDisply"><h3>6.30pm</h3><h5>7.00pm</h5></div>
              <div class="col-md-11">
                  <div class="box">
                    <h3>Welcome Session</h3>
                     <div class="box-body marginB2">
                      Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups. Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups
                     </div>
                     <h3>Speaker & Host</h3>
                     <div class="box-header">
                        <img src="https://via.placeholder.com/80x80" alt="">
                        <span>
                        <h4>Prof. ABC PQR</h4>
                        <h6>H.O.D , University</h6>
                        </span>
                        <button class="btn btn-md btnorange btnpos"><i class="fa fa-sign-in"></i> Join Session</button>
                     </div>

                  </div>
              </div>
            </div>
            <div class="col-md-12 marOnlyTB">
              <div class="col-md-1 timeDisply"><h3>6.30pm</h3><h5>7.00pm</h5></div>
              <div class="col-md-11">
                  <div class="box">
                    <h3>Welcome Session</h3>
                     <div class="box-body marginB2">
                      Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups. Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups
                     </div>
                     <h3>Speaker & Host</h3>
                     <div class="box-header">
                        <img src="https://via.placeholder.com/80x80" alt="">
                        <span>
                        <h4>Prof. ABC PQR</h4>
                        <h6>H.O.D , University</h6>
                        </span>
                        <button class="btn btn-md btnorange btnpos"><i class="fa fa-sign-in"></i> Join Session</button>
                     </div>

                  </div>
              </div>
            </div>
          </div>

        </div><!-- container -->


    </div>














        <script type="text/javascript">

           $(document).ready(function(){

  $('ul.tabs li').click(function(){
    var tab_id = $(this).attr('data-tab');

    $('ul.tabs li').removeClass('current');
    $('.tab-content').removeClass('current');

    $(this).addClass('current');
    $("#"+tab_id).addClass('current');
  })

})
        </script>
    </body>
</html>
