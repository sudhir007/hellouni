<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
<style>
   div {
       background: 0 0;
       border: 0;
       margin: 0;
       outline: 0;
       padding: 0;
       vertical-align: baseline;
   }
   .bannerSection{
       background-color: rgba(0, 0, 0, 0);
       background-repeat: no-repeat;
       background-image: url(<?php echo base_url();?>application/images/topBannerVF.png);
       background-size: cover;
       background-position: center center;
       width: 100%;
   }
   .sectionClass {
       padding: 20px 0px 50px 0px;
       position: relative;
       display: block;
   }
   .projectFactsWrap{
       display: flex;
       margin-top: 30px;
       flex-direction: row;
       flex-wrap: wrap;
   }
   .projectFactsWrap .item{
       width: 20%;
       height: 100%;
       padding: 25px 5px;
       text-align: center;
       border-radius: 25%;
       margin: 20px;
       margin-top: -150px;
   }
   .projectFactsWrap .item:nth-child(1){
       background: rgb(246 136 44);
   }
   .projectFactsWrap .item:nth-child(2){
       background: rgb(246 136 44);
   }
   .projectFactsWrap .item:nth-child(3){
       background: rgb(246 136 44);
   }
   .projectFactsWrap .item:nth-child(4){
       background: rgb(246 136 44);
   }
   .projectFactsWrap .item p.number{
       font-size: 25px;
       padding: 0;
       font-weight: bold;
   }
   .projectFactsWrap .item p{
       color: black;
       font-size: 16px;
       margin: 0;
       padding: 10px;
       font-family: 'Open Sans';
       font-weight: 600;
   }
   .projectFactsWrap .item span{
       width: 60px;
       background: rgba(255, 255, 255, 0.8);
       height: 2px;
       display: block;
       margin: 0 auto;
   }
   .projectFactsWrap .item i{
       vertical-align: middle;
       font-size: 50px;
       color: rgba(255, 255, 255, 0.8);
   }
   .projectFactsWrap .item:hover i, .projectFactsWrap .item:hover p{
       color: white;
   }
   .projectFactsWrap .item:hover span{
       background: white;
   }
   @media (max-width: 786px){
       .projectFactsWrap .item {
           flex: 0 0 50%;
       }
   }
   h1:after{
       content: "";
       position: absolute;
       left: 40%;
       margin-left: 0;
       bottom: 0;
       width: 20%;
       border-bottom: 4px solid white;
   }
   h1:before {
       content: "";
       position: absolute;
       left: 40%;
       margin-left: 0;
       top: 0;
       width: 20%;
       border-bottom: 4px solid white;
   }
   .inset {
       padding: 15px;
    }
   @media screen and (max-width: 992px){
       .inset {
           padding: 15px;
        }
   }
   .team-style-five .team-items .item {
       -moz-box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
       -webkit-box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
       -o-box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
       box-shadow: -1px 3px 10px 0 rgba(0, 0, 0, 0.06);
       padding-top: 0px;
       background-color: #62367d;
       margin: 10px;
       height: 115px;
       border-radius: 10px;
   }
   .team-style-five .single-item.team-standard .thumb {
       overflow: hidden;
   }
   .team-style-five .team-items .item .thumb img{
       width: 100%;
       height: 28%;
   }
   .team-style-five .team-items .item .thumb {
       position: relative;
       z-index: 1;
       height: 115px;
   }
   .team-style-five .team-items .item .info {
       background: #ffffff none repeat scroll 0 0;
       padding: 20px 20px;
   }
   .team-style-five .team-items .item .info h3 {
       font-weight: 700;
       margin-bottom: 5px;
       font-size: 18px;
       font-family:"montserrat", sans-serif;
       text-transform: uppercase;
   }
   .team-style-five .single-item.team-standard .item .info span {
       font-size: 13px;
       color: #1cb9c8;
   }
   .team-style-five .single-item.team-standard .item .info .spanName::after {
       background: #1cb9c8 none repeat scroll 0 0;
       bottom: 0;
       content: "";
       height: 2px;
       left: 50%;
       margin-left: -20px;
       position: absolute;
       width: 40px;
   }
   .team-style-five .team-items .item .info p {
       margin-bottom: 0;
       color: #666666;
       text-align: left;
       font-size: 14px;
   }
   @import url("https://fonts.googleapis.com/css?family=Lato:400,400i,700");
   .joinbtn{
       background: #f6872c !important;
       border-color: #f6872c !important;
       border-radius: 5px;
       display: none;
   }
   .joinbtn-queue{
       background: #f6872c !important;
       border-color: #f6872c !important;
       border-radius: 5px;
   }
   @media only screen and (min-width: 1700px) {
       .projectFactsWrap .item{
           width: 17%;
           margin-left: 6%;
       }
   }

   .pulse {
    display: block;
    border-radius: 8px;
    background: #f6882c;
    cursor: pointer;
    transform: scale(1);
    box-shadow: 0 0 0 rgb(246 136 44);
    animation: pulse 1.5s infinite;
}
    .pulse:hover {
      animation: none;
    }

    @-webkit-keyframes pulse {
      0% {
        transform: scale(0.95);
        -webkit-box-shadow: 0 0 0 0 rgba(98,54,125, 0.4);
      }
      70% {
        transform: scale(1);
          -webkit-box-shadow: 0 0 0 10px rgba(98,54,125, 1);
      }
      100% {
        transform: scale(0.95);
          -webkit-box-shadow: 0 0 0 0 rgba(98,54,125, 0);
      }
    }
    @keyframes pulse {
      0% {
        transform: scale(0.95);
        -moz-box-shadow: 0 0 0 0 rgba(98,54,125, 0.4);
        box-shadow: 0 0 0 0 rgba(98,54,125, 0.4);
      }
      70% {
        transform: scale(1);
          -moz-box-shadow: 0 0 0 10px rgba(98,54,125, 0);
          box-shadow: 0 0 0 10px rgba(98,54,125, 0);
      }
      100% {
        transform: scale(0.95);
          -moz-box-shadow: 0 0 0 0 rgba(98,54,125, 0);
          box-shadow: 0 0 0 0 rgba(98,54,125, 0);
      }
    }

    .modal{
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        z-index: 112;
    }

    .modal-content{
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 50%;
    }
</style>

<section class="bannerSection">
    <div class="container" style="height: 400px;">
        <div class="row">
            <div class="col-md-12" style="margin-top: 80px;">
                <h1 class="page-top-title" style="color: #ffffff; text-align:center;font-weight:bold;font-size:45px;margin-top: 25px;">University Of Texas At Arlington</h1>
                <h1 class="page-top-title" style="color: #ffffff; text-align:center;font-weight:bold;font-size:45px;margin-bottom: 25px;">Decision Day 2021</h1>
            </div>
        </div>
    </div>
</section>

<div class="content-wrapper">
    <?php
    /*
    <section>
        <div class="col-md-12">
            <div class="col-md-12 col-lg-12 col-sm-12 hidden-xs">
                <div id="projectFacts" class="sectionClass">
                    <div class="projectFactsWrap " style="margin-left: 50px">
                        <div class="item wow fadeInUpBig animated animated " data-number="200" style="visibility: visible;">
                            <img src="<?php echo base_url();?>application/images/icons/1.png" width="20%">
                            <p id="total_registrations" class="number"></p>
                            <span></span>
                            <p>Total Registrations</p>
                        </div>
                        <div class="item wow fadeInUpBig animated animated" data-number="55" style="visibility: visible;">
                            <img src="<?php echo base_url();?>application/images/icons/2.png" width="35%">
                            <p id="active_participants" class="number"></p>
                            <span></span>
                            <p>Active Participants</p>
                        </div>
                        <div class="item wow fadeInUpBig animated animated" data-number="359" style="visibility: visible;">
                            <img src="<?php echo base_url();?>application/images/icons/3.png" width="20%">
                            <p id="active_rooms" class="number">10</p>
                            <span></span>
                            <p>Active Rooms</p>
                        </div>
                        <div class="item wow fadeInUpBig animated animated" data-number="246" style="visibility: visible;">
                            <img src="<?php echo base_url();?>application/images/icons/4.png" width="20%">
                            <p id="session_attended" class="number"></p>
                            <span></span>
                            <p>Sessions Attended</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    */
    ?>
    <section>
        <div class="container-fluid">
            <div class="row scrollSpyArea" >
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="page-content inset" data-spy="scroll" data-target="#spy">
                        <div class="row team-style-five">
                            <div class="jumbotron text-center row " style="margin-bottom: 15px;">
                                <h2 style="font-size: 30px">Faculties Rooms</h2>
                                <span style="top: 20px; position: absolute; right: 10px; font-size: 16px; font-weight: bold;" id="queue-count"></span>
                                <?php
                                switch($selectedRoom)
                                {
                                    case 188:
                                        ?>
                                        <div>
                                            <div class="col-md-12 col-sm-12 team-items">
                                                <div class="single-item text-center team-standard team-style-block">
                                                    <div class="item">
                                                        <div class="thumb">
                                                            <h4 style="color: white;"> College of Engineering – Computer Science Only Room 1</h4>
                                                            <div class="col-md-12">
                                                                <?php
                                                                if($launch)
                                                                {
                                                                    ?>
                                                                    <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('MTg4')" id="188-button-join">Join</button>
                                                                    <button class="btn btn-md btn-info joinbtn-queue" onclick="checkQueue('MTg4')" id="188-button-queue">Join Queue</button>
                                                                    <?php
                                                                }
                                                                else
                                                                {
                                                                    ?>
                                                                    <h5 style="color: white;"> Please come back on your given slot time <?=$slotTime?></h5>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        break;
                                    case 189:
                                    ?>
                                        <div>
                                            <div class="col-md-12 col-sm-12 team-items">
                                                <div class="single-item text-center team-standard team-style-block">
                                                    <div class="item">
                                                        <div class="thumb">
                                                            <h4 style="color: white;"> College of Engineering – Computer Science Only Room 2</h4>
                                                            <div class="col-md-12">
                                                                <?php
                                                                if($launch)
                                                                {
                                                                    ?>
                                                                    <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('MTg5')" id="189-button-join">Join</button>
                                                                    <button class="btn btn-md btn-info joinbtn-queue" onclick="checkQueue('MTg5')" id="189-button-queue">Join Queue</button>
                                                                    <?php
                                                                }
                                                                else
                                                                {
                                                                    ?>
                                                                    <h5 style="color: white;"> Please come back on your given slot time <?=$slotTime?></h5>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        break;
                                    case 190:
                                        ?>
                                        <div>
                                            <div class="col-md-12 col-sm-12 team-items">
                                                <div class="single-item text-center team-standard team-style-block">
                                                    <div class="item">
                                                        <div class="thumb">
                                                            <h4 style="color: white;"> College of Engineering – Computer Science Only Room 3</h4>
                                                            <div class="col-md-12">
                                                                <?php
                                                                if($launch)
                                                                {
                                                                    ?>
                                                                    <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('MTkw')" id="190-button-join">Join</button>
                                                                    <button class="btn btn-md btn-info joinbtn-queue" onclick="checkQueue('MTkw')" id="190-button-queue">Join Queue</button>
                                                                    <?php
                                                                }
                                                                else
                                                                {
                                                                    ?>
                                                                    <h5 style="color: white;"> Please come back on your given slot time <?=$slotTime?></h5>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        break;
                                    case 191:
                                        ?>
                                        <div>
                                            <div class="col-md-12 col-sm-12 team-items">
                                                <div class="single-item text-center team-standard team-style-block">
                                                    <div class="item">
                                                        <div class="thumb">
                                                            <h4 style="color: white;"> College of Engineering – Engineering All Other Majors Room</h4>
                                                            <div class="col-md-12">
                                                                <?php
                                                                if($launch)
                                                                {
                                                                    ?>
                                                                    <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('MTkx')" id="191-button-join">Join</button>
                                                                    <button class="btn btn-md btn-info joinbtn-queue" onclick="checkQueue('MTkx')" id="191-button-queue">Join Queue</button>
                                                                    <?php
                                                                }
                                                                else
                                                                {
                                                                    ?>
                                                                    <h5 style="color: white;"> Please come back on your given slot time <?=$slotTime?></h5>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        break;
                                    case 192:
                                        ?>
                                        <div>
                                            <div class="col-md-12 col-sm-12 team-items">
                                                <div class="single-item text-center team-standard team-style-block">
                                                    <div class="item">
                                                        <div class="thumb">
                                                            <h4 style="color: white;"> College of Business – Information Systems and Business Analytics Only Room</h4>
                                                            <div class="col-md-12">
                                                                <?php
                                                                if($launch)
                                                                {
                                                                    ?>
                                                                    <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('MTky')" id="192-button-join">Join</button>
                                                                    <button class="btn btn-md btn-info joinbtn-queue" onclick="checkQueue('MTky')" id="192-button-queue">Join Queue</button>
                                                                    <?php
                                                                }
                                                                else
                                                                {
                                                                    ?>
                                                                    <h5 style="color: white;"> Please come back on your given slot time <?=$slotTime?></h5>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        break;
                                    case 193:
                                        ?>
                                        <div>
                                            <div class="col-md-12 col-sm-12 team-items">
                                                <div class="single-item text-center team-standard team-style-block">
                                                    <div class="item">
                                                        <div class="thumb">
                                                            <h4 style="color: white;"> All Other Graduate Majors (Non-ENGR / NON-BUSA)</h4>
                                                            <div class="col-md-12">
                                                                <?php
                                                                if($launch)
                                                                {
                                                                    ?>
                                                                    <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('MTkz')" id="193-button-join">Join</button>
                                                                    <button class="btn btn-md btn-info joinbtn-queue" onclick="checkQueue('MTkz')" id="193-button-queue">Join Queue</button>
                                                                    <?php
                                                                }
                                                                else
                                                                {
                                                                    ?>
                                                                    <h5 style="color: white;"> Please come back on your given slot time <?=$slotTime?></h5>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        break;
                                    case 194:
                                        ?>
                                        <div>
                                            <div class="col-md-12 col-sm-12 team-items">
                                                <div class="single-item text-center team-standard team-style-block">
                                                    <div class="item">
                                                        <div class="thumb">
                                                            <h4 style="color: white;"> Civil &amp; Contruction Room</h4>
                                                            <div class="col-md-12">
                                                                <?php
                                                                if($launch)
                                                                {
                                                                    ?>
                                                                    <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('MTk0')" id="194-button-join">Join</button>
                                                                    <button class="btn btn-md btn-info joinbtn-queue" onclick="checkQueue('MTk0')" id="194-button-queue">Join Queue</button>
                                                                    <?php
                                                                }
                                                                else
                                                                {
                                                                    ?>
                                                                    <h5 style="color: white;"> Please come back on your given slot time <?=$slotTime?></h5>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        break;
                                    case 195:
                                        ?>
                                        <div>
                                            <div class="col-md-12 col-sm-12 team-items">
                                                <div class="single-item text-center team-standard team-style-block">
                                                    <div class="item">
                                                        <div class="thumb">
                                                            <h4 style="color: white;"> General Inquiries / Applications Not Submitted Room</h4>
                                                            <div class="col-md-12">
                                                                <?php
                                                                if($launch)
                                                                {
                                                                    ?>
                                                                    <button class="btn btn-md btn-info joinbtn" onclick="joinRoom('MTk1')" id="195-button-join">Join</button>
                                                                    <button class="btn btn-md btn-info joinbtn-queue" onclick="checkQueue('MTk1')" id="195-button-queue">Join Queue</button>
                                                                    <?php
                                                                }
                                                                else
                                                                {
                                                                    ?>
                                                                    <h5 style="color: white;"> Please come back on your given slot time <?=$slotTime?></h5>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        break;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="page-content inset" data-spy="scroll" data-target="#spy">
                        <div class="row team-style-five">
                            <div class="jumbotron text-center row " style="margin-bottom: 15px;">
                                <h2 style="font-size: 30px">Post Counselling Rooms</h2>
                                <div>
                                    <div class="col-md-12 col-sm-12 team-items">
                                        <div class="single-item text-center team-standard team-style-block">
                                            <div class="item">
                                                <div class="thumb">
                                                    <h5 style="color: white;"> Room </h5>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-md btn-info joinbtn" style="display: inline;" onclick="joinPostCounselling()" id="196-button-join">Join</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div id="modal" class="modal" >
    <!-- Modal content -->
    <div class="row modal-content">
        <div id="quiz">
            <div style="font-size: 16px; text-align:center;">You did not attend post-counselling session. Would you like to join? Press <b>Yes</b> to remain on same page or press <b>Exit</b> to exit the seminar.</div>
            <button id="exit-button" class="btn btn-md btn-primary" style="float:left;" onclick="exit()">Exit</button>
            <button id="yes-button" class="btn btn-md btn-primary" style="float:right;" onclick="hideModal()">Yes</button>
        </div>
    </div>
</div>

<div id="modal-queue" class="modal" >
    <!-- Modal content -->
    <div class="row modal-content">
        <div>
            <div style="font-size: 16px; text-align:center;">You are already in queue for another room. Would you like to cancel old request and raise new?</div>
            <button id="exit-button" class="btn btn-md btn-primary" style="float:left;" onclick="joinQueue()">Yes</button>
            <button id="yes-button" class="btn btn-md btn-primary" style="float:right;" onclick="hideQueueModal()">No</button>
        </div>
    </div>
</div>

<div id="modal-join-room" class="modal" >
    <!-- Modal content -->
    <div class="row modal-content">
        <div>
            <div style="font-size: 16px; text-align:center;">Join the room within <span id="join-timer"></span> Sec</div>
            <button id="exit-button" class="btn btn-md btn-primary" style="float:right;" onclick="joinRoom('abc')">Join</button>
        </div>
    </div>
</div>

<div id="modal-missed-room" class="modal" >
    <!-- Modal content -->
    <div class="row modal-content">
        <div>
            <div style="font-size: 16px; text-align:center;">Sorry!! You missed the room. Please join the queue again.</div>
            <button id="exit-button" class="btn btn-md btn-primary" style="float:right;" onclick="closeMissedRoom()">Close</button>
        </div>
    </div>
</div>

<script type="text/javascript">
    var requestedTokboxId = '';
    function joinRoom(tokboxId){
        delete_cookie('tid');
        tokboxId = tokboxId == 'abc' ? requestedTokboxId : tokboxId;
        window.location.href = "/meeting/uta/student/" + tokboxId + "/<?=$username?>/<?=$linkCount?>";
    }

    function checkQueue(tokboxId){
        requestedTokboxId = tokboxId;
        var queuedTokboxId = get_cookie('tid');
        if(queuedTokboxId){
            var queueModal = document.getElementById("modal-queue");
            queueModal.style.display = "block";
        }
        else{
            joinQueue();
        }
    }

    function joinQueue(){
        $.ajax({
            url: '/meeting/uta/join_queue/' + requestedTokboxId + '/<?=$username?>',
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                set_cookie('tid', requestedTokboxId, 1);
                alert("Successfully added in queue");
                var queueModal = document.getElementById("modal-queue");
                queueModal.style.display = "none";
            },
            error: function error() {
            }
        });
    }

    function joinPostCounselling(){
        $.ajax({
            url: '/meeting/uta/post_cid/',
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                set_cookie('counselling_room', 1, 1);
                window.location.href = '/meeting/uta/student/' + data + '/<?=$username?>';
            },
            error: function error() {
            }
        });
    }

    var confirmBoxClicked = 0;

    setTimeout(function(){
        document.addEventListener("mousemove", function(e) {
            if(parseInt(e.clientY) < 10 && !confirmBoxClicked){
                var counselling_room = get_cookie('counselling_room');

                if(!parseInt(counselling_room)){
                    confirmBoxClicked = 1;
                    var modal = document.getElementById("modal");
                    modal.style.display = "block";
                }
            }
        });
    }, 10000);

    function hideModal(){
        var modal = document.getElementById("modal");
        modal.style.display = "none";
    }

    function hideQueueModal(){
        var queueModal = document.getElementById("modal-queue");
        queueModal.style.display = "none";
    }

    function exit(){
        delete_cookie('tid');
        window.location.href = '/meeting/uta/thankyou';
    }

    function get_cookie(cname){
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++){
            var c = ca[i];
            while (c.charAt(0) == ' '){
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0){
                if(cname == 'cid'){
                    campaign_set = 1;
                }
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    /*
    $.ajax({
        url: '/meeting/uta/get_stats',
        type: 'GET',
        complete: function complete() {
        },
        success: function success(data) {
            data = JSON.parse(data);
            document.getElementById("total_registrations").innerHTML = data.total_registrations;
            document.getElementById("active_participants").innerHTML = data.active_participants;
            document.getElementById("active_rooms").innerHTML = data.active_rooms;
            document.getElementById("session_attended").innerHTML = data.session_attended;
        },
        error: function error() {
        }
    });

    var stats = setInterval(function() {
        $.ajax({
            url: '/meeting/uta/get_stats',
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                data = JSON.parse(data);
                document.getElementById("total_registrations").innerHTML = data.total_registrations;
                document.getElementById("active_participants").innerHTML = data.active_participants;
                document.getElementById("active_rooms").innerHTML = data.active_rooms;
                document.getElementById("session_attended").innerHTML = data.session_attended;
            },
            error: function error() {
            }
        });
    }, 5000);
    */

    var chk_rooms = setInterval(function() {
        $.ajax({
            url: '/meeting/uta/get_rooms_status/',
            type: 'GET',
            complete: function complete() {
            },
            success: function success(data) {
                data = JSON.parse(data);
                data.forEach(function(val){
                    if(val.available == 1 && document.getElementById(val.tokbox_id + '-button-join') !== undefined && document.getElementById(val.tokbox_id + '-button-join') && document.getElementById(val.tokbox_id + '-button-queue')){
                        document.getElementById(val.tokbox_id + '-button-join').style.display = 'inline';
                        document.getElementById(val.tokbox_id + '-button-queue').style.display = 'none';
                    }
                    /*else{
                        document.getElementById(val.tokbox_id + '-button-join').style.display = 'none';
                        document.getElementById(val.tokbox_id + '-button-queue').style.display = 'inline';
                    }*/
                })
            },
            error: function error() {
            }
        });
    }, 1000);

    var timerInterval = null;
    var chk_queue = null;

    function joinTimerInterval(flag){
        if(flag){
            var joinRoomModal = document.getElementById("modal-join-room");
            joinRoomModal.style.display = "block";
            var joinRoomTimer = 15;
            var timerInterval = setInterval(function() {
                if(joinRoomTimer >= 0){
                    document.getElementById("join-timer").innerHTML = joinRoomTimer;
                }
                else{
                    delete_cookie('tid');
                    var missedRoomModal = document.getElementById("modal-missed-room");
                    joinRoomModal.style.display = "none";
                    missedRoomModal.style.display = "block";
                    clearInterval(timerInterval);
                    joinTimerInterval(false);
                    queueInterval(true);

                    $.ajax({
                        url: '/meeting/uta/delete_queue/<?=$username?>',
                        type: 'GET',
                        complete: function complete() {
                        },
                        success: function success(data) {
                        },
                        error: function error() {
                        }
                    });
                }
                joinRoomTimer--;
            }, 1000);
        }
        else{
            clearInterval(timerInterval);
        }
    }

    function queueInterval(flag){
        if(flag){
            var chk_queue = setInterval(function() {
                var queuedTokboxId = '<?=$base64_room?>';
                if(queuedTokboxId){
                    $.ajax({
                        url: '/meeting/uta/get_queue_status/' + queuedTokboxId + '/<?=$username?>',
                        type: 'GET',
                        complete: function complete() {
                        },
                        success: function success(data) {
                            if(data == 0){
                                requestedTokboxId = queuedTokboxId;
                                clearInterval(chk_queue);
                                queueInterval(false);
                                joinTimerInterval(true);
                                alert("Room is available");
                            }
                            else{
                                //console.log("your position is " + data);
                                if(data == -1){
                                    //document.getElementById("queue-count").innerHTML = "The room is not available. You can join queue";
                                }
                                else if(data == -2){
                                        //document.getElementById("queue-count").innerHTML = "You are the next.";
                                }
                                else{
                                    document.getElementById("queue-count").innerHTML = "In Queue: " + data;
                                }
                            }
                        },
                        error: function error() {
                        }
                    });
                }
            }, 15000);
        }
        else{
            clearInterval(chk_queue);
        }
    }

    function set_cookie(name,value,days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days*24*60*60*1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    }

    function delete_cookie(name,value) {
        var expires = "";
        document.cookie = name + "=; path=/";
    }

    function closeMissedRoom(){
        var missedRoomModal = document.getElementById("modal-missed-room");
        missedRoomModal.style.display = "none";
    }

    queueInterval(true);
</script>
