<style type="text/css">
   .box {
   background:#fff;
   transition:all 0.2s ease;
   border:2px dashed #f6882c;
   margin-top: 10px;
   box-sizing: border-box;
   border-radius: 5px;
   background-clip: padding-box;
   padding:0 20px 20px 20px;
   min-height:435px;
   }
   .box:hover {
   border:2px solid #59429e;
   }
   .box span.box-title {
   color: #fff;
   font-size: 24px;
   font-weight: 300;
   text-transform: uppercase;
   }
   .box .box-content {
   padding: 16px 0px 0px 0px;
   border-radius: 0 0 2px 2px;
   background-clip: padding-box;
   box-sizing: border-box;
   }
   .box .box-content p {
   color:#282d31;
   text-transform:none;
   }

   .aboutus-section {
    padding: 90px 0;
}
.aboutus-title {
    font-size: 35px;
    letter-spacing: 1.8px;
    /* line-height: 32px; */
    /* margin: 0 0 0px; */
    /* padding: 0 0 11px; */
    position: relative;
    /* text-transform: uppercase; */
    color: #313131;
}
hr {
    margin-top: 15px;
    margin-bottom: 15px;
    border: 0;
    border-top: 3px solid #f6882c;
}

.tag-title{
    color: #59429e;
}
</style>
<div>
   <section style="background-color: rgba(0, 0, 0, 0);background-repeat: no-repeat;background-image: url(<?php echo base_url();?>application/images/AboutUs.png); background-size: cover;background-position: center center;width: 100%;height: 250%;" class="aboutSection">
      <div class="container" style="height: 330px;">
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="z-index: 5 !important; text-align: center;">
               <h1 class="page-top-title" style="margin-top: 8%;color: #ffffff; text-align:center;font-weight:bold;font-size:37px;"><span></span></h1>
            </div>
         </div>
      </div>
   </section>
</div>
<div class="shade_border"></div>
<div class="container">
   <div class="row">
      <div class="aboutus" style="text-align: center;">
        <h2 class="aboutus-title">About</h2>
        <p style="color: #4a4949;    font-size: 20px;">
            <font color="#f9992f"><b>Hello-Uni</b></font> is a digital platform connecting prospective students with Universities. It’s a virtual one on one meeting environment through our inbuilt web conferencing technology where in we connect pre-qualified students to prospective universities.
        </p>
      </div>
      <div class="col-lg-12" >
         <div class="container">
            <div class="row">
               <div class="aboutus" style="text-align: center;">
                    <h3 class="aboutus-title">Our services include</h3>
               </div>
               <div class="row">
                  <div class="col-md-4 text-center">
                     <div class="box">
                        <div class="box-content">
                           <div class="icon-image">
                              <img src="images/icons/01.png" alt="Connectivity">
                           </div>
                           <h3 class="tag-title">Connectivity</h3>
                           <hr />
                           <p>A platform where in prospective students can connect to the University delegates directly at mutual convenience</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 text-center">
                     <div class="box">
                        <div class="box-content">
                           <div class="icon-image">
                              <img src="images/icons/02.png" alt="Make Friends">
                           </div>
                           <h3 class="tag-title">Make Friends</h3>
                           <hr />
                           <p>Compare and interact with fellow students who are applying to same or similar universities</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 text-center">
                     <div class="box">
                        <div class="box-content">
                           <div class="icon-image">
                              <img src="images/icons/03.png" alt="Connect with Alumni">
                           </div>
                           <h3 class="tag-title">Connect with Alumni</h3>
                           <hr />
                           <p>We facilitate Alumni connect with prospective students to address minor but important queries about the university and help them share first hand experiences with the prospective student</p>
                           <!-- <a href="ppc.html" class="btn btn-block btn-primary">Learn more</a> -->
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-4 text-center">
                     <div class="box">
                        <div class="box-content">
                           <div class="icon-image">
                              <img src="images/icons/04.png" alt="Share Experiences">
                           </div>
                           <h3 class="tag-title">Share Experiences</h3>
                           <hr />
                           <p>We request alumni to write articles about the course and university to share there experiences with prospective students</p>
                           <!-- <a href="ppc.html" class="btn btn-block btn-primary">Learn more</a> -->
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 text-center">
                     <div class="box">
                        <div class="box-content">
                           <div class="icon-image">
                              <img src="images/icons/05.png" alt="Research Insights">
                           </div>
                           <h3 class="tag-title">Research Insights</h3>
                           <hr />
                           <p>We conduct informative webinars in association with professors detailing about research projects and grants. The same leads to higher chances of Research assistantship and students can get first hand information about research in Universities.</p>
                           <!-- <a href="ppc.html" class="btn btn-block btn-primary">Learn more</a> -->
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 text-center">
                     <div class="box">
                        <div class="box-content">
                           <div class="icon-image">
                              <img src="images/icons/06.png" alt="Guidance">
                           </div>
                           <h3 class="tag-title">Guidance</h3>
                           <hr />
                           <p>We help you get fee waiver codes for some of the University where in you have had a successful interaction with University delegate. The same is however at the sole discretion of the University.</p>
                           <!-- <a href="ppc.html" class="btn btn-block btn-primary">Learn more</a> -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <p>&nbsp;</p>
   </div>
</div>
</div>
<div class="container">

   <div class="row padtb">
   <div class="aboutus" style="text-align: center;">
        <h3 class="aboutus-title">Additional Services</h3>
        <p style="color: #4a4949;    font-size: 20px;">
            Our partners provide you additional support in your applications through our GRE App, Virtual Counselling services; Courier services, loan assistance and currency transfer services.
        </p>
      </div>
     
   </div>
</div>
<style>
   p {
   font-size: 16px;
   line-height: 1.6;
   margin:0 0 10px;
   } 
   .italic {
   font-style:italic;
   }
   .padtb {
   padding:30px 0px;
   }
</style>
<!-- @media (min-width: 1200px){
   .container {
       width: 1330px !important;
   }
   }
   
   <li>
     <a  class="page-scroll" href="<?php echo base_url();?>about">About Us</a>
   </li> -->