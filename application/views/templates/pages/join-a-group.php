<style type="text/css">
   table.dataTable thead .sorting:after,
   table.dataTable thead .sorting:before,
   table.dataTable thead .sorting_asc:after,
   table.dataTable thead .sorting_asc:before,
   table.dataTable thead .sorting_asc_disabled:after,
   table.dataTable thead .sorting_asc_disabled:before,
   table.dataTable thead .sorting_desc:after,
   table.dataTable thead .sorting_desc:before,
   table.dataTable thead .sorting_desc_disabled:after,
   table.dataTable thead .sorting_desc_disabled:before {
   bottom: .5em;
   }

   thead{
    background-color: #f6882c;
    color: white;
    font-size: 18px;
    font-weight: 600;
    letter-spacing: 0.8px;
}

.table>thead>tr>th{

    vertical-align: middle;
    text-align: center;
    padding: 8px 10px 8px 10px;
}
 
/*td, th {
  border: 1px solid #000;
  text-align: left;
  padding: 8px;
}

th{
	font-weight: bold;
}*/

/*tr:nth-child(even) {
  background-color: #dddddd;
}*/
/*.table>tbody>tr{
    
}*/

.table>tbody>tr>td{
    vertical-align: middle;
    text-align: center;
    padding: 8px 10px 8px 10px;
}

.table>tbody>tr:hover{
    background-color: #815cd5;
    color: white;
}

.btnGroup{
	    color: #fff;
    background-color: #815cd5;
    border-color: #815cd5;
    border-radius: 5px;
    padding: 10px 50px;
    max-width: 450px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}

.btnGroup:hover{
	    color: #fff;
    background-color: #f6882c;
    border-color: #f6882c;
}

.faIcon{
	    font-size: 16px;
    font-weight: 700;
}

.dataTables_filter label , .dataTables_length label{
	font-size: 15px;
}

.dataTables_filter input{
	border-radius: 6px;
    width: 250px;
}

</style>
<div class="container" >
   <div class="row">
      <div class="col-md-12" style="text-align:center;">
         <h3>Join a Group</h3>
      </div>
      <!--div class="col-md-6 text-right frnd-search ">
         Search <input type="search" placeholder="friends / alumns" autocomplete="off" >
         </div-->
   </div>
</div>
<div class="container" >
   <div class="row">
      <div class="col-md-12 " style="text-align:center;">
         <table id="dtBasicExample" class="table table-hover table-responsive" cellspacing="0" width="100%">
            <thead>
               <tr>
                  <th class="th-sm">Group Name
                  </th>
                  <th class="th-sm">WhatsApp Group Link
                  </th>
               </tr>
            </thead>
            <tbody>
            	<?php
                  foreach($groups as $group)
                  {
                      ?>
               <tr>
                  <td><p style="font-size: 15px"><?=$group['name']?></p></td>
                  <td><a class="btn btn-md btnGroup " href="<?=$group['group_url']?>" target="_blank"><i class="fab fa-whatsapp faIcon"></i>&nbsp;&nbsp;<?=$group['group_url']?> </a></td>
               </tr>
               <?php
                  }
                  ?>
            </tbody>
         </table>
      </div>
   </div>
</div>
<style>
   p {
   font-size: 16px;
   line-height: 1.6;
   margin:0 0 10px;
   color:#000;
   }
   /*.frnd-search {
   font-weight:bold;
   color:#000000;
   font-size:18px;
   }
   .frnd-search input {
   padding:10px;
   border:1px solid #ccc;
   font-weight:bold;
   color:#000000;
   font-size:18px;
   }
   td, th {
   font-size: 16px;
   }
   th {
   font-weight:bold;
   font-size:18px;
   background-color:#f1f1f1;
   }
   table tr:nth-child(even) td{
   background-color:#f1f1f1;
   }
   td a {
   color:#000000;
   }
   td a:hover {
   color:#F6881F;
   }*/
</style>
<script type="text/javascript">
   $(document).ready(function () {
   $('#dtBasicExample').DataTable();
   $('.dataTables_length').addClass('bs-select');
   });
</script>