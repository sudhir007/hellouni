<div class="container" style="margin:30px auto">
        <div class="row">


			<div class="col-md-6 text-left">
				<h3>Friends and alumnus</h3>
			</div>
			<!--div class="col-md-6 text-right frnd-search ">
				Search <input type="search" placeholder="friends / alumns" autocomplete="off" >
			</div-->
	</div>
</div>





<div class="container">

                <div class="row">
                    <div class="col-md-3">
                        <div class="profile-img">
                            <img src="https://www.imperial-overseas.com/images/team/1.jpg" alt=""/>
                            <!--div class="file btn btn-lg btn-primary">
                                Change Photo
                                <input type="file" name="file"/>
                            </div-->
                        </div>
						<div class="profile-work">
						<h5>Name</h5>
						<p>Banit Sawhney</p>
						<h5>University</h5>
						<p>Northeastern University</p>
						<h5>College</h5>
						<p>College of Engineering</p>
						<h5>Course</h5>
						<p>MS in Data Analytics Engineering </p>
						<h5>Duration</h5>
						<p>Fall 2016 – Fall 2018</p>

						</div>
                    </div>
                    <div class="col-md-9">
                        <div class="profile-head">
						 <h5>My Story</h5>
						<p>I hail from Mumbai the financial capital of India. From the very beginning I was interested in the advent of technology and keen to imbibe new ideas. </p>
						<p>With super computers, data centers becoming more accessible it has become more natural for the subject of data analytics engineering.</p>
						<p>With this hope I was looking to apply globally to the best Universities. Northeastern program struck me having the right mix of theoretical and practical training. I applied to ABC, XYZ universities.  </p>
						<p>I was thrilled to receive an admit from the University to pursue my goals. </p>

						<h5>My College  </h5>
						<p>We are located in Boston. We are like a semi urban campus . I like the professor - - - - - . I like the  infrastructure  - - - - -</p>
						<p>Apart from academics we have a good campus life. Discuss  -  Food, Living etc</p>


						<h5>My Current Placement</h5>
						<p>With Northeastern help I was able to get an internship at Apple. During the internship I was involved in analyzing upgrading features in the Mac and analyzing data to suggest improvements. I conversted the internship and currently I am working as a data scientist at Apple. My work involves. . . . . </p>


						<h5>My Insights</h5>
						<p>In hindsight that coming to USA was one of the best decisions of my life. Although there are challenges in terms of finding a job, current visa rules but the MS prepares you for a global role. I travel to work every day looking at the life which I have been leading</p>


                        </div>
                    </div>

                </div>




        </div>

<div class="container" style="margin:30px auto;">
	<div class="row">
	<div class="col-md-6">
	<h5>My Campus Life Photos</h5>
	<ul class="testimonial-list owl-carousel owl-theme">
		<li>
			<div class="testimonial-item ">
				<img src="https://upload.wikimedia.org/wikipedia/en/a/a3/CampusLife_sm.jpg">
			</div>
		</li>
		<li>
			<div class="testimonial-item ">
				<img src="https://www.ryerson.ca/content/dam/campus-life/optimized/Ryerson-university-balzackscafe-campus-life.jpg">
			</div>
		</li>
		<li>
			<div class="testimonial-item ">
				<img src="https://static.dezeen.com/uploads/2015/12/Ryerson-University_Snohetta_Zeidler-Partnership_dezeen_936_12.jpg">
			</div>
		</li>
	</ul>
	</div>

	<div class="col-md-6">
	<h5>My Campus Life Videos</h5>
	<ul class="owl-carousel owl-theme" id="testimonial-video">
		<li>
			<div class="item-video">
								<iframe width="560" height="315" src="https://www.youtube.com/embed/bg1DWh4c35Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
		</li>
		<li>
			<div class="item-video">
								<!--video preload="yes"  controls="true">
									<source src="images/mytestvideo.mp4" type="video/mp4">
								</video-->
								<iframe width="560" height="315" src="https://www.youtube.com/embed/cdfMgotGKIM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
		</li>

	</ul>
	</div>

	</div>

	</div>
</div>




<link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.carousel.min.css" type="text/css">
<link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.theme.default.min.css" type="text/css">

<style>
.testimonial-list {
	list-style-type:none;
	/*border-left:1px solid #c3c3c3;*/
	margin:0px auto;
}

blockquote{
  font-size: 1em;
  width:100%;
  margin:20px auto;
  /*font-family:Open Sans; */
  font-style:italic;
  color: #555555;
  padding:1em 30px 1em 75px;
  line-height:1.6;
  position: relative;
  /*border-left:8px solid #78C0A8 ;
   background:#EDEDED; */
   border-left:none;
}

blockquote::before{
  font-family:roboto;
  content: "\201C";
  color:#ff9900;
  font-size:4em;
  position: absolute;
  left: 10px;
  top:-10px;
}

blockquote::after{
  content: '';
}

blockquote span{
  display:block;
  color:#555555;
  font-style: normal;
  font-weight: bold;
  margin-top:1em;
}

.profile-circle ul {
	list-style-type:none;
	margin:0px;
	padding:15px 0px 0px 0px;
}
.profile-circle ul li {

}
.profile-circle .content {

	width: 500px;
	height:150px;
	margin:0px auto;

}
.profile-circle .content strong{
color:#f6881f;
}
.testi-profile-img{

	width:150px!important;
	float:left;
	margin-right:20px;
	vertical-align: middle;
	border-radius: 50%;
    border: 3px solid lightgrey;
}

.owl-theme .owl-dots .owl-dot span {
	background:#dddddd;
}
.owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span {
	background:#f6881f;
}
#testimonial-video {
	text-align:center;
    margin:0px 0px;
}
ul#testimonial-video {
	list-style:none;
}
 ul.tpage > li:nth-child(odd) {
    background-color: #f1f1f1;
}
.item-video {
	width: 560px;
	height: 315px;
	margin:0 auto;
	text-align:center;


    height: auto;
}
.item-video video {
	max-width: 100%;
	width:100%;
}
p {
    font-size: 16px;
    line-height: 1.6;
    margin:0 0 10px;
    color:#000;
}

.testimonial-list li {
    font-size: 16px;
    color:#000;
    /*margin-left:15px;*/

}
.testimonial-list > li {
    border-left:0px solid #c3c3c3;
	margin-bottom:25px;
	padding:20px;
}

.frnd-search {
	font-weight:bold;
	color:#000000;
	font-size:18px;
}
.frnd-search input {
	padding:10px;
	border:1px solid #ccc;
	font-weight:bold;
	color:#000000;
	font-size:18px;
}

@media only screen and (max-width: 640px)  {
	.item-video {
	width: 100%;
	height: auto;
	margin:0 auto;
	text-align:center;


    height: auto;
}
.item-video video {
	max-width: 100%;
	width:100%;
}
.profile-circle img{
	width:120px!important;
	margin:10px;
	float:unset;
}
.profile-circle .content {
	height:auto;

}


}

</style>

<style>

.profile-img{
    text-align: left;
}
.profile-img img{
    width: 100%;
    height: 100%;
}
.profile-img .file {
    position: relative;
    overflow: hidden;
    width: 100%;
    border: none;
    border-radius: 0;
    font-size: 15px;
    background: #212529b8;
}
.profile-img .file input {
    position: absolute;
    opacity: 0;
    right: 0;
    top: 0;
}



.profile-work{
    padding:10px 0px;
}
.profile-work p, .profile-head p{
    font-size: 15px;
    color: #818182;
	margin:4px 0px 4px 0px;
}
.profile-work h5, .profile-head h5{
    margin:16px 0px 0px 0px;
}
.profile-work a{
    text-decoration: none;
    color: #495057;
    font-weight: 600;
    font-size: 15px;
}

.profile-list {
	box-shadow:2px 5px 5px rgba(0,0,0, 0.3);
	padding:15px;
}
.profile-list .profile-work h5 {
	font-size: 10px;
	margin-top:8px;
	margin-bottom:0px;
}
.profile-list .profile-work p {
	margin:0px;
}
.profile-list p, .profile-list p{
    font-size: 13px;
    color: #818182;
	margin:4px 0px 4px 0px;
}
.profile-list .profile-work{
    padding:0px;
}

</style>

















<script src="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/owl.carousel.js"></script>

 <script>
 $('.owl-carousel').owlCarousel({
        items:1,
        merge:true,
        loop:true,
        margin:10,
		autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
        video:true,
        lazyLoad:true,
        center:true
    });

	$('#testimonial-video').owlCarousel({
		 items:1,
        merge:true,
        loop:false,
        margin:10,
        video:true,
        lazyLoad:true,
        center:true,
		autoplay: false,
		autoplayTimeout: 5000,
		autoplayHoverPause: false,
		autoplaySpeed: false
    });
</script>
