<style>
    .shade_border{
        background:url(img/shade1.png) repeat-x; height:11px;
    }
    .search_by{
        background-color:#f3f4f4; box-shadow:5px 5px 5px #eae8e8; height:auto;
        padding-top: 10px;
        padding-left: 30px;
        padding-right: 30px;
    }
    .search_by h4{
        color:#3c3c3c;
        font-family:lato_regular;
        font-weight:500;
    }
    .table_heading{
        padding:5px;
        font-weight:bold;
    }
    .more_detail{
        background-color:#F6881F;
        text-transform:uppercase;
        height:28px;
        border:0px;
        color:#000;
        border-radius:2px;
        width:25px;
    }
    .right_side_form{
        background:#3c3c3c;
        box-shadow:5px 5px 2px #CCCCCC;
        height:auto;
    }
    label{
        color:#818182;
        font-size:11px;
        font-weight:300;
        margin-top:5px;
    }
    .input_field{
        width:400px !important;
        height:30px;
    }
    .more_detail{
        background-color:#F6881F;
        text-transform:uppercase;
        height:28px;
        border:0px;
        color:#fff;
        border-radius:2px;
    }
    .input_container{
        position:relative;
        padding:0;
        margin:0;
        padding-top: 25px;
    }
    #input{
        margin:0;
        padding-left: 62px;
    }
    #input_img{
        position:absolute;
        bottom:150px;
        left:15px;
        width:50px;
        height:27px;
        margin-left:-5px;
    }
    #input2{
        margin:0;
        padding-left: 62px;
    }
    #input_img2{
        position:absolute;
        bottom:100px;
        left:15px;
        width:50px;
        height:27px;
    }
    #input3{
        margin:0;
        padding-left: 62px;
    }
    #input_img3{
        position:absolute;
        bottom:163px;
        left:15px;
        width:50px;
        height:27px;
    }
    .login_background{
        background-color:#f3f4f6; height:auto;
    }
    .form_font_color{
        color:#818182;
    }
    .form_font_color a{
        color:#818182;
    }
    .button_sign_in{
        background-color:#F6881F;
        border:none;
        width: 100px;
        height: 28px;
        margin-bottom:20px;
    }
    .sign_line{
        background:url(img/line2.png) repeat-y;
        height:100px;
    }
    .login_head{
        color:#F6881F;
        padding-left:0px;
    }
    .login_head2{
        text-transform:capitalize;
        color:#3C3C3C;"
    }
    .header_icons ul li{
        display:inline-block;
        list-style-type:none;
        margin-bottom:-10px;
    }
    .sep{
        margin-top: 20px;
        margin-bottom: 20px;
        background-color: #e6c3c3;
    }
    .invalid_color{
        color:red;
    }
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }
    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }
    tr:nth-child(even) {
        background-color: #dddddd;
    }
    #12thidd{
        display:none;
    }
    #diploma_id{
        display:none;
    }
    #masters_id{
        display:none;
    }
    #locationcountries{
        widows:50%;
    }
    #state{
        width:50%;
    }
    #city{
        width:23%;
    }
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 18px;
        font-weight: 700;
        color:#F6881F;
    }
    .tab button:hover {
        background-color: #ddd;
    }
    .tab button.active {
        background-color: #ccc;
    }
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }
    .showcontent{
        display: block;
    }
    .update_button{
        text-align: center;
    }
    #exam_stage, #exam_date, #result_date, #exam_score, #english_exam_stage, #english_exam_date, #english_result_date, #english_exam_score, #exam_score_gre, #exam_score_gmat, #exam_score_sat{
        display: none;
    }
    #personal_details .form-inline .form-group{
        margin-bottom: 15px;
    }
    #competitive_exam label, #english_proficiency_exam label, #exam_stage label, #exam_date label, #result_date label, #english_exam_stage label{
        margin-left: 10px;
        margin-right: 5px;
    }
    #exam_score label, #english_exam_score label{
        margin-left: 25px;
    }
    .col-lg-12, .col-lg-6{
        padding-left: 0px;
    }
    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }
    /* Modal Content */
        .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 50%;
    }
    /* The Close Button */
    .close {
        color: #8e0f0f;
        float: right;
        font-size: 28px;
        font-weight: bold;
        margin: -20px -10px -10px 0px;
    }
    .close:hover,.close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }
    #internship-description{
        text-align: center;
        font-size: 16px;
        padding: 10px;
    }
</style>

<div class="shade_border"></div>
    <div class="container login_background" >
        <div class="col-lg-12 col-md-12"> <h4 class="form_font_color" style="text-align:center;">Internship</h4></div>
        <div class="col-lg-12 col-md-12">
            <?php
            if($this->session->flashdata('flash_message'))
            {
                ?>
                <h6 class="invalid_color"><?php echo $this->session->flashdata('flash_message');?></h6>
                <?php
            }
            ?>
            <div id="personal_details" class = "tabcontent showcontent">
                <form action="<?php echo base_url()?>internship/<?=$internshipDetail->id?>" class="form-group form-inline input_container" method="post" enctype="multipart/form-data">
                    <div class="form-group col-md-12">
                        <label class="col-md-6">University Name</label>
                        <input type="text" name="university_name" class="form-control form_font_color input_field" value="<?=$internshipDetail->university_name;?>" disabled>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">College Name</label>
                        <input type="text" name="college_name" class="form-control form_font_color input_field" value="<?=$internshipDetail->college_name;?>" disabled>
                    </div>
                    <div class="form-group col-md-12 " >
                        <label class="col-md-6">Department Name</label>
                        <input type="text" name="department_name" class="form-control form_font_color input_field" value="<?=$internshipDetail->department_name;?>" disabled>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">Title</label>
                        <input type="text" name="title" class="form-control form_font_color input_field" value="<?=$internshipDetail->title;?>" disabled>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">Description</label>
                        <textarea class="form-control form_font_color input_field" row="3" col="5" disabled><?=$internshipDetail->description;?></textarea>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">Application Deadline</label>
                        <input type="text" name="application_deadline" class="form-control form_font_color input_field" value="<?=$internshipDetail->application_deadline;?>" disabled>
                    </div>
                    <div class="form-group col-md-12 " >
                        <label class="col-md-6">Internship Start Date</label>
                        <input type="text" name="internship_start_date" class="form-control form_font_color input_field" value="<?=$internshipDetail->internship_start_date;?>" disabled>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6" >Internship End Date</label>
                        <input type="text" name="internship_end_date" class="form-control form_font_color input_field" value="<?=$internshipDetail->internship_end_date;?>" disabled>
                    </div>
                    <?php
                    if($internshipDetail->professors_guide)
                    {
                        foreach($internshipDetail->professors_guide as $index => $value)
                        {
                            ?>
                            <div class="form-group col-md-12">
                                <label class="col-md-6">Professor / Guide Name-<?=($index+1)?></label>
                                <input type="text" name="professor_name[]" class="form-control form_font_color input_field" value="<?=$value->name;?>" disabled>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-6">Professor / Guide Profile Link-<?=($index+1)?></label>
                                <input type="text" name="professor_link[]" class="form-control form_font_color input_field" value="<?=$value->link;?>" disabled>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-6">Professor / Guide Photo-<?=($index+1)?></label>
                                <img src="<?=$value->photo;?>" width="100" height="100" />
                                <!--input type="text" name="professor_photo[]" class="form-control form_font_color input_field" value="<?=$value->photo;?>" disabled-->
                            </div>
                            <?php
                        }
                    }
                    ?>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">Charges</label>
                        <input type="text" name="charges" class="form-control form_font_color input_field" value="<?=$internshipDetail->charges;?>" disabled>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">Other Fees</label>
                        <textarea class="form-control form_font_color input_field" row="3" col="5" disabled><?=$internshipDetail->other_fees ? $internshipDetail->other_fees[0] : '';?></textarea>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">Current University*</label>
                        <input type="text" name="current_university" class="form-control form_font_color input_field" value="<?php if(isset($userDetails) && isset($userDetails->student_other_details->current_university)) echo $userDetails->student_other_details->current_university; ?>" required>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">Current Course*</label>
                        <input type="text" name="current_course" class="form-control form_font_color input_field" value="<?php if(isset($userDetails) && isset($userDetails->student_other_details->current_course)) echo $userDetails->student_other_details->current_course; ?>" required>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">Expected Graduation Year*</label>
                        <input type="date" name="expected_graduation_year" class="form-control form_font_color input_field" value="<?php if(isset($userDetails) && isset($userDetails->student_other_details->expected_graduation_year)) echo $userDetails->student_other_details->expected_graduation_year; ?>" required>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">Nationality*</label>
                        <select class="form-control col-md-6" id="nationality" name="nationality">
                           <?php
                           foreach($countries as $country)
                           {
                               ?>
                               <option value="<?php echo $country->id;?>" <?php if(isset($userDetails) && $country->id==$userDetails->country) echo 'selected="selected"'; ?> ><?php echo $country->name;?></option>
                                <?php
                            }
                            ?>
                         </select>
                    </div>
                    <?php
                    if($internshipDetail->other_details)
                    {
                        foreach($internshipDetail->other_details as $index => $question)
                        {
                            ?>
                            <div class="form-group col-md-12">
                                <label class="col-md-6">Question <?=($index + 1)?></label>
                                <input type="text" name="questions[]" class="form-control form_font_color input_field" value="<?=$question;?>" disabled>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="col-md-6">Answer <?=($index + 1)?></label>
                                <textarea class="form-control form_font_color input_field" row="3" col="5" name="answers[]"><?php if(isset($student_internship) && $student_internship) echo $student_internship->other_details[$index]?></textarea>
                            </div>
                            <?php
                        }
                    }
                    ?>
                    <div class="form-group col-md-12">
                        <label class="col-md-6">Updated Resume</label>
                        <input class="col-md-6" type="file" name="document">
                        <?php
                        if(isset($document) && $document && $document->name)
                        {
                            ?>
                            <a href="<?=$document->url?>" target="_blank" style='color:#337ab7;'><?=$document->name?></a>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="update_button">
                        <?php
                        if($user_data)
                        {
                            ?>
                            <input type="hidden" name="internship_id" value="<?=$internshipDetail->id?>">
                            <input type="hidden" name="student_id" value="<?=$userDetails->student_id?>">
                            <input type="submit" class="button_sign_in" value="APPLY">
                            <?php
                        }
                        else
                        {
                            ?>
                            <input type="button" class="button_sign_in" value="LOGIN & APPLY" onclick="goToLogin()">
                            <?php
                        }
                        ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>
        <div id="internship-description">
            Dear <?=($user_data) ? $user_data['name'] : ''?>, We thank you for showing interest in the Internship Program. The University representative will reach out to you shortly for further details.
        </div>
    </div>
</div>
<script type="text/javascript">
    function goToLogin()
    {
        window.location.href = '/user/account';
    }
    <?php
    if(isset($success))
    {
        if($success)
        {
            ?>
            var modal = document.getElementById("myModal");
            var span = document.getElementsByClassName("close")[0];
            modal.style.display = "block";
            span.onclick = function() {
                modal.style.display = "none";
            }

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }
            <?php
        }
    }
    ?>
</script>
