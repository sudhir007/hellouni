<div class="container" style="margin:30px auto">
   <div class="row">
      <div class="col-md-6 text-left">
         <h3>Friends and alumnus</h3>
      </div>
      <div class="col-md-6 text-right frnd-search ">
         Search <input type="search" placeholder="friends / alumns" autocomplete="off" >
      </div>
   </div>
</div>
<div class="container" style="margin:30px auto">
   <div class="row">
      <div class="col-md-3">
      	<div class="card-content card-contentfrndAlum">
	          <div class="card-img card-imgfrndAlum">
	          	  <span><h5><i class="fas fa-quote-left purple" style="    font-size: 40px;"></i></h5></span>
	              <p>I hail from Mumbai the financial capital of India. From the very beginning I was interested in the advent of technology and keen to imbibe new ideas. </p>
	              
	          </div>
	          <div class="card-desc marginBottomP">
	              <div style="text-align: center;">
	                <img src="https://www.imperial-overseas.com/images/team/1.jpg" alt="Avatar" class="img-frndAlumn">
	                <h3 class="purple">Mr. Banit Sawhney</h3>

	                <p><i class="fas fa-university purple"></i> Northeastern University  &nbsp;&nbsp;<br> <i class="fa fa-graduation-cap purple"></i> MS in Data Analytics Engineering</p>
	                <br>
	                <a href="/alumnu-profile" class="btn-cardPurple ">Read More</a>

	              </div>

	          </div>
      	</div>
         <!-- <div class="profile-list">
            <a href="/alumnu-profile" >
               <h5>Banit Sawhney</h5>
               <div class="profile-img">
                  <img src="https://www.imperial-overseas.com/images/team/1.jpg" alt=""/>
               </div>
               <div class="profile-work">
                  <h5>University</h5>
                  <p>Northeastern University</p>
                  <h5>College</h5>
                  <p>College of Engineering</p>
                  <h5>Course</h5>
                  <p>MS in Data Analytics Engineering </p>
                  <h5>Duration</h5>
                  <p>Fall 2016 – Fall 2018</p>
               </div>
            </a>
         </div> -->
      </div>
      <div class="col-md-3">
      	<div class="card-content card-contentfrndAlumP">
	          <div class="card-img card-imgfrndAlumP">
	          	  <span><h5><i class="fas fa-quote-left orange" style="    font-size: 40px;"></i></h5></span>
	              <p>I hail from Mumbai the financial capital of India. From the very beginning I was interested in the advent of technology and keen to imbibe new ideas. </p>
	              
	          </div>
	          <div class="card-desc marginBottomP">
	              <div style="text-align: center;">
	                <img src="https://www.imperial-overseas.com/images/team/1.jpg" alt="Avatar" class="img-frndAlumn">
	                <h3 class="orange">Mr. Banit Sawhney</h3>

	                <p class="wht"><i class="fas fa-university orange"></i> Northeastern University  &nbsp;&nbsp;<br> <i class="fa fa-graduation-cap orange"></i> MS in Data Analytics Engineering</p>
	                <br>
	                <a href="#" class="btn-card ">Read More</a>
	              </div>

	          </div>
	     </div>
         <!-- <div class="profile-list">
            <a href="/alumnu-profile" >
               <h5>Banit Sawhney</h5>
               <div class="profile-img">
                  <img src="https://www.imperial-overseas.com/images/team/1.jpg" alt=""/>
               </div>
               <div class="profile-work">
                  <h5>University</h5>
                  <p>Northeastern University</p>
                  <h5>College</h5>
                  <p>College of Engineering</p>
                  <h5>Course</h5>
                  <p>MS in Data Analytics Engineering </p>
                  <h5>Duration</h5>
                  <p>Fall 2016 – Fall 2018</p>
               </div>
            </a>
         </div> -->
      </div>
      <div class="col-md-3">
         <div class="card-content card-contentfrndAlum">
	          <div class="card-img card-imgfrndAlum">
	          	  <span><h5><i class="fas fa-quote-left purple" style="    font-size: 40px;"></i></h5></span>
	              <p>I hail from Mumbai the financial capital of India. From the very beginning I was interested in the advent of technology and keen to imbibe new ideas. </p>
	              
	          </div>
	          <div class="card-desc marginBottomP">
	              <div style="text-align: center;">
	                <img src="https://www.imperial-overseas.com/images/team/1.jpg" alt="Avatar" class="img-frndAlumn">
	                <h3 class="purple">Mr. Banit Sawhney</h3>

	                <p><i class="fas fa-university purple"></i> Northeastern University  &nbsp;&nbsp;<br> <i class="fa fa-graduation-cap purple"></i> MS in Data Analytics Engineering</p>
	                <br>
	                <a href="/alumnu-profile" class="btn-cardPurple ">Read More</a>

	              </div>

	          </div>
      	</div>
      </div>
      <div class="col-md-3">
         	<div class="card-content card-contentfrndAlumP">
	          <div class="card-img card-imgfrndAlumP">
	          	   <span><h5><i class="fas fa-quote-left orange" style="font-size: 40px;"></i></h5></span>
	              <p>I hail from Mumbai the financial capital of India. From the very beginning I was interested in the advent of technology and keen to imbibe new ideas. </p>
	             
	          </div>
	          <div class="card-desc marginBottomP">
	              <div style="text-align: center;">
	                <img src="https://www.imperial-overseas.com/images/team/1.jpg" alt="Avatar" class="img-frndAlumn">
	                <h3 class="orange">Mr. Banit Sawhney</h3>

	                <p class="wht"><i class="fas fa-university orange"></i> Northeastern University  &nbsp;&nbsp;<br> <i class="fa fa-graduation-cap orange"></i> MS in Data Analytics Engineering</p>
	                <br>
	                <a href="#" class="btn-card ">Read More</a>
	              </div>

	          </div>
	     </div>
      </div>
   </div>
</div>
<!--link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.carousel.min.css" type="text/css">
   <link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.theme.default.min.css" type="text/css"-->
<style>
   .testimonial-list {
   list-style-type:none;
   /*border-left:1px solid #c3c3c3;*/
   margin:0px auto;
   }
   blockquote{
   font-size: 1em;
   width:100%;
   margin:20px auto;
   /*font-family:Open Sans; */
   font-style:italic;
   color: #555555;
   padding:1em 30px 1em 75px;
   line-height:1.6;
   position: relative;
   /*border-left:8px solid #78C0A8 ;
   background:#EDEDED; */
   border-left:none;
   }
   blockquote::before{
   font-family:roboto;
   content: "\201C";
   color:#ff9900;
   font-size:4em;
   position: absolute;
   left: 10px;
   top:-10px;
   }
   blockquote::after{
   content: '';
   }
   blockquote span{
   display:block;
   color:#555555;
   font-style: normal;
   font-weight: bold;
   margin-top:1em;
   }
   .profile-circle ul {
   list-style-type:none;
   margin:0px;
   padding:15px 0px 0px 0px;
   }
   .profile-circle ul li {
   }
   .profile-circle .content {
   width: 500px;
   height:150px;
   margin:0px auto;
   }
   .profile-circle .content strong{
   color:#f6881f;
   }
   .testi-profile-img{
   width:150px!important;
   float:left;
   margin-right:20px;
   vertical-align: middle;
   border-radius: 50%;
   border: 3px solid lightgrey;
   }
   .owl-theme .owl-dots .owl-dot span {
   background:#dddddd;
   }
   .owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span {
   background:#f6881f;
   }
   #testimonial-video {
   text-align:center;
   margin:0px 0px;
   }
   ul#testimonial-video {
   list-style:none;
   }
   ul.tpage > li:nth-child(odd) {
   background-color: #f1f1f1;
   }
   .item-video {
   width: 560px;
   height: 315px;
   margin:0 auto;
   text-align:center;
   height: auto;
   }
   .item-video video {
   max-width: 100%;
   width:100%;
   }
   p {
   font-size: 16px;
   line-height: 1.6;
   margin:0 0 10px;
   color:#000;
   }
   .testimonial-list li {
   font-size: 16px;
   color:#000;
   /*margin-left:15px;*/
   }
   .testimonial-list > li {
   border-left:0px solid #c3c3c3;
   margin-bottom:25px;
   padding:20px;
   }
   .frnd-search {
   font-weight:bold;
   color:#000000;
   font-size:18px;
   }
   .frnd-search input {
   padding:10px;
   border:1px solid #ccc;
   font-weight:bold;
   color:#000000;
   font-size:18px;
   }
   @media only screen and (max-width: 640px)  {
   .item-video {
   width: 100%;
   height: auto;
   margin:0 auto;
   text-align:center;
   height: auto;
   }
   .item-video video {
   max-width: 100%;
   width:100%;
   }
   .profile-circle img{
   width:120px!important;
   margin:10px;
   float:unset;
   }
   .profile-circle .content {
   height:auto;
   }
   }
</style>
<style>
   .profile-img{
   text-align: left;
   }
   .profile-img img{
   width: 100%;
   height: 100%;
   }
   .profile-img .file {
   position: relative;
   overflow: hidden;
   width: 100%;
   border: none;
   border-radius: 0;
   font-size: 15px;
   background: #212529b8;
   }
   .profile-img .file input {
   position: absolute;
   opacity: 0;
   right: 0;
   top: 0;
   }
   .profile-work{
   padding:10px 0px;
   }
   .profile-work p, .profile-head p{
   font-size: 15px;
   color: #818182;
   margin:4px 0px 4px 0px;
   }
   .profile-work h5, .profile-head h5{
   margin:16px 0px 0px 0px;
   }
   .profile-work a{
   text-decoration: none;
   color: #495057;
   font-weight: 600;
   font-size: 15px;
   }
   .profile-list {
   box-shadow:2px 5px 5px rgba(0,0,0, 0.3);
   padding:15px;
   border:1px solid #ccc;
   }
   .profile-list .profile-work h5 {
   font-size: 10px;
   margin-top:8px;
   margin-bottom:0px;
   }
   .profile-list .profile-work p {
   margin:0px;
   }
   .profile-list p, .profile-list p{
   font-size: 13px;
   color: #818182;
   margin:4px 0px 4px 0px;
   }
   .profile-list .profile-work{
   padding:0px;
   }


   .card-content {
  background: #ffffff;
  border: 4px;
  box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
}
.card-contentfrndAlum{
   background: #f6872c;border-radius: 58px;padding: 4px 4px 2px 9px;
}

.card-contentfrndAlumP{
  background: #815cd5;border-radius: 58px;padding: 4px 4px 2px 9px;
}

.card-imgfrndAlum{
  padding: 3px 10px 10px 10px; text-align: center;
}

.card-imgfrndAlumP{
  color:white; padding: 3px 10px 10px 10px;; text-align: center;
}

.card-imgfrndAlum p {
  font-size: 15px;
  font-family: inherit;
  letter-spacing: 0.6px;
  margin: 0px 5px 0px 5px;
}

.card-imgfrndAlumP p{
  font-size: 15px;
  font-family: inherit;
  letter-spacing: 0.6px;
  margin: 0px 5px 0px 5px;
  color:white;
}

.card-imgfrndAlum span, .card-imgfrndAlumP span{
  left: 32px;
  top: -38px;
  width: 13%;
  border-radius: 50%;
  background: #ffffff00;
  padding: 0px 0px 0px 0px;
}



.img-frndAlumn{
  width:108px;
  border-radius: 50%;
  margin-bottom: 5px;
  border: 2px solid #2b2626;
}

.btn-cardPurple {
  background-color: #8703c5;
  color: #fff;
  box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    padding: .84rem 2.14rem;
    font-size: 1.5rem;
    margin: 0;
    border: 0;
    border-radius: .125rem;
    cursor: pointer;
    text-transform: uppercase;
    white-space: normal;
    word-wrap: break-word;
}
.btn-cardPurple:hover {
    background: #ffffff;
    color: #f6872c;
}
a.btn-cardPurple {
    text-decoration: none;
    color: #fff;
}

.btn-card {
  background-color: #f6882c;
  color: #fff;
  box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
    padding: .84rem 2.14rem;
    font-size: 1.5rem;
    margin: 0;
    border: 0;
    border-radius: .125rem;
    cursor: pointer;
    text-transform: uppercase;
    white-space: normal;
    word-wrap: break-word;
}
.btn-card:hover {
    background: #ffffff;
    color: #815cd5;
}
a.btn-card {
    text-decoration: none;
    color: #fff;
}
</style>
<!--script src="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/owl.carousel.js"></script>
   <script>
   $('.owl-carousel').owlCarousel({
          items:1,
          merge:true,
          loop:true,
          margin:10,
   	autoplay:true,
      autoplayTimeout:3000,
      autoplayHoverPause:true,
          video:true,
          lazyLoad:true,
          center:true
      });
   
   $('#testimonial-video').owlCarousel({
   	 items:1,
          merge:true,
          loop:false,
          margin:10,
          video:true,
          lazyLoad:true,
          center:true,
   	autoplay: false,
   	autoplayTimeout: 5000,
   	autoplayHoverPause: false,
   	autoplaySpeed: false
      });
   </script-->