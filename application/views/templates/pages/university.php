<!--div><img src="images/university-header.jpg" width="100%"> </div-->
<div class="shade_border"></div>

<div class="container">
<div class="row">

  <div class="col-lg-12 text-right">


    <p class="sort-filter">Sort by
      <select id="mysort">
        <option value="ranking_world" <?=($sort_by == 'ranking_world') ? 'selected' : ''?>>Ranking</option>
        <option value="admission_success_rate" <?=($sort_by == 'admission_success_rate') ? 'selected' : ''?>>Admission Success Rate</option>
        <option value="placement_percentage" <?=($sort_by == 'placement_percentage') ? 'selected' : ''?>>Placement</option>
      </select>
    </p>
  </div>

</div>



<div class="row">
    <div class="col-lg-12" style="color:#000;">
        <h4>University</h4>
    <form id="compare-university-form" name="compare-university-form" action="/university/compare" method="POST">
        <?php
        foreach($universities as $university)
        {
            ?>



            <div  class="uni-box">
                <div class="flLt">
                    <div class="uni-image">
                        <a target="_blank" href="javascript:void(0);"><img class="lazy" data-original="<?=$university->banner?>" alt="<?=$university->name?>" title="<?=$university->name?>" align="center" width="172" height="115" src="<?=$university->banner ? $university->banner : '/uploads/banner.jpg'?>" style="display: inline;"></a>
                        <div class="uni-shrtlist-image"></div>
                    </div>
                </div>
                <div class="uni-detail">
                    <div class="uni-title">
                        <p>
                            <a target="_blank" href="/<?=$university->slug?>" id="university-<?=$university->id?>"><span class="font-16" style="color: #247bcc"><?=$university->name?></span></a><span class="font-11">, <?=$university->country_name?></span>
                        </p>
                    </div>
                    <div class="course-touple clearwidth">
                        <!--p><a target="_blank" href="#" class="uni-sub-title">Computer Science&nbsp;MSc</a></p-->
                        <div class="clearwidth">
                            <div class="uni-course-details flLt">
                                <div class="detail-col flLt" style="width:33%;">
                                    <strong><i class="fa fa-users orangeTag" >&nbsp;</i>Total International Students</strong>
                  <p><?=$university->total_students_international?></p>
                                </div>
                                <div class="detail-col flLt" style="width:17%;">
                                    <strong><i class="fa fa-line-chart orangeTag">&nbsp;</i>Ranking</strong>
                                    <p style="position: relative;width:117px !important;">World: <?=$university->ranking_world ? $university->ranking_world : 'N/A'?></p>
                                    <p style="position: relative;width:117px !important;">National: <?=$university->ranking_usa ? $university->ranking_usa : 'N/A'?></p>
                                </div>
                <div class="detail-col flLt" style="width:30%;">
                  <strong><i class="fa fa-graduation-cap orangeTag" >&nbsp;</i>Admission Success Rate</strong>
                  <p><?=$university->admission_success_rate ? $university->admission_success_rate . '%' : 'NA'?></p>
                </div>
                <div class="detail-col flLt" style="width:20%;">
                  <strong><i class="fa fa-briefcase orangeTag" >&nbsp;</i>Placement</strong>
                  <p><?=$university->placement_percentage ? $university->placement_percentage . '%' : 'NA'?></p>
                </div>
                <?php
                /*
                                <div class="detail-col flLt" style="width:25%">
                                    <p><!--span class="tick-mark">✔</span--><?=ucwords(strtolower($university->type))?></p>
                                    <p><!--span class="tick-mark">✔</span-->Scholarship</p>
                                    <p><!--span class="tick-mark">✔</span-->Accommodation</p>
                                </div>
                */
                ?>
                            </div>
                            <div class="btn-col btn-brochure" style="margin-top: 27px !important">
                                <a href="/<?=$university->slug?>" class="button-style" target="_blank">View Detail</a>
                            </div>
                        </div>
                        <div class="flLt" style="width:83%"></div>
                         <div class="row">
                           <div class="col-md-3">
                              <strong><i class="fa fa-eye orangeTag" >&nbsp;</i> Views</strong>
                              <p>100</p>
                           </div>
                           <div class="col-md-3">
                              <strong><i class="fa fa-heart orangeTag" >&nbsp;</i> Favourites</strong>
                              <p>220</p>
                           </div>
                           <div class="col-md-3">
                              <strong><i class="fa fa-star orangeTag" >&nbsp;</i> Reviews</strong>
                              <p>6</p>
                           </div>
                        </div>
                          <div class="compare-box flRt customInputs">
                              <input type="checkbox" name="compare_university[]" value="<?=$university->id?>" onclick="openComparePopup()">
                              <span class="common-sprite"></span>
                              <label><p>Add to compare</p></label>
                <input type="hidden" name="sort_by" value="<?=$sort_by?>">
                          </div>

                    </div>
          <?php
          if($userdata)
          {
            if($university->favourites_link)
            {
              ?>
              <i title="Saved to favourites" class="saved-fav added-shortlist" id="fav-university-<?=$university->id?>" onclick="removeFavourite(<?=$university->id?>, <?=$university->fav_id?>)"></i>
              <?php
            }
            else
            {
              ?>
              <i title="Click to save" class="cate-sprite add-shortlist" onclick="addFavourite(<?=$university->id?>)" id="fav-university-<?=$university->id?>"></i>
              <?php
            }
          }
          else
          {
            ?>
            <i title="Click to save" class="cate-sprite add-shortlist" onclick="goToLogin()" ></i>
            <?php
          }
          ?>
                    <div class="clearfix"></div>
                </div>
            </div>
            <?php
        }
        ?>
    </form>
    </div>
</div>
</div>

<div id="myModal" class="modal">

 <!-- Modal content -->
 <div class="modal-content">
   <span class="close">&times;</span>
   <div id="compare-university-modal"></div>
 </div>

</div>
<style>
.font-16 {
   font-size: 16px;
   letter-spacing: 0.5px;
   color: #247bcc !important;
   }
   .orangeTag{
   color: #f6882c;
   }
.sort-filter {
    font-size: 16px;
    line-height: 1.6;
    margin:0 0 10px;
    color:#000;
}

.font-11 {
    font-size: 12px;
}

.uni-image {
    background: #fff;
    border: 1px solid #f3f3f3;
    padding: 5px;
    position: relative;
    width: 184px;
}
ul.uni-cont li {
    background: #fbfbfb;
    border: 1px solid #f3f3f3;
    margin-bottom: 8px;
    list-style: none;
}
.clearwidth {
    width: 100%;
    float: left;
}
.univ-tab-details {
    margin: 30px 0 10px 0!important;
}

.flLt {
    float: left;
}
.flRt {
    float: right;
}

.uni-box {
    float: left;
    width: 100%;
    padding: 8px;
    position: relative;
    background-color: #fbfbfb;
    /* border: 1px solid #f0f0f0; */
    box-shadow: 2px 2px 4px #ccccc4;
    margin: 20px 0px;
}
.uni-box p {
    margin:0px;
    line-height:unset;
}
.uni-detail {
    margin-left: 208px;
}
.uni-title {
    margin-bottom: 10px;
    width: 90%;
}
.uni-title a {
    font-weight: 600;
    font-size: 13px;
    color: #111111;
}
.uni-title span {
    color: #666666;
}
.uni-sub-title {
    font-weight: 600;
    font-size: 15px;
    color:#666666;
}
a.uni-sub-title {
    color:#666666;
}
.uni-course-details {
    width: 65%;
    border-right: 1px solid #dadada;
    background: #fff;
    margin: 8px 0;
    padding: 8px;
    position: relative;
    margin-right: 25px;
}
.uni-course-details:before {
    width: 100%;
    height: 1px;
    right: -1px;
    bottom: -1px;
    background-image: -webkit-gradient(linear,0 100%,0 0,from(#fff),to(#dadada));
    background-image: -webkit-linear-gradient(right,#dadada,#fff);
    background-image: -moz-linear-gradient(right,#dadada,#fff);
    background-image: -o-linear-gradient(#fff,#dadada);
}
.uni-course-details:after, .uni-course-details:before {
    content: "";
    position: absolute;
}
.detail-col {
    width: 145px;
    font-size: 13px;
    color: #111111;
}
.detail-col p {
    margin-bottom: 3px;
}
.detail-col strong {
    color: #666666;
    margin-bottom: 5px;
    display: block;
    font-weight: 400;
}
.btn-brochure a {
    padding: 8px 18px 9px!important;
}
a.button-style {
    padding: 6px 9px;
}
.compare-box {
    font-size: 14px;
    color: #000;
    width: 140px;
    line-height: 15px;
}
.compare-box p{
    display:inline;
    margin:0px;
}

div {
    background: 0 0;
    border: 0;
    margin: 0;
    outline: 0;
    padding: 0;
    vertical-align: baseline;
}
.sponsered-text {
    color: #999;
    font-size: 12px;
    font-style: italic;
    margin: 15px 0 5px 0;
    display: none;
}
.tar {
    text-align: right;
}
.button-style, .login-btn, a.button-style, a.login-btn {
    background: #F6881F;
    border: 1px solid transparent;
    padding: 5px 10px;
    color: #fff;
    font-size: 14px;
    text-decoration: none;
    vertical-align: middle;
    display: inline-block;
}
.add-shortlist, .added-shortlist {
  background-position: 0 -51px;
  width: 78px;
  height: 77px;
  position: absolute;
  top: 0;
  right: 0px;
  cursor: pointer;
}

.cate-sprite {
    background: url(/images/save-fav-btn-white.png);
    display: inline-block;
    font-style: none;
    vertical-align: middle;
}


/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 30%;
}

/* The Close Button */
.close {
  color: #8e0f0f;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

#compare-university-modal{
  text-align: center;
}

#compare-university-modal p{
  font-size: 16px;
    font-weight: bold;
  margin: 0 0 15px;
}

#compare-university-modal label{
  color:#000;
  font-size: 13px;
  margin-top: 10px;
  font-weight: 500;
}

.compare_universities_submit{
  text-align: center;
    font-size: 15px;
  margin-top: 10px;
}

#compare-university-modal input[type='submit']{
  padding: 5px;
}

.saved-fav {
  background: url(/images/save-fav-btn.png);
}

</style>

<script>
$( document ).ready(function() {
  $("#mysort").change(function(){
    window.location.href = '/university/?sort=' + $( "#mysort option:selected" ).val();
  });

});

var modal = document.getElementById("myModal");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
function openComparePopup() {
  var favorite = [];
  var htmlContent = '<p>Following Universities Are Going To Be Compared.</p>';
    $.each($("input[name='compare_university[]']:checked"), function(){
        favorite.push($(this).val());
    htmlContent += '<label>' + document.getElementById("university-" + $(this).val()).innerHTML + '</label><br/>';
    });
  htmlContent += '<div class="compare_universities_submit"><input type="button" id="compare_universities_submit" name="compare_universities_submit" value="Compare" onclick="submitForm()"></div>';
  if(favorite.length > 1){
    document.getElementById("compare-university-modal").innerHTML = htmlContent;
    modal.style.display = "block";
  }
}

function submitForm(){
  var form = document["compare-university-form"];
  form.submit();
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

function goToLogin(){
  window.location.href="/user/account";
}

function addFavourite(universityId){
  jQuery.ajax({
    type: "GET",
    url: "/user/favourites/add/" + universityId,
    success: function(response) {
       alert("The university has been added successfully in your favourites");
       document.getElementById("fav-university-"+universityId).className = "saved-fav added-shortlist";
       var onclickFunction = 'removeFavourite(' + universityId + ',' + response + ')';
       document.getElementById("fav-university-"+universityId).setAttribute('onclick', onclickFunction);
    }
  });
}

function removeFavourite(universityId, favId){
  jQuery.ajax({
    type: "GET",
    url: "/user/favourites/delete/" + favId,
    success: function(response) {
       alert("The university has been removed successfully from your favourites");
       document.getElementById("fav-university-"+universityId).className = "cate-sprite add-shortlist";
       var onclickFunction = 'addFavourite(' + universityId + ')';
       document.getElementById("fav-university-"+universityId).setAttribute('onclick', onclickFunction);
    }
  });
}
</script>
