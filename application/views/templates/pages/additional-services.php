

<!--div><img src="images/additional-services-header.jpg" width="100%"> </div>
<div class="shade_border"></div-->



    <div class="shade_border"></div>



<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
				<h2>Offers & privileges for students applying for an Education Loan</h2>

			</div>
		</div>
		<div class="row">

		 <div class="col-sm-6 col-md-4">
			<div class="card-offer">
			  <a href=""><img src="images/offers/offer-exams.jpg" alt=" " class="img-responsive"></a>
			  <div class="caption">
				<h3>Get 250 OFF on TOEFL Exam</h3>
				<div class="vertical-align-middle">
				   <p class=""><!-- Flat Rs.200 Cashback | -->On Registering Exams </p>
				   <p><span class=""><a href="/user/account/signup" class="btn btn-default">Register</a></span><!--span class=""><a href="/exam-fees" class="btn btn-secondary">Know more</a></span--> </p>
				</div>
			  </div>
			</div>
		  </div>

		 <div class="col-sm-6 col-md-4">
			<div class="card-offer">
			  <a href=""><img src="images/offers/offer-page-forex.jpg" alt=" " class="img-responsive"></a>
			  <div class="caption">
				<h3>Forex Cashback</h3>
				<div class="vertical-align-middle">
				   <p class=""><!-- Flat 10% Cashback | -->On Forex Bookings </p>
				   <p> <span class=""><a href="/user/account/signup" class="btn btn-default">Register</a></span><!--span class=""><a href="/forex-cards" class="btn btn-secondary">Know more</a></span-->
				</div>
			  </div>
			</div>
		  </div>


		<div class="col-sm-6 col-md-4">
			<div class="card-offer">
			  <a href=""><img src="images/offers/offer-page-medical.jpg" alt=" " class="img-responsive"></a>
			  <div class="caption">
				<h3>Earn Exciting Offers</h3>
				<div class="vertical-align-middle">
				   <p class=""><!-- Flat 10% Cashback | -->On Medical Insurance </p>
				   <p><span class=""><a href="/user/account/signup" class="btn btn-default">Register</a></span>
						<!--span class=""><a href="/medical-insurance" class="btn btn-secondary">Know more</a></span-->
					</p>
				</div>
			  </div>
			</div>
		  </div>




		</div>

		 <div class="row">

	  <div class="col-sm-6 col-md-4">
			<div class="card-offer">
			  <a href=""><img src="images/offers/offer-courier-dhl.jpg" alt=" " class="img-responsive"></a>
			  <div class="caption">
				<h3>Discount with FedEx or DHL</h3>
				<div class="vertical-align-middle">
				   <p class="">@ Discounted price Rs.999  </p>
				   <p>
						<span class=""><a href="/user/account/signup" class="btn btn-default">Register</a></span>
						<!--span class=""><a href="/courier-dhl" class="btn btn-secondary">Know more</a></span-->
					</p>
				</div>
			  </div>
			</div>
		  </div>



 <div class="col-sm-6 col-md-4">
			<div class="card-offer">
			  <a href=""><img src="images/offers/offer-page-flights.jpg" alt=" " class="img-responsive"></a>
			  <div class="caption">
				<h3>Exciting Cashback</h3>
				<div class="vertical-align-middle">
				   <p class="">Flat Rs500 Cashback | On Travel Bookings </p>
				   <p><span class=""><a href="/user/account/signup" class="btn btn-default">Register</a></span><!--span class=""><a href="/flight-tickets" class="btn btn-secondary">Know more</a></span--> </p>
				</div>
			  </div>
			</div>
		  </div>



		</div>

		<div class="row">
			<div class="col-md-12">
			<p>&nbsp;</p>
			<p>Disclaimer - Please note that HelloUni has negotiated this bulk discount offer directly from DHL Couriers. The information shared by students is sent to DHL system and DHL team will contact you to execute the shipment. In all of this transaction, HelloUni is a mere facilitator and is by no means responsible for delivery and shipment of items.</p>

			<p>Offer is only applicable for shipment of documents weighing less than 2 kg.</p>
			</div>
		</div>


    </div>
    <p>&nbsp;</p>
</section>


<style>
p {
    font-size: 16px;
    line-height: 1.6;
    margin:0 0 10px;
    color:#000;
}
.italic {
 font-style:italic;
}
/** offers page styles start ***/
.card-offer, .card {
    background-color: #f7f7f7;
    margin-bottom: 24px;
    border-radius: 0px;
    border: 1px solid #e5e5e5;
	/*box-shadow: 0 0 4px 0 rgba(119,119,119,.3) !important;
    -moz-box-shadow: 0 0 4px 0 rgba(119,119,119,.3) !important;
    -webkit-box-shadow: 0 0 4px 0 rgba(119,119,119,.3) !important; */
}
.card {
	padding:10px;
}
.card-offer .caption {
	padding: 10px 12px;
}
.h3, h3 {
    font-size: 20px;
}
.btn-secondary, .btn.focus, .btn:focus, .btn:hover {
    color: #333;
    text-decoration: none;
}
/** offers page styles end ***/
</style>
