<style type="text/css">
.footer-bottom{
    bottom: 0;
    position: fixed;
}

.footer1{
    bottom: 4rem;
    position: fixed;
    width: 85%;
}

.modal-content{
        height: -webkit-fill-available;
        position: relative;
    background-color: #fff;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
    border: 1px solid #999;
    border: 1px solid rgba(0,0,0,.2);
    border-radius: 6px;
    outline: 0;
    -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
    box-shadow: 0 3px 9px rgba(0,0,0,.5);
}
.modal-dialog{
    z-index: 10000;
    width: 85%;
    margin: 5% auto;
    height: 80%
}

.imageRow{
    background-image: url(<?php echo base_url();?>application/images/LaunchingSoonBanner01-01.png);
    height: -webkit-fill-available;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: contain;
   
}

.weRow{
    font-size: 35px;
    font-weight: lighter;
    letter-spacing: 1px;
    font-family: inherit;
    margin-bottom: 5px;
}

.launchRow{
    font-size: 5rem;
    text-transform: uppercase;
    font-weight: 700;
    letter-spacing: 5px;
    margin: 5px 0 16px;
    text-shadow: 3px 3px black;
}

.pBy{
    margin: 16px 0 16px;
    text-transform: initial;
    font-weight: 600;
    letter-spacing: 1.5px;
    font-style: oblique;
}

.borderCount{
    border-right: 3px solid #ef7829;
}
</style>
<div class="container">
   <div class="row">
      <div class="col-lg-12" >
        
      </div>
   </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <!-- <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div> -->
      <div class="modal-body imageRow">
        <!-- <p>Some text in the modal.</p> -->
        <div class="col-md-12" style="padding: 10% 0px;">
            <div class="col-md-7">
                <div class="row txtAlignCentre ">
                    <h5 class="text-center wht weRow">WE'RE</h5>
                </div>
                <div class="row txtAlignCentre ">
                    <h1 class="text-center wht launchRow">LAUNCHING SOON</h1>
                </div>
                <div class="row">
                    <div class="col-md-3 txtAlignCentre borderCount">
                        <h1 id="day" class="text-center wht"></h1>
                        <h4 class="orange"><b>Days</b></h4>
                    </div>
                    <div class="col-md-3 txtAlignCentre borderCount">
                        <h1 id="hour" class="text-center wht"></h1>
                        <h4 class="orange">Hours</h4>
                    </div>
                    <div class="col-md-3 txtAlignCentre borderCount">
                        <h1 id="min" class="text-center wht"></h1>
                        <h4 class="orange">Minutes</h4>
                    </div>
                    <div class="col-md-3 txtAlignCentre">
                        <h1 id="sec" class="text-center wht"></h1>
                        <h4 class="orange">Seconds</h4>
                    </div>
                </div>
                <div class="row txtAlignCentre ">
                    <h4 class="text-center wht pBy">Powered By</h4>
                    <img src="<?php echo base_url();?>application/images/ImperialLogo.png" width="35%" alt="...">
                </div>                
                
                
            </div>
        </div>
         
               <!-- <img src="<?php echo base_url();?>application/images/LaunchingSoonBanner01-01.png" width="100%" alt="..."> -->
               
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>
<script type="text/javascript">
 $(window).load(function(){        
   $('#myModal').modal('show');
    }); 

$(document).ready(function(){
    $("#myModal").modal({
            backdrop: 'static',
            keyboard: false
        });
});
// Set the date we're counting down to
var countDownDate = new Date("July 27, 2020 20:00:00").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  // document.getElementById("demo").innerHTML = days + "d " + hours + "h "
  // + minutes + "m " + seconds + "s ";
  document.getElementById("day").innerHTML = days ;
  document.getElementById("hour").innerHTML = hours ;
  document.getElementById("min").innerHTML = minutes ;
  document.getElementById("sec").innerHTML = seconds ;
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "EXPIRED";
    $('#myModal').modal('hide');
  }
}, 1000);
</script>
