<div class="container">
   <div class="row">
      <p>&nbsp;</p>
   </div>
   <div class="row">
      <div class="col-lg-12 guidance">
         <p>We at Hello Uni look forward to guide the students towards their interest and dreams. We act as not just counselors and look to guide students in the best interest possible. </p>
         <!--ol>
            <li> <b>Co ordinate between student and University </b><br>
            Once you have connected with the university through our online web-conferencing tool we can support you in your applications.  We would help you with the Universities deadlines, fees structure etc and address clarifications. </li>
            
            <li> <b>Whats App group & Discussion Forum </b><br>
            We have created a Whats App and discussion platform where in you are not only connected with the co-applicant but also connected with Hello Uni co –ordinator as well as university delegate. The same can be used to highlight issues and provide real time addressing of  the issues. </li>
            
            <li><b>Check University Applications </b><br>
            Once you are ready to submit the online applications  our counsellors would help you check the applications. Many Universities also provide us fee waiver codes where in we can use the same to reduce your cost of applying to the institutions.  We also run live waivers and cash back schemes with regard to the University. </li>
            
            <li><b>Statement Of Purpose </b> <br>
            We provide you Statement of Purpose review by one of our empanelled counsellors. </li>
            
            <li><b>Letter of Recommendation </b><br>
            We provide you skeleton structures on which you can discuss and develop your letter of recommendation in discussion with your university professors. </li>
            
            <li><b>Resume</b><br>
            We would help you refine your resume and construct the same under our guidance. </li>
            
            <li><b>University Selection Guidance </b> <br>
                         We help you explore all universities globally. We through our empanelled counsellors would have one on one guidance sessions that would help you select all the relevant Universities.  </li>
            
            
            <li><b>Transcripts Review  </b><br>
            We help you prepare and send transcripts to universities. </li>
            
            <li><b>Visa Financial Documents & Interview Preparation </b><br>
            We guide and fill completely form DS160 and help you with the documentation required for Visa purpose. </li>
            
            <li><b>Guidance on Education Loan </b><br>
            We through our empanelled dedicated loan officer help students select the best education loan provider.  We also provide various Cash backs and references. </li>
            
            <li><b>Guidance on Medical Insurance</b> <br>
            We help you select the best insurance provider during your education. </li>
            
            <li><b>Assistance on Ticket Booking </b><br>
            We work with many ticketing partners where in we would subsidise your  cost involved with your air travel. </li>
            </ol-->
      </div>
   </div>
</div>
<div class="container">
   <div class="row">
      <p>&nbsp;</p>
   </div>
   <div class="row col-sm-12" style="overflow-x: auto;">
      <table class="table table-bordered pricing-table-features">
         <thead class="thead-default">
            <tr>
               <th>
               </th>
               <th class="text-center">
                  <h3>Free Guidance</h3>
               </th>
               <th class="text-center">
                  <h3>Application Plan</h3>
               </th>
               <th class="text-center">
                  <h3>&nbsp;&nbsp;Visa  Plan&nbsp;&nbsp;</h3>
               </th>
               <th class="bg-primary text-center" style="background-color: #6e6e6e;">
                  <h3 style="color:#FFFFFF">Complete Plan </h3>
               </th>
            </tr>
         </thead>
         <tbody>
            <tr>
               <th scope="row">
                  <h3 class="tutor-plan">Price</h3>
               </th>
               <td class="text-center">
                  <div class="pricing-column-price ">
                     FREE
                  </div>
                  <small>Easy to upgrade</small>
               </td>
               <td class="text-center">
                  <div class="pricing-column-price">
                     <!-- INR --><i class="fas fa-rupee-sign"></i> 27,500
                  </div>
                  <small> </small>
               </td>
               <td class="text-center">
                  <div class="pricing-column-price">
                     <!-- INR --><i class="fas fa-rupee-sign"></i> 27,500
                  </div>
                  <small> </small>
               </td>
               <td style="background-color: #eeeeee;" class="text-center">
                  <div class="pricing-column-price">
                     <!-- INR --><i class="fas fa-rupee-sign"></i> 39,500<small> </small>
                  </div>
                  <small> </small>
               </td>
            </tr>
            <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="Once you have connected with the university through our online web-conferencing tool we can support you in your applications.  We would help you with the Universities deadlines, fees structure etc and address clarifications. " >
                     <p class="tutor-plan " style="border-bottom: 1px dashed #dddcde;display:inline;">Co-ordinate between student and University</p>
                     <h5></h5>
                  </span>
               </th>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td style="background-color: #eeeeee;">
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
            </tr>
            <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="We have created a Whats App and discussion platform where in you are not only connected with the co-applicant but also connected with Hello Uni co –ordinator as well as university delegate. The same can be used to highlight issues and provide real time addressing of  the issues.">
                     <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">Whats App group & Discussion Forum</p>
                  </span>
               </th>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td style="background-color: #eeeeee;">
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
            </tr>
            <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="Once you are ready to submit the online applications  our counsellors would help you check the applications. Many Universities also provide us fee waiver codes where in we can use the same to reduce your cost of applying to the institutions.  We also run live waivers and cash back schemes with regard to the University. ">
                     <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">Check University Applications</p>
                  </span>
               </th>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td style="background-color: #eeeeee;">
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
            </tr>
            <!--tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="We provide you Statement of Purpose review by one of our empanelled counsellors.  ">
                 <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">Statement Of Purpose </p>
               </span>
               </th>
               <td>
               
               </td>
               <td>
               <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               
               <td style="background-color:#eeeeee">
                 <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               </tr>
               
               <tr>
               <th scope="row">
                 <span data-toggle="tooltip" title="" data-original-title="We provide you skeleton structures on which you can discuss and develop your letter of recommendation in discussion with your university professors. ">
                 <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">Letter of Recommendation</p>
               </span>
                </th>
               <td>
               
               </td>
               <td>
                 <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               
               <td style="background-color:#eeeeee">
                 <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               </tr>
               <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="We would help you refine your resume and construct the same under our guidance. ">
                 <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">Resume</p>
               </span>
                   </th>
               <td>
               
               </td>
               <td>
                 <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               
               <td style="background-color:#eeeeee">
                 <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               </tr-->
            <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="We provide you document review guidance.  ">
                     <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">Document Review Guidance </p>
                  </span>
               </th>
               <td>
               </td>
               
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td>
               </td>
               <td style="background-color:#eeeeee">
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
            </tr>
            <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="We help you explore all universities globally. We through our empanelled counsellors would have one on one guidance sessions that would help you select all the relevant Universities.">
                     <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">University Selection Guidance </p>
                  </span>
               </th>
               <td>
               </td>
              
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td>
               </td>
               <td style="background-color:#eeeeee">
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
            </tr>
            <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="We help you prepare and send transcripts to universities. ">
                     <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">Transcripts Review </p>
                  </span>
               </th>
               <td>
               </td>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td>
               </td>
               <td style="background-color:#eeeeee">
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
            </tr>
            <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="We guide and fill completely form DS160 and help you with the documentation required for Visa purpose.">
                     <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">Visa Financial Documents & Interview Preparation</p>
                  </span>
               </th>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td style="background-color:#eeeeee">
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
            </tr>
            <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="We guide and fill completely form DS160 and help you with the documentation required for Visa purpose.">
                     <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">Visa Form Filling Checking & Guidance</p>
                  </span>
               </th>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td>
                <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td style="background-color:#eeeeee">
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
            </tr>
            <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="We through our empanelled dedicated loan officer help students select the best education loan provider.  We also provide various Cash backs and references.">
                     <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">Guidance on Education Loan</p>
                  </span>
               </th>
               <td>
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td>
               </td>
               <td>
                <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td style="background-color:#eeeeee">
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
            </tr>
            <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="We help you select the best insurance provider during your education.">
                     <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">Guidance on Medical Insurance</p>
                  </span>
               </th>
               <td>
               </td>
               <td>
               </td>
               <td>
                <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
               <td style="background-color:#eeeeee">
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
            </tr>
            <tr>
               <th scope="row">
                  <span data-toggle="tooltip" title="" data-original-title="We work with many ticketing partners where in we would subsidise your  cost involved with your air travel. ">
                     <p class="tutor-plan" style="border-bottom: 1px dashed #dddcde;display:inline;">Assistance on Ticket Booking </p>
                  </span>
               </th>
               <td>
               </td>
               <td>
               </td>
               <td>
               </td>
               <td style="background-color:#eeeeee">
                  <p class="text-center" style="color:#37B95C"><i class="fa fa-check-circle"></i></p>
               </td>
            </tr>
            <tr>
               <th scope="row">
               </th>
               <td class="text-center">
                  <a href="/user/account/signup" class="btn btn-default btn-md tutor-btn">Sign Up Now</a>
               </td>
               <td class="text-center">
                  <a href="/user/account/signup" class="btn btn-default btn-md tutor-btn">Sign Up Now</a>
               </td>
               <td class="text-center">
                  <a href="/user/account/signup" class="btn btn-default btn-md tutor-btn">Sign Up Now</a>
               </td>
               <td style="background-color: #eeeeee;" class="text-center">
                  <a href="/user/account/signup" class="btn btn-default btn-md tutor-btn">Sign Up Now</a>
               </td>
            </tr>
         </tbody>
      </table>
   </div>
</div>
<div class="container">
   <div class="row">
      <p>&nbsp;</p>
   </div>
   <div class="row">
      <div class="col-md-6">
         <img style="float: left; width:100%;" alt=" " src="images/note-taking.jpg">
      </div>
      <div class="col-md-6">
         <h2>How it works</h2>
         <ul class="pricing-column-features">
            <li>
               <i class="fa fa-check-circle"></i>
               Sign up and receive a welcome email
            </li>
            <li>
               <i class="fa fa-check-circle"></i>
               Complete <b>basic form for your application</b>
            </li>
            <li>
               <i class="fa fa-check-circle"></i>
               Payment through our <b>online gateway system</b>
            </li>
            <li>
               <i class="fa fa-check-circle"></i>
               You would receive an <b>enrollment from Imperial Overseas our Empanelled Education counselors</b>
            </li>
            <li>
               <i class="fa fa-check-circle"></i>
               Imperial's dedicated Guidance counselor would reach out to you and start your process immediately.
            </li>
         </ul>
      </div>
   </div>
   <div class="row">
      <p>&nbsp;</p>
   </div>
</div>
<!--link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous"-->
<style type="text/css">
   p , .guidance li, td{
   font-size: 16px;
   line-height: 1.6;
   margin:0 0 10px;
   color:#000;
   }
   .guidance ol li{
   list-style:decimal;
   margin-left:24px;
   }
   .pricing-column-features li {
   color:#000;
   font-size: 16px;
   line-height: 1.6;
   }
   .italic {
   font-style:italic;
   }
   .text-center {
   text-align:center;
   }
   .pricing-column-price {
   font-size: 24px;
   font-weight: 700;
   }
   .tutor-plan {
   font-weight: 700;
   }
   .btn {
   padding:8px 12px;
   }
   .dashed-border  {
   border-bottom: 1px dashed #dddcde;
   }
</style>
<script>
   $(document).ready(function(){
       $('[data-toggle="tooltip"]').tooltip();
   });
</script>