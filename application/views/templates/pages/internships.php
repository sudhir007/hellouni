<style>
    .internship-description {
        padding-top:30px;
        margin-left:15px;
    }
    .internship-item a {
        color:#000000;
        text-decoration:underline;
    }
    .internship-item a:hover {
        color:#F6881F;
    }
    .testimonial-list {
        list-style-type:none;
        margin:0px auto;
    }
    blockquote{
        font-size: 1em;
        width:100%;
        margin:20px auto;
        font-style:italic;
        color: #555555;
        padding:1em 30px 1em 75px;
        line-height:1.6;
        position: relative;
        border-left:none;
    }
    blockquote::before{
        font-family:roboto;
        content: "\201C";
        color:#ff9900;
        font-size:4em;
        position: absolute;
        left: 10px;
        top:-10px;
    }
    blockquote::after{
        content: '';
    }
    blockquote span{
        display:block;
        color:#555555;
        font-style: normal;
        font-weight: bold;
        margin-top:1em;
    }
    .profile-circle ul {
        list-style-type:none;
        margin:0px;
        padding:15px 0px 0px 0px;
    }
    .profile-circle .content {
        width: 500px;
        height:150px;
        margin:0px auto;
    }
    .profile-circle .content strong{
        color:#f6881f;
    }
    .testi-profile-img{
        width:150px!important;
        float:left;
        margin-right:20px;
        vertical-align: middle;
        border-radius: 50%;
        border: 3px solid lightgrey;
    }
    .owl-theme .owl-dots .owl-dot span {
        background:#dddddd;
    }
    .owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span {
        background:#f6881f;
    }
    #testimonial-video {
        text-align:center;
        margin:30px 0px;
    }
    ul#testimonial-video {
        list-style:none;
    }
    ul.tpage > li:nth-child(odd) {
        background-color: #f1f1f1;
    }
    .item-video {
        margin:0 auto;
        text-align:center;
    }
    p {
        font-size: 16px;
        line-height: 1.6;
        margin:0 0 10px;
        color:#000;
    }
    .testimonial-list li {
        font-size: 16px;
        color:#000;
        margin-left:15px;
    }
    .testimonial-list > li {
        border-left:0px solid #c3c3c3;
        margin-bottom:25px;
        padding:20px;
    }
    .frnd-search {
        font-weight:bold;
        color:#000000;
        font-size:18px;
    }
    .frnd-search input {
        padding:10px;
        border:1px solid #ccc;
        font-weight:bold;
        color:#000000;
        font-size:18px;
    }
    #example_filter label, #example_length label, #example_info, #example_paginate{
        font-size: 16px;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }
    /* Modal Content */
        .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 50%;
    }
    /* The Close Button */
    .close {
        color: #8e0f0f;
        float: right;
        font-size: 28px;
        font-weight: bold;
        margin: -20px -10px -10px 0px;
    }
    .close:hover,.close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }
    #internship-description{
        text-align: center;
        font-size: 16px;
        padding: 10px;
    }
    thead input {
        width: 100%;
    }
    table{
        margin: 0 auto;
        width: 100%;
        clear: both;
        border-collapse: collapse;
        table-layout: fixed; // ***********add this
        word-wrap: break-word;
        word-break: break-all;
        white-space: normal;
    }
</style>
<div class="container" style="margin:30px auto; background:#fbfbfb">
    <div class="row">
        <div class="col-md-12 text-center frnd-search"><h3>Internships</h3></div>

        <div class="col-md-12 text-center" style="text-align:left; margin:0px auto;" id="internships_result">
            <table id="example" class="display" style="width:100%; font-size:15px; padding-top:15px;">
                <thead>
                    <tr>
                        <th>University</th>
                        <th>College</th>
                        <th>Department</th>
                        <th>Title</th>
                        <th>Breif</th>
                        <th>Application Deadline</th>
                        <th>Internship Start Date</th>
                        <th>Internship End Date</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach($internships as $index => $internship)
                    {
                        ?>
                        <tr>
                            <td><?=$internship->university_name?></td>
                            <td><?=$internship->college_name?></td>
                            <td><?=$internship->department_name?></td>
                            <td><?=$internship->title?></td>
                            <td><?=substr($internship->description, 0, 200) . "...<br><a href='#' onclick=\"showModal('internship-$index')\" style='color:#337ab7;'>Read More</a>"?></td>
                            <td><?=$internship->application_deadline?></td>
                            <td><?=$internship->internship_start_date?></td>
                            <td><?=$internship->internship_end_date?></td>
                            <td><a href="/internship/<?=$internship->id?>" style="color:#337ab7;"><button>Apply</button></a></td>
                            <input type="hidden" value="<?=$internship->description?>" id="internship-<?=$index?>">
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>University</th>
                        <th>College</th>
                        <th>Department</th>
                        <th>Title</th>
                        <th>Breif</th>
                        <th>Application Deadline</th>
                        <th>Internship Start Date</th>
                        <th>Internship End Date</th>
                        <th>&nbsp;</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<div id="myModal" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>
        <div id="internship-description"></div>
    </div>
</div>

<script>
    function search(){
        var searchStr = document.getElementById("search_str").value;
        if(searchStr.length >= 3){
            $.ajax({
                url: '/internships?str=' + searchStr,
                success: function(data){
                    var obj = JSON.parse(data);
                    document.getElementById("internships_result").innerHTML = '';
                    var html = '<h3>Internships</h3>';
                    for(var i in obj){
                        html += '<ul class="testimonial-list owl-carousel1 owl-theme tpage"><li><div class="internship-item "><div class="profile-circle"><ul><li><strong>' + obj[i].topic + '</strong></li><li>Department : ' + obj[i].department_name + ' </li><li>College  : ' + obj[i].college_name + ' </li><li>University : ' + obj[i].university_name + ' </li><li>Internship Page : <a href="' + obj[i].internship_url + '" target="_blank">' + obj[i].internship_url + '</a> </li></ul></div><div class="internship-description">' + obj[i].description + '</div></div></li></ul>';
                    }
                    document.getElementById("internships_result").innerHTML = html;
                }
            })
        }
    }

    $(document).ready(function() {
        $('#example thead tr').clone(true).appendTo( '#example thead' );
        $('#example thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text"  />' );
            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                    .column(i)
                    .search( this.value )
                    .draw();
                }
            });
        });

        var table = $('#example').DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            "columnDefs": [
                { "width": "10%", "targets": 0 },
                { "width": "14%", "targets": 1 },
                { "width": "15%", "targets": 2 },
                { "width": "10%", "targets": 3 },
                { "width": "14%", "targets": 4 },
                { "width": "10%", "targets": 5 },
                { "width": "10%", "targets": 6 },
                { "width": "10%", "targets": 7 },
                { "width": "7%", "targets": 7 }
            ]
        });
    });

    var modal = document.getElementById("myModal");
    var span = document.getElementsByClassName("close")[0];

    function showModal(internshipId) {
        document.getElementById("internship-description").innerHTML = document.getElementById(internshipId).value;
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

</script>
