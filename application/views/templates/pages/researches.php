<!--div><img src="images/university-header.jpg" width="100%"> </div-->
<style type="text/css">
    .flLt {
   float: left;
   }
.uni-image {
   background: #fff;
   border: 1px solid #f3f3f3;
   padding: 5px;
   position: relative;
   width: 160px;
   height: 145px;
   }

.uni-boxPurple{
   float: left;
   width: 100%;
   padding: 8px;
   position: relative;
   background-color:#ffffff;
   /*border:1px solid #f0f0f0;*/
   box-shadow: 3px 4px 6px #ccccc4;
   margin: 20px 0px;
   border-right: 7px solid #8c57c4;
   }
   .uni-boxPurple p{
   margin:0px;
   line-height:unset;
   font-size: 13px;
   }
   .uni-detail {
   margin-left: 160px;
   }
   .uni-title {
   margin-bottom: 10px;
   width: 100%;
   }
   .uni-title a {
   font-weight: 600;
   font-size: 13px;
   color: #111111;
   }
   .uni-title span {
   color: #666666;
   }
   .uni-sub-title {
   font-weight: 600;
   font-size: 15px;
   color:#666666;
   }
   a.uni-sub-title {
   color:#666666;
   }

   .iconRowPurple .iconCss{
   background: #8c57c4;
   padding: 7px;
   height: 30px;
   width: 30px;
   border-radius: 50%;
   color: white;
   font-size: 15px;
   margin: 5px 0px 5px 0px;
   }
   .iconRowPurple strong {
   font-size: 13px;
   color: #8c57c4;
   }
   .iconRowPurple span {
   font-size: 13px;
   color: #000;
   }
   .font14{
   font-size: 14px;
   }
   .mrgtop{
   margin: 9px 0px 5px 0px; ;
   }

   .btn-brochure a {
     padding: 8px 18px 9px!important;
   }

   .twoPXPadding{
    padding-right: 2px !important;
    padding-left: 2px !important;
   }

   .faPadding{
   padding-right: 0px !important;
    padding-left: 22px !important;
   }

   .hrRow>hr{
   margin-top: 12px;
   margin-bottom: 12px;
   border: 0;
   border-top: 2px solid #000;
   }

   .heart{
    font-size: 18px !important;
    margin: 0px 8px;
    color:red;
}

</style>
<div class="shade_border"></div>
<div class="container">
    <div class="row">
        <div class="col-lg-12" style="color:#000;">
            <h4>Research</h4>

            <?php
            /*
            foreach($researches as $index => $research)
            {
                if($research->topic)
                {
                    ?>
                    <tr>
                        <td><?=$research->topic?></td>
                        <td><?=$research->department_name?></td>
                        <td><?=$research->college_name?></td>
                        <td><?=$research->university_name?></td>
                        <td><?=substr($research->description, 0, 200) . "...<br><a href='#' onclick=\"showModal('research-$index')\" style='color:#337ab7;'>Read More</a>"?></td>
                        <td><a href="<?=$research->research_url?>" target="_blank" style="color:#337ab7;"><?=$research->research_url?></a></td>
                        <input type="hidden" value="<?=$research->description?>" id="research-<?=$index?>">
                    </tr>
                    <?php
                }
            }
            */
            foreach($researches as $research)
            {
                ?>
                <div class="col-md-12" >
                   <div class="uni-boxPurple">
                      <div class="flLt">
                         <div class="uni-image">
                            <a target="_blank" href="/research_Details">
                              <img class="lazy" data-original="<?=$research['banner']?>" alt="<?=$research['university_name']?>" title="<?=$research['university_name']?>" align="center" width="172" height="115" src="<?=$research['banner'] ? $research['banner'] : '/uploads/banner.jpg'?>" style="display: inline;"></a>
                            <div class="uni-shrtlist-image"></div>
                         </div>
                      </div>
                      <div class="uni-detail">
                         <div class="uni-title">
                            <p>
                                <a href="/research_Details" id="research-<?=$research['id']?>"><span class="font-16" style="color: #247bcc"> <?=$research['topic']?> </span></a>
                            </p>
                         </div>
                         <div class="clearwidth">
                            <div class="iconRowPurple">
                              <div class="col-md-12 ">
                                 <div class="col-md-4 col-sm-12 col-xs-12">
                                     <div class="col-md-2 col-sm-2 col-xs-2"><i class="fas fa-school iconCss" >&nbsp;&nbsp;</i></div>
                                     <div class="col-md-10 col-sm-10 col-xs-10"><strong>  University Name <span>&nbsp; <?=$research['university_name']?></span></strong></div>
                                  </div>
                                  <div class="col-md-4 col-sm-12 col-xs-12">
                                     <div class="col-md-2 col-sm-2 col-xs-2"><i class="far fa-clock iconCss" >&nbsp;&nbsp;</i></div>
                                     <div class="col-md-10 col-sm-10 col-xs-10" style="margin-top: 10px;"><strong>  College Name <span>&nbsp; <?=$research['college_name']?></span></strong></div>
                                  </div>
                                  <div class="col-md-4 col-sm-12 col-xs-12">
                                     <div class="col-md-2 col-sm-2 col-xs-2"><i class="far fa-calendar-alt iconCss"></i></div>
                                     <div class="col-md-10 col-sm-10 col-xs-10" style="margin-top: 10px;"><strong>  Department Name <span>&nbsp; <?=$research['department_name']?></span></strong></div>
                                  </div>
                              </div>

                              <!-- <div class="col-md-12 twoPXPadding" style="margin-top: 8px !important;    padding-bottom: 9px;border-bottom: 2px solid black;">
                                  <div class="col-md-4 twoPXPadding">
                                     <div class="col-md-2"><i class="fas fa-money-check-alt iconCss"></i></div>
                                     <div class="col-md-10 faPadding"><strong>  Application Fees <span>&nbsp; $34,160.00 (<i class="fa fa-inr"></i>26,05,981)  per year</span></strong></div>
                                  </div>
                                  <div class="col-md-4 twoPXPadding">
                                     <div class="col-md-2"><i class="fa fa-money iconCss" >&nbsp;&nbsp;</i></div>
                                     <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong>  Salary Expected<span>&nbsp; yes</span></strong></div>
                                  </div>
                                  <div class="col-md-4 twoPXPadding">
                                     <div class="col-md-2"><i class="fas fa-university iconCss"></i></div>
                                     <div class="col-md-10 faPadding" style="margin-top: 10px;"><strong>  Stay Back Option <span>&nbsp; yes</span></strong></div>
                                  </div>
                              </div> -->
                            </div>

                         </div>
                         <div class="flLt hrRow" style="width:100%;"><hr></div>
                          <div class="row" style="margin-top: 15px !important">
                                <div class="col-md-3 btn-col btn-brochure" style="text-align: left;">
                                   <a href="/research_Details/<?=$research['id']?>" class="button-style" target="_blank" style="background-color: #8c57c4;">View Detail</a>
                                </div>
                                <div class="col-md-4 font14 mrgtop txtAlignRight pos">
                                   <i class="fa fa-eye" >&nbsp;<?=$research['research_view_count']?></i> | &nbsp;


                                   <i class="fa fa-heart" >&nbsp;<?=$research['research_likes_count']?></i> | &nbsp;


                                  <i class="fa fa-star" >&nbsp;<?=$research['research_fav_count']?></i>

                                </div>
                                <div class="col-md-2 compare-box flRt customInputs float-right mrgtop">
                                  <?php
                                  if($research['likes_link'] == 1){
                                  ?>
                                <i class="heart fa fa-heart" style="pointer-events: none;"> </i>
                              <?php } else { ?>
                                <i class="heart fa fa-heart-o" onclick="likesAdd(<?=$research['id']?>,'researches','<?=$research['topic']?>')"> </i>
                              <?php } ?>
                                 <!--   <input type="checkbox" name="compare_university[]" value="<?=$university['id']?>" onclick="openComparePopup()"> -->
                                   <span class="common-sprite"></span>
                                </div>
                          </div>
                      </div>
                      <?php
                         if(isset($research['favourites_link']) && !empty($research['favourites_link']))
                         {
                             ?>
                      <i title="Saved to favourites" class="saved-fav added-shortlist" id="fav-course-<?=$research['id']?>" onclick="removeResearchFavourite(<?=$research['id']?>, <?=$research['fav_id']?>)"></i>
                      <?php
                         }
                         else
                         {
                             ?>
                      <i title="Click to save" class="cate-sprite add-shortlist" onclick="addResearchFavourite(<?=$research['id']?>)" id="fav-course-<?=$research['id']?>" ></i>
                      <?php
                         }
                         ?>

                      <div class="clearfix"></div>

                   </div>
               </div>
                <!-- <div  class="uni-box">
                    <div class="flLt">
                        <div class="uni-image">
                            <a target="_blank" href="javascript:void(0);"><img class="lazy" data-original="<?=$research->banner?>" alt="<?=$research->university_name?>" title="<?=$research->university_name?>" align="center" width="172" height="115" src="<?=$research->banner ? $research->banner : '/uploads/banner.jpg'?>" style="display: inline;"></a>
                            <div class="uni-shrtlist-image"></div>
                        </div>
                    </div>
                    <div class="uni-detail">
                        <div class="uni-title">
                            <p>
                                <a href="#" id="research-<?=$research->id?>"><span class="font-16" style="color: #247bcc"> <?=$research->topic?> </span></a>
                            </p>
                        </div>
                        <div class="course-touple clearwidth">
                            <div class="clearwidth">
                                <div class="uni-course-details flLt">
                                    <div class="detail-col flLt" style="width:33%;">
                                        <strong><i class="fa fa-users orangeTag" >&nbsp;</i>University</strong>
                                        <p><?=$research->university_name?></p>
                                    </div>
                                    <div class="detail-col flLt" style="width:17%;">
                                        <strong><i class="fa fa-line-chart orangeTag">&nbsp;</i>College</strong>
                                        <p><?=$research->college_name?></p>
                                    </div>
                                    <div class="detail-col flLt" style="width:17%;">
                                        <strong><i class="fa fa-line-chart orangeTag">&nbsp;</i>Department</strong>
                                        <p><?=$research->department_name?></p>
                                    </div>
                                </div>
                                <div class="btn-col btn-brochure" style="margin-top: 27px !important">
                                    <a href="/<?=$university->slug?>" class="button-style" target="_blank">View Detail</a>
                                </div>
                            </div>
                            <div class="flLt" style="width:83%"></div>
                            <div class="row">
                                <div class="col-md-3">
                                    <strong><i class="fa fa-eye orangeTag" >&nbsp;</i> Views</strong>
                                    <p>100</p>
                                </div>
                                <div class="col-md-3">
                                    <strong><i class="fa fa-heart orangeTag" >&nbsp;</i> Favourites</strong>
                                    <p>220</p>
                                </div>
                                <div class="col-md-3">
                                    <strong><i class="fa fa-star orangeTag" >&nbsp;</i> Reviews</strong>
                                    <p>6</p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div> -->
                <?php
            }
            ?>
        </div>
    </div>
</div>

<div id="myModal" class="modal">

 <!-- Modal content -->
 <div class="modal-content">
   <span class="close">&times;</span>
   <div id="compare-university-modal"></div>
 </div>

</div>
<style>
.font-16 {
   font-size: 16px;
   letter-spacing: 0.5px;
   color: #247bcc !important;
   }
   .orangeTag{
   color: #f6882c;
   }
.sort-filter {
    font-size: 16px;
    line-height: 1.6;
    margin:0 0 10px;
    color:#000;
}

.font-11 {
    font-size: 12px;
}

.uni-image {
    background: #fff;
    border: 1px solid #f3f3f3;
    padding: 5px;
    position: relative;
    width: 184px;
}
ul.uni-cont li {
    background: #fbfbfb;
    border: 1px solid #f3f3f3;
    margin-bottom: 8px;
    list-style: none;
}
.clearwidth {
    width: 100%;
    float: left;
}
.univ-tab-details {
    margin: 30px 0 10px 0!important;
}

.flLt {
    float: left;
}
.flRt {
    float: right;
}

.uni-box {
    float: left;
    width: 100%;
    padding: 8px;
    position: relative;
    background-color: #fbfbfb;
    /* border: 1px solid #f0f0f0; */
    box-shadow: 2px 2px 4px #ccccc4;
    margin: 20px 0px;
}
.uni-box p {
    margin:0px;
    line-height:unset;
}
.uni-detail {
    margin-left: 208px;
}
.uni-title {
    margin-bottom: 10px;
    width: 90%;
}
.uni-title a {
    font-weight: 600;
    font-size: 13px;
    color: #111111;
}
.uni-title span {
    color: #666666;
}
.uni-sub-title {
    font-weight: 600;
    font-size: 15px;
    color:#666666;
}
a.uni-sub-title {
    color:#666666;
}
.uni-course-details {
    width: 65%;
    border-right: 1px solid #dadada;
    background: #fff;
    margin: 8px 0;
    padding: 8px;
    position: relative;
    margin-right: 25px;
}
.uni-course-details:before {
    width: 100%;
    height: 1px;
    right: -1px;
    bottom: -1px;
    background-image: -webkit-gradient(linear,0 100%,0 0,from(#fff),to(#dadada));
    background-image: -webkit-linear-gradient(right,#dadada,#fff);
    background-image: -moz-linear-gradient(right,#dadada,#fff);
    background-image: -o-linear-gradient(#fff,#dadada);
}
.uni-course-details:after, .uni-course-details:before {
    content: "";
    position: absolute;
}
.detail-col {
    width: 145px;
    font-size: 13px;
    color: #111111;
}
.detail-col p {
    margin-bottom: 3px;
}
.detail-col strong {
    color: #666666;
    margin-bottom: 5px;
    display: block;
    font-weight: 400;
}
.btn-brochure a {
    padding: 8px 18px 9px!important;
}
a.button-style {
    padding: 6px 9px;
}
.compare-box {
    font-size: 14px;
    color: #000;
    /*width: 140px;*/
    line-height: 15px;
    text-align: right;
}
.compare-box p{
    display:inline;
    margin:0px;
}

div {
    background: 0 0;
    border: 0;
    margin: 0;
    outline: 0;
    padding: 0;
    vertical-align: baseline;
}
.sponsered-text {
    color: #999;
    font-size: 12px;
    font-style: italic;
    margin: 15px 0 5px 0;
    display: none;
}
.tar {
    text-align: right;
}
.button-style, .login-btn, a.button-style, a.login-btn {
    background: #F6881F;
    border: 1px solid transparent;
    padding: 5px 10px;
    color: #fff;
    font-size: 14px;
    text-decoration: none;
    vertical-align: middle;
    display: inline-block;
}
.add-shortlist, .added-shortlist {
	background-position: 0 -51px;
	width: 78px;
	height: 77px;
	position: absolute;
	top: 0;
	right: 0px;
	cursor: pointer;
}

.cate-sprite {
    background: url(/images/save-fav-btn-white.png);
    display: inline-block;
    font-style: none;
    vertical-align: middle;
}


/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 30%;
}

/* The Close Button */
.close {
  color: #8e0f0f;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

#compare-university-modal{
	text-align: center;
}

#compare-university-modal p{
	font-size: 16px;
    font-weight: bold;
	margin: 0 0 15px;
}

#compare-university-modal label{
	color:#000;
	font-size: 13px;
	margin-top: 10px;
	font-weight: 500;
}

.compare_universities_submit{
	text-align: center;
    font-size: 15px;
	margin-top: 10px;
}

#compare-university-modal input[type='submit']{
	padding: 5px;
}

.saved-fav {
	background: url(/images/save-fav-btn.png);
}

</style>

<script>
$( document ).ready(function() {
	$("#mysort").change(function(){
		window.location.href = '/university/?sort=' + $( "#mysort option:selected" ).val();
	});

});

var modal = document.getElementById("myModal");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
function openComparePopup() {
	var favorite = [];
	var htmlContent = '<p>Following Universities Are Going To Be Compared.</p>';
    $.each($("input[name='compare_university[]']:checked"), function(){
        favorite.push($(this).val());
		htmlContent += '<label>' + document.getElementById("university-" + $(this).val()).innerHTML + '</label><br/>';
    });
	htmlContent += '<div class="compare_universities_submit"><input type="button" id="compare_universities_submit" name="compare_universities_submit" value="Compare" onclick="submitForm()"></div>';
	if(favorite.length > 1){
		document.getElementById("compare-university-modal").innerHTML = htmlContent;
		modal.style.display = "block";
	}
}

function submitForm(){
	var form = document["compare-university-form"];
	form.submit();
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

function goToLogin(){
	window.location.href="/user/account";
}

function addResearchFavourite(researchId){
  jQuery.ajax({
        type: "GET",
        url: "/user/socialcounts/favAdd/" + researchId + "/researches",
      success: function(response) {
            if(response == 'login'){
              window.location.href='/user/account'
            } else {
              alert("The Research has been added successfully in your favourites");
                document.getElementById("fav-course-"+researchId).className = "saved-fav added-shortlist";
                var onclickFunction = 'removeResearchFavourite(' + researchId + ',' + response + ')';
                document.getElementById("fav-course-"+researchId).setAttribute('onclick', onclickFunction);
            }
        }
    });
}

function removeResearchFavourite(researchId, favId){
  jQuery.ajax({
        type: "GET",
        url: "/user/socialcounts/favDelete/" + favId + "/researches",
      success: function(response) {
           alert("The Research has been removed successfully from your favourites");
             document.getElementById("fav-course-"+researchId).className = "cate-sprite add-shortlist";
             var onclickFunction = 'addResearchFavourite(' + researchId + ')';
             document.getElementById("fav-course-"+researchId).setAttribute('onclick', onclickFunction);
        }
    });
}

function likesAdd(entity_id,entity_type,entity_name){

  var sataString = "";

  $.ajax({

  type: "POST",

  url: "/user/socialcounts/likesAdd/"+entity_id+"/"+entity_type,

  data: sataString,

  cache: false,

  success: function(result){
    if(result == 'login'){
      window.location.href='/user/account'
    } else {

      alert("Thank You For Liking " + entity_name);

    }

  }

});

}

$(document).ready(function(){

if ($("i").hasClass("fa-heart-o")){
 // Do something if class exists
    $(".heart.fa-heart-o").click(function() {
      $(this).removeClass('fa-heart-o');
      $(this).addClass('fa-heart');
      $(".fa-heart").append('<style>{pointer-events:none;}</style>');
    });
} else {
    // Do something if class does not exist
   // $(".fa-heart").css({"pointer-events": "none !important"});
   $('.fa-heart').append('<style>{pointer-events:none !important;}</style>');
}

});
</script>
