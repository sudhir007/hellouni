<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<script type="text/javascript" src="
https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<style type="text/css">
   .styleCss{
   font-size: 14px;
   background-color: #f6882c;
   color: white;
   font-weight: 600;
   letter-spacing: 0.8px;
   }
   .formControl{
   color: #fff !important;
   background-color: #8c57c5 !important;
   /* border: 0px; */
   font-size: 12px;
   border: 0px !important;
   box-shadow: inset 0 0 0 rgba(0,0,0,.075) !important;
   margin-top: 15px;
   }

   #dt-example td.details-control {
     cursor: pointer;
}
 #dt-example tr.shown td {
     background-color: #815dd5;
     color: #fff !important;
     border: none;
     border-right: 1px solid #815dd5;
}
 #dt-example tr.shown td:last-child {
     border-right: none;
}
 #dt-example tr.details-row .details-table tr:first-child td {
     color: #fff;
     background: #f6882c;
     border: none;
}
 #dt-example tr.details-row .details-table > td {
     padding: 0;
}
 #dt-example tr.details-row .details-table .fchild td:first-child {
     cursor: pointer;
}
 #dt-example tr.details-row .details-table .fchild td:first-child:hover {
     background: #fff;
}
 #dt-example .form-group.agdb-dt-lst {
     padding: 2px;
     height: 23px;
     margin-bottom: 0;
}
 #dt-example .form-group.agdb-dt-lst .form-control {
     height: 23px;
     padding: 2px;
}
 #dt-example .adb-dtb-gchild {
     padding-left: 2px;
}
 #dt-example .adb-dtb-gchild td {
     background: #f5fafc;
     padding-left: 15px;
}
 #dt-example .adb-dtb-gchild td:first-child {
     cursor: default;
}
 #dt-example .fchild ~ .adb-dtb-gchild {
    /* display: none;
     */
}
 .dataTables_wrapper{
    overflow: auto;
 }

.listTableView thead tr{
 background-color: #f6882c;
    color: white;
}

/* The Modal (background) */
.modal {
display: none; /* Hidden by default */
position: fixed; /* Stay in place */
z-index: 1; /* Sit on top */
padding-top: 100px; /* Location of the box */
left: 0;
top: 0;
width: 100%; /* Full width */
height: 100%; /* Full height */
overflow: auto; /* Enable scroll if needed */
background-color: rgb(0,0,0); /* Fallback color */
background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}
/* Modal Content */
.modal-content {
background-color: #fefefe;
margin: auto;
padding: 20px;
border: 1px solid #888;
    width: 40%;
 z-index: 10000;
}

#fairModal .modal-content {
background-color: #fefefe;
margin: auto;
padding: 20px;
border: 1px solid #888;
    width: auto;
 z-index: 10000;
}

.fairModal a.pre-order-btn {
  color: #000;
  background-color: gold;
  border-radius: 1em;
  padding: 1em;
  display: block;
  margin: 1.5em auto;
  width: 50%;
  font-size: 1.25em;
  font-weight: 6600;
}
.fairModal a.pre-order-btn:hover {
  background-color: #3c1e4c;
  text-decoration: none;
  color: gold;
}
/* The Close Button */
.close {
color: #8e0f0f;
float: right;
font-size: 28px;
font-weight: bold;
}
.close:hover,
.close:focus {
color: #000;
text-decoration: none;
cursor: pointer;
}
</style>
<div class="container">
   <div class="row" style="padding: 30px 0px 30px 0px;display: flex;">

      <div class="col-md-12" >
        <div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-body listTableView">
          <div class="form-check" style="text-align: right;">
            <!-- <button type="submit" class="btn btn-login" ng-click="submitForm()">Submit</button> -->
             <a  style="text-align: center; border-radius:5px;" class="btn btn-md btn-warning" href="/student/profile/<?=$user_id?>"> SKIP </a>
              <a   style="text-align: center; border-radius:5px;" class="btn btn-md btn-info" href="/student/profile/<?=$user_id?>"> FINISH </a>
          </div>
          <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?=$user_id?>">
          <br/>
<!-- Dynamic table was here -->
<table id="dt-example" class="stripe row-border order-column cl-table dataTable no-footer" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th>Name</th>
      <th>Country</th>
      <th>Placement Percentage</th>
      <th>Establishment Year</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>
  </div>

 </div>
</div>
      </div>
   </div>
</div>
<div id="courseDataModal" class="modal">
<!-- Modal content style="display: block;"-->
   <div class="modal-content">
      <span class="close closeFillFairData">&times;</span>
      <div id="fair-data-modal" style="text-align: center;">

        <div class="row">
            <div class="col-md-12 login-sec">
                <h2 class="text-center" style="color: #8c57c5;">Select Course</h2>
                <form class="login-form" class="form-horizontal" onsubmit="applyUniversity()" method="post" autocomplete="off">

        <div class="col-md-12 form-group">
          <div class="col-md-12">
            <label for="inputEmail3" class="text-uppercase"> University Name </label>
             <input type="text" class="form-control" style="width:100%; height:30px; font-size:12px;" name="university_name" id="university_name" >
          </div>
        </div>

        <div class="col-md-12 form-group">
          <div class="col-md-12">
            <label for="inputEmail3" class="text-uppercase"> Select Course </label>
             <input type="text" name="searchtextvalue" id="searchtextvalue" class="form-control search-slt typeahead" data-provide="typeahead" placeholder="Type Course Name & Select From Dropdown" style="width:100%; height:30px; font-size:12px;" >
          </div>
        </div>

        <div class="form-check" style="text-align: center;">

            <input type="submit"  style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Apply"></input>

        </div>


      </form>
  </div>
</div>

      </div>
   </div>


</div>

<script src="<?php echo base_url();?>js/bootstrap-typeahead-min.js"></script>

<script type="text/javascript">

 var searchUniversityId = 0;
 var courseId = 0;

function courseModal(university_id, university_name){

  $("#courseDataModal").modal('show');

  document.getElementById("university_name").value = university_name;

  searchUniversityId = university_id;

  console.log(searchUniversityId);

}

var $input = $(".typeahead");
$input.typeahead({

source: function (query, process) {
   return $.get('/offer/course/textsuggestions/'+searchUniversityId, { query: query }, function (data) {
       return process(data.options);
   });
}

});

$input.change(function() {
  var current = $input.typeahead("getActive");
  if (current) {
    // Some item from your model is active!
    if (current.name == $input.val()) {
      courseId = current.id;
      // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
    } else {
      // This means it is only a partial match, you can either add a new item
      // or take the active if you don't want new items
    }
  } else {
    // Nothing is active so it is a new value (or maybe empty value)
  }
});
</script>
<script type="text/javascript">

var modalFillFairData = document.getElementById("courseDataModal");
var spanFillFair = document.getElementsByClassName("closeFillFairData")[0];

spanFillFair.onclick = function() {
  //modalFillFairData.style.display = "none";
  $("#courseDataModal").modal('hide');
}



function applyUniversity() {

  var university_id = searchUniversityId;
  var course_id = courseId;
  var user_id = $('#user_id').val();

  console.log(course_id);

  event.preventDefault();

    jQuery.ajax({
         type: "GET",
         url: "/university/details/apply/?uid=" + university_id + "&course_id="+ course_id +"&user_id=" + user_id,
     success: function(response) {
        alert("You have applied successfully");
        $("#courseDataModal").modal('hide');
        //window.location.href = '/search/university';
         }
     });
}
        $('#data-table').dataTable();
        $(".dataTable").on("draw.dt", function (e) {
          //console.log("drawing");
          setCustomPagingSigns.call($(this));
        }).each(function () {
          setCustomPagingSigns.call($(this)); // initialize
        });
        function setCustomPagingSigns() {
          var wrapper = this.parent();
          wrapper.find("a.previous").text("<");
          wrapper.find("a.next").text(">");
        }
// =============Display details onclick===============

  var studentId =  $('#user_id').val();
  var data = [];

  //console.log('hell',studentId);

  $.ajax({

  type: "GET",

  url: "/counsellor/university/offerlist/"+ studentId,

  cache: false,

  success: function(result){

    //console.log(result);

   var studentdata = result.offer_list;

   if(studentdata.length){

     studentdata.forEach(function(value, index){

       data.push({
         "Name" : value.name,
         "Country" : value.country_name,
         "Placement_percentage" : value.placement_percentage,
         "establishment_year" : value.establishment_year,
         "Action" : '<input type="button" class="btn btn-mg btn-info" onclick="courseModal(' + value.university_id + ', \'' + value.name + '\')" value="Apply" />'
         //"Action" : '<a class="btn btn-mg btn-info" href="#" onclick="applyUniversity(' + value.university_id + ', \'' + value.name + '\', \'' + value.university_email + '\', ' + studentId + ')"> Apply </a>'

       });

     });

     //console.log(data,"hi");

     var table = $('.dataTable').DataTable({

       // Column definitions
       columns : [
           {
               data : 'Name',
               className : 'details-control',
           },
           {data : 'Country'},
           {data : 'Placement_percentage'},
           {data : 'establishment_year'},
           {data : 'Action'}
       ],
       data : data,
       pagingType : 'full_numbers',
       // Localization
       language : {
         emptyTable     : 'No data to display.',
         zeroRecords    : 'No records found!',
         thousands      : ',',
         // processing     : 'Processing...',
         loadingRecords : 'Loading...',
         search         : 'Search:',
         paginate       : {
           next     : 'next',
           previous : 'previous'
         }
       },
       "bDestroy": true
     });

     $('.dataTable tbody').on('click', 'td.details-control', function () {
        var tr  = $(this).closest('tr'),
            row = table.row(tr);

        if (row.child.isShown()) {
          tr.next('tr').removeClass('details-row');
          row.child.hide();
          tr.removeClass('shown');
        }
        else {
          row.child(format(row.data())).show();
          tr.next('tr').addClass('details-row');
          tr.addClass('shown');
        }
     });

   $('#dt-example').on('click','.details-table tr',function(){
     $('.details-table').find('.adb-dtb-gchild:not(:first-child)').slideToggle();
       });


   }

  }

      });

</script>
