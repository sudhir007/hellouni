<style type="text/css">
@import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");
.login-block {
  background: #004b7a;
  /* fallback for old browsers */
  background: -webkit-linear-gradient(to bottom, #fafafb, #d8d9da);
  /* Chrome 10-25, Safari 5.1-6 */
  background: linear-gradient(to bottom, #fafafb, #d8d9da);
  /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
  float: left;
  width: 100%;
  padding: 50px 0;
}

.banner-sec {
  background: url(<?php echo base_url();
  ?>application/images/register.png) no-repeat left bottom;
  background-size: cover;
  min-height: 600px;
  border-radius: 0 10px 10px 0;
  padding: 0;
  background-position: center;
  margin-top: 25px;
}

.login-sec {
  padding: 50px 30px;
  position: relative;
}

.login-sec .copy-text {
  position: absolute;
  width: 80%;
  bottom: 20px;
  font-size: 13px;
  text-align: center;
}

.login-sec h2 {
  margin-bottom: 30px;
  font-weight: 800;
  font-size: 30px;
  color: #004b7a;
}

.login-sec h2:after {
  content: " ";
  width: 100px;
  height: 5px;
  background: #f9992f;
  display: block;
  margin-top: 20px;
  border-radius: 3px;
  margin-left: auto;
  margin-right: auto
}

.btn-login {
  background: #f9992f;
  color: #fff;
  font-weight: 600;
}

.form-control {
  display: block;
  width: 100%;
  height: 34px;
  padding: 6px 12px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
  background-color: #fff;
  background-image: none;
  border: 1px solid #ccc;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}


/* Style the tab */

.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}


/* Style the buttons inside the tab */

.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}


/* Change background color of buttons on hover */

.tab button:hover {
  background-color: #ddd;
}


/* Create an active/current tablink class */

.tab button.active {
  background-color: #ccc;
}


/* Style the tab content */

.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}

table {
  border-collapse: collapse;
}
tr {
  border-bottom: 1px solid #ccc;
}
th, td {
  text-align: left;
  padding: 4px;
}
</style>
<section class="login-block">
  <div class="container" style="background:#fff; border-radius: 10px; box-shadow: 5px 5px 0px rgba(0,0,0,0.1);">
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6 login-sec">
        <h2 class="text-center"> COUNSELLOR Profile Update </h2>
        <div class="tab">
          <button class="tablinks" onclick="openCity(event, 'personalDetails')" id="defaultOpen">Personal Details</button>
          <?php
          $userData = $this->session->userdata('user');
          if($userData['type'] == 8 ) {
          ?>
          <button class="tablinks" onclick="openCity(event, 'documentUpload')"> Upload Document </button>
          <button class="tablinks" onclick="openCity(event, 'paymentDetails')"> Payment Details</button>
        <?php } ?>

        </div>
        <div id="personalDetails" class="tabcontent">
          <h3>Edit Personal Details</h3>
          <form class="login-form form-horizontal" onsubmit="updateProfile()" method="post">
            <div class="col-md-12 form-group">
              <div class="col-md-12">
                <label for="inputEmail3" class="text-uppercase"> Name * </label>
                <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Name" value="<?=$profile_detail['name']?>" required>
                <input type="hidden" id="user_id" name="user_id" value="<?=$profile_detail['user_id']?>" /> </div>
            </div>
            <div class="col-md-12 form-group">
              <div class="col-md-12">
                <label for="inputEmail3" class="text-uppercase">Email * </label>
                <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email" value="<?=$profile_detail['email']?>" required> </div>
            </div>
            <div class="col-md-12 form-group">
              <div class="col-md-12">
                <label for="inputEmail3" class="text-uppercase">Mobile Number ( WhatsApp ) * </label>
                <input type="text" class="form-control" id="user_mobile" name="user_mobile" value="<?=$profile_detail['phone']?>" placeholder="Mobile Number" required> </div>
            </div>
            <div class="col-md-12 form-group">
              <div class="col-md-12">
                <label for="inputEmail3" class="text-uppercase"> Password * </label>
                <input type="password" class="form-control" id="user_password" name="user_password" placeholder="Password"> </div>
            </div>

            <?php if($profile_detail['type'] == 9 || $profile_detail['type'] == 10) {?>
            <div class="col-md-12 form-group">
                <div class="col-md-12">
                 <label for="inputEmail3" class="text-uppercase"> Role * </label>
                 <select name="user_role" id="user_role" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required onchange="getCity();">
                     <option value="">Select Role</option>
                     <option value="9" <?php if($profile_detail['type'] == 9){ echo "selected";}?>>Counselor Member</option>
                     <option value="10" <?php if($profile_detail['type'] == 10){ echo "selected";}?>> Application Counselor Member </option>
                  </Select>
                </div>

              </div>
            <?php } ?>

            <?php if($profile_detail['type'] == 8) {?>
              <div class="col-md-12 form-group">
                <div class="col-md-12">
                  <label for="inputEmail3" class="text-uppercase">Name of Agency *</label>
                  <input type="text" class="form-control" id="agency_name" name="agency_name" placeholder="Agency Name" value="<?php echo $profile_detail['other_details']['agency_name'];?>" required> </div>
              </div>
              <?php } ?>
                <div class="col-md-12 form-group">
                  <div class="col-md-12">
                    <label for="inputEmail3" class="text-uppercase"> State * </label>
                    <select name="sstate" id="sstate" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required onchange="getCity();">
                      <option value="">Select State</option>
                      <?php

                            foreach ($state_list as $key => $value) {
                              // code...
                              $selected = "";
                              if($value['zone_id'] == $profile_detail['state']){
                                $selected = "SELECTED";
                              }
                              echo "<option value='".$value['zone_id']."' $selected >". $value['name']."</option>";

                            }
                            ?>
                    </Select>
                  </div>
                </div>
                <div class="col-md-12 form-group">
                  <div class="col-md-12">
                    <label for="inputEmail3" class="text-uppercase"> City * </label>
                    <select name="ccity" id="ccity" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                      <?php

                          foreach ($city_list as $keyc => $valuec) {
                            // code...
                            $selectedc = "";
                            if($valuec['id'] == $profile_detail['city']){
                              $selectedc = "SELECTED";
                            }
                            echo "<option value='".$valuec['id']."' $selectedc >". $valuec['city_name'] ."</option>";

                          }

                          ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-12 form-group">
                  <div class="col-md-12">
                    <label for="inputEmail3" class="text-uppercase"> Address </label>
                    <textarea name="user_add" id="user_add" class="form-control" style="font-size:12px;" rows="4" cols="50"> <?=$profile_detail['address']?> </textarea>
                  </div>
                </div>
                <div class="form-check" style="text-align: center;">
                  <!-- <button type="submit" class="btn btn-login" ng-click="submitForm()">Submit</button> -->
                  <button type="button" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                  <input type="submit" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Update"></input>
                </div>
          </form>
        </div>
        <div id="documentUpload" class="tabcontent">
          <h3>Upload Your Document</h3>
          <table class="table">
            <tr>
              <th>Document Type</th>
              <th>Upload</th>
              <th>Status</th>
              <th>Submit</th>
            </tr>
            <tr>
              <td>KYC of agent - PAN card (company)</td>
              <td>
                <input type="file" name="document47" id="document47">
              </td>
              <td><?php
              foreach ($doc_list as $keyd => $valued1) {
                if($valued1['document_meta_id'] === 47 || $valued1['document_meta_id'] === "47"){
                  echo "<a href='".$valued1['url']."' style='color:black;' target='_blank'> View </a>";
                } else{
                  echo "PENDING";
                }
              }?></td>
              <td><button type="button" onclick="uploadAppDoc(47,<?=$profile_detail["user_id"]?>)"> <i class="fas fa-upload">&nbsp;</i>Upload </button> </td>
            </tr>
            <tr>
              <td>Registration certification/ partnership deed</td>
              <td>
                <input type="file" name="document48" id="document48">
              </td>
              <td><?php foreach ($doc_list as $keyd2 => $valued2) {
                if($valued2['document_meta_id'] == 48 ){
                  echo "<a href='".$valued2['url']."' style='color:black;' target='_blank'> View </a>";
                } else{
                  echo "PENDING";
                }
              }?></td>
              <td><button type="button" onclick="uploadAppDoc(48,<?=$profile_detail["user_id"]?>)"> <i class="fas fa-upload">&nbsp;</i>Upload </button> </td>
            </tr>
            <tr>
              <td>Proof of operation (electricity bill / ghumasta license / municipal coperation)</td>
              <td>
                <input type="file" name="document49" id="document49">
              </td>
              <td><?php foreach ($doc_list as $keyd3 => $valued3) {
                if($valued3['document_meta_id'] == 49 ){
                  echo "<a href='".$valued3['url']."' style='color:black;' target='_blank'> View </a>";
                } else{
                  echo "PENDING";
                }
              }?></td>
              <td><button type="button" onclick="uploadAppDoc(49,<?=$profile_detail["user_id"]?>)"> <i class="fas fa-upload">&nbsp;</i>Upload </button> </td>
            </tr>
            <tr>
              <td>GST Document (if available)</td>
              <td>
                <input type="file" name="document50" id="document50">
              </td>
              <td><?php foreach ($doc_list as $keyd4 => $valued4) {
                if($valued4['document_meta_id'] == 50 ){
                  echo "<a href='".$valued4['url']."' style='color:black;' target='_blank'> View </a>";
                } else{
                  echo "PENDING";
                }
              }?></td>
              <td><button type="button" onclick="uploadAppDoc(50,<?=$profile_detail["user_id"]?>)"> <i class="fas fa-upload">&nbsp;</i>Upload </button> </td>
            </tr>
            <tr>
              <td> cancelled cheque (company)</td>
              <td>
                <input type="file" name="document51" id="document51">
              </td>
              <td><?php foreach ($doc_list as $keyd5 => $valued5) {
                if($valued5['document_meta_id'] == 51 ){
                  echo "<a href='".$valued5['url']."' style='color:black;' target='_blank'> View </a>";
                } else{
                  echo "PENDING";
                }
              }?></td>
              <td><button type="button" onclick="uploadAppDoc(51,<?=$profile_detail["user_id"]?>)"> <i class="fas fa-upload">&nbsp;</i>Upload </button> </td>
            </tr>
          </table>
        </div>
        <div id="paymentDetails" class="tabcontent">
          <h3>Payment Details (Bank account details)</h3>
          <form class="login-form form-horizontal" >
            <div class="col-md-12 form-group">
              <div class="col-md-12">
                <label for="inputEmail3" class="text-uppercase"> Company / Firm Name * </label>
                <input type="text" class="form-control" id="company_name" name="company_name" value="<?=$bank_info['company_name']?>" placeholder="Name"  required />
              </div>
            </div>
            <div class="col-md-12 form-group">
              <div class="col-md-12">
                <label for="inputEmail3" class="text-uppercase">ifsc code * </label>
                <input type="ifsc" class="form-control" id="ifsc_code" name="ifsc_code" placeholder="IFSC" value="<?=$bank_info['ifsc_code']?>" required />
              </div>
            </div>
            <div class="col-md-12 form-group">
              <div class="col-md-12">
                <label for="inputEmail3" class="text-uppercase">Swift code * </label>
                <input type="swift" class="form-control" id="swift_code" name="swift_code" placeholder="Swift" value="<?=$bank_info['swift_code']?>" required />
              </div>
            </div>
            <div class="col-md-12 form-group">
              <div class="col-md-12">
                <label for="inputEmail3" class="text-uppercase">company address </label>
                <textarea name="user_add" id="company_address" class="form-control" style="font-size:12px;" rows="4" cols="50"><?=$bank_info['company_address']?></textarea>
              </div>
            </div>
            <div class="col-md-12 form-group">
              <div class="col-md-12">
                <label for="inputEmail3" class="text-uppercase"> Bank Name * </label>
                <input type="text" class="form-control" id="bank_name" name="bank_name" placeholder="Bank name"  value="<?=$bank_info['bank_name']?>" required />

              </div>
            </div>
            <div class="col-md-12 form-group">
              <div class="col-md-12">
                <label for="inputEmail3" class="text-uppercase">Account number </label>
                <input type="Account" class="form-control" id="account_number" name="account_number" placeholder=""  value="<?=$bank_info['account_number']?>" required />
              </div>
            </div>
            <div class="col-md-12 form-group">
              <div class="col-md-12">
                <label for="inputEmail3" class="text-uppercase">Branch Name</label>
                <input type="branch" class="form-control" id="branch_name" name="branch_name" placeholder=""  value="<?=$bank_info['branch_name']?>" required>
              </div>
            </div>
                <div class="form-check" style="text-align: center;">
                  <!-- <button type="submit" class="btn btn-login" ng-click="submitForm()">Submit</button> -->

                  <input type="hidden" id="user_id" name="user_id" value="<?=$profile_detail['user_id']?>" />

                  <button type="button" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                  <input type="button" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Update" onclick="updateBankInfo()"></input>
                </div>
          </form>
        </div>
      </div>
      <!--div class="col-md-6 banner-sec"></div-->
    </div>
  </div>
</section>
<style>
p {
  font-size: 16px;
  line-height: 1.6;
  margin: 0 0 10px;
}

.italic {
  font-style: italic;
}

.padtb {
  padding: 30px 0px;
}
</style>
<script type="text/javascript">
//profile update
function updateProfile() {

  var user_name = $('#user_name').val();
  var user_email = $('#user_email').val();
  var user_id = $('#user_id').val();
  var user_mobile = $('#user_mobile').val();
  var user_password = $('#user_password').val();
  var agency_name = $('#agency_name').val();
  var sstate = $('#sstate').val();
  var ccity = $('#ccity').val();
  var user_add = $('#user_add').val();
  var user_role =  $('#user_role').val() !== undefined ? $('#user_role').val() : 8;

  var dataString = 'user_name=' + user_name + '&user_email=' + user_email + '&user_mobile=' + user_mobile + '&agency_name='
  + agency_name + '&user_id=' + user_id + '&user_password=' + user_password + '&state_id=' + sstate + '&city_id='
  + ccity + '&user_add=' + user_add + '&user_role=' + user_role;

  event.preventDefault();

  $.ajax({
    type: "POST",
    url: "/counsellor/profile/update",
    data: dataString,
    cache: false,
    success: function(result) {
      if(result == "SUCCESS") {
        alert("Profile Updated Successfully...!!!");
        window.location.href = '/counsellor/team';
      } else if(result == 'With Email-ID Or Mobile Account Already Exists') {
        alert(result);
        //window.location.href='/user/account';
      } else {
        alert(result);
      }
    }
  });
}

//bank Info update
function updateBankInfo() {
  var company_name = $('#company_name').val();
  var ifsc_code = $('#ifsc_code').val();
  var swift_code = $('#swift_code').val();
  var company_address = $('#company_address').val();
  var bank_name = $('#bank_name').val();
  var account_number = $('#account_number').val();
  var branch_name = $('#branch_name').val();
  var user_id = $('#user_id').val();

  var dataString = 'company_name=' + company_name + '&ifsc_code=' + ifsc_code + '&swift_code=' + swift_code + '&company_address=' + company_address + '&bank_name=' + bank_name + '&account_number=' + account_number + '&branch_name=' + branch_name + '&user_id=' + user_id;
  event.preventDefault();
  $.ajax({
    type: "POST",
    url: "/counsellor/profile/updatebankinfo",
    data: dataString,
    cache: false,
    success: function(result) {
      if(result == "SUCCESS") {
        alert("Bank Info Updated Successfully...!!!");
        window.location.reload();
      } else if(result == 'With Email-ID Or Mobile Account Already Exists') {
        alert(result);
        //window.location.href='/user/account';
      } else {
        alert(result);
      }
    }
  });
}
// sent OTP
function getCity() {
  var stateId = $('#sstate').val();
  var dataString = "state_id=" + stateId;
  $.ajax({
    type: "POST",
    url: "/counsellor/registration/cityListByStateId",
    data: dataString,
    cache: false,
    success: function(result) {
      var cityList = result.city_list;
      if(cityList.length) {
        var cityDropDown = '';
        cityList.forEach(function(value, index) {
          cityDropDown += "<option value='" + value.id + "' >" + value.city_name + "</option>";
        });
      }
      cityDropDown += "<option value='1000000' > OTHER </option>";
      document.getElementById("ccity").innerHTML = cityDropDown;
    }
  });
}

function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for(i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for(i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();


function uploadAppDoc(docId,partnerId){

  var fd = new FormData();
        var files = $('#document'+docId)[0].files;

        // Check file selected or not
        if(files.length > 0 ){

           fd.append('document',files[0]);
           fd.append('document_master_id',docId);
           fd.append('user_id',partnerId);
           fd.append('user_type','PARTNER');

           event.preventDefault();

           $.ajax({
              url: '/counsellor/profile/updatedoc',
              type: 'post',
              data: fd,
              contentType: false,
              processData: false,
              success: function(response){
                console.log(response);
                 if(response == 'SUCCESS'){
                    alert('file uploaded');
                 }else{
                    alert('file not uploaded');
                 }
              },
           });
        }else{
           alert("Please select a file.");
        }

}
</script>
