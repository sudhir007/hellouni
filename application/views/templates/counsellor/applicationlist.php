<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<script type="text/javascript" src="
https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<style type="text/css">
   .styleCss{
   font-size: 14px;
   background-color: #f6882c;
   color: white;
   font-weight: 600;
   letter-spacing: 0.8px;
   }
   .formControl{
   color: #fff !important;
   background-color: #8c57c5 !important;
   /* border: 0px; */
   font-size: 12px;
   border: 0px !important;
   box-shadow: inset 0 0 0 rgba(0,0,0,.075) !important;
   margin-top: 15px;
   }

   #dt-example td.details-control {
     cursor: pointer;
}
 #dt-example tr.shown td {
     background-color: #815dd5;
     color: #fff !important;
     border: none;
     border-right: 1px solid #815dd5;
}
 #dt-example tr.shown td:last-child {
     border-right: none;
}
 #dt-example tr.details-row .details-table tr:first-child td {
     color: #fff;
     background: #f6882c;
     border: none;
}
 #dt-example tr.details-row .details-table > td {
     padding: 0;
}
 #dt-example tr.details-row .details-table .fchild td:first-child {
     cursor: pointer;
}
 #dt-example tr.details-row .details-table .fchild td:first-child:hover {
     background: #fff;
}
 #dt-example .form-group.agdb-dt-lst {
     padding: 2px;
     height: 23px;
     margin-bottom: 0;
}
 #dt-example .form-group.agdb-dt-lst .form-control {
     height: 23px;
     padding: 2px;
}
 #dt-example .adb-dtb-gchild {
     padding-left: 2px;
}
 #dt-example .adb-dtb-gchild td {
     background: #f5fafc;
     padding-left: 15px;
}
 #dt-example .adb-dtb-gchild td:first-child {
     cursor: default;
}
 #dt-example .fchild ~ .adb-dtb-gchild {
    /* display: none;
     */
}
 .dataTables_wrapper{
    overflow: auto;
 }

.listTableView thead tr{
 background-color: #f6882c;
    color: white;
}
</style>
<div class="container">
   <div class="row" style="padding: 30px 0px 30px 0px;display: flex;">
      <div class="col-md-3 purpleBg" style="box-shadow: 0px 0px 8px 0px #dcd9d9;    padding: 5px;">
         <div class="col-md-12" style="border-radius: 6px;text-align: center;background-color: white;">
            <h4 style="color: #f6882c;">Filters</h4>
            <a style="text-align: center; border-radius:5px;"  class="btn btn-md btn-info" href="/counsellor/application"> RESET FILTER </a>
         </div>
         <div class="col-md-12">
            <label class="form-control formControl">From Date</label>
            <input type="date" id="fromDate" name="fromDate" class="searchoptions form-control ht30" value="<?=$start_date?>">
         </div>
         <div class="col-md-12">
            <label class="form-control formControl">To Date</label>
            <input type="date" id="toDate" name="toDate" class="searchoptions form-control ht30" onchange="filterdata()" value="<?=$end_date?>">
         </div>

         <div class="col-md-12">
            <label class="form-control formControl">Lead Type *</label>
            <select class="form-control ht30" name="lead_state_type" id="lead_state_type" onchange="filterdata()">
              <option value=""> Select Lead Type </option>
              <?php foreach($lead_state_dropdown as $leadState){?>
                <?php if($leadState['id'] == $lead_state_type) { ?>
                  <option value="<?php echo $leadState['id'];?>" data-badge="" selected> <?php echo strtoupper($leadState['value']);?> </option>
                <?php } else { ?>
              <option value="<?php echo $leadState['id'];?>" data-badge=""> <?php echo strtoupper($leadState['value']);?> </option>
            <?php }
          } ?>
             </select>

         </div>

         <div class="col-md-12">
            <label class="form-control formControl">COUNTRY *</label>
            <select class="form-control ht30" name="countryId" id="countryId" onchange="filterdata()">
              <option value=""> Select Country </option>
               <?php foreach($countries as $country){?>
                 <?php if($country->name == $country_id) { ?>
                   <option value="<?php echo strtoupper($country->name);?>" data-badge="" selected> <?php echo strtoupper($country->name);?> </option>
                 <?php } else { ?>
               <option value="<?php echo strtoupper($country->name);?>" data-badge=""> <?php echo strtoupper($country->name);?> </option>
             <?php }
           } ?>
             </select>

         </div>
         <div class="col-md-12">
            <label class="form-control formControl">INTAKE YEAR</label>
            <select class=" form-control ht30" id="intake_year" name="intake_year" onchange="filterdata()">
               <option value="">Select Intake Year</option>
               <option value="2020" <?php if($intake_year == 2020){ echo "selected";} ?>>2020</option>
               <option value="2021" <?php if($intake_year == 2021){ echo "selected";} ?>>2021</option>
               <option value="2022" <?php if($intake_year == 2022){ echo "selected";} ?>>2022</option>
               <option value="2023" <?php if($intake_year == 2023){ echo "selected";} ?>>2023</option>
            </select>
         </div>

         <div class="col-md-12">
            <label class="form-control formControl">INTAKE MONTH</label>
            <select class=" form-control ht30" id="intake_month" name="intake_month" onchange="filterdata()">
               <option value="">Select Intake Month</option>
               <option value="January" <?php if($intake_month == 'January'){ echo "selected";} ?>>January</option>
               <option value="February" <?php if($intake_month == 'February'){ echo "selected";} ?>>February</option>
               <option value="March" <?php if($intake_month == 'March'){ echo "selected";} ?>>March</option>
               <option value="April" <?php if($intake_month == 'April'){ echo "selected";} ?>>April</option>
               <option value="May" <?php if($intake_month == 'May'){ echo "selected";} ?>>May</option>
               <option value="June" <?php if($intake_month == 'June'){ echo "selected";} ?>>June</option>
               <option value="July" <?php if($intake_month == 'July'){ echo "selected";} ?>>July</option>
               <option value="August" <?php if($intake_month == 'August'){ echo "selected";} ?>>August</option>
               <option value="September" <?php if($intake_month == 'September'){ echo "selected";} ?>>September</option>
               <option value="October" <?php if($intake_month == 'October'){ echo "selected";} ?>>October</option>
               <option value="November" <?php if($intake_month == 'November'){ echo "selected";} ?>>November</option>
               <option value="December" <?php if($intake_month == 'December'){ echo "selected";} ?>>December</option>
            </select>
         </div>

      </div>
      <div class="col-md-9" >
        <div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-body listTableView">
<!-- Dynamic table was here -->
<table id="dt-example" class="stripe row-border order-column cl-table dataTable no-footer" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th>Name</th>
      <th>Email</th>
      <th>Mobile</th>
      <th>Status</th>
      <th>Lead Status</th>
      <th>University</th>
      <th>College</th>
      <th>Course</th>
      <th>Created By</th>
      <th>Created Date</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>
  </div>

 </div>
</div>
      </div>
   </div>
</div>
<script type="text/javascript">

      $(document).ready(function() {

        $('#data-table').dataTable();
        $(".dataTable").on("draw.dt", function (e) {
          //console.log("drawing");
          setCustomPagingSigns.call($(this));
        }).each(function () {
          setCustomPagingSigns.call($(this)); // initialize
        });
        function setCustomPagingSigns() {
          var wrapper = this.parent();
          wrapper.find("a.previous").text("<");
          wrapper.find("a.next").text(">");
        }
// =============Display details onclick===============
var dataList = [
    {"Name":"Deepika","Email":"deepika@gmail.com","Country":"India","created_date":"27.159.97.60","status":"active","calls":"22","duration":"50"},
    {"Name":"Sudhir","Email":"s@gmail.com","Country":"USA","created_date":"67.135.55.135","status":"active","calls":"22","duration":"25"},
    {"Name":"Nikhil","Email":"n@gmail.com","Country":"UK","created_date":"74.193.127.5","status":"active","calls":"22","duration":"56"},
    {"Name":"Banit","Email":"b@gmail.com","Country":"Australia","created_date":"4.104.253.210","status":"active","calls":"22","duration":"22"}
  ];

  var data = [];

  var startDate = $('#fromDate').val();
  var endDate = $('#toDate').val();
  var countryId = $('#countryId').val();
  var intake_month = $('#intake_month').val();
  var intake_year = $('#intake_year').val();
  var leadstatetype = $('#lead_state_type').val();

  var dataString = 'start_date=' + startDate + '&end_date=' + endDate + '&application_status=' + leadstatetype + '&country_id=' + countryId + '&intake_month=' + intake_month + '&intake_year=' + intake_year ;


  $.ajax({

  type: "POST",

  url: "/counsellor/application/filter/list",

  data: dataString,

  cache: false,

  success: function(result){

    console.log(result);

   var studentdata = result.application_list;

   if(studentdata.length){

     studentdata.forEach(function(value, index){

       data.push({
         "Name" : value.name,
         "Email" : value.email,
         "Mobile" : value.phone,
         "lead_state" : value.lead_state,
         "Status" : value.status,
         "University" : value.university_name,
         "College" : value.college_name,
         "Course" : value.course_name,
         "Created_By" : value.createdby,
         "Created_Date" : value.createdate,
         "Action" : "<a class='btn btn-mg btn-info' href='/student/profile/"+ value.user_id +"' > EDIT </a>"

       });

     });

     //console.log(data,"hi");

     var table = $('.dataTable').DataTable({

       // Column definitions
       columns : [
           {
               data : 'Name',
               className : 'details-control',
           },
           {data : 'Email'},
           {data : 'Mobile'},
           {data : 'lead_state'},
           {data : 'Status'},
           {data : 'University'},
           {data : 'College'},
           {data : 'Course'},
           {data : 'Created_By'},
           {data : 'Created_Date'},
           {data : 'Action'}
       ],
       data : data,
       pagingType : 'full_numbers',
       // Localization
       language : {
         emptyTable     : 'No data to display.',
         zeroRecords    : 'No records found!',
         thousands      : ',',
         // processing     : 'Processing...',
         loadingRecords : 'Loading...',
         search         : 'Search:',
         paginate       : {
           next     : 'next',
           previous : 'previous'
         }
       },
       "bDestroy": true
     });

     $('.dataTable tbody').on('click', 'td.details-control', function () {
        var tr  = $(this).closest('tr'),
            row = table.row(tr);

        if (row.child.isShown()) {
          tr.next('tr').removeClass('details-row');
          row.child.hide();
          tr.removeClass('shown');
        }
        else {
          row.child(format(row.data())).show();
          tr.next('tr').addClass('details-row');
          tr.addClass('shown');
        }
     });

   $('#dt-example').on('click','.details-table tr',function(){
     $('.details-table').find('.adb-dtb-gchild:not(:first-child)').slideToggle();
       });


   } else {

     $('#dt-example tbody').empty();

     var table = $('.dataTable').DataTable({

       // Column definitions
       columns : [
           {
               data : 'Name',
               className : 'details-control',
           },
           {data : 'Email'},
           {data : 'Mobile'},
           {data : 'lead_state'},
           {data : 'Status'},
           {data : 'University'},
           {data : 'College'},
           {data : 'Course'},
           {data : 'Created_By'},
           {data : 'Created_Date'},
           {data : 'Action'}
       ],
       data : filterdata,
       pagingType : 'full_numbers',
       // Localization
       language : {
         emptyTable     : 'No data to display.',
         zeroRecords    : 'No records found!',
         thousands      : ',',
         // processing     : 'Processing...',
         loadingRecords : 'Loading...',
         search         : 'Search:',
         paginate       : {
           next     : 'next',
           previous : 'previous'
         }
       },
       "bDestroy": true
     });

     $('.dataTable tbody').on('click', 'td.details-control', function () {
        var tr  = $(this).closest('tr'),
            row = table.row(tr);

        if (row.child.isShown()) {
          tr.next('tr').removeClass('details-row');
          row.child.hide();
          tr.removeClass('shown');
        }
        else {
          row.child(format(row.data())).show();
          tr.next('tr').addClass('details-row');
          tr.addClass('shown');
        }
     });

   $('#dt-example').on('click','.details-table tr',function(){
     $('.details-table').find('.adb-dtb-gchild:not(:first-child)').slideToggle();
       });

   }

  }

      });

});


//filter list function

function filterdata(){


  var startDate = $('#fromDate').val();
  var endDate = $('#toDate').val();
  var countryId = $('#countryId').val();
  var intake_month = $('#intake_month').val();
  var intake_year = $('#intake_year').val();
  var leadstatetype = $('#lead_state_type').val();

  var dataString = 'start_date=' + startDate + '&end_date=' + endDate + '&application_status=' + leadstatetype + '&country_id=' + countryId + '&intake_month=' + intake_month + '&intake_year=' + intake_year ;

  event.preventDefault();

  $.ajax({

  type: "POST",

  url: "/counsellor/application/filter/list",

  data: dataString,

  cache: false,

  success: function(result){

    //console.log(result);

   var studentdata = result.application_list;
   var filterdata = [];

   if(studentdata.length){

     $('#dt-example tbody').empty();

     studentdata.forEach(function(value, index){

       filterdata.push({
           "Name" : value.name,
           "Email" : value.email,
           "Mobile" : value.phone,
           "lead_state" : value.lead_state,
           "Status" : value.status,
           "University" : value.university_name,
           "College" : value.college_name,
           "Course" : value.course_name,
           "Created_By" : value.createdby,
           "Created_Date" : value.createdate,
           "Action" : "<a class='btn btn-mg btn-info' href='/student/profile/"+ value.user_id +"' > EDIT </a>"

       });

     });

     //console.log(filterdata,"hi");

     var table = $('.dataTable').DataTable({

       // Column definitions
       columns : [
           {
               data : 'Name',
               className : 'details-control',
           },
           {data : 'Email'},
           {data : 'Mobile'},
           {data : 'lead_state'},
           {data : 'Status'},
           {data : 'University'},
           {data : 'College'},
           {data : 'Course'},
           {data : 'Created_By'},
           {data : 'Created_Date'},
           {data : 'Action'}
       ],
       data : filterdata,
       pagingType : 'full_numbers',
       // Localization
       language : {
         emptyTable     : 'No data to display.',
         zeroRecords    : 'No records found!',
         thousands      : ',',
         // processing     : 'Processing...',
         loadingRecords : 'Loading...',
         search         : 'Search:',
         paginate       : {
           next     : 'next',
           previous : 'previous'
         }
       },
       "bDestroy": true
     });

     $('.dataTable tbody').on('click', 'td.details-control', function () {
        var tr  = $(this).closest('tr'),
            row = table.row(tr);

        if (row.child.isShown()) {
          tr.next('tr').removeClass('details-row');
          row.child.hide();
          tr.removeClass('shown');
        }
        else {
          row.child(format(row.data())).show();
          tr.next('tr').addClass('details-row');
          tr.addClass('shown');
        }
     });

   $('#dt-example').on('click','.details-table tr',function(){
     $('.details-table').find('.adb-dtb-gchild:not(:first-child)').slideToggle();
       });


   } else {

     $('#dt-example tbody').empty();

     var table = $('.dataTable').DataTable({

       // Column definitions
       columns : [
           {
               data : 'Name',
               className : 'details-control',
           },
           {data : 'Email'},
           {data : 'Mobile'},
           {data : 'lead_state'},
           {data : 'Status'},
           {data : 'University'},
           {data : 'College'},
           {data : 'Course'},
           {data : 'Created_By'},
           {data : 'Created_Date'},
           {data : 'Action'}
       ],
       data : filterdata,
       pagingType : 'full_numbers',
       // Localization
       language : {
         emptyTable     : 'No data to display.',
         zeroRecords    : 'No records found!',
         thousands      : ',',
         // processing     : 'Processing...',
         loadingRecords : 'Loading...',
         search         : 'Search:',
         paginate       : {
           next     : 'next',
           previous : 'previous'
         }
       },
       "bDestroy": true
     });

     $('.dataTable tbody').on('click', 'td.details-control', function () {
        var tr  = $(this).closest('tr'),
            row = table.row(tr);

        if (row.child.isShown()) {
          tr.next('tr').removeClass('details-row');
          row.child.hide();
          tr.removeClass('shown');
        }
        else {
          row.child(format(row.data())).show();
          tr.next('tr').addClass('details-row');
          tr.addClass('shown');
        }
     });

   $('#dt-example').on('click','.details-table tr',function(){
     $('.details-table').find('.adb-dtb-gchild:not(:first-child)').slideToggle();
       });

   }

  }

});

}

</script>
