<style type="text/css">
    @import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");
.login-block{
    background: #004b7a;  /* fallback for old browsers */
background: -webkit-linear-gradient(to bottom, #fafafb, #d8d9da);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to bottom, #fafafb, #d8d9da); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
float:left;
width:100%;
padding : 50px 0;
}
.banner-sec{background:url(<?php echo base_url();?>application/images/register.png)  no-repeat left bottom; background-size:cover; min-height:600px; border-radius: 0 10px 10px 0; padding:0;background-position: center;    margin-top: 25px;}

.login-sec{padding: 50px 30px; position:relative;}
.login-sec .copy-text{position:absolute; width:80%; bottom:20px; font-size:13px; text-align:center;}
.login-sec h2{margin-bottom:30px; font-weight:800; font-size:30px; color: #004b7a;}
.login-sec h2:after{content:" "; width:100px; height:5px; background:#f9992f; display:block; margin-top:20px; border-radius:3px; margin-left:auto;margin-right:auto}
.btn-login{background: #f9992f; color:#fff; font-weight:600;}
.form-control{
  display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}
</style>

<section class="login-block">
    <div class="container" style="background:#fff; border-radius: 10px; box-shadow: 5px 5px 0px rgba(0,0,0,0.1);">
    <div class="row">

        <div class="col-md-6 login-sec">
            <h2 class="text-center"> COUNSELLOR Registration </h2>
            <form class="login-form" class="form-horizontal" onsubmit="resgistration()" method="post">

              <div class="col-md-12 form-group">
                <div class="col-md-12">
                  <label for="inputEmail3" class="text-uppercase"> Name * </label>

                    <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Name" required>

               </div>
              </div>

               <div class="col-md-12 form-group">
                <div class="col-md-6">
                    <label for="inputEmail3" class="text-uppercase">Email * </label>
                    <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email" required>
                  </div>

                  <div class="col-md-6">
                  <label for="inputEmail3" class="text-uppercase">Username * </label><br>
                    <input type="text" class="form-control" id="user_id" name="user_id" placeholder="Ex - JHON62  " required>
                  </div>

                </div>

                <div class="col-md-12 form-group">
                    <div class="col-md-12">
                     <label for="inputEmail3" class="text-uppercase">Mobile Number ( WhatsApp ) * </label>
                     <input type="text" class="form-control" id="user_mobile" name="user_mobile" onfocusout="sendOTP();" placeholder="Mobile Number"  required>
                    </div>

                  </div>

                  <div class="col-md-12 form-group">
                    <div class="col-md-6">
                    <label for="inputEmail3" class="text-uppercase">OTP</label><br>
                      <input type="text" class="form-control" id="otp_number" name="otp_number" placeholder="OTP Sent On Mobile ">
                    </div>
                    <div class="col-md-6">
                       <button type="button" class="btn btn-primary" id="verifyotp" style="margin-top: 25px;">Verify</button>
                    </div>
                  </div>

                  <div class="col-md-12 form-group">
                      <div class="col-md-12">
                       <label for="inputEmail3" class="text-uppercase"> Password * </label>
                       <input type="password" class="form-control" id="user_password" name="user_password" placeholder="Mobile Number"  required>
                      </div>

                    </div>

                  <div class="col-md-12 form-group">
                    <div class="col-md-12">
                      <label for="inputEmail3" class="text-uppercase">Name of Agency *</label>

                        <input type="text" class="form-control" id="agency_name" name="agency_name" placeholder="Agency Name" required>

                   </div>
                  </div>

                  <div class="col-md-12 form-group">
                    <div class="col-md-12">
                        <label for="inputEmail3" class="text-uppercase"> State *  </label>
                        <select name="sstate" id="sstate" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required onchange="getCity();">
                            <option value="">Select State</option>
                            <?php foreach ($state_list as $key => $value) {
                              // code...
                              echo "<option value='".$value['zone_id']."' >". $value['name'] ."</option>";
                            ?>

                          <?php } ?>
                         </Select>
                    </div>

                    </div>

                  <div class="col-md-12 form-group">
                    <div class="col-md-12">
                        <label for="inputEmail3" class="text-uppercase"> City *  </label>
                        <select name="ccity" id="ccity" class="form-control searchoptions" style="width:100%; height:30px; font-size:12px;" required>
                        </select>
                    </div>

                    </div>

                    <div class="col-md-12 form-group">
                      <div class="col-md-12">
                          <label for="inputEmail3" class="text-uppercase"> Address  </label>
                          <textarea  name="user_add" id="user_add" class="form-control" style="font-size:12px;" rows="4" cols="50">

                          </textarea>

                      </div>

                      </div>

              <div class="form-check" style="text-align: center;">
                <!-- <button type="submit" class="btn btn-login" ng-click="submitForm()">Submit</button> -->
                 <button type="button" style="text-align: center; border-radius:5px;" class="btn btn-lg btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                  <input type="submit"  style="text-align: center; border-radius:5px;" class="btn btn-lg btn-info" value="Register"></input>
              </div>


            </form>
        </div>
        <div class="col-md-6 banner-sec">
           <!--  <ul>
              <li>kjbkjbkjbkjbbbkjbkj kjbjkjnkj kjnkjnjk kjn</li>
                            <li>kjbkjbkjbkjbbbkjbkj kjbjkjnkj kjnkjnjk kjn</li>

              <li>kjbkjbkjbkjbbbkjbkj kjbjkjnkj kjnkjnjk kjn</li>

            </ul> -->
        </div>

    </div>
</div>
</section>

<style>
   p {
   font-size: 16px;
   line-height: 1.6;
   margin:0 0 10px;
   }
   .italic {
   font-style:italic;
   }
   .padtb {
   padding:30px 0px;
   }
</style>

  <script type="text/javascript">

$( document ).ready(function() {

  // verify otp_number
  $('#verifyotp').on('click', function() {

       var mobileNumber = $('#user_mobile').val();
       var otpNumber = $('#otp_number').val();

       var dataString = 'mobile_number=' + mobileNumber + '&otp=' + otpNumber;

       $.ajax({

       type: "POST",

       url: "/webinar/registration/verifyotp",

       data: dataString,

       cache: false,

       success: function(result){

        alert(result);

       }

           });

      });

    });

    // sent OTP
    function sendOTP() {


         var mobileNumber = $('#user_mobile').val();
         var mobilenumberlength = $('#user_mobile').val().length;
         var userEmail = $('#user_email').val();

         if(mobilenumberlength != 10) {

           alert("Mobile Number Not Proper ...!!!");

         } else {
           var dataString = 'mobile_number=' + mobileNumber + '&user_email=' + userEmail;

           $.ajax({

           type: "POST",

           url: "/webinar/registration/sentotp",

           data: dataString,

           cache: false,

           success: function(result){
             if(result == 'Mobile Number Already Registered'){

               alert(result);
               window.location.href='/user/account';

             } else{

               alert(result);

             }

           }
               });
         }

       }

   //registration

   function resgistration(){

     var user_name = $('#user_name').val();
     var user_email = $('#user_email').val();
     var user_id = $('#user_id').val();
     var user_mobile = $('#user_mobile').val();
     var user_password = $('#user_password').val();
     var agency_name = $('#agency_name').val();
     var sstate = $('#sstate').val();
     var ccity = $('#ccity').val();
     var user_add = $('#user_add').val();

     var dataString = 'user_name=' + user_name + '&user_email=' + user_email + '&user_mobile=' + user_mobile + '&agency_name=' + agency_name
                      + '&user_id=' + user_id + '&user_password=' + user_password + '&state_id=' + sstate + '&city_id=' + ccity + '&user_add=' + user_add;

     event.preventDefault();

     $.ajax({

     type: "POST",

     url: "/counsellor/registration/counsellorSubmit",

     data: dataString,

     cache: false,

     success: function(result){

      if(result == "SUCCESS"){

        alert("Thank You For Registration With HelloUni..!!!");
        window.location.href='/counsellor/dashboard';

      } else if(result == 'With Email-ID Or Mobile Account Already Exists'){

        alert(result);
        window.location.href='/user/account';

      } else {

        alert(result);

      }

     }

         });

   }

   // sent OTP
   function getCity() {

        var stateId = $('#sstate').val();

        var dataString = "state_id="+stateId;

          $.ajax({

          type: "POST",

          url: "/counsellor/registration/cityListByStateId",

          data: dataString,

          cache: false,

          success: function(result){

            var cityList = result.city_list;

            if(cityList.length){
                var cityDropDown = '';
                cityList.forEach(function(value, index){

                  cityDropDown += "<option value='"+ value.id+"' >" + value.city_name + "</option>";

                });
              }

              cityDropDown += "<option value='1000000' > OTHER </option>";

              document.getElementById("ccity").innerHTML = cityDropDown;

          }
              });

      }

     </script>
