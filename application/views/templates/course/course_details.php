<style>
   /*.leftcolumn {
   float: left;
   width: 75%;
   }
   .rightcolumn {
   float: left;
   width: 25%;
   background-color: #f1f1f1;
   padding-left: 20px;
   }*/
   /* .fakeimg {
   background-color: #aaa;
   width: 100%;
   padding: 20px;
   }
   .card {
   background-color: white;
   padding: 10px;
   margin-top: 15px;
   }
   .card p{
   font-size: 14px;
   font-family: sans-serif;
   letter-spacing: 0.5px;
   margin: 0 0 15px !important;
   }
   .textFont{
   font-size: 14px;
   font-family: sans-serif;
   letter-spacing: 0.5px;
   }
   .bshadow1{
   box-shadow: 0px 2px 4px 0px #d4d1ce;
   }
   .orangeClr{
   color: #f6882c;
   }
   .margin5{
   margin: 5px;
   }
   .pTag{
   margin: 0 0 5px !important;
   font-size: 14px !important;
   }*/
   /*
   color: #374552;*/
   /* .row:after {
   content: "";
   display: table;
   clear: both;
   }
   @media screen and (max-width: 800px) {
   .leftcolumn, .rightcolumn {
   width: 100%;
   padding: 0;
   }
   }*/
   .affix {
   top:0;
   left:0;
   width: 100%;
   z-index: 9999 !important;
   }
   .navbar {
   margin-bottom: 0px;
   }
   /*.sectionArea .container-fluid {
   position: relative;
   top: 100px !important;
   margin-top: 5%;
   }*/

   .navbar-inverse{
    padding: 18px 8px 0px 8px;
    border-color: white;
    background: linear-gradient(to left, rgba(141, 87, 197) , rgba(246, 136, 44));
   }
   /*.navbar-inverse .navbar-nav>li>a:active{
    background-color: white;
    color:orange;
   }*/
   .navbar-inverse .navbar-nav>li>a{
    color: white;
    padding: 15px 10px;
   }
   .navButton{
    /*background-color: #988787;*/
    color: black;
    border: 0px;
    width: 100%;
    font-size: 1.6rem;
    font-weight: 400;
    text-transform: capitalize;
    height: calc(5rem + 6px) !important;
    border-radius: 15px;
    border-bottom-left-radius: 0px;
    border-bottom-right-radius: 0px;
   }

   /*.navButton:active{
    background-color: white !important;
    color: black !important;
   }*/
   .navbar-inverse .navbar-nav>li>a:focus, .navbar-inverse .navbar-nav>li>a:hover{
    color: #f5862c;
    background-color: white;
   }
   .navbar-inverse .navbar-nav>li{
        margin: 0px 35px;
   }

   .btnBefor{
    background: white;
    color: #f4852d;
   }

   #containerNav {
    width: 100%;
    display: flex;
    justify-content: space-between;
  }

    #containerNav > li {
      width: -webkit-fill-available;
    }

    .navbar-inverse .navbar-nav>.active>a{
      color: #f5862c !important;
      background-color: #ffffff !important;
    }

    .imgCss{
      padding: 20px;
    text-align: center;
    }

    .margin2{
      margin-top: 2%;
    }

    .pTag{
          margin: 0 0 20px;
          font-size: 15px;
          line-height: 1.6;
          letter-spacing: 0.4px;
    }
    .onlyCourse p{
      margin: 0 0 8px;
      font-size: 15px;
      line-height: 1.6;
      letter-spacing: 0.5px;
    }

    .courseEligibility p {
      background: #f4852d;
      padding: 3px;
      text-align: center;
      border-radius: 9px;
      color: white;
      box-shadow: 3px 3px 5px 0px #cacaca;
      font-size: 14px;
    }

    .boxCard{
      box-shadow: 0px 0px 50px 1px #0000001a;
      border-radius: 3rem;
      padding: 10px 20px 15px 20px;
          margin-bottom: 2rem;
         /* max-height: 635px;
    overflow: scroll;*/
    }

    .borderBottom{
      border-bottom: 2px solid black;
    }

    .eligibilityCards{
      padding-right: 5px !important;
      padding-left: 5px !important;
    }

    /*top stick*/
   .BottomDiv {

    padding: 10px 16px;
    background: #f6882c;
    color: #000000;
    position: fixed;
    bottom: 0;
    z-index: 2;
    height: 85px;
   }

   .footer-bottom{
        margin-bottom: 85px;
   }

    /*Modal Content*/
   .modal-content  {
   background-color: #fefefe;
   margin: auto;
   padding: 60px;
   border: 1px solid #888;
   width: 50%;
   }

  /*.modal-content {
  background-color: #fefefe;
  position: absolute;
  margin: auto;
  left: 50%;
  top: 50%;
  border: 1px solid #888;
  width: 50%;
  transform: translate(-50%, -50%);
}*/

   /*.navbar-inverse .navbar-nav>li>a:active{
    color: black;
    background-color: white;
   }*/
</style>
<!-- <div class="container-fluid" style="background-color: RGBA(0, 170, 168, 1); color:#fff; height:200px;">
   <h1>Scrollspy & Affix Example</h1>
   <h3>Fixed navbar on scroll</h3>
   <p>Scroll this page to see how the navbar behaves with data-spy="affix" and data-spy="scrollspy".</p>
   <p>The navbar is attached to the top of the page after you have scrolled a specified amount of pixels, and the links in the navbar are automatically updated based on scroll position.</p>
   </div> -->
<div class="clearfix"></div>
<div id="page_loader">
   <div>
      <section style="background-color: rgba(0, 0, 0, 0);background-repeat: no-repeat;background-image: url(<?php echo base_url();?>uploads/banner.jpg); background-size: cover;background-position: center center;width: 100%;height: 250%;box-shadow: inset 0 0 0 2000px rgba(4, 4, 4, 0.5);">
         <div class="container" style="height: 330px;">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="z-index: 5 !important; text-align: center;">
                  <h1 class="page-top-title" style="margin-top: 8%;color: #ffffff; text-align:center;font-weight:bold;font-size:37px;"><span><?=$course_detail['name']?></span></h1>
                  <h2 class="page-top-title" style="margin-top: 2%;color: #ffffff; text-align:center;font-weight:bold;font-size:37px;"><span><?=$course_detail['department_name']?></span>
                  </h2>
                  <!-- <img src="<?php echo base_url();?>uploads/arizona_state/logo.jpg" style="width:150px;"><a href="<?php echo base_url();?>arizona-state-university" target="_blank" style="font-size:15px;color:#F6881F"> -->
               </div>
            </div>
         </div>
      </section>
      <div class="container scrollSpyArea">
         <nav id="nav" class="navbar navbar-inverse hidden-xs" data-spy="affix" data-offset-top="197">
            <div class="col-md-12">
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>

               </div>
               <div>
                  <div class="collapse navbar-collapse" id="myNavbar">
                     <ul class="nav navbar-nav" id="containerNav">
                        <li><a class="btn btn-md navButton" href="#section1"><b>Program Description</b></a></li>
                        <li><a class="btn btn-md navButton" href="#section2"><b>Admission Requirements</b></a></li>
                        <li><a class="btn btn-md navButton" href="#section3"><b>Course Details</b></a></li>
                        <li><a class="btn btn-md navButton" href="#section4"><b>Course Eligibility</b></a></li>
                        <li><a class="btn btn-md navButton" href="#section5"><b>More Details</b></a></li>
                        <!-- <li><a class="btn btn-md navButton" href="#section6"><b>Similar Courses</b></a></li> -->
                     </ul>
                  </div>
               </div>
            </div>
         </nav>
         <div class="col-md-12" data-spy="scroll" data-target="#spy">
           <div id="section1" class="container-fluid">
              <div class="col-md-12 margin2" >
                <div class="col-md-6 imgCss">
                  <img src="<?php echo base_url();?>application/images/course_page/progDesc.png" width="80%" alt="...">
                </div>
                <div class="col-md-6">
                  <h3 class="purple">Program Description</h3>
                  <!--<p class="pTag">Agricultural Science and Management is a very flexible degree that provides training in a wide area of studies. Students will pick a “primary” agricultural interest area and then pick a variety of agriculture classes to complete the degree. It is customizable for each student. Graduates have lots of options but should work closely with their advisor for the best outcome.</p>
                  <p class="pTag">The primary interest could be animal science, agribusiness management, agronomy, agriculture engineering technology, communication, etc. It will provide a solid background to enter many areas of agriculture from production to extension to agribusiness. </p>
                -->
                <?=$course_detail['description']?>
              </div>
              </div>
           </div>
           <div id="section2" class="container-fluid">
              <div class="col-md-12 margin2" >
                <div class="col-md-6">
                    <h3 class="orange">Admission Requirements</h3>
                    <?php
                    if($course_detail['admission_requirements'])
                    {
                        foreach($course_detail['admission_requirements'] as $detail)
                        {
                            if($detail)
                            {
                                ?>
                                <p class="pTag"><i class="fa fa-check-square-o" style="font-size:15px;color:#f6882c">&nbsp;</i><?=$detail?></p>
                                <?php
                            }
                        }
                    }
                    ?>
                    <p class="pTag"><i class="fa fa-check-square-o" style="font-size:15px;color:#f6882c">&nbsp;</i> Transfer students must have a minimum 2.50 GPA (a "C+" or better where "A"=4.00) from a college or unive</p>
                </div>
                <div class="col-md-6 imgCss">
                  <img src="<?php echo base_url();?>application/images/course_page/addmissionReq.png" width="80%" alt="...">
                </div>
              </div>
           </div>
           <div id="section3" class="container-fluid">
              <div class="col-md-12 margin2" >
                <div class="col-md-6 imgCss">
                  <img src="<?php echo base_url();?>application/images/course_page/courseDetails.png" width="80%" alt="...">
                </div>
                <div class="col-md-6 onlyCourse">
                    <h3 class="purple">Course Details</h3>
                    <div class="col-md-12">
                      <p><i class="fas fa-signal orange"></i>&nbsp;</i><b>Program level :</b> <?=$course_detail['degree_name']?></p>
                    </div>
                    <div class="col-md-6">
                      <p><i class="fas fa-calendar-alt orange">&nbsp;</i><b>Duration (In Months) : </b> <?=$course_detail['duration']?></p>
                    </div>

                    <div class="col-md-6">
                      <p><i class="fas fa-clipboard-check orange">&nbsp;</i><b>No of Intakes : </b> <?=$course_detail['no_of_intake']?></p>
                    </div>
                    <div class="col-md-6">
                      <p><i class="fas fa-calendar-alt orange">&nbsp;</i><b>Months of Intake : </b><?=$course_detail['intake_months']?></p>
                    </div>
                    <div class="col-md-6">
                      <p><i class="fas fa-external-link-square-alt orange">&nbsp;</i><b>Deadline URL :  </b>&nbsp;&nbsp; <a class="btn btn-sm btn-primary purpleBg" href="<?=$course_detail['deadline_link']?>" target="_blank" style="border-radius: 6px;"><b>Admission</b></a></p>
                    </div>

                    <div class="col-md-12">
                      <div class="col-md-6">
                        <div class="row">
                          <p><b class="purple">Course Start Month : </b> <?=$course_detail['start_months']?></p>
                        </div>
                        <div class="row">
                          <p><b class="purple">Decision Month : </b> <?=$course_detail['decision_months']?></p>
                        </div>
                        <div class="row">
                          <p><b class="purple">Conditional Admit : </b> <?=$course_detail['conditional_admit']?></p>
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="row">
                          <p><b class="purple">Application fee : </b> <?=$course_detail['application_fee_amount']?></p>
                        </div>
                        <div class="row">
                          <p><b class="purple">Tuition : </b> <?=$course_detail['total_fees']?></p>
                        </div>
                        <div class="row">
                          <p><b class="purple">Cost of Living : </b> <?=$course_detail['cost_of_living_year']?> / Year</p>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12 onlyCourse">
                      <p><i class="fa fa-user orangeClr">&nbsp;</i><b>Person Name :</b> <?=$course_detail['contact_name']?></p>
                      <p><i class="fa fa-envelope-o orangeClr">&nbsp;</i><b>Email ID :</b> <?=$course_detail['contact_email']?></p>
                    </div>
                </div>
              </div>
           </div>
           <div class="col-md-12" style="margin: 4rem 0px;">
              <div class="col-md-6">
                <div id="section4" class="col-md-12 onlyCourse boxCard">
                  <h3 class="orange">Course Eligibility</h3>
                    <div class="col-md-12">
                      <p><i class="fas fa-university orange"></i>&nbsp;</i><b>GPA :</b> <?=$course_detail['eligibility_gpa']?> </p>
                    </div>
                    <div class="col-md-12">
                      <p><i class="fas fa-file-alt orange">&nbsp;</i><b>Test Required (Yes/No) : </b> <?=$course_detail['test_required']?></p>
                    </div>
                    <div class="col-md-12">
                      <p><i class="fas fa-check-double orange">&nbsp;</i><b>English Profiency : </b> <?=$course_detail['eligibility_english_profiency']?></p>
                    </div>
                    <div class="row courseEligibility">
                        <div class="col-md-3 eligibilityCards">
                          <p><b>TOEFL : <?=$course_detail['eligibility_toefl']?></b></p>
                        </div>
                        <div class="col-md-3 eligibilityCards">
                          <p><b>IELTS : <?=$course_detail['eligibility_ielts']?></b></p>
                        </div>
                        <div class="col-md-3 eligibilityCards">
                          <p><b>PTE : <?=$course_detail['eligibility_pte']?></b></p>
                        </div>
                        <div class="col-md-3 eligibilityCards">
                          <p><b>DuoLingo : <?=$course_detail['eligibility_duolingo']?></b></p>
                        </div>
                    </div>
                </div>
                <div id="section5" class="col-md-12 onlyCourse boxCard">

                  <h3 class="purple">More Details</h3>
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <p><i class="far fa-edit orange">&nbsp;</i><b>No Of LOR : </b>  <?=$course_detail['eligibility_duolingo']?></p>
                      </div>
                      <div class="col-md-6">
                        <p><i class="fas fa-user-edit orange">&nbsp;</i><b>SOP Required : </b> <?=$course_detail['eligibility_duolingo']?></p>
                      </div>
                      <div class="col-md-6">
                        <p><i class="far fa-address-card orange">&nbsp;</i><b>Resume Required : </b><?=$course_detail['resume_required']?></p>
                      </div>
                      <div class="col-md-6">
                        <p><i class="fas fa-copy orange">&nbsp;</i><b>Backlogs Accpeted : </b><?=$course_detail['eligibility_duolingo']?></p>
                      </div>
                      <div class="col-md-6">
                        <p><i class="fas fa-calendar-alt orange">&nbsp;</i><b>Drop Year : </b><?=$course_detail['drop_years']?></p>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <h5 class="purple">Scholarship</h5>
                      <div class="col-md-6">
                        <p><i class="fas fa-user-graduate orange">&nbsp;</i><b>Scholarship Available : </b> <?=$course_detail['scholarship_available']?></p>
                      </div>
                      <div class="col-md-6">
                        <p><i class="fas fa-clipboard-check orange">&nbsp;</i><b>With 15 years Education : </b> <?=$course_detail['with_15_years_education']?></p>
                      </div>
                      <div class="col-md-6">
                        <p><i class="far fa-file-alt orange">&nbsp;</i><b>WES Requirement : </b><?=$course_detail['wes_requirement']?></p>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <h5 class="purple">Work And Study</h5>
                      <div class="col-md-6">
                        <p><i class="fas fa-mail-bulk orange">&nbsp;</i><b>Work While Studying : </b> <?=$course_detail['work_while_studying']?></p>
                      </div>
                      <div class="col-md-6">
                        <p><i class="fas fa-briefcase orange">&nbsp;</i><b>Co-op Iternship : </b> <?=$course_detail['cooperate_internship_participation']?></p>
                      </div>
                      <div class="col-md-6">
                        <p><i class="far fa-calendar-check orange">&nbsp;</i><b>Work permit : </b><?=$course_detail['eligibility_duolingo']?></p>
                      </div>
                      <div class="col-md-6">
                        <p><i class="fas fa-calendar-alt orange">&nbsp;</i><b>Work Permit (Months) : </b><?=$course_detail['no_of_month_work_permit']?></p>
                      </div>
                    </div>
                </div>
              </div>
              <div id="section6" class="col-md-6 boxCard">
                    <h3 class="purple">Similar Course</h3>

                    <?php
                    foreach ($related_courses as $rckey => $rcvalue) {
                      // code...

                    ?>

                    <div class="col-md-12 onlyCourse borderBottom">
                        <h4 class="orange"><?php echo $rcvalue['name'] . " - " .$rcvalue['department_name'] ;  ?></h4>
                        <div class="col-md-12">
                          <p><i class="fas fa-signal"></i>&nbsp;</i><b>Program level :</b> <?=$rcvalue['degree_name']?></p>
                        </div>
                        <div class="col-md-6">
                          <p><i class="fas fa-calendar-alt">&nbsp;</i><b>Duration (In Months) : </b> <?=$rcvalue['duration']?></p>
                        </div>
                        <div class="col-md-6">
                          <p><i class="fas fa-clipboard-check">&nbsp;</i><b>No of Intakes : </b> <?=$rcvalue['no_of_intake']?></p>
                        </div>
                        <div class="col-md-6">
                          <p><i class="fas fa-calendar-alt">&nbsp;</i><b>Months of Intake : </b><?=$rcvalue['intake_month']?></p>
                        </div>
                        <div class="col-md-6">
                          <p><a class="btn btn-sm btn-primary purpleBg" href="/course/coursemaster/courselist/<?=$rcvalue['id']?>" target="_blank" style="border-radius: 6px;"><b>Check Your Eligibility</b></a></p>
                        </div>
                    </div>

                  <?php } ?>

                    <!--<div class="col-md-12 onlyCourse borderBottom">
                        <h4 class="orange">Bachelor of Science - Agricultural Science and Management</h4>
                        <div class="col-md-12">
                          <p><i class="fas fa-signal"></i>&nbsp;</i><b>Program level :</b> Doctoral Degree (PHd)</p>
                        </div>
                        <div class="col-md-6">
                          <p><i class="fas fa-calendar-alt">&nbsp;</i><b>Duration 1 (In Months) : </b> 60</p>
                        </div>
                        <div class="col-md-6">
                          <p><i class="fas fa-clipboard-check">&nbsp;</i><b>No of Intakes : </b> 1</p>
                        </div>
                        <div class="col-md-6">
                          <p><i class="fas fa-calendar-alt">&nbsp;</i><b>Months of Intake : </b>August</p>
                        </div>
                        <div class="col-md-6">
                          <p><a class="btn btn-sm btn-primary orangeBg" href="#" target="_blank" style="border-radius: 6px;"><b>Check Your Eligibility</b></a></p>
                        </div>
                    </div>

                    <div class="col-md-12 onlyCourse borderBottom">
                        <h4 class="purple">Bachelor of Science - Agricultural Science and Management</h4>
                        <div class="col-md-12">
                          <p><i class="fas fa-signal"></i>&nbsp;</i><b>Program level :</b> Doctoral Degree (PHd)</p>
                        </div>
                        <div class="col-md-6">
                          <p><i class="fas fa-calendar-alt">&nbsp;</i><b>Duration (In Months) : </b> 60</p>
                        </div>
                        <div class="col-md-6">
                          <p><i class="fas fa-clipboard-check">&nbsp;</i><b>No of Intakes : </b> 1</p>
                        </div>
                        <div class="col-md-6">
                          <p><i class="fas fa-calendar-alt">&nbsp;</i><b>Months of Intake : </b>August</p>
                        </div>
                        <div class="col-md-6">
                          <p><a class="btn btn-sm btn-primary purpleBg" href="#" target="_blank" style="border-radius: 6px;"><b>Check Your Eligibility</b></a></p>
                        </div>
                    </div>

                    <div class="col-md-12 onlyCourse">
                        <h4 class="orange">Bachelor of Science - Agricultural Science and Management</h4>
                        <div class="col-md-12">
                          <p><i class="fas fa-signal"></i>&nbsp;</i><b>Program level :</b> Doctoral Degree (PHd)</p>
                        </div>
                        <div class="col-md-6">
                          <p><i class="fas fa-calendar-alt">&nbsp;</i><b>Duration (In Months) : </b> 60</p>
                        </div>
                        <div class="col-md-6">
                          <p><i class="fas fa-clipboard-check">&nbsp;</i><b>No of Intakes : </b> 1</p>
                        </div>
                        <div class="col-md-6">
                          <p><i class="fas fa-calendar-alt">&nbsp;</i><b>Months of Intake : </b>August</p>
                        </div>
                        <div class="col-md-6">
                          <p><a class="btn btn-sm btn-primary orangeBg" href="#" target="_blank" style="border-radius: 6px;"><b>Check Your Eligibility</b></a></p>
                        </div>
                    </div>-->
              </div>
           </div>
         </div>
      </div>

      <div class="col-md-12 BottomDiv " id="myBottomone">

          <?php
             if(isset($alreadyConnected) && $alreadyConnected)
             {
               if($alreadyConnected->slot_1 || $alreadyConnected->slot_2 || $alreadyConnected->slot_3)
               {
                         if(!$alreadyConnected->confirmed_slot)
                         {
                             ?>
              <div class="col-md-12">
                <form class="form-horizontal" method="post" action="/university/details/slotconfirm">
                  <div class="col-md-5" style="text-align: right;margin-top: 8px;">
                      <h4 style="color:#fff;">Please select any one slot :</h4>
                    </div>
                  <div class="col-md-4" style="display: inline-flex;margin-top: 8px;">
                      <?php
                      if($alreadyConnected->slot_1)
                      {
                          ?>
                   <div style="color:#fff; padding:5px;"><input type="radio" name="confirmed_slot" value="<?=$alreadyConnected->slot_1?>" <?=($alreadyConnected->confirmed_slot == $alreadyConnected->slot_1)? 'checked' : ''?>>&nbsp;&nbsp;<b>Slot 1 :</b> &nbsp;&nbsp;<?=$alreadyConnected->slot_1_show?></div>
                   <?php
                      }
                      if($alreadyConnected->slot_2)
                      {
                          ?>
                   <div style="color:#fff; padding:5px;"><input type="radio" name="confirmed_slot" value="<?=$alreadyConnected->slot_2?>" <?=($alreadyConnected->confirmed_slot == $alreadyConnected->slot_2)? 'checked' : ''?>>&nbsp;&nbsp;<b>Slot 2 :</b> &nbsp;&nbsp;<?=$alreadyConnected->slot_2_show?></div>
                   <?php
                      }
                      if($alreadyConnected->slot_3)
                      {
                          ?>
                   <div style="color:#fff; padding:5px;"><input type="radio" name="confirmed_slot" value="<?=$alreadyConnected->slot_3?>" <?=($alreadyConnected->confirmed_slot == $alreadyConnected->slot_3)? 'checked' : ''?>>&nbsp;&nbsp;<b>Slot 3 :</b> &nbsp;&nbsp;<?=$alreadyConnected->slot_3_show?></div>
                   <?php
                      }
                      ?>
                  </div>
                  <div class="col-md-3">
                   <input type="hidden" name="slot_id" value="<?=$alreadyConnected->id?>">
                   <input type="hidden" name="university_id" value="<?=$id?>">
                   <input type="hidden" name="university_login_id" value="<?=$login_id?>">
                   <input type="hidden" name="university_website" value="<?=$website?>">
                   <input type="hidden" name="university_name" value="<?=$name?>">
                   <input type="hidden" name="university_email" value="<?=$email?>">
                   <input type="hidden" name="slug" value="<?=$slug?>">
                   <div style="text-align:left;"><input type="submit" class="btn btn-lg btn-primary" name="submit" style="margin:10px auto;" value="Submit"></div>
                  </div>


                </form>

              </div>

          <?php
             }
             else
             {
                 if($applied)
                 {
                     ?>
          <div style="text-align:center;"><input type="button" class="btn btn-lg btn-primary" name="apply" style="margin:10px auto; background-color:#F6881F" value="Applied" id="apply_button" disabled></div>
          <?php
             }
             else if(isset($tokbox_url) && $tokbox_url)
             {
                 if($show_url)
                 {
                     ?>
          <div style="margin-top: 18px; font-weight: bold;"><a href="<?=$tokbox_url?>" target="_blank">Click Here For Video Conferene</a></div>
          <?php
             }
             else if($alreadyConnected->status == 'Attained')
             {
                 ?>
          <!--h5 style="color:#fff;">Book one to one sessions Again:</h5-->
          <form class="form-horizontal" method="post" action="/university/details/connect">
             <div class="form-group" style="text-align:center;">
                <input type="submit" class="btn btn-lg btn-primary" name="submit" style="margin:10px auto;" value="Connect Again">
             </div>
             <input type="hidden" name="university_id" value="<?=$id?>">
             <input type="hidden" name="university_login_id" value="<?=$login_id?>">
             <input type="hidden" name="slug" value="<?=$slug?>">
          </form>
          <form method="get" action="/university/details/application">
             <div style="text-align:center;">
                <input type="submit" class="btn btn-lg btn-primary" name="submit" style="margin:10px auto;" value="Start Documentation">
             </div>
             <input type="hidden" name="uid" value="<?=$id?>">
          </form>
          <?php
             }
             else if($alreadyConnected->status == 'Scheduled')
             {
                 ?>
          <h4 style="color:#fff; text-align:center;"><?=$this->session->flashdata('flash_message') ? $this->session->flashdata('flash_message') : '<p style="font-size:11px;">Your request is in process.<br/>Please check your mail for further information.</p>'?></h4>
          <?php
             }
             }
             else
             {
             ?>
          <h4 style="color:#fff; text-align:center;"><?=$this->session->flashdata('flash_message') ? $this->session->flashdata('flash_message') : '<p style="font-size:11px;">Your request is in process.<br/>Please check your mail for further information.</p>'?></h4>
          <?php
             }
               }
             }
             else
             {
             ?>
          <h4 style="color:#fff; text-align:center;"><?=$this->session->flashdata('flash_message') ? $this->session->flashdata('flash_message') : '<p style="font-size:11px;">Your request is in process.<br/>Please check your mail for further information.</p>'?></h4>
          <?php
             }
             }
             else
             {
              ?>
            <div class="col-md-12">
                <div class="col-md-8 col-xs-8" style="text-align: right;margin-top: 8px;">
                  <h4 class="hidden-xs visible-md visible-lg" style="color:#fff;"><?php echo $course_detail['uni_name'];?> &nbsp; Book one to one sessions :</h4>
                  <h5 class="visible-xs hidden-md hidden-lg" style="color:#fff;"><?php echo $course_detail['uni_name'];?> &nbsp; Book one to one sessions :</h5>
                </div>
                <div class="col-md-3 col-xs-3" style="text-align: left;">
                  <form class="form-horizontal" method="post" action="/university/details/connect" id="connect-university">
                     <div class="form-group" style="text-align:left;">
                        <?php
                           if($userLoggedIn)
                           {
                               ?>
                        <input type="button" class="btn btn-lg btn-primary hidden-xs visible-md visible-lg" name="connect" style="margin:15px 10px 0px 10px;" value="Connect" onclick="openConsent()">
                        <input type="button" class="btn btn-md btn-primary visible-xs hidden-md hidden-lg" name="connect" style="margin:15px 10px 0px 10px;" value="Connect" onclick="openConsent()">
                        <?php
                           }
                           else
                           {
                               ?>
                        <input type="submit" class="btn btn-lg btn-primary hidden-xs visible-md visible-lg" name="submit" style="margin:15px 10px 0px 10px;" value="Connect">
                        <input type="submit" class="btn btn-md btn-primary visible-xs hidden-md hidden-lg" name="submit" style="margin:15px 10px 0px 10px;" value="Connect">
                        <?php
                           }
                           ?>
                     </div>
                     <input type="hidden" name="university_id" value="<?=$id?>">
                     <input type="hidden" name="university_login_id" value="<?=$login_id?>">
                     <input type="hidden" name="slug" value="<?=$slug?>">
                     <input type="hidden" name="student_signature" value="" id="student_signature">
                     <input type="hidden" name="student_name" value="" id="student_name">
                     <input type="hidden" name="student_email" value="" id="student_email">
                     <input type="hidden" name="student_phone" value="" id="student_phone">
                     <input type="hidden" name="student_dob" value="" id="student_dob">
                     <input type="hidden" name="student_country" value="" id="student_country">
                     <input type="hidden" name="student_city" value="" id="student_city">
                     <input type="hidden" name="student_passport" value="" id="student_passport">
                     <input type="hidden" name="student_intake_month" value="" id="student_intake_month">
                     <input type="hidden" name="student_intake_year" value="" id="student_intake_year">
                     <input type="hidden" name="student_mainstream" value="" id="student_mainstream">
                     <input type="hidden" name="student_subcourse" value="" id="student_subcourse">
                     <input type="hidden" name="student_level" value="" id="student_level">
                     <input type="hidden" name="student_hear_about_us" value="" id="student_hear_about_us">
                  </form>
                </div>
            </div>


          <?php
             }
             ?>

      </div>

      <style type="text/css">
         .mrg{
         margin: 10px 5px;
         }
         .ht30{
         height: 30px;
         }
      </style>

      <div id="consent-modal" class="modal">

       <!-- Modal content -->
       <div class="row modal-content">
         <span class="close" id="consent-close">&times;</span>
         <form class="col-md-12 col-lg-12 col-sm-12 col-xs-12 form-group">
             <div id="fieldsBox">
             <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 mrg">
                     <div class="col-md-3">
                        <input type="text" name="user_name" id="user_name" class="form-control ht30" placeholder="Your Name" value="<?=isset($user_master) && $user_master['name'] ? $user_master['name'] : ''?>">
                     </div>
                     <div class="col-md-3">
                        <input type="text" name="user_email" id="user_email" class="form-control ht30" value="<?=isset($user_master) && $user_master['email'] ? $user_master['email'] : 'Your Email'?>" disabled>
                     </div>
                     <div class="col-md-3">
                     <input type="text" name="user_phone" id="user_phone" class="form-control ht30" value="<?=isset($user_master) && $user_master['phone'] ? $user_master['phone'] : 'Your Phone'?>" disabled>
                   </div>
                     <div class="col-md-3">
                     <input type="text" name="user_passport" id="user_passport" class="form-control ht30" value="<?=isset($student_details) && $student_details['passport_number'] ? $student_details['passport_number'] : ''?>" placeholder="Your Passport Number">
                   </div>
             </div>
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 mrg">
                   <div class="col-md-4">
                     <input type="date" name="user_dob" id="user_dob" class="form-control ht30" placeholder="Your DOB" value="<?=isset($student_details) && $student_details['dob'] ? $student_details['dob'] : ''?>">
                   </div>
                     <div class="col-md-4">
                     <select class="form-control ht30" id="user_country" name="user_country">
                             <option value="">Select Country</option>
                             <?php
                             foreach($locationcountries as $location)
                             {
                                 ?>
                                 <option value="<?=$location->id . '|' . $location->name?>" <?=(isset($student_details) && $student_details['country'] && $student_details['country'] == $location->id) ? 'selected' : ''?>><?=$location->name?></option>
                                 <?php
                             }
                             ?>
                         </select>
                   </div>
                     <div class="col-md-4">
                     <select class="form-control ht30" id="user_city" name="user_city">
                             <option value="">Select City</option>
                             <?php
                             foreach($cities as $city)
                             {
                                 ?>
                                 <option value="<?=$city->id . '|' . $city->city_name?>" <?=(isset($student_details) && $student_details['city'] && $student_details['city'] == $city->id) ? 'selected' : ''?>><?=$city->city_name?></option>
                                 <?php
                             }
                             ?>
                         </select>
                   </div>
              </div>
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 mrg">
                   <div class="col-md-4">
                       <select class="form-control ht30" id="user_mainstream" name="user_mainstream">
                           <option value="">Select Main Stream</option>
                           <?php
                           foreach($mainstreams as $mainstream)
                           {
                               ?>
                               <option value="<?=$mainstream->id . '|' . $mainstream->display_name?>" <?=(isset($lead_master) && $lead_master['mainstream'] && $lead_master['mainstream'] == $mainstream->id) ? 'selected' : ''?>><?=$mainstream->display_name?></option>
                               <?php
                           }
                           ?>
                       </select>
                   </div>
                   <div class="col-md-4">
                       <select class="form-control ht30" id="user_subcourse" name="user_subcourse">
                           <?php
                           if($subCourses)
                           {
                               ?>
                               <option value="">Select Subcourse</option>
                               <?php
                               foreach($subCourses as $subCourse)
                               {
                                   ?>
                                   <option value="<?=$subCourse->id . '|' . $subCourse->display_name?>" <?=(isset($lead_master) && $lead_master['courses'] && $lead_master['courses'] == $subCourse->id) ? 'selected' : ''?>><?=$subCourse->display_name?></option>
                                   <?php
                               }
                           }
                           ?>
                       </select>
                   </div>
                   <div class="col-md-4">
                       <select class="form-control ht30" id="user_level" name="user_level">
                           <option value="">Select Level</option>
                           <?php
                           foreach($levels as $level)
                           {
                               ?>
                               <option value="<?=$level->id . '|' . $level->name?>" <?=(isset($lead_master) && $lead_master['degree'] && $lead_master['degree'] == $level->id) ? 'selected' : ''?>><?=$level->name?></option>
                               <?php
                           }
                           ?>
                       </select>
                   </div>
               </div>
               <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 mrg">
                     <div class="col-md-4">
                     <select class="form-control ht30" id="user_intake_year" name="user_intake_year">
                             <option value="">Select Intake Year</option>
                             <option value="2020" <?=(isset($student_details) && $student_details['intake_year'] && $student_details['intake_year'] == "2020") ? 'selected' : ''?> >2020</option>
                             <option value="2021" <?=(isset($student_details) && $student_details['intake_year'] && $student_details['intake_year'] == "2021") ? 'selected' : ''?> >2021</option>
                             <option value="2022" <?=(isset($student_details) && $student_details['intake_year'] && $student_details['intake_year'] == "2022") ? 'selected' : ''?> >2022</option>
                         </select>
                   </div>
                     <div class="col-md-4">
                     <select class="form-control ht30" id="user_intake_month" name="user_intake_month">
                             <option value="">Select Intake Month</option>
                             <option value="Jan-Mar" <?=(isset($student_details) && $student_details['intake_month'] && $student_details['intake_month'] == "Jan-Mar") ? 'selected' : ''?> >January - March</option>
                             <option value="Apr-Jun" <?=(isset($student_details) && $student_details['intake_month'] && $student_details['intake_month'] == "Apr-Jun") ? 'selected' : ''?> >April - June</option>
                             <option value="Aug-Oct" <?=(isset($student_details) && $student_details['intake_month'] && $student_details['intake_month'] == "Aug-Oct") ? 'selected' : ''?> >August - October</option>
                         </select>
                   </div>
                     <div class="col-md-4">
                     <select class="form-control ht30" id="user_hear_about_us" name="user_hear_about_us">
                             <option value="">From Where Did You Hear About Us?</option>
                             <option value="Friends / Family" <?=(isset($student_details) && $student_details['hear_about_us'] && $student_details['hear_about_us'] == "Friends / Family") ? 'selected' : ''?> >Friends / Family</option>
                             <option value="Google / Facebook" <?=(isset($student_details) && $student_details['hear_about_us'] && $student_details['hear_about_us'] == "Google / Facebook") ? 'selected' : ''?> >Google / Facebook</option>
                             <option value="Stupidsid Seminar" <?=(isset($student_details) && $student_details['hear_about_us'] && $student_details['hear_about_us'] == "Stupidsid Seminar") ? 'selected' : ''?> >Stupidsid Seminar</option>
                             <option value="Stupidsid Website" <?=(isset($student_details) && $student_details['hear_about_us'] && $student_details['hear_about_us'] == "Stupidsid Website") ? 'selected' : ''?> >Stupidsid Website</option>
                             <option value="College Seminar" <?=(isset($student_details) && $student_details['hear_about_us'] && $student_details['hear_about_us'] == "College Seminar") ? 'selected' : ''?> >College Seminar</option>
                             <option value="News Paper" <?=(isset($student_details) && $student_details['hear_about_us'] && $student_details['hear_about_us'] == "News Paper") ? 'selected' : ''?> >News Paper</option>
                             <option value="Calling from Imperial" <?=(isset($student_details) && $student_details['hear_about_us'] && $student_details['hear_about_us'] == "Calling from Imperial") ? 'selected' : ''?> >Calling from Imperial</option>
                         </select>
                   </div>
                  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 mrg" style="text-align: center;margin-top : 15px;">

                          <!--input type="button" class="btn btn-lg btn-primary" name="submitValue" value="submit" id="submitValue" onclick="openConsentModal()" disabled="disabled"-->
                          <input type="button" class="btn btn-lg btn-primary" name="submitValue" value="submit" id="submitValue" onclick="openConsentModal()">

                  </div>
               </div>
          </div>
          <div class="col-md-12 mrg" id="consentBlock" style="display: none;">
            <!-- consent form -->
              <?=$consent?>

            <div class="col-md-12" style="text-align: right;margin-top : 15px;"><input type="button" class="btn btn-lg btn-primary" name="agreed" value="Agreed" onclick="connectUniversity()"></div>
            <!-- consent form end -->
          </div>
           <div class="col-md-12" ></div>

         </form>

       </div>

      </div>

   </div>
</div>

<script src="js/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/signature_pad/1.5.3/signature_pad.min.js"></script>

<script type="text/javascript">
   //  var headerHeight = $(".navbar-inverse").height();
   //  var exactHeight = headerHeight + 5;
   //  alert("headerHeight "+exactHeight);
   // $("#nav ul li a[href^='#']").on('click', function(e) {


   //  e.preventDefault();


   //  var hash = this.hash;


   //  $('html, body').animate({
   //      scrollTop: $(hash).offset().top - headerHeight
   //    }, 1000, function(){


   //      window.location.hash = hash;
   //    });

   // });

   $('.scrollSpyArea').scrollspy({ target: '#spy', offset:300});

    headerHeight = $(".navbar-inverse").height() + 5;

   /*Smooth link animation*/
   $("#nav ul li a[href^='#']").click(function() {
      // $('#sidebar-wrapper').css({'top' : headerHeight });
       if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

           var target = $(this.hash);
           target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
           if (target.length) {
               $('html,body').animate({
                   scrollTop: target.offset().top - headerHeight
               }, 1000);
               return false;

           }

       }

   });


</script>

<script type="text/javascript">

var modal = document.getElementById("myModal");
var consentModal = document.getElementById("consent-modal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
var consentSpan = document.getElementById("consent-close");

consentSpan.onclick = function() {
consentModal.style.display = "none";
}

var signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
 backgroundColor: 'rgba(255, 255, 255, 0)',
 penColor: 'rgb(0, 0, 0)'
});

function openConsent()
{
 consentModal.style.display = "block";
}

function openConsentModal() {
    var country = document.getElementById("user_country").value;
    var split_country = country.split("|");
    var city = document.getElementById("user_city").value;
    var split_city = city.split("|");
    var mainstream = document.getElementById("user_mainstream").value;
    var split_mainstream = mainstream.split("|");
    var subcourse = document.getElementById("user_subcourse").value;
    var split_subcourse = subcourse.split("|");
    var level = document.getElementById("user_level").value;
    var split_level = level.split("|");

    var studentName = document.getElementById("user_name").value;
    var dob = document.getElementById("user_dob").value;
    var passport_number = document.getElementById("user_passport").value;
    var resident = split_city[1] + ', ' + split_country[1];
    var program = split_level[1] + ', ' + split_subcourse[1] + ', ' + split_mainstream[1];
    var intake = document.getElementById("user_intake_month").value + ', ' + document.getElementById("user_intake_year").value;
    var email = document.getElementById("user_email").value;
    var mobile = document.getElementById("user_phone").value;
    var university_name = '<?=$name?>';
    var today_date = '<?=date('Y-m-d')?>';

    for(i = studentName.length; i < 80; i++){
        studentName += '&nbsp;';
    }
    for(i = dob.length; i < 40; i++){
        dob += '&nbsp;';
    }
    for(i = passport_number.length; i < 65; i++){
        passport_number += '&nbsp;';
    }
    for(i = resident.length; i < 50; i++){
        resident += '&nbsp;';
    }
    for(i = program.length; i < 70; i++){
        program += '&nbsp;';
    }
    for(i = intake.length; i < 50; i++){
        intake += '&nbsp;';
    }
    for(i = university_name.length; i < 70; i++){
        university_name += '&nbsp;';
    }
    for(i = email.length; i < 70; i++){
        email += '&nbsp;';
    }
    for(i = mobile.length; i < 70; i++){
        mobile += '&nbsp;';
    }
    for(i = today_date.length; i < 40; i++){
        today_date += '&nbsp;';
    }

    document.getElementById("student_name_1").innerHTML = '<u>' + studentName + '</u>';
    document.getElementById("student_name_2").innerHTML = '<u>' + studentName + '</u>';
    document.getElementById("dob").innerHTML = '<u>' + dob + '</u>';
    document.getElementById("passport").innerHTML = '<u>' + passport_number + '</u>';
    document.getElementById("resident").innerHTML = '<u>' + resident + '</u>';
    document.getElementById("program").innerHTML = '<u>' + program + '</u>';
    document.getElementById("intake").innerHTML = '<u>' + intake + '</u>';
    document.getElementById("university").innerHTML = '<u>' + university_name + '</u>';
    document.getElementById("email").innerHTML = '<u>' + email + '</u>';
    document.getElementById("mobile").innerHTML = '<u>' + mobile + '</u>';
    document.getElementById("date").innerHTML = '<u>' + today_date + '</u>';

    document.getElementById('consentBlock').style.display = 'block' ;
    document.getElementById('fieldsBox').style.display = 'none' ;
    }

function connectUniversity()
{
    var country = document.getElementById("user_country").value;
    var split_country = country.split("|");
    var city = document.getElementById("user_city").value;
    var split_city = city.split("|");
    var mainstream = document.getElementById("user_mainstream").value;
    var split_mainstream = mainstream.split("|");
    var subcourse = document.getElementById("user_subcourse").value;
    var split_subcourse = subcourse.split("|");
    var level = document.getElementById("user_level").value;
    var split_level = level.split("|");

    var country = split_country[0];
    var city = split_city[0];
    var mainstream = split_mainstream[0];
    var subcourse = split_subcourse[0];
    var level = split_level[0];

    var data = signaturePad.toDataURL('image/png');
    document.getElementById("student_signature").value = data;
    document.getElementById("student_name").value = document.getElementById("user_name").value;
    document.getElementById("student_email").value = document.getElementById("user_email").value;
    document.getElementById("student_phone").value = document.getElementById("user_phone").value;
    document.getElementById("student_dob").value = document.getElementById("user_dob").value;
    document.getElementById("student_country").value = country;
    document.getElementById("student_city").value = city;
    document.getElementById("student_mainstream").value = mainstream;
    document.getElementById("student_subcourse").value = subcourse;
    document.getElementById("student_level").value = level;
    document.getElementById("student_passport").value = document.getElementById("user_passport").value;
    document.getElementById("student_intake_month").value = document.getElementById("user_intake_month").value;
    document.getElementById("student_intake_year").value = document.getElementById("user_intake_year").value;
    document.getElementById("student_hear_about_us").value = document.getElementById("user_hear_about_us").value;
    var form = document.getElementById("connect-university");
    form.submit();
}

$(document).ready(function(){
     $('#user_mainstream').on('change', function() {
         var dataString = 'id='+this.value;
         $.ajax({
             type: "POST",
             url: "/search/university/getSubcoursesList",
             data: dataString,
             cache: false,
             success: function(result){
                 $('#user_subcourse').html(result);
             }
         });
     });
if ($("i").hasClass("fa-heart-o")){
 // Do something if class exists
    $(".heart.fa-heart-o").click(function() {
      $(this).removeClass('fa-heart-o');
      $(this).addClass('fa-heart');
      $(".fa-heart").append('<style>{pointer-events:none;}</style>');
    });
} else {
    // Do something if class does not exist
   // $(".fa-heart").css({"pointer-events": "none !important"});
   $('.fa-heart').append('<style>{pointer-events:none !important;}</style>');
}

});

</script>
