<?php if($this->uri->segment('5') && $this->uri->segment('5')=='view'){

$view='disabled'; ?>

<script type="text/javascript">
$( document ).ready(function() {
$("#cleditor").cleditor()[0].disable("true");
//$(".btn .btn-primary .pull-right").attr('disabled' , true);
$('.pull-right').prop('disabled', true);

});

</script>
<?php } else { $view=''; }


 ?>
        
        <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- Statistics Button Container -->
            	
                
                <!-- Panels Start -->
				
				<!-- <?php if($msg = $this->session->flashdata('flash_message')){?> 
				<div class="mws-form-message success"><?php echo $msg;?></div>
				 <?php } ?>
				 <?php if(validation_errors()){?> 
    <div class="mws-form-message error"></div>
     <?php } ?>-->

            	<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-magic"></i> State</span>
                    </div>
                    <div class="mws-panel-body no-padding">
					
					 <?php if($this->uri->segment('4')){?>
                      <form class="mws-form wzd-default" action="<?php echo base_url();?>location/state/edit/<?php if(isset($id)) echo $id;?>" method="post" enctype="multipart/form-data">
					  <?php }else{?>
						<form class="mws-form wzd-default" action="<?php echo base_url();?>location/state/create" method="post" enctype="multipart/form-data">
                       <?php }?>         
                            <fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> General</legend>
                                <div id class="mws-form-row">
                                    <label class="mws-form-label">State Name <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" <?php echo $view; ?> name="state_name" class="required large" value="<?php if(isset($name)) echo $name; ?>" required>
                                    </div>
                                </div>
								
								<div id class="mws-form-row">
                                    <label class="mws-form-label">State Code <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" <?php echo $view; ?> name="state_code" class="required large" value="<?php if(isset($code)) echo $code; ?>" required>
                                    </div>
                                </div>
                              
                                <div class="mws-form-row">
                                    <label class="mws-form-label">Country</label>
                                    <div class="mws-form-item">
                                        
										<select name="country" <?php echo $view; ?>>
										
										<?php foreach($active_countries as $country):?>
									    <option value="<?php echo $country->id;?>" <?php if(isset($country_id) && $country_id==$country->id){echo 'selected="selected"';}?>><?php echo $country->name; ?></option>
											<?php endforeach; ?>
										</select>
                                    </div>
                                </div>
                                 <div class="mws-form-row">
                                    <label class="mws-form-label">Status</label>
                                    <div class="mws-form-item">
                                        <select name="status" <?php echo $view; ?>>
										
									   <option value="1" <?php if(isset($status) && $status=='1'){ echo 'selected="selected"'; } ?>>Active</option>
									   <option value="2" <?php if(isset($status) && $status=='2'){ echo 'selected="selected"'; } ?>>Inactive</option>	
											
										</select>
                                    </div>
                                </div>
								
                            </fieldset>
                            
                            
                            
                          
							
							
							
							
                      
					   
					    </form>
                    </div>
                </div>

              
                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->
			
