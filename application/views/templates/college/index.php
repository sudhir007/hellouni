<div class="container" >
        <div class="row">


			<div class="col-md-12" style="text-align:center;">
				<h3>College Detail</h3>
			</div>
			<!--div class="col-md-6 text-right frnd-search ">
				Search <input type="search" placeholder="friends / alumns" autocomplete="off" >
			</div-->
	</div>
</div>

<div class="container" >

            <div class="row">
			<div class="col-md-12 " style="text-align:center;">
			<table class="table table-bordered">
  <tbody>
      <?php
      foreach($collegeDetail as $key => $value)
      {
          ?>
          <tr>
              <td style="vertical-align: middle"><?=$key?></td>
              <td>
                  <?php
                  if(is_array($value))
                  {
                      foreach($value as $valueData)
                      {
                          switch($key)
                          {
                              case 'Enrollment':
                                    echo "Year: " . $valueData['year'] . "&emsp; Number: " . $valueData['number'] . "<br>";
                                    break;
                              case 'Ranking':
                                    echo "Year: " . $valueData['year'] . "&emsp; By Whom: " . $valueData['by_whom'] . "&emsp; Ranking: " . $valueData['ranking'] . "<br>";
                                    break;
                              case 'Application Deadline (USA)':
                              case 'Application Deadline (International)':
                                    echo "Date: " . $valueData['date'] . "&emsp; Year: " . $valueData['year'] . "<br>";
                                    break;
                              case 'Application Fees (USA)':
                              case 'Application Fees (International)':
                                    echo "Currency: " . $valueData['currency'] . "&emsp; Amount: " . $valueData['amount'] . "<br>";
                                    break;
                              case 'Average GRE':
                                    echo "Quant: " . $valueData['quant'] . "&emsp; Verbal: " . $valueData['verbal'] . "&emsp; AWA: " . $valueData['awa'] . "<br>";
                                    break;
                          }
                          ?>
                          <?php
                      }
                  }
                  else
                  {
                      echo $value;
                  }
                  ?>
              </td>
          </tr>
              <?php
      }
      ?>
	<!--tr><td>Northeastern University</td> <td><a href="https://chat.whatsapp.com/JLJGdVIFJqMEKhfLSJZr1J" target="_blank">https://chat.whatsapp.com/JLJGdVIFJqMEKhfLSJZr1J </a></td> </tr>
	<tr><td>University of Texas Arlington	</td><td><a href="https://chat.whatsapp.com/EqYbkbhejRUCwhQkupGpTb " target="_blank">https://chat.whatsapp.com/EqYbkbhejRUCwhQkupGpTb </a></td></tr>
	<tr><td>University of South Florida </td><td> <a href="https://chat.whatsapp.com/KDswNg1B2itI4Mo9H6C3xY" target="_blank">https://chat.whatsapp.com/KDswNg1B2itI4Mo9H6C3xY </a></td></tr>
	<tr><td>Arizona State University </td><td> <a href="https://chat.whatsapp.com/Jg1jETXzLGLE9OdrvvLcBq" target="_blank">https://chat.whatsapp.com/Jg1jETXzLGLE9OdrvvLcBq</a></td></tr>
	<tr><td>New Jersey Institute of Technology </td><td> <a href="https://chat.whatsapp.com/K2bic7SkDNVIyWMu0xZrDR " target="_blank">https://chat.whatsapp.com/K2bic7SkDNVIyWMu0xZrDR </a></td></tr>
	<tr><td>University of Maryland Baltimore county </td><td> 	<a href="https://chat.whatsapp.com/KfKGPDpYrkmDeLPRZLxdZF " target="_blank">https://chat.whatsapp.com/KfKGPDpYrkmDeLPRZLxdZF </a></td></tr>
	<tr><td>San Jose State University </td><td><a href="https://chat.whatsapp.com/KyxH55oV3ksEa6ZKAn4YgK" target="_blank">https://chat.whatsapp.com/KyxH55oV3ksEa6ZKAn4YgK </a></td></tr>
	<tr><td>California State University Fullerton </td><td><a href="https://chat.whatsapp.com/IdHWajeyyM35VqjmklOsVn " target="_blank">https://chat.whatsapp.com/IdHWajeyyM35VqjmklOsVn </a></td></tr>
	<tr><td>Pace University </td><td><a href="https://chat.whatsapp.com/KxNBX8CB9H1IKY6hMbw7LS " target="_blank">https://chat.whatsapp.com/KxNBX8CB9H1IKY6hMbw7LS </a></td></tr>
	<tr><td>California State University Long beach </td><td><a href="https://chat.whatsapp.com/FGjcwHzoaTL0e9ux6SnlMb " target="_blank">https://chat.whatsapp.com/FGjcwHzoaTL0e9ux6SnlMb </a></td></tr>
	<tr><td>San Francisco State University </td><td><a href="https://chat.whatsapp.com/GrfQxZephTRCFlbgEPXgdw " target="_blank">https://chat.whatsapp.com/GrfQxZephTRCFlbgEPXgdw </a></td></tr>
	<tr><td>University of Massachusetts Lowell </td><td><a href="https://chat.whatsapp.com/COyQunuIEUOIXh8ayQcvgA" target="_blank">https://chat.whatsapp.com/COyQunuIEUOIXh8ayQcvgA </a></td></tr>
	<tr><td>University of Massachusetts Dartmouth </td><td><a href="https://chat.whatsapp.com/IqkjY8YoimCHuTCZWYcl1v" target="_blank">https://chat.whatsapp.com/IqkjY8YoimCHuTCZWYcl1v </a></td></tr>
	<tr><td>New York Institute of Technology </td><td><a href="https://chat.whatsapp.com/FqUlg0cvmXaD2k2Mb4T66r" target="_blank">https://chat.whatsapp.com/FqUlg0cvmXaD2k2Mb4T66r </a></td></tr>
	<tr><td>California State University East Bay </td><td><a href="https://chat.whatsapp.com/Cx1nxQ9Gh5FEcwV0E1WQln" target="_blank">https://chat.whatsapp.com/Cx1nxQ9Gh5FEcwV0E1WQln </a></td></tr>
	<tr><td>MCPHS University </td><td> <a href="https://chat.whatsapp.com/EftWJGPBHeG2cORBScGqVN " target="_blank">https://chat.whatsapp.com/EftWJGPBHeG2cORBScGqVN </a></td></tr>
	<tr><td>University at Albany (SUNY) </td><td><a href="https://chat.whatsapp.com/CKx6OsG61opCLvxzh2EzfP" target="_blank">https://chat.whatsapp.com/CKx6OsG61opCLvxzh2EzfP </a></td></tr>
	<tr><td>University of Bridgeport </td><td><a href="https://chat.whatsapp.com/KBfLrmZpHlVCyWFQPrcZcV " target="_blank">https://chat.whatsapp.com/KBfLrmZpHlVCyWFQPrcZcV </a></td></tr>
	<tr><td>Binghamton University (SUNY) </td><td><a href="https://chat.whatsapp.com/CDalAI67ctL2fdDj6J6xWN " target="_blank">https://chat.whatsapp.com/CDalAI67ctL2fdDj6J6xWN </a></td></tr>
	<tr><td>University of Colorado Denver </td><td><a href="https://chat.whatsapp.com/BBBHFdQejTQERqdIv5mQ5o" target="_blank">https://chat.whatsapp.com/BBBHFdQejTQERqdIv5mQ5o </a></td></tr>
	<tr><td>SUNY Polytechnic Institute </td><td><a href="https://chat.whatsapp.com/BhP3KHhco7x7pdT7fJEb41" target="_blank">https://chat.whatsapp.com/BhP3KHhco7x7pdT7fJEb41 </a></td></tr>
	<tr><td>University of North Texas </td><td><a href="https://chat.whatsapp.com/EMSvBg6uYzeIh5iCc3rNSv" target="_blank">https://chat.whatsapp.com/EMSvBg6uYzeIh5iCc3rNSv </a></td></tr-->
</tbody>
</table>
</div>

            </div>
</div>





<style>

p {
    font-size: 16px;
    line-height: 1.6;
    margin:0 0 10px;
    color:#000;
}
.frnd-search {
	font-weight:bold;
	color:#000000;
	font-size:18px;
}
.frnd-search input {
	padding:10px;
	border:1px solid #ccc;
	font-weight:bold;
	color:#000000;
	font-size:18px;
}


td, th {
font-size: 16px;
}
th {
font-weight:bold;
font-size:18px;
background-color:#f1f1f1;
}
table tr:nth-child(even) td{
background-color:#f1f1f1;
}
td a {
	color:#000000;
}
td a:hover {
	color:#F6881F;
}

</style>
