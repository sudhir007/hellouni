

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Settings
            <small>Manage Settings</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Settings</a></li>
            <li class="active">Settings</li>
          </ol>
        </section>
		
		
		 <?php if($this->session->flashdata('flash_message')){ ?>
				<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                <?php echo $this->session->flashdata('flash_message'); ?>
                </div>
			    <?php } ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
           
            <!-- right column -->
            <div class="col-md-10">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Settings</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
				
				
				<form class="form-horizontal" action="<?php echo base_url();?>elements/settings/update" method="post" name="updatesettings">
			
				
				
                
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Site Title</label>
                      <div class="col-sm-10">
                        <input type="name" class="form-control" id="config_title" name="config_title" placeholder="Site Title" value="<?php if(isset($config_title)) echo $config_title; ?>">
                      </div>
                    </div>
					
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Meta Title</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="config_meta_title" name="config_meta_title" placeholder="Meta Title" value="<?php if(isset($config_meta_title)) echo $config_meta_title; ?>">
                      </div>
                    </div>
					
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Meta Description</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="config_meta_desc" name="config_meta_desc" placeholder="Meta Description" value="<?php if(isset($config_meta_desc)) echo $config_meta_desc; ?>">
                      </div>
                    </div>
					
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Facebook</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="config_facebook" name="config_facebook" placeholder="Facebook" value="<?php if(isset($config_facebook)) echo $config_facebook; ?>">
                      </div>
                    </div>
					
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Twitter</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="config_twitter" name="config_twitter" placeholder="Twitter" value="<?php if(isset($config_twitter)) echo $config_twitter; ?>">
                      </div>
                    </div>
					
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Google Plus</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="config_gplus" name="config_gplus" placeholder="Google Plus" value="<?php if(isset($config_gplus)) echo $config_gplus; ?>">
                      </div>
                    </div>
					
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">LinkedIn</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="config_linkedin" name="config_linkedin" placeholder="LinkedIn" value="<?php if(isset($config_linkedin)) echo $config_linkedin; ?>">
                      </div>
                    </div>
					
					
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Phone Number</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="config_phone" name="config_phone" placeholder="Phone Number" value="<?php if(isset($config_phone)) echo $config_phone; ?>">
                      </div>
                    </div>
					
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="config_email" name="config_email" placeholder="Email" value="<?php if(isset($config_email)) echo $config_email; ?>">
                      </div>
                    </div>
					
					<div class="form-group">
                  		 <label for="status" class="col-sm-2 control-label">Site Status</label>
						  <div class="col-sm-10">
						  <select class="form-control" name="config_site_status">
							<option value="1" <?php if(isset($config_site_status) && $config_site_status=='1'){ echo 'selected="selected"'; } ?>>Active</option>
							<option value="2" <?php if(isset($config_site_status) && $config_site_status=='2'){ echo 'selected="selected"'; } ?>>Inactive</option>	
						  </select>
						  </div>
               		</div>
					
					<div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Google Analytic</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="config_analytic " name="config_analytic " placeholder="Google Analytic" value="<?php if(isset($config_analytic )) echo $config_analytic ; ?>">
                      </div>
                    </div>
					
					
                    
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
					<button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                    <button type="submit" class="btn btn-info pull-right">Update</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              <!-- general form elements disabled -->
             
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     