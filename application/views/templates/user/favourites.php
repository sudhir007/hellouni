<style type="text/css">
   .shade_border{
   background:url(<?php echo base_url();?>img/shade1.png) repeat-x; height:11px;
   }
   .search_by{
   background-color:#f3f4f4; box-shadow:5px 5px 5px #eae8e8; height:auto;
   padding-top: 10px;
   padding-left: 30px;
   padding-right: 30px;
   }
   .search_by h4{
   color:#3c3c3c;
   font-family:lato_regular;
   font-weight:500;
   }
   .first{
   border-left:1px solid #eaeaeb;
   border-top:1px solid #eaeaeb;
   border-bottom:1px solid #eaeaeb;
   text-align:center;
   background:#fbfbfb;
   /*padding: 30px 15px !important;*/
   }
   .last{
   border-right:1px solid #eaeaeb;
   border-top:1px solid #eaeaeb;
   border-bottom:1px solid #eaeaeb;
   text-align:center;
   background:#fbfbfb;
   padding: 30px 15px !important;
   }
   .element{
   border-top:1px solid #eaeaeb;
   border-bottom:1px solid #eaeaeb;
   text-align:center;
   background:#fbfbfb;
   padding: 30px 15px !important;
   }
   #table_heading{
   font-weight:bold;
   padding: 12px 9px 10px 9px;
   text-align: center;
   border-bottom: 1px solid #eaeaeb;
   border-top:1px solid #eaeaeb;
   }
   #table_heading-first{
   width:169px;
   font-weight:bold;
   border-left:1px solid #eaeaeb;
   border-bottom:1px solid #eaeaeb;
   border-top:1px solid #eaeaeb;
   text-align:center;
   background:#fbfbfb;
   padding: 12px 15px 10px 15px;
   }
   #table_heading-last{
   border-right:1px solid #eaeaeb;
   border-bottom:1px solid #eaeaeb;
   border-top:1px solid #eaeaeb;
   text-align:center;
   background:#fbfbfb;
   padding: 12px 15px 10px 15px;
   width:126px;
   }
   .more_detail{
   background-color:#F6881F;
   text-transform:uppercase;
   height:100%;
   border:0px;
   color:#fff;
   border-radius:2px;
   }
   .menutables {
   border-collapse: separate;
   }
   tr .menutables {
   margin-top:10px;
   }
   .delimg{
   cursor:pointer;
   }
   .bkbtn{ background-color: red; width: 9%; height: 35px; border-color: red;}
   .bkbtnDiv{text-align: right; height: 50px;}
   a.button-style {
   padding: 6px 9px;
   }
   .button-style {
   background: #F6881F;
   border: 1px solid transparent;
   padding: 5px 10px;
   color: #fff;
   font-size: 14px;
   text-decoration: none;
   vertical-align: middle;
   display: inline-block;
   border-radius: 10px;
   }


   .tabbable-panel {
   border:1px solid #eee;
   padding: 10px;
   background-color: #f9f9f9;
   }
   /* Default mode */
   .nav-tabs{
      border-bottom: 0px solid #ddd !important;
   }
   .tabbable-line > .nav-tabs {
   border: none !important;
   margin: 0px;
   background-color: #f9f7f7 !important   ;
   }
   .tabbable-line > .nav-tabs > li {
   margin-right: 2px;
   }
   .tabbable-line > .nav-tabs > li > a {
   border: 0;
   margin-right: 0;
   color: #737373;
   }
   .tabbable-line > .nav-tabs > li > a > i {
   color: #a6a6a6;
   }
   .tabbable-line > .nav-tabs > li.open, .tabbable-line > .nav-tabs > li:hover {
   border-bottom: 4px solid #2f7ecd;
   }
   .tabbable-line > .nav-tabs > li.open > a, .tabbable-line > .nav-tabs > li:hover > a {
   border: 0;
   background: none !important;
   color: #333333;
   }
   .tabbable-line > .nav-tabs > li.open > a > i, .tabbable-line > .nav-tabs > li:hover > a > i {
   color: #a6a6a6;
   }
   .tabbable-line > .nav-tabs > li.open .dropdown-menu, .tabbable-line > .nav-tabs > li:hover .dropdown-menu {
   margin-top: 0px;
   }
   .tabbable-line > .nav-tabs > li.active {
   border-bottom: 4px solid #f6882c;
   position: relative;
   }
   .tabbable-line > .nav-tabs > li.active > a {
   border: 0;
   color: #333333;
   }
   .tabbable-line > .nav-tabs > li.active > a > i {
   color: #404040;
   }
   .tabbable-line > .tab-content {
   margin-top: -3px;
   background-color: #fff;
   border: 0;
   border-top: 1px solid #eee;
   padding: 15px 0;
   }
   .portlet .tabbable-line > .tab-content {
   padding-bottom: 0;
   }
   /*course list*/
   .glyphicon { margin-right:5px;}
   .section-box h2 { margin-top:0px;}
   .section-box h2 a { font-size:15px; }
   .glyphicon-heart { color:#e74c3c;}
   .glyphicon-comment { color:#27ae60;}
   .separator { padding-right:5px;padding-left:5px; }
   .section-box hr {margin-top: 0;margin-bottom: 5px;border: 0;border-top: 1px solid rgb(199, 199, 199);}
   .bottom-margin{
   margin-bottom: 3px !important;
   }
   .purple{
   color: #8703c5 !important;
   }
   .orange{
   color: #f6872c !important;
   }
   .wht{
   color: #ffffff !important;
   }
   .purpleBg{
   background: #8703c5 !important;
   }
   .orangeBg{
   background: #f6872c !important;
   }
   .formControl{
   color: #fff !important;
   background-color: #8c57c5 !important;
   /* border: 0px; */
   border: 0px !important;
   box-shadow: inset 0 0 0 rgba(0,0,0,.075) !important;
   }
   .ht30{
   height: 30px;
   }
   .wrn-btnPurple{
   width:30%;
   font-size: 1.8rem;
   font-weight: 400;
   text-transform: capitalize;
   height: calc(4rem + 8px) !important;
   border-radius: 12px;
   background-color: #8c57c5;
   margin-top: 10px;
   color: white;
   }
   .tabButtonUni{
   height: 50px !important;
   color: #f6872c !important;
   background-color: #ffffff !important;
   margin: 15px 15px 0px 0px !important;
   font-size: 18px !important;
   padding: 13px 20px 13px 20px !important;
   border: 2px solid #f6872c !important;
   border-radius: 10px !important;
   }
   .mainTabs .nav-tabs>li.active .tabButtonUni{
   height: 50px !important;
   color: white !important;
   background-color: #f6872c !important;
   margin: 15px 15px 0px 0px !important;
   font-size: 18px !important;
   padding: 13px 20px 13px 20px !important;
   border: 1px solid #f6872c !important;
   border-radius: 10px !important;
   }
   .tabButtonCourse{
   height: 50px !important;
   color: #8c57c5 !important;
   background-color: #ffffff !important;
   margin: 15px 15px 0px 0px !important;
   font-size: 18px !important;
   padding: 13px 20px 13px 20px !important;
   border: 2px solid #8c57c5 !important;
   border-radius: 10px !important;
   }
   .mainTabs .nav-tabs>li.active .tabButtonCourse{
   height: 50px !important;
   color: #ffffff !important;
   background-color: #8c57c5 !important;
   margin: 15px 15px 0px 0px !important;
   font-size: 18px !important;
   padding: 13px 20px 13px 20px !important;
   border: 1px solid #8c57c5 !important;
   border-radius: 10px !important;
   }
   .hrRow>hr{
   margin-top: 12px;
   margin-bottom: 12px;
   border: 0;
   border-top: 2px solid #000;
   }
    .uni-box {
   float: left;
   width: 100%;
   padding: 8px;
   position: relative;
   background-color:#ffffff;
   /*border:1px solid #f0f0f0;*/
   box-shadow: 3px 4px 6px #ccccc4;
   margin: 20px 0px;
   border-right: 7px solid #f6882c;
   }
   .uni-boxPurple{
   float: left;
   width: 100%;
   padding: 8px;
   position: relative;
   background-color:#ffffff;
   /*border:1px solid #f0f0f0;*/
   box-shadow: 3px 4px 6px #ccccc4;
   margin: 20px 0px;
   border-right: 7px solid #8c57c4;
   }
   .uni-box p , .uni-boxPurple p{
   margin:0px;
   line-height:unset;
   font-size: 13px;
   }
   .uni-detail {
   margin-left: 208px;
   }
   .uni-title {
   margin-bottom: 10px;
   width: 90%;
   }
   .uni-title a {
   font-weight: 600;
   font-size: 13px;
   color: #111111;
   }
   .uni-title span {
   color: #666666;
   }
   .uni-sub-title {
   font-weight: 600;
   font-size: 15px;
   color:#666666;
   }
   a.uni-sub-title {
   color:#666666;
   }
   .uni-course-details {
   width: 65%;
   border-right: 1px solid #dadada;
   background: #fff;
   margin: 8px 0;
   padding: 8px;
   position: relative;
   margin-right: 25px;
   }
   .uni-course-details:before {
   width: 100%;
   height: 1px;
   right: -1px;
   bottom: -1px;
   background-image: -webkit-gradient(linear,0 100%,0 0,from(#fff),to(#dadada));
   background-image: -webkit-linear-gradient(right,#dadada,#fff);
   background-image: -moz-linear-gradient(right,#dadada,#fff);
   background-image: -o-linear-gradient(#fff,#dadada);
   }
   .uni-course-details:after, .uni-course-details:before {
   content: "";
   position: absolute;
   }
   .uni-image {
   background: #fff;
   border: 1px solid #f3f3f3;
   padding: 5px;
   position: relative;
   width: 200px;
   height: 185px;
   }
   ul.uni-cont li {
   background: #fbfbfb;
   border: 1px solid #f3f3f3;
   margin-bottom: 8px;
   list-style: none;
   }
   .clearwidth {
   width: 100%;
   float: left;
   }
   .univ-tab-details {
   margin: 30px 0 10px 0!important;
   }
   .flLt {
   float: left;
   }
   .flRt {
   float: right;
   }
   .iconRow .iconCss{
   background: #f6872c;
   padding: 7px;
   height: 30px;
   width: 30px;
   border-radius: 50%;
   color: white;
   font-size: 15px;
   margin: 5px 0px 5px 0px;
   }
   .iconRow strong {
   font-size: 13px;
   color: #f6872c;
   }
   .iconRow span {
   font-size: 13px;
   color: #000;
   }
   .iconRowPurple .iconCss{
   background: #8c57c4;
   padding: 7px;
   height: 30px;
   width: 30px;
   border-radius: 50%;
   color: white;
   font-size: 15px;
   margin: 5px 0px 5px 0px;
   }
   .iconRowPurple strong {
   font-size: 13px;
   color: #8c57c4;
   }
   .iconRowPurple span {
   font-size: 13px;
   color: #000;
   }
   .font14{
   font-size: 14px;
   }
   .mrgtop{
   margin: 9px 0px 5px 0px; ;
   }

   .mrginNEw{
    margin: 10px 0px;
   }

   .font-16 {
   font-size: 16px;
   letter-spacing: 0.5px;
   }

   .backButton{
    height: 50px !important;
    color: #000000 !important;
    background-color: #ffffff !important;
    margin: 15px 15px 0px 0px !important;
    font-size: 18px !important;
    padding: 13px 20px 13px 20px !important;
    border: 2px solid #000000 !important;
    border-radius: 10px !important;
   }
</style>
<div class="clearfix"></div>
<div id="page_loader">
   <div class="shade_border"></div>
   <!--div class="container hidden-xs " style="padding: 17px; " >
      <img src="<?php echo base_url();?>img/page1.png" width="100%"  />

      </div-->
   <!-- <h4 style="text-align:center;">My Favourites Universities</h4> -->
   <!-- <div class="container visible-xs" style="text-align:center;">
      <img src="<?php echo base_url();?>img/page1-1.jpg" />
   </div> -->
   <div class="shade_border"></div>
   <div class="container" style="display: none;">
      <div class="clearfix visible-xs visible-sm "></div>
      <br class="visible-xs visible-sm "/>
      <div class="col-md-12 col-sm-12">
         <div class="table-responsive">
            <form name="myform" action="checkboxes.asp" method="post">
               <table class="table menutables" width="100%" cellspacing="10">
                  <thead>
                     <tr>
                        <td style="border:1px solid #eaeaeb; text-align:center; padding: 10px 15px 10px 15px;" valign="middle" width="48">
                           <input type="checkbox" name="Check_ctr" value="yes" onClick="Check(this)">
                        </td>
                        <td class="table_heading" id="table_heading-first">University</td>
                        <td class="table_heading" id="table_heading">University Name</td>
                        <td class="table_heading" id="table_heading">Country</td>
                        <td class="table_heading" id="table_heading">Area Of Interest</td>
                        <td  class="table_heading" id="table_heading">Level Of Course</td>
                        <td class="table_heading" id="table_heading"></td>
                        <td class="table_heading" id="table_heading-last"></td>
                     </tr>
                  </thead>
                  <tr>
                     <td colspan="8">&nbsp;</td>
                  </tr>
                  <?php if($favourites){
                     foreach($favourites as $fav){?>
                  <tr>
                     <td class="table_heading" style="border:1px solid #eaeaeb; text-align:center; padding:40px 15px; "valign="middle" width="48">
                        <input type="checkbox" name="check_list" value="<?php echo $fav['id'];?>">
                     </td>
                     <td class="table_heading first" valign="middle">
                        <img class="lazy" data-original="<?=$fav['banner']?>" alt="<?=$fav['name']?>" title="<?=$fav['name']?>" align="center" width="115" height="100" src="<?=$fav['banner']?>" style="display: inline;">
                        <!--img src="<?php echo base_url();?>img/university_logo.png" width="115"/></td-->
                     <td class="table_heading element" width="129"><?php echo $fav['name'];?></td>
                     <td class="table_heading element" width="88"><?php echo $fav['countryname'];?></td>
                     <td class="table_heading element" width="137"><?php echo $fav['coursename'];?></td>
                     <td  class="table_heading element" width="131"><?php echo $fav['degreename'];?></td>
                     <td class="table_heading element" width="137"><img onclick="deletefav(<?php echo $fav['id'];?>)" class="img-responsive delimg" src="<?php echo base_url();?>img/delete.png"></td>
                     <td class="table_heading last"><a href="/<?php echo $fav['slug'];?>" class="button-style" target="_blank">View Detail</a></td>
                  </tr>
                  <?php } }else{?>
                  <tr>
                     <td class="table_heading" style="border:1px solid #eaeaeb; text-align:center; padding:40px 15px; "valign="middle" width="48" colspan="8">
                        There is no university in the favourites listing
                     </td>
                  </tr>
                  <?php } ?>
                  <tr>
                     <td colspan="8">&nbsp;</td>
                  </tr>
               </table>
            </form>
            <div class="clearfix"></div>
            <div class="bkbtnDiv"><a href="<?php echo base_url();?>search/university"><input type="button" class="bkbtn" value="Back To Listing"/></a></div>
            <div class="clearfix"></div>
         </div>
      </div>
   </div>
<!--    <h4 style="text-align:center;">My Favourites Courses</h4>
 -->   <div class="container" style="display: none;">
      <div class="clearfix visible-xs visible-sm "></div>
      <br class="visible-xs visible-sm "/>
      <div class="col-md-12 col-sm-12">
         <div class="table-responsive">
            <form name="myform" action="checkboxes.asp" method="post">
               <table class="table menutables" width="100%" cellspacing="10">
                  <thead>
                     <tr>
                        <td style="border:1px solid #eaeaeb; text-align:center; padding: 10px 15px 10px 15px;" valign="middle" width="48">
                           <input type="checkbox" name="Check_ctr" value="yes" onClick="Check(this)">
                        </td>
                        <td class="table_heading" id="table_heading-first">University</td>
                        <td class="table_heading" id="table_heading">Course Name</td>
                        <td class="table_heading" id="table_heading">University Name</td>
                        <td class="table_heading" id="table_heading">Country</td>
                        <td  class="table_heading" id="table_heading">Level Of Course</td>
                        <td class="table_heading" id="table_heading"></td>
                        <td class="table_heading" id="table_heading-last"></td>
                     </tr>
                  </thead>
                  <tr>
                     <td colspan="8">&nbsp;</td>
                  </tr>
                  <?php if($favourites_courses){
                     foreach($favourites_courses as $fav){?>
                  <tr>
                     <td class="table_heading" style="border:1px solid #eaeaeb; text-align:center; padding:40px 15px; "valign="middle" width="48">
                        <input type="checkbox" name="check_list" value="<?php echo $fav['id'];?>">
                     </td>
                     <td class="table_heading first" valign="middle">
                        <img class="lazy" data-original="<?=$fav['banner']?>" alt="<?=$fav['name']?>" title="<?=$fav['name']?>" align="center" width="115" height="100" src="<?=$fav['banner']?>" style="display: inline;">
                        <!--img src="<?php echo base_url();?>img/university_logo.png" width="115"/></td-->
                     <td class="table_heading element" width="129"><?php echo $fav['coursename'];?></td>
                     <td class="table_heading element" width="129"><?php echo $fav['name'];?></td>
                     <td class="table_heading element" width="88"><?php echo $fav['countryname'];?></td>
                     <td  class="table_heading element" width="131"><?php echo $fav['degreename'];?></td>
                     <td class="table_heading element" width="137"><img onclick="deletefavcourse(<?php echo $fav['id'];?>)" class="img-responsive delimg" src="<?php echo base_url();?>img/delete.png"></td>
                     <td class="table_heading last"><a href="/course/coursemaster/courselist/<?php echo $fav['course_id'];?>" class="button-style" target="_blank" style="background-color: #8c57c4;">View Detail</a></td>
                  </tr>
                  <?php } }else{?>
                  <tr>
                     <td class="table_heading" style="border:1px solid #eaeaeb; text-align:center; padding:40px 15px; "valign="middle" width="48" colspan="8">
                        There is no university in the favourites listing
                     </td>
                  </tr>
                  <?php } ?>
                  <tr>
                     <td colspan="8">&nbsp;</td>
                  </tr>
               </table>
            </form>
            <div class="clearfix"></div>
            <div class="bkbtnDiv"><a href="<?php echo base_url();?>search/university"><input type="button" class="bkbtn" value="Back To Listing"/></a></div>
            <div class="clearfix"></div>
         </div>
      </div>
   </div>

   <div class="container">
    <div id="universityListBox" class="col-md-12">
         <!-- <div class="row">
         </div> -->
         <div class="row">
            <div class="tabbable-panel">
                  <div class="col-md-12" style="padding-bottom: 15px; margin-bottom: 5px;border-bottom: 1px solid #505050;">
                     <div class="col-md-12 mainTabs">
                        <ul class="nav nav-tabs ">
                           <li class="active">
                              <a class="btn btn-default tabButtonUni" href="#tab_default_1" data-toggle="tab" onclick="changeColurOrg()">
                                 <b>Favourites Universities</b>
                              </a>

                           </li>
                           <li>
                              <a class="btn btn-default tabButtonCourse" href="#tab_default_2" data-toggle="tab" onclick="changeColurPuplr()">
                                 <b>Favourites Courses</b>
                              </a>
                           </li>
                           <li>
                              <a class="btn btn-default tabButtonUni" href="#tab_default_3" data-toggle="tab" onclick="changeColurOrg()">
                                 <b>Favourites Researches</b>
                              </a>
                           </li>
                           <li>
                              <a class="btn btn-lg btn-danger backButton" href="<?php echo base_url();?>search/university" >
                                <b>Back To Listing</b>
                              </a>
                           </li>
                        </ul>
                     </div>
                     <!-- <div class="col-md-5 text-right" style="display: inline-flex;margin-top: 20px;">
                        <p class="sort-filter">
                           Sort by &nbsp;&nbsp;</p>
                           <div id="mainselection" style="height: 35px;width: 70%;">
                              <select name="unsort" id="unsort" onchange="searchFilters()">
                                 <option value="ranking_world" selected>Ranking</option>
                                 <option value="application_fee_amount" >Application Fees</option>
                                 <option value="total_fees" >Tuition Fees</option>
                              </select>
                           </div>
                     </div> -->
                  </div>
                  <div class="tab-content">
                     <div class="tab-pane active" id="tab_default_1">
                        <form id="compare-university-form" name="compare-university-form" action="" method="POST">
                          <?php if($favourites){
                          foreach($favourites as $fav){?>
                           <div  class="uni-box">
                              <div class="flLt">
                                 <div class="uni-image">
                                    <a target="_blank" href="#"><img class="lazy" data-original="<?=$fav['banner']?>" alt="<?=$fav['name']?>" title="<?=$fav['name']?>" align="center" width="190px" height="175px" src="<?=$fav['banner']?>" style="display: inline;"></a>


                                    <div class="uni-shrtlist-image"></div>
                                 </div>
                              </div>

                              <div class="uni-detail">
                                 <div class="uni-title">
                                    <p>
                                       <a href="/<?php echo $fav['slug'];?>" target="_blank"><span class="font-16 orange"
                                       ><?php echo $fav['name'];?></span></a><span class="font-11"></span>
                                    </p>
                                 </div>
                                 <div class="clearwidth">
                                    <!--p><a target="_blank" href="#" class="uni-sub-title">Computer Science&nbsp;MSc</a></p-->
                                    <div class="row iconRow">
                                       <div class="col-md-12">
                                          <div class="col-md-6">

                                             <div class="col-md-12 mrginNEw"><strong> <i class="fas fa-university iconCss" >&nbsp;&nbsp;</i>  University Name :<span>&nbsp; <?php echo $fav['name'];?></span></strong></div>
                                          </div>
                                          <div class="col-md-6">

                                             <div class="col-md-12 mrginNEw"><strong>  <i class="fas fa-globe iconCss" >&nbsp;&nbsp;</i>  Country :<span>&nbsp; <?php echo $fav['countryname'];?></span></strong></div>
                                          </div>
                                          <div class="col-md-6">

                                             <div class="col-md-12 mrginNEw"><strong>  <i class="fas fa-bookmark iconCss" >&nbsp;&nbsp;</i>  Area Of Interest <span>&nbsp; <?php echo $fav['coursename'];?></span></strong></div>
                                          </div>
                                          <div class="col-md-6">

                                             <div class="col-md-12 mrginNEw"><strong>  <i class="fas fa-layer-group iconCss" >&nbsp;&nbsp;</i>  Level Of Course <span>&nbsp; <?php echo $fav['degreename'];?></span></strong></div>
                                          </div>

                                       </div>


                                    </div>
                                    <div class="flLt hrRow" style="width:100%;"><hr></div>
                                    <div class="row" style="margin-top: 10px !important">
                                          <div class="col-md-3 btn-col btn-brochure" style="text-align: center;">
                                             <a href="/<?php echo $fav['slug'];?>" class="button-style" target="_blank">View Detail</a>
                                          </div>
                                          <div class="col-md-3" style="text-align: left;">
                                             <button class="btn btn-col btn-danger btn-brochure"  onclick="deletefav(<?php echo $fav['id'];?>)" style="border-radius: 8px;padding: 7px 22px;">Delete</button>
                                          </div>
                                          <!-- <td class="table_heading element" width="137"><img onclick="deletefav(<?php echo $fav['id'];?>)" class="img-responsive delimg" src="<?php echo base_url();?>img/delete.png"></td> -->
                                          <div class="col-md-4 font14 mrgtop">
                                             <i class="fa fa-eye" >&nbsp;<?php echo $fav['university_view_count'] ;?></i> | &nbsp;


                                             <i class="fa fa-heart" >&nbsp;<?php echo $fav['university_likes_count'] ;?></i> |&nbsp;


                                            <i class="fa fa-star" >&nbsp;<?php echo $fav['university_fav_count'] ;?></i>

                                          </div>

                                    </div>

                                 </div>

                                 <div class="clearfix"></div>
                              </div>
                           </div>
                           <?php
                              }
                              ?>
                        </form>
                        <?php
                           }else{?>
                        <h1>There is no university in the favourites listing</h1>
                        <?php } ?>

                        <!-- Keepit  -->
                        <p>
                           <a class="btn" href="#" target="_blank" >
                           </a>
                        </p>
                        <!-- keepit -->
                     </div>
                     <div class="tab-pane" id="tab_default_2">

                           <form id="compare-course-form" name="compare-course-form" action="" method="">
                            <?php if($favourites_courses){
                     foreach($favourites_courses as $fav){?>
                           <div class="col-md-12">
                              <div  class="uni-boxPurple">
                              <div class="flLt">
                                 <div class="uni-image">
                                    <a target="_blank" href="#"><img class="lazy" data-original="<?=$fav['banner']?>" alt="<?=$fav['name']?>" title="<?=$fav['name']?>" align="center" width="190px" height="175px" src="<?=$fav['banner']?>" style="display: inline;"></a>
                                    <div class="uni-shrtlist-image"></div>
                                 </div>
                              </div>
                              <div class="uni-detail">
                                 <div class="uni-title">
                                    <p>
                                       <a target="_blank" href="/course/coursemaster/courselist/<?php echo $fav['course_id'];?>" target="_blank"><span class="font-16 purple"><?php echo $fav['coursename'];?> </span> </a>
                                    </p>
                                 </div>
                                 <div class="clearwidth">
                                    <!--p><a target="_blank" href="#" class="uni-sub-title">Computer Science&nbsp;MSc</a></p-->
                                    <div class="row iconRowPurple">
                                       <div class="col-md-12">
                                          <div class="col-md-6">

                                             <div class="col-md-12 mrginNEw"><strong>  <i class="fas fa-school iconCss" >&nbsp;&nbsp;</i>  Course Name <span>&nbsp;<?php echo $fav['name'];?></span></strong></div>
                                          </div>
                                          <div class="col-md-6">

                                             <div class="col-md-12 mrginNEw"><strong>  <i class="fas fa-globe iconCss" >&nbsp;&nbsp;</i>  Country <span>&nbsp; <?php echo $fav['countryname'];?></span></strong></div>
                                          </div>
                                          <div class="col-md-6">

                                             <div class="col-md-12 mrginNEw"><strong>
                                             <i class="fas fa-layer-group iconCss"></i>  Level Of Course <span>&nbsp; <?php echo $fav['degreename'];?>  </span></strong></div>
                                          </div>

                                       </div>

                                    </div>
                                    <div class="flLt hrRow" style="width:100%;"><hr></div>
                                    <div class="row" style="margin-top: 10px !important">
                                          <div class="col-md-3 btn-col btn-brochure" style="text-align: center;">
                                             <a href="/course/coursemaster/courselist/<?php echo $fav['course_id'];?>" class="button-style purpleBg" target="_blank">View Detail</a>
                                          </div>
                                          <div class="col-md-3" style="text-align: left;">
                                             <button class="btn btn-col btn-danger btn-brochure"  onclick="deletefavcourse(<?php echo $fav['id'];?>,'courses')" style="border-radius: 8px;padding: 7px 22px;">Delete</button>
                                          </div>

                                          <div class="col-md-4 font14 mrgtop txtAlignRight">
                                             <i class="fa fa-eye" >&nbsp;<?php echo $fav['course_view_count'] ;?></i> | &nbsp;


                                             <i class="fa fa-heart" >&nbsp;<?php echo $fav['course_likes_count'] ;?></i> | &nbsp;


                                            <i class="fa fa-star" >&nbsp;<?php echo $fav['course_fav_count'] ;?></i>

                                          </div>


                                    </div>

                                 </div>


                                 <div class="clearfix"></div>
                              </div>
                           </div>
                           </div>
                           <?php
                              }
                              ?>
                        </form>
                       <?php
                           }else{?>
                        <h1>There are no courses in the favourite listing</h1>
                        <?php } ?>

                        <!-- Keepit  -->
                        <p>
                           <a class="btn" href="#" target="_blank" >
                           </a>
                        </p>
                        <!-- keepit -->
                     </div>

                     <div class="tab-pane" id="tab_default_3">

                           <form id="compare-course-form" name="compare-course-form" action="" method="">
                            <?php if($favourites_researches){
                     foreach($favourites_researches as $fav){?>
                           <div class="col-md-12">
                              <div  class="uni-boxPurple">
                              <div class="flLt">
                                 <div class="uni-image">
                                    <a target="_blank" href="#"><img class="lazy" data-original="<?=$fav['banner']?>" alt="<?=$fav['name']?>" title="<?=$fav['name']?>" align="center" width="190px" height="175px" src="<?=$fav['banner']?>" style="display: inline;"></a>
                                    <div class="uni-shrtlist-image"></div>
                                 </div>
                              </div>
                              <div class="uni-detail">
                                 <div class="uni-title">
                                    <p>
                                       <a target="_blank" href="/research_Details/<?php echo $fav['research_id'];?>"><span class="font-16 purple"><?php echo $fav['researchname'];?> </span> </a>
                                    </p>
                                 </div>
                                 <div class="clearwidth">
                                    <!--p><a target="_blank" href="#" class="uni-sub-title">Computer Science&nbsp;MSc</a></p-->
                                    <div class="row iconRowPurple">
                                       <div class="col-md-12">
                                          <div class="col-md-6">

                                             <div class="col-md-12 mrginNEw"><strong>  <i class="fas fa-school iconCss" >&nbsp;&nbsp;</i>  University Name <span>&nbsp;<?php echo $fav['name'];?></span></strong></div>
                                          </div>
                                          <div class="col-md-6">

                                             <div class="col-md-12 mrginNEw"><strong>  <i class="fas fa-globe iconCss" >&nbsp;&nbsp;</i>  Country <span>&nbsp; <?php echo $fav['countryname'];?></span></strong></div>
                                          </div>
                                          <div class="col-md-6">

                                             <div class="col-md-12 mrginNEw"><strong>
                                             <i class="fas fa-calendar-alt iconCss"></i>  Department Name <span>&nbsp; <?php echo $fav['department_name'];?>  </span></strong></div>
                                          </div>

                                       </div>

                                    </div>
                                    <div class="flLt hrRow" style="width:100%;"><hr></div>
                                    <div class="row" style="margin-top: 10px !important">
                                          <div class="col-md-3 btn-col btn-brochure" style="text-align: center;">
                                             <a href="/research_Details/<?php echo $fav['research_id'];?>" class="button-style purpleBg" target="_blank">View Detail</a>
                                          </div>
                                          <div class="col-md-3" style="text-align: left;">
                                             <button class="btn btn-col btn-danger btn-brochure"  onclick="deletefavcourse(<?php echo $fav['id'];?>,'researches')" style="border-radius: 8px;padding: 7px 22px;">Delete</button>
                                          </div>

                                          <div class="col-md-4 font14 mrgtop txtAlignRight">
                                             <i class="fa fa-eye" >&nbsp;<?php echo $fav['research_view_count'] ;?></i> | &nbsp;


                                             <i class="fa fa-heart" >&nbsp;<?php echo $fav['research_likes_count'] ;?></i> | &nbsp;


                                            <i class="fa fa-star" >&nbsp;<?php echo $fav['research_fav_count'] ;?></i>

                                          </div>


                                    </div>

                                 </div>


                                 <div class="clearfix"></div>
                              </div>
                           </div>
                           </div>
                           <?php
                              }
                              ?>
                        </form>
                       <?php
                           }else{?>
                        <h1>There are no courses in the favourite listing</h1>
                        <?php } ?>

                        <!-- Keepit  -->
                        <p>
                           <a class="btn" href="#" target="_blank" >
                           </a>
                        </p>
                        <!-- keepit -->
                     </div>

                  </div>

            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
   function deletefav(id){
   if(confirm("Do you really want to delete this favourite")==true){
      window.location.href='<?php echo base_url();?>user/favourites/delete/'+id;
     }
   }

   function deletefavcourse(favId,type){
     if(confirm("Do you really want to delete this "+type+" favourite")==true){
     jQuery.ajax({
           type: "GET",
           url: "/user/socialcounts/favDelete/" + favId + "/"+type,
         success: function(response) {
              alert("The "+type+" has been removed successfully from your favourites");
               window.location.href='<?php echo base_url();?>user/favourites';
           }
       });
     }
   }
</script>
