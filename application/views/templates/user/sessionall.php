<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<style type="text/css">
    .styleCss{
        font-size: 14px;
        background-color: #f6882c;
        color: white;
        font-weight: 600;
        letter-spacing: 0.8px;
    }
    .formControl{
        color: #fff !important;
        background-color: #8c57c5 !important;
        font-size: 12px;
        border: 0px !important;
        box-shadow: inset 0 0 0 rgba(0,0,0,.075) !important;
        margin-top: 15px;
    }
    #dt-example td.details-control {
        cursor: pointer;
    }
    #dt-example tr.shown td {
        background-color: #815dd5;
        color: #fff !important;
        border: none;
        border-right: 1px solid #815dd5;
    }
    #dt-example tr.shown td:last-child {
        border-right: none;
    }
    #dt-example tr.details-row .details-table tr:first-child td {
        color: #fff;
        background: #f6882c;
        border: none;
    }
    #dt-example tr.details-row .details-table > td {
        padding: 0;
    }
    #dt-example tr.details-row .details-table .fchild td:first-child {
        cursor: pointer;
    }
    #dt-example tr.details-row .details-table .fchild td:first-child:hover {
        background: #fff;
    }
    #dt-example .form-group.agdb-dt-lst {
        padding: 2px;
        height: 23px;
        margin-bottom: 0;
    }
    #dt-example .form-group.agdb-dt-lst .form-control {
        height: 23px;
        padding: 2px;
    }
    #dt-example .adb-dtb-gchild {
        padding-left: 2px;
    }
    #dt-example .adb-dtb-gchild td {
        background: #f5fafc;
        padding-left: 15px;
    }
    #dt-example .adb-dtb-gchild td:first-child {
        cursor: default;
    }
    .dataTables_wrapper{
        overflow: auto;
    }
    .listTableView thead tr{
        background-color: #f6882c;
        color: white;
    }
    a{
        color: #337ab7;
    }
</style>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>

<div class="container" style="clear:both;">
    <div class="row" style="padding: 30px 0px 30px 0px;display: flex;">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body listTableView">
                    <table id="dt-example" class="stripe row-border order-column cl-table dataTable no-footer" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th>Sl No</th>
                            <th>Student Name</th>
                            <th>Student Email</th>
                            <th>Slot 1</th>
                            <th>Slot 2</th>
                            <th>Slot 3</th>
                            <th>Confirmed Slot</th>
                            <th>Status</th>
                            <th>Timezone</th>
                            <th>Option</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#data-table').dataTable();
        $(".dataTable").on("draw.dt", function (e) {
            setCustomPagingSigns.call($(this));
        }).each(function () {
            setCustomPagingSigns.call($(this)); // initialize
        });

        function setCustomPagingSigns() {
            var wrapper = this.parent();
            wrapper.find("a.previous").text("<");
            wrapper.find("a.next").text(">");
        }

        var data = [];
        var dataString = '';

        $.ajax({
            type: "POST",
            url: "/user/session/allsession",
            data: dataString,
            cache: false,
            success: function(result){
                var sessiondata = result.sessions;
                if(sessiondata.length){
                    sessiondata.forEach(function(value, index){
                        data.push({
                            "Sl_No" : '<input type="checkbox" class="multichk" name="slot_ids[]" value="'+ value.id +'"/> &nbsp; ' + index+1,
                            "Student_Name" : value.name,
                            "Student_Email" : value.email,
                            "Slot_1" : value.slot_1,
                            "Slot_2" : value.slot_2,
                            "Slot_3" : value.slot_3,
                            "Confirmed_Slot" : value.confirmed_slot ? value.confirmed_slot : '',
                            "Status" : value.status,
                            "Timezone" : "GMT "+value.timezone,
                            "Action" : '<a href="<?php echo base_url();?>user/session/form/"'+ value.id +'" title="Modify"><i class="fa fa-edit"></i></a> &nbsp;\
                                        <a href="<?php echo base_url();?>user/session/delete?slot_id="'+ value.id +'"&redirectUrl="'+ value.id +'" title="Delete" class="del" id="'+ value.id +'"><i class="fa fa-trash-o"></i></a> '
                        });
                    });

                    var table = $('.dataTable').DataTable({
                        columns : [
                            {data : 'Sl_No'},
                          {
                                data : 'Student_Name',
                                className : 'details-control',
                            },
                            {data : 'Student_Email'},
                            {data : 'Slot_1'},
                            {data : 'Slot_2'},
                            {data : 'Slot_3'},
                            {data : 'Confirmed_Slot'},
                            {data : 'Status'},
                            {data : 'Timezone'},
                            {data : 'Action'}
                        ],
                        data : data,
                        pagingType : 'full_numbers',
                        language : {
                            emptyTable     : 'No data to display.',
                            zeroRecords    : 'No records found!',
                            thousands      : ',',
                            loadingRecords : 'Loading...',
                            search         : 'Search:',
                            paginate       : {
                                next     : 'next',
                                previous : 'previous'
                            }
                        },
                        "bDestroy": true
                    });

                    $('.dataTable tbody').on('click', 'td.details-control', function () {
                        var tr  = $(this).closest('tr'),
                        row = table.row(tr);

                        if (row.child.isShown()) {
                            tr.next('tr').removeClass('details-row');
                            row.child.hide();
                            tr.removeClass('shown');
                        }
                        else {
                            row.child(format(row.data())).show();
                            tr.next('tr').addClass('details-row');
                            tr.addClass('shown');
                        }
                    });

                    $('#dt-example').on('click','.details-table tr',function(){
                        $('.details-table').find('.adb-dtb-gchild:not(:first-child)').slideToggle();
                    });
                }
            }
        });
    });
</script>
