
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Sessions
            <small>Pre define Slot List</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Session Management</a></li>
            <li class="active">Pre define Slot List</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
                <?php
                if($this->session->flashdata('flash_message'))
                {
                    ?>
      				<div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-fw fa-close"></i></button>
                      <h4><i class="icon fa fa-times"></i> Error</h4>
                      <?php echo $this->session->flashdata('flash_message'); ?>
                    </div>
      			  <?php
                }
                ?>
			  <!-- /.box -->
              <form action="<?php echo base_url();?>user/student/form/" method="POST">

              <div class="box">

                <div class="box-header">
                 <a href="<?php echo base_url();?>user/student/slotform">
                   <button type="button" style="width:12%; float:left; margin-right:5px;" class="btn btn-block btn-primary">
                     <i class="fa fa-plus"></i> Add Slots</button>
                   </a>

                </div>

                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sl No</th>
                        <?=($user_detail['type'] != 3) ? '<th>University</th>' : ''?>

                        <th>Slot 1</th>
                        <th>Slot 2</th>
                        <th>Slot 3</th>

                        <th>Timezone</th>
                        <th> </th>
                      </tr>
                    </thead>
                    <tbody>
					<?php $i=1;
					 foreach($predefined_slots as $session){ ?>
                      <tr>
                        <td><input type="checkbox" class="multichk" name="slot_ids[]" value="<?php echo $session->id;?>"/>&nbsp;<?=$i++?></td>
                        <?=($user_detail['type'] != 3 && isset($session->university_name)) ? '<td>' . $session->university_name . '</td>' : ''?>

                        <td><?=$session->slot_1?></td>
                        <td><?=$session->slot_2?></td>
                        <td><?=$session->slot_3?></td>
                        <td><?php echo 'GMT' . $session->timezone;?></td>

                        <td>
                            <a href="<?php echo base_url();?>user/student/predefineslotdelete/<?php echo $session->id;?>" title="Delete" class="del" ><i class="fa fa-trash-o"></i></a>
                        </td>

                    </tr>
					<?php } ?>


                    </tbody>
                    <tfoot>
                      <tr>
                          <th>Sl No</th>
                          <?=($user_detail['type'] != 3) ? '<th>University</th>' : ''?>

                          <th>Slot 1</th>
                          <th>Slot 2</th>
                          <th>Slot 3</th>

                          <th>Timezone</th>
                          <th> <th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
          </form>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
