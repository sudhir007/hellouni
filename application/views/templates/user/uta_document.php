<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.7/js/dataTables.fixedHeader.min.js"></script>
<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<style type="text/css">
   .mytabs{
   margin: 2% 0px;
   }
   .mytabs .nav-tabs {
   background-color: #815dd5;
   }
   .mytabs .nav-tabs>li>a : hover {
   color: black;
   }
   .prfileBox3{
   background-color: #815dd5;
   margin: 2% 0px;
   padding: 1%;
   border-radius: 7px;
   }
   .margin2{
   margin: 2px 0 2px !important;
   }
   .ptag{
   margin: 0px;
   font-size: 13px;
   line-height: 1.8;
   letter-spacing: 1px;
   }
   .h3Heading{
   background-color: #f6882c;
   padding: 8px 2px;
   text-align: center;
   color: white;
   }
   .boxAppList{
   box-shadow: 0px 0px 12px #d8d1d1;
   border-radius: 7px;
   }
   .listtableMain thead tr {
   color: #815dd5;
   font-size: 13px;
   /* font-weight: 600; */
   letter-spacing: 0.8px;
   }
   table.dataTable.stripe tbody tr.odd, table.dataTable.display tbody tr.odd{
   background-color: #ffffff !important;
   }
   table.dataTable.display tbody tr.odd>.sorting_1, table.dataTable.order-column.stripe tbody tr.odd>.sorting_1{
   background-color: #ffffff !important;
   }
   table.dataTable.display tbody tr.even>.sorting_1, table.dataTable.order-column.stripe tbody tr.even>.sorting_1{
   background-color: #ffffff !important;
   }
   .form-control {
   height: 30px !important;
   }
   .prplButton{
   color: #fff;
   background: #8703c5;
   border-color: #8703c5;
   border-radius: 12px;
   }
   .resetButton{
   color: #8c57c6;
   background: #f1f1f1;
   border-color: #8703c5;
   border-radius: 12px;
   }
   radio-item {
   display: inline-block;
   position: relative;
   padding: 0 6px;
   /*margin: 10px 0 0;*/
   }
   .radio-item label {
   color: #666;
   font-weight: normal;
   }
   .radio-item label:before {
   content: " ";
   display: inline-block;
   position: relative;
   top: 4px;
   margin: 0 5px 0 0;
   width: 15px;
   height: 15px;
   border-radius: 11px;
   border: 2px solid #f6882c;
   background-color: transparent;
   }
   .radio-item input[type=radio]:checked + label:after {
   border-radius: 11px;
   width: 7px;
   height: 7px;
   position: absolute;
   top: 8px;
   left: 10px;
   content: " ";
   display: block;
   background: #f6882c;
   }
   .tabName{
   letter-spacing: 0.6px;
   font-size: 14px;
   }
   .checkBoxDesign{
   box-shadow: 0 0 black !important;
   /*margin-top: -4px !important;*/
   }
   /* Style tab links */
   .tablink {
   background-color: #f7f7f7;
   color: #f39d3f;
   float: left;
   border: none;
   outline: none;
   cursor: pointer;
   padding: 10px 10px;
   font-size: 18px;
   font-weight: 600;
   letter-spacing: 1.3px;
   width: 25%;
   }
   .tablink:hover {
   background-color: #eae7e7;
   }
   /* Style the tab content (and add height:100% for full page content) */
   .tabcontent {
   color: white;
   padding: 70px 20px;
   height: 100%;
   }
   /*application*/
   h1 {
   color: #333;
   font-family: arial, sans-serif;
   margin: 1em auto;
   width: 80%;
   }
   .tabordion {
   color: #333;
   display: block;
   font-family: arial, sans-serif;
   margin: auto;
   position: relative;
   width: 100%;
   }
   .tabordion input[name="sections"] {
   left: -9999px;
   position: absolute;
   top: -9999px;
   }
   .tabordion section {
   display: block;
   }
   .tabordion section label {
   background: #ffffff;
   border-right: 3px solid #815dd5;
   cursor: pointer;
   display: block;
   font-size: 1.2em;
   font-weight: bold;
   padding: 1px 15px;
   position: relative;
   width: 26%;
   z-index:100;
   }
   .tabordion section article {
   background: white;
   left: 27%;
   min-width: 300px;
   /* padding: 0 0 0 21px;*/
   position: absolute;
   top: 0;
   }
   .tabordion section article:after {
   /*background-color: #ccc;*/
   bottom: 0;
   content: "";
   display: block;
   left:-229px;
   position: absolute;
   top: 0;
   width: 220px;
   z-index:1;
   }
   .tabordion input[name="sections"]:checked + label {
   background: white;
   color: #fff;
   border-left: 3px solid #f39d3f;
   }
   .tabordion input[name="sections"]:checked + label i {
   color: #f6882c;
   }
   .tabordion input[name="sections"]:checked ~ article {
   display: block;
   }
   @media (max-width: 533px) {
   h1 {
   width: 100%;
   }
   .tabordion {
   width: 100%;
   }
   .tabordion section label {
   font-size: 1em;
   width: 160px;
   }
   .tabordion section article {
   left: 200px;
   min-width: 270px;
   }
   .tabordion section article:after {
   background-color: #ccc;
   bottom: 0;
   content: "";
   display: block;
   left:-199px;
   position: absolute;
   top: 0;
   width: 200px;
   }
   }
   .tabordion section label h6{
   text-transform: initial;
   font-size: 13px;
   }
   .tabordion section label h6 i{
   color: #815dd5;
   }
   @media (max-width: 768px) {
   h1 {
   width: 96%;
   }
   .tabordion {
   width: 100%;
   }
   }
   @media (min-width: 1366px) {
   h1 {
   width: 70%;
   }
   .tabordion {
   width: 100%;
   }
   }
   /* Style the tab */
   .newTab .tab {
   overflow: hidden;
   border: 1px solid #815dd5;
   /*background-color: #f1f1f1;*/
   }
   /* Style the buttons inside the tab */
   .newTab .tab button {
   background-color: inherit;
   float: left;
   border: none;
   outline: none;
   cursor: pointer;
   padding: 14px 16px;
   transition: 0.3s;
   font-size: 17px;
   }
   /* Change background color of buttons on hover */
   .newTab .tab button:hover {
   background-color: #815dd533;
   }
   /* Create an active/current tablink class */
   .newTab .tab button.active {
   background-color: #815dd5;
   color: white;
   }
   /* Style the tab content */
   .newTab .tabcontent1 {
   padding: 6px 12px;
   border: 1px solid #ccc;
   border-top: none;
   max-height: 520px;
   overflow: overlay;
   }
   .textVertical{
   writing-mode: tb-rl;
   transform: rotate(-180deg);
   color: white;
   }
   .verticalDiv{
   padding: 20px;
   /*background-color: #5d82d5;*/
   color: white;
   /* margin-top: 12px; */
   text-align: -webkit-center;
   }
   .verticalDiv .col-md-11{
   padding-left: 0px;
   }
   .requiredDoc{
   background-color: #5d82d5;
   }
   .optDoc{
   background-color: #5dd5c4;
   }
   .docUplodedName{
   background-color: ghostwhite;
   }
   .docUploadNameLink {
   color: #815dd5 !important;
   }
   .docUploadBox{
   box-shadow: 1px 1px 5px 10px #efe7e7;
   padding-left: 0px !important;
   margin-bottom: 2%;
   }
   .docName{
       font-weight: bold;
       text-align: center;
   }
   .docName h5{
   text-transform: capitalize;
   font-family: inherit;
   margin: 10px 0px;
   color: #000000;
   font-weight: inherit;
   letter-spacing: 1.5px;
   }
   .docName p{
   margin: 8px;
   }
   .statusBox{
   padding: 2% 1%;
   text-align: center;
   }
   .fa-times{
   font-size: 30px;
   color: red;
   }
   .fa-check{
   font-size: 30px;
   color: green;
   }
   .comment-box {
   margin-top: 30px !important;
   }
   /* CSS Test end */
   .comment-box img {
   width: 50px;
   height: 50px;
   }
   .comment-box .media-left {
   padding-right: 10px;
   width: 9%;
   }
   .comment-box .media-body p {
   border: 1px solid #ddd;
   padding: 10px;
   }
   .comment-box .media-body .media p {
   margin-bottom: 0;
   }
   .comment-box .media-heading {
   background-color: #815dd5;
   border: 1px solid #815dd5;
   padding: 7px 10px;
   position: relative;
   margin-bottom: -1px;
   color: white;
   }
   .comment-box .media-heading:before {
   content: "";
   width: 12px;
   height: 12px;
   background-color: #f5f5f5;
   border: 1px solid #ddd;
   border-width: 1px 0 0 1px;
   -webkit-transform: rotate(-45deg);
   transform: rotate(-45deg);
   position: absolute;
   top: 10px;
   left: -6px;
   }

   .sep{
       margin-top: 20px;
       margin-bottom: 20px;
       background-color: #e6c3c3;
   }

   .requiredInfoForm label{
          border-right: 3px solid #ffffff !important;
    cursor: pointer !important;
    display: inline-block !important;
   }
</style>
<div class="container-fluid" style="background-color: #f5f5f5c2;">
    <div class="col-sm-12">
        <div id="Applications" class="tabcontent">
            <div>
                <section id="section">
                    <article>
                        <div class="newTab" id="tab">
                            <div id="DocUp" class="col-md-12">
                                <h3>Doc Upload (Only Pdf or Image File)</h3>
                                <?php
                                foreach($documents as $keyd => $valued)
                                {
                                    ?>
                                    <div class="col-md-12 docUploadBox noPaddingCol">
                                        <?php
                                        if($valued['required'] == 1)
                                        {
                                            ?>
                                            <div class="col-md-1 verticalDiv requiredDoc">
                                                <h6 class="textVertical">Required</h6>
                                            </div>
                                            <?php
                                        }
                                        else
                                        {
                                            ?>
                                            <div class="col-md-1 verticalDiv optDoc">
                                                <h6 class="textVertical">Optional</h6>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <div class="col-md-11 noPaddingCol" >
                                            <div class="col-md-12">
                                                <div class="col-md-2 statusBox">
                                                    <h5>
                                                        <?php
                                                        if(isset($valued['document_url']))
                                                        {
                                                            echo " <i class='fas fa-check'></i><br> Completed";
                                                        }
                                                        else
                                                        {
                                                            echo " <i class='fas fa-times'></i><br> Pending";
                                                        }
                                                        ?>
                                                    </h5>
                                                </div>
                                                <div class="col-md-9 noPaddingCol" >
                                                    <div class="col-md-12 noPaddingCol">
                                                        <div class="col-md-8 docName">
                                                            <h5><?=$valued['name']?></h5>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <form method="post" action="" enctype="multipart/form-data" id="myform<?=$valued["id"]?>" style="margin-top:15%">
                                                                <input class="form-control" type="file" name="document<?=$valued["id"]?>" id="document<?=$valued["id"]?>">
                                                                <button class="btn btn-md prplButton" style="margin-top: 5%;" onclick="uploadAppDoc(<?=$valued["id"]?>, <?=$userId?>)"><i class="fas fa-upload">&nbsp;</i>Upload</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 docUplodedName">
                                                <?php
                                                if($valued['document_url'])
                                                {
                                                    ?>
                                                    <h6><a href="<?=$valued['document_url']?>" target="_blank" class="docUploadNameLink"> <?=$valued['name']?> View </a></h6>
                                                    <?php
                                                }
                                                else
                                                {
                                                    ?>
                                                    <h6>No Document</h6>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </article>
                </section>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function uploadAppDoc(docId, studentId){
        var fd = new FormData();
        var files = $('#document' + docId)[0].files;
        if(files.length > 0 ){
            fd.append('document',files[0]);
            fd.append('document_type',docId);
            fd.append('student_id',studentId);
            event.preventDefault();
            $.ajax({
                url: '/student/profile/appdocupdate',
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response){
                    console.log(response);
                    if(response == 'SUCCESS'){
                        alert('file uploaded');
                    }else{
                        alert('file not uploaded');
                    }
                },
            });
        }
        else{
            alert("Please select a file.");
        }
    }
</script>
