     <?php //include 'head.php'; ?>
	 <?php //include 'header_solid.php'; ?>
	 <style>
.grayscale {
    filter: url("data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\'><filter id=\'grayscale\'><feColorMatrix type=\'matrix\' values=\'0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0\'/></filter></svg>#grayscale"); /* Firefox 10+, Firefox on Android */
   -webkit-filter: grayscale(100%);
   -moz-filter: grayscale(100%);
   -ms-filter: grayscale(100%);
   filter: grayscale(100%);
   filter: gray; /* IE 6-9 */
   text-align:center;
}

.grayscale:hover {
   -webkit-filter: none;
   -moz-filter: none;
   -ms-filter: none;
   filter: none;
}
.engg{
	    text-align: center;
    margin-top: -80px;
    /* margin-left: 2px; */
    padding-left: 110px;
}
.business{
	    position: relative;
}
.medicine{

text-align: center;
    margin-top: -80px;


    padding-right: 72px;
}
.humanities{
	position: relative;
}
.architect{
	    margin-top: -30px;
}
.hospitality{
	 margin-top: -30px;
}
.arts{
	 margin-top: -30px;
}
.personal_info_heading{
	    padding-left: 0;
    padding-right: 0;

    text-align: center;
}
.personal_info_heading h3{
	color:#f6881f;
	/*text-shadow:1px 2px #CCC;	*/
}
.formbg{
	background-color:#EFEFF1;
	border-radius:2px;
	/*background-image:url(img/formbg1.png);
	height:286px;
	/*width:320px;*/

}
.container-table {
    height: 100%;
}
.container-table {
    display: table;
}
.vertical-center-row {
    display: table-cell;
    vertical-align: middle;
}
#input_container { position:relative; padding:0; margin:0;     padding-top: 25px;
    padding-left: 10px;
    padding-right: 10px;}
#input { margin:0; padding-left: 62px; }

#input_img { position:absolute; bottom:205px; left:15px; width:50px; height:27px; }

#input2 { margin:0; padding-left: 62px; }

#input_img2 { position:absolute; bottom:157px; left:15px; width:50px; height:27px; }
#input3 { margin:0; padding-left: 62px; }

#input_img3 { position:absolute; bottom:109px; left:15px; width:50px; height:27px; }

#msg{color:red; font-size: 15px;}
 </style>

 <script src="js/jquery.js"></script>
    <script src="jqvmap/jquery.vmap.js" type="text/javascript"></script>
    <script src="jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
	<script src="jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>

	<script type="text/javascript">
	function page4_loader(){
		//alert($('page_loader.php').html())
		$('#page3_loader').load('page4_loader.php');
	}
	</script>

    <script type="text/javascript">
	function validate(f)
	{
		var letter=/^[A-Za-z]+$/;
		var numbers = /^[0-9]+$/;
		var mailexp=/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}(.[a-zA-Z]{2,4})?$/;
		var mobileexp=/^[0-9\-\+]{6,20}$/;
		var correct=true;
		if(f.name.value=="")
		{
				f.name.focus();
				alert("Enter Your Name.");
				return false;
		}
		else if(f.phone.value=="")
		{
			f.phone.focus();
			alert("Enter Your Phone No.");
			return false;
		}
		else if(!f.phone.value.match(mobileexp))
		{
			f.email.focus();
			alert("Invalid Tel No.");
			return false;
		}
		else if(f.email.value=="")
		{
			f.email.focus();
			alert("Enter Your email id.");
			return false;
		}
		else if(!f.email.value.match(mailexp))
		{
			f.email.focus();
			alert("Invalid email address");
			return false;
		}


		else
			return true;




	}

</script>


	<div id="page3_loader"><div class="container hidden-xs hidden-sm hidden-md" style="padding: 17px; " >
            <img src="<?php echo base_url();?>img/page1.png" width="100%"  />

       </div>
        <div class="container visible-xs visible-sm" style="text-align:center;">

           <img src="<?php echo base_url();?>img/page1-1.jpg" />
       </div>

  	 <div class="shade_border"></div>


        <div class="container container-table">

           	<div class="container container-table">
    <div class="row vertical-center-row">
    	<div class="col-md-4 col-md-offset-4 personal_info_heading"><h3>Personal Info</h3></div>
        <div class="text-center col-md-4 col-md-offset-4 formbg" >

        <form action="<?php echo base_url();?>user/lead/create" class="form-group" id="input_container" method="post">

                    	<div class="form-group">
                        	<span id="msg"> <?=$error?></span>
                        </div>
						            <div class="form-group" >
                        	<input type="text" name="name" placeholder="Name" id="name" value="<?=$name?>" class="form-control" style="height:34px;"/>
                            <!--img src="<?php echo base_url();?>img/name.png" id="input_img"-->
                        </div>

                        <div class="form-group">
                        	<input type="text" name="email" placeholder="Email"  id="email" value="<?=$email?>" class="form-control" style="height:34px;"/>
                        </div>

                    	<div class="form-group">
                        	<input type="text" name="phone" placeholder="Mobile Number" id="mobile" value="<?=$phone?>" class="form-control" style="height:34px;"/>
                             <!--img src="<?php echo base_url();?>img/mobile.png" id="input_img2"-->
                        </div>

                        <!--<div class="form-group" >
                        	<input type="text" name="username" placeholder="Username" id="username" value="<?=$username?>" class="form-control" style="height:34px;"/>
                        </div>-->

                        <div class="form-group">
                            <input type="password" name="password" placeholder="Password" id="password" class="form-control" style="height:34px;"/>
                        </div>

                       <div class="form-group">

                               <input type="hidden" name="countries" value="<?php echo $countries; ?>"/>
                               <input type="hidden" name="mainstream" value="<?php echo $mainstream; ?>"/>
                               <input type="hidden" name="coursecategories" value="<?php echo $coursecategories;?>"/>
           							       <input type="hidden" name="degree" value="<?php echo $degree;?>"/>

                               <button type="button" class="back_btn">BACK</button>&nbsp;
                        	     <button type="submit" id="lead_submit" class="go_btn" onClick="">GO</button>
                               
                       </div>

                    </form>
                  </div>
    </div>
</div>





            </div>
            <br/>

         <br/>


         </div>
         </div>
