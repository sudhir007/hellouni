<style type="text/css">
    .shade_border{
        background:url(<?php echo base_url();?>img/shade1.png) repeat-x; height:11px;
    }
    .search_by{
        background-color:#f3f4f4; box-shadow:5px 5px 5px #eae8e8; height:auto;
        padding-top: 10px;
        padding-left: 30px;
        padding-right: 30px;
    }
    .search_by h4{
        color:#3c3c3c;
        font-family:lato_regular;
        font-weight:500;
    }
    .first{
        border-left:1px solid #eaeaeb;
        border-top:1px solid #eaeaeb;
        border-bottom:1px solid #eaeaeb;
        text-align:center;
        background:#fbfbfb;
    }
    .last{
        border-right:1px solid #eaeaeb;
        border-top:1px solid #eaeaeb;
        border-bottom:1px solid #eaeaeb;
        text-align:center;
        background:#fbfbfb;
        padding: 30px 15px !important;
    }
    .element{
        border-top:1px solid #eaeaeb;
        border-bottom:1px solid #eaeaeb;
        text-align:center;
        background:#fbfbfb;
        padding: 30px 15px !important;
    }
    .table_heading{
        font-weight:bold;
        padding: 12px 9px 10px 9px;
        text-align: center;
        border-bottom: 1px solid #eaeaeb;
        border-top:1px solid #eaeaeb;
    }
    .table_heading-first{
        width:169px;
        font-weight:bold;
        border-left:1px solid #eaeaeb;
        border-bottom:1px solid #eaeaeb;
        border-top:1px solid #eaeaeb;
        text-align:center;
        background:#fbfbfb;
        padding: 12px 15px 10px 15px;
    }
    .table_heading-last{
        border-right:1px solid #eaeaeb;
        border-bottom:1px solid #eaeaeb;
        border-top:1px solid #eaeaeb;
        text-align:center;
        background:#fbfbfb;
        padding: 12px 15px 10px 15px;
        width:126px;
    }
    .more_detail{
        background-color:#F6881F;
        text-transform:uppercase;
        height:100%;
        border:0px;
        color:#fff;
        border-radius:2px;
    }
    .menutables {
        border-collapse: separate;
    }
    tr .menutables {
        margin-top:10px;
    }
    .delimg{
        cursor:pointer;
    }
    .bkbtn{
        background-color: red;
        width: 9%;
        height: 35px;
        border-color: red;
    }
    .bkbtnDiv{
        text-align: right;
        height: 50px;
    }
    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 50%;
    }

    /* The Close Button */
    .close {
        color: #8e0f0f;
        float: right;
        font-size: 28px;
        font-weight: bold;
        margin: -20px -10px -10px 0px;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }

    #visa-application{
    	text-align: center;
        font-size: 16px;
        padding: 10px;
    }
</style>

<div class="clearfix"></div>
<div>
    <div class="shade_border"></div>
    <h4 style="text-align:center;">My Applied Universities</h4>
    <div style="position:fixed; right:0px; top:95px;">
        <!--button class="btn btn-default">Apply In Other College</button-->
        <form method="get" action="/university/details/application">
            <div style="text-align:center;">
                <input type="submit" class="btn btn-default" name="submit" value="Apply In Other College">
            </div>
            <input type="hidden" name="uid" value="<?=$university_id?>">
        </form>
    </div>
    <div class="shade_border"></div>
    <div class="container">
        <br class="visible-xs visible-sm"/>
        <div class="col-md-12 col-sm-12">
            <div class="table-responsive">
                <table class="table menutables" width="100%" cellspacing="10">
                    <thead>
                        <tr>
                            <td style="border:1px solid #eaeaeb; text-align:center; padding: 10px 15px 10px 15px;" valign="middle" width="48">
                                <input type="checkbox" name="Check_ctr" value="yes" onClick="Check(this)">
                            </td>
                            <td class="table_heading-first">University</td>
                            <td class="table_heading">University Name</td>
                            <td class="table_heading">College Name</td>
                            <td class="table_heading">Applied Date</td>
                            <td class="table_heading">Status</td>
                            <td class="table_heading">Action</td>
                            <td class="table_heading-last"></td>
                        </tr>
                    </thead>
                    <tr><td colspan="8">&nbsp;</td></tr>
                    <?php
                    if($applied_colleges)
                    {
                        foreach($applied_colleges as $appliedCollege)
                        {
                            ?>
                            <tr>
                                <td class="table_heading" style="border:1px solid #eaeaeb; text-align:center; padding:40px 15px;" valign="middle" width="48">
                                    <input type="checkbox" name="check_list" value="<?php echo $appliedCollege['id'];?>">
                                </td>
                                <td class="table_heading first" valign="middle">
                                    <img class="lazy" data-original="<?=$appliedCollege['banner']?>" alt="<?=$appliedCollege['name']?>" title="<?=$appliedCollege['name']?>" align="center" width="115" height="100" src="<?=$appliedCollege['banner']?>" style="display: inline;">
                                </td>
                                <td class="table_heading element" width="129"><?php echo $appliedCollege['name'];?></td>
                                <td class="table_heading element" width="88"><?php echo $appliedCollege['college_name'];?></td>
                                <td class="table_heading element" width="137"><?php echo $appliedCollege['date_created'];?></td>
                                <td class="table_heading element" width="131"><?php echo $appliedCollege['status'];?></td>
                                <td class="table_heading element" width="131"><?php echo $appliedCollege['action'];?></td>
                                <td class="table_heading last"><?php echo $appliedCollege['view_application'];?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    <tr><td colspan="8">&nbsp;</td></tr>
                </table>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>
        <div id="visa-application"></div>
    </div>
</div>

<script type="text/javascript">
    var modal = document.getElementById("myModal");
    var span = document.getElementsByClassName("close")[0];

    function showModal(collegeId, universityId, status) {
        var html = 'You can start VISA process only for one application. If you want to change it further, please contact to our customer support. Do you want to continue with this application?<form method="get" action="/user/account/update"><div><input type="submit" class="btn btn-default" name="submit" value="Yes"></div><input type="hidden" name="aid" value="' + collegeId + '"><input type="hidden" name="uid" value="' + universityId + '"><input type="hidden" name="status" value="' + status + '"><input type="hidden" name="tab" value="visa_form"></form>';
        document.getElementById("visa-application").innerHTML = html;
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
