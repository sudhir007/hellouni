<style>
    .shade_border{
        background:url(img/shade1.png) repeat-x; height:11px;
    }
    .search_by{
        background-color:#f3f4f4; box-shadow:5px 5px 5px #eae8e8; height:auto;
        padding-top: 10px;
        padding-left: 30px;
        padding-right: 30px;
    }
    .search_by h4{
        color:#3c3c3c;
        font-family:lato_regular;
        font-weight:500;
    }
    .table_heading{
        padding:5px;
        font-weight:bold;
    }
    .more_detail{
        background-color:#F6881F;
        text-transform:uppercase;
        height:28px;
        border:0px;
        color:#000;
        border-radius:2px;
        width:25px;
    }
    .right_side_form{
        background:#3c3c3c;
        box-shadow:5px 5px 2px #CCCCCC;
        height:auto;
    }
    label{
        color:#fff;
    }
    .more_detail{
        background-color:#F6881F;
        text-transform:uppercase;
        height:28px;
        border:0px;
        color:#fff;
        border-radius:2px;
    }
    label{
        font-size:11px;
        font-weight:300;
    }
    #input_container {
        position:relative;
        padding:0;
        margin:0 auto;
        padding-top: 25px;
        width: 240px;
    }
    .input {
        margin:0;
        padding-left: 62px;
    }
    #input_img {
        position:absolute;
        bottom:48px;
        left:15px; width:50px;
        height:27px;
        margin-left:-5px;
    }
    #input2 {
        margin:0;
        padding-left: 62px;
    }
    #input_img2 {
        position:absolute;
        bottom:100px;
        left:15px;
        width:50px;
        height:27px;
    }
    #input3 {
        margin:0;
        padding-left: 62px;
    }
    #input_img3 {
        position:absolute;
        bottom:163px;
        left:15px;
        width:50px;
        height:27px;
    }
    .login_background{
        background-color:#f3f4f6;
        height:auto;
        text-align: center;
    }
    .form_font_color{
        color:#818182;
    }
    .invalid_color{
        color:red;
    }
    .form_font_color a{
        color:#818182;
    }
    .button_sign_in{
        background-color:#F6881F; border:none;  width: 100px;height: 28px;
    }
    .sign_line{
        background:url(img/line2.png) repeat-y; height:100px;
    }
    .login_head{
        color:#F6881F;padding-left:0px;
    }
    .login_head2{
        text-transform:capitalize; color:#3C3C3C;"
    }
    ##errormsg{
        color:red;
    }
    .header_icons ul li{
        display:inline-block;
        list-style-type:none;
        margin-bottom:-10px;
    }
</style>

</head>
<body>
    <div id="page_loader">
        <div class="shade_border"></div>
        <div class="container login_background" >
            <div class="col-lg-12 col-md-12">
                <?php
                if($this->session->flashdata('flash_message')) {
                    ?>
                    <h4 class="invalid_color"><?php echo $this->session->flashdata('flash_message');?></h4>
                    <?php
                }
                else{
                    ?>
                    <h4 class="form_font_color">Forgot Password</h4>
                    <?php
                }?>
            </div>

            <div class="col-lg-12 col-md-12">
                <form action="<?php echo base_url();?>user/account/forgotPassword" class="form-group" id="input_container" method="post">
                    <div class="form-group " >
                        <div id="errormsg"></div>
                        <input type="text" name="uname_email" placeholder="Username Or Email" id="username" class="form-control form_font_color input" style="height:34px; width:240px" required/>
                        <img src="<?php echo base_url();?>img/email.png" id="input_img" class="hidden-xs">
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <button type="submit" class="button_sign_in" id="signin">Submit</button>&nbsp;
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    </div>
    <br/>
