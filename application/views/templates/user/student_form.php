<style type="text/css">
    @import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");
    .login-block{
        background: #004b7a;  /* fallback for old browsers */
        background: -webkit-linear-gradient(to bottom, #fafafb, #d8d9da);  /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to bottom, #fafafb, #d8d9da); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
        float:left;
        width:100%;
        padding : 50px 0;
    }
    .banner-sec{
        background:url(<?php echo base_url();?>application/images/register.png)  no-repeat left bottom;
        background-size:cover;
        min-height:600px;
        border-radius: 0 10px 10px 0;
        padding:0;
        background-position: center;
        margin-top: 25px;
    }

    .login-sec{
        padding: 50px 30px;
        position:relative;
    }
    .login-sec .copy-text{
        position:absolute;
        width:80%;
        bottom:20px;
        font-size:13px;
        text-align:center;
    }
    .login-sec h2{
        margin-bottom:30px;
        font-weight:800;
        font-size:30px;
        color: #004b7a;
    }
    .login-sec h2:after{
        content:" ";
        width:100px;
        height:5px;
        background:#f9992f;
        display:block;
        margin-top:20px;
        border-radius:3px;
        margin-left:auto;
        margin-right:auto
    }
    .btn-login{
        background: #f9992f;
        color:#fff;
        font-weight:600;
    }
    .form-control{
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    }

    p {
        font-size: 16px;
        line-height: 1.6;
        margin:0 0 10px;
    }
    .italic {
        font-style:italic;
    }
    .padtb {
        padding:30px 0px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
        <!-- Main content -->
        <section class="login-block">

        <div class="container" style="background:#fff; border-radius: 10px; box-shadow: 5px 5px 0px rgba(0,0,0,0.1);">
          <div class="row">

            <!-- right column -->
            <div class="col-md-10">
              <!-- Horizontal Form -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Slot Management</h3>
                </div><!-- /.box-header -->
                <!-- form start -->

                <!--div style="font-size: 16px;font-weight: bold;text-align: center;margin-bottom: 15px;">Your provided timezone is GMT<?=$timezone?>. By default all the slots will be created in this timezone OR you can select your preferred timezone.</div-->
				 <?php if($slot_id){?>
				<form class="form-horizontal" action="<?php echo base_url();?>user/session/edit/<?php if(isset($id)) echo $id;?>" method="post" name="addadmin" autocomplete="off">
				<?php }else{?>
                <form class="form-horizontal" action="<?php echo base_url();?>user/session/create" method="post" name="adduni" autocomplete="off">
                <?php } ?>



                  <div class="box-body">

                      <div class="form-group">
                        <label for="timezone" class="col-sm-2 control-label">Timezone</label>
                        <div class="col-sm-2">
                            <select class="form-control" name="timezone">
                                <?php
                                for($i = 12; $i > 0; $i-=.5)
                                {
                                    ?>
                                    <option value="-<?=$i?>" <?php if(isset($timezone) && $timezone=="-".$i){ echo 'selected="selected"'; } ?>>GMT-<?=$i?></option>
                                    <?php
                                }
                                ?>
                                <option value="0" <?php if(isset($timezone) && $timezone==0){ echo 'selected="selected"'; } ?>>GMT+0</option>
                                <?php
                                for($i = .5; $i <= 12; $i+=.5)
                                {
                                    ?>
                                    <option value="+<?=$i?>" <?php if(isset($timezone) && $timezone=="+".$i){ echo 'selected="selected"'; } ?>>GMT+<?=$i?></option>
                                    <?php
                                }
                                 ?>
                            </select>
                        </div>
                      </div>

                      <div class="form-group">
                          <input type="hidden" name="slot_id" value="<?=$slot_id?>">
                        <label for="slot_1_date" class="col-sm-2 control-label">Slot 1 Date</label>
                        <div class="col-sm-2">
                          <input type="text" class="mws-datepicker small" id="datepicker" name="slot_1_date" value="<?php if(isset($slot_1_date)) echo $slot_1_date; ?>">
                        </div>
                        <label for="slot_1_time" class="col-sm-2 control-label">Slot 1 Time</label>
                        <div class="col-sm-4">
                            <select class="form-control small" name="slot_1_time">
                            <?php
                            foreach($time_array as $index => $time)
                            {
                                ?>
                                <option value="<?=$index?>" <?php if(isset($slot_1_time) && $slot_1_time==$time){ echo 'selected="selected"'; } ?>><?=$time?></option>
                                <?php
                            }
                            ?>
                        </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="slot_2_date" class="col-sm-2 control-label">Slot 2 Date</label>
                        <div class="col-sm-2">
                          <input type="text" class="mws-datepicker small" id="datepicker_1" name="slot_2_date" value="<?php if(isset($slot_2_date)) echo $slot_2_date; ?>">
                        </div>
                        <label for="slot_2_time" class="col-sm-2 control-label">Slot 2 Time</label>
                        <div class="col-sm-4">
                            <select class="form-control small" name="slot_2_time">
                            <?php
                            foreach($time_array as $index => $time)
                            {
                                ?>
                                <option value="<?=$index?>" <?php if(isset($slot_2_time) && $slot_2_time==$time){ echo 'selected="selected"'; } ?>><?=$time?></option>
                                <?php
                            }
                            ?>
                        </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="slot_3_date" class="col-sm-2 control-label">Slot 3 Date</label>
                        <div class="col-sm-2">
                          <input type="text" class="mws-datepicker small" id="datepicker_2" name="slot_3_date" value="<?php if(isset($slot_3_date)) echo $slot_3_date; ?>">
                        </div>
                        <label for="slot_3_time" class="col-sm-2 control-label">Slot 3 Time</label>
                        <div class="col-sm-4">
                            <select class="form-control small" name="slot_3_time">
                            <?php
                            foreach($time_array as $index => $time)
                            {
                                ?>
                                <option value="<?=$index?>" <?php if(isset($slot_3_time) && $slot_3_time==$time){ echo 'selected="selected"'; } ?>><?=$time?></option>
                                <?php
                            }
                            ?>
                        </select>
                        </div>
                      </div>






                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="button" class="btn btn-default" onclick="window.history.back();">Back</button>
					<button type="button" class="btn btn-warning" id="reset" onclick="window.location.reload();">Reset</button>
                    <!--input type="hidden" name="timezone" value="<?=$timezone?>"-->
                    <button type="submit" class="btn btn-info pull-right">Create</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
              <!-- general form elements disabled -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </div><!-- /.content -->
      </section>
