<style>
    .shade_border{
        background:url(img/shade1.png) repeat-x; height:11px;
    }
    #input_container{
        position:relative;
        padding:0;
        margin:0;
        padding-top: 25px;
    }
    .input{
        margin:0;
        padding-left: 62px;
    }
    .login_background{
        background-color:#f3f4f6; height:auto;
    }
    .form_font_color{
        color:#818182;
    }
    .invalid_color{
        color:red;
    }
    .form_font_color a{
        color:#818182;
    }
    .button_sign_in{
        background-color:#F6881F; border:none;  width: 100px;height: 28px;
    }
    .login_head{
        color:#F6881F;padding-left:0px;
    }
    ##errormsg{
        color:red;
    }
</style>

</head>

<body>
    <div id="page_loader">
        <div class="shade_border"></div>
        <div class="container login_background" style="text-align:center;">
            <div class="col-lg-12 col-md-12">
                <?php
                if($invited)
                {
                    ?>
                    <h4 class="form_font_color">Thank You For Inviting Your Friends!!</h4>
                    <?php
                }
                else
                {
                    ?>
                    <h4 class="form_font_color">Invite Friends!!</h4>
                    <?php
                }
                ?>
            </div>
            <?php
            if(!$invited)
            {
                ?>
                <div class="col-lg-12 col-md-12">
                    <div class="form-group"><h4 class="login_head">Enter Email Address (Comma Seperated)</h4></div>
                    <form action="<?php echo base_url();?>user/invite/send" class="form-group" id="input_container" method="post">
                        <div class="form-group">
                            <div id="errormsg"></div>
                            <textarea name="friends_email" placeholder="abc@gmail.com, xyz@yahoo.com" id="friends_email" rows="4" cols="100" style="font-size:16px;" required></textarea>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <input type="hidden" name="euname" value="<?=$encryptedUsername?>">
                            <button type="submit" class="button_sign_in" id="signin">INVITE</button>&nbsp;
                        </div>
                    </form>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
