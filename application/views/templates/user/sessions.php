<style type="text/css">
    .shade_border{
        background:url(<?php echo base_url();?>img/shade1.png) repeat-x; height:11px;
    }
    .search_by{
        background-color:#f3f4f4; box-shadow:5px 5px 5px #eae8e8; height:auto;
        padding-top: 10px;
        padding-left: 30px;
        padding-right: 30px;
    }
    .search_by h4{
        color:#3c3c3c;
        font-family:lato_regular;
        font-weight:500;
    }
    .first{
        border-left:1px solid #eaeaeb;
        border-top:1px solid #eaeaeb;
        border-bottom:1px solid #eaeaeb;
        text-align:center;
        background:#fbfbfb;
    }
    .last{
        border-right:1px solid #eaeaeb;
        border-top:1px solid #eaeaeb;
        border-bottom:1px solid #eaeaeb;
        text-align:center;
        background:#fbfbfb;
        padding: 30px 15px !important;
    }
    .element{
        border-top:1px solid #eaeaeb;
        border-bottom:1px solid #eaeaeb;
        text-align:center;
        background:#fbfbfb;
        padding: 30px 15px !important;
    }
    .table_heading{
        font-weight:bold;
        padding: 12px 9px 10px 9px;
        text-align: center;
        border-bottom: 1px solid #eaeaeb;
        border-top:1px solid #eaeaeb;
    }
    .table_heading-first{
        width:169px;
        font-weight:bold;
        border-left:1px solid #eaeaeb;
        border-bottom:1px solid #eaeaeb;
        border-top:1px solid #eaeaeb;
        text-align:center;
        background:#fbfbfb;
        padding: 12px 15px 10px 15px;
    }
    .table_heading-last{
        border-right:1px solid #eaeaeb;
        border-bottom:1px solid #eaeaeb;
        border-top:1px solid #eaeaeb;
        text-align:center;
        background:#fbfbfb;
        padding: 12px 15px 10px 15px;
        width:126px;
    }
    .more_detail{
        background-color:#F6881F;
        text-transform:uppercase;
        height:100%;
        border:0px;
        color:#fff;
        border-radius:2px;
    }
    .menutables {
        border-collapse: separate;
    }
    tr .menutables {
        margin-top:10px;
    }
    .delimg{
        cursor:pointer;
    }
    .bkbtn{
        background-color: red;
        width: 9%;
        height: 35px;
        border-color: red;
    }
    .bkbtnDiv{
        text-align: right;
        height: 50px;
    }
</style>

<div class="clearfix"></div>
<?php
if($my_applied_universities)
{
    ?>
    <div>
        <div class="shade_border"></div>
        <h4 style="text-align:center;">My Applied Universities</h4>
        <div class="shade_border"></div>
        <div class="container">
            <br class="visible-xs visible-sm"/>
            <div class="col-md-12 col-sm-12">
                <div class="table-responsive">
                    <table class="table menutables" width="100%" cellspacing="10">
                        <thead>
                            <tr>
                                <td style="border:1px solid #eaeaeb; text-align:center; padding: 10px 15px 10px 15px;" valign="middle" width="48">
                                    <input type="checkbox" name="Check_ctr" value="yes" onClick="Check(this)">
                                </td>
                                <td class="table_heading-first">University</td>
                                <td class="table_heading">University Name</td>
                                <td class="table_heading">Applied Date</td>
                                <td class="table_heading">Status</td>
                                <td class="table_heading">Action</td>
                                <td class="table_heading-last"></td>
                            </tr>
                        </thead>
                        <tr><td colspan="8">&nbsp;</td></tr>
                        <?php
                        if($my_applied_universities)
                        {
                            foreach($my_applied_universities as $appliedUniversity)
                            {
                                ?>
                                <tr>
                                    <td class="table_heading" style="border:1px solid #eaeaeb; text-align:center; padding:40px 15px;" valign="middle" width="48">
                                        <input type="checkbox" name="check_list" value="<?php echo $appliedUniversity['id'];?>">
                                    </td>
                                    <td class="table_heading first" valign="middle">
                                        <img class="lazy" data-original="<?=$appliedUniversity['banner']?>" alt="<?=$appliedUniversity['name']?>" title="<?=$appliedUniversity['name']?>" align="center" width="115" height="100" src="<?=$appliedUniversity['banner']?>" style="display: inline;">
                                    </td>
                                    <td class="table_heading element" width="129"><?php echo $appliedUniversity['name'];?></td>
                                    <td class="table_heading element" width="137"><?php echo $appliedUniversity['date_created'];?></td>
                                    <td class="table_heading element" width="131"><?php echo $appliedUniversity['status'];?></td>
                                    <td class="table_heading element"><?php echo $appliedUniversity['action'];?></td>
                                    <td class="table_heading last"><?php echo $appliedUniversity['view_application'];?></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        <tr><td colspan="8">&nbsp;</td></tr>
                    </table>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>
<div>
    <div class="shade_border"></div>
    <h4 style="text-align:center;">My Sessions</h4>
    <div class="shade_border"></div>
    <div class="container">
        <br class="visible-xs visible-sm "/>
        <div class="col-md-12 col-sm-12">
            <div class="table-responsive">
                <form name="myform" action="checkboxes.asp" method="post">
                    <table class="table menutables" width="100%" cellspacing="10">
                        <thead>
                            <tr>
                                <td style="border:1px solid #eaeaeb; text-align:center; padding: 10px 15px 10px 15px;" valign="middle" width="48">
                                    <input type="checkbox" name="Check_ctr" value="yes" onClick="Check(this)">
                                </td>
                                <td class="table_heading-first">University</td>
                                <td class="table_heading">University Name</td>
                                <td class="table_heading">Date</td>
                                <td class="table_heading">Time</td>
                                <td class="table_heading">Status</td>
                                <td class="table_heading-last">Action</td>
                            </tr>
                        </thead>
                        <tr><td colspan="8">&nbsp;</td></tr>
                        <?php
                        if($my_sessions)
                        {
                            foreach($my_sessions as $session)
                            {
                                ?>
                                <tr>
                                    <td class="table_heading" style="border:1px solid #eaeaeb; text-align:center; padding:40px 15px; "valign="middle" width="48">
                                        <input type="checkbox" name="check_list" value="<?php echo $session['id'];?>">
                                    </td>
                                    <td class="table_heading first" valign="middle">
                                        <img class="lazy" data-original="<?=$session['banner']?>" alt="<?=$session['name']?>" title="<?=$session['name']?>" align="center" width="115" height="100" src="<?=$session['banner']?>" style="display: inline;">
                                    </td>
                                    <td class="table_heading element" width="129"><?php echo $session['name'];?></td>
                                    <td class="table_heading element" width="88"><?=$session['confirmed_slot'] ? date('Y-m-d', strtotime($session['confirmed_slot'])) : ''?></td>
                                    <td class="table_heading element" width="137"><?=$session['confirmed_slot'] ? date('h:i', strtotime($session['confirmed_slot'])) : ''?></td>
                                    <td  class="table_heading element" width="131"><?=$session['status']?></td>
                                    <td class="table_heading last"><?=$session['action']?></td>
                                </tr>
                                <?php
                            }
                        }
                        else
                        {
                            ?>
                            <tr>
                                <td class="table_heading" style="border:1px solid #eaeaeb; text-align:center; padding:40px 15px; "valign="middle" width="48" colspan="8">There is no university in the favourites listing</td>
                            </tr>
                            <?php
                        }
                        ?>
                        <tr><td colspan="8">&nbsp;</td></tr>
                    </table>
                </form>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function deletefav(id){
        if(confirm("Do you really want to delete this favourite")==true){
            window.location.href='<?php echo base_url();?>user/favourites/delete/'+id;
        }
    }
</script>
