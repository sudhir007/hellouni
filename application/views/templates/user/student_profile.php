<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.7/js/dataTables.fixedHeader.min.js"></script>
<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<style type="text/css">
    .mytabs{
            margin: 2% 0px;
    }
   .mytabs .nav-tabs {
   background-color: #815dd5;
   }
   .mytabs .nav-tabs>li>a : hover {
   color: black;
   }
   .prfileBox3{
    background-color: #815dd5;
    margin: 2% 0px;
    padding: 1%;
    border-radius: 7px;
   }
   .margin2{
    margin: 2px 0 2px !important;
   }
   .ptag{
    margin: 0px;
    font-size: 13px;
    line-height: 1.8;
    letter-spacing: 1px;
   }
   .h3Heading{
    background-color: #f6882c;
    padding: 8px 2px;
    text-align: center;
    color: white;
   }

   .boxAppList{
    box-shadow: 0px 0px 12px #d8d1d1;
    border-radius: 7px;
   }

   .listtableMain thead tr {
    color: #815dd5;
    font-size: 13px;
    /* font-weight: 600; */
    letter-spacing: 0.8px;
   }

   table.dataTable.stripe tbody tr.odd, table.dataTable.display tbody tr.odd{
        background-color: #ffffff !important;
   }

   table.dataTable.display tbody tr.odd>.sorting_1, table.dataTable.order-column.stripe tbody tr.odd>.sorting_1{
        background-color: #ffffff !important;
   }

   table.dataTable.display tbody tr.even>.sorting_1, table.dataTable.order-column.stripe tbody tr.even>.sorting_1{
        background-color: #ffffff !important;
   }

   .form-control {
    height: 30px !important;
   }

   .prplButton{
    color: #fff;
    background: #8703c5;
    border-color: #8703c5;
    border-radius: 12px;
   }

   .resetButton{
        color: #8c57c6;
    background: #f1f1f1;
    border-color: #8703c5;
    border-radius: 12px;
   }

   radio-item {
  display: inline-block;
  position: relative;
  padding: 0 6px;
  /*margin: 10px 0 0;*/
}

.radio-item input[type='radio'] {
  display: none;
}

.radio-item label {
  color: #666;
  font-weight: normal;
}

.radio-item label:before {
      content: " ";
    display: inline-block;
    position: relative;
    top: 4px;
    margin: 0 5px 0 0;
    width: 15px;
    height: 15px;
    border-radius: 11px;
    border: 2px solid #f6882c;
    background-color: transparent;
}

.radio-item input[type=radio]:checked + label:after {
  border-radius: 11px;
    width: 7px;
    height: 7px;
    position: absolute;
    top: 8px;
    left: 10px;
    content: " ";
    display: block;
    background: #f6882c;
}

.tabName{
    letter-spacing: 0.6px;
    font-size: 14px;
}
#exam_stage, #exam_date, #result_date, #exam_score, #english_exam_stage, #english_exam_date, #english_result_date, #english_exam_score, #exam_score_gre, #exam_score_gmat, #exam_score_sat, #diploma_id, #masters_id{
    display: none;
}

.checkBoxDesign{
    box-shadow: 0 0 black !important;
    /*margin-top: -4px !important;*/
}

.sep{
    margin-top: 20px;
    margin-bottom: 20px;
    background-color: #e6c3c3;
}

table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}


</style>
<div class="container">
   <div class="col-sm-3 prfileBox3">
      <!--left col-->
      <div class="text-center">
         <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar img-circle img-thumbnail" alt="avatar" style="width: 40%;">
         <h4 style="color: white"><?=$name?></h4>
      </div>
      </hr><br>
      <div class="panel panel-default">
         <div class="panel-heading"><h5 class="margin2">Basic Detailss &nbsp;<i class="far fa-address-card"></i></h5></div>
         <div class="panel-body"><p class="ptag"><b class="purple">Email id : </b><?=$email?><br><b class="purple">Ph. No. : </b><?=$phone?> </p></div>
      </div>
      <ul class="list-group">
         <li class="list-group-item text-muted"><h5 class="margin2">Activity &nbsp;<i class="fa fa-dashboard fa-1x"></i></h5></li>
         <!--li class="list-group-item text-right"><span class="pull-left"><strong class="purple">Basic Profile</strong></span> 75%</li-->
         <li class="list-group-item text-right"><span class="pull-left"><strong class="purple">Assigned To</strong></span> Banit Sawhney</li>
         <li class="list-group-item text-right"><span class="pull-left"><strong class="purple">Intake</strong></span> <?=$intake?></li>
         <li class="list-group-item text-right"><span class="pull-left"><strong class="purple">City</strong></span> Mumbai</li>
         <li class="list-group-item text-right">
             <span class="pull-left"><strong class="purple">Comment History</strong></span>
             <a href="/counsellor/student/offerlist/view/<?=$id?>" target="_blank" style="color: #f6882c"> <b>Apply University</b> </a>
         </li>
      </ul>
   </div>
   <div class="col-sm-9 mytabs">
        <div class="col-md-12 boxAppList" style="margin-bottom: 3%;">
            <h3 class="h3Heading">Application List</h3>
            <div class="col-md-12" style="overflow: scroll;margin-bottom: 1%;">
                <table id="example" class="display listtableMain" style="width:100%">
                    <thead>
                        <tr>
                            <th>Application ID</th>
                            <th>Univrsity Name</th>
                            <th>College</th>
                            <th>Course</th>
                            <th>Applied Date</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if($myApplications)
                        {
                            foreach($myApplications as $myApplication)
                            {
                                ?>
                                <tr>
                                    <td>
                                        <?php
                                        if($logged_in_type == 5)
                                        {
                                            ?>
                                            <a href="/user/account/update?aid=<?=$myApplication['id']?>" style="color:#000"><?=$myApplication['id']?></a>
                                            <?php
                                        }
                                        else
                                        {
                                            ?>
                                            <a href="/student/profile/<?=$id?>?aid=<?=$myApplication['id']?>" style="color:#000"><?=$myApplication['id']?></a>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td><?=$myApplication['name']?></td>
                                    <td><?=$myApplication['college_name']?></td>
                                    <td><?=$myApplication['course_name']?></td>
                                    <td><?=$myApplication['date_created']?></td>
                                    <td><?=$myApplication['status']?></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Application ID</th>
                            <th>Univrsity Name</th>
                            <th>College</th>
                            <th>Course</th>
                            <th>Applied Date</th>
                            <th>Status</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div class="col-md-12 boxAppList" style="padding: 2% 2%;">
          <ul class="nav nav-tabs" style="padding-top: 9px;padding-left: 9px;">
             <li class="active"><a data-toggle="tab" href="#home"><b class="tabName">Personal Detail</b></a></li>
             <li><a data-toggle="tab" href="#qualification_detail"><b class="tabName">Qualification Detail</b></a></li>
             <li><a data-toggle="tab" href="#desired_courses"><b class="tabName">Desired Courses</b></a></li>
             <li><a data-toggle="tab" href="#parent_detail"><b class="tabName">Parent Detail</b></a></li>
             <?php
             if(isset($university_id))
             {
                 ?>
                 <li><a data-toggle="tab" href="#application_form"><b class="tabName">Application</b></a></li>
                 <li><a data-toggle="tab" href="#visa_form"><b class="tabName">VISA</b></a></li>
                 <?php
             }
             ?>
             <li><a data-toggle="tab" href="#document_upload"><b class="tabName">Upload Document</b></a></li>
             <li><a data-toggle="tab" href="#password_change"><b class="tabName">Change Password</b></a></li>
          </ul>
          <div class="tab-content">
             <div class="tab-pane active" id="home">
                <hr>
                <form class="form" action="/user/account/save" method="post" id="personal-detail-form">
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="first_name">
                            <h5>Name</h5>
                         </label>
                         <input type="text" class="form-control" name="name" id="student_name" placeholder="Name" value="<?=$name?>">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="phone">
                            <h5>Mobile</h5>
                         </label>
                         <input type="text" class="form-control" name="phone" id="student_phone" placeholder="Mobile" value="<?=$phone?>">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="email">
                            <h5>Email</h5>
                         </label>
                         <input type="email" class="form-control" name="email" id="student_email" placeholder="Email" value="<?=$email?>">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="dob">
                            <h5>DOB</h5>
                         </label>
                         <input type="date" name="dob"  class="form-control" id="student_dob" value="<?=$dob?>"/>
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="address">
                            <h5>Address</h5>
                         </label>
                         <input type="text" class="form-control" name="address" id="student_address" placeholder="Address" value="<?=$address?>">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="country">
                            <h5>Country</h5>
                         </label>
                         <select name="locationcountries" id="student_locationcountries" class="form-control">
                             <option value="0">Select Country</option>
                             <?php
                             foreach($locationcountries as $location)
                             {
                                 ?>
                                 <option value="<?=$location->id?>" <?php if($location->id==$selected_country) echo 'selected="selected"';?>><?=$location->name?></option>
                                 <?php
                            }
                            ?>
                        </select>
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="state">
                            <h5>State</h5>
                         </label>
                         <select name="state" id="student_state" class="form-control">
                             <option value="0">Select State</option>
                             <?php
                             foreach($related_state_list as $location)
                             {
                                 ?>
                                 <option value="<?=$location->zone_id?>" <?php if($location->zone_id==$selected_state) echo 'selected="selected"';?>><?=$location->name?></option>
                                 <?php
                            }
                            ?>
                        </select>
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="city">
                            <h5>City</h5>
                         </label>
                         <select name="city" id="student_city" class="form-control">
                             <option value="0">Select City</option>
                             <?php
                             foreach($related_city_list as $location)
                             {
                                 ?>
                                 <option value="<?=$location->id?>" <?php if($location->id==$city) echo 'selected="selected"';?>><?=$location->city_name?></option>
                                 <?php
                            }
                            ?>
                        </select>
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="zipcode">
                            <h5>Zipcode</h5>
                         </label>
                         <input type="text" class="form-control" name="zip" id="student_zip" placeholder="Zipcode" value="<?=$zip?>">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="hear_about_us">
                            <h5>Where Did You Hear About Us?</h5>
                         </label>
                         <select name="hear_info" id="student_hear_info" class="form-control">
                             <option value="0">Select Option</option>
                             <option value="Friends / Family" <?php if(isset($hear_about_us) && $hear_about_us == 'Friends / Family') echo 'selected="selected"';?> >Friends / Family</option>
                             <option value="Google / Facebook" <?php if(isset($hear_about_us) && $hear_about_us == 'Google / Facebook') echo 'selected="selected"';?>>Google / Facebook</option>
                             <option value="Stupidsid Seminar" <?php if(isset($hear_about_us) && $hear_about_us == 'Stupidsid Seminar') echo 'selected="selected"';?>>Stupidsid Seminar</option>
                             <option value="Stupidsid Website" <?php if(isset($hear_about_us) && $hear_about_us == 'Stupidsid Website') echo 'selected="selected"';?>>Stupidsid Website</option>
                             <option value="College Seminar" <?php if(isset($hear_about_us) && $hear_about_us == 'College Seminar') echo 'selected="selected"';?>>College Seminar</option>
                             <option value="News Paper" <?php if(isset($hear_about_us) && $hear_about_us == 'News Paper') echo 'selected="selected"';?>>News Paper</option>
                             <option value="Calling from Imperial" <?php if(isset($hear_about_us) && $hear_about_us == 'Calling from Imperial') echo 'selected="selected"';?>>Calling from Imperial</option>
                        </select>
                      </div>
                   </div>
                    <div class="form-group">
                        <div class="col-xs-6">
                           <label for="gender">
                             <h5>Gender</h5>
                           </label>
                          <div class="col-md-12">
                             <label class="radio-inline">
                                 <input type="radio" name="gender" <?=$gender == 'M' ? 'checked' : ''?> value="M">Male
                             </label>
                             <label class="radio-inline">
                                 <input type="radio" name="gender" <?=$gender == 'F' ? 'checked' : ''?> value="F">Female
                             </label>
                             <label class="radio-inline">
                                 <input type="radio" name="gender" <?=$gender == 'O' ? 'checked' : ''?> value="O">Other
                             </label>
                           </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-6">
                           <label for="passport_availalibility">
                             <h5>Do you have passport?</h5>
                           </label>
                          <div class="col-md-12">
                           <label class="radio-inline">
                               <input type="radio" name="passport_availalibility" <?=$passport == 1 ? 'checked' : ''?> value="1">Yes
                           </label>
                           <label class="radio-inline">
                               <input type="radio" name="passport_availalibility" <?=$passport == 0 ? 'checked' : ''?> value="0">No
                           </label>
                          </div>
                        </div>
                    </div>
                   <div class="form-group">
                      <div class="col-xs-12">
                         <br>
                         <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                         <!--button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button-->
                      </div>
                   </div>
                   <input type="hidden" name="form_type" value="personal_details" />
                   <input type="hidden" name="user_id" value="<?=$id?>" />
                </form>
                <hr>
             </div>
             <!--/tab-pane-->
             <div class="tab-pane" id="qualification_detail">
                <hr>
                <form class="form" action="/user/account/save" method="post" id="qualification-detail-form">
                   <div class="form-group" id="competitive_exam">
                      <div class="col-xs-12">
                         <label for="competitive_exam">
                            <h5>Have you appeared any of the following exam?</h5>
                         </label>
                        <div class="col-md-12">
                         <label class="radio-inline">
                             <input type="radio" name="competitive_exam" <?php if($qualified_exams && $qualified_exams->competitive_exam_type == 'GRE') echo 'checked="checked"';?> id="gre" value="GRE">GRE
                         </label>
                         <label class="radio-inline">
                             <input type="radio" name="competitive_exam" <?php if($qualified_exams && $qualified_exams->competitive_exam_type == 'GMAT') echo 'checked="checked"';?> id="gmat" value="GMAT">GMAT
                         </label>
                         <label class="radio-inline">
                             <input type="radio" name="competitive_exam" <?php if($qualified_exams && $qualified_exams->competitive_exam_type == 'SAT') echo 'checked="checked"';?> id="sat" value="SAT">SAT
                         </label>
                         <label class="radio-inline">
                             <input type="radio" name="competitive_exam" <?php if($qualified_exams && $qualified_exams->competitive_exam_type == 'NO') echo 'checked="checked"';?> id="no_competitive_exam" value="NO">NO
                         </label>
                         <label class="radio-inline">
                             <input type="radio" name="competitive_exam" <?php if($qualified_exams && $qualified_exams->competitive_exam_type == 'NOT_REQUIRED') echo 'checked="checked"';?> id="not_required_competitive_exam" value="NOT_REQUIRED">NOT_REQUIRED
                         </label>
                        </div>
                      </div>
                      <span class="sep col-lg-12"></span>
                   </div>

                   <div class="form-group" id="exam_stage" <?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name']) echo 'style=display:block;'?>>
                      <div class="col-xs-12">
                         <label for="exam_stage">
                            <h5>Which stage of the exam have you reached?</h5>
                         </label>
                        <div class="col-md-12">
                         <label class="radio-inline">
                             <input type="radio" name="exam_stage" <?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'REGISTERED') echo 'checked="checked"';?> id="registered" value="REGISTERED">Registered
                         </label>
                         <label class="radio-inline">
                             <input type="radio" name="exam_stage" <?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_WAIT') echo 'checked="checked"';?> id="result_wait" value="RESULT_WAIT">Waiting For Result
                         </label>
                         <label class="radio-inline">
                             <input type="radio" name="exam_stage" <?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT') echo 'checked="checked"';?> id="result_out" value="RESULT_OUT">Have Result
                         </label>
                        </div>
                      </div>
                      <span class="sep col-lg-12"></span>
                   </div>

                   <div class="form-group" id="exam_date" <?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'REGISTERED') echo 'style=display:block;'?>>
                      <div class="col-xs-12">
                         <label for="exam_date">
                            <h5>Exam Date</h5>
                         </label>
                         <input type="date" name="exam_date" class="form-control" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'REGISTERED') echo $qualified_exams->competitive_exam_stage['value']?>"/>
                      </div>
                      <span class="sep col-lg-12"></span>
                   </div>
                   <div class="form-group" id="result_date" <?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_WAIT') echo 'style=display:block;'?>>
                      <div class="col-xs-12">
                         <label for="result_date">
                            <h5>Expected Result Date</h5>
                         </label>
                         <input type="date" name="result_date" class="form-control" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_WAIT') echo $qualified_exams->competitive_exam_stage['value']?>"/>
                      </div>
                      <span class="sep col-lg-12"></span>
                   </div>
                   <div class="form-group" id="exam_score" <?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT') echo 'style=display:block;' ?>>
                       <div class="col-xs-12" id="exam_score_gre" <?php if($qualified_exams && $qualified_exams->competitive_exam_type == 'GRE' && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT') echo 'style=display:block;' ?>>
                           <div class="col-xs-3">
                               <label for="exam_score_quant_gre">
                                  <h5>Quant</h5>
                               </label>
                               <input name="exam_score_quant_gre" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_quant_gre'])) echo $qualified_exams->competitive_exam_stage['exam_score_quant_gre'] ?>">
                           </div>

                           <div class="col-xs-3">
                               <label for="exam_score_verbal_gre">
                                  <h5>Verbal</h5>
                               </label>
                               <input name="exam_score_verbal_gre" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_verbal_gre'])) echo $qualified_exams->competitive_exam_stage['exam_score_verbal_gre'] ?>">
                           </div>

                           <div class="col-xs-3">
                               <label for="exam_score_awa_gre">
                                  <h5>AWA</h5>
                               </label>
                               <input name="exam_score_awa_gre" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_awa_gre'])) echo $qualified_exams->competitive_exam_stage['exam_score_awa_gre'] ?>">
                           </div>

                           <div class="col-xs-3">
                               <label for="exam_score_total_gre">
                                  <h5>Total</h5>
                               </label>
                               <input name="exam_score_total_gre" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_total_gre'])) echo $qualified_exams->competitive_exam_stage['exam_score_total_gre'] ?>">
                           </div>
                       </div>

                       <div class="col-xs-12" id="exam_score_gmat" <?php if($qualified_exams && $qualified_exams->competitive_exam_type == 'GMAT' && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT') echo 'style=display:block;' ?>>
                           <div class="col-xs-2">
                               <label for="exam_score_quant_gmat">
                                  <h5>Quant</h5>
                               </label>
                               <input name="exam_score_quant_gmat" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_quant_gmat'])) echo $qualified_exams->competitive_exam_stage['exam_score_quant_gmat'] ?>">
                           </div>

                           <div class="col-xs-2">
                               <label for="exam_score_verbal_gmat">
                                  <h5>Verbal</h5>
                               </label>
                               <input name="exam_score_verbal_gmat" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_verbal_gmat'])) echo $qualified_exams->competitive_exam_stage['exam_score_verbal_gmat'] ?>">
                           </div>

                           <div class="col-xs-2">
                               <label for="exam_score_ir">
                                  <h5>IR</h5>
                               </label>
                               <input name="exam_score_ir" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_ir'])) echo $qualified_exams->competitive_exam_stage['exam_score_ir'] ?>">
                           </div>

                           <div class="col-xs-3">
                               <label for="exam_score_awa_gmat">
                                  <h5>AWA</h5>
                               </label>
                               <input name="exam_score_awa_gmat" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_awa_gmat'])) echo $qualified_exams->competitive_exam_stage['exam_score_awa_gmat'] ?>">
                           </div>

                           <div class="col-xs-3">
                               <label for="exam_score_total_gmat">
                                  <h5>Total</h5>
                               </label>
                               <input name="exam_score_total_gmat" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_total_gmat'])) echo $qualified_exams->competitive_exam_stage['exam_score_total_gmat'] ?>">
                           </div>
                       </div>

                       <div class="col-xs-12" id="exam_score_sat" <?php if($qualified_exams && $qualified_exams->competitive_exam_type == 'SAT' && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT') echo 'style=display:block;' ?>>
                           <div class="col-xs-3">
                               <label for="exam_score_cirital_reading">
                                  <h5>Critical Reading</h5>
                               </label>
                               <input name="exam_score_cirital_reading" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_cirital_reading'])) echo $qualified_exams->competitive_exam_stage['exam_score_cirital_reading'] ?>">
                           </div>

                           <div class="col-xs-3">
                               <label for="exam_score_writing">
                                  <h5>Writing</h5>
                               </label>
                               <input name="exam_score_writing" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_writing'])) echo $qualified_exams->competitive_exam_stage['exam_score_writing'] ?>">
                           </div>

                           <div class="col-xs-3">
                               <label for="exam_score_math">
                                  <h5>Math</h5>
                               </label>
                               <input name="exam_score_math" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_math'])) echo $qualified_exams->competitive_exam_stage['exam_score_math'] ?>">
                           </div>

                           <div class="col-xs-3">
                               <label for="exam_score_total_sat">
                                  <h5>Total</h5>
                               </label>
                               <input name="exam_score_total_sat" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->competitive_exam_stage['name'] == 'RESULT_OUT' && isset($qualified_exams->competitive_exam_stage['exam_score_total_sat'])) echo $qualified_exams->competitive_exam_stage['exam_score_total_sat'] ?>">
                           </div>
                       </div>
                       <span class="sep col-lg-12"></span>
                   </div>
                   <div class="form-group" id="english_proficiency_exam">
                      <div class="col-xs-12">
                         <label for="english_proficiency_exam">
                            <h5>Have you appeared any of the following exam?</h5>
                         </label>
                        <div class="col-md-12">
                         <label class="radio-inline">
                             <input type="radio" name="english_proficiency_exam" <?php if($qualified_exams && $qualified_exams->english_exam_type == 'TOEFL') echo 'checked="checked"';?> id="toefl" value="TOEFL">TOEFL
                         </label>
                         <label class="radio-inline">
                             <input type="radio" name="english_proficiency_exam" <?php if($qualified_exams && $qualified_exams->english_exam_type == 'IELTS') echo 'checked="checked"';?> id="ielts" value="IELTS">IELTS
                         </label>
                         <label class="radio-inline">
                             <input type="radio" name="english_proficiency_exam" <?php if($qualified_exams && $qualified_exams->english_exam_type == 'PTE') echo 'checked="checked"';?> id="pte" value="PTE">PTE
                         </label>
                         <label class="radio-inline">
                             <input type="radio" name="english_proficiency_exam" <?php if($qualified_exams && $qualified_exams->english_exam_type == 'NO') echo 'checked="checked"';?> id="no_english_proficiency_exam" value="NO">NO
                         </label>
                         <label class="radio-inline">
                             <input type="radio" name="english_proficiency_exam" <?php if($qualified_exams && $qualified_exams->english_exam_type == 'NOT_REQUIRED') echo 'checked="checked"';?> id="not_required_english_proficiency_exam" value="NOT_REQUIRED">NOT_REQUIRED
                         </label>
                        </div>
                      </div>
                      <span class="sep col-lg-12"></span>
                   </div>

                   <div class="form-group" id="english_exam_stage" <?php if($qualified_exams && $qualified_exams->english_exam_stage['name']) echo 'style=display:block;'?>>
                      <div class="col-xs-12">
                         <label for="english_exam_stage">
                            <h5>Which stage of the exam have you reached?</h5>
                         </label>
                        <div class="col-md-12">
                         <label class="radio-inline">
                             <input type="radio" name="english_exam_stage" <?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'REGISTERED') echo 'checked="checked"';?> id="english_registered" value="REGISTERED">Registered
                         </label>
                         <label class="radio-inline">
                             <input type="radio" name="english_exam_stage" <?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_WAIT') echo 'checked="checked"';?> id="english_result_wait" value="RESULT_WAIT">Waiting For Result
                         </label>
                         <label class="radio-inline">
                             <input type="radio" name="english_exam_stage" <?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT') echo 'checked="checked"';?> id="english_result_out" value="RESULT_OUT">Have Result
                         </label>
                        </div>
                      </div>
                      <span class="sep col-lg-12"></span>
                   </div>

                   <div class="form-group" id="english_exam_date" <?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'REGISTERED') echo 'style=display:block;'?>>
                      <div class="col-xs-12">
                         <label for="english_exam_date">
                            <h5>Exam Date</h5>
                         </label>
                         <input type="date" name="english_exam_date" class="form-control" value="<?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'REGISTERED') echo $qualified_exams->english_exam_stage['value']?>"/>
                      </div>
                      <span class="sep col-lg-12"></span>
                   </div>
                   <div class="form-group" id="english_result_date" <?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_WAIT') echo 'style=display:block;'?>>
                      <div class="col-xs-12">
                         <label for="english_result_date">
                            <h5>Expected Result Date</h5>
                         </label>
                         <input type="date" name="english_result_date" class="form-control" value="<?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_WAIT') echo $qualified_exams->english_exam_stage['value']?>"/>
                      </div>
                      <span class="sep col-lg-12"></span>
                   </div>
                   <div class="form-group" id="english_exam_score" <?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT') echo 'style=display:block;' ?>>
                       <div class="col-xs-12" id="english_exam_score_all">
                           <div class="col-xs-2">
                               <label for="english_exam_score_reading">
                                  <h5>Reading</h5>
                               </label>
                               <input name="english_exam_score_reading" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_reading']) echo $qualified_exams->english_exam_stage['english_exam_score_reading'] ?>">
                           </div>

                           <div class="col-xs-2">
                               <label for="english_exam_score_listening">
                                  <h5>Listening</h5>
                               </label>
                               <input name="english_exam_score_listening" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_listening']) echo $qualified_exams->english_exam_stage['english_exam_score_listening'] ?>">
                           </div>

                           <div class="col-xs-2">
                               <label for="english_exam_score_writing">
                                  <h5>Writing</h5>
                               </label>
                               <input name="english_exam_score_writing" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_writing']) echo $qualified_exams->english_exam_stage['english_exam_score_writing'] ?>">
                           </div>

                           <div class="col-xs-3">
                               <label for="english_exam_score_speaking">
                                  <h5>Speaking</h5>
                               </label>
                               <input name="english_exam_score_speaking" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_speaking']) echo $qualified_exams->english_exam_stage['english_exam_score_speaking'] ?>">
                           </div>

                           <div class="col-xs-3">
                               <label for="english_exam_score_total">
                                  <h5>Total</h5>
                               </label>
                               <input name="english_exam_score_total" class="form-control" type="text" value="<?php if($qualified_exams && $qualified_exams->english_exam_stage['name'] == 'RESULT_OUT' && $qualified_exams->english_exam_stage['english_exam_score_total']) echo $qualified_exams->english_exam_stage['english_exam_score_total'] ?>">
                           </div>
                       </div>
                       <span class="sep col-lg-12"></span>
                   </div>

                   <div class="form-group">
                       <div class="col-xs-12">
                            <label for="secondary_qualification">
                                <h5>Secondary Qualification</h5>
                            </label>
                            <table style="width: 100%;">
                                <th><label style="font-weight: 600;">Qualification</label></th>
                                <th><label style="font-weight: 600;">Institution Name</label></th>
                                <th><label style="font-weight: 600;">Board</label></th>
                                <th><label style="font-weight: 600;">Year Started</label></th>
                                <th><label style="font-weight: 600;">Year Finished</label></th>
                                <th><label style="font-weight: 600;">Grade/ %</label></th>
                                <th><label style="font-weight: 600;">Out Of Total</label></th>
                                <tbody>
                                    <tr>
                                        <td><label>Grade X</label></</td>
                                        <td><input type="text" name="secondary_institution_name" id="secondary_institution_name" class="form-control" value="<?php if($secondaryqualification_details !='') echo $secondaryqualification_details->institution_name; ?>"/></td>
                                        <td><input type="text" name="board" id="board" class="form-control" value="<?php if($secondaryqualification_details !='') echo $secondaryqualification_details->board; ?>"/></td>
                                        <td>
                                            <select class="form-control" id="secondary_year_starrted" name="secondary_year_starrted">
                                                <?php
                                                for($i=1980;$i<=2030;$i++)
                                                {
                                                    ?>
                                                    <option value="<?php echo $i;?>" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->year_started == $i) echo 'selected="selected"';?> ><?php echo $i;?></option>
                                                    <?php
                                                }?>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control" id="secondary_year_finished" name="secondary_year_finished">
                                                <?php
                                                for($i=1980;$i<=2030;$i++)
                                                {
                                                    ?>
                                                    <option value="<?php echo $i;?>" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->year_finished == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </td>
                                        <td><input type="text" name="secondary_grade" id="secondary_grade" class="form-control" value="<?php if($secondaryqualification_details !='') echo $secondaryqualification_details->grade; ?>"/></td>
                                        <td>
                                            <select class="form-control" id="secondary_total" name="secondary_total">
                                                <option value="100" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 100) echo 'selected="selected"';?>>100</option>
                                                <option value="10"  <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 10) echo 'selected="selected"';?>>10</option>
                                                <option value="7"  <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 7) echo 'selected="selected"';?>>7</option>
                                                <option value="4"  <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 4) echo 'selected="selected"';?>>4</option>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                      </div>
                      <span class="sep col-lg-12"></span>
                   </div>

                   <div class="form-group">
                      <div class="col-xs-12">
                         <label for="diploma_12">
                            <h5>Have you done Diploma or 12th After School?</h5>
                         </label>
                        <div class="col-md-12">
                         <label class="radio-inline">
                             <input type="radio" name="diploma_12" <?php if($diplomaqualification_details !='') echo 'checked="checked"';?> id="diploma" value="1">Diploma
                         </label>
                         <label class="radio-inline">
                             <input type="radio" name="diploma_12" <?php if($highersecondaryqualification_details !='') echo 'checked="checked"';?> id="12th" value="2">12TH
                         </label>
                        </div>
                      </div>
                      <span class="sep col-lg-12"></span>
                   </div>

                   <div class="form-group diploma_hs" id="12thidd" <?php if($highersecondaryqualification_details !=''){?> style="display:block;"<?php }else{?>style="display:none;"<?php }?>>
                      <div class="col-xs-12">
                         <label for="higher_secondary_qualification">
                            <h5>Higher Secondary Qualification</h5>
                         </label>
                         <table style="width: 100%;">
                             <th><label style="font-weight: 600;">Qualification</label></th>
                             <th><label style="font-weight: 600;">Institution Name</label></th>
                             <th><label style="font-weight: 600;">Board</label></th>
                             <th><label style="font-weight: 600;">Year Started</label></th>
                             <th><label style="font-weight: 600;">Year Finished</label></th>
                             <th><label style="font-weight: 600;">Grade/ %</label></th>
                             <th><label style="font-weight: 600;">Out Of Total</label></th>
                             <tbody>
                                 <tr>
                                     <td><label>Grade XII</label></</td>
                                     <td><input type="text" name="hs_institution_name" id="hs_institution_name" class="form-control" value="<?php if($highersecondaryqualification_details !='') echo $highersecondaryqualification_details->institution_name; ?>"/></td>
                                     <td><input type="text" name="hs_board" id="hs_board" class="form-control" value="<?php if($highersecondaryqualification_details !='') echo $highersecondaryqualification_details->board; ?>"/></td>
                                     <td>
                                         <select class="form-control" id="hs_year_starrted" name="hs_year_starrted">
                                             <?php
                                             for($i=1980;$i<=2030;$i++)
                                             {
                                                 ?>
                                                 <option value="<?php echo $i;?>" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->year_started == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control" id="hs_year_finished" name="hs_year_finished">
                                            <?php
                                            for($i=1980;$i<=2030;$i++)
                                            {
                                                ?>
                                                <option value="<?php echo $i;?>" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->year_finished == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td><input type="text" name="hs_grade" id="hs_grade" class="form-control" value="<?php if($highersecondaryqualification_details !='') echo $highersecondaryqualification_details->grade; ?>"/></td>
                                    <td>
                                        <select class="form-control" id="hs_total" name="hs_total">
                                            <option value="100" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 100) echo 'selected="selected"';?>>100</option>
                                            <option value="10" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 10) echo 'selected="selected"';?>>10</option>
                                            <option value="7" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 7) echo 'selected="selected"';?>>7</option>
                                            <option value="4" <?php if($secondaryqualification_details !='' && $secondaryqualification_details->total == 4) echo 'selected="selected"';?>>4</option>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                      </div>
                      <span class="sep col-lg-12"></span>
                   </div>

                   <div class="form-group diploma_hs" id="diploma_id" <?php if($diplomaqualification_details !=''){?> style="display:block;"<?php }else{?>style="display:none;"<?php }?>>
                      <div class="col-xs-12">
                         <label for="diploma">
                            <h5>Diploma</h5>
                         </label>
                         <table style="width: 100%;">
                             <th><label style="font-weight: 600;">Name of Course</label></th>
                             <th><label style="font-weight: 600;">College  Name</label></th>
                             <th><label style="font-weight: 600;">Year Started</label></th>
                             <th><label style="font-weight: 600;">Year Finished</label></th>
                             <th><label style="font-weight: 600;">Aggregate Percentage (1 to 6 Sem)</label></th>
                             <th><label style="font-weight: 600;">Out Of Total</label></th>
                             <tbody>
                                 <tr>
                                     <td><input type="text" name="diploma_course_name" id="diploma_course_name" class="form-control" value="<?php if($diplomaqualification_details !='') echo $diplomaqualification_details->course; ?>"/></</td>
                                     <td><input type="text" name="diploma_institution_name" id="diploma_institution_name" class="form-control" value="<?php if($diplomaqualification_details !='') echo $diplomaqualification_details->college; ?>"/></td>
                                     <td>
                                         <select class="form-control" id="diploma_year_starrted" name="diploma_year_starrted">
                                             <?php
                                             for($i=1980;$i<=2030;$i++)
                                             {
                                                 ?>
                                                 <option value="<?php echo $i;?>" <?php if($diplomaqualification_details !='' && $diplomaqualification_details->year_started == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control" id="diploma_year_finished" name="diploma_year_finished">
                                            <?php
                                            for($i=1980;$i<=2030;$i++)
                                            {
                                                ?>
                                                <option value="<?php echo $i;?>" <?php if($diplomaqualification_details !='' && $diplomaqualification_details->year_finished == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td><input type="text" name="diploma_grade" id="diploma_grade" class="form-control" value="<?php if($diplomaqualification_details !='') echo $diplomaqualification_details->aggregate; ?>"/></td>
                                    <td>
                                        <select class="form-control" id="diploma_total" name="diploma_total">
                                            <option value="100" <?php if($diplomaqualification_details !='' && $diplomaqualification_details->total == 100) echo 'selected="selected"';?>>100</option>
                                            <option value="10" <?php if($diplomaqualification_details !='' && $diplomaqualification_details->total == 10) echo 'selected="selected"';?>>10</option>
                                            <option value="7" <?php if($diplomaqualification_details !='' && $diplomaqualification_details->total == 7) echo 'selected="selected"';?>>7</option>
                                            <option value="4" <?php if($diplomaqualification_details !='' && $diplomaqualification_details->total == 10) echo 'selected="selected"';?>>4</option>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                     </div>
                     <span class="sep col-lg-12"></span>
                   </div>
                   <div class="form-group">
                       <div class="col-xs-12">
                            <label for="bachelor_graduation">
                                <h5>Bachelor's/Graduation</h5>
                            </label>
                            <table style="width: 100%;">
                                <th><label style="font-weight: 600;">Name of Course</label></th>
                                <th><label style="font-weight: 600;">University</label></th>
                                <th><label style="font-weight: 600;">College  Name</label></th>
                                <th><label style="font-weight: 600;">Major</label></th>
                                <th><label style="font-weight: 600;">Year Started</label></th>
                                <th><label style="font-weight: 600;">Year Finished</label></th>
                                <th><label style="font-weight: 600;">Grade / %</label></th>
                                <th><label style="font-weight: 600;">Out Of Total</label></th>
                                <tbody>
                                    <tr>
                                        <td width="15%"><input type="text" name="bachelor_course" id="bachelor_course" class="form-control" value="<?php if($bachelorqualification_details !='') echo $bachelorqualification_details->course; ?>"/></td>
                                        <td width="15%"><input type="text" name="bachelor_university" id="bachelor_university" class="form-control" value="<?php if($bachelorqualification_details !='') echo $bachelorqualification_details->university; ?>"/></td>
                                        <td width="15%"><input type="text" name="bachelor_college" id="bachelor_college" class="form-control" value="<?php if($bachelorqualification_details !='') echo $bachelorqualification_details->college; ?>"/></td>
                                        <td width="15%"><input type="text" name="bachelor_major" id="bachelor_major" class="form-control" value="<?php if($bachelorqualification_details !='') echo $bachelorqualification_details->major; ?>"/></td>
                                        <td width="10%">
                                            <select class="form-control" id="bachelor_year_starrted" name="bachelor_year_starrted">
                                                <?php
                                                for($i=1980;$i<=2030;$i++)
                                                {
                                                    ?>
                                                    <option value="<?php echo $i;?>" <?php if($bachelorqualification_details !='' && $bachelorqualification_details->year_started == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </td>
                                        <td width="10%">
                                            <select class="form-control" id="bachelor_year_finished" name="bachelor_year_finished">
                                                <?php
                                                for($i=1980;$i<=2030;$i++)
                                                {
                                                    ?>
                                                    <option value="<?php echo $i;?>" <?php if($bachelorqualification_details !='' && $bachelorqualification_details->year_finished == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </td>
                                        <td width="10%"><input type="text" name="bachelor_grade" id="bachelor_grade" class="form-control" value="<?php if($bachelorqualification_details !='') echo $bachelorqualification_details->grade;?>"/></td>
                                        <td width="10%">
                                            <select class="form-control" id="bachelor_total" name="bachelor_total">
                                                <option value="100" <?php if($bachelorqualification_details !='' && $bachelorqualification_details->total == 100) echo 'selected="selected"';?>>100</option>
                                                <option value="10" <?php if($bachelorqualification_details !='' && $bachelorqualification_details->total == 10) echo 'selected="selected"';?>>10</option>
                                                <option value="7" <?php if($bachelorqualification_details !='' && $bachelorqualification_details->total == 7) echo 'selected="selected"';?>>7</option>
                                                <option value="4" <?php if($bachelorqualification_details !='' && $bachelorqualification_details->total == 4) echo 'selected="selected"';?>>4</option>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <span class="sep col-lg-12"></span>
                    </div>

                    <div class="form-group">
                       <div class="col-xs-12">
                          <label for="masters">
                             <h5>Have you done Master?</h5>
                          </label>
                         <div class="col-md-12">
                          <label class="radio-inline">
                              <input type="radio" name="masters" <?php if($mastersqualification_details !='') echo 'checked="checked"';?> id="master_yes" value="1">Yes
                          </label>
                          <label class="radio-inline">
                              <input type="radio" name="masters" <?php if($mastersqualification_details =='') echo 'checked="checked"';?> id="master_no" value="2">No
                          </label>
                         </div>
                       </div>
                       <span class="sep col-lg-12"></span>
                    </div>

                    <div class="form-group" id="masters_id" <?php if($mastersqualification_details !=''){?> style="display:block;"<?php }else{?>style="display:none;"<?php }?>>
                        <div class="col-xs-12">
                             <label for="master_degree">
                                 <h5>Master's</h5>
                             </label>
                             <table style="width: 100%;">
                                 <th><label style="font-weight: 600;">Name of Course</label></th>
                                 <th><label style="font-weight: 600;">University</label></th>
                                 <th><label style="font-weight: 600;">College  Name</label></th>
                                 <th><label style="font-weight: 600;">Major</label></th>
                                 <th><label style="font-weight: 600;">Year Started</label></th>
                                 <th><label style="font-weight: 600;">Year Finished</label></th>
                                 <th><label style="font-weight: 600;">Grade / %</label></th>
                                 <th><label style="font-weight: 600;">Out Of Total</label></th>
                                 <tbody>
                                     <tr>
                                         <td width="15%"><input type="text" name="master_course" id="master_course" class="form-control" style="width:85%;" value="<?php if($mastersqualification_details !='') echo $mastersqualification_details->course; ?>"/></td>
                                         <td width="15%"><input type="text" name="master_university" id="master_university" class="form-control" style="width:85%;" value="<?php if($mastersqualification_details !='') echo $mastersqualification_details->university; ?>"/></td>
                                         <td width="15%"><input type="text" name="master_college" id="master_college" class="form-control" style="width:85%;" value="<?php if($mastersqualification_details !='') echo $mastersqualification_details->college; ?>"/></td>
                                         <td width="15%"><input type="text" name="master_major" id="master_major" class="form-control" style="width:85%;" value="<?php if($mastersqualification_details !='') echo $mastersqualification_details->major; ?>"/></td>
                                         <td width="10%">
                                             <select class="form-control" id="master_year_started" name="master_year_started">
                                                 <?php
                                                 for($i=1980;$i<=2030;$i++)
                                                 {
                                                     ?>
                                                     <option value="<?php echo $i;?>" <?php if($mastersqualification_details !='' && $mastersqualification_details->year_started == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </td>
                                        <td width="10%">
                                            <select class="form-control" id="master_year_finished" name="master_year_finished">
                                                <?php
                                                for($i=1980;$i<=2030;$i++)
                                                {
                                                    ?>
                                                    <option value="<?php echo $i;?>" <?php if($mastersqualification_details !='' && $mastersqualification_details->year_finished == $i) echo 'selected="selected"';?>><?php echo $i;?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </td>
                                        <td width="10%"><input type="text" name="master_grade" id="master_grade" class="form-control" style="width:85%;" value="<?php if($mastersqualification_details !='') echo $mastersqualification_details->grade; ?>" /></td>
                                        <td width="10%">
                                            <select class="form-control" id="master_total" name="master_total">
                                                <option value="100" <?php if($mastersqualification_details !='' && $mastersqualification_details->total == 100) echo 'selected="selected"';?>>100</option>
                                                <option value="10" <?php if($mastersqualification_details !='' && $mastersqualification_details->total == 10) echo 'selected="selected"';?>>10</option>
                                                <option value="7" <?php if($mastersqualification_details !='' && $mastersqualification_details->total == 7) echo 'selected="selected"';?>>7</option>
                                                <option value="4" <?php if($mastersqualification_details !='' && $mastersqualification_details->total == 4) echo 'selected="selected"';?>>4</option>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                         </div>
                         <span class="sep col-lg-12"></span>
                     </div>

                   <div class="form-group">
                      <div class="col-xs-12">
                         <br>
                         <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                         <!--button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button-->
                      </div>
                   </div>
                   <input type="hidden" name="form_type" value="qualification_details" />
                   <input type="hidden" name="user_id" value="<?=$id?>" />
                </form>
             </div>
             <div class="tab-pane" id="desired_courses">
                 <hr>
                 <?php
                 $destination = array();
                 $destination = explode(",",$desired_destination);
                 ?>
                 <form class="form" action="/user/account/save" method="post" id="desired-courses-form">
                     <div class="form-group">
                        <div class="col-md-12">
                           <label for="desired_destination">
                              <h5>Desired Destination</h5>
                           </label>
                        </div>
                        <div class="col-md-12">

                           <?php
                           foreach($countries as $val)
                           {
                               ?>
                               <div class="col-md-4">
                                <div class="row" style="display: flex;">
                                  <div class="col-md-8">
                                  <label><h6><?php echo $val->name;?></h6></label>
                                  </div>
                                  <div class="col-md-4">
                                  <input type="checkbox" class="form-control checkBoxDesign" name="deisred_destinations[]" value="<?php echo $val->id;?>" <?php if(isset($destination) && in_array($val->id, $destination)) echo 'checked="checked"';?>/>
                                 </div>
                                <!-- <label><?php echo $val->name;?></label>&nbsp;&nbsp;&nbsp;&nbsp; -->
                                </div>
                               </div>
                               <?php
                            }
                            ?>

                        </div>
                     </div>

                     <div class="form-group">
                         <div class="col-xs-12">
                              <label for="desired_courses">
                                  <h5>Desired Courses</h5>
                              </label>
                              <table style="width: 100%;">
                                  <th><label style="font-weight: 600;">Desired Course</label></th>
                                  <th><label style="font-weight: 600;">Level</label></th>
                                  <th><label style="font-weight: 600;">Desired intake in Months</label></th>
                                  <th><label style="font-weight: 600;">Year of Admission</label></th>
                                  <tbody>
                                      <tr>
                                          <td><input type="text" name="desired_course_name" id="desired_course_name" class="form-control form_font_color input_field" value="<?php if($desiredcourse_details !='') echo $desiredcourse_details->desired_course_name;?>" /></td>
                                          <td>
                                              <select class="form-control" id="desired_coures_level" name="desired_coures_level">
                                                  <option value="1" <?php if($desiredcourse_details !='' && $desiredcourse_details->desired_course_level == 1) echo 'selected="selected"';?>>Graduate/Diploma</option>
                                                  <option value="2" <?php if($desiredcourse_details !='' && $desiredcourse_details->desired_course_level == 2) echo 'selected="selected"';?>>Post Graduation/Masters</option>
                                                  <option value="3" <?php if($desiredcourse_details !='' && $desiredcourse_details->desired_course_level == 3) echo 'selected="selected"';?>>Professional Program</option>
                                                  <option value="4" <?php if($desiredcourse_details !='' && $desiredcourse_details->desired_course_level == 4) echo 'selected="selected"';?>>Doctorate</option>
                                              </select>
                                          </td>
                                          <td>
                                              <select class="form-control" id="desired_course_intake" name="desired_course_intake">
                                                  <?php
                                                  foreach($intakes as $intk)
                                                  {
                                                      ?>
                                                      <option value="<?php echo $intk->id;?>" <?php if($desiredcourse_details !='' && $desiredcourse_details->desired_course_intake == $intk->id) echo 'selected="selected"';?>><?php echo $intk->name;?></option>
                                                      <?php
                                                  }
                                                  ?>
                                              </select>
                                          </td>
                                          <td>
                                              <select class="form-control" id="desired_coures_year" name="desired_coures_year">
                                                  <option value="2016" <?php if($desiredcourse_details !='' && $desiredcourse_details->desired_course_year == 2016) echo 'selected="selected"';?>>2016</option>
                                                  <option value="2017" <?php if($desiredcourse_details !='' && $desiredcourse_details->desired_course_year == 2017) echo 'selected="selected"';?>>2017</option>
                                                  <option value="2018" <?php if($desiredcourse_details !='' && $desiredcourse_details->desired_course_year == 2018) echo 'selected="selected"';?>>2018</option>
                                                  <option value="2019" <?php if($desiredcourse_details !='' && $desiredcourse_details->desired_course_year == 2019) echo 'selected="selected"';?>>2019</option>
                                              </select>
                                          </td>
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                      <div class="form-group">
                         <div class="col-xs-12">
                            <br>
                            <button class="btn btn-lg prplButton" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                            <!--button class="btn btn-lg resetButton" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button-->
                         </div>
                      </div>
                      <input type="hidden" name="form_type" value="desired_courses" />
                      <input type="hidden" name="user_id" value="<?=$id?>" />
                 </form>
             </div>
             <!--/tab-pane-->
             <div class="tab-pane" id="parent_detail">
                <hr>
                <form class="form" action="/user/account/save" method="post" id="parent-detail-form">
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="parent_name">
                            <h5>Parent Name</h5>
                         </label>
                         <input type="text" class="form-control" name="parent_name" id="parent_name" placeholder="Parent Name" value="<?php if($parent_name) echo $parent_name;?>">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="parent_occupation">
                            <h5>Parent Occupation</h5>
                         </label>
                         <input type="text" class="form-control" name="parent_occupation" id="parent_occupation" placeholder="Parent Occupation" value="<?php if($parent_occupation) echo $parent_occupation;?>">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="parent_mob">
                            <h5>Parent Mobile</h5>
                         </label>
                         <input type="text" class="form-control" name="parent_mob" id="parent_mob" placeholder="Parent Mobile" value="<?php if($parent_mob) echo $parent_mob;?>">
                      </div>
                   </div>

                   <div class="form-group">
                      <div class="col-xs-12">
                         <br>
                         <button class="btn btn-lg prplButton pull-right" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                         <!--<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>-->
                      </div>
                   </div>
                   <input type="hidden" name="form_type" value="parent_details" />
                   <input type="hidden" name="user_id" value="<?=$id?>" />
                </form>
             </div>

             <div class="tab-pane" id="application_form">
                <hr>
                <form class="form" action="/university/details/application_submit" method="post" id="application-form">
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="university_name">
                            <h5>University Name</h5>
                         </label>
                         <input type="text" class="form-control" name="university_name" id="university_name" placeholder="University Name" value="<?=$university_name?>">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="college">
                            <h5>College*</h5>
                         </label>
                         <select name="college" id="college" class="form-control" <?=isset($applied_university_detail) && $applied_university_detail['college_id'] ? 'disabled' : ''?> onchange="showDepartments(this.value)">
                             <option value="0">Select College</option>
                             <?php
                             foreach($colleges as $college_id => $college_name)
                             {
                                 ?>
                                 <option value="<?php echo $college_id;?>" <?=isset($applied_university_detail) && $applied_university_detail['college_id'] == $college_id ? 'selected' : ''?>><?php echo $college_name;?></option>
                                  <?php
                              }
                              ?>
                        </select>
                    </div>
                 </div>
                    <?php
                    if(!$course_detail)
                    {
                        ?>
                        <div class="form-group">
                           <div class="col-xs-6">
                              <label for="department">
                                 <h5>Department*</h5>
                              </label>
                              <select class="form-control" name="department" id="departments_list" onchange="showCourses(this.value)"></select>
                           </div>
                        </div>

                        <div class="form-group">
                           <div class="col-xs-6">
                              <label for="course">
                                 <h5>Course*</h5>
                              </label>
                              <select class="form-control" name="course" id="courses_list"></select>
                           </div>
                        </div>
                        <?php
                    }
                    else
                    {
                        ?>
                        <div class="form-group">
                           <div class="col-xs-6">
                              <label for="department">
                                 <h5>Department</h5>
                              </label>
                              <input type="text" class="form-control" name="department" id="department" value="<?=$course_detail['department_name']?>" disabled>
                           </div>
                        </div>

                        <div class="form-group">
                           <div class="col-xs-6">
                              <label for="course">
                                 <h5>Course</h5>
                              </label>
                              <input type="text" class="form-control" name="course" id="course" value="<?=$course_detail['name'] . ' (' . $course_detail['degree_name'] . ')'?>" disabled>
                           </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="form-group">
                       <div class="col-xs-6">
                          <label for="login_url">
                             <h5>Login URL</h5>
                          </label>
                          <input type="text" class="form-control" name="login_url" id="login_url" placeholder="Login URL" value="<?=isset($applied_university_detail) ? $applied_university_detail['login_url'] : ''?>">
                       </div>
                    </div>

                    <div class="form-group">
                       <div class="col-xs-6">
                          <label for="username">
                             <h5>Username</h5>
                          </label>
                          <input type="text" class="form-control" name="username" id="username" value="<?=isset($applied_university_detail) ? $applied_university_detail['username'] : ''?>" autocomplete="false" readonly onfocus="this.removeAttribute('readonly');">
                       </div>
                    </div>

                    <div class="form-group">
                       <div class="col-xs-6">
                          <label for="password">
                             <h5>Password</h5>
                          </label>
                          <input type="text" class="form-control" name="password" id="password" value="<?=isset($applied_university_detail) ? $applied_university_detail['password'] : ''?>" autocomplete="false" readonly onfocus="this.removeAttribute('readonly');">
                       </div>
                    </div>

                    <div class="form-group">
                       <div class="col-xs-6">
                          <label for="application_deadline">
                             <h5>Application Deadline</h5>
                          </label>
                          <input type="date" class="form-control" name="application_deadline" id="application_deadline" value="<?=isset($applied_university_detail) ? $applied_university_detail['dead_line'] : ''?>">
                       </div>
                    </div>

                    <div class="form-group">
                       <div class="col-xs-6">
                          <label for="gre_code">
                             <h5>GRE (Code)</h5>
                          </label>
                          <input type="text" class="form-control" name="gre_code" id="gre_code" placeholder="GRE (Code)" value="<?=isset($applied_university_detail) ? $applied_university_detail['gre_code'] : ''?>">
                       </div>
                    </div>

                    <div class="form-group">
                       <div class="col-xs-6">
                          <label for="toefl_code">
                             <h5>TOEFL (Code)</h5>
                          </label>
                          <input type="text" class="form-control" name="toefl_code" id="toefl_code" placeholder="TOEFL (Code)" value="<?=isset($applied_university_detail) ? $applied_university_detail['toefl_code'] : ''?>">
                       </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-6">
                           <label for="sop_needed">
                             <h5>SOP (Statement Of Purpose)</h5>
                           </label>
                          <div class="col-md-12">
                           <label class="radio-inline">
                               <input type="radio" name="sop_needed" value="1" onclick="showNoOfWords()" <?=isset($applied_university_detail) && isset($applied_university_detail['sop']['sop_needed']) && $applied_university_detail['sop']['sop_needed'] == 1 ? 'checked' : ''?>>Yes
                           </label>
                           <label class="radio-inline">
                               <input type="radio" name="sop_needed" value="0" onclick="hideNoOfWords()" <?=isset($applied_university_detail) && isset($applied_university_detail['sop']['sop_needed']) && $applied_university_detail['sop']['sop_needed'] == 0 ? 'checked' : ''?>>No
                           </label>
                          </div>
                        </div>
                    </div>

                    <div class="form-group" id="sop-no-of-words" style="display:<?=$sop_no_of_words?>">
                       <div class="col-xs-6">
                          <label for="sop_no_of_words">
                             <h5>SOP - Number Of Words</h5>
                          </label>
                          <input type="text" class="form-control" name="sop_no_of_words" id="sop_no_of_words" placeholder="SOP - Number Of Words" value="<?=isset($applied_university_detail) && isset($applied_university_detail['sop']['sop_no_of_words']) ? $applied_university_detail['sop']['sop_no_of_words'] : ''?>">
                       </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-6">
                           <label for="lor_needed">
                             <h5>LOR (Letter Of Recommendation)</h5>
                           </label>
                          <div class="col-md-12">
                           <label class="radio-inline">
                               <input type="radio" name="lor_needed" value="1" onclick="showLOR()" <?=isset($applied_university_detail) && isset($applied_university_detail['lor']['lor_needed']) && $applied_university_detail['lor']['lor_needed'] == 1 ? 'checked' : ''?>>Yes
                           </label>
                           <label class="radio-inline">
                               <input type="radio" name="lor_needed" value="0" onclick="hideLOR()" <?=isset($applied_university_detail) && isset($applied_university_detail['lor']['lor_needed']) && $applied_university_detail['lor']['lor_needed'] == 0 ? 'checked' : ''?>>No
                           </label>
                          </div>
                        </div>
                    </div>

                    <div class="form-group" id="lor-required" style="display:<?=$lor_required?>">
                       <div class="col-xs-6">
                          <label for="lor_required">
                             <h5>LOR Required</h5>
                          </label>
                          <select name="lor_required" id="lor_required" class="form-control">
                              <?php
                              foreach($colleges as $college_id => $college_name)
                              {
                                  ?>
                                  <option value="<?=$i?>" <?=isset($applied_university_detail) && isset($applied_university_detail['lor']['lor_required']) && $applied_university_detail['lor']['lor_required'] == $i ? 'selected' : ''?>><?php echo $i;?></option>
                                   <?php
                               }
                               ?>
                         </select>
                       </div>
                    </div>

                    <div class="form-group" id="lor-type" style="display:<?=$lor_type?>">
                        <div class="col-xs-6">
                           <label for="lor_type">
                             <h5>LOR Type</h5>
                           </label>
                          <div class="col-md-12">
                           <label class="radio-inline">
                               <input type="radio" name="lor_type" value="offline" <?=isset($applied_university_detail) && isset($applied_university_detail['lor']['lor_type']) && $applied_university_detail['lor']['lor_type'] == "offline" ? 'checked' : ''?>>Offline
                           </label>
                           <label class="radio-inline">
                               <input type="radio" name="lor_type" value="online" <?=isset($applied_university_detail) && isset($applied_university_detail['lor']['lor_type']) && $applied_university_detail['lor']['lor_type'] == "online" ? 'checked' : ''?>>Online
                           </label>
                           <label class="radio-inline">
                               <input type="radio" name="lor_type" value="online_after_application" <?=isset($applied_university_detail) && isset($applied_university_detail['lor']['lor_type']) && $applied_university_detail['lor']['lor_type'] == "online_after_application" ? 'checked' : ''?>>Online (After Application)
                           </label>
                          </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-6">
                           <label for="transcript">
                             <h5>Transcripts</h5>
                           </label>
                          <div class="col-md-12">
                           <label class="radio-inline">
                               <input type="radio" name="transcript" value="after_admit" <?=isset($applied_university_detail) && isset($applied_university_detail['transcripts']['transcript']) && $applied_university_detail['transcripts']['transcript'] == "after_admit" ? 'checked' : ''?>>After Admit
                           </label>
                           <label class="radio-inline">
                               <input type="radio" name="transcript" value="before_admit" <?=isset($applied_university_detail) && isset($applied_university_detail['transcripts']['transcript']) && $applied_university_detail['transcripts']['transcript'] == "before_admit" ? 'checked' : ''?>>Before Admit
                           </label>
                          </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-6">
                           <label for="transcript_submitted">
                             <h5>Transcripts Submitted?</h5>
                           </label>
                          <div class="col-md-12">
                           <label class="radio-inline">
                               <input type="radio" name="transcript_submitted" value="1" <?=isset($applied_university_detail) && isset($applied_university_detail['transcripts']['transcript_submitted']) && $applied_university_detail['transcripts']['transcript_submitted'] == "1" ? 'checked' : ''?>>Yes
                           </label>
                           <label class="radio-inline">
                               <input type="radio" name="transcript_submitted" value="0" <?=isset($applied_university_detail) && isset($applied_university_detail['transcripts']['transcript_submitted']) && $applied_university_detail['transcripts']['transcript_submitted'] == "0" ? 'checked' : ''?>>No
                           </label>
                          </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-6">
                           <label for="application_fee_paid">
                             <h5>Application Fee Paid?</h5>
                           </label>
                          <div class="col-md-12">
                           <label class="radio-inline">
                               <input type="radio" name="application_fee_paid" value="1" <?=isset($applied_university_detail) && isset($applied_university_detail['application_fee']['application_fee_paid']) && $applied_university_detail['application_fee']['application_fee_paid'] == "1" ? 'checked' : ''?>>Yes
                           </label>
                           <label class="radio-inline">
                               <input type="radio" name="application_fee_paid" value="0" <?=isset($applied_university_detail) && isset($applied_university_detail['application_fee']['application_fee_paid']) && $applied_university_detail['application_fee']['application_fee_paid'] == "0" ? 'checked' : ''?>>No
                           </label>
                          </div>
                        </div>
                    </div>

                    <div class="form-group">
                       <div class="col-xs-6">
                          <label for="application_fee">
                             <h5>Application Fee</h5>
                          </label>
                          <input type="text" class="form-control" name="application_fee" id="application_fee" placeholder="Application Fee" value="<?=isset($applied_university_detail) && isset($applied_university_detail['application_fee']['application_fee']) ? $applied_university_detail['application_fee']['application_fee'] : ''?>">
                       </div>
                    </div>

                    <div class="form-group">
                       <div class="col-xs-6">
                          <label for="request_url">
                             <h5>Request URL</h5>
                          </label>
                          <input type="text" class="form-control" name="request_url" id="request_url" placeholder="Request URL" value="<?=isset($applied_university_detail) ? $applied_university_detail['requested_url'] : ''?>">
                       </div>
                    </div>

                    <div class="form-group">
                       <div class="col-xs-6">
                          <label for="status">
                             <h5>Status</h5>
                          </label>
                          <select name="status" id="status" class="form-control">
                              <?php
                              switch($current_status)
                              {
                                  case 'APPLICATION_PROCESS':
                                      ?>
                                      <option value="APPLICATION_PROCESS" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "APPLICATION_PROCESS" ? 'selected' : ''?>>In Process</option>
                                      <option value="APPLICATION_SUBMITTED" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "APPLICATION_SUBMITTED" ? 'selected' : ''?>>Submitted</option>
                                      <?php
                                      break;
                                  case 'APPLICATION_SUBMITTED':
                                      ?>
                                      <option value="APPLICATION_SUBMITTED" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "APPLICATION_SUBMITTED" ? 'selected' : ''?>>Submitted</option>
                                      <option value="ADMIT_RECEIVED_CONDITIONAL" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "ADMIT_RECEIVED_CONDITIONAL" ? 'selected' : ''?>>Admit Received (Conditional)</option>
                                      <option value="ADMIT_DENIED" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "ADMIT_DENIED" ? 'selected' : ''?>>Admit Denied</option>
                                      <option value="ADMIT_WITHDRAW" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "ADMIT_WITHDRAW" ? 'selected' : ''?>>Withdraw Admit</option>
                                      <?php
                                      break;
                                  case 'ADMIT_RECEIVED_CONDITIONAL':
                                      ?>
                                      <option value="ADMIT_RECEIVED_CONDITIONAL" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "ADMIT_RECEIVED_CONDITIONAL" ? 'selected' : ''?>>Admit Received (Conditional)</option>
                                      <option value="ADMIT_RECEIVED_UNCONDITIONAL" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "ADMIT_RECEIVED_UNCONDITIONAL" ? 'selected' : ''?>>Admit Received (Unconditional) / I 20 Received</option>
                                      <option value="ADMIT_DENIED" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "ADMIT_DENIED" ? 'selected' : ''?>>Admit Denied</option>
                                      <option value="ADMIT_WITHDRAW" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "ADMIT_WITHDRAW" ? 'selected' : ''?>>Withdraw Admit</option>
                                      <?php
                                      break;
                                  case 'ADMIT_RECEIVED_UNCONDITIONAL':
                                      ?>
                                      <option value="ADMIT_RECEIVED_UNCONDITIONAL" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "ADMIT_RECEIVED_UNCONDITIONAL" ? 'selected' : ''?>>Admit Received (Unconditional) / I 20 Received</option>
                                      <option value="ADMIT_DENIED" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "ADMIT_DENIED" ? 'selected' : ''?>>Admit Denied</option>
                                      <option value="ADMIT_WITHDRAW" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "ADMIT_WITHDRAW" ? 'selected' : ''?>>Withdraw Admit</option>
                                      <?php
                                      break;
                                  case 'ADMIT_DENIED':
                                      ?>
                                      <option value="ADMIT_DENIED" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "ADMIT_DENIED" ? 'selected' : ''?>>Admit Denied</option>
                                      <?php
                                      break;
                                  case 'ADMIT_WITHDRAW':
                                      ?>
                                      <option value="ADMIT_WITHDRAW" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "ADMIT_WITHDRAW" ? 'selected' : ''?>>Admit Withdrawn</option>
                                      <?php
                                      break;
                                  case 'VISA_PROCESS_FINANCIAL_DOCUMENTS':
                                      ?>
                                      <option value="VISA_PROCESS_FINANCIAL_DOCUMENTS" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "VISA_PROCESS_FINANCIAL_DOCUMENTS" ? 'selected' : ''?>>VISA In Process</option>
                                      <?php
                                      break;
                                  case 'VISA_APPLIED':
                                      ?>
                                      <option value="VISA_APPLIED" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "VISA_APPLIED" ? 'selected' : ''?>>VISA Applied</option>
                                      <?php
                                      break;
                                  case 'VISA_APPROVED':
                                      ?>
                                      <option value="VISA_APPROVED" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "VISA_APPROVED" ? 'selected' : ''?>>VISA Approved</option>
                                      <?php
                                      break;
                                  case 'VISA_REJECTED':
                                      ?>
                                      <option value="VISA_REJECTED" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "VISA_REJECTED" ? 'selected' : ''?>>VISA Rejected</option>
                                      <?php
                                      break;
                                  case 'VISA_APPROVED_OFFER':
                                      ?>
                                      <option value="VISA_APPROVED_OFFER" <?=isset($applied_university_detail) && $applied_university_detail['status'] == "VISA_APPROVED_OFFER" ? 'selected' : ''?>>VISA Approved With Offer</option>
                                      <?php
                                      break;
                              }
                              ?>
                         </select>
                       </div>
                    </div>
                    <div class="form-group">
                       <div class="col-xs-12">
                          <br>
                          <button class="btn btn-lg prplButton pull-right" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                       </div>
                    </div>
                    <input type="hidden" name="university_id" value="<?=$university_id?>">
                    <input type="hidden" name="application_id" value="<?=$application_id?>">
                    <input type="hidden" name="user_id" value="<?=$id?>" />
               </form>
           </div>

           <div class="tab-pane" id="visa_form">
              <hr>
              <form class="form" action="/university/details/visa_submit" method="post" id="visa-form">
                  <div style="text-align: center;font-size: 16px;margin-bottom: 10px;color: #ff0000;">You can start VISA process only for one application. Once you submit the form, you can't change it. If you want to change it further, please contact to our customer support.</div>
                  <div class="col-lg-12" >
                      <label class="col-md-12" style="text-align:center; font-size: 14px;">Manage VISA Dates</label>
                  </div>
                  <span class="sep col-lg-12"></span>
                 <div class="form-group">
                    <div class="col-xs-6">
                       <label for="visa_ofc_date">
                          <h5>OFC Date</h5>
                       </label>
                       <input type="date" class="form-control" name="visa_ofc_date" id="visa_ofc_date" value="<?=$visa_detail ? $visa_detail['ofc_date'] : ''?>" required>
                    </div>
                 </div>
                 <div class="form-group">
                    <div class="col-xs-6">
                       <label for="visa_interview_date">
                          <h5>VISA Interview/Approval/Filing Date</h5>
                       </label>
                       <input type="date" class="form-control" name="visa_interview_date" id="visa_interview_date" value="<?=$visa_detail ? $visa_detail['interview_date'] : ''?>" required>
                    </div>
                 </div>

                 <span class="sep col-lg-12"></span>
                 <div class="col-lg-12" >
                     <label class="col-md-12" style="text-align:center; font-size: 14px;">Manage VISA Applications</label>
                 </div>
                 <span class="sep col-lg-12"></span>
                 <div class="form-group">
                    <div class="col-xs-6">
                       <label for="visa_ds_160">
                          <h5>DS 160</h5>
                       </label>
                       <select name="visa_ds_160" id="visa_ds_160" class="form-control">
                           <option value="Pending" <?=$visa_detail && $visa_detail['ds_160'] == 'Pending' ? 'selected' : ''?>>Pending</option>
                           <option value="Submitted" <?=$visa_detail && $visa_detail['ds_160'] == 'Submitted' ? 'selected' : ''?>>Submitted</option>
                      </select>
                    </div>
                 </div>
                 <div class="form-group">
                    <div class="col-xs-6">
                       <label for="visa_cgi_federal">
                          <h5>CGI Federal</h5>
                       </label>
                       <select name="visa_cgi_federal" id="visa_cgi_federal" class="form-control">
                           <option value="Pending" <?=$visa_detail && $visa_detail['cgi_federal'] == 'Pending' ? 'selected' : ''?>>Pending</option>
                           <option value="Submitted" <?=$visa_detail && $visa_detail['cgi_federal'] == 'Submitted' ? 'selected' : ''?>>Submitted</option>
                      </select>
                    </div>
                 </div>
                 <div class="form-group">
                    <div class="col-xs-6">
                       <label for="visa_sevis">
                          <h5>Sevis</h5>
                       </label>
                       <select name="visa_sevis" id="visa_sevis" class="form-control">
                           <option value="Pending" <?=$visa_detail && $visa_detail['sevis'] == 'Pending' ? 'selected' : ''?>>Pending</option>
                           <option value="Submitted" <?=$visa_detail && $visa_detail['sevis'] == 'Submitted' ? 'selected' : ''?>>Submitted</option>
                      </select>
                    </div>
                 </div>

                 <span class="sep col-lg-12"></span>
                 <div class="col-lg-12" >
                     <label class="col-md-12" style="text-align:center; font-size: 14px;">Manage VISA Mock Dates</label>
                 </div>
                 <span class="sep col-lg-12"></span>
                 <div class="form-group">
                    <div class="col-xs-6">
                       <label for="visa_mock_date_1">
                          <h5>VISA Mock Date 1</h5>
                       </label>
                       <input type="date" class="form-control" name="visa_mock_date_1" id="visa_mock_date_1" value="<?=$visa_detail && $visa_detail['mock_dates']['mock_date_1'] ? $visa_detail['mock_dates']['mock_date_1'] : ''?>">
                    </div>
                 </div>
                 <div class="form-group">
                    <div class="col-xs-6">
                       <label for="visa_mock_slot_1">
                          <h5>Visa Mock Slot 1</h5>
                       </label>
                       <select name="visa_mock_slot_1" id="visa_mock_slot_1" class="form-control">
                           <option value="10AM_to_2PM" <?=$visa_detail && $visa_detail['mock_dates']['mock_slot_1'] == '10AM_to_2PM' ? 'selected' : ''?>>10AM to 2PM</option>
                           <option value="3PM_to_7PM" <?=$visa_detail && $visa_detail['mock_dates']['mock_slot_1'] == '3PM_to_7PM' ? 'selected' : ''?>>3PM to 7PM</option>
                      </select>
                    </div>
                 </div>
                 <div class="form-group">
                    <div class="col-xs-6">
                       <label for="visa_mock_date_2">
                          <h5>VISA Mock Date 2</h5>
                       </label>
                       <input type="date" class="form-control" name="visa_mock_date_2" id="visa_mock_date_2" value="<?=$visa_detail && $visa_detail['mock_dates']['mock_date_2'] ? $visa_detail['mock_dates']['mock_date_2'] : ''?>">
                    </div>
                 </div>
                 <div class="form-group">
                    <div class="col-xs-6">
                       <label for="visa_mock_slot_2">
                          <h5>Visa Mock Slot 2</h5>
                       </label>
                       <select name="visa_mock_slot_2" id="visa_mock_slot_2" class="form-control">
                           <option value="10AM_to_2PM" <?=$visa_detail && $visa_detail['mock_dates']['mock_slot_2'] == '10AM_to_2PM' ? 'selected' : ''?>>10AM to 2PM</option>
                           <option value="3PM_to_7PM" <?=$visa_detail && $visa_detail['mock_dates']['mock_slot_2'] == '3PM_to_7PM' ? 'selected' : ''?>>3PM to 7PM</option>
                      </select>
                    </div>
                 </div>
                 <div class="form-group">
                    <div class="col-xs-6">
                       <label for="visa_mock_date_3">
                          <h5>VISA Mock Date 3</h5>
                       </label>
                       <input type="date" class="form-control" name="visa_mock_date_3" id="visa_mock_date_3" value="<?=$visa_detail && $visa_detail['mock_dates']['mock_date_3'] ? $visa_detail['mock_dates']['mock_date_3'] : ''?>">
                    </div>
                 </div>
                 <div class="form-group">
                    <div class="col-xs-6">
                       <label for="visa_mock_slot_3">
                          <h5>Visa Mock Slot 3</h5>
                       </label>
                       <select name="visa_mock_slot_3" id="visa_mock_slot_3" class="form-control">
                           <option value="10AM_to_2PM" <?=$visa_detail && $visa_detail['mock_dates']['mock_slot_3'] == '10AM_to_2PM' ? 'selected' : ''?>>10AM to 2PM</option>
                           <option value="3PM_to_7PM" <?=$visa_detail && $visa_detail['mock_dates']['mock_slot_3'] == '3PM_to_7PM' ? 'selected' : ''?>>3PM to 7PM</option>
                      </select>
                    </div>
                 </div>
                 <span class="sep col-lg-12"></span>

                 <div class="form-group">
                    <div class="col-xs-6">
                       <label for="visa_status">
                          <h5>Visa Status</h5>
                       </label>
                       <select name="visa_status" id="visa_status" class="form-control">
                           <option value="VISA_PROCESS_FINANCIAL_DOCUMENTS" <?=$visa_detail && $visa_detail['status'] == "VISA_PROCESS_FINANCIAL_DOCUMENTS" ? 'selected' : ''?>>In Process</option>
                           <option value="VISA_APPLIED" <?=$visa_detail && $visa_detail['status'] == "VISA_APPLIED" ? 'selected' : ''?>>Applied</option>
                           <option value="VISA_APPROVED" <?=$visa_detail && $visa_detail['status'] == "VISA_APPROVED" ? 'selected' : ''?>>Approved</option>
                           <option value="VISA_REJECTED" <?=$visa_detail && $visa_detail['status'] == "VISA_REJECTED" ? 'selected' : ''?>>Rejected</option>
                           <option value="VISA_APPROVED_OFFER" <?=$visa_detail && $visa_detail['status'] == "VISA_APPROVED_OFFER" ? 'selected' : ''?>>Approved With Offer</option>
                      </select>
                    </div>
                 </div>
                 <div class="form-group">
                    <div class="col-xs-12">
                       <br>
                       <button class="btn btn-lg prplButton pull-right" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                    </div>
                 </div>
                 <input type="hidden" name="application_id" value="<?=$application_id?>" />
                 <input type="hidden" name="university_id" value="<?=$university_id?>" />
                 <input type="hidden" name="user_id" value="<?=$id?>" />
             </form>
         </div>

             <div class="tab-pane" id="document_upload">
                <hr>
                <form class="form" action="/user/account/document" method="post" id="document-upload-form" enctype="multipart/form-data">
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="document_type">
                            <h5>Select Document</h5>
                         </label>
                         <select class="form-control" id="document_type" name="document_type">
                             <?php
                             foreach($documentTypes as $documentType)
                             {
                                 ?>
                                 <option value="<?php echo $documentType['id'];?>" <?php  if($documentType['id']==$selected_country) echo 'selected="selected"'; ?> ><?php echo $documentType['name'];?></option>
                                  <?php
                              }
                              ?>
                         </select>
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="document">
                            <h5>Upload File</h5>
                         </label>
                         <input class="form-control" type="file" name="document" id="document">
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="col-xs-6">
                         <label for="documents_list">
                            <h5>Uploaded Documents</h5>
                         </label>
                         <table>
                             <thead>
                                 <tr>
                                     <th>Type</th>
                                     <th>Name</th>
                                     <th>URL</th>
                                 </tr>
                             </thead>
                             <tbody>
                                 <?php
                                 foreach($uploadedDocuments as $index => $uploadedDocument)
                                 {
                                     ?>
                                     <tr>
                                         <td><?=$uploadedDocument['document_type']?></td>
                                         <td><?=$uploadedDocument['name']?></td>
                                         <td><a href="<?=$uploadedDocument['url']?>" target="_blank" style="color:#3a66ad"><?=$uploadedDocument['url']?></a></td>
                                     </tr>
                                     <?php
                                 }
                                 ?>
                             </tbody>
                         </table>
                      </div>
                   </div>

                   <div class="form-group">
                      <div class="col-xs-12">
                         <br>
                         <button class="btn btn-lg prplButton pull-right" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                         <!--<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>-->
                      </div>
                   </div>
                </form>
             </div>
          </div>
          <!--/tab-pane-->
        </div>
   </div>
   <!--/tab-content-->
</div>
<!--/col-9-->
</div>
<script type="text/javascript">
   $(document).ready(function() {


   var readURL = function(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();

           reader.onload = function (e) {
               $('.avatar').attr('src', e.target.result);
           }

           reader.readAsDataURL(input.files[0]);
       }
   }


   $(".file-upload").on('change', function(){
       readURL(this);
   });
   });

   $(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#example thead tr').clone(true).appendTo( '#example thead' );
    $('#example thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );

        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    var table = $('#example').DataTable( {
        orderCellsTop: true
    } );
} );

    $('#student_locationcountries').on('change', function() {
        var id = $(this).find(":selected").val();
        var dataSet = 'id=' + id;

        $.ajax({
            type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url 		: '/user/account/getStates', // the url where we want to POST
            data 		: dataSet, // our data object
            success: function (data1) {
                $('#student_state').html(data1);
            }
        });
    });

    $('#student_state').on('change', function() {
        var id = $(this).find(":selected").val();
        var dataSet2 = 'id=' + id;
        $.ajax({
            type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url 		: '/user/account/getCities', // the url where we want to POST
            data 		: dataSet2, // our data object
            success: function (data2) {
                $('#student_city').html(data2);
            }
        });
    });

    $('input[type=radio][name=diploma_12]').change(function() {
        if (this.value == '1') {
            $('.diploma_hs').hide();
            $('#diploma_id').show();
        }

        else if (this.value == '2') {
            $('.diploma_hs').hide();
            $('#12thidd').show();
        }
    });

    $('input[type=radio][name=competitive_exam]').change(function() {
        if(this.value == 'NO' || this.value == 'NOT_REQUIRED'){
            $('#exam_stage').hide();
            $('#result_date').hide();
            $('#exam_date').hide();
            $('#exam_score').hide();
        }
        else{
            $('#exam_stage').show();
            if($('input[type=radio][name=exam_stage]:checked').val() == 'REGISTERED'){
                $('#result_date').hide();
                $('#exam_score').hide();
                $('#exam_date').show();
            }
            else if($('input[type=radio][name=exam_stage]:checked').val() == 'RESULT_WAIT'){
                $('#exam_date').hide();
                $('#exam_score').hide();
                $('#result_date').show();
            }
            else if($('input[type=radio][name=exam_stage]:checked').val() == 'RESULT_OUT'){
                $('#result_date').hide();
                $('#exam_date').hide();
                $('#exam_score').show();
                if($('input[type=radio][name=competitive_exam]:checked').val() == 'GRE'){
                    $('#exam_score_gmat').hide();
                    $('#exam_score_sat').hide();
                    $('#exam_score_gre').show();
                }
                else if($('input[type=radio][name=competitive_exam]:checked').val() == 'GMAT'){
                    $('#exam_score_sat').hide();
                    $('#exam_score_gre').hide();
                    $('#exam_score_gmat').show();
                }
                else if($('input[type=radio][name=competitive_exam]:checked').val() == 'SAT'){
                    $('#exam_score_gmat').hide();
                    $('#exam_score_gre').hide();
                    $('#exam_score_sat').show();
                }
            }
        }
    });

    $('input[type=radio][name=exam_stage]').change(function() {

        if (this.value == 'REGISTERED') {
            $('#result_date').hide();
            $('#exam_score').hide();
            $('#exam_date').show();
        }

        else if (this.value == 'RESULT_WAIT') {
            $('#exam_date').hide();
            $('#exam_score').hide();
            $('#result_date').show();
        }
        else if (this.value == 'RESULT_OUT') {
            $('#result_date').hide();
            $('#exam_date').hide();
            $('#exam_score').show();
            if($('input[type=radio][name=competitive_exam]:checked').val() == 'GRE'){
                $('#exam_score_gmat').hide();
                $('#exam_score_sat').hide();
                $('#exam_score_gre').show();
            }
            else if($('input[type=radio][name=competitive_exam]:checked').val() == 'GMAT'){
                $('#exam_score_sat').hide();
                $('#exam_score_gre').hide();
                $('#exam_score_gmat').show();
            }
            else if($('input[type=radio][name=competitive_exam]:checked').val() == 'SAT'){
                $('#exam_score_gmat').hide();
                $('#exam_score_gre').hide();
                $('#exam_score_sat').show();
            }

        }

    });

    /*$('input[type=radio][name=english_proficiency_exam]').change(function() {
        if(this.value == 'NO' || this.value == 'NOT_REQUIRED'){
            $('#english_exam_stage').hide();
        }
        else{
            $('#english_exam_stage').show();
        }
    });*/

    $('input[type=radio][name=english_proficiency_exam]').change(function() {
        if(this.value == 'NO' || this.value == 'NOT_REQUIRED'){
            $('#english_exam_stage').hide();
            $('#english_result_date').hide();
            $('#english_exam_date').hide();
            $('#english_exam_score').hide();
        }
        else{
            $('#english_exam_stage').show();
            if($('input[type=radio][name=english_exam_stage]:checked').val() == 'REGISTERED'){
                $('#english_result_date').hide();
                $('#english_exam_score').hide();
                $('#english_exam_date').show();
            }
            else if($('input[type=radio][name=english_exam_stage]:checked').val() == 'RESULT_WAIT'){
                $('#english_exam_date').hide();
                $('#english_exam_score').hide();
                $('#english_result_date').show();
            }
            else if($('input[type=radio][name=english_exam_stage]:checked').val() == 'RESULT_OUT'){
                $('#english_result_date').hide();
                $('#english_exam_date').hide();
                $('#english_exam_score').show();
            }
        }
    });

    $('input[type=radio][name=english_exam_stage]').change(function() {

        if (this.value == 'REGISTERED') {
            $('#english_result_date').hide();
            $('#english_exam_score').hide();
            $('#english_exam_date').show();
        }

        else if (this.value == 'RESULT_WAIT') {
            $('#english_exam_date').hide();
            $('#english_exam_score').hide();
            $('#english_result_date').show();
        }
        else if (this.value == 'RESULT_OUT') {
            $('#english_result_date').hide();
            $('#english_exam_date').hide();
            $('#english_exam_score').show();
        }

    });

    $('input[type=radio][name=masters]').change(function() {
        if (this.value == '1') {
            $('#masters_id').show();
        }
        else if (this.value == '2') {
            $('#masters_id').hide();
        }
    });

    function showNoOfWords(){
        document.getElementById("sop-no-of-words").style.display = "block";
    }
    function hideNoOfWords(){
        document.getElementById("sop-no-of-words").style.display = "none";
    }
    function showLOR(){
        document.getElementById("lor-required").style.display = "block";
        document.getElementById("lor-type").style.display = "block";
    }
    function hideLOR(){
        document.getElementById("lor-required").style.display = "none";
        document.getElementById("lor-type").style.display = "none";
    }
</script>
