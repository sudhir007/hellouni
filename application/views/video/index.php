<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width" />
        <title>WebRTC Video Demo</title>
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="/assets/css/style.css">
    </head>

    <body>
        <div class="main">
            <div class="main__left">
                <div class="main__videos">
                    <div id="video-grid"></div>
                </div>
                <div class="main__controls">
                    <div class="main__controls__block">
                        <div onclick="muteUnmute()" class="main__controls__button main__mute_button">
                            <i class="fas fa-microphone"></i>
                            <span>Mute</span>
                        </div>
                        <div onclick="playStop()" class="main__controls__button main__video_button" >
                            <i class="fas fa-video"></i>
                            <span>Stop Video</span>
                        </div>
                    </div>
                    <div class="main__controls__block">
                        <div class="main__controls__button">
                            <i class="fas fa-shield-alt"></i>
                            <span>Security</span>
                        </div>
                        <div class="main__controls__button">
                            <i class="fas fa-user-friends"></i>
                            <span>Participants</span>
                        </div>
                        <div class="main__controls__button">
                            <i class="fas fa-comment-alt"></i>
                            <span>Chat</span>
                        </div>
                    </div>
                    <div class="main__controls__block">
                        <div class="main__controls__button">
                            <span class="leave_meeting">Leave Meeting</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main__right">
                <div class="main__header">
                    <h6>Chat</h6>
                </div>
                <div class="main__chat_window">
                    <ul class="messages"></ul>
                </div>
                <div class="main__message_container">
                    <input id="chat_message" type="text" placeholder="Type message here...">
                </div>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="/assets/js/peerjs.min.js"></script>
        <script src="/assets/js/popper.min.js"></script>
        <script src="/assets/js/socket.io.js"></script>
        <script src="/assets/js/c939d0e917.js"></script>
        <script src="/assets/js/video.js"></script>

        <script>
            const ROOM_ID = "<?=$room_id?>";
        </script>
    </body>
</html>
