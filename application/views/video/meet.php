<!DOCTYPE html>
<html lang="en" class="h-100">
    <head>
        <meta name="viewport" content="width=device-width" />
        <title>Joining Form</title>
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="/assets/css/style.css">
    </head>

    <body class="h-100">
        <div class="container h-100">
            <div class="row h-100 justify-content-center align-items-center">
                <div class="col-10 col-md-8 col-lg-6">
                    <div id="meet"></div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="https://meeting.duoex.com/external_api.js"></script>
        <script type="text/javascript">
            const domain = 'meeting.duoex.com';
            const options = {
                roomName: 'JitsiMeetAPIExample',
                width: 700,
                height: 700,
                parentNode: document.querySelector('#meet')
            };
            const api = new JitsiMeetExternalAPI(domain, options);
        </script>
    </body>
</html>
