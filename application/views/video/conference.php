<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width" />
        <title>WebRTC Video Demo</title>
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="/assets/css/style.css">
    </head>

    <body>
        <div class="main">
            <div class="main__left">
                <div class="main__videos">
                    <div id="video-grid"></div>
                </div>
                <div class="main__controls">
                    <div class="main__controls__block">
                        <div id="btnMuteUnmute" class="main__controls__button main__mute_button">
                            <i class="fas fa-microphone"></i>
                            <span>Mute</span>
                        </div>
                        <div id="btnStartStopCam" class="main__controls__button main__video_button" >
                            <i class="fas fa-video"></i>
                            <span>Stop Video</span>
                        </div>
                    </div>
                    <div class="main__controls__block">
                        <div class="main__controls__button">
                            <i class="fas fa-shield-alt"></i>
                            <span>Security</span>
                        </div>
                        <div class="main__controls__button">
                            <i class="fas fa-user-friends"></i>
                            <span>Participants</span>
                        </div>
                        <div class="main__controls__button">
                            <i class="fas fa-comment-alt"></i>
                            <span>Chat</span>
                        </div>
                        <div id="btnStartStopScreenshare" class="main__controls__button main__screen_button">
                            <i class="fa fa-desktop"></i>
                            <span>Screen Share</span>
                        </div>
                        <div id="btnStartReco" class="main__controls__button main__record_button">
                            <i class="fas fa-record-vinyl"></i>
                            <span>Record</span>
                        </div>
                        <div id="btnStopReco" class="main__controls__button main__record_button" style="display:none;">
                            <i class="fas fa-record-vinyl"></i>
                            <span>Stop</span>
                        </div>
                    </div>
                    <div class="main__controls__block">
                        <div class="main__controls__button">
                            <span class="leave_meeting">Leave Meeting</span>
                        </div>
                    </div>
                    <div class="main__controls__block">
                        <div class="main__controls__button">
                            <a href="" id="downloadRecording">Download</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main__right">
                <div class="main__header">
                    <h6>Chat</h6>
                </div>
                <div class="main__chat_window">
                    <ul class="messages"></ul>
                </div>
                <div class="main__message_container">
                    <input id="chat_message" type="text" placeholder="Type message here...">
                </div>
            </div>
        </div>

        <canvas id="canvas-grid"></canvas>

        <script src="/assets/js/jquery-3.4.1.min.js"></script>
        <script src="/assets/js/peerjs.min.js"></script>
        <script src="/assets/js/popper.min.js"></script>
        <script src="/assets/js/socket.io.js"></script>
        <script src="/assets/js/c939d0e917.js"></script>
        <script src="/assets/js/vm.js"></script>
        <script src="/assets/js/bundle.min.js"></script>

        <script>
            const ROOM_ID = "<?=$room_id?>";
            const USER_NAME = "<?=$user_name?>";
            $(function () {
                Conference.init();
            });
        </script>
    </body>
</html>
