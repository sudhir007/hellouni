
<!DOCTYPE html>

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>HelloUni Webinar</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="https://hellouni.org/admin/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://hellouni.org/admin/bootstrap/css/bootstrap.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="https://hellouni.org/admin/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="https://hellouni.org/admin/plugins/iCheck/square/blue.css">
	<style type="text/css">
	.error{color:red;}

	</style>

<link rel="icon" type="image/png" sizes="96x96" href="https://hellouni.org/admin/images/favicon-96x96.png">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page" style="background-color: white;">
    <div class="login-box">
      <div class="login-logo">
	  <img src="https://hellouni.org/admin/images/HelloUni-Logo.png" style="width: 75%;">
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <form action="/test/load" method="get">
          <div class="form-group has-feedback" >
            <img src="<?php echo base_url();?>application/images/thankYou.jpg" alt="thankYou" width="100%" height="100%">
          </div>
        </form>


      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="https://hellouni.org/admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="https://hellouni.org/admin/bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="https://hellouni.org/admin/plugins/iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>

	<script type="text/javascript">
	$( document ).ready(function() {

	 $( "#signin" ).click(function(e) {
       var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	   var username 	= $( "#username" ).val();
	   var password = $( "#password" ).val();

	   if(username==''){

	   $( "#emailerror" ).text("Username is Required"); return false; e.stoppropagation();

	   }else if(password==''){

	    $( "#emailerror" ).text("");
		$( "#passworderror" ).text("Password is Empty"); return false; e.stoppropagation();
	   }


     });

      });
	</script>
  </body>
