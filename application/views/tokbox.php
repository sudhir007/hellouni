<html>
    <head>
        <title> OpenTok Getting Started </title>
        <script src="https://static.opentok.com/v2/js/opentok.min.js"></script>
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <!-- <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"> -->
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <style>
            body, html {
                background-color: gray;
                height: 100%;
            }

            .OT_root {
                padding: 10px;
                float:left;
                display:inline;
            }

            #videos {
                position: relative;
                width: 75%;
                height: 100%;
                margin-left: auto;
                margin-right: auto;
                float: left;
            }

            #subscriber {
                position: absolute;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                z-index: 10;
            }

            #publisher {
                width: 100%;
                height: 100%;
                bottom: 10px;
                left: 10px;
                z-index: 100;
            }

         /*   #textchat {
                
                position: relative;
                width: 20%;
                float: right;
                right: 0;
                height: 100%;
                background-color: #333;
            }*/

            #history {
                width: 100%;
                height: calc(100% - 40px);
                overflow: auto;
                background-color: #d4d4d4;
            }

            input#msgTxt {
                /*height: 40px;
                position: absolute;
                bottom: 0;
                width: 100%;*/
                    border-radius: 7px;
                    margin-bottom: 8px;
                    height: 40px;
                    position: absolute;
                    bottom: 0px;
                    width: 93%;
                    padding: 5px;

            }

            #history .mine {
                padding: 10px 8px;
                height: auto;
                color: #0c0c0c;
                /* text-align: right; */
                margin: 10px 6px;
                background-color: white;
                border-radius: 7px;
            }

            #history .theirs {
                color: #00FFFF;
                margin-left: 10px;
            }

           /* #buttonholder{
                position: absolute;
                width: 70%;
                margin-left: auto;
                margin-right: auto;
                bottom: 2px;
                left: 20px;
                z-index: 200;
            }*/

            .OT_video{
                right: 0;
                height: 20px;
                width: 117px;
                display: block;
                cursor: pointer;
                position: absolute;
                margin-right: 60px;
                margin-top: 8px;
                z-index: 1000 !important;
            }

            /*.OT_publisher .OT_edge-bar-item.OT_mode-auto{
                top: 0px;
                opacity: 1;
            }*/
            .attendeesListModal, .pollModal {
               display: none; /* Hidden by default */
               position: fixed; /* Stay in place */
               z-index: 1; /* Sit on top */
               padding-top: 100px; /* Location of the box */
               left: 0;
               top: 0;
               width: 100%; /* Full width */
               height: 100%; /* Full height */
               overflow: auto; /* Enable scroll if needed */
               background-color: rgb(0,0,0); /* Fallback color */
               background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
                z-index: 100;
               }

            .attendeesList-content, .pollModal-content  {
               background-color: #fefefe;
               margin: auto;
               padding: 20px;
               border: 1px solid #888;
               width: 80%;
               }
               /* The Close Button */
               .listclose, .pollclose {
               color: #8e0f0f;
               float: right;
               font-size: 28px;
               font-weight: bold;
               }

                .listclose:hover,
               .listclose:focus {
               color: #000;
               text-decoration: none;
               cursor: pointer;
               }

                .pollclose:hover,
               .pollclose:focus {
               color: #000;
               text-decoration: none;
               cursor: pointer;
               }

                .box{
                    /*float:left;*/
                    /*overflow: hidden;*/
                    /*background: #ff5d65;*/
                    display: none;
                    position: fixed;
                    width: 20%;
                    /*float: right;*/
                    right: 0;
                    bottom: 0;
                    height: 97%;
                    background-color: #333;

                }
                /* Add padding and border to inner content
                for better animation effect */
                .box-inner{
                    width: auto;
                    padding: 10px; color:#fff;
                  }
                 
                  .fa-fw {width: 2em;}

                  /*Table Attendees*/
                  .panel {
                        border: 1px solid #ddd;
                        background-color: #fcfcfc;
                    }
                    .panel .btn-group {
                        margin: 15px 0 30px;
                    }
                    .panel .btn-group .btn {
                        transition: background-color .3s ease;
                    }

                    /*table button*/
                    .AVButton{
                        font-size: 20px;
                        margin: 1px 12px 1px 5px;
                        color: darkblue;
                    }

                    /*Poll Quiz*/
                   #quiz {
  margin: -44px 50px 0px;
  position: relative;
  width: calc(100% - 100px);
}

/*#quiz h1 {
  color: #FAFAFA;
  font-weight: 600;
  font-size: 36px;
  text-transform: uppercase;
  text-align: left;
  line-height: 44px;
}*/

#quiz button {
    /* float: left; */
    margin: 8px 0px 0px 8px;
    /* padding: 4px 8px; */
    /* background: #9ACFCC; */
    /* color: #00403C; */
    /* font-size: 14px; */
    cursor: pointer;
    /* outline: none; */
}

/*#quiz button:hover {
  background: #36a39c;
  color: #FFF;
}*/

#quiz button:disabled {
  opacity: 0.5;
/*\*/
  cursor: default;
}

#question {
  padding: 20px;
  background: #FAFAFA;
  text-align: center;
}

#question h2 {
  margin-bottom: 16px;
  font-weight: 600;
  font-size: 20px;
}

#question input[type=radio] {
  display: none;
}

#question label {
    display: inline-block;
    margin: 10px;
    padding: 8px;
    background: #ffffff;
    color: #4C3000;
    width: calc(50% - 8px);
    min-width: 50px;
    cursor: pointer;
    letter-spacing: 0.5px;
    border-radius: 6px;
    box-shadow: 0px 2px 2px #dedada;
}

#question label:hover {
  background: #347ab7;
  color: white;
}

#question input[type=radio]:checked + label {
  background: #5bb85b;
  color: white;
}

#quiz-results {
  display: flex;
  flex-direction: column;
  justify-content: center;
  position: absolute;
  /*top: 44px;*/
  top: 5px;
  left: 0px;
  background: #FAFAFA;
  width: 100%;
  /*height: calc(100% - 44px);*/
      height: calc(100% - 2px);
}

#quiz-results-message {
  display: block;
  color: #00403C;
  font-size: 20px;
  font-weight: bold;
}

#quiz-results-score {
  display: block;
  color: #31706c;
  font-size: 20px;
}

#quiz-results-score b {
  color: #00403C;
  font-weight: 600;
  font-size: 20px;
}

#quiz-retry-button {

  float: left;
  margin: 8px 0px 0px 8px;
  padding: 4px 8px;
  background: #9ACFCC;
  color: #00403C;
  font-size: 14px;
  cursor: pointer;
  outline: none;
  
}
                  
        </style>
    </head>
    <body>
        <div id="videos">
            <div id="subscriber"></div>
            <div id="publisher"></div>
            <div id="screen-preview"></div>
        </div>
    <?php
    if($role != 'subscriber'){
        ?>
            <!-- <div id="buttonholder">
                <div id="buttons">
                    <button type="button" id="start" onClick="javascript:startArchive()" class="btn btn-md btn-primary">Start Archive</button>
                    <button type="button" id="stop" onClick="javascript:stopArchive()" class="btn btn-danger">Stop Archive</button>
                    <button type="button" id="view" onClick="javascript:viewArchive()" class="btn btn-md btn-info" disabled>View Archive</button>
                    <button type="button" id="screen" onClick="javascript:shareScreen()" class="btn btn-md btn-primary">Share Screen</button>
                    <button type="button" onclick="openListPopup()" class="btn btn-md btn-success">Show attendees </button>
                     <button type="button" class="slide-toggle btn btn-md btn-success" >Questions</button>
                </div>
            </div> -->
          
                 <div class="col-md-12 col-sm-12 panel-group" id="accordion1" style="float: left;position: absolute;background: cornsilk;padding-left: 0px;padding-right: 0px; z-index: 110;">
                     <div class="panel panel-primary">
                        <div class="panel-heading col-md-12" style="padding: 5px 0px;background-color: black;">
                            <div class="col-md-2 float-left" >
                                <a data-toggle="collapse" data-parent="#accordion1" data-target="#collapseOne" href="#" aria-expanded="true" aria-controls="collapseOne" style="color: white;">MENU</a>
                            </div>
                           <div class="col-md-8" style="text-align: center;">
                           <span>Talking : Mr. ABC PQR</span>
                           </div>
                           <div class="col-md-2" style="text-align: right;">
                           <span class="float-right">
                            <a href="javascript:void" id="screenView" style="color: white;"><i class="fas fa-arrows-alt">&nbsp;</i></a>
                            <a href="javascript:void" id="exit" style="color: white;"><i class="fas fa-times-circle">&nbsp;</i></a>
                           </span>
                          <!--  <span class="float-right"><i class="fas fa-arrows-alt"> &nbsp;&nbsp;</i></span>
                            <span class="float-right"><i class="fas fa-times-circle"></i></span> -->
                           </div>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse col-md-12">
                           <div class="panel-body" style="padding: 5px 5px 5px 5px;">
                           
                                <div id="buttons">
                                    <button type="button" id="start" onClick="javascript:startArchive()" class="btn btn-sm btn-primary"><i class="fas fa-plus-circle">&nbsp;</i><b>Start Archive</b></button>
                                    <button type="button" id="stop" onClick="javascript:stopArchive()" class="btn btn-sm btn-danger"><i class="fas fa-minus-circle">&nbsp;</i><b>Stop Archive</b></button>
                                    <button type="button" id="view" onClick="javascript:viewArchive()" class="btn btn-sm btn-info" disabled><i class="fas fa-file-archive">&nbsp;</i><b>View Archive</b></button>
                                    <button type="button" id="screen" onClick="javascript:shareScreen()" class="btn btn-sm btn-primary"><i class="fas fa-chalkboard-teacher">&nbsp;</i><b>Share Screen</b></button>
                                    <button type="button" onclick="openListPopup()" class="btn btn-sm btn-success"><i class="fas fa-users">&nbsp;</i><b>Show attendees</b> </button>
                                     <button type="button" class="slide-toggle btn btn-sm btn-info" ><i class="fas fa-comments">&nbsp;</i><b>Ask</b></button>
                                    <button type="button" onclick="openPollPopup()" class="btn btn-sm btn-success"><i class="fas fa-poll">&nbsp;</i><b>Polling</b> </button>
                                </div>
                         
                           </div>
                        </div>
                     </div>
                  </div>
             
            
        <?php
    }
    ?> 
       
        <div class="box">
            <div class="box-inner">
                 <div id="textchat">
                    <p id="history"></p>
                    <form style="color: black;">
                        <input type="text" placeholder="Input your text here" id="msgTxt"></input>
                        <input type="hidden" id="username" value="<?=$username?>"></input>
                    </form>
                </div>
            </div>
        </div>
         <div id="attendeesList" class="attendeesListModal" >
              <!-- Modal content -->
              <div class="row attendeesList-content">
                 <span class="listclose" id="closeModal">&times;</span>
                 <!-- <button type="button" onclick="closeListPopup()" class="btn btn-danger">close </button> -->
                  <div class="col-md-12 content">
                 
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="pull-right">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-success btn-filter" data-target="Publisher">Publisher</button>
                                            <button type="button" class="btn btn-warning btn-filter" data-target="Organizer">Organizer</button>
                                            <button type="button" class="btn btn-danger btn-filter" data-target="Subscriber">Subscriber</button>
                                            <button type="button" class="btn btn-default btn-filter" data-target="all">All</button>
                                        </div>
                                    </div>
                                    <div class="table-container">
                                        <table class="table table-filter">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Name</th>
                                                    <th>Type</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr data-status="Publisher">
                                                    <td>1</td>
                                                    <td>Nikhil</td>
                                                    <td>Publisher</td>
                                                    <td class="text-center"><a href="javascript:void" id="videoOff" ><i class="AVButton fas fa-video">&nbsp;</i></a> <a href="javascript:void" id="audioOff" ><i class="AVButton fas fa-microphone">&nbsp;</i></a> </td>
                                                    
                                                </tr>
                                                <tr data-status="Organizer">
                                                    <td>4</td>
                                                    <td>Nikhil</td>
                                                    <td>Organizer</td>
                                                    <td class="text-center"><a href="javascript:void" id="videoOff" ><i class="AVButton fas fa-video">&nbsp;</i></a> <a href="javascript:void" id="audioOff" ><i class="AVButton fas fa-microphone">&nbsp;</i></a></td>
                                                </tr>
                                                <tr data-status="Subscriber">
                                                    <td>2</td>
                                                    <td>Sudhir</td>
                                                    <td>Subscriber</td>
                                                    <td class="text-center"><a href="javascript:void" id="videoOff" ><i class="AVButton fas fa-video">&nbsp;</i></a> <a href="javascript:void" id="audioOff" ><i class="AVButton fas fa-microphone">&nbsp;</i></a></td>
                                                </tr>
                                                <tr data-status="Subscriber" class="selected">
                                                   <td>3</td>
                                                    <td>Deepika</td>
                                                    <td>Subscriber</td>
                                                    <td class="text-center"><a href="javascript:void" id="videoOff" ><i class="AVButton fas fa-video">&nbsp;</i></a> <a href="javascript:void" id="audioOff" ><i class="AVButton fas fa-microphone">&nbsp;</i></a></td>
                                                </tr>
                                                <tr data-status="Organizer">
                                                    <td>5</td>
                                                    <td>Banit</td>
                                                    <td>Organizer</td>
                                                    <td class="text-center"><a href="javascript:void" id="videoOff" ><i class="AVButton fas fa-video">&nbsp;</i></a> <a href="javascript:void" id="audioOff" ><i class="AVButton fas fa-microphone">&nbsp;</i></a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                          
                       
                 </div>
              </div>
        </div>

         <div id="pollId" class="pollModal" >
              <!-- Modal content -->
              <div class="row pollModal-content">
                 <span class="pollclose" id="pollcloseModal">&times;</span>
                    <div id="quiz">
                      <h1 id="quiz-name"></h1>
                      <button id="submit-button" class="btn btn-md btn-primary">Submit Answers</button>
                      <button id="next-question-button" class="btn btn-md btn-primary">Next Question</button>
                      <button id="prev-question-button" class="btn btn-md btn-primary">Previous</button>
                      
                      <div id="quiz-results">
                        
                        <p id="quiz-results-message"></p>
                        <p id="quiz-results-score"></p>
                        <!-- <button id="quiz-retry-button">Retry</button> -->
                        
                      </div>
                    </div>

                </div>
        </div>
        <!-- <div id="textchat">
            <p id="history"></p>
            <form>
                <input type="text" placeholder="Input your text here" id="msgTxt"></input>
                <input type="hidden" id="username" value="<?=$username?>"></input>
            </form>
        </div> -->

       

        <script type="text/javascript">
            
            var attendeesListModal = document.getElementById("attendeesList");
            var coursespan = document.getElementById("closeModal");

            var pollModal = document.getElementById("pollId");
            var pollspan = document.getElementById("pollcloseModal");

            function openListPopup() {
                // document.getElementById("course-list-modal").innerHTML = response;
                attendeesListModal.style.display = "block";
               }

                coursespan.onclick = function() {
               attendeesListModal.style.display = "none";
               }

               function closeListPopup() {
                // document.getElementById("course-list-modal").innerHTML = response;
                attendeesListModal.style.display = "none";
               }

             window.onclick = function(event) {
               if (event.target == attendeesListModal) {
                attendeesListModal.style.display = "none";
               }
               }

               function openPollPopup() {
                // document.getElementById("course-list-modal").innerHTML = response;
                pollModal.style.display = "block";
               }

                pollspan.onclick = function() {
               pollModal.style.display = "none";
               }

               function closeListPopup() {
                // document.getElementById("course-list-modal").innerHTML = response;
                pollModal.style.display = "none";
               }

             window.onclick = function(event) {
               if (event.target == pollModal) {
                pollModal.style.display = "none";
               }
               }

                $(document).ready(function(){
                    $(".slide-toggle").click(function(){
                        $(".box").animate({
                            width: "toggle"
                        });
                    });
                });

                $(document).ready(function () {

                    

                    $('.btn-filter').on('click', function () {
                      var $target = $(this).data('target');
                      if ($target != 'all') {
                        $('.table tr').css('display', 'none');
                        $('.table tr[data-status="' + $target + '"]').fadeIn('slow');
                      } else {
                        $('.table tr').css('display', 'none').fadeIn('slow');
                      }
                    });

                 

           
                $('#videoOff').click(function(){
                    $(this).find('i').toggleClass('fa-video fa-video-slash')
                });

                $('#audioOff').click(function(){
                    $(this).find('i').toggleClass('fa-microphone fa-microphone-slash')
                });

                
                $('#screenView').click(function(){
                    $(this).find('i').toggleClass('fa-arrows-alt fa-compress-arrows-alt')
                });
                
                });


                // Poll Quiz


        </script>

        <script type="text/javascript">
            var apiKey = "46220952";
            var sessionId = "<?=$sessionId?>";
            var token = "<?=$token?>";
            var tokboxId = "<?=$tokboxId?>";
            var session;
            var archiveID;
            var allConnections = {};
            var publisher;

            // Handling all of our errors here by alerting them
            function handleError(error) {
                if (error) {
                    //alert(error.message);
                }
            }

            function initializeSession() {
                session = OT.initSession(apiKey, sessionId);
                // Subscribe to a newly created stream
                /*session.on('streamCreated', function(event) {
                    if(event.stream.videoType == 'camera'){
                        session.subscribe(event.stream, 'subscriber', {
                            insertMode: 'append',
                            width: '360px',
                            height: '240px',
                            name: '<?=$username?>',
                        }, handleError);
                    }
                    if(event.stream.videoType == 'screen'){
                        session.subscribe(event.stream, 'subscriber', {
                            insertMode: 'append',
                            width: '100%',
                            height: '100%',
                            name: '<?=$username?>',
                        }, handleError);
                    }
                });*/

                // Create a publisher
                publisher = OT.initPublisher('publisher', {
                    insertMode: 'append',
                    width: '100%',
                    height: '100%',
                    name: '<?=$username?>',
                    style: {buttonDisplayMode: 'on', nameDisplayMode: "on"}
                    //videoSource: 'screen'
                }, handleError);

                //publisher.publishVideo(false);

                // Connect to the session
                session.connect(token, function(error) {
                    // If the connection is successful, publish to the session
                    if (error) {
                        handleError(error);
                    } else {
                        var node = document.createElement("button");
                        node.setAttribute("id", "video-button-" + publisher.id);
                        node.setAttribute("class", "OT_edge-bar-item OT_mode-on OT_video");
                        node.setAttribute("onClick", "disableVideo()")
                        document.getElementById(publisher.id).appendChild(node);
                        document.getElementById("video-button-" + publisher.id).innerHTML = "Disable Video";
                        session.publish(publisher, handleError);
                    }
                });

                var msgHistory = document.querySelector('#history');

                session.on('signal:msg', function signalCallback(event) {
                    var msg = document.createElement('p');
                    msg.textContent = event.data;
                    msg.className = event.from.connectionId === session.connection.connectionId ? 'mine' : 'theirs';
                    msgHistory.appendChild(msg);
                    msg.scrollIntoView();
                });

                session.on('archiveStarted', function archiveStarted(event) {
                archiveID = event.id;
                    console.log('Archive started ' + archiveID);
                    $('#stop').show();
                    $('#start').hide();
                });

                session.on('archiveStopped', function archiveStopped(event) {
                    archiveID = event.id;
                    console.log('Archive stopped ' + archiveID);
                    $('#start').hide();
                    $('#stop').hide();
                    $('#view').show();
                });

                session.on({
                    connectionCreated: function (event) {
                        console.log("Connection Created " , event);
                        //connectionCount++;
                        if (event.connection.connectionId != session.connection.connectionId) {
                            //console.log(event.connection.data + ' connected.');
                            if (session.capabilities.forceDisconnect == 1) {
                                var node = document.createElement("button");
                                node.setAttribute("id", event.connection.connectionId);
                                node.setAttribute("class", "btn btn-danger");
                                node.setAttribute("onClick", "disconnectUser('" + event.connection.connectionId + "')")
                                document.getElementById("buttons").appendChild(node);
                                document.getElementById(event.connection.connectionId).innerHTML = event.connection.data;
                                allConnections[event.connection.connectionId] = event.connection;
                            }
                            //resizeSubscriber();
                        }
                    },
                    connectionDestroyed: function connectionDestroyedHandler(event) {
                        //connectionCount--;
                        //console.log(event.connection.data + ' disconnected.');
                        if (event.connection.connectionId != session.connection.connectionId) {
                            if (session.capabilities.forceDisconnect == 1) {
                                var element = document.getElementById(event.connection.connectionId);
                                element.parentNode.removeChild(element);
                            }
                        }
                        else{
                            alert("You have been disconnected. Please refresh the page to join again");
                        }
                    },
                    streamDestroyed: function (event){
                        //console.log(publisher2);
                        console.log("Stream Destroyed ", event);
                        document.getElementById("subscriber").style.display = "block";
                        document.getElementById("publisher").style.display = "block";
                    },
                    streamCreated: function(event){
                        if(event.stream.videoType == 'camera'){
                            var subscriber = session.subscribe(event.stream, 'subscriber', {
                                insertMode: 'append',
                                width: '360px',
                                height: '240px',
                                name: '<?=$username?>',
                            }, handleError);

                            var SpeakerDetection = function(subscriber, startTalking, stopTalking) {
                                var activity = null;
                                subscriber.on('audioLevelUpdated', function(event) {
                                    var now = Date.now();
                                    if (event.audioLevel > 0.2) {
                                        if (!activity) {
                                            activity = {timestamp: now, talking: false};
                                        }
                                        else if (activity.talking) {
                                            activity.timestamp = now;
                                        }
                                        else if (now- activity.timestamp > 1000) {
                                            // detected audio activity for more than 1s
                                            // for the first time.
                                            activity.talking = true;
                                            if (typeof(startTalking) === 'function') {
                                                startTalking();
                                            }
                                        }
                                    }
                                    else if (activity && now - activity.timestamp > 3000) {
                                        // detected low audio activity for more than 3s
                                        if (activity.talking) {
                                            if (typeof(stopTalking) === 'function') {
                                                stopTalking();
                                            }
                                        }
                                        activity = null;
                                    }
                                });
                            };

                            SpeakerDetection(subscriber, function() {
                                console.log('Started Talking ', subscriber.id, subscriber);
                                document.getElementById(subscriber.id).style.background = "#2bc312";
                            }, function() {
                                document.getElementById(subscriber.id).style.background = "";
                                console.log('stopped talking');
                            });
                        }
                        if(event.stream.videoType == 'screen'){
                            document.getElementById("subscriber").style.display = "none";
                            document.getElementById("publisher").style.display = "none";
                            session.subscribe(event.stream, 'screen-preview', {
                                insertMode: 'append',
                                width: '100%',
                                height: '100%',
                                name: '<?=$username?>',
                            }, handleError);
                        }
                        console.log("Stream Created ", event);
                    }
                });

                function resizeSubscriber() {
                    publisher.element.style.width = "100px";
                    publisher.element.style.height = "75px";
                    document.getElementById("subscriber").appendChild(publisher.element);
                }
            }

            function disableVideo(){
                publisher.publishVideo(false);
                document.getElementById("video-button-" + publisher.id).setAttribute("onClick", "enableVideo('" + publisher + "')");
                document.getElementById("video-button-" + publisher.id).innerHTML = "Enable Video";
            }

            function enableVideo(){
                publisher.publishVideo(true);
                document.getElementById("video-button-" + publisher.id).setAttribute("onClick", "disableVideo('" + publisher + "')");
                document.getElementById("video-button-" + publisher.id).innerHTML = "Disable Video";
            }

            function disconnectUser(connectionId){
                if (session.capabilities.forceDisconnect == 1) {
                    alert(connectionId);
                    session.forceDisconnect(allConnections[connectionId]);
                } else {
                    alert("No permission");
                }
            }

            function shareScreen(){
                OT.checkScreenSharingCapability(function(response) {
                    if(!response.supported || response.extensionRegistered === false) {
                        // This browser does not support screen sharing.
                    } else if (response.extensionInstalled === false) {
                        // Prompt to install the extension.
                    } else {
                        // Create a publisher for screen
                        var publisherScreen = OT.initPublisher('screen-preview', {
                            insertMode: 'append',
                            width: '100%',
                            height: '100%',
                            name: '<?=$username?>',
                            style: {buttonDisplayMode: 'on', nameDisplayMode: "on"},
                            videoSource: 'screen'
                        }, handleError);

                        document.getElementById("publisher").style.display = "none";
                        document.getElementById("subscriber").style.display = "none";
                        session.publish(publisherScreen, function(error) {
                            if (error) {
                                // Look error.message to see what went wrong.
                            }
                        });

                        publisherScreen.on('streamDestroyed', function(event) {
                            console.log(event);
                            document.getElementById("publisher").style.display = "block";
                            document.getElementById("subscriber").style.display = "block";
                            if (event.reason === 'mediaStopped') {
                                // User clicked stop sharing
                            } else if (event.reason === 'forceUnpublished') {
                                // A moderator forced the user to stop sharing.
                            }
                        });
                    }
                });
            }

            // Text chat
            var form = document.querySelector('form');
            var msgTxt = document.querySelector('#msgTxt');
            var username = document.querySelector('#username');

            // Send a signal once the user enters data in the form
            form.addEventListener('submit', function submit(event) {
            event.preventDefault();

            session.signal({
                type: 'msg',
                data: username.value + ': ' + msgTxt.value
            }, function signalCallback(error) {
                if (error) {
                    console.error('Error sending signal:', error.name, error.message);
                } else {
                    msgTxt.value = '';
                }
            });
            });
            // Start recording
            function startArchive() { // eslint-disable-line no-unused-vars
                $.ajax({
                    url: '/tokbox/startArchive',
                    type: 'POST',
                    contentType: 'application/json', // send as JSON
                    data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId}),
                    complete: function complete() {
                        // called when complete
                        console.log('startArchive() complete');
                    },
                    success: function success() {
                        // called when successful
                        console.log('successfully called startArchive()');
                    },
                    error: function error() {
                        // called when there is an error
                        console.log('error calling startArchive()');
                    }
                });

                $('#start').hide();
                $('#stop').show();
            }

            // Stop recording
            function stopArchive() { // eslint-disable-line no-unused-vars
                $.post('/tokbox/stopArchive/' + archiveID);
                $('#stop').hide();
                $('#view').prop('disabled', false);
                $('#stop').show();
            }

            // Get the archive status. If it is  "available", download it. Otherwise, keep checking
            // every 5 secs until it is "available"
            function viewArchive() { // eslint-disable-line no-unused-vars
                $('#view').prop('disabled', true);
                window.location = '/tokbox/viewArchive/' + archiveID;
            }

            $('#start').show();
            $('#view').hide();
            // (optional) add server code here
            initializeSession();
        </script>

        <script type="text/javascript">
            // Array of all the questions and choices to populate the questions. This might be saved in some JSON file or a database and we would have to read the data in.
                var all_questions = [{
                  question_string: "What color is the sky?",
                  choices: {
                    correct: "Blue",
                    wrong: ["Pink", "Orange", "Green"]
                  }
                }, {
                  question_string: 'Who is the main character of Harry Potter?',
                  choices: {
                    correct: "Harry Potter",
                    wrong: ["Hermione Granger", "Ron Weasley", "Voldemort"]
                  }
                }];


                // An object for a Quiz, which will contain Question objects.
                var Quiz = function(quiz_name) {
                  // Private fields for an instance of a Quiz object.
                  this.quiz_name = quiz_name;
                  
                  // This one will contain an array of Question objects in the order that the questions will be presented.
                  this.questions = [];
                }

                // A function that you can enact on an instance of a quiz object. This function is called add_question() and takes in a Question object which it will add to the questions field.
                Quiz.prototype.add_question = function(question) {
                  // Randomly choose where to add question
                  var index_to_add_question = Math.floor(Math.random() * this.questions.length);
                  this.questions.splice(index_to_add_question, 0, question);
                }

                // A function that you can enact on an instance of a quiz object. This function is called render() and takes in a variable called the container, which is the <div> that I will render the quiz in.
                Quiz.prototype.render = function(container) {
                  // For when we're out of scope
                  var self = this;
                  
                  // Hide the quiz results modal
                  $('#quiz-results').hide();
                  
                  // Write the name of the quiz
                  $('#quiz-name').text(this.quiz_name);
                  
                  // Create a container for questions
                  var question_container = $('<div>').attr('id', 'question').insertAfter('#quiz-name');
                  
                  // Helper function for changing the question and updating the buttons
                  function change_question() {
                    self.questions[current_question_index].render(question_container);
                    $('#prev-question-button').prop('disabled', current_question_index === 0);
                    $('#next-question-button').prop('disabled', current_question_index === self.questions.length - 1);
                   
                    
                    // Determine if all questions have been answered
                    var all_questions_answered = true;
                    for (var i = 0; i < self.questions.length; i++) {
                      if (self.questions[i].user_choice_index === null) {
                        all_questions_answered = false;
                        break;
                      }
                    }
                    $('#submit-button').prop('disabled', !all_questions_answered);
                  }
                  
                  // Render the first question
                  var current_question_index = 0;
                  change_question();
                  
                  // Add listener for the previous question button
                  $('#prev-question-button').click(function() {
                    if (current_question_index > 0) {
                      current_question_index--;
                      change_question();
                    }
                  });
                  
                  // Add listener for the next question button
                  $('#next-question-button').click(function() {
                    if (current_question_index < self.questions.length - 1) {
                      current_question_index++;
                      change_question();
                    }
                  });
                 
                  // Add listener for the submit answers button
                  $('#submit-button').click(function() {
                    // Determine how many questions the user got right
                    var score = 0;
                    for (var i = 0; i < self.questions.length; i++) {
                      if (self.questions[i].user_choice_index === self.questions[i].correct_choice_index) {
                        score++;
                      }
                      
                   // $('#quiz-retry-button').click(function(reset) {
                   //    quiz.render(quiz_container);
                   // });
                    
                    }
                    
                   
                    
                    // Display the score with the appropriate message
                    var percentage = score / self.questions.length;
                    console.log(percentage);
                    var message;
                    if (percentage === 1) {
                      message = 'Great job!'
                    } else if (percentage >= .75) {
                      message = 'You did alright.'
                    } else if (percentage >= .5) {
                      message = 'Better luck next time.'
                    } else {
                      message = 'Maybe you should try a little harder.'
                    }
                    $('#quiz-results-message').text(message);
                    $('#quiz-results-score').html('You got <b>' + score + '/' + self.questions.length + '</b> questions correct.');
                    $('#quiz-results').slideDown();
                    $('#submit-button').slideUp();
                    $('#next-question-button').slideUp();
                    $('#prev-question-button').slideUp();
                    $('#quiz-retry-button').sideDown();
                    
                  });
                  
                  // Add a listener on the questions container to listen for user select changes. This is for determining whether we can submit answers or not.
                  question_container.bind('user-select-change', function() {
                    var all_questions_answered = true;
                    for (var i = 0; i < self.questions.length; i++) {
                      if (self.questions[i].user_choice_index === null) {
                        all_questions_answered = false;
                        break;
                      }
                    }
                    $('#submit-button').prop('disabled', !all_questions_answered);
                  });
                }

                // An object for a Question, which contains the question, the correct choice, and wrong choices. This block is the constructor.
                var Question = function(question_string, correct_choice, wrong_choices) {
                  // Private fields for an instance of a Question object.
                  this.question_string = question_string;
                  this.choices = [];
                  this.user_choice_index = null; // Index of the user's choice selection
                  
                  // Random assign the correct choice an index
                  this.correct_choice_index = Math.floor(Math.random(0, wrong_choices.length + 1));
                  
                  // Fill in this.choices with the choices
                  var number_of_choices = wrong_choices.length + 1;
                  for (var i = 0; i < number_of_choices; i++) {
                    if (i === this.correct_choice_index) {
                      this.choices[i] = correct_choice;
                    } else {
                      // Randomly pick a wrong choice to put in this index
                      var wrong_choice_index = Math.floor(Math.random(0, wrong_choices.length));
                      this.choices[i] = wrong_choices[wrong_choice_index];
                      
                      // Remove the wrong choice from the wrong choice array so that we don't pick it again
                      wrong_choices.splice(wrong_choice_index, 1);
                    }
                  }
                }

                // A function that you can enact on an instance of a question object. This function is called render() and takes in a variable called the container, which is the <div> that I will render the question in. This question will "return" with the score when the question has been answered.
                Question.prototype.render = function(container) {
                  // For when we're out of scope
                  var self = this;
                  
                  // Fill out the question label
                  var question_string_h2;
                  if (container.children('h2').length === 0) {
                    question_string_h2 = $('<h2>').appendTo(container);
                  } else {
                    question_string_h2 = container.children('h2').first();
                  }
                  question_string_h2.text(this.question_string);
                  
                  // Clear any radio buttons and create new ones
                  if (container.children('input[type=radio]').length > 0) {
                    container.children('input[type=radio]').each(function() {
                      var radio_button_id = $(this).attr('id');
                      $(this).remove();
                      container.children('label[for=' + radio_button_id + ']').remove();
                    });
                  }
                  for (var i = 0; i < this.choices.length; i++) {
                    // Create the radio button
                    var choice_radio_button = $('<input>')
                      .attr('id', 'choices-' + i)
                      .attr('type', 'radio')
                      .attr('name', 'choices')
                      .attr('value', 'choices-' + i)
                      .attr('checked', i === this.user_choice_index)
                      .appendTo(container);
                    
                    // Create the label
                    var choice_label = $('<label>')
                      .text(this.choices[i])
                      .attr('for', 'choices-' + i)
                      .appendTo(container);
                  }
                  
                  // Add a listener for the radio button to change which one the user has clicked on
                  $('input[name=choices]').change(function(index) {
                    var selected_radio_button_value = $('input[name=choices]:checked').val();
                    
                    // Change the user choice index
                    self.user_choice_index = parseInt(selected_radio_button_value.substr(selected_radio_button_value.length - 1, 1));
                    
                    // Trigger a user-select-change
                    container.trigger('user-select-change');
                  });
                }

                // "Main method" which will create all the objects and render the Quiz.
                $(document).ready(function() {
                  // Create an instance of the Quiz object
                  var quiz = new Quiz('ANWER THE FOLLOWING');
                  
                  // Create Question objects from all_questions and add them to the Quiz object
                  for (var i = 0; i < all_questions.length; i++) {
                    // Create a new Question object
                    var question = new Question(all_questions[i].question_string, all_questions[i].choices.correct, all_questions[i].choices.wrong);
                    
                    // Add the question to the instance of the Quiz object that we created previously
                    quiz.add_question(question);
                  }
                  
                  // Render the quiz
                  var quiz_container = $('#quiz');
                  quiz.render(quiz_container);
                });
        </script>
    </body>
</html>
