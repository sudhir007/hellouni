<html>
    <head>
        <title> Hellouni : Search | Apply | Connect </title>
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
        <script src="https://static.opentok.com/v2/js/opentok.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.13.0/css/all.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <style>
            body, html {
                background-color: #ffffff;
                height: 100%;
            }

            .OT_root {
                padding: 10px;
                float:left;
                display:inline;
            }

            #videos {
                position: relative;
                width: 100%;
                height: 100%;
                margin-left: auto;
                margin-right: auto;
                float: left;
            }

            #subscriber {
                /*position: absolute;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                z-index: 10;*/
                position: absolute;
                left: 0;
                top: 0;
                width: 100%;
                /* height: 100%; */
                z-index: 10;
                display: inline;
                max-height: 150px;
                overflow-x: scroll;
                overflow-y: hidden;
                overflow: overlay;
            }

            #publisher {
                width: 100%;
                height: 73%;
                bottom: 10px;
                left: 10px;
                z-index: 100;
            }

             #textchatOTO {
                position: relative;
                width: 100%;
                float: left;
                left: 0;
                height: 50%;
                background-color: #333;
            }

            #historyOTO {
                width: 100%;
                /*height: calc(100% - 40px);*/
                overflow: auto;
                background-color: #d4d4d4;
            }

            input#msgTxtOTO {
                height: 40px;
                position: absolute;
                bottom: 1px;
                width: 100%;
                padding: 5px;
            }

            /* #textchat {
                position: relative;
                width: 20%;
                float: right;
                right: 0;
                height: 100%;
                background-color: #333;
            }*/
            #history {
                width: 100%;
                height: calc(100% - 74px);
                overflow: auto;
                background-color: #f1f1f1;
            }

            input#msgTxt {
                border-radius: 7px;
                margin-bottom: 8px;
                height: 40px;
                position: absolute;
                bottom: 0px;
                width: 93%;
                padding: 5px;
            }

            #history .mine {
                padding: 10px 8px;
                height: auto;
                color: #0c0c0c;
                text-align: right;
                margin: 10px 6px;
                background-color: white;
                border-radius: 7px;
                border-right: 4px solid blue;
            }

            #history .theirs {
                padding: 10px 8px;
                height: auto;
                color: #0c0c0c;
                text-align: left;
                margin: 10px 6px;
                background-color: white;
                border-radius: 7px;
                border-left: 4px solid green;
            }

            #historyOTO .mine {
                padding: 10px 8px;
                height: auto;
                color: #0c0c0c;
                text-align: right;
                margin: 10px 6px;
                background-color: white;
                border-radius: 7px;
                border-right: 4px solid blue;
            }

            #historyOTO .theirs {
                padding: 10px 8px;
                height: auto;
                color: #0c0c0c;
                text-align: left;
                margin: 10px 6px;
                background-color: white;
                border-radius: 7px;
                border-left: 4px solid green;
            }

           /* #buttonholder{
                position: absolute;
                width: 70%;
                margin-left: auto;
                margin-right: auto;
                bottom: 2px;
                left: 20px;
                z-index: 200;
            }*/

            .OT_video{
                right: 0;
                height: 20px;
                width: 40px;
                display: block;
                cursor: pointer;
                position: absolute;
                margin-right: 60px;
                margin-top: 8px;
                z-index: 1000 !important;
            }
            .OT_mode-on{
                z-index: 111 !important;
            }

            /*.OT_publisher .OT_edge-bar-item.OT_mode-auto{
                top: 0px;
                opacity: 1;
            }*/
            .attendeesListModal, .pollModal , .chatBoxModal{
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 80%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
                z-index: 112;
            }

            .attendeesList-content, .pollModal-content  {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: 80%;
            }

            .chatBoxModal-content  {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: 60%;
            }
            /* The Close Button */
            .listclose, .pollclose, .chatclose {
                color: #8e0f0f;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .listclose:hover, .listclose:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }

            .pollclose:hover, .pollclose:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }

            .chatclose:hover, .chatclose:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }

            .box{
                display: none;
                position: fixed;
                width: 20%;
                right: 0;
                bottom: 5%;
                height: 54%;
                background-color: #d4d4d4;
                z-index:999;
            }
            /* Add padding and border to inner content
            for better animation effect */
            .box-inner{
                width: auto;
                padding: 10px; color:#fff;
            }

            .fa-fw {
                width: 2em;
            }

            /*Table Attendees*/
            .panel {
                border: 1px solid #ddd;
                background-color: #fcfcfc;
            }
            .panel .btn-group {
                margin: 15px 0 30px;
            }
            .panel .btn-group .btn {
                transition: background-color .3s ease;
            }

            .marginbox{
              margin: 2px 10px;
            }


            /*table button*/
            .AVButton{
                font-size: 20px;
                margin: 1px 12px 1px 5px;
                color: darkblue;
            }

            /*Poll Quiz*/
            #quiz {
                margin: -44px 50px 0px;
                position: relative;
                width: calc(100% - 100px);
                text-align: center;
            }

            #quiz button {
                margin: 8px 0px 0px 8px;
                cursor: pointer;
            }

            #quiz button:disabled {
                opacity: 0.5;
                cursor: default;
            }

            #question {
                padding: 20px;
                background: #FAFAFA;
                text-align: center;
            }

            #question h2 {
                margin-bottom: 16px;
                font-weight: 600;
                font-size: 20px;
            }

            #question input[type=radio] {
                display: none;
            }

            #question label {
                display: inline-block;
                margin: 10px;
                padding: 8px;
                background: #ffffff;
                color: #4C3000;
                width: calc(50% - 8px);
                min-width: 50px;
                cursor: pointer;
                letter-spacing: 0.5px;
                border-radius: 6px;
                box-shadow: 0px 2px 2px #dedada;
            }

            #question label:hover {
                background: #347ab7;
                color: white;
            }

            #question input[type=radio]:checked + label {
                background: #5bb85b;
                color: white;
            }

            #quiz-results {
                display: flex;
                flex-direction: column;
                justify-content: center;
                position: absolute;
                top: 5px;
                left: 0px;
                background: #FAFAFA;
                width: 100%;
                height: calc(100% - 2px);
            }

            #quiz-results-message {
                display: block;
                color: #00403C;
                font-size: 20px;
                font-weight: bold;
            }

            #quiz-results-score {
                display: block;
                color: #31706c;
                font-size: 20px;
            }

            #quiz-results-score b {
                color: #00403C;
                font-weight: 600;
                font-size: 20px;
            }

            #quiz-retry-button {
                float: left;
                margin: 8px 0px 0px 8px;
                padding: 4px 8px;
                background: #9ACFCC;
                color: #00403C;
                font-size: 14px;
                cursor: pointer;
                outline: none;
            }

            #poll-end-button{
                display: none;
            }


            /*Sticky Button*/
#mySidenav a {
    position: fixed;
    right: -24px;
    transition: 0.5s;
    padding: 12px 11px 9px 20px;
    width: 85px;
    text-decoration: none;
    font-size: 20px;
    color: white;
    border-radius: 30px 0px 0px 30px;
    z-index: 110;
}

#mySidenav a:hover {
    right: 0;
}


#MenuTab {
    top: 0%;
    background-color: #184658;
}

/*Sticky Menu*/
.sidepanel1  {
    width: 0%;
    position: fixed;
    z-index: 110;
    height: 95%;
    top: 0%;
    right: 0;
    background-color: #ffffff;
    overflow-x: hidden;
    transition: 0.7s;
    padding-top: 30px;
    border-radius: 10px 2px 2px 10px;
    box-shadow: 0px 4px 6px 2px #88888887;
}

.sidepanel1 a {
        padding: 8px 4px 8px 5px;
    /* text-decoration: none; */
    /* font-size: 18px; */
    /* color: #b8c7ce; */
    display: block;
    transition: 0.3s;
    /*box-shadow: 2px 1px;*/
        border-top: thin solid;
}

.sidepanel1 a:hover {
    color: black  ;
    background-color: #eee;
}

.sidepanel1 .closebtn {
    padding: 0px !important;
    box-shadow: 0px 0px !important;
    position: absolute;
    top: 15px;
    right: 17px;
    font-size: 28px;
    background-color: #ffffff !important;

}

.openbtn {
    font-size: 20px;
    cursor: pointer;
    background-color: #111;
    color: white;
    padding: 10px 15px;
    border: none;
}

.openbtn:hover {
    background-color:#444;
}

.width100{
  width: 100%;
}

.listTable  th, td {
  padding: 2px 8px !important;
}
#group-chat-button{
    display: none;
}
.bg-text {
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0, 0.4); /* Black w/opacity/see-through */
  color: white;
  font-weight: bold;
  border: 3px solid #f1f1f1;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 2;
  width: 80%;
  padding: 20px;
  text-align: center;
}

@media only screen and (max-device-width: 480px) {

    .box{
            display: none;
            position: fixed;
            width: 50%;
            right: 0;
            top: 3%;
            /* bottom: 5%; */
            height: 95%;
            background-color: #d4d4d4;
            z-index: 999;
        }

}
        </style>
    </head>
    <body>
        
        <?php

        if($role != 'subscriber'){
            ?>
            <div class="container-fluid" id="videos">
                <div class="container">
                  <div class="col-md-12" id="subscriber"></div>
                </div>
                <div id="publisher" class="container-fluid" style="margin-top: 180px;"></div>
                <div id="screen-preview"></div>
            </div>
            <div id="mySidenav" class="sidenav" >
              <a href="#" id="MenuTab" class="openbtn" onclick="openNav()" ><i class="fas fa-bars"></i></a>
            </div>

            <div id="mySidepanel1" class="sidepanel1" >
              <a href="javascript:void(0)" class="closebtn" onclick="closeNav()" style="border-top: none !important;top: 2px;right: 10px;font-size: 19px;"><i class="fa fa-times" aria-hidden="true"></i></a>
              <a href="#" title="Start Recoring" > <button type="button" id="start" onClick="javascript:startArchive()" class="btn btn-sm btn-primary width100"><i class="fas fa-plus-circle">&nbsp;</i><b>Start Recording</b></button></a>
              <a href="#" ><button type="button" id="stop" onClick="javascript:stopArchive()" class="btn btn-sm btn-danger width100"><i class="fas fa-minus-circle">&nbsp;</i><b>Stop Recording</b></button></a>
              <a href="#" ><button type="button" id="view" onClick="javascript:viewArchive()" class="btn btn-sm btn-info width100" disabled><i class="fas fa-file-archive">&nbsp;</i><b>View Archive</b></button></a>
              <a href="#" ><button type="button" id="screen" onClick="javascript:shareScreen()" class="btn btn-sm btn-primary width100"><i class="fas fa-chalkboard-teacher">&nbsp;</i><b>Share Screen</b></button></a>

              <div class="panel-group" id="accordion1" style="margin-bottom: 0px;">
                 <div class="panel">
                    <div class="panel-heading" style="padding: 5px 4px;">
                       <a data-toggle="collapse" data-parent="#accordion1" data-target="#collapseOne" href="#" aria-expanded="true" aria-controls="collapseOne" style="border-top: none;background-color: #449d44" class="btn btn-sm btn-success "><i class="fas fa-users">&nbsp;</i>Show attendees</a>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse">
                       <div class="panel-body" style="color: black; padding: 9px;max-height: 200px;overflow: auto;">
                         <table class="table table-bordered listTable" style="font-size: small;" >
                            <thead><tr>
                              <th>ID</th>
                              <th>Name</th>
                              <th></th>
                              <th><button type="button" onclick="openListPopup()" class="btn btn-sm btn-warning"><i class="fas fa-external-link-alt"></i></button></th>
                            </tr>
                            </thead>
                            <tbody id="attendees-list-menu">
                              <!-- <tr>
                                <td>1</td>
                                <td>Deepika</td>
                                <td><span><i class=" fas fa-video"></i><span> </td>
                                <td><span><i class=" fas fa-microphone"></i></span></td>
                              </tr>
                              <tr>
                                <td>2</td>
                                <td>Sudhir</td>
                                <td><span><i class=" fas fa-video"></i><span> </td>
                                <td><span><i class=" fas fa-microphone"></i><span></td>
                              </tr>
                              <tr>
                                <td>3</td>
                                <td>Sudhir</td>
                                <td><span><i class=" fas fa-video"></i><span> </td>
                                <td><span><i class=" fas fa-microphone"></i><span></td>
                              </tr> -->
                            </tbody>
                         </table>
                       </div>
                    </div>
                 </div>
              </div>
              <!-- <a href="#" ><button type="button" onclick="openListPopup()" class="btn btn-sm btn-success width100"><i class="fas fa-users">&nbsp;</i><b>Show attendees</b>
              </button></a> -->
              <a href="#" ><button type="button" class="slide-toggle btn btn-sm btn-info width100" ><i class="fas fa-comments">&nbsp;</i><b>Ask</b></button></a>
              <?php
              if($polling)
              {
                  ?>
                  <a href="#" ><button type="button" onclick="openPollPopup()" class="btn btn-sm btn-success width100" id="poll-start-button"><i class="fas fa-poll">&nbsp;</i><b>Polling</b> </button><button type="button" onclick="closePollPopup()" class="btn btn-sm btn-danger width100" id="poll-end-button"><i class="fas fa-poll">&nbsp;</i><b>End Polling</b> </button></a>
                  <?php
              }
              ?>
              <a href="#" ><button type="button"  class="btn btn-sm btn-danger width100" onclick="leaveMeeting()"><b>Leave Meeting</b> </button></a>
            </div>
            <?php
        } else{

        ?>
        <div class="container-fluid" id="videos">
            <div class="container">
              <div class="col-md-12" id="subscriber"></div>
            </div>
            <div id="publisher" class="container-fluid" style="margin-top: 180px;background-color: white;background-image: url(<?php echo base_url();?>application/images/bgImg.png);background-position: center center;background-repeat: no-repeat;background-size: cover;filter: blur(1.8px);">
                
            </div>
            <div class="bg-text">
                  <h2>Detail ONE</h2>
                  <h1 style="font-size:50px">Some Information</h1>
                  <p>And more information or link we can add</p>
                </div>
            <div id="screen-preview"></div>
        </div>
        <div class="col-md-12 col-sm-12 visible-xs visible-sm" style="float: left;position: fixed;background: #184557;padding-left: 0px;padding-right: 0px; z-index: 110; top: 0px;     text-align: center;    box-shadow: 0px -3px 0px #424141;">
                <div class="col-md-12" style="padding: 5px 5px 5px 5px;text-align: right;">
                    <div id="buttons" style="display: inline-flex;">
                        <button type="button" class="slide-toggle btn btn-sm btn-info marginbox" ><i class="fas fa-comments">&nbsp;</i><b>Ask</b></button>
                        <button type="button"  class="btn btn-sm btn-danger" onclick="leaveMeeting()"><b>Leave Meeting</b> </button>
                    </div>
                </div>
        </div>
         <div id="mySidenav" class="sidenav hidden-xs hidden-sm">
              <a href="#" id="MenuTab" class="openbtn" onclick="openNav()" ><i class="fas fa-bars"></i></a>
            </div>

            <div id="mySidepanel1" class="sidepanel1" >
              <a href="javascript:void(0)" class="closebtn" onclick="closeNav()" style="    border-top: none !important;top: 2px;right: 10px;font-size: 19px;"><i class="fa fa-times" aria-hidden="true"></i></a>
              <a href="#" ><button type="button" class="slide-toggle btn btn-sm btn-info width100" ><i class="fas fa-comments">&nbsp;</i><b>Ask</b></button></a>
              <a href="#" ><button type="button"  class="btn btn-sm btn-danger width100" onclick="leaveMeeting()"><b>Leave Meeting</b> </button></a>
            </div>
       <?php } ?>


        <div class="box" id="chat-box">
            <span style="display: flex;float: right;">
              <button id="chatBoxOut" onclick="expandChat()" type="button" class="btn btn-sm btn-primary marginbox hidden-xs hidden-sm" ><i class="fas fa-chevron-circle-up"></i></button>
              <button id="chatBoxIn" onclick="collapsChat()" type="button" class="btn btn-sm btn-primary marginbox" style="display: none;"><i class="fas fa-chevron-circle-down"></i></button>
              <button type="button" class="slide-toggle btn btn-sm btn-danger marginbox" ><i class="fas fa-times-circle"></i></button>
            </span>
            <div class="box-inner">
                 <div id="textchat">
                    <div id="history">
                        <?php
                        if($common_chat_history)
                        {
                            foreach($common_chat_history as $history)
                            {
                                ?>
                                <p class="<?=$history['chat_of']?>"><?=$history['name'] . ': ' . $history['message']?></p>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <form style="color: black;">
                        <input type="text" placeholder="Input your text here" id="msgTxt"></input>
                        <input type="hidden" id="username" value="<?=$username?>"></input>
                    </form>
                </div>
            </div>
        </div>
        <div id="attendeesList" class="attendeesListModal" >
              <!-- Modal content -->
              <div class="row attendeesList-content">
                 <span class="listclose" id="closeModal">&times;</span>
                 <!-- <button type="button" onclick="closeListPopup()" class="btn btn-danger">close </button> -->
                  <div class="col-md-12 content">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="pull-left">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success btn-filter" data-target="Publisher">Publisher</button>
                                        <button type="button" class="btn btn-warning btn-filter" data-target="Organizer">Organizer</button>
                                        <button type="button" class="btn btn-danger btn-filter" data-target="Subscriber">Subscriber</button>
                                        <button type="button" class="btn btn-default btn-filter" data-target="all">All</button>
                                        <button type="button" class="btn btn-default btn-filter" onclick="openGroupChat()" id="group-chat-button">Send Message</button>
                                    </div>
                                </div>
                                <div class="table-container">
                                    <table class="table table-filter">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Name</th>
                                                <th>Type</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="attendees-list"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
              </div>
        </div>

         <div id="pollId" class="pollModal" >
              <!-- Modal content -->
              <div class="row pollModal-content">
                 <!-- <span class="pollclose" id="pollcloseModal">&times;</span> -->
                    <div id="quiz">
                      <h1 id="quiz-name"></h1>
                      <button id="submit-button" class="btn btn-md btn-primary">Submit Answers</button>
                      <button id="next-question-button" class="btn btn-md btn-primary">Next Question</button>
                      <button id="prev-question-button" class="btn btn-md btn-primary">Previous</button>

                      <div id="quiz-results">

                        <p id="quiz-results-message"></p>
                        <p id="quiz-results-score"></p>
                        <!-- <button id="quiz-retry-button">Retry</button> -->

                      </div>
                    </div>

                </div>
        </div>

        <div id="chatModal" class="chatBoxModal" >
              <!-- Modal content -->
              <div class="row chatBoxModal-content" >
                 <span class="chatclose" id="chatcloseModal">&times;</span>
                 <div id="historyOTO">
                     <?php
                     if($private_chat_history)
                     {
                         foreach($private_chat_history as $history)
                         {
                             ?>
                             <p class="<?=$history['chat_of']?>"><?=$history['name'] . ': ' . $history['message']?></p>
                             <?php
                         }
                     }
                     ?>
                 </div>
                 <div id="textchatOTO"></div>
                 <!--div id="OneToOneChat">
                 </div-->
              </div>
        </div>
        <div class="col-md-12 col-sm-12" style="float: left;position: fixed;background: #184658;padding-left: 0px;padding-right: 0px; z-index: 115; bottom: 0px;     text-align: center;    box-shadow: 0px -3px 0px #d6d3d3;">

                <div class="col-md-12" style="padding: 5px 5px 5px 5px;">
                   <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="text-align: left;">
                      <div class="copyright">
                         <span style="color: white;letter-spacing: 0.6px;font-size: 13px;">© 2018, </span><img src="https://www.hellouni.org/img/logo.png" width="90">&nbsp;<span style="color: white;letter-spacing: 0.6px;font-size: 13px;"> All rights reserved</span>
                         <!-- COPYRIGHT 2018 &copy; HELLOUNI 2018 -->
                      </div>
                   </div>
                   <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="text-align: right;">
                      <div class="design">
                         <a href="https://www.hellouni.org" style="color: white;letter-spacing: 0.6px;font-size: 13px;"><b>Hellouni</b> </a> |  <a target="_blank" href="https://www.hellouni.org" style="color: white;letter-spacing: 0.6px;font-size: 13px;">MAKE RIGHT CHOICE !</a>
                      </div>
                   </div>
                </div>
            </div>
        <script type="text/javascript">
            var attendeesListModal = document.getElementById("attendeesList");
            var coursespan = document.getElementById("closeModal");

            var pollModal = document.getElementById("pollId");
            var pollspan = document.getElementById("pollcloseModal");


            function openNav() {
                // document.getElementById("mySidepanel1").style.width = "";
                document.getElementById("mySidepanel1").style.setProperty("width", "20%", "important");
                document.getElementById("videos").style.setProperty("width", "80%", "important");
            }

            function closeNav() {
                // document.getElementById("mySidepanel1").style.width = "0";
                document.getElementById("mySidepanel1").style.setProperty("width", "0", "important");
                document.getElementById("videos").style.setProperty("width", "100%", "important");
            }

            function expandChat() {
                // document.getElementById("mySidepanel1").style.width = "";

                document.getElementById("chat-box").style.setProperty("height", "95%", "important");
                document.getElementById("chat-box").style.display = "block";
                document.getElementById("chatBoxOut").style.display = "none";
                document.getElementById("chatBoxIn").style.display = "block";
            }

            function collapsChat() {
                // document.getElementById("mySidepanel1").style.width = "";

                document.getElementById("chat-box").style.setProperty("height", "54%", "important");
                document.getElementById("chat-box").style.display = "block";
                document.getElementById("chatBoxIn").style.display = "none";
                document.getElementById("chatBoxOut").style.display = "block";
            }

            function openListPopup() {
                // document.getElementById("course-list-modal").innerHTML = response;
                attendeesListModal.style.display = "block";
            }
            coursespan.onclick = function() {
                attendeesListModal.style.display = "none";
            }

            function closeListPopup() {
                // document.getElementById("course-list-modal").innerHTML = response;
                attendeesListModal.style.display = "none";
            }

            window.onclick = function(event) {
                if (event.target == attendeesListModal) {
                    attendeesListModal.style.display = "none";
                }
            }

            function openPollPopup() {
                // document.getElementById("course-list-modal").innerHTML = response;

                session.signal({
                    type: 'poll-start',
                    data: ''
                }, function signalCallback(error) {
                    if (error) {
                        console.error('Error sending signal:', error.name, error.message);
                    } else {
                        console.log("polling started");
                    }
                });
            }

            function closePollPopup() {
                // document.getElementById("course-list-modal").innerHTML = response;

                session.signal({
                    type: 'poll-stop',
                    data: ''
                }, function signalCallback(error) {
                    if (error) {
                        console.error('Error sending signal:', error.name, error.message);
                    } else {
                        console.log("polling started");
                    }
                });
            }

            /*pollspan.onclick = function() {
                pollModal.style.display = "none";
            }*/

            function closeListPopup() {
                // document.getElementById("course-list-modal").innerHTML = response;
                pollModal.style.display = "none";
            }

            window.onclick = function(event) {
                if (event.target == pollModal) {
                    pollModal.style.display = "none";
                }
            }


            var chatBoxModal = document.getElementById("chatModal");
            var chatspan = document.getElementById("chatcloseModal");

            chatspan.onclick = function() {
                chatBoxModal.style.display = "none";
            }

            function closeListPopup() {
                // document.getElementById("course-list-modal").innerHTML = response;
                chatBoxModal.style.display = "none";
            }

            window.onclick = function(event) {
                if (event.target == chatBoxModal) {
                    chatBoxModal.style.display = "none";
                }
            }

            $(document).ready(function(){
                $(".slide-toggle").click(function(){
                    $(".box").animate({
                        width: "toggle"
                    });
                });
            });



            $(document).ready(function () {
                $('.btn-filter').on('click', function () {
                    var $target = $(this).data('target');
                    if ($target != 'all') {
                        $('.table tr').css('display', 'none');
                        $('.table tr[data-status="' + $target + '"]').fadeIn('slow');
                    } else {
                        $('.table tr').css('display', 'none').fadeIn('slow');
                    }
                });
                $('#videoOff').click(function(){
                    $(this).find('i').toggleClass('fa-video fa-video-slash')
                });

                $('#audioOff').click(function(){
                    $(this).find('i').toggleClass('fa-microphone fa-microphone-slash')
                });

                $('#screenView').click(function(){
                    $(this).find('i').toggleClass('fa-arrows-alt fa-compress-arrows-alt')
                });
            });

            var apiKey = "46220952";
            var sessionId = "<?=$sessionId?>";
            var token = "<?=$token?>";
            var tokboxId = "<?=$tokboxId?>";
            var session;
            var archiveID;
            var allConnections = {};
            var publisher;

            var organizerCount = 0;
            var publisherCount = 0;
            var subscriberCount = 0;
            var connectionCount = 0;

            var msgHistory = document.querySelector('#history');
            var privateHistory = document.querySelector("#historyOTO");

            // Handling all of our errors here by alerting them
            function handleError(error) {
                if (error) {
                    //alert(error.message);
                }
            }

            function initializeSession() {
                session = OT.initSession(apiKey, sessionId);

                // Create a publisher
                publisher = OT.initPublisher('publisher', {
                    insertMode: 'append',
                    width: '100%',
                    height: '100%',
                    name: '<?=$username?>',
                    style: {buttonDisplayMode: 'on', nameDisplayMode: "on"}
                    //videoSource: 'screen'
                }, handleError);

                //publisher.publishVideo(false);

                // Connect to the session
                session.connect(token, function(error) {
                    // If the connection is successful, publish to the session
                    if (error) {
                        handleError(error);
                    } else {
                        var node = document.createElement("button");
                        node.setAttribute("id", "video-button-" + publisher.id);
                        node.setAttribute("class", "OT_edge-bar-item OT_mode-on OT_video AVButton fas fa-video");
                        node.setAttribute("onClick", "disableVideo()")
                        document.getElementById(publisher.id).appendChild(node);
                        // document.getElementById("video-button-" + publisher.id).innerHTML = "Disable Video";
                         document.getElementById("video-button-" + publisher.id).style.setProperty("background-color", "#ffffff00", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("font-size", "25px", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("right", "30px", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("top", "4px", "important");
                         document.getElementById("video-button-" + publisher.id).style.setProperty("color", "white", "important");

                        session.publish(publisher, handleError);
                    }
                });



                session.on('signal:msg', function signalCallback(event) {
                    console.log(event);
                    document.getElementById("chat-box").style.display="block";
                    var msg = document.createElement('p');
                    msg.textContent = event.data;
                    msg.className = event.from.connectionId === session.connection.connectionId ? 'mine' : 'theirs';
                    msgHistory.appendChild(msg);
                    msg.scrollIntoView();
                });

                session.on('signal:poll-start', function signalCallback(event) {
                    //publisher.publishVideo(false);
                    //document.getElementById("videos").style.display = "none";
                    if(document.getElementById("poll-start-button")){
                        document.getElementById("poll-start-button").style.display = "none";
                        document.getElementById("poll-end-button").style.display = "inline-block";
                    }
                    pollModal.style.display = "block";
                });

                session.on('signal:poll-stop', function signalCallback(event) {
                    //publisher.publishVideo(true);
                    //document.getElementById("videos").style.display = "block";
                    if(document.getElementById("poll-start-button")){
                        document.getElementById("poll-start-button").style.display = "inline-block";
                        document.getElementById("poll-end-button").style.display = "none";
                    }
                    pollModal.style.display = "none";
                });

                session.on('archiveStarted', function archiveStarted(event) {
                    archiveID = event.id;
                    console.log('Archive started ' + archiveID);
                    $('#stop').show();
                    $('#start').hide();
                });

                session.on('archiveStopped', function archiveStopped(event) {
                    archiveID = event.id;
                    //console.log('Archive stopped ' + archiveID);
                    $('#start').hide();
                    $('#stop').hide();
                    $('#view').show();
                });

                session.on('signal:msg-private', function signalCallback(event) {
                    var msg = document.createElement('p');
                    msg.textContent = event.data;
                    msg.className = event.from.connectionId === session.connection.connectionId ? 'mine' : 'theirs';
                    privateHistory.appendChild(msg);
                    msg.scrollIntoView();
                    //console.log(event);
                    //modal.style.display = "block";
                    //chatBoxModal.style.display = "block";
                    allConnections[event.from.connectionId] = event.from;
                    openChatBox(event.from.connectionId);
                });

                session.on({
                    connectionCreated: function (event) {
                        $.ajax({
                            url: '/tokbox/connection',
                            type: 'POST',
                            contentType: 'application/json', // send as JSON
                            data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId, 'connectionId': event.connection.id}),
                            complete: function complete() {
                                // called when complete
                                //console.log('connection function completed');
                            },
                            success: function success() {
                                // called when successful
                                //console.log('successfully called connection');
                            },
                            error: function error() {
                                // called when there is an error
                                //console.log('error calling connection');
                            }
                        });
                        //console.log("Connection Created ", event);
                        //console.log("Current Session ", session);
                        //connectionCount++;
                        if (event.connection.connectionId != session.connection.connectionId) {
                            //console.log(event.connection.permissions.forceDisconnect + ' connected.');
                            //console.log(event.connection.permissions.publish + ' connected.');
                            //console.log(event.connection.permissions.forceDisconnect + ' connected.');
                            if (session.capabilities.publish == 1) {
                                /*var node = document.createElement("button");
                                node.setAttribute("id", event.connection.connectionId);
                                node.setAttribute("class", "btn btn-danger");
                                node.setAttribute("onClick", "disconnectUser('" + event.connection.connectionId + "')")
                                document.getElementById("buttons").appendChild(node);
                                document.getElementById(event.connection.connectionId).innerHTML = event.connection.data;

                                var chat_node = document.createElement("button");
                                chat_node.setAttribute("id", "chat-" + event.connection.connectionId);
                                chat_node.setAttribute("class", "btn btn-danger");
                                chat_node.setAttribute("onClick", "openChatBox('" + event.connection.connectionId + "')")
                                document.getElementById("buttons").appendChild(chat_node);
                                document.getElementById("chat-" + event.connection.connectionId).innerHTML = event.connection.data + " - Chat";*/

                                var tr_node = document.createElement("tr");
                                tr_node.setAttribute("id", "attendees-list-" + event.connection.connectionId);

                                var tr_node1 = document.createElement("tr");
                                tr_node1.setAttribute("id", "attendees-list-menu-" + event.connection.connectionId);

                                connectionCount++;
                                var tdHtml1 = '<td>' + connectionCount + '</td><td>' + event.connection.data + '</td><td>Organizer</td><td class="text-center"><a href="javascript:void" onClick="openChatBox(\'' + event.connection.connectionId + '\')"><i class="fas fa-comments">&nbsp;</i></a></td>';

                                if(event.connection.permissions.forceDisconnect){
                                    organizerCount++;
                                    tr_node.setAttribute("data-status", "Organizer");

                                    var tdHtml = '<td><input type="checkbox" value="' + event.connection.connectionId + '" onclick="showGroupChatButton(this.value)"></td><td>' + event.connection.data + '</td><td>Organizer</td><td class="text-center"><a href="javascript:void" onClick="openChatBox(\'' + event.connection.connectionId + '\')"><i class="fas fa-comments">&nbsp;</i></a></td>';
                                }
                                else if(event.connection.permissions.publish){
                                    publisherCount++;
                                    tr_node.setAttribute("data-status", "Publisher");

                                    var tdHtml = '<td><input type="checkbox" value="' + event.connection.connectionId + '" onclick="showGroupChatButton(this.value)"></td><td>' + event.connection.data + '</td><td>Publisher</td><td class="text-center"><a href="javascript:void" onClick="openChatBox(\'' + event.connection.connectionId + '\')"><i class="fas fa-comments">&nbsp;</i></a></td>';
                                }
                                else{
                                    subscriberCount++;
                                    tr_node.setAttribute("data-status", "Subscriber");

                                    var tdHtml = '<td><input type="checkbox" value="' + event.connection.connectionId + '" onclick="showGroupChatButton(this.value)"></td><td>' + event.connection.data + '</td><td>Subscriber</td><td class="text-center"><a href="javascript:void" onClick="openChatBox(\'' + event.connection.connectionId + '\')"><i class="fas fa-comments">&nbsp;</i></a></td>';
                                }

                                document.getElementById("attendees-list").appendChild(tr_node);
                                document.getElementById("attendees-list-" + event.connection.connectionId).innerHTML = tdHtml;

                                document.getElementById("attendees-list-menu").appendChild(tr_node1);
                                document.getElementById("attendees-list-menu-" + event.connection.connectionId).innerHTML = tdHtml1;

                                allConnections[event.connection.connectionId] = event.connection;
                            }
                            //resizeSubscriber();
                        }
                    },
                    connectionDestroyed: function connectionDestroyedHandler(event) {
                        //connectionCount--;
                        //console.log(event.connection.data + ' disconnected.');
                        if (event.connection.connectionId != session.connection.connectionId) {
                            /*if (session.capabilities.forceDisconnect == 1) {
                                var element = document.getElementById(event.connection.connectionId);
                                element.parentNode.removeChild(element);
                            }*/
                            if(document.getElementById("attendees-list-" + event.connection.connectionId) !== undefined){
                                var element = document.getElementById("attendees-list-" + event.connection.connectionId);
                                element.parentNode.removeChild(element);
                            }
                        }
                        else{
                            alert("You have been disconnected. Please refresh the page to join again");
                        }
                    },
                    streamDestroyed: function (event){
                        //console.log(publisher2);
                        console.log("Stream Destroyed ", event);
                        document.getElementById("subscriber").style.display = "block";
                        document.getElementById("publisher").style.display = "block";
                    },
                    streamCreated: function(event){
                        if(event.stream.videoType == 'camera'){
                            var subscriber = session.subscribe(event.stream, 'subscriber', {
                                insertMode: 'append',
                                width: '220px',
                                height: '140px',
                                name: event.stream.name,
                            }, handleError);

                            var SpeakerDetection = function(subscriber, startTalking, stopTalking) {
                                var activity = null;
                                subscriber.on('audioLevelUpdated', function(event) {
                                    var now = Date.now();
                                    if (event.audioLevel > 0.2) {
                                        if (!activity) {
                                            activity = {timestamp: now, talking: false};
                                        }
                                        else if (activity.talking) {
                                            activity.timestamp = now;
                                        }
                                        else if (now- activity.timestamp > 500) {
                                            // detected audio activity for more than 1s
                                            // for the first time.
                                            activity.talking = true;
                                            if (typeof(startTalking) === 'function') {
                                                startTalking();
                                            }
                                        }
                                    }
                                    else if (activity && now - activity.timestamp > 3000) {
                                        // detected low audio activity for more than 3s
                                        if (activity.talking) {
                                            if (typeof(stopTalking) === 'function') {
                                                stopTalking();
                                            }
                                        }
                                        activity = null;
                                    }
                                });
                            };

                            SpeakerDetection(subscriber, function() {
                                console.log('Started Talking ', subscriber.id, subscriber);
                                //document.getElementById(subscriber.id).style.overflow = "inherit";
                                document.getElementById(subscriber.id).style.setProperty("overflow", "inherit", "important");

                                var x = document.getElementById(subscriber.id).querySelectorAll(".OT_name");
                                x[0].style.display = "block";
                                x[0].style.opacity = "1";
                                x[0].style.top = "0";


                                var y = document.getElementById(subscriber.id).querySelectorAll(".OT_widget-container");
                                y[0].style.display = "block";
                                y[0].style.setProperty("border", "3px solid #00ca00", "important");



                            }, function() {
                                document.getElementById(subscriber.id).style.background = "";
                                var x = document.getElementById(subscriber.id).querySelectorAll(".OT_name");
                                x[0].style.display = "none";
                                x[0].style.opacity = "0";
                                x[0].style.top = "-25px";

                                var y = document.getElementById(subscriber.id).querySelectorAll(".OT_widget-container");
                                y[0].style.display = "block";
                                y[0].style.setProperty("border", "none", "important");

                                document.getElementById(subscriber.id).style.setProperty("overflow", "hidden", "important");
                                console.log('stopped talking');
                            });
                        }
                        if(event.stream.videoType == 'screen'){
                            document.getElementById("subscriber").style.display = "none";
                            document.getElementById("publisher").style.display = "none";
                            session.subscribe(event.stream, 'screen-preview', {
                                insertMode: 'append',
                                width: '100%',
                                height: '100%',
                                name: '<?=$username?>'
                            }, handleError);
                        }
                        console.log("Stream Created ", event);
                    }
                });

                function resizeSubscriber() {
                    publisher.element.style.width = "100px";
                    publisher.element.style.height = "75px";
                    document.getElementById("subscriber").appendChild(publisher.element);
                }
            }

            function disableVideo(){
                publisher.publishVideo(false);
                document.getElementById("video-button-" + publisher.id).setAttribute("onClick", "enableVideo('" + publisher + "')");
                // document.getElementById("video-button-" + publisher.id).innerHTML = "Enable Video";
                document.getElementById("video-button-" + publisher.id).classList.remove('fa-video');
                document.getElementById("video-button-" + publisher.id).classList.add('fa-video-slash');

            }

            function enableVideo(){
                publisher.publishVideo(true);
                document.getElementById("video-button-" + publisher.id).setAttribute("onClick", "disableVideo('" + publisher + "')");
                // document.getElementById("video-button-" + publisher.id).innerHTML = "Disable Video";
                document.getElementById("video-button-" + publisher.id).classList.remove('fa-video-slash');
                document.getElementById("video-button-" + publisher.id).classList.add('fa-video');

            }

            function disconnectUser(connectionId){
                if (session.capabilities.forceDisconnect == 1) {
                    alert(connectionId);
                    session.forceDisconnect(allConnections[connectionId]);
                } else {
                    alert("No permission");
                }
            }

            var group_chat_connections = [];
            function showGroupChatButton(value){
                var group_connections = document.getElementsByName("group_chat_connections[]");
                /*if(group_connections){
                    group_connections.forEach(function(group_connection){
                        console.log("Value ", group_connection.value);
                    });
                }*/
                console.log(value);
                var index = group_chat_connections.indexOf(value);
                if (index > -1) {
                    group_chat_connections.splice(index, 1);
                }
                else{
                    group_chat_connections.push(value);
                }
                if(group_chat_connections.length){
                    document.getElementById("group-chat-button").style.display = "block";
                }
                else{
                    document.getElementById("group-chat-button").style.display = "none";
                }
            }

            function openGroupChat(){
                var html = '<input type="text" placeholder="Input your text here" id="msgTxtOTO"></input><input type="hidden" id="username-private" value="<?=$username?>"></input><input type="button" class="btn btn-danger" onclick="sendGroupChat()" value="Send">';
                document.getElementById("textchatOTO").innerHTML = html;
                // modal.style.display = "block";
                chatBoxModal.style.display = "block";
            }

            function sendGroupChat(connectionId){
                var msg = document.getElementById("msgTxtOTO").value;
                var uname = document.getElementById("username-private").value;
                //console.log(allConnections[connectionId]);
                var msgEle = document.createElement('p');
                msgEle.textContent = uname + ': ' + msg;
                msgEle.className = 'mine';
                privateHistory.appendChild(msgEle);
                msgEle.scrollIntoView();

                for(var i = 0; i < group_chat_connections.length; i++){
                    $.ajax({
                        url: '/tokbox/chat',
                        type: 'POST',
                        contentType: 'application/json', // send as JSON
                        data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId, 'to': group_chat_connections[i], 'msg': msg}),
                        complete: function complete() {
                            // called when complete
                            console.log('chat function completed');
                        },
                        success: function success() {
                            // called when successful
                            console.log('successfully called chat');
                        },
                        error: function error() {
                            // called when there is an error
                            console.log('error calling chat');
                        }
                    });
                    session.signal({
                        to: allConnections[group_chat_connections[i]],
                        type: 'msg-private',
                        data: uname + ': ' + msg
                    }, function signalCallback(error) {
                        if (error) {
                            console.error('Error sending signal:', error.name, error.message);
                        } else {
                            msgTxt.value = '';
                        }
                    });
                }

            }

            function openChatBox(connectionId){
                var html = '<input type="text" placeholder="Input your text here" id="msgTxtOTO"></input><input type="hidden" id="username-private" value="<?=$username?>"></input><input type="button" class="btn btn-danger" onclick="sendChat(\'' + connectionId + '\')" value="Send">';
                document.getElementById("textchatOTO").innerHTML = html;
                // modal.style.display = "block";
                chatBoxModal.style.display = "block";
            }

            function sendChat(connectionId){
                var msg = document.getElementById("msgTxtOTO").value;
                var uname = document.getElementById("username-private").value;
                //console.log(allConnections[connectionId]);
                var msgEle = document.createElement('p');
                msgEle.textContent = uname + ': ' + msg;
                msgEle.className = 'mine';
                privateHistory.appendChild(msgEle);
                msgEle.scrollIntoView();

                $.ajax({
                    url: '/tokbox/chat',
                    type: 'POST',
                    contentType: 'application/json', // send as JSON
                    data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId, 'to': connectionId, 'msg': msg}),
                    complete: function complete() {
                        // called when complete
                        console.log('chat function completed');
                    },
                    success: function success() {
                        // called when successful
                        console.log('successfully called chat');
                    },
                    error: function error() {
                        // called when there is an error
                        console.log('error calling chat');
                    }
                });

                session.signal({
                    to: allConnections[connectionId],
                    type: 'msg-private',
                    data: uname + ': ' + msg
                }, function signalCallback(error) {
                    if (error) {
                        console.error('Error sending signal:', error.name, error.message);
                    } else {
                        msgTxt.value = '';
                    }
                });
            }

            function shareScreen(){
                OT.checkScreenSharingCapability(function(response) {
                    if(!response.supported || response.extensionRegistered === false) {
                        // This browser does not support screen sharing.
                    } else if (response.extensionInstalled === false) {
                        // Prompt to install the extension.
                    } else {
                        // Create a publisher for screen
                        var publisherScreen = OT.initPublisher('screen-preview', {
                            maxResolution:{width: 1920, height: 1080},
                            videoSource: 'screen'
                        }, handleError);

                        session.publish(publisherScreen, function(error) {
                            if (error) {
                                // Look error.message to see what went wrong.
                            }
                            else{
                                document.getElementById("publisher").style.display = "none";
                                document.getElementById("subscriber").style.display = "none";
                            }
                        });

                        publisherScreen.on('streamDestroyed', function(event) {
                            document.getElementById("publisher").style.display = "block";
                            document.getElementById("subscriber").style.display = "block";
                            var node = document.createElement("div");
                            node.setAttribute("id", "screen-preview");
                            document.getElementById("videos").appendChild(node);
                            if (event.reason === 'mediaStopped') {
                                // User clicked stop sharing
                            } else if (event.reason === 'forceUnpublished') {
                                // A moderator forced the user to stop sharing.
                            }
                        });
                    }
                });
            }

            function startPolling(){
                session.signal({
                    type: 'poll-start',
                    data: ''
                }, function signalCallback(error) {
                    if (error) {
                        console.error('Error sending signal:', error.name, error.message);
                    } else {
                        console.log("polling started");
                    }
                });
            }

            function stopPolling(){
                session.signal({
                    type: 'poll-stop',
                    data: ''
                }, function signalCallback(error) {
                    if (error) {
                        console.error('Error sending signal:', error.name, error.message);
                    } else {
                        console.log("polling stopped");
                    }
                });
            }

            // Text chat
            var form = document.querySelector('form');
            var msgTxt = document.querySelector('#msgTxt');
            var username = document.querySelector('#username');

            // Send a signal once the user enters data in the form
            form.addEventListener('submit', function submit(event) {
                event.preventDefault();
                $.ajax({
                    url: '/tokbox/chat',
                    type: 'POST',
                    contentType: 'application/json', // send as JSON
                    data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId, 'to': 'All', 'msg': msgTxt.value}),
                    complete: function complete() {
                        // called when complete
                        console.log('chat function completed');
                    },
                    success: function success() {
                        // called when successful
                        console.log('successfully called chat');
                    },
                    error: function error() {
                        // called when there is an error
                        console.log('error calling chat');
                    }
                });
                session.signal({
                    type: 'msg',
                    data: username.value + ': ' + msgTxt.value
                }, function signalCallback(error) {
                    if (error) {
                        console.error('Error sending signal:', error.name, error.message);
                    } else {
                        msgTxt.value = '';
                    }
                });
            });
            // Start recording
            function startArchive() { // eslint-disable-line no-unused-vars
                $.ajax({
                    url: '/tokbox/startArchive',
                    type: 'POST',
                    contentType: 'application/json', // send as JSON
                    data: JSON.stringify({'sessionId': sessionId, 'tokboxId': tokboxId}),
                    complete: function complete() {
                        // called when complete
                        console.log('startArchive() complete');
                    },
                    success: function success() {
                        // called when successful
                        console.log('successfully called startArchive()');
                    },
                    error: function error() {
                        // called when there is an error
                        console.log('error calling startArchive()');
                    }
                });

                $('#start').hide();
                $('#stop').show();
            }

            // Stop recording
            function stopArchive() { // eslint-disable-line no-unused-vars
                $.post('/tokbox/stopArchive/' + archiveID);
                $('#stop').hide();
                $('#view').prop('disabled', false);
                $('#stop').show();
            }

            // Get the archive status. If it is  "available", download it. Otherwise, keep checking
            // every 5 secs until it is "available"
            function viewArchive() { // eslint-disable-line no-unused-vars
                $('#view').prop('disabled', true);
                window.location = '/tokbox/viewArchive/' + archiveID;
            }

            $('#start').show();
            $('#view').hide();
            // (optional) add server code here
            initializeSession();
        </script>

        <script type="text/javascript">
            function leaveMeeting(){
                window.open('/test/thankyou', '_self');
            }
            // Array of all the questions and choices to populate the questions. This might be saved in some JSON file or a database and we would have to read the data in.
            /*var all_questions = [
                {
                    question_string: "What color is the sky?",
                    choices: {
                        correct: "Blue",
                        wrong: ["Pink", "Orange", "Green"]
                    }
                },
                {
                    question_string: 'Who is the main character of Harry Potter?',
                    choices: {
                        correct: "Harry Potter",
                        wrong: ["Hermione Granger", "Ron Weasley", "Voldemort"]
                    }
                }
            ];*/

            var all_questions = <?=$polling_questions?>

            var Quiz = function(quiz_name) {
                // Private fields for an instance of a Quiz object.
                this.quiz_name = quiz_name;
                // This one will contain an array of Question objects in the order that the questions will be presented.
                this.questions = [];
            }

            // A function that you can enact on an instance of a quiz object. This function is called add_question() and takes in a Question object which it will add to the questions field.
            Quiz.prototype.add_question = function(question) {
                // Randomly choose where to add question
                var index_to_add_question = Math.floor(Math.random() * this.questions.length);
                this.questions.splice(index_to_add_question, 0, question);
            }

            // A function that you can enact on an instance of a quiz object. This function is called render() and takes in a variable called the container, which is the <div> that I will render the quiz in.
            Quiz.prototype.render = function(container) {
                // For when we're out of scope
                var self = this;

                // Hide the quiz results modal
                $('#quiz-results').hide();

                // Write the name of the quiz
                $('#quiz-name').text(this.quiz_name);

                // Create a container for questions
                var question_container = $('<div>').attr('id', 'question').insertAfter('#quiz-name');

                // Helper function for changing the question and updating the buttons
                function change_question() {
                    self.questions[current_question_index].render(question_container);
                    $('#prev-question-button').prop('disabled', current_question_index === 0);
                    $('#next-question-button').prop('disabled', current_question_index === self.questions.length - 1);

                    // Determine if all questions have been answered
                    var all_questions_answered = true;
                    for (var i = 0; i < self.questions.length; i++) {
                        if (self.questions[i].user_choice_index === null) {
                            all_questions_answered = false;
                            break;
                        }
                    }
                    $('#submit-button').prop('disabled', !all_questions_answered);
                }

                // Render the first question
                var current_question_index = 0;
                change_question();

                // Add listener for the previous question button
                $('#prev-question-button').click(function() {
                    if (current_question_index > 0) {
                        current_question_index--;
                        change_question();
                    }
                });

                // Add listener for the next question button
                $('#next-question-button').click(function() {
                    if (current_question_index < self.questions.length - 1) {
                        current_question_index++;
                        change_question();
                    }
                });

                // Add listener for the submit answers button
                $('#submit-button').click(function() {
                    // Determine how many questions the user got right
                    var score = 0;
                    for (var i = 0; i < self.questions.length; i++) {
                        if (self.questions[i].user_choice_index === self.questions[i].correct_choice_index) {
                            score++;
                        }
                    }

                    console.log(self.questions);
                    jQuery.ajax({
                        type: "POST",
                        contentType: "application/json",
                        data: JSON.stringify(self.questions),
                        url: "/tokbox/poll",
                      success: function(response) {
                            console.log(response);
                        }
                    });

                    // Display the score with the appropriate message
                    var percentage = score / self.questions.length;
                    console.log(percentage);
                    var message;
                    if (percentage === 1) {
                        message = 'Great job!'
                    }
                    else if (percentage >= .75) {
                        message = 'You did alright.'
                    }
                    else if (percentage >= .5) {
                        message = 'Better luck next time.'
                    }
                    else {
                        message = 'Maybe you should try a little harder.'
                    }
                    $('#quiz-results-message').text(message);
                    $('#quiz-results-score').html('You got <b>' + score + '/' + self.questions.length + '</b> questions correct.');
                    $('#quiz-results').slideDown();
                    $('#submit-button').slideUp();
                    $('#next-question-button').slideUp();
                    $('#prev-question-button').slideUp();
                    $('#quiz-retry-button').slideDown();

                });

                // Add a listener on the questions container to listen for user select changes. This is for determining whether we can submit answers or not.
                question_container.bind('user-select-change', function() {
                    var all_questions_answered = true;
                    for (var i = 0; i < self.questions.length; i++) {
                        if (self.questions[i].user_choice_index === null) {
                            all_questions_answered = false;
                            break;
                        }
                    }
                    $('#submit-button').prop('disabled', !all_questions_answered);
                });
            }

            // An object for a Question, which contains the question, the correct choice, and wrong choices. This block is the constructor.
            var Question = function(question_obj) {
                //console.log("All Questions ", all_questions);
                // Private fields for an instance of a Question object.
                var question_string = question_obj.question_string;
                var correct_choice = question_obj.choices.correct;
                var wrong_choices = question_obj.choices.wrong;
                var question_id = question_obj.id;

                this.question_string = question_string;
                this.choices = [];
                this.user_choice_index = null; // Index of the user's choice selection
                this.correct_ans = correct_choice;
                this.question_id = question_id;
                this.user_ans = null;

                // Random assign the correct choice an index
                this.correct_choice_index = Math.floor(Math.random() * (wrong_choices.length + 1));
                //console.log(this.correct_choice_index, wrong_choices.length + 1);

                // Fill in this.choices with the choices
                var number_of_choices = wrong_choices.length + 1;
                for (var i = 0; i < number_of_choices; i++) {
                    if (i === this.correct_choice_index) {
                        this.choices[i] = correct_choice;
                    }
                    else {
                        // Randomly pick a wrong choice to put in this index
                        var wrong_choice_index = Math.floor(Math.random() * (wrong_choices.length));;
                        this.choices[i] = wrong_choices[wrong_choice_index];

                        // Remove the wrong choice from the wrong choice array so that we don't pick it again
                        wrong_choices.splice(wrong_choice_index, 1);
                    }
                }
                //console.log(this.choices);
            }

            // A function that you can enact on an instance of a question object. This function is called render() and takes in a variable called the container, which is the <div> that I will render the question in. This question will "return" with the score when the question has been answered.
            Question.prototype.render = function(container) {
                // For when we're out of scope
                var self = this;

                // Fill out the question label
                var question_string_h2;
                if (container.children('h2').length === 0) {
                    question_string_h2 = $('<h2>').appendTo(container);
                }
                else {
                    question_string_h2 = container.children('h2').first();
                }
                question_string_h2.text(this.question_string);

                // Clear any radio buttons and create new ones
                if (container.children('input[type=radio]').length > 0) {
                    container.children('input[type=radio]').each(function() {
                        var radio_button_id = $(this).attr('id');
                        $(this).remove();
                        container.children('label[for=' + radio_button_id + ']').remove();
                    });
                }
                for (var i = 0; i < this.choices.length; i++) {
                    // Create the radio button
                    var choice_radio_button = $('<input>')
                    .attr('id', 'choices-' + i)
                    .attr('type', 'radio')
                    .attr('name', 'choices')
                    .attr('value', 'choices-' + i)
                    .attr('checked', i === this.user_choice_index)
                    .appendTo(container);

                    // Create the label
                    var choice_label = $('<label>')
                    .text(this.choices[i])
                    .attr('for', 'choices-' + i)
                    .appendTo(container);
                }

                // Add a listener for the radio button to change which one the user has clicked on
                $('input[name=choices]').change(function(index) {
                    var selected_radio_button_value = $('input[name=choices]:checked').val();

                    // Change the user choice index
                    self.user_choice_index = parseInt(selected_radio_button_value.substr(selected_radio_button_value.length - 1, 1));
                    self.user_ans = self.choices[self.user_choice_index];

                    // Trigger a user-select-change
                    container.trigger('user-select-change');
                });
            }

            // "Main method" which will create all the objects and render the Quiz.
            $(document).ready(function() {
                // Create an instance of the Quiz object
                var quiz = new Quiz('ANWER THE FOLLOWING');

                // Create Question objects from all_questions and add them to the Quiz object
                for (var i = 0; i < all_questions.length; i++) {
                    // Create a new Question object
                    var question = new Question(all_questions[i]);

                    // Add the question to the instance of the Quiz object that we created previously
                    quiz.add_question(question);
                }

                // Render the quiz
                var quiz_container = $('#quiz');
                quiz.render(quiz_container);
            });


        </script>
    </body>
</html>
