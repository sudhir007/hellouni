<?php $leaddata=$this->session->userdata('leaduser'); ?>
   
    <style>
   	 .shade_border{
		background:url(img/shade1.png) repeat-x; height:11px;
	}
	.search_by{
	background-color:#f3f4f4; box-shadow:5px 5px 5px #eae8e8; height:auto;
	    padding-top: 10px;
    padding-left: 30px;
	 padding-right: 30px;
	}
	.search_by h4{
		color:#3c3c3c;
		font-family:lato_regular;
		font-weight:500;
	}
	.table_heading{
		padding:5px;
		font-weight:bold;
	}
	.more_detail{
	background-color:#F6881F;
	text-transform:uppercase;
	height:28px;	
	border:0px;
	  color:#000; 
		border-radius:2px;
		width:25px;
}
.right_side_form{
background:#3c3c3c;
box-shadow:5px 5px 2px #CCCCCC; 
height:auto;

}
label{
	color:#818182;
	font-size:11px;
font-weight:300;	
margin-top:5px;
}
.input_field{
	
width:215px;
height:25px;	
}
.more_detail{
	background-color:#F6881F;
	text-transform:uppercase;
	height:28px;	
	border:0px;
	  color:#fff; 
		border-radius:2px;
		
}

#input_container { position:relative; padding:0; margin:0;     padding-top: 25px;
    }
#input { margin:0; padding-left: 62px; }

#input_img { position:absolute; bottom:150px; left:15px; width:50px; height:27px; margin-left:-5px;}

#input2 { margin:0; padding-left: 62px; }

#input_img2 { position:absolute; bottom:100px; left:15px; width:50px; height:27px; }
#input3 { margin:0; padding-left: 62px; }

#input_img3 { position:absolute; bottom:163px; left:15px; width:50px; height:27px; }
.login_background{
	background-color:#f3f4f6; height:auto;
}
.form_font_color{
	color:#818182;
}
.form_font_color a{
	color:#818182;
}

.button_sign_in{
	background-color:#F6881F; border:none;  width: 100px;height: 28px;
	margin-bottom:20px;
}
.sign_line{
background:url(img/line2.png) repeat-y; height:100px;
}
.login_head{
color:#F6881F;padding-left:0px;
}
.login_head2{
text-transform:capitalize; color:#3C3C3C;"
}
    </style>

</head>

<body>

    <!-- Navigation -->
    <style>
.header_icons{
}
.header_icons ul li{
	display:inline-block;
	list-style-type:none;
	margin-bottom:-10px;
	
}
.sep{
    margin-top: 20px;
    margin-bottom: 20px;
   
    
}
.invalid_color{
color:red;
}
</style>

  
      <div id="page_loader">  <div class="container hidden-xs" style="padding: 17px; " > 
            <img src="<?php echo base_url();?>img/page1.png" width="100%"  />
          
       </div>
        <div class="container visible-xs " style="text-align:center;">
           
           <img src="img/page1-1.jpg" />
       </div>
  
  	 <div class="shade_border"></div>
 	
   
        <div class="container login_background" >
        	<div class="col-lg-12 col-md-12"> <h4 class="form_font_color">Update Your Profile </h4></div>
           		<div class="col-lg-12 col-md-12">
				<?php if($this->session->flashdata('flash_message')) { ?>
				<h4 class="invalid_color"><?php echo $this->session->flashdata('flash_message');?></h4>
				<?php }?>
				
                	 <h4 class="login_head">Personal Details</h4>
                    
                  
                    <form action="<?php echo base_url()?>user/account/save" class="form-group form-inline" id="input_container" method="post"  >
                    	<div class="form-group col-md-6 " >
                        	<label class="col-md-3">Name</label>
							<?php if($fid){?>
                        	<span class="form-control form_font_color input_field"><?php if($name) echo $name;?></span>
                           <?php }else{?>
						   <input type="text" name="name" placeholder=""  class="form-control form_font_color input_field" value="<?php if($name) echo $name;?>" />
						   <?php } ?>
                        </div>
                    	
                        <div class="form-group col-md-6">
                        <label class="col-md-3">DOB</label>
                      <input type="text" name="dob"  class="form-control form_font_color input_field" value="<?php if($dob) echo $dob;?>"/>
                      </div>
                      <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span>  
                      <div class="form-group col-md-6 " >
                        	<label class="col-md-3">Email</label>
							<?php if($fid){?>
                        	<span class="form-control form_font_color input_field"><?php if($email) echo $email;?></span>
                           <?php }else{?>
                        	<input type="text" name="email"class="form-control form_font_color input_field" value="<?php if($email) echo $email;?>" />
                            <?php } ?>
                        </div>
                    	
                        <div class="form-group col-md-6">
                        <label class="col-md-3">Phone No.</label>
                      <input type="text" name="phone"    class="form-control form_font_color input_field" value="<?php if($phone) echo $phone;?>"/>
                      </div>
                       <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span>  
                      <div class="form-group col-md-6 " >
                        	<label class="col-md-3">Address</label>
                   <textarea type="text" name="address"   class="form-control form_font_color input_field " style="width:60%; height:100px;"  ><?php if($address) echo $address;?></textarea>
                           
                        </div>
                    	
                        <div class="form-group col-md-6">
                       <div class="col-md-12" style="padding-left:0px;"><label class="col-md-3" >Country</label>
                      <input type="text" name="country"   class="form-control form_font_color input_field" value="<?php if($country) echo $country;?>"/></div>
                        <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span>
                      <div class="col-md-12" style="padding-left:0px;"><label class="col-md-3" >City</label>
                        	<input type="text" name="city"   class="form-control form_font_color input_field" value="<?php if($city) echo $city;?>" /></div>
                      </div>
                       <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span>  
                      
                    	
                        <div class="form-group col-md-6">
                        <label class="col-md-3">Zipcode</label>
                      <input type="text" name="zip"    class="form-control form_font_color input_field" value="<?php if($zip) echo $zip;?>"/>
                      </div>
                         <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span> 
                         
                      <div class="form-group col-lg-6" >
                        	<label class="col-md-3">Password</label>
                        	<input type="password" name="password"  class="form-control form_font_color input_field" />
                           
                        </div>
                    	<div class="clearfix"></div>
                        <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span> 
                        <div class="form-group col-lg-6 ">
                        <label class="col-md-3">Retype Password</label>
                      <input type="password" name="password2"    class="form-control form_font_color input_field"/>
                      </div>
                       
                        <div class="clearfix"></div>
                        <span class="sep hidden-xs visible-sm visible-lg visible-md col-lg-12"></span> 
                       <div class="form-group">
            <button type="submit" class="button_sign_in">UPDATE</button>&nbsp;
                        	
                       </div>
                    </form>
                    
                </div>
                
                
                
           
         
     </div>	
   </div>
   <br/>

   