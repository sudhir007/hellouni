<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model 
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 
  <Reverse bidding system> 
    Copyright (C) <2009>  <FreeLance PHP Script>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
    If you want more information, please email me at support@freelancephpscript.com or 
    contact us from http://www.freelancephpscript.com/contact 
 
 
 */
	 class Package_model extends CI_Model {
	 
 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
	function insertPackage($insertData=array())
	 {
	  		
	
			
	 $description_tbl_data = array(
     'name' => $insertData['title'],
     'description' => $insertData['description']
     );
	 
	 $this->db->insert('infra_description_master', $description_tbl_data);
	 $description_id = $this->db->insert_id();
	 
	 // insert into seo_master
	 $seomaster_tbl_data = array(
	 'meta_tag' 			=> $insertData['meta_tag'],
     'meta_description' 	=> $insertData['meta_description']
     
     );
	 $this->db->insert('seo_master', $seomaster_tbl_data);
	 $seomaster_id = $this->db->insert_id();
	 
	 
	 $package_tbl_data = array(
     'description_id' =>  $description_id,
	 'seo_id'			=> $seomaster_id,
     'start_date' => $insertData['start_date'],
	 'end_date' => $insertData['end_date'],
	 'status' => $insertData['status'],
	 'virtual_tour' => $insertData['virtual_tour']
	 );
	 
	 $this->db->insert('package_master', $package_tbl_data);
	 
	 $package_id = $this->db->insert_id(); 
	 // insert into gallery_to_package	(photo)
	 
	 foreach ($insertData['photo'] as $key1){
	 $gallery_to_package_data1 = array(
	 'gallery_id' => $key1,
     'package_id' => $package_id
      );
	 $this->db->insert('gallery_to_package', $gallery_to_package_data1);
	 }
	 // insert into gallery_to_package	(video)
	 if($insertData['video']){
	 foreach ($insertData['video'] as $key2){
	 $gallery_to_package_data2 = array(
	 'gallery_id' => $key2,
     'property_id' => $package_id
      );
	 $this->db->insert('gallery_to_package', $gallery_to_package_data2);
	 }
	 }

	 
	 if($insertData['attr']){
	   foreach ($insertData['attr'] as $attribute){
	  
	    $package_to_attribute = array(
	 	'package_id' 	=> $package_id,
     	'attribute_id' 	=> $attribute['attr_id'],
	 	'description' 	=> $attribute['value'],
		'sort_order' 	=> $attribute['sort_order'],
		'status' 		=> '1'
        );
	   $this->db->insert('package_to_attribute', $package_to_attribute);
	   }
	 }
	 
	
	 return 'success';
	 }
	 
	 
	 function editPackage($id,$insertData=array())
	 {
	  	
	 
	  $description_tbl_data = array(
       'name' => $insertData['title'],
       'description' => $insertData['description'],
	   
       );
	 
	 $package_details = $this->db->get_where('package_master', array('package_master.id' => $id))->row();
	
	
	 $description_id = $package_details->description_id;
	 
	 
	 // update infra_description_master
	 $this->db->where('id',$description_id);
	 $this->db->update('infra_description_master',$description_tbl_data);
	 
	 // update seo_master
	  $seomaster_tbl_data = array(
	 'meta_tag' 			=> $insertData['meta_tag'],
     'meta_description' 	=> $insertData['meta_description']
     
     );
	 $this->db->where('id',$package_details->seo_id);
	 $this->db->update('seo_master',$seomaster_tbl_data);
	 
	 
	 
	 $package_master_tbl_data = array(
	 'start_date' => $insertData['start_date'],
	 'end_date' => $insertData['end_date'],
	 'status' => $insertData['status'],
	 'virtual_tour' => $insertData['virtual_tour']
	 );
	 
	 // update amenities_master
	 $this->db->where('id',$id);
	 $this->db->update('package_master',$package_master_tbl_data);
	 
	 
	 
	 $this->db->delete('gallery_to_package', array('package_id' => $id)); 
	 
	  // insert into gallery_to_room	(photo)
	 if($insertData['photo']){
	 foreach ($insertData['photo'] as $key1){
	 $photodata = array(
	 'gallery_id' => $key1,
     'package_id' => $id
      );
	 $this->db->insert('gallery_to_package', $photodata);
	 }
	 }
	
	 // insert into gallery_to_property	(video)
	 if($insertData['video']){
	 foreach ($insertData['video'] as $key2){
	 $videodata = array(
	 'gallery_id' => $key2,
     'package_id' => $id
      );
	 $this->db->insert('gallery_to_package', $videodata);
	 }
	 }
	 
	 $this->db->delete('package_to_attribute', array('package_id' => $id)); 
	  
	 if($insertData['attr']){
	   foreach ($insertData['attr'] as $attribute){
	  
	    $package_to_attribute = array(
	 	'package_id' 	=> $id,
     	'attribute_id' 	=> $attribute['attr_id'],
	 	'description' 	=> $attribute['value'],
		'sort_order' 	=> $attribute['sort_order'],
		'status' 		=> '1'
        );
	   $this->db->insert('package_to_attribute', $package_to_attribute);
	   }
	 }
	 
	 return 'success';
	 
    }
	  
	  
	  
	  
	 function getPackageByid($conditions)
     { 
	 $this->db->where($conditions);
	 $this->db->from('package_master');
	 $this->db->join('infra_description_master', 'package_master.description_id = infra_description_master.id','left');
	 $this->db->join('infra_status_master', 'infra_status_master.id = package_master.status','left');
	 
	 $this->db->select('package_master.id,package_master.seo_id,package_master.virtual_tour,infra_description_master.name,infra_description_master.description,package_master.start_date,package_master.end_date, infra_status_master.name as status');
	 
	 $result=$this->db->get()->row();

	 return $result;
	 
    } 
	  
	  
	  
	  function getPackages($condition1,$condition2)
     { 
	  if(count($condition1)>0 || count($condition2)>0  || isset($condition1) || isset($condition2)){
	  $this->db->where($condition1);
	  $this->db->or_where($condition2);
	  }
	 
	 $this->db->from('package_master');
	 $this->db->join('infra_description_master', 'package_master.description_id = infra_description_master.id','left');
	 $this->db->join('infra_status_master', 'infra_status_master.id = package_master.status','left');
	 
	 
	 
	 $this->db->select('package_master.id,package_master.description_id,package_master.start_date, package_master.end_date, infra_description_master.name,infra_description_master.description, infra_description_master.id as desc_id,infra_status_master.name as status');
	 
	 $result=$this->db->get()->result();
	 
	
	 return $result;
	 
    } 
	  
	  function getAttributes_BY_Package($id){
	  
	 $conditions = array('package_to_attribute.package_id' =>$id,'attribute_master.status' => '1');
	 $this->db->where($conditions);
	 $this->db->from('package_to_attribute');
	 $this->db->join('attribute_master', 'attribute_master.id = package_to_attribute.attribute_id','left');
	 $this->db->select('package_to_attribute.id,package_to_attribute.package_id,package_to_attribute.attribute_id,package_to_attribute.description,package_to_attribute.sort_order,package_to_attribute.status');
	 
	 $result=$this->db->get()->result();
	 return $result;
	  
	  }
	  
	  
	  function deletePackage($id)
	 {
	  $data = array('status' => '4');  
	   
	 $this->db->where('id',$id);
	 $this->db->update('package_master',$data);
	 return 'success';
	 }
	  
	  
	 
}
	
	
	
	
	
	
	
	
	
	
	
	
	
?>