<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model 
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 
  <Reverse bidding system> 
    Copyright (C) <2009>  <FreeLance PHP Script>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
    If you want more information, please email me at support@freelancephpscript.com or 
    contact us from http://www.freelancephpscript.com/contact 
 
 
 */
	 class Amenity_model extends CI_Model {
	 
 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
	function insertAmenity($insertData=array())
	 {
	  		
	 $description_tbl_data = array(
     'name' => $insertData['name'],
     'description' => $insertData['description']
     );
	 
	 $this->db->insert('infra_description_master', $description_tbl_data);
	 $description_id = $this->db->insert_id();
	 
	 $amenity_tbl_data = array(
     'description_id' =>  $description_id,
     'sort_order' => $insertData['sort_order'],
	 'type_id' => $insertData['type_id'],
	 'for' => $insertData['for']
	 );
	 
	 $this->db->insert('amenities_master', $amenity_tbl_data);
	
	 return 'success';
	 }
	 
	 
	 
	 
	 function getAmenities($condition1,$condition2)
     { 
	 if(count($condition1)>0 || count($condition2)>0  || isset($condition1) || isset($condition2)){
	  $this->db->where($condition1);
	  $this->db->or_where($condition2);
	  }
	 $this->db->from('amenities_master');
	 $this->db->join('infra_description_master', 'amenities_master.description_id = infra_description_master.id','left');
	 
	 
	 
	 
	 $this->db->select('amenities_master.id,amenities_master.description_id,amenities_master.sort_order,infra_description_master.name,infra_description_master.description, infra_description_master.id as desc_id');
	 
	 $result=$this->db->get()->result();
	 
	
	 return $result;
	 
    }
	
	 function getAmenities_by_Attribute($conditions)
     { 
	
	 $this->db->where($conditions);
	 $this->db->from('amenities_master');
	 $this->db->join('infra_description_master', 'amenities_master.description_id = infra_description_master.id','left');
	 
	 $this->db->select('amenities_master.id,amenities_master.description_id,amenities_master.sort_order,infra_description_master.name,infra_description_master.description, infra_description_master.id as desc_id');
	 
	 $result=$this->db->get()->result();
	 
	 return $result;
	 
    }
	
	
	
	function getAmenities_from_amenity_to_property($conditions)
     { 
	
	 $this->db->where($conditions);
	 $this->db->from('amenity_to_property');
	 $this->db->join('amenities_master', 'amenities_master.id = amenity_to_property.amenity_id','left');
	 $this->db->join('infra_description_master', 'amenities_master.description_id = infra_description_master.id','left');
	 
	 $this->db->select('amenities_master.id,amenities_master.description_id,amenities_master.sort_order,infra_description_master.name,infra_description_master.description, infra_description_master.id as desc_id');
	 
	 $result=$this->db->get()->result();
	 
	 return $result;
	 
    }
	
	
	
	
	function getAmenityByid($conditions)
     { 
	 $this->db->where($conditions);
	 $this->db->from('amenities_master');
	 $this->db->join('infra_description_master', 'amenities_master.description_id = infra_description_master.id','left');
	 $this->db->join('amenities_type_master', 'amenities_type_master.id = amenities_master.type_id','left');
	 
	 $this->db->select('amenities_master.id,amenities_master.description_id,amenities_master.sort_order,infra_description_master.name,infra_description_master.description, infra_description_master.id as desc_id,amenities_master.type_id, amenities_master.for');
	 
	 $result=$this->db->get()->result();

	 return $result;
	 
    }
	 
	 
	 
	 function getContactmaster_info($conditions)
     { 
	    $query = $this->db->get_where('infra_contact_master', $conditions)->result();
		return $query; 
		 
	 }
	 
	 
	 
	 
	 function editAmenity($insertData=array())
	 {
	   $description_tbl_data = array(
       'name' => $insertData['name'],
       'description' => $insertData['description'],
	   
       );
	 
	 $amenity_details = $this->db->get_where('amenities_master', array('amenities_master.id' => $insertData['amenity_id']))->result();
	
	 foreach($amenity_details as $key):
	 $description_id = $key->description_id;
	 endforeach;
	 
	 // update infra_description_master
	 $this->db->where('id',$description_id);
	 $this->db->update('infra_description_master',$description_tbl_data);
	 
	 $amenity_master_tbl_data = array(
	 'sort_order' => $insertData['sort_order'],
	 'type_id' => $insertData['type_id'],
	 'for' => $insertData['for']
	 );
	 
	 // update amenities_master
	 $this->db->where('id',$insertData['amenity_id']);
	 $this->db->update('amenities_master',$amenity_master_tbl_data);
	 
	 return 'success';
	 }
	 
	 
	 /*function deleteAmenity($id)
	 {
	   
	 $amenity_details = $this->db->get_where('amenities_master', array('amenities_master.id' => $id))->result();
	
	 foreach($amenity_details as $amenity):
	 $description_id = $amenity->description_id;
	 
	 endforeach;
	 
	 
	 $this->db->delete('infra_description_master', array('id' => $description_id)); 
	 $this->db->delete('amenities_master', array('id' => $id)); 
	 return 'success';
	 }*/
	
	
	function deleteAmenity($id)
	 {
	  $data = array('status' => '4');  
	   
	 $this->db->where('id',$id);
	 $this->db->update('amenities_master',$data);
	 return 'success';
	 }
	
	
	function getAmenitiesType()
     { 
	 $this->db->from('amenities_type_master');
	 $this->db->select('*');
	 
	 $result=$this->db->get()->result();
	 
	
	 return $result;
	 
    }
	
	 
}
	
	
	
	
	
	
	
	
	
	
	
	
	
?>