<?php
class Uta_model extends CI_Model
{
    function insertIp($data)
    {
        $this->db->insert('seminar_ips', $data);
    }

    function deleteIp($userId)
    {
        $this->db->delete('seminar_ips', array('user_id' => $userId));
    }

    function insertConnection($data)
    {
        $this->db->insert('seminar_connections', $data);
    }

    function deleteConnection($userId)
    {
        $this->db->delete('seminar_connections', array('user_id' => $userId));
    }

    function updateConnection($userId, $data)
    {
        $this->db->where('user_id', $userId);
        $this->db->update('seminar_connections', $data);
    }

    function validUserInfo($userEmailId, $userMobileNumber)
    {
        $query1 = "SELECT username, email FROM user_master WHERE email = '$userEmailId' OR phone = '$userMobileNumber' ;";
        $query_1 = $this->db->query($query1);
        $result = $query_1->result_array();
    	return $result;
    }

    function storeChat($data)
    {
        $this->db->insert('seminar_chat', $data);
    }

    function getUserByUsername($username)
    {
        $this->db->from('user_master');
        $this->db->where('username', $username);
        $result = $this->db->get();
        return $result->row_array();
    }

    function insertUser($data)
    {
        $this->db->insert('user_master', $data);
        return $this->db->insert_id();
    }

    function addGuest($data)
    {
        $checkGuest = $this->checkGuest($data['email'], $data['session_id']);
        if($checkGuest)
        {
            return $checkGuest;
        }

        $this->db->insert('guests', $data);
        return $this->checkGuest($data['email'], $data['session_id']);
    }

    function checkGuest($email, $sessionId)
    {
        $this->db->from('guests');
        $this->db->where('email', $email);
        $this->db->where('session_id', $sessionId);
        return $this->db->get()->row_array();
    }

    function getGuestsCount()
    {
        $this->db->select('count(*) as count');
        $this->db->from('guests');
        return $this->db->get()->row_array();
    }
}
