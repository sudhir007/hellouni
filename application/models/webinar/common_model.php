<?php


class Common_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }


     function post($data, $tableName) {

    $insert_query = $this->db->insert($tableName, $data);

    return "1";
    //$error = $this->db->error();
    $lastInsertedId = 0;
    if ($insert_query)
    {
        $lastInsertedId = $this->db->insert_id();
    }

    $where = ['id' => $lastInsertedId];
    $data = $this->get($where, $tableName);



    return [
        "data" => $data,
        "last_inserted_id" => $lastInsertedId
    ];
}

   function get($where = array(), $tableName) {

        $this->db->select('*');
        $this->db->from($tableName);
        $this->db->where($where);

        $query = $this->db->get();
        $data = $query->result_array();

        return $response = [
            'data' => $data
        ];
    }


   function put ($where = array(), $data = array(), $tableName) {

        $this->db->where($where);
        $this->db->update($tableName, $data);
        //echo $this->db->last_query();

        $this->db->select('*');
        $this->db->from($tableName);
        $this->db->where($where);
        $query = $this->db->get();
        $data = $query->result_array();

        return $data ;
    }

    public function upsert ($wheredata = array(), $insertData = array(), $tableName) {

      $this->db->select('*');
      $this->db->from($tableName);
      $this->db->where($wheredata);
      $query = $this->db->get();
      //$error_result = $this->db->error();
      $otpData = $query->result_array();
      //return $query->num_rows();

      if($query->num_rows() === 0) {

          $insert_query = $this->db->insert($tableName, $insertData);
          //return $insert_query;
          $data = $this->db->insert_id();

      } else {

      $this->db->where($wheredata);
      $this->db->update($tableName, $insertData);

      $data = "1";

      }

        return $response = [
            'data' => $data,
            "last_inserted_id" => "1"
        //    'error' => $error_result
        ];
    }

    public function delete($where = array(), $tableName){

  		$this->db-> where($where);
      $this->db-> delete($tableName);

  		return [
        "data" => 1
  	];

  	}

    function getAppliedUniversitiesByUser($userId,$appId)
  {
    $this->db->select('users_applied_universities.*, universities.name, universities.slug, universities.website, user_master.email, universities.banner, colleges.name AS college_name, courses.name AS course_name, fa.name AS facilitator_name, fa.last_name AS facilitator_last_name,fa.email AS facilitator_email, fa.phone AS facilitator_mobile');
    $this->db->from('users_applied_universities');
    $this->db->join('universities', 'universities.id = users_applied_universities.university_id');
    $this->db->join('user_master', 'user_master.id = universities.user_id');
    $this->db->join('colleges', 'colleges.id = users_applied_universities.college_id', 'left');
    $this->db->join('courses', 'courses.id = users_applied_universities.course_id', 'left');
    $this->db->join('user_master fa', 'fa.id = users_applied_universities.facilitator_id', 'left');
    $this->db->where('users_applied_universities.user_id', $userId);
    $this->db->where('users_applied_universities.id', $appId);
    $result = $this->db->get();
    return $result->result_array();
  }

}

?>
