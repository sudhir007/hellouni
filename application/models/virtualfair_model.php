<?php
class Virtualfair_model extends CI_Model
{
    function getByTokenId($tokenId)
    {
        $this->db->from('virtual_fair_participants');
        $this->db->where('token_id', $tokenId);
        return $this->db->get()->row();
    }

    function getParticipantById($id)
    {
        $this->db->from('virtual_fair_participants');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    function getSlotById($slotId)
    {
        $this->db->from('virtual_fair_slots');
        $this->db->where('id', $slotId);
        return $this->db->get()->row();
    }

    function getSlotByParticipantTime($participantId, $time)
    {
        $this->db->from('virtual_fair_slots');
        $this->db->where('participant_id', $participantId);
        $this->db->where('start_time <=', $time);
        $this->db->where('end_time >', $time);
        $result = $this->db->get()->result();
        return $result;
    }

    function getSlotsByParticipant($participantId)
    {
        $this->db->from('virtual_fair_slots');
        $this->db->where('participant_id', $participantId);
        $result = $this->db->get()->result_array();
        return $result;
    }

    function updateSlot($slotId, $data)
    {
        $this->db->where('id', $slotId);
        $this->db->update('virtual_fair_slots', $data);
    }

    function updateSlotByParticipantId($participantId, $data)
    {
        $this->db->where('participant_id', $participantId);
        $this->db->update('virtual_fair_slots', $data);
    }

    function updateParticipant($participantId, $data)
    {
        $this->db->where('id', $participantId);
        $this->db->update('virtual_fair_participants', $data);
    }

    function deleteCounselorBooking($userId)
    {
        $this->db->where('user_id', $userId);
        $this->db->delete('virtualfailr_counselor_booking');
    }

    function checkRegistration($userId, $virtualFairId, $participantId)
    {
        $this->db->from('virtualfair_registrations');
        $this->db->where('user_id', $userId);
        $this->db->where('participant_id', $participantId);
        $this->db->where('virtual_fair_id', $virtualFairId);
        return $this->db->get()->row();
    }

    function registerVirtualFair($data)
    {
        $this->db->insert('virtualfair_registrations', $data);
    }

    function updateVirtualFairRegister($userId, $participantId, $data)
    {
        $this->db->where('user_id', $userId);
        $this->db->where('participant_id', $participantId);
        $this->db->update('virtualfair_registrations', $data);
    }

    function getStudentDetailByUserId($userId)
    {
        $this->db->from('student_details');
        $this->db->where('user_id', $userId);
        return $this->db->get()->row();
    }

    function checkHelp($userId, $tokenId, $type)
    {
        $this->db->from('virtual_fair_help');
        $this->db->where('user_id', $userId);
        $this->db->where('token_id', $tokenId);
        $this->db->where('type', $type);
        $this->db->where('status', '0');
        return $this->db->get()->row();
    }

    function addHelp($data)
    {
        $this->db->insert('virtual_fair_help', $data);
    }

    function updateHelp($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('virtual_fair_help', $data);
    }

    function getPrecounselorWaiting()
    {
        $this->db->select('count(id) as count');
        $this->db->from('virtualfailr_counselor_booking');
        $this->db->where('status', 0);
        return $this->db->get()->row();
    }

    function getPostcounselorWaiting()
    {
        $this->db->select('count(id) as count');
        $this->db->from('virtualfailr_post_counselor_booking');
        $this->db->where('status', 0);
        return $this->db->get()->row();
    }

    function getEducounselorWaiting()
    {
        $this->db->select('count(id) as count');
        $this->db->from('virtualfailr_edu_counselor_booking');
        $this->db->where('status', 0);
        return $this->db->get()->row();
    }
    
   function queuecountquery() {

      $query1 = "SELECT count(id) as total_count,'pre_counsellor' AS type
                  FROM virtualfailr_counselor_booking
                  WHERE status = 0
                  UNION
                  SELECT count(id) as total_count,'post_counsellor' AS type
                  FROM virtualfailr_post_counselor_booking
                  WHERE status = 0
                  UNION
                  SELECT count(id) as total_count,'edu_counsellor' AS type
                  FROM virtualfailr_edu_counselor_booking
                  WHERE status = 0 ;
                  ";

      $query = $this->db->query($query1);

      $result_data = $query->result_array();

      return $result_data;
    }

}
