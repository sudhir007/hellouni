<?php
class Tokbox_model extends CI_Model
{
    function addToken($tokenData)
    {
        $this->db->insert('tokbox_token', $tokenData);
        //echo $this->db->last_query();
        return $this->db->insert_id();
    }

    function editToken($tokenData, $tokenId)
    {
        $this->db->where('id', $tokenId);
        $this->db->update('tokbox_token', $tokenData);
    }

    function getTokenById($id)
    {
        $this->db->from('tokbox_token');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    function getTokenByParticipant($participants)
    {
        $query_string = "SELECT * FROM tokbox_token WHERE JSON_CONTAINS(participants, '" . $participants . "') ORDER BY id DESC LIMIT 1";
        //echo $query_string;
        $query = $this->db->query($query_string);
        return $query->row();
    }

    function getAllComingSlots()
    {
        $query_string = "SELECT * FROM slots WHERE confirmed_slot IS NOT NULL AND status = 'Scheduled' AND TIMESTAMPDIFF(MINUTE, now(), confirmed_slot) BETWEEN 15 AND 30";
        $query = $this->db->query($query_string);
        return $query->result_array();
    }

    function addPollResult($data)
    {
        $this->db->insert('poll_result', $data);
    }

    function getAllActivePolls()
    {
        $this->db->from('poll_questions');
        $this->db->where('active', 1);
        $this->db->order_by('id', 'asc');
        return $this->db->get()->result_array();
    }

    function addGuest($data)
    {
        $data['ip'] = $this->get_ip_address();
        $checkGuest = $this->checkGuest($data['email'], $data['session_id']);
        if($checkGuest)
        {
            return $checkGuest;
        }

        $this->db->insert('guests', $data);
        return $this->checkGuest($data['email'], $data['session_id']);
    }

    function checkGuest($email, $sessionId)
    {
        $this->db->from('guests');
        $this->db->where('email', $email);
        $this->db->where('session_id', $sessionId);
        return $this->db->get()->row_array();
    }

    function addConnection($data, $myConnectionId)
    {
        $checkConnection = $this->checkConnection($data['connection_id'], $data['session_id']);
        if($checkConnection)
        {
            $updatedData['connected'] = 1;
            $this->updateConnection($updatedData, $data['connection_id'], $data['session_id']);
            return $this->getAllConnections($data['session_id'], $myConnectionId);
        }

        if(!isset($data['role']) && $data['role'])
        {
            return $this->getAllConnections($data['session_id'], $myConnectionId);
        }
        $this->db->insert('tokbox_connections', $data);
        $this->checkConnection($data['connection_id'], $data['session_id']);
        return $this->getAllConnections($data['session_id'], $myConnectionId);
    }

    function updateConnection($updatedData, $connectionId, $sessionId)
    {
        $this->db->where('connection_id', $connectionId);
        $this->db->where('session_id', $sessionId);
        $this->db->update('tokbox_connections', $updatedData);
    }

    function updateConnectionBySession($updatedData, $sessionId)
    {
        $this->db->where('session_id', $sessionId);
        $this->db->update('tokbox_connections', $updatedData);
    }

    function getAllConnections($sessionId, $excludedConnection)
    {
        $this->db->from('tokbox_connections');
        $this->db->where('session_id', $sessionId);
        $this->db->where('connected', 1);
        //$this->db->where('connection_id != ', $excludedConnection);
        $this->db->group_by('user_id');
        return $this->db->get()->result_array();
    }

    function checkConnection($connectionId, $sessionId)
    {
        $this->db->from('tokbox_connections');
        $this->db->where('connection_id', $connectionId);
        $this->db->where('session_id', $sessionId);
        return $this->db->get()->row_array();
    }

    function get_ip_address()
    {
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = '';

        return $ipaddress;
    }

    function storeChat($data)
    {
        $this->db->insert('tokbox_chat', $data);
    }

    function deleteChat($chatId)
    {
        $this->db->where('chat_id', $chatId);
        $this->db->delete('tokbox_chat');
    }

    function getChatFromUserSession($userId, $userType, $sessionId)
    {
        $this->db->select('id, message, chat_id, `to`, "mine" AS "chat_of"', false);
        $this->db->from('tokbox_chat');
        $this->db->where('session_id', $sessionId);
        $this->db->where('from', $userId);
        $this->db->where('user_type', $userType);
        return $this->db->get()->result_array();
    }

    function getChatToUserSession($userId, $userType, $sessionId)
    {
        $queryString = "SELECT id, message, chat_id, `from`, user_type, `to`, 'theirs' AS 'chat_of' FROM tokbox_chat WHERE session_id = '" . $sessionId . "' AND ((`to` = " . $userId . " AND to_user_type = '" . $userType . "') OR (`to` = -1 AND `from` != " . $userId . "))";

        if($userType == 'GUEST')
        {
            $queryString = "SELECT id, message, chat_id, `from`, user_type, `to`, 'theirs' AS 'chat_of' FROM tokbox_chat WHERE session_id = '" . $sessionId . "' AND ((`to` = " . $userId . " AND to_user_type = '" . $userType . "') OR (`to` = -1 AND `from` != " . $userId . ")) AND from_role = 'PUBLISHER'";
        }
        $query = $this->db->query($queryString);
        $result = $query->result_array();
        return $result;
    }

    function getUserName($userIds)
    {
        $this->db->select('id, name');
        $this->db->from('user_master');
        $this->db->where_in('id', $userIds);
        return $this->db->get()->result_array();
    }

    function getGuestName($guestIds)
    {
        $this->db->select('id, name');
        $this->db->from('guests');
        $this->db->where_in('id', $guestIds);
        return $this->db->get()->result_array();
    }

    function getPollResult($sessionId)
    {
        $this->db->select('COUNT(*) AS count, answer, question_id, other_detail');
        $this->db->from('poll_result');
        $this->db->where('session_id', $sessionId);
        $this->db->group_by('question_id, answer');
        return $this->db->get()->result_array();
    }

    function getPollResultByQuestion($sessionId, $questionId)
    {
        $this->db->select('COUNT(*) AS count, answer, question_id, other_detail');
        $this->db->from('poll_result');
        $this->db->where('session_id', $sessionId);
        $this->db->where('question_id', $questionId);
        $this->db->group_by('question_id, answer');
        return $this->db->get()->result_array();
    }

    function addRunPoll($data)
    {
        $this->db->insert('running_polls', $data);
    }

    function deleteRunPoll($tokenId)
    {
        $this->db->where('token_id', $tokenId);
        $this->db->delete('running_polls');
    }

    function getRunningPollByToken($tokenId)
    {
        $this->db->from('running_polls');
        $this->db->where('token_id', $tokenId);
        $this->db->order_by('id', 'desc');
        $this->db->limit(1);
        return $this->db->get()->row_array();
    }

    function getAllComingWebinar(){

      $query_string = "SELECT * FROM webinar_master
      WHERE webinar_date = CURDATE() AND status = '1' ;";

      $query = $this->db->query($query_string);
      return $query->result_array();
    }

    function webinarUserInfo($webinarId){

      $query_string = "SELECT u.name,u.email FROM webinar_log_master w
      JOIN user_master u ON u.id = w.user_id
      WHERE w.webinar_id = $webinarId AND u.type = 5";

      $query = $this->db->query($query_string);
      return $query->result_array();
    }

    function addWebinarConnection($data)
    {
        $this->db->insert('tokbox_connections', $data);
    }
}
?>
