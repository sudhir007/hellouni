<?php
class Filter_model extends CI_Model
{
    function getAllUniversityById($id=0){

      $this->db->select('id,name');
      $this->db->from('universities');
      if($id){
        $this->db->where('id', $id);
      }

      return $this->db->get()->result_array();

    }

    function getAllCampusesByUniversityId($id=0){

      $this->db->select('id,name');
      $this->db->from('campuses');
      if($id){
        $this->db->where('university_id', $id);
      }

      return $this->db->get()->result_array();

    }

    function getAllCollegeByCampusId($id=0){

      $this->db->select('id,name');
      $this->db->from('colleges');
      if($id){
        $this->db->where('campus_id', $id);
      }

      return $this->db->get()->result_array();

    }

    function getAllDepartmentByCollegeId($id=0){

      $this->db->select('id,name');
      $this->db->from('departments');
      if($id){
        $this->db->where('college_id', $id);
      }

      return $this->db->get()->result_array();

    }

    function getAllCoursesByDepartmentId($id=0){

      $this->db->select('courses.id,courses.name,degree_master.name AS dname');
      $this->db->from('courses');
      $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
      if($id){
        $this->db->where('courses.department_id', $id);
      }

      return $this->db->get()->result_array();

    }

//course filter
    function getFilterCourses($selectedUniversityId,$selectedCampusId,$selectedCollegeId,$selectedDepartmentId,$selectedCourseId) {

      $this->db->select('courses.*, departments.name AS department_name, degree_master.name AS degree_name, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
      $this->db->from('courses');
      $this->db->join('departments', 'departments.id = courses.department_id');
      $this->db->join('degree_master', 'degree_master.id = courses.degree_id');
      $this->db->join('colleges', 'colleges.id = departments.college_id');
      $this->db->join('campuses', 'campuses.id = colleges.campus_id');
      $this->db->join('universities', 'universities.id = campuses.university_id');
      if($selectedUniversityId){
        $this->db->where('universities.id', $selectedUniversityId);
      }

      if($selectedCampusId){
        $this->db->where('campuses.id', $selectedCampusId);
      }

      if($selectedCollegeId){
        $this->db->where('colleges.id', $selectedCollegeId);
      }

      if($selectedDepartmentId){
        $this->db->where('departments.id', $selectedDepartmentId);
      }

      if($selectedCourseId){
        $this->db->where('courses.id', $selectedCourseId);
      }

      $this->db->order_by('courses.id', 'desc');
      $result=$this->db->get()->result();

      return $result;

    }

// Department filter

function getFilterDepartment($selectedUniversityId,$selectedCampusId,$selectedCollegeId,$selectedDepartmentId) {
    $this->db->select('departments.*, colleges.name AS college_name, campuses.name AS campus_name, universities.name AS university_name');
    $this->db->from('departments');
    $this->db->join('colleges', 'colleges.id = departments.college_id');
    $this->db->join('campuses', 'campuses.id = colleges.campus_id');
    $this->db->join('universities', 'universities.id = campuses.university_id');
    if($selectedUniversityId){
      $this->db->where('universities.id', $selectedUniversityId);
    }

    if($selectedCampusId){
      $this->db->where('campuses.id', $selectedCampusId);
    }

    if($selectedCollegeId){
      $this->db->where('colleges.id', $selectedCollegeId);
    }

    if($selectedDepartmentId){
      $this->db->where('departments.id', $selectedDepartmentId);
    }
    $this->db->order_by('departments.id', 'desc');
    $result=$this->db->get()->result();
return $result;
}

//College filter

function getFilterCollege($selectedUniversityId,$selectedCampusId,$selectedCollegeId){
    $this->db->select('colleges.*, campuses.name AS campus_name, universities.name AS university_name');
    $this->db->from('colleges');
    $this->db->join('campuses', 'campuses.id = colleges.campus_id');
    $this->db->join('universities', 'universities.id = campuses.university_id');
    if($selectedUniversityId){
      $this->db->where('universities.id', $selectedUniversityId);
    }

    if($selectedCampusId){
      $this->db->where('campuses.id', $selectedCampusId);
    }

    if($selectedCollegeId){
      $this->db->where('colleges.id', $selectedCollegeId);
    }
    $this->db->order_by('colleges.id', 'desc');
    $result=$this->db->get()->result();
return $result;
}

// campus filter
function getFilterCampus($selectedUniversityId,$selectedCampusId){
    $this->db->select('campuses.*, universities.name AS university_name');
    $this->db->from('campuses');
    $this->db->join('universities', 'universities.id = campuses.university_id');
    if($selectedUniversityId){
      $this->db->where('universities.id', $selectedUniversityId);
    }

    if($selectedCampusId){
      $this->db->where('campuses.id', $selectedCampusId);
    }
    $this->db->order_by('campuses.id', 'desc');
    $result=$this->db->get()->result();
return $result;
}

}
?>
