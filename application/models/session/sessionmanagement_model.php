<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 */
	 class Sessionmanagement_model extends CI_Model {

 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function getUniversityById($universityId)
    {
        $this->db->from('universities');
        $this->db->join('campuses', 'campuses.university_id = universities.id');
        $this->db->where('universities.id', $universityId);
        $query=$this->db->get();
		return $query->result_array();
    }

    function getSlotByUniversity($universityLoginId, $slotType="UNIVERSITY")
  	{

  		$this->db->from('slots');
  		$this->db->join('user_master', 'slots.user_id = user_master.id');
  		$this->db->join('student_details', 'slots.user_id = student_details.user_id');
  		if($universityLoginId)
  		{
  			$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id');
  			$this->db->where('slots.university_login_id', $universityLoginId);
  		}
  		else
  		{
  			$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id,universities.name AS university_name');
  			$this->db->join('universities', 'universities.id = slots.university_id');
  		}
  		$this->db->where('slots.status != "Deleted"');
  		$this->db->where('slots.slot_type', $slotType);
  		$this->db->order_by('slots.date_created', 'DESC');
  		$result=$this->db->get()->result();
  	   	return $result;
  	}

    function getComingSlotByUniversity($universityLoginId, $slotType = 'UNIVERSITY')
  	{
  		//$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id');
  		$this->db->from('slots');
  		$this->db->join('user_master', 'slots.user_id = user_master.id');
  		$this->db->join('student_details', 'slots.user_id = student_details.user_id');
  		$this->db->where('slots.status != "Deleted"');
  		if($universityLoginId)
  		{
  			$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id');
  			$this->db->where('slots.university_login_id', $universityLoginId);
  		}
  		else
  		{
  			$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id,universities.name AS university_name');
  			$this->db->join('universities', 'universities.id = slots.university_id');
  		}
  		$this->db->where('slots.slot_type', $slotType);
  		$this->db->where('slots.confirmed_slot >= now()');
  		$this->db->order_by('slots.confirmed_slot', 'ASC');
  		$result=$this->db->get()->result();
  	   	return $result;
  	}

  	function getCompletedSlotByUniversity($universityLoginId, $slotType = 'UNIVERSITY')
  	{
  		//$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id');
  		$this->db->from('slots');
  		$this->db->join('user_master', 'slots.user_id = user_master.id');
  		$this->db->join('student_details', 'slots.user_id = student_details.user_id');
  		$this->db->where('slots.status != "Deleted"');
  		if($universityLoginId)
  		{
  			$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id');
  			$this->db->where('slots.university_login_id', $universityLoginId);
  		}
  		else
  		{
  			$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id,universities.name AS university_name');
  			$this->db->join('universities', 'universities.id = slots.university_id');
  		}
  		$this->db->where('slots.slot_type', $slotType);
  		$this->db->where('slots.confirmed_slot < now()');
  		$this->db->order_by('slots.confirmed_slot', 'DESC');
  		$result=$this->db->get()->result();
  	   	return $result;
  	}

  	function getUnassignedSlotByUniversity($universityLoginId, $slotType = 'UNIVERSITY')
  	{
  		//$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id, universities.name AS university_name');
  		$this->db->from('slots');
  		$this->db->join('user_master', 'slots.user_id = user_master.id');
  		$this->db->join('student_details', 'slots.user_id = student_details.user_id');
  		//$this->db->join('universities', 'universities.id = slots.university_id');
  		$this->db->where('slots.status != "Deleted"');
  		if($universityLoginId)
  		{
  			$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id');
  			$this->db->where('slots.university_login_id', $universityLoginId);
  		}
  		else
  		{
  			$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id,universities.name AS university_name');
  			$this->db->join('universities', 'universities.id = slots.university_id');
  		}
  		$this->db->where('slots.slot_type', $slotType);
  		$this->db->where('slots.slot_1 IS NULL');
  		$this->db->where('slots.slot_2 IS NULL');
  		$this->db->where('slots.slot_3 IS NULL');
  		$this->db->order_by('slots.date_created', 'ASC');
  		$result=$this->db->get()->result();
  	   	return $result;
  	}

  	function getUnconfirmedSlotByUniversity($universityLoginId, $slotType = 'UNIVERSITY')
  	{
  		if($universityLoginId)
  		{
  			$query_string = "SELECT slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id
  			FROM slots
  			JOIN user_master ON slots.user_id = user_master.id
  			JOIN student_details ON slots.user_id = student_details.user_id
  			WHERE slots.status != 'Deleted' AND slots.university_login_id = " . $universityLoginId . "
  			AND slots.slot_type = '" . $slotType . "' AND (slots.slot_1 IS NOT NULL OR slots.slot_2 IS NOT NULL OR slots.slot_3 IS NOT NULL) AND slots.confirmed_slot IS NULL ORDER BY slots.date_created DESC";
  		}
  		else
  		{
  			$query_string = "SELECT slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id, universities.name AS university_name
  			FROM slots
  			JOIN user_master ON slots.user_id = user_master.id
  			JOIN student_details ON slots.user_id = student_details.user_id
  			JOIN universities ON universities.id = slots.university_id
  			WHERE slots.status != 'Deleted' AND slots.slot_type = '" . $slotType . "' AND (slots.slot_1 IS NOT NULL OR slots.slot_2 IS NOT NULL OR slots.slot_3 IS NOT NULL) AND slots.confirmed_slot IS NULL ORDER BY slots.date_created DESC";
  		}
          $query = $this->db->query($query_string);
          return $query->result();
  	}

  	function preDefineList($universityLoginId){

  		if($universityLoginId)
  		{
  			$query_string = "SELECT predefine_slots.*
  			FROM predefine_slots
  			WHERE university_id = $universityLoginId AND status = 1 ;";
  			} else {
  			$query_string = "SELECT predefine_slots.*, universities.name AS university_name
  			FROM predefine_slots
  			JOIN universities ON universities.id = predefine_slots.university_id
  			WHERE predefine_slots.status = 1
  			;";
  			}
  				$query = $this->db->query($query_string);
  				return $query->result();

  	}

  	function getSlotById($slotId)
  	{
  		$this->db->select('slots.*, user_master.name AS name, user_master.email AS email, user_master.phone AS phone, student_details.id AS student_id');
  		$this->db->from('slots');
  		$this->db->join('user_master', 'slots.user_id = user_master.id');
  		$this->db->join('student_details', 'slots.user_id = student_details.user_id');
  		$this->db->where('slots.id', $slotId);
  		$result=$this->db->get()->result();
  	   	return $result;
  	}

  	function editSlot($slotId, $updatedData)
  	{
  		$this->db->where('id', $slotId);
  		$this->db->update('slots', $updatedData);
  	}

}

?>
