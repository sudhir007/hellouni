<?php
class Seminar_model extends CI_Model
{
    function insertIp($data)
    {
        $this->db->insert('seminar_ips', $data);
    }

    function deleteIp($userId)
    {
        $this->db->delete('seminar_ips', array('user_id' => $userId));
    }

    function insertConnection($data)
    {
        $this->db->insert('seminar_connections', $data);
    }

    function deleteConnection($userId)
    {
        $this->db->delete('seminar_connections', array('user_id' => $userId));
    }

    function validUserInfo($userEmailId, $userMobileNumber)
    {
        $query1 = "SELECT username, email FROM user_master WHERE email = '$userEmailId' OR phone = '$userMobileNumber' ;";
        $query_1 = $this->db->query($query1);
        $result = $query_1->result_array();
    	return $result;
    }

    function storeChat($data)
    {
        $this->db->insert('seminar_chat', $data);
    }

    function getUserByUsername($username)
    {
        $this->db->from('user_master');
        $this->db->where('username', $username);
        $result = $this->db->get();
        return $result->row_array();
    }

    function insertUser($data)
    {
        $this->db->insert('user_master', $data);
        return $this->db->insert_id();
    }

    function insertAttendedRoom($data)
    {
        $this->db->insert('seminar_attended_rooms', $data);
    }

    function updateAttendedRoom($userId, $tokboxId, $data)
    {
        $this->db->where('user_id', $userId);
        $this->db->where('tokbox_id', $tokboxId);
        $this->db->update('seminar_attended_rooms', $data);
    }

    function updateSeminarLog($data)
    {
        $this->db->update('seminar_logs', $data);
    }

    function addRoom($data)
    {
        $this->db->insert('seminar_rooms', $data);
    }

    function updateRoom($tokboxId, $data)
    {
        $this->db->where('tokbox_id', $tokboxId);
        $this->db->update('seminar_rooms', $data);
    }

    function addQueue($data)
    {
        $this->db->insert('seminar_queue', $data);
    }

    function deleteQueue($userId)
    {
        $this->db->delete('seminar_queue', array('user_id' => $userId));
    }

    function addUser($data)
    {
        $this->db->insert('user_master', $data);
        return $this->db->insert_id();
    }

    function checkUsername($username)
    {
        $this->db->from('user_master');
        $this->db->where('username', $username);
        $query = $this->db->get();
        return $query->result_array();
    }

    function updateSlot($userId, $tokboxId, $data)
    {
        $this->db->where('user_id', $userId);
        $this->db->where('tokbox_id', $tokboxId);
        $this->db->update('seminar_slots', $data);
    }

    function addSlot($data)
    {
        $this->db->insert('seminar_slots', $data);
    }

    function checkUserMobile($mobileNumber, $eventName)
    {
        $this->db->select('*');
        $this->db->from('user_master');
        $this->db->where('phone', $mobileNumber);
        $this->db->where('registration_type', $eventName);
        $this->db->where('mobile_verified != 0');
        $query = $this->db->get();
        $result = $query->result_array();

        return $result;
    }

    function checkUser($mobileNumber)
    {
        $this->db->select('*');
        $this->db->from('user_master');
        $this->db->where('phone', $mobileNumber);
        $query = $this->db->get();
        $result = $query->row_array();

        return $result;
    }

    function checkMasterOtp($mobileNumber)
    {
        $this->db->select('*');
        $this->db->from('otp_master');
        $this->db->where('mobile_number', $mobileNumber);
        $query = $this->db->get();
        $result = $query->row_array();

        return $result;
    }

    function insertOtp($data)
    {
        $this->db->insert('otp_master', $data);

        $sendsms = sendsms($data['otp'], $data['mobile_number']);
    }

    function checkMobileOTP($mobileNumber, $otp)
    {
        $this->db->select('*');
        $this->db->from('otp_master');
        $this->db->where('mobile_number', $mobileNumber);
        $this->db->where('otp', $otp);
        $query = $this->db->get();
        $result = $query->result_array();

        return $result;
	}

    function updateOtp($mobileNumber, $data)
    {
        $this->db->where('mobile_number', $mobileNumber);
        $this->db->update('otp_master', $data);
    }

    function insertUserMaster($insertData=array())
    {
        $this->db->insert('user_master', $insertData);
        return $this->db->insert_id();
    }

    function insertLead($insertData=array())
    {
        $this->db->insert('lead_master', $insertData);
    }

    function insertStudentDetails($insertData=array())
    {
        $this->db->insert('student_details', $insertData);
    }

    function updateUser($userId, $data)
    {
        $this->db->where('id', $userId);
        $this->db->update('user_master', $data);
    }

    function checkSeminarStudent($userId, $currentUse, $otherFields = [])
    {
        $this->db->from('seminar_students');
        $this->db->where('user_id', $userId);
        $this->db->where('current_use', $currentUse);
        if($otherFields)
        {
            foreach($otherFields as $field => $value)
            {
                $this->db->where($field, $value);
            }
        }
        $query = $this->db->get();
        return $query->row_array();
    }

    function insertSeminarStudent($insertData=array())
    {
        $this->db->insert('seminar_students', $insertData);
        return $this->db->insert_id();
    }

    function getUserById($userId)
    {
        $this->db->from('user_master');
        $this->db->where('id', $userId);
        $result = $this->db->get();
        return $result->row_array();
    }

    function getSeminarStudentById($seminarId)
    {
        $this->db->from('seminar_students');
        $this->db->where('id', $seminarId);
        $result = $this->db->get();
        return $result->row_array();
    }

    function getAppliedUniversityByUsre($userId, $universityId)
    {
        $this->db->from('users_applied_universities');
        $this->db->where('user_id', $userId);
        $this->db->where('university_id', $universityId);
        $result=$this->db->get()->row_array();
        return $result;
    }

    function applyUniversity($data=array())
	{
		$this->db->insert('users_applied_universities', $data);
		return $this->db->insert_id();
	}

    function insertFairDelegate($data)
    {
        $this->db->insert('fair_delegates', $data);
        return $this->db->insert_id();
    }

    function editToken($tokenData, $tokenId)
    {
        $this->db->where('id', $tokenId);
        $this->db->update('tokbox_token', $tokenData);
    }
}
