<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'third_party/predis/predis/autoload.php';

class Croncontroller_model extends CI_Model
{
	public function HomePageCounter(){

		$query1 = "SELECT  (
					        SELECT COUNT(*)
					        FROM   universities WHERE active = 1
								) AS university_count,
					        (
					        SELECT COUNT(DISTINCT(name))
					        FROM   courses WHERE status = 1
								) AS course_count,
									(
					        SELECT COUNT(*)
					        FROM   country_master WHERE status = 1
								) AS country_count,
									(
					        SELECT COUNT(*)
					        FROM   user_master WHERE status = 1 AND type = 3
								) AS user_count
										FROM    dual
								;";

						$query = $this->db->query($query1);

						$result_data = $query->result_array();

						return $result_data;
	}

	public function featuredUniversity(){

		$query1 = "SELECT u.id AS university_id, u.name AS university_name, u.logo AS university_logo,
		 					  u.slug AS university_slug, cu.name AS course_name, dm.name AS degree_name FROM universities u
								JOIN country_master cm ON u.country_id = cm.id
								JOIN user_master um ON u.user_id = um.id
								JOIN campuses c ON c.university_id = u.id
								JOIN colleges cl ON cl.campus_id = c.id
								JOIN departments d ON d.college_id = cl.id
								JOIN courses cu ON cu.department_id = d.id
								JOIN degree_master dm ON cu.degree_id = dm.id
								WHERE u.featured_university = 1 AND cu.top_course = 1;";

		$query = $this->db->query($query1);

		$result_data = $query->result_array();

		return $result_data;

	}

	public function upcommingEvent(){

		$query1 = "SELECT * FROM webinar_master
							 WHERE webinar_date >= CURDATE() AND status = 1 ORDER BY webinar_date ASC limit 4;";

		$query = $this->db->query($query1);

		$result_data = $query->result_array();

		return $result_data;

	}

	public function researchHome(){

		$query1 = "SELECT researches.*, departments.name AS department_name, colleges.name AS college_name, universities.name AS university_name, universities.banner AS banner
		 						FROM researches
								JOIN departments ON departments.id = researches.department_id
								JOIN colleges ON colleges.id = departments.college_id
								JOIN campuses ON campuses.id = colleges.campus_id
								JOIN universities ON universities.id = campuses.university_id
								WHERE universities.active = 1 ORDER BY researches.id DESC limit 6;";

		$query = $this->db->query($query1);

		$result_data = $query->result_array();

		return $result_data;

	}



	public function searchTextKey(){

										$query1 = "SELECT c.id, c.name AS searchkey, group_concat(distinct(u.id)) university_ids
													        FROM country_master c
																	JOIN universities u ON u.country_id = c.id
																	WHERE u.active = 1 GROUP BY u.country_id
																;";

										$query6 = "SELECT c.id, concat (c.name , ' universities') AS searchkey, group_concat(distinct(u.id)) university_ids
															FROM country_master c
															JOIN universities u ON u.country_id = c.id
															WHERE u.active = 1 GROUP BY u.country_id
																						;";

										$query2 ="SELECT u.id,concat(u.name,' ',c.name) AS searchkey, u.id AS university_ids
												          FROM universities u
																	JOIN country_master c ON c.id = u.country_id
																	WHERE u.active = 1 ;";

										$query3 = "SELECT cu.id,cu.name AS searchkey, group_concat(distinct(u.id)) university_ids
																		FROM courses cu
																		JOIN departments d ON d.id = cu.department_id
																		JOIN colleges cl ON cl.id = d.college_id
																		JOIN campuses c ON c.id = cl.campus_id
																		JOIN universities u ON u.id = c.university_id
																		JOIN country_master cm ON u.country_id = cm.id
																		WHERE cu.status = 1 AND u.active = 1 GROUP BY cu.id
																	 ;";

									 $query7 = "SELECT cu.id,concat (cu.name , ' universities') AS searchkey, group_concat(distinct(u.id)) university_ids
									 								FROM courses cu
									 								JOIN departments d ON d.id = cu.department_id
									 								JOIN colleges cl ON cl.id = d.college_id
									 								JOIN campuses c ON c.id = cl.campus_id
									 								JOIN universities u ON u.id = c.university_id
									 								JOIN country_master cm ON u.country_id = cm.id
									 								WHERE cu.status = 1 AND u.active = 1 GROUP BY cu.id
									 							 ;";

									$query4 = "SELECT cu.degree_id,dm.name AS searchkey, group_concat(distinct(u.id)) university_ids
									 					 FROM degree_master dm
													 	 JOIN courses cu ON cu.degree_id = dm.id
									 					 JOIN departments d ON d.id = cu.department_id
									 					 JOIN colleges cl ON cl.id = d.college_id
									 					 JOIN campuses c ON c.id = cl.campus_id
									 					 JOIN universities u ON u.id = c.university_id
									 					 JOIN country_master cm ON u.country_id = cm.id
									 					 WHERE dm.status = 1 AND u.active = 1 GROUP BY dm.id
									 					 ;";


								 $queryCourse = "SELECT cu.id,concat (cu.name , u.name) AS searchkey, group_concat(distinct(u.id)) university_ids
						 								FROM courses cu
						 							 	JOIN departments d ON d.id = cu.department_id
													 	JOIN colleges cl ON cl.id = d.college_id
													 	JOIN campuses c ON c.id = cl.campus_id
													 	JOIN universities u ON u.id = c.university_id
						 						 		JOIN country_master cm ON u.country_id = cm.id
													 	WHERE cu.status = 1 AND u.active = 1 GROUP BY cu.id ;";

		$query_1 = $this->db->query($query1);
		$query_2 = $this->db->query($query2);
		$query_3 = $this->db->query($query3);
		$query_4 = $this->db->query($query4);

		$query_6 = $this->db->query($query6);
		$query_7 = $this->db->query($query7);

		$query_course = $this->db->query($queryCourse);

		$result_one = $query_1->result_array();
		$result_two = $query_2->result_array();
		$result_three = $query_3->result_array();
		$result_four = $query_4->result_array();

		$result_six = $query_6->result_array();
		$result_seven = $query_7->result_array();

		$result_course = $query_course->result_array();

		$result_five = array_merge($result_one,$result_six,$result_two,$result_three, $result_seven,$result_four);
		$result_two = array_merge($result_three,$result_four);

		$finalArray = [
			"one" => $result_one,
			"two" => $result_two,
			"three" => $result_three,
			"four" => $result_four,
			"five" => $result_five,
			"courses" => $result_course

		];

		return $finalArray;

	}

	public function searchkey($searchText, $type = 'UNIVERSITIES'){

		if($type == 'COURSES'){

			$query1 = "SELECT *,MATCH(search_key)
									AGAINST('$searchText' IN NATURAL LANGUAGE MODE) as scored FROM search_keys
									WHERE entity_type = 'COURSES'
									ORDER BY scored DESC LIMIT 150;";

		} else{

			$query1 = "SELECT *,MATCH(search_key)
									AGAINST('$searchText' IN NATURAL LANGUAGE MODE) as scored FROM search_keys
									WHERE entity_type = 'UNIVERSITIES'
									ORDER BY scored DESC LIMIT 5;";

		}


		$query = $this->db->query($query1);

		$result_data = $query->result_array();

		return $result_data;

	}

	public function searchKeyNew($keyword){

			$query1 = "SELECT MATCH(keywords) AGAINST('(\"" . $keyword . "\")' IN BOOLEAN MODE) score,
	    					 university_id, course_id FROM general_search HAVING score > 0;";


		$query = $this->db->query($query1);

		$result_data = $query->result_array();

		return $result_data;

	}



}
?>
