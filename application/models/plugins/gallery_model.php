<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model 
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 
  <Reverse bidding system> 
    Copyright (C) <2009>  <FreeLance PHP Script>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
    If you want more information, please email me at support@freelancephpscript.com or 
    contact us from http://www.freelancephpscript.com/contact 
 
 
 */
	 class Gallery_model extends CI_Model {
	 
 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
	function insertGallery($general=array(),$links=array())
	 {
	 $this->load->model('administrator/common_model');
	 
	 $this->db->insert('gallery_master', $general);
     $gallery_id = $this->db->insert_id();	 
	 
	
	 
	 foreach($links as $insdt){
	 
	 
	  if(isset($insdt['link'])){
	 $link=$insdt['link'];
	 } else {
	 $link='';
	 
	 }
	 
	 
	 $file_info = $this->common_model->getFileinfo($insdt['img']);
	 
	 $file_tbl_data = array(
	 'id' => '',
	 'file_name' => $file_info['logo_name'],
     'file_type' => $file_info['logo_type'],
     'path' => $insdt['img'],
	 'alter' => $insdt['alter'],
	 'file_size' => $file_info['logo_size']
     );
	 $this->db->insert('infra_file_master', $file_tbl_data);
	 $file_id = $this->db->insert_id();
	 
	  $img_tbl_data = array(
	 'id' => '',
	 'gallery_id' => $gallery_id,
	 'title' =>$insdt['title'],
	 'file_id' => $file_id,
     'link' => $link,
     'sort_order' => $insdt['sort'],
	 'status' => '1'
     );
	 $this->db->insert('gallery_to_image', $img_tbl_data);
	 
	 }
	
	 }
	 
	 
	 
	 function getPhotoGallaries($cond1,$cond2)
     { 
	 if(count($cond1)>0||count($cond2)>0 || isset($cond1) || isset($cond2)){
	  $this->db->where($cond1);
	  $this->db->or_where($cond2);
	  }
	 
	 $conditions = array('type' =>'photo');
	 $this->db->where($conditions);
	 $this->db->from('gallery_master');
	 $this->db->join('infra_status_master', 'infra_status_master.id = gallery_master.status','left');
	 
	 $this->db->select('gallery_master.id,gallery_master.name,gallery_master.sort_order,infra_status_master.name as stname');
	 
	 
	 $result=$this->db->get()->result();
	 return $result;
	 
    }
	
	 function getVideoGallaries()
     { 
	 
	 $conditions = array('type' =>'video');
	 $this->db->where($conditions);
	 $this->db->from('gallery_master');
	 $this->db->join('infra_status_master', 'infra_status_master.id = gallery_master.status','left');
	 
	 $this->db->select('gallery_master.id,gallery_master.name,gallery_master.sort_order,infra_status_master.name as stname');
	 
	 
	 $result=$this->db->get()->result();
	 return $result;
	 
    }
	
	
	function getGalleryByid($conditions)
     { 
	 $this->db->where($conditions);
	 $this->db->from('gallery_master');
	 $this->db->join('infra_status_master', 'infra_status_master.id = gallery_master.status','left');
	 
	 $this->db->select('gallery_master.id,gallery_master.name,gallery_master.sort_order,infra_status_master.name as stname');
	 
	 $result=$this->db->get()->row();
	 return $result;
	 
    }
	
	
	function getGalleryBy_Room($conditions)
     { 
	 
	
	 $this->db->where($conditions);
	 $this->db->from('gallery_to_room');
	 $this->db->join('gallery_master', 'gallery_master.id = gallery_to_room.gallery_id','left');
	 $this->db->join('infra_status_master', 'infra_status_master.id = gallery_master.status','left');
	 
	 $this->db->select('gallery_master.id,gallery_master.name,gallery_master.sort_order,infra_status_master.name as stname');
	 
	 
	 $result=$this->db->get()->result();
	 
	/* echo '<pre>';
	 print_r($result);
	 echo '</pre>'; exit();*/
	 return $result;
	 
    }
	
	
	function getGalleryImages($id)
     { 
	 $conditions = array('gallery_id' =>$id);
	 $this->db->where($conditions);
	 $this->db->from('gallery_to_image');
	 $this->db->select('*');
	 
	 $result=$this->db->get()->result();
	 
	 
	 foreach($result as $rst){
	 
	 $conditions = array('id' =>$rst->file_id);
	 $this->db->where($conditions);
	 $this->db->from('infra_file_master');
	 $this->db->select('*');
	 
	 $files=$this->db->get()->row();
	 
	  $data[] = array(
     'id' => $rst->id,
     'title' => $rst->title,
     'path' => $files->path,
	 'link' => $rst->link,
	 'alter' => $files->alter,
	 'sort_order' => $rst->sort_order
	 
	 
	 );
	 
	 
	 }
	 return $data;
    }
	
	
	
	function updateGallery($id,$general=array(),$links=array())
	 {
	 $this->load->model('administrator/common_model');
	 
	 
	 $conditions = array('gallery_id' =>$id);
	 $this->db->where($conditions);
	 $this->db->from('gallery_to_image');
	 $this->db->select('*');
	 
	 $result=$this->db->get()->result();
	 
	 foreach($result as $rst){
	 $this->db->delete('infra_file_master', array('id' => $rst->file_id)); 
	 
	 }
	 $this->db->delete('gallery_to_image', array('gallery_id' => $id)); 

	 
	 $this->db->where('id',$id);
	 $this->db->update('gallery_master',$general);
	 
     $gallery_id = $id;	 
	 
	 
	 
	 foreach($links as $insdt){
	 
	 $file_info = $this->common_model->getFileinfo($insdt['img']);
	 
	 $file_tbl_data = array(
	 'id' => '',
	 'file_name' => $file_info['logo_name'],
     'file_type' => $file_info['logo_type'],
     'path' => $insdt['img'],
	 'alter' => $insdt['alter'],
	 'file_size' => $file_info['logo_size']
     );
	 $this->db->insert('infra_file_master', $file_tbl_data);
	 $file_id = $this->db->insert_id();
	 
	  $img_tbl_data = array(
	 'id' => '',
	 'gallery_id' => $id,
	 'title' =>$insdt['title'],
	 'file_id' => $file_id,
     'link' => $insdt['link'],
     'sort_order' => $insdt['sort'],
	 'status' => '1'
     );
	 $this->db->insert('gallery_to_image', $img_tbl_data);
	 
	 }
	
	 }
	 
	
	
	function deleteGallery($id)
	 {
	   
/*	 $conditions = array('gallery_id' =>$id);
	 $this->db->where($conditions);
	 $this->db->from('gallery_to_image');
	 $this->db->select('*');
	 
	 $result=$this->db->get()->result();
	 
	 foreach($result as $rst){
	 
	 $this->db->delete('infra_file_master', array('id' => $rst->file_id)); 
	 
	 }
	 $this->db->delete('gallery_to_image', array('gallery_id' => $id)); 
	 $this->db->delete('gallery_master', array('id' => $id)); 

	 
	 return 'success';*/
	 
	 $data = array('status' => '4');  
	   
	 $this->db->where('id',$id);
	 $this->db->update('gallery_master',$data);
	 return 'success';
	 }
	
	
	function getGallery_By_Property($condition=array()){
	
	
	 $conditions = array('gallery_to_property.property_id' =>$condition['property_id'], 'gallery_master.type' =>$condition['type'], 'gallery_master.status' =>'1');
	 $this->db->where($conditions);
	 $this->db->from('gallery_to_property');
	 $this->db->join('gallery_master', 'gallery_master.id = gallery_to_property.gallery_id','left');
	 
	 $this->db->select('gallery_master.id,gallery_master.name,gallery_master.sort_order');
	 
	 
	 $result=$this->db->get()->result();
	 return $result;
	
	}
	
	
	
	
	function getGallery_By_Package($condition=array()){
	
	
	 $conditions = array('gallery_to_package.package_id' =>$condition['package_id'], 'gallery_master.type' =>$condition['type'], 'gallery_master.status' =>'1');
	 $this->db->where($conditions);
	 $this->db->from('gallery_to_package');
	 $this->db->join('gallery_master', 'gallery_master.id = gallery_to_package.gallery_id','left');
	 
	 $this->db->select('gallery_master.id,gallery_master.name,gallery_master.sort_order');
	 
	 
	 $result=$this->db->get()->result();
	 return $result;
	
	}
	
	
	function getGallery_By_Loyality($condition=array()){
	
	
	 $conditions = array('gallery_to_loyality.loyality_id' =>$condition['loyality_id'], 'gallery_master.type' =>$condition['type'], 'gallery_master.status' =>'1');
	 $this->db->where($conditions);
	 $this->db->from('gallery_to_loyality');
	 $this->db->join('gallery_master', 'gallery_master.id = gallery_to_loyality.gallery_id','left');
	 
	 $this->db->select('gallery_master.id,gallery_master.name,gallery_master.sort_order');
	 
	 
	 $result=$this->db->get()->result();
	 
	 
	 
	 /*echo '<pre>';
		 print_r($result);
		 echo '</pre>';
		 exit();*/
	 return $result;
	
	}
	
	
	function getGallery_By_Event($condition=array()){
	
	
	 $conditions = array('gallery_to_event.event_id' =>$condition['event_id'], 'gallery_master.type' =>$condition['type'], 'gallery_master.status' =>'1');
	 $this->db->where($conditions);
	 $this->db->from('gallery_to_event');
	 $this->db->join('gallery_master', 'gallery_master.id = gallery_to_event.gallery_id','left');
	 
	 $this->db->select('gallery_master.id,gallery_master.name,gallery_master.sort_order');
	 
	 
	 $result=$this->db->get()->result();
	 
	 
	 
	 /*echo '<pre>';
		 print_r($result);
		 echo '</pre>';
		 exit();*/
	 return $result;
	
	}
	
	function getGallery_By_Cmspage($condition=array()){
	
	
	 $conditions = array('gallery_to_cmspage.cmspage_id' =>$condition['cmspage_id'], 'gallery_master.type' =>$condition['type'], 'gallery_master.status' =>'1');
	 $this->db->where($conditions);
	 $this->db->from('gallery_to_cmspage');
	 $this->db->join('gallery_master', 'gallery_master.id = gallery_to_cmspage.gallery_id','left');
	 
	 $this->db->select('gallery_master.id,gallery_master.name,gallery_master.sort_order');
	 
	 
	 $result=$this->db->get()->result();
	 
	 
	 
	 /*echo '<pre>';
		 print_r($result);
		 echo '</pre>';
		 exit();*/
	 return $result;
	
	}
	
	
	
	
	 
}
	
	
	
	
	
	
	
	
	
	
	
	
	
?>