<?php
class Session_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_mapped_url()
    {
        $this->db->from('sms_url_mapping');
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }
}
