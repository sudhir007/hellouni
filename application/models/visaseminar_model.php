<?php
class Visaseminar_model extends CI_Model
{
    function insertIp($data)
    {
        $this->db->insert('visaseminar_ips', $data);
    }

    function deleteIp($userId)
    {
        $this->db->delete('visaseminar_ips', array('user_id' => $userId));
    }

    function insertConnection($data)
    {
        $this->db->insert('visaseminar_connections', $data);
    }

    function deleteConnection($userId)
    {
        $this->db->delete('visaseminar_connections', array('user_id' => $userId));
    }

    function validUserInfo($userEmailId, $userMobileNumber)
    {
        $query1 = "SELECT username, email, link_allowed FROM user_master WHERE email = '$userEmailId' OR phone = '$userMobileNumber' ;";
        $query_1 = $this->db->query($query1);
        $result = $query_1->result_array();
    	return $result;
    }

    function storeChat($data)
    {
        $this->db->insert('visaseminar_chat', $data);
    }

    function getUserByUsername($username)
    {
        $this->db->from('user_master');
        $this->db->where('username', $username);
        $result = $this->db->get();
        return $result->row_array();
    }

    function insertUser($data)
    {
        $this->db->insert('user_master', $data);
        return $this->db->insert_id();
    }
}
