var Conference = (function(){
    var my_user_media = null;
    var my_display_media = null;
    var my_media_recorder = null;
    var socket = null;
    var my_peer = null;
    var my_current_media = null;
    var my_user_id = null;
    var remote_user_id = null;
    var screen_share = false;
    var peers = {};
    var all_streams = {};
    var is_recording_started = false;
    var is_recording_stopped = false;
    var canvas_grid = document.getElementById("canvas-grid");
    var context = canvas_grid.getContext('2d');
    //var record_streams = null;
    var video_grid = document.getElementById("video-grid");

    function get_peer_config(){
        return $.ajax({
            url: '/video/config/',
            type: 'GET'
        })
        .then(data => {
            return data;
        });
    }

    function connect_to_user(user_id, stream, stream_id, user_name) {
        const call = my_peer.call(user_id, stream, {metadata: {user_name: USER_NAME}});

        const video = document.createElement('video');
        video.setAttribute("id", stream_id);

        const container_div = document.createElement('div');
        container_div.setAttribute('class', 'video-container');
        container_div.setAttribute('id', 'div-' + stream_id);
        container_div.append(video);

        const overlay_div = document.createElement('div');
        overlay_div.setAttribute('class', 'overlay');

        const float_text = document.createElement('p');
        float_text.innerHTML = user_name;
        overlay_div.append(float_text);
        container_div.append(overlay_div);

        call.on('stream', user_video_stream => {
            all_streams[stream_id] = user_video_stream;
            add_stream(video, container_div, user_video_stream);
        });

        call.on('close', () => {
            //video.remove();
            $("#" + stream_id).remove();
        });

        peers[user_id] = call;
    }

    function add_stream(video, container_div, stream) {
        video.srcObject = stream;
        video.addEventListener('loadedmetadata', () => {
            video.play();
        });
        //video_grid.append(video);
        video_grid.append(container_div);
    }

    function send_message(user_id, message){
        const conn = my_peer.connect(user_id);
        conn.on('open', function() {
            // Send messages
            conn.send(message);
        });
    }

    function scroll_to_bottom(){
        var d = $('.main__chat_window');
        d.scrollTop(d.prop("scrollHeight"));
    }

    function start_recording(){
        /*let record_streams = [];
        if(all_streams){
            for(let key in all_streams){
                record_streams.push(all_streams[key]);
            }
        }

        my_media_recorder = RecordRTC(record_streams, {
            type: 'video',
            mimeType: 'video/webm'
        });*/

        canvas_grid.width = video_grid.clientWidth;
        canvas_grid.height = video_grid.clientHeight;

        my_media_recorder = RecordRTC(canvas_grid, {
            type: 'canvas',
            video: 'HTMLVideoElement',
            canvas: {
                width: 640,
                height: 480
            }
        });

        $("#btnStartReco").hide();
        $("#btnStopReco").show();

        is_recording_started = true;
        is_recording_stopped = false;
        my_media_recorder.startRecording();
    }

    function stop_recording(){
        my_media_recorder.stopRecording(function() {
            is_recording_started = false;
            is_recording_stopped = true;
            let blob = my_media_recorder.getBlob();
            let url = window.URL.createObjectURL(blob);

            $("#downloadRecording").attr({ href: url, download: 'video.webm' }).show();

            $("#btnStartReco").show();
            $("#btnStopReco").hide();
        });
    }

    /*(function looper() {
        if(!is_recording_started) {
            return setTimeout(looper, 500);
        }

        html2canvas(video_grid).then(function(canvas) {
            context.clearRect(0, 0, canvas_grid.width, canvas_grid.height);
            context.drawImage(canvas, 0, 0, canvas_grid.width, canvas_grid.height);

            if(is_recording_stopped) {
                return;
            }

            requestAnimationFrame(looper);
        });
    })();*/

    async function _get_user_media(){
        my_user_media = await navigator.mediaDevices.getUserMedia({
            video: true,
            audio: true
        });
    }

    async function _get_display_media(){
        my_display_media = await navigator.mediaDevices.getDisplayMedia({
            audio: true,
            video: {
                frameRate: 1,
            },
        });
    }

    async function _initiate_video_merger(){
        let _width = video_grid.clientWidth;
        let _height = video_grid.clientHeight;

        let merger = new VideoStreamMerger({
            width: _width,   // Width of the output video
            height: _height,  // Height of the output video
            //fps: 1,       // Video capture frames per second
            audioContext: null
        });

        if(all_streams){
            var _x = 0;
            var _y = 0;
            for(let key in all_streams){
                merger.addStream(all_streams[key], {
                    x: _x,
                    y: _y,
                    width: _width / 2,
                    height: _height,
                    mute: false
                });
                _x = _x + 400;
                _y = _y + _height / 2;
            }
        }

        merger.start();
        let merged_stream = merger.result;

        var recorded_chunks = [];
        my_media_recorder = new MediaRecorder(merged_stream, { mimeType: 'video/webm; codecs=vp8,opus' });
        my_media_recorder.ondataavailable = (e) => {
            if(e.data.size > 0)
                recorded_chunks.push(e.data);
        };
        my_media_recorder.onstart = async () => {
            $("#btnStartReco").hide();
            $("#btnStopReco").show();
        };

        my_media_recorder.onstop = async () => {
            let blob = new Blob(recorded_chunks, { type: 'video/webm' });
            let url = window.URL.createObjectURL(blob);

            $("#downloadRecording").attr({ href: url, download: 'video.webm' }).show();

            $("#btnStartReco").show();
            $("#btnStopReco").hide();
        };
    }

    /*async function _initiate_media_recorder(){
        record_streams = new MediaStream();
        if(all_streams){
            for(let key in all_streams){
                console.log(key, all_streams[key]);
                record_streams.addTrack(all_streams[key].getVideoTracks()[0]);
            }
        }
        record_streams.getTracks().forEach(track => {
            console.log(track);
        })
        var recorded_chunks = [];
        my_media_recorder = new MediaRecorder(record_streams, { mimeType: 'video/webm; codecs=vp8,opus' });
        my_media_recorder.ondataavailable = (e) => {
            if(e.data.size > 0)
                recorded_chunks.push(e.data);
        };
        my_media_recorder.onstart = async () => {
            $("#btnStartReco").hide();
            $("#btnStopReco").show();
        };

        my_media_recorder.onstop = async () => {
            let blob = new Blob(recorded_chunks, { type: 'video/webm' });
            let url = window.URL.createObjectURL(blob);

            $("#downloadRecording").attr({ href: url, download: 'video.webm' }).show();

            $("#btnStartReco").show();
            $("#btnStopReco").hide();
        };
    }*/

    async function _create_socket_connection(){
        socket = io.connect('https://video-server.egamesplay.com');

        my_current_media = my_user_media;

        socket.on('user-connected', (user_id, user_name) => {
            let video_id = 'video-' + user_id;
            connect_to_user(user_id, my_current_media, video_id, user_name);
        });

        socket.on("createMessage", message => {
            $("ul").append('<li class="message"><b>user</b><br/>' + message + '</li>');
            scroll_to_bottom();
        });

        socket.on('user-disconnected', user_id => {
            let video_id = "#video-" + user_id;
            let video_container_id = "#div-video-" + user_id;
            $(video_container_id).remove();
            if (all_streams[video_id]) delete all_streams[video_id];
            if (peers[user_id]) peers[user_id].close();
        });

        socket.on('user-share-screen', user_id => {
            let screen_id = "screen-" + user_id;
            connect_to_user(user_id, my_current_media, screen_id);
        });

        socket.on('user-remove-screen', user_id => {
            let screen_id = "screen-" + user_id;
            if (all_streams[screen_id]) delete all_streams[screen_id];
            $("#" + screen_id).remove();
        });
    }

    async function _create_peer_connection(){
        let encoded_config = await get_peer_config();
        let peer_config = atob(encoded_config);
        peer_config = JSON.parse(peer_config);
        my_peer = new Peer(undefined, peer_config);

        my_peer.on('open', id => {
            my_user_id = id;
            let video_id = "video-" + my_user_id;

            const video = document.createElement('video');
            video.muted = true;
            video.setAttribute("id", video_id);

            const container_div = document.createElement('div');
            container_div.setAttribute('class', 'video-container');
            container_div.setAttribute('id', 'div-' + video_id);
            container_div.append(video);

            const overlay_div = document.createElement('div');
            overlay_div.setAttribute('class', 'overlay');

            const float_text = document.createElement('p');
            float_text.innerHTML = USER_NAME;
            overlay_div.append(float_text);
            container_div.append(overlay_div);

            all_streams[video_id] = my_user_media;
            add_stream(video, container_div, my_user_media);

            socket.emit('join-room', ROOM_ID, id, USER_NAME);
        });

        my_peer.on('call', call => {
            call.answer(my_current_media);
            remote_user_id = call.peer;
            peers[remote_user_id] = call;
            const video = document.createElement('video');
            video.setAttribute("id", "video-" + remote_user_id);

            const container_div = document.createElement('div');
            container_div.setAttribute('class', 'video-container');
            container_div.setAttribute('id', 'div-video-' + remote_user_id);
            container_div.append(video);

            const overlay_div = document.createElement('div');
            overlay_div.setAttribute('class', 'overlay');

            const float_text = document.createElement('p');
            float_text.innerHTML = call.metadata.user_name;
            overlay_div.append(float_text);
            container_div.append(overlay_div);

            call.on('stream', user_video_stream => {
                if(!screen_share){
                    all_streams["video-" + remote_user_id] = user_video_stream;
                    add_stream(video, container_div, user_video_stream);
                }
            });
        });

        my_peer.on('connection', function(conn) {
            conn.peerConnection.getStats(function callback(stat){
                let result = stat.result();
            });
            conn.on('open', function() {
                // Receive messages
                conn.on('data', function(data) {
                    switch(data.type){
                        case 'speaker-add':
                            $('#video-' + data.id).addClass("blink");
                            break;
                        case 'speaker-remove':
                            for(let key in peers){
                                $('#video-' + key).removeClass("blink");
                            }
                            $('#video-' + data.id).removeClass("blink");
                            break;
                        case 'msg':
                            $("ul").append('<li class="message"><b>' + data.user_name + ': </b><br/>' + data.message + '</li>');
                            scroll_to_bottom();
                            break;
                    }
                });
            });
        });
    }

    async function _speaker_detection(){
        window.SpeechRecognition = window.SpeechRecognistion || window.webkitSpeechRecognition;
        const recognition = new SpeechRecognition();
        recognition.interimResults = true;

        recognition.addEventListener('result', e => {
            if(e.results[0].isFinal){
                for(let key in peers){
                    let send_data = {
                        type: "speaker-remove",
                        id: my_user_id
                    };

                    send_message(key, send_data);
                }
            }
            else{
                for(let key in peers){
                    let send_data = {
                        type: "speaker-add",
                        id: my_user_id
                    };

                    send_message(key, send_data);
                }
            }
        });

        recognition.addEventListener('end', recognition.start);
        recognition.start();
    }

    async function _event_binding(){
        $("#btnMuteUnmute").on('click', function () {
            let enabled = my_user_media.getAudioTracks()[0].enabled;
            if (enabled) {
                my_user_media.getAudioTracks()[0].enabled = false;
                let html = '<i class="unmute fas fa-microphone-slash"></i><span>Unmute</span>';
                document.querySelector('.main__mute_button').innerHTML = html;
            }
            else {
                my_user_media.getAudioTracks()[0].enabled = true;
                let html = '<i class="fas fa-microphone"></i><span>Mute</span>';
                document.querySelector('.main__mute_button').innerHTML = html;
            }
        });

        $("#btnStartStopCam").on('click', function () {
            let enabled = my_user_media.getVideoTracks()[0].enabled;
            if (enabled) {
                my_user_media.getVideoTracks()[0].enabled = false;
                let html = '<i class="stop fas fa-video-slash"></i><span>Play Video</span>';
                document.querySelector('.main__video_button').innerHTML = html;
            }
            else {
                my_user_media.getVideoTracks()[0].enabled = true;
                let html = '<i class="fas fa-video"></i><span>Stop Video</span>';
                document.querySelector('.main__video_button').innerHTML = html;
            }
        });

        $("#btnStartStopScreenshare").on('click', async function () {
            if (my_display_media) {
                my_display_media.getVideoTracks()[0].stop();
                let html = '<i class="fa fa-desktop"></i><span>Screen Share</span>';
                document.querySelector('.main__screen_button').innerHTML = html;
                my_display_media = null;
                screen_share = false;
                socket.emit('remove-screen');
            }
            else{
                try {
                    await _get_display_media();
                    if (my_display_media && my_display_media.getVideoTracks().length > 0) {
                        let html = '<i class="fa fa-desktop"></i><span>Stop Share</span>';
                        document.querySelector('.main__screen_button').innerHTML = html;
                        my_current_media = my_display_media;
                        screen_share = true;

                        my_display_media.getVideoTracks()[0].onended = function(){
                            my_display_media.getVideoTracks()[0].stop();
                            let html = '<i class="fa fa-desktop"></i><span>Screen Share</span>';
                            document.querySelector('.main__screen_button').innerHTML = html;
                            my_display_media = null;
                            screen_share = false;
                            socket.emit('remove-screen');
                        };
                        socket.emit('share-screen');
                    }
                } catch (e) {
                    console.log(e);
                }
            }
        });

        $("#btnStartReco").on('click', async function () {
            //await _initiate_media_recorder();
            //my_media_recorder.start(1000);

            //start_recording();

            await _initiate_video_merger();
            my_media_recorder.start(1000);
        });

        $("#btnStopReco").on('click', function () {
            my_media_recorder.stop();
            //stop_recording();
        });

        // input value
        let text = $("input");
        // when press enter send message
        $('html').keydown(function (e) {
            if (e.which == 13 && text.val().length !== 0) {
                //socket.emit('message', text.val());
                $("ul").append('<li class="message"><b>' + USER_NAME + ': </b><br/>' + text.val() + '</li>');
                scroll_to_bottom();
                for(let key in peers){
                    let send_data = {
                        type: "msg",
                        message: text.val(),
                        id: my_user_id,
                        user_name: USER_NAME
                    };

                    send_message(key, send_data);
                }
                text.val('');
            }
        });
    }

    async function _init() {
        await _get_user_media();
        await _create_socket_connection();
        await _create_peer_connection();
        await _speaker_detection();
        await _event_binding();
    }

    return {
        init: async function(){
            await _init();
        }
    }
}());
