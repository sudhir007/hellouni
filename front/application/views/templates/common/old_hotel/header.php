<!DOCTYPE html>

<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->

<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->

<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->

<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->

<head>

<meta charset="utf-8">



<!-- Viewport Metatag -->

<meta name="viewport" content="width=device-width,initial-scale=1.0">



<!-- Plugin Stylesheets first to ease overrides -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/colorpicker/colorpicker.css" media="screen">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>custom-plugins/wizard/wizard.css" media="screen">

<link rel="stylesheet" href="<?php echo base_url(); ?>plugins/plupload/jquery.plupload.queue.css" media="screen">
<link rel="stylesheet" href="<?php echo base_url(); ?>plugins/elfinder/css/elfinder.css"media="screen" >

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>jui/css/jquery.ui.timepicker.css" media="screen">


<!-- Required Stylesheets -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css" media="screen">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/fonts/ptsans/stylesheet.css" media="screen">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/fonts/icomoon/style.css" media="screen">



<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/mws-style.css" media="screen">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/icons/icol16.css" media="screen">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/icons/icol32.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/cleditor/jquery.cleditor.css" media="screen">



<!-- Demo Stylesheet -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/demo.css" media="screen">



<!-- jQuery-UI Stylesheet -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>jui/css/jquery.ui.all.css" media="screen">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>jui/jquery-ui.custom.css" media="screen">



<!-- Theme Stylesheet -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/mws-theme.css" media="screen">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/themer.css" media="screen">

     <!-- JavaScript Plugins -->

    <script src="<?php echo base_url(); ?>js/libs/jquery-1.8.3.min.js"></script>

    <script src="<?php echo base_url(); ?>js/libs/jquery.mousewheel.min.js"></script>

    <script src="<?php echo base_url(); ?>js/libs/jquery.placeholder.min.js"></script>

    <script src="<?php echo base_url(); ?>custom-plugins/fileinput.js"></script>

    

    <!-- jQuery-UI Dependent Scripts -->

    <script src="<?php echo base_url(); ?>jui/js/jquery-ui-1.9.2.min.js"></script>

    <script src="<?php echo base_url(); ?>jui/jquery-ui.custom.min.js"></script>

    <script src="<?php echo base_url(); ?>jui/js/jquery.ui.touch-punch.js"></script>
    <script src="<?php echo base_url(); ?>jui/js/timepicker/jquery-ui-timepicker.min.js"></script>


    <!-- Plugin Scripts -->

    <script src="<?php echo base_url(); ?>plugins/datatables/jquery.dataTables.min.js"></script>

    <!--[if lt IE 9]>

    <script src="js/libs/excanvas.min.js"></script>

    <![endif]-->

    <script src="<?php echo base_url(); ?>plugins/flot/jquery.flot.min.js"></script>

    <script src="<?php echo base_url(); ?>plugins/flot/plugins/jquery.flot.tooltip.min.js"></script>

    <script src="<?php echo base_url(); ?>plugins/flot/plugins/jquery.flot.pie.min.js"></script>

    <script src="<?php echo base_url(); ?>plugins/flot/plugins/jquery.flot.stack.min.js"></script>

    <script src="<?php echo base_url(); ?>plugins/flot/plugins/jquery.flot.resize.min.js"></script>


    <script src="<?php echo base_url(); ?>custom-plugins/wizard/jquery.form.min.js"></script>
	
    <script src="<?php echo base_url(); ?>plugins/colorpicker/colorpicker-min.js"></script>

    <script src="<?php echo base_url(); ?>plugins/validate/jquery.validate-min.js"></script>

    <script src="<?php echo base_url(); ?>custom-plugins/wizard/wizard.min.js"></script>



    <!-- Core Script -->

    <script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url(); ?>js/core/mws.js"></script>
    <script src="<?php echo base_url(); ?>plugins/cleditor/jquery.cleditor.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/cleditor/jquery.cleditor.table.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/cleditor/jquery.cleditor.xhtml.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/cleditor/jquery.cleditor.icon.min.js"></script>
    <script src="<?php echo base_url(); ?>js/demo/demo.formelements.js"></script>


    <!-- Themer Script (Remove if not needed) -->

    <script src="<?php echo base_url(); ?>js/core/themer.js"></script>

 <script src="<?php echo base_url(); ?>js/demo/demo.table.js"></script>

    <!-- Demo Scripts (remove if not needed) -->
    <?php if($this->uri->segment('1')=='common' && $this->uri->segment('2')=='dashboard'){?>
    <script src="<?php echo base_url(); ?>js/demo/demo.dashboard.js"></script>
    <?php } elseif($this->uri->segment('1')=='administrator' && $this->uri->segment('2')=='settings'){?>
		<script src="<?php echo base_url(); ?>js/demo/demo.wizard.js"></script>

	 <?php } /*elseif(($this->uri->segment('1')=='website-element' && $this->uri->segment('2')=='brand') || ($this->uri->segment('1')=='website-element' && $this->uri->segment('2')=='property')||($this->uri->segment('1')=='website-element' && $this->uri->segment('2')=='amenity')||($this->uri->segment('1')=='website-element' && $this->uri->segment('2')=='room') ||($this->uri->segment('1')=='plugins' && $this->uri->segment('2')=='gallery')||($this->uri->segment('1')=='location' && $this->uri->segment('2')=='city')||($this->uri->segment('1')=='location' && $this->uri->segment('2')=='country') ||($this->uri->segment('1')=='website-element' && $this->uri->segment('2')=='places') ||($this->uri->segment('1')=='website-element' && $this->uri->segment('2')=='options')){*/?>
	 
	<script src="<?php echo base_url(); ?>plugins/plupload/plupload.js"></script>
    <script src="<?php echo base_url(); ?>plugins/plupload/plupload.flash.js"></script>
    <script src="<?php echo base_url(); ?>plugins/plupload/plupload.html4.js"></script>
    <script src="<?php echo base_url(); ?>plugins/plupload/plupload.html5.js"></script>
    <script src="<?php echo base_url(); ?>plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>
	<script src="<?php echo base_url(); ?>plugins/elfinder/js/elfinder.min.js"></script>
    <script src="<?php echo base_url(); ?>js/demo/demo.wizard.js"></script>
    <script src="<?php echo base_url(); ?>js/demo/demo.widget.js"></script>
    <script src="<?php echo base_url(); ?>js/demo/demo.files.js"></script>

 
    <?php //}?>
 
 
 

<title>MWS Admin - Dashboard</title>



</head>



<body>



	<!-- Themer (Remove if not needed) -->  

	<!--<div id="mws-themer">

        <div id="mws-themer-content">

        	<div id="mws-themer-ribbon"></div>

            <div id="mws-themer-toggle">

                <i class="icon-bended-arrow-left"></i> 

                <i class="icon-bended-arrow-right"></i>

            </div>

        	<div id="mws-theme-presets-container" class="mws-themer-section">

	        	<label for="mws-theme-presets">Color Presets</label>

            </div>

            <div class="mws-themer-separator"></div>

        	<div id="mws-theme-pattern-container" class="mws-themer-section">

	        	<label for="mws-theme-patterns">Background</label>

            </div>

            <div class="mws-themer-separator"></div>

            <div class="mws-themer-section">

                <ul>

                    <li class="clearfix"><span>Base Color</span> <div id="mws-base-cp" class="mws-cp-trigger"></div></li>

                    <li class="clearfix"><span>Highlight Color</span> <div id="mws-highlight-cp" class="mws-cp-trigger"></div></li>

                    <li class="clearfix"><span>Text Color</span> <div id="mws-text-cp" class="mws-cp-trigger"></div></li>

                    <li class="clearfix"><span>Text Glow Color</span> <div id="mws-textglow-cp" class="mws-cp-trigger"></div></li>

                    <li class="clearfix"><span>Text Glow Opacity</span> <div id="mws-textglow-op"></div></li>

                </ul>

            </div>

            <div class="mws-themer-separator"></div>

            <div class="mws-themer-section">

	            <button class="btn btn-danger small" id="mws-themer-getcss">Get CSS</button>

            </div>

        </div>

        <div id="mws-themer-css-dialog">

        	<form class="mws-form">

            	<div class="mws-form-row">

		        	<div class="mws-form-item">

                    	<textarea cols="auto" rows="auto" readonly="readonly"></textarea>

                    </div>

                </div>

            </form>

        </div>

    </div>-->

    <!-- Themer End -->



	<!-- Header -->

	<div id="mws-header" class="clearfix">

    

    	<!-- Logo Container -->

    	<div id="mws-logo-container">

        

        	<!-- Logo Wrapper, images put within this wrapper will always be vertically centered -->

        	<div id="mws-logo-wrap">

            	<img src="<?php echo base_url(); ?>images/logo-small1.png" alt="mws admin">

			</div>

        </div>

        

        <!-- User Tools (notifications, logout, profile, change password) -->

        <div id="mws-user-tools" class="clearfix">

        

        	<!-- Notifications -->

        	<div id="mws-user-notif" class="mws-dropdown-menu">

            	<a href="#" data-toggle="dropdown" class="mws-dropdown-trigger"><i class="icon-exclamation-sign"></i></a>

                

                <!-- Unread notification count -->

                <span class="mws-dropdown-notif">35</span>

                

                <!-- Notifications dropdown -->

                <div class="mws-dropdown-box">

                	<div class="mws-dropdown-content">

                        <ul class="mws-notifications">

                        	<li class="read">

                            	<a href="#">

                                    <span class="message">

                                        Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore

                                    </span>

                                    <span class="time">

                                        January 21, 2012

                                    </span>

                                </a>

                            </li>

                        	<li class="read">

                            	<a href="#">

                                    <span class="message">

                                        Lorem ipsum dolor sit amet

                                    </span>

                                    <span class="time">

                                        January 21, 2012

                                    </span>

                                </a>

                            </li>

                        	<li class="unread">

                            	<a href="#">

                                    <span class="message">

                                        Lorem ipsum dolor sit amet

                                    </span>

                                    <span class="time">

                                        January 21, 2012

                                    </span>

                                </a>

                            </li>

                        	<li class="unread">

                            	<a href="#">

                                    <span class="message">

                                        Lorem ipsum dolor sit amet

                                    </span>

                                    <span class="time">

                                        January 21, 2012

                                    </span>

                                </a>

                            </li>

                        </ul>

                        <div class="mws-dropdown-viewall">

	                        <a href="#">View All Notifications</a>

                        </div>

                    </div>

                </div>

            </div>

            

            <!-- Messages -->

            <div id="mws-user-message" class="mws-dropdown-menu">

            	<a href="#" data-toggle="dropdown" class="mws-dropdown-trigger"><i class="icon-envelope"></i></a>

                

                <!-- Unred messages count -->

                <span class="mws-dropdown-notif">35</span>

                

                <!-- Messages dropdown -->

                <div class="mws-dropdown-box">

                	<div class="mws-dropdown-content">

                        <ul class="mws-messages">

                        	<li class="read">

                            	<a href="#">

                                    <span class="sender">John Doe</span>

                                    <span class="message">

                                        Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore

                                    </span>

                                    <span class="time">

                                        January 21, 2012

                                    </span>

                                </a>

                            </li>

                        	<li class="read">

                            	<a href="#">

                                    <span class="sender">John Doe</span>

                                    <span class="message">

                                        Lorem ipsum dolor sit amet

                                    </span>

                                    <span class="time">

                                        January 21, 2012

                                    </span>

                                </a>

                            </li>

                        	<li class="unread">

                            	<a href="#">

                                    <span class="sender">John Doe</span>

                                    <span class="message">

                                        Lorem ipsum dolor sit amet

                                    </span>

                                    <span class="time">

                                        January 21, 2012

                                    </span>

                                </a>

                            </li>

                        	<li class="unread">

                            	<a href="#">

                                    <span class="sender">John Doe</span>

                                    <span class="message">

                                        Lorem ipsum dolor sit amet

                                    </span>

                                    <span class="time">

                                        January 21, 2012

                                    </span>

                                </a>

                            </li>

                        </ul>

                        <div class="mws-dropdown-viewall">

	                        <a href="#">View All Messages</a>

                        </div>

                    </div>

                </div>

            </div>

            

            <!-- User Information and functions section -->

            <div id="mws-user-info" class="mws-inset">

            

            	<!-- User Photo -->

            	<div id="mws-user-photo">
                <img src="<?php echo base_url();?>images/profile.jpg" alt="User Photo">


                </div>

                

                <!-- Username and Functions -->

                <div id="mws-user-functions">

                    <div id="mws-username">

                        Hello, <?php echo $user['name'];?>

                    </div>

                    <ul>

                    	<li><a href="#">Profile</a></li>

                        <li><a href="#">Change Password</a></li>

                        <li><a href="<?php echo base_url();?>common/login/logout">Logout</a></li>

                    </ul>

                </div>

            </div>

        </div>

    </div>