 <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
<?php if($this->uri->segment('5') && $this->uri->segment('5')=='view'){

$view='disabled'; ?>

<script type="text/javascript">
$( document ).ready(function() {
$('.pull-right').prop('disabled', true);
});

</script>
<?php } else { $view=''; }


 ?>
        
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- Statistics Button Container -->
            	
                
                <!-- Panels Start -->
				
				 <?php if($msg = $this->session->flashdata('flash_message')){?> 
				<div class="mws-form-message success"><?php echo $msg;?></div>
				 <?php } ?>
				 
				<?php if(validation_errors()){?> 
                <div class="mws-form-message error"><?php echo validation_errors(); ?></div>
                <?php } ?>

            	<div class="mws-panel grid_8">
                	<div class="mws-panel-header">
                    	<span><i class="icon-magic"></i> Property</span>
                    </div>
                    <div class="mws-panel-body no-padding">
					 <?php if($this->uri->segment('4')){?>
					  <form class="mws-form wzd-default" action="<?php echo base_url();?>website-element/property/edit_property/<?php if(isset($id)) echo $id;?>" method="post">
					 <?php }else{?>
                        <form class="mws-form wzd-default" action="<?php echo base_url();?>website-element/property/create" method="post">
                       <?php }?>         
                            <fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> General</legend>
                                <div id class="mws-form-row">
                                    <label class="mws-form-label">Name <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" name="property_name" class="required large" <?php echo $view; ?> value="<?php if(isset($name)) echo $name; ?>" required>
                                    </div>
                                </div>
                              
							  <div id class="mws-form-row">
                                    <label class="mws-form-label">Title <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" name="property_title" class="required large" <?php echo $view; ?> value="<?php if(isset($title)) echo $title; ?>" required>
                                    </div>
                                </div>
								
                                <div class="mws-form-row">
                                    <label class="mws-form-label">Description</label>
                                    <div class="mws-form-item">
                                        <textarea name="property_description" rows="" cols="" class="required large" <?php echo $view; ?>><?php if(isset($description)) echo $description; ?></textarea>
                                    </div>
                                </div>
                                 <div class="mws-form-row">
                                    <label class="mws-form-label">Sort Order</label>
                                    <div class="mws-form-item">
                                        <input type="text" name="property_sort_order" class="required email large" <?php echo $view; ?> value="<?php if(isset($sort_order)) echo $sort_order; ?>">
                                    </div>
                                </div>
								
								<div class="mws-form-row">
                    				<label class="mws-form-label">Status</label>
                    				<div class="mws-form-item">
                    					<select class="large" <?php echo $view; ?> name="property_status">
                    						<?php 
											if(isset($status) && $status=='Active'){ ?>
											<option value="1" selected="selected">Active</option>
                    						<option value="2">Inactive</option>
											<?php }else if((isset($status) && $status=='Inactive')){?>
											<option value="1">Active</option>
                    						<option value="2" selected="selected">Inactive</option>
											<?php }else if(isset($status)=='' ||isset($status) =='null'){?>
											<option value="1">Active</option>
                    						<option value="2">Inactive</option>
											<?php }?>
                    					</select>
                    				</div>
                    			</div>
								
								
                            </fieldset>
                            
							
							 <fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> Contact Info</legend>
								
								 <div id class="mws-form-row">
                                    <label class="mws-form-label">Country <span class="required">*</span></label>
                                    <div class="mws-form-item">
									
                                        <select class="required large ajax_country" <?php echo $view; ?> name="property_country" id="property_country" required>
                                            <option>Select Country</option>
											<?php foreach($countries as $country_details) {?>
											
											<?php if(isset($country_id) && $country_id== $country_details->id){?>
											<option value="<?php echo $country_details->id;?>" selected="selected"><?php echo $country_details->name;?></option>
                                           <?php }else {?>
										   <option value="<?php echo $country_details->id;?>"><?php echo $country_details->name;?></option>
										   <?php }
										     }?>
                                        </select>
                                    </div>
                                </div>
								
							  <div id class="mws-form-row">
                                    <label class="mws-form-label">State <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        
										<select class="required large ajax_state" <?php echo $view; ?> name="property_state" id="property_state" required>
                                            <option>Select State</option>
											
											<?php 
											if($this->uri->segment('4')){
											foreach($states as $st) {
											$id = $st->zone_id; //$id = $st->id; ?>
											<option value="<?php echo $id;?>" <?php if(isset($state_id) && $state_id==$id){echo 'selected="selected"';}?>><?php echo $st->name; ?></option>
											<?php } }?>
											
                                           
                                        </select>
                                    </div>
                                </div>
								
                               <div id class="mws-form-row">
                                    <label class="mws-form-label">City <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        
										<select class="required large ajax_city" <?php echo $view; ?> name="property_city" id="property_city" required>
                                            <option>Select City</option>
											<?php if($this->uri->segment('4')){
											foreach($cities as $cts) {?>
											
											<option value="<?php echo $cts->id;?>" <?php if(isset($city_id) && $city_id==$cts->id){echo 'selected="selected"';}?>><?php echo $cts->city_name; ?></option>
											<?php }}?>
											
                                           
                                        </select>
                                    </div>
                                </div>
								
								
								
								
								
                                <div id class="mws-form-row">
                                    <label class="mws-form-label">Address <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" name="property_address" class="required large" <?php echo $view; ?> value="<?php if(isset($addressline1)) echo $addressline1;?>" required>
                                    </div>
                                </div>
								
								 <div class="mws-form-row">
                                    <label class="mws-form-label">Postcode</label>
                                    <div class="mws-form-item">
										<input type="text" name="property_postcode" class="required email large" <?php echo $view; ?> value="<?php if(isset($postcode)) echo $postcode;?>" required>
                                    </div>
                                </div>
								
                              
                                <div class="mws-form-row">
                                    <label class="mws-form-label">Email</label>
                                    <div class="mws-form-item">
										<input type="text" name="property_email" class="required email large" <?php echo $view; ?> value="<?php if(isset($email)) echo $email;?>">
                                    </div>
                                </div>
                                 <div class="mws-form-row">
                                    <label class="mws-form-label">Telephone</label>
                                    <div class="mws-form-item">
                                        <input type="text" name="property_telephone" class="required email large" <?php echo $view; ?> value="<?php if(isset($telephone)) echo $telephone;?>">
                                    </div>
                                </div>
								
								<div class="mws-form-row">
                                    <label class="mws-form-label">Tollfree</label>
                                    <div class="mws-form-item">
                                        <input type="text" name="property_tollfree" class="required email large" <?php echo $view; ?> value="<?php if(isset($tollfree)) echo $tollfree;?>">
                                    </div>
                                </div>
								
								 <div class="mws-form-row">
                                    <label class="mws-form-label">Fax</label>
                                    <div class="mws-form-item">
                                        <input type="text" name="property_fax" class="required email large" <?php echo $view; ?> value="<?php if(isset($fax)) echo $fax;?>">
                                    </div>
                                </div>
								
								<div class="mws-form-row">
                                    <label class="mws-form-label">Latitude</label>
                                    <div class="mws-form-item">
                                        <input type="text" name="property_lat" class="required email large" <?php echo $view; ?> value="<?php if(isset($latitude)) echo $latitude;?>">
                                    </div>
                                </div>
								
								<div class="mws-form-row">
                                    <label class="mws-form-label">Longitude</label>
                                    <div class="mws-form-item">
                                        <input type="text" name="property_long" class="required email large" <?php echo $view; ?> value="<?php if(isset($longitude)) echo $longitude;?>">
                                    </div>
                                </div>
								
                            </fieldset>
							
							<fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> Links</legend>
                                 <div class="mws-form-row">
                                        <label class="mws-form-label">Photo Gallery</label>
                                        <div class="mws-form-item">
                                        <select name="photo_gallery[]" multiple="multiple" size="10" class="large" <?php echo $view; ?>>    
										<?php if($this->uri->segment('4')){ 
										    foreach($photos as $photo){
											?>
										<option value="<?php echo $photo->id;?>" <?php foreach($selected_photo as $sp){ if($photo->id==$sp->id){ echo 'selected="selected"' ; break; } } ?>><?php echo $photo->name; ?></option>
											<?php }
										     } else{ 
											     foreach($photos as $photo){?>
									    <option value="<?php echo $photo->id;?>"><?php echo $photo->name; ?></option>
											<?php } }?>
										</select>
										 
                                        </div>
										</div>
										<div class="mws-form-row">
										<label class="mws-form-label">Video Gallery</label>
                                        <div class="mws-form-item">
                                        <select name="video_gallery[]" multiple="multiple" size="10" class="large" <?php echo $view; ?>>    
										<?php if($this->uri->segment('4')){ 
										    foreach($videos as $video){
											?>
										<option value="<?php echo $video->id;?>" <?php foreach($selected_video as $sv){ if($video->id==$sv->id){ echo 'selected="selected"' ; break; } } ?>><?php echo $video->name; ?></option>
											<?php }
										     } else{ 
											     foreach($videos as $video){?>
									    <option value="<?php echo $video->id;?>"><?php echo $video->name; ?></option>
											<?php } }?>
										</select>	 
                                        </div>
                                    </div>
									
									
									
									<div class="mws-form-row">
										<label class="mws-form-label">Near By Places</label>
                                        <div class="mws-form-item">
										
										
                                        <select name="near_place[]" multiple="multiple" size="10" class="large" <?php echo $view; ?>>    
										<?php if($this->uri->segment('4')){ 
										    foreach($places as $place){
											?>
										<option value="<?php echo $place->id;?>" <?php foreach($selected_places as $pls){ if($place->id==$pls->id){ echo 'selected="selected"' ; break; } } ?>><?php echo $place->name; ?></option>
											<?php }
										     } else{ 
											     foreach($places as $place){?>
									    <option value="<?php echo $place->id;?>"><?php echo $place->name; ?></option>
											<?php } }?>
										</select>	 
                                        </div>
                                    </div>
									
									<div class="mws-form-row">
										<label class="mws-form-label">Amenites</label>
                                        <div class="mws-form-item">
										
										
                                        <select name="amenities[]" multiple="multiple" size="10" class="large" <?php echo $view; ?>>    
										<?php if($this->uri->segment('4')){ 
										    foreach($amenities as $amnty){
											?>
										<option value="<?php echo $amnty->id;?>" <?php foreach($selected_amenities as $key){ if($amnty->id==$key->id){ echo 'selected="selected"' ; break; } } ?>><?php echo $amnty->name; ?></option>
										<?php 
										   }
										     } else{ 
											    foreach($amenities as $amnty){?>
									    <option value="<?php echo $amnty->id;?>"><?php echo $amnty->name; ?></option>
											<?php  }}?>
										</select>	 
                                        </div>
                                    </div>
									
									
									<div class="mws-form-row">
                                    <label class="mws-form-label">Virtual Tour Link</label>
                                    <div class="mws-form-item">
                                        <input type="text" name="property_virtual_tour" class="email large" <?php echo $view; ?> value="<?php if(isset($virtual_tour)) echo $virtual_tour;?>">
                                    </div>
                                </div>
                              
                               
                            </fieldset>
							
							<fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> Image</legend>
                                <div id class="mws-form-row">
                                    <label class="mws-form-label">Property Logo</label>
                                    <div class="mws-form-item">
                                       <div class="image">
									   <?php if(isset($fileid)||isset($fileid)!=''){?>
									   <img src="<?php echo $path;?>" width="100" height="100" alt="" id="elfileimg-1" class="elfileimg">
									   <input type="hidden" name="file_id" value="<?php echo $fileid;?>"/>
									   <input type="hidden" name="property_logo" value="<?php echo $path;?>" id="elfileurl-1" class="elfileurl" ><br>
									   <?php }else{?>
									   <img src="<?php echo base_url();?>images/no_image.jpg" width="100" id="elfileimg-1" height="100" alt="" class="elfileimg">
									   <input type="hidden" name="property_logo" value="" id="elfileurl-1" class="elfileurl" ><br>

									   <?php }?>
									   <input type="button" id="browse-1" class="btn btn-danger elselect-button" value="Browse" <?php echo $view; ?>>
                                       <div class="mws-panel-body no-padding no-border">
                                       <div id="elfinder"></div>
                                       </div>
                            
                           
                                    </div>
                                </div>
                              
                               
                            </fieldset>
							
							<fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> SEO</legend>
                            
							<div class="mws-form-row">
                            	 <label class="mws-form-label">Meta Tag</label>
                                <div class="mws-form-item">
                                	<textarea name="property_meta_tag" <?php echo $view; ?> class="large"><?php if(isset($meta_tag)) echo $meta_tag; ?></textarea>
                                </div>
                            </div>	
								
							<div class="mws-form-row">
                            	 <label class="mws-form-label">Meta Description</label>
                                <div class="mws-form-item">
                                	<textarea name="property_meta_description" <?php echo $view; ?> class="large"><?php if(isset($meta_description)) echo $meta_description; ?></textarea>
                                </div>
                            </div>	
								
								
								
                            </fieldset>
                            
                            
                        
					   
					    </form>
                    </div>
                </div>

              

                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->
			
