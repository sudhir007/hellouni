<?php if($this->uri->segment('5') && $this->uri->segment('5')=='view'){

$view='disabled'; ?>

<script type="text/javascript">
$( document ).ready(function() {
$("#cleditor").cleditor()[0].disable("true");
//$(".btn .btn-primary .pull-right").attr('disabled' , true);
$('.pull-right').prop('disabled', true);

});

</script>
<?php } else { $view=''; }


 ?>

        

        <!-- Main Container Start -->

        <div id="mws-container" class="clearfix">

        

        	<!-- Inner Container Start -->

            <div class="container">

            

            	<!-- Statistics Button Container -->

            	

                

                <!-- Panels Start -->

				

				 <?php if($msg = $this->session->flashdata('flash_message')){?> 

				<div class="mws-form-message success"><?php echo $msg;?></div>

				 <?php } ?>

				 

				<?php if(validation_errors()){?> 

                <div class="mws-form-message error"><?php echo validation_errors(); ?></div>

                <?php } ?>



            	<div class="mws-panel grid_8">

                	<div class="mws-panel-header">

                    	<span><i class="icon-magic"></i> Package</span>

                    </div>

                    <div class="mws-panel-body no-padding">

					

					  <?php if($this->uri->segment('4')){?>

					   <form class="mws-form wzd-default" action="<?php echo base_url();?>website-element/package/edit/<?php if(isset($id)) echo $id;?>" method="post">

					  <?php }else{?>

                        <form class="mws-form wzd-default" action="<?php echo base_url();?>website-element/package/create" method="post">

                      <?php } ?>

                            <fieldset class="wizard-step mws-form-inline">

                                <legend class="wizard-label"><i class="icol-accept"></i> General</legend>

                                <div id class="mws-form-row">

                                    <label class="mws-form-label">Title <span class="required">*</span></label>

                                    <div class="mws-form-item">

                                        <input type="text" name="package_title" <?php echo $view; ?> class="required large" value="<?php if(isset($title)) echo $title; ?>" required>

                                    </div>

                                </div>

								

								

								

								<div class="mws-form-row">

                                    <label class="mws-form-label">Start Date<span class="required">*</span></label>

                                    <div class="mws-form-item">

                                    	<input type="text" <?php echo $view; ?> class="mws-datepicker small" name="package_start" value="<?php if(isset($start)) echo $start; ?>">

                                    </div>

                                </div>

								

								<div class="mws-form-row">

                                    <label class="mws-form-label">End Date<span class="required">*</span></label>

                                    <div class="mws-form-item">

                                    	<input type="text" <?php echo $view; ?> class="mws-datepicker small" name="package_end" value="<?php if(isset($end)) echo $end; ?>">

                                    </div>

                                </div>

                              

                                <div class="mws-form-row">

                                    <label class="mws-form-label">Description<span class="required">*</span></label>

                                    <div class="mws-form-item">

                                        <textarea name="package_description" id="cleditor" rows="" cols="" class="required large"><?php if(isset($description)) echo $description; ?></textarea>

                                    </div>

                                </div>

                                

								

								

								 <div class="mws-form-row">

                                    <label class="mws-form-label">Status</label>

                                    <div class="mws-form-item">

										<select name="package_status" <?php echo $view; ?>>

										<option value="1" <?php if(isset($status) && $status=='Active'){ echo 'selected="selected"'; } ?>>Active</option>

										<option value="2" <?php if(isset($status) && $status=='Inactive'){ echo 'selected="selected"'; } ?>>Inactive</option>										   

										</select>

                                    </div>

                                </div>

								

							</fieldset>

							

							

							<fieldset class="wizard-step mws-form-inline">

                                <legend class="wizard-label"><i class="icol-accept"></i> Links</legend>

                                 <div class="mws-form-row">

                                        <label class="mws-form-label">Photo Gallery</label>

                                        <div class="mws-form-item">

                                        <select name="photo_gallery[]" multiple="multiple" size="10" class="large" <?php echo $view; ?>>    

										<?php if($this->uri->segment('4')){ 

										    foreach($photos as $photo){

											?>

										<option value="<?php echo $photo->id;?>" <?php foreach($selected_photo as $sp){ if($photo->id==$sp->id){ echo 'selected="selected"' ; break; } } ?>><?php echo $photo->name; ?></option>

											<?php }

										     } else{ 

											     foreach($photos as $photo){?>

									    <option value="<?php echo $photo->id;?>"><?php echo $photo->name; ?></option>

											<?php } }?>

										</select>

										 

                                        </div>

										</div>

									

									

										<div class="mws-form-row">

										<label class="mws-form-label">Video Gallery</label>

                                        <div class="mws-form-item">

                                        <select name="video_gallery[]" multiple="multiple" size="10" class="large" <?php echo $view; ?>>    

										<?php if($this->uri->segment('4')){ 

										    foreach($videos as $video){

											?>

										<option value="<?php echo $video->id;?>" <?php foreach($selected_video as $sv){ if($video->id==$sv->id){ echo 'selected="selected"' ; break; } } ?>><?php echo $video->name; ?></option>

											<?php }

										     } else{ 

											     foreach($videos as $video){?>

									    <option value="<?php echo $video->id;?>"><?php echo $video->name; ?></option>

											<?php } }?>

										</select>	 

                                        </div>

                                    </div>

									

									

									<div class="mws-form-row">

                                    <label class="mws-form-label">Virtual Tour Link</label>

                                    <div class="mws-form-item">

                                        <input type="text" <?php echo $view; ?> name="virtual_tour" class="email large" value="<?php if(isset($virtual_tour)) echo $virtual_tour;?>">

                                    </div>

                                </div>

                              

                               

                            </fieldset>
							
							
							<fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i>Attribute</legend>
								
								
								<table class="mws-table" id="plugins">
                            <thead>
                                <tr>
                                    
                                    <th>Attributes</th>
									<th>Value</th>
                                    <th>Sort Order</th>
									 <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
							<?php if($this->uri->segment('4')){ $i=1; foreach($package_attributes as $attr){ ?>
							<tr id="attribute_row_<?php echo $i; ?>">
                                   
                                    <td><select class="large" name="attribute[<?php echo $i; ?>][attr_id]" <?php echo $view; ?>>
									<?php foreach($attributes as $key){ ?>
									<option value="<?php echo $key->id; ?>" <?php if($key->id==$attr->attribute_id){ echo 'selected="selected"' ;}?>><?php echo $key->name; ?></option>
									<?php } ?>
									</select>
									</td>
                                    <td><input type="text" name="attribute[<?php echo $i; ?>][value]" value="<?php echo $attr->description; ?>"></td>
                                   
                                    <td><input type="text" name="attribute[<?php echo $i; ?>][sort_order]" value="<?php echo $attr->sort_order;?>" ></td>
									
									 <td><input type="button" class="btn btn-danger" value="Remove" onclick="$('#attribute_row_<?php echo $i;?>').remove();" /></td>
                                    
                                </tr>
							<?php  $i++; } }else{?>	
							
							 <tr id="attribute_row_<?php echo $row; ?>">
                                    
                                    <!--<td><input type="text" name="attribute[<?php echo $row; ?>][attr_id]" value="" >-->
									<td>
									<select class="large" name="attribute[<?php echo $row; ?>][attr_id]" <?php echo $view; ?>>
									<?php foreach($attributes as $key){ ?>
									<option value="<?php echo $key->id; ?>"><?php echo $key->name; ?></option>
									<?php } ?>
									</select>
									</td>
                                    <td><textarea name="attribute[<?php echo $row; ?>][value]" rows="10" cols="30"></textarea></td>
                                  
                                    <td><input type="text" name="attribute[<?php echo $row; ?>][sort_order]" value="" ></td>
									
									 <td><input type="button" class="btn btn-danger" value="Remove" onclick="$('#attribute_row_<?php echo $row; ?>').remove();" /></td>
                                    
                                </tr>
							<?php }?>	
						
                                <tr id="gal-img-foot">
                                    <td colspan="3">&nbsp;</td>
                                    <td ><input type="button" <?php echo $view; ?> id="add-image" class="btn btn-danger" value="Add Attribute" /></td>
                                </tr>
                            </tbody>
                        </table>
							
								
								
                             
								
                            </fieldset>
							<fieldset class="wizard-step mws-form-inline">
                                <legend class="wizard-label"><i class="icol-accept"></i> SEO</legend>
                            
							<div class="mws-form-row">
                            	 <label class="mws-form-label">Meta Tag</label>
                                <div class="mws-form-item">
                                	<textarea name="package_meta_tag" <?php echo $view; ?> class="large"><?php if(isset($meta_tag)) echo $meta_tag; ?></textarea>
                                </div>
                            </div>	
								
							<div class="mws-form-row">
                            	 <label class="mws-form-label">Meta Description</label>
                                <div class="mws-form-item">
                                	<textarea name="package_meta_description" <?php echo $view; ?> class="large"><?php if(isset($meta_description)) echo $meta_description; ?></textarea>
                                </div>
                            </div>	
								
								
								
                            </fieldset>

							 </form>

                    </div>

                </div>

                <!-- Panels End -->

            </div>

            <!-- Inner Container End -->
			
<script>
$(document).ready(function()
{
var row = <?php echo $row+1; ?>;

$("#add-image").live('click',function()
{

   var	html  = '';
	html += '<tr id="attribute_row_'+row+'">';
	html += '<td><select class="large" name="attribute[+row+][attr_id]">';
	<?php foreach($attributes as $key){ ?>
	html += '<option value="<?php echo $key->id; ?>"><?php echo $key->name; ?></option>';
	<?php } ?>
	html += '</select></td>';
	
	html += '<td><textarea name="attribute['+row+'][value]" rows="10" cols="30"></textarea></td>';
	//html += '<td><input type="text" name="attribute['+row+'][value]" value=""></td>';
	html += '<td><input type="text" name="attribute['+row+'][sort_order]" value="" ></td>';
	html += '<td><input type="button" class="btn btn-danger" value="Remove" onclick="$(\'#attribute_row_' + row + '\').remove();" /></td>';
	html += '</tr>';


	$('#gal-img-foot').before(html);
 row++;
});

});


</script>