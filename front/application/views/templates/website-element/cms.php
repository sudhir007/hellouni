
    
        <!-- Main Container Start -->
        <div id="mws-container" class="clearfix">
        
        	<!-- Inner Container Start -->
            <div class="container">
            
            	<!-- Statistics Button Container -->
            	
                
                <!-- Panels Start -->
                 
            	 <?php if($msg = $this->session->flashdata('flash_message')){?> 
    <div class="mws-form-message success"><?php echo $msg;?></div>
     <?php } ?>
	 
	 
	  <?php if($ermsg = $this->session->flashdata('error_message')){?> 
    <div class="mws-form-message error"><?php echo $ermsg;?></div>
     <?php } ?>
				<div class="mws-panel grid_8">
				<form action="<?php echo base_url();?>website-element/event/delete_mult" method="post">
                	<div class="mws-panel-header">
					
                    	<span><i class="icon-table"></i> CMS Page
						<a href="<?php echo base_url();?>website-element/cms/form"><button type="button" class="btn btn-small">Insert</button></a>
						<button type="submit" name="delete-Brand" id="del" class="btn btn-small">Delete</button>
						
						</span>
						
					
						
                    </div>
                    <div class="mws-panel-body no-padding">
                        <table class="mws-datatable-fn mws-table">
                            <thead>
                                <tr>
                                    <th style="width: 1px;">Select</th>
									
									<th>Title</th>
                                  
                                    <th>Status</th>
                                    <th>Sort Order</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($cmspages as $cmspg):?>
								<tr>
                                    <td><input type="checkbox" name="chk[]" class="brand_chk" id="<?php echo $cmspg->id;?>" value="<?php echo $cmspg->id;?>"/></td>
									<td><?php echo $cmspg->name;?></td>
                                    
                                    <td><?php echo $cmspg->status_name;?></td>
									<td><?php echo $cmspg->sort_order;?></td>
									<td class=" ">
                                        <span class="btn-group">
                                            
											<a class="btn btn-small" href="<?php echo base_url();?>website-element/cms/form/<?php echo $cmspg->id;?>/view"><i class="icon-search"></i></a>
											
                                            <a class="btn btn-small" href="<?php echo base_url();?>website-element/cms/form/<?php echo $cmspg->id;?>"><i class="icon-pencil"></i></a>
                                            <a class="btn btn-small delete-row" id="<?php echo base_url(); ?>website-element/cms/delete/<?php echo $cmspg->id;?>" href="javascript:void(o);"><i class="icon-trash"></i></a></span>
                                    </td>
                                </tr>
                               <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
					
					</form>
                </div>

                                 

                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->
			
