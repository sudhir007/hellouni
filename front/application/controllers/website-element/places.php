<?php

/**

 * Reverse bidding system Home Class

 *

 * Permits admin to set the site settings like site title,site mission,site offline status.

 *

 * @package		Reverse bidding system

 * @subpackage	Controllers

 * @category	Common Display 

 * @author		FreeLance PHP Script Team

 * @version		Version 1.0

 * @created		December 30 2008

 * @link		http://www.freelancephpscript.com

 

 <Reverse bidding system> 

    Copyright (C) <2009>  <FreeLance PHP Script>



    This program is free software: you can redistribute it and/or modify

    it under the terms of the GNU General Public License as published by

    the Free Software Foundation, either version 3 of the License, or

    (at your option) any later version.



    This program is distributed in the hope that it will be useful,

    but WITHOUT ANY WARRANTY; without even the implied warranty of

    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

    GNU General Public License for more details.



    You should have received a copy of the GNU General Public License

    along with this program.  If not, see <http://www.gnu.org/licenses/>

    If you want more information, please email me at support@freelancephpscript.com or 

    contact us from http://www.freelancephpscript.com/contact  

 */

class Places extends MY_Controller {



	//Global variable  

    public $outputData;		//Holds the output data for each view

	public $loggedInUser;

   

    /**

	 * Constructor 

	 *

	 * Loads language files and models needed for this controller

	 */

	 function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->lang->load('enduser/home', $this->config->item('language_code'));
		$this->load->library('form_validation');
    }

	
	function index()
	{   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('website-element/place_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }		
        $this->outputData['places'] = $this->place_model->getPlaces();
        $this->render_page('templates/website-element/places',$this->outputData);
	
} 
	
	
	 function form()
	 {   
	     $this->load->helper('cookie_helper'); 
         $this->load->model('website-element/amenity_model');
         $this->load->model('website-element/place_model');
         $this->load->model('plugins/gallery_model');
		 $userdata=$this->session->userdata('user');
	     if(empty($userdata)){  redirect('common/login'); }		
		 
		 if($this->uri->segment('4')){
		 $conditions = array(
	 				  'nearby_place_master.id' => $this->uri->segment('4')
	 					);
		 $place_info = $this->place_model->getPlaceByid($conditions);
		
		foreach($place_info as $place_in):
		$this->outputData['id'] 			= $place_in->id;
		$this->outputData['name'] 			= $place_in->name;
		$this->outputData['description'] 	= $place_in->description;
		$this->outputData['distance'] 	= $place_in->distance;
		$this->outputData['photo_id'] 	= $place_in->photo_gallery_id;
		$this->outputData['video_id'] 	= $place_in->video_gallery_id;
		endforeach;
		  
		  }
		  
		  $cond_gallery = array('gallery_master.status' => '1'); 
		 $this->outputData['videos']	= $this->gallery_model->getVideoGallaries($cond_gallery,$array=array());   
		 $this->outputData['photoes']	= $this->gallery_model->getPhotoGallaries($cond_gallery,$array=array());   

        $this->render_page('templates/website-element/places_form',$this->outputData);
	
     } 
	 
	 
	 
	 function create()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
       $this->load->model('website-element/place_model');
	   
	   $this->form_validation->set_rules('place_name', 'Place Name', 'required|trim|xss_clean');
	   
	   if ($this->form_validation->run() == FALSE)
		{
		
			  $this->render_page('templates/website-element/places_form');
		}
		else
		{
			//$this->load->view('formsuccess');
		 $data = array(
        'name' => $this->input->post('place_name'),
        'description' => $this->input->post('place_description'),
        'distance' =>$this->input->post('place_distance'),
		'photo_gallery_id' =>$this->input->post('place_photo'),
		'video_gallery_id' =>$this->input->post('place_video'),
		'status' =>'1'
	     );
	 
	     if($this->place_model->insertPlace($data)=='success'){
	 
	     $this->session->set_flashdata('flash_message', "Success: You have saved Place!");
	     redirect('website-element/places/index');
	     }
	 
	 
	   }
	}
	 
	 
	 function edit()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

	   $this->load->library('form_validation');
       $this->load->model('website-element/place_model');
	   
	   $this->form_validation->set_rules('place_name', 'Place Name', 'required|trim|xss_clean');
	   
	   if ($this->form_validation->run() == FALSE)
		{
		
			  $this->render_page('templates/website-element/places_form');
		}
		else
		{
			//$this->load->view('formsuccess');
		 $data = array(
		 'place_id' => $this->uri->segment('4'),
        'name' => $this->input->post('place_name'),
        'description' => $this->input->post('place_description'),
        'distance' =>$this->input->post('place_distance'),
		'photo_gallery_id' =>$this->input->post('place_photo'),
		'video_gallery_id' =>$this->input->post('place_video'),
		'status' =>'1'
	     );
	 
	     if($this->place_model->editPlace($data)=='success'){
	 
	     $this->session->set_flashdata('flash_message', "Success: You have modified Place!");
	     redirect('website-element/places/index');
	     }
	 
	 
	   }
	}
    
	
	function delete()
	 {   
	   
	   $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
       $this->load->model('website-element/place_model');
		
		
		if($this->place_model->deletePlace($this->uri->segment('4'))=='success'){
	 
	         $this->session->set_flashdata('flash_message', "Success: You have deleted Place!");
	         redirect('website-element/places/index');
	        
	 
	 
	    }
	}
	
	
	function delete_mult_places(){
	
	    $this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
        $this->load->model('website-element/place_model');
		
		$array = $this->input->post('chk');
		foreach($array as $id):
		
		$this->place_model->deletePlace($id);
		
		endforeach;
		$this->session->set_flashdata('flash_message', "Success: You have deleted places!");
		redirect('website-element/places/index');
	
	
	}
	 
	 
	 
	 

	

}//End  Home Class





?>