<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model 
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 
  <Reverse bidding system> 
    Copyright (C) <2009>  <FreeLance PHP Script>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
    If you want more information, please email me at support@freelancephpscript.com or 
    contact us from http://www.freelancephpscript.com/contact 
 
 
 */
	 class State_model extends CI_Model {
	 
 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
	

	
	function getStates($condition1,$condition2)
    { if(count($condition1)>0 || count($condition2)>0  || isset($condition1) || isset($condition2)){
	  $this->db->where($condition1);
	  $this->db->or_where($condition2);
	  }
   
   
       $this->db->from('infra_state_master');
	   $this->db->join('infra_country_master', 'infra_country_master.id = infra_state_master.country_id','left');
	   $this->db->join('infra_status_master', 'infra_status_master.id = infra_state_master.status','left');
	   
	   
       $this->db->select('infra_state_master.zone_id as id,infra_state_master.country_id,infra_state_master.status,infra_state_master.code,infra_state_master.name,infra_status_master.name as status_name,infra_country_master.name as country_name');
	   
	   $result = $this->db->get()->result();
       return $result; 
    }
	
	
	function getCitiesByCountry($country_id)
     { 
	  $this->db->from('infra_city_master'); 
	  $this->db->where('country_id', $country_id);
      $result = $this->db->get()->result();
	  return $result; 
    }
	
	function getStatesByCountry($country_id)
     { 
	  $this->db->from('infra_state_master'); 
	  $this->db->where('country_id', $country_id);
      $result = $this->db->get()->result();
	  return $result; 
    }
	 
	  function getCity($id)
	 {
	 
	 	$conditions = array('id'=> $id);
	 	$this->db->where($conditions);
	 	$this->db->select('*');
		$query = $this->db->get('infra_city_master');		
		$row   = $query->row();
		return $row;
	 }
	 
	 
	 function getStateDetailsBy_Id($conditions)
	 {
	 
	 	
	   $this->db->where($conditions);
	   $this->db->from('infra_state_master');
	   $this->db->join('infra_country_master', 'infra_country_master.id = infra_state_master.country_id','left');
	   $this->db->join('infra_status_master', 'infra_status_master.id = infra_state_master.status','left');
       $this->db->select('infra_state_master.zone_id as id,infra_state_master.country_id,infra_state_master.status,infra_state_master.code,infra_state_master.name,infra_status_master.name as status_name,infra_country_master.name as country_name');
	   
	   $result = $this->db->get()->row();
       return $result; 
	 }
	 
	 
	 
	 
	 function insertState($insertData=array())
	 {
	  $this->db->insert('infra_state_master', $insertData);
	 return 'success';
	 }
	 
	 
	 function editState($insertData=array(),$id)
	 {
	 $this->db->where('zone_id',$id);
	 $this->db->update('infra_state_master',$insertData);
	 
	 
	 
	 return 'success';
	 }
	 
	 
	 
	 function deleteState($id)
	 {
	  $data = array('status' => '4');  
	   
	 $this->db->where('zone_id',$id);
	 $this->db->update('infra_state_master',$data);
	 return 'success';
	 }
	 
	 
}
	
	
	
	
	
	
	
	
	
	
	
	
	
?>