<?php
/**
 * Reverse bidding system User_model Class
 *
 * helps to achieve common tasks related to the site like flash message formats,pagination variables.
 *
 * @package		Reverse bidding system
 * @subpackage	Models
 * @category	Common_model 
 * @author		FreeLance PHP Script Team
 * @version		Version 1.0
 * @link		http://www.freelancephpscript.com
 
  <Reverse bidding system> 
    Copyright (C) <2009>  <FreeLance PHP Script>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
    If you want more information, please email me at support@freelancephpscript.com or 
    contact us from http://www.freelancephpscript.com/contact 
 
 
 */
	 class Loyality_model extends CI_Model {
	 
 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
	function insertLoyality($insertData=array())
	 {
	  
	  // insert into infra_description_master
	 $description_tbl_data = array(
     'name' => $insertData['name'],
     'description' => $insertData['description'],
	 
     );
	 
	 $this->db->insert('infra_description_master', $description_tbl_data);
	 $description_id = $this->db->insert_id();
	 
	 // insert into infra_file_master	
	 $filemaster_tbl_data = array(
	 'file_name' 	=> $insertData['logo_name'],
     'file_type' 	=> $insertData['logo_type'],
     'path' 		=> $insertData['logo_path'],
	 'file_size'	=> $insertData['logo_size']
     );
	 $this->db->insert('infra_file_master', $filemaster_tbl_data);
	 $filemaster_id = $this->db->insert_id();
	 
	 
	 // insert into property_master	
	 $loyality_tbl_data = array(
     'description_id' => $description_id,
     'sort_order' => $insertData['sort_order'],
	 'logo' => $filemaster_id,
	 'status' => $insertData['status']
     );
	 $this->db->insert('loyality_master', $loyality_tbl_data);
	 $loyality_id = $this->db->insert_id();
	 // insert into gallery_to_property	(photo)
	 
	 foreach($insertData['properties'] as $key):
	  $loyalityToproperty_data = array(
      'property_id' =>  $key,
      'loyality_id' => $loyality_id
	  );
	  $this->db->insert('loyality_to_property', $loyalityToproperty_data);
	  endforeach;
	 
	 
	 foreach ($insertData['photo'] as $key1){
	 $gallery_to_loyality_tbl_data1 = array(
	 'gallery_id' => $key1,
     'loyality_id' => $loyality_id
      );
	 $this->db->insert('gallery_to_loyality', $gallery_to_loyality_tbl_data1);
	 }
	 // insert into gallery_to_property	(video)
	 foreach ($insertData['video'] as $key2){
	 $gallery_to_loyality_tbl_data2 = array(
	 'gallery_id' => $key2,
     'loyality_id' => $loyality_id
      );
	 $this->db->insert('gallery_to_loyality', $gallery_to_loyality_tbl_data2);
	 }
	 
	 
	 
	 
	 
	 return 'success';
	 }
	 
	 
	 
	 
	 function getLoyalities($conditions)
     { 
	 $this->db->where($conditions);
	 $this->db->from('loyality_master');
	 $this->db->join('infra_description_master', 'loyality_master.description_id = infra_description_master.id','left');
	 $this->db->join('infra_status_master', 'infra_status_master.id = loyality_master.status','left');
	 $this->db->join('infra_file_master', 'infra_file_master.id = loyality_master.logo','left');
	 
	 $this->db->select('loyality_master.id,loyality_master.status,loyality_master.sort_order,infra_description_master.name,infra_description_master.description, infra_status_master.name as status_name,infra_file_master.id as fileid, infra_file_master.file_name, infra_file_master.file_type, infra_file_master.path, infra_file_master.file_size');
	 
	 $result=$this->db->get()->result();
	 
	
	 return $result;
	 
    }
	
	function getLoyalityByid($conditions)
     { 
	 $this->db->where($conditions);
	 $this->db->from('loyality_master');
	 $this->db->join('infra_description_master', 'loyality_master.description_id = infra_description_master.id','left');
	 $this->db->join('infra_status_master', 'infra_status_master.id = loyality_master.status','left');
	 $this->db->join('infra_file_master', 'infra_file_master.id = loyality_master.logo','left');
	 
	 $this->db->select('loyality_master.id,loyality_master.status,loyality_master.sort_order,infra_description_master.name,infra_description_master.description, infra_status_master.name as status_name,infra_file_master.id as fileid, infra_file_master.file_name, infra_file_master.file_type, infra_file_master.path, infra_file_master.file_size');
	 
	 $result=$this->db->get()->row();
	 
	 /* echo '<pre>';
	 print_r($result);
	 echo '</pre>';	
	 exit(); */
	 return $result;
	 
    }
	
	
	
	function getPropertiesBy_Loyality($conditions)
     { 
	 $this->db->where($conditions);
	 $this->db->from('loyality_to_property');
	
	 $this->db->select('loyality_to_property.id,loyality_to_property.property_id,loyality_to_property.loyality_id');
	 
	 $result=$this->db->get()->result();

	 return $result;
	 
    }
	
	
	 
	 function getContactmaster_info($conditions)
     { 
	    $query = $this->db->get_where('infra_contact_master', $conditions)->result();
		return $query; 
		 /*echo '<pre>';
	 print_r($query);
	 echo '</pre>';	
	 exit();*/
	 
	    /*$this->db->where($conditions);
	    $this->db->from('infra_contact_master');
	    $this->db->select();
		$result=$this->db->get()->result();
		return $result; 
		
		 echo '<pre>';
	 print_r($result);
	 echo '</pre>';	
	 exit();*/
	 }
	 
	 
	 
	 
	 function editLoyality($insertData=array())
	 {
	 
	   $description_tbl_data = array(
     'name' => $insertData['name'],
     'description' => $insertData['description']
	 );
	 $loyaity_details = $this->db->get_where('loyality_master', array('loyality_master.id' => $insertData['id']))->row();
	
	 
	 $description_id = $loyaity_details->description_id;
	 
	 // update infra_description_master
	 $this->db->where('id',$description_id);
	 $this->db->update('infra_description_master',$description_tbl_data);
	 
	 $filemaster_tbl_data = array(
	 'file_name' => $insertData['logo_name'],
     'file_type' => $insertData['logo_type'],
     'path' => $insertData['logo_path'],
	 'file_size' => $insertData['logo_size']
     );
	 
	 $this->db->where('id',$insertData['loyality_logo']);
	 $this->db->update('infra_file_master',$filemaster_tbl_data);
	 
	 
	 $loyality_master_data = array(
	 'status' => $insertData['status'],
     'sort_order' => $insertData['sort_order'],
	 );
	 
	 // update property_master
	 $this->db->where('id',$insertData['id']);
	 $this->db->update('loyality_master',$loyality_master_data);
	 
      
	  $this->db->delete('gallery_to_loyality', array('loyality_id' => $insertData['id'])); 
	 
	 
	 if($insertData['photo']){
	  foreach ($insertData['photo'] as $key1){
	  $gallery_to_loyality_data1 = array(
	 'gallery_id' => $key1,
     'loyality_id' => $insertData['id']
      );
	  $this->db->insert('gallery_to_loyality', $gallery_to_loyality_data1);
	   }
	  
	  }
	  
	  
	 if($insertData['video']){
	 	 
	 foreach ($insertData['video'] as $key2){ 
	  $gallery_to_loyality_data2 = array(
	 'gallery_id' => $key2,
     'loyality_id' => $insertData['id']
      );
	  $this->db->insert('gallery_to_loyality', $gallery_to_loyality_data2);
	  }
	  }
	  
	  $this->db->delete('loyality_to_property', array('loyality_id' => $insertData['id'])); 
	  
	  if($insertData['properties']){
	  foreach ($insertData['properties'] as $key3){ 
	  $loyality_to_property_data = array(
	 'property_id' 	=> $key3,
     'loyality_id' 	=> $insertData['id']
      );
	  $this->db->insert('loyality_to_property', $loyality_to_property_data);
	  }
	  }
	 return 'success';
	 }
	 
	 
	  function deleteLoyality($id)
	 {
	  $data = array('status' => '4');  
	   
	 $this->db->where('id',$id);
	 $this->db->update('loyality_master',$data);
	 return 'success';
	 }
	
	 
}
	
	
	
	
	
	
	
	
	
	
	
	
	
?>